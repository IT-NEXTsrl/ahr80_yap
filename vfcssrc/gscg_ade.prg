* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_ade                                                        *
*              Comunicazione lettere di intento emesse                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-12-02                                                      *
* Last revis.: 2017-10-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_ade"))

* --- Class definition
define class tgscg_ade as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 784
  Height = 543+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-10-17"
  HelpContextID=92939625
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=84

  * --- Constant Properties
  GEDICEME_IDX = 0
  AZIENDA_IDX = 0
  TITOLARI_IDX = 0
  DAT_RAPP_IDX = 0
  cFile = "GEDICEME"
  cKeySelect = "DGSERIAL"
  cKeyWhere  = "DGSERIAL=this.w_DGSERIAL"
  cKeyWhereODBC = '"DGSERIAL="+cp_ToStrODBC(this.w_DGSERIAL)';

  cKeyWhereODBCqualified = '"GEDICEME.DGSERIAL="+cp_ToStrODBC(this.w_DGSERIAL)';

  cPrg = "gscg_ade"
  cComment = "Comunicazione lettere di intento emesse"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DGSERIAL = space(10)
  w_DGDATCOM = ctod('  /  /  ')
  w_DGDESAGG = space(254)
  w_CODAZI = space(5)
  w_AZCOFAZI = space(16)
  w_AZPIVAZI = space(12)
  w_AZPERAZI = space(1)
  w_AZTELEFO = space(18)
  w_AZ_EMAIL = space(50)
  w_CODAZI2 = space(5)
  w_TELEFO1 = space(18)
  w_COGTIT1 = space(25)
  w_NOMTIT1 = space(25)
  w_SESSO1 = space(1)
  w_LOCTIT1 = space(30)
  w_PROTIT1 = space(2)
  w_DATNAS1 = ctod('  /  /  ')
  w_DGCODFIS = space(16)
  o_DGCODFIS = space(16)
  w_DGPARIVA = space(11)
  w_DGCOGNOM = space(24)
  w_DGFONOME = space(20)
  w_DG_SESSO = space(1)
  w_DGCOMUNE = space(40)
  w_DGPRNASC = space(2)
  w_DGDTNASC = ctod('  /  /  ')
  w_DGDENOMI = space(60)
  w_DG_EMAIL = space(100)
  w_DGRFTELE = space(12)
  w_DGFLGCOR = space(1)
  o_DGFLGCOR = space(1)
  w_DGPROTEC = space(17)
  o_DGPROTEC = space(17)
  w_DGPRODOC = 0
  o_DGPRODOC = 0
  w_DGRAPFIR = space(1)
  o_DGRAPFIR = space(1)
  w_RAPFIRM = space(10)
  w_RFCOMNAS = space(40)
  w_RFPRONAS = space(2)
  w_RFCODFIS = space(16)
  w_RFCOGNOM = space(40)
  w_RF__NOME = space(40)
  w_RF_SESSO = space(1)
  w_RFDATNAS = ctod('  /  /  ')
  w_DGRFCFIS = space(16)
  w_DGRFPIVA = space(11)
  w_DGCODCAR = space(2)
  o_DGCODCAR = space(2)
  w_DGRFCOGN = space(24)
  w_DGRFNOME = space(20)
  w_DGRFSESS = space(1)
  w_DGRFDTNS = ctod('  /  /  ')
  w_DGRFCOMN = space(40)
  w_DGRFPRNS = space(2)
  w_DGFIRDIC = space(1)
  w_DirName = space(200)
  w_DGTIPPLA = space(1)
  w_DGDICIVA = space(1)
  w_DGESPORT = space(1)
  w_DGCESINT = space(1)
  w_DGCES_SM = space(1)
  w_DGOPEASS = space(1)
  w_DGOPESTR = space(1)
  w_DGTIPFOR = space(2)
  o_DGTIPFOR = space(2)
  w_DGFISINT = space(16)
  o_DGFISINT = space(16)
  w_DGDATIMP = ctod('  /  /  ')
  w_DGFIRINT = space(1)
  o_DGFIRINT = space(1)
  w_DGTIPGEN = space(1)
  w_DGANNDIC = 0
  o_DGANNDIC = 0
  w_DGDATINI = ctod('  /  /  ')
  o_DGDATINI = ctod('  /  /  ')
  w_DGDATFIN = ctod('  /  /  ')
  o_DGDATFIN = ctod('  /  /  ')
  w_DGSEPARA = space(1)
  w_DGGENERA = space(1)
  w_DGNOMFIL = space(254)
  o_DGNOMFIL = space(254)
  w_NOGENERA = space(1)
  w_NOCODFIS = space(100)
  w_NOPARIVA = space(100)
  w_NOCOGNOM = space(100)
  w_NO__NOME = space(100)
  w_NOIMPOPE = space(100)
  w_NOPERIODO = space(100)
  w_DISERIAL = space(15)
  o_DISERIAL = space(15)
  w_ERROREGENE = space(0)
  w_DICODICE = space(15)
  w_DIPROTEC = space(17)
  w_DIPRODOC = 0
  w_PATH = space(150)
  o_PATH = space(150)
  w_LPATH = space(10)
  w_HASEVENT = .F.

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_DGSERIAL = this.W_DGSERIAL

  * --- Children pointers
  Gscg_Mde = .NULL.
  w_CalcZoom = .NULL.
  w_IntZoom = .NULL.
  w_RettZoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscg_ade
  Proc EcpDelete()
  	  This.NotifyEvent('HasEvent')
       if !This.w_HASEVENT
           L_Mess='Impossibile cancellare.%0Esistono dichiarazioni di intento protocollate'
           ah_ERRORMSG(L_MESS,48)
       Else
          DoDefault()
       Endif
  Endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GEDICEME','gscg_ade')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_adePag1","gscg_ade",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati del dichiarante")
      .Pages(1).HelpContextID = 247649500
      .Pages(2).addobject("oPag","tgscg_adePag2","gscg_ade",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Rappresentante firmatario/firma")
      .Pages(2).HelpContextID = 252073046
      .Pages(3).addobject("oPag","tgscg_adePag3","gscg_ade",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Dichiarazione")
      .Pages(3).HelpContextID = 154786103
      .Pages(4).addobject("oPag","tgscg_adePag4","gscg_ade",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Quadro A/Dati trasmissione")
      .Pages(4).HelpContextID = 223645176
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_CalcZoom = this.oPgFrm.Pages(3).oPag.CalcZoom
      this.w_IntZoom = this.oPgFrm.Pages(3).oPag.IntZoom
      this.w_RettZoom = this.oPgFrm.Pages(3).oPag.RettZoom
      DoDefault()
    proc Destroy()
      this.w_CalcZoom = .NULL.
      this.w_IntZoom = .NULL.
      this.w_RettZoom = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='TITOLARI'
    this.cWorkTables[3]='DAT_RAPP'
    this.cWorkTables[4]='GEDICEME'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GEDICEME_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GEDICEME_IDX,3]
  return

  function CreateChildren()
    this.Gscg_Mde = CREATEOBJECT('stdDynamicChild',this,'Gscg_Mde',this.oPgFrm.Page4.oPag.oLinkPC_4_33)
    this.Gscg_Mde.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.Gscg_Mde)
      this.Gscg_Mde.DestroyChildrenChain()
      this.Gscg_Mde=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_33')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.Gscg_Mde.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.Gscg_Mde.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.Gscg_Mde.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.Gscg_Mde.SetKey(;
            .w_DGSERIAL,"DDSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .Gscg_Mde.ChangeRow(this.cRowID+'      1',1;
             ,.w_DGSERIAL,"DDSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.Gscg_Mde)
        i_f=.Gscg_Mde.BuildFilter()
        if !(i_f==.Gscg_Mde.cQueryFilter)
          i_fnidx=.Gscg_Mde.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.Gscg_Mde.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.Gscg_Mde.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.Gscg_Mde.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.Gscg_Mde.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DGSERIAL = NVL(DGSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from GEDICEME where DGSERIAL=KeySet.DGSERIAL
    *
    i_nConn = i_TableProp[this.GEDICEME_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GEDICEME_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GEDICEME')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GEDICEME.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GEDICEME '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DGSERIAL',this.w_DGSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_AZCOFAZI = space(16)
        .w_AZPIVAZI = space(12)
        .w_AZPERAZI = space(1)
        .w_AZTELEFO = space(18)
        .w_AZ_EMAIL = space(50)
        .w_CODAZI2 = i_CODAZI
        .w_TELEFO1 = space(18)
        .w_COGTIT1 = space(25)
        .w_NOMTIT1 = space(25)
        .w_SESSO1 = space(1)
        .w_LOCTIT1 = space(30)
        .w_PROTIT1 = space(2)
        .w_DATNAS1 = ctod("  /  /  ")
        .w_RAPFIRM = i_Codazi
        .w_RFCOMNAS = space(40)
        .w_RFPRONAS = space(2)
        .w_RFCODFIS = space(16)
        .w_RFCOGNOM = space(40)
        .w_RF__NOME = space(40)
        .w_RF_SESSO = space(1)
        .w_RFDATNAS = ctod("  /  /  ")
        .w_DirName = sys(5)+sys(2003)+'\'
        .w_ERROREGENE = space(0)
        .w_PATH = sys(5)+sys(2003)+'\'
        .w_LPATH = space(10)
        .w_HASEVENT = .f.
        .w_DGSERIAL = NVL(DGSERIAL,space(10))
        .op_DGSERIAL = .w_DGSERIAL
        .w_DGDATCOM = NVL(cp_ToDate(DGDATCOM),ctod("  /  /  "))
        .w_DGDESAGG = NVL(DGDESAGG,space(254))
          .link_1_4('Load')
          .link_1_10('Load')
        .w_DGCODFIS = NVL(DGCODFIS,space(16))
        .w_DGPARIVA = NVL(DGPARIVA,space(11))
        .w_DGCOGNOM = NVL(DGCOGNOM,space(24))
        .w_DGFONOME = NVL(DGFONOME,space(20))
        .w_DG_SESSO = NVL(DG_SESSO,space(1))
        .w_DGCOMUNE = NVL(DGCOMUNE,space(40))
        .w_DGPRNASC = NVL(DGPRNASC,space(2))
        .w_DGDTNASC = NVL(cp_ToDate(DGDTNASC),ctod("  /  /  "))
        .w_DGDENOMI = NVL(DGDENOMI,space(60))
        .w_DG_EMAIL = NVL(DG_EMAIL,space(100))
        .w_DGRFTELE = NVL(DGRFTELE,space(12))
        .w_DGFLGCOR = NVL(DGFLGCOR,space(1))
        .w_DGPROTEC = NVL(DGPROTEC,space(17))
        .w_DGPRODOC = NVL(DGPRODOC,0)
        .w_DGRAPFIR = NVL(DGRAPFIR,space(1))
          .link_2_2('Load')
        .w_DGRFCFIS = NVL(DGRFCFIS,space(16))
        .w_DGRFPIVA = NVL(DGRFPIVA,space(11))
        .w_DGCODCAR = NVL(DGCODCAR,space(2))
        .w_DGRFCOGN = NVL(DGRFCOGN,space(24))
        .w_DGRFNOME = NVL(DGRFNOME,space(20))
        .w_DGRFSESS = NVL(DGRFSESS,space(1))
        .w_DGRFDTNS = NVL(cp_ToDate(DGRFDTNS),ctod("  /  /  "))
        .w_DGRFCOMN = NVL(DGRFCOMN,space(40))
        .w_DGRFPRNS = NVL(DGRFPRNS,space(2))
        .w_DGFIRDIC = NVL(DGFIRDIC,space(1))
        .w_DGTIPPLA = NVL(DGTIPPLA,space(1))
        .w_DGDICIVA = NVL(DGDICIVA,space(1))
        .w_DGESPORT = NVL(DGESPORT,space(1))
        .w_DGCESINT = NVL(DGCESINT,space(1))
        .w_DGCES_SM = NVL(DGCES_SM,space(1))
        .w_DGOPEASS = NVL(DGOPEASS,space(1))
        .w_DGOPESTR = NVL(DGOPESTR,space(1))
        .w_DGTIPFOR = NVL(DGTIPFOR,space(2))
        .w_DGFISINT = NVL(DGFISINT,space(16))
        .w_DGDATIMP = NVL(cp_ToDate(DGDATIMP),ctod("  /  /  "))
        .w_DGFIRINT = NVL(DGFIRINT,space(1))
        .w_DGTIPGEN = NVL(DGTIPGEN,space(1))
        .w_DGANNDIC = NVL(DGANNDIC,0)
        .w_DGDATINI = NVL(cp_ToDate(DGDATINI),ctod("  /  /  "))
        .w_DGDATFIN = NVL(cp_ToDate(DGDATFIN),ctod("  /  /  "))
        .w_DGSEPARA = NVL(DGSEPARA,space(1))
        .oPgFrm.Page3.oPag.CalcZoom.Calculate()
        .w_DGGENERA = NVL(DGGENERA,space(1))
        .w_DGNOMFIL = NVL(DGNOMFIL,space(254))
        .w_NOGENERA = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Nogenera'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Nogenera'), ' ')))
        .w_NOCODFIS = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Nocodfis'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Nocodfis'), ' ')))
        .w_NOPARIVA = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Nopariva'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Nopariva'), ' ')))
        .w_NOCOGNOM = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Nocognom'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Nocognom'), ' ')))
        .w_NO__NOME = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('No__Nome'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('No__Nome'), ' ')))
        .w_NOIMPOPE = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Noimpope'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Noimpope'), ' ')))
        .w_NOPERIODO = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Noperiodo'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Noperiodo'), ' ')))
        .w_DISERIAL = iif(.cFunction='Query' or .w_Dggenera='S',Nvl(.w_IntZoom.getVar('Diserial'),' '),iif(.w_Dgflgcor='1',Nvl(.w_RettZoom.getVar('Diserial'),' '),Nvl(.w_CalcZoom.getVar('Diserial'),' ')))
        .oPgFrm.Page3.oPag.IntZoom.Calculate()
        .w_DICODICE = iif(.cFunction='Query' or .w_Dggenera='S',Nvl(.w_IntZoom.getVar('Dicodice'),' '),iif(.w_Dgflgcor='1',Nvl(.w_RettZoom.getVar('Dicodice'),' '),Nvl(.w_CalcZoom.getVar('Dicodice'),' ')))
        .w_DIPROTEC = Alltrim(Nvl(.w_IntZoom.getVar('Diprotec'),' '))
        .w_DIPRODOC = Nvl(.w_IntZoom.getVar('Diprodoc'),0)
        .oPgFrm.Page3.oPag.RettZoom.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_37.Calculate('','',RGB(255,0,0))
        .oPgFrm.Page3.oPag.oObj_3_38.Calculate('','',RGB(0,255,0))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'GEDICEME')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page3.oPag.oBtn_3_6.enabled = this.oPgFrm.Page3.oPag.oBtn_3_6.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_18.enabled = this.oPgFrm.Page3.oPag.oBtn_3_18.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_19.enabled = this.oPgFrm.Page3.oPag.oBtn_3_19.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_20.enabled = this.oPgFrm.Page3.oPag.oBtn_3_20.mCond()
      this.oPgFrm.Page4.oPag.oBtn_4_32.enabled = this.oPgFrm.Page4.oPag.oBtn_4_32.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DGSERIAL = space(10)
      .w_DGDATCOM = ctod("  /  /  ")
      .w_DGDESAGG = space(254)
      .w_CODAZI = space(5)
      .w_AZCOFAZI = space(16)
      .w_AZPIVAZI = space(12)
      .w_AZPERAZI = space(1)
      .w_AZTELEFO = space(18)
      .w_AZ_EMAIL = space(50)
      .w_CODAZI2 = space(5)
      .w_TELEFO1 = space(18)
      .w_COGTIT1 = space(25)
      .w_NOMTIT1 = space(25)
      .w_SESSO1 = space(1)
      .w_LOCTIT1 = space(30)
      .w_PROTIT1 = space(2)
      .w_DATNAS1 = ctod("  /  /  ")
      .w_DGCODFIS = space(16)
      .w_DGPARIVA = space(11)
      .w_DGCOGNOM = space(24)
      .w_DGFONOME = space(20)
      .w_DG_SESSO = space(1)
      .w_DGCOMUNE = space(40)
      .w_DGPRNASC = space(2)
      .w_DGDTNASC = ctod("  /  /  ")
      .w_DGDENOMI = space(60)
      .w_DG_EMAIL = space(100)
      .w_DGRFTELE = space(12)
      .w_DGFLGCOR = space(1)
      .w_DGPROTEC = space(17)
      .w_DGPRODOC = 0
      .w_DGRAPFIR = space(1)
      .w_RAPFIRM = space(10)
      .w_RFCOMNAS = space(40)
      .w_RFPRONAS = space(2)
      .w_RFCODFIS = space(16)
      .w_RFCOGNOM = space(40)
      .w_RF__NOME = space(40)
      .w_RF_SESSO = space(1)
      .w_RFDATNAS = ctod("  /  /  ")
      .w_DGRFCFIS = space(16)
      .w_DGRFPIVA = space(11)
      .w_DGCODCAR = space(2)
      .w_DGRFCOGN = space(24)
      .w_DGRFNOME = space(20)
      .w_DGRFSESS = space(1)
      .w_DGRFDTNS = ctod("  /  /  ")
      .w_DGRFCOMN = space(40)
      .w_DGRFPRNS = space(2)
      .w_DGFIRDIC = space(1)
      .w_DirName = space(200)
      .w_DGTIPPLA = space(1)
      .w_DGDICIVA = space(1)
      .w_DGESPORT = space(1)
      .w_DGCESINT = space(1)
      .w_DGCES_SM = space(1)
      .w_DGOPEASS = space(1)
      .w_DGOPESTR = space(1)
      .w_DGTIPFOR = space(2)
      .w_DGFISINT = space(16)
      .w_DGDATIMP = ctod("  /  /  ")
      .w_DGFIRINT = space(1)
      .w_DGTIPGEN = space(1)
      .w_DGANNDIC = 0
      .w_DGDATINI = ctod("  /  /  ")
      .w_DGDATFIN = ctod("  /  /  ")
      .w_DGSEPARA = space(1)
      .w_DGGENERA = space(1)
      .w_DGNOMFIL = space(254)
      .w_NOGENERA = space(1)
      .w_NOCODFIS = space(100)
      .w_NOPARIVA = space(100)
      .w_NOCOGNOM = space(100)
      .w_NO__NOME = space(100)
      .w_NOIMPOPE = space(100)
      .w_NOPERIODO = space(100)
      .w_DISERIAL = space(15)
      .w_ERROREGENE = space(0)
      .w_DICODICE = space(15)
      .w_DIPROTEC = space(17)
      .w_DIPRODOC = 0
      .w_PATH = space(150)
      .w_LPATH = space(10)
      .w_HASEVENT = .f.
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_DGDATCOM = i_Datsys
          .DoRTCalc(3,3,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_CODAZI))
          .link_1_4('Full')
          endif
          .DoRTCalc(5,9,.f.)
        .w_CODAZI2 = i_CODAZI
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_CODAZI2))
          .link_1_10('Full')
          endif
          .DoRTCalc(11,17,.f.)
        .w_DGCODFIS = IIF(LEN(ALLTRIM(.w_AZCOFAZI))<=16, LEFT(ALLTRIM(.w_AZCOFAZI),16), RIGHT(ALLTRIM(.w_AZCOFAZI),16))
        .w_DGPARIVA = left(.w_AZPIVAZI, 11)
        .w_DGCOGNOM = LEFT(iif(.w_AZPERAZI='S', UPPER(.w_COGTIT1), space(24)), 24)
        .w_DGFONOME = LEFT(iif(.w_AZPERAZI='S', UPPER(.w_NOMTIT1), space(20)), 20)
        .w_DG_SESSO = iif(.w_AZPERAZI='S' and not empty(.w_SESSO1), .w_SESSO1, 'M')
        .w_DGCOMUNE = left(iif(.w_AZPERAZI='S', UPPER(.w_LOCTIT1), space(40)), 40)
        .w_DGPRNASC = iif(.w_AZPERAZI='S', UPPER(.w_PROTIT1), space(2))
        .w_DGDTNASC = iif(.w_AZPERAZI='S', .w_DATNAS1, CTOD("  -  -    "))
        .w_DGDENOMI = LEFT(iif(.w_AZPERAZI<>'S', UPPER(g_RAGAZI), space(60)), 60)
        .w_DG_EMAIL = Left(UPPER(.w_AZ_EMAIL), 100)
        .w_DGRFTELE = left(iif(.w_AZPERAZI='S', .w_TELEFO1, .w_AZTELEFO), 12)
        .w_DGFLGCOR = '0'
        .w_DGPROTEC = SPACE(17)
        .w_DGPRODOC = 0
        .w_DGRAPFIR = iif(.w_Azperazi<>'S','S','N')
        .w_RAPFIRM = i_Codazi
        .DoRTCalc(33,33,.f.)
          if not(empty(.w_RAPFIRM))
          .link_2_2('Full')
          endif
          .DoRTCalc(34,40,.f.)
        .w_DGRFCFIS = iif(.w_DgRapFir='S',.w_Rfcodfis,Space(16))
        .w_DGRFPIVA = iif(.w_DgRapFir='S' And .w_Dgcodcar $ '169',.w_Dgrfpiva,Space(11))
        .w_DGCODCAR = '1'
        .w_DGRFCOGN = iif(.w_DgRapFir='N',space(20),Left(.w_Rfcognom,24))
        .w_DGRFNOME = iif(.w_DgRapFir='N',space(20),Left(.w_Rf__Nome,20))
        .w_DGRFSESS = iif(.w_DgRapFir='N','M',.w_Rf_Sesso)
        .w_DGRFDTNS = iif(.w_DgRapFir='N',Ctod('  -  -    '),.w_Rfdatnas)
        .w_DGRFCOMN = iif(.w_DgRapFir='N',space(40),.w_Rfcomnas)
        .w_DGRFPRNS = iif(.w_DgRapFir='N',space(2),.w_Rfpronas)
        .w_DGFIRDIC = '0'
        .w_DirName = sys(5)+sys(2003)+'\'
        .w_DGTIPPLA = '1'
        .w_DGDICIVA = '0'
        .w_DGESPORT = '0'
        .w_DGCESINT = '0'
        .w_DGCES_SM = '0'
        .w_DGOPEASS = '0'
        .w_DGOPESTR = '0'
        .w_DGTIPFOR = '01'
        .w_DGFISINT = iif(.w_Dgtipfor='01', space(16), .w_Dgfisint)
        .w_DGDATIMP = iif(Empty(.w_Dgfisint) And .w_Dgfirint='0', Ctod('  -  -    '), .w_Dgdatimp)
        .w_DGFIRINT = '0'
        .w_DGTIPGEN = 'M'
        .w_DGANNDIC = Year(i_datsys)
        .w_DGDATINI = cp_CharToDate('01-'+'01-'+Str(.w_DGANNDIC))
        .w_DGDATFIN = cp_CharToDate('31-'+'12-'+Str(.w_DGANNDIC))
        .oPgFrm.Page3.oPag.CalcZoom.Calculate()
          .DoRTCalc(67,67,.f.)
        .w_DGGENERA = 'N'
          .DoRTCalc(69,69,.f.)
        .w_NOGENERA = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Nogenera'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Nogenera'), ' ')))
        .w_NOCODFIS = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Nocodfis'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Nocodfis'), ' ')))
        .w_NOPARIVA = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Nopariva'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Nopariva'), ' ')))
        .w_NOCOGNOM = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Nocognom'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Nocognom'), ' ')))
        .w_NO__NOME = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('No__Nome'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('No__Nome'), ' ')))
        .w_NOIMPOPE = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Noimpope'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Noimpope'), ' ')))
        .w_NOPERIODO = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Noperiodo'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Noperiodo'), ' ')))
        .w_DISERIAL = iif(.cFunction='Query' or .w_Dggenera='S',Nvl(.w_IntZoom.getVar('Diserial'),' '),iif(.w_Dgflgcor='1',Nvl(.w_RettZoom.getVar('Diserial'),' '),Nvl(.w_CalcZoom.getVar('Diserial'),' ')))
        .oPgFrm.Page3.oPag.IntZoom.Calculate()
          .DoRTCalc(78,78,.f.)
        .w_DICODICE = iif(.cFunction='Query' or .w_Dggenera='S',Nvl(.w_IntZoom.getVar('Dicodice'),' '),iif(.w_Dgflgcor='1',Nvl(.w_RettZoom.getVar('Dicodice'),' '),Nvl(.w_CalcZoom.getVar('Dicodice'),' ')))
        .w_DIPROTEC = Alltrim(Nvl(.w_IntZoom.getVar('Diprotec'),' '))
        .w_DIPRODOC = Nvl(.w_IntZoom.getVar('Diprodoc'),0)
        .oPgFrm.Page3.oPag.RettZoom.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_37.Calculate('','',RGB(255,0,0))
        .oPgFrm.Page3.oPag.oObj_3_38.Calculate('','',RGB(0,255,0))
        .w_PATH = sys(5)+sys(2003)+'\'
      endif
    endwith
    cp_BlankRecExtFlds(this,'GEDICEME')
    this.DoRTCalc(83,84,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page3.oPag.oBtn_3_6.enabled = this.oPgFrm.Page3.oPag.oBtn_3_6.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_18.enabled = this.oPgFrm.Page3.oPag.oBtn_3_18.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_19.enabled = this.oPgFrm.Page3.oPag.oBtn_3_19.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_20.enabled = this.oPgFrm.Page3.oPag.oBtn_3_20.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_32.enabled = this.oPgFrm.Page4.oPag.oBtn_4_32.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_ade
    * Pulisco gli zoom presenti in anagrafica
    ZAP IN ( this.w_CalcZoom.cCursor )
    ZAP IN ( this.w_IntZoom.cCursor )
    ZAP IN ( this.w_RettZoom.cCursor )
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GEDICEME_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GEDICEME_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"DGDIE","i_codazi,w_DGSERIAL")
      .op_codazi = .w_codazi
      .op_DGSERIAL = .w_DGSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDGDATCOM_1_2.enabled = i_bVal
      .Page1.oPag.oDGDESAGG_1_3.enabled = i_bVal
      .Page1.oPag.oDGCODFIS_1_18.enabled = i_bVal
      .Page1.oPag.oDGPARIVA_1_19.enabled = i_bVal
      .Page1.oPag.oDGCOGNOM_1_20.enabled = i_bVal
      .Page1.oPag.oDGFONOME_1_21.enabled = i_bVal
      .Page1.oPag.oDG_SESSO_1_22.enabled = i_bVal
      .Page1.oPag.oDGCOMUNE_1_23.enabled = i_bVal
      .Page1.oPag.oDGPRNASC_1_24.enabled = i_bVal
      .Page1.oPag.oDGDTNASC_1_25.enabled = i_bVal
      .Page1.oPag.oDGDENOMI_1_26.enabled = i_bVal
      .Page1.oPag.oDG_EMAIL_1_27.enabled = i_bVal
      .Page1.oPag.oDGRFTELE_1_28.enabled = i_bVal
      .Page1.oPag.oDGFLGCOR_1_29.enabled = i_bVal
      .Page1.oPag.oDGPROTEC_1_30.enabled = i_bVal
      .Page1.oPag.oDGPRODOC_1_31.enabled = i_bVal
      .Page2.oPag.oDGRAPFIR_2_1.enabled = i_bVal
      .Page2.oPag.oDGRFCFIS_2_10.enabled = i_bVal
      .Page2.oPag.oDGRFPIVA_2_11.enabled = i_bVal
      .Page2.oPag.oDGCODCAR_2_12.enabled = i_bVal
      .Page2.oPag.oDGRFCOGN_2_14.enabled = i_bVal
      .Page2.oPag.oDGRFNOME_2_16.enabled = i_bVal
      .Page2.oPag.oDGRFSESS_2_17.enabled = i_bVal
      .Page2.oPag.oDGRFDTNS_2_19.enabled = i_bVal
      .Page2.oPag.oDGRFCOMN_2_21.enabled = i_bVal
      .Page2.oPag.oDGRFPRNS_2_23.enabled = i_bVal
      .Page2.oPag.oDGFIRDIC_2_24.enabled = i_bVal
      .Page4.oPag.oDGTIPPLA_4_2.enabled = i_bVal
      .Page4.oPag.oDGDICIVA_4_3.enabled = i_bVal
      .Page4.oPag.oDGESPORT_4_4.enabled = i_bVal
      .Page4.oPag.oDGCESINT_4_5.enabled = i_bVal
      .Page4.oPag.oDGCES_SM_4_6.enabled = i_bVal
      .Page4.oPag.oDGOPEASS_4_7.enabled = i_bVal
      .Page4.oPag.oDGOPESTR_4_8.enabled = i_bVal
      .Page4.oPag.oDGTIPFOR_4_9.enabled = i_bVal
      .Page4.oPag.oDGFISINT_4_10.enabled = i_bVal
      .Page4.oPag.oDGDATIMP_4_14.enabled = i_bVal
      .Page4.oPag.oDGFIRINT_4_16.enabled = i_bVal
      .Page4.oPag.oDGTIPGEN_4_17.enabled = i_bVal
      .Page3.oPag.oDGANNDIC_3_2.enabled = i_bVal
      .Page3.oPag.oDGDATINI_3_3.enabled = i_bVal
      .Page3.oPag.oDGDATFIN_3_4.enabled = i_bVal
      .Page3.oPag.oDGSEPARA_3_5.enabled = i_bVal
      .Page4.oPag.oDGNOMFIL_4_23.enabled = i_bVal
      .Page4.oPag.oPATH_4_36.enabled = i_bVal
      .Page3.oPag.oBtn_3_6.enabled = .Page3.oPag.oBtn_3_6.mCond()
      .Page4.oPag.oBtn_4_20.enabled = i_bVal
      .Page3.oPag.oBtn_3_18.enabled = .Page3.oPag.oBtn_3_18.mCond()
      .Page3.oPag.oBtn_3_19.enabled = .Page3.oPag.oBtn_3_19.mCond()
      .Page3.oPag.oBtn_3_20.enabled = .Page3.oPag.oBtn_3_20.mCond()
      .Page4.oPag.oBtn_4_24.enabled = i_bVal
      .Page4.oPag.oBtn_4_32.enabled = .Page4.oPag.oBtn_4_32.mCond()
      .Page3.oPag.IntZoom.enabled = i_bVal
    endwith
    this.Gscg_Mde.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'GEDICEME',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.Gscg_Mde.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GEDICEME_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGSERIAL,"DGSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGDATCOM,"DGDATCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGDESAGG,"DGDESAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGCODFIS,"DGCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGPARIVA,"DGPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGCOGNOM,"DGCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGFONOME,"DGFONOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DG_SESSO,"DG_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGCOMUNE,"DGCOMUNE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGPRNASC,"DGPRNASC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGDTNASC,"DGDTNASC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGDENOMI,"DGDENOMI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DG_EMAIL,"DG_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGRFTELE,"DGRFTELE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGFLGCOR,"DGFLGCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGPROTEC,"DGPROTEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGPRODOC,"DGPRODOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGRAPFIR,"DGRAPFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGRFCFIS,"DGRFCFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGRFPIVA,"DGRFPIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGCODCAR,"DGCODCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGRFCOGN,"DGRFCOGN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGRFNOME,"DGRFNOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGRFSESS,"DGRFSESS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGRFDTNS,"DGRFDTNS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGRFCOMN,"DGRFCOMN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGRFPRNS,"DGRFPRNS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGFIRDIC,"DGFIRDIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGTIPPLA,"DGTIPPLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGDICIVA,"DGDICIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGESPORT,"DGESPORT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGCESINT,"DGCESINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGCES_SM,"DGCES_SM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGOPEASS,"DGOPEASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGOPESTR,"DGOPESTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGTIPFOR,"DGTIPFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGFISINT,"DGFISINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGDATIMP,"DGDATIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGFIRINT,"DGFIRINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGTIPGEN,"DGTIPGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGANNDIC,"DGANNDIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGDATINI,"DGDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGDATFIN,"DGDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGSEPARA,"DGSEPARA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGGENERA,"DGGENERA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DGNOMFIL,"DGNOMFIL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GEDICEME_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GEDICEME_IDX,2])
    i_lTable = "GEDICEME"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GEDICEME_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GEDICEME_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GEDICEME_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.GEDICEME_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"DGDIE","i_codazi,w_DGSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into GEDICEME
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GEDICEME')
        i_extval=cp_InsertValODBCExtFlds(this,'GEDICEME')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DGSERIAL,DGDATCOM,DGDESAGG,DGCODFIS,DGPARIVA"+;
                  ",DGCOGNOM,DGFONOME,DG_SESSO,DGCOMUNE,DGPRNASC"+;
                  ",DGDTNASC,DGDENOMI,DG_EMAIL,DGRFTELE,DGFLGCOR"+;
                  ",DGPROTEC,DGPRODOC,DGRAPFIR,DGRFCFIS,DGRFPIVA"+;
                  ",DGCODCAR,DGRFCOGN,DGRFNOME,DGRFSESS,DGRFDTNS"+;
                  ",DGRFCOMN,DGRFPRNS,DGFIRDIC,DGTIPPLA,DGDICIVA"+;
                  ",DGESPORT,DGCESINT,DGCES_SM,DGOPEASS,DGOPESTR"+;
                  ",DGTIPFOR,DGFISINT,DGDATIMP,DGFIRINT,DGTIPGEN"+;
                  ",DGANNDIC,DGDATINI,DGDATFIN,DGSEPARA,DGGENERA"+;
                  ",DGNOMFIL "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DGSERIAL)+;
                  ","+cp_ToStrODBC(this.w_DGDATCOM)+;
                  ","+cp_ToStrODBC(this.w_DGDESAGG)+;
                  ","+cp_ToStrODBC(this.w_DGCODFIS)+;
                  ","+cp_ToStrODBC(this.w_DGPARIVA)+;
                  ","+cp_ToStrODBC(this.w_DGCOGNOM)+;
                  ","+cp_ToStrODBC(this.w_DGFONOME)+;
                  ","+cp_ToStrODBC(this.w_DG_SESSO)+;
                  ","+cp_ToStrODBC(this.w_DGCOMUNE)+;
                  ","+cp_ToStrODBC(this.w_DGPRNASC)+;
                  ","+cp_ToStrODBC(this.w_DGDTNASC)+;
                  ","+cp_ToStrODBC(this.w_DGDENOMI)+;
                  ","+cp_ToStrODBC(this.w_DG_EMAIL)+;
                  ","+cp_ToStrODBC(this.w_DGRFTELE)+;
                  ","+cp_ToStrODBC(this.w_DGFLGCOR)+;
                  ","+cp_ToStrODBC(this.w_DGPROTEC)+;
                  ","+cp_ToStrODBC(this.w_DGPRODOC)+;
                  ","+cp_ToStrODBC(this.w_DGRAPFIR)+;
                  ","+cp_ToStrODBC(this.w_DGRFCFIS)+;
                  ","+cp_ToStrODBC(this.w_DGRFPIVA)+;
                  ","+cp_ToStrODBC(this.w_DGCODCAR)+;
                  ","+cp_ToStrODBC(this.w_DGRFCOGN)+;
                  ","+cp_ToStrODBC(this.w_DGRFNOME)+;
                  ","+cp_ToStrODBC(this.w_DGRFSESS)+;
                  ","+cp_ToStrODBC(this.w_DGRFDTNS)+;
                  ","+cp_ToStrODBC(this.w_DGRFCOMN)+;
                  ","+cp_ToStrODBC(this.w_DGRFPRNS)+;
                  ","+cp_ToStrODBC(this.w_DGFIRDIC)+;
                  ","+cp_ToStrODBC(this.w_DGTIPPLA)+;
                  ","+cp_ToStrODBC(this.w_DGDICIVA)+;
                  ","+cp_ToStrODBC(this.w_DGESPORT)+;
                  ","+cp_ToStrODBC(this.w_DGCESINT)+;
                  ","+cp_ToStrODBC(this.w_DGCES_SM)+;
                  ","+cp_ToStrODBC(this.w_DGOPEASS)+;
                  ","+cp_ToStrODBC(this.w_DGOPESTR)+;
                  ","+cp_ToStrODBC(this.w_DGTIPFOR)+;
                  ","+cp_ToStrODBC(this.w_DGFISINT)+;
                  ","+cp_ToStrODBC(this.w_DGDATIMP)+;
                  ","+cp_ToStrODBC(this.w_DGFIRINT)+;
                  ","+cp_ToStrODBC(this.w_DGTIPGEN)+;
                  ","+cp_ToStrODBC(this.w_DGANNDIC)+;
                  ","+cp_ToStrODBC(this.w_DGDATINI)+;
                  ","+cp_ToStrODBC(this.w_DGDATFIN)+;
                  ","+cp_ToStrODBC(this.w_DGSEPARA)+;
                  ","+cp_ToStrODBC(this.w_DGGENERA)+;
                  ","+cp_ToStrODBC(this.w_DGNOMFIL)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GEDICEME')
        i_extval=cp_InsertValVFPExtFlds(this,'GEDICEME')
        cp_CheckDeletedKey(i_cTable,0,'DGSERIAL',this.w_DGSERIAL)
        INSERT INTO (i_cTable);
              (DGSERIAL,DGDATCOM,DGDESAGG,DGCODFIS,DGPARIVA,DGCOGNOM,DGFONOME,DG_SESSO,DGCOMUNE,DGPRNASC,DGDTNASC,DGDENOMI,DG_EMAIL,DGRFTELE,DGFLGCOR,DGPROTEC,DGPRODOC,DGRAPFIR,DGRFCFIS,DGRFPIVA,DGCODCAR,DGRFCOGN,DGRFNOME,DGRFSESS,DGRFDTNS,DGRFCOMN,DGRFPRNS,DGFIRDIC,DGTIPPLA,DGDICIVA,DGESPORT,DGCESINT,DGCES_SM,DGOPEASS,DGOPESTR,DGTIPFOR,DGFISINT,DGDATIMP,DGFIRINT,DGTIPGEN,DGANNDIC,DGDATINI,DGDATFIN,DGSEPARA,DGGENERA,DGNOMFIL  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DGSERIAL;
                  ,this.w_DGDATCOM;
                  ,this.w_DGDESAGG;
                  ,this.w_DGCODFIS;
                  ,this.w_DGPARIVA;
                  ,this.w_DGCOGNOM;
                  ,this.w_DGFONOME;
                  ,this.w_DG_SESSO;
                  ,this.w_DGCOMUNE;
                  ,this.w_DGPRNASC;
                  ,this.w_DGDTNASC;
                  ,this.w_DGDENOMI;
                  ,this.w_DG_EMAIL;
                  ,this.w_DGRFTELE;
                  ,this.w_DGFLGCOR;
                  ,this.w_DGPROTEC;
                  ,this.w_DGPRODOC;
                  ,this.w_DGRAPFIR;
                  ,this.w_DGRFCFIS;
                  ,this.w_DGRFPIVA;
                  ,this.w_DGCODCAR;
                  ,this.w_DGRFCOGN;
                  ,this.w_DGRFNOME;
                  ,this.w_DGRFSESS;
                  ,this.w_DGRFDTNS;
                  ,this.w_DGRFCOMN;
                  ,this.w_DGRFPRNS;
                  ,this.w_DGFIRDIC;
                  ,this.w_DGTIPPLA;
                  ,this.w_DGDICIVA;
                  ,this.w_DGESPORT;
                  ,this.w_DGCESINT;
                  ,this.w_DGCES_SM;
                  ,this.w_DGOPEASS;
                  ,this.w_DGOPESTR;
                  ,this.w_DGTIPFOR;
                  ,this.w_DGFISINT;
                  ,this.w_DGDATIMP;
                  ,this.w_DGFIRINT;
                  ,this.w_DGTIPGEN;
                  ,this.w_DGANNDIC;
                  ,this.w_DGDATINI;
                  ,this.w_DGDATFIN;
                  ,this.w_DGSEPARA;
                  ,this.w_DGGENERA;
                  ,this.w_DGNOMFIL;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.GEDICEME_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GEDICEME_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.GEDICEME_IDX,i_nConn)
      *
      * update GEDICEME
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'GEDICEME')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DGDATCOM="+cp_ToStrODBC(this.w_DGDATCOM)+;
             ",DGDESAGG="+cp_ToStrODBC(this.w_DGDESAGG)+;
             ",DGCODFIS="+cp_ToStrODBC(this.w_DGCODFIS)+;
             ",DGPARIVA="+cp_ToStrODBC(this.w_DGPARIVA)+;
             ",DGCOGNOM="+cp_ToStrODBC(this.w_DGCOGNOM)+;
             ",DGFONOME="+cp_ToStrODBC(this.w_DGFONOME)+;
             ",DG_SESSO="+cp_ToStrODBC(this.w_DG_SESSO)+;
             ",DGCOMUNE="+cp_ToStrODBC(this.w_DGCOMUNE)+;
             ",DGPRNASC="+cp_ToStrODBC(this.w_DGPRNASC)+;
             ",DGDTNASC="+cp_ToStrODBC(this.w_DGDTNASC)+;
             ",DGDENOMI="+cp_ToStrODBC(this.w_DGDENOMI)+;
             ",DG_EMAIL="+cp_ToStrODBC(this.w_DG_EMAIL)+;
             ",DGRFTELE="+cp_ToStrODBC(this.w_DGRFTELE)+;
             ",DGFLGCOR="+cp_ToStrODBC(this.w_DGFLGCOR)+;
             ",DGPROTEC="+cp_ToStrODBC(this.w_DGPROTEC)+;
             ",DGPRODOC="+cp_ToStrODBC(this.w_DGPRODOC)+;
             ",DGRAPFIR="+cp_ToStrODBC(this.w_DGRAPFIR)+;
             ",DGRFCFIS="+cp_ToStrODBC(this.w_DGRFCFIS)+;
             ",DGRFPIVA="+cp_ToStrODBC(this.w_DGRFPIVA)+;
             ",DGCODCAR="+cp_ToStrODBC(this.w_DGCODCAR)+;
             ",DGRFCOGN="+cp_ToStrODBC(this.w_DGRFCOGN)+;
             ",DGRFNOME="+cp_ToStrODBC(this.w_DGRFNOME)+;
             ",DGRFSESS="+cp_ToStrODBC(this.w_DGRFSESS)+;
             ",DGRFDTNS="+cp_ToStrODBC(this.w_DGRFDTNS)+;
             ",DGRFCOMN="+cp_ToStrODBC(this.w_DGRFCOMN)+;
             ",DGRFPRNS="+cp_ToStrODBC(this.w_DGRFPRNS)+;
             ",DGFIRDIC="+cp_ToStrODBC(this.w_DGFIRDIC)+;
             ",DGTIPPLA="+cp_ToStrODBC(this.w_DGTIPPLA)+;
             ",DGDICIVA="+cp_ToStrODBC(this.w_DGDICIVA)+;
             ",DGESPORT="+cp_ToStrODBC(this.w_DGESPORT)+;
             ",DGCESINT="+cp_ToStrODBC(this.w_DGCESINT)+;
             ",DGCES_SM="+cp_ToStrODBC(this.w_DGCES_SM)+;
             ",DGOPEASS="+cp_ToStrODBC(this.w_DGOPEASS)+;
             ",DGOPESTR="+cp_ToStrODBC(this.w_DGOPESTR)+;
             ",DGTIPFOR="+cp_ToStrODBC(this.w_DGTIPFOR)+;
             ",DGFISINT="+cp_ToStrODBC(this.w_DGFISINT)+;
             ",DGDATIMP="+cp_ToStrODBC(this.w_DGDATIMP)+;
             ",DGFIRINT="+cp_ToStrODBC(this.w_DGFIRINT)+;
             ",DGTIPGEN="+cp_ToStrODBC(this.w_DGTIPGEN)+;
             ",DGANNDIC="+cp_ToStrODBC(this.w_DGANNDIC)+;
             ",DGDATINI="+cp_ToStrODBC(this.w_DGDATINI)+;
             ",DGDATFIN="+cp_ToStrODBC(this.w_DGDATFIN)+;
             ",DGSEPARA="+cp_ToStrODBC(this.w_DGSEPARA)+;
             ",DGGENERA="+cp_ToStrODBC(this.w_DGGENERA)+;
             ",DGNOMFIL="+cp_ToStrODBC(this.w_DGNOMFIL)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'GEDICEME')
        i_cWhere = cp_PKFox(i_cTable  ,'DGSERIAL',this.w_DGSERIAL  )
        UPDATE (i_cTable) SET;
              DGDATCOM=this.w_DGDATCOM;
             ,DGDESAGG=this.w_DGDESAGG;
             ,DGCODFIS=this.w_DGCODFIS;
             ,DGPARIVA=this.w_DGPARIVA;
             ,DGCOGNOM=this.w_DGCOGNOM;
             ,DGFONOME=this.w_DGFONOME;
             ,DG_SESSO=this.w_DG_SESSO;
             ,DGCOMUNE=this.w_DGCOMUNE;
             ,DGPRNASC=this.w_DGPRNASC;
             ,DGDTNASC=this.w_DGDTNASC;
             ,DGDENOMI=this.w_DGDENOMI;
             ,DG_EMAIL=this.w_DG_EMAIL;
             ,DGRFTELE=this.w_DGRFTELE;
             ,DGFLGCOR=this.w_DGFLGCOR;
             ,DGPROTEC=this.w_DGPROTEC;
             ,DGPRODOC=this.w_DGPRODOC;
             ,DGRAPFIR=this.w_DGRAPFIR;
             ,DGRFCFIS=this.w_DGRFCFIS;
             ,DGRFPIVA=this.w_DGRFPIVA;
             ,DGCODCAR=this.w_DGCODCAR;
             ,DGRFCOGN=this.w_DGRFCOGN;
             ,DGRFNOME=this.w_DGRFNOME;
             ,DGRFSESS=this.w_DGRFSESS;
             ,DGRFDTNS=this.w_DGRFDTNS;
             ,DGRFCOMN=this.w_DGRFCOMN;
             ,DGRFPRNS=this.w_DGRFPRNS;
             ,DGFIRDIC=this.w_DGFIRDIC;
             ,DGTIPPLA=this.w_DGTIPPLA;
             ,DGDICIVA=this.w_DGDICIVA;
             ,DGESPORT=this.w_DGESPORT;
             ,DGCESINT=this.w_DGCESINT;
             ,DGCES_SM=this.w_DGCES_SM;
             ,DGOPEASS=this.w_DGOPEASS;
             ,DGOPESTR=this.w_DGOPESTR;
             ,DGTIPFOR=this.w_DGTIPFOR;
             ,DGFISINT=this.w_DGFISINT;
             ,DGDATIMP=this.w_DGDATIMP;
             ,DGFIRINT=this.w_DGFIRINT;
             ,DGTIPGEN=this.w_DGTIPGEN;
             ,DGANNDIC=this.w_DGANNDIC;
             ,DGDATINI=this.w_DGDATINI;
             ,DGDATFIN=this.w_DGDATFIN;
             ,DGSEPARA=this.w_DGSEPARA;
             ,DGGENERA=this.w_DGGENERA;
             ,DGNOMFIL=this.w_DGNOMFIL;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- Gscg_Mde : Saving
      this.Gscg_Mde.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DGSERIAL,"DDSERIAL";
             )
      this.Gscg_Mde.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- Gscg_Mde : Deleting
    this.Gscg_Mde.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DGSERIAL,"DDSERIAL";
           )
    this.Gscg_Mde.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GEDICEME_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GEDICEME_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.GEDICEME_IDX,i_nConn)
      *
      * delete GEDICEME
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DGSERIAL',this.w_DGSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GEDICEME_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GEDICEME_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_1_4('Full')
        .DoRTCalc(5,9,.t.)
          .link_1_10('Full')
        .DoRTCalc(11,29,.t.)
        if .o_DGFLGCOR<>.w_DGFLGCOR
            .w_DGPROTEC = SPACE(17)
        endif
        if .o_DGFLGCOR<>.w_DGFLGCOR
            .w_DGPRODOC = 0
        endif
        .DoRTCalc(32,32,.t.)
          .link_2_2('Full')
        .DoRTCalc(34,40,.t.)
        if .o_DgRapFir<>.w_DgRapFir
            .w_DGRFCFIS = iif(.w_DgRapFir='S',.w_Rfcodfis,Space(16))
        endif
        if .o_Dgcodcar<>.w_Dgcodcar.or. .o_Dgrapfir<>.w_Dgrapfir
            .w_DGRFPIVA = iif(.w_DgRapFir='S' And .w_Dgcodcar $ '169',.w_Dgrfpiva,Space(11))
        endif
        if .o_DgRapFir<>.w_DgRapFir
            .w_DGCODCAR = '1'
        endif
        if .o_DgRapFir<>.w_DgRapFir
            .w_DGRFCOGN = iif(.w_DgRapFir='N',space(20),Left(.w_Rfcognom,24))
        endif
        if .o_DgRapFir<>.w_DgRapFir
            .w_DGRFNOME = iif(.w_DgRapFir='N',space(20),Left(.w_Rf__Nome,20))
        endif
        if .o_DgRapFir<>.w_DgRapFir
            .w_DGRFSESS = iif(.w_DgRapFir='N','M',.w_Rf_Sesso)
        endif
        if .o_DgRapFir<>.w_DgRapFir
            .w_DGRFDTNS = iif(.w_DgRapFir='N',Ctod('  -  -    '),.w_Rfdatnas)
        endif
        if .o_DgRapFir<>.w_DgRapFir
            .w_DGRFCOMN = iif(.w_DgRapFir='N',space(40),.w_Rfcomnas)
        endif
        if .o_DgRapFir<>.w_DgRapFir
            .w_DGRFPRNS = iif(.w_DgRapFir='N',space(2),.w_Rfpronas)
        endif
        .DoRTCalc(50,59,.t.)
        if .o_DGTIPFOR<>.w_DGTIPFOR
            .w_DGFISINT = iif(.w_Dgtipfor='01', space(16), .w_Dgfisint)
        endif
        if .o_DGFISINT<>.w_DGFISINT.or. .o_DGFIRINT<>.w_DGFIRINT
            .w_DGDATIMP = iif(Empty(.w_Dgfisint) And .w_Dgfirint='0', Ctod('  -  -    '), .w_Dgdatimp)
        endif
        .DoRTCalc(62,64,.t.)
        if .o_DGANNDIC<>.w_DGANNDIC
            .w_DGDATINI = cp_CharToDate('01-'+'01-'+Str(.w_DGANNDIC))
        endif
        if .o_DGANNDIC<>.w_DGANNDIC
            .w_DGDATFIN = cp_CharToDate('31-'+'12-'+Str(.w_DGANNDIC))
        endif
        .oPgFrm.Page3.oPag.CalcZoom.Calculate()
        if .o_DGCODFIS<>.w_DGCODFIS
          .Calculate_ZELNCQUIYD()
        endif
        if .o_DGCODFIS<>.w_DGCODFIS.or. .o_DGNOMFIL<>.w_DGNOMFIL
          .Calculate_ZJFMMVWDUN()
        endif
        .DoRTCalc(67,69,.t.)
            .w_NOGENERA = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Nogenera'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Nogenera'), ' ')))
            .w_NOCODFIS = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Nocodfis'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Nocodfis'), ' ')))
            .w_NOPARIVA = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Nopariva'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Nopariva'), ' ')))
            .w_NOCOGNOM = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Nocognom'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Nocognom'), ' ')))
            .w_NO__NOME = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('No__Nome'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('No__Nome'), ' ')))
            .w_NOIMPOPE = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Noimpope'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Noimpope'), ' ')))
            .w_NOPERIODO = iif(.w_Dgflgcor='1',Alltrim(Nvl(.w_RettZoom.getVar('Noperiodo'), ' ')),Alltrim(Nvl(.w_CalcZoom.getVar('Noperiodo'), ' ')))
            .w_DISERIAL = iif(.cFunction='Query' or .w_Dggenera='S',Nvl(.w_IntZoom.getVar('Diserial'),' '),iif(.w_Dgflgcor='1',Nvl(.w_RettZoom.getVar('Diserial'),' '),Nvl(.w_CalcZoom.getVar('Diserial'),' ')))
        if .o_DISERIAL<>.w_DISERIAL
          .Calculate_ZXXNBXLBRH()
        endif
        .oPgFrm.Page3.oPag.IntZoom.Calculate()
        if .o_DGANNDIC<>.w_DGANNDIC.or. .o_DGDATINI<>.w_DGDATINI.or. .o_DGDATFIN<>.w_DGDATFIN
          .Calculate_ACZEDGMSJB()
        endif
        .DoRTCalc(78,78,.t.)
            .w_DICODICE = iif(.cFunction='Query' or .w_Dggenera='S',Nvl(.w_IntZoom.getVar('Dicodice'),' '),iif(.w_Dgflgcor='1',Nvl(.w_RettZoom.getVar('Dicodice'),' '),Nvl(.w_CalcZoom.getVar('Dicodice'),' ')))
            .w_DIPROTEC = Alltrim(Nvl(.w_IntZoom.getVar('Diprotec'),' '))
            .w_DIPRODOC = Nvl(.w_IntZoom.getVar('Diprodoc'),0)
        .oPgFrm.Page3.oPag.RettZoom.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_37.Calculate('','',RGB(255,0,0))
        .oPgFrm.Page3.oPag.oObj_3_38.Calculate('','',RGB(0,255,0))
        if .o_DGFLGCOR<>.w_DGFLGCOR.or. .o_DGPROTEC<>.w_DGPROTEC.or. .o_DGPRODOC<>.w_DGPRODOC
          .Calculate_QMLIUVHTXB()
        endif
        if .o_PATH<>.w_PATH
          .Calculate_BOEFVOUVSD()
        endif
        * --- Area Manuale = Calculate
        * --- gscg_ade
        *** Effettuo questa operazione per poter gestire la messaggistica
        *** di errore nel caso sia presente un solo record
        if This.cFunction='Load' And This.w_Dggenera<>'S'
           This.w_ErroreGene=iif(This.w_Nogenera='S',iif(Not Empty(This.w_Nocodfis),This.w_Nocodfis+Chr(13),'')+iif(Not Empty(This.w_Nopariva),This.w_Nopariva+Chr(13),'')+iif(Not Empty(This.w_Nocognom),This.w_Nocognom+Chr(13),'')+iif(Not Empty(This.w_No__Nome),This.w_No__Nome+Chr(13),'')+iif(Not Empty(This.w_Noimpope),This.w_Noimpope+Chr(13),'')+iif(Not Empty(This.w_Noperiodo),This.w_Noperiodo+Chr(13),''),' ')
        Endif
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"DGDIE","i_codazi,w_DGSERIAL")
          .op_DGSERIAL = .w_DGSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(82,84,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page3.oPag.CalcZoom.Calculate()
        .oPgFrm.Page3.oPag.IntZoom.Calculate()
        .oPgFrm.Page3.oPag.RettZoom.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_37.Calculate('','',RGB(255,0,0))
        .oPgFrm.Page3.oPag.oObj_3_38.Calculate('','',RGB(0,255,0))
    endwith
  return

  proc Calculate_ZELNCQUIYD()
    with this
          * --- Pulisco valore campo Nome file
          .w_DGNOMFIL = Space(254)
    endwith
  endproc
  proc Calculate_ZJFMMVWDUN()
    with this
          * --- Inizializzo nome file
          .w_DGNOMFIL = iif(not empty(.w_DGNOMFIL), .w_DGNOMFIL, left(.w_DirName+Alltrim(.w_DGSERIAL)+'.IVI'+space(254),254))
    endwith
  endproc
  proc Calculate_ZXXNBXLBRH()
    with this
          * --- Valorizzazione variabile controllo congruenza
          .w_ErroreGene = iif(.w_Nogenera='S',iif(Not Empty(.w_Nocodfis),.w_Nocodfis+Chr(13),'')+iif(Not Empty(.w_Nopariva),.w_Nopariva+Chr(13),'')+iif(Not Empty(.w_Nocognom),.w_Nocognom+Chr(13),'')+iif(Not Empty(.w_No__Nome),.w_No__Nome+Chr(13),'')+iif(Not Empty(.w_Noimpope),.w_Noimpope+Chr(13),'')+iif(Not Empty(.w_Noperiodo),.w_Noperiodo+Chr(13),''),' ')
    endwith
  endproc
  proc Calculate_ACZEDGMSJB()
    with this
          * --- Riesegue la query di elaborazione dello zoom
          Gscg_Bdt(this;
              ,'A';
             )
    endwith
  endproc
  proc Calculate_QMLIUVHTXB()
    with this
          * --- Comunicazione lettera di intento integrativa
          Gscg_bdt(this;
              ,'N';
             )
    endwith
  endproc
  proc Calculate_BOEFVOUVSD()
    with this
          * --- Compone la variabile Path
          .w_PATH = IIF(right(alltrim(.w_PATH),1)='\' or empty(.w_PATH),.w_PATH,alltrim(.w_PATH)+iif(len(alltrim(.w_PATH))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_PATH,'F')
          .w_PATH = iif(Directory(.w_PATH),.w_Path,sys(5)+sys(2003)+'\')
    endwith
  endproc
  proc Calculate_DHKXCVOMYU()
    with this
          * --- Blank,AcitvatePage 3,Edit Started
          Gscg_bdt(this;
              ,'Z';
             )
    endwith
  endproc
  proc Calculate_SVOWCOLKLE()
    with this
          * --- Cancellazione comunicazione lettere di intento
          Gscg_Bdt(this;
              ,'C';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDGCOGNOM_1_20.enabled = this.oPgFrm.Page1.oPag.oDGCOGNOM_1_20.mCond()
    this.oPgFrm.Page1.oPag.oDGFONOME_1_21.enabled = this.oPgFrm.Page1.oPag.oDGFONOME_1_21.mCond()
    this.oPgFrm.Page1.oPag.oDG_SESSO_1_22.enabled = this.oPgFrm.Page1.oPag.oDG_SESSO_1_22.mCond()
    this.oPgFrm.Page1.oPag.oDGCOMUNE_1_23.enabled = this.oPgFrm.Page1.oPag.oDGCOMUNE_1_23.mCond()
    this.oPgFrm.Page1.oPag.oDGPRNASC_1_24.enabled = this.oPgFrm.Page1.oPag.oDGPRNASC_1_24.mCond()
    this.oPgFrm.Page1.oPag.oDGDTNASC_1_25.enabled = this.oPgFrm.Page1.oPag.oDGDTNASC_1_25.mCond()
    this.oPgFrm.Page1.oPag.oDGDENOMI_1_26.enabled = this.oPgFrm.Page1.oPag.oDGDENOMI_1_26.mCond()
    this.oPgFrm.Page1.oPag.oDGFLGCOR_1_29.enabled = this.oPgFrm.Page1.oPag.oDGFLGCOR_1_29.mCond()
    this.oPgFrm.Page1.oPag.oDGPROTEC_1_30.enabled = this.oPgFrm.Page1.oPag.oDGPROTEC_1_30.mCond()
    this.oPgFrm.Page1.oPag.oDGPRODOC_1_31.enabled = this.oPgFrm.Page1.oPag.oDGPRODOC_1_31.mCond()
    this.oPgFrm.Page2.oPag.oDGRFCFIS_2_10.enabled = this.oPgFrm.Page2.oPag.oDGRFCFIS_2_10.mCond()
    this.oPgFrm.Page2.oPag.oDGRFPIVA_2_11.enabled = this.oPgFrm.Page2.oPag.oDGRFPIVA_2_11.mCond()
    this.oPgFrm.Page2.oPag.oDGCODCAR_2_12.enabled = this.oPgFrm.Page2.oPag.oDGCODCAR_2_12.mCond()
    this.oPgFrm.Page2.oPag.oDGRFCOGN_2_14.enabled = this.oPgFrm.Page2.oPag.oDGRFCOGN_2_14.mCond()
    this.oPgFrm.Page2.oPag.oDGRFNOME_2_16.enabled = this.oPgFrm.Page2.oPag.oDGRFNOME_2_16.mCond()
    this.oPgFrm.Page2.oPag.oDGRFSESS_2_17.enabled = this.oPgFrm.Page2.oPag.oDGRFSESS_2_17.mCond()
    this.oPgFrm.Page2.oPag.oDGRFDTNS_2_19.enabled = this.oPgFrm.Page2.oPag.oDGRFDTNS_2_19.mCond()
    this.oPgFrm.Page2.oPag.oDGRFCOMN_2_21.enabled = this.oPgFrm.Page2.oPag.oDGRFCOMN_2_21.mCond()
    this.oPgFrm.Page2.oPag.oDGRFPRNS_2_23.enabled = this.oPgFrm.Page2.oPag.oDGRFPRNS_2_23.mCond()
    this.oPgFrm.Page4.oPag.oDGFISINT_4_10.enabled = this.oPgFrm.Page4.oPag.oDGFISINT_4_10.mCond()
    this.oPgFrm.Page4.oPag.oDGDATIMP_4_14.enabled = this.oPgFrm.Page4.oPag.oDGDATIMP_4_14.mCond()
    this.oPgFrm.Page3.oPag.oDGANNDIC_3_2.enabled = this.oPgFrm.Page3.oPag.oDGANNDIC_3_2.mCond()
    this.oPgFrm.Page3.oPag.oDGDATINI_3_3.enabled = this.oPgFrm.Page3.oPag.oDGDATINI_3_3.mCond()
    this.oPgFrm.Page3.oPag.oDGDATFIN_3_4.enabled = this.oPgFrm.Page3.oPag.oDGDATFIN_3_4.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_24.enabled = this.oPgFrm.Page4.oPag.oBtn_4_24.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_32.enabled = this.oPgFrm.Page4.oPag.oBtn_4_32.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page4.oPag.oStr_4_18.visible=!this.oPgFrm.Page4.oPag.oStr_4_18.mHide()
    this.oPgFrm.Page3.oPag.oDGANNDIC_3_2.visible=!this.oPgFrm.Page3.oPag.oDGANNDIC_3_2.mHide()
    this.oPgFrm.Page3.oPag.oDGDATINI_3_3.visible=!this.oPgFrm.Page3.oPag.oDGDATINI_3_3.mHide()
    this.oPgFrm.Page3.oPag.oDGDATFIN_3_4.visible=!this.oPgFrm.Page3.oPag.oDGDATFIN_3_4.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_6.visible=!this.oPgFrm.Page3.oPag.oBtn_3_6.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_7.visible=!this.oPgFrm.Page3.oPag.oStr_3_7.mHide()
    this.oPgFrm.Page4.oPag.oDGNOMFIL_4_23.visible=!this.oPgFrm.Page4.oPag.oDGNOMFIL_4_23.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_18.visible=!this.oPgFrm.Page3.oPag.oBtn_3_18.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_19.visible=!this.oPgFrm.Page3.oPag.oBtn_3_19.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_20.visible=!this.oPgFrm.Page3.oPag.oBtn_3_20.mHide()
    this.oPgFrm.Page3.oPag.oERROREGENE_3_21.visible=!this.oPgFrm.Page3.oPag.oERROREGENE_3_21.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_22.visible=!this.oPgFrm.Page3.oPag.oStr_3_22.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_32.visible=!this.oPgFrm.Page4.oPag.oBtn_4_32.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_23.visible=!this.oPgFrm.Page3.oPag.oStr_3_23.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_24.visible=!this.oPgFrm.Page3.oPag.oStr_3_24.mHide()
    this.oPgFrm.Page3.oPag.oDIPROTEC_3_30.visible=!this.oPgFrm.Page3.oPag.oDIPROTEC_3_30.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_31.visible=!this.oPgFrm.Page3.oPag.oStr_3_31.mHide()
    this.oPgFrm.Page3.oPag.oDIPRODOC_3_32.visible=!this.oPgFrm.Page3.oPag.oDIPRODOC_3_32.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_33.visible=!this.oPgFrm.Page3.oPag.oStr_3_33.mHide()
    this.oPgFrm.Page4.oPag.oLinkPC_4_33.visible=!this.oPgFrm.Page4.oPag.oLinkPC_4_33.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_35.visible=!this.oPgFrm.Page4.oPag.oStr_4_35.mHide()
    this.oPgFrm.Page4.oPag.oPATH_4_36.visible=!this.oPgFrm.Page4.oPag.oPATH_4_36.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page3.oPag.CalcZoom.Event(cEvent)
        if lower(cEvent)==lower("New record")
          .Calculate_ZJFMMVWDUN()
          bRefresh=.t.
        endif
      .oPgFrm.Page3.oPag.IntZoom.Event(cEvent)
      .oPgFrm.Page3.oPag.RettZoom.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_37.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_38.Event(cEvent)
        if lower(cEvent)==lower("ActivatePage 3") or lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Edit Started")
          .Calculate_DHKXCVOMYU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("HasEvent")
          .Calculate_SVOWCOLKLE()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCOFAZI,AZPIVAZI,AZPERAZI,AZTELEFO,AZ_EMAIL";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZCOFAZI,AZPIVAZI,AZPERAZI,AZTELEFO,AZ_EMAIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZCOFAZI = NVL(_Link_.AZCOFAZI,space(16))
      this.w_AZPIVAZI = NVL(_Link_.AZPIVAZI,space(12))
      this.w_AZPERAZI = NVL(_Link_.AZPERAZI,space(1))
      this.w_AZTELEFO = NVL(_Link_.AZTELEFO,space(18))
      this.w_AZ_EMAIL = NVL(_Link_.AZ_EMAIL,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_AZCOFAZI = space(16)
      this.w_AZPIVAZI = space(12)
      this.w_AZPERAZI = space(1)
      this.w_AZTELEFO = space(18)
      this.w_AZ_EMAIL = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI2
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTTELEFO,TTCOGTIT,TTNOMTIT,TT_SESSO,TTLUONAS,TTDATNAS,TTPRONAS";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_CODAZI2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_CODAZI2)
            select TTCODAZI,TTTELEFO,TTCOGTIT,TTNOMTIT,TT_SESSO,TTLUONAS,TTDATNAS,TTPRONAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    local i_bTmp
    i_bTmp=.t.
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI2 = NVL(_Link_.TTCODAZI,space(5))
      this.w_TELEFO1 = NVL(_Link_.TTTELEFO,space(18))
      this.w_COGTIT1 = NVL(_Link_.TTCOGTIT,space(25))
      this.w_NOMTIT1 = NVL(_Link_.TTNOMTIT,space(25))
      this.w_SESSO1 = NVL(_Link_.TT_SESSO,space(1))
      this.w_LOCTIT1 = NVL(_Link_.TTLUONAS,space(30))
      this.w_DATNAS1 = NVL(cp_ToDate(_Link_.TTDATNAS),ctod("  /  /  "))
      this.w_PROTIT1 = NVL(_Link_.TTPRONAS,space(2))
    else
      this.w_TELEFO1 = space(18)
      this.w_COGTIT1 = space(25)
      this.w_NOMTIT1 = space(25)
      this.w_SESSO1 = space(1)
      this.w_LOCTIT1 = space(30)
      this.w_DATNAS1 = ctod("  /  /  ")
      this.w_PROTIT1 = space(2)
    endif
    i_bRes=i_reccount=1  or i_bTmp
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAPFIRM
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DAT_RAPP_IDX,3]
    i_lTable = "DAT_RAPP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2], .t., this.DAT_RAPP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAPFIRM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAPFIRM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RFCODAZI,RFCODFIS,RFCOGNOM,RF__NOME,RF_SESSO,RFDATNAS,RFCOMNAS,RFPRONAS";
                   +" from "+i_cTable+" "+i_lTable+" where RFCODAZI="+cp_ToStrODBC(this.w_RAPFIRM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RFCODAZI',this.w_RAPFIRM)
            select RFCODAZI,RFCODFIS,RFCOGNOM,RF__NOME,RF_SESSO,RFDATNAS,RFCOMNAS,RFPRONAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAPFIRM = NVL(_Link_.RFCODAZI,space(10))
      this.w_RFCODFIS = NVL(_Link_.RFCODFIS,space(16))
      this.w_RFCOGNOM = NVL(_Link_.RFCOGNOM,space(40))
      this.w_RF__NOME = NVL(_Link_.RF__NOME,space(40))
      this.w_RF_SESSO = NVL(_Link_.RF_SESSO,space(1))
      this.w_RFDATNAS = NVL(cp_ToDate(_Link_.RFDATNAS),ctod("  /  /  "))
      this.w_RFCOMNAS = NVL(_Link_.RFCOMNAS,space(40))
      this.w_RFPRONAS = NVL(_Link_.RFPRONAS,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_RAPFIRM = space(10)
      endif
      this.w_RFCODFIS = space(16)
      this.w_RFCOGNOM = space(40)
      this.w_RF__NOME = space(40)
      this.w_RF_SESSO = space(1)
      this.w_RFDATNAS = ctod("  /  /  ")
      this.w_RFCOMNAS = space(40)
      this.w_RFPRONAS = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2])+'\'+cp_ToStr(_Link_.RFCODAZI,1)
      cp_ShowWarn(i_cKey,this.DAT_RAPP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAPFIRM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDGDATCOM_1_2.value==this.w_DGDATCOM)
      this.oPgFrm.Page1.oPag.oDGDATCOM_1_2.value=this.w_DGDATCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDGDESAGG_1_3.value==this.w_DGDESAGG)
      this.oPgFrm.Page1.oPag.oDGDESAGG_1_3.value=this.w_DGDESAGG
    endif
    if not(this.oPgFrm.Page1.oPag.oDGCODFIS_1_18.value==this.w_DGCODFIS)
      this.oPgFrm.Page1.oPag.oDGCODFIS_1_18.value=this.w_DGCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDGPARIVA_1_19.value==this.w_DGPARIVA)
      this.oPgFrm.Page1.oPag.oDGPARIVA_1_19.value=this.w_DGPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDGCOGNOM_1_20.value==this.w_DGCOGNOM)
      this.oPgFrm.Page1.oPag.oDGCOGNOM_1_20.value=this.w_DGCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDGFONOME_1_21.value==this.w_DGFONOME)
      this.oPgFrm.Page1.oPag.oDGFONOME_1_21.value=this.w_DGFONOME
    endif
    if not(this.oPgFrm.Page1.oPag.oDG_SESSO_1_22.RadioValue()==this.w_DG_SESSO)
      this.oPgFrm.Page1.oPag.oDG_SESSO_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDGCOMUNE_1_23.value==this.w_DGCOMUNE)
      this.oPgFrm.Page1.oPag.oDGCOMUNE_1_23.value=this.w_DGCOMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oDGPRNASC_1_24.value==this.w_DGPRNASC)
      this.oPgFrm.Page1.oPag.oDGPRNASC_1_24.value=this.w_DGPRNASC
    endif
    if not(this.oPgFrm.Page1.oPag.oDGDTNASC_1_25.value==this.w_DGDTNASC)
      this.oPgFrm.Page1.oPag.oDGDTNASC_1_25.value=this.w_DGDTNASC
    endif
    if not(this.oPgFrm.Page1.oPag.oDGDENOMI_1_26.value==this.w_DGDENOMI)
      this.oPgFrm.Page1.oPag.oDGDENOMI_1_26.value=this.w_DGDENOMI
    endif
    if not(this.oPgFrm.Page1.oPag.oDG_EMAIL_1_27.value==this.w_DG_EMAIL)
      this.oPgFrm.Page1.oPag.oDG_EMAIL_1_27.value=this.w_DG_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDGRFTELE_1_28.value==this.w_DGRFTELE)
      this.oPgFrm.Page1.oPag.oDGRFTELE_1_28.value=this.w_DGRFTELE
    endif
    if not(this.oPgFrm.Page1.oPag.oDGFLGCOR_1_29.RadioValue()==this.w_DGFLGCOR)
      this.oPgFrm.Page1.oPag.oDGFLGCOR_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDGPROTEC_1_30.value==this.w_DGPROTEC)
      this.oPgFrm.Page1.oPag.oDGPROTEC_1_30.value=this.w_DGPROTEC
    endif
    if not(this.oPgFrm.Page1.oPag.oDGPRODOC_1_31.value==this.w_DGPRODOC)
      this.oPgFrm.Page1.oPag.oDGPRODOC_1_31.value=this.w_DGPRODOC
    endif
    if not(this.oPgFrm.Page2.oPag.oDGRAPFIR_2_1.RadioValue()==this.w_DGRAPFIR)
      this.oPgFrm.Page2.oPag.oDGRAPFIR_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDGRFCFIS_2_10.value==this.w_DGRFCFIS)
      this.oPgFrm.Page2.oPag.oDGRFCFIS_2_10.value=this.w_DGRFCFIS
    endif
    if not(this.oPgFrm.Page2.oPag.oDGRFPIVA_2_11.value==this.w_DGRFPIVA)
      this.oPgFrm.Page2.oPag.oDGRFPIVA_2_11.value=this.w_DGRFPIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oDGCODCAR_2_12.RadioValue()==this.w_DGCODCAR)
      this.oPgFrm.Page2.oPag.oDGCODCAR_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDGRFCOGN_2_14.value==this.w_DGRFCOGN)
      this.oPgFrm.Page2.oPag.oDGRFCOGN_2_14.value=this.w_DGRFCOGN
    endif
    if not(this.oPgFrm.Page2.oPag.oDGRFNOME_2_16.value==this.w_DGRFNOME)
      this.oPgFrm.Page2.oPag.oDGRFNOME_2_16.value=this.w_DGRFNOME
    endif
    if not(this.oPgFrm.Page2.oPag.oDGRFSESS_2_17.RadioValue()==this.w_DGRFSESS)
      this.oPgFrm.Page2.oPag.oDGRFSESS_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDGRFDTNS_2_19.value==this.w_DGRFDTNS)
      this.oPgFrm.Page2.oPag.oDGRFDTNS_2_19.value=this.w_DGRFDTNS
    endif
    if not(this.oPgFrm.Page2.oPag.oDGRFCOMN_2_21.value==this.w_DGRFCOMN)
      this.oPgFrm.Page2.oPag.oDGRFCOMN_2_21.value=this.w_DGRFCOMN
    endif
    if not(this.oPgFrm.Page2.oPag.oDGRFPRNS_2_23.value==this.w_DGRFPRNS)
      this.oPgFrm.Page2.oPag.oDGRFPRNS_2_23.value=this.w_DGRFPRNS
    endif
    if not(this.oPgFrm.Page2.oPag.oDGFIRDIC_2_24.RadioValue()==this.w_DGFIRDIC)
      this.oPgFrm.Page2.oPag.oDGFIRDIC_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDGTIPPLA_4_2.RadioValue()==this.w_DGTIPPLA)
      this.oPgFrm.Page4.oPag.oDGTIPPLA_4_2.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDGDICIVA_4_3.RadioValue()==this.w_DGDICIVA)
      this.oPgFrm.Page4.oPag.oDGDICIVA_4_3.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDGESPORT_4_4.RadioValue()==this.w_DGESPORT)
      this.oPgFrm.Page4.oPag.oDGESPORT_4_4.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDGCESINT_4_5.RadioValue()==this.w_DGCESINT)
      this.oPgFrm.Page4.oPag.oDGCESINT_4_5.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDGCES_SM_4_6.RadioValue()==this.w_DGCES_SM)
      this.oPgFrm.Page4.oPag.oDGCES_SM_4_6.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDGOPEASS_4_7.RadioValue()==this.w_DGOPEASS)
      this.oPgFrm.Page4.oPag.oDGOPEASS_4_7.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDGOPESTR_4_8.RadioValue()==this.w_DGOPESTR)
      this.oPgFrm.Page4.oPag.oDGOPESTR_4_8.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDGTIPFOR_4_9.RadioValue()==this.w_DGTIPFOR)
      this.oPgFrm.Page4.oPag.oDGTIPFOR_4_9.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDGFISINT_4_10.value==this.w_DGFISINT)
      this.oPgFrm.Page4.oPag.oDGFISINT_4_10.value=this.w_DGFISINT
    endif
    if not(this.oPgFrm.Page4.oPag.oDGDATIMP_4_14.value==this.w_DGDATIMP)
      this.oPgFrm.Page4.oPag.oDGDATIMP_4_14.value=this.w_DGDATIMP
    endif
    if not(this.oPgFrm.Page4.oPag.oDGFIRINT_4_16.RadioValue()==this.w_DGFIRINT)
      this.oPgFrm.Page4.oPag.oDGFIRINT_4_16.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oDGTIPGEN_4_17.RadioValue()==this.w_DGTIPGEN)
      this.oPgFrm.Page4.oPag.oDGTIPGEN_4_17.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oDGANNDIC_3_2.value==this.w_DGANNDIC)
      this.oPgFrm.Page3.oPag.oDGANNDIC_3_2.value=this.w_DGANNDIC
    endif
    if not(this.oPgFrm.Page3.oPag.oDGDATINI_3_3.value==this.w_DGDATINI)
      this.oPgFrm.Page3.oPag.oDGDATINI_3_3.value=this.w_DGDATINI
    endif
    if not(this.oPgFrm.Page3.oPag.oDGDATFIN_3_4.value==this.w_DGDATFIN)
      this.oPgFrm.Page3.oPag.oDGDATFIN_3_4.value=this.w_DGDATFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oDGSEPARA_3_5.value==this.w_DGSEPARA)
      this.oPgFrm.Page3.oPag.oDGSEPARA_3_5.value=this.w_DGSEPARA
    endif
    if not(this.oPgFrm.Page4.oPag.oDGNOMFIL_4_23.value==this.w_DGNOMFIL)
      this.oPgFrm.Page4.oPag.oDGNOMFIL_4_23.value=this.w_DGNOMFIL
    endif
    if not(this.oPgFrm.Page3.oPag.oERROREGENE_3_21.value==this.w_ERROREGENE)
      this.oPgFrm.Page3.oPag.oERROREGENE_3_21.value=this.w_ERROREGENE
    endif
    if not(this.oPgFrm.Page3.oPag.oDIPROTEC_3_30.value==this.w_DIPROTEC)
      this.oPgFrm.Page3.oPag.oDIPROTEC_3_30.value=this.w_DIPROTEC
    endif
    if not(this.oPgFrm.Page3.oPag.oDIPRODOC_3_32.value==this.w_DIPRODOC)
      this.oPgFrm.Page3.oPag.oDIPRODOC_3_32.value=this.w_DIPRODOC
    endif
    if not(this.oPgFrm.Page4.oPag.oPATH_4_36.value==this.w_PATH)
      this.oPgFrm.Page4.oPag.oPATH_4_36.value=this.w_PATH
    endif
    cp_SetControlsValueExtFlds(this,'GEDICEME')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DGDATCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDGDATCOM_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DGDATCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DGCODFIS)) or not(chkcfp(alltrim(.w_DGCODFIS),'CF')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDGCODFIS_1_18.SetFocus()
            i_bnoObbl = !empty(.w_DGCODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DGPARIVA)) or not(CHKCFP(.w_DGPARIVA,"PI", "", "", "")))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDGPARIVA_1_19.SetFocus()
            i_bnoObbl = !empty(.w_DGPARIVA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DGCOGNOM))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDGCOGNOM_1_20.SetFocus()
            i_bnoObbl = !empty(.w_DGCOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DGFONOME))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDGFONOME_1_21.SetFocus()
            i_bnoObbl = !empty(.w_DGFONOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DGCOMUNE))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDGCOMUNE_1_23.SetFocus()
            i_bnoObbl = !empty(.w_DGCOMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DGPRNASC))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDGPRNASC_1_24.SetFocus()
            i_bnoObbl = !empty(.w_DGPRNASC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DGDTNASC))  and (.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDGDTNASC_1_25.SetFocus()
            i_bnoObbl = !empty(.w_DGDTNASC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DGDENOMI))  and (.w_AZPERAZI<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDGDENOMI_1_26.SetFocus()
            i_bnoObbl = !empty(.w_DGDENOMI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(at(' ',alltrim(.w_DGRFTELE))=0 and at('/',alltrim(.w_DGRFTELE))=0 and at('-',alltrim(.w_DGRFTELE))=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDGRFTELE_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Non inserire caratteri separatori tra prefisso e numero")
          case   ((empty(.w_DGPROTEC)) or not(VAL(.w_DGPROTEC)<>0))  and (.w_DGFLGCOR<>'0' And .w_DGGENERA<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDGPROTEC_1_30.SetFocus()
            i_bnoObbl = !empty(.w_DGPROTEC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DGPRODOC))  and (.w_DGFLGCOR<>'0' And .w_DGGENERA<>'S' )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDGPRODOC_1_31.SetFocus()
            i_bnoObbl = !empty(.w_DGPRODOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DGRFCFIS)) or not(iif(not empty(.w_Dgrfcfis),chkcfp(alltrim(.w_Dgrfcfis),'CF'),.T.)))  and (.w_DgRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDGRFCFIS_2_10.SetFocus()
            i_bnoObbl = !empty(.w_DGRFCFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(iif(not empty(.w_DGRFPIVA),chkcfp(alltrim(.w_DGRFPIVA),'PI'),.T.))  and (.w_DgRapFir='S' And .w_Dgcodcar $ '169')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDGRFPIVA_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DGCODCAR))  and (.w_DgRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDGCODCAR_2_12.SetFocus()
            i_bnoObbl = !empty(.w_DGCODCAR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DGRFCOGN))  and (.w_DgRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDGRFCOGN_2_14.SetFocus()
            i_bnoObbl = !empty(.w_DGRFCOGN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DGRFNOME))  and (.w_DgRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDGRFNOME_2_16.SetFocus()
            i_bnoObbl = !empty(.w_DGRFNOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DGRFSESS))  and (.w_DgRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDGRFSESS_2_17.SetFocus()
            i_bnoObbl = !empty(.w_DGRFSESS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DGRFDTNS))  and (.w_DgRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDGRFDTNS_2_19.SetFocus()
            i_bnoObbl = !empty(.w_DGRFDTNS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DGRFCOMN))  and (.w_DgRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDGRFCOMN_2_21.SetFocus()
            i_bnoObbl = !empty(.w_DGRFCOMN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DGRFPRNS))  and (.w_DgRapFir='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDGRFPRNS_2_23.SetFocus()
            i_bnoObbl = !empty(.w_DGRFPRNS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DGFISINT)) or not((empty(.w_Dgfisint) or Chkcfp(alltrim(.w_Dgfisint),'CF')) And (Not Empty(.w_Dgfisint) Or .w_Dgtipfor='01')))  and (.w_Dgtipfor='10')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oDGFISINT_4_10.SetFocus()
            i_bnoObbl = !empty(.w_DGFISINT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fiscale dell'intermediario obbligatorio")
          case   (empty(.w_DGDATIMP))  and (Not Empty(.w_Dgfisint) Or .w_Dgfirint='1')
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oDGDATIMP_4_14.SetFocus()
            i_bnoObbl = !empty(.w_DGDATIMP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DGANNDIC))  and not(.w_DGFLGCOR='1')  and (.w_DGGENERA='N')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oDGANNDIC_3_2.SetFocus()
            i_bnoObbl = !empty(.w_DGANNDIC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DGDATINI)) or not(.w_DGDATFIN>=.w_DGDATINI))  and not(.w_DGFLGCOR='1')  and (.w_DGGENERA='N')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oDGDATINI_3_3.SetFocus()
            i_bnoObbl = !empty(.w_DGDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore della data finale ")
          case   ((empty(.w_DGDATFIN)) or not(.w_DGDATFIN>=.w_DGDATINI))  and not(.w_DGFLGCOR='1')  and (.w_DGGENERA='N')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oDGDATFIN_3_4.SetFocus()
            i_bnoObbl = !empty(.w_DGDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .Gscg_Mde.CheckForm()
      if i_bres
        i_bres=  .Gscg_Mde.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DGCODFIS = this.w_DGCODFIS
    this.o_DGFLGCOR = this.w_DGFLGCOR
    this.o_DGPROTEC = this.w_DGPROTEC
    this.o_DGPRODOC = this.w_DGPRODOC
    this.o_DGRAPFIR = this.w_DGRAPFIR
    this.o_DGCODCAR = this.w_DGCODCAR
    this.o_DGTIPFOR = this.w_DGTIPFOR
    this.o_DGFISINT = this.w_DGFISINT
    this.o_DGFIRINT = this.w_DGFIRINT
    this.o_DGANNDIC = this.w_DGANNDIC
    this.o_DGDATINI = this.w_DGDATINI
    this.o_DGDATFIN = this.w_DGDATFIN
    this.o_DGNOMFIL = this.w_DGNOMFIL
    this.o_DISERIAL = this.w_DISERIAL
    this.o_PATH = this.w_PATH
    * --- Gscg_Mde : Depends On
    this.Gscg_Mde.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_adePag1 as StdContainer
  Width  = 780
  height = 543
  stdWidth  = 780
  stdheight = 543
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDGDATCOM_1_2 as StdField with uid="TWFLVODDQH",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DGDATCOM", cQueryName = "DGDATCOM",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di generazione del file",;
    HelpContextID = 218405245,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=134, Top=11

  add object oDGDESAGG_1_3 as StdField with uid="ZJAXFDWJCR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DGDESAGG", cQueryName = "DGDESAGG",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Campo che accoglie le note aggiuntive",;
    HelpContextID = 15689341,;
   bGlobalFont=.t.,;
    Height=21, Width=636, Left=134, Top=40, InputMask=replicate('X',254)

  add object oDGCODFIS_1_18 as StdField with uid="ZFLQCFKABQ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DGCODFIS", cQueryName = "DGCODFIS",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del soggetto dichiarante",;
    HelpContextID = 183937399,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=134, Top=99, cSayPict="repl('!',16)", cGetPict="repl('!',16)", InputMask=replicate('X',16)

  func oDGCODFIS_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chkcfp(alltrim(.w_DGCODFIS),'CF'))
    endwith
    return bRes
  endfunc

  add object oDGPARIVA_1_19 as StdField with uid="JPNFHTXPJM",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DGPARIVA", cQueryName = "DGPARIVA",;
    bObbl = .t. , nPag = 1, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA del dichiarante",;
    HelpContextID = 148645495,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=487, Top=99, cSayPict='REPLICATE("!",11)', cGetPict='REPLICATE("!",11)', InputMask=replicate('X',11)

  func oDGPARIVA_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_DGPARIVA,"PI", "", "", ""))
    endwith
    return bRes
  endfunc

  add object oDGCOGNOM_1_20 as StdField with uid="USTYABUOUZ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DGCOGNOM", cQueryName = "DGCOGNOM",;
    bObbl = .t. , nPag = 1, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 46573949,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=134, Top=194, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  func oDGCOGNOM_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oDGFONOME_1_21 as StdField with uid="LKFBHLXRZV",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DGFONOME", cQueryName = "DGFONOME",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 22444421,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=377, Top=194, cSayPict="REPL('!',20)", cGetPict="REPL('!',20)", InputMask=replicate('X',20)

  func oDGFONOME_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc


  add object oDG_SESSO_1_22 as StdCombo with uid="UFMSDHWBVZ",rtseq=22,rtrep=.f.,left=635,top=193,width=91,height=22;
    , HelpContextID = 35591813;
    , cFormVar="w_DG_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDG_SESSO_1_22.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oDG_SESSO_1_22.GetRadio()
    this.Parent.oContained.w_DG_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oDG_SESSO_1_22.SetRadio()
    this.Parent.oContained.w_DG_SESSO=trim(this.Parent.oContained.w_DG_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_DG_SESSO=='M',1,;
      iif(this.Parent.oContained.w_DG_SESSO=='F',2,;
      0))
  endfunc

  func oDG_SESSO_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oDGCOMUNE_1_23 as StdField with uid="UCAJUOVOUI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DGCOMUNE", cQueryName = "DGCOMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di nascita",;
    HelpContextID = 191277445,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=134, Top=245, cSayPict="REPL('!',40)", cGetPict="REPL('!',40)", InputMask=replicate('X',40), bHasZoom = .t. 

  func oDGCOMUNE_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  proc oDGCOMUNE_1_23.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_DGCOMUNE",".w_DGPRNASC")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDGPRNASC_1_24 as StdField with uid="HHSQKKMXTC",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DGPRNASC", cQueryName = "DGPRNASC",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita",;
    HelpContextID = 11347577,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=479, Top=245, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oDGPRNASC_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  proc oDGPRNASC_1_24.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_DGPRNASC")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDGDTNASC_1_25 as StdField with uid="ECDVVXPNAO",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DGDTNASC", cQueryName = "DGDTNASC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 11429497,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=635, Top=245

  func oDGDTNASC_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
   endif
  endfunc

  add object oDGDENOMI_1_26 as StdField with uid="QITJWPXNAA",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DGDENOMI", cQueryName = "DGDENOMI",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione ",;
    HelpContextID = 23107969,;
   bGlobalFont=.t.,;
    Height=21, Width=461, Left=134, Top=333, cSayPict="REPL('!',60)", cGetPict="REPL('!',60)", InputMask=replicate('X',60)

  func oDGDENOMI_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
   endif
  endfunc

  add object oDG_EMAIL_1_27 as StdField with uid="MTZLJHHSMZ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DG_EMAIL", cQueryName = "DG_EMAIL",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo mail del dichiarante",;
    HelpContextID = 258926974,;
   bGlobalFont=.t.,;
    Height=21, Width=601, Left=134, Top=398, cSayPict="replicate('!', 100)", cGetPict="replicate('!', 100)", InputMask=replicate('X',100)

  add object oDGRFTELE_1_28 as StdField with uid="MUYEBPTRWW",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DGRFTELE", cQueryName = "DGRFTELE",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    sErrorMsg = "Non inserire caratteri separatori tra prefisso e numero",;
    ToolTipText = "Numero di telefono o cellulare del dichiarante",;
    HelpContextID = 184465797,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=134, Top=454, cSayPict='"999999999999"', cGetPict='"999999999999"', InputMask=replicate('X',12)

  func oDGRFTELE_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (at(' ',alltrim(.w_DGRFTELE))=0 and at('/',alltrim(.w_DGRFTELE))=0 and at('-',alltrim(.w_DGRFTELE))=0)
    endwith
    return bRes
  endfunc

  add object oDGFLGCOR_1_29 as StdCheck with uid="UPEXTOJRYC",rtseq=29,rtrep=.f.,left=134, top=512, caption="Integrativa",;
    ToolTipText = "Dichiarazione integrativa",;
    HelpContextID = 231307640,;
    cFormVar="w_DGFLGCOR", bObbl = .f. , nPag = 1;
    , tabstop =.f.;
   , bGlobalFont=.t.


  func oDGFLGCOR_1_29.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oDGFLGCOR_1_29.GetRadio()
    this.Parent.oContained.w_DGFLGCOR = this.RadioValue()
    return .t.
  endfunc

  func oDGFLGCOR_1_29.SetRadio()
    this.Parent.oContained.w_DGFLGCOR=trim(this.Parent.oContained.w_DGFLGCOR)
    this.value = ;
      iif(this.Parent.oContained.w_DGFLGCOR=='1',1,;
      0)
  endfunc

  func oDGFLGCOR_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DGGENERA<>'S')
    endwith
   endif
  endfunc

  add object oDGPROTEC_1_30 as StdField with uid="ZJVNFQQLIO",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DGPROTEC", cQueryName = "DGPROTEC",;
    bObbl = .t. , nPag = 1, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Protocollo telematico della dichiarazione da integrare (Prima parte)",;
    HelpContextID = 62727801,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=511, Top=513, cSayPict='"99999999999999999"', cGetPict='"99999999999999999"', InputMask=replicate('X',17), Alignment=1

  func oDGPROTEC_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DGFLGCOR<>'0' And .w_DGGENERA<>'S')
    endwith
   endif
  endfunc

  func oDGPROTEC_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_DGPROTEC)<>0)
    endwith
    return bRes
  endfunc

  add object oDGPRODOC_1_31 as StdField with uid="TSSGDCKFDL",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DGPRODOC", cQueryName = "DGPRODOC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Protocollo telematico della dichiarazione da integrare (Parte seconda)",;
    HelpContextID = 205707655,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=663, Top=513, cSayPict='"999999"', cGetPict='"999999"'

  func oDGPRODOC_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DGFLGCOR<>'0' And .w_DGGENERA<>'S' )
    endwith
   endif
  endfunc

  add object oStr_1_32 as StdString with uid="XACALZRRWL",Visible=.t., Left=3, Top=100,;
    Alignment=1, Width=125, Height=18,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="DSZQYEUOKT",Visible=.t., Left=9, Top=66,;
    Alignment=0, Width=395, Height=19,;
    Caption="Dati del dichiarante"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_34 as StdString with uid="IXLIJYIGMO",Visible=.t., Left=399, Top=100,;
    Alignment=1, Width=84, Height=18,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="BQYEOWHSUJ",Visible=.t., Left=9, Top=146,;
    Alignment=0, Width=220, Height=18,;
    Caption="Dati anagrafici persone fisiche"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="BOGQGLFCGC",Visible=.t., Left=134, Top=311,;
    Alignment=0, Width=188, Height=18,;
    Caption="Denominazione o ragione sociale"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="KWZECAJTFT",Visible=.t., Left=9, Top=281,;
    Alignment=0, Width=283, Height=18,;
    Caption="Dati anagrafici soggetti diversi da persone fisiche"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="DIRMDFWZQS",Visible=.t., Left=134, Top=377,;
    Alignment=0, Width=165, Height=18,;
    Caption="Indirizzo di posta elettronica"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="QFCZBTIUBY",Visible=.t., Left=134, Top=432,;
    Alignment=0, Width=139, Height=18,;
    Caption="Numero di telefono"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="HEUTCFKDNL",Visible=.t., Left=134, Top=224,;
    Alignment=0, Width=193, Height=18,;
    Caption="Comune di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="OSGCYPTITY",Visible=.t., Left=134, Top=173,;
    Alignment=0, Width=58, Height=18,;
    Caption="Cognome"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="LGSUOYWZDY",Visible=.t., Left=377, Top=173,;
    Alignment=0, Width=37, Height=18,;
    Caption="Nome"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="JOPERGGNNX",Visible=.t., Left=635, Top=173,;
    Alignment=0, Width=39, Height=18,;
    Caption="Sesso"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="CNJREOUFHV",Visible=.t., Left=635, Top=224,;
    Alignment=2, Width=85, Height=18,;
    Caption="Data di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="GTAPKTSGGM",Visible=.t., Left=479, Top=224,;
    Alignment=0, Width=109, Height=18,;
    Caption="Provincia di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="ADCCXLUNIW",Visible=.t., Left=9, Top=377,;
    Alignment=0, Width=90, Height=19,;
    Caption="Recapiti"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_52 as StdString with uid="ZBWKKGSIJD",Visible=.t., Left=264, Top=514,;
    Alignment=1, Width=246, Height=18,;
    Caption="Protocollo telematico da integrare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="ZMXBSMWCMB",Visible=.t., Left=9, Top=493,;
    Alignment=0, Width=90, Height=19,;
    Caption="Integrativa"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_55 as StdString with uid="HGECVVAAWR",Visible=.t., Left=652, Top=515,;
    Alignment=0, Width=12, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="JOEJGOSTIM",Visible=.t., Left=2, Top=13,;
    Alignment=1, Width=129, Height=18,;
    Caption="Data comunicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="KTZEYIUZXG",Visible=.t., Left=13, Top=41,;
    Alignment=1, Width=118, Height=18,;
    Caption="Note aggiuntive:"  ;
  , bGlobalFont=.t.

  add object oBox_1_46 as StdBox with uid="RDMWYOEEFE",left=-1, top=87, width=778,height=456

  add object oBox_1_47 as StdBox with uid="WWEVXJGUIS",left=-1, top=299, width=777,height=2

  add object oBox_1_48 as StdBox with uid="GOSZELPAYD",left=-1, top=166, width=777,height=2

  add object oBox_1_51 as StdBox with uid="YUDCCAXXTI",left=-1, top=367, width=777,height=2

  add object oBox_1_54 as StdBox with uid="QMESIUQKJN",left=-1, top=484, width=777,height=2
enddefine
define class tgscg_adePag2 as StdContainer
  Width  = 780
  height = 543
  stdWidth  = 780
  stdheight = 543
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDGRAPFIR_2_1 as StdCheck with uid="CXKZSHQWCD",rtseq=32,rtrep=.f.,left=560, top=13, caption="Rappresentante firmatario",;
    ToolTipText = "Se attivo, abilita l'inserimento dei dati relativi al rappresentante firmatario",;
    HelpContextID = 172210552,;
    cFormVar="w_DGRAPFIR", bObbl = .f. , nPag = 2;
    , Tabstop=.F.;
   , bGlobalFont=.t.


  func oDGRAPFIR_2_1.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oDGRAPFIR_2_1.GetRadio()
    this.Parent.oContained.w_DGRAPFIR = this.RadioValue()
    return .t.
  endfunc

  func oDGRAPFIR_2_1.SetRadio()
    this.Parent.oContained.w_DGRAPFIR=trim(this.Parent.oContained.w_DGRAPFIR)
    this.value = ;
      iif(this.Parent.oContained.w_DGRAPFIR=='S',1,;
      0)
  endfunc

  add object oDGRFCFIS_2_10 as StdField with uid="VAOMXTNVDK",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DGRFCFIS", cQueryName = "DGRFCFIS",;
    bObbl = .t. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del rappresentante",;
    HelpContextID = 185514359,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=20, Top=106, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oDGRFCFIS_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DgRapFir='S')
    endwith
   endif
  endfunc

  func oDGRFCFIS_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_Dgrfcfis),chkcfp(alltrim(.w_Dgrfcfis),'CF'),.T.))
    endwith
    return bRes
  endfunc

  add object oDGRFPIVA_2_11 as StdField with uid="LVWSYVJCWD",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DGRFPIVA", cQueryName = "DGRFPIVA",;
    bObbl = .f. , nPag = 2, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale societ� dichiarante",;
    HelpContextID = 146884215,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=213, Top=106, InputMask=replicate('X',11)

  func oDGRFPIVA_2_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DgRapFir='S' And .w_Dgcodcar $ '169')
    endwith
   endif
  endfunc

  func oDGRFPIVA_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (iif(not empty(.w_DGRFPIVA),chkcfp(alltrim(.w_DGRFPIVA),'PI'),.T.))
    endwith
    return bRes
  endfunc


  add object oDGCODCAR_2_12 as StdCombo with uid="YYMESQRZRO",rtseq=43,rtrep=.f.,left=399,top=104,width=286,height=22;
    , ToolTipText = "Codice carica";
    , HelpContextID = 34166408;
    , cFormVar="w_DGCODCAR",RowSource=""+"1) Rappr. legale o negoziale,"+"2) Rappr. di minore - curatore eredit�,"+"3) Curatore fallimentare,"+"4) Commissario liquidatore,"+"5) Commissario giudiziale,"+"6) Rappr. fiscale di soggetto non residente,"+"7) Erede del dichiarante,"+"8) Liquidatore volontario,"+"9) Sogg. obbl. per operaz. straord.", bObbl = .t. , nPag = 2;
  , bGlobalFont=.t.


  func oDGCODCAR_2_12.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    iif(this.value =8,'8',;
    iif(this.value =9,'9',;
    '  '))))))))))
  endfunc
  func oDGCODCAR_2_12.GetRadio()
    this.Parent.oContained.w_DGCODCAR = this.RadioValue()
    return .t.
  endfunc

  func oDGCODCAR_2_12.SetRadio()
    this.Parent.oContained.w_DGCODCAR=trim(this.Parent.oContained.w_DGCODCAR)
    this.value = ;
      iif(this.Parent.oContained.w_DGCODCAR=='1',1,;
      iif(this.Parent.oContained.w_DGCODCAR=='2',2,;
      iif(this.Parent.oContained.w_DGCODCAR=='3',3,;
      iif(this.Parent.oContained.w_DGCODCAR=='4',4,;
      iif(this.Parent.oContained.w_DGCODCAR=='5',5,;
      iif(this.Parent.oContained.w_DGCODCAR=='6',6,;
      iif(this.Parent.oContained.w_DGCODCAR=='7',7,;
      iif(this.Parent.oContained.w_DGCODCAR=='8',8,;
      iif(this.Parent.oContained.w_DGCODCAR=='9',9,;
      0)))))))))
  endfunc

  func oDGCODCAR_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DgRapFir='S')
    endwith
   endif
  endfunc

  add object oDGRFCOGN_2_14 as StdField with uid="RXOMZIHAKM",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DGRFCOGN", cQueryName = "DGRFCOGN",;
    bObbl = .t. , nPag = 2, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del rappresentante",;
    HelpContextID = 233916036,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=60, Top=261, cSayPict="REPL('!',24)", cGetPict="REPL('!',24)", InputMask=replicate('X',24)

  func oDGRFCOGN_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DgRapFir='S')
    endwith
   endif
  endfunc

  add object oDGRFNOME_2_16 as StdField with uid="JCXFNSENGE",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DGRFNOME", cQueryName = "DGRFNOME",;
    bObbl = .t. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del rappresentante",;
    HelpContextID = 22985093,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=309, Top=261, cSayPict="REPL('!',20)", cGetPict="REPL('!',20)", InputMask=replicate('X',20)

  func oDGRFNOME_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DgRapFir='S')
    endwith
   endif
  endfunc


  add object oDGRFSESS_2_17 as StdCombo with uid="WCRTCQLBZV",rtseq=46,rtrep=.f.,left=656,top=259,width=91,height=22;
    , HelpContextID = 82921097;
    , cFormVar="w_DGRFSESS",RowSource=""+"Maschio,"+"Femmina", bObbl = .t. , nPag = 2;
  , bGlobalFont=.t.


  func oDGRFSESS_2_17.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oDGRFSESS_2_17.GetRadio()
    this.Parent.oContained.w_DGRFSESS = this.RadioValue()
    return .t.
  endfunc

  func oDGRFSESS_2_17.SetRadio()
    this.Parent.oContained.w_DGRFSESS=trim(this.Parent.oContained.w_DGRFSESS)
    this.value = ;
      iif(this.Parent.oContained.w_DGRFSESS=='M',1,;
      iif(this.Parent.oContained.w_DGRFSESS=='F',2,;
      0))
  endfunc

  func oDGRFSESS_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DgRapFir='S')
    endwith
   endif
  endfunc

  add object oDGRFDTNS_2_19 as StdField with uid="BGQJMZVANZ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DGRFDTNS", cQueryName = "DGRFDTNS",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del rappresentante",;
    HelpContextID = 218020215,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=60, Top=335

  func oDGRFDTNS_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DgRapFir='S')
    endwith
   endif
  endfunc

  add object oDGRFCOMN_2_21 as StdField with uid="MEMPOPBQLG",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DGRFCOMN", cQueryName = "DGRFCOMN",;
    bObbl = .t. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune o stato estero di nascita del rappresentante  ",;
    HelpContextID = 34519420,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=309, Top=335, cSayPict="REPL('!',40)", cGetPict="REPL('!',40)", InputMask=replicate('X',40), bHasZoom = .t. 

  func oDGRFCOMN_2_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DgRapFir='S')
    endwith
   endif
  endfunc

  proc oDGRFCOMN_2_21.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_Dgrfcomn",".w_Dgrfprns")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDGRFPRNS_2_23 as StdField with uid="ZSQNJWKRWC",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DGRFPRNS", cQueryName = "DGRFPRNS",;
    bObbl = .t. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita del rappresentante",;
    HelpContextID = 238991735,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=656, Top=335, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oDGRFPRNS_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DgRapFir='S')
    endwith
   endif
  endfunc

  proc oDGRFPRNS_2_23.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_Dgrfprns")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oDGFIRDIC_2_24 as StdCheck with uid="YPKFIPTFIT",rtseq=50,rtrep=.f.,left=60, top=458, caption="Flag conferma",;
    ToolTipText = "Firma del dichiarante ",;
    HelpContextID = 203192711,;
    cFormVar="w_DGFIRDIC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oDGFIRDIC_2_24.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oDGFIRDIC_2_24.GetRadio()
    this.Parent.oContained.w_DGFIRDIC = this.RadioValue()
    return .t.
  endfunc

  func oDGFIRDIC_2_24.SetRadio()
    this.Parent.oContained.w_DGFIRDIC=trim(this.Parent.oContained.w_DGFIRDIC)
    this.value = ;
      iif(this.Parent.oContained.w_DGFIRDIC=='1',1,;
      0)
  endfunc

  add object oStr_2_13 as StdString with uid="FNFBPKWGUY",Visible=.t., Left=20, Top=88,;
    Alignment=0, Width=123, Height=18,;
    Caption="Codice fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="PPEHDYKWYQ",Visible=.t., Left=60, Top=243,;
    Alignment=0, Width=165, Height=18,;
    Caption="Cognome"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="JFZOCGHHIH",Visible=.t., Left=309, Top=244,;
    Alignment=0, Width=144, Height=18,;
    Caption="Nome"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="WXSCBRPDLD",Visible=.t., Left=60, Top=315,;
    Alignment=0, Width=92, Height=18,;
    Caption="Data di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="ZDPVXMBGXY",Visible=.t., Left=309, Top=317,;
    Alignment=0, Width=300, Height=18,;
    Caption="Comune/stato estero di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="UAQHHMUPJS",Visible=.t., Left=656, Top=317,;
    Alignment=0, Width=54, Height=18,;
    Caption="Provincia"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="HAGPFVSWCH",Visible=.t., Left=11, Top=47,;
    Alignment=0, Width=382, Height=19,;
    Caption="Dati relativi al rappresentante firmatario della dichiarazione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_28 as StdString with uid="FANRLAONBU",Visible=.t., Left=656, Top=244,;
    Alignment=0, Width=39, Height=18,;
    Caption="Sesso"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="LFXDIKGIJH",Visible=.t., Left=399, Top=88,;
    Alignment=0, Width=175, Height=18,;
    Caption="Codice carica"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="ZNXMFUEIMT",Visible=.t., Left=10, Top=193,;
    Alignment=0, Width=220, Height=18,;
    Caption="Dati anagrafici persone fisiche"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="YACQCZUTUU",Visible=.t., Left=213, Top=88,;
    Alignment=0, Width=135, Height=18,;
    Caption="Codice fiscale societ�"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="IMMABZWHQW",Visible=.t., Left=14, Top=430,;
    Alignment=0, Width=164, Height=19,;
    Caption="Firma della dichiarazione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_27 as StdBox with uid="FKHDCRQNTU",left=3, top=41, width=775,height=484

  add object oBox_2_29 as StdBox with uid="MJVFLEHPAA",left=4, top=166, width=772,height=2

  add object oBox_2_34 as StdBox with uid="PSVRHFYXEM",left=4, top=392, width=772,height=2
enddefine
define class tgscg_adePag3 as StdContainer
  Width  = 780
  height = 543
  stdWidth  = 780
  stdheight = 543
  resizeXpos=706
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDGANNDIC_3_2 as StdField with uid="MSWRDPELZK",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DGANNDIC", cQueryName = "DGANNDIC",;
    bObbl = .t. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Filtra sull'anno di inizio validit� della lettera di intento",;
    HelpContextID = 207079815,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=200, Top=39, cSayPict='"9999"', cGetPict='"9999"'

  func oDGANNDIC_3_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DGGENERA='N')
    endwith
   endif
  endfunc

  func oDGANNDIC_3_2.mHide()
    with this.Parent.oContained
      return (.w_DGFLGCOR='1')
    endwith
  endfunc

  add object oDGDATINI_3_3 as StdField with uid="HHEWTYGVRI",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DGDATINI", cQueryName = "DGDATINI",;
    bObbl = .t. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore della data finale ",;
    ToolTipText = "Data di inizio validit� della dichiarazione ",;
    HelpContextID = 117741953,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=341, Top=39

  func oDGDATINI_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DGGENERA='N')
    endwith
   endif
  endfunc

  func oDGDATINI_3_3.mHide()
    with this.Parent.oContained
      return (.w_DGFLGCOR='1')
    endwith
  endfunc

  func oDGDATINI_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DGDATFIN>=.w_DGDATINI)
    endwith
    return bRes
  endfunc

  add object oDGDATFIN_3_4 as StdField with uid="MKGPVVAVNS",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DGDATFIN", cQueryName = "DGDATFIN",;
    bObbl = .t. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine validit� della dichiarazione",;
    HelpContextID = 168073596,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=489, Top=39

  func oDGDATFIN_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DGGENERA='N')
    endwith
   endif
  endfunc

  func oDGDATFIN_3_4.mHide()
    with this.Parent.oContained
      return (.w_DGFLGCOR='1')
    endwith
  endfunc

  func oDGDATFIN_3_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DGDATFIN>=.w_DGDATINI)
    endwith
    return bRes
  endfunc

  add object oDGSEPARA_3_5 as StdField with uid="AYXOTFSTCA",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DGSEPARA", cQueryName = "DGSEPARA",;
    bObbl = .f. , nPag = 3, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Separatore",;
    HelpContextID = 12605047,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=683, Top=39, InputMask=replicate('X',1)


  add object oBtn_3_6 as StdButton with uid="EGVAZQXZKQ",left=726, top=16, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=3;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 83982614;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_6.Click()
      with this.Parent.oContained
        GSCG_BDT(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_DGGENERA='S' Or .cFunction='Query' Or .w_DGFLGCOR='1')
     endwith
    endif
  endfunc


  add object CalcZoom as cp_szoombox with uid="INGOODONLJ",left=2, top=68, width=771,height=330,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DIC_INTE",cZoomFile="GSCG_ADE",bOptions=.f.,bQueryOnLoad=.f.,bQueryondblClick=.f.,bAdvOptions=.t.,cMenuFile="GSCG_ADE",cZoomOnZoom="",bReadOnly=.t.,;
    cEvent = "Init,Interroga,Esegui",;
    nPag=3;
    , HelpContextID = 85058022


  add object oBtn_3_18 as StdButton with uid="TSNZLTUIAB",left=6, top=496, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per selezionare le dichiarazioni di intento";
    , HelpContextID = 266441750;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_18.Click()
      with this.Parent.oContained
        Gscg_Bdt(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_18.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_DGGENERA='S' Or .cFunction='Query' Or .w_DGFLGCOR='1')
     endwith
    endif
  endfunc


  add object oBtn_3_19 as StdButton with uid="CVAOPNFYJG",left=60, top=496, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per deselezionare le dichiarazioni di intento";
    , HelpContextID = 266441750;
    ,  caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_19.Click()
      with this.Parent.oContained
        Gscg_Bdt(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_19.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_DGGENERA='S' Or .cFunction='Query' Or .w_DGFLGCOR='1')
     endwith
    endif
  endfunc


  add object oBtn_3_20 as StdButton with uid="VESJBKZVMB",left=114, top=496, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per invertire la selezione di tutte le dichiarazioni di intento";
    , HelpContextID = 266441750;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_20.Click()
      with this.Parent.oContained
        Gscg_Bdt(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_20.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_DGGENERA='S' Or .cFunction='Query' Or .w_DGFLGCOR='1')
     endwith
    endif
  endfunc

  add object oERROREGENE_3_21 as StdMemo with uid="IDZMQJJGRV",rtseq=78,rtrep=.f.,;
    cFormVar = "w_ERROREGENE", cQueryName = "ERROREGENE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 82484075,;
   bGlobalFont=.t.,;
    Height=118, Width=363, Left=410, Top=408

  func oERROREGENE_3_21.mHide()
    with this.Parent.oContained
      return (.w_Dggenera='S' Or .cFunction='Query' Or .w_DGFLGCOR='1')
    endwith
  endfunc


  add object IntZoom as cp_zoombox with uid="CHQPBRTJCX",left=2, top=68, width=771,height=330,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSCG1ADE",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,cTable="DIC_INTE",cMenuFile="GSCG_ADE",bQueryOnLoad=.f.,cZoomOnZoom="GSCG_ADE",bRetriveAllRows=.f.,;
    cEvent = "Init,Interroga,AggProtocollo",;
    nPag=3;
    , HelpContextID = 85058022

  add object oDIPROTEC_3_30 as StdField with uid="ITLIDSZYQE",rtseq=80,rtrep=.f.,;
    cFormVar = "w_DIPROTEC", cQueryName = "DIPROTEC",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Protocollo telematico (prima parte)",;
    HelpContextID = 62728313,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=163, Top=406, cSayPict='"99999999999999999"', cGetPict='"99999999999999999"', InputMask=replicate('X',17), Alignment=1

  func oDIPROTEC_3_30.mHide()
    with this.Parent.oContained
      return (.w_Dggenera<>'S')
    endwith
  endfunc

  add object oDIPRODOC_3_32 as StdField with uid="JQGEXMHZZN",rtseq=81,rtrep=.f.,;
    cFormVar = "w_DIPRODOC", cQueryName = "DIPRODOC",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Protocollo telematico (Parte seconda)",;
    HelpContextID = 205707143,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=311, Top=406, cSayPict='"999999"', cGetPict='"999999"'

  func oDIPRODOC_3_32.mHide()
    with this.Parent.oContained
      return (.w_Dggenera<>'S')
    endwith
  endfunc


  add object RettZoom as cp_zoombox with uid="UKUAKWIUZS",left=2, top=68, width=771,height=330,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DIC_INTE",cZoomFile="GSCG2ADE",bOptions=.f.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.f.,cMenuFile="GSCG_ADE",cZoomOnZoom="",bRetriveAllRows=.f.,;
    cEvent = "Init,Interroga,Esegui,Rettifica",;
    nPag=3;
    , HelpContextID = 85058022


  add object oObj_3_37 as cp_calclbl with uid="FMHBXRBUEX",left=2, top=472, width=15,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=3;
    , HelpContextID = 85058022


  add object oObj_3_38 as cp_calclbl with uid="ENVHEIQEVM",left=2, top=443, width=15,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=3;
    , HelpContextID = 85058022

  add object oStr_3_1 as StdString with uid="UGLOFURGTE",Visible=.t., Left=7, Top=10,;
    Alignment=0, Width=165, Height=19,;
    Caption="Parametri di selezione"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_7 as StdString with uid="QQOTXKZPDP",Visible=.t., Left=10, Top=40,;
    Alignment=1, Width=185, Height=18,;
    Caption="Anno della dichiarazione:"  ;
  , bGlobalFont=.t.

  func oStr_3_7.mHide()
    with this.Parent.oContained
      return (.w_DGFLGCOR='1')
    endwith
  endfunc

  add object oStr_3_22 as StdString with uid="TOUTVAQEZC",Visible=.t., Left=235, Top=406,;
    Alignment=1, Width=170, Height=19,;
    Caption="Controlli congruenza:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_22.mHide()
    with this.Parent.oContained
      return (.w_Dggenera='S' Or .cFunction='Query' Or .w_DGFLGCOR='1')
    endwith
  endfunc

  add object oStr_3_23 as StdString with uid="OHTDMKDSUG",Visible=.t., Left=271, Top=40,;
    Alignment=1, Width=65, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  func oStr_3_23.mHide()
    with this.Parent.oContained
      return (.w_DGFLGCOR='1')
    endwith
  endfunc

  add object oStr_3_24 as StdString with uid="UCFIWSLGSQ",Visible=.t., Left=441, Top=40,;
    Alignment=1, Width=44, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  func oStr_3_24.mHide()
    with this.Parent.oContained
      return (.w_DGFLGCOR='1')
    endwith
  endfunc

  add object oStr_3_26 as StdString with uid="WDXOCNLHEF",Visible=.t., Left=592, Top=40,;
    Alignment=1, Width=88, Height=18,;
    Caption="Separatore:"  ;
  , bGlobalFont=.t.

  add object oStr_3_31 as StdString with uid="NXKGUXFHKN",Visible=.t., Left=1, Top=408,;
    Alignment=1, Width=156, Height=18,;
    Caption="Protocollo telematico:"  ;
  , bGlobalFont=.t.

  func oStr_3_31.mHide()
    with this.Parent.oContained
      return (.w_Dggenera<>'S')
    endwith
  endfunc

  add object oStr_3_33 as StdString with uid="ETHQKLINOG",Visible=.t., Left=296, Top=407,;
    Alignment=0, Width=14, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  func oStr_3_33.mHide()
    with this.Parent.oContained
      return (.w_Dggenera<>'S')
    endwith
  endfunc

  add object oStr_3_34 as StdString with uid="FEHEPEEWTM",Visible=.t., Left=25, Top=444,;
    Alignment=0, Width=354, Height=18,;
    Caption="Dichiarazione di intento con protocollo telematico valorizzato"  ;
  , bGlobalFont=.t.

  add object oStr_3_36 as StdString with uid="KOBZWAWPBP",Visible=.t., Left=25, Top=472,;
    Alignment=0, Width=354, Height=18,;
    Caption="Dichiarazione di intento con incongruenze"  ;
  , bGlobalFont=.t.

  add object oStr_3_39 as StdString with uid="SLWBAPVYDC",Visible=.t., Left=44, Top=55,;
    Alignment=1, Width=151, Height=18,;
    Caption="Soggetto terzo"    , ForeColor=RGB(0,0,255);
  ;
  , bGlobalFont=.t.

  add object oBox_3_25 as StdBox with uid="AVYWWDZTAZ",left=1, top=26, width=701,height=2
enddefine
define class tgscg_adePag4 as StdContainer
  Width  = 780
  height = 543
  stdWidth  = 780
  stdheight = 543
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oDGTIPPLA_4_2 as StdCombo with uid="NVXLZRSPGC",rtseq=52,rtrep=.f.,left=140,top=45,width=85,height=22;
    , ToolTipText = "Identifica la natura del plafond";
    , HelpContextID = 3905929;
    , cFormVar="w_DGTIPPLA",RowSource=""+"Fisso,"+"Mobile", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oDGTIPPLA_4_2.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    space(1))))
  endfunc
  func oDGTIPPLA_4_2.GetRadio()
    this.Parent.oContained.w_DGTIPPLA = this.RadioValue()
    return .t.
  endfunc

  func oDGTIPPLA_4_2.SetRadio()
    this.Parent.oContained.w_DGTIPPLA=trim(this.Parent.oContained.w_DGTIPPLA)
    this.value = ;
      iif(this.Parent.oContained.w_DGTIPPLA=='1',1,;
      iif(this.Parent.oContained.w_DGTIPPLA=='2',2,;
      0))
  endfunc

  add object oDGDICIVA_4_3 as StdCheck with uid="NMMHKQGXNC",rtseq=53,rtrep=.f.,left=188, top=119, caption="Dichiarazione annuale IVA presentata",;
    ToolTipText = "Dichiarazione annuale IVA presentata",;
    HelpContextID = 133391991,;
    cFormVar="w_DGDICIVA", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oDGDICIVA_4_3.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oDGDICIVA_4_3.GetRadio()
    this.Parent.oContained.w_DGDICIVA = this.RadioValue()
    return .t.
  endfunc

  func oDGDICIVA_4_3.SetRadio()
    this.Parent.oContained.w_DGDICIVA=trim(this.Parent.oContained.w_DGDICIVA)
    this.value = ;
      iif(this.Parent.oContained.w_DGDICIVA=='1',1,;
      0)
  endfunc

  add object oDGESPORT_4_4 as StdCheck with uid="NNFIWIBBKB",rtseq=54,rtrep=.f.,left=77, top=165, caption="Esportazioni",;
    ToolTipText = "Esportazioni",;
    HelpContextID = 248346250,;
    cFormVar="w_DGESPORT", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oDGESPORT_4_4.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oDGESPORT_4_4.GetRadio()
    this.Parent.oContained.w_DGESPORT = this.RadioValue()
    return .t.
  endfunc

  func oDGESPORT_4_4.SetRadio()
    this.Parent.oContained.w_DGESPORT=trim(this.Parent.oContained.w_DGESPORT)
    this.value = ;
      iif(this.Parent.oContained.w_DGESPORT=='1',1,;
      0)
  endfunc

  add object oDGCESINT_4_5 as StdCheck with uid="HTLQEXQTPF",rtseq=55,rtrep=.f.,left=188, top=165, caption="Cessioni intracomunitarie",;
    ToolTipText = "Cessioni intracomunitarie di beni",;
    HelpContextID = 118532470,;
    cFormVar="w_DGCESINT", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oDGCESINT_4_5.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oDGCESINT_4_5.GetRadio()
    this.Parent.oContained.w_DGCESINT = this.RadioValue()
    return .t.
  endfunc

  func oDGCESINT_4_5.SetRadio()
    this.Parent.oContained.w_DGCESINT=trim(this.Parent.oContained.w_DGCESINT)
    this.value = ;
      iif(this.Parent.oContained.w_DGCESINT=='1',1,;
      0)
  endfunc

  add object oDGCES_SM_4_6 as StdCheck with uid="JECDMKMQUE",rtseq=56,rtrep=.f.,left=365, top=165, caption="Cessioni verso San Marino",;
    ToolTipText = "Cessioni di beni effettuate nei confronti di operatori sammarinesi",;
    HelpContextID = 250566275,;
    cFormVar="w_DGCES_SM", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oDGCES_SM_4_6.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oDGCES_SM_4_6.GetRadio()
    this.Parent.oContained.w_DGCES_SM = this.RadioValue()
    return .t.
  endfunc

  func oDGCES_SM_4_6.SetRadio()
    this.Parent.oContained.w_DGCES_SM=trim(this.Parent.oContained.w_DGCES_SM)
    this.value = ;
      iif(this.Parent.oContained.w_DGCES_SM=='1',1,;
      0)
  endfunc

  add object oDGOPEASS_4_7 as StdCheck with uid="DQMPCYVXYF",rtseq=57,rtrep=.f.,left=561, top=165, caption="Operazioni assimilate",;
    ToolTipText = "Operazioni assimilate alle cessioni ",;
    HelpContextID = 1775241,;
    cFormVar="w_DGOPEASS", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oDGOPEASS_4_7.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oDGOPEASS_4_7.GetRadio()
    this.Parent.oContained.w_DGOPEASS = this.RadioValue()
    return .t.
  endfunc

  func oDGOPEASS_4_7.SetRadio()
    this.Parent.oContained.w_DGOPEASS=trim(this.Parent.oContained.w_DGOPEASS)
    this.value = ;
      iif(this.Parent.oContained.w_DGOPEASS=='1',1,;
      0)
  endfunc

  add object oDGOPESTR_4_8 as StdCheck with uid="GRZTKCXXZV",rtseq=58,rtrep=.f.,left=77, top=202, caption="Operazione straordinarie",;
    ToolTipText = "Operazioni straordinarie",;
    HelpContextID = 35329672,;
    cFormVar="w_DGOPESTR", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oDGOPESTR_4_8.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oDGOPESTR_4_8.GetRadio()
    this.Parent.oContained.w_DGOPESTR = this.RadioValue()
    return .t.
  endfunc

  func oDGOPESTR_4_8.SetRadio()
    this.Parent.oContained.w_DGOPESTR=trim(this.Parent.oContained.w_DGOPESTR)
    this.value = ;
      iif(this.Parent.oContained.w_DGOPESTR=='1',1,;
      0)
  endfunc


  add object oDGTIPFOR_4_9 as StdCombo with uid="TMIIFPZQPN",rtseq=59,rtrep=.f.,left=234,top=278,width=279,height=22;
    , height = 21;
    , ToolTipText = "Tipo fornitore";
    , HelpContextID = 171678072;
    , cFormVar="w_DGTIPFOR",RowSource=""+"01: Soggetti che inviano le proprie dichiarazioni,"+"10: Altri soggetti", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oDGTIPFOR_4_9.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'10',;
    space(2))))
  endfunc
  func oDGTIPFOR_4_9.GetRadio()
    this.Parent.oContained.w_DGTIPFOR = this.RadioValue()
    return .t.
  endfunc

  func oDGTIPFOR_4_9.SetRadio()
    this.Parent.oContained.w_DGTIPFOR=trim(this.Parent.oContained.w_DGTIPFOR)
    this.value = ;
      iif(this.Parent.oContained.w_DGTIPFOR=='01',1,;
      iif(this.Parent.oContained.w_DGTIPFOR=='10',2,;
      0))
  endfunc

  add object oDGFISINT_4_10 as StdField with uid="VHBKITNWRO",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DGFISINT", cQueryName = "DGFISINT",;
    bObbl = .t. , nPag = 4, value=space(16), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fiscale dell'intermediario obbligatorio",;
    ToolTipText = "Codice fiscale dell'intermediario che effettua la trasmissione",;
    HelpContextID = 118258038,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=234, Top=315, cSayPict='REPLICATE("!",16)', cGetPict='REPLICATE("!",16)', InputMask=replicate('X',16)

  func oDGFISINT_4_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_Dgtipfor='10')
    endwith
   endif
  endfunc

  func oDGFISINT_4_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_Dgfisint) or Chkcfp(alltrim(.w_Dgfisint),'CF')) And (Not Empty(.w_Dgfisint) Or .w_Dgtipfor='01'))
    endwith
    return bRes
  endfunc

  add object oDGDATIMP_4_14 as StdField with uid="SYRFGCWSSA",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DGDATIMP", cQueryName = "DGDATIMP",;
    bObbl = .t. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dell'impegno",;
    HelpContextID = 117741946,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=234, Top=353

  func oDGDATIMP_4_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_Dgfisint) Or .w_Dgfirint='1')
    endwith
   endif
  endfunc

  add object oDGFIRINT_4_16 as StdCheck with uid="BUMXXWYYBJ",rtseq=62,rtrep=.f.,left=234, top=385, caption="Firma intermediario",;
    ToolTipText = "Firma dell'intermediario",;
    HelpContextID = 119306614,;
    cFormVar="w_DGFIRINT", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oDGFIRINT_4_16.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oDGFIRINT_4_16.GetRadio()
    this.Parent.oContained.w_DGFIRINT = this.RadioValue()
    return .t.
  endfunc

  func oDGFIRINT_4_16.SetRadio()
    this.Parent.oContained.w_DGFIRINT=trim(this.Parent.oContained.w_DGFIRINT)
    this.value = ;
      iif(this.Parent.oContained.w_DGFIRINT=='1',1,;
      0)
  endfunc


  add object oDGTIPGEN_4_17 as StdCombo with uid="MZNEHMGOQQ",rtseq=63,rtrep=.f.,left=101,top=423,width=102,height=22;
    , ToolTipText = "Identifica il tipo di generazione, multipla o per singolo file";
    , HelpContextID = 113534596;
    , cFormVar="w_DGTIPGEN",RowSource=""+"Multipla,"+"Singola", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oDGTIPGEN_4_17.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oDGTIPGEN_4_17.GetRadio()
    this.Parent.oContained.w_DGTIPGEN = this.RadioValue()
    return .t.
  endfunc

  func oDGTIPGEN_4_17.SetRadio()
    this.Parent.oContained.w_DGTIPGEN=trim(this.Parent.oContained.w_DGTIPGEN)
    this.value = ;
      iif(this.Parent.oContained.w_DGTIPGEN=='M',1,;
      iif(this.Parent.oContained.w_DGTIPGEN=='S',2,;
      0))
  endfunc


  add object oBtn_4_20 as StdButton with uid="TQEMIMWBDV",left=743, top=453, width=21,height=20,;
    caption="...", nPag=4;
    , ToolTipText = "Seleziona la cartella dove si vuole salvare il file";
    , HelpContextID = 92738602;
  , bGlobalFont=.t.

    proc oBtn_4_20.Click()
      with this.Parent.oContained
        do SfogliaDir with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDGNOMFIL_4_23 as StdField with uid="VPKTICWKHO",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DGNOMFIL", cQueryName = "DGNOMFIL",;
    bObbl = .f. , nPag = 4, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome file e percorso per inoltro telematico",;
    HelpContextID = 174455166,;
   bGlobalFont=.t.,;
    Height=21, Width=636, Left=101, Top=453, InputMask=replicate('X',254)

  func oDGNOMFIL_4_23.mHide()
    with this.Parent.oContained
      return (.w_DGTIPGEN='S')
    endwith
  endfunc


  add object oBtn_4_24 as StdButton with uid="AYLATODQCL",left=716, top=497, width=48,height=45,;
    CpPicture="bmp\SAVE.bmp", caption="", nPag=4;
    , ToolTipText = "Genera file";
    , HelpContextID = 266441750;
    , caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_24.Click()
      with this.Parent.oContained
        do GSUT_BDE with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((.w_Dgtipgen='M' And Not empty(.w_Dgnomfil) Or (.w_Dgtipgen='S' And Not Empty(.w_Path))) and not g_DEMO AND BBVERSION>=1)
      endwith
    endif
  endfunc


  add object oBtn_4_32 as StdButton with uid="PJPVILDQVM",left=662, top=498, width=48,height=45,;
    CpPicture="bmp\STAMPA.BMP", caption="", nPag=4;
    , ToolTipText = "Stampa frontespizio e riepilogo";
    , HelpContextID = 266441750;
    , caption='M\<odello';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_32.Click()
      with this.Parent.oContained
        GSCG_BDT (this.Parent.oContained,"F")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_32.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not g_DEMO And .cFunction<> 'Load'  And .w_Dggenera='S')
      endwith
    endif
  endfunc

  func oBtn_4_32.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY (.w_DGSERIAL))
     endwith
    endif
  endfunc


  add object oLinkPC_4_33 as stdDynamicChildContainer with uid="FQZQYYQCSH",left=2, top=500, width=180, height=39, bOnScreen=.t.;
    , tabstop=.f.

  func oLinkPC_4_33.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (1=1)
     endwith
    endif
  endfunc

  add object oPATH_4_36 as StdField with uid="YJAUALRRMY",rtseq=82,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 4, value=space(150), bMultilanguage =  .f.,;
    HelpContextID = 87858954,;
   bGlobalFont=.t.,;
    Height=21, Width=636, Left=101, Top=453, InputMask=replicate('X',150)

  func oPATH_4_36.mHide()
    with this.Parent.oContained
      return (.w_DGTIPGEN='M')
    endwith
  endfunc

  add object oStr_4_11 as StdString with uid="NECDVAUAQY",Visible=.t., Left=9, Top=317,;
    Alignment=1, Width=220, Height=18,;
    Caption="Codice fiscale dell'intermediario:"  ;
  , bGlobalFont=.t.

  add object oStr_4_12 as StdString with uid="MDEDFCKUHY",Visible=.t., Left=142, Top=279,;
    Alignment=1, Width=87, Height=15,;
    Caption="Tipo fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_4_13 as StdString with uid="ERDKNGNDJP",Visible=.t., Left=12, Top=245,;
    Alignment=0, Width=249, Height=19,;
    Caption="Impegno alla presentazione telematica"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_15 as StdString with uid="AOYTOWESTU",Visible=.t., Left=102, Top=354,;
    Alignment=1, Width=127, Height=18,;
    Caption="Data dell'impegno:"  ;
  , bGlobalFont=.t.

  add object oStr_4_18 as StdString with uid="SSBWQANJWI",Visible=.t., Left=14, Top=455,;
    Alignment=1, Width=81, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  func oStr_4_18.mHide()
    with this.Parent.oContained
      return (.w_DGTIPGEN='S')
    endwith
  endfunc

  add object oStr_4_25 as StdString with uid="RGOUURYTBN",Visible=.t., Left=14, Top=7,;
    Alignment=0, Width=135, Height=19,;
    Caption="Quadro A - Plafond"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_26 as StdString with uid="DTTUPTNQMB",Visible=.t., Left=47, Top=47,;
    Alignment=1, Width=87, Height=18,;
    Caption="Tipo plafond:"  ;
  , bGlobalFont=.t.

  add object oStr_4_27 as StdString with uid="VKEJVAXGVD",Visible=.t., Left=14, Top=46,;
    Alignment=0, Width=18, Height=19,;
    Caption="A1"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_28 as StdString with uid="SDLGFJVRJT",Visible=.t., Left=14, Top=87,;
    Alignment=0, Width=385, Height=19,;
    Caption="Operazioni che concorrono alla formazione del plafond"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_30 as StdString with uid="QTXVWFXMHF",Visible=.t., Left=14, Top=137,;
    Alignment=0, Width=18, Height=19,;
    Caption="A2"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_34 as StdString with uid="RTTAWXVYHF",Visible=.t., Left=2, Top=424,;
    Alignment=1, Width=93, Height=18,;
    Caption="Generazione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_35 as StdString with uid="KETDWHVEYG",Visible=.t., Left=18, Top=455,;
    Alignment=1, Width=77, Height=18,;
    Caption="Percorso:"  ;
  , bGlobalFont=.t.

  func oStr_4_35.mHide()
    with this.Parent.oContained
      return (.w_DGTIPGEN='M')
    endwith
  endfunc

  add object oBox_4_19 as StdBox with uid="VKGHAQHIBX",left=2, top=29, width=777,height=387

  add object oBox_4_29 as StdBox with uid="UCUPGYQUZO",left=3, top=79, width=775,height=2

  add object oBox_4_31 as StdBox with uid="PBSRADUHJG",left=4, top=266, width=775,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_ade','GEDICEME','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DGSERIAL=GEDICEME.DGSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_ade
proc SfogliaDir (parent)
local PathName, oField , L_PATHMEM
   if  parent.w_DGTIPGEN='M'
     PathName = Cp_GetDir()
     if !empty(PathName)
       parent.w_DirName = PathName
       parent.w_DGNOMFIL=left(parent.w_DirName+Alltrim(parent.w_DGSERIAL)+'.IVI'+space(254),254)
     endif
   else
     L_PATHMEM=parent.w_PATH
    parent.w_PATH=Alltrim(left(Cp_Getdir(IIF(EMPTY(parent.w_PATH),sys(5)+sys(2003),parent.w_PATH),"Selezionare il percorso di salvataggio.","----")+space(200),200))
    If empty(parent.w_PATH)
      parent.w_PATH=L_PATHMEM
    Endif
    Release L_PATHMEM
   endif
endproc
* --- Fine Area Manuale
