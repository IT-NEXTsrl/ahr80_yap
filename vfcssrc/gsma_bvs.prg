* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bvs                                                        *
*              Test modifiche inventari                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_13]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-08                                                      *
* Last revis.: 2000-09-08                                                      *
*                                                                              *
* Per non ricalcolare l'inventario se viene modificato solo lo stato nell'anagrafi*
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bvs",oParentObject)
return(i_retval)

define class tgsma_bvs as StdBatch
  * --- Local variables
  w_DATMEM = ctod("  /  /  ")
  w_DATINV = ctod("  /  /  ")
  w_TIPINV = space(1)
  w_MAGCOL = space(1)
  w_CODMAG = space(5)
  w_FLGLCO = space(1)
  w_CATOMO = space(5)
  w_FLGLSC = space(1)
  w_GRUMER = space(5)
  w_FLGFCO = space(1)
  w_CODFAM = space(5)
  w_ESEPRE = space(4)
  w_NUMPRE = space(6)
  * --- WorkFile variables
  INVENTAR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invocato da GSMA_AIN su Update Start
    * --- Setta a .T. il flag w_UPDINV (aggiorna l'inventario) se � stata modificata un campo che non sia lo stato INSTAINV
    * --- Variabili globali (da GSMA_AIN)
    * --- Variabili locali
    * --- Leggo le caratteristiche dell'inventario salvate nel database
    * --- Read from INVENTAR
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.INVENTAR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2],.t.,this.INVENTAR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "INDATMEM,INTIPINV,INCODMAG,INCATOMO,INGRUMER,INCODFAM,INDATINV,INMAGCOL,INFLGLCO,INFLGLSC,INFLGFCO,INESEPRE,INNUMPRE"+;
        " from "+i_cTable+" INVENTAR where ";
            +"INNUMINV = "+cp_ToStrODBC(this.oParentObject.w_INNUMINV);
            +" and INCODESE = "+cp_ToStrODBC(this.oParentObject.w_INCODESE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        INDATMEM,INTIPINV,INCODMAG,INCATOMO,INGRUMER,INCODFAM,INDATINV,INMAGCOL,INFLGLCO,INFLGLSC,INFLGFCO,INESEPRE,INNUMPRE;
        from (i_cTable) where;
            INNUMINV = this.oParentObject.w_INNUMINV;
            and INCODESE = this.oParentObject.w_INCODESE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATMEM = NVL(cp_ToDate(_read_.INDATMEM),cp_NullValue(_read_.INDATMEM))
      this.w_TIPINV = NVL(cp_ToDate(_read_.INTIPINV),cp_NullValue(_read_.INTIPINV))
      this.w_CODMAG = NVL(cp_ToDate(_read_.INCODMAG),cp_NullValue(_read_.INCODMAG))
      this.w_CATOMO = NVL(cp_ToDate(_read_.INCATOMO),cp_NullValue(_read_.INCATOMO))
      this.w_GRUMER = NVL(cp_ToDate(_read_.INGRUMER),cp_NullValue(_read_.INGRUMER))
      this.w_CODFAM = NVL(cp_ToDate(_read_.INCODFAM),cp_NullValue(_read_.INCODFAM))
      this.w_DATINV = NVL(cp_ToDate(_read_.INDATINV),cp_NullValue(_read_.INDATINV))
      this.w_MAGCOL = NVL(cp_ToDate(_read_.INMAGCOL),cp_NullValue(_read_.INMAGCOL))
      this.w_FLGLCO = NVL(cp_ToDate(_read_.INFLGLCO),cp_NullValue(_read_.INFLGLCO))
      this.w_FLGLSC = NVL(cp_ToDate(_read_.INFLGLSC),cp_NullValue(_read_.INFLGLSC))
      this.w_FLGFCO = NVL(cp_ToDate(_read_.INFLGFCO),cp_NullValue(_read_.INFLGFCO))
      this.w_ESEPRE = NVL(cp_ToDate(_read_.INESEPRE),cp_NullValue(_read_.INESEPRE))
      this.w_NUMPRE = NVL(cp_ToDate(_read_.INNUMPRE),cp_NullValue(_read_.INNUMPRE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Verifico se � stato cambiato uno dei campi
    this.oParentObject.w_UPDINV = this.oParentObject.w_INDATMEM<>this.w_DATMEM or this.oParentObject.w_INTIPINV<>this.w_TIPINV or this.oParentObject.w_INCODMAG<>this.w_CODMAG or this.oParentObject.w_INCATOMO<>this.w_CATOMO
    this.oParentObject.w_UPDINV = this.oParentObject.w_UPDINV or this.oParentObject.w_INGRUMER<>this.w_GRUMER or this.oParentObject.w_INCODFAM<>this.w_CODFAM or this.oParentObject.w_INDATINV<>this.w_DATINV
    this.oParentObject.w_UPDINV = this.oParentObject.w_UPDINV or this.oParentObject.w_INMAGCOL<>this.w_MAGCOL or this.oParentObject.w_INFLGLCO<>this.w_FLGLCO or this.oParentObject.w_INFLGLSC<>this.w_FLGLSC or this.oParentObject.w_INFLGFCO<>this.w_FLGFCO
    this.oParentObject.w_UPDINV = this.oParentObject.w_UPDINV or this.oParentObject.w_INESEPRE<>this.w_ESEPRE or this.oParentObject.w_INNUMPRE<>this.w_NUMPRE
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='INVENTAR'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
