* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bzm                                                        *
*              Tasto destro per doc/M.M/pos                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_10]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-09-13                                                      *
* Last revis.: 2012-06-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bzm",oParentObject)
return(i_retval)

define class tgszm_bzm as StdBatch
  * --- Local variables
  w_SERIALE = space(10)
  w_NUMRIF = 0
  w_SERIAL1 = space(15)
  w_GEST = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Per lanciare gestione Documenti, Movimenti di magazzino, POS, ODL/OCL da tasto destro su zoom integrato
    this.w_SERIALE = g_oMenu.getValue("MVSERIAL")
    if Empty(this.w_SERIALE)
      this.w_SERIALE = g_oMenu.oParentObject.w_SERIALE
      this.w_NUMRIF = g_oMenu.oParentObject.w_NUMRIF
    else
      if USED(g_oMenu.ccursor)
        this.w_NUMRIF = Nvl(g_oMenu.getValue("MVNUMRIF"),-20)
      else
        this.w_NUMRIF = -20
      endif
    endif
    if this.w_NUMRIF=-40 or this.w_NUMRIF=-50
      * --- Solo ODL
      this.w_SERIAL1 = g_oMenu.oParentObject.w_SERIAL1
    endif
    this.w_GEST = gsar_bzm(g_oMenu.oParentObject,this.w_SERIALE,this.w_NUMRIF,this.w_SERIAL1)
    do case
      case g_oMenu.cBatchType = "M"
        if this.w_GEST.HasCpEvents("ecpEdit")
          this.w_GEST.ecpEdit()     
        endif
      case g_oMenu.cBatchType = "L"
        if this.w_GEST.HasCpEvents("ecpLoad")
          this.w_GEST.ecpLoad()     
        endif
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
