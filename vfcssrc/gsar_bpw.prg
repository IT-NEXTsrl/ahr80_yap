* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bpw                                                        *
*              WE - gestione account                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_103]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-11                                                      *
* Last revis.: 2002-05-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OPERAZIONE,w_MODALITA
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bpw",oParentObject,m.w_OPERAZIONE,m.w_MODALITA)
return(i_retval)

define class tgsar_bpw as StdBatch
  * --- Local variables
  w_OPERAZIONE = space(10)
  w_MODALITA = space(10)
  w_DESCRITTORE = space(50)
  w_CODAZI = space(5)
  w_EMLDESC = space(75)
  w_NOMDESC = space(50)
  w_DOMINIO = space(50)
  w_PUNPAD = .NULL.
  w_RET = .f.
  w_TMPC = space(100)
  w_TIPCON = space(1)
  w_CODICE = space(15)
  w_TMPC = space(100)
  w_NC = space(10)
  w_COUNTER = 0
  w_N = 0
  w_ACCATT = space(1)
  w_ANTIPCON = space(1)
  w_ANCODCON = space(15)
  w_ANDESCRI = space(30)
  w_ANCODFIS = space(16)
  w_ANPARIVA = space(12)
  w_ANCATCOM = space(3)
  w_ANTELEFO = space(20)
  w_WELOGIN = space(35)
  w_WEINDEML = space(50)
  w_WENUMSMS = space(20)
  w_WENUMFAX = space(20)
  w_GRUPPO = space(30)
  w_COGNOME = space(40)
  w_NOME = space(40)
  w_PASSWORD = space(50)
  w_PROFILO = space(50)
  * --- WorkFile variables
  AZIENDA_idx=0
  CATECOMM_idx=0
  WECONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Richiamato da GSAR_APW
    * --- Dichiarazione Variabili
    this.w_PUNPAD = this.oParentObject
    do case
      case this.w_OPERAZIONE = "INSERT" or this.w_OPERAZIONE="DELETE" or this.w_OPERAZIONE="UPDATE"
        * --- Verifica se attivo sistema di posta
        if oCPMapi.mapiService<>1
          ah_ErrorMsg("Sevizio MAPI non disponibile","STOP","")
          i_retcode = 'stop'
          return
        endif
        * --- Controlli
        if this.w_MODALITA="SINGOLA"
          * --- Controlli LOGIN
          do case
            case empty(this.oParentObject.w_WE_LOGIN)
              ah_ErrorMsg("Occorre specificare la login","!","")
              i_retcode = 'stop'
              return
            case LEN(ALLTRIM(this.oParentObject.w_WETXTSMS))>150
              ah_ErrorMsg("Lunghezza messaggio per SMS superiore a 150 caratteri","!","")
              i_retcode = 'stop'
              return
            case this.oParentObject.w_WEAVVFAX="S" and empty(this.w_PUNPAD.w_WENUMFAX)
              ah_ErrorMsg("Occorre specificare un numero di FAX","!","")
              i_retcode = 'stop'
              return
            case this.oParentObject.w_WEAVVSMS="S" and empty(this.w_PUNPAD.w_WENUMSMS)
              ah_ErrorMsg("Occorre specificare un numero di cellulare per invio SMS di notifica","!","")
              i_retcode = 'stop'
              return
            case this.oParentObject.w_WEAVVEML="S" and empty(this.w_PUNPAD.w_WEINDEML)
              ah_ErrorMsg("Occorre specificare l'indirizzo E-mail per invio della notifica","!","")
              i_retcode = 'stop'
              return
            otherwise
              * --- Verifica che l'account non sia gi� presente
              * --- Read from WECONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.WECONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.WECONTI_idx,2],.t.,this.WECONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "WETIPCON,WECODICE"+;
                  " from "+i_cTable+" WECONTI where ";
                      +"WE_LOGIN = "+cp_ToStrODBC(this.oParentObject.w_WE_LOGIN);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  WETIPCON,WECODICE;
                  from (i_cTable) where;
                      WE_LOGIN = this.oParentObject.w_WE_LOGIN;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_TIPCON = NVL(cp_ToDate(_read_.WETIPCON),cp_NullValue(_read_.WETIPCON))
                this.w_CODICE = NVL(cp_ToDate(_read_.WECODICE),cp_NullValue(_read_.WECODICE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if NOT EMPTY(this.w_CODICE) and not (this.oParentObject.w_WETIPCON=this.w_TIPCON and this.oParentObject.w_WECODICE=this.w_CODICE)
                if this.w_TIPCON="F"
                  this.w_TMPC = "La login <%1> � gi� stata utilizzata%0per il fornitore cod. %2"
                else
                  this.w_TMPC = "La login <%1> � gi� stata utilizzata%0per il cliente cod. %2"
                endif
                ah_ErrorMsg(this.w_TMPC,"!","", alltrim(this.oParentObject.w_WE_LOGIN), this.w_CODICE )
                i_retcode = 'stop'
                return
              endif
          endcase
        endif
        * --- Controllo avvisi
        if this.oParentObject.w_WEAVVEML<>"S" and this.oParentObject.w_WEAVVSMS<>"S" and this.oParentObject.w_WEAVVFAX<>"S" and this.w_OPERAZIONE<>"DELETE"
          do case
            case this.w_OPERAZIONE="INSERT"
              if this.w_MODALITA="SINGOLA"
                this.w_TMPC = "Non � stato selezionata nessuna modalit� di notifica pubblicazione (E-mail, SMS, FAX)%0Proseguo con l'attivazione dell'account?"
              else
                this.w_TMPC = "Non � stato selezionata nessuna modalit� di notifica pubblicazione (E-mail, SMS, FAX)%0Proseguo con l'attivazione degli account?"
              endif
            case this.w_OPERAZIONE="UPDATE"
              if this.w_MODALITA="SINGOLA"
                this.w_TMPC = "Non � stato selezionata nessuna modalit� di notifica pubblicazione (E-mail, SMS, FAX)%0Proseguo con l'aggiornamento dell'account?"
              else
                this.w_TMPC = "Non � stato selezionata nessuna modalit� di notifica pubblicazione (E-mail, SMS, FAX)%0Proseguo con l'aggiornamento degli account?"
              endif
            otherwise
              if this.w_MODALITA="SINGOLA"
                this.w_TMPC = "Non � stato selezionata nessuna modalit� di notifica pubblicazione (E-mail, SMS, FAX)%0Proseguo con la cancellazione dell'account?"
              else
                this.w_TMPC = "Non � stato selezionata nessuna modalit� di notifica pubblicazione (E-mail, SMS, FAX)%0Proseguo con la cancellazione degli account?"
              endif
          endcase
          if not ah_YesNo(this.w_TMPC)
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Definisce array per lista anagrafiche
        Dimension L_ARRAYANAG(1,12)
        * --- Legge da Azienda l'indirizzo di email per inviare il descrittore
        this.w_CODAZI = i_CODAZI
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZ_EMLDE,AZNOMDSC,AZ_DOMWE"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZ_EMLDE,AZNOMDSC,AZ_DOMWE;
            from (i_cTable) where;
                AZCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_EMLDESC = NVL(cp_ToDate(_read_.AZ_EMLDE),cp_NullValue(_read_.AZ_EMLDE))
          this.w_NOMDESC = NVL(cp_ToDate(_read_.AZNOMDSC),cp_NullValue(_read_.AZNOMDSC))
          this.w_DOMINIO = NVL(cp_ToDate(_read_.AZ_DOMWE),cp_NullValue(_read_.AZ_DOMWE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Controlli ------------------------------------------------------------------------------------------------------------------------------------
        if empty(this.w_EMLDESC)
          ah_ErrorMsg("Indirizzo E-mail per descrittore non specificato in parametri WE","!","")
          i_retcode = 'stop'
          return
        endif
        if empty(this.w_DOMINIO)
          ah_ErrorMsg("Dominio WE non specificato in parametri WE","!","")
          i_retcode = 'stop'
          return
        endif
        if empty(this.w_NOMDESC)
          ah_ErrorMsg("Nome file descrittore non specificato in parametri WE","!","")
          i_retcode = 'stop'
          return
        endif
        * --- Crea Descrittore ------------------------------------------------------------------------------------------------------------------------------
        this.w_RET = .F.
        do case
          case this.w_MODALITA="SINGOLA"
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.w_MODALITA="MULTIPLA"
            Dimension L_LISTAACCOUNT(1,2)
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
        endcase
        * --- Invia e-mail
        if this.w_RET
          this.w_RET = .F.
          i_eMAIL = this.w_EMLDESC
          dimension L_AllegatiWE(1)
          L_AllegatiWE(1) = this.w_DESCRITTORE
          this.w_RET = EMAIL( @L_AllegatiWE , g_UEDIAL, .T.)
          i_eMAIL = " "
          * --- Se � andato bene l'invio della email, aggiorna anagrafiche
          if this.w_RET 
            do case
              case this.w_MODALITA="SINGOLA"
                * --- Torna al padre
                this.w_PUNPAD.ECPSave()     
              case this.w_MODALITA="MULTIPLA"
                this.Pag5()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                do case
                  case this.w_OPERAZIONE="INSERT"
                    ah_ErrorMsg("Generazione account WE terminata con successo","i","")
                  case this.w_OPERAZIONE="UPDATE"
                    ah_ErrorMsg("Aggiornamento account WE terminata con successo","i","")
                  otherwise
                    ah_ErrorMsg("Cancellazione account WE terminata con successo","i","")
                endcase
                * --- Torna al padre
                this.w_PUNPAD.NotifyEvent("Interroga")     
                this.oParentObject.w_SELEZI = "D"
            endcase
          endif
        endif
      case this.w_OPERAZIONE="LOGIN"
        * --- Crea LOGIN
        if this.oParentObject.w_WEACCATT="S"
          ah_ErrorMsg("Impossibile variare la login, account gi� attivato","!","")
        else
          this.w_PUNPAD = this.oParentObject
          this.oParentObject.w_WE_LOGIN = WEVERTXT( this.w_PUNPAD.w_WETIPCON+alltrim(i_CODAZI)+alltrim(this.w_PUNPAD.w_WECODICE) , .F., "A")
        endif
      case this.w_OPERAZIONE="CHECK"
        if EMPTY( this.oParentObject.w_WE_LOGIN )
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=ah_Msgformat("Occorre specificare la login per i servizi WE")
        else
          * --- Verifica che l'account non sia gi� presente
          * --- Read from WECONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.WECONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.WECONTI_idx,2],.t.,this.WECONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "WETIPCON,WECODICE"+;
              " from "+i_cTable+" WECONTI where ";
                  +"WE_LOGIN = "+cp_ToStrODBC(this.oParentObject.w_WE_LOGIN);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              WETIPCON,WECODICE;
              from (i_cTable) where;
                  WE_LOGIN = this.oParentObject.w_WE_LOGIN;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPCON = NVL(cp_ToDate(_read_.WETIPCON),cp_NullValue(_read_.WETIPCON))
            this.w_CODICE = NVL(cp_ToDate(_read_.WECODICE),cp_NullValue(_read_.WECODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT EMPTY(this.w_CODICE) and not (this.oParentObject.w_WETIPCON=this.w_TIPCON and this.oParentObject.w_WECODICE=this.w_CODICE)
            if this.w_TIPCON="F"
              this.w_TMPC = ah_Msgformat("La login <%1> � gi� stata utilizzata%0per il fornitore cod. %2", alltrim(this.oParentObject.w_WE_LOGIN), this.w_CODICE )
            else
              this.w_TMPC = ah_Msgformat("La login <%1> � gi� stata utilizzata%0per il cliente cod. %2", alltrim(this.oParentObject.w_WE_LOGIN), this.w_CODICE )
            endif
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_TMPC
          endif
        endif
      case this.w_OPERAZIONE="SELDESEL"
        * --- Punta al padre
        this.w_NC = this.w_PUNPAD.w_ZoomAnag.cCursor
        if used(this.w_NC)
          * --- Seleziona tutte le righe dello zoom
          UPDATE (this.w_NC) SET xChk=iif(this.oParentObject.w_SELEZI="S",1,0)
        endif
      case this.w_OPERAZIONE="INTERROGA"
        * --- Punta al padre
        this.w_PUNPAD.NotifyEvent("Interroga")     
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Anagrafica SINGOLA ----------------------------
    this.w_WELOGIN = WEVERTXT( alltrim(this.w_PUNPAD.w_WE_LOGIN) , .F., "A")
    this.w_PASSWORD = this.oParentObject.w_WE_LOGIN
    this.w_ANPARIVA = this.w_PUNPAD.w_PARIVA
    this.w_ANCODFIS = this.w_PUNPAD.w_CODFIS
    this.w_ANPARIVA = IIF(EMPTY(this.w_ANPARIVA),this.w_ANCODFIS,this.w_ANPARIVA)
    this.w_ANDESCRI = WEVERTXT(this.w_PUNPAD.w_RAGSOC ,.F.)
    this.w_COGNOME = this.w_ANDESCRI
    this.w_NOME = " "
    this.w_WEINDEML = this.w_PUNPAD.w_WEINDEML
    this.w_ANTIPCON = this.w_PUNPAD.w_WETIPCON
    this.w_ANCODCON = this.w_PUNPAD.w_WECODICE
    this.w_ANTELEFO = this.w_PUNPAD.w_ANTELEFO
    this.w_WENUMSMS = this.w_PUNPAD.w_WENUMSMS
    this.w_WENUMFAX = this.w_PUNPAD.w_WENUMFAX
    this.w_GRUPPO = IIF(EMPTY(this.w_PUNPAD.w_GRUPPO), "DEFAULT", this.w_PUNPAD.w_GRUPPO)
    this.w_PROFILO = "Default"
    * --- Controlli campi obbligatori
    if empty(this.w_ANPARIVA)
      ah_ErrorMsg("Partita IVA o codice fiscale non specificati%0Impossibile continuare","!","")
      i_retcode = 'stop'
      return
    endif
    * --- Prepara array da passare alla WEDESXML
    this.w_COUNTER = 1
    Dimension L_ARRAYANAG(1,12)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Lancia programma di creazione descrittore XML
    this.w_DESCRITTORE = tempadhoc()+"\"+alltrim(this.w_NOMDESC)
    this.w_RET = WEDESXML(this.w_DESCRITTORE,this.w_DOMINIO,"ANAGRAFICA",this.w_OPERAZIONE, @L_ARRAYANAG)
    * --- Se ok rende account attivo
    if this.w_RET 
      this.oParentObject.w_WEACCATT = IIF(this.w_OPERAZIONE="INSERT","S",IIF(this.w_OPERAZIONE="DELETE","N",this.oParentObject.w_WEACCATT))
    endif
    * --- *
    *     
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione anagrafiche MULTIPLE ---------------------------------------------------------------------------
    this.w_NC = this.w_PUNPAD.w_ZoomAnag.cCursor
    if used(this.w_NC)
      * --- Seleziona tutte le righe dello zoom
      this.w_COUNTER = 1
      select (this.w_NC)
      go top
      scan for xChk=1
      * --- Prepara variabili
      do case
        case this.w_OPERAZIONE="INSERT" 
          this.w_WELOGIN = WEVERTXT( NVL(ANTIPCON," ") + alltrim(i_CODAZI) + alltrim(NVL(ANCODICE," ")) , .F., "A")
          this.w_ANTIPCON = NVL(ANTIPCON," ")
          this.w_ANCODCON = NVL(ANCODICE," ")
          * --- Verifica se nel database di AHE c'� gi� un utente con questa login
          this.w_ACCATT = "N"
          this.w_TIPCON = " "
          this.w_CODICE = " "
          * --- Read from WECONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.WECONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.WECONTI_idx,2],.t.,this.WECONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "WETIPCON,WECODICE,WEACCATT"+;
              " from "+i_cTable+" WECONTI where ";
                  +"WE_LOGIN = "+cp_ToStrODBC(this.w_WELOGIN);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              WETIPCON,WECODICE,WEACCATT;
              from (i_cTable) where;
                  WE_LOGIN = this.w_WELOGIN;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPCON = NVL(cp_ToDate(_read_.WETIPCON),cp_NullValue(_read_.WETIPCON))
            this.w_CODICE = NVL(cp_ToDate(_read_.WECODICE),cp_NullValue(_read_.WECODICE))
            this.w_ACCATT = NVL(cp_ToDate(_read_.WEACCATT),cp_NullValue(_read_.WEACCATT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if empty(this.w_CODICE) or (this.w_TIPCON+this.w_CODICE=this.w_ANTIPCON+this.w_ANCODCON and this.w_ACCATT<>"S")
            * --- Se tutto ok continua
            this.w_PASSWORD = this.w_WELOGIN
            this.w_ANPARIVA = rtrim(IIF(EMPTY(NVL(ANPARIVA," ")),ANCODFIS,ANPARIVA))
            this.w_COGNOME = WEVERTXT( ALLTRIM(NVL(ANDESCRI," ")) , .F.)
            this.w_NOME = " "
            this.w_WEINDEML = alltrim(AN_EMAIL)
            this.w_GRUPPO = WEVERTXT( IIF(empty(NVL(CTDESCRI," ")),"DEFAULT",upper(NVL(CTDESCRI," "))), .F.)
            this.w_PROFILO = "Default"
            this.w_ANDESCRI = this.w_COGNOME
            this.w_WENUMSMS = NVL(ANNUMCEL," ")
            this.w_WENUMSMS = STRTRAN(STRTRAN(STRTRAN(STRTRAN(this.w_WENUMSMS,"-",""),"/",""),"\","")," ","")
            this.w_WENUMFAX = NVL(ANTELFAX," ")
            this.w_WENUMFAX = STRTRAN(STRTRAN(STRTRAN(STRTRAN(this.w_WENUMFAX,"-",""),"/",""),"\","")," ","")
            this.w_ANTELEFO = NVL(ANTELEFO," ")
            this.w_ANTELEFO = STRTRAN(STRTRAN(STRTRAN(STRTRAN(this.w_ANTELEFO,"-",""),"/",""),"\","")," ","")
            * --- Crea Array
            this.Pag4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Incrementa contatore
            this.w_COUNTER = this.w_COUNTER + 1
          endif
        case this.w_OPERAZIONE="UPDATE" or this.w_OPERAZIONE="DELETE"
          this.w_WELOGIN = NVL(WE_LOGIN, " ")
          this.w_ANTIPCON = NVL(ANTIPCON," ")
          this.w_ANCODCON = NVL(ANCODICE," ")
          * --- La password non deve essere considerata dal descrittore
          this.w_PASSWORD = this.w_WELOGIN
          this.w_ANPARIVA = rtrim(IIF(EMPTY(NVL(ANPARIVA," ")),ANCODFIS,ANPARIVA))
          this.w_COGNOME = WEVERTXT( ALLTRIM(NVL(ANDESCRI," ")) , .F.)
          this.w_NOME = " "
          this.w_WEINDEML = alltrim( IIF(EMPTY(NVL(WEINDEML," ")), NVL(AN_EMAIL," "), NVL(WEINDEML," ")) )
          this.w_GRUPPO = WEVERTXT( IIF(empty(NVL(CTDESCRI," ")),"DEFAULT",upper(NVL(CTDESCRI," "))), .F.)
          this.w_PROFILO = "Default"
          this.w_ANDESCRI = this.w_COGNOME
          this.w_WENUMSMS = alltrim( IIF(EMPTY(NVL(WENUMSMS," ")), NVL(ANNUMCEL," "), NVL(WENUMSMS," ")) ) 
          this.w_WENUMSMS = STRTRAN(STRTRAN(STRTRAN(STRTRAN(this.w_WENUMSMS,"-",""),"/",""),"\","")," ","")
          this.w_WENUMFAX = alltrim( IIF(EMPTY(NVL(WENUMFAX," ")), NVL(ANTELFAX," "), NVL(WENUMFAX," ")) ) 
          this.w_WENUMFAX = STRTRAN(STRTRAN(STRTRAN(STRTRAN(this.w_WENUMFAX,"-",""),"/",""),"\","")," ","")
          this.w_ANTELEFO = NVL(ANTELEFO," ")
          this.w_ANTELEFO = STRTRAN(STRTRAN(STRTRAN(STRTRAN(this.w_ANTELEFO,"-",""),"/",""),"\","")," ","")
          * --- Crea Array
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Incrementa contatore
          this.w_COUNTER = this.w_COUNTER + 1
      endcase
      endscan
      * --- Lancia programma di creazione descrittore XML
      if this.w_COUNTER>1
        this.w_DESCRITTORE = tempadhoc()+"\"+alltrim(this.w_NOMDESC)
        this.w_RET = WEDESXML(this.w_DESCRITTORE,this.w_DOMINIO,"ANAGRAFICA",this.w_OPERAZIONE, @L_ARRAYANAG)
      else
        do case
          case this.w_OPERAZIONE="INSERT"
            ah_ErrorMsg("Non sono stati selezionati account da generare","i","")
          case this.w_OPERAZIONE="UPDATE"
            ah_ErrorMsg("Non sono stati selezionati account da aggiornare","i","")
          otherwise
            ah_ErrorMsg("Non sono stati selezionati account da cancellare","i","")
        endcase
        this.w_RET = .F.
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Prepara ARRAY
    Dimension L_ARRAYANAG(this.w_COUNTER,12)
    L_ARRAYANAG(this.w_COUNTER,1) = alltrim(this.w_WELOGIN)
    L_ARRAYANAG(this.w_COUNTER,2) = alltrim(this.w_PASSWORD)
    L_ARRAYANAG(this.w_COUNTER,3) = alltrim(this.w_ANPARIVA)
    L_ARRAYANAG(this.w_COUNTER,4) = alltrim(this.w_COGNOME)
    L_ARRAYANAG(this.w_COUNTER,5) = alltrim(this.w_NOME)
    L_ARRAYANAG(this.w_COUNTER,6) = alltrim(this.w_WEINDEML)
    L_ARRAYANAG(this.w_COUNTER,7) = alltrim(this.w_GRUPPO)
    L_ARRAYANAG(this.w_COUNTER,8) = alltrim(this.w_PROFILO)
    L_ARRAYANAG(this.w_COUNTER,9) = alltrim(this.w_ANDESCRI)
    L_ARRAYANAG(this.w_COUNTER,10) = alltrim(this.w_WENUMSMS)
    L_ARRAYANAG(this.w_COUNTER,11) = alltrim(this.w_WENUMFAX)
    L_ARRAYANAG(this.w_COUNTER,12) = alltrim(this.w_ANTELEFO)
    * --- Aggiorna Lista Account
    Dimension L_LISTAACCOUNT(this.w_COUNTER,2)
    L_LISTAACCOUNT(this.w_COUNTER,1) = this.w_ANTIPCON
    L_LISTAACCOUNT(this.w_COUNTER,2) = this.w_ANCODCON
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna WECONTI
    this.w_COUNTER = 1
    do while this.w_COUNTER <= ALEN(L_LISTAACCOUNT,1)
      * --- Identifica CLI/FOR
      this.w_ANTIPCON = L_LISTAACCOUNT(this.w_COUNTER,1)
      this.w_ANCODCON = L_LISTAACCOUNT(this.w_COUNTER,2)
      this.w_WELOGIN = L_ARRAYANAG(this.w_COUNTER,1)
      this.w_WEINDEML = L_ARRAYANAG(this.w_COUNTER,6)
      this.w_WENUMSMS = L_ARRAYANAG(this.w_COUNTER,10)
      this.w_WENUMFAX = L_ARRAYANAG(this.w_COUNTER,11)
      * --- Aggiorna WECONTI
      if this.w_OPERAZIONE="DELETE"
        * --- Try
        local bErr_0385A4F8
        bErr_0385A4F8=bTrsErr
        this.Try_0385A4F8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0385A4F8
        * --- End
      else
        * --- Try
        local bErr_03ED9B70
        bErr_03ED9B70=bTrsErr
        this.Try_03ED9B70()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_03ED9B70
        * --- End
        * --- Try
        local bErr_0402E9B0
        bErr_0402E9B0=bTrsErr
        this.Try_0402E9B0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0402E9B0
        * --- End
      endif
      * --- Incrementa contatore
      this.w_COUNTER = this.w_COUNTER + 1
    enddo
  endproc
  proc Try_0385A4F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into WECONTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.WECONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.WECONTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.WECONTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"WEACCATT ="+cp_NullLink(cp_ToStrODBC("N"),'WECONTI','WEACCATT');
          +i_ccchkf ;
      +" where ";
          +"WETIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
          +" and WECODICE = "+cp_ToStrODBC(this.w_ANCODCON);
             )
    else
      update (i_cTable) set;
          WEACCATT = "N";
          &i_ccchkf. ;
       where;
          WETIPCON = this.w_ANTIPCON;
          and WECODICE = this.w_ANCODCON;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_03ED9B70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into WECONTI
    i_nConn=i_TableProp[this.WECONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.WECONTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.WECONTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"WETIPCON"+",WECODICE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ANTIPCON),'WECONTI','WETIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODCON),'WECONTI','WECODICE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'WETIPCON',this.w_ANTIPCON,'WECODICE',this.w_ANCODCON)
      insert into (i_cTable) (WETIPCON,WECODICE &i_ccchkf. );
         values (;
           this.w_ANTIPCON;
           ,this.w_ANCODCON;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0402E9B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into WECONTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.WECONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.WECONTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.WECONTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"WE_LOGIN ="+cp_NullLink(cp_ToStrODBC(this.w_WELOGIN),'WECONTI','WE_LOGIN');
      +",WEACCATT ="+cp_NullLink(cp_ToStrODBC("S"),'WECONTI','WEACCATT');
      +",WEDESCSM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_WEDESCSM),'WECONTI','WEDESCSM');
      +",WEAVVEML ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_WEAVVEML),'WECONTI','WEAVVEML');
      +",WEINDEML ="+cp_NullLink(cp_ToStrODBC(this.w_WEINDEML),'WECONTI','WEINDEML');
      +",WETXTEML ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_WETXTEML),'WECONTI','WETXTEML');
      +",WEAVVFAX ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_WEAVVFAX),'WECONTI','WEAVVFAX');
      +",WENUMFAX ="+cp_NullLink(cp_ToStrODBC(this.w_WENUMFAX),'WECONTI','WENUMFAX');
      +",WEAVVSMS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_WEAVVSMS),'WECONTI','WEAVVSMS');
      +",WENUMSMS ="+cp_NullLink(cp_ToStrODBC(this.w_WENUMSMS),'WECONTI','WENUMSMS');
      +",WETXTSMS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_WETXTSMS),'WECONTI','WETXTSMS');
      +",WETXTPDF ="+cp_NullLink(cp_ToStrODBC("N"),'WECONTI','WETXTPDF');
          +i_ccchkf ;
      +" where ";
          +"WETIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
          +" and WECODICE = "+cp_ToStrODBC(this.w_ANCODCON);
             )
    else
      update (i_cTable) set;
          WE_LOGIN = this.w_WELOGIN;
          ,WEACCATT = "S";
          ,WEDESCSM = this.oParentObject.w_WEDESCSM;
          ,WEAVVEML = this.oParentObject.w_WEAVVEML;
          ,WEINDEML = this.w_WEINDEML;
          ,WETXTEML = this.oParentObject.w_WETXTEML;
          ,WEAVVFAX = this.oParentObject.w_WEAVVFAX;
          ,WENUMFAX = this.w_WENUMFAX;
          ,WEAVVSMS = this.oParentObject.w_WEAVVSMS;
          ,WENUMSMS = this.w_WENUMSMS;
          ,WETXTSMS = this.oParentObject.w_WETXTSMS;
          ,WETXTPDF = "N";
          &i_ccchkf. ;
       where;
          WETIPCON = this.w_ANTIPCON;
          and WECODICE = this.w_ANCODCON;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_OPERAZIONE,w_MODALITA)
    this.w_OPERAZIONE=w_OPERAZIONE
    this.w_MODALITA=w_MODALITA
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CATECOMM'
    this.cWorkTables[3]='WECONTI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OPERAZIONE,w_MODALITA"
endproc
