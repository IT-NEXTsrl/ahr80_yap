* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_apf                                                        *
*              Dati azienda F24                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-31                                                      *
* Last revis.: 2008-07-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_apf")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_apf")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_apf")
  return

* --- Class definition
define class tgscg_apf as StdPCForm
  Width  = 598
  Height = 168
  Top    = 10
  Left   = 10
  cComment = "Dati azienda F24"
  cPrg = "gscg_apf"
  HelpContextID=160048489
  add object cnt as tcgscg_apf
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_apf as PCContext
  w_AZCODAZI = space(5)
  w_AZCONBAN = space(15)
  w_BATIPCON = space(1)
  w_DATOBSO1 = space(8)
  w_BACONSBF = space(1)
  w_AZBANPRO = space(15)
  w_BATIPCON_B = space(1)
  w_DATOBSO2 = space(8)
  w_BACONSBF_1 = space(1)
  w_AZDESBAN = space(50)
  w_ABIBAN = space(5)
  w_CABBAN = space(5)
  w_CONCOR = space(12)
  w_BANPROABI = space(5)
  w_BANPROCAB = space(5)
  w_BANPROSIA = space(5)
  w_BADESCRI = space(35)
  proc Save(oFrom)
    this.w_AZCODAZI = oFrom.w_AZCODAZI
    this.w_AZCONBAN = oFrom.w_AZCONBAN
    this.w_BATIPCON = oFrom.w_BATIPCON
    this.w_DATOBSO1 = oFrom.w_DATOBSO1
    this.w_BACONSBF = oFrom.w_BACONSBF
    this.w_AZBANPRO = oFrom.w_AZBANPRO
    this.w_BATIPCON_B = oFrom.w_BATIPCON_B
    this.w_DATOBSO2 = oFrom.w_DATOBSO2
    this.w_BACONSBF_1 = oFrom.w_BACONSBF_1
    this.w_AZDESBAN = oFrom.w_AZDESBAN
    this.w_ABIBAN = oFrom.w_ABIBAN
    this.w_CABBAN = oFrom.w_CABBAN
    this.w_CONCOR = oFrom.w_CONCOR
    this.w_BANPROABI = oFrom.w_BANPROABI
    this.w_BANPROCAB = oFrom.w_BANPROCAB
    this.w_BANPROSIA = oFrom.w_BANPROSIA
    this.w_BADESCRI = oFrom.w_BADESCRI
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_AZCODAZI = this.w_AZCODAZI
    oTo.w_AZCONBAN = this.w_AZCONBAN
    oTo.w_BATIPCON = this.w_BATIPCON
    oTo.w_DATOBSO1 = this.w_DATOBSO1
    oTo.w_BACONSBF = this.w_BACONSBF
    oTo.w_AZBANPRO = this.w_AZBANPRO
    oTo.w_BATIPCON_B = this.w_BATIPCON_B
    oTo.w_DATOBSO2 = this.w_DATOBSO2
    oTo.w_BACONSBF_1 = this.w_BACONSBF_1
    oTo.w_AZDESBAN = this.w_AZDESBAN
    oTo.w_ABIBAN = this.w_ABIBAN
    oTo.w_CABBAN = this.w_CABBAN
    oTo.w_CONCOR = this.w_CONCOR
    oTo.w_BANPROABI = this.w_BANPROABI
    oTo.w_BANPROCAB = this.w_BANPROCAB
    oTo.w_BANPROSIA = this.w_BANPROSIA
    oTo.w_BADESCRI = this.w_BADESCRI
    PCContext::Load(oTo)
enddefine

define class tcgscg_apf as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 598
  Height = 168
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-03"
  HelpContextID=160048489
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  AZIENDA_IDX = 0
  COC_MAST_IDX = 0
  cFile = "AZIENDA"
  cKeySelect = "AZCODAZI"
  cKeyWhere  = "AZCODAZI=this.w_AZCODAZI"
  cKeyWhereODBC = '"AZCODAZI="+cp_ToStrODBC(this.w_AZCODAZI)';

  cKeyWhereODBCqualified = '"AZIENDA.AZCODAZI="+cp_ToStrODBC(this.w_AZCODAZI)';

  cPrg = "gscg_apf"
  cComment = "Dati azienda F24"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AZCODAZI = space(5)
  w_AZCONBAN = space(15)
  w_BATIPCON = space(1)
  w_DATOBSO1 = ctod('  /  /  ')
  w_BACONSBF = space(1)
  w_AZBANPRO = space(15)
  w_BATIPCON_B = space(1)
  w_DATOBSO2 = ctod('  /  /  ')
  w_BACONSBF_1 = space(1)
  w_AZDESBAN = space(50)
  w_ABIBAN = space(5)
  w_CABBAN = space(5)
  w_CONCOR = space(12)
  w_BANPROABI = space(5)
  w_BANPROCAB = space(5)
  w_BANPROSIA = space(5)
  w_BADESCRI = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_apfPag1","gscg_apf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 101601034
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAZCONBAN_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='AZIENDA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.AZIENDA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.AZIENDA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_apf'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    local link_1_6_joined
    link_1_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from AZIENDA where AZCODAZI=KeySet.AZCODAZI
    *
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('AZIENDA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "AZIENDA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' AZIENDA '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AZCODAZI',this.w_AZCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_BATIPCON = space(1)
        .w_DATOBSO1 = ctod("  /  /  ")
        .w_BACONSBF = space(1)
        .w_BATIPCON_B = space(1)
        .w_DATOBSO2 = ctod("  /  /  ")
        .w_BACONSBF_1 = space(1)
        .w_ABIBAN = space(5)
        .w_CABBAN = space(5)
        .w_CONCOR = space(12)
        .w_BANPROABI = space(5)
        .w_BANPROCAB = space(5)
        .w_BANPROSIA = space(5)
        .w_BADESCRI = space(35)
        .w_AZCODAZI = NVL(AZCODAZI,space(5))
        .w_AZCONBAN = NVL(AZCONBAN,space(15))
          if link_1_2_joined
            this.w_AZCONBAN = NVL(BACODBAN102,NVL(this.w_AZCONBAN,space(15)))
            this.w_AZDESBAN = NVL(BADESCRI102,space(50))
            this.w_ABIBAN = NVL(BACODABI102,space(5))
            this.w_CABBAN = NVL(BACODCAB102,space(5))
            this.w_BATIPCON = NVL(BATIPCON102,space(1))
            this.w_CONCOR = NVL(BACONCOR102,space(12))
            this.w_DATOBSO1 = NVL(cp_ToDate(BADTOBSO102),ctod("  /  /  "))
            this.w_BACONSBF = NVL(BACONSBF102,space(1))
          else
          .link_1_2('Load')
          endif
        .w_AZBANPRO = NVL(AZBANPRO,space(15))
          if link_1_6_joined
            this.w_AZBANPRO = NVL(BACODBAN106,NVL(this.w_AZBANPRO,space(15)))
            this.w_BANPROABI = NVL(BACODABI106,space(5))
            this.w_BANPROCAB = NVL(BACODCAB106,space(5))
            this.w_BANPROSIA = NVL(BACODSIA106,space(5))
            this.w_BATIPCON_B = NVL(BATIPCON106,space(1))
            this.w_BADESCRI = NVL(BADESCRI106,space(35))
            this.w_DATOBSO2 = NVL(cp_ToDate(BADTOBSO106),ctod("  /  /  "))
            this.w_BACONSBF_1 = NVL(BACONSBF106,space(1))
          else
          .link_1_6('Load')
          endif
        .w_AZDESBAN = NVL(AZDESBAN,space(50))
        cp_LoadRecExtFlds(this,'AZIENDA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_AZCODAZI = space(5)
      .w_AZCONBAN = space(15)
      .w_BATIPCON = space(1)
      .w_DATOBSO1 = ctod("  /  /  ")
      .w_BACONSBF = space(1)
      .w_AZBANPRO = space(15)
      .w_BATIPCON_B = space(1)
      .w_DATOBSO2 = ctod("  /  /  ")
      .w_BACONSBF_1 = space(1)
      .w_AZDESBAN = space(50)
      .w_ABIBAN = space(5)
      .w_CABBAN = space(5)
      .w_CONCOR = space(12)
      .w_BANPROABI = space(5)
      .w_BANPROCAB = space(5)
      .w_BANPROSIA = space(5)
      .w_BADESCRI = space(35)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
          if not(empty(.w_AZCONBAN))
          .link_1_2('Full')
          endif
        .DoRTCalc(3,6,.f.)
          if not(empty(.w_AZBANPRO))
          .link_1_6('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'AZIENDA')
    this.DoRTCalc(7,17,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oAZCONBAN_1_2.enabled = i_bVal
      .Page1.oPag.oAZBANPRO_1_6.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'AZIENDA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCODAZI,"AZCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCONBAN,"AZCONBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZBANPRO,"AZBANPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDESBAN,"AZDESBAN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.AZIENDA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.AZIENDA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into AZIENDA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'AZIENDA')
        i_extval=cp_InsertValODBCExtFlds(this,'AZIENDA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(AZCODAZI,AZCONBAN,AZBANPRO,AZDESBAN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_AZCODAZI)+;
                  ","+cp_ToStrODBCNull(this.w_AZCONBAN)+;
                  ","+cp_ToStrODBCNull(this.w_AZBANPRO)+;
                  ","+cp_ToStrODBC(this.w_AZDESBAN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'AZIENDA')
        i_extval=cp_InsertValVFPExtFlds(this,'AZIENDA')
        cp_CheckDeletedKey(i_cTable,0,'AZCODAZI',this.w_AZCODAZI)
        INSERT INTO (i_cTable);
              (AZCODAZI,AZCONBAN,AZBANPRO,AZDESBAN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_AZCODAZI;
                  ,this.w_AZCONBAN;
                  ,this.w_AZBANPRO;
                  ,this.w_AZDESBAN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.AZIENDA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.AZIENDA_IDX,i_nConn)
      *
      * update AZIENDA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'AZIENDA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " AZCONBAN="+cp_ToStrODBCNull(this.w_AZCONBAN)+;
             ",AZBANPRO="+cp_ToStrODBCNull(this.w_AZBANPRO)+;
             ",AZDESBAN="+cp_ToStrODBC(this.w_AZDESBAN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'AZIENDA')
        i_cWhere = cp_PKFox(i_cTable  ,'AZCODAZI',this.w_AZCODAZI  )
        UPDATE (i_cTable) SET;
              AZCONBAN=this.w_AZCONBAN;
             ,AZBANPRO=this.w_AZBANPRO;
             ,AZDESBAN=this.w_AZDESBAN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.AZIENDA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.AZIENDA_IDX,i_nConn)
      *
      * delete AZIENDA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'AZCODAZI',this.w_AZCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,17,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oAZDESBAN_1_13.visible=!this.oPgFrm.Page1.oPag.oAZDESBAN_1_13.mHide()
    this.oPgFrm.Page1.oPag.oABIBAN_1_16.visible=!this.oPgFrm.Page1.oPag.oABIBAN_1_16.mHide()
    this.oPgFrm.Page1.oPag.oCABBAN_1_17.visible=!this.oPgFrm.Page1.oPag.oCABBAN_1_17.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZCONBAN
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCONBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_AZCONBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB,BATIPCON,BACONCOR,BADTOBSO,BACONSBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_AZCONBAN))
          select BACODBAN,BADESCRI,BACODABI,BACODCAB,BATIPCON,BACONCOR,BADTOBSO,BACONSBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCONBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZCONBAN) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oAZCONBAN_1_2'),i_cWhere,'GSTE_ACB',"Conti banche",'GSCG_API.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB,BATIPCON,BACONCOR,BADTOBSO,BACONSBF";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BACODABI,BACODCAB,BATIPCON,BACONCOR,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCONBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB,BATIPCON,BACONCOR,BADTOBSO,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_AZCONBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_AZCONBAN)
            select BACODBAN,BADESCRI,BACODABI,BACODCAB,BATIPCON,BACONCOR,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCONBAN = NVL(_Link_.BACODBAN,space(15))
      this.w_AZDESBAN = NVL(_Link_.BADESCRI,space(50))
      this.w_ABIBAN = NVL(_Link_.BACODABI,space(5))
      this.w_CABBAN = NVL(_Link_.BACODCAB,space(5))
      this.w_BATIPCON = NVL(_Link_.BATIPCON,space(1))
      this.w_CONCOR = NVL(_Link_.BACONCOR,space(12))
      this.w_DATOBSO1 = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_BACONSBF = NVL(_Link_.BACONSBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZCONBAN = space(15)
      endif
      this.w_AZDESBAN = space(50)
      this.w_ABIBAN = space(5)
      this.w_CABBAN = space(5)
      this.w_BATIPCON = space(1)
      this.w_CONCOR = space(12)
      this.w_DATOBSO1 = ctod("  /  /  ")
      this.w_BACONSBF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_BATIPCON<>'S' AND (EMPTY(.w_DATOBSO1) OR .w_DATOBSO1>.w_OBTEST) AND .w_BACONSBF='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice banca inesistente, obsoleta, di compensazione o salvo buon fine")
        endif
        this.w_AZCONBAN = space(15)
        this.w_AZDESBAN = space(50)
        this.w_ABIBAN = space(5)
        this.w_CABBAN = space(5)
        this.w_BATIPCON = space(1)
        this.w_CONCOR = space(12)
        this.w_DATOBSO1 = ctod("  /  /  ")
        this.w_BACONSBF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCONBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.BACODBAN as BACODBAN102"+ ",link_1_2.BADESCRI as BADESCRI102"+ ",link_1_2.BACODABI as BACODABI102"+ ",link_1_2.BACODCAB as BACODCAB102"+ ",link_1_2.BATIPCON as BATIPCON102"+ ",link_1_2.BACONCOR as BACONCOR102"+ ",link_1_2.BADTOBSO as BADTOBSO102"+ ",link_1_2.BACONSBF as BACONSBF102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on AZIENDA.AZCONBAN=link_1_2.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and AZIENDA.AZCONBAN=link_1_2.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AZBANPRO
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZBANPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_AZBANPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_AZBANPRO))
          select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZBANPRO)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZBANPRO) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oAZBANPRO_1_6'),i_cWhere,'GSTE_ACB',"Conti banche",'GSCG_API.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZBANPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_AZBANPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_AZBANPRO)
            select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZBANPRO = NVL(_Link_.BACODBAN,space(15))
      this.w_BANPROABI = NVL(_Link_.BACODABI,space(5))
      this.w_BANPROCAB = NVL(_Link_.BACODCAB,space(5))
      this.w_BANPROSIA = NVL(_Link_.BACODSIA,space(5))
      this.w_BATIPCON_B = NVL(_Link_.BATIPCON,space(1))
      this.w_BADESCRI = NVL(_Link_.BADESCRI,space(35))
      this.w_DATOBSO2 = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_BACONSBF_1 = NVL(_Link_.BACONSBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZBANPRO = space(15)
      endif
      this.w_BANPROABI = space(5)
      this.w_BANPROCAB = space(5)
      this.w_BANPROSIA = space(5)
      this.w_BATIPCON_B = space(1)
      this.w_BADESCRI = space(35)
      this.w_DATOBSO2 = ctod("  /  /  ")
      this.w_BACONSBF_1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_BATIPCON_B<>'S' AND (EMPTY(.w_DATOBSO2) OR .w_DATOBSO2>.w_OBTEST) AND .w_BACONSBF_1='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice banca inesistente, obsoleta, di compensazione o salvo buon fine")
        endif
        this.w_AZBANPRO = space(15)
        this.w_BANPROABI = space(5)
        this.w_BANPROCAB = space(5)
        this.w_BANPROSIA = space(5)
        this.w_BATIPCON_B = space(1)
        this.w_BADESCRI = space(35)
        this.w_DATOBSO2 = ctod("  /  /  ")
        this.w_BACONSBF_1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZBANPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.BACODBAN as BACODBAN106"+ ",link_1_6.BACODABI as BACODABI106"+ ",link_1_6.BACODCAB as BACODCAB106"+ ",link_1_6.BACODSIA as BACODSIA106"+ ",link_1_6.BATIPCON as BATIPCON106"+ ",link_1_6.BADESCRI as BADESCRI106"+ ",link_1_6.BADTOBSO as BADTOBSO106"+ ",link_1_6.BACONSBF as BACONSBF106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on AZIENDA.AZBANPRO=link_1_6.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and AZIENDA.AZBANPRO=link_1_6.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAZCONBAN_1_2.value==this.w_AZCONBAN)
      this.oPgFrm.Page1.oPag.oAZCONBAN_1_2.value=this.w_AZCONBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oAZBANPRO_1_6.value==this.w_AZBANPRO)
      this.oPgFrm.Page1.oPag.oAZBANPRO_1_6.value=this.w_AZBANPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oAZDESBAN_1_13.value==this.w_AZDESBAN)
      this.oPgFrm.Page1.oPag.oAZDESBAN_1_13.value=this.w_AZDESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oABIBAN_1_16.value==this.w_ABIBAN)
      this.oPgFrm.Page1.oPag.oABIBAN_1_16.value=this.w_ABIBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oCABBAN_1_17.value==this.w_CABBAN)
      this.oPgFrm.Page1.oPag.oCABBAN_1_17.value=this.w_CABBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oCONCOR_1_18.value==this.w_CONCOR)
      this.oPgFrm.Page1.oPag.oCONCOR_1_18.value=this.w_CONCOR
    endif
    if not(this.oPgFrm.Page1.oPag.oBANPROABI_1_21.value==this.w_BANPROABI)
      this.oPgFrm.Page1.oPag.oBANPROABI_1_21.value=this.w_BANPROABI
    endif
    if not(this.oPgFrm.Page1.oPag.oBANPROCAB_1_22.value==this.w_BANPROCAB)
      this.oPgFrm.Page1.oPag.oBANPROCAB_1_22.value=this.w_BANPROCAB
    endif
    if not(this.oPgFrm.Page1.oPag.oBANPROSIA_1_23.value==this.w_BANPROSIA)
      this.oPgFrm.Page1.oPag.oBANPROSIA_1_23.value=this.w_BANPROSIA
    endif
    if not(this.oPgFrm.Page1.oPag.oBADESCRI_1_27.value==this.w_BADESCRI)
      this.oPgFrm.Page1.oPag.oBADESCRI_1_27.value=this.w_BADESCRI
    endif
    cp_SetControlsValueExtFlds(this,'AZIENDA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_BATIPCON<>'S' AND (EMPTY(.w_DATOBSO1) OR .w_DATOBSO1>.w_OBTEST) AND .w_BACONSBF='C')  and not(empty(.w_AZCONBAN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAZCONBAN_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice banca inesistente, obsoleta, di compensazione o salvo buon fine")
          case   not(.w_BATIPCON_B<>'S' AND (EMPTY(.w_DATOBSO2) OR .w_DATOBSO2>.w_OBTEST) AND .w_BACONSBF_1='C')  and not(empty(.w_AZBANPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAZBANPRO_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice banca inesistente, obsoleta, di compensazione o salvo buon fine")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_apfPag1 as StdContainer
  Width  = 594
  height = 168
  stdWidth  = 594
  stdheight = 168
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAZCONBAN_1_2 as StdField with uid="ZLRMNIVFBP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_AZCONBAN", cQueryName = "AZCONBAN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice banca inesistente, obsoleta, di compensazione o salvo buon fine",;
    ToolTipText = "Codice banca",;
    HelpContextID = 229206356,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=170, Top=40, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_AZCONBAN"

  func oAZCONBAN_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCONBAN_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCONBAN_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oAZCONBAN_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banche",'GSCG_API.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oAZCONBAN_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_AZCONBAN
     i_obj.ecpSave()
  endproc

  add object oAZBANPRO_1_6 as StdField with uid="CVAPNODCSK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_AZBANPRO", cQueryName = "AZBANPRO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice banca inesistente, obsoleta, di compensazione o salvo buon fine",;
    ToolTipText = "Banca proponente",;
    HelpContextID = 194730325,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=165, Top=102, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_AZBANPRO"

  func oAZBANPRO_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZBANPRO_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZBANPRO_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oAZBANPRO_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banche",'GSCG_API.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oAZBANPRO_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_AZBANPRO
     i_obj.ecpSave()
  endproc

  add object oAZDESBAN_1_13 as StdField with uid="NRCOLPTNWU",rtseq=10,rtrep=.f.,;
    cFormVar = "w_AZDESBAN", cQueryName = "AZDESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 233797972,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=304, Top=40, InputMask=replicate('X',50)

  func oAZDESBAN_1_13.mHide()
    with this.Parent.oContained
      return (g_TESO='S')
    endwith
  endfunc

  add object oABIBAN_1_16 as StdField with uid="VHBVRTXYVH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ABIBAN", cQueryName = "ABIBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 147632390,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=168, Top=71, InputMask=replicate('X',5)

  func oABIBAN_1_16.mHide()
    with this.Parent.oContained
      return (g_TESO='S')
    endwith
  endfunc

  add object oCABBAN_1_17 as StdField with uid="CBNSVDFSGT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CABBAN", cQueryName = "CABBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 147603494,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=304, Top=71, InputMask=replicate('X',5)

  func oCABBAN_1_17.mHide()
    with this.Parent.oContained
      return (g_TESO='S')
    endwith
  endfunc

  add object oCONCOR_1_18 as StdField with uid="HOFOWRYSPS",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CONCOR", cQueryName = "CONCOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    ToolTipText = "Conto corrente",;
    HelpContextID = 229510694,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=476, Top=71, InputMask=replicate('X',12)

  add object oBANPROABI_1_21 as StdField with uid="FQQQNXGIIW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_BANPROABI", cQueryName = "BANPROABI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice ABI banca proponente",;
    HelpContextID = 183174376,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=168, Top=130, InputMask=replicate('X',5)

  add object oBANPROCAB_1_22 as StdField with uid="UIMUAZCRVN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_BANPROCAB", cQueryName = "BANPROCAB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice CAB banca proponente",;
    HelpContextID = 183174263,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=304, Top=130, InputMask=replicate('X',5)

  add object oBANPROSIA_1_23 as StdField with uid="DQZRJNNZIZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_BANPROSIA", cQueryName = "BANPROSIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice SIA banca proponente",;
    HelpContextID = 183174255,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=471, Top=130, InputMask=replicate('X',5)

  add object oBADESCRI_1_27 as StdField with uid="VGHBPVAHAD",rtseq=17,rtrep=.f.,;
    cFormVar = "w_BADESCRI", cQueryName = "BADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 250568799,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=304, Top=102, InputMask=replicate('X',35)

  add object oStr_1_10 as StdString with uid="TCWAIJDHZN",Visible=.t., Left=11, Top=16,;
    Alignment=0, Width=207, Height=15,;
    Caption="Dati per la gestione del modello F24"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="QHZVIYRQAD",Visible=.t., Left=8, Top=42,;
    Alignment=1, Width=157, Height=18,;
    Caption="Conto corrente versamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="KWKJBRACQK",Visible=.t., Left=94, Top=71,;
    Alignment=1, Width=71, Height=15,;
    Caption="Codice ABI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="UFYMKANPEY",Visible=.t., Left=250, Top=71,;
    Alignment=1, Width=50, Height=15,;
    Caption="CAB:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="CHLUVQNXPP",Visible=.t., Left=356, Top=71,;
    Alignment=1, Width=117, Height=18,;
    Caption="Num.C/Corrente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="ZBRTWVBUCK",Visible=.t., Left=39, Top=103,;
    Alignment=1, Width=126, Height=18,;
    Caption="Banca proponente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="MNBNDRRUYS",Visible=.t., Left=93, Top=133,;
    Alignment=1, Width=72, Height=15,;
    Caption="Codice ABI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="DLBRTBFBSC",Visible=.t., Left=250, Top=133,;
    Alignment=1, Width=50, Height=15,;
    Caption="CAB:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="NFKBXGDDCZ",Visible=.t., Left=402, Top=133,;
    Alignment=1, Width=66, Height=18,;
    Caption="Codice SIA:"  ;
  , bGlobalFont=.t.

  add object oBox_1_11 as StdBox with uid="YAXYPOIOLN",left=5, top=30, width=577,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_apf','AZIENDA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AZCODAZI=AZIENDA.AZCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
