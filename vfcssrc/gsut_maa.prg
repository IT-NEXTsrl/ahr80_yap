* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_maa                                                        *
*              Attributi allegati                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-14                                                      *
* Last revis.: 2011-04-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsut_maa")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsut_maa")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsut_maa")
  return

* --- Class definition
define class tgsut_maa as StdPCForm
  Width  = 803
  Height = 355
  Top    = 10
  Left   = 10
  cComment = "Attributi allegati"
  cPrg = "gsut_maa"
  HelpContextID=137804951
  add object cnt as tcgsut_maa
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsut_maa as PCContext
  w_CPROWORD = 0
  w_IDCODATT = space(15)
  w_IDDESATT = space(40)
  w_IDTIPATT = space(1)
  w_IDVALATT = space(100)
  w_IDCHKOBB = space(1)
  w_IDTABKEY = space(15)
  w_IDVALDAT = space(8)
  w_IDVALNUM = 0
  w_IDSERIAL = space(20)
  proc Save(i_oFrom)
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_IDCODATT = i_oFrom.w_IDCODATT
    this.w_IDDESATT = i_oFrom.w_IDDESATT
    this.w_IDTIPATT = i_oFrom.w_IDTIPATT
    this.w_IDVALATT = i_oFrom.w_IDVALATT
    this.w_IDCHKOBB = i_oFrom.w_IDCHKOBB
    this.w_IDTABKEY = i_oFrom.w_IDTABKEY
    this.w_IDVALDAT = i_oFrom.w_IDVALDAT
    this.w_IDVALNUM = i_oFrom.w_IDVALNUM
    this.w_IDSERIAL = i_oFrom.w_IDSERIAL
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_IDCODATT = this.w_IDCODATT
    i_oTo.w_IDDESATT = this.w_IDDESATT
    i_oTo.w_IDTIPATT = this.w_IDTIPATT
    i_oTo.w_IDVALATT = this.w_IDVALATT
    i_oTo.w_IDCHKOBB = this.w_IDCHKOBB
    i_oTo.w_IDTABKEY = this.w_IDTABKEY
    i_oTo.w_IDVALDAT = this.w_IDVALDAT
    i_oTo.w_IDVALNUM = this.w_IDVALNUM
    i_oTo.w_IDSERIAL = this.w_IDSERIAL
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsut_maa as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 803
  Height = 355
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-04-05"
  HelpContextID=137804951
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PRODINDI_IDX = 0
  cFile = "PRODINDI"
  cKeySelect = "IDSERIAL"
  cKeyWhere  = "IDSERIAL=this.w_IDSERIAL"
  cKeyDetail  = "IDSERIAL=this.w_IDSERIAL"
  cKeyWhereODBC = '"IDSERIAL="+cp_ToStrODBC(this.w_IDSERIAL)';

  cKeyDetailWhereODBC = '"IDSERIAL="+cp_ToStrODBC(this.w_IDSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"PRODINDI.IDSERIAL="+cp_ToStrODBC(this.w_IDSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PRODINDI.CPROWORD '
  cPrg = "gsut_maa"
  cComment = "Attributi allegati"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 17
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CPROWORD = 0
  w_IDCODATT = space(15)
  w_IDDESATT = space(40)
  w_IDTIPATT = space(1)
  o_IDTIPATT = space(1)
  w_IDVALATT = space(100)
  o_IDVALATT = space(100)
  w_IDCHKOBB = space(1)
  w_IDTABKEY = space(15)
  w_IDVALDAT = ctod('  /  /  ')
  w_IDVALNUM = 0
  w_IDSERIAL = space(20)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_maaPag1","gsut_maa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PRODINDI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PRODINDI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PRODINDI_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsut_maa'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PRODINDI where IDSERIAL=KeySet.IDSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PRODINDI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRODINDI_IDX,2],this.bLoadRecFilter,this.PRODINDI_IDX,"gsut_maa")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PRODINDI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PRODINDI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PRODINDI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'IDSERIAL',this.w_IDSERIAL  )
      select * from (i_cTable) PRODINDI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_IDSERIAL = NVL(IDSERIAL,space(20))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PRODINDI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_IDCODATT = NVL(IDCODATT,space(15))
          .w_IDDESATT = NVL(IDDESATT,space(40))
          .w_IDTIPATT = NVL(IDTIPATT,space(1))
          .w_IDVALATT = NVL(IDVALATT,space(100))
          .w_IDCHKOBB = NVL(IDCHKOBB,space(1))
          .w_IDTABKEY = NVL(IDTABKEY,space(15))
          .w_IDVALDAT = NVL(cp_ToDate(IDVALDAT),ctod("  /  /  "))
          .w_IDVALNUM = NVL(IDVALNUM,0)
        .oPgFrm.Page1.oPag.oObj_2_10.Calculate()
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CPROWORD=10
      .w_IDCODATT=space(15)
      .w_IDDESATT=space(40)
      .w_IDTIPATT=space(1)
      .w_IDVALATT=space(100)
      .w_IDCHKOBB=space(1)
      .w_IDTABKEY=space(15)
      .w_IDVALDAT=ctod("  /  /  ")
      .w_IDVALNUM=0
      .w_IDSERIAL=space(20)
      if .cFunction<>"Filter"
        .DoRTCalc(1,5,.f.)
        .w_IDCHKOBB = 'N'
        .DoRTCalc(7,7,.f.)
        .w_IDVALDAT = IIF(.w_IDTIPATT='D',cp_CharToDate(.w_IDVALATT),cp_CharToDate('  -  -  '))
        .w_IDVALNUM = IIF(.w_IDTIPATT='N',val(.w_IDVALATT),0)
        .oPgFrm.Page1.oPag.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PRODINDI')
    this.DoRTCalc(10,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oObj_2_10.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PRODINDI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PRODINDI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IDSERIAL,"IDSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_IDCODATT C(15);
      ,t_IDDESATT C(40);
      ,t_IDTIPATT N(3);
      ,t_IDVALATT C(100);
      ,t_IDCHKOBB N(3);
      ,t_IDTABKEY C(15);
      ,CPROWNUM N(10);
      ,t_IDVALDAT D(8);
      ,t_IDVALNUM N(20,5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsut_maabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIDCODATT_2_2.controlsource=this.cTrsName+'.t_IDCODATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIDDESATT_2_3.controlsource=this.cTrsName+'.t_IDDESATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIDTIPATT_2_4.controlsource=this.cTrsName+'.t_IDTIPATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIDVALATT_2_5.controlsource=this.cTrsName+'.t_IDVALATT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIDCHKOBB_2_6.controlsource=this.cTrsName+'.t_IDCHKOBB'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIDTABKEY_2_7.controlsource=this.cTrsName+'.t_IDTABKEY'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(44)
    this.AddVLine(139)
    this.AddVLine(348)
    this.AddVLine(420)
    this.AddVLine(620)
    this.AddVLine(663)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PRODINDI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRODINDI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PRODINDI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRODINDI_IDX,2])
      *
      * insert into PRODINDI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PRODINDI')
        i_extval=cp_InsertValODBCExtFlds(this,'PRODINDI')
        i_cFldBody=" "+;
                  "(CPROWORD,IDCODATT,IDDESATT,IDTIPATT,IDVALATT"+;
                  ",IDCHKOBB,IDTABKEY,IDVALDAT,IDVALNUM,IDSERIAL,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_IDCODATT)+","+cp_ToStrODBC(this.w_IDDESATT)+","+cp_ToStrODBC(this.w_IDTIPATT)+","+cp_ToStrODBC(this.w_IDVALATT)+;
             ","+cp_ToStrODBC(this.w_IDCHKOBB)+","+cp_ToStrODBC(this.w_IDTABKEY)+","+cp_ToStrODBC(this.w_IDVALDAT)+","+cp_ToStrODBC(this.w_IDVALNUM)+","+cp_ToStrODBC(this.w_IDSERIAL)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PRODINDI')
        i_extval=cp_InsertValVFPExtFlds(this,'PRODINDI')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'IDSERIAL',this.w_IDSERIAL)
        INSERT INTO (i_cTable) (;
                   CPROWORD;
                  ,IDCODATT;
                  ,IDDESATT;
                  ,IDTIPATT;
                  ,IDVALATT;
                  ,IDCHKOBB;
                  ,IDTABKEY;
                  ,IDVALDAT;
                  ,IDVALNUM;
                  ,IDSERIAL;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CPROWORD;
                  ,this.w_IDCODATT;
                  ,this.w_IDDESATT;
                  ,this.w_IDTIPATT;
                  ,this.w_IDVALATT;
                  ,this.w_IDCHKOBB;
                  ,this.w_IDTABKEY;
                  ,this.w_IDVALDAT;
                  ,this.w_IDVALNUM;
                  ,this.w_IDSERIAL;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PRODINDI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRODINDI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_IDDESATT))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PRODINDI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PRODINDI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_IDDESATT))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PRODINDI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PRODINDI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",IDCODATT="+cp_ToStrODBC(this.w_IDCODATT)+;
                     ",IDDESATT="+cp_ToStrODBC(this.w_IDDESATT)+;
                     ",IDTIPATT="+cp_ToStrODBC(this.w_IDTIPATT)+;
                     ",IDVALATT="+cp_ToStrODBC(this.w_IDVALATT)+;
                     ",IDCHKOBB="+cp_ToStrODBC(this.w_IDCHKOBB)+;
                     ",IDTABKEY="+cp_ToStrODBC(this.w_IDTABKEY)+;
                     ",IDVALDAT="+cp_ToStrODBC(this.w_IDVALDAT)+;
                     ",IDVALNUM="+cp_ToStrODBC(this.w_IDVALNUM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PRODINDI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,IDCODATT=this.w_IDCODATT;
                     ,IDDESATT=this.w_IDDESATT;
                     ,IDTIPATT=this.w_IDTIPATT;
                     ,IDVALATT=this.w_IDVALATT;
                     ,IDCHKOBB=this.w_IDCHKOBB;
                     ,IDTABKEY=this.w_IDTABKEY;
                     ,IDVALDAT=this.w_IDVALDAT;
                     ,IDVALNUM=this.w_IDVALNUM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PRODINDI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PRODINDI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_IDDESATT))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PRODINDI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_IDDESATT))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PRODINDI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PRODINDI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_IDTIPATT<>.w_IDTIPATT.or. .o_IDVALATT<>.w_IDVALATT
          .w_IDVALDAT = IIF(.w_IDTIPATT='D',cp_CharToDate(.w_IDVALATT),cp_CharToDate('  -  -  '))
        endif
        if .o_IDTIPATT<>.w_IDTIPATT.or. .o_IDVALATT<>.w_IDVALATT
          .w_IDVALNUM = IIF(.w_IDTIPATT='N',val(.w_IDVALATT),0)
        endif
        .oPgFrm.Page1.oPag.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_IDVALDAT with this.w_IDVALDAT
      replace t_IDVALNUM with this.w_IDVALNUM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_10.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_10.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_2_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDCODATT_2_2.value==this.w_IDCODATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDCODATT_2_2.value=this.w_IDCODATT
      replace t_IDCODATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDCODATT_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDDESATT_2_3.value==this.w_IDDESATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDDESATT_2_3.value=this.w_IDDESATT
      replace t_IDDESATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDDESATT_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTIPATT_2_4.RadioValue()==this.w_IDTIPATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTIPATT_2_4.SetRadio()
      replace t_IDTIPATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTIPATT_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDVALATT_2_5.value==this.w_IDVALATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDVALATT_2_5.value=this.w_IDVALATT
      replace t_IDVALATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDVALATT_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDCHKOBB_2_6.RadioValue()==this.w_IDCHKOBB)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDCHKOBB_2_6.SetRadio()
      replace t_IDCHKOBB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDCHKOBB_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTABKEY_2_7.value==this.w_IDTABKEY)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTABKEY_2_7.value=this.w_IDTABKEY
      replace t_IDTABKEY with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTABKEY_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'PRODINDI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut_maa
      *Verifico l'univocitÓ
      this.oParentObject.NotifyEvent('UnivocitÓ')
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_IDDESATT))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IDTIPATT = this.w_IDTIPATT
    this.o_IDVALATT = this.w_IDVALATT
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_IDDESATT)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_IDCODATT=space(15)
      .w_IDDESATT=space(40)
      .w_IDTIPATT=space(1)
      .w_IDVALATT=space(100)
      .w_IDCHKOBB=space(1)
      .w_IDTABKEY=space(15)
      .w_IDVALDAT=ctod("  /  /  ")
      .w_IDVALNUM=0
      .DoRTCalc(1,5,.f.)
        .w_IDCHKOBB = 'N'
      .DoRTCalc(7,7,.f.)
        .w_IDVALDAT = IIF(.w_IDTIPATT='D',cp_CharToDate(.w_IDVALATT),cp_CharToDate('  -  -  '))
        .w_IDVALNUM = IIF(.w_IDTIPATT='N',val(.w_IDVALATT),0)
        .oPgFrm.Page1.oPag.oObj_2_10.Calculate()
    endwith
    this.DoRTCalc(10,10,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_IDCODATT = t_IDCODATT
    this.w_IDDESATT = t_IDDESATT
    this.w_IDTIPATT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTIPATT_2_4.RadioValue(.t.)
    this.w_IDVALATT = t_IDVALATT
    this.w_IDCHKOBB = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDCHKOBB_2_6.RadioValue(.t.)
    this.w_IDTABKEY = t_IDTABKEY
    this.w_IDVALDAT = t_IDVALDAT
    this.w_IDVALNUM = t_IDVALNUM
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_IDCODATT with this.w_IDCODATT
    replace t_IDDESATT with this.w_IDDESATT
    replace t_IDTIPATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDTIPATT_2_4.ToRadio()
    replace t_IDVALATT with this.w_IDVALATT
    replace t_IDCHKOBB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIDCHKOBB_2_6.ToRadio()
    replace t_IDTABKEY with this.w_IDTABKEY
    replace t_IDVALDAT with this.w_IDVALDAT
    replace t_IDVALNUM with this.w_IDVALNUM
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsut_maaPag1 as StdContainer
  Width  = 799
  height = 355
  stdWidth  = 799
  stdheight = 355
  resizeXpos=273
  resizeYpos=240
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=1, width=784,height=21,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Seq.",Field2="IDCODATT",Label2="Attributo",Field3="IDDESATT",Label3="Descrizione",Field4="IDTIPATT",Label4="Tipo",Field5="IDVALATT",Label5="Valore",Field6="IDCHKOBB",Label6="Obbl",Field7="IDTABKEY",Label7="Tabella",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 185460346

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=21,;
    width=782+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*17*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=22,width=781+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*17*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oObj_2_10 as cp_runprogram with uid="WOLELFHAMY",width=236,height=19,;
   left=96, top=393,;
    caption='GSUT_BBI',;
   bGlobalFont=.t.,;
    prg="GSUT_BBI('VALORE')",;
    cEvent = "w_IDVALATT Changed",;
    nPag=2;
    , HelpContextID = 260021073

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsut_maaBodyRow as CPBodyRowCnt
  Width=772
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="AAEUHRSJMZ",rtseq=1,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 50646678,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=38, Left=-2, Top=0

  add object oIDCODATT_2_2 as StdTrsField with uid="QBLBDFIPAZ",rtseq=2,rtrep=.t.,;
    cFormVar="w_IDCODATT",value=space(15),;
    HelpContextID = 37079590,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=93, Left=38, Top=0, InputMask=replicate('X',15)

  add object oIDDESATT_2_3 as StdTrsField with uid="LWQARWXNRI",rtseq=3,rtrep=.t.,;
    cFormVar="w_IDDESATT",value=space(40),;
    HelpContextID = 22002214,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=208, Left=133, Top=0, InputMask=replicate('X',40)

  add object oIDTIPATT_2_4 as StdTrsCombo with uid="GDYMOBKLYF",rtrep=.t.,;
    cFormVar="w_IDTIPATT", RowSource=""+"Numerico,"+"Carattere,"+"Data" , ;
    HelpContextID = 24820262,;
    Height=21, Width=71, Left=342, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oIDTIPATT_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IDTIPATT,&i_cF..t_IDTIPATT),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'C',;
    iif(xVal =3,'D',;
    space(1)))))
  endfunc
  func oIDTIPATT_2_4.GetRadio()
    this.Parent.oContained.w_IDTIPATT = this.RadioValue()
    return .t.
  endfunc

  func oIDTIPATT_2_4.ToRadio()
    this.Parent.oContained.w_IDTIPATT=trim(this.Parent.oContained.w_IDTIPATT)
    return(;
      iif(this.Parent.oContained.w_IDTIPATT=='N',1,;
      iif(this.Parent.oContained.w_IDTIPATT=='C',2,;
      iif(this.Parent.oContained.w_IDTIPATT=='D',3,;
      0))))
  endfunc

  func oIDTIPATT_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oIDVALATT_2_5 as StdTrsField with uid="ZCYKHKUHAJ",rtseq=5,rtrep=.t.,;
    cFormVar="w_IDVALATT",value=space(100),;
    HelpContextID = 29530662,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=197, Left=414, Top=0, InputMask=replicate('X',100), bHasZoom = .t. 

  proc oIDVALATT_2_5.mZoom
    do GSUT_KKA with this.Parent.oContained
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oIDCHKOBB_2_6 as StdTrsCombo with uid="DPTGBKUMXI",rtrep=.t.,;
    cFormVar="w_IDCHKOBB", RowSource=""+"Si,"+"No" , ;
    HelpContextID = 63752760,;
    Height=22, Width=38, Left=616, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , TABSTOP=.F.;
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oIDCHKOBB_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IDCHKOBB,&i_cF..t_IDCHKOBB),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    'N')))
  endfunc
  func oIDCHKOBB_2_6.GetRadio()
    this.Parent.oContained.w_IDCHKOBB = this.RadioValue()
    return .t.
  endfunc

  func oIDCHKOBB_2_6.ToRadio()
    this.Parent.oContained.w_IDCHKOBB=trim(this.Parent.oContained.w_IDCHKOBB)
    return(;
      iif(this.Parent.oContained.w_IDCHKOBB=='S',1,;
      iif(this.Parent.oContained.w_IDCHKOBB=='N',2,;
      0)))
  endfunc

  func oIDCHKOBB_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oIDTABKEY_2_7 as StdTrsField with uid="WSHTYPKKMY",rtseq=7,rtrep=.t.,;
    cFormVar="w_IDTABKEY",value=space(15),;
    HelpContextID = 127747551,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=108, Left=659, Top=0, InputMask=replicate('X',15)
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=16
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_maa','PRODINDI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".IDSERIAL=PRODINDI.IDSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
