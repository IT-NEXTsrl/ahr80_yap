* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bus                                                        *
*              Abbinamento partite in distint                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_193]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-01                                                      *
* Last revis.: 2016-11-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bus",oParentObject,m.pOper)
return(i_retval)

define class tgste_bus as StdBatch
  * --- Local variables
  pOper = space(5)
  w_OK = .f.
  w_PTNUMPAR = space(31)
  w_PTDATSCA = ctod("  /  /  ")
  w_APPO = 0
  w_PTNUMDOC = 0
  w_TOTDAR = 0
  w_PTALFDOC = space(10)
  w_TOTAVE = 0
  w_PTDATDOC = ctod("  /  /  ")
  w_SCCODICE = space(10)
  w_PT_SEGNO = space(1)
  w_SCTIPSCA = space(1)
  w_PTTOTIMP = 0
  w_PTCAOAPE = 0
  w_SCVALNAZ = space(3)
  w_SCDESCRI = space(50)
  w_PTDATAPE = ctod("  /  /  ")
  w_SCIMPSCA = 0
  w_PTIMPDOC = 0
  w_PTFLINDI = space(1)
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PTBANAPP = space(10)
  w_PTBANNOS = space(15)
  w_PTMODPAG = space(10)
  w_PTDESRIG = space(50)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_PTCODAGE = space(5)
  w_CPROWNUM = 0
  w_OSERIAL = space(10)
  w_OROWORD = 0
  w_OROWNUM = 0
  w_NUMPRO = 0
  w_PADRE = .NULL.
  w_PTDATREG = ctod("  /  /  ")
  w_COSERIAL = space(10)
  w_CONUMREG = 0
  w_CODATREG = ctod("  /  /  ")
  w_COCOMPET = space(4)
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_SERRIF = space(10)
  w_ORDRIF = 0
  w_NUMRIF = 0
  w_AZFLRAGG = space(1)
  w_CODAZI = space(5)
  w_LOOP = 0
  w_O_ZNDOINI = 0
  w_O_ZNDOFIN = 0
  w_O_ZADOINI = space(10)
  w_O_ZADOFIN = space(10)
  w_O_ZDDOINI = ctod("  /  /  ")
  w_O_ZDDOFIN = ctod("  /  /  ")
  w_O_ZBANNOS = space(0)
  w_O_ZBANAPP = space(10)
  w_O_ZREGINI = ctod("  /  /  ")
  w_O_ZREGFIN = ctod("  /  /  ")
  w_O_ZMODPAG = space(10)
  w_O_ZCODAGE = space(5)
  * --- WorkFile variables
  PAR_TITE_idx=0
  SCA_VARI_idx=0
  BAN_CONTI_idx=0
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Abbinamento Partite/Scadenze in Distinta (da GSTE_KUS)
    this.w_PADRE = This.oParentObject
    this.w_CODAZI = i_CODAZI
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZFLRAGG"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZFLRAGG;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AZFLRAGG = NVL(cp_ToDate(_read_.AZFLRAGG),cp_NullValue(_read_.AZFLRAGG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.pOper="CALC"
        this.w_O_ZNDOINI = this.oParentObject.w_ZNDOINI
        this.w_O_ZNDOFIN = this.oParentObject.w_ZNDOFIN
        this.w_O_ZADOINI = this.oParentObject.w_ZADOINI
        this.w_O_ZADOFIN = this.oParentObject.w_ZADOFIN
        this.w_O_ZDDOINI = this.oParentObject.w_ZDDOINI
        this.w_O_ZDDOFIN = this.oParentObject.w_ZDDOFIN
        this.w_O_ZBANNOS = this.oParentObject.w_ZBANNOS
        this.w_O_ZBANAPP = this.oParentObject.w_ZBANAPP
        this.w_O_ZREGINI = this.oParentObject.w_ZREGINI
        this.w_O_ZREGFIN = this.oParentObject.w_ZREGFIN
        this.w_O_ZMODPAG = this.oParentObject.w_ZMODPAG
        this.w_O_ZCODAGE = this.oParentObject.w_ZCODAGE
        this.oParentObject.w_ZNDOINI = 0
        this.oParentObject.w_ZNDOFIN = 0
        this.oParentObject.w_ZADOINI = ""
        this.oParentObject.w_ZADOFIN = ""
        this.oParentObject.w_ZDDOINI = cp_CharToDate("  /  /    ")
        this.oParentObject.w_ZDDOFIN = cp_CharToDate("  /  /    ")
        this.oParentObject.w_ZBANNOS = ""
        this.oParentObject.w_ZBANAPP = ""
        this.oParentObject.w_ZREGINI = cp_CharToDate("  /  /    ")
        this.oParentObject.w_ZREGFIN = cp_CharToDate("  /  /    ")
        this.oParentObject.w_ZMODPAG = Space(10)
        this.oParentObject.w_ZCODAGE = Space(5)
        * --- Lancio la query per popolare lo zoom
        this.w_PADRE.NotifyEvent("Accorpa")     
        this.w_LOOP = 1
        do while this.w_LOOP<=this.oParentObject.w_CALCZOOM.Grd.ColumnCount
          if NOT ("XCHK" $ UPPER( this.oParentObject.w_CALCZOOM.grd.Columns[ this.w_LOOP ].ControlSource) OR "TOTIMP" $ UPPER(this.oParentObject.w_CALCZOOM.grd.Columns[ this.w_LOOP ].ControlSource)) 
            this.oParentObject.w_CALCZOOM.grd.Columns[ This.w_LOOP ].Enabled = .F.
          else
            this.oParentObject.w_CALCZOOM.grd.Columns[ This.w_LOOP ].Enabled = .T.
            this.oParentObject.w_CALCZOOM.grd.Columns[ This.w_LOOP ].Format = "KR"
          endif
          this.w_LOOP = this.w_LOOP + 1
        enddo
        this.oParentObject.w_ZNDOINI = this.w_O_ZNDOINI
        this.oParentObject.w_ZNDOFIN = this.w_O_ZNDOFIN
        this.oParentObject.w_ZADOINI = this.w_O_ZADOINI
        this.oParentObject.w_ZADOFIN = this.w_O_ZADOFIN
        this.oParentObject.w_ZDDOINI = this.w_O_ZDDOINI
        this.oParentObject.w_ZDDOFIN = this.w_O_ZDDOFIN
        this.oParentObject.w_ZBANNOS = this.w_O_ZBANNOS
        this.oParentObject.w_ZBANAPP = this.w_O_ZBANAPP
        this.oParentObject.w_ZREGINI = this.w_O_ZREGINI
        this.oParentObject.w_ZREGFIN = this.w_O_ZREGFIN
        this.oParentObject.w_ZMODPAG = this.w_O_ZMODPAG
        this.oParentObject.w_ZCODAGE = this.w_O_ZCODAGE
        GSTE_BCP(this,"A",this.oParentObject.w_CALCZOOM.cCursor," ")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Applico i filtri sul risultato:
        *     - Intervallo date di registrazione
        *     - Intervallo numero documento
        *     - intervallo alfa
        *     - intervallo date documento
        *     - Banca d'appoggio
        *     - Nostra banca
        *     - Pagamento
        *     - Agente
         
 DELETE FROM (this.oParentObject.w_CALCZOOM.cCursor) WHERE Not ; 
 (( Empty( this.oParentObject.w_ZREGINI ) Or CP_TODATE(DATREG) >= this.oParentObject.w_ZREGINI ) And ( Empty( this.oParentObject.w_ZREGFIN ) Or CP_TODATE(DATREG) <= this.oParentObject.w_ZREGFIN ) ; 
 And ( Empty( this.oParentObject.w_ZNDOINI ) Or NUMDOC>= this.oParentObject.w_ZNDOINI ) And ( Empty( this.oParentObject.w_ZNDOFIN ) Or NUMDOC<= this.oParentObject.w_ZNDOFIN ); 
 And ( Empty( this.oParentObject.w_ZADOINI ) Or ALFDOC>= this.oParentObject.w_ZADOINI ) And ( Empty( this.oParentObject.w_ZADOFIN ) Or ALFDOC<= this.oParentObject.w_ZADOFIN ); 
 And ( Empty( this.oParentObject.w_ZDDOINI ) Or CP_TODATE(DATDOC) >= this.oParentObject.w_ZDDOINI ) And ( Empty( this.oParentObject.w_ZDDOFIN ) Or CP_TODATE(DATDOC) <= this.oParentObject.w_ZDDOFIN ) ; 
 And ( Empty( this.oParentObject.w_ZBANAPP ) Or Nvl(BANAPP,SPACE(10)) = this.oParentObject.w_ZBANAPP ) ; 
 And ( Empty( this.oParentObject.w_ZBANNOS ) Or Nvl(BANNOS,SPACE(15)) = this.oParentObject.w_ZBANNOS ) And ; 
 ( NVL(MODPAG,SPACE(5)) = this.oParentObject.w_ZMODPAG Or Empty( this.oParentObject.w_ZMODPAG )) and ( NVL(CODAGE,SPACE(5)) = this.oParentObject.w_ZCODAGE Or Empty( this.oParentObject.w_ZCODAGE ) ) )
        * --- Metto in positivo gli importi negativi...
        UPDATE (this.oParentObject.w_CALCZOOM.cCursor) SET TOTALE1=ABS(TOTIMP) 
         
 Select (this.oParentObject.w_CALCZOOM.cCursor) 
 Go Top
        this.oParentObject.w_CALCZOOM.Refresh()     
      case this.pOper="SELE"
        * --- Seleziona/Deseleziona Tutto
        if this.oParentObject.w_SELEZI="S"
           
 Select ( this.oParentObject.w_CALCZOOM.cCursor ) 
 Go Top 
 Scan for XCHK=0 
 REPLACE XCHK WITH 1 
 this.w_PADRE.Notifyevent("CALCZOOM row checked") 
 endscan
          this.oParentObject.w_FLSELE = 1
        else
          UPDATE ( this.oParentObject.w_CALCZOOM.cCursor ) SET XCHK = 0
          this.oParentObject.w_FLSELE = 0
        endif
      case this.pOper="AGGIO"
        if this.w_AZFLRAGG="S"
          NC = this.oParentObject.w_CALCZOOM.cCursor
          * --- Controlla banca Appoggio/Nostra Banca
          this.w_OK = .T.
          * --- seleziono il cursore
          SELECT &NC
          GO TOP
          * --- Cicla sui record Selezionati
          SCAN FOR XCHK<>0 AND (NOT EMPTY(NVL(PNAGG_01,"")) OR NOT EMPTY(NVL(PNAGG_02,"")) OR NOT EMPTY(NVL(PNAGG_03,"")) OR NOT EMPTY(NVL(PNAGG_04,"")) OR NOT EMPTY(NVL(PNAGG_05,CP_CHARTODATE("  -  -  "))) OR NOT EMPTY(NVL(PNAGG_05,CP_CHARTODATE("  -  -  "))) OR NOT EMPTY(NVL(PNNUMFAT,"")))
          * --- METTO A false  IL CHECK
          REPLACE XCHK WITH 0
          ah_ErrorMsg("La partita: %1 del %2%0ha almeno un dato aggiuntivo valorizzato",,"", ALLTRIM(NVL(NUMPAR,"")), DTOC(CP_TODATE(DATSCA)) )
          this.w_OK = .F.
          if this.w_OK=.F.
            * --- ALMENO UNA PARTITA NON HA LA BANCA DI APPOGGIO
            if not ah_YesNo("Assegno comunque le altre partite alla distinta?")
              * --- esco
              i_retcode = 'stop'
              return
            endif
          endif
          ENDSCAN
        endif
        * --- Abbina Partite
        * --- Raggruppa Scadenze
        if EMPTY(this.oParentObject.w_NMODPAG)
          ah_ErrorMsg("Selezionare un tipo pagamento",,"")
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.oParentObject.w_ZCODCON)
          ah_ErrorMsg("Selezionare un intestatario",,"")
          i_retcode = 'stop'
          return
        endif
        this.w_APPO = 0
        this.w_TOTDAR = 0
        this.w_TOTAVE = 0
         
 Select (this.oParentObject.w_CALCZOOM.cCursor) 
 Go Top
        * --- Calcolo il totale dare/avere delle scadenze selezionate
        SCAN FOR XCHK<>0 AND NOT EMPTY(NVL(PTSERIAL," ")) AND NVL(PTROWORD,0)<>0 AND NVL(CPROWNUM,0)>0
        this.w_APPO = this.w_APPO + 1
        this.w_TOTDAR = this.w_TOTDAR + IIF(SEGNO="D", ABS(TOTIMP), 0)
        this.w_TOTAVE = this.w_TOTAVE + IIF(SEGNO="A", ABS(TOTIMP), 0)
        ENDSCAN
        if this.w_APPO<2
          ah_ErrorMsg("Selezionare almeno 2 scadenze da raggruppare",,"")
          i_retcode = 'stop'
          return
        endif
         
 CREATE CURSOR SOLL (COSERIAL C(10),PTCODCON C(15),PTDATSCA D(8),PTNUMPAR C(31),CONUMREG N(6),; 
 COCOMPET C(4),CODATREG D(10), PTCODVAL C(3),PTMODPAG C(10))
        this.w_OK = .F.
        this.w_SCTIPSCA = IIF(this.w_TOTAVE>this.w_TOTDAR, "F", "C")
        this.w_SCVALNAZ = g_PERVAL
        this.w_SCIMPSCA = ABS(cp_ROUND(this.w_TOTDAR-this.w_TOTAVE,6))
        this.w_SCDESCRI = LEFT(this.oParentObject.w_RIFRAG, 35)
        * --- Try
        local bErr_04D41400
        bErr_04D41400=bTrsErr
        this.Try_04D41400()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Errore durante l'aggiornamento; operazione abbandonata",,"")
          i_retcode = 'stop'
          return
        endif
        bTrsErr=bTrsErr or bErr_04D41400
        * --- End
        if RECCOUNT("SOLL")<>0 AND g_SOLL="S"
          if ah_YesNo("Esistono partite associate a solleciti che verranno considerati chiusi nel successivo aggiornamento status%0Si desidera stampare il dettaglio solleciti?")
             
 Select * from SOLL into cursor __TMP__ 
 CP_CHPRN("..\SOLL\EXE\QUERY\GSSO2BIK.FRX"," ",This)
          endif
        endif
        if USED("SOLL")
           
 Select SOLL 
 Use
        endif
        if this.w_OK
          * --- Rieseguo la Query in GSTE_KMS
          this.w_PADRE.oParentObject.NotifyEvent("Riesegui")     
          * --- Chiudo la Maschera
          this.w_PADRE.EcpQuit()     
        else
          ah_ErrorMsg("Non ci sono scadenze da raggruppare",,"")
        endif
    endcase
  endproc
  proc Try_04D41400()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_SCCODICE = SPACE(10)
    i_Conn=i_TableProp[this.SCA_VARI_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SESCA", "i_codazi,w_SCCODICE")
    * --- Insert into SCA_VARI
    i_nConn=i_TableProp[this.SCA_VARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SCA_VARI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SCA_VARI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODSEC"+",SCDESCRI"+",SCTIPSCA"+",SCTIPCLF"+",SCCODCLF"+",SCNUMDOC"+",SCALFDOC"+",SCDATDOC"+",SCVALNAZ"+",SCCODVAL"+",SCCAOVAL"+",SCIMPSCA"+",SCFLIMPG"+",SCCODPAG"+",SCDATVAL"+",SCCODAGE"+",SCDATREG"+",SCFLVABD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SCCODICE),'SCA_VARI','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(-1),'SCA_VARI','SCCODSEC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SCDESCRI),'SCA_VARI','SCDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SCTIPSCA),'SCA_VARI','SCTIPSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ZTIPCON),'SCA_VARI','SCTIPCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ZCODCON),'SCA_VARI','SCCODCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NNUMDOC),'SCA_VARI','SCNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NALFDOC),'SCA_VARI','SCALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NDATDOC),'SCA_VARI','SCDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SCVALNAZ),'SCA_VARI','SCVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ZCODVAL),'SCA_VARI','SCCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CAOVAL),'SCA_VARI','SCCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(0),'SCA_VARI','SCIMPSCA');
      +","+cp_NullLink(cp_ToStrODBC("G"),'SCA_VARI','SCFLIMPG');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'SCA_VARI','SCCODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NDATDOC),'SCA_VARI','SCDATVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NCODAGE),'SCA_VARI','SCCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(i_datsys),'SCA_VARI','SCDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NFLVABD),'SCA_VARI','SCFLVABD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_SCCODICE,'SCCODSEC',-1,'SCDESCRI',this.w_SCDESCRI,'SCTIPSCA',this.w_SCTIPSCA,'SCTIPCLF',this.oParentObject.w_ZTIPCON,'SCCODCLF',this.oParentObject.w_ZCODCON,'SCNUMDOC',this.oParentObject.w_NNUMDOC,'SCALFDOC',this.oParentObject.w_NALFDOC,'SCDATDOC',this.oParentObject.w_NDATDOC,'SCVALNAZ',this.w_SCVALNAZ,'SCCODVAL',this.oParentObject.w_ZCODVAL,'SCCAOVAL',this.oParentObject.w_CAOVAL)
      insert into (i_cTable) (SCCODICE,SCCODSEC,SCDESCRI,SCTIPSCA,SCTIPCLF,SCCODCLF,SCNUMDOC,SCALFDOC,SCDATDOC,SCVALNAZ,SCCODVAL,SCCAOVAL,SCIMPSCA,SCFLIMPG,SCCODPAG,SCDATVAL,SCCODAGE,SCDATREG,SCFLVABD &i_ccchkf. );
         values (;
           this.w_SCCODICE;
           ,-1;
           ,this.w_SCDESCRI;
           ,this.w_SCTIPSCA;
           ,this.oParentObject.w_ZTIPCON;
           ,this.oParentObject.w_ZCODCON;
           ,this.oParentObject.w_NNUMDOC;
           ,this.oParentObject.w_NALFDOC;
           ,this.oParentObject.w_NDATDOC;
           ,this.w_SCVALNAZ;
           ,this.oParentObject.w_ZCODVAL;
           ,this.oParentObject.w_CAOVAL;
           ,0;
           ,"G";
           ,SPACE(5);
           ,this.oParentObject.w_NDATDOC;
           ,this.oParentObject.w_NCODAGE;
           ,i_datsys;
           ,this.oParentObject.w_NFLVABD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Cicla sui record Selezionati
    this.w_PTSERIAL = this.w_SCCODICE
    this.w_PTROWORD = -1
    this.w_CPROWNUM = 0
     
 Select (this.oParentObject.w_CALCZOOM.cCursor) 
 Go Top
    SCAN FOR XCHK<>0 AND NOT EMPTY(NVL(PTSERIAL," ")) AND NVL(PTROWORD,0)<>0 AND NVL(CPROWNUM,0)>0
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    ah_Msg("Aggiornamento scadenza del: %1",.T.,.F.,.F., DTOC(this.w_PTDATSCA) ) 
    this.w_OSERIAL = PTSERIAL
    this.w_OROWORD = PTROWORD
    this.w_OROWNUM = CPROWNUM
    this.w_PTNUMPAR = NVL(NUMPAR, SPACE(31))
    this.w_PTDATSCA = CP_TODATE(DATSCA)
    this.w_PTNUMDOC = NVL(NUMDOC, 0)
    this.w_PTALFDOC = NVL(ALFDOC, Space(10))
    this.w_PTDATDOC = CP_TODATE(DATDOC)
    this.w_PT_SEGNO = IIF(NVL(IMPDAR,0)<>0, "A", "D")
    if NVL(TOTIMP,0)>NVL(TOTALE1,0)
      this.w_PTTOTIMP = ABS(NVL(TOTALE1, 0))
    else
      this.w_PTTOTIMP = ABS(NVL(TOTIMP, 0))
    endif
    this.w_PTCAOAPE = NVL(CAOAPE, 0)
    this.w_PTDATAPE = CP_TODATE(DATAPE)
    this.w_PTIMPDOC = NVL(IMPDOC, 0)
    this.w_PTFLINDI = IIF(EMPTY(NVL(PTFLINDI, 0)), " ", "S")
    this.w_PTTIPCON = TIPCON
    this.w_PTCODCON = CODCON
    this.w_PTBANAPP = NVL(BANAPP, SPACE(10))
    this.w_PTBANNOS = NVL(BANNOS, SPACE(15))
    this.w_PTMODPAG = NVL(MODPAG, SPACE(10))
    this.w_PTDESRIG = NVL(DESRIG, SPACE(10))
    this.w_PTCODAGE = NVL(CODAGE,SPACE(5))
    this.w_PTDATREG = CP_TODATE(PTDATREG)
    this.w_PTSERRIF = Nvl(PTSERRIF,Space(10))
    this.w_PTORDRIF = Nvl(PTORDRIF,0)
    this.w_PTNUMRIF = Nvl(PTNUMRIF,0)
    this.w_SERRIF = IIF(Not Empty(this.w_PTSERRIF),this.w_PTSERRIF,this.w_OSERIAL)
    this.w_ORDRIF = IIF(Not Empty(this.w_PTORDRIF),this.w_PTORDRIF,this.w_OROWORD)
    this.w_NUMRIF = IIF(Not Empty(this.w_PTNUMRIF),this.w_PTNUMRIF,this.w_OROWNUM)
    * --- Read from BAN_CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.BAN_CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BAN_CONTI_idx,2],.t.,this.BAN_CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCCONCOR"+;
        " from "+i_cTable+" BAN_CONTI where ";
            +"CCTIPCON = "+cp_ToStrODBC(this.w_PTTIPCON);
            +" and CCCODCON = "+cp_ToStrODBC(this.w_PTCODCON);
            +" and CCCODBAN = "+cp_ToStrODBC(this.w_PTBANAPP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCCONCOR;
        from (i_cTable) where;
            CCTIPCON = this.w_PTTIPCON;
            and CCCODCON = this.w_PTCODCON;
            and CCCODBAN = this.w_PTBANAPP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_PTNUMCOR = NVL(cp_ToDate(_read_.CCCONCOR),cp_NullValue(_read_.CCCONCOR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if "Acconto" $ this.w_PTNUMPAR AND NVL(FLCRSA, " ")="A"
      * --- Riassegna Acconto da Accorpare
      * --- Un acconto selezionato dall'utente assume come numero partita
      *     il numero registrazione seguito immediatamente da una 'A'.
      *     Una 'A' alla fine per evitare di creare un numero partita potenzialmente
      *     utilizzabile da una partita normale.
      *     Acconto tolto dal numero partita per evitare che la stessa sia riconsiderata
      *     nelle varie elaborazione relative agli acconti (Like '%acconto%' � il filtro
      *     utilizzato)
      this.w_PTNUMPAR = CANUMPAR("A", g_CODESE, this.oParentObject.w_NNUMDOC)
      * --- Write into PAR_TITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PTNUMPAR ="+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
        +",PTCODAGE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NCODAGE),'PAR_TITE','PTCODAGE');
            +i_ccchkf ;
        +" where ";
            +"PTSERIAL = "+cp_ToStrODBC(this.w_OSERIAL);
            +" and PTROWORD = "+cp_ToStrODBC(this.w_OROWORD);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_OROWNUM);
               )
      else
        update (i_cTable) set;
            PTNUMPAR = this.w_PTNUMPAR;
            ,PTCODAGE = this.oParentObject.w_NCODAGE;
            &i_ccchkf. ;
         where;
            PTSERIAL = this.w_OSERIAL;
            and PTROWORD = this.w_OROWORD;
            and CPROWNUM = this.w_OROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if g_SOLL="S" and Not Empty(this.w_OSERIAL)
      * --- Select from GSSO1BIK
      do vq_exec with 'GSSO1BIK',this,'_Curs_GSSO1BIK','',.f.,.t.
      if used('_Curs_GSSO1BIK')
        select _Curs_GSSO1BIK
        locate for 1=1
        do while not(eof())
        this.w_COSERIAL =  _Curs_GSSO1BIK.COSERIAL
        this.w_CONUMREG = NVL(_Curs_GSSO1BIK.CONUMREG,0)
        this.w_CODATREG = CP_todate(_Curs_GSSO1BIK.CODATREG)
        this.w_COCOMPET = Nvl(_Curs_GSSO1BIK.COCOMPET,Space(4))
         
 Select SOLL 
 APPEND BLANK 
 REPLACE SOLL.COSERIAL WITH this.w_COSERIAL 
 REPLACE SOLL.PTCODCON WITH this.w_PTCODCON 
 REPLACE SOLL.PTDATSCA WITH this.w_PTDATSCA 
 REPLACE SOLL.PTNUMPAR WITH this.w_PTNUMPAR 
 REPLACE SOLL.CONUMREG WITH this.w_CONUMREG 
 REPLACE SOLL.COCOMPET WITH this.w_COCOMPET 
 REPLACE SOLL.CODATREG WITH this.w_CODATREG 
 REPLACE SOLL.PTCODVAL WITH this.oParentObject.w_ZCODVAL 
 REPLACE SOLL.PTMODPAG WITH this.w_PTMODPAG
          select _Curs_GSSO1BIK
          continue
        enddo
        use
      endif
    endif
    * --- Insert into PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTFLIMPE"+",PTFLINDI"+",PTBANNOS"+",PTNUMDIS"+",PTFLCRSA"+",PTFLRAGG"+",PTIMPDOC"+",PTDESRIG"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTCODAGE"+",PTFLVABD"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PTSERIAL),'PAR_TITE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'PAR_TITE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTIPCON),'PAR_TITE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODCON),'PAR_TITE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ZCODVAL),'PAR_TITE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CAOVAL),'PAR_TITE','PTCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOAPE),'PAR_TITE','PTCAOAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDOC),'PAR_TITE','PTNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTALFDOC),'PAR_TITE','PTALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATDOC),'PAR_TITE','PTDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLINDI),'PAR_TITE','PTFLINDI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTNUMDIS');
      +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLCRSA');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSERIAL),'PAR_TITE','PTSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OROWORD),'PAR_TITE','PTORDRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OROWNUM),'PAR_TITE','PTNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NFLVABD),'PAR_TITE','PTFLVABD');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PAR_TITE','PTDATREG');
      +","+cp_NullLink(cp_ToStrODBC(w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PTSERIAL,'PTROWORD',this.w_PTROWORD,'CPROWNUM',this.w_CPROWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PTTIPCON',this.w_PTTIPCON,'PTCODCON',this.w_PTCODCON,'PT_SEGNO',this.w_PT_SEGNO,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.oParentObject.w_ZCODVAL,'PTCAOVAL',this.oParentObject.w_CAOVAL,'PTCAOAPE',this.w_PTCAOAPE)
      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTFLIMPE,PTFLINDI,PTBANNOS,PTNUMDIS,PTFLCRSA,PTFLRAGG,PTIMPDOC,PTDESRIG,PTSERRIF,PTORDRIF,PTNUMRIF,PTCODAGE,PTFLVABD,PTDATREG,PTNUMCOR &i_ccchkf. );
         values (;
           this.w_PTSERIAL;
           ,this.w_PTROWORD;
           ,this.w_CPROWNUM;
           ,this.w_PTNUMPAR;
           ,this.w_PTDATSCA;
           ,this.w_PTTIPCON;
           ,this.w_PTCODCON;
           ,this.w_PT_SEGNO;
           ,this.w_PTTOTIMP;
           ,this.oParentObject.w_ZCODVAL;
           ,this.oParentObject.w_CAOVAL;
           ,this.w_PTCAOAPE;
           ,this.w_PTDATAPE;
           ,this.w_PTNUMDOC;
           ,this.w_PTALFDOC;
           ,this.w_PTDATDOC;
           ,this.w_PTMODPAG;
           ," ";
           ,this.w_PTBANAPP;
           ,"  ";
           ,this.w_PTFLINDI;
           ,this.w_PTBANNOS;
           ,SPACE(10);
           ,"S";
           ," ";
           ,this.w_PTIMPDOC;
           ,this.w_PTDESRIG;
           ,this.w_OSERIAL;
           ,this.w_OROWORD;
           ,this.w_OROWNUM;
           ,this.w_PTCODAGE;
           ,this.oParentObject.w_NFLVABD;
           ,i_DATSYS;
           ,w_PTNUMCOR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    Select (this.oParentObject.w_CALCZOOM.cCursor)
    this.w_OK = .T.
    ENDSCAN
    * --- Nuova Partita da Abbinare
    if this.w_OK AND this.w_SCIMPSCA<>0
      this.w_CPROWNUM = this.w_CPROWNUM + 1
      this.w_PT_SEGNO = IIF(this.w_TOTDAR-this.w_TOTAVE>0, "D", "A")
      this.w_NUMPRO = CERCPRO(i_datsys,this.oParentObject.w_NEWSCA,this.oParentObject.w_ZCODCON,this.oParentObject.w_ZTIPCON,this.oParentObject.w_ZCODVAL,this.oParentObject.w_NEWPAR) +1
      * --- Insert into PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PTFLIMPE"+",PTFLSOSP"+",PTFLINDI"+",PTFLCRSA"+",PTFLRAGG"+",PTNUMDIS"+",PTCAOVAL"+",PTCAOAPE"+",CPROWNUM"+",PTALFDOC"+",PTBANAPP"+",PTBANNOS"+",PTDATAPE"+",PTDATDOC"+",PTNUMPAR"+",PTDATSCA"+",PTMODPAG"+",PTNUMDOC"+",PT_SEGNO"+",PTROWORD"+",PTSERIAL"+",PTDESRIG"+",PTTOTIMP"+",PTIMPDOC"+",PTCODCON"+",PTCODVAL"+",PTTIPCON"+",PTNUMPRO"+",PTCODAGE"+",PTFLVABD"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLSOSP');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
        +","+cp_NullLink(cp_ToStrODBC("C"),'PAR_TITE','PTFLCRSA');
        +","+cp_NullLink(cp_ToStrODBC("S"),'PAR_TITE','PTFLRAGG');
        +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTNUMDIS');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CAOVAL),'PAR_TITE','PTCAOVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CAOVAL),'PAR_TITE','PTCAOAPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NALFDOC),'PAR_TITE','PTALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NBANAPP),'PAR_TITE','PTBANAPP');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NBANNOS),'PAR_TITE','PTBANNOS');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NDATDOC),'PAR_TITE','PTDATAPE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NDATDOC),'PAR_TITE','PTDATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWPAR),'PAR_TITE','PTNUMPAR');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NEWSCA),'PAR_TITE','PTDATSCA');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NMODPAG),'PAR_TITE','PTMODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NNUMDOC),'PAR_TITE','PTNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'PAR_TITE','PTROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTSERIAL),'PAR_TITE','PTSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RIFRAG),'PAR_TITE','PTDESRIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCIMPSCA),'PAR_TITE','PTTOTIMP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SCIMPSCA),'PAR_TITE','PTIMPDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ZCODCON),'PAR_TITE','PTCODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ZCODVAL),'PAR_TITE','PTCODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ZTIPCON),'PAR_TITE','PTTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMPRO),'PAR_TITE','PTNUMPRO');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NCODAGE),'PAR_TITE','PTCODAGE');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NFLVABD),'PAR_TITE','PTFLVABD');
        +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'PAR_TITE','PTDATREG');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NNUMCOR),'PAR_TITE','PTNUMCOR');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PTFLIMPE',"  ",'PTFLSOSP'," ",'PTFLINDI'," ",'PTFLCRSA',"C",'PTFLRAGG',"S",'PTNUMDIS',SPACE(10),'PTCAOVAL',this.oParentObject.w_CAOVAL,'PTCAOAPE',this.oParentObject.w_CAOVAL,'CPROWNUM',this.w_CPROWNUM,'PTALFDOC',this.oParentObject.w_NALFDOC,'PTBANAPP',this.oParentObject.w_NBANAPP,'PTBANNOS',this.oParentObject.w_NBANNOS)
        insert into (i_cTable) (PTFLIMPE,PTFLSOSP,PTFLINDI,PTFLCRSA,PTFLRAGG,PTNUMDIS,PTCAOVAL,PTCAOAPE,CPROWNUM,PTALFDOC,PTBANAPP,PTBANNOS,PTDATAPE,PTDATDOC,PTNUMPAR,PTDATSCA,PTMODPAG,PTNUMDOC,PT_SEGNO,PTROWORD,PTSERIAL,PTDESRIG,PTTOTIMP,PTIMPDOC,PTCODCON,PTCODVAL,PTTIPCON,PTNUMPRO,PTCODAGE,PTFLVABD,PTDATREG,PTNUMCOR &i_ccchkf. );
           values (;
             "  ";
             ," ";
             ," ";
             ,"C";
             ,"S";
             ,SPACE(10);
             ,this.oParentObject.w_CAOVAL;
             ,this.oParentObject.w_CAOVAL;
             ,this.w_CPROWNUM;
             ,this.oParentObject.w_NALFDOC;
             ,this.oParentObject.w_NBANAPP;
             ,this.oParentObject.w_NBANNOS;
             ,this.oParentObject.w_NDATDOC;
             ,this.oParentObject.w_NDATDOC;
             ,this.oParentObject.w_NEWPAR;
             ,this.oParentObject.w_NEWSCA;
             ,this.oParentObject.w_NMODPAG;
             ,this.oParentObject.w_NNUMDOC;
             ,this.w_PT_SEGNO;
             ,this.w_PTROWORD;
             ,this.w_PTSERIAL;
             ,this.oParentObject.w_RIFRAG;
             ,this.w_SCIMPSCA;
             ,this.w_SCIMPSCA;
             ,this.oParentObject.w_ZCODCON;
             ,this.oParentObject.w_ZCODVAL;
             ,this.oParentObject.w_ZTIPCON;
             ,this.w_NUMPRO;
             ,this.oParentObject.w_NCODAGE;
             ,this.oParentObject.w_NFLVABD;
             ,i_DATSYS;
             ,this.oParentObject.w_NNUMCOR;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='SCA_VARI'
    this.cWorkTables[3]='BAN_CONTI'
    this.cWorkTables[4]='AZIENDA'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_GSSO1BIK')
      use in _Curs_GSSO1BIK
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
