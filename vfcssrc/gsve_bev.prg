* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bev                                                        *
*              Check riga evasa senza import                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-07-01                                                      *
* Last revis.: 2004-07-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bev",oParentObject)
return(i_retval)

define class tgsve_bev as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_TIPDOC = space(5)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_MESS = space(1)
  * --- WorkFile variables
  RIGHEEVA_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per controllo riga in RIGHEEVA lanciato da GSVE_KDA, GSAC_KDA, GSOR_KDA, GSOR_MDV
    *     Se presente il record in questa tabella (la riga � stata Evasa senza Importazione) avviso l'utente.
    if this.oParentObject.w_MVFLEVAS<>"S"
      * --- Read from RIGHEEVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.RIGHEEVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIGHEEVA_idx,2],.t.,this.RIGHEEVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "RESERIAL"+;
          " from "+i_cTable+" RIGHEEVA where ";
              +"RESERRIF = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
              +" and REROWRIF = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
              +" and RENUMRIF = "+cp_ToStrODBC(-20);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          RESERIAL;
          from (i_cTable) where;
              RESERRIF = this.oParentObject.w_MVSERIAL;
              and REROWRIF = this.oParentObject.w_CPROWNUM;
              and RENUMRIF = -20;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SERIAL = NVL(cp_ToDate(_read_.RESERIAL),cp_NullValue(_read_.RESERIAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows>0
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC;
            from (i_cTable) where;
                MVSERIAL = this.w_SERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
          this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
          this.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
          this.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MESS = "Riga evasa senza importazione nel doc.: %1 n. %2 del: %3"
        ah_ErrorMsg(this.w_MESS,"!","", this.w_TIPDOC, Alltrim(Str(this.w_NUMDOC,15))+IIF(Empty(this.w_ALFDOC),"",Alltrim(this.w_ALFDOC)), DTOC(this.w_DATDOC) )
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='RIGHEEVA'
    this.cWorkTables[2]='DOC_MAST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
