* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bc4                                                        *
*              Controllo obsolescenza                                          *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_69]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-11-17                                                      *
* Last revis.: 2006-03-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bc4",oParentObject)
return(i_retval)

define class tgsar_bc4 as StdBatch
  * --- Local variables
  w_NUMREC = 0
  w_AbsRow = 0
  w_nRelRow = 0
  w_RECPOS = 0
  MASCHERA = .NULL.
  w_OQRY = space(50)
  w_OREP = space(50)
  w_SCAVAR = space(1)
  w_ROWP = 0
  w_ROWV = 0
  w_PAGRD = space(2)
  w_PAGCA = space(2)
  w_PAGBO = space(2)
  w_PAGRA = space(2)
  w_PAGRB = space(2)
  w_PAGMA = space(2)
  w_PAGRI = space(2)
  w_STASAL = space(1)
  w_STANSA = space(1)
  w_FSALD = space(1)
  w_TIPORD = space(1)
  w_ADOINI = space(2)
  w_ADOFIN = space(2)
  w_DDOINI = ctod("  /  /  ")
  w_DDOFIN = ctod("  /  /  ")
  w_NDOINI = 0
  w_NDOFIN = 0
  w_SCACON = space(1)
  w_CONSEL1 = space(15)
  w_SCACLI = space(1)
  w_CLISEL1 = space(15)
  w_FORSEL1 = space(15)
  w_SCAFOR = space(1)
  w_SCAINI = ctod("  /  /  ")
  w_SCAFIN = ctod("  /  /  ")
  w_VALUTA = space(3)
  w_BANNOS = space(15)
  w_BANPRE = space(15)
  w_AGENTE = space(5)
  w_CODBUN = space(3)
  w_MODPAG = space(10)
  w_PARINI = space(14)
  w_FSOSP = space(1)
  w_STASOS = space(1)
  w_STANSO = space(1)
  w_ONUM = 0
  w_FLGSOS = space(1)
  w_FLGNSO = space(1)
  w_OQRY = space(50)
  w_OREP = space(50)
  w_CONTOCOR = space(25)
  w_CODBANAP = space(10)
  * --- WorkFile variables
  ZOOMPART_idx=0
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato alla modifica del flag di obsolescenza sulla tabella BAN_CONTI.
    if ah_YesNo("Il conto %1 num. %2 verr� reso obsoleto%0Si vuole continuare?",,alltrim(this.oParentObject.w_CCCODBAN), alltrim(this.oParentObject.w_CCCONCOR))
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Seleziono il transitorio per verificare se la riga che si intende rendere obsoleta
      *     ha il flag di default, in questo caso devo sbiancare il conto corrente nell'anagrafica
      *     cliente/fornitori
      this.w_AbsRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nAbsRow
      this.w_nRelRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nRelRow
      SELECT (this.oParentObject.cTrsName)
      this.w_RECPOS = IIF( Eof() , RECNO(this.oParentObject.cTrsName)-1 , RECNO(this.oParentObject.cTrsName) )
      SELECT (this.oParentObject.cTrsName)
      Go this.w_RECPOS
      if t_DEFAULT=1 and t_CCOBSOLE=1
        This.oParentObject.oParentObject.w_Annumcor=space(25) 
 this.oParentObject.oParentObject.bUpdated=.t. 
 this.oParentObject.oParentObject.SetControlsValue()
        Replace t_DEFAULT with 0
      endif
      * --- Questa Parte derivata dal Metodo LoadRec
      SELECT (this.oParentObject.cTrsName)
      GO this.w_RECPOS
      With this.oParentObject
      .WorkFromTrs()
      .SaveDependsOn()
      .SetControlsValue()
      .ChildrenChangeRow()
      .oPgFrm.Page1.oPag.oBody.Refresh()
      .oPgFrm.Page1.oPag.oBody.nAbsRow=this.w_AbsRow
      .oPgFrm.Page1.oPag.oBody.nRelRow=this.w_nRelRow
      EndWith
      this.oparentObject.ecpSave()
      if this.oParentObject.w_CCTIPCON="C"
        ah_ErrorMsg("Il cliente %1 � stato aggiornato","!","",alltrim(this.oParentObject.w_CCCODCON))
      else
        ah_ErrorMsg("Il fornitore %1 � stato aggiornato","!","",alltrim(this.oParentObject.w_CCCODCON))
      endif
      * --- Inizializzo le variabili per poter lanciare la stampa.
      ah_Msg("Elaborazione partite...",.T.)
      * --- Elabora lo zoom con tutti i dati delle partite
      * --- Create temporary table ZOOMPART
      i_nIdx=cp_AddTableDef('ZOOMPART') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_YMRIVPKHFS[2]
      indexes_YMRIVPKHFS[1]='NUMPAR,DATSCA,TIPCON,CODCON,CODVAL,MODPAG,CODBUN'
      indexes_YMRIVPKHFS[2]='PTSERIAL'
      i_nConn=i_TableProp[this.PAR_TITE_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
            +" where 1=0";
            ,.f.,@indexes_YMRIVPKHFS)
      this.ZOOMPART_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      this.MASCHERA = This
      GSTE_BVS (this,this.MASCHERA)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Controllo il tipo di ordinamento selezionato
      vq_exec(this.w_OQRY,this,"__TMP__")
      * --- Drop temporary table ZOOMPART
      i_nIdx=cp_GetTableDefIdx('ZOOMPART')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('ZOOMPART')
      endif
      if RECCOUNT("__TMP__") > 0
        * --- Controllo se l'eventuale cancellazione di riga � andata a buon fine
        if ah_YesNo("Si vuole stampare elenco delle partite aperte aventi:%0Codice banca =%1;%0Conto corrente =%2;%0associati al conto reso obsoleto?",,space(10)+alltrim(this.w_CODBANAP), space(10)+alltrim(this.w_CONTOCOR))
          L_SCACLI=this.w_SCACLI
          L_PARB=this.w_PAGRB
          L_SCAFIN=this.w_SCAFIN
          L_ADOFIN=this.w_ADOFIN
          L_SCAFOR=this.w_SCAFOR
          L_PAMA=this.w_PAGMA
          L_CLISEL=this.w_CLISEL1
          L_DDOINI=this.w_DDOINI
          L_SCACON=this.w_SCACON
          L_PARI=this.w_PAGRI
          L_FORSEL=this.w_FORSEL1
          L_DDOFIN=this.w_DDOFIN
          L_STANSA=this.w_STANSA
          L_PACA=this.w_PAGCA
          L_CONSEL=this.w_CONSEL1
          L_PARINI=this.w_PARINI
          L_STASOS=this.w_STASOS
          L_PARA=this.w_PAGRA
          L_BANPRE=this.w_BANPRE
          L_VALUTA=this.w_VALUTA
          L_STANSO=this.w_STANSO
          L_ROWP=this.w_ROWP
          L_NDOINI=this.w_NDOINI
          L_AGENTE=this.w_AGENTE
          L_PARD=this.w_PAGRD
          L_ROWV=this.w_ROWV
          L_NDOFIN=this.w_NDOFIN
          L_CODBUN=this.w_CODBUN
          L_PABO=this.w_PAGBO
          L_SCAINI=this.w_SCAINI
          L_ADOINI=this.w_ADOFIN
          L_STASAL=this.w_STASAL
          L_BANNOS=this.w_BANNOS
          CP_CHPRN( ALLTRIM(this.w_OREP),"",this.oParentObject)
          if used("__tmp__")
            select __tmp__
            use
          endif
        endif
      endif
      if USED("__TMP__")
        Select __TMP__
        USE
      endif
      ah_Msg("Elaborazione documenti...",.T.)
      this.w_OQRY = "query\Gsarmbc4.vqr"
      vq_exec(this.w_OQRY,this,"__TMP__")
      this.w_OREP = "query\Gsarmbc4.frx"
      if RECCOUNT("__TMP__")>0
        if ah_YesNo("Si vuole stampare elenco dei documenti con scadenze appoggiate alla banca resa obsoleta?")
          L_CCTIPCON=this.oParentObject.w_CCTIPCON
          L_CCCODCON=this.oParentObject.w_CCCODCON
          L_CONTOCOR=this.w_CONTOCOR
          L_CODBANAP=this.w_CODBANAP
          CP_CHPRN( ALLTRIM(this.w_OREP),"",this.oParentObject)
          if used("__tmp__")
            select __tmp__
            use
          endif
        endif
      endif
      if USED("__TMP__")
        Select __TMP__
        USE
      endif
    else
      this.oParentObject.w_CCOBSOLE = "N"
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Effettuo questi assegnamenti per poter lanciare la stampa partite e scadenze.
    this.w_SCAVAR = "E"
    this.w_ROWP = 0
    this.w_ROWV = -1
    * --- Rimessa Diretta
    this.w_PAGRD = "RD"
    * --- Cambiale
    this.w_PAGCA = "CA"
    * --- Bonifico
    this.w_PAGBO = "BO"
    * --- Ri.Ba.
    this.w_PAGRA = "RA"
    * --- Ricevuta Bancaria
    this.w_PAGRB = "RB"
    * --- M.AV.
    this.w_PAGMA = "MA"
    * --- R.I.D.
    this.w_PAGRI = "RI"
    * --- Stampa le scadenze Saldate/Non Saldate e Tutte
    this.w_STASAL = "X"
    this.w_STANSA = SPACE(1)
    this.w_FSALD = "N"
    * --- Stampa Ordinata Per Codice Conto o per Sola Data Scadenza
    this.w_TIPORD = "S"
    * --- Serie Documento di Inizio Selezione
    this.w_ADOINI = space(2)
    * --- Serie Documento di Fine Selezione
    this.w_ADOFIN = space(2)
    this.w_DDOINI = cp_CharToDate("  -  -    ")
    this.w_DDOFIN = cp_CharToDate("  -  -    ")
    * --- Stampa le partite legate ai Conti
    this.w_CONSEL1 = space(15)
    if this.oParentObject.oparentObject.w_ANTIPCON="C"
      * --- Check Cliente
      this.w_SCACLI = "C"
      * --- Codice Cliente
      this.w_CLISEL1 = this.oParentObject.w_CCCODCON
      this.w_SCAFOR = "X"
      this.w_SCACON = "X"
      this.w_FORSEL1 = space(15)
    else
      * --- Check Fornitore
      this.w_SCAFOR = "F"
      * --- Codice Fornitore
      this.w_FORSEL1 = this.oParentObject.w_CCCODCON
      this.w_SCACLI = "X"
      this.w_SCACON = "X"
      this.w_CLISEL1 = SPACE(15)
    endif
    * --- Data Scadenza di inizio stampa
    this.w_SCAINI = cp_CharToDate("  -  -    ")
    this.w_SCAFIN = cp_CharToDate("  -  -    ")
    * --- Valuta selezionata (valuta di conto per l'esercizio in essere)
    this.w_VALUTA = space(3)
    * --- Conto Corrente Nostra banca di Appoggio per Bonifici/MAV
    this.w_BANNOS = space(15)
    this.w_BANPRE = space(15)
    this.w_AGENTE = space(5)
    * --- Business Unit selezionata
    this.w_CODBUN = space(3)
    this.w_MODPAG = space(10)
    * --- Numero Partita Selezionata
    this.w_PARINI = space(14)
    * --- Stampa le scadenze Sospese/Non Sospese e Tutte
    this.w_FSOSP = "T"
    this.w_STASOS = "S"
    this.w_STANSO = space(1)
    this.w_ONUM = 0
    this.w_OQRY = "query\gste11sis.vqr"
    this.w_OREP = "query\gste8kis.frx"
    this.w_CONTOCOR = this.oParentObject.w_CCCONCOR
    this.w_CODBANAP = this.oParentObject.w_CCCODBAN
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*ZOOMPART'
    this.cWorkTables[2]='PAR_TITE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
