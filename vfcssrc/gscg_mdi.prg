* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mdi                                                        *
*              Dati IVA annuali                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_46]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-02                                                      *
* Last revis.: 2018-01-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mdi")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mdi")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mdi")
  return

* --- Class definition
define class tgscg_mdi as StdPCForm
  Width  = 625
  Height = 381
  Top    = 10
  Left   = 10
  cComment = "Dati IVA annuali"
  cPrg = "gscg_mdi"
  HelpContextID=80356713
  add object cnt as tcgscg_mdi
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mdi as PCContext
  w_IACODAZI = space(5)
  w_IA__ANNO = space(4)
  w_IAPEAIVA = 0
  w_IADATVER = space(8)
  w_IAVALIVA = space(3)
  w_DECTOT = 0
  w_IAACCIVA = 0
  w_IACODBAN = space(15)
  w_IADESBAN = space(50)
  w_IAMINIVE = 0
  w_IAMINIVA = 0
  w_IAMACIVE = 0
  w_IAMACIVA = 0
  w_IAPLAMOB = space(1)
  w_IAVALPLA = space(3)
  w_DECTOP = 0
  w_IAPLAINI = 0
  w_DATACONCOMP = space(8)
  w_IALIMCOM = space(8)
  w_CALCPIC = 0
  w_CALCPIP = 0
  w_SIMVAL = space(5)
  w_DATSYS = space(8)
  w_IAMINFAT = 0
  w_IAMINCOR = 0
  w_IAMINFCO = 0
  w_IALORFCO = space(1)
  proc Save(i_oFrom)
    this.w_IACODAZI = i_oFrom.w_IACODAZI
    this.w_IA__ANNO = i_oFrom.w_IA__ANNO
    this.w_IAPEAIVA = i_oFrom.w_IAPEAIVA
    this.w_IADATVER = i_oFrom.w_IADATVER
    this.w_IAVALIVA = i_oFrom.w_IAVALIVA
    this.w_DECTOT = i_oFrom.w_DECTOT
    this.w_IAACCIVA = i_oFrom.w_IAACCIVA
    this.w_IACODBAN = i_oFrom.w_IACODBAN
    this.w_IADESBAN = i_oFrom.w_IADESBAN
    this.w_IAMINIVE = i_oFrom.w_IAMINIVE
    this.w_IAMINIVA = i_oFrom.w_IAMINIVA
    this.w_IAMACIVE = i_oFrom.w_IAMACIVE
    this.w_IAMACIVA = i_oFrom.w_IAMACIVA
    this.w_IAPLAMOB = i_oFrom.w_IAPLAMOB
    this.w_IAVALPLA = i_oFrom.w_IAVALPLA
    this.w_DECTOP = i_oFrom.w_DECTOP
    this.w_IAPLAINI = i_oFrom.w_IAPLAINI
    this.w_DATACONCOMP = i_oFrom.w_DATACONCOMP
    this.w_IALIMCOM = i_oFrom.w_IALIMCOM
    this.w_CALCPIC = i_oFrom.w_CALCPIC
    this.w_CALCPIP = i_oFrom.w_CALCPIP
    this.w_SIMVAL = i_oFrom.w_SIMVAL
    this.w_DATSYS = i_oFrom.w_DATSYS
    this.w_IAMINFAT = i_oFrom.w_IAMINFAT
    this.w_IAMINCOR = i_oFrom.w_IAMINCOR
    this.w_IAMINFCO = i_oFrom.w_IAMINFCO
    this.w_IALORFCO = i_oFrom.w_IALORFCO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_IACODAZI = this.w_IACODAZI
    i_oTo.w_IA__ANNO = this.w_IA__ANNO
    i_oTo.w_IAPEAIVA = this.w_IAPEAIVA
    i_oTo.w_IADATVER = this.w_IADATVER
    i_oTo.w_IAVALIVA = this.w_IAVALIVA
    i_oTo.w_DECTOT = this.w_DECTOT
    i_oTo.w_IAACCIVA = this.w_IAACCIVA
    i_oTo.w_IACODBAN = this.w_IACODBAN
    i_oTo.w_IADESBAN = this.w_IADESBAN
    i_oTo.w_IAMINIVE = this.w_IAMINIVE
    i_oTo.w_IAMINIVA = this.w_IAMINIVA
    i_oTo.w_IAMACIVE = this.w_IAMACIVE
    i_oTo.w_IAMACIVA = this.w_IAMACIVA
    i_oTo.w_IAPLAMOB = this.w_IAPLAMOB
    i_oTo.w_IAVALPLA = this.w_IAVALPLA
    i_oTo.w_DECTOP = this.w_DECTOP
    i_oTo.w_IAPLAINI = this.w_IAPLAINI
    i_oTo.w_DATACONCOMP = this.w_DATACONCOMP
    i_oTo.w_IALIMCOM = this.w_IALIMCOM
    i_oTo.w_CALCPIC = this.w_CALCPIC
    i_oTo.w_CALCPIP = this.w_CALCPIP
    i_oTo.w_SIMVAL = this.w_SIMVAL
    i_oTo.w_DATSYS = this.w_DATSYS
    i_oTo.w_IAMINFAT = this.w_IAMINFAT
    i_oTo.w_IAMINCOR = this.w_IAMINCOR
    i_oTo.w_IAMINFCO = this.w_IAMINFCO
    i_oTo.w_IALORFCO = this.w_IALORFCO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mdi as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 625
  Height = 381
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-01-02"
  HelpContextID=80356713
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DAT_IVAN_IDX = 0
  VALUTE_IDX = 0
  cFile = "DAT_IVAN"
  cKeySelect = "IACODAZI"
  cKeyWhere  = "IACODAZI=this.w_IACODAZI"
  cKeyDetail  = "IACODAZI=this.w_IACODAZI and IA__ANNO=this.w_IA__ANNO"
  cKeyWhereODBC = '"IACODAZI="+cp_ToStrODBC(this.w_IACODAZI)';

  cKeyDetailWhereODBC = '"IACODAZI="+cp_ToStrODBC(this.w_IACODAZI)';
      +'+" and IA__ANNO="+cp_ToStrODBC(this.w_IA__ANNO)';

  cKeyWhereODBCqualified = '"DAT_IVAN.IACODAZI="+cp_ToStrODBC(this.w_IACODAZI)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gscg_mdi"
  cComment = "Dati IVA annuali"
  i_nRowNum = 0
  i_nRowPerPage = 5
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_IACODAZI = space(5)
  w_IA__ANNO = space(4)
  w_IAPEAIVA = 0
  w_IADATVER = ctod('  /  /  ')
  w_IAVALIVA = space(3)
  w_DECTOT = 0
  w_IAACCIVA = 0
  w_IACODBAN = space(15)
  w_IADESBAN = space(50)
  w_IAMINIVE = 0
  w_IAMINIVA = 0
  w_IAMACIVE = 0
  w_IAMACIVA = 0
  w_IAPLAMOB = space(1)
  w_IAVALPLA = space(3)
  w_DECTOP = 0
  w_IAPLAINI = 0
  w_DATACONCOMP = ctod('  /  /  ')
  w_IALIMCOM = ctod('  /  /  ')
  w_CALCPIC = 0
  w_CALCPIP = 0
  w_SIMVAL = space(5)
  w_DATSYS = ctod('  /  /  ')
  w_IAMINFAT = 0
  w_IAMINCOR = 0
  w_IAMINFCO = 0
  w_IALORFCO = space(1)

  * --- Children pointers
  GSCG_MPF = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSCG_MPF additive
    with this
      .Pages(1).addobject("oPag","tgscg_mdiPag1","gscg_mdi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSCG_MPF
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='DAT_IVAN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DAT_IVAN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DAT_IVAN_IDX,3]
  return

  function CreateChildren()
    this.GSCG_MPF = CREATEOBJECT('stdLazyChild',this,'GSCG_MPF')
    return

  procedure NewContext()
    return(createobject('tsgscg_mdi'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSCG_MPF)
      this.GSCG_MPF.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSCG_MPF.HideChildrenChain()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCG_MPF)
      this.GSCG_MPF.DestroyChildrenChain()
      this.GSCG_MPF=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_MPF.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_MPF.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_MPF.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSCG_MPF.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_IACODAZI,"DICODAZI";
             ,.w_IA__ANNO,"DI__ANNO";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_14_joined
    link_2_14_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DAT_IVAN where IACODAZI=KeySet.IACODAZI
    *                            and IA__ANNO=KeySet.IA__ANNO
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DAT_IVAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2],this.bLoadRecFilter,this.DAT_IVAN_IDX,"gscg_mdi")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DAT_IVAN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DAT_IVAN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DAT_IVAN '
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_14_joined=this.AddJoinedLink_2_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'IACODAZI',this.w_IACODAZI  )
      select * from (i_cTable) DAT_IVAN where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DATSYS = i_datsys
        .w_IACODAZI = NVL(IACODAZI,space(5))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DAT_IVAN')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DECTOT = 0
          .w_DECTOP = 0
          .w_SIMVAL = space(5)
          .w_IA__ANNO = NVL(IA__ANNO,space(4))
          .w_IAPEAIVA = NVL(IAPEAIVA,0)
          .w_IADATVER = NVL(cp_ToDate(IADATVER),ctod("  /  /  "))
          .w_IAVALIVA = NVL(IAVALIVA,space(3))
          if link_2_4_joined
            this.w_IAVALIVA = NVL(VACODVAL204,NVL(this.w_IAVALIVA,space(3)))
            this.w_DECTOT = NVL(VADECTOT204,0)
          else
          .link_2_4('Load')
          endif
          .w_IAACCIVA = NVL(IAACCIVA,0)
          .w_IACODBAN = NVL(IACODBAN,space(15))
          .w_IADESBAN = NVL(IADESBAN,space(50))
          .w_IAMINIVE = NVL(IAMINIVE,0)
          .w_IAMINIVA = NVL(IAMINIVA,0)
          .w_IAMACIVE = NVL(IAMACIVE,0)
          .w_IAMACIVA = NVL(IAMACIVA,0)
          .w_IAPLAMOB = NVL(IAPLAMOB,space(1))
          .w_IAVALPLA = NVL(IAVALPLA,space(3))
          if link_2_14_joined
            this.w_IAVALPLA = NVL(VACODVAL214,NVL(this.w_IAVALPLA,space(3)))
            this.w_DECTOP = NVL(VADECTOT214,0)
            this.w_SIMVAL = NVL(VASIMVAL214,space(5))
          else
          .link_2_14('Load')
          endif
          .w_IAPLAINI = NVL(IAPLAINI,0)
        .w_DATACONCOMP = iif(Empty(.w_IA__ANNO),{},Date(Val(.w_IA__ANNO),12,31))
          .w_IALIMCOM = NVL(cp_ToDate(IALIMCOM),ctod("  /  /  "))
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .w_CALCPIP = DEFPIP(.w_DECTOP)
        .oPgFrm.Page1.oPag.oObj_2_21.Calculate()
          .w_IAMINFAT = NVL(IAMINFAT,0)
          .w_IAMINCOR = NVL(IAMINCOR,0)
          .w_IAMINFCO = NVL(IAMINFCO,0)
          .w_IALORFCO = NVL(IALORFCO,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace IA__ANNO with .w_IA__ANNO
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_IACODAZI=space(5)
      .w_IA__ANNO=space(4)
      .w_IAPEAIVA=0
      .w_IADATVER=ctod("  /  /  ")
      .w_IAVALIVA=space(3)
      .w_DECTOT=0
      .w_IAACCIVA=0
      .w_IACODBAN=space(15)
      .w_IADESBAN=space(50)
      .w_IAMINIVE=0
      .w_IAMINIVA=0
      .w_IAMACIVE=0
      .w_IAMACIVA=0
      .w_IAPLAMOB=space(1)
      .w_IAVALPLA=space(3)
      .w_DECTOP=0
      .w_IAPLAINI=0
      .w_DATACONCOMP=ctod("  /  /  ")
      .w_IALIMCOM=ctod("  /  /  ")
      .w_CALCPIC=0
      .w_CALCPIP=0
      .w_SIMVAL=space(5)
      .w_DATSYS=ctod("  /  /  ")
      .w_IAMINFAT=0
      .w_IAMINCOR=0
      .w_IAMINFCO=0
      .w_IALORFCO=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        .w_IAVALIVA = g_PERVAL
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_IAVALIVA))
         .link_2_4('Full')
        endif
        .DoRTCalc(6,14,.f.)
        .w_IAVALPLA = g_PERVAL
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_IAVALPLA))
         .link_2_14('Full')
        endif
        .DoRTCalc(16,17,.f.)
        .w_DATACONCOMP = iif(Empty(.w_IA__ANNO),{},Date(Val(.w_IA__ANNO),12,31))
        .DoRTCalc(19,19,.f.)
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .w_CALCPIP = DEFPIP(.w_DECTOP)
        .oPgFrm.Page1.oPag.oObj_2_21.Calculate()
        .DoRTCalc(22,22,.f.)
        .w_DATSYS = i_datsys
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(24,26,.f.)
        .w_IALORFCO = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'DAT_IVAN')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oIADESBAN_2_8.enabled = i_bVal
      .Page1.oPag.oIAMINIVE_2_9.enabled = i_bVal
      .Page1.oPag.oIAMINIVA_2_10.enabled = i_bVal
      .Page1.oPag.oIAMACIVE_2_11.enabled = i_bVal
      .Page1.oPag.oIAMACIVA_2_12.enabled = i_bVal
      .Page1.oPag.oIAPLAMOB_2_13.enabled = i_bVal
      .Page1.oPag.oIAVALPLA_2_14.enabled = i_bVal
      .Page1.oPag.oIAPLAINI_2_16.enabled = i_bVal
      .Page1.oPag.oIALIMCOM_2_18.enabled = i_bVal
      .Page1.oPag.oIAMINFAT_2_24.enabled = i_bVal
      .Page1.oPag.oIAMINCOR_2_25.enabled = i_bVal
      .Page1.oPag.oIAMINFCO_2_26.enabled = i_bVal
      .Page1.oPag.oIALORFCO_2_27.enabled = i_bVal
      .Page1.oPag.oObj_2_21.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSCG_MPF.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'DAT_IVAN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_MPF.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DAT_IVAN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_IACODAZI,"IACODAZI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_IA__ANNO C(4);
      ,t_IAPEAIVA N(5,2);
      ,t_IADATVER D(8);
      ,t_IAVALIVA N(3);
      ,t_IAACCIVA N(18,4);
      ,t_IACODBAN C(15);
      ,t_IADESBAN C(50);
      ,t_IAMINIVE N(18,4);
      ,t_IAMINIVA N(18,4);
      ,t_IAMACIVE N(18,4);
      ,t_IAMACIVA N(18,4);
      ,t_IAPLAMOB N(3);
      ,t_IAVALPLA N(3);
      ,t_IAPLAINI N(18,4);
      ,t_IALIMCOM D(8);
      ,t_IAMINFAT N(18,5);
      ,t_IAMINCOR N(18,5);
      ,t_IAMINFCO N(18,5);
      ,t_IALORFCO N(3);
      ,IA__ANNO C(4);
      ,t_DECTOT N(1);
      ,t_DECTOP N(1);
      ,t_DATACONCOMP D(8);
      ,t_CALCPIC N(1);
      ,t_CALCPIP N(1);
      ,t_SIMVAL C(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mdibodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIA__ANNO_2_1.controlsource=this.cTrsName+'.t_IA__ANNO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIAPEAIVA_2_2.controlsource=this.cTrsName+'.t_IAPEAIVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIADATVER_2_3.controlsource=this.cTrsName+'.t_IADATVER'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIAVALIVA_2_4.controlsource=this.cTrsName+'.t_IAVALIVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIAACCIVA_2_6.controlsource=this.cTrsName+'.t_IAACCIVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oIACODBAN_2_7.controlsource=this.cTrsName+'.t_IACODBAN'
    this.oPgFRm.Page1.oPag.oIADESBAN_2_8.controlsource=this.cTrsName+'.t_IADESBAN'
    this.oPgFRm.Page1.oPag.oIAMINIVE_2_9.controlsource=this.cTrsName+'.t_IAMINIVE'
    this.oPgFRm.Page1.oPag.oIAMINIVA_2_10.controlsource=this.cTrsName+'.t_IAMINIVA'
    this.oPgFRm.Page1.oPag.oIAMACIVE_2_11.controlsource=this.cTrsName+'.t_IAMACIVE'
    this.oPgFRm.Page1.oPag.oIAMACIVA_2_12.controlsource=this.cTrsName+'.t_IAMACIVA'
    this.oPgFRm.Page1.oPag.oIAPLAMOB_2_13.controlsource=this.cTrsName+'.t_IAPLAMOB'
    this.oPgFRm.Page1.oPag.oIAVALPLA_2_14.controlsource=this.cTrsName+'.t_IAVALPLA'
    this.oPgFRm.Page1.oPag.oIAPLAINI_2_16.controlsource=this.cTrsName+'.t_IAPLAINI'
    this.oPgFRm.Page1.oPag.oIALIMCOM_2_18.controlsource=this.cTrsName+'.t_IALIMCOM'
    this.oPgFRm.Page1.oPag.oIAMINFAT_2_24.controlsource=this.cTrsName+'.t_IAMINFAT'
    this.oPgFRm.Page1.oPag.oIAMINCOR_2_25.controlsource=this.cTrsName+'.t_IAMINCOR'
    this.oPgFRm.Page1.oPag.oIAMINFCO_2_26.controlsource=this.cTrsName+'.t_IAMINFCO'
    this.oPgFRm.Page1.oPag.oIALORFCO_2_27.controlsource=this.cTrsName+'.t_IALORFCO'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(51)
    this.AddVLine(123)
    this.AddVLine(213)
    this.AddVLine(300)
    this.AddVLine(442)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIA__ANNO_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DAT_IVAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DAT_IVAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2])
      *
      * insert into DAT_IVAN
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DAT_IVAN')
        i_extval=cp_InsertValODBCExtFlds(this,'DAT_IVAN')
        i_cFldBody=" "+;
                  "(IACODAZI,IA__ANNO,IAPEAIVA,IADATVER,IAVALIVA"+;
                  ",IAACCIVA,IACODBAN,IADESBAN,IAMINIVE,IAMINIVA"+;
                  ",IAMACIVE,IAMACIVA,IAPLAMOB,IAVALPLA,IAPLAINI"+;
                  ",IALIMCOM,IAMINFAT,IAMINCOR,IAMINFCO,IALORFCO,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_IACODAZI)+","+cp_ToStrODBC(this.w_IA__ANNO)+","+cp_ToStrODBC(this.w_IAPEAIVA)+","+cp_ToStrODBC(this.w_IADATVER)+","+cp_ToStrODBCNull(this.w_IAVALIVA)+;
             ","+cp_ToStrODBC(this.w_IAACCIVA)+","+cp_ToStrODBC(this.w_IACODBAN)+","+cp_ToStrODBC(this.w_IADESBAN)+","+cp_ToStrODBC(this.w_IAMINIVE)+","+cp_ToStrODBC(this.w_IAMINIVA)+;
             ","+cp_ToStrODBC(this.w_IAMACIVE)+","+cp_ToStrODBC(this.w_IAMACIVA)+","+cp_ToStrODBC(this.w_IAPLAMOB)+","+cp_ToStrODBCNull(this.w_IAVALPLA)+","+cp_ToStrODBC(this.w_IAPLAINI)+;
             ","+cp_ToStrODBC(this.w_IALIMCOM)+","+cp_ToStrODBC(this.w_IAMINFAT)+","+cp_ToStrODBC(this.w_IAMINCOR)+","+cp_ToStrODBC(this.w_IAMINFCO)+","+cp_ToStrODBC(this.w_IALORFCO)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DAT_IVAN')
        i_extval=cp_InsertValVFPExtFlds(this,'DAT_IVAN')
        cp_CheckDeletedKey(i_cTable,0,'IACODAZI',this.w_IACODAZI,'IA__ANNO',this.w_IA__ANNO)
        INSERT INTO (i_cTable) (;
                   IACODAZI;
                  ,IA__ANNO;
                  ,IAPEAIVA;
                  ,IADATVER;
                  ,IAVALIVA;
                  ,IAACCIVA;
                  ,IACODBAN;
                  ,IADESBAN;
                  ,IAMINIVE;
                  ,IAMINIVA;
                  ,IAMACIVE;
                  ,IAMACIVA;
                  ,IAPLAMOB;
                  ,IAVALPLA;
                  ,IAPLAINI;
                  ,IALIMCOM;
                  ,IAMINFAT;
                  ,IAMINCOR;
                  ,IAMINFCO;
                  ,IALORFCO;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_IACODAZI;
                  ,this.w_IA__ANNO;
                  ,this.w_IAPEAIVA;
                  ,this.w_IADATVER;
                  ,this.w_IAVALIVA;
                  ,this.w_IAACCIVA;
                  ,this.w_IACODBAN;
                  ,this.w_IADESBAN;
                  ,this.w_IAMINIVE;
                  ,this.w_IAMINIVA;
                  ,this.w_IAMACIVE;
                  ,this.w_IAMACIVA;
                  ,this.w_IAPLAMOB;
                  ,this.w_IAVALPLA;
                  ,this.w_IAPLAINI;
                  ,this.w_IALIMCOM;
                  ,this.w_IAMINFAT;
                  ,this.w_IAMINCOR;
                  ,this.w_IAMINFCO;
                  ,this.w_IALORFCO;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DAT_IVAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_IA__ANNO))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DAT_IVAN')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and IA__ANNO="+cp_ToStrODBC(&i_TN.->IA__ANNO)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DAT_IVAN')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and IA__ANNO=&i_TN.->IA__ANNO;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_IA__ANNO))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSCG_MPF.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_IACODAZI,"DICODAZI";
                     ,this.w_IA__ANNO,"DI__ANNO";
                     )
              this.GSCG_MPF.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and IA__ANNO="+cp_ToStrODBC(&i_TN.->IA__ANNO)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and IA__ANNO=&i_TN.->IA__ANNO;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace IA__ANNO with this.w_IA__ANNO
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DAT_IVAN
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DAT_IVAN')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " IAPEAIVA="+cp_ToStrODBC(this.w_IAPEAIVA)+;
                     ",IADATVER="+cp_ToStrODBC(this.w_IADATVER)+;
                     ",IAVALIVA="+cp_ToStrODBCNull(this.w_IAVALIVA)+;
                     ",IAACCIVA="+cp_ToStrODBC(this.w_IAACCIVA)+;
                     ",IACODBAN="+cp_ToStrODBC(this.w_IACODBAN)+;
                     ",IADESBAN="+cp_ToStrODBC(this.w_IADESBAN)+;
                     ",IAMINIVE="+cp_ToStrODBC(this.w_IAMINIVE)+;
                     ",IAMINIVA="+cp_ToStrODBC(this.w_IAMINIVA)+;
                     ",IAMACIVE="+cp_ToStrODBC(this.w_IAMACIVE)+;
                     ",IAMACIVA="+cp_ToStrODBC(this.w_IAMACIVA)+;
                     ",IAPLAMOB="+cp_ToStrODBC(this.w_IAPLAMOB)+;
                     ",IAVALPLA="+cp_ToStrODBCNull(this.w_IAVALPLA)+;
                     ",IAPLAINI="+cp_ToStrODBC(this.w_IAPLAINI)+;
                     ",IALIMCOM="+cp_ToStrODBC(this.w_IALIMCOM)+;
                     ",IAMINFAT="+cp_ToStrODBC(this.w_IAMINFAT)+;
                     ",IAMINCOR="+cp_ToStrODBC(this.w_IAMINCOR)+;
                     ",IAMINFCO="+cp_ToStrODBC(this.w_IAMINFCO)+;
                     ",IALORFCO="+cp_ToStrODBC(this.w_IALORFCO)+;
                     ",IA__ANNO="+cp_ToStrODBC(this.w_IA__ANNO)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and IA__ANNO="+cp_ToStrODBC(IA__ANNO)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DAT_IVAN')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      IAPEAIVA=this.w_IAPEAIVA;
                     ,IADATVER=this.w_IADATVER;
                     ,IAVALIVA=this.w_IAVALIVA;
                     ,IAACCIVA=this.w_IAACCIVA;
                     ,IACODBAN=this.w_IACODBAN;
                     ,IADESBAN=this.w_IADESBAN;
                     ,IAMINIVE=this.w_IAMINIVE;
                     ,IAMINIVA=this.w_IAMINIVA;
                     ,IAMACIVE=this.w_IAMACIVE;
                     ,IAMACIVA=this.w_IAMACIVA;
                     ,IAPLAMOB=this.w_IAPLAMOB;
                     ,IAVALPLA=this.w_IAVALPLA;
                     ,IAPLAINI=this.w_IAPLAINI;
                     ,IALIMCOM=this.w_IALIMCOM;
                     ,IAMINFAT=this.w_IAMINFAT;
                     ,IAMINCOR=this.w_IAMINCOR;
                     ,IAMINFCO=this.w_IAMINFCO;
                     ,IALORFCO=this.w_IALORFCO;
                     ,IA__ANNO=this.w_IA__ANNO;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and IA__ANNO=&i_TN.->IA__ANNO;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not(Empty(t_IA__ANNO)))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSCG_MPF.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_IACODAZI,"DICODAZI";
               ,this.w_IA__ANNO,"DI__ANNO";
               )
          this.GSCG_MPF.mReplace()
          this.GSCG_MPF.bSaveContext=.f.
        endif
      endscan
     this.GSCG_MPF.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DAT_IVAN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_IA__ANNO))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSCG_MPF : Deleting
        this.GSCG_MPF.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_IACODAZI,"DICODAZI";
               ,this.w_IA__ANNO,"DI__ANNO";
               )
        this.GSCG_MPF.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DAT_IVAN
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and IA__ANNO="+cp_ToStrODBC(&i_TN.->IA__ANNO)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and IA__ANNO=&i_TN.->IA__ANNO;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_IA__ANNO))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DAT_IVAN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAT_IVAN_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,17,.t.)
          .w_DATACONCOMP = iif(Empty(.w_IA__ANNO),{},Date(Val(.w_IA__ANNO),12,31))
        .DoRTCalc(19,19,.t.)
          .w_CALCPIC = DEFPIC(.w_DECTOT)
          .w_CALCPIP = DEFPIP(.w_DECTOP)
        .oPgFrm.Page1.oPag.oObj_2_21.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(22,27,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DECTOT with this.w_DECTOT
      replace t_DECTOP with this.w_DECTOP
      replace t_DATACONCOMP with this.w_DATACONCOMP
      replace t_CALCPIC with this.w_CALCPIC
      replace t_CALCPIP with this.w_CALCPIP
      replace t_SIMVAL with this.w_SIMVAL
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_21.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_21.Calculate()
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oIAPLAINI_2_16.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oIAPLAINI_2_16.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_2_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IAVALIVA
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IAVALIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_IAVALIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_IAVALIVA))
          select VACODVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IAVALIVA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IAVALIVA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oIAVALIVA_2_4'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IAVALIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_IAVALIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_IAVALIVA)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IAVALIVA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_IAVALIVA = space(3)
      endif
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IAVALIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.VACODVAL as VACODVAL204"+ ",link_2_4.VADECTOT as VADECTOT204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on DAT_IVAN.IAVALIVA=link_2_4.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and DAT_IVAN.IAVALIVA=link_2_4.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=IAVALPLA
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IAVALPLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_IAVALPLA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_IAVALPLA))
          select VACODVAL,VADECTOT,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IAVALPLA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IAVALPLA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oIAVALPLA_2_14'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IAVALPLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_IAVALPLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_IAVALPLA)
            select VACODVAL,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IAVALPLA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOP = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_IAVALPLA = space(3)
      endif
      this.w_DECTOP = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IAVALPLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_14.VACODVAL as VACODVAL214"+ ",link_2_14.VADECTOT as VADECTOT214"+ ",link_2_14.VASIMVAL as VASIMVAL214"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_14 on DAT_IVAN.IAVALPLA=link_2_14.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_14"
          i_cKey=i_cKey+'+" and DAT_IVAN.IAVALPLA=link_2_14.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oIADESBAN_2_8.value==this.w_IADESBAN)
      this.oPgFrm.Page1.oPag.oIADESBAN_2_8.value=this.w_IADESBAN
      replace t_IADESBAN with this.oPgFrm.Page1.oPag.oIADESBAN_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIAMINIVE_2_9.value==this.w_IAMINIVE)
      this.oPgFrm.Page1.oPag.oIAMINIVE_2_9.value=this.w_IAMINIVE
      replace t_IAMINIVE with this.oPgFrm.Page1.oPag.oIAMINIVE_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIAMINIVA_2_10.value==this.w_IAMINIVA)
      this.oPgFrm.Page1.oPag.oIAMINIVA_2_10.value=this.w_IAMINIVA
      replace t_IAMINIVA with this.oPgFrm.Page1.oPag.oIAMINIVA_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIAMACIVE_2_11.value==this.w_IAMACIVE)
      this.oPgFrm.Page1.oPag.oIAMACIVE_2_11.value=this.w_IAMACIVE
      replace t_IAMACIVE with this.oPgFrm.Page1.oPag.oIAMACIVE_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIAMACIVA_2_12.value==this.w_IAMACIVA)
      this.oPgFrm.Page1.oPag.oIAMACIVA_2_12.value=this.w_IAMACIVA
      replace t_IAMACIVA with this.oPgFrm.Page1.oPag.oIAMACIVA_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIAPLAMOB_2_13.RadioValue()==this.w_IAPLAMOB)
      this.oPgFrm.Page1.oPag.oIAPLAMOB_2_13.SetRadio()
      replace t_IAPLAMOB with this.oPgFrm.Page1.oPag.oIAPLAMOB_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIAVALPLA_2_14.RadioValue()==this.w_IAVALPLA)
      this.oPgFrm.Page1.oPag.oIAVALPLA_2_14.SetRadio()
      replace t_IAVALPLA with this.oPgFrm.Page1.oPag.oIAVALPLA_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIAPLAINI_2_16.value==this.w_IAPLAINI)
      this.oPgFrm.Page1.oPag.oIAPLAINI_2_16.value=this.w_IAPLAINI
      replace t_IAPLAINI with this.oPgFrm.Page1.oPag.oIAPLAINI_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIALIMCOM_2_18.value==this.w_IALIMCOM)
      this.oPgFrm.Page1.oPag.oIALIMCOM_2_18.value=this.w_IALIMCOM
      replace t_IALIMCOM with this.oPgFrm.Page1.oPag.oIALIMCOM_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIAMINFAT_2_24.value==this.w_IAMINFAT)
      this.oPgFrm.Page1.oPag.oIAMINFAT_2_24.value=this.w_IAMINFAT
      replace t_IAMINFAT with this.oPgFrm.Page1.oPag.oIAMINFAT_2_24.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIAMINCOR_2_25.value==this.w_IAMINCOR)
      this.oPgFrm.Page1.oPag.oIAMINCOR_2_25.value=this.w_IAMINCOR
      replace t_IAMINCOR with this.oPgFrm.Page1.oPag.oIAMINCOR_2_25.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIAMINFCO_2_26.value==this.w_IAMINFCO)
      this.oPgFrm.Page1.oPag.oIAMINFCO_2_26.value=this.w_IAMINFCO
      replace t_IAMINFCO with this.oPgFrm.Page1.oPag.oIAMINFCO_2_26.value
    endif
    if not(this.oPgFrm.Page1.oPag.oIALORFCO_2_27.RadioValue()==this.w_IALORFCO)
      this.oPgFrm.Page1.oPag.oIALORFCO_2_27.SetRadio()
      replace t_IALORFCO with this.oPgFrm.Page1.oPag.oIALORFCO_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIA__ANNO_2_1.value==this.w_IA__ANNO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIA__ANNO_2_1.value=this.w_IA__ANNO
      replace t_IA__ANNO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIA__ANNO_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIAPEAIVA_2_2.value==this.w_IAPEAIVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIAPEAIVA_2_2.value=this.w_IAPEAIVA
      replace t_IAPEAIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIAPEAIVA_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIADATVER_2_3.value==this.w_IADATVER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIADATVER_2_3.value=this.w_IADATVER
      replace t_IADATVER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIADATVER_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIAVALIVA_2_4.RadioValue()==this.w_IAVALIVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIAVALIVA_2_4.SetRadio()
      replace t_IAVALIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIAVALIVA_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIAACCIVA_2_6.value==this.w_IAACCIVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIAACCIVA_2_6.value=this.w_IAACCIVA
      replace t_IAACCIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIAACCIVA_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIACODBAN_2_7.value==this.w_IACODBAN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIACODBAN_2_7.value=this.w_IACODBAN
      replace t_IACODBAN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIACODBAN_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'DAT_IVAN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_IAVALIVA) and (not(Empty(.w_IA__ANNO)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIAVALIVA_2_4
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   empty(.w_IAVALPLA) and (not(Empty(.w_IA__ANNO)))
          .oNewFocus=.oPgFrm.Page1.oPag.oIAVALPLA_2_14
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(Empty(.w_IALIMCOM) Or .w_IALIMCOM>.w_DATACONCOMP) and (not(Empty(.w_IA__ANNO)))
          .oNewFocus=.oPgFrm.Page1.oPag.oIALIMCOM_2_18
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      i_bRes = i_bRes .and. .GSCG_MPF.CheckForm()
      if not(Empty(.w_IA__ANNO))
        * --- Area Manuale = Check Row
        * --- gscg_mdi
        * --- Controlla che l'Acconto Dicembre sia 0 o maggiore del Versamento Minimo Acconto
        if .w_IAACCIVA<>0 AND .w_IAACCIVA<IIF(.w_IAVALIVA=g_CODLIR, .w_IAMACIVA, .w_IAMACIVE)
           i_bRes = .f.
           i_bnoChk = .f.
           i_cErrorMsg = ah_MsgFormat("Importo acconto IVA inferiore al versamento minimo")
        endif
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSCG_MPF : Depends On
    this.GSCG_MPF.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_IA__ANNO)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_IA__ANNO=space(4)
      .w_IAPEAIVA=0
      .w_IADATVER=ctod("  /  /  ")
      .w_IAVALIVA=space(3)
      .w_DECTOT=0
      .w_IAACCIVA=0
      .w_IACODBAN=space(15)
      .w_IADESBAN=space(50)
      .w_IAMINIVE=0
      .w_IAMINIVA=0
      .w_IAMACIVE=0
      .w_IAMACIVA=0
      .w_IAPLAMOB=space(1)
      .w_IAVALPLA=space(3)
      .w_DECTOP=0
      .w_IAPLAINI=0
      .w_DATACONCOMP=ctod("  /  /  ")
      .w_IALIMCOM=ctod("  /  /  ")
      .w_CALCPIC=0
      .w_CALCPIP=0
      .w_SIMVAL=space(5)
      .w_IAMINFAT=0
      .w_IAMINCOR=0
      .w_IAMINFCO=0
      .w_IALORFCO=space(1)
      .DoRTCalc(1,4,.f.)
        .w_IAVALIVA = g_PERVAL
      .DoRTCalc(5,5,.f.)
      if not(empty(.w_IAVALIVA))
        .link_2_4('Full')
      endif
      .DoRTCalc(6,14,.f.)
        .w_IAVALPLA = g_PERVAL
      .DoRTCalc(15,15,.f.)
      if not(empty(.w_IAVALPLA))
        .link_2_14('Full')
      endif
      .DoRTCalc(16,17,.f.)
        .w_DATACONCOMP = iif(Empty(.w_IA__ANNO),{},Date(Val(.w_IA__ANNO),12,31))
      .DoRTCalc(19,19,.f.)
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .w_CALCPIP = DEFPIP(.w_DECTOP)
        .oPgFrm.Page1.oPag.oObj_2_21.Calculate()
      .DoRTCalc(22,26,.f.)
        .w_IALORFCO = 'N'
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_IA__ANNO = t_IA__ANNO
    this.w_IAPEAIVA = t_IAPEAIVA
    this.w_IADATVER = t_IADATVER
    this.w_IAVALIVA = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIAVALIVA_2_4.RadioValue(.t.)
    this.w_DECTOT = t_DECTOT
    this.w_IAACCIVA = t_IAACCIVA
    this.w_IACODBAN = t_IACODBAN
    this.w_IADESBAN = t_IADESBAN
    this.w_IAMINIVE = t_IAMINIVE
    this.w_IAMINIVA = t_IAMINIVA
    this.w_IAMACIVE = t_IAMACIVE
    this.w_IAMACIVA = t_IAMACIVA
    this.w_IAPLAMOB = this.oPgFrm.Page1.oPag.oIAPLAMOB_2_13.RadioValue(.t.)
    this.w_IAVALPLA = this.oPgFrm.Page1.oPag.oIAVALPLA_2_14.RadioValue(.t.)
    this.w_DECTOP = t_DECTOP
    this.w_IAPLAINI = t_IAPLAINI
    this.w_DATACONCOMP = t_DATACONCOMP
    this.w_IALIMCOM = t_IALIMCOM
    this.w_CALCPIC = t_CALCPIC
    this.w_CALCPIP = t_CALCPIP
    this.w_SIMVAL = t_SIMVAL
    this.w_IAMINFAT = t_IAMINFAT
    this.w_IAMINCOR = t_IAMINCOR
    this.w_IAMINFCO = t_IAMINFCO
    this.w_IALORFCO = this.oPgFrm.Page1.oPag.oIALORFCO_2_27.RadioValue(.t.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_IA__ANNO with this.w_IA__ANNO
    replace t_IAPEAIVA with this.w_IAPEAIVA
    replace t_IADATVER with this.w_IADATVER
    replace t_IAVALIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oIAVALIVA_2_4.ToRadio()
    replace t_DECTOT with this.w_DECTOT
    replace t_IAACCIVA with this.w_IAACCIVA
    replace t_IACODBAN with this.w_IACODBAN
    replace t_IADESBAN with this.w_IADESBAN
    replace t_IAMINIVE with this.w_IAMINIVE
    replace t_IAMINIVA with this.w_IAMINIVA
    replace t_IAMACIVE with this.w_IAMACIVE
    replace t_IAMACIVA with this.w_IAMACIVA
    replace t_IAPLAMOB with this.oPgFrm.Page1.oPag.oIAPLAMOB_2_13.ToRadio()
    replace t_IAVALPLA with this.oPgFrm.Page1.oPag.oIAVALPLA_2_14.ToRadio()
    replace t_DECTOP with this.w_DECTOP
    replace t_IAPLAINI with this.w_IAPLAINI
    replace t_DATACONCOMP with this.w_DATACONCOMP
    replace t_IALIMCOM with this.w_IALIMCOM
    replace t_CALCPIC with this.w_CALCPIC
    replace t_CALCPIP with this.w_CALCPIP
    replace t_SIMVAL with this.w_SIMVAL
    replace t_IAMINFAT with this.w_IAMINFAT
    replace t_IAMINCOR with this.w_IAMINCOR
    replace t_IAMINFCO with this.w_IAMINFCO
    replace t_IALORFCO with this.oPgFrm.Page1.oPag.oIALORFCO_2_27.ToRadio()
    if i_srv='A'
      replace IA__ANNO with this.w_IA__ANNO
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mdiPag1 as StdContainer
  Width  = 621
  height = 381
  stdWidth  = 621
  stdheight = 381
  resizeXpos=619
  resizeYpos=104
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=12, width=590,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="IA__ANNO",Label1="Anno",Field2="IAPEAIVA",Label2="% Acc.IVA",Field3="IADATVER",Label3="Versamento",Field4="IAVALIVA",Label4="Valuta",Field5="IAACCIVA",Label5="Acconto dicembre",Field6="IACODBAN",Label6="C/C versamento",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 32701318

  add object oStr_1_2 as StdString with uid="AKGMMFKFFN",Visible=.t., Left=64, Top=204,;
    Alignment=1, Width=98, Height=18,;
    Caption="IVA in Lire:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="KPPBHOGIQL",Visible=.t., Left=64, Top=178,;
    Alignment=1, Width=98, Height=18,;
    Caption="IVA in Euro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="NEHQUIRCXZ",Visible=.t., Left=309, Top=204,;
    Alignment=1, Width=99, Height=18,;
    Caption="Acconto in Lire:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="REBXDQFVMK",Visible=.t., Left=304, Top=178,;
    Alignment=1, Width=104, Height=18,;
    Caption="Acconto in Euro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="PRUALAALRW",Visible=.t., Left=10, Top=154,;
    Alignment=0, Width=139, Height=18,;
    Caption="Versamenti minimi"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="QXQAXPHPXG",Visible=.t., Left=147, Top=232,;
    Alignment=1, Width=73, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="ZWEZQVJAPD",Visible=.t., Left=318, Top=234,;
    Alignment=1, Width=90, Height=18,;
    Caption="Plafond iniziale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="SRZUXVTQVR",Visible=.t., Left=120, Top=130,;
    Alignment=1, Width=110, Height=18,;
    Caption="Descrizione C/C:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="MSUDMKGDFA",Visible=.t., Left=6, Top=299,;
    Alignment=1, Width=156, Height=18,;
    Caption="Importo minimo fatture:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="FDFMSXESCA",Visible=.t., Left=6, Top=327,;
    Alignment=1, Width=156, Height=18,;
    Caption="Importo minimo corrispettivi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="ELFQIEBXAF",Visible=.t., Left=6, Top=355,;
    Alignment=1, Width=156, Height=18,;
    Caption="Imp. min. fattura corrispettivi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="AMNIEHSGIY",Visible=.t., Left=10, Top=271,;
    Alignment=0, Width=278, Height=18,;
    Caption="Comunicazione operazioni superiori a 3.000 euro"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="IOZTQRGIPN",Visible=.t., Left=309, Top=264,;
    Alignment=1, Width=99, Height=18,;
    Caption="Data limite comp.:"  ;
  , bGlobalFont=.t.

  add object oBox_1_10 as StdBox with uid="UAXMDYLRBZ",left=10, top=172, width=601,height=2

  add object oBox_1_13 as StdBox with uid="NBPQNHRNEW",left=10, top=289, width=601,height=2

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=31,;
    width=586+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=32,width=585+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='VALUTE|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oIADESBAN_2_8.Refresh()
      this.Parent.oIAMINIVE_2_9.Refresh()
      this.Parent.oIAMINIVA_2_10.Refresh()
      this.Parent.oIAMACIVE_2_11.Refresh()
      this.Parent.oIAMACIVA_2_12.Refresh()
      this.Parent.oIAPLAMOB_2_13.Refresh()
      this.Parent.oIAVALPLA_2_14.Refresh()
      this.Parent.oIAPLAINI_2_16.Refresh()
      this.Parent.oIALIMCOM_2_18.Refresh()
      this.Parent.oIAMINFAT_2_24.Refresh()
      this.Parent.oIAMINCOR_2_25.Refresh()
      this.Parent.oIAMINFCO_2_26.Refresh()
      this.Parent.oIALORFCO_2_27.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='VALUTE'
        oDropInto=this.oBodyCol.oRow.oIAVALIVA_2_4
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oIADESBAN_2_8 as StdTrsField with uid="ROTYGAXLJV",rtseq=9,rtrep=.t.,;
    cFormVar="w_IADESBAN",value=space(50),;
    ToolTipText = "Descrizione banca di versamento",;
    HelpContextID = 45048020,;
    cTotal="", bFixedPos=.t., cQueryName = "IADESBAN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=235, Top=130, InputMask=replicate('X',50)

  add object oIAMINIVE_2_9 as StdTrsField with uid="VICDYYETWH",rtseq=10,rtrep=.t.,;
    cFormVar="w_IAMINIVE",value=0,;
    ToolTipText = "Importo minimo in Euro di versamento IVA",;
    HelpContextID = 110890805,;
    cTotal="", bFixedPos=.t., cQueryName = "IAMINIVE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=168, Top=178, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oIAMINIVA_2_10 as StdTrsField with uid="NOVUWSIAER",rtseq=11,rtrep=.t.,;
    cFormVar="w_IAMINIVA",value=0,;
    ToolTipText = "Importo minimo in Lire di versamento IVA",;
    HelpContextID = 110890809,;
    cTotal="", bFixedPos=.t., cQueryName = "IAMINIVA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=168, Top=204, cSayPict=[v_PV(40)], cGetPict=[v_GV(40)]

  add object oIAMACIVE_2_11 as StdTrsField with uid="DJYJHGRQWP",rtseq=12,rtrep=.t.,;
    cFormVar="w_IAMACIVE",value=0,;
    ToolTipText = "Importo minimo in Euro di acconto IVA",;
    HelpContextID = 122949429,;
    cTotal="", bFixedPos=.t., cQueryName = "IAMACIVE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=414, Top=178, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oIAMACIVA_2_12 as StdTrsField with uid="RTCBFBNZPT",rtseq=13,rtrep=.t.,;
    cFormVar="w_IAMACIVA",value=0,;
    ToolTipText = "Importo minimo in Lire di acconto IVA",;
    HelpContextID = 122949433,;
    cTotal="", bFixedPos=.t., cQueryName = "IAMACIVA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=414, Top=204, cSayPict=[v_PV(40)], cGetPict=[v_GV(40)]

  add object oIAPLAMOB_2_13 as StdTrsCheck with uid="LEJFPXHUDU",rtrep=.t.,;
    cFormVar="w_IAPLAMOB",  caption="Plafond mobile",;
    ToolTipText = "Se attivo: gestisce il plafond mobile",;
    HelpContextID = 211230920,;
    Left=14, Top=228,;
    cTotal="", cQueryName = "IAPLAMOB",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oIAPLAMOB_2_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IAPLAMOB,&i_cF..t_IAPLAMOB),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oIAPLAMOB_2_13.GetRadio()
    this.Parent.oContained.w_IAPLAMOB = this.RadioValue()
    return .t.
  endfunc

  func oIAPLAMOB_2_13.ToRadio()
    this.Parent.oContained.w_IAPLAMOB=trim(this.Parent.oContained.w_IAPLAMOB)
    return(;
      iif(this.Parent.oContained.w_IAPLAMOB=='S',1,;
      0))
  endfunc

  func oIAPLAMOB_2_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oIAVALPLA_2_14 as StdTrsCombo with uid="DADWADGDUZ",rtrep=.t.,;
    cFormVar="w_IAVALPLA", RowSource=""+"Euro,"+"Lire" , ;
    ToolTipText = "Valuta di riferimento per la gestione del plafond",;
    HelpContextID = 3965127,;
    Height=26, Width=77, Left=223, Top=231,;
    cTotal="", cQueryName = "IAVALPLA",;
    bObbl = .t. , nPag=2, cLinkFile="VALUTE", bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oIAVALPLA_2_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IAVALPLA,&i_cF..t_IAVALPLA),this.value)
    return(iif(xVal =1,ALLTRIM(g_CODEUR),;
    iif(xVal =2,ALLTRIM(g_CODLIR),;
    space(3))))
  endfunc
  func oIAVALPLA_2_14.GetRadio()
    this.Parent.oContained.w_IAVALPLA = this.RadioValue()
    return .t.
  endfunc

  func oIAVALPLA_2_14.ToRadio()
    this.Parent.oContained.w_IAVALPLA=trim(this.Parent.oContained.w_IAVALPLA)
    return(;
      iif(this.Parent.oContained.w_IAVALPLA==ALLTRIM(g_CODEUR),1,;
      iif(this.Parent.oContained.w_IAVALPLA==ALLTRIM(g_CODLIR),2,;
      0)))
  endfunc

  func oIAVALPLA_2_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oIAVALPLA_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oIAVALPLA_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  add object oIAPLAINI_2_16 as StdTrsField with uid="FVNYHZXRHD",rtseq=17,rtrep=.t.,;
    cFormVar="w_IAPLAINI",value=0,;
    ToolTipText = "Importo del plafond ad inizio anno",;
    HelpContextID = 144122063,;
    cTotal="", bFixedPos=.t., cQueryName = "IAPLAINI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=414, Top=230, cSayPict=[v_PV(40+(20*g_PERPVL))], cGetPict=[v_GV(40+(20*g_PERPVL))]

  func oIAPLAINI_2_16.mCond()
    with this.Parent.oContained
      return (.w_IAPLAMOB<>'S')
    endwith
  endfunc

  add object oIALIMCOM_2_18 as StdTrsField with uid="DBJBEZLGYL",rtseq=19,rtrep=.t.,;
    cFormVar="w_IALIMCOM",value=ctod("  /  /  "),;
    ToolTipText = "Rappresenta la data entro cui far concorrere in liquidazione dell'ultimo periodo dell'anno precedente le registrazioni con competenza dell'anno, ma registrate l'anno dopo. Pu� essere valorizzata per indicare una data diversa dal 15/01 dell'anno.",;
    HelpContextID = 55828691,;
    cTotal="", bFixedPos=.t., cQueryName = "IALIMCOM",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=414, Top=262

  func oIALIMCOM_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Empty(.w_IALIMCOM) Or .w_IALIMCOM>.w_DATACONCOMP)
    endwith
    return bRes
  endfunc

  add object oObj_2_21 as cp_runprogram with uid="RSITEHGONI",width=141,height=22,;
   left=623, top=184,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BBA('V')",;
    cEvent = "w_IACODBAN Changed",;
    nPag=2;
    , HelpContextID = 97640934

  add object oLinkPC_2_22 as StdButton with uid="TLMCPJMSPL",width=48,height=45,;
   left=566, top=208,;
    CpPicture="bmp\costi.bmp", caption="", nPag=2;
    , HelpContextID = 258843658;
    , Caption='\<Plafond';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_22.Click()
      this.Parent.oContained.GSCG_MPF.LinkPCClick()
    endproc

  add object oIAMINFAT_2_24 as StdTrsField with uid="NNKAADRTSS",rtseq=24,rtrep=.t.,;
    cFormVar="w_IAMINFAT",value=0,;
    ToolTipText = "Importo minimo da controllare per l'estrazione delle fatture",;
    HelpContextID = 107213018,;
    cTotal="", bFixedPos=.t., cQueryName = "IAMINFAT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=24, Width=132, Left=168, Top=295, cSayPict=[v_pv(80)], cGetPict=[v_gv(80)]

  add object oIAMINCOR_2_25 as StdTrsField with uid="FEUHYHCCNT",rtseq=25,rtrep=.t.,;
    cFormVar="w_IAMINCOR",value=0,;
    ToolTipText = "Importo minimo da controllare per l'estrazione dei corrispettivi",;
    HelpContextID = 56881368,;
    cTotal="", bFixedPos=.t., cQueryName = "IAMINCOR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=24, Width=132, Left=168, Top=323, cSayPict=[v_pv(80)], cGetPict=[v_gv(80)]

  add object oIAMINFCO_2_26 as StdTrsField with uid="TZJODRKQTJ",rtseq=26,rtrep=.t.,;
    cFormVar="w_IAMINFCO",value=0,;
    ToolTipText = "Importo minimo da controllare per l'estrazione delle fatture comprese nei corrispettivi",;
    HelpContextID = 107213013,;
    cTotal="", bFixedPos=.t., cQueryName = "IAMINFCO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=24, Width=132, Left=168, Top=351, cSayPict=[v_pv(80)], cGetPict=[v_gv(80)]

  add object oIALORFCO_2_27 as StdTrsCheck with uid="TGWVHYGIOP",rtrep=.t.,;
    cFormVar="w_IALORFCO",  caption="Lordo iva",;
    ToolTipText = "Se attivo, il minimo importo delle fatture comprese nei corrispettivi � imponibile pi� imposta",;
    HelpContextID = 111796437,;
    Left=325, Top=352,;
    cTotal="", cQueryName = "IALORFCO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oIALORFCO_2_27.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IALORFCO,&i_cF..t_IALORFCO),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oIALORFCO_2_27.GetRadio()
    this.Parent.oContained.w_IALORFCO = this.RadioValue()
    return .t.
  endfunc

  func oIALORFCO_2_27.ToRadio()
    this.Parent.oContained.w_IALORFCO=trim(this.Parent.oContained.w_IALORFCO)
    return(;
      iif(this.Parent.oContained.w_IALORFCO=='S',1,;
      0))
  endfunc

  func oIALORFCO_2_27.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_mdiBodyRow as CPBodyRowCnt
  Width=576
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oIA__ANNO_2_1 as StdTrsField with uid="FMOHSGVTMT",rtseq=2,rtrep=.t.,;
    cFormVar="w_IA__ANNO",value=space(4),nZero=4,isprimarykey=.t.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 229314773,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=['9999'], cGetPict=['9999'], InputMask=replicate('X',4)

  add object oIAPEAIVA_2_2 as StdTrsField with uid="RQSYJOEYVE",rtseq=3,rtrep=.t.,;
    cFormVar="w_IAPEAIVA",value=0,;
    ToolTipText = "Percentuale di acconto IVA",;
    HelpContextID = 124772153,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=46, Top=0, cSayPict=["99.99"], cGetPict=["99.99"]

  add object oIADATVER_2_3 as StdTrsField with uid="XGEQVWIHZG",rtseq=4,rtrep=.t.,;
    cFormVar="w_IADATVER",value=ctod("  /  /  "),;
    ToolTipText = "Data versamento acconto IVA",;
    HelpContextID = 155492136,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=121, Top=0

  add object oIAVALIVA_2_4 as StdTrsCombo with uid="RSYAACTHEX",rtrep=.t.,;
    cFormVar="w_IAVALIVA", RowSource=""+"Euro,"+"Lire" , ;
    ToolTipText = "Valuta di riferimento per la gestione dell'acconto IVA",;
    HelpContextID = 113475385,;
    Height=22, Width=77, Left=211, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag=2, cLinkFile="VALUTE", bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oIAVALIVA_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..IAVALIVA,&i_cF..t_IAVALIVA),this.value)
    return(iif(xVal =1,ALLTRIM(g_CODEUR),;
    iif(xVal =2,ALLTRIM(g_CODLIR),;
    space(3))))
  endfunc
  func oIAVALIVA_2_4.GetRadio()
    this.Parent.oContained.w_IAVALIVA = this.RadioValue()
    return .t.
  endfunc

  func oIAVALIVA_2_4.ToRadio()
    this.Parent.oContained.w_IAVALIVA=trim(this.Parent.oContained.w_IAVALIVA)
    return(;
      iif(this.Parent.oContained.w_IAVALIVA==ALLTRIM(g_CODEUR),1,;
      iif(this.Parent.oContained.w_IAVALIVA==ALLTRIM(g_CODLIR),2,;
      0)))
  endfunc

  func oIAVALIVA_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oIAVALIVA_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oIAVALIVA_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  add object oIAACCIVA_2_6 as StdTrsField with uid="AXUALHZECZ",rtseq=7,rtrep=.t.,;
    cFormVar="w_IAACCIVA",value=0,;
    ToolTipText = "Importo acconto IVA",;
    HelpContextID = 122867513,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=295, Top=0, cSayPict=[v_PV(40+(20*g_PERPVL))], cGetPict=[v_GV(40+(20*g_PERPVL))]

  add object oIACODBAN_2_7 as StdTrsField with uid="AHRLATFNUR",rtseq=8,rtrep=.t.,;
    cFormVar="w_IACODBAN",value=space(15),;
    ToolTipText = "Conto corrente del versamento acconto",;
    HelpContextID = 29970644,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=439, Top=0, InputMask=replicate('X',15), bHasZoom = .t. 

  proc oIACODBAN_2_7.mZoom
      with this.Parent.oContained
        GSCG_BBA(this.Parent.oContained,"C")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oIA__ANNO_2_1.When()
    return(.t.)
  proc oIA__ANNO_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oIA__ANNO_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=4
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mdi','DAT_IVAN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".IACODAZI=DAT_IVAN.IACODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
