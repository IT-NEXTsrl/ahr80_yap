* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bpw                                                        *
*              WE - gestione parametri                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_23]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-10                                                      *
* Last revis.: 2007-09-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Azione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bpw",oParentObject,m.w_Azione)
return(i_retval)

define class tgsut_bpw as StdBatch
  * --- Local variables
  w_Azione = space(10)
  w_CODAZI = space(5)
  w_cParms = space(10)
  w_RISULTATO = space(10)
  w_NOMEFILE = space(75)
  w_TIPOFILE = space(75)
  w_ChiaveRicerca = space(20)
  w_posizpunto = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione parametri WE (richiamato da GSUT_KPW)
    * --- Dichiarazione Variabili 
    * --- Esegue Azioni
    this.w_CODAZI = i_CODAZI
    do case
      case this.w_AZIONE="LOAD"
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZ_DOMWE,AZ_LOGWE,AZ_PASWE,AZ_EMLDE,AZ_EMLSM,AZ_INDWE,AZNOMDSC,AZCDESWE,AZWEENAB"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZ_DOMWE,AZ_LOGWE,AZ_PASWE,AZ_EMLDE,AZ_EMLSM,AZ_INDWE,AZNOMDSC,AZCDESWE,AZWEENAB;
            from (i_cTable) where;
                AZCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_AZ_DOMWE = NVL(cp_ToDate(_read_.AZ_DOMWE),cp_NullValue(_read_.AZ_DOMWE))
          this.oParentObject.w_AZ_LOGWE = NVL(cp_ToDate(_read_.AZ_LOGWE),cp_NullValue(_read_.AZ_LOGWE))
          this.oParentObject.w_AZ_PASWE = NVL(cp_ToDate(_read_.AZ_PASWE),cp_NullValue(_read_.AZ_PASWE))
          this.oParentObject.w_AZ_EMLDE = NVL(cp_ToDate(_read_.AZ_EMLDE),cp_NullValue(_read_.AZ_EMLDE))
          this.oParentObject.w_AZ_EMLSM = NVL(cp_ToDate(_read_.AZ_EMLSM),cp_NullValue(_read_.AZ_EMLSM))
          this.oParentObject.w_AZ_INDWE = NVL(cp_ToDate(_read_.AZ_INDWE),cp_NullValue(_read_.AZ_INDWE))
          this.oParentObject.w_AZNOMDSC = NVL(cp_ToDate(_read_.AZNOMDSC),cp_NullValue(_read_.AZNOMDSC))
          this.oParentObject.w_AZCDESWE = NVL(cp_ToDate(_read_.AZCDESWE),cp_NullValue(_read_.AZCDESWE))
          this.oParentObject.w_AZWEENAB = NVL(cp_ToDate(_read_.AZWEENAB),cp_NullValue(_read_.AZWEENAB))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_AZNOMDSC = IIF(EMPTY(this.oParentObject.w_AZNOMDSC),PADR("WWPACTION.XML",35),this.oParentObject.w_AZNOMDSC)
        this.oParentObject.w_AZCDESWE = IIF(EMPTY(this.oParentObject.w_AZCDESWE), "N", this.oParentObject.w_AZCDESWE)
        this.oParentObject.w_AZWEENAB = IIF(EMPTY(this.oParentObject.w_AZWEENAB), "N", this.oParentObject.w_AZWEENAB)
      case this.w_AZIONE="SAVE"
        * --- Write into AZIENDA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AZ_DOMWE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AZ_DOMWE),'AZIENDA','AZ_DOMWE');
          +",AZ_LOGWE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AZ_LOGWE),'AZIENDA','AZ_LOGWE');
          +",AZ_PASWE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AZ_PASWE),'AZIENDA','AZ_PASWE');
          +",AZ_EMLDE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AZ_EMLDE),'AZIENDA','AZ_EMLDE');
          +",AZ_EMLSM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AZ_EMLSM),'AZIENDA','AZ_EMLSM');
          +",AZ_INDWE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AZ_INDWE),'AZIENDA','AZ_INDWE');
          +",AZNOMDSC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AZNOMDSC),'AZIENDA','AZNOMDSC');
          +",AZCDESWE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AZCDESWE),'AZIENDA','AZCDESWE');
          +",AZWEENAB ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_AZWEENAB),'AZIENDA','AZWEENAB');
              +i_ccchkf ;
          +" where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              AZ_DOMWE = this.oParentObject.w_AZ_DOMWE;
              ,AZ_LOGWE = this.oParentObject.w_AZ_LOGWE;
              ,AZ_PASWE = this.oParentObject.w_AZ_PASWE;
              ,AZ_EMLDE = this.oParentObject.w_AZ_EMLDE;
              ,AZ_EMLSM = this.oParentObject.w_AZ_EMLSM;
              ,AZ_INDWE = this.oParentObject.w_AZ_INDWE;
              ,AZNOMDSC = this.oParentObject.w_AZNOMDSC;
              ,AZCDESWE = this.oParentObject.w_AZCDESWE;
              ,AZWEENAB = this.oParentObject.w_AZWEENAB;
              &i_ccchkf. ;
           where;
              AZCODAZI = this.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        g_WEENABLED = this.oParentObject.w_AZWEENAB
      case this.w_AZIONE="EXECUTE"
        * --- Si acquisisce l'estensione di ogni file
        this.w_NOMEFILE = this.oParentObject.w_AZ_INDWE
        this.w_posizpunto = rat(".",this.w_NOMEFILE)
        this.w_ChiaveRicerca = alltrim(lower(substr(this.w_NOMEFILE,this.w_posizpunto)))
        this.w_TIPOFILE = GETEXTDESC(this.w_ChiaveRicerca)
        * --- Lancia eseguibile associato al file
        this.w_cParms = space(1)
        declare integer ShellExecute in shell32.dll integer nHwnd, string cOperation, string cFileName, string cParms, string cDir, integer nShowCmd
         declare integer FindWindow in win32api string cNull, string cWinName 
        w_hWnd = FindWindow(0,_screen.caption)
        this.w_RISULTATO = (ShellExecute(w_hWnd,"open",alltrim(this.w_NOMEFILE),alltrim(this.w_cParms),sys(5)+curdir(),1))
    endcase
  endproc


  proc Init(oParentObject,w_Azione)
    this.w_Azione=w_Azione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Azione"
endproc
