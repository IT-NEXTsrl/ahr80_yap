* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mst                                                        *
*              Manutenzione storico                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_26]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-11                                                      *
* Last revis.: 2013-03-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_mst"))

* --- Class definition
define class tgscg_mst as StdTrsForm
  Top    = 13
  Left   = 14

  * --- Standard Properties
  Width  = 703
  Height = 305+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-03-21"
  HelpContextID=97133929
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=30

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  STO_MAST_IDX = 0
  STO_DETT_IDX = 0
  STORCOST_IDX = 0
  CONTI_IDX = 0
  CAU_CONT_IDX = 0
  PAG_AMEN_IDX = 0
  VALUTE_IDX = 0
  ESERCIZI_IDX = 0
  cFile = "STO_MAST"
  cFileDetail = "STO_DETT"
  cKeySelect = "STSERIAL"
  cKeyWhere  = "STSERIAL=this.w_STSERIAL"
  cKeyDetail  = "STSERIAL=this.w_STSERIAL"
  cKeyWhereODBC = '"STSERIAL="+cp_ToStrODBC(this.w_STSERIAL)';

  cKeyDetailWhereODBC = '"STSERIAL="+cp_ToStrODBC(this.w_STSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"STO_DETT.STSERIAL="+cp_ToStrODBC(this.w_STSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'STO_DETT.CPROWORD '
  cPrg = "gscg_mst"
  cComment = "Manutenzione storico"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_STSERIAL = space(10)
  o_STSERIAL = space(10)
  w_CODAZI = space(5)
  w_STNUMREG = 0
  w_STCODESE = space(4)
  w_STDATREG = ctod('  /  /  ')
  w_STCODUTE = 0
  w_STCODCAU = space(5)
  w_STNUMDOC = 0
  w_STDATDOC = ctod('  /  /  ')
  w_STCODVAL = space(3)
  o_STCODVAL = space(3)
  w_CAOVAL = 0
  w_STCOMPET = space(4)
  w_STNUMPRO = 0
  w_STALFPRO = space(2)
  w_STCAOVAL = 0
  w_STDESSUP = space(50)
  w_DESAPP = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_DECTOT = 0
  w_CALCPICT = 0
  w_SIMVAL = space(5)
  w_CPROWORD = 0
  w_STTIPCON = space(1)
  o_STTIPCON = space(1)
  w_STCODCON = space(15)
  o_STCODCON = space(15)
  w_STIMPDAR = 0
  w_STIMPAVE = 0
  w_STCAURIG = space(5)
  w_STCODPAG = space(5)
  w_MASTRO = space(15)
  w_SEZB = space(1)

  * --- Children pointers
  GSCG_MCS = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'STO_MAST','gscg_mst')
    stdPageFrame::Init()
    *set procedure to GSCG_MCS additive
    with this
      .Pages(1).addobject("oPag","tgscg_mstPag1","gscg_mst",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Manutenzione storico")
      .Pages(1).HelpContextID = 53209249
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCG_MCS
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='STORCOST'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='CAU_CONT'
    this.cWorkTables[4]='PAG_AMEN'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='ESERCIZI'
    this.cWorkTables[7]='STO_MAST'
    this.cWorkTables[8]='STO_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.STO_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.STO_MAST_IDX,3]
  return

  function CreateChildren()
    this.GSCG_MCS = CREATEOBJECT('stdLazyChild',this,'GSCG_MCS')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCG_MCS)
      this.GSCG_MCS.DestroyChildrenChain()
      this.GSCG_MCS=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_MCS.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_MCS.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_MCS.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSCG_MCS.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_STSERIAL,"MRSERIAL";
             ,.w_CPROWNUM,"MRROWORD";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_STSERIAL = NVL(STSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from STO_MAST where STSERIAL=KeySet.STSERIAL
    *
    i_nConn = i_TableProp[this.STO_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STO_MAST_IDX,2],this.bLoadRecFilter,this.STO_MAST_IDX,"gscg_mst")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('STO_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "STO_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"STO_DETT.","STO_MAST.")
      i_cTable = i_cTable+' STO_MAST '
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'STSERIAL',this.w_STSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CODAZI = i_codazi
        .w_CAOVAL = 0
        .w_DESAPP = space(35)
        .w_OBTEST = i_INIDAT
        .w_DECTOT = 0
        .w_SIMVAL = space(5)
        .w_STSERIAL = NVL(STSERIAL,space(10))
        .w_STNUMREG = NVL(STNUMREG,0)
        .w_STCODESE = NVL(STCODESE,space(4))
          * evitabile
          *.link_1_4('Load')
        .w_STDATREG = NVL(cp_ToDate(STDATREG),ctod("  /  /  "))
        .w_STCODUTE = NVL(STCODUTE,0)
        .w_STCODCAU = NVL(STCODCAU,space(5))
          * evitabile
          *.link_1_7('Load')
        .w_STNUMDOC = NVL(STNUMDOC,0)
        .w_STDATDOC = NVL(cp_ToDate(STDATDOC),ctod("  /  /  "))
        .w_STCODVAL = NVL(STCODVAL,space(3))
          if link_1_10_joined
            this.w_STCODVAL = NVL(VACODVAL110,NVL(this.w_STCODVAL,space(3)))
            this.w_DESAPP = NVL(VADESVAL110,space(35))
            this.w_STCAOVAL = NVL(VACAOVAL110,0)
            this.w_CAOVAL = NVL(VACAOVAL110,0)
            this.w_SIMVAL = NVL(VASIMVAL110,space(5))
            this.w_DECTOT = NVL(VADECTOT110,0)
          else
          .link_1_10('Load')
          endif
        .w_STCOMPET = NVL(STCOMPET,space(4))
          * evitabile
          *.link_1_12('Load')
        .w_STNUMPRO = NVL(STNUMPRO,0)
        .w_STALFPRO = NVL(STALFPRO,space(2))
        .w_STCAOVAL = NVL(STCAOVAL,0)
        .w_STDESSUP = NVL(STDESSUP,space(50))
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'STO_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from STO_DETT where STSERIAL=KeySet.STSERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.STO_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STO_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('STO_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "STO_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" STO_DETT"
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'STSERIAL',this.w_STSERIAL  )
        select * from (i_cTable) STO_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_MASTRO = space(15)
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_STTIPCON = NVL(STTIPCON,space(1))
          .w_STCODCON = NVL(STCODCON,space(15))
          if link_2_3_joined
            this.w_STCODCON = NVL(ANCODICE203,NVL(this.w_STCODCON,space(15)))
            this.w_DESAPP = NVL(ANDESCRI203,space(35))
            this.w_MASTRO = NVL(ANCONSUP203,space(15))
          else
          .link_2_3('Load')
          endif
          .w_STIMPDAR = NVL(STIMPDAR,0)
          .w_STIMPAVE = NVL(STIMPAVE,0)
          .w_STCAURIG = NVL(STCAURIG,space(5))
          * evitabile
          *.link_2_6('Load')
          .w_STCODPAG = NVL(STCODPAG,space(5))
          if link_2_7_joined
            this.w_STCODPAG = NVL(PACODICE207,NVL(this.w_STCODPAG,space(5)))
            this.w_DESAPP = NVL(PADESCRI207,space(35))
          else
          .link_2_7('Load')
          endif
        .w_SEZB = IIF(.w_STTIPCON="G", CALCSEZ(.w_MASTRO), " ")
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_STSERIAL=space(10)
      .w_CODAZI=space(5)
      .w_STNUMREG=0
      .w_STCODESE=space(4)
      .w_STDATREG=ctod("  /  /  ")
      .w_STCODUTE=0
      .w_STCODCAU=space(5)
      .w_STNUMDOC=0
      .w_STDATDOC=ctod("  /  /  ")
      .w_STCODVAL=space(3)
      .w_CAOVAL=0
      .w_STCOMPET=space(4)
      .w_STNUMPRO=0
      .w_STALFPRO=space(2)
      .w_STCAOVAL=0
      .w_STDESSUP=space(50)
      .w_DESAPP=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_SIMVAL=space(5)
      .w_CPROWORD=10
      .w_STTIPCON=space(1)
      .w_STCODCON=space(15)
      .w_STIMPDAR=0
      .w_STIMPAVE=0
      .w_STCAURIG=space(5)
      .w_STCODPAG=space(5)
      .w_MASTRO=space(15)
      .w_SEZB=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CODAZI = i_codazi
        .DoRTCalc(3,3,.f.)
        .w_STCODESE = g_CODESE
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_STCODESE))
         .link_1_4('Full')
        endif
        .w_STDATREG = i_datsys
        .w_STCODUTE = i_codute
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_STCODCAU))
         .link_1_7('Full')
        endif
        .DoRTCalc(8,9,.f.)
        .w_STCODVAL = g_PERVAL
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_STCODVAL))
         .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        .w_STCOMPET = g_CODESE
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_STCOMPET))
         .link_1_12('Full')
        endif
        .DoRTCalc(13,17,.f.)
        .w_OBTEST = i_INIDAT
        .DoRTCalc(19,19,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .DoRTCalc(21,24,.f.)
        if not(empty(.w_STCODCON))
         .link_2_3('Full')
        endif
        .DoRTCalc(25,27,.f.)
        if not(empty(.w_STCAURIG))
         .link_2_6('Full')
        endif
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_STCODPAG))
         .link_2_7('Full')
        endif
        .DoRTCalc(29,29,.f.)
        .w_SEZB = IIF(.w_STTIPCON="G", CALCSEZ(.w_MASTRO), " ")
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'STO_MAST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSTNUMREG_1_3.enabled = i_bVal
      .Page1.oPag.oSTCODESE_1_4.enabled = i_bVal
      .Page1.oPag.oSTDATREG_1_5.enabled = i_bVal
      .Page1.oPag.oSTCODCAU_1_7.enabled = i_bVal
      .Page1.oPag.oSTNUMDOC_1_8.enabled = i_bVal
      .Page1.oPag.oSTDATDOC_1_9.enabled = i_bVal
      .Page1.oPag.oSTCODVAL_1_10.enabled = i_bVal
      .Page1.oPag.oSTCOMPET_1_12.enabled = i_bVal
      .Page1.oPag.oSTNUMPRO_1_13.enabled = i_bVal
      .Page1.oPag.oSTALFPRO_1_14.enabled = i_bVal
      .Page1.oPag.oSTCAOVAL_1_15.enabled = i_bVal
      .Page1.oPag.oSTDESSUP_1_16.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oSTDATREG_1_5.enabled = .t.
        .Page1.oPag.oSTCODCAU_1_7.enabled = .t.
      endif
    endwith
    this.GSCG_MCS.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'STO_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_MCS.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.STO_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STSERIAL,"STSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STNUMREG,"STNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STCODESE,"STCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STDATREG,"STDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STCODUTE,"STCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STCODCAU,"STCODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STNUMDOC,"STNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STDATDOC,"STDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STCODVAL,"STCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STCOMPET,"STCOMPET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STNUMPRO,"STNUMPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STALFPRO,"STALFPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STCAOVAL,"STCAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_STDESSUP,"STDESSUP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.STO_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STO_MAST_IDX,2])
    i_lTable = "STO_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.STO_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_STTIPCON C(1);
      ,t_STCODCON C(15);
      ,t_STIMPDAR N(18,4);
      ,t_STIMPAVE N(18,4);
      ,t_STCAURIG C(5);
      ,t_STCODPAG C(5);
      ,CPROWNUM N(10);
      ,t_MASTRO C(15);
      ,t_SEZB C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mstbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTTIPCON_2_2.controlsource=this.cTrsName+'.t_STTIPCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTCODCON_2_3.controlsource=this.cTrsName+'.t_STCODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPDAR_2_4.controlsource=this.cTrsName+'.t_STIMPDAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPAVE_2_5.controlsource=this.cTrsName+'.t_STIMPAVE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTCAURIG_2_6.controlsource=this.cTrsName+'.t_STCAURIG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSTCODPAG_2_7.controlsource=this.cTrsName+'.t_STCODPAG'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(49)
    this.AddVLine(70)
    this.AddVLine(203)
    this.AddVLine(336)
    this.AddVLine(469)
    this.AddVLine(537)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.STO_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STO_MAST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into STO_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'STO_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'STO_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(STSERIAL,STNUMREG,STCODESE,STDATREG,STCODUTE"+;
                  ",STCODCAU,STNUMDOC,STDATDOC,STCODVAL,STCOMPET"+;
                  ",STNUMPRO,STALFPRO,STCAOVAL,STDESSUP"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_STSERIAL)+;
                    ","+cp_ToStrODBC(this.w_STNUMREG)+;
                    ","+cp_ToStrODBCNull(this.w_STCODESE)+;
                    ","+cp_ToStrODBC(this.w_STDATREG)+;
                    ","+cp_ToStrODBC(this.w_STCODUTE)+;
                    ","+cp_ToStrODBCNull(this.w_STCODCAU)+;
                    ","+cp_ToStrODBC(this.w_STNUMDOC)+;
                    ","+cp_ToStrODBC(this.w_STDATDOC)+;
                    ","+cp_ToStrODBCNull(this.w_STCODVAL)+;
                    ","+cp_ToStrODBCNull(this.w_STCOMPET)+;
                    ","+cp_ToStrODBC(this.w_STNUMPRO)+;
                    ","+cp_ToStrODBC(this.w_STALFPRO)+;
                    ","+cp_ToStrODBC(this.w_STCAOVAL)+;
                    ","+cp_ToStrODBC(this.w_STDESSUP)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'STO_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'STO_MAST')
        cp_CheckDeletedKey(i_cTable,0,'STSERIAL',this.w_STSERIAL)
        INSERT INTO (i_cTable);
              (STSERIAL,STNUMREG,STCODESE,STDATREG,STCODUTE,STCODCAU,STNUMDOC,STDATDOC,STCODVAL,STCOMPET,STNUMPRO,STALFPRO,STCAOVAL,STDESSUP &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_STSERIAL;
                  ,this.w_STNUMREG;
                  ,this.w_STCODESE;
                  ,this.w_STDATREG;
                  ,this.w_STCODUTE;
                  ,this.w_STCODCAU;
                  ,this.w_STNUMDOC;
                  ,this.w_STDATDOC;
                  ,this.w_STCODVAL;
                  ,this.w_STCOMPET;
                  ,this.w_STNUMPRO;
                  ,this.w_STALFPRO;
                  ,this.w_STCAOVAL;
                  ,this.w_STDESSUP;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.STO_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STO_DETT_IDX,2])
      *
      * insert into STO_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(STSERIAL,CPROWORD,STTIPCON,STCODCON,STIMPDAR"+;
                  ",STIMPAVE,STCAURIG,STCODPAG,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_STSERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_STTIPCON)+","+cp_ToStrODBCNull(this.w_STCODCON)+","+cp_ToStrODBC(this.w_STIMPDAR)+;
             ","+cp_ToStrODBC(this.w_STIMPAVE)+","+cp_ToStrODBCNull(this.w_STCAURIG)+","+cp_ToStrODBCNull(this.w_STCODPAG)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'STSERIAL',this.w_STSERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_STSERIAL,this.w_CPROWORD,this.w_STTIPCON,this.w_STCODCON,this.w_STIMPDAR"+;
                ",this.w_STIMPAVE,this.w_STCAURIG,this.w_STCODPAG,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.STO_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STO_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update STO_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'STO_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " STNUMREG="+cp_ToStrODBC(this.w_STNUMREG)+;
             ",STCODESE="+cp_ToStrODBCNull(this.w_STCODESE)+;
             ",STDATREG="+cp_ToStrODBC(this.w_STDATREG)+;
             ",STCODUTE="+cp_ToStrODBC(this.w_STCODUTE)+;
             ",STCODCAU="+cp_ToStrODBCNull(this.w_STCODCAU)+;
             ",STNUMDOC="+cp_ToStrODBC(this.w_STNUMDOC)+;
             ",STDATDOC="+cp_ToStrODBC(this.w_STDATDOC)+;
             ",STCODVAL="+cp_ToStrODBCNull(this.w_STCODVAL)+;
             ",STCOMPET="+cp_ToStrODBCNull(this.w_STCOMPET)+;
             ",STNUMPRO="+cp_ToStrODBC(this.w_STNUMPRO)+;
             ",STALFPRO="+cp_ToStrODBC(this.w_STALFPRO)+;
             ",STCAOVAL="+cp_ToStrODBC(this.w_STCAOVAL)+;
             ",STDESSUP="+cp_ToStrODBC(this.w_STDESSUP)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'STO_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'STSERIAL',this.w_STSERIAL  )
          UPDATE (i_cTable) SET;
              STNUMREG=this.w_STNUMREG;
             ,STCODESE=this.w_STCODESE;
             ,STDATREG=this.w_STDATREG;
             ,STCODUTE=this.w_STCODUTE;
             ,STCODCAU=this.w_STCODCAU;
             ,STNUMDOC=this.w_STNUMDOC;
             ,STDATDOC=this.w_STDATDOC;
             ,STCODVAL=this.w_STCODVAL;
             ,STCOMPET=this.w_STCOMPET;
             ,STNUMPRO=this.w_STNUMPRO;
             ,STALFPRO=this.w_STALFPRO;
             ,STCAOVAL=this.w_STCAOVAL;
             ,STDESSUP=this.w_STDESSUP;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_STCODCON)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.STO_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.STO_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Deleting row children
              this.GSCG_MCS.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_STSERIAL,"MRSERIAL";
                     ,this.w_CPROWNUM,"MRROWORD";
                     )
              this.GSCG_MCS.mDelete()
              *
              * delete from STO_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update STO_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",STTIPCON="+cp_ToStrODBC(this.w_STTIPCON)+;
                     ",STCODCON="+cp_ToStrODBCNull(this.w_STCODCON)+;
                     ",STIMPDAR="+cp_ToStrODBC(this.w_STIMPDAR)+;
                     ",STIMPAVE="+cp_ToStrODBC(this.w_STIMPAVE)+;
                     ",STCAURIG="+cp_ToStrODBCNull(this.w_STCAURIG)+;
                     ",STCODPAG="+cp_ToStrODBCNull(this.w_STCODPAG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,STTIPCON=this.w_STTIPCON;
                     ,STCODCON=this.w_STCODCON;
                     ,STIMPDAR=this.w_STIMPDAR;
                     ,STIMPAVE=this.w_STIMPAVE;
                     ,STCAURIG=this.w_STCAURIG;
                     ,STCODPAG=this.w_STCODPAG;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask children belonging to rows to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (NOT EMPTY(t_STCODCON))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        this.GSCG_MCS.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_STSERIAL,"MRSERIAL";
             ,this.w_CPROWNUM,"MRROWORD";
             )
        this.GSCG_MCS.mReplace()
        this.GSCG_MCS.bSaveContext=.f.
      endscan
     this.GSCG_MCS.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)2
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_STCODCON)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSCG_MCS : Deleting
        this.GSCG_MCS.bSaveContext=.f.
        this.GSCG_MCS.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_STSERIAL,"MRSERIAL";
               ,this.w_CPROWNUM,"MRROWORD";
               )
        this.GSCG_MCS.bSaveContext=.t.
        this.GSCG_MCS.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.STO_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.STO_DETT_IDX,2])
        *
        * delete STO_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.STO_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.STO_MAST_IDX,2])
        *
        * delete STO_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_STCODCON)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.STO_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STO_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,19,.t.)
        if .o_STCODVAL<>.w_STCODVAL.or. .o_STSERIAL<>.w_STSERIAL
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(21,29,.t.)
        if .o_STTIPCON<>.w_STTIPCON.or. .o_STCODCON<>.w_STCODCON
          .w_SEZB = IIF(.w_STTIPCON="G", CALCSEZ(.w_MASTRO), " ")
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_MASTRO with this.w_MASTRO
      replace t_SEZB with this.w_SEZB
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSTCAOVAL_1_15.enabled = this.oPgFrm.Page1.oPag.oSTCAOVAL_1_15.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oLinkPC_2_8.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_8.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSCG_MCS.visible")=='L' And this.GSCG_MCS.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_8.enabled
      this.GSCG_MCS.HideChildrenChain()
    endif 
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=STCODESE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_STCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_STCODESE))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oSTCODESE_1_4'),i_cWhere,'GSAR_KES',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_STCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_STCODESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STCODESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_STCODESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STCODCAU
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STCODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_STCODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_STCODCAU))
          select CCCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STCODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STCODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oSTCODCAU_1_7'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STCODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_STCODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_STCODCAU)
            select CCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STCODCAU = NVL(_Link_.CCCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_STCODCAU = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STCODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STCODVAL
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_STCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_STCODVAL))
          select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_STCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_STCODVAL)+"%");

            select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_STCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oSTCODVAL_1_10'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_STCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_STCODVAL)
            select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESAPP = NVL(_Link_.VADESVAL,space(35))
      this.w_STCAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_STCODVAL = space(3)
      endif
      this.w_DESAPP = space(35)
      this.w_STCAOVAL = 0
      this.w_CAOVAL = 0
      this.w_SIMVAL = space(5)
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.VACODVAL as VACODVAL110"+ ",link_1_10.VADESVAL as VADESVAL110"+ ",link_1_10.VACAOVAL as VACAOVAL110"+ ",link_1_10.VACAOVAL as VACAOVAL110"+ ",link_1_10.VASIMVAL as VASIMVAL110"+ ",link_1_10.VADECTOT as VADECTOT110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on STO_MAST.STCODVAL=link_1_10.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and STO_MAST.STCODVAL=link_1_10.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=STCOMPET
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STCOMPET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_STCOMPET)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_STCOMPET))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STCOMPET)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STCOMPET) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oSTCOMPET_1_12'),i_cWhere,'GSAR_KES',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STCOMPET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_STCOMPET);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_STCOMPET)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STCOMPET = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_STCOMPET = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STCOMPET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STCODCON
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_STCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_STTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_STTIPCON;
                     ,'ANCODICE',trim(this.w_STCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_STCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_STTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_STCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_STTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_STCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSTCODCON_2_3'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_STTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_STTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_STCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_STTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_STTIPCON;
                       ,'ANCODICE',this.w_STCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESAPP = NVL(_Link_.ANDESCRI,space(35))
      this.w_MASTRO = NVL(_Link_.ANCONSUP,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_STCODCON = space(15)
      endif
      this.w_DESAPP = space(35)
      this.w_MASTRO = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.ANCODICE as ANCODICE203"+ ",link_2_3.ANDESCRI as ANDESCRI203"+ ",link_2_3.ANCONSUP as ANCONSUP203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on STO_DETT.STCODCON=link_2_3.ANCODICE"+" and STO_DETT.STTIPCON=link_2_3.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and STO_DETT.STCODCON=link_2_3.ANCODICE(+)"'+'+" and STO_DETT.STTIPCON=link_2_3.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=STCAURIG
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STCAURIG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_STCAURIG)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_STCAURIG))
          select CCCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STCAURIG)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_STCAURIG) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oSTCAURIG_2_6'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STCAURIG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_STCAURIG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_STCAURIG)
            select CCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STCAURIG = NVL(_Link_.CCCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_STCAURIG = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STCAURIG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=STCODPAG
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_STCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_STCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_STCODPAG))
          select PACODICE,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_STCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_STCODPAG)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_STCODPAG))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_STCODPAG)+"%");

            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_STCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oSTCODPAG_2_7'),i_cWhere,'GSAR_APA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_STCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_STCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_STCODPAG)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_STCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESAPP = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_STCODPAG = space(5)
      endif
      this.w_DESAPP = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_STCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.PACODICE as PACODICE207"+ ","+cp_TransLinkFldName('link_2_7.PADESCRI')+" as PADESCRI207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on STO_DETT.STCODPAG=link_2_7.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and STO_DETT.STCODPAG=link_2_7.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oSTNUMREG_1_3.value==this.w_STNUMREG)
      this.oPgFrm.Page1.oPag.oSTNUMREG_1_3.value=this.w_STNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oSTCODESE_1_4.value==this.w_STCODESE)
      this.oPgFrm.Page1.oPag.oSTCODESE_1_4.value=this.w_STCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oSTDATREG_1_5.value==this.w_STDATREG)
      this.oPgFrm.Page1.oPag.oSTDATREG_1_5.value=this.w_STDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oSTCODUTE_1_6.value==this.w_STCODUTE)
      this.oPgFrm.Page1.oPag.oSTCODUTE_1_6.value=this.w_STCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oSTCODCAU_1_7.value==this.w_STCODCAU)
      this.oPgFrm.Page1.oPag.oSTCODCAU_1_7.value=this.w_STCODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oSTNUMDOC_1_8.value==this.w_STNUMDOC)
      this.oPgFrm.Page1.oPag.oSTNUMDOC_1_8.value=this.w_STNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oSTDATDOC_1_9.value==this.w_STDATDOC)
      this.oPgFrm.Page1.oPag.oSTDATDOC_1_9.value=this.w_STDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oSTCODVAL_1_10.value==this.w_STCODVAL)
      this.oPgFrm.Page1.oPag.oSTCODVAL_1_10.value=this.w_STCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSTCOMPET_1_12.value==this.w_STCOMPET)
      this.oPgFrm.Page1.oPag.oSTCOMPET_1_12.value=this.w_STCOMPET
    endif
    if not(this.oPgFrm.Page1.oPag.oSTNUMPRO_1_13.value==this.w_STNUMPRO)
      this.oPgFrm.Page1.oPag.oSTNUMPRO_1_13.value=this.w_STNUMPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oSTALFPRO_1_14.value==this.w_STALFPRO)
      this.oPgFrm.Page1.oPag.oSTALFPRO_1_14.value=this.w_STALFPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oSTCAOVAL_1_15.value==this.w_STCAOVAL)
      this.oPgFrm.Page1.oPag.oSTCAOVAL_1_15.value=this.w_STCAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSTDESSUP_1_16.value==this.w_STDESSUP)
      this.oPgFrm.Page1.oPag.oSTDESSUP_1_16.value=this.w_STDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_34.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_34.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTTIPCON_2_2.value==this.w_STTIPCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTTIPCON_2_2.value=this.w_STTIPCON
      replace t_STTIPCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTTIPCON_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTCODCON_2_3.value==this.w_STCODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTCODCON_2_3.value=this.w_STCODCON
      replace t_STCODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTCODCON_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPDAR_2_4.value==this.w_STIMPDAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPDAR_2_4.value=this.w_STIMPDAR
      replace t_STIMPDAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPDAR_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPAVE_2_5.value==this.w_STIMPAVE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPAVE_2_5.value=this.w_STIMPAVE
      replace t_STIMPAVE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTIMPAVE_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTCAURIG_2_6.value==this.w_STCAURIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTCAURIG_2_6.value=this.w_STCAURIG
      replace t_STCAURIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTCAURIG_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTCODPAG_2_7.value==this.w_STCODPAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTCODPAG_2_7.value=this.w_STCODPAG
      replace t_STCODPAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTCODPAG_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'STO_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_STCAOVAL))  and (GETVALUT(g_PERVAL, 'VADATEUR')>=IIF(EMPTY(.w_STDATDOC), .w_STDATREG, .w_STDATDOC) OR .w_CAOVAL=0)
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSTCAOVAL_1_15.SetFocus()
            i_bnoObbl = !empty(.w_STCAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_STTIPCON $ 'CFG') and (NOT EMPTY(.w_STCODCON))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSTTIPCON_2_2
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      i_bRes = i_bRes .and. .GSCG_MCS.CheckForm()
      if NOT EMPTY(.w_STCODCON)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_STSERIAL = this.w_STSERIAL
    this.o_STCODVAL = this.w_STCODVAL
    this.o_STTIPCON = this.w_STTIPCON
    this.o_STCODCON = this.w_STCODCON
    * --- GSCG_MCS : Depends On
    this.GSCG_MCS.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_STCODCON))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_STTIPCON=space(1)
      .w_STCODCON=space(15)
      .w_STIMPDAR=0
      .w_STIMPAVE=0
      .w_STCAURIG=space(5)
      .w_STCODPAG=space(5)
      .w_MASTRO=space(15)
      .w_SEZB=space(1)
      .DoRTCalc(1,24,.f.)
      if not(empty(.w_STCODCON))
        .link_2_3('Full')
      endif
      .DoRTCalc(25,27,.f.)
      if not(empty(.w_STCAURIG))
        .link_2_6('Full')
      endif
      .DoRTCalc(28,28,.f.)
      if not(empty(.w_STCODPAG))
        .link_2_7('Full')
      endif
      .DoRTCalc(29,29,.f.)
        .w_SEZB = IIF(.w_STTIPCON="G", CALCSEZ(.w_MASTRO), " ")
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_STTIPCON = t_STTIPCON
    this.w_STCODCON = t_STCODCON
    this.w_STIMPDAR = t_STIMPDAR
    this.w_STIMPAVE = t_STIMPAVE
    this.w_STCAURIG = t_STCAURIG
    this.w_STCODPAG = t_STCODPAG
    this.w_MASTRO = t_MASTRO
    this.w_SEZB = t_SEZB
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_STTIPCON with this.w_STTIPCON
    replace t_STCODCON with this.w_STCODCON
    replace t_STIMPDAR with this.w_STIMPDAR
    replace t_STIMPAVE with this.w_STIMPAVE
    replace t_STCAURIG with this.w_STCAURIG
    replace t_STCODPAG with this.w_STCODPAG
    replace t_MASTRO with this.w_MASTRO
    replace t_SEZB with this.w_SEZB
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mstPag1 as StdContainer
  Width  = 699
  height = 305
  stdWidth  = 699
  stdheight = 305
  resizeXpos=87
  resizeYpos=224
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSTNUMREG_1_3 as StdField with uid="YBGRTSEKHZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_STNUMREG", cQueryName = "STNUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 23073901,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=152, Top=10, cSayPict='"999999"', cGetPict='"999999"'

  add object oSTCODESE_1_4 as StdField with uid="NYDCJFXADE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_STCODESE", cQueryName = "STCODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza",;
    HelpContextID = 63530091,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=235, Top=10, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_STCODESE"

  func oSTCODESE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTCODESE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTCODESE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oSTCODESE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"Esercizi",'',this.parent.oContained
  endproc
  proc oSTCODESE_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_CODAZI
     i_obj.w_ESCODESE=this.parent.oContained.w_STCODESE
    i_obj.ecpSave()
  endproc

  add object oSTDATREG_1_5 as StdField with uid="UIECGUVXSK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_STDATREG", cQueryName = "STDATREG",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 29062253,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=425, Top=10

  add object oSTCODUTE_1_6 as StdField with uid="LCBMMDHQQJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_STCODUTE", cQueryName = "STCODUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente",;
    HelpContextID = 63530091,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=648, Top=10, cSayPict='"9999"', cGetPict='"9999"'

  add object oSTCODCAU_1_7 as StdField with uid="UCJOKWPHGZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_STCODCAU", cQueryName = "STCODCAU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contabile della registrazione",;
    HelpContextID = 29975675,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=152, Top=40, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_STCODCAU"

  func oSTCODCAU_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTCODCAU_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTCODCAU_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oSTCODCAU_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oSTCODCAU_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_STCODCAU
    i_obj.ecpSave()
  endproc

  add object oSTNUMDOC_1_8 as StdField with uid="FFGCVOESNZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_STNUMDOC", cQueryName = "STNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 211807127,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=315, Top=40, cSayPict='"999999"', cGetPict='"999999"'

  add object oSTDATDOC_1_9 as StdField with uid="VMIDGKAKJZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_STDATDOC", cQueryName = "STDATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 205818775,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=425, Top=40

  add object oSTCODVAL_1_10 as StdField with uid="IFKUQUDNIT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_STCODVAL", cQueryName = "STCODVAL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta della registrazione",;
    HelpContextID = 80307314,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=582, Top=40, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_STCODVAL"

  func oSTCODVAL_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTCODVAL_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTCODVAL_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oSTCODVAL_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oSTCODVAL_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_STCODVAL
    i_obj.ecpSave()
  endproc

  add object oSTCOMPET_1_12 as StdField with uid="ZRBSOOTPOX",rtseq=12,rtrep=.f.,;
    cFormVar = "w_STCOMPET", cQueryName = "STCOMPET",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 257516666,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=152, Top=70, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_STCOMPET"

  func oSTCOMPET_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTCOMPET_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSTCOMPET_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oSTCOMPET_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"Esercizi",'',this.parent.oContained
  endproc
  proc oSTCOMPET_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_CODAZI
     i_obj.w_ESCODESE=this.parent.oContained.w_STCOMPET
    i_obj.ecpSave()
  endproc

  add object oSTNUMPRO_1_13 as StdField with uid="DGCNWBYOQT",rtseq=13,rtrep=.f.,;
    cFormVar = "w_STNUMPRO", cQueryName = "STNUMPRO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 257954933,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=315, Top=70, cSayPict='"999999"', cGetPict='"999999"'

  add object oSTALFPRO_1_14 as StdField with uid="NUQWEHXGYD",rtseq=14,rtrep=.f.,;
    cFormVar = "w_STALFPRO", cQueryName = "STALFPRO",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 249971829,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=392, Top=70, InputMask=replicate('X',2)

  add object oSTCAOVAL_1_15 as StdField with uid="OWUSGBNMOK",rtseq=15,rtrep=.f.,;
    cFormVar = "w_STCAOVAL", cQueryName = "STCAOVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio al momento della registrazione",;
    HelpContextID = 90924146,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=582, Top=70, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  proc oSTCAOVAL_1_15.mDefault
    with this.Parent.oContained
      if empty(.w_STCAOVAL)
        .w_STCAOVAL = GETCAM(.w_STCODVAL, IIF(EMPTY(.w_STDATDOC), .w_STDATREG, .w_STDATDOC),0)
      endif
    endwith
  endproc

  func oSTCAOVAL_1_15.mCond()
    with this.Parent.oContained
      return (GETVALUT(g_PERVAL, 'VADATEUR')>=IIF(EMPTY(.w_STDATDOC), .w_STDATREG, .w_STDATDOC) OR .w_CAOVAL=0)
    endwith
  endfunc

  add object oSTDESSUP_1_16 as StdField with uid="FKGLDPUNKN",rtseq=16,rtrep=.f.,;
    cFormVar = "w_STDESSUP", cQueryName = "STDESSUP",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 45053046,;
   bGlobalFont=.t.,;
    Height=21, Width=352, Left=152, Top=100, InputMask=replicate('X',50)

  add object oSIMVAL_1_34 as StdField with uid="XADGLWOGAS",rtseq=21,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 178321702,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=633, Top=40, InputMask=replicate('X',5)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=127, width=617,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Riga",Field2="STTIPCON",Label2="T",Field3="STCODCON",Label3="Conto",Field4="STIMPDAR",Label4="DARE",Field5="STIMPAVE",Label5="AVERE",Field6="STCAURIG",Label6="Causale",Field7="STCODPAG",Label7="Pagamento",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218956922

  add object oStr_1_17 as StdString with uid="OIFKZMWUCN",Visible=.t., Left=515, Top=10,;
    Alignment=1, Width=131, Height=15,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="NVLCBGUUOM",Visible=.t., Left=372, Top=10,;
    Alignment=1, Width=50, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="CERQCRIQJG",Visible=.t., Left=46, Top=40,;
    Alignment=1, Width=104, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="MAGJNUBVMC",Visible=.t., Left=438, Top=70,;
    Alignment=1, Width=142, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="ZJFXSIOVUW",Visible=.t., Left=3, Top=100,;
    Alignment=1, Width=147, Height=15,;
    Caption="Descr. suppl.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="IXLZJOBTZO",Visible=.t., Left=381, Top=70,;
    Alignment=0, Width=9, Height=15,;
    Caption="\"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="KLTPCQMDGA",Visible=.t., Left=46, Top=10,;
    Alignment=1, Width=104, Height=15,;
    Caption="Registrazione n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_24 as StdString with uid="GTIDGIOUDW",Visible=.t., Left=223, Top=10,;
    Alignment=0, Width=8, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="FUVQGECXDJ",Visible=.t., Left=215, Top=40,;
    Alignment=1, Width=98, Height=15,;
    Caption="Doc.N.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="ULCHOIVZNK",Visible=.t., Left=372, Top=40,;
    Alignment=1, Width=50, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="UWMZNTCUDH",Visible=.t., Left=510, Top=40,;
    Alignment=1, Width=69, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="VSRJSRPBAE",Visible=.t., Left=46, Top=70,;
    Alignment=1, Width=104, Height=15,;
    Caption="Competenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="EBKISYMDOH",Visible=.t., Left=214, Top=70,;
    Alignment=1, Width=99, Height=15,;
    Caption="Protocollo n.:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=146,;
    width=613+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=147,width=612+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CONTI|CAU_CONT|PAG_AMEN|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CONTI'
        oDropInto=this.oBodyCol.oRow.oSTCODCON_2_3
      case cFile='CAU_CONT'
        oDropInto=this.oBodyCol.oRow.oSTCAURIG_2_6
      case cFile='PAG_AMEN'
        oDropInto=this.oBodyCol.oRow.oSTCODPAG_2_7
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_8 as StdButton with uid="LPEOUDVAYG",width=48,height=45,;
   left=625, top=148,;
    CpPicture="BMP\CENCOST.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai movimenti di analitica";
    , HelpContextID = 76221845;
    , TabStop=.f., Caption='A\<nalitica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_8.Click()
      this.Parent.oContained.GSCG_MCS.LinkPCClick()
    endproc

  func oLinkPC_2_8.mCond()
    with this.Parent.oContained
      return (.w_STTIPCON='G' AND .w_SEZB $ "CR" AND g_PERCCR="S")
    endwith
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_mstBodyRow as CPBodyRowCnt
  Width=603
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="ADIDATADLE",rtseq=22,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 251285354,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=44, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oSTTIPCON_2_2 as StdTrsField with uid="AZRNGDBLOO",rtseq=23,rtrep=.t.,;
    cFormVar="w_STTIPCON",value=space(1),;
    HelpContextID = 226200460,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=16, Left=47, Top=0, cSayPict=['!'], cGetPict=['!'], InputMask=replicate('X',1)

  func oSTTIPCON_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_STTIPCON $ 'CFG')
      if .not. empty(.w_STCODCON)
        bRes2=.link_2_3('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oSTCODCON_2_3 as StdTrsField with uid="VUURHVQUJO",rtseq=24,rtrep=.t.,;
    cFormVar="w_STCODCON",value=space(15),;
    ToolTipText = "Codice conto",;
    HelpContextID = 238459788,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=68, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_STTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_STCODCON"

  func oSTCODCON_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTCODCON_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSTCODCON_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_STTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_STTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSTCODCON_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oSTCODCON_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_STTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_STCODCON
    i_obj.ecpSave()
  endproc

  add object oSTIMPDAR_2_4 as StdTrsField with uid="CULNTIMGTF",rtseq=25,rtrep=.t.,;
    cFormVar="w_STIMPDAR",value=0,;
    ToolTipText = "Importo dare",;
    HelpContextID = 59229304,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=201, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oSTIMPAVE_2_5 as StdTrsField with uid="QZXAPLSRPS",rtseq=26,rtrep=.t.,;
    cFormVar="w_STIMPAVE",value=0,;
    ToolTipText = "Importo avere",;
    HelpContextID = 8897643,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=334, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oSTCAURIG_2_6 as StdTrsField with uid="GQADQYPBYG",rtseq=27,rtrep=.t.,;
    cFormVar="w_STCAURIG",value=space(5),;
    ToolTipText = "Causale di riga",;
    HelpContextID = 238328723,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=467, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_STCAURIG"

  func oSTCAURIG_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTCAURIG_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSTCAURIG_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oSTCAURIG_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oSTCAURIG_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_STCAURIG
    i_obj.ecpSave()
  endproc

  add object oSTCODPAG_2_7 as StdTrsField with uid="XLTMCRZRIP",rtseq=28,rtrep=.t.,;
    cFormVar="w_STCODPAG",value=space(5),;
    ToolTipText = "Pagamento",;
    HelpContextID = 248079469,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=540, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_STCODPAG"

  func oSTCODPAG_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oSTCODPAG_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oSTCODPAG_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oSTCODPAG_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"",'',this.parent.oContained
  endproc
  proc oSTCODPAG_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_STCODPAG
    i_obj.ecpSave()
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mst','STO_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".STSERIAL=STO_MAST.STSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
