* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_spd                                                        *
*              Stampa piano PDA                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_100]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-20                                                      *
* Last revis.: 2013-04-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsac_spd",oParentObject))

* --- Class definition
define class tgsac_spd as StdForm
  Top    = 9
  Left   = 18

  * --- Standard Properties
  Width  = 535
  Height = 308
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-04-10"
  HelpContextID=141191017
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Constant Properties
  _IDX = 0
  PDA_TPER_IDX = 0
  KEY_ARTI_IDX = 0
  CONTI_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  cPrg = "gsac_spd"
  cComment = "Stampa piano PDA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPCON = space(1)
  w_INTERPER = 0
  w_PERINI = space(4)
  w_DESPIN = space(4)
  w_DINIINI = ctod('  /  /  ')
  w_DINIFIN = ctod('  /  /  ')
  w_PERFIN = space(4)
  w_DESPFI = space(4)
  w_CODINI = space(20)
  w_DFININI = ctod('  /  /  ')
  w_DESINI = space(40)
  w_DFINFIN = ctod('  /  /  ')
  w_CODFIN = space(20)
  w_DESFIN = space(40)
  w_CODFOR = space(15)
  w_DESFOR = space(40)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_FLGDES = .F.
  w_TPPERASS = space(3)
  w_OBTEST = ctod('  /  /  ')
  w_DESCOM = space(30)
  w_DESATT = space(30)
  w_TIPATT = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsac_spdPag1","gsac_spd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='PDA_TPER'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='CAN_TIER'
    this.cWorkTables[5]='ATTIVITA'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSAC_BPO with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCON=space(1)
      .w_INTERPER=0
      .w_PERINI=space(4)
      .w_DESPIN=space(4)
      .w_DINIINI=ctod("  /  /  ")
      .w_DINIFIN=ctod("  /  /  ")
      .w_PERFIN=space(4)
      .w_DESPFI=space(4)
      .w_CODINI=space(20)
      .w_DFININI=ctod("  /  /  ")
      .w_DESINI=space(40)
      .w_DFINFIN=ctod("  /  /  ")
      .w_CODFIN=space(20)
      .w_DESFIN=space(40)
      .w_CODFOR=space(15)
      .w_DESFOR=space(40)
      .w_CODCOM=space(15)
      .w_CODATT=space(15)
      .w_FLGDES=.f.
      .w_TPPERASS=space(3)
      .w_OBTEST=ctod("  /  /  ")
      .w_DESCOM=space(30)
      .w_DESATT=space(30)
      .w_TIPATT=space(1)
        .w_TIPCON = 'F'
        .w_INTERPER = 17
        .w_PERINI = .w_TPPERASS
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_PERINI))
          .link_1_4('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate(IIF(EMPTY(.w_DESPIN),"","dal " + dtoc(.w_DINIINI) + "   al " + dtoc(.w_DINIFIN)))
          .DoRTCalc(4,6,.f.)
        .w_PERFIN = right('000'+alltrim(str(val(.w_PERINI)+.w_INTERPER,3,0)),3)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_PERFIN))
          .link_1_9('Full')
        endif
        .DoRTCalc(8,9,.f.)
        if not(empty(.w_CODINI))
          .link_1_11('Full')
        endif
        .DoRTCalc(10,13,.f.)
        if not(empty(.w_CODFIN))
          .link_1_15('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate(IIF(EMPTY(.w_DESPFI),"","dal " + dtoc(.w_DFININI) + "   al " + dtoc(.w_DFINFIN)))
        .DoRTCalc(14,15,.f.)
        if not(empty(.w_CODFOR))
          .link_1_20('Full')
        endif
        .DoRTCalc(16,17,.f.)
        if not(empty(.w_CODCOM))
          .link_1_22('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_CODATT))
          .link_1_23('Full')
        endif
        .w_FLGDES = .f.
        .w_TPPERASS = '000'
        .w_OBTEST = i_datsys
          .DoRTCalc(22,23,.f.)
        .w_TIPATT = 'A'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_TIPCON = 'F'
        .DoRTCalc(2,2,.t.)
            .w_PERINI = .w_TPPERASS
          .link_1_4('Full')
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(IIF(EMPTY(.w_DESPIN),"","dal " + dtoc(.w_DINIINI) + "   al " + dtoc(.w_DINIFIN)))
        .DoRTCalc(4,6,.t.)
            .w_PERFIN = right('000'+alltrim(str(val(.w_PERINI)+.w_INTERPER,3,0)),3)
          .link_1_9('Full')
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(IIF(EMPTY(.w_DESPFI),"","dal " + dtoc(.w_DFININI) + "   al " + dtoc(.w_DFINFIN)))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(IIF(EMPTY(.w_DESPIN),"","dal " + dtoc(.w_DINIINI) + "   al " + dtoc(.w_DINIFIN)))
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(IIF(EMPTY(.w_DESPFI),"","dal " + dtoc(.w_DFININI) + "   al " + dtoc(.w_DFINFIN)))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODATT_1_23.enabled = this.oPgFrm.Page1.oPag.oCODATT_1_23.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODCOM_1_22.visible=!this.oPgFrm.Page1.oPag.oCODCOM_1_22.mHide()
    this.oPgFrm.Page1.oPag.oCODATT_1_23.visible=!this.oPgFrm.Page1.oPag.oCODATT_1_23.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PERINI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PDA_TPER_IDX,3]
    i_lTable = "PDA_TPER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PDA_TPER_IDX,2], .t., this.PDA_TPER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PDA_TPER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPPERASS,TPPERREL,TPDATINI,TPDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where TPPERASS="+cp_ToStrODBC(this.w_PERINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPPERASS',this.w_PERINI)
            select TPPERASS,TPPERREL,TPDATINI,TPDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERINI = NVL(_Link_.TPPERASS,space(4))
      this.w_DESPIN = NVL(_Link_.TPPERREL,space(4))
      this.w_DINIINI = NVL(cp_ToDate(_Link_.TPDATINI),ctod("  /  /  "))
      this.w_DINIFIN = NVL(cp_ToDate(_Link_.TPDATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PERINI = space(4)
      endif
      this.w_DESPIN = space(4)
      this.w_DINIINI = ctod("  /  /  ")
      this.w_DINIFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PDA_TPER_IDX,2])+'\'+cp_ToStr(_Link_.TPPERASS,1)
      cp_ShowWarn(i_cKey,this.PDA_TPER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PERFIN
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PDA_TPER_IDX,3]
    i_lTable = "PDA_TPER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PDA_TPER_IDX,2], .t., this.PDA_TPER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PDA_TPER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPPERASS,TPPERREL,TPDATINI,TPDATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where TPPERASS="+cp_ToStrODBC(this.w_PERFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPPERASS',this.w_PERFIN)
            select TPPERASS,TPPERREL,TPDATINI,TPDATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERFIN = NVL(_Link_.TPPERASS,space(4))
      this.w_DESPFI = NVL(_Link_.TPPERREL,space(4))
      this.w_DFININI = NVL(cp_ToDate(_Link_.TPDATINI),ctod("  /  /  "))
      this.w_DFINFIN = NVL(cp_ToDate(_Link_.TPDATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PERFIN = space(4)
      endif
      this.w_DESPFI = space(4)
      this.w_DFININI = ctod("  /  /  ")
      this.w_DFINFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PDA_TPER_IDX,2])+'\'+cp_ToStr(_Link_.TPPERASS,1)
      cp_ShowWarn(i_cKey,this.PDA_TPER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINI
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODINI))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODINI)+"%");

            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODINI_1_11'),i_cWhere,'GSMA_ACA',"Codici articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODINI)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.CACODICE,space(20))
      this.w_DESINI = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(20)
      endif
      this.w_DESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI<=.w_CODFIN or empty(.w_CODFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo iniziale maggiore di quello finale")
        endif
        this.w_CODINI = space(20)
        this.w_DESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CODFIN))
          select CACODICE,CADESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CADESART like "+cp_ToStr(trim(this.w_CODFIN)+"%");

            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCODFIN_1_15'),i_cWhere,'GSMA_ACA',"Codici articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CODFIN)
            select CACODICE,CADESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.CACODICE,space(20))
      this.w_DESFIN = NVL(_Link_.CADESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(20)
      endif
      this.w_DESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CODINI<=.w_CODFIN or empty(.w_CODINI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Articolo iniziale maggiore di quello finale")
        endif
        this.w_CODFIN = space(20)
        this.w_DESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFOR
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODFOR))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFOR_1_20'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODFOR)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFOR = space(15)
      endif
      this.w_DESFOR = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_1_22'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_23'),i_cWhere,'',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice attivit� incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDESPIN_1_5.value==this.w_DESPIN)
      this.oPgFrm.Page1.oPag.oDESPIN_1_5.value=this.w_DESPIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPFI_1_10.value==this.w_DESPFI)
      this.oPgFrm.Page1.oPag.oDESPFI_1_10.value=this.w_DESPFI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_11.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_11.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_13.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_13.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_15.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_15.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_17.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_17.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFOR_1_20.value==this.w_CODFOR)
      this.oPgFrm.Page1.oPag.oCODFOR_1_20.value=this.w_CODFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_21.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_21.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCOM_1_22.value==this.w_CODCOM)
      this.oPgFrm.Page1.oPag.oCODCOM_1_22.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_23.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_23.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGDES_1_24.RadioValue()==this.w_FLGDES)
      this.oPgFrm.Page1.oPag.oFLGDES_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_32.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_32.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_34.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_34.value=this.w_DESATT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_CODINI<=.w_CODFIN or empty(.w_CODFIN))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo iniziale maggiore di quello finale")
          case   not(.w_CODINI<=.w_CODFIN or empty(.w_CODINI))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Articolo iniziale maggiore di quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsac_spdPag1 as StdContainer
  Width  = 531
  height = 308
  stdWidth  = 531
  stdheight = 308
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_3 as StdButton with uid="FNZDDLNSYM",left=310, top=18, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per impostare l'intervallo dei periodi da stampare";
    , HelpContextID = 140989994;
  , bGlobalFont=.t.

    proc oBtn_1_3.Click()
      vx_exec("STD\GSAC_PER.VZM",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESPIN_1_5 as StdField with uid="QVJVQBUXHG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESPIN", cQueryName = "DESPIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Periodo di inizio selezione",;
    HelpContextID = 175837750,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=83, Top=18, InputMask=replicate('X',4)


  add object oObj_1_8 as cp_calclbl with uid="BTEDDQBFEO",left=131, top=18, width=173,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Periodo dal: ",;
    nPag=1;
    , HelpContextID = 36806630

  add object oDESPFI_1_10 as StdField with uid="FKVKYCTTVJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESPFI", cQueryName = "DESPFI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Periodo di fine selezione",;
    HelpContextID = 88805942,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=83, Top=47, InputMask=replicate('X',4)

  add object oCODINI_1_11 as StdField with uid="TUZPCDFBAA",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo iniziale maggiore di quello finale",;
    ToolTipText = "Selezionare l'intervallo dei codici articoli da stampare",;
    HelpContextID = 96676902,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=83, Top=86, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_CODINI"

  func oCODINI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODINI_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici articoli",'',this.parent.oContained
  endproc
  proc oCODINI_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_13 as StdField with uid="KCTHKDOQLR",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 96735798,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=233, Top=86, InputMask=replicate('X',40)

  add object oCODFIN_1_15 as StdField with uid="OBZWHJFDZM",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Articolo iniziale maggiore di quello finale",;
    ToolTipText = "Selezionare l'intervallo dei codici da stampare",;
    HelpContextID = 175123494,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=83, Top=110, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCODFIN_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici articoli",'',this.parent.oContained
  endproc
  proc oCODFIN_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc


  add object oObj_1_16 as cp_calclbl with uid="ISOFJMRIBZ",left=131, top=47, width=173,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="Periodo dal: ",;
    nPag=1;
    , HelpContextID = 36806630

  add object oDESFIN_1_17 as StdField with uid="KJXNLIUMXA",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 175182390,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=233, Top=110, InputMask=replicate('X',40)

  add object oCODFOR_1_20 as StdField with uid="MESZSSWCFT",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODFOR", cQueryName = "CODFOR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice fornitore di selezione; (spazio) = no selezione",;
    HelpContextID = 248523814,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=83, Top=142, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFOR"

  func oCODFOR_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFOR_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFOR_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFOR_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oCODFOR_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODFOR
     i_obj.ecpSave()
  endproc

  add object oDESFOR_1_21 as StdField with uid="LXENJHEHTG",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 248582710,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=233, Top=142, InputMask=replicate('X',40)

  add object oCODCOM_1_22 as StdField with uid="OHCXLYKMDQ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice commessa incongruente",;
    ToolTipText = "Commessa",;
    HelpContextID = 164441126,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=86, Top=175, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_1_22.mHide()
    with this.Parent.oContained
      return ( g_PERCAN<>'S' and g_COMM<>'S')
    endwith
  endfunc

  func oCODCOM_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
      if .not. empty(.w_CODATT)
        bRes2=.link_1_23('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCODATT_1_23 as StdField with uid="TZMOGDJAHP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice attivit� incongruente",;
    ToolTipText = "Attivit�",;
    HelpContextID = 18557990,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=86, Top=199, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT"

  func oCODATT_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_CODCOM))
    endwith
   endif
  endfunc

  func oCODATT_1_23.mHide()
    with this.Parent.oContained
      return ( g_PERCAN<>'S' and g_COMM<>'S')
    endwith
  endfunc

  func oCODATT_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Attivit�",'',this.parent.oContained
  endproc

  add object oFLGDES_1_24 as StdCheck with uid="RRWKGLFRCN",rtseq=19,rtrep=.f.,left=80, top=239, caption="Stampa descrizione",;
    ToolTipText = "Stampa descrizione articolo",;
    HelpContextID = 254695766,;
    cFormVar="w_FLGDES", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGDES_1_24.RadioValue()
    return(iif(this.value =1,.t.,;
    .f.))
  endfunc
  func oFLGDES_1_24.GetRadio()
    this.Parent.oContained.w_FLGDES = this.RadioValue()
    return .t.
  endfunc

  func oFLGDES_1_24.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLGDES==.t.,1,;
      0)
  endfunc


  add object oBtn_1_30 as StdButton with uid="XAOFRYAIID",left=421, top=254, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare la stampa";
    , HelpContextID = 141162266;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        do GSAC_BPO with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_31 as StdButton with uid="KYQESPJULE",left=471, top=254, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 133873594;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCOM_1_32 as StdField with uid="OKRUEJONSV",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 164500022,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=213, Top=175, InputMask=replicate('X',30)

  add object oDESATT_1_34 as StdField with uid="WMKTLSOVSA",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 18616886,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=213, Top=199, InputMask=replicate('X',30)

  add object oStr_1_18 as StdString with uid="ELHXOYRMRR",Visible=.t., Left=6, Top=18,;
    Alignment=1, Width=75, Height=15,;
    Caption="Da periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="QAXKHIJJWI",Visible=.t., Left=9, Top=47,;
    Alignment=1, Width=72, Height=15,;
    Caption="A periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="VJRTLBFTIX",Visible=.t., Left=9, Top=86,;
    Alignment=1, Width=72, Height=15,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="JDTCUHPRVB",Visible=.t., Left=9, Top=110,;
    Alignment=1, Width=72, Height=15,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="GSCVMJELIL",Visible=.t., Left=9, Top=142,;
    Alignment=1, Width=72, Height=15,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="KSGESHCIFC",Visible=.t., Left=5, Top=199,;
    Alignment=1, Width=76, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="TGCQLLKMBK",Visible=.t., Left=5, Top=177,;
    Alignment=1, Width=76, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsac_spd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
