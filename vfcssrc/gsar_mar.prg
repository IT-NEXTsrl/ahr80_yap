* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mar                                                        *
*              Dettaglio contributi accessori                                  *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-26                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mar")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mar")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mar")
  return

* --- Class definition
define class tgsar_mar as StdPCForm
  Width  = 602
  Height = 275
  Top    = 10
  Left   = 10
  cComment = "Dettaglio contributi accessori"
  cPrg = "gsar_mar"
  HelpContextID=130643817
  add object cnt as tcgsar_mar
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mar as PCContext
  w_MCCODART = space(20)
  w_MCTIPCAT = space(15)
  w_MCCODCAT = space(5)
  w_DESCCAT = space(40)
  w_TPDESCRI = space(40)
  w_TPAPPPES = space(1)
  proc Save(i_oFrom)
    this.w_MCCODART = i_oFrom.w_MCCODART
    this.w_MCTIPCAT = i_oFrom.w_MCTIPCAT
    this.w_MCCODCAT = i_oFrom.w_MCCODCAT
    this.w_DESCCAT = i_oFrom.w_DESCCAT
    this.w_TPDESCRI = i_oFrom.w_TPDESCRI
    this.w_TPAPPPES = i_oFrom.w_TPAPPPES
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_MCCODART = this.w_MCCODART
    i_oTo.w_MCTIPCAT = this.w_MCTIPCAT
    i_oTo.w_MCCODCAT = this.w_MCCODCAT
    i_oTo.w_DESCCAT = this.w_DESCCAT
    i_oTo.w_TPDESCRI = this.w_TPDESCRI
    i_oTo.w_TPAPPPES = this.w_TPAPPPES
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mar as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 602
  Height = 275
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=130643817
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CONTRART_IDX = 0
  TIPCONTR_IDX = 0
  CATDCONT_IDX = 0
  CATMCONT_IDX = 0
  cFile = "CONTRART"
  cKeySelect = "MCCODART"
  cKeyWhere  = "MCCODART=this.w_MCCODART"
  cKeyDetail  = "MCCODART=this.w_MCCODART and MCTIPCAT=this.w_MCTIPCAT"
  cKeyWhereODBC = '"MCCODART="+cp_ToStrODBC(this.w_MCCODART)';

  cKeyDetailWhereODBC = '"MCCODART="+cp_ToStrODBC(this.w_MCCODART)';
      +'+" and MCTIPCAT="+cp_ToStrODBC(this.w_MCTIPCAT)';

  cKeyWhereODBCqualified = '"CONTRART.MCCODART="+cp_ToStrODBC(this.w_MCCODART)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsar_mar"
  cComment = "Dettaglio contributi accessori"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MCCODART = space(20)
  w_MCTIPCAT = space(15)
  o_MCTIPCAT = space(15)
  w_MCCODCAT = space(5)
  w_DESCCAT = space(40)
  w_TPDESCRI = space(40)
  w_TPAPPPES = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_marPag1","gsar_mar",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='TIPCONTR'
    this.cWorkTables[2]='CATDCONT'
    this.cWorkTables[3]='CATMCONT'
    this.cWorkTables[4]='CONTRART'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONTRART_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONTRART_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mar'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CONTRART where MCCODART=KeySet.MCCODART
    *                            and MCTIPCAT=KeySet.MCTIPCAT
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CONTRART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTRART_IDX,2],this.bLoadRecFilter,this.CONTRART_IDX,"gsar_mar")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONTRART')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONTRART.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONTRART '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MCCODART',this.w_MCCODART  )
      select * from (i_cTable) CONTRART where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_MCCODART = NVL(MCCODART,space(20))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CONTRART')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESCCAT = space(40)
          .w_TPDESCRI = space(40)
          .w_TPAPPPES = space(1)
          .w_MCTIPCAT = NVL(MCTIPCAT,space(15))
          if link_2_1_joined
            this.w_MCTIPCAT = NVL(TPCODICE201,NVL(this.w_MCTIPCAT,space(15)))
            this.w_TPDESCRI = NVL(TPDESCRI201,space(40))
            this.w_TPAPPPES = NVL(TPAPPPES201,space(1))
          else
          .link_2_1('Load')
          endif
          .w_MCCODCAT = NVL(MCCODCAT,space(5))
          .link_2_2('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace MCTIPCAT with .w_MCTIPCAT
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_MCCODART=space(20)
      .w_MCTIPCAT=space(15)
      .w_MCCODCAT=space(5)
      .w_DESCCAT=space(40)
      .w_TPDESCRI=space(40)
      .w_TPAPPPES=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_MCTIPCAT))
         .link_2_1('Full')
        endif
        .w_MCCODCAT = SPACE( 5 )
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_MCCODCAT))
         .link_2_2('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONTRART')
    this.DoRTCalc(4,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CONTRART',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONTRART_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MCCODART,"MCCODART",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_MCTIPCAT C(15);
      ,t_MCCODCAT C(5);
      ,t_DESCCAT C(40);
      ,t_TPDESCRI C(40);
      ,MCTIPCAT C(15);
      ,t_TPAPPPES C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_marbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCTIPCAT_2_1.controlsource=this.cTrsName+'.t_MCTIPCAT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMCCODCAT_2_2.controlsource=this.cTrsName+'.t_MCCODCAT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCCAT_2_3.controlsource=this.cTrsName+'.t_DESCCAT'
    this.oPgFRm.Page1.oPag.oTPDESCRI_2_4.controlsource=this.cTrsName+'.t_TPDESCRI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(143)
    this.AddVLine(280)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCTIPCAT_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONTRART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTRART_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONTRART_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTRART_IDX,2])
      *
      * insert into CONTRART
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONTRART')
        i_extval=cp_InsertValODBCExtFlds(this,'CONTRART')
        i_cFldBody=" "+;
                  "(MCCODART,MCTIPCAT,MCCODCAT,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MCCODART)+","+cp_ToStrODBCNull(this.w_MCTIPCAT)+","+cp_ToStrODBCNull(this.w_MCCODCAT)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONTRART')
        i_extval=cp_InsertValVFPExtFlds(this,'CONTRART')
        cp_CheckDeletedKey(i_cTable,0,'MCCODART',this.w_MCCODART,'MCTIPCAT',this.w_MCTIPCAT)
        INSERT INTO (i_cTable) (;
                   MCCODART;
                  ,MCTIPCAT;
                  ,MCCODCAT;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_MCCODART;
                  ,this.w_MCTIPCAT;
                  ,this.w_MCCODCAT;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.CONTRART_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTRART_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_MCTIPCAT))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CONTRART')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and MCTIPCAT="+cp_ToStrODBC(&i_TN.->MCTIPCAT)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CONTRART')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and MCTIPCAT=&i_TN.->MCTIPCAT;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_MCTIPCAT))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and MCTIPCAT="+cp_ToStrODBC(&i_TN.->MCTIPCAT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and MCTIPCAT=&i_TN.->MCTIPCAT;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace MCTIPCAT with this.w_MCTIPCAT
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CONTRART
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CONTRART')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MCCODCAT="+cp_ToStrODBCNull(this.w_MCCODCAT)+;
                     ",MCTIPCAT="+cp_ToStrODBC(this.w_MCTIPCAT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and MCTIPCAT="+cp_ToStrODBC(MCTIPCAT)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CONTRART')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MCCODCAT=this.w_MCCODCAT;
                     ,MCTIPCAT=this.w_MCTIPCAT;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and MCTIPCAT=&i_TN.->MCTIPCAT;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONTRART_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTRART_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_MCTIPCAT))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CONTRART
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and MCTIPCAT="+cp_ToStrODBC(&i_TN.->MCTIPCAT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and MCTIPCAT=&i_TN.->MCTIPCAT;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_MCTIPCAT))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTRART_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTRART_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_MCTIPCAT<>.w_MCTIPCAT
          .w_MCCODCAT = SPACE( 5 )
          .link_2_2('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_TPAPPPES with this.w_TPAPPPES
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MCTIPCAT
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCONTR_IDX,3]
    i_lTable = "TIPCONTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2], .t., this.TIPCONTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCTIPCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATP',True,'TIPCONTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TPCODICE like "+cp_ToStrODBC(trim(this.w_MCTIPCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI,TPAPPPES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TPCODICE',trim(this.w_MCTIPCAT))
          select TPCODICE,TPDESCRI,TPAPPPES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCTIPCAT)==trim(_Link_.TPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCTIPCAT) and !this.bDontReportError
            deferred_cp_zoom('TIPCONTR','*','TPCODICE',cp_AbsName(oSource.parent,'oMCTIPCAT_2_1'),i_cWhere,'GSAR_ATP',"Contributi accessori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI,TPAPPPES";
                     +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',oSource.xKey(1))
            select TPCODICE,TPDESCRI,TPAPPPES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCTIPCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI,TPAPPPES";
                   +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(this.w_MCTIPCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',this.w_MCTIPCAT)
            select TPCODICE,TPDESCRI,TPAPPPES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCTIPCAT = NVL(_Link_.TPCODICE,space(15))
      this.w_TPDESCRI = NVL(_Link_.TPDESCRI,space(40))
      this.w_TPAPPPES = NVL(_Link_.TPAPPPES,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MCTIPCAT = space(15)
      endif
      this.w_TPDESCRI = space(40)
      this.w_TPAPPPES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])+'\'+cp_ToStr(_Link_.TPCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCONTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCTIPCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIPCONTR_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.TPCODICE as TPCODICE201"+ ",link_2_1.TPDESCRI as TPDESCRI201"+ ",link_2_1.TPAPPPES as TPAPPPES201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on CONTRART.MCTIPCAT=link_2_1.TPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and CONTRART.MCTIPCAT=link_2_1.TPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MCCODCAT
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATMCONT_IDX,3]
    i_lTable = "CATMCONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2], .t., this.CATMCONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MCCODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MCT',True,'CATMCONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CGCODICE like "+cp_ToStrODBC(trim(this.w_MCCODCAT)+"%");
                   +" and CGCONTRI="+cp_ToStrODBC(this.w_MCTIPCAT);

          i_ret=cp_SQL(i_nConn,"select CGCONTRI,CGCODICE,CGDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CGCONTRI,CGCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CGCONTRI',this.w_MCTIPCAT;
                     ,'CGCODICE',trim(this.w_MCCODCAT))
          select CGCONTRI,CGCODICE,CGDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CGCONTRI,CGCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MCCODCAT)==trim(_Link_.CGCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MCCODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATMCONT','*','CGCONTRI,CGCODICE',cp_AbsName(oSource.parent,'oMCCODCAT_2_2'),i_cWhere,'GSAR_MCT',"Categorie contributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MCTIPCAT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGCONTRI,CGCODICE,CGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CGCONTRI,CGCODICE,CGDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGCONTRI,CGCODICE,CGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CGCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CGCONTRI="+cp_ToStrODBC(this.w_MCTIPCAT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CGCONTRI',oSource.xKey(1);
                       ,'CGCODICE',oSource.xKey(2))
            select CGCONTRI,CGCODICE,CGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MCCODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGCONTRI,CGCODICE,CGDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CGCODICE="+cp_ToStrODBC(this.w_MCCODCAT);
                   +" and CGCONTRI="+cp_ToStrODBC(this.w_MCTIPCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CGCONTRI',this.w_MCTIPCAT;
                       ,'CGCODICE',this.w_MCCODCAT)
            select CGCONTRI,CGCODICE,CGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MCCODCAT = NVL(_Link_.CGCODICE,space(5))
      this.w_DESCCAT = NVL(_Link_.CGDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MCCODCAT = space(5)
      endif
      this.w_DESCCAT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])+'\'+cp_ToStr(_Link_.CGCONTRI,1)+'\'+cp_ToStr(_Link_.CGCODICE,1)
      cp_ShowWarn(i_cKey,this.CATMCONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MCCODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTPDESCRI_2_4.value==this.w_TPDESCRI)
      this.oPgFrm.Page1.oPag.oTPDESCRI_2_4.value=this.w_TPDESCRI
      replace t_TPDESCRI with this.oPgFrm.Page1.oPag.oTPDESCRI_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCTIPCAT_2_1.value==this.w_MCTIPCAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCTIPCAT_2_1.value=this.w_MCTIPCAT
      replace t_MCTIPCAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCTIPCAT_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCODCAT_2_2.value==this.w_MCCODCAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCODCAT_2_2.value=this.w_MCCODCAT
      replace t_MCCODCAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMCCODCAT_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCCAT_2_3.value==this.w_DESCCAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCCAT_2_3.value=this.w_DESCCAT
      replace t_DESCCAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCCAT_2_3.value
    endif
    cp_SetControlsValueExtFlds(this,'CONTRART')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_mar
      IF i_bRes
        L_OLDAREA=SELECT()
        THIS.EXEC_SELECT("CONTROLLORIGA","COUNT(t_MCTIPCAT) AS OCCORRENZE",;
        "NOT DELETED()",;
        "t_MCTIPCAT",;
        "t_MCTIPCAT",;
        "OCCORRENZE > 1")
        IF RECCOUNT("CONTROLLORIGA") > 0
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = Ah_MsgFormat("Ogni contributo accessorio deve essere utilizzato una sola volta")
        ENDIF
      SELECT(L_OLDAREA)
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_MCTIPCAT))
        * --- Area Manuale = Check Row
        * --- gsar_mar
        IF not empty( this.w_MCTIPCAT ) and empty( this.w_MCCODCAT )
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = Ah_MsgFormat("Specificare la categoria contributo")
        ENDIF
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MCTIPCAT = this.w_MCTIPCAT
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_MCTIPCAT)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_MCTIPCAT=space(15)
      .w_MCCODCAT=space(5)
      .w_DESCCAT=space(40)
      .w_TPDESCRI=space(40)
      .w_TPAPPPES=space(1)
      .DoRTCalc(1,2,.f.)
      if not(empty(.w_MCTIPCAT))
        .link_2_1('Full')
      endif
        .w_MCCODCAT = SPACE( 5 )
      .DoRTCalc(3,3,.f.)
      if not(empty(.w_MCCODCAT))
        .link_2_2('Full')
      endif
    endwith
    this.DoRTCalc(4,6,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_MCTIPCAT = t_MCTIPCAT
    this.w_MCCODCAT = t_MCCODCAT
    this.w_DESCCAT = t_DESCCAT
    this.w_TPDESCRI = t_TPDESCRI
    this.w_TPAPPPES = t_TPAPPPES
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_MCTIPCAT with this.w_MCTIPCAT
    replace t_MCCODCAT with this.w_MCCODCAT
    replace t_DESCCAT with this.w_DESCCAT
    replace t_TPDESCRI with this.w_TPDESCRI
    replace t_TPAPPPES with this.w_TPAPPPES
    if i_srv='A'
      replace MCTIPCAT with this.w_MCTIPCAT
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_marPag1 as StdContainer
  Width  = 598
  height = 275
  stdWidth  = 598
  stdheight = 275
  resizeXpos=218
  resizeYpos=134
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=7, top=3, width=585,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="MCTIPCAT",Label1="Contributo accessorio",Field2="MCCODCAT",Label2="Categoria contributo",Field3="DESCCAT",Label3="Descrizione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 185447034

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=21,;
    width=579+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=22,width=578+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TIPCONTR|CATMCONT|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oTPDESCRI_2_4.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TIPCONTR'
        oDropInto=this.oBodyCol.oRow.oMCTIPCAT_2_1
      case cFile='CATMCONT'
        oDropInto=this.oBodyCol.oRow.oMCCODCAT_2_2
    endcase
    return(oDropInto)
  EndFunc


  add object oTPDESCRI_2_4 as StdTrsField with uid="OCFPVAUVER",rtseq=5,rtrep=.t.,;
    cFormVar="w_TPDESCRI",value=space(40),enabled=.f.,;
    HelpContextID = 256893313,;
    cTotal="", bFixedPos=.t., cQueryName = "TPDESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=166, Top=227, InputMask=replicate('X',40)

  add object oStr_2_5 as StdString with uid="WMYIJEQZEE",Visible=.t., Left=13, Top=229,;
    Alignment=1, Width=148, Height=18,;
    Caption="Contributo accessorio:"  ;
  , bGlobalFont=.t.

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_marBodyRow as CPBodyRowCnt
  Width=569
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oMCTIPCAT_2_1 as StdTrsField with uid="WXPMGLFVYQ",rtseq=2,rtrep=.t.,;
    cFormVar="w_MCTIPCAT",value=space(15),isprimarykey=.t.,;
    ToolTipText = "Contributo accessorio",;
    HelpContextID = 259714790,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=-2, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TIPCONTR", cZoomOnZoom="GSAR_ATP", oKey_1_1="TPCODICE", oKey_1_2="this.w_MCTIPCAT"

  func oMCTIPCAT_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
      if .not. empty(.w_MCCODCAT)
        bRes2=.link_2_2('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMCTIPCAT_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMCTIPCAT_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oMCTIPCAT_2_1.readonly and this.parent.oMCTIPCAT_2_1.isprimarykey)
    do cp_zoom with 'TIPCONTR','*','TPCODICE',cp_AbsName(this.parent,'oMCTIPCAT_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATP',"Contributi accessori",'',this.parent.oContained
   endif
  endproc
  proc oMCTIPCAT_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TPCODICE=this.parent.oContained.w_MCTIPCAT
    i_obj.ecpSave()
  endproc

  add object oMCCODCAT_2_2 as StdTrsField with uid="XMKBUAQHCI",rtseq=3,rtrep=.t.,;
    cFormVar="w_MCCODCAT",value=space(5),;
    ToolTipText = "Codice categoria contributo",;
    HelpContextID = 3538662,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=135, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATMCONT", cZoomOnZoom="GSAR_MCT", oKey_1_1="CGCONTRI", oKey_1_2="this.w_MCTIPCAT", oKey_2_1="CGCODICE", oKey_2_2="this.w_MCCODCAT"

  func oMCCODCAT_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMCCODCAT_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oMCCODCAT_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CATMCONT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CGCONTRI="+cp_ToStrODBC(this.Parent.oContained.w_MCTIPCAT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CGCONTRI="+cp_ToStr(this.Parent.oContained.w_MCTIPCAT)
    endif
    do cp_zoom with 'CATMCONT','*','CGCONTRI,CGCODICE',cp_AbsName(this.parent,'oMCCODCAT_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MCT',"Categorie contributi",'',this.parent.oContained
  endproc
  proc oMCCODCAT_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MCT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CGCONTRI=w_MCTIPCAT
     i_obj.w_CGCODICE=this.parent.oContained.w_MCCODCAT
    i_obj.ecpSave()
  endproc

  add object oDESCCAT_2_3 as StdTrsField with uid="WCXQJYNSFR",rtseq=4,rtrep=.t.,;
    cFormVar="w_DESCCAT",value=space(40),enabled=.f.,;
    HelpContextID = 229573174,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=293, Left=271, Top=0, InputMask=replicate('X',40)
  add object oLast as LastKeyMover
  * ---
  func oMCTIPCAT_2_1.When()
    return(.t.)
  proc oMCTIPCAT_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMCTIPCAT_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mar','CONTRART','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MCCODART=CONTRART.MCCODART";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
