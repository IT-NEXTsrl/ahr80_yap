* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bet                                                        *
*              Evasione righe documento                                        *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_54]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-03                                                      *
* Last revis.: 2009-12-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bet",oParentObject,m.pTipo)
return(i_retval)

define class tgsve_bet as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_PUNPAD = .NULL.
  w_SERIAL = space(10)
  w_LDATREG = ctod("  /  /  ")
  w_LTIPDOC = space(5)
  w_LCLADOC = space(2)
  w_LFLVEAC = space(1)
  w_LCODCON = space(15)
  w_LTIPCON = space(1)
  w_ROWNUM = 0
  w_LDATDOC = ctod("  /  /  ")
  w_LNUMDOC = 0
  w_LALFDOC = space(10)
  w_QTAMOV = 0
  w_QTAUM1 = 0
  w_QTAUM1 = 0
  w_PREZZO = 0
  w_OK = .f.
  w_ROWS = 0
  w_MESS = space(10)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  * --- WorkFile variables
  DOC_DETT_idx=0
  TMP_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riempimento Zoom Evasione Righe Descrittive.. GSVE_KET;  Update DOC_DETT
    this.w_PUNPAD = this.oParentObject
    MM = this.w_PUNPAD.w_ZoomMast.cCursor
    do case
      case this.pTipo="R"
        this.w_PUNPAD.NotifyEvent("Esegui")     
        this.w_PUNPAD.NotifyEvent("EseDett")     
      case this.pTipo="E"
        if USED("AppMM")
          if Not (_TALLY>0)
            ah_ErrorMsg("Nessun documento selezionato","!")
            SELECT AppMM
            USE
            i_retcode = 'stop'
            return
          endif
        endif
        if USED("AppMM")
          SELECT AppMM
          USE
        endif
        * --- Create temporary table TMP_MAST
        i_nIdx=cp_AddTableDef('TMP_MAST') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSVE2KET',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.TMP_MAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        SELECT (MM) 
        SCAN FOR XCHK=1
        this.w_SERIAL = MVSERIAL
        this.w_LDATREG = CP_TODATE(MVDATREG)
        this.w_LTIPDOC = NVL(MVTIPDOC,SPACE(5))
        this.w_LCLADOC = NVL(MVCLADOC,SPACE(2))
        this.w_LFLVEAC = NVL(MVFLVEAC," ")
        this.w_LCODCON = NVL(MVCODCON,SPACE(15))
        this.w_LTIPCON = NVL(MVTIPCON," ")
        this.w_LDATDOC = CP_TODATE(MVDATDOC)
        this.w_LNUMDOC = NVL(MVNUMDOC,0)
        this.w_LALFDOC = NVL(MVALFDOC,Space(10))
        * --- Insert into TMP_MAST
        i_nConn=i_TableProp[this.TMP_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMP_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MVSERIAL"+",MVDATREG"+",MVTIPDOC"+",MVFLVEAC"+",MVCLADOC"+",MVTIPCON"+",MVCODCON"+",MVDATDOC"+",MVNUMDOC"+",MVALFDOC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'TMP_MAST','MVSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LDATREG),'TMP_MAST','MVDATREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LTIPDOC),'TMP_MAST','MVTIPDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LFLVEAC),'TMP_MAST','MVFLVEAC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LCLADOC),'TMP_MAST','MVCLADOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LTIPCON),'TMP_MAST','MVTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LCODCON),'TMP_MAST','MVCODCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LDATDOC),'TMP_MAST','MVDATDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LNUMDOC),'TMP_MAST','MVNUMDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LALFDOC),'TMP_MAST','MVALFDOC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_SERIAL,'MVDATREG',this.w_LDATREG,'MVTIPDOC',this.w_LTIPDOC,'MVFLVEAC',this.w_LFLVEAC,'MVCLADOC',this.w_LCLADOC,'MVTIPCON',this.w_LTIPCON,'MVCODCON',this.w_LCODCON,'MVDATDOC',this.w_LDATDOC,'MVNUMDOC',this.w_LNUMDOC,'MVALFDOC',this.w_LALFDOC)
          insert into (i_cTable) (MVSERIAL,MVDATREG,MVTIPDOC,MVFLVEAC,MVCLADOC,MVTIPCON,MVCODCON,MVDATDOC,MVNUMDOC,MVALFDOC &i_ccchkf. );
             values (;
               this.w_SERIAL;
               ,this.w_LDATREG;
               ,this.w_LTIPDOC;
               ,this.w_LFLVEAC;
               ,this.w_LCLADOC;
               ,this.w_LTIPCON;
               ,this.w_LCODCON;
               ,this.w_LDATDOC;
               ,this.w_LNUMDOC;
               ,this.w_LALFDOC;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Caricamento Testata'
          return
        endif
        ENDSCAN
        this.w_OK = .F.
        Vq_exec("query\gsve3ket",This,"temp")
        * --- Try
        local bErr_03558118
        bErr_03558118=bTrsErr
        this.Try_03558118()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Aggiornamento non eseguito %1","!","", message() )
        endif
        bTrsErr=bTrsErr or bErr_03558118
        * --- End
        if this.w_OK
          if ah_YesNo("Eseguo stampa documenti evasi?")
             
 Select * From temp into cursor __TMP__
            L_flevas=this.oParentObject.w_FLEVAS
            cp_chprn("query\gsve3ket.frx", " ", this)
          endif
        endif
      case this.pTipo="S"
        if this.oParentObject.w_SELEZI = "S"
          * --- Seleziona Tutto 
          Update (MM) Set XCHK=1
        else
          * --- Deseleziona Tutto
          UPDATE (MM) SET XCHK=0 
        endif
        * --- Si riposiziona all'inizio
        Select (MM) 
 Go top
      case this.pTipo="C"
        * --- Imposta il titolo della maschera
        if this.oParentObject.w_CICLO="V"
          this.w_PUNPAD.Caption = ah_Msgformat("Evasione righe documenti di vendita")
        else
          this.w_PUNPAD.Caption = ah_Msgformat("Evasione righe documenti di acquisto")
        endif
        this.w_oMESS=createobject("ah_message")
        this.w_oMESS.AddMsgPartNL("Gestione specifica per l'evasione diretta dei documenti selezionati")     
        this.w_oMESS.AddMsgPartNL("Esegue valorizzazione della qta evasa, flag evaso, importo evaso (se servizio forfettario)%0per tutte le righe totalmente riaperte del documento selezionato")     
        this.w_oMESS.AddMsgPart("a seguito dell'esecuzione della procedura di conversione relativa all'errore 4645")     
        this.w_oMESS.Ah_ErrorMsg()     
    endcase
  endproc
  proc Try_03558118()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if this.oParentObject.w_FLEVAS="S"
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MVSERIAL,CPROWNUM"
        do vq_exec with 'GSVE1KET',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
            +i_ccchkf;
            +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
        +"DOC_DETT.MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
            +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
            +"MVFLEVAS";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
        +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="MVSERIAL,CPROWNUM"
        do vq_exec with 'GSVE1KET',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
            +",MVQTAEV1 = _t2.MVQTAUM1";
            +",MVQTAEVA = _t2.MVQTAMOV";
            +",MVIMPEVA = _t2.MVPREZZO";
        +",MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DOC_DETT','MVEFFEVA');
            +i_ccchkf;
            +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
        +"DOC_DETT.MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
            +",DOC_DETT.MVQTAEV1 = _t2.MVQTAUM1";
            +",DOC_DETT.MVQTAEVA = _t2.MVQTAMOV";
            +",DOC_DETT.MVIMPEVA = _t2.MVPREZZO";
        +",DOC_DETT.MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DOC_DETT','MVEFFEVA');
            +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
            +"MVFLEVAS,";
            +"MVQTAEV1,";
            +"MVQTAEVA,";
            +"MVIMPEVA,";
            +"MVEFFEVA";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS')+",";
            +"t2.MVQTAUM1,";
            +"t2.MVQTAMOV,";
            +"t2.MVPREZZO,";
            +cp_NullLink(cp_ToStrODBC(i_DATSYS),'DOC_DETT','MVEFFEVA')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
        +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
            +",MVQTAEV1 = _t2.MVQTAUM1";
            +",MVQTAEVA = _t2.MVQTAMOV";
            +",MVIMPEVA = _t2.MVPREZZO";
        +",MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DOC_DETT','MVEFFEVA');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
            +",MVQTAEV1 = (select MVQTAUM1 from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVQTAEVA = (select MVQTAMOV from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVIMPEVA = (select MVPREZZO from "+i_cQueryTable+" where "+i_cWhere+")";
        +",MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'DOC_DETT','MVEFFEVA');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    this.w_ROWS = i_rows
    * --- Drop temporary table TMP_MAST
    i_nIdx=cp_GetTableDefIdx('TMP_MAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_MAST')
    endif
    * --- commit
    cp_EndTrs(.t.)
    if this.w_ROWS<>0
      ah_ErrorMsg("Aggiornamento terminato")
      this.w_OK = .T.
    else
      ah_ErrorMsg("Attenzione, nessun documento selezionato")
    endif
    this.w_PUNPAD.NotifyEvent("Esegui")     
    this.w_PUNPAD.NotifyEvent("EseDett")     
    return


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='*TMP_MAST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
