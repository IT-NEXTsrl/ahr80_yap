* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_brr                                                        *
*              Cancella riferim. nei dati rilevati                             *
*                                                                              *
*      Author: Zucchetti Tam Spa                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-08-09                                                      *
* Last revis.: 2001-08-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_brr",oParentObject)
return(i_retval)

define class tgsma_brr as StdBatch
  * --- Local variables
  w_RETT = space(1)
  * --- WorkFile variables
  RILEVAZI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancella Riferimento Movimento Magazzino in Dati Rilevati
    * --- Rettifiche Inventariali,se cancello il Movimento
    *     aggiorno (a space) i campi di riferimento della tabella RILEVAZI (DRCONFER, DRSERMOV)
    this.w_RETT = NVL(this.oParentObject.w_MMFLRETT,"")
    if !Empty( this.w_RETT )
      * --- Aggiorno la tabella RILEVAZI
      * --- Write into RILEVAZI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.RILEVAZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RILEVAZI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RILEVAZI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DRCONFER ="+cp_NullLink(cp_ToStrODBC(" " ),'RILEVAZI','DRCONFER');
        +",DRSERMOV ="+cp_NullLink(cp_ToStrODBC(" "),'RILEVAZI','DRSERMOV');
            +i_ccchkf ;
        +" where ";
            +"DRSERMOV = "+cp_ToStrODBC(this.oParentObject.w_MMSERIAL);
               )
      else
        update (i_cTable) set;
            DRCONFER = " " ;
            ,DRSERMOV = " ";
            &i_ccchkf. ;
         where;
            DRSERMOV = this.oParentObject.w_MMSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='RILEVAZI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
