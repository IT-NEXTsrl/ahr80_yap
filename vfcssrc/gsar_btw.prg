* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_btw                                                        *
*              COSTRUZIONE ORGANIGRAMMA                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-06                                                      *
* Last revis.: 2009-11-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOPE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_btw",oParentObject,m.pTIPOPE)
return(i_retval)

define class tgsar_btw as StdBatch
  * --- Local variables
  pTIPOPE = space(3)
  w_FLINS_OK = .f.
  w_CURNODE = space(5)
  w_CURLVLKEY = space(254)
  w_RECATT = 0
  w_CURLEVEL = 0
  w_TRWERROR = .f.
  w_ROOTCODE = space(5)
  w_MSGERROR = space(254)
  w_CURLEVEL = 0
  w_GRUBAS = space(5)
  w_FLRESP = space(1)
  w_CODRIS = space(5)
  w_GRPPAD = space(250)
  w_FLVISIB = space(1)
  w_ATTGRU = space(5)
  * --- WorkFile variables
  TRW_ORGA_idx=0
  RIS_ESPLO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if ! Used("ORGANIGR")
      this.pTIPOPE = IIF(VARTYPE(this.pTIPOPE)<>"C", "CHK", this.pTIPOPE)
      this.w_MSGERROR = ""
      this.w_TRWERROR = .F.
      if this.pTIPOPE=="SAV" OR this.pTIPOPE=="ALL"
        addmsgnl("%0"+REPL("-",30)+"%0", this.oParentObject.oParentObject)
        addmsgnl("Inizio elaborazione - %1", this.oParentObject.oParentObject, TTOC(DATETIME()))
        addmsgnl("%1Inizio elaborazione treeview - %2", this.oParentObject.oParentObject, CHR(9), TTOC(DATETIME()))
      endif
      VQ_EXEC("QUERY\GSAR_BTW.VQR", this, "RISORSE")
      VQ_EXEC("QUERY\GSAR1BTW.VQR", this, "STRUTTUR")
      CREATE CURSOR "ORGANIGR" ( CODICE C(5), DESCRI C(101), TIPRIS C(1), GRPPAD C(5), GRPBAS C(5), LEVEL N(5), LVLKEY C(240), FLRESP C(1), FLVISIB C(1) )
      INSERT INTO "ORGANIGR" SELECT DPCODICE, DPDESCRI, DPTIPRIS, SPACE(5), DPCODICE, 1 , "Z"+ALLTRIM(DPCODICE), "N", DPFLVISI FROM RISORSE WHERE DPTIPRIS="G" AND DPCODICE NOT IN (SELECT SRRISCOL FROM STRUTTUR INNER JOIN RISORSE ON RISORSE.DPCODICE=STRUTTUR.SRRISCOL WHERE RISORSE.DPTIPRIS="G")
      if RECCOUNT("Organigr")>0
        SELECT "Organigr"
        GO TOP
        this.w_ROOTCODE = ALLTRIM(Organigr.CODICE)
        SCAN
        this.w_RECATT = RECNO()
        if ORGANIGR.TIPRIS="G"
          this.w_CURNODE = Organigr.CODICE
          this.w_CURLVLKEY = ALLTRIM(Organigr.LVLKEY)
          this.w_CURLEVEL = Organigr.LEVEL
          INSERT INTO "ORGANIGR" SELECT DPCODICE, DPDESCRI, DPTIPRIS, this.w_CURNODE, IIF(DPTIPRIS="G", DPCODICE, this.w_CURNODE), this.w_CURLEVEL + 1, this.w_CURLVLKEY + "." + ICASE(DPTIPRIS="P", IIF(NVL(SRFLRESP, "N")="S", "A","B" ) , DPTIPRIS="M", "Y", "Z") + ALLTRIM(DPCODICE), IIF(EMPTY(NVL(SRFLRESP, "N")), "N", SRFLRESP), DPFLVISI FROM RISORSE INNER JOIN STRUTTUR ON RISORSE.DPCODICE = STRUTTUR.SRRISCOL WHERE STRUTTUR.SRCODICE= this.w_CURNODE
          if this.pTIPOPE=="CHK" OR this.pTIPOPE=="ALL"
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if this.w_TRWERROR
            exit
          else
            SELECT "Organigr"
            GOTO this.w_RECATT
          endif
        endif
        ENDSCAN
        if this.pTIPOPE=="SAV" OR this.pTIPOPE=="ALL"
          addmsgnl("%1Fine elaborazione treeview - %2", this.oParentObject.oParentObject, CHR(9), TTOC(DATETIME()))
        endif
        if !this.w_TRWERROR AND (this.pTIPOPE=="SAV" OR this.pTIPOPE=="ALL")
          * --- Inserisco cursore organigramma in tabella database
          * --- begin transaction
          cp_BeginTrs()
          * --- Try
          local bErr_039DECC0
          bErr_039DECC0=bTrsErr
          this.Try_039DECC0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
          bTrsErr=bTrsErr or bErr_039DECC0
          * --- End
        endif
      else
        this.w_MSGERROR = ah_MsgFormat("Impossibile individuare gruppo radice della struttura")
        this.w_TRWERROR = .T.
      endif
      USE IN SELECT("RISORSE")
      USE IN SELECT("STRUTTUR")
      USE IN SELECT("ORGANIGR")
      USE IN SELECT("PERSONE")
      USE IN SELECT("RisAss")
    else
      this.w_MSGERROR = ah_MsgFormat("Attenzione, visualizzazione struttura aziendale aperta procedere alla chiusura per eseguire elaborazione")
    endif
    i_retcode = 'stop'
    i_retval = this.w_MSGERROR
    return
  endproc
  proc Try_039DECC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    addmsgnl("%1Inizio eliminazione struttura aziendale precedente - %2", this.oParentObject.oParentObject, CHR(9), TTOC(DATETIME()))
    * --- Delete from TRW_ORGA
    i_nConn=i_TableProp[this.TRW_ORGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRW_ORGA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    addmsgnl("%1Fine eliminazione struttura aziendale precedente - %2", this.oParentObject.oParentObject, CHR(9), TTOC(DATETIME()))
    addmsgnl("%1Inizio salvataggio struttura aziendale - %2", this.oParentObject.oParentObject, CHR(9), TTOC(DATETIME()))
    SELECT "Organigr"
    GO TOP
    SCAN
    * --- Insert into TRW_ORGA
    i_nConn=i_TableProp[this.TRW_ORGA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TRW_ORGA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TRW_ORGA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TOTIPRIS"+",TOCODICE"+",TODESCRI"+",TOGRPPAD"+",TOGRPBAS"+",TO_LEVEL"+",TOLVLKEY"+",TOFLRESP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(Organigr.TIPRIS),'TRW_ORGA','TOTIPRIS');
      +","+cp_NullLink(cp_ToStrODBC(Organigr.CODICE),'TRW_ORGA','TOCODICE');
      +","+cp_NullLink(cp_ToStrODBC(Organigr.DESCRI),'TRW_ORGA','TODESCRI');
      +","+cp_NullLink(cp_ToStrODBC(Organigr.GRPPAD),'TRW_ORGA','TOGRPPAD');
      +","+cp_NullLink(cp_ToStrODBC(Organigr.GRPBAS),'TRW_ORGA','TOGRPBAS');
      +","+cp_NullLink(cp_ToStrODBC(Organigr.LEVEL),'TRW_ORGA','TO_LEVEL');
      +","+cp_NullLink(cp_ToStrODBC(Organigr.LVLKEY),'TRW_ORGA','TOLVLKEY');
      +","+cp_NullLink(cp_ToStrODBC(Organigr.FLRESP),'TRW_ORGA','TOFLRESP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TOTIPRIS',Organigr.TIPRIS,'TOCODICE',Organigr.CODICE,'TODESCRI',Organigr.DESCRI,'TOGRPPAD',Organigr.GRPPAD,'TOGRPBAS',Organigr.GRPBAS,'TO_LEVEL',Organigr.LEVEL,'TOLVLKEY',Organigr.LVLKEY,'TOFLRESP',Organigr.FLRESP)
      insert into (i_cTable) (TOTIPRIS,TOCODICE,TODESCRI,TOGRPPAD,TOGRPBAS,TO_LEVEL,TOLVLKEY,TOFLRESP &i_ccchkf. );
         values (;
           Organigr.TIPRIS;
           ,Organigr.CODICE;
           ,Organigr.DESCRI;
           ,Organigr.GRPPAD;
           ,Organigr.GRPBAS;
           ,Organigr.LEVEL;
           ,Organigr.LVLKEY;
           ,Organigr.FLRESP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ENDSCAN
    addmsgnl("%1Fine salvataggio struttura aziendale - %2", this.oParentObject.oParentObject, CHR(9), TTOC(DATETIME()))
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    addmsgnl("Fine elaborazione - %1", this.oParentObject.oParentObject, TTOC(DATETIME()))
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifiche ricorsione
    SELECT LVLKEY FROM ORGANIGR WHERE OCCURS(ALLTRIM(CODICE) , ALLTRIM(LVLKEY))>1 AND TIPRIS="G" INTO CURSOR "CHKRICOR"
    if USED("CHKRICOR")
      SELECT "CHKRICOR"
      GO TOP
      this.w_CURLVLKEY = ALLTRIM(NVL(CHKRICOR.LVLKEY, " "))
      if !EMPTY(this.w_CURLVLKEY)
        ALINES(L_ARRLVLKEY, this.w_CURLVLKEY, 1, ".")
        this.w_MSGERROR = ah_msgformat( "Rilevata struttura ricorsiva. Gruppo %1 figlio di %2", ALLTRIM(L_ARRLVLKEY(ALEN(L_ARRLVLKEY,1))), ALLTRIM(L_ARRLVLKEY(ALEN(L_ARRLVLKEY,1)-1)) )
        this.w_TRWERROR = .T.
      endif
      USE IN SELECT("CHKRICOR")
    endif
    if !this.w_TRWERROR
      * --- Verifiche unilizzo univoco gruppi
      SELECT COUNT(CODICE) AS CONTA, GRPBAS FROM "ORGANIGR" GROUP BY GRPBAS WHERE TIPRIS="G" HAVING CONTA>1 INTO CURSOR "CHKUNIVO"
      if USED("CHKUNIVO")
        SELECT "CHKUNIVO"
        GO TOP
        this.w_CURLVLKEY = ALLTRIM(NVL(CHKUNIVO.GRPBAS, " "))
        if !EMPTY(this.w_CURLVLKEY)
          this.w_MSGERROR = ah_msgformat( "Gruppo %1 gi� utilizzato.", this.w_CURLVLKEY )
          this.w_TRWERROR = .T.
        endif
        USE IN SELECT("CHKUNIVO")
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    addmsgnl("%1Inizio eliminazione dati precedenti persone/risorse esplosi - %2", this.oParentObject.oParentObject, CHR(9), TTOC(DATETIME()))
    * --- Svuoto tabella risorse esplose
    * --- Delete from RIS_ESPLO
    i_nConn=i_TableProp[this.RIS_ESPLO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESPLO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    addmsgnl("%1Fine eliminazione dati precedenti persone/risorse esplosi - %2", this.oParentObject.oParentObject, CHR(9), TTOC(DATETIME()))
    * --- Inserimento figli della root di primo livello e i figli di primo livello degli utenti amministratori
    addmsgnl("%1Inizio inserimento dati persone/risorse esplosi - %2", this.oParentObject.oParentObject, CHR(9), TTOC(DATETIME()))
    select CODICE, DESCRI, GRPBAS, FLRESP, LEVEL, LVLKEY from "ORGANIGR" where (TIPRIS = "P") ORDER BY Codice Into cursor "Persone"
    SELECT "Persone"
    GO TOP
    SCAN
    this.w_GRUBAS = Persone.GRPBAS
    this.w_FLRESP = Persone.FLRESP
    this.w_CODRIS = Persone.CODICE
    this.w_CURLEVEL = Persone.LEVEL
    this.w_GRPPAD = ALLTRIM(Persone.LVLKEY)
    this.w_GRPPAD = LEFT(this.w_GRPPAD, LEN(this.w_GRPPAD) - 7 ) + "%"
    SELECT "Organigr"
    GO TOP
    LOCATE FOR CODICE=this.w_GRUBAS
    this.w_FLVISIB = FLVISIB
    SELECT "Persone"
    ah_msg("Analisi persona %1 (%2) livello %3", .T. , , ,ALLTRIM(this.w_CODRIS), ALLTRIM(Persone.DESCRI), ALLTRIM(STR(this.w_CURLEVEL)))
    * --- Estrazione persone/risorse/gruppi a cui la persona pu� aver accesso
    if this.w_FLRESP="S" 
      if Not Empty(this.w_GRUBAS)
        * --- Se responsabile eseguo ricerca elenco risorse visibili
         
 w_CURSORE=sys(2015) 
 create cursor (w_CURSORE) (RISORSA C(5),GRUPPO C(5)) 
 a=getlisris(this.w_GRUBAS,0,w_CURSORE)
         
 SELECT * FROM Organigr INNER JOIN (w_CURSORE) ON GRUPPO=Organigr.GRPBAS AND RISORSA=Organigr.CODICE INTO CURSOR "RisAss"
        USE IN SELECT(w_CURSORE)
      endif
    else
      SELECT * FROM Organigr WHERE ((this.w_FLRESP="S" AND lvlkey like "%") OR (this.w_FLRESP="N" AND ((this.w_FLVISIB="T" AND (GrpBas=this.w_GRUBAS or GrpPad=this.w_GRUBAS )) OR (this.w_FLVISIB="S" AND (Codice=this.w_GRUBAS OR (Codice=this.w_CODRIS AND Grpbas=this.w_GRUBAS)))))) INTO CURSOR "RisAss"
    endif
    if USED("RisAss")
      SELECT "RisAss"
      GO TOP
      SCAN
      if TipRis="G"
        this.w_ATTGRU = ALLTRIM(Codice)
      else
        if ALLTRIM(GrpBas)<>this.w_ATTGRU
          this.w_ATTGRU = ALLTRIM(GrpBas)
        endif
      endif
      * --- Insert into RIS_ESPLO
      i_nConn=i_TableProp[this.RIS_ESPLO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIS_ESPLO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RIS_ESPLO_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"REGRUAPP"+",RECODRIS"+",RELIVRIS"+",REFLADMI"+",RETIPRAS"+",RECODRAS"+",RELIVRAS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_ATTGRU),'RIS_ESPLO','REGRUAPP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODRIS),'RIS_ESPLO','RECODRIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CURLEVEL),'RIS_ESPLO','RELIVRIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLRESP),'RIS_ESPLO','REFLADMI');
        +","+cp_NullLink(cp_ToStrODBC(TipRis),'RIS_ESPLO','RETIPRAS');
        +","+cp_NullLink(cp_ToStrODBC(Codice),'RIS_ESPLO','RECODRAS');
        +","+cp_NullLink(cp_ToStrODBC(Level),'RIS_ESPLO','RELIVRAS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'REGRUAPP',this.w_ATTGRU,'RECODRIS',this.w_CODRIS,'RELIVRIS',this.w_CURLEVEL,'REFLADMI',this.w_FLRESP,'RETIPRAS',TipRis,'RECODRAS',Codice,'RELIVRAS',Level)
        insert into (i_cTable) (REGRUAPP,RECODRIS,RELIVRIS,REFLADMI,RETIPRAS,RECODRAS,RELIVRAS &i_ccchkf. );
           values (;
             this.w_ATTGRU;
             ,this.w_CODRIS;
             ,this.w_CURLEVEL;
             ,this.w_FLRESP;
             ,TipRis;
             ,Codice;
             ,Level;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      SELECT "RisAss"
      ENDSCAN
    endif
    USE IN SELECT("RisAss")
    SELECT "Persone"
    ENDSCAN
    addmsgnl("%1Fine inserimento dati persone/risorse esplosi - %2", this.oParentObject.oParentObject, CHR(9), TTOC(DATETIME()))
  endproc


  proc Init(oParentObject,pTIPOPE)
    this.pTIPOPE=pTIPOPE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='TRW_ORGA'
    this.cWorkTables[2]='RIS_ESPLO'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOPE"
endproc
