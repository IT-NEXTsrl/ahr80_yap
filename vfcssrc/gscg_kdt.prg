* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kdt                                                        *
*              Visualizza utilizzo dichiarazioni                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-04-03                                                      *
* Last revis.: 2017-12-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kdt",oParentObject))

* --- Class definition
define class tgscg_kdt as StdForm
  Top    = 0
  Left   = 14

  * --- Standard Properties
  Width  = 751
  Height = 578
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-12-07"
  HelpContextID=82453865
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  BUSIUNIT_IDX = 0
  TIP_DOCU_IDX = 0
  AZIENDA_IDX = 0
  DIC_INTE_IDX = 0
  cPrg = "gscg_kdt"
  cComment = "Visualizza utilizzo dichiarazioni"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FLVEAC = space(1)
  o_FLVEAC = space(1)
  w_CLADOC = space(2)
  o_CLADOC = space(2)
  w_FILDOC = space(2)
  w_TIPDOC = space(5)
  w_DESDOC = space(35)
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_FORSEL = space(15)
  w_CLISEL = space(15)
  w_DATINI = ctod('  /  /  ')
  w_CODCON = space(10)
  o_CODCON = space(10)
  w_DATFIN = ctod('  /  /  ')
  w_DOCINI = ctod('  /  /  ')
  w_DOCFIN = ctod('  /  /  ')
  w_RIFDIC = space(15)
  w_NUMDIC = 0
  w_ANNDIC = space(4)
  w_DATDIC = ctod('  /  /  ')
  w_SERIALE = space(10)
  w_DESCON = space(40)
  w_PARAME = space(3)
  w_TCATDOC = space(2)
  w_TFLVEAC = space(1)
  w_TIPATT = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_SERDOC = space(3)
  w_CLADOC = space(2)
  w_ANCODICE = space(15)
  w_ANTIPCON = space(1)
  w_DDIC = ctod('  /  /  ')
  w_NDIC = 0
  w_TIPO = space(1)
  w_SERIE = space(5)
  w_NPROT = 0
  w_SERDIC = space(10)
  w_ADIC = space(4)
  w_DATA = ctod('  /  /  ')
  w_DIIMPDIC = 0
  w_DISERIAL = space(15)
  w_DIIMPUTI = 0
  w_DIIMPRES = 0
  w_ZoomDoc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kdtPag1","gscg_kdt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLVEAC_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomDoc = this.oPgFrm.Pages(1).oPag.ZoomDoc
    DoDefault()
    proc Destroy()
      this.w_ZoomDoc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='BUSIUNIT'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='DIC_INTE'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLVEAC=space(1)
      .w_CLADOC=space(2)
      .w_FILDOC=space(2)
      .w_TIPDOC=space(5)
      .w_DESDOC=space(35)
      .w_TIPCON=space(1)
      .w_FORSEL=space(15)
      .w_CLISEL=space(15)
      .w_DATINI=ctod("  /  /  ")
      .w_CODCON=space(10)
      .w_DATFIN=ctod("  /  /  ")
      .w_DOCINI=ctod("  /  /  ")
      .w_DOCFIN=ctod("  /  /  ")
      .w_RIFDIC=space(15)
      .w_NUMDIC=0
      .w_ANNDIC=space(4)
      .w_DATDIC=ctod("  /  /  ")
      .w_SERIALE=space(10)
      .w_DESCON=space(40)
      .w_PARAME=space(3)
      .w_TCATDOC=space(2)
      .w_TFLVEAC=space(1)
      .w_TIPATT=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_SERDOC=space(3)
      .w_CLADOC=space(2)
      .w_ANCODICE=space(15)
      .w_ANTIPCON=space(1)
      .w_DDIC=ctod("  /  /  ")
      .w_NDIC=0
      .w_TIPO=space(1)
      .w_SERIE=space(5)
      .w_NPROT=0
      .w_SERDIC=space(10)
      .w_ADIC=space(4)
      .w_DATA=ctod("  /  /  ")
      .w_DIIMPDIC=0
      .w_DISERIAL=space(15)
      .w_DIIMPUTI=0
      .w_DIIMPRES=0
        .w_FLVEAC = 'A'
        .w_CLADOC = 'DT'
        .w_FILDOC = .w_CLADOC
        .w_TIPDOC = SPACE(5)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_TIPDOC))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_TIPCON = iif(.w_FLVEAC='A','F','C')
        .w_FORSEL = SPACE(15)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_FORSEL))
          .link_1_7('Full')
        endif
        .w_CLISEL = SPACE(15)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CLISEL))
          .link_1_8('Full')
        endif
        .w_DATINI = g_INIESE
        .w_CODCON = IIF(.w_TIPCON = 'F', .w_FORSEL, .w_CLISEL)
        .w_DATFIN = g_FINESE
        .w_DOCINI = g_INIESE
        .w_DOCFIN = g_FINESE
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_RIFDIC))
          .link_1_14('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
          .DoRTCalc(15,17,.f.)
        .w_SERIALE = .w_ZoomDoc.getVar('MVSERIAL')
          .DoRTCalc(19,19,.f.)
        .w_PARAME = .w_ZoomDoc.getVar('MVFLVEAC')+.w_ZoomDoc.getVar('MVCLADOC')
          .DoRTCalc(21,22,.f.)
        .w_TIPATT = 'A'
        .w_OBTEST = i_INIDAT
      .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
          .DoRTCalc(25,26,.f.)
        .w_CLADOC = 'DT'
        .w_ANCODICE = .w_CLISEL
        .w_ANTIPCON = .w_TIPCON
        .w_DDIC = .w_ZoomDoc.getVar('DIDATLET')
        .w_NDIC = .w_ZoomDoc.getVar('DINUMDOC')
        .w_TIPO = .w_ZoomDoc.getVar('TIPO')
        .w_SERIE = .w_ZoomDoc.getVar('DISERDOC')
        .w_NPROT = .w_ZoomDoc.getVar('DINUMDIC')
        .w_SERDIC = .w_ZoomDoc.getVar('DISERDIC')
        .w_ADIC = .w_ZoomDoc.getVar('DI__ANNO')
        .w_DATA = .w_ZoomDoc.getVar('DIDATDIC')
        .w_DIIMPDIC = .w_ZoomDoc.getVar('DIIMPDIC')
      .oPgFrm.Page1.oPag.oObj_1_60.Calculate(Ah_MsgFormat(IIF(NOT EMPTY(.w_SERIALE),"Rif. dich: N.doc."+' '+alltrim(str(.w_NDIC))+IIF(Not Empty(.w_SERIE),'/'+Alltrim(.w_SERIE),'')+' '+'del'+' '+dtoc(.w_DDIC)+' '+'N. prot.:'+' '+alltrim(str(.w_NPROT))+IIF(Not Empty(.w_SERDIC),'/'+Alltrim(.w_SERDIC),'')+"/"+ alltrim(.w_ADIC)+' '+'del'+' '+dtoc(.w_DATA)+'  '+ah_MsgFormat("Importo:"),'')))
      .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .w_DISERIAL = .w_ZoomDoc.getVar('DISERIAL')
      .oPgFrm.Page1.oPag.oObj_1_65.Calculate(Ah_MsgFormat(IIF(NOT EMPTY(.w_SERIALE),("Importo utilizzato:"),'')))
      .oPgFrm.Page1.oPag.oObj_1_66.Calculate(Ah_MsgFormat(IIF(NOT EMPTY(.w_SERIALE),("Importo residuo:"),'')))
        .w_DIIMPUTI = .w_ZoomDoc.getVar('DIIMPUTI')
        .w_DIIMPRES = .w_ZoomDoc.getVar('DIIMPRES')
      .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_CLADOC<>.w_CLADOC
            .w_FILDOC = .w_CLADOC
        endif
        if .o_FLVEAC<>.w_FLVEAC.or. .o_CLADOC<>.w_CLADOC
            .w_TIPDOC = SPACE(5)
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.t.)
        if .o_FLVEAC<>.w_FLVEAC
            .w_TIPCON = iif(.w_FLVEAC='A','F','C')
        endif
        if .o_TIPCON<>.w_TIPCON
          .link_1_7('Full')
        endif
        if .o_TIPCON<>.w_TIPCON
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.t.)
            .w_CODCON = IIF(.w_TIPCON = 'F', .w_FORSEL, .w_CLISEL)
        .DoRTCalc(11,13,.t.)
          .link_1_14('Full')
        .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
        .DoRTCalc(15,17,.t.)
            .w_SERIALE = .w_ZoomDoc.getVar('MVSERIAL')
        .DoRTCalc(19,19,.t.)
            .w_PARAME = .w_ZoomDoc.getVar('MVFLVEAC')+.w_ZoomDoc.getVar('MVCLADOC')
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .DoRTCalc(21,27,.t.)
            .w_ANCODICE = .w_CLISEL
            .w_ANTIPCON = .w_TIPCON
        if .o_CODCON<>.w_CODCON
          .Calculate_HEYUGRWZZZ()
        endif
            .w_DDIC = .w_ZoomDoc.getVar('DIDATLET')
            .w_NDIC = .w_ZoomDoc.getVar('DINUMDOC')
            .w_TIPO = .w_ZoomDoc.getVar('TIPO')
            .w_SERIE = .w_ZoomDoc.getVar('DISERDOC')
            .w_NPROT = .w_ZoomDoc.getVar('DINUMDIC')
            .w_SERDIC = .w_ZoomDoc.getVar('DISERDIC')
            .w_ADIC = .w_ZoomDoc.getVar('DI__ANNO')
            .w_DATA = .w_ZoomDoc.getVar('DIDATDIC')
            .w_DIIMPDIC = .w_ZoomDoc.getVar('DIIMPDIC')
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate(Ah_MsgFormat(IIF(NOT EMPTY(.w_SERIALE),"Rif. dich: N.doc."+' '+alltrim(str(.w_NDIC))+IIF(Not Empty(.w_SERIE),'/'+Alltrim(.w_SERIE),'')+' '+'del'+' '+dtoc(.w_DDIC)+' '+'N. prot.:'+' '+alltrim(str(.w_NPROT))+IIF(Not Empty(.w_SERDIC),'/'+Alltrim(.w_SERDIC),'')+"/"+ alltrim(.w_ADIC)+' '+'del'+' '+dtoc(.w_DATA)+'  '+ah_MsgFormat("Importo:"),'')))
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
            .w_DISERIAL = .w_ZoomDoc.getVar('DISERIAL')
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate(Ah_MsgFormat(IIF(NOT EMPTY(.w_SERIALE),("Importo utilizzato:"),'')))
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate(Ah_MsgFormat(IIF(NOT EMPTY(.w_SERIALE),("Importo residuo:"),'')))
            .w_DIIMPUTI = .w_ZoomDoc.getVar('DIIMPUTI')
            .w_DIIMPRES = .w_ZoomDoc.getVar('DIIMPRES')
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_60.Calculate(Ah_MsgFormat(IIF(NOT EMPTY(.w_SERIALE),"Rif. dich: N.doc."+' '+alltrim(str(.w_NDIC))+IIF(Not Empty(.w_SERIE),'/'+Alltrim(.w_SERIE),'')+' '+'del'+' '+dtoc(.w_DDIC)+' '+'N. prot.:'+' '+alltrim(str(.w_NPROT))+IIF(Not Empty(.w_SERDIC),'/'+Alltrim(.w_SERDIC),'')+"/"+ alltrim(.w_ADIC)+' '+'del'+' '+dtoc(.w_DATA)+'  '+ah_MsgFormat("Importo:"),'')))
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate(Ah_MsgFormat(IIF(NOT EMPTY(.w_SERIALE),("Importo utilizzato:"),'')))
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate(Ah_MsgFormat(IIF(NOT EMPTY(.w_SERIALE),("Importo residuo:"),'')))
        .oPgFrm.Page1.oPag.oObj_1_70.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate()
    endwith
  return

  proc Calculate_HEYUGRWZZZ()
    with this
          * --- Azzero i campi relativi al rif. dichiazione
          .w_RIFDIC = ' '
          .link_1_14('Full')
    endwith
  endproc
  proc Calculate_YTPYOGOLAQ()
    with this
          * --- Apro dichiarazione
          .w_RET = OPENGEST('A','GSCG_ADI','DISERIAL',.w_DISERIAL)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFORSEL_1_7.enabled = this.oPgFrm.Page1.oPag.oFORSEL_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCLISEL_1_8.enabled = this.oPgFrm.Page1.oPag.oCLISEL_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCLADOC_1_2.visible=!this.oPgFrm.Page1.oPag.oCLADOC_1_2.mHide()
    this.oPgFrm.Page1.oPag.oFORSEL_1_7.visible=!this.oPgFrm.Page1.oPag.oFORSEL_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCLISEL_1_8.visible=!this.oPgFrm.Page1.oPag.oCLISEL_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCLADOC_1_47.visible=!this.oPgFrm.Page1.oPag.oCLADOC_1_47.mHide()
    this.oPgFrm.Page1.oPag.oDIIMPDIC_1_59.visible=!this.oPgFrm.Page1.oPag.oDIIMPDIC_1_59.mHide()
    this.oPgFrm.Page1.oPag.oDIIMPUTI_1_67.visible=!this.oPgFrm.Page1.oPag.oDIIMPUTI_1_67.mHide()
    this.oPgFrm.Page1.oPag.oDIIMPRES_1_68.visible=!this.oPgFrm.Page1.oPag.oDIIMPRES_1_68.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomDoc.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_60.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_62.Event(cEvent)
        if lower(cEvent)==lower("w_ZoomDoc selected")
          .Calculate_YTPYOGOLAQ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_65.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_66.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_70.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_72.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPDOC
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BLT',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPDOC))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPDOC_1_4'),i_cWhere,'GSAR_BLT',"Tipi documenti",'GSVE1QZM.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_TCATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_TFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_TCATDOC = space(2)
      this.w_TFLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TFLVEAC=.w_FLVEAC AND (.w_TCATDOC=.w_CLADOC)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente")
        endif
        this.w_TIPDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_TCATDOC = space(2)
        this.w_TFLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORSEL
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_FORSEL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORSEL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_FORSEL)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FORSEL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORSEL_1_7'),i_cWhere,'GSAR_AFR',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORSEL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_FORSEL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORSEL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_FORSEL = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        endif
        this.w_FORSEL = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLISEL
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLISEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACL',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CLISEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CLISEL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLISEL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CLISEL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CLISEL)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CLISEL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCLISEL_1_8'),i_cWhere,'GSAR_ACL',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLISEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CLISEL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CLISEL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLISEL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CLISEL = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
        endif
        this.w_CLISEL = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLISEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RIFDIC
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIC_INTE_IDX,3]
    i_lTable = "DIC_INTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2], .t., this.DIC_INTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RIFDIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RIFDIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DISERIAL,DINUMDOC,DIDATDIC,DI__ANNO,DISERDOC";
                   +" from "+i_cTable+" "+i_lTable+" where DISERIAL="+cp_ToStrODBC(this.w_RIFDIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DISERIAL',this.w_RIFDIC)
            select DISERIAL,DINUMDOC,DIDATDIC,DI__ANNO,DISERDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RIFDIC = NVL(_Link_.DISERIAL,space(15))
      this.w_NUMDIC = NVL(_Link_.DINUMDOC,0)
      this.w_DATDIC = NVL(cp_ToDate(_Link_.DIDATDIC),ctod("  /  /  "))
      this.w_ANNDIC = NVL(_Link_.DI__ANNO,space(4))
      this.w_SERDOC = NVL(_Link_.DISERDOC,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_RIFDIC = space(15)
      endif
      this.w_NUMDIC = 0
      this.w_DATDIC = ctod("  /  /  ")
      this.w_ANNDIC = space(4)
      this.w_SERDOC = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIC_INTE_IDX,2])+'\'+cp_ToStr(_Link_.DISERIAL,1)
      cp_ShowWarn(i_cKey,this.DIC_INTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RIFDIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLVEAC_1_1.RadioValue()==this.w_FLVEAC)
      this.oPgFrm.Page1.oPag.oFLVEAC_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADOC_1_2.RadioValue()==this.w_CLADOC)
      this.oPgFrm.Page1.oPag.oCLADOC_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_4.value==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_4.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_5.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_5.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_6.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFORSEL_1_7.value==this.w_FORSEL)
      this.oPgFrm.Page1.oPag.oFORSEL_1_7.value=this.w_FORSEL
    endif
    if not(this.oPgFrm.Page1.oPag.oCLISEL_1_8.value==this.w_CLISEL)
      this.oPgFrm.Page1.oPag.oCLISEL_1_8.value=this.w_CLISEL
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_9.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_9.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_11.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_11.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCINI_1_12.value==this.w_DOCINI)
      this.oPgFrm.Page1.oPag.oDOCINI_1_12.value=this.w_DOCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDOCFIN_1_13.value==this.w_DOCFIN)
      this.oPgFrm.Page1.oPag.oDOCFIN_1_13.value=this.w_DOCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDIC_1_15.value==this.w_NUMDIC)
      this.oPgFrm.Page1.oPag.oNUMDIC_1_15.value=this.w_NUMDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oANNDIC_1_16.value==this.w_ANNDIC)
      this.oPgFrm.Page1.oPag.oANNDIC_1_16.value=this.w_ANNDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDIC_1_17.value==this.w_DATDIC)
      this.oPgFrm.Page1.oPag.oDATDIC_1_17.value=this.w_DATDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_33.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_33.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oSERDOC_1_46.value==this.w_SERDOC)
      this.oPgFrm.Page1.oPag.oSERDOC_1_46.value=this.w_SERDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADOC_1_47.RadioValue()==this.w_CLADOC)
      this.oPgFrm.Page1.oPag.oCLADOC_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIIMPDIC_1_59.value==this.w_DIIMPDIC)
      this.oPgFrm.Page1.oPag.oDIIMPDIC_1_59.value=this.w_DIIMPDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDIIMPUTI_1_67.value==this.w_DIIMPUTI)
      this.oPgFrm.Page1.oPag.oDIIMPUTI_1_67.value=this.w_DIIMPUTI
    endif
    if not(this.oPgFrm.Page1.oPag.oDIIMPRES_1_68.value==this.w_DIIMPRES)
      this.oPgFrm.Page1.oPag.oDIIMPRES_1_68.value=this.w_DIIMPRES
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TFLVEAC=.w_FLVEAC AND (.w_TCATDOC=.w_CLADOC))  and not(empty(.w_TIPDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPDOC_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(!.w_tipcon ='F')  and (.w_tipcon ='F')  and not(empty(.w_FORSEL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFORSEL_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(!.w_tipcon ='C')  and (.w_tipcon ='C')  and not(empty(.w_CLISEL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLISEL_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice cliente inesistente oppure obsoleto")
          case   ((empty(.w_DATINI)) or not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_9.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DATFIN)) or not(.w_DATINI<=.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_11.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DOCINI)) or not(.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCINI_1_12.SetFocus()
            i_bnoObbl = !empty(.w_DOCINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DOCFIN)) or not(.w_DOCINI<=.w_DOCFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDOCFIN_1_13.SetFocus()
            i_bnoObbl = !empty(.w_DOCFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLVEAC = this.w_FLVEAC
    this.o_CLADOC = this.w_CLADOC
    this.o_TIPCON = this.w_TIPCON
    this.o_CODCON = this.w_CODCON
    return

enddefine

* --- Define pages as container
define class tgscg_kdtPag1 as StdContainer
  Width  = 747
  height = 578
  stdWidth  = 747
  stdheight = 578
  resizeXpos=633
  resizeYpos=250
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFLVEAC_1_1 as StdCombo with uid="ZQSVNGVRJV",rtseq=1,rtrep=.f.,left=93,top=6,width=83,height=21;
    , ToolTipText = "Tipo gestione interessata";
    , HelpContextID = 227505322;
    , cFormVar="w_FLVEAC",RowSource=""+"Vendite,"+"Acquisti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLVEAC_1_1.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oFLVEAC_1_1.GetRadio()
    this.Parent.oContained.w_FLVEAC = this.RadioValue()
    return .t.
  endfunc

  func oFLVEAC_1_1.SetRadio()
    this.Parent.oContained.w_FLVEAC=trim(this.Parent.oContained.w_FLVEAC)
    this.value = ;
      iif(this.Parent.oContained.w_FLVEAC=='V',1,;
      iif(this.Parent.oContained.w_FLVEAC=='A',2,;
      0))
  endfunc


  add object oCLADOC_1_2 as StdCombo with uid="DVOJYWQWFC",rtseq=2,rtrep=.f.,left=248,top=5,width=142,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 212976858;
    , cFormVar="w_CLADOC",RowSource=""+"Documenti interni,"+"Ordini,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ordini previsionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLADOC_1_2.RadioValue()
    return(iif(this.value =1,'DI',;
    iif(this.value =2,'OR',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    iif(this.value =6,'OP',;
    space(2))))))))
  endfunc
  func oCLADOC_1_2.GetRadio()
    this.Parent.oContained.w_CLADOC = this.RadioValue()
    return .t.
  endfunc

  func oCLADOC_1_2.SetRadio()
    this.Parent.oContained.w_CLADOC=trim(this.Parent.oContained.w_CLADOC)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOC=='DI',1,;
      iif(this.Parent.oContained.w_CLADOC=='OR',2,;
      iif(this.Parent.oContained.w_CLADOC=='DT',3,;
      iif(this.Parent.oContained.w_CLADOC=='FA',4,;
      iif(this.Parent.oContained.w_CLADOC=='NC',5,;
      iif(this.Parent.oContained.w_CLADOC=='OP',6,;
      0))))))
  endfunc

  func oCLADOC_1_2.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION) = "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oTIPDOC_1_4 as StdField with uid="JAINMPNBRJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente",;
    ToolTipText = "Codice tipo documento di selezione",;
    HelpContextID = 212915914,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=423, Top=7, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAR_BLT", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPDOC"

  func oTIPDOC_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPDOC_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPDOC_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPDOC_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BLT',"Tipi documenti",'GSVE1QZM.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPDOC_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BLT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPDOC
     i_obj.ecpSave()
  endproc

  add object oDESDOC_1_5 as StdField with uid="GLKHKTUIJU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 212904906,;
   bGlobalFont=.t.,;
    Height=21, Width=212, Left=529, Top=7, InputMask=replicate('X',35)


  add object oTIPCON_1_6 as StdCombo with uid="GAGLAKOTEP",rtseq=6,rtrep=.f.,left=93,top=35,width=84,height=21;
    , ToolTipText = "Tipo intestatario - cliente o fornitore";
    , HelpContextID = 28432074;
    , cFormVar="w_TIPCON",RowSource=""+"Clienti,"+"Fornitori", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_6.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oTIPCON_1_6.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_6.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      0))
  endfunc

  func oTIPCON_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_FORSEL)
        bRes2=.link_1_7('Full')
      endif
      if .not. empty(.w_CLISEL)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oFORSEL_1_7 as StdField with uid="PFRQYKBUPL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_FORSEL", cQueryName = "FORSEL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente oppure obsoleto",;
    ToolTipText = "Fornitore di selezione scadenze",;
    HelpContextID = 71414186,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=192, Top=35, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORSEL"

  func oFORSEL_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_tipcon ='F')
    endwith
   endif
  endfunc

  func oFORSEL_1_7.mHide()
    with this.Parent.oContained
      return (!.w_tipcon ='F')
    endwith
  endfunc

  func oFORSEL_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORSEL_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORSEL_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORSEL_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'',this.parent.oContained
  endproc
  proc oFORSEL_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_FORSEL
     i_obj.ecpSave()
  endproc

  add object oCLISEL_1_8 as StdField with uid="BXVLTGMVIP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CLISEL", cQueryName = "CLISEL",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice cliente inesistente oppure obsoleto",;
    ToolTipText = "Cliente di selezione scadenze",;
    HelpContextID = 71451866,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=192, Top=35, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_ACL", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CLISEL"

  func oCLISEL_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_tipcon ='C')
    endwith
   endif
  endfunc

  func oCLISEL_1_8.mHide()
    with this.Parent.oContained
      return (!.w_tipcon ='C')
    endwith
  endfunc

  func oCLISEL_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLISEL_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLISEL_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCLISEL_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACL',"Clienti",'',this.parent.oContained
  endproc
  proc oCLISEL_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CLISEL
     i_obj.ecpSave()
  endproc

  add object oDATINI_1_9 as StdField with uid="VQIOCNGNZD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione di inizio selezione",;
    HelpContextID = 112959434,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=93, Top=67

  func oDATINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_11 as StdField with uid="WTJZXNNZRA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data registrazione di fine selezione",;
    HelpContextID = 34512842,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=259, Top=67

  func oDATFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oDOCINI_1_12 as StdField with uid="AQIFKTREOL",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DOCINI", cQueryName = "DOCINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento di inizio selezione",;
    HelpContextID = 113025482,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=419, Top=67

  func oDOCINI_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN OR EMPTY(.w_DOCFIN))
    endwith
    return bRes
  endfunc

  add object oDOCFIN_1_13 as StdField with uid="CNUAUUXTHP",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DOCFIN", cQueryName = "DOCFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento di fine selezione",;
    HelpContextID = 34578890,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=598, Top=67

  func oDOCFIN_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DOCINI<=.w_DOCFIN)
    endwith
    return bRes
  endfunc

  add object oNUMDIC_1_15 as StdField with uid="XAAHAAWCIG",rtseq=15,rtrep=.f.,;
    cFormVar = "w_NUMDIC", cQueryName = "NUMDIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero della eventuale dichiarazione di esenzione",;
    HelpContextID = 219216682,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=93, Top=100

  add object oANNDIC_1_16 as StdField with uid="VRNLSZZRRL",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ANNDIC", cQueryName = "ANNDIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno della eventuale dichiarazione di esenzione",;
    HelpContextID = 219214586,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=215, Top=100, InputMask=replicate('X',4)

  add object oDATDIC_1_17 as StdField with uid="MUZRFQLKAF",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DATDIC", cQueryName = "DATDIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della eventuale dichiarazione di esenzione",;
    HelpContextID = 219193290,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=298, Top=100


  add object oBtn_1_18 as StdButton with uid="PKKSUWHQWC",left=379, top=103, width=20,height=19,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare la dichiarazione di intento";
    , HelpContextID = 82252842;
  , bGlobalFont=.t.

    proc oBtn_1_18.Click()
      do GSCG_KLI with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_CODCON))
      endwith
    endif
  endfunc


  add object oBtn_1_19 as StdButton with uid="VJARRGRPGZ",left=692, top=87, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per rimuovere il riferimento alla dichiarazione di intento";
    , HelpContextID = 1694534;
    , Tabstop=.f.,Caption='E\<limina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSCG_BLD(this.Parent.oContained,"DEL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_RIFDIC ))
      endwith
    endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="QYBPKEREZO",left=692, top=33, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per rieseguire la ricerca con le nuove selezioni";
    , HelpContextID = 198419014;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        GSCG_BLD(this.Parent.oContained,"RIC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_21 as StdButton with uid="IVUYXSPMRW",left=8, top=498, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il documento associato alla riga selezionata";
    , HelpContextID = 219292218;
    , Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_SERIALE, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="OZKTUAQLAH",left=640, top=498, width=48,height=45,;
    CpPicture="bmp\INTENTI.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere all'aggiornamento della data prima applicazione";
    , HelpContextID = 243184998;
    , caption='\<Data appl.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        GSCG_BDL(this.Parent.oContained,"AGGP")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FLVEAC='V' And (.w_CLADOC='FA' Or .w_CLADOC='DT' Or .w_CLADOC='NC') And .w_TIPCON='C')
      endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="PXBGFOADEM",left=691, top=498, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 8492054;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomDoc as cp_zoombox with uid="FMGYZDFNXS",left=5, top=194, width=740,height=301,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSVE_KDT",bOptions=.f.,bQueryOnLoad=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 95543782

  add object oDESCON_1_33 as StdField with uid="AZDEMLKLLW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 28421066,;
   bGlobalFont=.t.,;
    Height=21, Width=354, Left=334, Top=35, InputMask=replicate('X',40)


  add object oObj_1_44 as cp_runprogram with uid="EBQXIWLHIN",left=11, top=583, width=151,height=24,;
    caption='GSCG_BLD(INT)',;
   bGlobalFont=.t.,;
    prg="GSCG_BLD('INT')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 163934678

  add object oSERDOC_1_46 as StdField with uid="HXGMGPIEFF",rtseq=26,rtrep=.f.,;
    cFormVar = "w_SERDOC", cQueryName = "SERDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 212908762,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=163, Top=100, InputMask=replicate('X',3)


  add object oCLADOC_1_47 as StdCombo with uid="FRJFSLJCUX",rtseq=27,rtrep=.f.,left=248,top=5,width=142,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 212976858;
    , cFormVar="w_CLADOC",RowSource=""+"Documenti interni,"+"Ordini,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLADOC_1_47.RadioValue()
    return(iif(this.value =1,'DI',;
    iif(this.value =2,'OR',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    space(2)))))))
  endfunc
  func oCLADOC_1_47.GetRadio()
    this.Parent.oContained.w_CLADOC = this.RadioValue()
    return .t.
  endfunc

  func oCLADOC_1_47.SetRadio()
    this.Parent.oContained.w_CLADOC=trim(this.Parent.oContained.w_CLADOC)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOC=='DI',1,;
      iif(this.Parent.oContained.w_CLADOC=='OR',2,;
      iif(this.Parent.oContained.w_CLADOC=='DT',3,;
      iif(this.Parent.oContained.w_CLADOC=='FA',4,;
      iif(this.Parent.oContained.w_CLADOC=='NC',5,;
      0)))))
  endfunc

  func oCLADOC_1_47.mHide()
    with this.Parent.oContained
      return (UPPER (g_APPLICATION)<>"ADHOC REVOLUTION")
    endwith
  endfunc

  add object oDIIMPDIC_1_59 as StdField with uid="FBXNPHAKRQ",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DIIMPDIC", cQueryName = "DIIMPDIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo utilizzato in dichiarazione comprensivo delle righe di esenzione IVA",;
    HelpContextID = 194529159,;
   bGlobalFont=.t.,;
    Height=21, Width=122, Left=512, Top=498, cSayPict="v_PV(80)", cGetPict="v_PV(80)"

  func oDIIMPDIC_1_59.mHide()
    with this.Parent.oContained
      return (EMPTY(NVL(.w_SERIALE,'')))
    endwith
  endfunc


  add object oObj_1_60 as cp_calclbl with uid="IXPYCUJJKH",left=58, top=498, width=453,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 95543782


  add object oObj_1_62 as cp_showimage with uid="BIRGPRHWUV",left=98, top=177, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    stretch=1,default="bmp\arancio.ico",;
    nPag=1;
    , ToolTipText = "Documento senza codice IVA di esenzione di testata e con codice IVA di esenzione di riga";
    , HelpContextID = 95543782


  add object oObj_1_65 as cp_calclbl with uid="EVTGOHNZBH",left=202, top=528, width=309,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 95543782


  add object oObj_1_66 as cp_calclbl with uid="FUNPRFZZVF",left=202, top=553, width=309,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,bGlobalFont=.t.,;
    nPag=1;
    , HelpContextID = 95543782

  add object oDIIMPUTI_1_67 as StdField with uid="ETHCWGIGKL",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DIIMPUTI", cQueryName = "DIIMPUTI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo utilizzato in dichiarazione comprensivo delle righe di esenzione IVA",;
    HelpContextID = 90683519,;
   bGlobalFont=.t.,;
    Height=21, Width=122, Left=512, Top=526, cSayPict="v_PV(80)", cGetPict="v_PV(80)"

  func oDIIMPUTI_1_67.mHide()
    with this.Parent.oContained
      return (EMPTY(NVL(.w_SERIALE,'')))
    endwith
  endfunc

  add object oDIIMPRES_1_68 as StdField with uid="DRCHQGBBHO",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DIIMPRES", cQueryName = "DIIMPRES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importo utilizzato in dichiarazione comprensivo delle righe di esenzione IVA",;
    HelpContextID = 40351881,;
   bGlobalFont=.t.,;
    Height=21, Width=122, Left=512, Top=551, cSayPict="v_PV(80)", cGetPict="v_PV(80)"

  func oDIIMPRES_1_68.mHide()
    with this.Parent.oContained
      return (EMPTY(NVL(.w_SERIALE,'')))
    endwith
  endfunc


  add object oObj_1_70 as cp_showimage with uid="LIRNOGXEYH",left=98, top=135, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    stretch=1,default="bmp\rosso.ico",;
    nPag=1;
    , ToolTipText = "Documento senza codice IVA di esenzione di testata e con codice IVA di esenzione di riga";
    , HelpContextID = 95543782


  add object oObj_1_72 as cp_showimage with uid="SCPTSWEPXY",left=98, top=156, width=16,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    stretch=1,default="bmp\giallo.ico",;
    nPag=1;
    , ToolTipText = "Documento non presente nel dettaglio importo utilizzato";
    , HelpContextID = 95543782

  add object oStr_1_25 as StdString with uid="KCXNCIRHVM",Visible=.t., Left=6, Top=67,;
    Alignment=1, Width=86, Height=15,;
    Caption="Da data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="JYYRYYQBHK",Visible=.t., Left=177, Top=67,;
    Alignment=1, Width=80, Height=15,;
    Caption="A data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="KUTBRHJKUV",Visible=.t., Left=180, Top=7,;
    Alignment=1, Width=65, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="PEGPUESRJN",Visible=.t., Left=391, Top=7,;
    Alignment=1, Width=31, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="XEJOWZAGNC",Visible=.t., Left=337, Top=67,;
    Alignment=1, Width=80, Height=15,;
    Caption="Da data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="HRIKGDXKQT",Visible=.t., Left=510, Top=67,;
    Alignment=1, Width=76, Height=15,;
    Caption="A data doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="NYIFVFJARA",Visible=.t., Left=6, Top=7,;
    Alignment=1, Width=86, Height=15,;
    Caption="Ciclo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="UERGAHZWIL",Visible=.t., Left=6, Top=35,;
    Alignment=1, Width=86, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="THYYFUEPJR",Visible=.t., Left=201, Top=103,;
    Alignment=2, Width=8, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="MYVZGHLMUE",Visible=.t., Left=265, Top=100,;
    Alignment=1, Width=31, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="PUCRTMLYAO",Visible=.t., Left=6, Top=100,;
    Alignment=1, Width=86, Height=18,;
    Caption="Rif. dich.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="LTWQFAUBZL",Visible=.t., Left=153, Top=103,;
    Alignment=2, Width=8, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="LUGNFBYLPF",Visible=.t., Left=119, Top=176,;
    Alignment=0, Width=210, Height=17,;
    Caption="Codice di esenzione su riga documento"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_69 as StdString with uid="VFGPYUNBLA",Visible=.t., Left=30, Top=134,;
    Alignment=1, Width=62, Height=18,;
    Caption="Legenda:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="MOCBYVPGRY",Visible=.t., Left=119, Top=136,;
    Alignment=0, Width=238, Height=17,;
    Caption="Causale priva di aggiornamento lettere di intento"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_73 as StdString with uid="FGHHWLGFTN",Visible=.t., Left=119, Top=156,;
    Alignment=0, Width=274, Height=17,;
    Caption="Documento non presente nel dettaglio importo utilizzato"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_74 as StdString with uid="RQFCUAIFTZ",Visible=.t., Left=428, Top=136,;
    Alignment=0, Width=203, Height=17,;
    Caption="Documento provvisorio"    , BackStyle=1, BackColor=16744448;
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_75 as StdString with uid="PLGAPCYJSB",Visible=.t., Left=428, Top=156,;
    Alignment=0, Width=203, Height=17,;
    Caption="Importo utilizzato in dich. collegata"    , BackStyle=1, BackColor=65535;
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kdt','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
