* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut1kis                                                        *
*              Manutenzione sincronizzazione indici documenti pratiche         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-29                                                      *
* Last revis.: 2012-11-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut1kis",oParentObject))

* --- Class definition
define class tgsut1kis as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 775
  Height = 479
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-06"
  HelpContextID=1524585
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  cPrg = "gsut1kis"
  cComment = "Manutenzione sincronizzazione indici documenti pratiche"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_STATUS = space(1)
  w_DESCAN = space(100)
  w_PRAAPE = space(15)
  w_FLAPE = space(1)
  w_SELEZIONE = space(1)
  o_SELEZIONE = space(1)
  w_INDICE = space(20)
  w_LSFILENM = space(254)
  w_RET = space(1)
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut1kisPag1","gsut1kis",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTATUS_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CAN_TIER'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_STATUS=space(1)
      .w_DESCAN=space(100)
      .w_PRAAPE=space(15)
      .w_FLAPE=space(1)
      .w_SELEZIONE=space(1)
      .w_INDICE=space(20)
      .w_LSFILENM=space(254)
      .w_RET=space(1)
        .w_STATUS = 'B'
          .DoRTCalc(2,3,.f.)
        .w_FLAPE = 'A'
        .w_SELEZIONE = 'D'
      .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .w_INDICE = .w_Zoom.GetVar("LDSER_ID")
        .w_LSFILENM = .w_Zoom.GetVar("LSFILENM")
    endwith
    this.DoRTCalc(8,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .DoRTCalc(1,5,.t.)
            .w_INDICE = .w_Zoom.GetVar("LDSER_ID")
            .w_LSFILENM = .w_Zoom.GetVar("LSFILENM")
        if .o_SELEZIONE<>.w_SELEZIONE
          .Calculate_IMNWWEQOAJ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
    endwith
  return

  proc Calculate_DUSDWURJMN()
    with this
          * --- Apri indice
          .w_RET = IIF(Not Empty(.w_INDICE), OpenGest('A', "GSUT_AID", "IDSERIAL", .w_INDICE), '')
    endwith
  endproc
  proc Calculate_IMNWWEQOAJ()
    with this
          * --- Seleziona tutti/Deseleziona tutti
          Seleziona_Deseleziona(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDESCAN_1_2.visible=!this.oPgFrm.Page1.oPag.oDESCAN_1_2.mHide()
    this.oPgFrm.Page1.oPag.oPRAAPE_1_3.visible=!this.oPgFrm.Page1.oPag.oPRAAPE_1_3.mHide()
    this.oPgFrm.Page1.oPag.oFLAPE_1_4.visible=!this.oPgFrm.Page1.oPag.oFLAPE_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
        if lower(cEvent)==lower("w_ZOOM selected")
          .Calculate_DUSDWURJMN()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTATUS_1_1.RadioValue()==this.w_STATUS)
      this.oPgFrm.Page1.oPag.oSTATUS_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_2.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_2.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oPRAAPE_1_3.value==this.w_PRAAPE)
      this.oPgFrm.Page1.oPag.oPRAAPE_1_3.value=this.w_PRAAPE
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAPE_1_4.RadioValue()==this.w_FLAPE)
      this.oPgFrm.Page1.oPag.oFLAPE_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZIONE_1_10.RadioValue()==this.w_SELEZIONE)
      this.oPgFrm.Page1.oPag.oSELEZIONE_1_10.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SELEZIONE = this.w_SELEZIONE
    return

enddefine

* --- Define pages as container
define class tgsut1kisPag1 as StdContainer
  Width  = 771
  height = 479
  stdWidth  = 771
  stdheight = 479
  resizeXpos=590
  resizeYpos=368
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oSTATUS_1_1 as StdCombo with uid="JIONEDRRIP",rtseq=1,rtrep=.f.,left=71,top=11,width=210,height=21;
    , ToolTipText = "Stato del file";
    , HelpContextID = 124705242;
    , cFormVar="w_STATUS",RowSource=""+"Provvisori/Indici non creati,"+"Confermati,"+"Provvisori,"+"Ignorati,"+"Indice non creato,"+"Documenti archiviati non pi� esistenti,"+"Copie di backup import zip,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTATUS_1_1.RadioValue()
    return(iif(this.value =1,'B',;
    iif(this.value =2,'C',;
    iif(this.value =3,'P',;
    iif(this.value =4,'S',;
    iif(this.value =5,'I',;
    iif(this.value =6,'F',;
    iif(this.value =7,'Z',;
    iif(this.value =8,'T',;
    space(1))))))))))
  endfunc
  func oSTATUS_1_1.GetRadio()
    this.Parent.oContained.w_STATUS = this.RadioValue()
    return .t.
  endfunc

  func oSTATUS_1_1.SetRadio()
    this.Parent.oContained.w_STATUS=trim(this.Parent.oContained.w_STATUS)
    this.value = ;
      iif(this.Parent.oContained.w_STATUS=='B',1,;
      iif(this.Parent.oContained.w_STATUS=='C',2,;
      iif(this.Parent.oContained.w_STATUS=='P',3,;
      iif(this.Parent.oContained.w_STATUS=='S',4,;
      iif(this.Parent.oContained.w_STATUS=='I',5,;
      iif(this.Parent.oContained.w_STATUS=='F',6,;
      iif(this.Parent.oContained.w_STATUS=='Z',7,;
      iif(this.Parent.oContained.w_STATUS=='T',8,;
      0))))))))
  endfunc

  add object oDESCAN_1_2 as AH_SEARCHFLD with uid="CBKKYKLBHC",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Nome pratica o parte di esso (ricerca contestuale)",;
    HelpContextID = 230607306,;
   bGlobalFont=.t.,;
    Height=21, Width=538, Left=71, Top=38, InputMask=replicate('X',100)

  func oDESCAN_1_2.mHide()
    with this.Parent.oContained
      return (.w_STATUS$'S-I')
    endwith
  endfunc

  add object oPRAAPE_1_3 as AH_SEARCHFLD with uid="LEFUSAGYNV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PRAAPE", cQueryName = "PRAAPE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice o numero della pratica o parte di esso (ricerca contestuale)",;
    HelpContextID = 97639434,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=71, Top=68, InputMask=replicate('X',15)

  func oPRAAPE_1_3.mHide()
    with this.Parent.oContained
      return (.w_STATUS$'S-I')
    endwith
  endfunc

  add object oFLAPE_1_4 as StdRadio with uid="MLIGHJJTAM",rtseq=4,rtrep=.f.,left=625, top=38, width=77,height=51;
    , ToolTipText = "Selezione su apertura/chiusura";
    , cFormVar="w_FLAPE", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oFLAPE_1_4.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Aperte"
      this.Buttons(1).HelpContextID = 192078506
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Chiuse"
      this.Buttons(2).HelpContextID = 192078506
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Tutte"
      this.Buttons(3).HelpContextID = 192078506
      this.Buttons(3).Top=32
      this.SetAll("Width",75)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Selezione su apertura/chiusura")
      StdRadio::init()
    endproc

  func oFLAPE_1_4.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLAPE_1_4.GetRadio()
    this.Parent.oContained.w_FLAPE = this.RadioValue()
    return .t.
  endfunc

  func oFLAPE_1_4.SetRadio()
    this.Parent.oContained.w_FLAPE=trim(this.Parent.oContained.w_FLAPE)
    this.value = ;
      iif(this.Parent.oContained.w_FLAPE=='A',1,;
      iif(this.Parent.oContained.w_FLAPE=='C',2,;
      iif(this.Parent.oContained.w_FLAPE=='T',3,;
      0)))
  endfunc

  func oFLAPE_1_4.mHide()
    with this.Parent.oContained
      return (.w_STATUS$'S-I')
    endwith
  endfunc


  add object oBtn_1_5 as StdButton with uid="VAJGJMORRC",left=712, top=38, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la sincronizzazione";
    , HelpContextID = 128828650;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      this.parent.oContained.NotifyEvent("Ricerca")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_6 as StdButton with uid="GJZUUMSDLJ",left=13, top=425, width=48,height=45,;
    CpPicture="bmp\codici.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il documento selezionato";
    , HelpContextID = 243129962;
    , caption='A\<pri file';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        STAPDF(.w_LSFILENM, 'Open')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_LSFILENM))
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="BGOOKNIKBY",left=65, top=425, width=48,height=45,;
    CpPicture="BMP\apertura.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare/manutenere l'indice corrispondente (tabella indici documenti)";
    , HelpContextID = 193490822;
    , caption='Vi\<sualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        OpenGest('A', "GSUT_AID", "IDSERIAL", .w_INDICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_INDICE))
      endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="HYEVJZSTWA",left=117, top=425, width=48,height=45,;
    CpPicture="BMP\elimina2.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare gli indici selezionati";
    , HelpContextID = 82623814;
    , Caption='E\<limina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        GSUT_BIS(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="RMCMOHTSVV",left=169, top=425, width=48,height=45,;
    CpPicture="bitmap.bmp", caption="", nPag=1;
    , ToolTipText = "Legenda";
    , HelpContextID = 187995830;
    , caption='\<Legenda';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      do GSUT2KIS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZIONE_1_10 as StdRadio with uid="GAZFYPZPND",rtseq=5,rtrep=.f.,left=228, top=428, width=131,height=34;
    , cFormVar="w_SELEZIONE", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZIONE_1_10.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 19739708
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 19739708
      this.Buttons(2).Top=16
      this.SetAll("Width",129)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZIONE_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZIONE_1_10.GetRadio()
    this.Parent.oContained.w_SELEZIONE = this.RadioValue()
    return .t.
  endfunc

  func oSELEZIONE_1_10.SetRadio()
    this.Parent.oContained.w_SELEZIONE=trim(this.Parent.oContained.w_SELEZIONE)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZIONE=='S',1,;
      iif(this.Parent.oContained.w_SELEZIONE=='D',2,;
      0))
  endfunc


  add object ZOOM as cp_szoombox with uid="WKMOPXEQDU",left=9, top=99, width=752,height=319,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="LOGSININ",cZoomFile="GSUT1KIS",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Init,Ricerca",;
    nPag=1;
    , HelpContextID = 176473062


  add object oBtn_1_14 as StdButton with uid="JZOFTCQXPH",left=606, top=425, width=48,height=45,;
    CpPicture="BMP\DocConf.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare gli indici selezionati";
    , HelpContextID = 123334521;
    , Caption='\<Conferma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSUT_BIS(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="RVEFEDLOYY",left=658, top=425, width=48,height=45,;
    CpPicture="BMP\elimina.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per ignorare i file selezionati alla prossima sincronizzazione";
    , HelpContextID = 142542982;
    , Caption='\<Ignora';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSUT_BIS(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="XXLQWKTDJJ",left=710, top=425, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 262642618;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_17 as StdString with uid="CLFQTYWQLP",Visible=.t., Left=15, Top=13,;
    Alignment=1, Width=53, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="AUXASRUDLA",Visible=.t., Left=4, Top=38,;
    Alignment=1, Width=64, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (.w_STATUS$'S-I')
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="PXYZUVRXPL",Visible=.t., Left=15, Top=68,;
    Alignment=1, Width=53, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_STATUS$'S-I')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut1kis','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut1kis
Proc Seleziona_Deseleziona(pParent)
  local l_Cursor
  l_Cursor = pParent.w_ZOOM.cCursor
  UPDATE (l_Cursor) SET XCHK = IIF(pParent.w_SELEZIONE='S', 1, 0)
EndProc
* --- Fine Area Manuale
