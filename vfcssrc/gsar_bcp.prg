* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bcp                                                        *
*              Calcola percentuale c.C.                                        *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_28]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2006-03-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bcp",oParentObject)
return(i_retval)

define class tgsar_bcp as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_PARAME = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola Percentuale di ripartizione C.Costo (GSAR_MRC,GSCA_MRC,GSCA_MCC)
    this.w_PADRE = This.oParentObject
    this.w_PADRE.MarkPos()     
    this.oParentObject.w_TOTPER = 0
     
 Select ( this.w_PADRE.cTrsName) 
 Go Top
    do while Not Eof( this.w_PADRE.cTrsName )
      if Lower( this.w_PADRE.Class )="tgsca_mco"
        this.w_PARAME = t_MCPARAME
      else
        this.w_PARAME = t_MRPARAME
      endif
      * --- Se impostato il parametro eseguo i calcoli, anche se non ho la condizione di riga
      *     piena verificata...
      this.w_PADRE.WorkFromTrs()     
      if Lower( this.w_PADRE.Class )="tcgscg_mca"
        this.oParentObject.w_PERCEN = IIF(this.oParentObject.w_TOTPAR=0 OR this.oParentObject.w_MRPARAME=0, 0, cp_ROUND((this.oParentObject.w_MRPARAME/this.oParentObject.w_TOTPAR)*IIF(this.oParentObject.w_MR_SEGNO="D",1,-1)*100,2))
      else
        if Lower( this.w_PADRE.Class )="tgsca_mco"
          this.oParentObject.w_PERCEN = IIF(this.oParentObject.w_TOTPAR=0 OR this.oParentObject.w_MCPARAME=0, 0, (this.oParentObject.w_MCPARAME/this.oParentObject.w_TOTPAR)*100)
        else
          this.oParentObject.w_PERCEN = IIF(this.oParentObject.w_TOTPAR=0 OR this.oParentObject.w_MRPARAME=0, 0, (this.oParentObject.w_MRPARAME/this.oParentObject.w_TOTPAR)*100)
        endif
      endif
      this.oParentObject.w_TOTPER = this.oParentObject.w_TOTPER + this.oParentObject.w_PERCEN
      this.w_PADRE.TrsFromWork()     
       
 Select ( this.w_PADRE.cTrsName) 
 Skip
    enddo
    this.w_PADRE.RePos()     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
