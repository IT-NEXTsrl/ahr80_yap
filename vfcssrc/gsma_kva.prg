* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_kva                                                        *
*              Variazione dati articoli/servizi                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_277]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-09                                                      *
* Last revis.: 2018-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_kva",oParentObject))

* --- Class definition
define class tgsma_kva as StdForm
  Top    = 5
  Left   = 13

  * --- Standard Properties
  Width  = 649
  Height = 596+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-17"
  HelpContextID=48921449
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=202

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  PAR_RIOR_IDX = 0
  VOCIIVA_IDX = 0
  GRUMERC_IDX = 0
  CACOARTI_IDX = 0
  FAM_ARTI_IDX = 0
  CATEGOMO_IDX = 0
  CLA_RICA_IDX = 0
  CAT_SCMA_IDX = 0
  MARCHI_IDX = 0
  GRUPRO_IDX = 0
  VOC_COST_IDX = 0
  CONTI_IDX = 0
  KEY_ARTI_IDX = 0
  TIP_COLL_IDX = 0
  TIPICONF_IDX = 0
  INVENTAR_IDX = 0
  ESERCIZI_IDX = 0
  LISTINI_IDX = 0
  VALUTE_IDX = 0
  REP_ARTI_IDX = 0
  TIPCODIV_IDX = 0
  TIPCONTR_IDX = 0
  CATMCONT_IDX = 0
  UNIMIS_IDX = 0
  cPrg = "gsma_kva"
  cComment = "Variazione dati articoli/servizi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_IVASEL = space(1)
  w_IVAALL = space(1)
  o_IVAALL = space(1)
  w_IVAINI = space(5)
  w_DESIVA = space(35)
  w_IVAFIN = space(5)
  o_IVAFIN = space(5)
  w_DESIVA1 = space(35)
  w_CIMSEL = space(1)
  w_CIMALL = space(1)
  w_CIMINI = space(10)
  w_CIMFIN = space(10)
  o_CIMFIN = space(10)
  w_GRMSEL = space(1)
  w_GRMALL = space(1)
  o_GRMALL = space(1)
  w_GRMINI = space(5)
  w_DESGRU = space(35)
  w_GRMFIN = space(5)
  o_GRMFIN = space(5)
  w_DESGRU1 = space(35)
  w_CATSEL = space(1)
  w_CATALL = space(1)
  o_CATALL = space(1)
  w_CATINI = space(5)
  w_DESCAT = space(35)
  w_CATFIN = space(5)
  o_CATFIN = space(5)
  w_DESCAT1 = space(35)
  w_FAMSEL = space(1)
  w_FAMALL = space(1)
  o_FAMALL = space(1)
  w_FAMINI = space(5)
  w_DESFAM = space(35)
  w_FAMFIN = space(5)
  o_FAMFIN = space(5)
  w_DESFAM1 = space(35)
  w_CATOSEL = space(1)
  w_CATOALL = space(1)
  o_CATOALL = space(1)
  w_CATOINI = space(5)
  w_DESCATO = space(35)
  w_CATOFIN = space(5)
  o_CATOFIN = space(5)
  w_DESCATO1 = space(35)
  w_CLASEL = space(1)
  w_CLAALL = space(1)
  o_CLAALL = space(1)
  w_CLAINI = space(5)
  w_DESCLA = space(35)
  w_CLAFIN = space(5)
  o_CLAFIN = space(5)
  w_DESCLA1 = space(35)
  w_SCOSEL = space(1)
  w_SCOALL = space(1)
  o_SCOALL = space(1)
  w_SCOINI = space(5)
  w_DESSCO = space(35)
  w_SCOFIN = space(5)
  o_SCOFIN = space(5)
  w_DESSCO1 = space(35)
  w_MARSEL = space(1)
  w_MARALL = space(1)
  o_MARALL = space(1)
  w_MARINI = space(5)
  w_DESMAR = space(35)
  w_MARFIN = space(5)
  o_MARFIN = space(5)
  w_DESMAR1 = space(35)
  w_CAPSEL = space(1)
  w_CAPALL = space(1)
  o_CAPALL = space(1)
  w_CAPINI = space(5)
  w_DESCAP = space(35)
  w_CAPFIN = space(5)
  o_CAPFIN = space(5)
  w_DESCAP1 = space(35)
  w_VOCSEL = space(1)
  w_VOCALL = space(1)
  o_VOCALL = space(1)
  w_VOCINI = space(15)
  w_DESVOC = space(35)
  w_VOCFIN = space(15)
  o_VOCFIN = space(15)
  w_DESVOC1 = space(35)
  w_VORSEL = space(1)
  w_VORALL = space(1)
  o_VORALL = space(1)
  w_VORINI = space(15)
  w_DESVOV = space(35)
  w_VORFIN = space(15)
  o_VORFIN = space(15)
  w_DESVOV1 = space(35)
  w_FORSEL = space(10)
  w_FORALL = space(1)
  o_FORALL = space(1)
  w_FORINI = space(15)
  w_DESFOR = space(35)
  w_FORFIN = space(15)
  o_FORFIN = space(15)
  w_DESFOR1 = space(35)
  w_GESSEL = space(1)
  w_GESINI = space(1)
  w_GESFIN = space(10)
  w_PROVSEL = space(1)
  w_PROVINI = space(1)
  w_PROVFIN = space(1)
  w_TIPSEL = space(1)
  o_TIPSEL = space(1)
  w_TIPARTI = space(2)
  w_TIPARTI = space(2)
  w_PROVART = space(1)
  w_ARTOBS = space(1)
  o_ARTOBS = space(1)
  w_ARTINI = space(20)
  w_DESART = space(35)
  w_ARTFIN = space(20)
  w_DESART1 = space(35)
  w_IVAFIL = space(5)
  w_IVADES = space(35)
  w_GRMFIL = space(5)
  w_GRMDES = space(35)
  w_CATFIL = space(5)
  w_CATDES = space(35)
  w_FAMFIL = space(5)
  w_FAMDES = space(35)
  w_CATOFIL = space(5)
  w_CATODES = space(35)
  w_PROFIL = space(15)
  w_PRODES = space(35)
  w_FORFIL = space(15)
  w_FORDES = space(35)
  w_MARFIL = space(5)
  w_MARDES = space(35)
  w_TIPCON = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPART = space(2)
  w_TIPVOC = space(1)
  w_TIPVOR = space(1)
  w_CODCOS = space(1)
  w_DATA = ctod('  /  /  ')
  w_ARTIPART = space(2)
  w_TIPCAT = space(1)
  w_TIPPRO = space(1)
  w_COLFIL = space(5)
  w_CONFIL = space(3)
  w_COLDES = space(35)
  w_CONDES = space(35)
  w_COLSEL = space(1)
  w_COLALL = space(1)
  o_COLALL = space(1)
  w_COLINI = space(5)
  w_DESCOL = space(35)
  w_COLFIN = space(5)
  o_COLFIN = space(5)
  w_DESCOL1 = space(35)
  w_AZIENDA = space(5)
  w_CONDEF = space(3)
  w_sQTAMIN = space(1)
  o_sQTAMIN = space(1)
  w_UMPRI = space(3)
  o_UMPRI = space(3)
  w_FLFRAZ = space(1)
  w_DESCOF = space(35)
  w_cMINVEN = 0
  w_PUBINI = space(1)
  o_PUBINI = space(1)
  w_PUBSEL = space(1)
  o_PUBSEL = space(1)
  w_PUBFIN = space(1)
  w_COSSEL = space(1)
  o_COSSEL = space(1)
  w_COSALL = space(1)
  w_DISTIN = space(1)
  w_Aggiorn = space(2)
  o_Aggiorn = space(2)
  w_VALUTA = space(3)
  o_VALUTA = space(3)
  w_LISTRIF = space(5)
  w_VALRIF = space(3)
  o_VALRIF = space(3)
  w_SIMVAL = space(5)
  w_CAOVAL = 0
  w_VALNAZ = space(3)
  w_CAOLIS = 0
  w_ESERCI = space(4)
  o_ESERCI = space(4)
  w_NUMINV = space(6)
  w_CAMBIO = 0
  w_RICPE = 0
  w_DATRIFE = ctod('  /  /  ')
  w_FLGPOS = space(1)
  w_REPSEL = space(1)
  w_REPALL = space(1)
  o_REPALL = space(1)
  w_REPINI = space(3)
  w_DESREP = space(35)
  w_REPFIN = space(3)
  o_REPFIN = space(3)
  w_DESREP1 = space(35)
  w_PECOSEL = space(1)
  w_PECOINI = space(1)
  w_PECOFIN = space(1)
  w_REPFIL = space(3)
  w_REPDES = space(35)
  w_DESOI = space(35)
  w_DESOI1 = space(35)
  w_TIPOPEIVA = space(2)
  w_TIPOPOBS = ctod('  /  /  ')
  w_FLGCAC = space(1)
  w_cMCTIPCAT = space(15)
  o_cMCTIPCAT = space(15)
  w_cMCCODCAT = space(5)
  w_TPDESCRI = space(40)
  w_CGDESCRI = space(40)
  w_TIPARTIN = space(2)
  w_TIPARTIF = space(2)
  w_TIPASEL = space(1)
  w_SACSEL = space(1)
  w_SACALL = space(1)
  w_SACINI = space(5)
  w_SACFIN = space(5)
  w_UTISEL = space(1)
  w_UTIALL = space(1)
  o_UTIALL = space(1)
  w_UTIINI = space(5)
  w_UTIFIN = space(5)
  w_TIPOSEL = space(1)
  w_TIPOALL = space(1)
  o_TIPOALL = space(1)
  w_TIPOINI = space(35)
  w_TIPOFIN = space(35)
  o_TIPOFIN = space(35)
  w_VALOSEL = space(1)
  w_VALOALL = space(1)
  o_VALOALL = space(1)
  w_VALOINI = space(35)
  w_VALOFIN = space(35)
  o_VALOFIN = space(35)
  w_CLASSEL = space(1)
  w_CLASALL = space(5)
  o_CLASALL = space(5)
  w_CLASINI = space(5)
  w_CLASFIN = space(5)
  o_CLASFIN = space(5)
  w_GESCSEL = space(1)
  w_GESCALL = space(5)
  o_GESCALL = space(5)
  w_GESCINI = space(5)
  w_GESCFIN = space(5)
  o_GESCFIN = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kvaPag1","gsma_kva",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principali")
      .Pages(2).addobject("oPag","tgsma_kvaPag2","gsma_kva",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Commerciali/magazzino")
      .Pages(3).addobject("oPag","tgsma_kvaPag3","gsma_kva",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Analitica/produzione/P.O.S.")
      .Pages(4).addobject("oPag","tgsma_kvaPag4","gsma_kva",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Selezioni")
      .Pages(4).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIVAALL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsma_kva
    if Isalt()
    This.PARENT.cComment = "Variazione dati tariffario"
    endif
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[25]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='PAR_RIOR'
    this.cWorkTables[3]='VOCIIVA'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='CACOARTI'
    this.cWorkTables[6]='FAM_ARTI'
    this.cWorkTables[7]='CATEGOMO'
    this.cWorkTables[8]='CLA_RICA'
    this.cWorkTables[9]='CAT_SCMA'
    this.cWorkTables[10]='MARCHI'
    this.cWorkTables[11]='GRUPRO'
    this.cWorkTables[12]='VOC_COST'
    this.cWorkTables[13]='CONTI'
    this.cWorkTables[14]='KEY_ARTI'
    this.cWorkTables[15]='TIP_COLL'
    this.cWorkTables[16]='TIPICONF'
    this.cWorkTables[17]='INVENTAR'
    this.cWorkTables[18]='ESERCIZI'
    this.cWorkTables[19]='LISTINI'
    this.cWorkTables[20]='VALUTE'
    this.cWorkTables[21]='REP_ARTI'
    this.cWorkTables[22]='TIPCODIV'
    this.cWorkTables[23]='TIPCONTR'
    this.cWorkTables[24]='CATMCONT'
    this.cWorkTables[25]='UNIMIS'
    return(this.OpenAllTables(25))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSMA_BVA with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_IVASEL=space(1)
      .w_IVAALL=space(1)
      .w_IVAINI=space(5)
      .w_DESIVA=space(35)
      .w_IVAFIN=space(5)
      .w_DESIVA1=space(35)
      .w_CIMSEL=space(1)
      .w_CIMALL=space(1)
      .w_CIMINI=space(10)
      .w_CIMFIN=space(10)
      .w_GRMSEL=space(1)
      .w_GRMALL=space(1)
      .w_GRMINI=space(5)
      .w_DESGRU=space(35)
      .w_GRMFIN=space(5)
      .w_DESGRU1=space(35)
      .w_CATSEL=space(1)
      .w_CATALL=space(1)
      .w_CATINI=space(5)
      .w_DESCAT=space(35)
      .w_CATFIN=space(5)
      .w_DESCAT1=space(35)
      .w_FAMSEL=space(1)
      .w_FAMALL=space(1)
      .w_FAMINI=space(5)
      .w_DESFAM=space(35)
      .w_FAMFIN=space(5)
      .w_DESFAM1=space(35)
      .w_CATOSEL=space(1)
      .w_CATOALL=space(1)
      .w_CATOINI=space(5)
      .w_DESCATO=space(35)
      .w_CATOFIN=space(5)
      .w_DESCATO1=space(35)
      .w_CLASEL=space(1)
      .w_CLAALL=space(1)
      .w_CLAINI=space(5)
      .w_DESCLA=space(35)
      .w_CLAFIN=space(5)
      .w_DESCLA1=space(35)
      .w_SCOSEL=space(1)
      .w_SCOALL=space(1)
      .w_SCOINI=space(5)
      .w_DESSCO=space(35)
      .w_SCOFIN=space(5)
      .w_DESSCO1=space(35)
      .w_MARSEL=space(1)
      .w_MARALL=space(1)
      .w_MARINI=space(5)
      .w_DESMAR=space(35)
      .w_MARFIN=space(5)
      .w_DESMAR1=space(35)
      .w_CAPSEL=space(1)
      .w_CAPALL=space(1)
      .w_CAPINI=space(5)
      .w_DESCAP=space(35)
      .w_CAPFIN=space(5)
      .w_DESCAP1=space(35)
      .w_VOCSEL=space(1)
      .w_VOCALL=space(1)
      .w_VOCINI=space(15)
      .w_DESVOC=space(35)
      .w_VOCFIN=space(15)
      .w_DESVOC1=space(35)
      .w_VORSEL=space(1)
      .w_VORALL=space(1)
      .w_VORINI=space(15)
      .w_DESVOV=space(35)
      .w_VORFIN=space(15)
      .w_DESVOV1=space(35)
      .w_FORSEL=space(10)
      .w_FORALL=space(1)
      .w_FORINI=space(15)
      .w_DESFOR=space(35)
      .w_FORFIN=space(15)
      .w_DESFOR1=space(35)
      .w_GESSEL=space(1)
      .w_GESINI=space(1)
      .w_GESFIN=space(10)
      .w_PROVSEL=space(1)
      .w_PROVINI=space(1)
      .w_PROVFIN=space(1)
      .w_TIPSEL=space(1)
      .w_TIPARTI=space(2)
      .w_TIPARTI=space(2)
      .w_PROVART=space(1)
      .w_ARTOBS=space(1)
      .w_ARTINI=space(20)
      .w_DESART=space(35)
      .w_ARTFIN=space(20)
      .w_DESART1=space(35)
      .w_IVAFIL=space(5)
      .w_IVADES=space(35)
      .w_GRMFIL=space(5)
      .w_GRMDES=space(35)
      .w_CATFIL=space(5)
      .w_CATDES=space(35)
      .w_FAMFIL=space(5)
      .w_FAMDES=space(35)
      .w_CATOFIL=space(5)
      .w_CATODES=space(35)
      .w_PROFIL=space(15)
      .w_PRODES=space(35)
      .w_FORFIL=space(15)
      .w_FORDES=space(35)
      .w_MARFIL=space(5)
      .w_MARDES=space(35)
      .w_TIPCON=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPART=space(2)
      .w_TIPVOC=space(1)
      .w_TIPVOR=space(1)
      .w_CODCOS=space(1)
      .w_DATA=ctod("  /  /  ")
      .w_ARTIPART=space(2)
      .w_TIPCAT=space(1)
      .w_TIPPRO=space(1)
      .w_COLFIL=space(5)
      .w_CONFIL=space(3)
      .w_COLDES=space(35)
      .w_CONDES=space(35)
      .w_COLSEL=space(1)
      .w_COLALL=space(1)
      .w_COLINI=space(5)
      .w_DESCOL=space(35)
      .w_COLFIN=space(5)
      .w_DESCOL1=space(35)
      .w_AZIENDA=space(5)
      .w_CONDEF=space(3)
      .w_sQTAMIN=space(1)
      .w_UMPRI=space(3)
      .w_FLFRAZ=space(1)
      .w_DESCOF=space(35)
      .w_cMINVEN=0
      .w_PUBINI=space(1)
      .w_PUBSEL=space(1)
      .w_PUBFIN=space(1)
      .w_COSSEL=space(1)
      .w_COSALL=space(1)
      .w_DISTIN=space(1)
      .w_Aggiorn=space(2)
      .w_VALUTA=space(3)
      .w_LISTRIF=space(5)
      .w_VALRIF=space(3)
      .w_SIMVAL=space(5)
      .w_CAOVAL=0
      .w_VALNAZ=space(3)
      .w_CAOLIS=0
      .w_ESERCI=space(4)
      .w_NUMINV=space(6)
      .w_CAMBIO=0
      .w_RICPE=0
      .w_DATRIFE=ctod("  /  /  ")
      .w_FLGPOS=space(1)
      .w_REPSEL=space(1)
      .w_REPALL=space(1)
      .w_REPINI=space(3)
      .w_DESREP=space(35)
      .w_REPFIN=space(3)
      .w_DESREP1=space(35)
      .w_PECOSEL=space(1)
      .w_PECOINI=space(1)
      .w_PECOFIN=space(1)
      .w_REPFIL=space(3)
      .w_REPDES=space(35)
      .w_DESOI=space(35)
      .w_DESOI1=space(35)
      .w_TIPOPEIVA=space(2)
      .w_TIPOPOBS=ctod("  /  /  ")
      .w_FLGCAC=space(1)
      .w_cMCTIPCAT=space(15)
      .w_cMCCODCAT=space(5)
      .w_TPDESCRI=space(40)
      .w_CGDESCRI=space(40)
      .w_TIPARTIN=space(2)
      .w_TIPARTIF=space(2)
      .w_TIPASEL=space(1)
      .w_SACSEL=space(1)
      .w_SACALL=space(1)
      .w_SACINI=space(5)
      .w_SACFIN=space(5)
      .w_UTISEL=space(1)
      .w_UTIALL=space(1)
      .w_UTIINI=space(5)
      .w_UTIFIN=space(5)
      .w_TIPOSEL=space(1)
      .w_TIPOALL=space(1)
      .w_TIPOINI=space(35)
      .w_TIPOFIN=space(35)
      .w_VALOSEL=space(1)
      .w_VALOALL=space(1)
      .w_VALOINI=space(35)
      .w_VALOFIN=space(35)
      .w_CLASSEL=space(1)
      .w_CLASALL=space(5)
      .w_CLASINI=space(5)
      .w_CLASFIN=space(5)
      .w_GESCSEL=space(1)
      .w_GESCALL=space(5)
      .w_GESCINI=space(5)
      .w_GESCFIN=space(5)
        .w_IVASEL = iif( Not Empty(.w_IVAFIN) ,'S' , ' ')
          .DoRTCalc(2,2,.f.)
        .w_IVAINI = IIF(.w_IVAALL='S',SPACE(5),.w_IVAINI)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_IVAINI))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_IVAFIN))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_CIMSEL = iif( Not Empty(.w_CIMFIN) ,'S' , ' ')
        .DoRTCalc(8,9,.f.)
        if not(empty(.w_CIMINI))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CIMFIN))
          .link_1_10('Full')
        endif
        .w_GRMSEL = iif( Not Empty(.w_GRMFIN) ,'S' , ' ')
          .DoRTCalc(12,12,.f.)
        .w_GRMINI = IIF(.w_GRMALL='S',SPACE(5),.w_GRMINI)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_GRMINI))
          .link_1_15('Full')
        endif
        .DoRTCalc(14,15,.f.)
        if not(empty(.w_GRMFIN))
          .link_1_17('Full')
        endif
          .DoRTCalc(16,16,.f.)
        .w_CATSEL = iif( Not Empty(.w_CATFIN) ,'S' , ' ')
          .DoRTCalc(18,18,.f.)
        .w_CATINI = IIF(.w_CATALL='S',SPACE(5),.w_CATINI)
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CATINI))
          .link_1_23('Full')
        endif
        .DoRTCalc(20,21,.f.)
        if not(empty(.w_CATFIN))
          .link_1_25('Full')
        endif
          .DoRTCalc(22,22,.f.)
        .w_FAMSEL = iif( Not Empty(.w_FAMFIN) ,'S' , ' ')
          .DoRTCalc(24,24,.f.)
        .w_FAMINI = IIF(.w_FAMALL='S',SPACE(5),.w_FAMINI)
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_FAMINI))
          .link_1_31('Full')
        endif
        .DoRTCalc(26,27,.f.)
        if not(empty(.w_FAMFIN))
          .link_1_33('Full')
        endif
          .DoRTCalc(28,28,.f.)
        .w_CATOSEL = iif( Not Empty(.w_CATOFIN) ,'S' , ' ')
          .DoRTCalc(30,30,.f.)
        .w_CATOINI = IIF(.w_CATOALL='S',SPACE(5),.w_CATOINI)
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_CATOINI))
          .link_1_39('Full')
        endif
        .DoRTCalc(32,33,.f.)
        if not(empty(.w_CATOFIN))
          .link_1_41('Full')
        endif
          .DoRTCalc(34,34,.f.)
        .w_CLASEL = iif( Not Empty(.w_CLAFIN) ,'S' , ' ')
          .DoRTCalc(36,36,.f.)
        .w_CLAINI = IIF(.w_CLAALL='S',SPACE(5),.w_CLAINI)
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_CLAINI))
          .link_2_3('Full')
        endif
        .DoRTCalc(38,39,.f.)
        if not(empty(.w_CLAFIN))
          .link_2_5('Full')
        endif
          .DoRTCalc(40,40,.f.)
        .w_SCOSEL = iif( Not Empty(.w_SCOFIN) ,'S' , ' ')
          .DoRTCalc(42,42,.f.)
        .w_SCOINI = IIF(.w_SCOALL='S',SPACE(5),.w_SCOINI)
        .DoRTCalc(43,43,.f.)
        if not(empty(.w_SCOINI))
          .link_2_10('Full')
        endif
        .DoRTCalc(44,45,.f.)
        if not(empty(.w_SCOFIN))
          .link_2_12('Full')
        endif
          .DoRTCalc(46,46,.f.)
        .w_MARSEL = iif( Not Empty(.w_MARFIN) ,'S' , ' ')
          .DoRTCalc(48,48,.f.)
        .w_MARINI = IIF(.w_MARALL='S',SPACE(5),.w_MARINI)
        .DoRTCalc(49,49,.f.)
        if not(empty(.w_MARINI))
          .link_2_17('Full')
        endif
        .DoRTCalc(50,51,.f.)
        if not(empty(.w_MARFIN))
          .link_2_19('Full')
        endif
          .DoRTCalc(52,52,.f.)
        .w_CAPSEL = iif( Not Empty(.w_CAPFIN) ,'S' , ' ')
          .DoRTCalc(54,54,.f.)
        .w_CAPINI = IIF(.w_CAPALL='S',SPACE(5), .w_CAPINI)
        .DoRTCalc(55,55,.f.)
        if not(empty(.w_CAPINI))
          .link_2_25('Full')
        endif
        .DoRTCalc(56,57,.f.)
        if not(empty(.w_CAPFIN))
          .link_2_27('Full')
        endif
          .DoRTCalc(58,58,.f.)
        .w_VOCSEL = iif( Not Empty(.w_VOCFIN) ,'S' , ' ')
          .DoRTCalc(60,60,.f.)
        .w_VOCINI = IIF(.w_VOCALL='S',SPACE(5),.w_VOCINI)
        .DoRTCalc(61,61,.f.)
        if not(empty(.w_VOCINI))
          .link_3_3('Full')
        endif
        .DoRTCalc(62,63,.f.)
        if not(empty(.w_VOCFIN))
          .link_3_5('Full')
        endif
          .DoRTCalc(64,64,.f.)
        .w_VORSEL = iif( Not Empty(.w_VORFIN) ,'S' , ' ')
          .DoRTCalc(66,66,.f.)
        .w_VORINI = IIF(.w_VORALL='S',SPACE(5),.w_VORINI)
        .DoRTCalc(67,67,.f.)
        if not(empty(.w_VORINI))
          .link_3_11('Full')
        endif
        .DoRTCalc(68,69,.f.)
        if not(empty(.w_VORFIN))
          .link_3_13('Full')
        endif
          .DoRTCalc(70,70,.f.)
        .w_FORSEL = iif( Not Empty(.w_FORFIN) ,'S' , ' ')
          .DoRTCalc(72,72,.f.)
        .w_FORINI = IIF(.w_FORALL='S',SPACE(5),.w_FORINI)
        .DoRTCalc(73,73,.f.)
        if not(empty(.w_FORINI))
          .link_3_19('Full')
        endif
        .DoRTCalc(74,75,.f.)
        if not(empty(.w_FORFIN))
          .link_3_21('Full')
        endif
          .DoRTCalc(76,77,.f.)
        .w_GESINI = 'F'
        .w_GESFIN = 'S'
          .DoRTCalc(80,80,.f.)
        .w_PROVINI = 'E'
        .w_PROVFIN = 'L'
        .w_TIPSEL = iif(!Isalt(),'A','S')
        .w_TIPARTI = '  '
        .w_TIPARTI = IIF(g_PROD<>'S' and !Isalt(), 'PF', '  ')
        .w_PROVART = ' '
        .w_ARTOBS = 'T'
        .w_ARTINI = IIF(.w_ARTOBS$'OVT',SPACE(20),.w_ARTINI)
        .DoRTCalc(88,88,.f.)
        if not(empty(.w_ARTINI))
          .link_4_8('Full')
        endif
          .DoRTCalc(89,89,.f.)
        .w_ARTFIN = IIF(.w_ARTOBS$'OVT',SPACE(20),.w_ARTFIN)
        .DoRTCalc(90,90,.f.)
        if not(empty(.w_ARTFIN))
          .link_4_10('Full')
        endif
        .DoRTCalc(91,92,.f.)
        if not(empty(.w_IVAFIL))
          .link_4_12('Full')
        endif
        .DoRTCalc(93,94,.f.)
        if not(empty(.w_GRMFIL))
          .link_4_14('Full')
        endif
        .DoRTCalc(95,96,.f.)
        if not(empty(.w_CATFIL))
          .link_4_18('Full')
        endif
        .DoRTCalc(97,98,.f.)
        if not(empty(.w_FAMFIL))
          .link_4_20('Full')
        endif
        .DoRTCalc(99,100,.f.)
        if not(empty(.w_CATOFIL))
          .link_4_24('Full')
        endif
        .DoRTCalc(101,102,.f.)
        if not(empty(.w_PROFIL))
          .link_4_26('Full')
        endif
        .DoRTCalc(103,104,.f.)
        if not(empty(.w_FORFIL))
          .link_4_30('Full')
        endif
        .DoRTCalc(105,106,.f.)
        if not(empty(.w_MARFIL))
          .link_4_32('Full')
        endif
          .DoRTCalc(107,107,.f.)
        .w_TIPCON = 'F'
        .w_OBTEST = i_datsys
          .DoRTCalc(110,110,.f.)
        .w_TIPART = .w_TIPSEL
        .w_TIPVOC = 'C'
        .w_TIPVOR = 'R'
          .DoRTCalc(114,114,.f.)
        .w_DATA = i_datsys
        .w_ARTIPART = .w_TIPSEL
        .DoRTCalc(117,119,.f.)
        if not(empty(.w_COLFIL))
          .link_4_44('Full')
        endif
        .DoRTCalc(120,120,.f.)
        if not(empty(.w_CONFIL))
          .link_4_45('Full')
        endif
          .DoRTCalc(121,122,.f.)
        .w_COLSEL = iif( Not Empty(.w_COLFIN) ,'S' , ' ')
          .DoRTCalc(124,124,.f.)
        .w_COLINI = IIF(.w_COLALL='S',SPACE(5),.w_COLINI)
        .DoRTCalc(125,125,.f.)
        if not(empty(.w_COLINI))
          .link_2_35('Full')
        endif
        .DoRTCalc(126,127,.f.)
        if not(empty(.w_COLFIN))
          .link_2_37('Full')
        endif
          .DoRTCalc(128,128,.f.)
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(130,130,.f.)
        if not(empty(.w_CONDEF))
          .link_2_42('Full')
        endif
        .w_sQTAMIN = 'N'
        .w_UMPRI = IIF(.w_sQTAMIN<>'S', SPACE(3), .w_sQTAMIN)
        .DoRTCalc(132,132,.f.)
        if not(empty(.w_UMPRI))
          .link_2_44('Full')
        endif
          .DoRTCalc(133,134,.f.)
        .w_cMINVEN = 0
        .w_PUBINI = 'N'
          .DoRTCalc(137,137,.f.)
        .w_PUBFIN = iif(g_CPIN#'S',iif(empty(.w_PUBFIN),'S',.w_PUBFIN),iif(.w_PUBINI $ 'S-T',icase(.w_PUBINI='S','T',.w_PUBINI='T','S',.w_PUBINI),'S'))
        .w_COSSEL = ' '
        .w_COSALL = 'T'
        .w_DISTIN = 'T'
        .w_Aggiorn = 'CA'
          .DoRTCalc(143,143,.f.)
        .w_LISTRIF = SPACE(5)
        .DoRTCalc(144,144,.f.)
        if not(empty(.w_LISTRIF))
          .link_2_57('Full')
        endif
        .w_VALRIF = .w_VALUTA
        .DoRTCalc(145,145,.f.)
        if not(empty(.w_VALRIF))
          .link_2_58('Full')
        endif
        .DoRTCalc(146,148,.f.)
        if not(empty(.w_VALNAZ))
          .link_2_64('Full')
        endif
          .DoRTCalc(149,149,.f.)
        .w_ESERCI = SPACE(4)
        .DoRTCalc(150,150,.f.)
        if not(empty(.w_ESERCI))
          .link_2_66('Full')
        endif
        .w_NUMINV = SPACE(6)
        .DoRTCalc(151,151,.f.)
        if not(empty(.w_NUMINV))
          .link_2_67('Full')
        endif
        .w_CAMBIO = GETCAM(.w_VALRIF, .w_DATRIFE, 7)
        .w_RICPE = 0
        .w_DATRIFE = i_datsys
        .w_FLGPOS = 'X'
        .w_REPSEL = iif( Not Empty(.w_REPFIN) ,'S' , ' ')
          .DoRTCalc(157,157,.f.)
        .w_REPINI = IIF(.w_REPALL='S',SPACE(2),.w_REPINI)
        .DoRTCalc(158,158,.f.)
        if not(empty(.w_REPINI))
          .link_3_42('Full')
        endif
        .DoRTCalc(159,160,.f.)
        if not(empty(.w_REPFIN))
          .link_3_44('Full')
        endif
          .DoRTCalc(161,162,.f.)
        .w_PECOINI = 'P'
        .w_PECOFIN = 'P'
        .DoRTCalc(165,165,.f.)
        if not(empty(.w_REPFIL))
          .link_4_48('Full')
        endif
          .DoRTCalc(166,168,.f.)
        .w_TIPOPEIVA = 'AR'
          .DoRTCalc(170,170,.f.)
        .w_FLGCAC = 'X'
        .DoRTCalc(172,172,.f.)
        if not(empty(.w_cMCTIPCAT))
          .link_2_83('Full')
        endif
        .w_cMCCODCAT = SPACE( 5 )
        .DoRTCalc(173,173,.f.)
        if not(empty(.w_cMCCODCAT))
          .link_2_84('Full')
        endif
          .DoRTCalc(174,175,.f.)
        .w_TIPARTIN = 'PF'
        .w_TIPARTIF = 'PF'
          .DoRTCalc(178,178,.f.)
        .w_SACSEL = iif( Not Empty(.w_GRMFIN) ,'S' , ' ')
          .DoRTCalc(180,180,.f.)
        .w_SACINI = IIF(.w_SACALL='S','N',.w_SACINI)
        .w_SACFIN = IIF(.w_SACALL='S','N',.w_SACINI)
          .DoRTCalc(183,184,.f.)
        .w_UTIINI = IIF(.w_UTIALL='S','N',.w_UTIINI)
        .w_UTIFIN = 'S'
        .w_TIPOSEL = iif( Not Empty(.w_TIPOFIN) ,'S' , ' ')
          .DoRTCalc(188,188,.f.)
        .w_TIPOINI = IIF(.w_TIPOALL='S',SPACE(35),.w_TIPOINI)
          .DoRTCalc(190,190,.f.)
        .w_VALOSEL = iif( Not Empty(.w_VALOFIN) ,'S' , ' ')
          .DoRTCalc(192,192,.f.)
        .w_VALOINI = IIF(.w_VALOALL='S',SPACE(35),.w_VALOINI)
          .DoRTCalc(194,194,.f.)
        .w_CLASSEL = iif( Not Empty(.w_CLASFIN) ,'S' , ' ')
          .DoRTCalc(196,196,.f.)
        .w_CLASINI = IIF(.w_CLASALL='S',SPACE(5),.w_CLASINI)
          .DoRTCalc(198,198,.f.)
        .w_GESCSEL = iif(.w_GESCFIN='S' ,'S' , ' ')
          .DoRTCalc(200,200,.f.)
        .w_GESCINI = IIF(.w_GESCALL='S','N',.w_GESCINI)
    endwith
    this.DoRTCalc(202,202,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page4.oPag.oBtn_4_36.enabled = this.oPgFrm.Page4.oPag.oBtn_4_36.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_37.enabled = this.oPgFrm.Page4.oPag.oBtn_4_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_62.enabled = this.oPgFrm.Page1.oPag.oBtn_1_62.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_57.enabled = this.oPgFrm.Page3.oPag.oBtn_3_57.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_58.enabled = this.oPgFrm.Page3.oPag.oBtn_3_58.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_IVAFIN<>.w_IVAFIN
            .w_IVASEL = iif( Not Empty(.w_IVAFIN) ,'S' , ' ')
        endif
        .DoRTCalc(2,2,.t.)
        if .o_IVAALL<>.w_IVAALL
            .w_IVAINI = IIF(.w_IVAALL='S',SPACE(5),.w_IVAINI)
          .link_1_3('Full')
        endif
        .DoRTCalc(4,6,.t.)
        if .o_CIMFIN<>.w_CIMFIN
            .w_CIMSEL = iif( Not Empty(.w_CIMFIN) ,'S' , ' ')
        endif
        .DoRTCalc(8,10,.t.)
        if .o_GRMFIN<>.w_GRMFIN
            .w_GRMSEL = iif( Not Empty(.w_GRMFIN) ,'S' , ' ')
        endif
        .DoRTCalc(12,12,.t.)
        if .o_GRMALL<>.w_GRMALL
            .w_GRMINI = IIF(.w_GRMALL='S',SPACE(5),.w_GRMINI)
          .link_1_15('Full')
        endif
        .DoRTCalc(14,16,.t.)
        if .o_CATFIN<>.w_CATFIN
            .w_CATSEL = iif( Not Empty(.w_CATFIN) ,'S' , ' ')
        endif
        .DoRTCalc(18,18,.t.)
        if .o_CATALL<>.w_CATALL
            .w_CATINI = IIF(.w_CATALL='S',SPACE(5),.w_CATINI)
          .link_1_23('Full')
        endif
        .DoRTCalc(20,22,.t.)
        if .o_FAMFIN<>.w_FAMFIN
            .w_FAMSEL = iif( Not Empty(.w_FAMFIN) ,'S' , ' ')
        endif
        .DoRTCalc(24,24,.t.)
        if .o_FAMALL<>.w_FAMALL
            .w_FAMINI = IIF(.w_FAMALL='S',SPACE(5),.w_FAMINI)
          .link_1_31('Full')
        endif
        .DoRTCalc(26,28,.t.)
        if .o_CATOFIN<>.w_CATOFIN
            .w_CATOSEL = iif( Not Empty(.w_CATOFIN) ,'S' , ' ')
        endif
        .DoRTCalc(30,30,.t.)
        if .o_CATOALL<>.w_CATOALL
            .w_CATOINI = IIF(.w_CATOALL='S',SPACE(5),.w_CATOINI)
          .link_1_39('Full')
        endif
        .DoRTCalc(32,34,.t.)
        if .o_CLAFIN<>.w_CLAFIN
            .w_CLASEL = iif( Not Empty(.w_CLAFIN) ,'S' , ' ')
        endif
        .DoRTCalc(36,36,.t.)
        if .o_CLAALL<>.w_CLAALL
            .w_CLAINI = IIF(.w_CLAALL='S',SPACE(5),.w_CLAINI)
          .link_2_3('Full')
        endif
        .DoRTCalc(38,40,.t.)
        if .o_SCOFIN<>.w_SCOFIN
            .w_SCOSEL = iif( Not Empty(.w_SCOFIN) ,'S' , ' ')
        endif
        .DoRTCalc(42,42,.t.)
        if .o_SCOALL<>.w_SCOALL
            .w_SCOINI = IIF(.w_SCOALL='S',SPACE(5),.w_SCOINI)
          .link_2_10('Full')
        endif
        .DoRTCalc(44,46,.t.)
        if .o_MARFIN<>.w_MARFIN
            .w_MARSEL = iif( Not Empty(.w_MARFIN) ,'S' , ' ')
        endif
        .DoRTCalc(48,48,.t.)
        if .o_MARALL<>.w_MARALL
            .w_MARINI = IIF(.w_MARALL='S',SPACE(5),.w_MARINI)
          .link_2_17('Full')
        endif
        .DoRTCalc(50,52,.t.)
        if .o_CAPFIN<>.w_CAPFIN
            .w_CAPSEL = iif( Not Empty(.w_CAPFIN) ,'S' , ' ')
        endif
        .DoRTCalc(54,54,.t.)
        if .o_CAPALL<>.w_CAPALL
            .w_CAPINI = IIF(.w_CAPALL='S',SPACE(5), .w_CAPINI)
          .link_2_25('Full')
        endif
        .DoRTCalc(56,58,.t.)
        if .o_VOCFIN<>.w_VOCFIN
            .w_VOCSEL = iif( Not Empty(.w_VOCFIN) ,'S' , ' ')
        endif
        .DoRTCalc(60,60,.t.)
        if .o_VOCALL<>.w_VOCALL
            .w_VOCINI = IIF(.w_VOCALL='S',SPACE(5),.w_VOCINI)
          .link_3_3('Full')
        endif
        .DoRTCalc(62,64,.t.)
        if .o_VORFIN<>.w_VORFIN
            .w_VORSEL = iif( Not Empty(.w_VORFIN) ,'S' , ' ')
        endif
        .DoRTCalc(66,66,.t.)
        if .o_VORALL<>.w_VORALL
            .w_VORINI = IIF(.w_VORALL='S',SPACE(5),.w_VORINI)
          .link_3_11('Full')
        endif
        .DoRTCalc(68,70,.t.)
        if .o_FORFIN<>.w_FORFIN
            .w_FORSEL = iif( Not Empty(.w_FORFIN) ,'S' , ' ')
        endif
        .DoRTCalc(72,72,.t.)
        if .o_FORALL<>.w_FORALL
            .w_FORINI = IIF(.w_FORALL='S',SPACE(5),.w_FORINI)
          .link_3_19('Full')
        endif
        .DoRTCalc(74,83,.t.)
        if .o_TIPSEL<>.w_TIPSEL
            .w_TIPARTI = '  '
        endif
        if .o_TIPSEL<>.w_TIPSEL
            .w_TIPARTI = IIF(g_PROD<>'S' and !Isalt(), 'PF', '  ')
        endif
        if .o_TIPSEL<>.w_TIPSEL
            .w_PROVART = ' '
        endif
        .DoRTCalc(87,87,.t.)
        if .o_ARTOBS<>.w_ARTOBS.or. .o_TIPSEL<>.w_TIPSEL
            .w_ARTINI = IIF(.w_ARTOBS$'OVT',SPACE(20),.w_ARTINI)
          .link_4_8('Full')
        endif
        .DoRTCalc(89,89,.t.)
        if .o_ARTOBS<>.w_ARTOBS
            .w_ARTFIN = IIF(.w_ARTOBS$'OVT',SPACE(20),.w_ARTFIN)
          .link_4_10('Full')
        endif
        .DoRTCalc(91,110,.t.)
            .w_TIPART = .w_TIPSEL
            .w_TIPVOC = 'C'
            .w_TIPVOR = 'R'
        .DoRTCalc(114,115,.t.)
            .w_ARTIPART = .w_TIPSEL
        .DoRTCalc(117,122,.t.)
        if .o_COLFIN<>.w_COLFIN
            .w_COLSEL = iif( Not Empty(.w_COLFIN) ,'S' , ' ')
        endif
        .DoRTCalc(124,124,.t.)
        if .o_COLALL<>.w_COLALL
            .w_COLINI = IIF(.w_COLALL='S',SPACE(5),.w_COLINI)
          .link_2_35('Full')
        endif
        .DoRTCalc(126,131,.t.)
        if .o_sQTAMIN<>.w_sQTAMIN
            .w_UMPRI = IIF(.w_sQTAMIN<>'S', SPACE(3), .w_sQTAMIN)
          .link_2_44('Full')
        endif
        .DoRTCalc(133,134,.t.)
        if .o_sQTAMIN<>.w_sQTAMIN.or. .o_UMPRI<>.w_UMPRI
            .w_cMINVEN = 0
        endif
        .DoRTCalc(136,137,.t.)
        if .o_PUBINI<>.w_PUBINI.or. .o_PUBSEL<>.w_PUBSEL
            .w_PUBFIN = iif(g_CPIN#'S',iif(empty(.w_PUBFIN),'S',.w_PUBFIN),iif(.w_PUBINI $ 'S-T',icase(.w_PUBINI='S','T',.w_PUBINI='T','S',.w_PUBINI),'S'))
        endif
        if .o_TIPSEL<>.w_TIPSEL
            .w_COSSEL = ' '
        endif
        .DoRTCalc(140,143,.t.)
        if .o_COSSEL<>.w_COSSEL.or. .o_Aggiorn<>.w_Aggiorn
            .w_LISTRIF = SPACE(5)
          .link_2_57('Full')
        endif
        if .o_COSSEL<>.w_COSSEL.or. .o_Aggiorn<>.w_Aggiorn.or. .o_VALUTA<>.w_VALUTA
            .w_VALRIF = .w_VALUTA
          .link_2_58('Full')
        endif
        .DoRTCalc(146,147,.t.)
        if .o_COSSEL<>.w_COSSEL.or. .o_Aggiorn<>.w_Aggiorn.or. .o_ESERCI<>.w_ESERCI
          .link_2_64('Full')
        endif
        .DoRTCalc(149,149,.t.)
        if .o_COSSEL<>.w_COSSEL.or. .o_Aggiorn<>.w_Aggiorn
            .w_ESERCI = SPACE(4)
          .link_2_66('Full')
        endif
        if .o_COSSEL<>.w_COSSEL.or. .o_ESERCI<>.w_ESERCI.or. .o_Aggiorn<>.w_Aggiorn
            .w_NUMINV = SPACE(6)
          .link_2_67('Full')
        endif
        if .o_Aggiorn<>.w_Aggiorn.or. .o_VALRIF<>.w_VALRIF
            .w_CAMBIO = GETCAM(.w_VALRIF, .w_DATRIFE, 7)
        endif
        .DoRTCalc(153,155,.t.)
        if .o_REPFIN<>.w_REPFIN
            .w_REPSEL = iif( Not Empty(.w_REPFIN) ,'S' , ' ')
        endif
        .DoRTCalc(157,157,.t.)
        if .o_REPALL<>.w_REPALL
            .w_REPINI = IIF(.w_REPALL='S',SPACE(2),.w_REPINI)
          .link_3_42('Full')
        endif
        .DoRTCalc(159,172,.t.)
        if .o_cMCTIPCAT<>.w_cMCTIPCAT
            .w_cMCCODCAT = SPACE( 5 )
          .link_2_84('Full')
        endif
        .DoRTCalc(174,175,.t.)
        if .o_TIPSEL<>.w_TIPSEL
            .w_TIPARTIN = 'PF'
        endif
        if .o_TIPSEL<>.w_TIPSEL
            .w_TIPARTIF = 'PF'
        endif
        .DoRTCalc(178,178,.t.)
        if .o_GRMFIN<>.w_GRMFIN
            .w_SACSEL = iif( Not Empty(.w_GRMFIN) ,'S' , ' ')
        endif
        .DoRTCalc(180,180,.t.)
        if .o_GRMALL<>.w_GRMALL
            .w_SACINI = IIF(.w_SACALL='S','N',.w_SACINI)
        endif
        if .o_GRMALL<>.w_GRMALL
            .w_SACFIN = IIF(.w_SACALL='S','N',.w_SACINI)
        endif
        .DoRTCalc(183,184,.t.)
        if .o_UTIALL<>.w_UTIALL
            .w_UTIINI = IIF(.w_UTIALL='S','N',.w_UTIINI)
        endif
        .DoRTCalc(186,186,.t.)
        if .o_TIPOFIN<>.w_TIPOFIN
            .w_TIPOSEL = iif( Not Empty(.w_TIPOFIN) ,'S' , ' ')
        endif
        .DoRTCalc(188,188,.t.)
        if .o_TIPOALL<>.w_TIPOALL
            .w_TIPOINI = IIF(.w_TIPOALL='S',SPACE(35),.w_TIPOINI)
        endif
        .DoRTCalc(190,190,.t.)
        if .o_VALOFIN<>.w_VALOFIN
            .w_VALOSEL = iif( Not Empty(.w_VALOFIN) ,'S' , ' ')
        endif
        .DoRTCalc(192,192,.t.)
        if .o_VALOALL<>.w_VALOALL
            .w_VALOINI = IIF(.w_VALOALL='S',SPACE(35),.w_VALOINI)
        endif
        .DoRTCalc(194,194,.t.)
        if .o_CLASFIN<>.w_CLASFIN
            .w_CLASSEL = iif( Not Empty(.w_CLASFIN) ,'S' , ' ')
        endif
        .DoRTCalc(196,196,.t.)
        if .o_CLASALL<>.w_CLASALL
            .w_CLASINI = IIF(.w_CLASALL='S',SPACE(5),.w_CLASINI)
        endif
        .DoRTCalc(198,198,.t.)
        if .o_GESCFIN<>.w_GESCFIN
            .w_GESCSEL = iif(.w_GESCFIN='S' ,'S' , ' ')
        endif
        .DoRTCalc(200,200,.t.)
        if .o_GESCALL<>.w_GESCALL
            .w_GESCINI = IIF(.w_GESCALL='S','N',.w_GESCINI)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(202,202,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oIVAINI_1_3.enabled = this.oPgFrm.Page1.oPag.oIVAINI_1_3.mCond()
    this.oPgFrm.Page1.oPag.oGRMINI_1_15.enabled = this.oPgFrm.Page1.oPag.oGRMINI_1_15.mCond()
    this.oPgFrm.Page1.oPag.oCATINI_1_23.enabled = this.oPgFrm.Page1.oPag.oCATINI_1_23.mCond()
    this.oPgFrm.Page1.oPag.oFAMALL_1_30.enabled = this.oPgFrm.Page1.oPag.oFAMALL_1_30.mCond()
    this.oPgFrm.Page1.oPag.oFAMINI_1_31.enabled = this.oPgFrm.Page1.oPag.oFAMINI_1_31.mCond()
    this.oPgFrm.Page1.oPag.oFAMFIN_1_33.enabled = this.oPgFrm.Page1.oPag.oFAMFIN_1_33.mCond()
    this.oPgFrm.Page1.oPag.oCATOSEL_1_37.enabled = this.oPgFrm.Page1.oPag.oCATOSEL_1_37.mCond()
    this.oPgFrm.Page1.oPag.oCATOALL_1_38.enabled = this.oPgFrm.Page1.oPag.oCATOALL_1_38.mCond()
    this.oPgFrm.Page1.oPag.oCATOINI_1_39.enabled = this.oPgFrm.Page1.oPag.oCATOINI_1_39.mCond()
    this.oPgFrm.Page1.oPag.oCATOFIN_1_41.enabled = this.oPgFrm.Page1.oPag.oCATOFIN_1_41.mCond()
    this.oPgFrm.Page2.oPag.oCLASEL_2_1.enabled = this.oPgFrm.Page2.oPag.oCLASEL_2_1.mCond()
    this.oPgFrm.Page2.oPag.oCLAALL_2_2.enabled = this.oPgFrm.Page2.oPag.oCLAALL_2_2.mCond()
    this.oPgFrm.Page2.oPag.oCLAINI_2_3.enabled = this.oPgFrm.Page2.oPag.oCLAINI_2_3.mCond()
    this.oPgFrm.Page2.oPag.oCLAFIN_2_5.enabled = this.oPgFrm.Page2.oPag.oCLAFIN_2_5.mCond()
    this.oPgFrm.Page2.oPag.oSCOINI_2_10.enabled = this.oPgFrm.Page2.oPag.oSCOINI_2_10.mCond()
    this.oPgFrm.Page2.oPag.oMARSEL_2_15.enabled = this.oPgFrm.Page2.oPag.oMARSEL_2_15.mCond()
    this.oPgFrm.Page2.oPag.oMARALL_2_16.enabled = this.oPgFrm.Page2.oPag.oMARALL_2_16.mCond()
    this.oPgFrm.Page2.oPag.oMARINI_2_17.enabled = this.oPgFrm.Page2.oPag.oMARINI_2_17.mCond()
    this.oPgFrm.Page2.oPag.oMARFIN_2_19.enabled = this.oPgFrm.Page2.oPag.oMARFIN_2_19.mCond()
    this.oPgFrm.Page2.oPag.oCAPINI_2_25.enabled = this.oPgFrm.Page2.oPag.oCAPINI_2_25.mCond()
    this.oPgFrm.Page3.oPag.oVOCSEL_3_1.enabled = this.oPgFrm.Page3.oPag.oVOCSEL_3_1.mCond()
    this.oPgFrm.Page3.oPag.oVOCALL_3_2.enabled = this.oPgFrm.Page3.oPag.oVOCALL_3_2.mCond()
    this.oPgFrm.Page3.oPag.oVOCINI_3_3.enabled = this.oPgFrm.Page3.oPag.oVOCINI_3_3.mCond()
    this.oPgFrm.Page3.oPag.oVOCFIN_3_5.enabled = this.oPgFrm.Page3.oPag.oVOCFIN_3_5.mCond()
    this.oPgFrm.Page3.oPag.oVORSEL_3_9.enabled = this.oPgFrm.Page3.oPag.oVORSEL_3_9.mCond()
    this.oPgFrm.Page3.oPag.oVORALL_3_10.enabled = this.oPgFrm.Page3.oPag.oVORALL_3_10.mCond()
    this.oPgFrm.Page3.oPag.oVORINI_3_11.enabled = this.oPgFrm.Page3.oPag.oVORINI_3_11.mCond()
    this.oPgFrm.Page3.oPag.oVORFIN_3_13.enabled = this.oPgFrm.Page3.oPag.oVORFIN_3_13.mCond()
    this.oPgFrm.Page3.oPag.oFORSEL_3_17.enabled = this.oPgFrm.Page3.oPag.oFORSEL_3_17.mCond()
    this.oPgFrm.Page3.oPag.oFORALL_3_18.enabled = this.oPgFrm.Page3.oPag.oFORALL_3_18.mCond()
    this.oPgFrm.Page3.oPag.oFORINI_3_19.enabled = this.oPgFrm.Page3.oPag.oFORINI_3_19.mCond()
    this.oPgFrm.Page3.oPag.oFORFIN_3_21.enabled = this.oPgFrm.Page3.oPag.oFORFIN_3_21.mCond()
    this.oPgFrm.Page3.oPag.oGESSEL_3_25.enabled = this.oPgFrm.Page3.oPag.oGESSEL_3_25.mCond()
    this.oPgFrm.Page3.oPag.oGESINI_3_26.enabled = this.oPgFrm.Page3.oPag.oGESINI_3_26.mCond()
    this.oPgFrm.Page3.oPag.oGESFIN_3_29.enabled = this.oPgFrm.Page3.oPag.oGESFIN_3_29.mCond()
    this.oPgFrm.Page3.oPag.oPROVSEL_3_30.enabled = this.oPgFrm.Page3.oPag.oPROVSEL_3_30.mCond()
    this.oPgFrm.Page3.oPag.oPROVINI_3_31.enabled = this.oPgFrm.Page3.oPag.oPROVINI_3_31.mCond()
    this.oPgFrm.Page3.oPag.oPROVFIN_3_33.enabled = this.oPgFrm.Page3.oPag.oPROVFIN_3_33.mCond()
    this.oPgFrm.Page4.oPag.oTIPARTI_4_2.enabled = this.oPgFrm.Page4.oPag.oTIPARTI_4_2.mCond()
    this.oPgFrm.Page4.oPag.oTIPARTI_4_3.enabled = this.oPgFrm.Page4.oPag.oTIPARTI_4_3.mCond()
    this.oPgFrm.Page4.oPag.oPROVART_4_4.enabled = this.oPgFrm.Page4.oPag.oPROVART_4_4.mCond()
    this.oPgFrm.Page4.oPag.oFAMFIL_4_20.enabled = this.oPgFrm.Page4.oPag.oFAMFIL_4_20.mCond()
    this.oPgFrm.Page4.oPag.oCATOFIL_4_24.enabled = this.oPgFrm.Page4.oPag.oCATOFIL_4_24.mCond()
    this.oPgFrm.Page4.oPag.oPROFIL_4_26.enabled = this.oPgFrm.Page4.oPag.oPROFIL_4_26.mCond()
    this.oPgFrm.Page4.oPag.oFORFIL_4_30.enabled = this.oPgFrm.Page4.oPag.oFORFIL_4_30.mCond()
    this.oPgFrm.Page4.oPag.oMARFIL_4_32.enabled = this.oPgFrm.Page4.oPag.oMARFIL_4_32.mCond()
    this.oPgFrm.Page4.oPag.oCOLFIL_4_44.enabled = this.oPgFrm.Page4.oPag.oCOLFIL_4_44.mCond()
    this.oPgFrm.Page4.oPag.oCONFIL_4_45.enabled = this.oPgFrm.Page4.oPag.oCONFIL_4_45.mCond()
    this.oPgFrm.Page2.oPag.oCOLSEL_2_33.enabled = this.oPgFrm.Page2.oPag.oCOLSEL_2_33.mCond()
    this.oPgFrm.Page2.oPag.oCOLALL_2_34.enabled = this.oPgFrm.Page2.oPag.oCOLALL_2_34.mCond()
    this.oPgFrm.Page2.oPag.oCOLINI_2_35.enabled = this.oPgFrm.Page2.oPag.oCOLINI_2_35.mCond()
    this.oPgFrm.Page2.oPag.oCOLFIN_2_37.enabled = this.oPgFrm.Page2.oPag.oCOLFIN_2_37.mCond()
    this.oPgFrm.Page2.oPag.oCONDEF_2_42.enabled = this.oPgFrm.Page2.oPag.oCONDEF_2_42.mCond()
    this.oPgFrm.Page2.oPag.oUMPRI_2_44.enabled = this.oPgFrm.Page2.oPag.oUMPRI_2_44.mCond()
    this.oPgFrm.Page2.oPag.ocMINVEN_2_47.enabled = this.oPgFrm.Page2.oPag.ocMINVEN_2_47.mCond()
    this.oPgFrm.Page2.oPag.oPUBINI_2_48.enabled = this.oPgFrm.Page2.oPag.oPUBINI_2_48.mCond()
    this.oPgFrm.Page2.oPag.oPUBSEL_2_49.enabled = this.oPgFrm.Page2.oPag.oPUBSEL_2_49.mCond()
    this.oPgFrm.Page2.oPag.oPUBFIN_2_50.enabled = this.oPgFrm.Page2.oPag.oPUBFIN_2_50.mCond()
    this.oPgFrm.Page2.oPag.oCOSSEL_2_52.enabled = this.oPgFrm.Page2.oPag.oCOSSEL_2_52.mCond()
    this.oPgFrm.Page2.oPag.oCOSALL_2_53.enabled = this.oPgFrm.Page2.oPag.oCOSALL_2_53.mCond()
    this.oPgFrm.Page2.oPag.oDISTIN_2_54.enabled = this.oPgFrm.Page2.oPag.oDISTIN_2_54.mCond()
    this.oPgFrm.Page2.oPag.oAggiorn_2_55.enabled = this.oPgFrm.Page2.oPag.oAggiorn_2_55.mCond()
    this.oPgFrm.Page2.oPag.oLISTRIF_2_57.enabled = this.oPgFrm.Page2.oPag.oLISTRIF_2_57.mCond()
    this.oPgFrm.Page2.oPag.oESERCI_2_66.enabled = this.oPgFrm.Page2.oPag.oESERCI_2_66.mCond()
    this.oPgFrm.Page2.oPag.oNUMINV_2_67.enabled = this.oPgFrm.Page2.oPag.oNUMINV_2_67.mCond()
    this.oPgFrm.Page2.oPag.oCAMBIO_2_68.enabled = this.oPgFrm.Page2.oPag.oCAMBIO_2_68.mCond()
    this.oPgFrm.Page3.oPag.oFLGPOS_3_38.enabled = this.oPgFrm.Page3.oPag.oFLGPOS_3_38.mCond()
    this.oPgFrm.Page3.oPag.oREPSEL_3_40.enabled = this.oPgFrm.Page3.oPag.oREPSEL_3_40.mCond()
    this.oPgFrm.Page3.oPag.oREPALL_3_41.enabled = this.oPgFrm.Page3.oPag.oREPALL_3_41.mCond()
    this.oPgFrm.Page3.oPag.oREPINI_3_42.enabled = this.oPgFrm.Page3.oPag.oREPINI_3_42.mCond()
    this.oPgFrm.Page3.oPag.oREPFIN_3_44.enabled = this.oPgFrm.Page3.oPag.oREPFIN_3_44.mCond()
    this.oPgFrm.Page3.oPag.oPECOSEL_3_47.enabled = this.oPgFrm.Page3.oPag.oPECOSEL_3_47.mCond()
    this.oPgFrm.Page3.oPag.oPECOINI_3_48.enabled = this.oPgFrm.Page3.oPag.oPECOINI_3_48.mCond()
    this.oPgFrm.Page3.oPag.oPECOFIN_3_51.enabled = this.oPgFrm.Page3.oPag.oPECOFIN_3_51.mCond()
    this.oPgFrm.Page4.oPag.oREPFIL_4_48.enabled = this.oPgFrm.Page4.oPag.oREPFIL_4_48.mCond()
    this.oPgFrm.Page3.oPag.oTIPARTIN_3_53.enabled = this.oPgFrm.Page3.oPag.oTIPARTIN_3_53.mCond()
    this.oPgFrm.Page3.oPag.oTIPARTIF_3_55.enabled = this.oPgFrm.Page3.oPag.oTIPARTIF_3_55.mCond()
    this.oPgFrm.Page3.oPag.oTIPASEL_3_56.enabled = this.oPgFrm.Page3.oPag.oTIPASEL_3_56.mCond()
    this.oPgFrm.Page1.oPag.oSACINI_1_53.enabled = this.oPgFrm.Page1.oPag.oSACINI_1_53.mCond()
    this.oPgFrm.Page1.oPag.oUTIALL_1_57.enabled = this.oPgFrm.Page1.oPag.oUTIALL_1_57.mCond()
    this.oPgFrm.Page1.oPag.oUTIINI_1_58.enabled = this.oPgFrm.Page1.oPag.oUTIINI_1_58.mCond()
    this.oPgFrm.Page1.oPag.oUTIFIN_1_61.enabled = this.oPgFrm.Page1.oPag.oUTIFIN_1_61.mCond()
    this.oPgFrm.Page1.oPag.oTIPOINI_1_67.enabled = this.oPgFrm.Page1.oPag.oTIPOINI_1_67.mCond()
    this.oPgFrm.Page1.oPag.oVALOINI_1_73.enabled = this.oPgFrm.Page1.oPag.oVALOINI_1_73.mCond()
    this.oPgFrm.Page1.oPag.oCLASINI_1_79.enabled = this.oPgFrm.Page1.oPag.oCLASINI_1_79.mCond()
    this.oPgFrm.Page1.oPag.oGESCINI_1_86.enabled = this.oPgFrm.Page1.oPag.oGESCINI_1_86.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show1
    i_show1=not(IsAlt())
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Commerciali/magazzino"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    this.oPgFrm.Page1.oPag.oGRMSEL_1_13.visible=!this.oPgFrm.Page1.oPag.oGRMSEL_1_13.mHide()
    this.oPgFrm.Page1.oPag.oGRMALL_1_14.visible=!this.oPgFrm.Page1.oPag.oGRMALL_1_14.mHide()
    this.oPgFrm.Page1.oPag.oGRMINI_1_15.visible=!this.oPgFrm.Page1.oPag.oGRMINI_1_15.mHide()
    this.oPgFrm.Page1.oPag.oDESGRU_1_16.visible=!this.oPgFrm.Page1.oPag.oDESGRU_1_16.mHide()
    this.oPgFrm.Page1.oPag.oGRMFIN_1_17.visible=!this.oPgFrm.Page1.oPag.oGRMFIN_1_17.mHide()
    this.oPgFrm.Page1.oPag.oDESGRU1_1_18.visible=!this.oPgFrm.Page1.oPag.oDESGRU1_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oFAMSEL_1_29.visible=!this.oPgFrm.Page1.oPag.oFAMSEL_1_29.mHide()
    this.oPgFrm.Page1.oPag.oFAMALL_1_30.visible=!this.oPgFrm.Page1.oPag.oFAMALL_1_30.mHide()
    this.oPgFrm.Page1.oPag.oFAMINI_1_31.visible=!this.oPgFrm.Page1.oPag.oFAMINI_1_31.mHide()
    this.oPgFrm.Page1.oPag.oDESFAM_1_32.visible=!this.oPgFrm.Page1.oPag.oDESFAM_1_32.mHide()
    this.oPgFrm.Page1.oPag.oFAMFIN_1_33.visible=!this.oPgFrm.Page1.oPag.oFAMFIN_1_33.mHide()
    this.oPgFrm.Page1.oPag.oDESFAM1_1_34.visible=!this.oPgFrm.Page1.oPag.oDESFAM1_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oCATOSEL_1_37.visible=!this.oPgFrm.Page1.oPag.oCATOSEL_1_37.mHide()
    this.oPgFrm.Page1.oPag.oCATOALL_1_38.visible=!this.oPgFrm.Page1.oPag.oCATOALL_1_38.mHide()
    this.oPgFrm.Page1.oPag.oCATOINI_1_39.visible=!this.oPgFrm.Page1.oPag.oCATOINI_1_39.mHide()
    this.oPgFrm.Page1.oPag.oDESCATO_1_40.visible=!this.oPgFrm.Page1.oPag.oDESCATO_1_40.mHide()
    this.oPgFrm.Page1.oPag.oCATOFIN_1_41.visible=!this.oPgFrm.Page1.oPag.oCATOFIN_1_41.mHide()
    this.oPgFrm.Page1.oPag.oDESCATO1_1_42.visible=!this.oPgFrm.Page1.oPag.oDESCATO1_1_42.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page3.oPag.oFORSEL_3_17.visible=!this.oPgFrm.Page3.oPag.oFORSEL_3_17.mHide()
    this.oPgFrm.Page3.oPag.oFORALL_3_18.visible=!this.oPgFrm.Page3.oPag.oFORALL_3_18.mHide()
    this.oPgFrm.Page3.oPag.oFORINI_3_19.visible=!this.oPgFrm.Page3.oPag.oFORINI_3_19.mHide()
    this.oPgFrm.Page3.oPag.oDESFOR_3_20.visible=!this.oPgFrm.Page3.oPag.oDESFOR_3_20.mHide()
    this.oPgFrm.Page3.oPag.oFORFIN_3_21.visible=!this.oPgFrm.Page3.oPag.oFORFIN_3_21.mHide()
    this.oPgFrm.Page3.oPag.oDESFOR1_3_22.visible=!this.oPgFrm.Page3.oPag.oDESFOR1_3_22.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_23.visible=!this.oPgFrm.Page3.oPag.oStr_3_23.mHide()
    this.oPgFrm.Page3.oPag.oGESSEL_3_25.visible=!this.oPgFrm.Page3.oPag.oGESSEL_3_25.mHide()
    this.oPgFrm.Page3.oPag.oGESINI_3_26.visible=!this.oPgFrm.Page3.oPag.oGESINI_3_26.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_28.visible=!this.oPgFrm.Page3.oPag.oStr_3_28.mHide()
    this.oPgFrm.Page3.oPag.oGESFIN_3_29.visible=!this.oPgFrm.Page3.oPag.oGESFIN_3_29.mHide()
    this.oPgFrm.Page3.oPag.oPROVSEL_3_30.visible=!this.oPgFrm.Page3.oPag.oPROVSEL_3_30.mHide()
    this.oPgFrm.Page3.oPag.oPROVINI_3_31.visible=!this.oPgFrm.Page3.oPag.oPROVINI_3_31.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_32.visible=!this.oPgFrm.Page3.oPag.oStr_3_32.mHide()
    this.oPgFrm.Page3.oPag.oPROVFIN_3_33.visible=!this.oPgFrm.Page3.oPag.oPROVFIN_3_33.mHide()
    this.oPgFrm.Page4.oPag.oTIPSEL_4_1.visible=!this.oPgFrm.Page4.oPag.oTIPSEL_4_1.mHide()
    this.oPgFrm.Page4.oPag.oTIPARTI_4_2.visible=!this.oPgFrm.Page4.oPag.oTIPARTI_4_2.mHide()
    this.oPgFrm.Page4.oPag.oTIPARTI_4_3.visible=!this.oPgFrm.Page4.oPag.oTIPARTI_4_3.mHide()
    this.oPgFrm.Page4.oPag.oPROVART_4_4.visible=!this.oPgFrm.Page4.oPag.oPROVART_4_4.mHide()
    this.oPgFrm.Page4.oPag.oGRMFIL_4_14.visible=!this.oPgFrm.Page4.oPag.oGRMFIL_4_14.mHide()
    this.oPgFrm.Page4.oPag.oGRMDES_4_15.visible=!this.oPgFrm.Page4.oPag.oGRMDES_4_15.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_17.visible=!this.oPgFrm.Page4.oPag.oStr_4_17.mHide()
    this.oPgFrm.Page4.oPag.oFAMFIL_4_20.visible=!this.oPgFrm.Page4.oPag.oFAMFIL_4_20.mHide()
    this.oPgFrm.Page4.oPag.oFAMDES_4_21.visible=!this.oPgFrm.Page4.oPag.oFAMDES_4_21.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_23.visible=!this.oPgFrm.Page4.oPag.oStr_4_23.mHide()
    this.oPgFrm.Page4.oPag.oCATOFIL_4_24.visible=!this.oPgFrm.Page4.oPag.oCATOFIL_4_24.mHide()
    this.oPgFrm.Page4.oPag.oCATODES_4_25.visible=!this.oPgFrm.Page4.oPag.oCATODES_4_25.mHide()
    this.oPgFrm.Page4.oPag.oPROFIL_4_26.visible=!this.oPgFrm.Page4.oPag.oPROFIL_4_26.mHide()
    this.oPgFrm.Page4.oPag.oPRODES_4_27.visible=!this.oPgFrm.Page4.oPag.oPRODES_4_27.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_28.visible=!this.oPgFrm.Page4.oPag.oStr_4_28.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_29.visible=!this.oPgFrm.Page4.oPag.oStr_4_29.mHide()
    this.oPgFrm.Page4.oPag.oFORFIL_4_30.visible=!this.oPgFrm.Page4.oPag.oFORFIL_4_30.mHide()
    this.oPgFrm.Page4.oPag.oFORDES_4_31.visible=!this.oPgFrm.Page4.oPag.oFORDES_4_31.mHide()
    this.oPgFrm.Page4.oPag.oMARFIL_4_32.visible=!this.oPgFrm.Page4.oPag.oMARFIL_4_32.mHide()
    this.oPgFrm.Page4.oPag.oMARDES_4_33.visible=!this.oPgFrm.Page4.oPag.oMARDES_4_33.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_34.visible=!this.oPgFrm.Page4.oPag.oStr_4_34.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_35.visible=!this.oPgFrm.Page4.oPag.oStr_4_35.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_42.visible=!this.oPgFrm.Page4.oPag.oStr_4_42.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_43.visible=!this.oPgFrm.Page4.oPag.oStr_4_43.mHide()
    this.oPgFrm.Page4.oPag.oCOLFIL_4_44.visible=!this.oPgFrm.Page4.oPag.oCOLFIL_4_44.mHide()
    this.oPgFrm.Page4.oPag.oCONFIL_4_45.visible=!this.oPgFrm.Page4.oPag.oCONFIL_4_45.mHide()
    this.oPgFrm.Page4.oPag.oCOLDES_4_46.visible=!this.oPgFrm.Page4.oPag.oCOLDES_4_46.mHide()
    this.oPgFrm.Page4.oPag.oCONDES_4_47.visible=!this.oPgFrm.Page4.oPag.oCONDES_4_47.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_41.visible=!this.oPgFrm.Page2.oPag.oStr_2_41.mHide()
    this.oPgFrm.Page2.oPag.oCONDEF_2_42.visible=!this.oPgFrm.Page2.oPag.oCONDEF_2_42.mHide()
    this.oPgFrm.Page2.oPag.oUMPRI_2_44.visible=!this.oPgFrm.Page2.oPag.oUMPRI_2_44.mHide()
    this.oPgFrm.Page2.oPag.oDESCOF_2_46.visible=!this.oPgFrm.Page2.oPag.oDESCOF_2_46.mHide()
    this.oPgFrm.Page2.oPag.ocMINVEN_2_47.visible=!this.oPgFrm.Page2.oPag.ocMINVEN_2_47.mHide()
    this.oPgFrm.Page2.oPag.oPUBINI_2_48.visible=!this.oPgFrm.Page2.oPag.oPUBINI_2_48.mHide()
    this.oPgFrm.Page2.oPag.oPUBSEL_2_49.visible=!this.oPgFrm.Page2.oPag.oPUBSEL_2_49.mHide()
    this.oPgFrm.Page2.oPag.oPUBFIN_2_50.visible=!this.oPgFrm.Page2.oPag.oPUBFIN_2_50.mHide()
    this.oPgFrm.Page2.oPag.oCOSALL_2_53.visible=!this.oPgFrm.Page2.oPag.oCOSALL_2_53.mHide()
    this.oPgFrm.Page2.oPag.oDISTIN_2_54.visible=!this.oPgFrm.Page2.oPag.oDISTIN_2_54.mHide()
    this.oPgFrm.Page2.oPag.oAggiorn_2_55.visible=!this.oPgFrm.Page2.oPag.oAggiorn_2_55.mHide()
    this.oPgFrm.Page2.oPag.oLISTRIF_2_57.visible=!this.oPgFrm.Page2.oPag.oLISTRIF_2_57.mHide()
    this.oPgFrm.Page2.oPag.oSIMVAL_2_59.visible=!this.oPgFrm.Page2.oPag.oSIMVAL_2_59.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_60.visible=!this.oPgFrm.Page2.oPag.oStr_2_60.mHide()
    this.oPgFrm.Page2.oPag.oESERCI_2_66.visible=!this.oPgFrm.Page2.oPag.oESERCI_2_66.mHide()
    this.oPgFrm.Page2.oPag.oNUMINV_2_67.visible=!this.oPgFrm.Page2.oPag.oNUMINV_2_67.mHide()
    this.oPgFrm.Page2.oPag.oCAMBIO_2_68.visible=!this.oPgFrm.Page2.oPag.oCAMBIO_2_68.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_69.visible=!this.oPgFrm.Page2.oPag.oStr_2_69.mHide()
    this.oPgFrm.Page2.oPag.oRICPE_2_70.visible=!this.oPgFrm.Page2.oPag.oRICPE_2_70.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_71.visible=!this.oPgFrm.Page2.oPag.oStr_2_71.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_72.visible=!this.oPgFrm.Page2.oPag.oStr_2_72.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_74.visible=!this.oPgFrm.Page2.oPag.oStr_2_74.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_75.visible=!this.oPgFrm.Page2.oPag.oStr_2_75.mHide()
    this.oPgFrm.Page3.oPag.oFLGPOS_3_38.visible=!this.oPgFrm.Page3.oPag.oFLGPOS_3_38.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_39.visible=!this.oPgFrm.Page3.oPag.oStr_3_39.mHide()
    this.oPgFrm.Page3.oPag.oREPSEL_3_40.visible=!this.oPgFrm.Page3.oPag.oREPSEL_3_40.mHide()
    this.oPgFrm.Page3.oPag.oREPALL_3_41.visible=!this.oPgFrm.Page3.oPag.oREPALL_3_41.mHide()
    this.oPgFrm.Page3.oPag.oREPINI_3_42.visible=!this.oPgFrm.Page3.oPag.oREPINI_3_42.mHide()
    this.oPgFrm.Page3.oPag.oDESREP_3_43.visible=!this.oPgFrm.Page3.oPag.oDESREP_3_43.mHide()
    this.oPgFrm.Page3.oPag.oREPFIN_3_44.visible=!this.oPgFrm.Page3.oPag.oREPFIN_3_44.mHide()
    this.oPgFrm.Page3.oPag.oDESREP1_3_45.visible=!this.oPgFrm.Page3.oPag.oDESREP1_3_45.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_46.visible=!this.oPgFrm.Page3.oPag.oStr_3_46.mHide()
    this.oPgFrm.Page3.oPag.oPECOSEL_3_47.visible=!this.oPgFrm.Page3.oPag.oPECOSEL_3_47.mHide()
    this.oPgFrm.Page3.oPag.oPECOINI_3_48.visible=!this.oPgFrm.Page3.oPag.oPECOINI_3_48.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_49.visible=!this.oPgFrm.Page3.oPag.oStr_3_49.mHide()
    this.oPgFrm.Page3.oPag.oPECOFIN_3_51.visible=!this.oPgFrm.Page3.oPag.oPECOFIN_3_51.mHide()
    this.oPgFrm.Page4.oPag.oREPFIL_4_48.visible=!this.oPgFrm.Page4.oPag.oREPFIL_4_48.mHide()
    this.oPgFrm.Page4.oPag.oREPDES_4_49.visible=!this.oPgFrm.Page4.oPag.oREPDES_4_49.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_50.visible=!this.oPgFrm.Page4.oPag.oStr_4_50.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_77.visible=!this.oPgFrm.Page2.oPag.oStr_2_77.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_51.visible=!this.oPgFrm.Page4.oPag.oStr_4_51.mHide()
    this.oPgFrm.Page2.oPag.oFLGCAC_2_79.visible=!this.oPgFrm.Page2.oPag.oFLGCAC_2_79.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_80.visible=!this.oPgFrm.Page2.oPag.oStr_2_80.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_81.visible=!this.oPgFrm.Page2.oPag.oStr_2_81.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_82.visible=!this.oPgFrm.Page2.oPag.oStr_2_82.mHide()
    this.oPgFrm.Page2.oPag.ocMCTIPCAT_2_83.visible=!this.oPgFrm.Page2.oPag.ocMCTIPCAT_2_83.mHide()
    this.oPgFrm.Page2.oPag.ocMCCODCAT_2_84.visible=!this.oPgFrm.Page2.oPag.ocMCCODCAT_2_84.mHide()
    this.oPgFrm.Page2.oPag.oTPDESCRI_2_85.visible=!this.oPgFrm.Page2.oPag.oTPDESCRI_2_85.mHide()
    this.oPgFrm.Page2.oPag.oCGDESCRI_2_86.visible=!this.oPgFrm.Page2.oPag.oCGDESCRI_2_86.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_53.visible=!this.oPgFrm.Page4.oPag.oStr_4_53.mHide()
    this.oPgFrm.Page3.oPag.oTIPARTIN_3_53.visible=!this.oPgFrm.Page3.oPag.oTIPARTIN_3_53.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_54.visible=!this.oPgFrm.Page3.oPag.oStr_3_54.mHide()
    this.oPgFrm.Page3.oPag.oTIPARTIF_3_55.visible=!this.oPgFrm.Page3.oPag.oTIPARTIF_3_55.mHide()
    this.oPgFrm.Page3.oPag.oTIPASEL_3_56.visible=!this.oPgFrm.Page3.oPag.oTIPASEL_3_56.mHide()
    this.oPgFrm.Page1.oPag.oSACSEL_1_51.visible=!this.oPgFrm.Page1.oPag.oSACSEL_1_51.mHide()
    this.oPgFrm.Page1.oPag.oSACALL_1_52.visible=!this.oPgFrm.Page1.oPag.oSACALL_1_52.mHide()
    this.oPgFrm.Page1.oPag.oSACINI_1_53.visible=!this.oPgFrm.Page1.oPag.oSACINI_1_53.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    this.oPgFrm.Page1.oPag.oSACFIN_1_55.visible=!this.oPgFrm.Page1.oPag.oSACFIN_1_55.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_87.visible=!this.oPgFrm.Page2.oPag.oStr_2_87.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_62.visible=!this.oPgFrm.Page1.oPag.oBtn_1_62.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_63.visible=!this.oPgFrm.Page1.oPag.oBtn_1_63.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_57.visible=!this.oPgFrm.Page3.oPag.oBtn_3_57.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_58.visible=!this.oPgFrm.Page3.oPag.oBtn_3_58.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IVAINI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IVAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_IVAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_IVAINI))
          select IVCODIVA,IVDESIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IVAINI)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IVAINI) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oIVAINI_1_3'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IVAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_IVAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_IVAINI)
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IVAINI = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_IVAINI = space(5)
      endif
      this.w_DESIVA = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Iva obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA incongruente o obsoleto")
        endif
        this.w_IVAINI = space(5)
        this.w_DESIVA = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IVAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IVAFIN
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IVAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_IVAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_IVAFIN))
          select IVCODIVA,IVDESIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IVAFIN)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IVAFIN) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oIVAFIN_1_5'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IVAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_IVAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_IVAFIN)
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IVAFIN = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA1 = NVL(_Link_.IVDESIVA,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_IVAFIN = space(5)
      endif
      this.w_DESIVA1 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Iva obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA incongruente o obsoleto")
        endif
        this.w_IVAFIN = space(5)
        this.w_DESIVA1 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IVAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CIMINI
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCODIV_IDX,3]
    i_lTable = "TIPCODIV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2], .t., this.TIPCODIV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CIMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATO',True,'TIPCODIV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_CIMINI)+"%");
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_TIPOPEIVA);

          i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TI__TIPO,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TI__TIPO',this.w_TIPOPEIVA;
                     ,'TICODICE',trim(this.w_CIMINI))
          select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TI__TIPO,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CIMINI)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TIDESCRI like "+cp_ToStrODBC(trim(this.w_CIMINI)+"%");
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_TIPOPEIVA);

            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TIDESCRI like "+cp_ToStr(trim(this.w_CIMINI)+"%");
                   +" and TI__TIPO="+cp_ToStr(this.w_TIPOPEIVA);

            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CIMINI) and !this.bDontReportError
            deferred_cp_zoom('TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(oSource.parent,'oCIMINI_1_9'),i_cWhere,'GSAR_ATO',"Tipologia operazione I.V.A.",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOPEIVA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice operazione I.V.A. inesistente, oppure non valida alla data odierna")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TI__TIPO="+cp_ToStrODBC(this.w_TIPOPEIVA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',oSource.xKey(1);
                       ,'TICODICE',oSource.xKey(2))
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CIMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_CIMINI);
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_TIPOPEIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',this.w_TIPOPEIVA;
                       ,'TICODICE',this.w_CIMINI)
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CIMINI = NVL(_Link_.TICODICE,space(10))
      this.w_DESOI = NVL(_Link_.TIDESCRI,space(35))
      this.w_TIPOPOBS = NVL(cp_ToDate(_Link_.TIDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CIMINI = space(10)
      endif
      this.w_DESOI = space(35)
      this.w_TIPOPOBS = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=i_datsys >= .w_TIPOPOBS
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice operazione I.V.A. inesistente, oppure non valida alla data odierna")
        endif
        this.w_CIMINI = space(10)
        this.w_DESOI = space(35)
        this.w_TIPOPOBS = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])+'\'+cp_ToStr(_Link_.TI__TIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCODIV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CIMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CIMFIN
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCODIV_IDX,3]
    i_lTable = "TIPCODIV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2], .t., this.TIPCODIV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CIMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATO',True,'TIPCODIV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_CIMFIN)+"%");
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_TIPOPEIVA);

          i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TI__TIPO,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TI__TIPO',this.w_TIPOPEIVA;
                     ,'TICODICE',trim(this.w_CIMFIN))
          select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TI__TIPO,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CIMFIN)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TIDESCRI like "+cp_ToStrODBC(trim(this.w_CIMFIN)+"%");
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_TIPOPEIVA);

            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TIDESCRI like "+cp_ToStr(trim(this.w_CIMFIN)+"%");
                   +" and TI__TIPO="+cp_ToStr(this.w_TIPOPEIVA);

            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CIMFIN) and !this.bDontReportError
            deferred_cp_zoom('TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(oSource.parent,'oCIMFIN_1_10'),i_cWhere,'GSAR_ATO',"Tipologia operazione I.V.A.",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOPEIVA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice operazione I.V.A. inesistente, oppure non valida alla data odierna")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TI__TIPO="+cp_ToStrODBC(this.w_TIPOPEIVA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',oSource.xKey(1);
                       ,'TICODICE',oSource.xKey(2))
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CIMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_CIMFIN);
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_TIPOPEIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',this.w_TIPOPEIVA;
                       ,'TICODICE',this.w_CIMFIN)
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CIMFIN = NVL(_Link_.TICODICE,space(10))
      this.w_DESOI1 = NVL(_Link_.TIDESCRI,space(35))
      this.w_TIPOPOBS = NVL(cp_ToDate(_Link_.TIDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CIMFIN = space(10)
      endif
      this.w_DESOI1 = space(35)
      this.w_TIPOPOBS = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=i_datsys >= .w_TIPOPOBS
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice operazione I.V.A. inesistente, oppure non valida alla data odierna")
        endif
        this.w_CIMFIN = space(10)
        this.w_DESOI1 = space(35)
        this.w_TIPOPOBS = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])+'\'+cp_ToStr(_Link_.TI__TIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCODIV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CIMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRMINI
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRMINI))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRMINI)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRMINI) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRMINI_1_15'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRMINI)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRMINI = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRMINI = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRMFIN
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRMFIN))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRMFIN)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRMFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRMFIN_1_17'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRMFIN)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRMFIN = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU1 = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRMFIN = space(5)
      endif
      this.w_DESGRU1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATINI
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_lTable = "CACOARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2], .t., this.CACOARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC1',True,'CACOARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C1CODICE like "+cp_ToStrODBC(trim(this.w_CATINI)+"%");

          i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C1CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C1CODICE',trim(this.w_CATINI))
          select C1CODICE,C1DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C1CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATINI)==trim(_Link_.C1CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATINI) and !this.bDontReportError
            deferred_cp_zoom('CACOARTI','*','C1CODICE',cp_AbsName(oSource.parent,'oCATINI_1_23'),i_cWhere,'GSAR_AC1',"Categorie contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',oSource.xKey(1))
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(this.w_CATINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',this.w_CATINI)
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATINI = NVL(_Link_.C1CODICE,space(5))
      this.w_DESCAT = NVL(_Link_.C1DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATINI = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])+'\'+cp_ToStr(_Link_.C1CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIN
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_lTable = "CACOARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2], .t., this.CACOARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC1',True,'CACOARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C1CODICE like "+cp_ToStrODBC(trim(this.w_CATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C1CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C1CODICE',trim(this.w_CATFIN))
          select C1CODICE,C1DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C1CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIN)==trim(_Link_.C1CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIN) and !this.bDontReportError
            deferred_cp_zoom('CACOARTI','*','C1CODICE',cp_AbsName(oSource.parent,'oCATFIN_1_25'),i_cWhere,'GSAR_AC1',"Categorie contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',oSource.xKey(1))
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(this.w_CATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',this.w_CATFIN)
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIN = NVL(_Link_.C1CODICE,space(5))
      this.w_DESCAT1 = NVL(_Link_.C1DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIN = space(5)
      endif
      this.w_DESCAT1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])+'\'+cp_ToStr(_Link_.C1CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMINI
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMINI)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMINI))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMINI)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMINI) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMINI_1_31'),i_cWhere,'GSAR_AFA',"Codici famiglie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMINI)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMINI = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMINI = space(5)
      endif
      this.w_DESFAM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMFIN
  func Link_1_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMFIN))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMFIN)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMFIN) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMFIN_1_33'),i_cWhere,'GSAR_AFA',"Codici famiglie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMFIN)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMFIN = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM1 = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMFIN = space(5)
      endif
      this.w_DESFAM1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATOINI
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATOINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATOINI)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATOINI))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATOINI)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATOINI) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATOINI_1_39'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATOINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATOINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATOINI)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATOINI = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATO = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATOINI = space(5)
      endif
      this.w_DESCATO = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATOINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATOFIN
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATOFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATOFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATOFIN))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATOFIN)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATOFIN) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATOFIN_1_41'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATOFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATOFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATOFIN)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATOFIN = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCATO1 = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATOFIN = space(5)
      endif
      this.w_DESCATO1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATOFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLAINI
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RICA_IDX,3]
    i_lTable = "CLA_RICA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2], .t., this.CLA_RICA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLAINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACR',True,'CLA_RICA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CRCODICE like "+cp_ToStrODBC(trim(this.w_CLAINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CRCODICE',trim(this.w_CLAINI))
          select CRCODICE,CRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLAINI)==trim(_Link_.CRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLAINI) and !this.bDontReportError
            deferred_cp_zoom('CLA_RICA','*','CRCODICE',cp_AbsName(oSource.parent,'oCLAINI_2_3'),i_cWhere,'GSAR_ACR',"Classi di ricarico",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',oSource.xKey(1))
            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLAINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(this.w_CLAINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',this.w_CLAINI)
            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLAINI = NVL(_Link_.CRCODICE,space(5))
      this.w_DESCLA = NVL(_Link_.CRDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CLAINI = space(5)
      endif
      this.w_DESCLA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])+'\'+cp_ToStr(_Link_.CRCODICE,1)
      cp_ShowWarn(i_cKey,this.CLA_RICA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLAINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLAFIN
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RICA_IDX,3]
    i_lTable = "CLA_RICA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2], .t., this.CLA_RICA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLAFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACR',True,'CLA_RICA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CRCODICE like "+cp_ToStrODBC(trim(this.w_CLAFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CRCODICE',trim(this.w_CLAFIN))
          select CRCODICE,CRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLAFIN)==trim(_Link_.CRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLAFIN) and !this.bDontReportError
            deferred_cp_zoom('CLA_RICA','*','CRCODICE',cp_AbsName(oSource.parent,'oCLAFIN_2_5'),i_cWhere,'GSAR_ACR',"Classi di ricarico",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',oSource.xKey(1))
            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLAFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(this.w_CLAFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',this.w_CLAFIN)
            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLAFIN = NVL(_Link_.CRCODICE,space(5))
      this.w_DESCLA1 = NVL(_Link_.CRDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CLAFIN = space(5)
      endif
      this.w_DESCLA1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])+'\'+cp_ToStr(_Link_.CRCODICE,1)
      cp_ShowWarn(i_cKey,this.CLA_RICA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLAFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCOINI
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
    i_lTable = "CAT_SCMA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2], .t., this.CAT_SCMA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCOINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASM',True,'CAT_SCMA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_SCOINI)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_SCOINI))
          select CSCODICE,CSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCOINI)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCOINI) and !this.bDontReportError
            deferred_cp_zoom('CAT_SCMA','*','CSCODICE',cp_AbsName(oSource.parent,'oSCOINI_2_10'),i_cWhere,'GSAR_ASM',"Cat.sconti/maggiorazioni",'GSAR_SCO.CAT_SCMA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCOINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_SCOINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_SCOINI)
            select CSCODICE,CSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCOINI = NVL(_Link_.CSCODICE,space(5))
      this.w_DESSCO = NVL(_Link_.CSDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SCOINI = space(5)
      endif
      this.w_DESSCO = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_SCMA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCOINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SCOFIN
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
    i_lTable = "CAT_SCMA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2], .t., this.CAT_SCMA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCOFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASM',True,'CAT_SCMA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_SCOFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_SCOFIN))
          select CSCODICE,CSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCOFIN)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCOFIN) and !this.bDontReportError
            deferred_cp_zoom('CAT_SCMA','*','CSCODICE',cp_AbsName(oSource.parent,'oSCOFIN_2_12'),i_cWhere,'GSAR_ASM',"Cat.sconti/maggiorazioni",'GSAR_SCO.CAT_SCMA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCOFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_SCOFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_SCOFIN)
            select CSCODICE,CSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCOFIN = NVL(_Link_.CSCODICE,space(5))
      this.w_DESSCO1 = NVL(_Link_.CSDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SCOFIN = space(5)
      endif
      this.w_DESSCO1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_SCMA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCOFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MARINI
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MARINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MARINI)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MARINI))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MARINI)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MARINI) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMARINI_2_17'),i_cWhere,'GSAR_AMH',"Codici marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MARINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MARINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MARINI)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MARINI = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAR = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MARINI = space(5)
      endif
      this.w_DESMAR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MARINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MARFIN
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MARFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MARFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MARFIN))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MARFIN)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MARFIN) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMARFIN_2_19'),i_cWhere,'GSAR_AMH',"Codici marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MARFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MARFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MARFIN)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MARFIN = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAR1 = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MARFIN = space(5)
      endif
      this.w_DESMAR1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MARFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAPINI
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_lTable = "GRUPRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2], .t., this.GRUPRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAPINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGP',True,'GRUPRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_CAPINI)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_CAPINI))
          select GPCODICE,GPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAPINI)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAPINI) and !this.bDontReportError
            deferred_cp_zoom('GRUPRO','*','GPCODICE',cp_AbsName(oSource.parent,'oCAPINI_2_25'),i_cWhere,'GSAR_AGP',"Cat.provvigioni",'GSAR_PRO.GRUPRO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAPINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_CAPINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_CAPINI)
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAPINI = NVL(_Link_.GPCODICE,space(5))
      this.w_DESCAP = NVL(_Link_.GPDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CAPINI = space(5)
      endif
      this.w_DESCAP = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAPINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAPFIN
  func Link_2_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_lTable = "GRUPRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2], .t., this.GRUPRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAPFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGP',True,'GRUPRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_CAPFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_CAPFIN))
          select GPCODICE,GPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAPFIN)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAPFIN) and !this.bDontReportError
            deferred_cp_zoom('GRUPRO','*','GPCODICE',cp_AbsName(oSource.parent,'oCAPFIN_2_27'),i_cWhere,'GSAR_AGP',"Cat.provvigioni",'GSAR_PRO.GRUPRO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAPFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_CAPFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_CAPFIN)
            select GPCODICE,GPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAPFIN = NVL(_Link_.GPCODICE,space(5))
      this.w_DESCAP1 = NVL(_Link_.GPDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CAPFIN = space(5)
      endif
      this.w_DESCAP1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAPFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VOCINI
  func Link_3_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VOCINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_VOCINI)+"%");
                   +" and VCTIPVOC="+cp_ToStrODBC(this.w_TIPVOC);

          i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCTIPVOC,VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCTIPVOC',this.w_TIPVOC;
                     ,'VCCODICE',trim(this.w_VOCINI))
          select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCTIPVOC,VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VOCINI)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VOCINI) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCTIPVOC,VCCODICE',cp_AbsName(oSource.parent,'oVOCINI_3_3'),i_cWhere,'GSCA_AVC',"Voci di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPVOC<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Voce di costo incongruente o obsoleta")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and VCTIPVOC="+cp_ToStrODBC(this.w_TIPVOC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCTIPVOC',oSource.xKey(1);
                       ,'VCCODICE',oSource.xKey(2))
            select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VOCINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_VOCINI);
                   +" and VCTIPVOC="+cp_ToStrODBC(this.w_TIPVOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCTIPVOC',this.w_TIPVOC;
                       ,'VCCODICE',this.w_VOCINI)
            select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VOCINI = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
      this.w_TIPVOC = NVL(_Link_.VCTIPVOC,space(1))
      this.w_CODCOS = NVL(_Link_.VCTIPCOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_VOCINI = space(15)
      endif
      this.w_DESVOC = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPVOC = space(1)
      this.w_CODCOS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Voce di costo obsoleta!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Voce di costo incongruente o obsoleta")
        endif
        this.w_VOCINI = space(15)
        this.w_DESVOC = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPVOC = space(1)
        this.w_CODCOS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCTIPVOC,1)+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VOCINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VOCFIN
  func Link_3_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VOCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_VOCFIN)+"%");
                   +" and VCTIPVOC="+cp_ToStrODBC(this.w_TIPVOC);

          i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCTIPVOC,VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCTIPVOC',this.w_TIPVOC;
                     ,'VCCODICE',trim(this.w_VOCFIN))
          select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCTIPVOC,VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VOCFIN)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VOCFIN) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCTIPVOC,VCCODICE',cp_AbsName(oSource.parent,'oVOCFIN_3_5'),i_cWhere,'GSCA_AVC',"Voci di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPVOC<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Voce di costo incongruente o obsoleta")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and VCTIPVOC="+cp_ToStrODBC(this.w_TIPVOC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCTIPVOC',oSource.xKey(1);
                       ,'VCCODICE',oSource.xKey(2))
            select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VOCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_VOCFIN);
                   +" and VCTIPVOC="+cp_ToStrODBC(this.w_TIPVOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCTIPVOC',this.w_TIPVOC;
                       ,'VCCODICE',this.w_VOCFIN)
            select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VOCFIN = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC1 = NVL(_Link_.VCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
      this.w_TIPVOC = NVL(_Link_.VCTIPVOC,space(1))
      this.w_CODCOS = NVL(_Link_.VCTIPCOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_VOCFIN = space(15)
      endif
      this.w_DESVOC1 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPVOC = space(1)
      this.w_CODCOS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Voce di costo obsoleta!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Voce di costo incongruente o obsoleta")
        endif
        this.w_VOCFIN = space(15)
        this.w_DESVOC1 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPVOC = space(1)
        this.w_CODCOS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCTIPVOC,1)+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VOCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VORINI
  func Link_3_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VORINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_VORINI)+"%");
                   +" and VCTIPVOC="+cp_ToStrODBC(this.w_TIPVOR);

          i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCTIPVOC,VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCTIPVOC',this.w_TIPVOR;
                     ,'VCCODICE',trim(this.w_VORINI))
          select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCTIPVOC,VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VORINI)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VORINI) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCTIPVOC,VCCODICE',cp_AbsName(oSource.parent,'oVORINI_3_11'),i_cWhere,'GSCA_AVC',"Voci di ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPVOR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Voce di ricavo incongruente o obsoleta")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and VCTIPVOC="+cp_ToStrODBC(this.w_TIPVOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCTIPVOC',oSource.xKey(1);
                       ,'VCCODICE',oSource.xKey(2))
            select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VORINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_VORINI);
                   +" and VCTIPVOC="+cp_ToStrODBC(this.w_TIPVOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCTIPVOC',this.w_TIPVOR;
                       ,'VCCODICE',this.w_VORINI)
            select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VORINI = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOV = NVL(_Link_.VCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
      this.w_TIPVOR = NVL(_Link_.VCTIPVOC,space(1))
      this.w_CODCOS = NVL(_Link_.VCTIPCOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_VORINI = space(15)
      endif
      this.w_DESVOV = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPVOR = space(1)
      this.w_CODCOS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Voce di ricavo obsoleta!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Voce di ricavo incongruente o obsoleta")
        endif
        this.w_VORINI = space(15)
        this.w_DESVOV = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPVOR = space(1)
        this.w_CODCOS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCTIPVOC,1)+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VORINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VORFIN
  func Link_3_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VORFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_VORFIN)+"%");
                   +" and VCTIPVOC="+cp_ToStrODBC(this.w_TIPVOR);

          i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCTIPVOC,VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCTIPVOC',this.w_TIPVOR;
                     ,'VCCODICE',trim(this.w_VORFIN))
          select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCTIPVOC,VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VORFIN)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VORFIN) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCTIPVOC,VCCODICE',cp_AbsName(oSource.parent,'oVORFIN_3_13'),i_cWhere,'GSCA_AVC',"Voci di ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPVOR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Voce di costo incongruente o obsoleta")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and VCTIPVOC="+cp_ToStrODBC(this.w_TIPVOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCTIPVOC',oSource.xKey(1);
                       ,'VCCODICE',oSource.xKey(2))
            select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VORFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_VORFIN);
                   +" and VCTIPVOC="+cp_ToStrODBC(this.w_TIPVOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCTIPVOC',this.w_TIPVOR;
                       ,'VCCODICE',this.w_VORFIN)
            select VCTIPVOC,VCCODICE,VCDESCRI,VCDTOBSO,VCTIPCOS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VORFIN = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOV1 = NVL(_Link_.VCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VCDTOBSO),ctod("  /  /  "))
      this.w_TIPVOR = NVL(_Link_.VCTIPVOC,space(1))
      this.w_CODCOS = NVL(_Link_.VCTIPCOS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_VORFIN = space(15)
      endif
      this.w_DESVOV1 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPVOR = space(1)
      this.w_CODCOS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Voce di ricavo obsoleta!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Voce di costo incongruente o obsoleta")
        endif
        this.w_VORFIN = space(15)
        this.w_DESVOV1 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPVOR = space(1)
        this.w_CODCOS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCTIPVOC,1)+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VORFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORINI
  func Link_3_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_FORINI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORINI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FORINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORINI_3_19'),i_cWhere,'GSAR_BZC',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_FORINI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORINI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FORINI = space(15)
      endif
      this.w_DESFOR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORFIN
  func Link_3_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_FORFIN))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FORFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORFIN_3_21'),i_cWhere,'GSAR_BZC',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_FORFIN)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR1 = NVL(_Link_.ANDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FORFIN = space(15)
      endif
      this.w_DESFOR1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTINI
  func Link_4_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARTINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ARTINI))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARTINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oARTINI_4_8'),i_cWhere,'',"Articoli/servizi",'ARTZOOM2.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTINI)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTINI = space(20)
      endif
      this.w_DESART = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Articolo obsoleto!",.F.) AND ((empty(.w_ARTFIN)) OR  (.w_ARTINI<=.w_ARTFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) OR .w_ARTOBS$'OT'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale superiore al codice finale oppure obsoleto")
        endif
        this.w_ARTINI = space(20)
        this.w_DESART = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTFIN
  func Link_4_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARTFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ARTFIN))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARTFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oARTFIN_4_10'),i_cWhere,'',"Articoli/servizi",'ARTZOOM2.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTFIN)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESART1 = NVL(_Link_.ARDESART,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTFIN = space(20)
      endif
      this.w_DESART1 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Articolo obsoleto!",.F.) AND ((.w_ARTFIN>=.w_ARTINI) or (empty(.w_ARTINI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) OR .w_ARTOBS$'OT'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale superiore al codice finale oppure obsoleto")
        endif
        this.w_ARTFIN = space(20)
        this.w_DESART1 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=IVAFIL
  func Link_4_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IVAFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_IVAFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_IVAFIL))
          select IVCODIVA,IVDESIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_IVAFIL)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_IVAFIL) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oIVAFIL_4_12'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IVAFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_IVAFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_IVAFIL)
            select IVCODIVA,IVDESIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IVAFIL = NVL(_Link_.IVCODIVA,space(5))
      this.w_IVADES = NVL(_Link_.IVDESIVA,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_IVAFIL = space(5)
      endif
      this.w_IVADES = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Iva obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA obsoleto")
        endif
        this.w_IVAFIL = space(5)
        this.w_IVADES = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IVAFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRMFIL
  func Link_4_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRMFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRMFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRMFIL))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRMFIL)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRMFIL) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRMFIL_4_14'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRMFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRMFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRMFIL)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRMFIL = NVL(_Link_.GMCODICE,space(5))
      this.w_GRMDES = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRMFIL = space(5)
      endif
      this.w_GRMDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRMFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATFIL
  func Link_4_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOARTI_IDX,3]
    i_lTable = "CACOARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2], .t., this.CACOARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC1',True,'CACOARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C1CODICE like "+cp_ToStrODBC(trim(this.w_CATFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C1CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C1CODICE',trim(this.w_CATFIL))
          select C1CODICE,C1DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C1CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATFIL)==trim(_Link_.C1CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATFIL) and !this.bDontReportError
            deferred_cp_zoom('CACOARTI','*','C1CODICE',cp_AbsName(oSource.parent,'oCATFIL_4_18'),i_cWhere,'GSAR_AC1',"Categorie contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',oSource.xKey(1))
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C1CODICE,C1DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C1CODICE="+cp_ToStrODBC(this.w_CATFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C1CODICE',this.w_CATFIL)
            select C1CODICE,C1DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATFIL = NVL(_Link_.C1CODICE,space(5))
      this.w_CATDES = NVL(_Link_.C1DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATFIL = space(5)
      endif
      this.w_CATDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOARTI_IDX,2])+'\'+cp_ToStr(_Link_.C1CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAMFIL
  func Link_4_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAMFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAMFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAMFIL))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAMFIL)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FAMFIL) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAMFIL_4_20'),i_cWhere,'GSAR_AFA',"Codici famiglie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAMFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAMFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAMFIL)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAMFIL = NVL(_Link_.FACODICE,space(5))
      this.w_FAMDES = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FAMFIL = space(5)
      endif
      this.w_FAMDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAMFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATOFIL
  func Link_4_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATOFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATOFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATOFIL))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATOFIL)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATOFIL) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATOFIL_4_24'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATOFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATOFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATOFIL)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATOFIL = NVL(_Link_.OMCODICE,space(5))
      this.w_CATODES = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATOFIL = space(5)
      endif
      this.w_CATODES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATOFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PROFIL
  func Link_4_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PROFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PROFIL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PROFIL))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PROFIL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PROFIL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPROFIL_4_26'),i_cWhere,'GSAR_BZC',"Produttori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PROFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PROFIL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PROFIL)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PROFIL = NVL(_Link_.ANCODICE,space(15))
      this.w_PRODES = NVL(_Link_.ANDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PROFIL = space(15)
      endif
      this.w_PRODES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PROFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORFIL
  func Link_4_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FORFIL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_FORFIL))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORFIL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FORFIL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFORFIL_4_30'),i_cWhere,'GSAR_BZC',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FORFIL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_FORFIL)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORFIL = NVL(_Link_.ANCODICE,space(15))
      this.w_FORDES = NVL(_Link_.ANDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_FORFIL = space(15)
      endif
      this.w_FORDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MARFIL
  func Link_4_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MARFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MARFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MARFIL))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MARFIL)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MARFIL) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMARFIL_4_32'),i_cWhere,'GSAR_AMH',"Codici marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MARFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MARFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MARFIL)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MARFIL = NVL(_Link_.MACODICE,space(5))
      this.w_MARDES = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MARFIL = space(5)
      endif
      this.w_MARDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MARFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COLFIL
  func Link_4_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_lTable = "TIP_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2], .t., this.TIP_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COLFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'TIP_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_COLFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_COLFIL))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COLFIL)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COLFIL) and !this.bDontReportError
            deferred_cp_zoom('TIP_COLL','*','TCCODICE',cp_AbsName(oSource.parent,'oCOLFIL_4_44'),i_cWhere,'GSAR_MTO',"Codici tipologie colli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COLFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_COLFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_COLFIL)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COLFIL = NVL(_Link_.TCCODICE,space(5))
      this.w_COLDES = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_COLFIL = space(5)
      endif
      this.w_COLDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COLFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONFIL
  func Link_4_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPICONF_IDX,3]
    i_lTable = "TIPICONF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2], .t., this.TIPICONF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATC',True,'TIPICONF')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CONFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CONFIL))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONFIL)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONFIL) and !this.bDontReportError
            deferred_cp_zoom('TIPICONF','*','TCCODICE',cp_AbsName(oSource.parent,'oCONFIL_4_45'),i_cWhere,'GSAR_ATC',"Codici tipi confezioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CONFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CONFIL)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONFIL = NVL(_Link_.TCCODICE,space(3))
      this.w_CONDES = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CONFIL = space(3)
      endif
      this.w_CONDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPICONF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COLINI
  func Link_2_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_lTable = "TIP_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2], .t., this.TIP_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COLINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'TIP_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_COLINI)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_COLINI))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COLINI)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COLINI) and !this.bDontReportError
            deferred_cp_zoom('TIP_COLL','*','TCCODICE',cp_AbsName(oSource.parent,'oCOLINI_2_35'),i_cWhere,'GSAR_MTO',"Tipologie colli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COLINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_COLINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_COLINI)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COLINI = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCOL = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_COLINI = space(5)
      endif
      this.w_DESCOL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COLINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COLFIN
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_lTable = "TIP_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2], .t., this.TIP_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COLFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'TIP_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_COLFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_COLFIN))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COLFIN)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COLFIN) and !this.bDontReportError
            deferred_cp_zoom('TIP_COLL','*','TCCODICE',cp_AbsName(oSource.parent,'oCOLFIN_2_37'),i_cWhere,'GSAR_MTO',"Tipologie colli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COLFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_COLFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_COLFIN)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COLFIN = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCOL1 = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_COLFIN = space(5)
      endif
      this.w_DESCOL1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COLFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONDEF
  func Link_2_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPICONF_IDX,3]
    i_lTable = "TIPICONF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2], .t., this.TIPICONF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONDEF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATC',True,'TIPICONF')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CONDEF)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CONDEF))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONDEF)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONDEF) and !this.bDontReportError
            deferred_cp_zoom('TIPICONF','*','TCCODICE',cp_AbsName(oSource.parent,'oCONDEF_2_42'),i_cWhere,'GSAR_ATC',"Codici tipi confezioni",'GSMA_ACF.TIPICONF_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONDEF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CONDEF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CONDEF)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONDEF = NVL(_Link_.TCCODICE,space(3))
      this.w_DESCOF = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CONDEF = space(3)
      endif
      this.w_DESCOF = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPICONF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONDEF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UMPRI
  func Link_2_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UMPRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_UMPRI)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_UMPRI))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UMPRI)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_UMPRI) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oUMPRI_2_44'),i_cWhere,'',"Unit� di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UMPRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UMPRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UMPRI)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UMPRI = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UMPRI = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UMPRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LISTRIF
  func Link_2_57(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LISTRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_LISTRIF)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_LISTRIF))
          select LSCODLIS,LSVALLIS,LSDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LISTRIF)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LISTRIF) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oLISTRIF_2_57'),i_cWhere,'GSAR_ALI',"Anagrafica listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSVALLIS,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LISTRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_LISTRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_LISTRIF)
            select LSCODLIS,LSVALLIS,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LISTRIF = NVL(_Link_.LSCODLIS,space(5))
      this.w_VALUTA = NVL(_Link_.LSVALLIS,space(3))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_LISTRIF = space(5)
      endif
      this.w_VALUTA = space(3)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Listino di Riferimento Obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Listino di riferimento ineistente o obsoleto")
        endif
        this.w_LISTRIF = space(5)
        this.w_VALUTA = space(3)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LISTRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALRIF
  func Link_2_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALRIF)
            select VACODVAL,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALRIF = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOLIS = NVL(_Link_.VACAOVAL,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALRIF = space(3)
      endif
      this.w_CAOLIS = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALNAZ
  func Link_2_64(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALNAZ)
            select VACODVAL,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALNAZ = space(3)
      endif
      this.w_CAOVAL = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESERCI
  func Link_2_66(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERCI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESERCI)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_ESERCI))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESERCI)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESERCI) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESERCI_2_66'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERCI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERCI);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_ESERCI)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERCI = NVL(_Link_.ESCODESE,space(4))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESERCI = space(4)
      endif
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERCI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMINV
  func Link_2_67(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMINV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsma_ain',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_NUMINV)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESERCI);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_ESERCI;
                     ,'INNUMINV',trim(this.w_NUMINV))
          select INCODESE,INNUMINV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMINV)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMINV) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oNUMINV_2_67'),i_cWhere,'gsma_ain',"Inventari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ESERCI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_ESERCI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMINV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_NUMINV);
                   +" and INCODESE="+cp_ToStrODBC(this.w_ESERCI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_ESERCI;
                       ,'INNUMINV',this.w_NUMINV)
            select INCODESE,INNUMINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMINV = NVL(_Link_.INNUMINV,space(6))
    else
      if i_cCtrl<>'Load'
        this.w_NUMINV = space(6)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMINV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=REPINI
  func Link_3_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
    i_lTable = "REP_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2], .t., this.REP_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_REPINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ARE',True,'REP_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODREP like "+cp_ToStrODBC(trim(this.w_REPINI)+"%");

          i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RECODREP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RECODREP',trim(this.w_REPINI))
          select RECODREP,REDESREP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RECODREP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_REPINI)==trim(_Link_.RECODREP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_REPINI) and !this.bDontReportError
            deferred_cp_zoom('REP_ARTI','*','RECODREP',cp_AbsName(oSource.parent,'oREPINI_3_42'),i_cWhere,'GSPS_ARE',"Reparti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP";
                     +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',oSource.xKey(1))
            select RECODREP,REDESREP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_REPINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP";
                   +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(this.w_REPINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',this.w_REPINI)
            select RECODREP,REDESREP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_REPINI = NVL(_Link_.RECODREP,space(3))
      this.w_DESREP = NVL(_Link_.REDESREP,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_REPINI = space(3)
      endif
      this.w_DESREP = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.RECODREP,1)
      cp_ShowWarn(i_cKey,this.REP_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_REPINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=REPFIN
  func Link_3_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
    i_lTable = "REP_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2], .t., this.REP_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_REPFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ARE',True,'REP_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODREP like "+cp_ToStrODBC(trim(this.w_REPFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RECODREP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RECODREP',trim(this.w_REPFIN))
          select RECODREP,REDESREP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RECODREP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_REPFIN)==trim(_Link_.RECODREP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_REPFIN) and !this.bDontReportError
            deferred_cp_zoom('REP_ARTI','*','RECODREP',cp_AbsName(oSource.parent,'oREPFIN_3_44'),i_cWhere,'GSPS_ARE',"Reparti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP";
                     +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',oSource.xKey(1))
            select RECODREP,REDESREP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_REPFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP";
                   +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(this.w_REPFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',this.w_REPFIN)
            select RECODREP,REDESREP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_REPFIN = NVL(_Link_.RECODREP,space(3))
      this.w_DESREP1 = NVL(_Link_.REDESREP,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_REPFIN = space(3)
      endif
      this.w_DESREP1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.RECODREP,1)
      cp_ShowWarn(i_cKey,this.REP_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_REPFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=REPFIL
  func Link_4_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REP_ARTI_IDX,3]
    i_lTable = "REP_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2], .t., this.REP_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_REPFIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPS_ARE',True,'REP_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RECODREP like "+cp_ToStrODBC(trim(this.w_REPFIL)+"%");

          i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RECODREP","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RECODREP',trim(this.w_REPFIL))
          select RECODREP,REDESREP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RECODREP into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_REPFIL)==trim(_Link_.RECODREP) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_REPFIL) and !this.bDontReportError
            deferred_cp_zoom('REP_ARTI','*','RECODREP',cp_AbsName(oSource.parent,'oREPFIL_4_48'),i_cWhere,'GSPS_ARE',"Reparti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP";
                     +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',oSource.xKey(1))
            select RECODREP,REDESREP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_REPFIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RECODREP,REDESREP";
                   +" from "+i_cTable+" "+i_lTable+" where RECODREP="+cp_ToStrODBC(this.w_REPFIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RECODREP',this.w_REPFIL)
            select RECODREP,REDESREP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_REPFIL = NVL(_Link_.RECODREP,space(3))
      this.w_REPDES = NVL(_Link_.REDESREP,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_REPFIL = space(3)
      endif
      this.w_REPDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REP_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.RECODREP,1)
      cp_ShowWarn(i_cKey,this.REP_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_REPFIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=cMCTIPCAT
  func Link_2_83(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCONTR_IDX,3]
    i_lTable = "TIPCONTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2], .t., this.TIPCONTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_cMCTIPCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATP',True,'TIPCONTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TPCODICE like "+cp_ToStrODBC(trim(this.w_cMCTIPCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TPCODICE',trim(this.w_cMCTIPCAT))
          select TPCODICE,TPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_cMCTIPCAT)==trim(_Link_.TPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_cMCTIPCAT) and !this.bDontReportError
            deferred_cp_zoom('TIPCONTR','*','TPCODICE',cp_AbsName(oSource.parent,'ocMCTIPCAT_2_83'),i_cWhere,'GSAR_ATP',"Tipi contributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',oSource.xKey(1))
            select TPCODICE,TPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_cMCTIPCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TPCODICE,TPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TPCODICE="+cp_ToStrODBC(this.w_cMCTIPCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TPCODICE',this.w_cMCTIPCAT)
            select TPCODICE,TPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_cMCTIPCAT = NVL(_Link_.TPCODICE,space(15))
      this.w_TPDESCRI = NVL(_Link_.TPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_cMCTIPCAT = space(15)
      endif
      this.w_TPDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCONTR_IDX,2])+'\'+cp_ToStr(_Link_.TPCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCONTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_cMCTIPCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=cMCCODCAT
  func Link_2_84(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATMCONT_IDX,3]
    i_lTable = "CATMCONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2], .t., this.CATMCONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_cMCCODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MCT',True,'CATMCONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CGCODICE like "+cp_ToStrODBC(trim(this.w_cMCCODCAT)+"%");
                   +" and CGCONTRI="+cp_ToStrODBC(this.w_cMCTIPCAT);

          i_ret=cp_SQL(i_nConn,"select CGCONTRI,CGCODICE,CGDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CGCONTRI,CGCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CGCONTRI',this.w_cMCTIPCAT;
                     ,'CGCODICE',trim(this.w_cMCCODCAT))
          select CGCONTRI,CGCODICE,CGDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CGCONTRI,CGCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_cMCCODCAT)==trim(_Link_.CGCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_cMCCODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATMCONT','*','CGCONTRI,CGCODICE',cp_AbsName(oSource.parent,'ocMCCODCAT_2_84'),i_cWhere,'GSAR_MCT',"Categorie contributo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_cMCTIPCAT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGCONTRI,CGCODICE,CGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CGCONTRI,CGCODICE,CGDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGCONTRI,CGCODICE,CGDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CGCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CGCONTRI="+cp_ToStrODBC(this.w_cMCTIPCAT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CGCONTRI',oSource.xKey(1);
                       ,'CGCODICE',oSource.xKey(2))
            select CGCONTRI,CGCODICE,CGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_cMCCODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CGCONTRI,CGCODICE,CGDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CGCODICE="+cp_ToStrODBC(this.w_cMCCODCAT);
                   +" and CGCONTRI="+cp_ToStrODBC(this.w_cMCTIPCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CGCONTRI',this.w_cMCTIPCAT;
                       ,'CGCODICE',this.w_cMCCODCAT)
            select CGCONTRI,CGCODICE,CGDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_cMCCODCAT = NVL(_Link_.CGCODICE,space(5))
      this.w_CGDESCRI = NVL(_Link_.CGDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_cMCCODCAT = space(5)
      endif
      this.w_CGDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATMCONT_IDX,2])+'\'+cp_ToStr(_Link_.CGCONTRI,1)+'\'+cp_ToStr(_Link_.CGCODICE,1)
      cp_ShowWarn(i_cKey,this.CATMCONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_cMCCODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIVASEL_1_1.RadioValue()==this.w_IVASEL)
      this.oPgFrm.Page1.oPag.oIVASEL_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVAALL_1_2.RadioValue()==this.w_IVAALL)
      this.oPgFrm.Page1.oPag.oIVAALL_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIVAINI_1_3.value==this.w_IVAINI)
      this.oPgFrm.Page1.oPag.oIVAINI_1_3.value=this.w_IVAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_4.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_4.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oIVAFIN_1_5.value==this.w_IVAFIN)
      this.oPgFrm.Page1.oPag.oIVAFIN_1_5.value=this.w_IVAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA1_1_6.value==this.w_DESIVA1)
      this.oPgFrm.Page1.oPag.oDESIVA1_1_6.value=this.w_DESIVA1
    endif
    if not(this.oPgFrm.Page1.oPag.oCIMSEL_1_7.RadioValue()==this.w_CIMSEL)
      this.oPgFrm.Page1.oPag.oCIMSEL_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIMALL_1_8.RadioValue()==this.w_CIMALL)
      this.oPgFrm.Page1.oPag.oCIMALL_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCIMINI_1_9.value==this.w_CIMINI)
      this.oPgFrm.Page1.oPag.oCIMINI_1_9.value=this.w_CIMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCIMFIN_1_10.value==this.w_CIMFIN)
      this.oPgFrm.Page1.oPag.oCIMFIN_1_10.value=this.w_CIMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGRMSEL_1_13.RadioValue()==this.w_GRMSEL)
      this.oPgFrm.Page1.oPag.oGRMSEL_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGRMALL_1_14.RadioValue()==this.w_GRMALL)
      this.oPgFrm.Page1.oPag.oGRMALL_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGRMINI_1_15.value==this.w_GRMINI)
      this.oPgFrm.Page1.oPag.oGRMINI_1_15.value=this.w_GRMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_16.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_16.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oGRMFIN_1_17.value==this.w_GRMFIN)
      this.oPgFrm.Page1.oPag.oGRMFIN_1_17.value=this.w_GRMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU1_1_18.value==this.w_DESGRU1)
      this.oPgFrm.Page1.oPag.oDESGRU1_1_18.value=this.w_DESGRU1
    endif
    if not(this.oPgFrm.Page1.oPag.oCATSEL_1_21.RadioValue()==this.w_CATSEL)
      this.oPgFrm.Page1.oPag.oCATSEL_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATALL_1_22.RadioValue()==this.w_CATALL)
      this.oPgFrm.Page1.oPag.oCATALL_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATINI_1_23.value==this.w_CATINI)
      this.oPgFrm.Page1.oPag.oCATINI_1_23.value=this.w_CATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_24.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_24.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCATFIN_1_25.value==this.w_CATFIN)
      this.oPgFrm.Page1.oPag.oCATFIN_1_25.value=this.w_CATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT1_1_26.value==this.w_DESCAT1)
      this.oPgFrm.Page1.oPag.oDESCAT1_1_26.value=this.w_DESCAT1
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMSEL_1_29.RadioValue()==this.w_FAMSEL)
      this.oPgFrm.Page1.oPag.oFAMSEL_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMALL_1_30.RadioValue()==this.w_FAMALL)
      this.oPgFrm.Page1.oPag.oFAMALL_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMINI_1_31.value==this.w_FAMINI)
      this.oPgFrm.Page1.oPag.oFAMINI_1_31.value=this.w_FAMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAM_1_32.value==this.w_DESFAM)
      this.oPgFrm.Page1.oPag.oDESFAM_1_32.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oFAMFIN_1_33.value==this.w_FAMFIN)
      this.oPgFrm.Page1.oPag.oFAMFIN_1_33.value=this.w_FAMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAM1_1_34.value==this.w_DESFAM1)
      this.oPgFrm.Page1.oPag.oDESFAM1_1_34.value=this.w_DESFAM1
    endif
    if not(this.oPgFrm.Page1.oPag.oCATOSEL_1_37.RadioValue()==this.w_CATOSEL)
      this.oPgFrm.Page1.oPag.oCATOSEL_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATOALL_1_38.RadioValue()==this.w_CATOALL)
      this.oPgFrm.Page1.oPag.oCATOALL_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATOINI_1_39.value==this.w_CATOINI)
      this.oPgFrm.Page1.oPag.oCATOINI_1_39.value=this.w_CATOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATO_1_40.value==this.w_DESCATO)
      this.oPgFrm.Page1.oPag.oDESCATO_1_40.value=this.w_DESCATO
    endif
    if not(this.oPgFrm.Page1.oPag.oCATOFIN_1_41.value==this.w_CATOFIN)
      this.oPgFrm.Page1.oPag.oCATOFIN_1_41.value=this.w_CATOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCATO1_1_42.value==this.w_DESCATO1)
      this.oPgFrm.Page1.oPag.oDESCATO1_1_42.value=this.w_DESCATO1
    endif
    if not(this.oPgFrm.Page2.oPag.oCLASEL_2_1.RadioValue()==this.w_CLASEL)
      this.oPgFrm.Page2.oPag.oCLASEL_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCLAALL_2_2.RadioValue()==this.w_CLAALL)
      this.oPgFrm.Page2.oPag.oCLAALL_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCLAINI_2_3.value==this.w_CLAINI)
      this.oPgFrm.Page2.oPag.oCLAINI_2_3.value=this.w_CLAINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLA_2_4.value==this.w_DESCLA)
      this.oPgFrm.Page2.oPag.oDESCLA_2_4.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page2.oPag.oCLAFIN_2_5.value==this.w_CLAFIN)
      this.oPgFrm.Page2.oPag.oCLAFIN_2_5.value=this.w_CLAFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLA1_2_6.value==this.w_DESCLA1)
      this.oPgFrm.Page2.oPag.oDESCLA1_2_6.value=this.w_DESCLA1
    endif
    if not(this.oPgFrm.Page2.oPag.oSCOSEL_2_8.RadioValue()==this.w_SCOSEL)
      this.oPgFrm.Page2.oPag.oSCOSEL_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSCOALL_2_9.RadioValue()==this.w_SCOALL)
      this.oPgFrm.Page2.oPag.oSCOALL_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSCOINI_2_10.value==this.w_SCOINI)
      this.oPgFrm.Page2.oPag.oSCOINI_2_10.value=this.w_SCOINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSCO_2_11.value==this.w_DESSCO)
      this.oPgFrm.Page2.oPag.oDESSCO_2_11.value=this.w_DESSCO
    endif
    if not(this.oPgFrm.Page2.oPag.oSCOFIN_2_12.value==this.w_SCOFIN)
      this.oPgFrm.Page2.oPag.oSCOFIN_2_12.value=this.w_SCOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSCO1_2_13.value==this.w_DESSCO1)
      this.oPgFrm.Page2.oPag.oDESSCO1_2_13.value=this.w_DESSCO1
    endif
    if not(this.oPgFrm.Page2.oPag.oMARSEL_2_15.RadioValue()==this.w_MARSEL)
      this.oPgFrm.Page2.oPag.oMARSEL_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMARALL_2_16.RadioValue()==this.w_MARALL)
      this.oPgFrm.Page2.oPag.oMARALL_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMARINI_2_17.value==this.w_MARINI)
      this.oPgFrm.Page2.oPag.oMARINI_2_17.value=this.w_MARINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAR_2_18.value==this.w_DESMAR)
      this.oPgFrm.Page2.oPag.oDESMAR_2_18.value=this.w_DESMAR
    endif
    if not(this.oPgFrm.Page2.oPag.oMARFIN_2_19.value==this.w_MARFIN)
      this.oPgFrm.Page2.oPag.oMARFIN_2_19.value=this.w_MARFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAR1_2_20.value==this.w_DESMAR1)
      this.oPgFrm.Page2.oPag.oDESMAR1_2_20.value=this.w_DESMAR1
    endif
    if not(this.oPgFrm.Page2.oPag.oCAPSEL_2_23.RadioValue()==this.w_CAPSEL)
      this.oPgFrm.Page2.oPag.oCAPSEL_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCAPALL_2_24.RadioValue()==this.w_CAPALL)
      this.oPgFrm.Page2.oPag.oCAPALL_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCAPINI_2_25.value==this.w_CAPINI)
      this.oPgFrm.Page2.oPag.oCAPINI_2_25.value=this.w_CAPINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAP_2_26.value==this.w_DESCAP)
      this.oPgFrm.Page2.oPag.oDESCAP_2_26.value=this.w_DESCAP
    endif
    if not(this.oPgFrm.Page2.oPag.oCAPFIN_2_27.value==this.w_CAPFIN)
      this.oPgFrm.Page2.oPag.oCAPFIN_2_27.value=this.w_CAPFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAP1_2_28.value==this.w_DESCAP1)
      this.oPgFrm.Page2.oPag.oDESCAP1_2_28.value=this.w_DESCAP1
    endif
    if not(this.oPgFrm.Page3.oPag.oVOCSEL_3_1.RadioValue()==this.w_VOCSEL)
      this.oPgFrm.Page3.oPag.oVOCSEL_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVOCALL_3_2.RadioValue()==this.w_VOCALL)
      this.oPgFrm.Page3.oPag.oVOCALL_3_2.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVOCINI_3_3.value==this.w_VOCINI)
      this.oPgFrm.Page3.oPag.oVOCINI_3_3.value=this.w_VOCINI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESVOC_3_4.value==this.w_DESVOC)
      this.oPgFrm.Page3.oPag.oDESVOC_3_4.value=this.w_DESVOC
    endif
    if not(this.oPgFrm.Page3.oPag.oVOCFIN_3_5.value==this.w_VOCFIN)
      this.oPgFrm.Page3.oPag.oVOCFIN_3_5.value=this.w_VOCFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oDESVOC1_3_6.value==this.w_DESVOC1)
      this.oPgFrm.Page3.oPag.oDESVOC1_3_6.value=this.w_DESVOC1
    endif
    if not(this.oPgFrm.Page3.oPag.oVORSEL_3_9.RadioValue()==this.w_VORSEL)
      this.oPgFrm.Page3.oPag.oVORSEL_3_9.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVORALL_3_10.RadioValue()==this.w_VORALL)
      this.oPgFrm.Page3.oPag.oVORALL_3_10.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVORINI_3_11.value==this.w_VORINI)
      this.oPgFrm.Page3.oPag.oVORINI_3_11.value=this.w_VORINI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESVOV_3_12.value==this.w_DESVOV)
      this.oPgFrm.Page3.oPag.oDESVOV_3_12.value=this.w_DESVOV
    endif
    if not(this.oPgFrm.Page3.oPag.oVORFIN_3_13.value==this.w_VORFIN)
      this.oPgFrm.Page3.oPag.oVORFIN_3_13.value=this.w_VORFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oDESVOV1_3_14.value==this.w_DESVOV1)
      this.oPgFrm.Page3.oPag.oDESVOV1_3_14.value=this.w_DESVOV1
    endif
    if not(this.oPgFrm.Page3.oPag.oFORSEL_3_17.RadioValue()==this.w_FORSEL)
      this.oPgFrm.Page3.oPag.oFORSEL_3_17.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFORALL_3_18.RadioValue()==this.w_FORALL)
      this.oPgFrm.Page3.oPag.oFORALL_3_18.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oFORINI_3_19.value==this.w_FORINI)
      this.oPgFrm.Page3.oPag.oFORINI_3_19.value=this.w_FORINI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESFOR_3_20.value==this.w_DESFOR)
      this.oPgFrm.Page3.oPag.oDESFOR_3_20.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oFORFIN_3_21.value==this.w_FORFIN)
      this.oPgFrm.Page3.oPag.oFORFIN_3_21.value=this.w_FORFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oDESFOR1_3_22.value==this.w_DESFOR1)
      this.oPgFrm.Page3.oPag.oDESFOR1_3_22.value=this.w_DESFOR1
    endif
    if not(this.oPgFrm.Page3.oPag.oGESSEL_3_25.RadioValue()==this.w_GESSEL)
      this.oPgFrm.Page3.oPag.oGESSEL_3_25.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGESINI_3_26.RadioValue()==this.w_GESINI)
      this.oPgFrm.Page3.oPag.oGESINI_3_26.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oGESFIN_3_29.RadioValue()==this.w_GESFIN)
      this.oPgFrm.Page3.oPag.oGESFIN_3_29.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPROVSEL_3_30.RadioValue()==this.w_PROVSEL)
      this.oPgFrm.Page3.oPag.oPROVSEL_3_30.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPROVINI_3_31.RadioValue()==this.w_PROVINI)
      this.oPgFrm.Page3.oPag.oPROVINI_3_31.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPROVFIN_3_33.RadioValue()==this.w_PROVFIN)
      this.oPgFrm.Page3.oPag.oPROVFIN_3_33.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTIPSEL_4_1.RadioValue()==this.w_TIPSEL)
      this.oPgFrm.Page4.oPag.oTIPSEL_4_1.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTIPARTI_4_2.RadioValue()==this.w_TIPARTI)
      this.oPgFrm.Page4.oPag.oTIPARTI_4_2.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oTIPARTI_4_3.RadioValue()==this.w_TIPARTI)
      this.oPgFrm.Page4.oPag.oTIPARTI_4_3.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oPROVART_4_4.RadioValue()==this.w_PROVART)
      this.oPgFrm.Page4.oPag.oPROVART_4_4.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oARTOBS_4_5.RadioValue()==this.w_ARTOBS)
      this.oPgFrm.Page4.oPag.oARTOBS_4_5.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oARTINI_4_8.value==this.w_ARTINI)
      this.oPgFrm.Page4.oPag.oARTINI_4_8.value=this.w_ARTINI
    endif
    if not(this.oPgFrm.Page4.oPag.oDESART_4_9.value==this.w_DESART)
      this.oPgFrm.Page4.oPag.oDESART_4_9.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page4.oPag.oARTFIN_4_10.value==this.w_ARTFIN)
      this.oPgFrm.Page4.oPag.oARTFIN_4_10.value=this.w_ARTFIN
    endif
    if not(this.oPgFrm.Page4.oPag.oDESART1_4_11.value==this.w_DESART1)
      this.oPgFrm.Page4.oPag.oDESART1_4_11.value=this.w_DESART1
    endif
    if not(this.oPgFrm.Page4.oPag.oIVAFIL_4_12.value==this.w_IVAFIL)
      this.oPgFrm.Page4.oPag.oIVAFIL_4_12.value=this.w_IVAFIL
    endif
    if not(this.oPgFrm.Page4.oPag.oIVADES_4_13.value==this.w_IVADES)
      this.oPgFrm.Page4.oPag.oIVADES_4_13.value=this.w_IVADES
    endif
    if not(this.oPgFrm.Page4.oPag.oGRMFIL_4_14.value==this.w_GRMFIL)
      this.oPgFrm.Page4.oPag.oGRMFIL_4_14.value=this.w_GRMFIL
    endif
    if not(this.oPgFrm.Page4.oPag.oGRMDES_4_15.value==this.w_GRMDES)
      this.oPgFrm.Page4.oPag.oGRMDES_4_15.value=this.w_GRMDES
    endif
    if not(this.oPgFrm.Page4.oPag.oCATFIL_4_18.value==this.w_CATFIL)
      this.oPgFrm.Page4.oPag.oCATFIL_4_18.value=this.w_CATFIL
    endif
    if not(this.oPgFrm.Page4.oPag.oCATDES_4_19.value==this.w_CATDES)
      this.oPgFrm.Page4.oPag.oCATDES_4_19.value=this.w_CATDES
    endif
    if not(this.oPgFrm.Page4.oPag.oFAMFIL_4_20.value==this.w_FAMFIL)
      this.oPgFrm.Page4.oPag.oFAMFIL_4_20.value=this.w_FAMFIL
    endif
    if not(this.oPgFrm.Page4.oPag.oFAMDES_4_21.value==this.w_FAMDES)
      this.oPgFrm.Page4.oPag.oFAMDES_4_21.value=this.w_FAMDES
    endif
    if not(this.oPgFrm.Page4.oPag.oCATOFIL_4_24.value==this.w_CATOFIL)
      this.oPgFrm.Page4.oPag.oCATOFIL_4_24.value=this.w_CATOFIL
    endif
    if not(this.oPgFrm.Page4.oPag.oCATODES_4_25.value==this.w_CATODES)
      this.oPgFrm.Page4.oPag.oCATODES_4_25.value=this.w_CATODES
    endif
    if not(this.oPgFrm.Page4.oPag.oPROFIL_4_26.value==this.w_PROFIL)
      this.oPgFrm.Page4.oPag.oPROFIL_4_26.value=this.w_PROFIL
    endif
    if not(this.oPgFrm.Page4.oPag.oPRODES_4_27.value==this.w_PRODES)
      this.oPgFrm.Page4.oPag.oPRODES_4_27.value=this.w_PRODES
    endif
    if not(this.oPgFrm.Page4.oPag.oFORFIL_4_30.value==this.w_FORFIL)
      this.oPgFrm.Page4.oPag.oFORFIL_4_30.value=this.w_FORFIL
    endif
    if not(this.oPgFrm.Page4.oPag.oFORDES_4_31.value==this.w_FORDES)
      this.oPgFrm.Page4.oPag.oFORDES_4_31.value=this.w_FORDES
    endif
    if not(this.oPgFrm.Page4.oPag.oMARFIL_4_32.value==this.w_MARFIL)
      this.oPgFrm.Page4.oPag.oMARFIL_4_32.value=this.w_MARFIL
    endif
    if not(this.oPgFrm.Page4.oPag.oMARDES_4_33.value==this.w_MARDES)
      this.oPgFrm.Page4.oPag.oMARDES_4_33.value=this.w_MARDES
    endif
    if not(this.oPgFrm.Page4.oPag.oCOLFIL_4_44.value==this.w_COLFIL)
      this.oPgFrm.Page4.oPag.oCOLFIL_4_44.value=this.w_COLFIL
    endif
    if not(this.oPgFrm.Page4.oPag.oCONFIL_4_45.value==this.w_CONFIL)
      this.oPgFrm.Page4.oPag.oCONFIL_4_45.value=this.w_CONFIL
    endif
    if not(this.oPgFrm.Page4.oPag.oCOLDES_4_46.value==this.w_COLDES)
      this.oPgFrm.Page4.oPag.oCOLDES_4_46.value=this.w_COLDES
    endif
    if not(this.oPgFrm.Page4.oPag.oCONDES_4_47.value==this.w_CONDES)
      this.oPgFrm.Page4.oPag.oCONDES_4_47.value=this.w_CONDES
    endif
    if not(this.oPgFrm.Page2.oPag.oCOLSEL_2_33.RadioValue()==this.w_COLSEL)
      this.oPgFrm.Page2.oPag.oCOLSEL_2_33.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOLALL_2_34.RadioValue()==this.w_COLALL)
      this.oPgFrm.Page2.oPag.oCOLALL_2_34.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOLINI_2_35.value==this.w_COLINI)
      this.oPgFrm.Page2.oPag.oCOLINI_2_35.value=this.w_COLINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOL_2_36.value==this.w_DESCOL)
      this.oPgFrm.Page2.oPag.oDESCOL_2_36.value=this.w_DESCOL
    endif
    if not(this.oPgFrm.Page2.oPag.oCOLFIN_2_37.value==this.w_COLFIN)
      this.oPgFrm.Page2.oPag.oCOLFIN_2_37.value=this.w_COLFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOL1_2_38.value==this.w_DESCOL1)
      this.oPgFrm.Page2.oPag.oDESCOL1_2_38.value=this.w_DESCOL1
    endif
    if not(this.oPgFrm.Page2.oPag.oCONDEF_2_42.value==this.w_CONDEF)
      this.oPgFrm.Page2.oPag.oCONDEF_2_42.value=this.w_CONDEF
    endif
    if not(this.oPgFrm.Page2.oPag.osQTAMIN_2_43.RadioValue()==this.w_sQTAMIN)
      this.oPgFrm.Page2.oPag.osQTAMIN_2_43.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oUMPRI_2_44.value==this.w_UMPRI)
      this.oPgFrm.Page2.oPag.oUMPRI_2_44.value=this.w_UMPRI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOF_2_46.value==this.w_DESCOF)
      this.oPgFrm.Page2.oPag.oDESCOF_2_46.value=this.w_DESCOF
    endif
    if not(this.oPgFrm.Page2.oPag.ocMINVEN_2_47.value==this.w_cMINVEN)
      this.oPgFrm.Page2.oPag.ocMINVEN_2_47.value=this.w_cMINVEN
    endif
    if not(this.oPgFrm.Page2.oPag.oPUBINI_2_48.RadioValue()==this.w_PUBINI)
      this.oPgFrm.Page2.oPag.oPUBINI_2_48.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPUBSEL_2_49.RadioValue()==this.w_PUBSEL)
      this.oPgFrm.Page2.oPag.oPUBSEL_2_49.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPUBFIN_2_50.RadioValue()==this.w_PUBFIN)
      this.oPgFrm.Page2.oPag.oPUBFIN_2_50.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOSSEL_2_52.RadioValue()==this.w_COSSEL)
      this.oPgFrm.Page2.oPag.oCOSSEL_2_52.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOSALL_2_53.RadioValue()==this.w_COSALL)
      this.oPgFrm.Page2.oPag.oCOSALL_2_53.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDISTIN_2_54.RadioValue()==this.w_DISTIN)
      this.oPgFrm.Page2.oPag.oDISTIN_2_54.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAggiorn_2_55.RadioValue()==this.w_Aggiorn)
      this.oPgFrm.Page2.oPag.oAggiorn_2_55.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oLISTRIF_2_57.value==this.w_LISTRIF)
      this.oPgFrm.Page2.oPag.oLISTRIF_2_57.value=this.w_LISTRIF
    endif
    if not(this.oPgFrm.Page2.oPag.oSIMVAL_2_59.value==this.w_SIMVAL)
      this.oPgFrm.Page2.oPag.oSIMVAL_2_59.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oESERCI_2_66.value==this.w_ESERCI)
      this.oPgFrm.Page2.oPag.oESERCI_2_66.value=this.w_ESERCI
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMINV_2_67.value==this.w_NUMINV)
      this.oPgFrm.Page2.oPag.oNUMINV_2_67.value=this.w_NUMINV
    endif
    if not(this.oPgFrm.Page2.oPag.oCAMBIO_2_68.value==this.w_CAMBIO)
      this.oPgFrm.Page2.oPag.oCAMBIO_2_68.value=this.w_CAMBIO
    endif
    if not(this.oPgFrm.Page2.oPag.oRICPE_2_70.value==this.w_RICPE)
      this.oPgFrm.Page2.oPag.oRICPE_2_70.value=this.w_RICPE
    endif
    if not(this.oPgFrm.Page3.oPag.oFLGPOS_3_38.RadioValue()==this.w_FLGPOS)
      this.oPgFrm.Page3.oPag.oFLGPOS_3_38.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oREPSEL_3_40.RadioValue()==this.w_REPSEL)
      this.oPgFrm.Page3.oPag.oREPSEL_3_40.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oREPALL_3_41.RadioValue()==this.w_REPALL)
      this.oPgFrm.Page3.oPag.oREPALL_3_41.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oREPINI_3_42.value==this.w_REPINI)
      this.oPgFrm.Page3.oPag.oREPINI_3_42.value=this.w_REPINI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESREP_3_43.value==this.w_DESREP)
      this.oPgFrm.Page3.oPag.oDESREP_3_43.value=this.w_DESREP
    endif
    if not(this.oPgFrm.Page3.oPag.oREPFIN_3_44.value==this.w_REPFIN)
      this.oPgFrm.Page3.oPag.oREPFIN_3_44.value=this.w_REPFIN
    endif
    if not(this.oPgFrm.Page3.oPag.oDESREP1_3_45.value==this.w_DESREP1)
      this.oPgFrm.Page3.oPag.oDESREP1_3_45.value=this.w_DESREP1
    endif
    if not(this.oPgFrm.Page3.oPag.oPECOSEL_3_47.RadioValue()==this.w_PECOSEL)
      this.oPgFrm.Page3.oPag.oPECOSEL_3_47.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPECOINI_3_48.RadioValue()==this.w_PECOINI)
      this.oPgFrm.Page3.oPag.oPECOINI_3_48.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oPECOFIN_3_51.RadioValue()==this.w_PECOFIN)
      this.oPgFrm.Page3.oPag.oPECOFIN_3_51.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oREPFIL_4_48.value==this.w_REPFIL)
      this.oPgFrm.Page4.oPag.oREPFIL_4_48.value=this.w_REPFIL
    endif
    if not(this.oPgFrm.Page4.oPag.oREPDES_4_49.value==this.w_REPDES)
      this.oPgFrm.Page4.oPag.oREPDES_4_49.value=this.w_REPDES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESOI_1_46.value==this.w_DESOI)
      this.oPgFrm.Page1.oPag.oDESOI_1_46.value=this.w_DESOI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESOI1_1_47.value==this.w_DESOI1)
      this.oPgFrm.Page1.oPag.oDESOI1_1_47.value=this.w_DESOI1
    endif
    if not(this.oPgFrm.Page2.oPag.oFLGCAC_2_79.RadioValue()==this.w_FLGCAC)
      this.oPgFrm.Page2.oPag.oFLGCAC_2_79.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.ocMCTIPCAT_2_83.value==this.w_cMCTIPCAT)
      this.oPgFrm.Page2.oPag.ocMCTIPCAT_2_83.value=this.w_cMCTIPCAT
    endif
    if not(this.oPgFrm.Page2.oPag.ocMCCODCAT_2_84.value==this.w_cMCCODCAT)
      this.oPgFrm.Page2.oPag.ocMCCODCAT_2_84.value=this.w_cMCCODCAT
    endif
    if not(this.oPgFrm.Page2.oPag.oTPDESCRI_2_85.value==this.w_TPDESCRI)
      this.oPgFrm.Page2.oPag.oTPDESCRI_2_85.value=this.w_TPDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCGDESCRI_2_86.value==this.w_CGDESCRI)
      this.oPgFrm.Page2.oPag.oCGDESCRI_2_86.value=this.w_CGDESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPARTIN_3_53.RadioValue()==this.w_TIPARTIN)
      this.oPgFrm.Page3.oPag.oTIPARTIN_3_53.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPARTIF_3_55.RadioValue()==this.w_TIPARTIF)
      this.oPgFrm.Page3.oPag.oTIPARTIF_3_55.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oTIPASEL_3_56.RadioValue()==this.w_TIPASEL)
      this.oPgFrm.Page3.oPag.oTIPASEL_3_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSACSEL_1_51.RadioValue()==this.w_SACSEL)
      this.oPgFrm.Page1.oPag.oSACSEL_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSACALL_1_52.RadioValue()==this.w_SACALL)
      this.oPgFrm.Page1.oPag.oSACALL_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSACINI_1_53.RadioValue()==this.w_SACINI)
      this.oPgFrm.Page1.oPag.oSACINI_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSACFIN_1_55.RadioValue()==this.w_SACFIN)
      this.oPgFrm.Page1.oPag.oSACFIN_1_55.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUTISEL_1_56.RadioValue()==this.w_UTISEL)
      this.oPgFrm.Page1.oPag.oUTISEL_1_56.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUTIALL_1_57.RadioValue()==this.w_UTIALL)
      this.oPgFrm.Page1.oPag.oUTIALL_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUTIINI_1_58.RadioValue()==this.w_UTIINI)
      this.oPgFrm.Page1.oPag.oUTIINI_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUTIFIN_1_61.RadioValue()==this.w_UTIFIN)
      this.oPgFrm.Page1.oPag.oUTIFIN_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOSEL_1_65.RadioValue()==this.w_TIPOSEL)
      this.oPgFrm.Page1.oPag.oTIPOSEL_1_65.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOALL_1_66.RadioValue()==this.w_TIPOALL)
      this.oPgFrm.Page1.oPag.oTIPOALL_1_66.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOINI_1_67.value==this.w_TIPOINI)
      this.oPgFrm.Page1.oPag.oTIPOINI_1_67.value=this.w_TIPOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOFIN_1_68.value==this.w_TIPOFIN)
      this.oPgFrm.Page1.oPag.oTIPOFIN_1_68.value=this.w_TIPOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOSEL_1_71.RadioValue()==this.w_VALOSEL)
      this.oPgFrm.Page1.oPag.oVALOSEL_1_71.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOALL_1_72.RadioValue()==this.w_VALOALL)
      this.oPgFrm.Page1.oPag.oVALOALL_1_72.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOINI_1_73.value==this.w_VALOINI)
      this.oPgFrm.Page1.oPag.oVALOINI_1_73.value=this.w_VALOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOFIN_1_74.value==this.w_VALOFIN)
      this.oPgFrm.Page1.oPag.oVALOFIN_1_74.value=this.w_VALOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASSEL_1_77.RadioValue()==this.w_CLASSEL)
      this.oPgFrm.Page1.oPag.oCLASSEL_1_77.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASALL_1_78.RadioValue()==this.w_CLASALL)
      this.oPgFrm.Page1.oPag.oCLASALL_1_78.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASINI_1_79.value==this.w_CLASINI)
      this.oPgFrm.Page1.oPag.oCLASINI_1_79.value=this.w_CLASINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASFIN_1_80.value==this.w_CLASFIN)
      this.oPgFrm.Page1.oPag.oCLASFIN_1_80.value=this.w_CLASFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGESCSEL_1_84.RadioValue()==this.w_GESCSEL)
      this.oPgFrm.Page1.oPag.oGESCSEL_1_84.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGESCALL_1_85.RadioValue()==this.w_GESCALL)
      this.oPgFrm.Page1.oPag.oGESCALL_1_85.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGESCINI_1_86.RadioValue()==this.w_GESCINI)
      this.oPgFrm.Page1.oPag.oGESCINI_1_86.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGESCFIN_1_87.RadioValue()==this.w_GESCFIN)
      this.oPgFrm.Page1.oPag.oGESCFIN_1_87.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Iva obsoleto!",.F.))  and (.w_IVAALL<>'S')  and not(empty(.w_IVAINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIVAINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA incongruente o obsoleto")
          case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Iva obsoleto!",.F.))  and not(empty(.w_IVAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oIVAFIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA incongruente o obsoleto")
          case   not(i_datsys >= .w_TIPOPOBS)  and not(empty(.w_CIMINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCIMINI_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice operazione I.V.A. inesistente, oppure non valida alla data odierna")
          case   not(i_datsys >= .w_TIPOPOBS)  and not(empty(.w_CIMFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCIMFIN_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice operazione I.V.A. inesistente, oppure non valida alla data odierna")
          case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Voce di costo obsoleta!",.F.))  and ((g_PERCCR='S' OR g_COMM='S') AND .w_VOCALL<>'S')  and not(empty(.w_VOCINI))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oVOCINI_3_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Voce di costo incongruente o obsoleta")
          case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Voce di costo obsoleta!",.F.))  and (g_PERCCR='S' OR g_COMM='S')  and not(empty(.w_VOCFIN))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oVOCFIN_3_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Voce di costo incongruente o obsoleta")
          case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Voce di ricavo obsoleta!",.F.))  and ((g_PERCCR='S' OR g_COMM='S') AND .w_VORALL<>'S')  and not(empty(.w_VORINI))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oVORINI_3_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Voce di ricavo incongruente o obsoleta")
          case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Voce di ricavo obsoleta!",.F.))  and (g_PERCCR='S' OR g_COMM='S')  and not(empty(.w_VORFIN))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oVORFIN_3_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Voce di costo incongruente o obsoleta")
          case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Articolo obsoleto!",.F.) AND ((empty(.w_ARTFIN)) OR  (.w_ARTINI<=.w_ARTFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) OR .w_ARTOBS$'OT')  and not(empty(.w_ARTINI))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oARTINI_4_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale superiore al codice finale oppure obsoleto")
          case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Articolo obsoleto!",.F.) AND ((.w_ARTFIN>=.w_ARTINI) or (empty(.w_ARTINI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) OR .w_ARTOBS$'OT')  and not(empty(.w_ARTFIN))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oARTFIN_4_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale superiore al codice finale oppure obsoleto")
          case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Codice Iva obsoleto!",.F.))  and not(empty(.w_IVAFIL))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oIVAFIL_4_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA obsoleto")
          case   (empty(.w_UMPRI))  and not(.w_sQTAMIN<>'S')  and (.w_sQTAMIN='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oUMPRI_2_44.SetFocus()
            i_bnoObbl = !empty(.w_UMPRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_FLFRAZ<>'S' Or .w_cMINVEN =Int(.w_cMINVEN))  and not(.w_sQTAMIN<>'S')  and (.w_sQTAMIN='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.ocMINVEN_2_47.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'unit� di misura selezionata non � frazionabile")
          case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Listino di Riferimento Obsoleto!",.F.))  and not(.w_AGGIORN<>'LI' OR .w_COSSEL<>'S')  and (.w_AGGIORN='LI'  AND .w_COSSEL='S')  and not(empty(.w_LISTRIF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oLISTRIF_2_57.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Listino di riferimento ineistente o obsoleto")
          case   (empty(.w_CAMBIO))  and not( .w_AGGIORN <>'LI' OR .w_COSSEL<>'S' OR EMPTY(.w_LISTRIF) OR .w_CAOLIS<>0)  and (.w_CAOLIS=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCAMBIO_2_68.SetFocus()
            i_bnoObbl = !empty(.w_CAMBIO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il cambio di riferimento")
          case   (empty(.w_DATRIFE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATRIFE_2_73.SetFocus()
            i_bnoObbl = !empty(.w_DATRIFE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_IVAALL = this.w_IVAALL
    this.o_IVAFIN = this.w_IVAFIN
    this.o_CIMFIN = this.w_CIMFIN
    this.o_GRMALL = this.w_GRMALL
    this.o_GRMFIN = this.w_GRMFIN
    this.o_CATALL = this.w_CATALL
    this.o_CATFIN = this.w_CATFIN
    this.o_FAMALL = this.w_FAMALL
    this.o_FAMFIN = this.w_FAMFIN
    this.o_CATOALL = this.w_CATOALL
    this.o_CATOFIN = this.w_CATOFIN
    this.o_CLAALL = this.w_CLAALL
    this.o_CLAFIN = this.w_CLAFIN
    this.o_SCOALL = this.w_SCOALL
    this.o_SCOFIN = this.w_SCOFIN
    this.o_MARALL = this.w_MARALL
    this.o_MARFIN = this.w_MARFIN
    this.o_CAPALL = this.w_CAPALL
    this.o_CAPFIN = this.w_CAPFIN
    this.o_VOCALL = this.w_VOCALL
    this.o_VOCFIN = this.w_VOCFIN
    this.o_VORALL = this.w_VORALL
    this.o_VORFIN = this.w_VORFIN
    this.o_FORALL = this.w_FORALL
    this.o_FORFIN = this.w_FORFIN
    this.o_TIPSEL = this.w_TIPSEL
    this.o_ARTOBS = this.w_ARTOBS
    this.o_COLALL = this.w_COLALL
    this.o_COLFIN = this.w_COLFIN
    this.o_sQTAMIN = this.w_sQTAMIN
    this.o_UMPRI = this.w_UMPRI
    this.o_PUBINI = this.w_PUBINI
    this.o_PUBSEL = this.w_PUBSEL
    this.o_COSSEL = this.w_COSSEL
    this.o_Aggiorn = this.w_Aggiorn
    this.o_VALUTA = this.w_VALUTA
    this.o_VALRIF = this.w_VALRIF
    this.o_ESERCI = this.w_ESERCI
    this.o_REPALL = this.w_REPALL
    this.o_REPFIN = this.w_REPFIN
    this.o_cMCTIPCAT = this.w_cMCTIPCAT
    this.o_UTIALL = this.w_UTIALL
    this.o_TIPOALL = this.w_TIPOALL
    this.o_TIPOFIN = this.w_TIPOFIN
    this.o_VALOALL = this.w_VALOALL
    this.o_VALOFIN = this.w_VALOFIN
    this.o_CLASALL = this.w_CLASALL
    this.o_CLASFIN = this.w_CLASFIN
    this.o_GESCALL = this.w_GESCALL
    this.o_GESCFIN = this.w_GESCFIN
    return

enddefine

* --- Define pages as container
define class tgsma_kvaPag1 as StdContainer
  Width  = 645
  height = 596
  stdWidth  = 645
  stdheight = 596
  resizeXpos=508
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIVASEL_1_1 as StdCheck with uid="LEZIBFLUTY",rtseq=1,rtrep=.f.,left=145, top=33, caption="Sostituire con:", enabled=.f.,;
    ToolTipText = "Se attivo: sostituisce il codice IVA",;
    HelpContextID = 37949562,;
    cFormVar="w_IVASEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIVASEL_1_1.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oIVASEL_1_1.GetRadio()
    this.Parent.oContained.w_IVASEL = this.RadioValue()
    return .t.
  endfunc

  func oIVASEL_1_1.SetRadio()
    this.Parent.oContained.w_IVASEL=trim(this.Parent.oContained.w_IVASEL)
    this.value = ;
      iif(this.Parent.oContained.w_IVASEL=='S',1,;
      0)
  endfunc

  add object oIVAALL_1_2 as StdCheck with uid="LQVMEHOEYX",rtseq=2,rtrep=.f.,left=145, top=10, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutti i codici IVA",;
    HelpContextID = 31789178,;
    cFormVar="w_IVAALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIVAALL_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oIVAALL_1_2.GetRadio()
    this.Parent.oContained.w_IVAALL = this.RadioValue()
    return .t.
  endfunc

  func oIVAALL_1_2.SetRadio()
    this.Parent.oContained.w_IVAALL=trim(this.Parent.oContained.w_IVAALL)
    this.value = ;
      iif(this.Parent.oContained.w_IVAALL=='S',1,;
      0)
  endfunc

  add object oIVAINI_1_3 as StdField with uid="IANYSSOCGF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_IVAINI", cQueryName = "IVAINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA incongruente o obsoleto",;
    ToolTipText = "Codice IVA originario/a da sostituire",;
    HelpContextID = 79499386,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=246, Top=10, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_IVAINI"

  func oIVAINI_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IVAALL<>'S')
    endwith
   endif
  endfunc

  func oIVAINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oIVAINI_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIVAINI_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oIVAINI_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oIVAINI_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_IVAINI
     i_obj.ecpSave()
  endproc

  add object oDESIVA_1_4 as StdField with uid="BROOVUECUP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 205259210,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=10, InputMask=replicate('X',35)

  add object oIVAFIN_1_5 as StdField with uid="DITGWVKJMP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_IVAFIN", cQueryName = "IVAFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA incongruente o obsoleto",;
    ToolTipText = "Codice IVA che � possibile inserire in sostituzione",;
    HelpContextID = 1052794,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=246, Top=33, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_IVAFIN"

  func oIVAFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oIVAFIN_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIVAFIN_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oIVAFIN_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oIVAFIN_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_IVAFIN
     i_obj.ecpSave()
  endproc

  add object oDESIVA1_1_6 as StdField with uid="KZOSDFSSXL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESIVA1", cQueryName = "DESIVA1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 205259210,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=33, InputMask=replicate('X',35)

  add object oCIMSEL_1_7 as StdCheck with uid="NHJBHRFWZE",rtseq=7,rtrep=.f.,left=145, top=86, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce il codice operazione IVA, se non specificato sbianca",;
    HelpContextID = 37903834,;
    cFormVar="w_CIMSEL", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oCIMSEL_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCIMSEL_1_7.GetRadio()
    this.Parent.oContained.w_CIMSEL = this.RadioValue()
    return .t.
  endfunc

  func oCIMSEL_1_7.SetRadio()
    this.Parent.oContained.w_CIMSEL=trim(this.Parent.oContained.w_CIMSEL)
    this.value = ;
      iif(this.Parent.oContained.w_CIMSEL=='S',1,;
      0)
  endfunc

  add object oCIMALL_1_8 as StdCheck with uid="EKEJGTMFLJ",rtseq=8,rtrep=.f.,left=145, top=64, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutti le operazioni IVA",;
    HelpContextID = 31743450,;
    cFormVar="w_CIMALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCIMALL_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCIMALL_1_8.GetRadio()
    this.Parent.oContained.w_CIMALL = this.RadioValue()
    return .t.
  endfunc

  func oCIMALL_1_8.SetRadio()
    this.Parent.oContained.w_CIMALL=trim(this.Parent.oContained.w_CIMALL)
    this.value = ;
      iif(this.Parent.oContained.w_CIMALL=='S',1,;
      0)
  endfunc

  add object oCIMINI_1_9 as StdField with uid="BJEOUHOKVK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CIMINI", cQueryName = "CIMINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice operazione I.V.A. inesistente, oppure non valida alla data odierna",;
    ToolTipText = "Codice operazione I.V.A. da sostituire",;
    HelpContextID = 79453658,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=246, Top=64, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPCODIV", cZoomOnZoom="GSAR_ATO", oKey_1_1="TI__TIPO", oKey_1_2="this.w_TIPOPEIVA", oKey_2_1="TICODICE", oKey_2_2="this.w_CIMINI"

  func oCIMINI_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCIMINI_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCIMINI_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIPCODIV_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_TIPOPEIVA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStr(this.Parent.oContained.w_TIPOPEIVA)
    endif
    do cp_zoom with 'TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(this.parent,'oCIMINI_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATO',"Tipologia operazione I.V.A.",'',this.parent.oContained
  endproc
  proc oCIMINI_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TI__TIPO=w_TIPOPEIVA
     i_obj.w_TICODICE=this.parent.oContained.w_CIMINI
     i_obj.ecpSave()
  endproc

  add object oCIMFIN_1_10 as StdField with uid="NWLEGJERXG",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CIMFIN", cQueryName = "CIMFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice operazione I.V.A. inesistente, oppure non valida alla data odierna",;
    ToolTipText = "Codice operazione I.V.A. che � possibile inserire in sostituzione",;
    HelpContextID = 1007066,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=246, Top=87, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPCODIV", cZoomOnZoom="GSAR_ATO", oKey_1_1="TI__TIPO", oKey_1_2="this.w_TIPOPEIVA", oKey_2_1="TICODICE", oKey_2_2="this.w_CIMFIN"

  func oCIMFIN_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCIMFIN_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCIMFIN_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIPCODIV_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_TIPOPEIVA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStr(this.Parent.oContained.w_TIPOPEIVA)
    endif
    do cp_zoom with 'TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(this.parent,'oCIMFIN_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATO',"Tipologia operazione I.V.A.",'',this.parent.oContained
  endproc
  proc oCIMFIN_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TI__TIPO=w_TIPOPEIVA
     i_obj.w_TICODICE=this.parent.oContained.w_CIMFIN
     i_obj.ecpSave()
  endproc

  add object oGRMSEL_1_13 as StdCheck with uid="JTAZUFIROQ",rtseq=11,rtrep=.f.,left=145, top=140, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce il gruppo merceologico, se non specificato sbianca",;
    HelpContextID = 37901466,;
    cFormVar="w_GRMSEL", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oGRMSEL_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oGRMSEL_1_13.GetRadio()
    this.Parent.oContained.w_GRMSEL = this.RadioValue()
    return .t.
  endfunc

  func oGRMSEL_1_13.SetRadio()
    this.Parent.oContained.w_GRMSEL=trim(this.Parent.oContained.w_GRMSEL)
    this.value = ;
      iif(this.Parent.oContained.w_GRMSEL=='S',1,;
      0)
  endfunc

  func oGRMSEL_1_13.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oGRMALL_1_14 as StdCheck with uid="SVKVJLWWMB",rtseq=12,rtrep=.f.,left=145, top=118, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutti i gruppi merceologici",;
    HelpContextID = 31741082,;
    cFormVar="w_GRMALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGRMALL_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oGRMALL_1_14.GetRadio()
    this.Parent.oContained.w_GRMALL = this.RadioValue()
    return .t.
  endfunc

  func oGRMALL_1_14.SetRadio()
    this.Parent.oContained.w_GRMALL=trim(this.Parent.oContained.w_GRMALL)
    this.value = ;
      iif(this.Parent.oContained.w_GRMALL=='S',1,;
      0)
  endfunc

  func oGRMALL_1_14.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oGRMINI_1_15 as StdField with uid="NBVGPIHKIM",rtseq=13,rtrep=.f.,;
    cFormVar = "w_GRMINI", cQueryName = "GRMINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merc. originario da sostituire",;
    HelpContextID = 79451290,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=246, Top=118, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRMINI"

  func oGRMINI_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GRMALL<>'S')
    endwith
   endif
  endfunc

  func oGRMINI_1_15.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oGRMINI_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRMINI_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRMINI_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRMINI_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oGRMINI_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRMINI
     i_obj.ecpSave()
  endproc

  add object oDESGRU_1_16 as StdField with uid="ZEOQXKZZEH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 142475722,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=118, InputMask=replicate('X',35)

  func oDESGRU_1_16.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oGRMFIN_1_17 as StdField with uid="YRHWXZQVOG",rtseq=15,rtrep=.f.,;
    cFormVar = "w_GRMFIN", cQueryName = "GRMFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merc. che � possibile inserire in sostituzione",;
    HelpContextID = 1004698,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=246, Top=141, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRMFIN"

  func oGRMFIN_1_17.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oGRMFIN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRMFIN_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRMFIN_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRMFIN_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oGRMFIN_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRMFIN
     i_obj.ecpSave()
  endproc

  add object oDESGRU1_1_18 as StdField with uid="EJCUXZLXZC",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESGRU1", cQueryName = "DESGRU1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 142475722,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=141, InputMask=replicate('X',35)

  func oDESGRU1_1_18.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCATSEL_1_21 as StdCheck with uid="BFKJSQQKYQ",rtseq=17,rtrep=.f.,left=145, top=194, caption="Sostituire con:", enabled=.f.,;
    ToolTipText = "Se attivo: sostituisce la categoria contabile di appartenenza",;
    HelpContextID = 37877210,;
    cFormVar="w_CATSEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCATSEL_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCATSEL_1_21.GetRadio()
    this.Parent.oContained.w_CATSEL = this.RadioValue()
    return .t.
  endfunc

  func oCATSEL_1_21.SetRadio()
    this.Parent.oContained.w_CATSEL=trim(this.Parent.oContained.w_CATSEL)
    this.value = ;
      iif(this.Parent.oContained.w_CATSEL=='S',1,;
      0)
  endfunc

  add object oCATALL_1_22 as StdCheck with uid="JGPOZJIVAJ",rtseq=18,rtrep=.f.,left=145, top=172, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutte le categorie contabili",;
    HelpContextID = 31716826,;
    cFormVar="w_CATALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCATALL_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCATALL_1_22.GetRadio()
    this.Parent.oContained.w_CATALL = this.RadioValue()
    return .t.
  endfunc

  func oCATALL_1_22.SetRadio()
    this.Parent.oContained.w_CATALL=trim(this.Parent.oContained.w_CATALL)
    this.value = ;
      iif(this.Parent.oContained.w_CATALL=='S',1,;
      0)
  endfunc

  add object oCATINI_1_23 as StdField with uid="RUTYXQFVCH",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CATINI", cQueryName = "CATINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Cat. contabile originaria da sostituire",;
    HelpContextID = 79427034,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=245, Top=172, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOARTI", cZoomOnZoom="GSAR_AC1", oKey_1_1="C1CODICE", oKey_1_2="this.w_CATINI"

  func oCATINI_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CATALL<>'S')
    endwith
   endif
  endfunc

  func oCATINI_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATINI_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATINI_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOARTI','*','C1CODICE',cp_AbsName(this.parent,'oCATINI_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC1',"Categorie contabili",'',this.parent.oContained
  endproc
  proc oCATINI_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC1()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C1CODICE=this.parent.oContained.w_CATINI
     i_obj.ecpSave()
  endproc

  add object oDESCAT_1_24 as StdField with uid="JQAHJNYCZI",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 177340874,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=172, InputMask=replicate('X',35)

  add object oCATFIN_1_25 as StdField with uid="RTVCKSHMGL",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CATFIN", cQueryName = "CATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Cat.contabile che � possibile inserire in sostituzione",;
    HelpContextID = 980442,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=245, Top=195, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOARTI", cZoomOnZoom="GSAR_AC1", oKey_1_1="C1CODICE", oKey_1_2="this.w_CATFIN"

  func oCATFIN_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIN_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIN_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOARTI','*','C1CODICE',cp_AbsName(this.parent,'oCATFIN_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC1',"Categorie contabili",'',this.parent.oContained
  endproc
  proc oCATFIN_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC1()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C1CODICE=this.parent.oContained.w_CATFIN
     i_obj.ecpSave()
  endproc

  add object oDESCAT1_1_26 as StdField with uid="EKZJMICXFV",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCAT1", cQueryName = "DESCAT1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 177340874,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=195, InputMask=replicate('X',35)

  add object oFAMSEL_1_29 as StdCheck with uid="DARIMQSYXD",rtseq=23,rtrep=.f.,left=145, top=249, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce il codice famiglia di appartenenza, se non specificato sbianca",;
    HelpContextID = 37905834,;
    cFormVar="w_FAMSEL", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oFAMSEL_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFAMSEL_1_29.GetRadio()
    this.Parent.oContained.w_FAMSEL = this.RadioValue()
    return .t.
  endfunc

  func oFAMSEL_1_29.SetRadio()
    this.Parent.oContained.w_FAMSEL=trim(this.Parent.oContained.w_FAMSEL)
    this.value = ;
      iif(this.Parent.oContained.w_FAMSEL=='S',1,;
      0)
  endfunc

  func oFAMSEL_1_29.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oFAMALL_1_30 as StdCheck with uid="VHPWITLKOF",rtseq=24,rtrep=.f.,left=145, top=227, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutte le famiglie di appartenenza",;
    HelpContextID = 31745450,;
    cFormVar="w_FAMALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFAMALL_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFAMALL_1_30.GetRadio()
    this.Parent.oContained.w_FAMALL = this.RadioValue()
    return .t.
  endfunc

  func oFAMALL_1_30.SetRadio()
    this.Parent.oContained.w_FAMALL=trim(this.Parent.oContained.w_FAMALL)
    this.value = ;
      iif(this.Parent.oContained.w_FAMALL=='S',1,;
      0)
  endfunc

  func oFAMALL_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oFAMALL_1_30.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oFAMINI_1_31 as StdField with uid="BEJQTGTUKS",rtseq=25,rtrep=.f.,;
    cFormVar = "w_FAMINI", cQueryName = "FAMINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia originario da sostituire",;
    HelpContextID = 79455658,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=244, Top=227, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMINI"

  func oFAMINI_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FAMALL<>'S' and .w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oFAMINI_1_31.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oFAMINI_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMINI_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMINI_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMINI_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Codici famiglie",'',this.parent.oContained
  endproc
  proc oFAMINI_1_31.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_FAMINI
     i_obj.ecpSave()
  endproc

  add object oDESFAM_1_32 as StdField with uid="XIXRCQVOWP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 26149322,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=227, InputMask=replicate('X',35)

  func oDESFAM_1_32.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oFAMFIN_1_33 as StdField with uid="MQQJFOAAWM",rtseq=27,rtrep=.f.,;
    cFormVar = "w_FAMFIN", cQueryName = "FAMFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia che � possibile inserire in sostituzione",;
    HelpContextID = 1009066,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=244, Top=250, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMFIN"

  func oFAMFIN_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oFAMFIN_1_33.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oFAMFIN_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMFIN_1_33.ecpDrop(oSource)
    this.Parent.oContained.link_1_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMFIN_1_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMFIN_1_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Codici famiglie",'',this.parent.oContained
  endproc
  proc oFAMFIN_1_33.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_FAMFIN
     i_obj.ecpSave()
  endproc

  add object oDESFAM1_1_34 as StdField with uid="JXIDNUYTIN",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESFAM1", cQueryName = "DESFAM1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 26149322,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=250, InputMask=replicate('X',35)

  func oDESFAM1_1_34.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCATOSEL_1_37 as StdCheck with uid="ZFPMVEDYKK",rtseq=29,rtrep=.f.,left=145, top=303, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce la categoria omogenea di appartenenza, se non specificato sbianca",;
    HelpContextID = 140899802,;
    cFormVar="w_CATOSEL", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oCATOSEL_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCATOSEL_1_37.GetRadio()
    this.Parent.oContained.w_CATOSEL = this.RadioValue()
    return .t.
  endfunc

  func oCATOSEL_1_37.SetRadio()
    this.Parent.oContained.w_CATOSEL=trim(this.Parent.oContained.w_CATOSEL)
    this.value = ;
      iif(this.Parent.oContained.w_CATOSEL=='S',1,;
      0)
  endfunc

  func oCATOSEL_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oCATOSEL_1_37.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCATOALL_1_38 as StdCheck with uid="DXRUIEGPYD",rtseq=30,rtrep=.f.,left=145, top=281, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutte le categorie omogenee",;
    HelpContextID = 42333658,;
    cFormVar="w_CATOALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCATOALL_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCATOALL_1_38.GetRadio()
    this.Parent.oContained.w_CATOALL = this.RadioValue()
    return .t.
  endfunc

  func oCATOALL_1_38.SetRadio()
    this.Parent.oContained.w_CATOALL=trim(this.Parent.oContained.w_CATOALL)
    this.value = ;
      iif(this.Parent.oContained.w_CATOALL=='S',1,;
      0)
  endfunc

  func oCATOALL_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oCATOALL_1_38.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCATOINI_1_39 as StdField with uid="WQUOEFJQSH",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CATOINI", cQueryName = "CATOINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Cat.omogenea originaria da sostituire",;
    HelpContextID = 268044838,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=244, Top=281, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATOINI"

  func oCATOINI_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CATOALL<>'S' and .w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oCATOINI_1_39.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oCATOINI_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATOINI_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATOINI_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATOINI_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc oCATOINI_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CATOINI
     i_obj.ecpSave()
  endproc

  add object oDESCATO_1_40 as StdField with uid="RJYWKHYWJT",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESCATO", cQueryName = "DESCATO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 177340874,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=281, InputMask=replicate('X',35)

  func oDESCATO_1_40.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCATOFIN_1_41 as StdField with uid="HYODICYWOX",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CATOFIN", cQueryName = "CATOFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Cat.omogenea che � possibile inserire in sostituzione",;
    HelpContextID = 87422426,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=244, Top=304, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATOFIN"

  func oCATOFIN_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oCATOFIN_1_41.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oCATOFIN_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_41('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATOFIN_1_41.ecpDrop(oSource)
    this.Parent.oContained.link_1_41('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATOFIN_1_41.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATOFIN_1_41'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc oCATOFIN_1_41.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CATOFIN
     i_obj.ecpSave()
  endproc

  add object oDESCATO1_1_42 as StdField with uid="KCDCYGLVTN",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESCATO1", cQueryName = "DESCATO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 177340825,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=304, InputMask=replicate('X',35)

  func oDESCATO1_1_42.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oDESOI_1_46 as StdField with uid="HHXTAYAOWD",rtseq=167,rtrep=.f.,;
    cFormVar = "w_DESOI", cQueryName = "DESOI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 235274698,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=64, InputMask=replicate('X',35)

  add object oDESOI1_1_47 as StdField with uid="ZNPJDOIYEM",rtseq=168,rtrep=.f.,;
    cFormVar = "w_DESOI1", cQueryName = "DESOI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 218497482,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=87, InputMask=replicate('X',35)

  add object oSACSEL_1_51 as StdCheck with uid="RPLXVPLOMF",rtseq=179,rtrep=.f.,left=144, top=140, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce il flag spesa e anticipazione collegate",;
    HelpContextID = 37946586,;
    cFormVar="w_SACSEL", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oSACSEL_1_51.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSACSEL_1_51.GetRadio()
    this.Parent.oContained.w_SACSEL = this.RadioValue()
    return .t.
  endfunc

  func oSACSEL_1_51.SetRadio()
    this.Parent.oContained.w_SACSEL=trim(this.Parent.oContained.w_SACSEL)
    this.value = ;
      iif(this.Parent.oContained.w_SACSEL=='S',1,;
      0)
  endfunc

  func oSACSEL_1_51.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oSACALL_1_52 as StdCheck with uid="ARTFEZPOQT",rtseq=180,rtrep=.f.,left=144, top=118, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutti i flag spesa e anticipazione collegate",;
    HelpContextID = 31786202,;
    cFormVar="w_SACALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSACALL_1_52.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSACALL_1_52.GetRadio()
    this.Parent.oContained.w_SACALL = this.RadioValue()
    return .t.
  endfunc

  func oSACALL_1_52.SetRadio()
    this.Parent.oContained.w_SACALL=trim(this.Parent.oContained.w_SACALL)
    this.value = ;
      iif(this.Parent.oContained.w_SACALL=='S',1,;
      0)
  endfunc

  func oSACALL_1_52.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oSACINI_1_53 as StdCheck with uid="ZVFUIXZTHU",rtseq=181,rtrep=.f.,left=245, top=118, caption="Spesa e anticipazione collegate",;
    ToolTipText = "Spesa e anticipazione collegate da sostituire",;
    HelpContextID = 79496410,;
    cFormVar="w_SACINI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSACINI_1_53.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSACINI_1_53.GetRadio()
    this.Parent.oContained.w_SACINI = this.RadioValue()
    return .t.
  endfunc

  func oSACINI_1_53.SetRadio()
    this.Parent.oContained.w_SACINI=trim(this.Parent.oContained.w_SACINI)
    this.value = ;
      iif(this.Parent.oContained.w_SACINI=='S',1,;
      0)
  endfunc

  func oSACINI_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SACALL<>'S')
    endwith
   endif
  endfunc

  func oSACINI_1_53.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oSACFIN_1_55 as StdCheck with uid="OHGEXCVXSZ",rtseq=182,rtrep=.f.,left=245, top=140, caption="Spesa e anticipazione collegate",;
    ToolTipText = "Spesa e anticipazione collegate che � possibile inserire in sostituzione",;
    HelpContextID = 1049818,;
    cFormVar="w_SACFIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSACFIN_1_55.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSACFIN_1_55.GetRadio()
    this.Parent.oContained.w_SACFIN = this.RadioValue()
    return .t.
  endfunc

  func oSACFIN_1_55.SetRadio()
    this.Parent.oContained.w_SACFIN=trim(this.Parent.oContained.w_SACFIN)
    this.value = ;
      iif(this.Parent.oContained.w_SACFIN=='S',1,;
      0)
  endfunc

  func oSACFIN_1_55.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oUTISEL_1_56 as StdCheck with uid="LYVWIPBIMO",rtseq=183,rtrep=.f.,left=145, top=357, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce gli utilizzi intra di appartenenza",;
    HelpContextID = 37917114,;
    cFormVar="w_UTISEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oUTISEL_1_56.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oUTISEL_1_56.GetRadio()
    this.Parent.oContained.w_UTISEL = this.RadioValue()
    return .t.
  endfunc

  func oUTISEL_1_56.SetRadio()
    this.Parent.oContained.w_UTISEL=trim(this.Parent.oContained.w_UTISEL)
    this.value = ;
      iif(this.Parent.oContained.w_UTISEL=='S',1,;
      0)
  endfunc

  add object oUTIALL_1_57 as StdCheck with uid="KLAXNEAZIA",rtseq=184,rtrep=.f.,left=145, top=335, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutti gli utilizzi intra",;
    HelpContextID = 31756730,;
    cFormVar="w_UTIALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oUTIALL_1_57.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oUTIALL_1_57.GetRadio()
    this.Parent.oContained.w_UTIALL = this.RadioValue()
    return .t.
  endfunc

  func oUTIALL_1_57.SetRadio()
    this.Parent.oContained.w_UTIALL=trim(this.Parent.oContained.w_UTIALL)
    this.value = ;
      iif(this.Parent.oContained.w_UTIALL=='S',1,;
      0)
  endfunc

  func oUTIALL_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_INTR='S')
    endwith
   endif
  endfunc


  add object oUTIINI_1_58 as StdCombo with uid="CYCARLRQGL",rtseq=185,rtrep=.f.,left=244,top=337,width=118,height=21;
    , ToolTipText = "Utilizzo intra originario da sostituire";
    , HelpContextID = 79466938;
    , cFormVar="w_UTIINI",RowSource=""+"Servizi,"+"Beni", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oUTIINI_1_58.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(5))))
  endfunc
  func oUTIINI_1_58.GetRadio()
    this.Parent.oContained.w_UTIINI = this.RadioValue()
    return .t.
  endfunc

  func oUTIINI_1_58.SetRadio()
    this.Parent.oContained.w_UTIINI=trim(this.Parent.oContained.w_UTIINI)
    this.value = ;
      iif(this.Parent.oContained.w_UTIINI=='S',1,;
      iif(this.Parent.oContained.w_UTIINI=='N',2,;
      0))
  endfunc

  func oUTIINI_1_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_INTR='S')
    endwith
   endif
  endfunc


  add object oUTIFIN_1_61 as StdCombo with uid="OUYPVMUNAY",rtseq=186,rtrep=.f.,left=244,top=362,width=118,height=21;
    , ToolTipText = "Utilizzo intra originario da sostituire";
    , HelpContextID = 1020346;
    , cFormVar="w_UTIFIN",RowSource=""+"Servizi,"+"Beni", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oUTIFIN_1_61.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(5))))
  endfunc
  func oUTIFIN_1_61.GetRadio()
    this.Parent.oContained.w_UTIFIN = this.RadioValue()
    return .t.
  endfunc

  func oUTIFIN_1_61.SetRadio()
    this.Parent.oContained.w_UTIFIN=trim(this.Parent.oContained.w_UTIFIN)
    this.value = ;
      iif(this.Parent.oContained.w_UTIFIN=='S',1,;
      iif(this.Parent.oContained.w_UTIFIN=='N',2,;
      0))
  endfunc

  func oUTIFIN_1_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_INTR='S')
    endwith
   endif
  endfunc


  add object oBtn_1_62 as StdButton with uid="FLHPKWGBVV",left=532, top=551, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 48892698;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_62.Click()
      with this.Parent.oContained
        do GSMA_BVA with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_62.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT ISALT())
     endwith
    endif
  endfunc


  add object oBtn_1_63 as StdButton with uid="UKUYWJQGDI",left=584, top=551, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 41604026;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_63.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_63.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT ISALT())
     endwith
    endif
  endfunc

  add object oTIPOSEL_1_65 as StdCheck with uid="GNFZDTVMQA",rtseq=187,rtrep=.f.,left=145, top=412, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce il codice tipo, se non specificato sbianca",;
    HelpContextID = 140913866,;
    cFormVar="w_TIPOSEL", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oTIPOSEL_1_65.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTIPOSEL_1_65.GetRadio()
    this.Parent.oContained.w_TIPOSEL = this.RadioValue()
    return .t.
  endfunc

  func oTIPOSEL_1_65.SetRadio()
    this.Parent.oContained.w_TIPOSEL=trim(this.Parent.oContained.w_TIPOSEL)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOSEL=='S',1,;
      0)
  endfunc

  add object oTIPOALL_1_66 as StdCheck with uid="RMTMDESJHW",rtseq=188,rtrep=.f.,left=145, top=390, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutte i codici tipo",;
    HelpContextID = 42347722,;
    cFormVar="w_TIPOALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPOALL_1_66.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTIPOALL_1_66.GetRadio()
    this.Parent.oContained.w_TIPOALL = this.RadioValue()
    return .t.
  endfunc

  func oTIPOALL_1_66.SetRadio()
    this.Parent.oContained.w_TIPOALL=trim(this.Parent.oContained.w_TIPOALL)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOALL=='S',1,;
      0)
  endfunc

  add object oTIPOINI_1_67 as StdField with uid="NCOKRNOCVK",rtseq=189,rtrep=.f.,;
    cFormVar = "w_TIPOINI", cQueryName = "TIPOINI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipo originario da sostituire",;
    HelpContextID = 268030774,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=244, Top=390, InputMask=replicate('X',35)

  func oTIPOINI_1_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOALL<>'S')
    endwith
   endif
  endfunc

  add object oTIPOFIN_1_68 as StdField with uid="DJXYZPZWBW",rtseq=190,rtrep=.f.,;
    cFormVar = "w_TIPOFIN", cQueryName = "TIPOFIN",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipo che � possibile inserire in sostituzione",;
    HelpContextID = 87436490,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=244, Top=413, InputMask=replicate('X',35)

  add object oVALOSEL_1_71 as StdCheck with uid="NIXMZIFRUX",rtseq=191,rtrep=.f.,left=145, top=468, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce il codice valore, se non specificato sbianca",;
    HelpContextID = 140932266,;
    cFormVar="w_VALOSEL", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oVALOSEL_1_71.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVALOSEL_1_71.GetRadio()
    this.Parent.oContained.w_VALOSEL = this.RadioValue()
    return .t.
  endfunc

  func oVALOSEL_1_71.SetRadio()
    this.Parent.oContained.w_VALOSEL=trim(this.Parent.oContained.w_VALOSEL)
    this.value = ;
      iif(this.Parent.oContained.w_VALOSEL=='S',1,;
      0)
  endfunc

  add object oVALOALL_1_72 as StdCheck with uid="CBNBIAHQWE",rtseq=192,rtrep=.f.,left=145, top=446, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutti i codici valore",;
    HelpContextID = 42366122,;
    cFormVar="w_VALOALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVALOALL_1_72.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVALOALL_1_72.GetRadio()
    this.Parent.oContained.w_VALOALL = this.RadioValue()
    return .t.
  endfunc

  func oVALOALL_1_72.SetRadio()
    this.Parent.oContained.w_VALOALL=trim(this.Parent.oContained.w_VALOALL)
    this.value = ;
      iif(this.Parent.oContained.w_VALOALL=='S',1,;
      0)
  endfunc

  add object oVALOINI_1_73 as StdField with uid="TLFDYFGFKR",rtseq=193,rtrep=.f.,;
    cFormVar = "w_VALOINI", cQueryName = "VALOINI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Codice valore originario da sostituire",;
    HelpContextID = 268012374,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=244, Top=446, InputMask=replicate('X',35)

  func oVALOINI_1_73.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALOALL<>'S' )
    endwith
   endif
  endfunc

  add object oVALOFIN_1_74 as StdField with uid="URURPOLANG",rtseq=194,rtrep=.f.,;
    cFormVar = "w_VALOFIN", cQueryName = "VALOFIN",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Codice valore che � possibile inserire in sostituzione",;
    HelpContextID = 87454890,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=244, Top=469, InputMask=replicate('X',35)

  add object oCLASSEL_1_77 as StdCheck with uid="RJKTLLUFMO",rtseq=195,rtrep=.f.,left=145, top=519, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce le Classificazioni FE, se non specificato sbianca",;
    HelpContextID = 140712666,;
    cFormVar="w_CLASSEL", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oCLASSEL_1_77.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCLASSEL_1_77.GetRadio()
    this.Parent.oContained.w_CLASSEL = this.RadioValue()
    return .t.
  endfunc

  func oCLASSEL_1_77.SetRadio()
    this.Parent.oContained.w_CLASSEL=trim(this.Parent.oContained.w_CLASSEL)
    this.value = ;
      iif(this.Parent.oContained.w_CLASSEL=='S',1,;
      0)
  endfunc

  add object oCLASALL_1_78 as StdCheck with uid="NHLFTRUSFE",rtseq=196,rtrep=.f.,left=145, top=497, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutte le Classificazione FE",;
    HelpContextID = 42146522,;
    cFormVar="w_CLASALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLASALL_1_78.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCLASALL_1_78.GetRadio()
    this.Parent.oContained.w_CLASALL = this.RadioValue()
    return .t.
  endfunc

  func oCLASALL_1_78.SetRadio()
    this.Parent.oContained.w_CLASALL=trim(this.Parent.oContained.w_CLASALL)
    this.value = ;
      iif(this.Parent.oContained.w_CLASALL=='S',1,;
      0)
  endfunc

  add object oCLASINI_1_79 as StdField with uid="ECUYOASGDE",rtseq=197,rtrep=.f.,;
    cFormVar = "w_CLASINI", cQueryName = "CLASINI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classificazione FE originaria da sostituire",;
    HelpContextID = 268231974,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=244, Top=497, InputMask=replicate('X',5)

  func oCLASINI_1_79.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CLASALL<>'S' )
    endwith
   endif
  endfunc

  add object oCLASFIN_1_80 as StdField with uid="GACAIFGING",rtseq=198,rtrep=.f.,;
    cFormVar = "w_CLASFIN", cQueryName = "CLASFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classificazione FE che � possibile inserire in sostituzione",;
    HelpContextID = 87235290,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=244, Top=520, InputMask=replicate('X',5)

  add object oGESCSEL_1_84 as StdCheck with uid="PRMBEYAUOK",rtseq=199,rtrep=.f.,left=145, top=572, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce gli Articoli esclusivi, se non specificato sbianca",;
    HelpContextID = 141689242,;
    cFormVar="w_GESCSEL", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oGESCSEL_1_84.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oGESCSEL_1_84.GetRadio()
    this.Parent.oContained.w_GESCSEL = this.RadioValue()
    return .t.
  endfunc

  func oGESCSEL_1_84.SetRadio()
    this.Parent.oContained.w_GESCSEL=trim(this.Parent.oContained.w_GESCSEL)
    this.value = ;
      iif(this.Parent.oContained.w_GESCSEL=='S',1,;
      0)
  endfunc

  add object oGESCALL_1_85 as StdCheck with uid="MISWMELRUM",rtseq=200,rtrep=.f.,left=145, top=550, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutti gli Articoli esclusivi",;
    HelpContextID = 43123098,;
    cFormVar="w_GESCALL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGESCALL_1_85.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oGESCALL_1_85.GetRadio()
    this.Parent.oContained.w_GESCALL = this.RadioValue()
    return .t.
  endfunc

  func oGESCALL_1_85.SetRadio()
    this.Parent.oContained.w_GESCALL=trim(this.Parent.oContained.w_GESCALL)
    this.value = ;
      iif(this.Parent.oContained.w_GESCALL=='S',1,;
      0)
  endfunc

  add object oGESCINI_1_86 as StdCheck with uid="MCDNIPCTJP",rtseq=201,rtrep=.f.,left=244, top=550, caption="Codice articolo esclusivo",;
    ToolTipText = "Flag articolo esclusivo originario da sostituire",;
    HelpContextID = 267255398,;
    cFormVar="w_GESCINI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGESCINI_1_86.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGESCINI_1_86.GetRadio()
    this.Parent.oContained.w_GESCINI = this.RadioValue()
    return .t.
  endfunc

  func oGESCINI_1_86.SetRadio()
    this.Parent.oContained.w_GESCINI=trim(this.Parent.oContained.w_GESCINI)
    this.value = ;
      iif(this.Parent.oContained.w_GESCINI=='S',1,;
      0)
  endfunc

  func oGESCINI_1_86.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GESCALL<>'S' )
    endwith
   endif
  endfunc

  add object oGESCFIN_1_87 as StdCheck with uid="CFUDKJKDQI",rtseq=202,rtrep=.f.,left=244, top=572, caption="Codice articolo esclusivo",;
    ToolTipText = "Flag articolo esclusivo che � possibile inserire in sostituzione",;
    HelpContextID = 88211866,;
    cFormVar="w_GESCFIN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGESCFIN_1_87.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGESCFIN_1_87.GetRadio()
    this.Parent.oContained.w_GESCFIN = this.RadioValue()
    return .t.
  endfunc

  func oGESCFIN_1_87.SetRadio()
    this.Parent.oContained.w_GESCFIN=trim(this.Parent.oContained.w_GESCFIN)
    this.value = ;
      iif(this.Parent.oContained.w_GESCFIN=='S',1,;
      0)
  endfunc

  add object oStr_1_11 as StdString with uid="QBWMDGSZTQ",Visible=.t., Left=6, Top=11,;
    Alignment=1, Width=129, Height=18,;
    Caption="Codici IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="APMRRFETRB",Visible=.t., Left=6, Top=120,;
    Alignment=1, Width=129, Height=18,;
    Caption="Gr.merceologico:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="AHJHQPEEZG",Visible=.t., Left=6, Top=174,;
    Alignment=1, Width=129, Height=18,;
    Caption="Cat.contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="RAGHBAARKE",Visible=.t., Left=6, Top=228,;
    Alignment=1, Width=129, Height=18,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="FMORHGXUOK",Visible=.t., Left=6, Top=282,;
    Alignment=1, Width=129, Height=18,;
    Caption="Cat.omogenee:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="VPUMGAQEOC",Visible=.t., Left=6, Top=66,;
    Alignment=1, Width=129, Height=18,;
    Caption="Codice operazione IVA.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="DHJCXSWEEV",Visible=.t., Left=0, Top=120,;
    Alignment=1, Width=135, Height=18,;
    Caption="Spesa e anticipazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="LMOKHXXIKE",Visible=.t., Left=5, Top=337,;
    Alignment=1, Width=129, Height=18,;
    Caption="Utilizzo intra:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="ZPNTWYYNEK",Visible=.t., Left=5, Top=391,;
    Alignment=1, Width=129, Height=18,;
    Caption="Codice tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="CANRTPJZWJ",Visible=.t., Left=5, Top=447,;
    Alignment=1, Width=129, Height=18,;
    Caption="Codice valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="WCGBIHSVVE",Visible=.t., Left=5, Top=498,;
    Alignment=1, Width=129, Height=18,;
    Caption="Classificazione FE:"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="ZDKVBONILM",Visible=.t., Left=27, Top=549,;
    Alignment=1, Width=107, Height=18,;
    Caption="Articolo esclusivo:"  ;
  , bGlobalFont=.t.

  add object oBox_1_12 as StdBox with uid="TJNEYCGMWS",left=5, top=112, width=627,height=2

  add object oBox_1_20 as StdBox with uid="WUPUBDEDMS",left=5, top=166, width=627,height=2

  add object oBox_1_28 as StdBox with uid="FHWHXJRMRT",left=5, top=221, width=627,height=2

  add object oBox_1_36 as StdBox with uid="JSKTRNGBOW",left=5, top=275, width=627,height=2

  add object oBox_1_45 as StdBox with uid="HPKSGURETV",left=5, top=58, width=627,height=2

  add object oBox_1_60 as StdBox with uid="WIENECEBYY",left=5, top=329, width=627,height=2

  add object oBox_1_64 as StdBox with uid="GTHBRHGPMB",left=5, top=386, width=627,height=2

  add object oBox_1_70 as StdBox with uid="CWTZDTMVTH",left=5, top=439, width=627,height=2

  add object oBox_1_76 as StdBox with uid="KXGYFJMCCQ",left=5, top=494, width=627,height=2

  add object oBox_1_82 as StdBox with uid="RAMVAHKZAN",left=5, top=546, width=627,height=2
enddefine
define class tgsma_kvaPag2 as StdContainer
  Width  = 645
  height = 596
  stdWidth  = 645
  stdheight = 596
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCLASEL_2_1 as StdCheck with uid="WBJXEZBQAG",rtseq=35,rtrep=.f.,left=158, top=45, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce la classe di ricarico, se non specificato sbianca",;
    HelpContextID = 37952218,;
    cFormVar="w_CLASEL", bObbl = .f. , nPag = 2;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oCLASEL_2_1.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCLASEL_2_1.GetRadio()
    this.Parent.oContained.w_CLASEL = this.RadioValue()
    return .t.
  endfunc

  func oCLASEL_2_1.SetRadio()
    this.Parent.oContained.w_CLASEL=trim(this.Parent.oContained.w_CLASEL)
    this.value = ;
      iif(this.Parent.oContained.w_CLASEL=='S',1,;
      0)
  endfunc

  func oCLASEL_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  add object oCLAALL_2_2 as StdCheck with uid="SLQVLTZFEY",rtseq=36,rtrep=.f.,left=158, top=22, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutte le classi di ricarico",;
    HelpContextID = 31791834,;
    cFormVar="w_CLAALL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCLAALL_2_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCLAALL_2_2.GetRadio()
    this.Parent.oContained.w_CLAALL = this.RadioValue()
    return .t.
  endfunc

  func oCLAALL_2_2.SetRadio()
    this.Parent.oContained.w_CLAALL=trim(this.Parent.oContained.w_CLAALL)
    this.value = ;
      iif(this.Parent.oContained.w_CLAALL=='S',1,;
      0)
  endfunc

  func oCLAALL_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  add object oCLAINI_2_3 as StdField with uid="GTTCMQQGLP",rtseq=37,rtrep=.f.,;
    cFormVar = "w_CLAINI", cQueryName = "CLAINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 79502042,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=261, Top=24, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_RICA", cZoomOnZoom="GSAR_ACR", oKey_1_1="CRCODICE", oKey_1_2="this.w_CLAINI"

  func oCLAINI_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CLAALL<>'S' AND .w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oCLAINI_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLAINI_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLAINI_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RICA','*','CRCODICE',cp_AbsName(this.parent,'oCLAINI_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACR',"Classi di ricarico",'',this.parent.oContained
  endproc
  proc oCLAINI_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CRCODICE=this.parent.oContained.w_CLAINI
     i_obj.ecpSave()
  endproc

  add object oDESCLA_2_4 as StdField with uid="DZYJXIVGIA",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 216138186,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=337, Top=24, InputMask=replicate('X',35)

  add object oCLAFIN_2_5 as StdField with uid="FRWZNTCOAQ",rtseq=39,rtrep=.f.,;
    cFormVar = "w_CLAFIN", cQueryName = "CLAFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 1055450,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=261, Top=47, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_RICA", cZoomOnZoom="GSAR_ACR", oKey_1_1="CRCODICE", oKey_1_2="this.w_CLAFIN"

  func oCLAFIN_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oCLAFIN_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLAFIN_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLAFIN_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RICA','*','CRCODICE',cp_AbsName(this.parent,'oCLAFIN_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACR',"Classi di ricarico",'',this.parent.oContained
  endproc
  proc oCLAFIN_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CRCODICE=this.parent.oContained.w_CLAFIN
     i_obj.ecpSave()
  endproc

  add object oDESCLA1_2_6 as StdField with uid="NVWJTFZRZM",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESCLA1", cQueryName = "DESCLA1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 216138186,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=337, Top=47, InputMask=replicate('X',35)

  add object oSCOSEL_2_8 as StdCheck with uid="PBTXWKXTAE",rtseq=41,rtrep=.f.,left=158, top=105, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce la cat.sconti/magg., se non specificato sbianca",;
    HelpContextID = 37896922,;
    cFormVar="w_SCOSEL", bObbl = .f. , nPag = 2;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oSCOSEL_2_8.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSCOSEL_2_8.GetRadio()
    this.Parent.oContained.w_SCOSEL = this.RadioValue()
    return .t.
  endfunc

  func oSCOSEL_2_8.SetRadio()
    this.Parent.oContained.w_SCOSEL=trim(this.Parent.oContained.w_SCOSEL)
    this.value = ;
      iif(this.Parent.oContained.w_SCOSEL=='S',1,;
      0)
  endfunc

  add object oSCOALL_2_9 as StdCheck with uid="WEROWLZXXV",rtseq=42,rtrep=.f.,left=158, top=81, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutte le cat.sconti/magg.",;
    HelpContextID = 31736538,;
    cFormVar="w_SCOALL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oSCOALL_2_9.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSCOALL_2_9.GetRadio()
    this.Parent.oContained.w_SCOALL = this.RadioValue()
    return .t.
  endfunc

  func oSCOALL_2_9.SetRadio()
    this.Parent.oContained.w_SCOALL=trim(this.Parent.oContained.w_SCOALL)
    this.value = ;
      iif(this.Parent.oContained.w_SCOALL=='S',1,;
      0)
  endfunc

  add object oSCOINI_2_10 as StdField with uid="LWREUCGCYI",rtseq=43,rtrep=.f.,;
    cFormVar = "w_SCOINI", cQueryName = "SCOINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 79446746,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=260, Top=81, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_SCMA", cZoomOnZoom="GSAR_ASM", oKey_1_1="CSCODICE", oKey_1_2="this.w_SCOINI"

  func oSCOINI_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SCOALL<>'S')
    endwith
   endif
  endfunc

  func oSCOINI_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCOINI_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCOINI_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_SCMA','*','CSCODICE',cp_AbsName(this.parent,'oSCOINI_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASM',"Cat.sconti/maggiorazioni",'GSAR_SCO.CAT_SCMA_VZM',this.parent.oContained
  endproc
  proc oSCOINI_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCODICE=this.parent.oContained.w_SCOINI
     i_obj.ecpSave()
  endproc

  add object oDESSCO_2_11 as StdField with uid="UNDTLYBOFG",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESSCO", cQueryName = "DESSCO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 258081226,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=81, InputMask=replicate('X',35)

  add object oSCOFIN_2_12 as StdField with uid="OCXLUVDSWX",rtseq=45,rtrep=.f.,;
    cFormVar = "w_SCOFIN", cQueryName = "SCOFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 1000154,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=260, Top=104, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_SCMA", cZoomOnZoom="GSAR_ASM", oKey_1_1="CSCODICE", oKey_1_2="this.w_SCOFIN"

  func oSCOFIN_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCOFIN_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCOFIN_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_SCMA','*','CSCODICE',cp_AbsName(this.parent,'oSCOFIN_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASM',"Cat.sconti/maggiorazioni",'GSAR_SCO.CAT_SCMA_VZM',this.parent.oContained
  endproc
  proc oSCOFIN_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCODICE=this.parent.oContained.w_SCOFIN
     i_obj.ecpSave()
  endproc

  add object oDESSCO1_2_13 as StdField with uid="IDNIXUOMTI",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESSCO1", cQueryName = "DESSCO1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 258081226,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=104, InputMask=replicate('X',35)

  add object oMARSEL_2_15 as StdCheck with uid="HAQFAWNPWM",rtseq=47,rtrep=.f.,left=158, top=162, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce il codice marchio, se non specificato sbianca",;
    HelpContextID = 37885242,;
    cFormVar="w_MARSEL", bObbl = .f. , nPag = 2;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oMARSEL_2_15.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMARSEL_2_15.GetRadio()
    this.Parent.oContained.w_MARSEL = this.RadioValue()
    return .t.
  endfunc

  func oMARSEL_2_15.SetRadio()
    this.Parent.oContained.w_MARSEL=trim(this.Parent.oContained.w_MARSEL)
    this.value = ;
      iif(this.Parent.oContained.w_MARSEL=='S',1,;
      0)
  endfunc

  func oMARSEL_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  add object oMARALL_2_16 as StdCheck with uid="RXOXCJEUGA",rtseq=48,rtrep=.f.,left=158, top=139, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutti i codici marchi",;
    HelpContextID = 31724858,;
    cFormVar="w_MARALL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oMARALL_2_16.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMARALL_2_16.GetRadio()
    this.Parent.oContained.w_MARALL = this.RadioValue()
    return .t.
  endfunc

  func oMARALL_2_16.SetRadio()
    this.Parent.oContained.w_MARALL=trim(this.Parent.oContained.w_MARALL)
    this.value = ;
      iif(this.Parent.oContained.w_MARALL=='S',1,;
      0)
  endfunc

  func oMARALL_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  add object oMARINI_2_17 as StdField with uid="WRVNPPDZQI",rtseq=49,rtrep=.f.,;
    cFormVar = "w_MARINI", cQueryName = "MARINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 79435066,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=260, Top=139, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_MARINI"

  func oMARINI_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MARALL<>'S' AND .w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oMARINI_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oMARINI_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMARINI_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMARINI_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Codici marchi",'',this.parent.oContained
  endproc
  proc oMARINI_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_MARINI
     i_obj.ecpSave()
  endproc

  add object oDESMAR_2_18 as StdField with uid="DNHKXUJHZF",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESMAR", cQueryName = "DESMAR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 210239946,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=139, InputMask=replicate('X',35)

  add object oMARFIN_2_19 as StdField with uid="NJUMXCYMES",rtseq=51,rtrep=.f.,;
    cFormVar = "w_MARFIN", cQueryName = "MARFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 988474,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=260, Top=162, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_MARFIN"

  func oMARFIN_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oMARFIN_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oMARFIN_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMARFIN_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMARFIN_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Codici marchi",'',this.parent.oContained
  endproc
  proc oMARFIN_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_MARFIN
     i_obj.ecpSave()
  endproc

  add object oDESMAR1_2_20 as StdField with uid="SPTMLRAVZM",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DESMAR1", cQueryName = "DESMAR1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 210239946,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=336, Top=162, InputMask=replicate('X',35)

  add object oCAPSEL_2_23 as StdCheck with uid="XWQNOBMRHC",rtseq=53,rtrep=.f.,left=158, top=221, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce la categoria provvigioni, se non specificato sbianca",;
    HelpContextID = 37893594,;
    cFormVar="w_CAPSEL", bObbl = .f. , nPag = 2;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oCAPSEL_2_23.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCAPSEL_2_23.GetRadio()
    this.Parent.oContained.w_CAPSEL = this.RadioValue()
    return .t.
  endfunc

  func oCAPSEL_2_23.SetRadio()
    this.Parent.oContained.w_CAPSEL=trim(this.Parent.oContained.w_CAPSEL)
    this.value = ;
      iif(this.Parent.oContained.w_CAPSEL=='S',1,;
      0)
  endfunc

  add object oCAPALL_2_24 as StdCheck with uid="OLQGCPZFXX",rtseq=54,rtrep=.f.,left=158, top=197, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutte le categorie provvigioni",;
    HelpContextID = 31733210,;
    cFormVar="w_CAPALL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCAPALL_2_24.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCAPALL_2_24.GetRadio()
    this.Parent.oContained.w_CAPALL = this.RadioValue()
    return .t.
  endfunc

  func oCAPALL_2_24.SetRadio()
    this.Parent.oContained.w_CAPALL=trim(this.Parent.oContained.w_CAPALL)
    this.value = ;
      iif(this.Parent.oContained.w_CAPALL=='S',1,;
      0)
  endfunc

  add object oCAPINI_2_25 as StdField with uid="VBWHAXYLGW",rtseq=55,rtrep=.f.,;
    cFormVar = "w_CAPINI", cQueryName = "CAPINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 79443418,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=259, Top=197, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUPRO", cZoomOnZoom="GSAR_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_CAPINI"

  func oCAPINI_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAPALL<>'S')
    endwith
   endif
  endfunc

  func oCAPINI_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAPINI_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAPINI_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPRO','*','GPCODICE',cp_AbsName(this.parent,'oCAPINI_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGP',"Cat.provvigioni",'GSAR_PRO.GRUPRO_VZM',this.parent.oContained
  endproc
  proc oCAPINI_2_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_CAPINI
     i_obj.ecpSave()
  endproc

  add object oDESCAP_2_26 as StdField with uid="GTZCAXTJEX",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DESCAP", cQueryName = "DESCAP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 244449738,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=335, Top=197, InputMask=replicate('X',35)

  add object oCAPFIN_2_27 as StdField with uid="XYXXXFBUEU",rtseq=57,rtrep=.f.,;
    cFormVar = "w_CAPFIN", cQueryName = "CAPFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 996826,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=259, Top=220, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUPRO", cZoomOnZoom="GSAR_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_CAPFIN"

  func oCAPFIN_2_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAPFIN_2_27.ecpDrop(oSource)
    this.Parent.oContained.link_2_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAPFIN_2_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPRO','*','GPCODICE',cp_AbsName(this.parent,'oCAPFIN_2_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGP',"Cat.provvigioni",'GSAR_PRO.GRUPRO_VZM',this.parent.oContained
  endproc
  proc oCAPFIN_2_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_CAPFIN
     i_obj.ecpSave()
  endproc

  add object oDESCAP1_2_28 as StdField with uid="YYGJEVCCIJ",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DESCAP1", cQueryName = "DESCAP1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 244449738,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=335, Top=220, InputMask=replicate('X',35)

  add object oCOLSEL_2_33 as StdCheck with uid="PALCSIUHGV",rtseq=123,rtrep=.f.,left=158, top=280, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce la la tipologia collo di appartenenza, se non specificato sbianca",;
    HelpContextID = 37906394,;
    cFormVar="w_COLSEL", bObbl = .f. , nPag = 2;
    , tabStop=.f.;
   , bGlobalFont=.t.


  func oCOLSEL_2_33.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCOLSEL_2_33.GetRadio()
    this.Parent.oContained.w_COLSEL = this.RadioValue()
    return .t.
  endfunc

  func oCOLSEL_2_33.SetRadio()
    this.Parent.oContained.w_COLSEL=trim(this.Parent.oContained.w_COLSEL)
    this.value = ;
      iif(this.Parent.oContained.w_COLSEL=='S',1,;
      0)
  endfunc

  func oCOLSEL_2_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  add object oCOLALL_2_34 as StdCheck with uid="NEYDZZXEPZ",rtseq=124,rtrep=.f.,left=158, top=256, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutte le tipologie colli",;
    HelpContextID = 31746010,;
    cFormVar="w_COLALL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCOLALL_2_34.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCOLALL_2_34.GetRadio()
    this.Parent.oContained.w_COLALL = this.RadioValue()
    return .t.
  endfunc

  func oCOLALL_2_34.SetRadio()
    this.Parent.oContained.w_COLALL=trim(this.Parent.oContained.w_COLALL)
    this.value = ;
      iif(this.Parent.oContained.w_COLALL=='S',1,;
      0)
  endfunc

  func oCOLALL_2_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  add object oCOLINI_2_35 as StdField with uid="PFSSJQVVDX",rtseq=125,rtrep=.f.,;
    cFormVar = "w_COLINI", cQueryName = "COLINI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice collo originario da sostituire",;
    HelpContextID = 79456218,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=259, Top=256, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_COLINI"

  func oCOLINI_2_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COLALL<>'S' and .w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oCOLINI_2_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOLINI_2_35.ecpDrop(oSource)
    this.Parent.oContained.link_2_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOLINI_2_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_COLL','*','TCCODICE',cp_AbsName(this.parent,'oCOLINI_2_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Tipologie colli",'',this.parent.oContained
  endproc
  proc oCOLINI_2_35.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_COLINI
     i_obj.ecpSave()
  endproc

  add object oDESCOL_2_36 as StdField with uid="UYWVKOEJFL",rtseq=126,rtrep=.f.,;
    cFormVar = "w_DESCOL", cQueryName = "DESCOL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 28443082,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=335, Top=256, InputMask=replicate('X',35)

  add object oCOLFIN_2_37 as StdField with uid="HIWXUUAVQU",rtseq=127,rtrep=.f.,;
    cFormVar = "w_COLFIN", cQueryName = "COLFIN",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice collo che � possibile inserire in sostituzione",;
    HelpContextID = 1009626,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=259, Top=279, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_COLFIN"

  func oCOLFIN_2_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oCOLFIN_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOLFIN_2_37.ecpDrop(oSource)
    this.Parent.oContained.link_2_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOLFIN_2_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_COLL','*','TCCODICE',cp_AbsName(this.parent,'oCOLFIN_2_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Tipologie colli",'',this.parent.oContained
  endproc
  proc oCOLFIN_2_37.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_COLFIN
     i_obj.ecpSave()
  endproc

  add object oDESCOL1_2_38 as StdField with uid="FYKVZZTUCS",rtseq=128,rtrep=.f.,;
    cFormVar = "w_DESCOL1", cQueryName = "DESCOL1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 28443082,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=335, Top=279, InputMask=replicate('X',35)

  add object oCONDEF_2_42 as StdField with uid="CHACYQZDBQ",rtseq=130,rtrep=.f.,;
    cFormVar = "w_CONDEF", cQueryName = "CONDEF",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice confezione da inserire assieme al collo in caso di articolo senza confezione",;
    HelpContextID = 139544538,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=259, Top=302, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="TIPICONF", cZoomOnZoom="GSAR_ATC", oKey_1_1="TCCODICE", oKey_1_2="this.w_CONDEF"

  func oCONDEF_2_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oCONDEF_2_42.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_COLFIN) OR .w_COLSEL<>'S')
    endwith
  endfunc

  func oCONDEF_2_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONDEF_2_42.ecpDrop(oSource)
    this.Parent.oContained.link_2_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONDEF_2_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPICONF','*','TCCODICE',cp_AbsName(this.parent,'oCONDEF_2_42'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATC',"Codici tipi confezioni",'GSMA_ACF.TIPICONF_VZM',this.parent.oContained
  endproc
  proc oCONDEF_2_42.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_CONDEF
     i_obj.ecpSave()
  endproc

  add object osQTAMIN_2_43 as StdCheck with uid="TIHIWDMYPH",rtseq=131,rtrep=.f.,left=17, top=338, caption="Qt� minima vendibile",;
    ToolTipText = "Se attivo: aggiorna la qt� minima vendibile ",;
    HelpContextID = 80995034,;
    cFormVar="w_sQTAMIN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func osQTAMIN_2_43.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func osQTAMIN_2_43.GetRadio()
    this.Parent.oContained.w_sQTAMIN = this.RadioValue()
    return .t.
  endfunc

  func osQTAMIN_2_43.SetRadio()
    this.Parent.oContained.w_sQTAMIN=trim(this.Parent.oContained.w_sQTAMIN)
    this.value = ;
      iif(this.Parent.oContained.w_sQTAMIN=='S',1,;
      0)
  endfunc

  add object oUMPRI_2_44 as StdField with uid="TUAHYXNNKZ",rtseq=132,rtrep=.f.,;
    cFormVar = "w_UMPRI", cQueryName = "UMPRI",;
    bObbl = .t. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Filtro che applica il criterio di aggiornamento della qt� minima vendibile solo agli articoli con l'UM primaria selezionata ",;
    HelpContextID = 235088058,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=259, Top=338, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", oKey_1_1="UMCODICE", oKey_1_2="this.w_UMPRI"

  func oUMPRI_2_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_sQTAMIN='S')
    endwith
   endif
  endfunc

  func oUMPRI_2_44.mHide()
    with this.Parent.oContained
      return (.w_sQTAMIN<>'S')
    endwith
  endfunc

  func oUMPRI_2_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oUMPRI_2_44.ecpDrop(oSource)
    this.Parent.oContained.link_2_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUMPRI_2_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oUMPRI_2_44'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Unit� di misura",'',this.parent.oContained
  endproc

  add object oDESCOF_2_46 as StdField with uid="YECXSAOKGV",rtseq=134,rtrep=.f.,;
    cFormVar = "w_DESCOF", cQueryName = "DESCOF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 129106378,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=335, Top=302, InputMask=replicate('X',35)

  func oDESCOF_2_46.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_COLFIN) OR .w_COLSEL<>'S')
    endwith
  endfunc

  add object ocMINVEN_2_47 as StdField with uid="GDNTBICNDO",rtseq=135,rtrep=.f.,;
    cFormVar = "w_cMINVEN", cQueryName = "cMINVEN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'unit� di misura selezionata non � frazionabile",;
    ToolTipText = "Quantitativo minimo vendibile espresso nell'unit� di misura primaria",;
    HelpContextID = 137861082,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=335, Top=338, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func ocMINVEN_2_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_sQTAMIN='S')
    endwith
   endif
  endfunc

  func ocMINVEN_2_47.mHide()
    with this.Parent.oContained
      return (.w_sQTAMIN<>'S')
    endwith
  endfunc

  func ocMINVEN_2_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_FLFRAZ<>'S' Or .w_cMINVEN =Int(.w_cMINVEN))
    endwith
    return bRes
  endfunc


  add object oPUBINI_2_48 as StdCombo with uid="NXPMHYWKWI",rtseq=136,rtrep=.f.,left=158,top=386,width=150,height=21;
    , ToolTipText = "Pubblica prodotto su web da sostituire nell'articolo";
    , HelpContextID = 79495434;
    , cFormVar="w_PUBINI",RowSource=""+"Pubblica su web,"+"Non pubblicare su web,"+"Trasferisci su web", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPUBINI_2_48.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oPUBINI_2_48.GetRadio()
    this.Parent.oContained.w_PUBINI = this.RadioValue()
    return .t.
  endfunc

  func oPUBINI_2_48.SetRadio()
    this.Parent.oContained.w_PUBINI=trim(this.Parent.oContained.w_PUBINI)
    this.value = ;
      iif(this.Parent.oContained.w_PUBINI=='S',1,;
      iif(this.Parent.oContained.w_PUBINI=='N',2,;
      iif(this.Parent.oContained.w_PUBINI=='T',3,;
      0)))
  endfunc

  func oPUBINI_2_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_IZCP $ 'SA' OR g_CPIN = 'S' Or g_REVICRM = 'S')
    endwith
   endif
  endfunc

  func oPUBINI_2_48.mHide()
    with this.Parent.oContained
      return (g_IZCP = 'N' AND g_CPIN = 'N' And g_REVICRM<>'S')
    endwith
  endfunc

  add object oPUBSEL_2_49 as StdCheck with uid="CGCKJZAXSV",rtseq=137,rtrep=.f.,left=316, top=385, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce la pubblicazione su web",;
    HelpContextID = 37945610,;
    cFormVar="w_PUBSEL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPUBSEL_2_49.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPUBSEL_2_49.GetRadio()
    this.Parent.oContained.w_PUBSEL = this.RadioValue()
    return .t.
  endfunc

  func oPUBSEL_2_49.SetRadio()
    this.Parent.oContained.w_PUBSEL=trim(this.Parent.oContained.w_PUBSEL)
    this.value = ;
      iif(this.Parent.oContained.w_PUBSEL=='S',1,;
      0)
  endfunc

  func oPUBSEL_2_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_IZCP $ 'SA' OR g_CPIN='S' Or g_REVICRM='S')
    endwith
   endif
  endfunc

  func oPUBSEL_2_49.mHide()
    with this.Parent.oContained
      return (g_IZCP = 'N' AND g_CPIN = 'N' And g_REVICRM<>'S')
    endwith
  endfunc


  add object oPUBFIN_2_50 as StdCombo with uid="ERUMELTSPJ",rtseq=138,rtrep=.f.,left=419,top=386,width=150,height=21;
    , ToolTipText = "Pubblica prodotto su web in sostituzione nell'articolo";
    , HelpContextID = 1048842;
    , cFormVar="w_PUBFIN",RowSource=""+"Pubblica su web,"+"Non pubblicare su web,"+"Trasferisci su web", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPUBFIN_2_50.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oPUBFIN_2_50.GetRadio()
    this.Parent.oContained.w_PUBFIN = this.RadioValue()
    return .t.
  endfunc

  func oPUBFIN_2_50.SetRadio()
    this.Parent.oContained.w_PUBFIN=trim(this.Parent.oContained.w_PUBFIN)
    this.value = ;
      iif(this.Parent.oContained.w_PUBFIN=='S',1,;
      iif(this.Parent.oContained.w_PUBFIN=='N',2,;
      iif(this.Parent.oContained.w_PUBFIN=='T',3,;
      0)))
  endfunc

  func oPUBFIN_2_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_IZCP $ 'SA' AND g_CPIN#'S') OR (.w_PUBINI='N' and g_CPIN='S') Or g_REVICRM='S')
    endwith
   endif
  endfunc

  func oPUBFIN_2_50.mHide()
    with this.Parent.oContained
      return (g_IZCP = 'N' AND g_CPIN='N' And g_REVICRM<>'S')
    endwith
  endfunc

  add object oCOSSEL_2_52 as StdCheck with uid="GPPJVIRSCW",rtseq=139,rtrep=.f.,left=17, top=430, caption="Ultimo costo standard",;
    ToolTipText = "Se attivo: sostituisce costo standard con la tipologia indicata",;
    HelpContextID = 37877722,;
    cFormVar="w_COSSEL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCOSSEL_2_52.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCOSSEL_2_52.GetRadio()
    this.Parent.oContained.w_COSSEL = this.RadioValue()
    return .t.
  endfunc

  func oCOSSEL_2_52.SetRadio()
    this.Parent.oContained.w_COSSEL=trim(this.Parent.oContained.w_COSSEL)
    this.value = ;
      iif(this.Parent.oContained.w_COSSEL=='S',1,;
      0)
  endfunc

  func oCOSSEL_2_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc


  add object oCOSALL_2_53 as StdCombo with uid="JHNGMBMLIZ",rtseq=140,rtrep=.f.,left=197,top=430,width=83,height=21;
    , ToolTipText = "Tipologie di articoli su cui valorizzare ultimo costo standard";
    , HelpContextID = 31717338;
    , cFormVar="w_COSALL",RowSource=""+"Tutti,"+"Solo a zero", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oCOSALL_2_53.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'Z',;
    ' ')))
  endfunc
  func oCOSALL_2_53.GetRadio()
    this.Parent.oContained.w_COSALL = this.RadioValue()
    return .t.
  endfunc

  func oCOSALL_2_53.SetRadio()
    this.Parent.oContained.w_COSALL=trim(this.Parent.oContained.w_COSALL)
    this.value = ;
      iif(this.Parent.oContained.w_COSALL=='T',1,;
      iif(this.Parent.oContained.w_COSALL=='Z',2,;
      0))
  endfunc

  func oCOSALL_2_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COSSEL='S')
    endwith
   endif
  endfunc

  func oCOSALL_2_53.mHide()
    with this.Parent.oContained
      return (.w_COSSEL<>'S')
    endwith
  endfunc


  add object oDISTIN_2_54 as StdCombo with uid="LWMUPWLWDL",rtseq=141,rtrep=.f.,left=284,top=430,width=122,height=21;
    , ToolTipText = "Valorizzazione ulrtimo costo standard per prodotti finiti\materie prime";
    , HelpContextID = 64970;
    , cFormVar="w_DISTIN",RowSource=""+"Tutti,"+"Con distinta\kit,"+"Senza distinta\kit", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oDISTIN_2_54.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    iif(this.value =3,'S',;
    ' '))))
  endfunc
  func oDISTIN_2_54.GetRadio()
    this.Parent.oContained.w_DISTIN = this.RadioValue()
    return .t.
  endfunc

  func oDISTIN_2_54.SetRadio()
    this.Parent.oContained.w_DISTIN=trim(this.Parent.oContained.w_DISTIN)
    this.value = ;
      iif(this.Parent.oContained.w_DISTIN=='T',1,;
      iif(this.Parent.oContained.w_DISTIN=='D',2,;
      iif(this.Parent.oContained.w_DISTIN=='S',3,;
      0)))
  endfunc

  func oDISTIN_2_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COSSEL='S')
    endwith
   endif
  endfunc

  func oDISTIN_2_54.mHide()
    with this.Parent.oContained
      return (.w_COSSEL<>'S' OR g_EACD<>'S')
    endwith
  endfunc


  add object oAggiorn_2_55 as StdCombo with uid="SVYFDQOGHY",rtseq=142,rtrep=.f.,left=408,top=430,width=217,height=21;
    , ToolTipText = "Criterio di valorizzazione degli articoli di magazzino";
    , HelpContextID = 160079866;
    , cFormVar="w_Aggiorn",RowSource=""+"Costo medio ponderato per esercizio,"+"Costo medio ponderato per periodo,"+"Costo ultimo,"+"Costo standard,"+"LIFO continuo,"+"FIFO continuo,"+"LIFO scatti,"+"Ultimo costo standard,"+"Listino,"+"Ultimo prezzo,"+"Prezzo medio ponderato per esercizio,"+"Prezzo medio ponderato per periodo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAggiorn_2_55.RadioValue()
    return(iif(this.value =1,'CA',;
    iif(this.value =2,'CP',;
    iif(this.value =3,'CU',;
    iif(this.value =4,'CS',;
    iif(this.value =5,'LC',;
    iif(this.value =6,'FC',;
    iif(this.value =7,'LS',;
    iif(this.value =8,'US',;
    iif(this.value =9,'LI',;
    iif(this.value =10,'UP',;
    iif(this.value =11,'PA',;
    iif(this.value =12,'PP',;
    space(2))))))))))))))
  endfunc
  func oAggiorn_2_55.GetRadio()
    this.Parent.oContained.w_Aggiorn = this.RadioValue()
    return .t.
  endfunc

  func oAggiorn_2_55.SetRadio()
    this.Parent.oContained.w_Aggiorn=trim(this.Parent.oContained.w_Aggiorn)
    this.value = ;
      iif(this.Parent.oContained.w_Aggiorn=='CA',1,;
      iif(this.Parent.oContained.w_Aggiorn=='CP',2,;
      iif(this.Parent.oContained.w_Aggiorn=='CU',3,;
      iif(this.Parent.oContained.w_Aggiorn=='CS',4,;
      iif(this.Parent.oContained.w_Aggiorn=='LC',5,;
      iif(this.Parent.oContained.w_Aggiorn=='FC',6,;
      iif(this.Parent.oContained.w_Aggiorn=='LS',7,;
      iif(this.Parent.oContained.w_Aggiorn=='US',8,;
      iif(this.Parent.oContained.w_Aggiorn=='LI',9,;
      iif(this.Parent.oContained.w_Aggiorn=='UP',10,;
      iif(this.Parent.oContained.w_Aggiorn=='PA',11,;
      iif(this.Parent.oContained.w_Aggiorn=='PP',12,;
      0))))))))))))
  endfunc

  func oAggiorn_2_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COSSEL='S')
    endwith
   endif
  endfunc

  func oAggiorn_2_55.mHide()
    with this.Parent.oContained
      return (.w_COSSEL<>'S')
    endwith
  endfunc

  add object oLISTRIF_2_57 as StdField with uid="RKRXOVLQTW",rtseq=144,rtrep=.f.,;
    cFormVar = "w_LISTRIF", cQueryName = "LISTRIF",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino di riferimento ineistente o obsoleto",;
    ToolTipText = "Codice di listino di riferimento",;
    HelpContextID = 193921718,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=98, Top=456, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_LISTRIF"

  func oLISTRIF_2_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGIORN='LI'  AND .w_COSSEL='S')
    endwith
   endif
  endfunc

  func oLISTRIF_2_57.mHide()
    with this.Parent.oContained
      return (.w_AGGIORN<>'LI' OR .w_COSSEL<>'S')
    endwith
  endfunc

  func oLISTRIF_2_57.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_57('Part',this)
    endwith
    return bRes
  endfunc

  proc oLISTRIF_2_57.ecpDrop(oSource)
    this.Parent.oContained.link_2_57('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLISTRIF_2_57.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oLISTRIF_2_57'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Anagrafica listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oLISTRIF_2_57.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_LISTRIF
     i_obj.ecpSave()
  endproc

  add object oSIMVAL_2_59 as StdField with uid="BLOPAYHDVH",rtseq=146,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 41901274,;
   bGlobalFont=.t.,;
    Height=21, Width=66, Left=223, Top=456, InputMask=replicate('X',5)

  func oSIMVAL_2_59.mHide()
    with this.Parent.oContained
      return (.w_AGGIORN<>'LI' OR .w_COSSEL<>'S')
    endwith
  endfunc

  add object oESERCI_2_66 as StdField with uid="RUJJMDHBYY",rtseq=150,rtrep=.f.,;
    cFormVar = "w_ESERCI", cQueryName = "ESERCI",;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio relativo all'inventario da considerare come valorizzazione iniziale",;
    HelpContextID = 90428346,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=112, Top=456, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESERCI"

  func oESERCI_2_66.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGIORN<>'LI' AND .w_AGGIORN<>'US' AND .w_COSSEL='S')
    endwith
   endif
  endfunc

  func oESERCI_2_66.mHide()
    with this.Parent.oContained
      return (.w_AGGIORN='LI' OR .w_AGGIORN='US' OR .w_COSSEL<>'S')
    endwith
  endfunc

  func oESERCI_2_66.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_66('Part',this)
      if .not. empty(.w_NUMINV)
        bRes2=.link_2_67('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oESERCI_2_66.ecpDrop(oSource)
    this.Parent.oContained.link_2_66('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESERCI_2_66.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESERCI_2_66'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oNUMINV_2_67 as StdField with uid="HAHVFRFFYG",rtseq=151,rtrep=.f.,;
    cFormVar = "w_NUMINV", cQueryName = "NUMINV",nZero=6,;
    bObbl = .f. , nPag = 2, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero dell'inventario da visualizzare",;
    HelpContextID = 129782058,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=223, Top=456, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="gsma_ain", oKey_1_1="INCODESE", oKey_1_2="this.w_ESERCI", oKey_2_1="INNUMINV", oKey_2_2="this.w_NUMINV"

  func oNUMINV_2_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGIORN<>'LI' AND .w_AGGIORN<>'US' AND .w_COSSEL='S')
    endwith
   endif
  endfunc

  func oNUMINV_2_67.mHide()
    with this.Parent.oContained
      return (.w_AGGIORN='LI' OR .w_AGGIORN='US' OR .w_COSSEL<>'S')
    endwith
  endfunc

  func oNUMINV_2_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_67('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMINV_2_67.ecpDrop(oSource)
    this.Parent.oContained.link_2_67('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMINV_2_67.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_ESERCI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_ESERCI)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oNUMINV_2_67'),iif(empty(i_cWhere),.f.,i_cWhere),'gsma_ain',"Inventari",'',this.parent.oContained
  endproc
  proc oNUMINV_2_67.mZoomOnZoom
    local i_obj
    i_obj=gsma_ain()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_ESERCI
     i_obj.w_INNUMINV=this.parent.oContained.w_NUMINV
     i_obj.ecpSave()
  endproc

  add object oCAMBIO_2_68 as StdField with uid="DONDGXBPVQ",rtseq=152,rtrep=.f.,;
    cFormVar = "w_CAMBIO", cQueryName = "CAMBIO",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il cambio di riferimento",;
    ToolTipText = "Cambio della valuta del listino selezionato",;
    HelpContextID = 252929498,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=358, Top=456, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oCAMBIO_2_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOLIS=0)
    endwith
   endif
  endfunc

  func oCAMBIO_2_68.mHide()
    with this.Parent.oContained
      return ( .w_AGGIORN <>'LI' OR .w_COSSEL<>'S' OR EMPTY(.w_LISTRIF) OR .w_CAOLIS<>0)
    endwith
  endfunc

  add object oRICPE_2_70 as StdField with uid="JCWLSRROBT",rtseq=153,rtrep=.f.,;
    cFormVar = "w_RICPE", cQueryName = "RICPE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di ricalcolo del costo standard",;
    HelpContextID = 239467754,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=567, Top=456, cSayPict='"999.99"', cGetPict='"999.99"'

  func oRICPE_2_70.mHide()
    with this.Parent.oContained
      return (.w_COSSEL<>'S')
    endwith
  endfunc


  add object oFLGCAC_2_79 as StdCombo with uid="MKYXXVYIRF",rtseq=171,rtrep=.f.,left=158,top=501,width=258,height=21;
    , ToolTipText = "Inserisce/toglie flag contributi accessori in anagrafica articoli";
    , HelpContextID = 194165418;
    , cFormVar="w_FLGCAC",RowSource=""+"Nessuna modifica,"+"Attiva flag contributi accessori,"+"Disattiva flag contributi accessori", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFLGCAC_2_79.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oFLGCAC_2_79.GetRadio()
    this.Parent.oContained.w_FLGCAC = this.RadioValue()
    return .t.
  endfunc

  func oFLGCAC_2_79.SetRadio()
    this.Parent.oContained.w_FLGCAC=trim(this.Parent.oContained.w_FLGCAC)
    this.value = ;
      iif(this.Parent.oContained.w_FLGCAC=='X',1,;
      iif(this.Parent.oContained.w_FLGCAC=='S',2,;
      iif(this.Parent.oContained.w_FLGCAC=='N',3,;
      0)))
  endfunc

  func oFLGCAC_2_79.mHide()
    with this.Parent.oContained
      return (G_COAC = 'N')
    endwith
  endfunc

  add object ocMCTIPCAT_2_83 as StdField with uid="DZVGPXKIMR",rtseq=172,rtrep=.f.,;
    cFormVar = "w_cMCTIPCAT", cQueryName = "cMCTIPCAT",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Tipo contributo",;
    HelpContextID = 33426855,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=157, Top=526, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="TIPCONTR", cZoomOnZoom="GSAR_ATP", oKey_1_1="TPCODICE", oKey_1_2="this.w_cMCTIPCAT"

  func ocMCTIPCAT_2_83.mHide()
    with this.Parent.oContained
      return (.w_FLGCAC <> 'S')
    endwith
  endfunc

  func ocMCTIPCAT_2_83.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_83('Part',this)
      if .not. empty(.w_cMCCODCAT)
        bRes2=.link_2_84('Full')
      endif
    endwith
    return bRes
  endfunc

  proc ocMCTIPCAT_2_83.ecpDrop(oSource)
    this.Parent.oContained.link_2_83('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ocMCTIPCAT_2_83.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPCONTR','*','TPCODICE',cp_AbsName(this.parent,'ocMCTIPCAT_2_83'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATP',"Tipi contributi",'',this.parent.oContained
  endproc
  proc ocMCTIPCAT_2_83.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TPCODICE=this.parent.oContained.w_cMCTIPCAT
     i_obj.ecpSave()
  endproc

  add object ocMCCODCAT_2_84 as StdField with uid="MOOIMATYXB",rtseq=173,rtrep=.f.,;
    cFormVar = "w_cMCCODCAT", cQueryName = "cMCCODCAT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria contributo",;
    HelpContextID = 105713063,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=157, Top=552, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATMCONT", cZoomOnZoom="GSAR_MCT", oKey_1_1="CGCONTRI", oKey_1_2="this.w_cMCTIPCAT", oKey_2_1="CGCODICE", oKey_2_2="this.w_cMCCODCAT"

  func ocMCCODCAT_2_84.mHide()
    with this.Parent.oContained
      return (.w_FLGCAC <> 'S')
    endwith
  endfunc

  func ocMCCODCAT_2_84.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_84('Part',this)
    endwith
    return bRes
  endfunc

  proc ocMCCODCAT_2_84.ecpDrop(oSource)
    this.Parent.oContained.link_2_84('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ocMCCODCAT_2_84.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CATMCONT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CGCONTRI="+cp_ToStrODBC(this.Parent.oContained.w_cMCTIPCAT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CGCONTRI="+cp_ToStr(this.Parent.oContained.w_cMCTIPCAT)
    endif
    do cp_zoom with 'CATMCONT','*','CGCONTRI,CGCODICE',cp_AbsName(this.parent,'ocMCCODCAT_2_84'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MCT',"Categorie contributo",'',this.parent.oContained
  endproc
  proc ocMCCODCAT_2_84.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MCT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CGCONTRI=w_cMCTIPCAT
     i_obj.w_CGCODICE=this.parent.oContained.w_cMCCODCAT
     i_obj.ecpSave()
  endproc

  add object oTPDESCRI_2_85 as StdField with uid="OBHGKVCTEC",rtseq=174,rtrep=.f.,;
    cFormVar = "w_TPDESCRI", cQueryName = "TPDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 175170945,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=281, Top=526, InputMask=replicate('X',40)

  func oTPDESCRI_2_85.mHide()
    with this.Parent.oContained
      return (.w_FLGCAC <> 'S')
    endwith
  endfunc

  add object oCGDESCRI_2_86 as StdField with uid="KNDWHCEWLR",rtseq=175,rtrep=.f.,;
    cFormVar = "w_CGDESCRI", cQueryName = "CGDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 175173521,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=281, Top=553, InputMask=replicate('X',40)

  func oCGDESCRI_2_86.mHide()
    with this.Parent.oContained
      return (.w_FLGCAC <> 'S')
    endwith
  endfunc

  add object oStr_2_21 as StdString with uid="UDFNICGYXQ",Visible=.t., Left=46, Top=139,;
    Alignment=1, Width=100, Height=18,;
    Caption="Marche:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="CTPVOPJLUR",Visible=.t., Left=13, Top=197,;
    Alignment=1, Width=133, Height=18,;
    Caption="Categ.provvigioni:"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="ANEEAZFTZN",Visible=.t., Left=39, Top=256,;
    Alignment=1, Width=107, Height=18,;
    Caption="Tipologie colli:"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="UPHUAKLOGQ",Visible=.t., Left=137, Top=305,;
    Alignment=1, Width=120, Height=18,;
    Caption="Confezione di default:"  ;
  , bGlobalFont=.t.

  func oStr_2_41.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_COLFIN) OR .w_COLSEL<>'S')
    endwith
  endfunc

  add object oStr_2_60 as StdString with uid="NYHQXBQHJL",Visible=.t., Left=458, Top=457,;
    Alignment=1, Width=108, Height=18,;
    Caption="Percentuale rical.:"  ;
  , bGlobalFont=.t.

  func oStr_2_60.mHide()
    with this.Parent.oContained
      return (.w_COSSEL<>'S')
    endwith
  endfunc

  add object oStr_2_62 as StdString with uid="HCPWSSBCUA",Visible=.t., Left=37, Top=25,;
    Alignment=1, Width=109, Height=18,;
    Caption="Classi di ricarico:"  ;
  , bGlobalFont=.t.

  add object oStr_2_63 as StdString with uid="YIDUYKTHVQ",Visible=.t., Left=38, Top=83,;
    Alignment=1, Width=108, Height=18,;
    Caption="Cat.sconti/magg.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_69 as StdString with uid="FITZNTKILM",Visible=.t., Left=41, Top=457,;
    Alignment=1, Width=66, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_2_69.mHide()
    with this.Parent.oContained
      return (.w_AGGIORN='LI' OR .w_AGGIORN='US' OR .w_COSSEL<>'S')
    endwith
  endfunc

  add object oStr_2_71 as StdString with uid="OCZLBOGXAO",Visible=.t., Left=174, Top=456,;
    Alignment=1, Width=48, Height=14,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  func oStr_2_71.mHide()
    with this.Parent.oContained
      return (.w_AGGIORN='LI' OR .w_AGGIORN='US' OR .w_COSSEL<>'S')
    endwith
  endfunc

  add object oStr_2_72 as StdString with uid="NLERSCDRCL",Visible=.t., Left=46, Top=457,;
    Alignment=1, Width=50, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  func oStr_2_72.mHide()
    with this.Parent.oContained
      return (.w_AGGIORN <>'LI' OR .w_COSSEL<>'S')
    endwith
  endfunc

  add object oStr_2_74 as StdString with uid="ZYWZKPXJGT",Visible=.t., Left=171, Top=457,;
    Alignment=1, Width=50, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  func oStr_2_74.mHide()
    with this.Parent.oContained
      return (.w_AGGIORN <>'LI' OR .w_COSSEL<>'S')
    endwith
  endfunc

  add object oStr_2_75 as StdString with uid="AOVRYNIMRS",Visible=.t., Left=290, Top=457,;
    Alignment=1, Width=65, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_2_75.mHide()
    with this.Parent.oContained
      return (.w_AGGIORN <>'LI' OR .w_COSSEL<>'S'  OR EMPTY(.w_LISTRIF) OR .w_CAOLIS<>0)
    endwith
  endfunc

  add object oStr_2_77 as StdString with uid="ZHEJYYVTNX",Visible=.t., Left=10, Top=386,;
    Alignment=1, Width=145, Height=18,;
    Caption="Web application:"  ;
  , bGlobalFont=.t.

  func oStr_2_77.mHide()
    with this.Parent.oContained
      return (g_IZCP = 'N' AND g_CPIN = 'N' And g_REVICRM<>'S')
    endwith
  endfunc

  add object oStr_2_80 as StdString with uid="FVDCGBYJIY",Visible=.t., Left=4, Top=502,;
    Alignment=1, Width=147, Height=18,;
    Caption="Flag contributi accessori:"  ;
  , bGlobalFont=.t.

  func oStr_2_80.mHide()
    with this.Parent.oContained
      return (G_COAC = 'N')
    endwith
  endfunc

  add object oStr_2_81 as StdString with uid="XAIUGJPPXU",Visible=.t., Left=68, Top=530,;
    Alignment=1, Width=84, Height=18,;
    Caption="Tipo contributo:"  ;
  , bGlobalFont=.t.

  func oStr_2_81.mHide()
    with this.Parent.oContained
      return (.w_FLGCAC <> 'S')
    endwith
  endfunc

  add object oStr_2_82 as StdString with uid="NWJNKVTFZK",Visible=.t., Left=38, Top=554,;
    Alignment=1, Width=114, Height=18,;
    Caption="Categoria contributo:"  ;
  , bGlobalFont=.t.

  func oStr_2_82.mHide()
    with this.Parent.oContained
      return (.w_FLGCAC <> 'S')
    endwith
  endfunc

  add object oStr_2_87 as StdString with uid="GAJWYYUTVO",Visible=.t., Left=180, Top=340,;
    Alignment=1, Width=77, Height=18,;
    Caption="Con la 1a UM:"  ;
  , bGlobalFont=.t.

  func oStr_2_87.mHide()
    with this.Parent.oContained
      return (.w_sQTAMIN<>'S')
    endwith
  endfunc

  add object oBox_2_7 as StdBox with uid="INMVDPWMPU",left=5, top=74, width=625,height=2

  add object oBox_2_14 as StdBox with uid="LRSZGSYBNE",left=4, top=131, width=625,height=2

  add object oBox_2_22 as StdBox with uid="KKSQBGKSPL",left=4, top=189, width=625,height=2

  add object oBox_2_32 as StdBox with uid="JGLWYEGLFU",left=4, top=248, width=625,height=2

  add object oBox_2_51 as StdBox with uid="THZYNASJZX",left=4, top=422, width=625,height=2

  add object oBox_2_76 as StdBox with uid="HOHQMLJNUV",left=4, top=329, width=625,height=2

  add object oBox_2_78 as StdBox with uid="UXBLBELRXM",left=5, top=490, width=625,height=2

  add object oBox_2_88 as StdBox with uid="EKMZIKFIPY",left=4, top=372, width=625,height=2
enddefine
define class tgsma_kvaPag3 as StdContainer
  Width  = 645
  height = 596
  stdWidth  = 645
  stdheight = 596
  resizeXpos=552
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVOCSEL_3_1 as StdCheck with uid="IPAQEVGTIC",rtseq=59,rtrep=.f.,left=132, top=50, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce la voce di costo, se non specificato sbianca",;
    HelpContextID = 37942954,;
    cFormVar="w_VOCSEL", bObbl = .f. , nPag = 3;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oVOCSEL_3_1.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVOCSEL_3_1.GetRadio()
    this.Parent.oContained.w_VOCSEL = this.RadioValue()
    return .t.
  endfunc

  func oVOCSEL_3_1.SetRadio()
    this.Parent.oContained.w_VOCSEL=trim(this.Parent.oContained.w_VOCSEL)
    this.value = ;
      iif(this.Parent.oContained.w_VOCSEL=='S',1,;
      0)
  endfunc

  func oVOCSEL_3_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' OR g_COMM='S')
    endwith
   endif
  endfunc

  add object oVOCALL_3_2 as StdCheck with uid="FWSMPWOWJW",rtseq=60,rtrep=.f.,left=132, top=27, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutte le voci di costo",;
    HelpContextID = 31782570,;
    cFormVar="w_VOCALL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVOCALL_3_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVOCALL_3_2.GetRadio()
    this.Parent.oContained.w_VOCALL = this.RadioValue()
    return .t.
  endfunc

  func oVOCALL_3_2.SetRadio()
    this.Parent.oContained.w_VOCALL=trim(this.Parent.oContained.w_VOCALL)
    this.value = ;
      iif(this.Parent.oContained.w_VOCALL=='S',1,;
      0)
  endfunc

  func oVOCALL_3_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' OR g_COMM='S')
    endwith
   endif
  endfunc

  add object oVOCINI_3_3 as StdField with uid="AFUBUBFJVV",rtseq=61,rtrep=.f.,;
    cFormVar = "w_VOCINI", cQueryName = "VOCINI",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Voce di costo incongruente o obsoleta",;
    HelpContextID = 79492778,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=232, Top=27, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCTIPVOC", oKey_1_2="this.w_TIPVOC", oKey_2_1="VCCODICE", oKey_2_2="this.w_VOCINI"

  func oVOCINI_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCCR='S' OR g_COMM='S') AND .w_VOCALL<>'S')
    endwith
   endif
  endfunc

  func oVOCINI_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oVOCINI_3_3.ecpDrop(oSource)
    this.Parent.oContained.link_3_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVOCINI_3_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VOC_COST_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VCTIPVOC="+cp_ToStrODBC(this.Parent.oContained.w_TIPVOC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VCTIPVOC="+cp_ToStr(this.Parent.oContained.w_TIPVOC)
    endif
    do cp_zoom with 'VOC_COST','*','VCTIPVOC,VCCODICE',cp_AbsName(this.parent,'oVOCINI_3_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo",'',this.parent.oContained
  endproc
  proc oVOCINI_3_3.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.VCTIPVOC=w_TIPVOC
     i_obj.w_VCCODICE=this.parent.oContained.w_VOCINI
     i_obj.ecpSave()
  endproc

  add object oDESVOC_3_4 as StdField with uid="ADMZWYKDLT",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESVOC", cQueryName = "DESVOC",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 178192842,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=359, Top=27, InputMask=replicate('X',35)

  add object oVOCFIN_3_5 as StdField with uid="AOIMOBNCJI",rtseq=63,rtrep=.f.,;
    cFormVar = "w_VOCFIN", cQueryName = "VOCFIN",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Voce di costo incongruente o obsoleta",;
    HelpContextID = 1046186,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=232, Top=51, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCTIPVOC", oKey_1_2="this.w_TIPVOC", oKey_2_1="VCCODICE", oKey_2_2="this.w_VOCFIN"

  func oVOCFIN_3_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' OR g_COMM='S')
    endwith
   endif
  endfunc

  func oVOCFIN_3_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oVOCFIN_3_5.ecpDrop(oSource)
    this.Parent.oContained.link_3_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVOCFIN_3_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VOC_COST_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VCTIPVOC="+cp_ToStrODBC(this.Parent.oContained.w_TIPVOC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VCTIPVOC="+cp_ToStr(this.Parent.oContained.w_TIPVOC)
    endif
    do cp_zoom with 'VOC_COST','*','VCTIPVOC,VCCODICE',cp_AbsName(this.parent,'oVOCFIN_3_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo",'',this.parent.oContained
  endproc
  proc oVOCFIN_3_5.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.VCTIPVOC=w_TIPVOC
     i_obj.w_VCCODICE=this.parent.oContained.w_VOCFIN
     i_obj.ecpSave()
  endproc

  add object oDESVOC1_3_6 as StdField with uid="UNMTFMVPHC",rtseq=64,rtrep=.f.,;
    cFormVar = "w_DESVOC1", cQueryName = "DESVOC1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 178192842,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=359, Top=51, InputMask=replicate('X',35)

  add object oVORSEL_3_9 as StdCheck with uid="SRHZJEHUDA",rtseq=65,rtrep=.f.,left=132, top=117, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce la voce di ricavo, se non specificato sbianca",;
    HelpContextID = 37881514,;
    cFormVar="w_VORSEL", bObbl = .f. , nPag = 3;
    , tabStop=.f.;
   , bGlobalFont=.t.


  func oVORSEL_3_9.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVORSEL_3_9.GetRadio()
    this.Parent.oContained.w_VORSEL = this.RadioValue()
    return .t.
  endfunc

  func oVORSEL_3_9.SetRadio()
    this.Parent.oContained.w_VORSEL=trim(this.Parent.oContained.w_VORSEL)
    this.value = ;
      iif(this.Parent.oContained.w_VORSEL=='S',1,;
      0)
  endfunc

  func oVORSEL_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' OR g_COMM='S')
    endwith
   endif
  endfunc

  add object oVORALL_3_10 as StdCheck with uid="PCYNMQGDNI",rtseq=66,rtrep=.f.,left=132, top=94, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutte le voci di ricavo",;
    HelpContextID = 31721130,;
    cFormVar="w_VORALL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVORALL_3_10.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVORALL_3_10.GetRadio()
    this.Parent.oContained.w_VORALL = this.RadioValue()
    return .t.
  endfunc

  func oVORALL_3_10.SetRadio()
    this.Parent.oContained.w_VORALL=trim(this.Parent.oContained.w_VORALL)
    this.value = ;
      iif(this.Parent.oContained.w_VORALL=='S',1,;
      0)
  endfunc

  func oVORALL_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' OR g_COMM='S')
    endwith
   endif
  endfunc

  add object oVORINI_3_11 as StdField with uid="RCXTMSSVLL",rtseq=67,rtrep=.f.,;
    cFormVar = "w_VORINI", cQueryName = "VORINI",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Voce di ricavo incongruente o obsoleta",;
    HelpContextID = 79431338,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=232, Top=94, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCTIPVOC", oKey_1_2="this.w_TIPVOR", oKey_2_1="VCCODICE", oKey_2_2="this.w_VORINI"

  func oVORINI_3_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_PERCCR='S' OR g_COMM='S') AND .w_VORALL<>'S')
    endwith
   endif
  endfunc

  func oVORINI_3_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oVORINI_3_11.ecpDrop(oSource)
    this.Parent.oContained.link_3_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVORINI_3_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VOC_COST_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VCTIPVOC="+cp_ToStrODBC(this.Parent.oContained.w_TIPVOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VCTIPVOC="+cp_ToStr(this.Parent.oContained.w_TIPVOR)
    endif
    do cp_zoom with 'VOC_COST','*','VCTIPVOC,VCCODICE',cp_AbsName(this.parent,'oVORINI_3_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di ricavo",'',this.parent.oContained
  endproc
  proc oVORINI_3_11.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.VCTIPVOC=w_TIPVOR
     i_obj.w_VCCODICE=this.parent.oContained.w_VORINI
     i_obj.ecpSave()
  endproc

  add object oDESVOV_3_12 as StdField with uid="HBXPWXRWCY",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DESVOV", cQueryName = "DESVOV",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 127861194,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=359, Top=94, InputMask=replicate('X',35)

  add object oVORFIN_3_13 as StdField with uid="LSYPFFKBKK",rtseq=69,rtrep=.f.,;
    cFormVar = "w_VORFIN", cQueryName = "VORFIN",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Voce di costo incongruente o obsoleta",;
    HelpContextID = 984746,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=232, Top=118, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCTIPVOC", oKey_1_2="this.w_TIPVOR", oKey_2_1="VCCODICE", oKey_2_2="this.w_VORFIN"

  func oVORFIN_3_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' OR g_COMM='S')
    endwith
   endif
  endfunc

  func oVORFIN_3_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oVORFIN_3_13.ecpDrop(oSource)
    this.Parent.oContained.link_3_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVORFIN_3_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VOC_COST_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VCTIPVOC="+cp_ToStrODBC(this.Parent.oContained.w_TIPVOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"VCTIPVOC="+cp_ToStr(this.Parent.oContained.w_TIPVOR)
    endif
    do cp_zoom with 'VOC_COST','*','VCTIPVOC,VCCODICE',cp_AbsName(this.parent,'oVORFIN_3_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di ricavo",'',this.parent.oContained
  endproc
  proc oVORFIN_3_13.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.VCTIPVOC=w_TIPVOR
     i_obj.w_VCCODICE=this.parent.oContained.w_VORFIN
     i_obj.ecpSave()
  endproc

  add object oDESVOV1_3_14 as StdField with uid="ELAMTOTWQH",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DESVOV1", cQueryName = "DESVOV1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 127861194,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=359, Top=118, InputMask=replicate('X',35)

  add object oFORSEL_3_17 as StdCheck with uid="MIYNMHOSHN",rtseq=71,rtrep=.f.,left=132, top=185, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce il fornitore abituale, se non specificato sbianca",;
    HelpContextID = 37881770,;
    cFormVar="w_FORSEL", bObbl = .f. , nPag = 3;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oFORSEL_3_17.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFORSEL_3_17.GetRadio()
    this.Parent.oContained.w_FORSEL = this.RadioValue()
    return .t.
  endfunc

  func oFORSEL_3_17.SetRadio()
    this.Parent.oContained.w_FORSEL=trim(this.Parent.oContained.w_FORSEL)
    this.value = ;
      iif(this.Parent.oContained.w_FORSEL=='S',1,;
      0)
  endfunc

  func oFORSEL_3_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oFORSEL_3_17.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oFORALL_3_18 as StdCheck with uid="FMYEDALWNB",rtseq=72,rtrep=.f.,left=132, top=161, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutti i fornitori",;
    HelpContextID = 31721386,;
    cFormVar="w_FORALL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oFORALL_3_18.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFORALL_3_18.GetRadio()
    this.Parent.oContained.w_FORALL = this.RadioValue()
    return .t.
  endfunc

  func oFORALL_3_18.SetRadio()
    this.Parent.oContained.w_FORALL=trim(this.Parent.oContained.w_FORALL)
    this.value = ;
      iif(this.Parent.oContained.w_FORALL=='S',1,;
      0)
  endfunc

  func oFORALL_3_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oFORALL_3_18.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oFORINI_3_19 as StdField with uid="EPJORBQYXR",rtseq=73,rtrep=.f.,;
    cFormVar = "w_FORINI", cQueryName = "FORINI",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 79431594,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=232, Top=161, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORINI"

  func oFORINI_3_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FORALL<>'S' AND .w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oFORINI_3_19.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oFORINI_3_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORINI_3_19.ecpDrop(oSource)
    this.Parent.oContained.link_3_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORINI_3_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORINI_3_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Fornitori",'',this.parent.oContained
  endproc
  proc oFORINI_3_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_FORINI
     i_obj.ecpSave()
  endproc

  add object oDESFOR_3_20 as StdField with uid="TBIDVSKJCK",rtseq=74,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 196018634,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=359, Top=161, InputMask=replicate('X',35)

  func oDESFOR_3_20.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oFORFIN_3_21 as StdField with uid="AADGKRKFOL",rtseq=75,rtrep=.f.,;
    cFormVar = "w_FORFIN", cQueryName = "FORFIN",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 985002,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=232, Top=185, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORFIN"

  func oFORFIN_3_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oFORFIN_3_21.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oFORFIN_3_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORFIN_3_21.ecpDrop(oSource)
    this.Parent.oContained.link_3_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORFIN_3_21.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORFIN_3_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Fornitori",'',this.parent.oContained
  endproc
  proc oFORFIN_3_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_FORFIN
     i_obj.ecpSave()
  endproc

  add object oDESFOR1_3_22 as StdField with uid="ZZCDEDAKII",rtseq=76,rtrep=.f.,;
    cFormVar = "w_DESFOR1", cQueryName = "DESFOR1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 196018634,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=359, Top=185, InputMask=replicate('X',35)

  func oDESFOR1_3_22.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oGESSEL_3_25 as StdCheck with uid="GKELKVQFZB",rtseq=77,rtrep=.f.,left=297, top=225, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce il tipo di gestione",;
    HelpContextID = 37880218,;
    cFormVar="w_GESSEL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oGESSEL_3_25.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oGESSEL_3_25.GetRadio()
    this.Parent.oContained.w_GESSEL = this.RadioValue()
    return .t.
  endfunc

  func oGESSEL_3_25.SetRadio()
    this.Parent.oContained.w_GESSEL=trim(this.Parent.oContained.w_GESSEL)
    this.value = ;
      iif(this.Parent.oContained.w_GESSEL=='S',1,;
      0)
  endfunc

  func oGESSEL_3_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oGESSEL_3_25.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oGESINI_3_26 as StdCombo with uid="IOWLWSLPTN",rtseq=78,rtrep=.f.,left=132,top=228,width=131,height=21;
    , ToolTipText = "Tipo gestione da sostituire per approvvigionamento articolo";
    , HelpContextID = 79430042;
    , cFormVar="w_GESINI",RowSource=""+"A fabbisogno,"+"A scorta,"+"A consumo", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oGESINI_3_26.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oGESINI_3_26.GetRadio()
    this.Parent.oContained.w_GESINI = this.RadioValue()
    return .t.
  endfunc

  func oGESINI_3_26.SetRadio()
    this.Parent.oContained.w_GESINI=trim(this.Parent.oContained.w_GESINI)
    this.value = ;
      iif(this.Parent.oContained.w_GESINI=='F',1,;
      iif(this.Parent.oContained.w_GESINI=='S',2,;
      iif(this.Parent.oContained.w_GESINI=='C',3,;
      0)))
  endfunc

  func oGESINI_3_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oGESINI_3_26.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oGESFIN_3_29 as StdCombo with uid="HLBGBRVMDO",rtseq=79,rtrep=.f.,left=399,top=228,width=131,height=21;
    , ToolTipText = "Tipo gestione inserito in sostituzione per approvvigionamento articolo";
    , HelpContextID = 983450;
    , cFormVar="w_GESFIN",RowSource=""+"A fabbisogno,"+"A scorta,"+"A consumo", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oGESFIN_3_29.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    space(10)))))
  endfunc
  func oGESFIN_3_29.GetRadio()
    this.Parent.oContained.w_GESFIN = this.RadioValue()
    return .t.
  endfunc

  func oGESFIN_3_29.SetRadio()
    this.Parent.oContained.w_GESFIN=trim(this.Parent.oContained.w_GESFIN)
    this.value = ;
      iif(this.Parent.oContained.w_GESFIN=='F',1,;
      iif(this.Parent.oContained.w_GESFIN=='S',2,;
      iif(this.Parent.oContained.w_GESFIN=='C',3,;
      0)))
  endfunc

  func oGESFIN_3_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oGESFIN_3_29.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPROVSEL_3_30 as StdCheck with uid="HUTLOPJVQA",rtseq=80,rtrep=.f.,left=297, top=270, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce la provenienza",;
    HelpContextID = 140456970,;
    cFormVar="w_PROVSEL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPROVSEL_3_30.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPROVSEL_3_30.GetRadio()
    this.Parent.oContained.w_PROVSEL = this.RadioValue()
    return .t.
  endfunc

  func oPROVSEL_3_30.SetRadio()
    this.Parent.oContained.w_PROVSEL=trim(this.Parent.oContained.w_PROVSEL)
    this.value = ;
      iif(this.Parent.oContained.w_PROVSEL=='S',1,;
      0)
  endfunc

  func oPROVSEL_3_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oPROVSEL_3_30.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oPROVINI_3_31 as StdCombo with uid="IDNOSFNPCU",rtseq=81,rtrep=.f.,left=132,top=273,width=131,height=21;
    , ToolTipText = "Provenienza preferenziale da sostituire dell'articolo";
    , HelpContextID = 52214;
    , cFormVar="w_PROVINI",RowSource=""+"Interna,"+"Esterna,"+"C/Lavoro", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oPROVINI_3_31.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    iif(this.value =3,'L',;
    space(1)))))
  endfunc
  func oPROVINI_3_31.GetRadio()
    this.Parent.oContained.w_PROVINI = this.RadioValue()
    return .t.
  endfunc

  func oPROVINI_3_31.SetRadio()
    this.Parent.oContained.w_PROVINI=trim(this.Parent.oContained.w_PROVINI)
    this.value = ;
      iif(this.Parent.oContained.w_PROVINI=='I',1,;
      iif(this.Parent.oContained.w_PROVINI=='E',2,;
      iif(this.Parent.oContained.w_PROVINI=='L',3,;
      0)))
  endfunc

  func oPROVINI_3_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oPROVINI_3_31.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oPROVFIN_3_33 as StdCombo with uid="XNYURCHNMO",rtseq=82,rtrep=.f.,left=400,top=273,width=131,height=21;
    , ToolTipText = "Provenienza preferenziale dell'articolo inserita in sostituzione";
    , HelpContextID = 86979594;
    , cFormVar="w_PROVFIN",RowSource=""+"Interna,"+"Esterna,"+"C/Lavoro", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oPROVFIN_3_33.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    iif(this.value =3,'L',;
    space(1)))))
  endfunc
  func oPROVFIN_3_33.GetRadio()
    this.Parent.oContained.w_PROVFIN = this.RadioValue()
    return .t.
  endfunc

  func oPROVFIN_3_33.SetRadio()
    this.Parent.oContained.w_PROVFIN=trim(this.Parent.oContained.w_PROVFIN)
    this.value = ;
      iif(this.Parent.oContained.w_PROVFIN=='I',1,;
      iif(this.Parent.oContained.w_PROVFIN=='E',2,;
      iif(this.Parent.oContained.w_PROVFIN=='L',3,;
      0)))
  endfunc

  func oPROVFIN_3_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oPROVFIN_3_33.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oFLGPOS_3_38 as StdCombo with uid="OMHWNSBACX",rtseq=155,rtrep=.f.,left=132,top=362,width=144,height=21;
    , ToolTipText = "Inserisce/toglie flag articolo P.O.S. in anagrafica articoli";
    , HelpContextID = 178633386;
    , cFormVar="w_FLGPOS",RowSource=""+"Nessuna modifica,"+"Attiva flag P.O.S.,"+"Disattiva flag P.O.S.", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oFLGPOS_3_38.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oFLGPOS_3_38.GetRadio()
    this.Parent.oContained.w_FLGPOS = this.RadioValue()
    return .t.
  endfunc

  func oFLGPOS_3_38.SetRadio()
    this.Parent.oContained.w_FLGPOS=trim(this.Parent.oContained.w_FLGPOS)
    this.value = ;
      iif(this.Parent.oContained.w_FLGPOS=='X',1,;
      iif(this.Parent.oContained.w_FLGPOS=='S',2,;
      iif(this.Parent.oContained.w_FLGPOS=='N',3,;
      0)))
  endfunc

  func oFLGPOS_3_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_GPOS='S')
    endwith
   endif
  endfunc

  func oFLGPOS_3_38.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oREPSEL_3_40 as StdCheck with uid="ZNKFTSVPIC",rtseq=156,rtrep=.f.,left=132, top=418, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce il codice reparto, se non specificato sbianca",;
    HelpContextID = 37892330,;
    cFormVar="w_REPSEL", bObbl = .f. , nPag = 3;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oREPSEL_3_40.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oREPSEL_3_40.GetRadio()
    this.Parent.oContained.w_REPSEL = this.RadioValue()
    return .t.
  endfunc

  func oREPSEL_3_40.SetRadio()
    this.Parent.oContained.w_REPSEL=trim(this.Parent.oContained.w_REPSEL)
    this.value = ;
      iif(this.Parent.oContained.w_REPSEL=='S',1,;
      0)
  endfunc

  func oREPSEL_3_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_GPOS='S')
    endwith
   endif
  endfunc

  func oREPSEL_3_40.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oREPALL_3_41 as StdCheck with uid="KGSQJCQWYT",rtseq=157,rtrep=.f.,left=132, top=392, caption="Tutti",;
    ToolTipText = "Se attivo: sostituisce tutti i codici reparto",;
    HelpContextID = 31731946,;
    cFormVar="w_REPALL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oREPALL_3_41.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oREPALL_3_41.GetRadio()
    this.Parent.oContained.w_REPALL = this.RadioValue()
    return .t.
  endfunc

  func oREPALL_3_41.SetRadio()
    this.Parent.oContained.w_REPALL=trim(this.Parent.oContained.w_REPALL)
    this.value = ;
      iif(this.Parent.oContained.w_REPALL=='S',1,;
      0)
  endfunc

  func oREPALL_3_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_GPOS='S')
    endwith
   endif
  endfunc

  func oREPALL_3_41.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oREPINI_3_42 as StdField with uid="VBWFAVXPRI",rtseq=158,rtrep=.f.,;
    cFormVar = "w_REPINI", cQueryName = "REPINI",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 79442154,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=232, Top=392, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="REP_ARTI", cZoomOnZoom="GSPS_ARE", oKey_1_1="RECODREP", oKey_1_2="this.w_REPINI"

  func oREPINI_3_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_REPALL<>'S' AND g_GPOS='S')
    endwith
   endif
  endfunc

  func oREPINI_3_42.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  func oREPINI_3_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oREPINI_3_42.ecpDrop(oSource)
    this.Parent.oContained.link_3_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oREPINI_3_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REP_ARTI','*','RECODREP',cp_AbsName(this.parent,'oREPINI_3_42'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ARE',"Reparti",'',this.parent.oContained
  endproc
  proc oREPINI_3_42.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ARE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RECODREP=this.parent.oContained.w_REPINI
     i_obj.ecpSave()
  endproc

  add object oDESREP_3_43 as StdField with uid="HIGQJIKGQU",rtseq=159,rtrep=.f.,;
    cFormVar = "w_DESREP", cQueryName = "DESREP",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 239272394,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=305, Top=392, InputMask=replicate('X',35)

  func oDESREP_3_43.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oREPFIN_3_44 as StdField with uid="HGPIMGBFRS",rtseq=160,rtrep=.f.,;
    cFormVar = "w_REPFIN", cQueryName = "REPFIN",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 995562,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=232, Top=418, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="REP_ARTI", cZoomOnZoom="GSPS_ARE", oKey_1_1="RECODREP", oKey_1_2="this.w_REPFIN"

  func oREPFIN_3_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_GPOS='S')
    endwith
   endif
  endfunc

  func oREPFIN_3_44.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  func oREPFIN_3_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oREPFIN_3_44.ecpDrop(oSource)
    this.Parent.oContained.link_3_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oREPFIN_3_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REP_ARTI','*','RECODREP',cp_AbsName(this.parent,'oREPFIN_3_44'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ARE',"Reparti",'',this.parent.oContained
  endproc
  proc oREPFIN_3_44.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ARE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RECODREP=this.parent.oContained.w_REPFIN
     i_obj.ecpSave()
  endproc

  add object oDESREP1_3_45 as StdField with uid="QESDGIFMFJ",rtseq=161,rtrep=.f.,;
    cFormVar = "w_DESREP1", cQueryName = "DESREP1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 239272394,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=305, Top=418, InputMask=replicate('X',35)

  func oDESREP1_3_45.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oPECOSEL_3_47 as StdCheck with uid="SBPCWCCNAE",rtseq=162,rtrep=.f.,left=301, top=451, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce il test peso/collo",;
    HelpContextID = 140968202,;
    cFormVar="w_PECOSEL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oPECOSEL_3_47.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPECOSEL_3_47.GetRadio()
    this.Parent.oContained.w_PECOSEL = this.RadioValue()
    return .t.
  endfunc

  func oPECOSEL_3_47.SetRadio()
    this.Parent.oContained.w_PECOSEL=trim(this.Parent.oContained.w_PECOSEL)
    this.value = ;
      iif(this.Parent.oContained.w_PECOSEL=='S',1,;
      0)
  endfunc

  func oPECOSEL_3_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S' AND g_GPOS='S')
    endwith
   endif
  endfunc

  func oPECOSEL_3_47.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc


  add object oPECOINI_3_48 as StdCombo with uid="WXQTHCDFDK",rtseq=163,rtrep=.f.,left=132,top=451,width=131,height=21;
    , ToolTipText = "Tipologia peso/collo da sostituire dell'articolo";
    , HelpContextID = 267976438;
    , cFormVar="w_PECOINI",RowSource=""+"Peso,"+"Collo,"+"Prezzo,"+"Tutti", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oPECOINI_3_48.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'C',;
    iif(this.value =3,'Z',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oPECOINI_3_48.GetRadio()
    this.Parent.oContained.w_PECOINI = this.RadioValue()
    return .t.
  endfunc

  func oPECOINI_3_48.SetRadio()
    this.Parent.oContained.w_PECOINI=trim(this.Parent.oContained.w_PECOINI)
    this.value = ;
      iif(this.Parent.oContained.w_PECOINI=='P',1,;
      iif(this.Parent.oContained.w_PECOINI=='C',2,;
      iif(this.Parent.oContained.w_PECOINI=='Z',3,;
      iif(this.Parent.oContained.w_PECOINI=='T',4,;
      0))))
  endfunc

  func oPECOINI_3_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S' AND g_GPOS='S')
    endwith
   endif
  endfunc

  func oPECOINI_3_48.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc


  add object oPECOFIN_3_51 as StdCombo with uid="CMMICBSWCS",rtseq=164,rtrep=.f.,left=400,top=451,width=131,height=21;
    , ToolTipText = "Tipologia peso/collo dell'articolo in sostituzione";
    , HelpContextID = 87490826;
    , cFormVar="w_PECOFIN",RowSource=""+"Peso,"+"Collo,"+"Prezzo", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oPECOFIN_3_51.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'C',;
    iif(this.value =3,'Z',;
    space(1)))))
  endfunc
  func oPECOFIN_3_51.GetRadio()
    this.Parent.oContained.w_PECOFIN = this.RadioValue()
    return .t.
  endfunc

  func oPECOFIN_3_51.SetRadio()
    this.Parent.oContained.w_PECOFIN=trim(this.Parent.oContained.w_PECOFIN)
    this.value = ;
      iif(this.Parent.oContained.w_PECOFIN=='P',1,;
      iif(this.Parent.oContained.w_PECOFIN=='C',2,;
      iif(this.Parent.oContained.w_PECOFIN=='Z',3,;
      0)))
  endfunc

  func oPECOFIN_3_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S' AND g_GPOS='S')
    endwith
   endif
  endfunc

  func oPECOFIN_3_51.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc


  add object oTIPARTIN_3_53 as StdCombo with uid="KBQGFVBMID",rtseq=176,rtrep=.f.,left=132,top=315,width=131,height=21;
    , ToolTipText = "Tipo di selezione: articoli/servizi";
    , HelpContextID = 108778372;
    , cFormVar="w_TIPARTIN",RowSource=""+"Prodotto Finito,"+"Semilavorato,"+"Materia Prima,"+"Fantasma", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTIPARTIN_3_53.RadioValue()
    return(iif(this.value =1,'PF',;
    iif(this.value =2,'SE',;
    iif(this.value =3,'MP',;
    iif(this.value =4,'PH',;
    space(2))))))
  endfunc
  func oTIPARTIN_3_53.GetRadio()
    this.Parent.oContained.w_TIPARTIN = this.RadioValue()
    return .t.
  endfunc

  func oTIPARTIN_3_53.SetRadio()
    this.Parent.oContained.w_TIPARTIN=trim(this.Parent.oContained.w_TIPARTIN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPARTIN=='PF',1,;
      iif(this.Parent.oContained.w_TIPARTIN=='SE',2,;
      iif(this.Parent.oContained.w_TIPARTIN=='MP',3,;
      iif(this.Parent.oContained.w_TIPARTIN=='PH',4,;
      0))))
  endfunc

  func oTIPARTIN_3_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL='A' AND g_PROD='S')
    endwith
   endif
  endfunc

  func oTIPARTIN_3_53.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oTIPARTIF_3_55 as StdCombo with uid="SWZRUZXFTK",rtseq=177,rtrep=.f.,left=400,top=315,width=131,height=21;
    , ToolTipText = "Tipo di selezione: articoli/servizi";
    , HelpContextID = 108778364;
    , cFormVar="w_TIPARTIF",RowSource=""+"Prodotto Finito,"+"Semilavorato,"+"Materia Prima,"+"Fantasma", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oTIPARTIF_3_55.RadioValue()
    return(iif(this.value =1,'PF',;
    iif(this.value =2,'SE',;
    iif(this.value =3,'MP',;
    iif(this.value =4,'PH',;
    space(2))))))
  endfunc
  func oTIPARTIF_3_55.GetRadio()
    this.Parent.oContained.w_TIPARTIF = this.RadioValue()
    return .t.
  endfunc

  func oTIPARTIF_3_55.SetRadio()
    this.Parent.oContained.w_TIPARTIF=trim(this.Parent.oContained.w_TIPARTIF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPARTIF=='PF',1,;
      iif(this.Parent.oContained.w_TIPARTIF=='SE',2,;
      iif(this.Parent.oContained.w_TIPARTIF=='MP',3,;
      iif(this.Parent.oContained.w_TIPARTIF=='PH',4,;
      0))))
  endfunc

  func oTIPARTIF_3_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL='A' AND g_PROD='S')
    endwith
   endif
  endfunc

  func oTIPARTIF_3_55.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oTIPASEL_3_56 as StdCheck with uid="NPKOXPDQJF",rtseq=178,rtrep=.f.,left=295, top=314, caption="Sostituire con:",;
    ToolTipText = "Se attivo: sostituisce la tipologia",;
    HelpContextID = 141831370,;
    cFormVar="w_TIPASEL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oTIPASEL_3_56.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTIPASEL_3_56.GetRadio()
    this.Parent.oContained.w_TIPASEL = this.RadioValue()
    return .t.
  endfunc

  func oTIPASEL_3_56.SetRadio()
    this.Parent.oContained.w_TIPASEL=trim(this.Parent.oContained.w_TIPASEL)
    this.value = ;
      iif(this.Parent.oContained.w_TIPASEL=='S',1,;
      0)
  endfunc

  func oTIPASEL_3_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S' AND g_PROD='S')
    endwith
   endif
  endfunc

  func oTIPASEL_3_56.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oBtn_3_57 as StdButton with uid="FHHYLKJPGR",left=538, top=529, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 48892698;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_57.Click()
      with this.Parent.oContained
        do GSMA_BVA with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_57.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT ISALT())
     endwith
    endif
  endfunc


  add object oBtn_3_58 as StdButton with uid="ZYYYXEKCUM",left=590, top=529, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 41604026;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_58.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_58.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT ISALT())
     endwith
    endif
  endfunc

  add object oStr_3_7 as StdString with uid="LZUCHCJQFJ",Visible=.t., Left=6, Top=27,;
    Alignment=1, Width=116, Height=17,;
    Caption="Voci di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_15 as StdString with uid="XHBXLRIDWK",Visible=.t., Left=6, Top=95,;
    Alignment=1, Width=116, Height=18,;
    Caption="Voci di ricavo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_23 as StdString with uid="WNWDBDCNTS",Visible=.t., Left=6, Top=162,;
    Alignment=1, Width=116, Height=18,;
    Caption="Fornitori abituali:"  ;
  , bGlobalFont=.t.

  func oStr_3_23.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_3_28 as StdString with uid="NYNPINILHQ",Visible=.t., Left=6, Top=228,;
    Alignment=1, Width=116, Height=18,;
    Caption="Gestione:"  ;
  , bGlobalFont=.t.

  func oStr_3_28.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_3_32 as StdString with uid="UUTLUXISUS",Visible=.t., Left=6, Top=273,;
    Alignment=1, Width=116, Height=18,;
    Caption="Provenienza:"  ;
  , bGlobalFont=.t.

  func oStr_3_32.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_3_39 as StdString with uid="NFRIAZJJIS",Visible=.t., Left=6, Top=363,;
    Alignment=1, Width=116, Height=18,;
    Caption="Flag articolo P.O.S.:"  ;
  , bGlobalFont=.t.

  func oStr_3_39.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oStr_3_46 as StdString with uid="ZIXUGUGNZF",Visible=.t., Left=6, Top=392,;
    Alignment=1, Width=116, Height=18,;
    Caption="Reparto:"  ;
  , bGlobalFont=.t.

  func oStr_3_46.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oStr_3_49 as StdString with uid="RRAWEMKLRB",Visible=.t., Left=6, Top=451,;
    Alignment=1, Width=116, Height=18,;
    Caption="Test articolo:"  ;
  , bGlobalFont=.t.

  func oStr_3_49.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oStr_3_54 as StdString with uid="ROHOUWECLQ",Visible=.t., Left=13, Top=317,;
    Alignment=1, Width=109, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  func oStr_3_54.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oBox_3_8 as StdBox with uid="VGPJBUKZHF",left=7, top=82, width=625,height=2

  add object oBox_3_16 as StdBox with uid="TXOEFIHNQU",left=3, top=150, width=625,height=2

  add object oBox_3_24 as StdBox with uid="FDFYLIJLKT",left=3, top=216, width=625,height=2

  add object oBox_3_27 as StdBox with uid="KRRRUHTYCY",left=2, top=260, width=625,height=2

  add object oBox_3_50 as StdBox with uid="JRGSKBUTOY",left=2, top=304, width=625,height=2

  add object oBox_3_52 as StdBox with uid="KEYUREQJYJ",left=2, top=348, width=625,height=2
enddefine
define class tgsma_kvaPag4 as StdContainer
  Width  = 645
  height = 596
  stdWidth  = 645
  stdheight = 596
  resizeXpos=439
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPSEL_4_1 as StdCombo with uid="LFZQTOCJUQ",rtseq=83,rtrep=.f.,left=189,top=18,width=101,height=21;
    , ToolTipText = "Tipo di selezione: articoli/servizi";
    , HelpContextID = 37891274;
    , cFormVar="w_TIPSEL",RowSource=""+"Articoli,"+"Servizi", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTIPSEL_4_1.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oTIPSEL_4_1.GetRadio()
    this.Parent.oContained.w_TIPSEL = this.RadioValue()
    return .t.
  endfunc

  func oTIPSEL_4_1.SetRadio()
    this.Parent.oContained.w_TIPSEL=trim(this.Parent.oContained.w_TIPSEL)
    this.value = ;
      iif(this.Parent.oContained.w_TIPSEL=='A',1,;
      iif(this.Parent.oContained.w_TIPSEL=='S',2,;
      0))
  endfunc

  func oTIPSEL_4_1.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oTIPARTI_4_2 as StdCombo with uid="APFAUWHHYB",value=1,rtseq=84,rtrep=.f.,left=189,top=46,width=131,height=21;
    , ToolTipText = "Tipo di selezione: articoli/servizi";
    , HelpContextID = 108778294;
    , cFormVar="w_TIPARTI",RowSource=""+"Tutti,"+"A quantit� e valore,"+"A valore,"+"Descrittivo", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTIPARTI_4_2.RadioValue()
    return(iif(this.value =1,'  ',;
    iif(this.value =2,'FM',;
    iif(this.value =3,'FO',;
    iif(this.value =4,'DE',;
    space(2))))))
  endfunc
  func oTIPARTI_4_2.GetRadio()
    this.Parent.oContained.w_TIPARTI = this.RadioValue()
    return .t.
  endfunc

  func oTIPARTI_4_2.SetRadio()
    this.Parent.oContained.w_TIPARTI=trim(this.Parent.oContained.w_TIPARTI)
    this.value = ;
      iif(this.Parent.oContained.w_TIPARTI=='',1,;
      iif(this.Parent.oContained.w_TIPARTI=='FM',2,;
      iif(this.Parent.oContained.w_TIPARTI=='FO',3,;
      iif(this.Parent.oContained.w_TIPARTI=='DE',4,;
      0))))
  endfunc

  func oTIPARTI_4_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL='S')
    endwith
   endif
  endfunc

  func oTIPARTI_4_2.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
  endfunc


  add object oTIPARTI_4_3 as StdCombo with uid="OQEKYITJPE",value=1,rtseq=85,rtrep=.f.,left=189,top=46,width=131,height=21;
    , ToolTipText = "Tipo di selezione: articoli/servizi";
    , HelpContextID = 108778294;
    , cFormVar="w_TIPARTI",RowSource=""+"Tutti,"+"Prodotto Finito,"+"Semilavorato,"+"Materia Prima,"+"Fantasma", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTIPARTI_4_3.RadioValue()
    return(iif(this.value =1,'  ',;
    iif(this.value =2,'PF',;
    iif(this.value =3,'SE',;
    iif(this.value =4,'MP',;
    iif(this.value =5,'PH',;
    space(2)))))))
  endfunc
  func oTIPARTI_4_3.GetRadio()
    this.Parent.oContained.w_TIPARTI = this.RadioValue()
    return .t.
  endfunc

  func oTIPARTI_4_3.SetRadio()
    this.Parent.oContained.w_TIPARTI=trim(this.Parent.oContained.w_TIPARTI)
    this.value = ;
      iif(this.Parent.oContained.w_TIPARTI=='',1,;
      iif(this.Parent.oContained.w_TIPARTI=='PF',2,;
      iif(this.Parent.oContained.w_TIPARTI=='SE',3,;
      iif(this.Parent.oContained.w_TIPARTI=='MP',4,;
      iif(this.Parent.oContained.w_TIPARTI=='PH',5,;
      0)))))
  endfunc

  func oTIPARTI_4_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL='A' AND g_PROD='S')
    endwith
   endif
  endfunc

  func oTIPARTI_4_3.mHide()
    with this.Parent.oContained
      return (.w_TIPSEL<>'A')
    endwith
  endfunc


  add object oPROVART_4_4 as StdCombo with uid="CQGEYTHWLH",value=1,rtseq=86,rtrep=.f.,left=189,top=74,width=131,height=21;
    , ToolTipText = "Provenienza preferenziale da sostituire dell'articolo";
    , HelpContextID = 58772470;
    , cFormVar="w_PROVART",RowSource=""+"Tutti,"+"Interna,"+"Esterna,"+"C/Lavoro", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oPROVART_4_4.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'I',;
    iif(this.value =3,'E',;
    iif(this.value =4,'L',;
    space(1))))))
  endfunc
  func oPROVART_4_4.GetRadio()
    this.Parent.oContained.w_PROVART = this.RadioValue()
    return .t.
  endfunc

  func oPROVART_4_4.SetRadio()
    this.Parent.oContained.w_PROVART=trim(this.Parent.oContained.w_PROVART)
    this.value = ;
      iif(this.Parent.oContained.w_PROVART=='',1,;
      iif(this.Parent.oContained.w_PROVART=='I',2,;
      iif(this.Parent.oContained.w_PROVART=='E',3,;
      iif(this.Parent.oContained.w_PROVART=='L',4,;
      0))))
  endfunc

  func oPROVART_4_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL='A')
    endwith
   endif
  endfunc

  func oPROVART_4_4.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oARTOBS_4_5 as StdCombo with uid="LQUEDYBAOC",rtseq=87,rtrep=.f.,left=189,top=102,width=101,height=21;
    , ToolTipText = "Tipo di articolo: solo validi, solo obsoleti, tutti";
    , HelpContextID = 192275706;
    , cFormVar="w_ARTOBS",RowSource=""+"Solo validi,"+"Solo obsoleti,"+"Tutti", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oARTOBS_4_5.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'O',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oARTOBS_4_5.GetRadio()
    this.Parent.oContained.w_ARTOBS = this.RadioValue()
    return .t.
  endfunc

  func oARTOBS_4_5.SetRadio()
    this.Parent.oContained.w_ARTOBS=trim(this.Parent.oContained.w_ARTOBS)
    this.value = ;
      iif(this.Parent.oContained.w_ARTOBS=='V',1,;
      iif(this.Parent.oContained.w_ARTOBS=='O',2,;
      iif(this.Parent.oContained.w_ARTOBS=='T',3,;
      0)))
  endfunc

  add object oARTINI_4_8 as StdField with uid="CGGTFZRXYO",rtseq=88,rtrep=.f.,;
    cFormVar = "w_ARTINI", cQueryName = "ARTINI",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale superiore al codice finale oppure obsoleto",;
    ToolTipText = "Codice articolo/servizio iniziale",;
    HelpContextID = 79422714,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=189, Top=131, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_ARTINI"

  func oARTINI_4_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTINI_4_8.ecpDrop(oSource)
    this.Parent.oContained.link_4_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTINI_4_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oARTINI_4_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli/servizi",'ARTZOOM2.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oDESART_4_9 as StdField with uid="MSUWPXJTWC",rtseq=89,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 159646154,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=345, Top=131, InputMask=replicate('X',35)

  add object oARTFIN_4_10 as StdField with uid="LXUPTNNEWH",rtseq=90,rtrep=.f.,;
    cFormVar = "w_ARTFIN", cQueryName = "ARTFIN",;
    bObbl = .f. , nPag = 4, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale superiore al codice finale oppure obsoleto",;
    ToolTipText = "Codice articolo/servizio finale",;
    HelpContextID = 976122,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=189, Top=159, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_ARTFIN"

  func oARTFIN_4_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTFIN_4_10.ecpDrop(oSource)
    this.Parent.oContained.link_4_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTFIN_4_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oARTFIN_4_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli/servizi",'ARTZOOM2.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oDESART1_4_11 as StdField with uid="NARWPVVQDH",rtseq=91,rtrep=.f.,;
    cFormVar = "w_DESART1", cQueryName = "DESART1",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 159646154,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=345, Top=159, InputMask=replicate('X',35)

  add object oIVAFIL_4_12 as StdField with uid="GVNSUHDTUL",rtseq=92,rtrep=.f.,;
    cFormVar = "w_IVAFIL", cQueryName = "IVAFIL",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA obsoleto",;
    ToolTipText = "Codice IVA",;
    HelpContextID = 34607226,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=189, Top=187, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_IVAFIL"

  func oIVAFIL_4_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oIVAFIL_4_12.ecpDrop(oSource)
    this.Parent.oContained.link_4_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIVAFIL_4_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oIVAFIL_4_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oIVAFIL_4_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_IVAFIL
     i_obj.ecpSave()
  endproc

  add object oIVADES_4_13 as StdField with uid="VFQRTDSPTW",rtseq=93,rtrep=.f.,;
    cFormVar = "w_IVADES", cQueryName = "IVADES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 189927546,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=345, Top=187, InputMask=replicate('X',35)

  add object oGRMFIL_4_14 as StdField with uid="PRTZOHWUZW",rtseq=94,rtrep=.f.,;
    cFormVar = "w_GRMFIL", cQueryName = "GRMFIL",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico",;
    HelpContextID = 34559130,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=189, Top=215, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRMFIL"

  func oGRMFIL_4_14.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oGRMFIL_4_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRMFIL_4_14.ecpDrop(oSource)
    this.Parent.oContained.link_4_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRMFIL_4_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRMFIL_4_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oGRMFIL_4_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRMFIL
     i_obj.ecpSave()
  endproc

  add object oGRMDES_4_15 as StdField with uid="MRDLTRJBSO",rtseq=95,rtrep=.f.,;
    cFormVar = "w_GRMDES", cQueryName = "GRMDES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 189879450,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=345, Top=215, InputMask=replicate('X',35)

  func oGRMDES_4_15.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCATFIL_4_18 as StdField with uid="XBFFRKXSTI",rtseq=96,rtrep=.f.,;
    cFormVar = "w_CATFIL", cQueryName = "CATFIL",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categ. contabile",;
    HelpContextID = 34534874,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=189, Top=245, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOARTI", cZoomOnZoom="GSAR_AC1", oKey_1_1="C1CODICE", oKey_1_2="this.w_CATFIL"

  func oCATFIL_4_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATFIL_4_18.ecpDrop(oSource)
    this.Parent.oContained.link_4_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATFIL_4_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOARTI','*','C1CODICE',cp_AbsName(this.parent,'oCATFIL_4_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC1',"Categorie contabili",'',this.parent.oContained
  endproc
  proc oCATFIL_4_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC1()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C1CODICE=this.parent.oContained.w_CATFIL
     i_obj.ecpSave()
  endproc

  add object oCATDES_4_19 as StdField with uid="XTEAKXXLNB",rtseq=97,rtrep=.f.,;
    cFormVar = "w_CATDES", cQueryName = "CATDES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 189855194,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=345, Top=245, InputMask=replicate('X',35)

  add object oFAMFIL_4_20 as StdField with uid="PMUNPVFYIV",rtseq=98,rtrep=.f.,;
    cFormVar = "w_FAMFIL", cQueryName = "FAMFIL",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia",;
    HelpContextID = 34563498,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=189, Top=273, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_FAMFIL"

  func oFAMFIL_4_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oFAMFIL_4_20.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oFAMFIL_4_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAMFIL_4_20.ecpDrop(oSource)
    this.Parent.oContained.link_4_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAMFIL_4_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAMFIL_4_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Codici famiglie",'',this.parent.oContained
  endproc
  proc oFAMFIL_4_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_FAMFIL
     i_obj.ecpSave()
  endproc

  add object oFAMDES_4_21 as StdField with uid="CGXLYUHRXC",rtseq=99,rtrep=.f.,;
    cFormVar = "w_FAMDES", cQueryName = "FAMDES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 189883818,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=345, Top=273, InputMask=replicate('X',35)

  func oFAMDES_4_21.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCATOFIL_4_24 as StdField with uid="YAZRXVZRLN",rtseq=100,rtrep=.f.,;
    cFormVar = "w_CATOFIL", cQueryName = "CATOFIL",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categ. omogenea",;
    HelpContextID = 87422426,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=189, Top=301, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATOFIL"

  func oCATOFIL_4_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oCATOFIL_4_24.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oCATOFIL_4_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATOFIL_4_24.ecpDrop(oSource)
    this.Parent.oContained.link_4_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATOFIL_4_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATOFIL_4_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc oCATOFIL_4_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CATOFIL
     i_obj.ecpSave()
  endproc

  add object oCATODES_4_25 as StdField with uid="RCAUBZPPJS",rtseq=101,rtrep=.f.,;
    cFormVar = "w_CATODES", cQueryName = "CATODES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 111807014,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=345, Top=301, InputMask=replicate('X',35)

  func oCATODES_4_25.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPROFIL_4_26 as StdField with uid="OSTIWNMGOI",rtseq=102,rtrep=.f.,;
    cFormVar = "w_PROFIL", cQueryName = "PROFIL",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice produttore",;
    HelpContextID = 34550794,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=189, Top=329, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PROFIL"

  func oPROFIL_4_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oPROFIL_4_26.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oPROFIL_4_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oPROFIL_4_26.ecpDrop(oSource)
    this.Parent.oContained.link_4_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPROFIL_4_26.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPROFIL_4_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Produttori",'',this.parent.oContained
  endproc
  proc oPROFIL_4_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PROFIL
     i_obj.ecpSave()
  endproc

  add object oPRODES_4_27 as StdField with uid="UXPEAGUHOB",rtseq=103,rtrep=.f.,;
    cFormVar = "w_PRODES", cQueryName = "PRODES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 189871114,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=345, Top=329, InputMask=replicate('X',35)

  func oPRODES_4_27.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oFORFIL_4_30 as StdField with uid="YODBPGPIZH",rtseq=104,rtrep=.f.,;
    cFormVar = "w_FORFIL", cQueryName = "FORFIL",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice fornitore abituale",;
    HelpContextID = 34539434,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=189, Top=357, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_FORFIL"

  func oFORFIL_4_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oFORFIL_4_30.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oFORFIL_4_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORFIL_4_30.ecpDrop(oSource)
    this.Parent.oContained.link_4_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORFIL_4_30.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFORFIL_4_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Fornitori",'',this.parent.oContained
  endproc
  proc oFORFIL_4_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_FORFIL
     i_obj.ecpSave()
  endproc

  add object oFORDES_4_31 as StdField with uid="HJMOBGXIYG",rtseq=105,rtrep=.f.,;
    cFormVar = "w_FORDES", cQueryName = "FORDES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 189859754,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=345, Top=357, InputMask=replicate('X',35)

  func oFORDES_4_31.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oMARFIL_4_32 as StdField with uid="ZHQUBXRTJK",rtseq=106,rtrep=.f.,;
    cFormVar = "w_MARFIL", cQueryName = "MARFIL",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice marca",;
    HelpContextID = 34542906,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=189, Top=385, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_MARFIL"

  func oMARFIL_4_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oMARFIL_4_32.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oMARFIL_4_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oMARFIL_4_32.ecpDrop(oSource)
    this.Parent.oContained.link_4_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMARFIL_4_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMARFIL_4_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Codici marchi",'',this.parent.oContained
  endproc
  proc oMARFIL_4_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_MARFIL
     i_obj.ecpSave()
  endproc

  add object oMARDES_4_33 as StdField with uid="GZJRQHFKFN",rtseq=107,rtrep=.f.,;
    cFormVar = "w_MARDES", cQueryName = "MARDES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 189863226,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=345, Top=385, InputMask=replicate('X',35)

  func oMARDES_4_33.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oBtn_4_36 as StdButton with uid="XAOFRYAIID",left=538, top=529, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 48892698;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_36.Click()
      with this.Parent.oContained
        do GSMA_BVA with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_4_37 as StdButton with uid="KYQESPJULE",left=590, top=529, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 41604026;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_37.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOLFIL_4_44 as StdField with uid="SRLDIEZGFW",rtseq=119,rtrep=.f.,;
    cFormVar = "w_COLFIL", cQueryName = "COLFIL",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipologia collo di selezione",;
    HelpContextID = 34564058,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=189, Top=413, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_COLFIL"

  func oCOLFIL_4_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oCOLFIL_4_44.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oCOLFIL_4_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOLFIL_4_44.ecpDrop(oSource)
    this.Parent.oContained.link_4_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOLFIL_4_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_COLL','*','TCCODICE',cp_AbsName(this.parent,'oCOLFIL_4_44'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Codici tipologie colli",'',this.parent.oContained
  endproc
  proc oCOLFIL_4_44.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_COLFIL
     i_obj.ecpSave()
  endproc

  add object oCONFIL_4_45 as StdField with uid="SEPUWZSWSC",rtseq=120,rtrep=.f.,;
    cFormVar = "w_CONFIL", cQueryName = "CONFIL",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice confezione di selezione",;
    HelpContextID = 34555866,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=189, Top=441, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="TIPICONF", cZoomOnZoom="GSAR_ATC", oKey_1_1="TCCODICE", oKey_1_2="this.w_CONFIL"

  func oCONFIL_4_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPSEL<>'S')
    endwith
   endif
  endfunc

  func oCONFIL_4_45.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oCONFIL_4_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONFIL_4_45.ecpDrop(oSource)
    this.Parent.oContained.link_4_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONFIL_4_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIPICONF','*','TCCODICE',cp_AbsName(this.parent,'oCONFIL_4_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATC',"Codici tipi confezioni",'',this.parent.oContained
  endproc
  proc oCONFIL_4_45.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_CONFIL
     i_obj.ecpSave()
  endproc

  add object oCOLDES_4_46 as StdField with uid="NBFQVIXYVZ",rtseq=121,rtrep=.f.,;
    cFormVar = "w_COLDES", cQueryName = "COLDES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 189884378,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=345, Top=413, InputMask=replicate('X',35)

  func oCOLDES_4_46.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCONDES_4_47 as StdField with uid="XHRUZOIOMR",rtseq=122,rtrep=.f.,;
    cFormVar = "w_CONDES", cQueryName = "CONDES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 189876186,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=345, Top=441, InputMask=replicate('X',35)

  func oCONDES_4_47.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oREPFIL_4_48 as StdField with uid="UQMNHIQAHQ",rtseq=165,rtrep=.f.,;
    cFormVar = "w_REPFIL", cQueryName = "REPFIL",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice reparto",;
    HelpContextID = 34549994,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=189, Top=469, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="REP_ARTI", cZoomOnZoom="GSPS_ARE", oKey_1_1="RECODREP", oKey_1_2="this.w_REPFIL"

  func oREPFIL_4_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_GPOS='S')
    endwith
   endif
  endfunc

  func oREPFIL_4_48.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  func oREPFIL_4_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_48('Part',this)
    endwith
    return bRes
  endfunc

  proc oREPFIL_4_48.ecpDrop(oSource)
    this.Parent.oContained.link_4_48('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oREPFIL_4_48.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REP_ARTI','*','RECODREP',cp_AbsName(this.parent,'oREPFIL_4_48'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPS_ARE',"Reparti",'',this.parent.oContained
  endproc
  proc oREPFIL_4_48.mZoomOnZoom
    local i_obj
    i_obj=GSPS_ARE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RECODREP=this.parent.oContained.w_REPFIL
     i_obj.ecpSave()
  endproc

  add object oREPDES_4_49 as StdField with uid="UNTSKOGGHC",rtseq=166,rtrep=.f.,;
    cFormVar = "w_REPDES", cQueryName = "REPDES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 189870314,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=345, Top=469, InputMask=replicate('X',35)

  func oREPDES_4_49.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oStr_4_6 as StdString with uid="PHXWTWHRDJ",Visible=.t., Left=5, Top=131,;
    Alignment=1, Width=180, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_4_7 as StdString with uid="KXXUPOONKM",Visible=.t., Left=5, Top=159,;
    Alignment=1, Width=180, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_4_16 as StdString with uid="EJCXMUIDIK",Visible=.t., Left=5, Top=187,;
    Alignment=1, Width=180, Height=18,;
    Caption="Codice IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_4_17 as StdString with uid="QPPQEBMYFK",Visible=.t., Left=5, Top=215,;
    Alignment=1, Width=180, Height=18,;
    Caption="Gr.merceologico:"  ;
  , bGlobalFont=.t.

  func oStr_4_17.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_22 as StdString with uid="BFATWZNSLW",Visible=.t., Left=5, Top=245,;
    Alignment=1, Width=180, Height=18,;
    Caption="Cat.contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_4_23 as StdString with uid="CUAXWLJUPY",Visible=.t., Left=5, Top=273,;
    Alignment=1, Width=180, Height=18,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  func oStr_4_23.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_28 as StdString with uid="UOILFNIOXV",Visible=.t., Left=5, Top=301,;
    Alignment=1, Width=180, Height=18,;
    Caption="Cat.omogenea:"  ;
  , bGlobalFont=.t.

  func oStr_4_28.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_29 as StdString with uid="IUASSYVPKA",Visible=.t., Left=5, Top=329,;
    Alignment=1, Width=180, Height=18,;
    Caption="Produttore:"  ;
  , bGlobalFont=.t.

  func oStr_4_29.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_34 as StdString with uid="NHIUVTQTZB",Visible=.t., Left=5, Top=357,;
    Alignment=1, Width=180, Height=18,;
    Caption="Fornitore abituale:"  ;
  , bGlobalFont=.t.

  func oStr_4_34.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_35 as StdString with uid="DMSIMQDKTU",Visible=.t., Left=5, Top=385,;
    Alignment=1, Width=180, Height=18,;
    Caption="Marca:"  ;
  , bGlobalFont=.t.

  func oStr_4_35.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_42 as StdString with uid="KCGICDYKJY",Visible=.t., Left=5, Top=413,;
    Alignment=1, Width=180, Height=18,;
    Caption="Tipo collo:"  ;
  , bGlobalFont=.t.

  func oStr_4_42.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_43 as StdString with uid="MZBTQCFRXA",Visible=.t., Left=5, Top=441,;
    Alignment=1, Width=180, Height=18,;
    Caption="Confezione:"  ;
  , bGlobalFont=.t.

  func oStr_4_43.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_50 as StdString with uid="TMRPMZQBFI",Visible=.t., Left=5, Top=469,;
    Alignment=1, Width=180, Height=18,;
    Caption="Reparto:"  ;
  , bGlobalFont=.t.

  func oStr_4_50.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S')
    endwith
  endfunc

  add object oStr_4_51 as StdString with uid="XDSYKLFPMZ",Visible=.t., Left=22, Top=20,;
    Alignment=1, Width=163, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_4_51.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_52 as StdString with uid="GXHLJUJXIN",Visible=.t., Left=5, Top=104,;
    Alignment=1, Width=180, Height=18,;
    Caption="Filtro obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_4_53 as StdString with uid="ULJBAZMVZP",Visible=.t., Left=69, Top=76,;
    Alignment=1, Width=116, Height=18,;
    Caption="Provenienza:"  ;
  , bGlobalFont=.t.

  func oStr_4_53.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_4_54 as StdString with uid="GVKNOWZSTN",Visible=.t., Left=33, Top=48,;
    Alignment=1, Width=152, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_kva','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
