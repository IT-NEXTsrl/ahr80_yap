* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bim                                                        *
*              Import da documenti collegati                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_177]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2014-09-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bim",oParentObject)
return(i_retval)

define class tgsve_bim as StdBatch
  * --- Local variables
  w_MVCODVAL = space(3)
  w_MVRIFDIC = space(10)
  w_MVCODIVE = space(5)
  w_FLIMPA = space(1)
  w_MVCODPAG = space(5)
  w_MVDATDIV = ctod("  /  /  ")
  w_MVSCOCL1 = 0
  w_MVSCOCL2 = 0
  w_MVSCOPAG = 0
  w_MVCODBAN = space(10)
  w_MVCODBA2 = space(15)
  w_MVCODAGE = space(5)
  w_MVCODAG2 = space(5)
  w_FLIMAC = space(1)
  w_MVCODVET = space(5)
  w_MVCODVE2 = space(5)
  w_MVCODVE3 = space(5)
  w_MVCODPOR = space(1)
  w_MVCODSPE = space(3)
  w_MVCONCON = space(1)
  w_MVCODDES = space(5)
  w_MVCODSED = space(5)
  w_MVCODORN = space(15)
  w_MVCLADOC = space(2)
  w_MVCODCON = space(15)
  w_OKSCF = .f.
  w_OKACC = .f.
  w_OKACO = .f.
  w_VARVAL = space(1)
  w_MVIVAINC = space(5)
  w_MVIVAIMB = space(5)
  w_MVIVATRA = space(5)
  w_MVIVABOL = space(5)
  w_MVIVACAU = space(5)
  w_PADRE = .NULL.
  w_GEST = .NULL.
  w_OBJCOLUM = .NULL.
  w_OBJCTSOU = space(10)
  w_CONTA = 0
  w_FIELDS = space(254)
  w_WHERE = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia Ricerca da Import Documenti Collegati (da GSVE_KIM)
    * --- Variabili dal Documento di Destinazione
    * --- Generali
    * --- Pagamento
    * --- Accompagnatori
    * --- Codici iva spese
    WITH This.oParentObject.oParentObject
    this.w_MVCODVAL = .w_MVCODVAL
    this.w_MVRIFDIC = .w_MVRIFDIC
    this.w_MVCODIVE = .w_MVCODIVE
    this.w_FLIMPA = .w_FLIMPA
    this.w_MVCODPAG = .w_MVCODPAG
    this.w_MVDATDIV = .w_MVDATDIV
    this.w_MVSCOCL1 = .w_MVSCOCL1
    this.w_MVSCOCL2 = .w_MVSCOCL2
    this.w_MVSCOPAG = .w_MVSCOPAG
    this.w_MVCODBAN = .w_MVCODBAN
    this.w_MVCODBA2 = .w_MVCODBA2
    this.w_MVCODAGE = .w_MVCODAGE
    this.w_MVCODAG2 = .w_MVCODAG2
    this.w_FLIMAC = .w_FLIMAC
    this.w_MVCODVET = .w_MVCODVET
    this.w_MVCODVE2 = .w_MVCODVE2
    this.w_MVCODVE3 = .w_MVCODVE3
    this.w_MVCODPOR = .w_MVCODPOR
    this.w_MVCODSPE = .w_MVCODSPE
    this.w_MVCONCON = .w_MVCONCON
    this.w_MVCODDES = .w_MVCODDES
    this.w_MVCODSED = .w_MVCODSED
    this.w_MVIVAINC = .w_MVIVAINC
    this.w_MVIVAIMB = .w_MVIVAIMB
    this.w_MVIVATRA = .w_MVIVATRA
    this.w_MVIVABOL = .w_MVIVABOL
    this.w_MVCODORN = .w_MVCODORN
    this.w_MVCLADOC = .w_MVCLADOC
    this.w_MVCODCON = .w_MVCODCON
    this.w_VARVAL = .w_VARVAL
    this.w_MVIVACAU = .w_MVIVACAU
    ENDWITH
    this.w_PADRE = This.oParentObject
    * --- Riferimento alla gestione documenti (GSVE_MDV,GSOR_MDV,GSAC_MDV)
    this.w_GEST = This.oParentObject.oParentObject
    if Empty(this.w_GEST.w_Cur_Ev_Row)
      * --- L_Cur_Ev_Row contiene il nome del cursore utilizzato per contenere le righe
      *     evase ma non importate. Viene valorizzata solo se non lo � gi�. 
      *     Nel caso si preme pi� volte il bottone importa, il nome del cursore deve rimanere sempre lo stesso
      this.w_GEST.w_Cur_Ev_Row = Sys(2015)
    endif
    if Empty(this.w_GEST.w_Cur_Rag_Row)
      * --- L_Cur_Rag_Row contiene il nome del cursore utilizzato per contenere le righe
      *     Raggruppate in Importazione. Viene valorizzata solo se non lo � gi�. 
      *     Nel caso si preme pi� volte il bottone importa, il nome del cursore deve rimanere sempre lo stesso
      this.w_GEST.w_Cur_Rag_Row = Sys(2015)
    endif
    L_Cur_Ev_Row=this.w_GEST.w_Cur_Ev_Row
    L_Cur_Rag_Row=this.w_GEST.w_Cur_Rag_Row
    * --- Verifica se sono all inizio del Caricamento del Documento
    this.oParentObject.w_RECTRS = this.w_GEST.NumRow()
    * --- Lancia la Query
    ah_Msg("Ricerca documenti da importare...")
    if Not ( isalt() and this.w_PADRE.Currentevent = "Blank")
      * --- Nel caso di alterego all'avvio non popolo la zoom di sinistra
      this.w_PADRE.NotifyEvent("Calcola")     
    endif
    local L_MACRO
    this.w_CONTA = this.oParentObject.w_ZOOMMAST.Grd.ColumnCount 
    do while this.w_CONTA > 0
      L_MACRO = "this.oParentObject.w_ZOOMMAST.Grd.Column"+ALLTRIM(STR(this.w_CONTA))+".ControlSource"
      this.w_OBJCTSOU = ALLTRIM( &L_MACRO )
      if !type("this.oParentObject.w_ZOOMMAST.Grd.Column" +ALLTRIM(STR(this.w_CONTA))+".aRefHeaders")="O"
        * --- Tema standard in configurazione interfaccia
        if type("this.oParentObject.w_ZOOMMAST.Grd.Column" +ALLTRIM(STR(this.w_CONTA))+".hdr")="O"
          L_MACRO = "this.oParentObject.w_ZOOMMAST.Grd.Column"+ALLTRIM(STR(this.w_CONTA))+".hdr"
        endif
      else
        L_MACRO = "this.oParentObject.w_ZOOMMAST.Grd.Column"+ALLTRIM(STR(this.w_CONTA))+".aRefHeaders"
      endif
      this.w_OBJCOLUM = &L_MACRO
      if INLIST( this.w_OBJCTSOU, "MVAGG_01", "MVAGG_02", "MVAGG_03", "MVAGG_04", "MVAGG_05", "MVAGG_06" )
        L_MACRO = "this.oParentObject.w_DACAM_"+RIGHT( this.w_OBJCTSOU , 2)
        this.w_OBJCOLUM.Caption = EVL( NVL( &L_MACRO, " " ), this.w_OBJCOLUM.Caption )
      endif
      this.w_CONTA = this.w_CONTA - 1
    enddo
    release L_MACRO
    WAIT CLEAR
    * --- Lettura Righe Documento
    * --- Eseguo raggruppamento per sommare quantit� righe collegate alla stessa riga documento di origine
    *     In fondo creo anche i campi relativi a Data Documento, Numero documento e alfa poich� mi servono come Ordinamento 
    *     in righedoc nel Gsve_Bi3
    this.w_FIELDS = "MAX(IIF(t_MVFLERIF='S', 1, 0)) AS XCHK ,Sum(t_MVQTAMOV) AS MVQTAMOV,IIF(t_MVTIPRIG='F',1.000,SUM(NVL(t_MVQTAIMP, 0)-NVL(MVQTAIMP, 0))) AS OLQTAEVA,"
    this.w_FIELDS = this.w_FIELDS + "SUM(t_MVQTAEVA*0) AS MVQTAEVA,IIF(t_MVTIPRIG='F',sum(NVL(t_MVPREZZO,0)*IIF("+ Cp_ToStrOdbc( this.w_MVCODVAL ) +"=g_PERVAL, t_CAORIF , 1)),t_MVPREZZO*0) AS OLIMPEVA,SUM(t_MVPREZZO*0) AS MVIMPEVA,"
    this.w_FIELDS = this.w_FIELDS + "t_MVSERRIF AS MVSERIAL,t_MVROWRIF AS CPROWNUM,MAX(t_CPROWORD) AS CPROWORD,max('S') AS FLDOCU, max(IIF(t_MVFLARIF=' ','S',' ')) AS FLINTE,max(IIF(t_MVFLARIF='-','S',' ')) AS FLACCO,"
    this.w_FIELDS = this.w_FIELDS + "max(t_MVTIPRIG) AS MVTIPRIG,sum(t_DOIMPEVA) AS DOIMPEVA,sum(t_DOQTAEV1) AS DOQTAEV1,cp_ROUND(t_MVPREZZO-t_MVIMPEVA,5) AS VISPRE,"
    this.w_FIELDS = this.w_FIELDS + "min(t_MVSCONT1) As MVSCONT1, min(t_MVSCONT2) As MVSCONT2, min(t_MVSCONT3) As MVSCONT3, min(t_MVSCONT4) As MVSCONT4, min(t_MVUNIMIS) As MVUNIMIS, "
    this.w_FIELDS = this.w_FIELDS + "min(t_MVCODICE) As MVCODICE, min(t_MVCODATT) As MVCODATT, min(t_MVCODCOM) As MVCODCOM, min(t_MVCODCEN) As MVCODCEN, ' ' As FLGROR, "
    this.w_FIELDS = this.w_FIELDS + " cp_CharToDate('    -  -  ') AS MVDATDOC, 0 AS MVNUMDOC, Space(10) AS MVALFDOC  , t_MVDATEVA As MVDATEVA, t_MVCODART As MVCODART, t_MVCODLOT As MVCODLOT, t_MVCODUBI As MVCODUBI, "
    this.w_FIELDS = this.w_FIELDS + " t_MVCODMAG As MVCODMAG, t_MVFLRISE As MVFLRISE, t_MVQTAUM1 As MVQTAUM1, max(t_MVFLERIF) AS MVFLERIF, "+ Cp_ToStrOdbc( this.w_MVCLADOC ) +" As MVCLADOC,"+ Cp_ToStrOdbc( this.oParentObject.w_MVFLVEAC ) +" As MVFLVEAC,"
    this.w_FIELDS = this.w_FIELDS + Cp_ToStrOdbc( this.oParentObject.w_MVTIPDOC ) +" As MVTIPDOC  " + ",' ' As PRSERIAL,t_MVCODIVA as ARCODIVA,t_MVRIFPRE AS MVRIFPRE,t_MVNUMRIF AS MVNUMRIF, "
    this.w_FIELDS = this.w_FIELDS + Cp_ToStrOdbc( this.oParentObject.oParentObject.w_MVAGG_01 ) +" As MVAGG_01, " + Cp_ToStrOdbc( this.oParentObject.oParentObject.w_MVAGG_02 ) +" As MVAGG_02, " + Cp_ToStrOdbc( this.oParentObject.oParentObject.w_MVAGG_03 ) +" As MVAGG_03, "
    this.w_FIELDS = this.w_FIELDS + Cp_ToStrOdbc( this.oParentObject.oParentObject.w_MVAGG_04 ) +" As MVAGG_04, "
    this.w_FIELDS = this.w_FIELDS + IIF( EMPTY( NVL( this.oParentObject.oParentObject.w_MVAGG_05, "" ) ) , "cp_CharToDate('  -  -    ')", "cp_CharToDate('"+DTOC(this.oParentObject.oParentObject.w_MVAGG_05) + "')" ) + " As MVAGG_05, "
    this.w_FIELDS = this.w_FIELDS + IIF( EMPTY( NVL(this.oParentObject.oParentObject.w_MVAGG_06, " " ) ), "cp_CharToDate('  -  -    ')" , "cp_CharToDate('" + DTOC(this.oParentObject.oParentObject.w_MVAGG_06) + "')" ) +" As MVAGG_06,' ' As PRSERAGG "
    this.w_WHERE = "(NOT EMPTY(NVL(t_MVSERRIF,' '))) AND NVL(t_MVROWRIF, 0)<>0 AND NOT DELETED() AND NVL(MVQTAIMP,0) <> NVL(t_MVQTAIMP,0)"
    this.w_GEST.Exec_Select("RigheDoc", this.w_FIELDS , this.w_WHERE , "t_MVSERRIF,t_MVROWRIF" , "t_MVSERRIF,t_MVROWRIF" , "")     
    * --- Aggiorna sul Master le Righe gia' presenti sul Documento (non deve modificarle)
    *     utilizzo una variabile (L_NC) per costruire tramite macro la frase di filtro sulla clausola Exist
    L_NC=this.oParentObject.w_ZOOMMAST.cCursor
     
 UPDATE ( this.oParentObject.w_ZOOMMAST.cCursor ) SET FLDOCU = "S" WHERE EXIST ; 
 (SELECT 1 FROM RigheDoc WHERE &L_NC..MVSERIAL = RigheDoc.MVSERIAL)
    * --- Se esiste cursore di righe evase senza import (per precedente pressione del tasto Import)
    *     segno anche se ho evaso una riga di un documento senza importarla
    if isalt()
       
 UPDATE ( this.oParentObject.w_ZOOMMAST.cCursor ) SET FLDOCU = "S" WHERE FLDOCU<> "S" AND EXIST ; 
 (SELECT 1 FROM RigheDoc WHERE &L_NC..PRSERIAL = RigheDoc.MVSERIAL AND RigheDoc.CPROWNUM=-1 )
    endif
    if USED (L_Cur_Ev_Row)
       
 UPDATE ( this.oParentObject.w_ZOOMMAST.cCursor ) SET FLDOCU = "S" WHERE EXIST ; 
 (SELECT 1 FROM ( L_Cur_Ev_Row ) WHERE &L_NC..MVSERIAL = &L_Cur_Ev_Row..RESERRIF)
    endif
    * --- Se esiste cursore di righe Raggruppate in Importazione (per precedente pressione del tasto Import)
    *     segno anche se la riga � gi� stata importata Raggruppata
    if USED (L_Cur_Rag_Row)
       
 UPDATE ( this.oParentObject.w_ZOOMMAST.cCursor ) SET FLDOCU = "S" WHERE EXIST ; 
 (SELECT 1 FROM ( L_Cur_Rag_Row ) WHERE &L_NC..MVSERIAL = &L_Cur_Rag_Row..RESERRIF)
    endif
    SELECT ( this.oParentObject.w_ZOOMMAST.cCursor )
    if this.oParentObject.w_FLTRASF="S"
      * --- Eseguo Filtro solo documenti gi� trasferiti
      DELETE FROM ( this.oParentObject.w_ZOOMMAST.cCursor ) WHERE NVL(FLDOCU,"N") <> "S"
      this.oParentObject.w_ZOOMMAST.Grd.Refresh()     
    endif
    this.w_OKSCF = .F.
    this.w_OKACC = .F.
    this.w_OKACO = .F.
    * --- Mi riposiziono sulla prima riga del Zoom Master in modo da poter eseguire 
    *     l'interrogazione del dettaglio (CalcRig) sul primo documento
     
 SELECT ( this.oParentObject.w_ZOOMMAST.cCursor ) 
 GO TOP
    this.w_PADRE.NotifyEvent("CalcRig")     
    this.w_PADRE.oPgFrm.ActivePage = 1
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
