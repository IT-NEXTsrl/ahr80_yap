* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_ktp                                                        *
*              Test invio mail                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-03-17                                                      *
* Last revis.: 2014-12-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_ktp",oParentObject))

* --- Class definition
define class tgsut_ktp as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 586
  Height = 348+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-22"
  HelpContextID=186039447
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  ACC_MAIL_IDX = 0
  cPrg = "gsut_ktp"
  cComment = "Test invio mail"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AMSERIAL = space(5)
  w_AM_EMAIL = space(254)
  w_TRINOEML = space(254)
  w_TR_CCEML = space(254)
  w_TRSUBEML = space(254)
  w_TRTXTEML = space(0)
  w_MSG = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_ktpPag1","gsut_ktp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati invio mail")
      .Pages(2).addobject("oPag","tgsut_ktpPag2","gsut_ktp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Log")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTRINOEML_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ACC_MAIL'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AMSERIAL=space(5)
      .w_AM_EMAIL=space(254)
      .w_TRINOEML=space(254)
      .w_TR_CCEML=space(254)
      .w_TRSUBEML=space(254)
      .w_TRTXTEML=space(0)
      .w_MSG=space(0)
      .w_AMSERIAL=oParentObject.w_AMSERIAL
      .w_AM_EMAIL=oParentObject.w_AM_EMAIL
          .DoRTCalc(1,2,.f.)
        .w_TRINOEML = .w_AM_EMAIL
          .DoRTCalc(4,4,.f.)
        .w_TRSUBEML = "Test invio mail da " + .w_AM_EMAIL
        .w_TRTXTEML = "Test invio mail da " + .w_AM_EMAIL
    endwith
    this.DoRTCalc(7,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_4.enabled = this.oPgFrm.Page2.oPag.oBtn_2_4.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_AMSERIAL=.w_AMSERIAL
      .oParentObject.w_AM_EMAIL=.w_AM_EMAIL
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTRINOEML_1_3.value==this.w_TRINOEML)
      this.oPgFrm.Page1.oPag.oTRINOEML_1_3.value=this.w_TRINOEML
    endif
    if not(this.oPgFrm.Page1.oPag.oTR_CCEML_1_4.value==this.w_TR_CCEML)
      this.oPgFrm.Page1.oPag.oTR_CCEML_1_4.value=this.w_TR_CCEML
    endif
    if not(this.oPgFrm.Page1.oPag.oTRSUBEML_1_6.value==this.w_TRSUBEML)
      this.oPgFrm.Page1.oPag.oTRSUBEML_1_6.value=this.w_TRSUBEML
    endif
    if not(this.oPgFrm.Page1.oPag.oTRTXTEML_1_8.value==this.w_TRTXTEML)
      this.oPgFrm.Page1.oPag.oTRTXTEML_1_8.value=this.w_TRTXTEML
    endif
    if not(this.oPgFrm.Page2.oPag.oMSG_2_1.value==this.w_MSG)
      this.oPgFrm.Page2.oPag.oMSG_2_1.value=this.w_MSG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TRINOEML))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRINOEML_1_3.SetFocus()
            i_bnoObbl = !empty(.w_TRINOEML)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut_ktp
      if i_bRes
         Dimension aArrAttach(1)
         Local nritorno
         aArrAttach[1]=' '
         nritorno=smtpssl(This.w_AMSERIAL, "", 0, This.w_AM_EMAIL, This.w_TRINOEML,;
                  This.w_TR_CCEML,'', This.w_TRSUBEML, This.w_TRTXTEML,@aArrAttach, This, "","","","")
         Release aArrAttach
         if nritorno=-1
            i_bnoChk=.F.
            i_bRes=.F.
            i_cErrorMsg=Ah_MsgFormat("Test invio mail fallito")
            This.opgfrm.activepage=2
          else
            Ah_errormsg("Test invio mail superato",'!')
          endif
       endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_ktpPag1 as StdContainer
  Width  = 582
  height = 348
  stdWidth  = 582
  stdheight = 348
  resizeXpos=418
  resizeYpos=162
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTRINOEML_1_3 as StdField with uid="PZQJXXTYKW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TRINOEML", cQueryName = "TRINOEML",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo mail del destinatario",;
    HelpContextID = 178674558,;
   bGlobalFont=.t.,;
    Height=21, Width=494, Left=81, Top=9, InputMask=replicate('X',254)

  add object oTR_CCEML_1_4 as StdField with uid="LYFAGTOTHN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TR_CCEML", cQueryName = "TR_CCEML",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali indirizzi a cui inviare copia della fattura",;
    HelpContextID = 191888254,;
   bGlobalFont=.t.,;
    Height=21, Width=494, Left=81, Top=35, InputMask=replicate('X',254)

  add object oTRSUBEML_1_6 as StdField with uid="QGGXBZNULM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TRSUBEML", cQueryName = "TRSUBEML",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto della mail",;
    HelpContextID = 191806334,;
   bGlobalFont=.t.,;
    Height=21, Width=494, Left=81, Top=61, InputMask=replicate('X',254)

  add object oTRTXTEML_1_8 as StdMemo with uid="ILXXDBXNBB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_TRTXTEML", cQueryName = "TRTXTEML",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Testo della mail",;
    HelpContextID = 172731262,;
   bGlobalFont=.t.,;
    Height=200, Width=494, Left=81, Top=86, bdisablefrasimodello=.t.


  add object oBtn_1_10 as StdButton with uid="MBXAIGGBZE",left=474, top=292, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 186060006;
    , caption='\<OK';
  , bGlobalFont=.t.

    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="ISDICPRYHO",left=527, top=292, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 28111098;
    , Caption='\<Esci';
  , bGlobalFont=.t.

    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_5 as StdString with uid="CGDXQFIXEN",Visible=.t., Left=-1, Top=37,;
    Alignment=1, Width=77, Height=18,;
    Caption="Cc:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="RDGKPSZQIJ",Visible=.t., Left=-1, Top=63,;
    Alignment=1, Width=77, Height=18,;
    Caption="Oggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="JCDRMQTGUY",Visible=.t., Left=-1, Top=88,;
    Alignment=1, Width=77, Height=18,;
    Caption="Testo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="NYNUORBCLN",Visible=.t., Left=-1, Top=11,;
    Alignment=1, Width=77, Height=18,;
    Caption="Destinatario:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsut_ktpPag2 as StdContainer
  Width  = 582
  height = 348
  stdWidth  = 582
  stdheight = 348
  resizeXpos=452
  resizeYpos=223
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMSG_2_1 as StdMemo with uid="SDVKETLQKB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MSG", cQueryName = "MSG",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 186352838,;
   bGlobalFont=.t.,;
    Height=278, Width=570, Left=5, Top=12, tabstop = .f., readonly = .t.


  add object oBtn_2_2 as StdButton with uid="XJTSIFJRQC",left=10, top=295, width=48,height=45,;
    CpPicture="COPY.BMP", caption="", nPag=2;
    , ToolTipText = "copia log negli appunti per incollarlo come testo";
    , HelpContextID = 186240470;
    , caption='\<Copia';
  , bGlobalFont=.t.

    proc oBtn_2_2.Click()
      with this.Parent.oContained
        _cliptext=.w_msg
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_3 as StdButton with uid="XEIGWUJKFC",left=474, top=295, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 186060006;
    , caption='\<OK';
  , bGlobalFont=.t.

    proc oBtn_2_3.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_4 as StdButton with uid="PMNHDVHEUZ",left=527, top=295, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Esci";
    , HelpContextID = 28111098;
    , Caption='\<Esci';
  , bGlobalFont=.t.

    proc oBtn_2_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_ktp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
