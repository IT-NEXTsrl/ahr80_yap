* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bc1                                                        *
*              Forzo output utente                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-03-16                                                      *
* Last revis.: 2008-08-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bc1",oParentObject)
return(i_retval)

define class tgste_bc1 as StdBatch
  * --- Local variables
  w_NOMPRG = space(8)
  w_UTE = 0
  w_AZI = space(5)
  w_objCtrl = .NULL.
  * --- WorkFile variables
  OUT_PUTS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- FORZO L'OUTPUT UTENTE. LANCIATO DA GSTE_SCA
    this.w_NOMPRG = "GSTE_SCA"
    if this.oParentObject.w_FLANSCA="S"
      * --- Select from OUT_PUTS
      i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2],.t.,this.OUT_PUTS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" OUT_PUTS ";
            +" where OUNOMPRG="+cp_ToStrODBC(this.w_NOMPRG)+" AND OUROWNUM=2";
             ,"_Curs_OUT_PUTS")
      else
        select * from (i_cTable);
         where OUNOMPRG=this.w_NOMPRG AND OUROWNUM=2;
          into cursor _Curs_OUT_PUTS
      endif
      if used('_Curs_OUT_PUTS')
        select _Curs_OUT_PUTS
        locate for 1=1
        do while not(eof())
        this.w_UTE = NVL(OUCODUTE,0)
        this.w_AZI = NVL(OUCODAZI, " ")
        if (this.w_UTE=i_CODUTE OR this.w_UTE=0) AND (this.w_AZI=i_CODAZI OR EMPTY(this.w_AZI))
          this.oParentObject.w_ODES = NVL(OUDESCRI, SPACE(50))
          this.oParentObject.w_OREP = NVL(OUNOMREP, SPACE(50))
          this.oParentObject.w_OQRY = NVL(OUNOMQUE, SPACE(50))
          this.oParentObject.w_ONUME = NVL(OUROWNUM, 0)
          this.w_objCtrl = this.oParentObject.getCtrl("w_Output")
          this.w_objCtrl.ListIndex = 2
          this.w_objCtrl.nDefItem = 2
        endif
          select _Curs_OUT_PUTS
          continue
        enddo
        use
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OUT_PUTS'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_OUT_PUTS')
      use in _Curs_OUT_PUTS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
