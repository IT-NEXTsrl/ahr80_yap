* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_agc                                                        *
*              Comunicazioni generate                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-30                                                      *
* Last revis.: 2009-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_agc"))

* --- Class definition
define class tgscg_agc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 461
  Height = 198+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-11-16"
  HelpContextID=42607977
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  DIC_GENE_IDX = 0
  CPUSERS_IDX = 0
  cFile = "DIC_GENE"
  cKeySelect = "GCSERIAL"
  cKeyWhere  = "GCSERIAL=this.w_GCSERIAL"
  cKeyWhereODBC = '"GCSERIAL="+cp_ToStrODBC(this.w_GCSERIAL)';

  cKeyWhereODBCqualified = '"DIC_GENE.GCSERIAL="+cp_ToStrODBC(this.w_GCSERIAL)';

  cPrg = "gscg_agc"
  cComment = "Comunicazioni generate"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_GCSERIAL = 0
  w_GCANNO = space(4)
  w_GCMESE = 0
  w_GDDATAEL = ctod('  /  /  ')
  w_GSFILEGE = space(254)
  w_GCDATAIN = ctod('  /  /  ')
  w_GCDATAFI = ctod('  /  /  ')
  w_GSNUMRIG = 0
  w_GCFLCORR = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_GCDESUTE = space(20)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_GCSERIAL = this.W_GCSERIAL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DIC_GENE','gscg_agc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_agcPag1","gscg_agc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Comunicazioni generate")
      .Pages(1).HelpContextID = 25168748
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CPUSERS'
    this.cWorkTables[2]='DIC_GENE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIC_GENE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIC_GENE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_GCSERIAL = NVL(GCSERIAL,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DIC_GENE where GCSERIAL=KeySet.GCSERIAL
    *
    i_nConn = i_TableProp[this.DIC_GENE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_GENE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIC_GENE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIC_GENE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIC_GENE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GCSERIAL',this.w_GCSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_GCDESUTE = space(20)
        .w_GCSERIAL = NVL(GCSERIAL,0)
        .op_GCSERIAL = .w_GCSERIAL
        .w_GCANNO = NVL(GCANNO,space(4))
        .w_GCMESE = NVL(GCMESE,0)
        .w_GDDATAEL = NVL(cp_ToDate(GDDATAEL),ctod("  /  /  "))
        .w_GSFILEGE = NVL(GSFILEGE,space(254))
        .w_GCDATAIN = NVL(cp_ToDate(GCDATAIN),ctod("  /  /  "))
        .w_GCDATAFI = NVL(cp_ToDate(GCDATAFI),ctod("  /  /  "))
        .w_GSNUMRIG = NVL(GSNUMRIG,0)
        .w_GCFLCORR = NVL(GCFLCORR,space(1))
        .w_UTCC = NVL(UTCC,0)
          .link_1_17('Load')
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DIC_GENE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_GCSERIAL = 0
      .w_GCANNO = space(4)
      .w_GCMESE = 0
      .w_GDDATAEL = ctod("  /  /  ")
      .w_GSFILEGE = space(254)
      .w_GCDATAIN = ctod("  /  /  ")
      .w_GCDATAFI = ctod("  /  /  ")
      .w_GSNUMRIG = 0
      .w_GCFLCORR = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_GCDESUTE = space(20)
      if .cFunction<>"Filter"
        .DoRTCalc(1,10,.f.)
          if not(empty(.w_UTCC))
          .link_1_17('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIC_GENE')
    this.DoRTCalc(11,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIC_GENE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_GENE_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEDIGE","i_codazi,w_GCSERIAL")
      .op_codazi = .w_codazi
      .op_GCSERIAL = .w_GCSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGCANNO_1_2.enabled = i_bVal
      .Page1.oPag.oGCMESE_1_4.enabled = i_bVal
      .Page1.oPag.oGDDATAEL_1_6.enabled = i_bVal
      .Page1.oPag.oGSFILEGE_1_8.enabled = i_bVal
      .Page1.oPag.oGCDATAIN_1_10.enabled = i_bVal
      .Page1.oPag.oGCDATAFI_1_12.enabled = i_bVal
      .Page1.oPag.oGSNUMRIG_1_14.enabled = i_bVal
      .Page1.oPag.oGCFLCORR_1_16.enabled = i_bVal
      .Page1.oPag.oBtn_1_23.enabled = .Page1.oPag.oBtn_1_23.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'DIC_GENE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIC_GENE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GCSERIAL,"GCSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GCANNO,"GCANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GCMESE,"GCMESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GDDATAEL,"GDDATAEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GSFILEGE,"GSFILEGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GCDATAIN,"GCDATAIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GCDATAFI,"GCDATAFI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GSNUMRIG,"GSNUMRIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GCFLCORR,"GCFLCORR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIC_GENE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_GENE_IDX,2])
    i_lTable = "DIC_GENE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DIC_GENE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIC_GENE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_GENE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DIC_GENE_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEDIGE","i_codazi,w_GCSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DIC_GENE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIC_GENE')
        i_extval=cp_InsertValODBCExtFlds(this,'DIC_GENE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(GCSERIAL,GCANNO,GCMESE,GDDATAEL,GSFILEGE"+;
                  ",GCDATAIN,GCDATAFI,GSNUMRIG,GCFLCORR,UTCC"+;
                  ",UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_GCSERIAL)+;
                  ","+cp_ToStrODBC(this.w_GCANNO)+;
                  ","+cp_ToStrODBC(this.w_GCMESE)+;
                  ","+cp_ToStrODBC(this.w_GDDATAEL)+;
                  ","+cp_ToStrODBC(this.w_GSFILEGE)+;
                  ","+cp_ToStrODBC(this.w_GCDATAIN)+;
                  ","+cp_ToStrODBC(this.w_GCDATAFI)+;
                  ","+cp_ToStrODBC(this.w_GSNUMRIG)+;
                  ","+cp_ToStrODBC(this.w_GCFLCORR)+;
                  ","+cp_ToStrODBCNull(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIC_GENE')
        i_extval=cp_InsertValVFPExtFlds(this,'DIC_GENE')
        cp_CheckDeletedKey(i_cTable,0,'GCSERIAL',this.w_GCSERIAL)
        INSERT INTO (i_cTable);
              (GCSERIAL,GCANNO,GCMESE,GDDATAEL,GSFILEGE,GCDATAIN,GCDATAFI,GSNUMRIG,GCFLCORR,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_GCSERIAL;
                  ,this.w_GCANNO;
                  ,this.w_GCMESE;
                  ,this.w_GDDATAEL;
                  ,this.w_GSFILEGE;
                  ,this.w_GCDATAIN;
                  ,this.w_GCDATAFI;
                  ,this.w_GSNUMRIG;
                  ,this.w_GCFLCORR;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DIC_GENE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_GENE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DIC_GENE_IDX,i_nConn)
      *
      * update DIC_GENE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DIC_GENE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " GCANNO="+cp_ToStrODBC(this.w_GCANNO)+;
             ",GCMESE="+cp_ToStrODBC(this.w_GCMESE)+;
             ",GDDATAEL="+cp_ToStrODBC(this.w_GDDATAEL)+;
             ",GSFILEGE="+cp_ToStrODBC(this.w_GSFILEGE)+;
             ",GCDATAIN="+cp_ToStrODBC(this.w_GCDATAIN)+;
             ",GCDATAFI="+cp_ToStrODBC(this.w_GCDATAFI)+;
             ",GSNUMRIG="+cp_ToStrODBC(this.w_GSNUMRIG)+;
             ",GCFLCORR="+cp_ToStrODBC(this.w_GCFLCORR)+;
             ",UTCC="+cp_ToStrODBCNull(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DIC_GENE')
        i_cWhere = cp_PKFox(i_cTable  ,'GCSERIAL',this.w_GCSERIAL  )
        UPDATE (i_cTable) SET;
              GCANNO=this.w_GCANNO;
             ,GCMESE=this.w_GCMESE;
             ,GDDATAEL=this.w_GDDATAEL;
             ,GSFILEGE=this.w_GSFILEGE;
             ,GCDATAIN=this.w_GCDATAIN;
             ,GCDATAFI=this.w_GCDATAFI;
             ,GSNUMRIG=this.w_GSNUMRIG;
             ,GCFLCORR=this.w_GCFLCORR;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIC_GENE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIC_GENE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DIC_GENE_IDX,i_nConn)
      *
      * delete DIC_GENE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'GCSERIAL',this.w_GCSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIC_GENE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIC_GENE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
          .link_1_17('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEDIGE","i_codazi,w_GCSERIAL")
          .op_GCSERIAL = .w_GCSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(11,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=UTCC
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTCC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTCC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_UTCC);
                   +" and CODE="+cp_ToStrODBC(this.w_UTCC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_UTCC;
                       ,'CODE',this.w_UTCC)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTCC = NVL(_Link_.CODE,0)
      this.w_GCDESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_UTCC = 0
      endif
      this.w_GCDESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTCC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGCANNO_1_2.value==this.w_GCANNO)
      this.oPgFrm.Page1.oPag.oGCANNO_1_2.value=this.w_GCANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oGCMESE_1_4.RadioValue()==this.w_GCMESE)
      this.oPgFrm.Page1.oPag.oGCMESE_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGDDATAEL_1_6.value==this.w_GDDATAEL)
      this.oPgFrm.Page1.oPag.oGDDATAEL_1_6.value=this.w_GDDATAEL
    endif
    if not(this.oPgFrm.Page1.oPag.oGSFILEGE_1_8.value==this.w_GSFILEGE)
      this.oPgFrm.Page1.oPag.oGSFILEGE_1_8.value=this.w_GSFILEGE
    endif
    if not(this.oPgFrm.Page1.oPag.oGCDATAIN_1_10.value==this.w_GCDATAIN)
      this.oPgFrm.Page1.oPag.oGCDATAIN_1_10.value=this.w_GCDATAIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGCDATAFI_1_12.value==this.w_GCDATAFI)
      this.oPgFrm.Page1.oPag.oGCDATAFI_1_12.value=this.w_GCDATAFI
    endif
    if not(this.oPgFrm.Page1.oPag.oGSNUMRIG_1_14.value==this.w_GSNUMRIG)
      this.oPgFrm.Page1.oPag.oGSNUMRIG_1_14.value=this.w_GSNUMRIG
    endif
    if not(this.oPgFrm.Page1.oPag.oGCFLCORR_1_16.RadioValue()==this.w_GCFLCORR)
      this.oPgFrm.Page1.oPag.oGCFLCORR_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUTCC_1_17.value==this.w_UTCC)
      this.oPgFrm.Page1.oPag.oUTCC_1_17.value=this.w_UTCC
    endif
    if not(this.oPgFrm.Page1.oPag.oGCDESUTE_1_22.value==this.w_GCDESUTE)
      this.oPgFrm.Page1.oPag.oGCDESUTE_1_22.value=this.w_GCDESUTE
    endif
    cp_SetControlsValueExtFlds(this,'DIC_GENE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.F.)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impossibile modificare informazioni di generazione")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  func CanEdit()
    local i_res
    i_res=.F.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile modificare informazioni di generazione"))
    endif
    return(i_res)
  func CanAdd()
    local i_res
    i_res=.F.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile aggiungere manualmente informazioni di generazione"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgscg_agcPag1 as StdContainer
  Width  = 457
  height = 198
  stdWidth  = 457
  stdheight = 198
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGCANNO_1_2 as StdField with uid="YFODDWXTWD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GCANNO", cQueryName = "GCANNO",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 240635290,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=121, Top=17, InputMask=replicate('X',4)


  add object oGCMESE_1_4 as StdCombo with uid="SYZBCHRWCP",rtseq=3,rtrep=.f.,left=278,top=18,width=153,height=21;
    , HelpContextID = 135269786;
    , cFormVar="w_GCMESE",RowSource=""+"Gennaio,"+"Febbraio,"+"Marzo,"+"Aprile,"+"Maggio,"+"Giugno,"+"Luglio,"+"Agosto,"+"Settembre,"+"Ottobre,"+"Novembre,"+"Dicembre", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGCMESE_1_4.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,5,;
    iif(this.value =6,6,;
    iif(this.value =7,7,;
    iif(this.value =8,8,;
    iif(this.value =9,9,;
    iif(this.value =10,10,;
    iif(this.value =11,11,;
    iif(this.value =12,12,;
    0)))))))))))))
  endfunc
  func oGCMESE_1_4.GetRadio()
    this.Parent.oContained.w_GCMESE = this.RadioValue()
    return .t.
  endfunc

  func oGCMESE_1_4.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_GCMESE==1,1,;
      iif(this.Parent.oContained.w_GCMESE==2,2,;
      iif(this.Parent.oContained.w_GCMESE==3,3,;
      iif(this.Parent.oContained.w_GCMESE==4,4,;
      iif(this.Parent.oContained.w_GCMESE==5,5,;
      iif(this.Parent.oContained.w_GCMESE==6,6,;
      iif(this.Parent.oContained.w_GCMESE==7,7,;
      iif(this.Parent.oContained.w_GCMESE==8,8,;
      iif(this.Parent.oContained.w_GCMESE==9,9,;
      iif(this.Parent.oContained.w_GCMESE==10,10,;
      iif(this.Parent.oContained.w_GCMESE==11,11,;
      iif(this.Parent.oContained.w_GCMESE==12,12,;
      0))))))))))))
  endfunc

  add object oGDDATAEL_1_6 as StdField with uid="ZLKNLXWHQA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_GDDATAEL", cQueryName = "GDDATAEL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 66806706,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=121, Top=42

  add object oGSFILEGE_1_8 as StdField with uid="OFHCVYPPKG",rtseq=5,rtrep=.f.,;
    cFormVar = "w_GSFILEGE", cQueryName = "GSFILEGE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 126063275,;
   bGlobalFont=.t.,;
    Height=21, Width=309, Left=121, Top=117, InputMask=replicate('X',254)

  add object oGCDATAIN_1_10 as StdField with uid="DKRYKQZSQL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_GCDATAIN", cQueryName = "GCDATAIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 66806452,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=121, Top=67

  add object oGCDATAFI_1_12 as StdField with uid="XOCNUAGCZU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_GCDATAFI", cQueryName = "GCDATAFI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 66806447,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=121, Top=92

  add object oGSNUMRIG_1_14 as StdField with uid="UGVJDNNZGH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_GSNUMRIG", cQueryName = "GSNUMRIG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 190836051,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=121, Top=142, cSayPict='"9999"', cGetPict='"9999"'

  add object oGCFLCORR_1_16 as StdCheck with uid="GYRGSRDTHZ",rtseq=9,rtrep=.f.,left=197, top=142, caption="Generazione correttiva",;
    HelpContextID = 16155320,;
    cFormVar="w_GCFLCORR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGCFLCORR_1_16.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oGCFLCORR_1_16.GetRadio()
    this.Parent.oContained.w_GCFLCORR = this.RadioValue()
    return .t.
  endfunc

  func oGCFLCORR_1_16.SetRadio()
    this.Parent.oContained.w_GCFLCORR=trim(this.Parent.oContained.w_GCFLCORR)
    this.value = ;
      iif(this.Parent.oContained.w_GCFLCORR=='1',1,;
      0)
  endfunc

  add object oUTCC_1_17 as StdField with uid="AUGWQANJZD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_UTCC", cQueryName = "UTCC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37919674,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=121, Top=167, cSayPict='"9999"', cGetPict='"9999"', cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_UTCC", oKey_2_1="CODE", oKey_2_2="this.w_UTCC"

  func oUTCC_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_UTCC)
        bRes2=.link_1_17('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oGCDESUTE_1_22 as StdField with uid="FQPFMMCQIB",rtseq=14,rtrep=.f.,;
    cFormVar = "w_GCDESUTE", cQueryName = "GCDESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 133128875,;
   bGlobalFont=.t.,;
    Height=21, Width=264, Left=165, Top=167, InputMask=replicate('X',20)


  add object oBtn_1_23 as StdButton with uid="ZNCBFEDQXA",left=433, top=117, width=21,height=21,;
    caption="...", nPag=1;
    , HelpContextID = 42406954;
  , bGlobalFont=.t.

    proc oBtn_1_23.Click()
      with this.Parent.oContained
        ShellEx(.w_GSFILEGE,'','')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="ASAYFRSCEH",Visible=.t., Left=6, Top=19,;
    Alignment=1, Width=112, Height=18,;
    Caption="Anno di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="VRBSSOGYKH",Visible=.t., Left=169, Top=19,;
    Alignment=1, Width=108, Height=18,;
    Caption="Mese di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="ZZQQQOHPMT",Visible=.t., Left=1, Top=44,;
    Alignment=1, Width=117, Height=18,;
    Caption="Data elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="CTEHYLYKDW",Visible=.t., Left=36, Top=119,;
    Alignment=1, Width=82, Height=18,;
    Caption="File generato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="FKPWLVSQXY",Visible=.t., Left=7, Top=69,;
    Alignment=1, Width=111, Height=18,;
    Caption="Dati estratti da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="SOVOWQWMFS",Visible=.t., Left=95, Top=94,;
    Alignment=1, Width=23, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="CBDBXTXJHC",Visible=.t., Left=19, Top=144,;
    Alignment=1, Width=99, Height=18,;
    Caption="Righi generati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="IZLJBGDVJP",Visible=.t., Left=24, Top=169,;
    Alignment=1, Width=94, Height=18,;
    Caption="Inserito da:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_agc','DIC_GENE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GCSERIAL=DIC_GENE.GCSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_agc
func ShellEx(cLink,cAction,cParms)
  declare integer ShellExecute in shell32.dll;
    integer nHwnd,;
    string cOperation,;
    string cFileName,;
    string cParms,;
    string cDir,;
    integer nShowCmd

  declare integer FindWindow in win32api;
    string cNull,;
    string cWinName
  if not empty(cLink)
    cAction=iif(empty(cAction),"Open",cAction)
    cParms=iif(empty(cParms),"",cParms)
    return(ShellExecute(FindWindow(0,_screen.caption),cAction,cLink,cParms,tempadhoc(),1))
  endif
endfunc
* --- Fine Area Manuale
