* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_btb                                                        *
*              Archiviazione file da toolbar                                   *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-04-04                                                      *
* Last revis.: 2015-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipoArch,pTipoOpScanner
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_btb",oParentObject,m.pTipoArch,m.pTipoOpScanner)
return(i_retval)

define class tgsut_btb as StdBatch
  * --- Local variables
  pTipoArch = space(2)
  pTipoOpScanner = space(1)
  w_OPERAZIONE = space(1)
  w_CLASSE = space(15)
  w_DESCRI = space(40)
  w_MODALLEG = space(1)
  w_CONFMASK = .f.
  w_ARCHIVIO = space(20)
  w_CDCLAINF = space(15)
  w_COORDCOL = 0
  w_COORDROW = 0
  w_CDPUBWEB = space(1)
  * --- WorkFile variables
  GESTFILE_idx=0
  CONTROPA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine richiamata dalla 'Application bar', quando si avvia l'archiviazione rapida (da file/da scanner)
    * --- Parametro indicante il tipo di archiviazione; da file (AF), da scanner (AS)
    * --- Variabili richiesti dalla maschera GSUT_KKW (Selezione classe documentale)
    this.w_CDPUBWEB = "N"
    this.w_OPERAZIONE = iif(this.pTipoArch="AF","C","S")
    * --- Apro la maschera di selezione della classe documentale
    Ah_ErrorMsg("Occorre selezionare una classe documentale valida",64,"")
    do GSUT_KKW with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if EMPTY(this.w_CLASSE)
      * --- Non ho selezionato alcuna classe documentale, quindi blocco l'operazione richiesta
      i_retcode = 'stop'
      return
    endif
    * --- Eseguo l'operazione di archiviazione richiesta
    do case
      case this.pTipoArch = "AF"
        GSUT_BCV(this," "," "," ","C",0,0,.f.,this.w_CLASSE,.f.,this.w_CDPUBWEB)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTipoArch = "AS"
        this.w_COORDCOL = 0
        this.w_COORDROW = 0
        if vartype(this.pTipoOpScanner) <> "C"
          * --- Vecchio tema
          this.pTipoOpScanner = "S"
        endif
        GSUT_BCV(this," "," "," ",this.pTipoOpScanner,this.w_COORDROW,this.w_COORDCOL,.f.,this.w_CLASSE,.f.,this.w_CDPUBWEB)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      otherwise
        Ah_ErrorMsg("Operazione non gestita!",48,"")
        i_retcode = 'stop'
        return
    endcase
  endproc


  proc Init(oParentObject,pTipoArch,pTipoOpScanner)
    this.pTipoArch=pTipoArch
    this.pTipoOpScanner=pTipoOpScanner
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='GESTFILE'
    this.cWorkTables[2]='CONTROPA'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipoArch,pTipoOpScanner"
endproc
