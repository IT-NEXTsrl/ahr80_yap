* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_kgo                                                        *
*              Generazione ordine da web                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_229]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-12-28                                                      *
* Last revis.: 2016-03-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscp_kgo",oParentObject))

* --- Class definition
define class tgscp_kgo as StdForm
  Top    = 3
  Left   = 16

  * --- Standard Properties
  Width  = 890
  Height = 441+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-03-25"
  HelpContextID=236350103
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=116

  * --- Constant Properties
  _IDX = 0
  ZREGVALI_IDX = 0
  CONTI_IDX = 0
  AGENTI_IDX = 0
  ZONE_IDX = 0
  BUSIUNIT_IDX = 0
  CENCOST_IDX = 0
  TIP_DOCU_IDX = 0
  CAN_TIER_IDX = 0
  ESERCIZI_IDX = 0
  MAGAZZIN_IDX = 0
  ATTIVITA_IDX = 0
  ZPARAMORD_IDX = 0
  cPrg = "gscp_kgo"
  cComment = "Generazione ordine da web"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_ORSERIAL = space(10)
  o_ORSERIAL = space(10)
  w_ORLIVCON = 0
  o_ORLIVCON = 0
  w_SELEZI = space(3)
  w_DESELEZI = space(3)
  w_LIVELLO = 0
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_CODINI = space(15)
  w_CODFIN = space(15)
  w_DESCL1 = space(40)
  w_DESCL2 = space(40)
  w_CODAGE = space(5)
  w_CODZON = space(5)
  w_AGDESAGE = space(35)
  w_ZODESZON = space(35)
  w_A = space(1)
  w_MVDATREG = ctod('  /  /  ')
  w_MVCODESE = space(4)
  w_MVDATCIV = ctod('  /  /  ')
  w_DATDIV = ctod('  /  /  ')
  w_MVCODBUN = space(10)
  w_MVTCOMME = space(10)
  w_MVTCOATT = space(15)
  w_BUDESCRI = space(40)
  w_CNDESCAN = space(30)
  w_MVTIPCON = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DTOBSO = ctod('  /  /  ')
  w_FLANAL = space(1)
  w_MVCODUTE = 0
  w_MVSERIAL = space(10)
  w_DESATT = space(30)
  w_VALNAZ = space(3)
  w_FLANBU = space(1)
  w_MVNUMREG = 0
  w_OR__NOTE = space(0)
  w_CCTIPREG = space(1)
  w_NOTECONT = space(0)
  w_ORMERISP = space(0)
  w_PAMODALI = space(1)
  w_LNKPARAM = space(10)
  o_LNKPARAM = space(10)
  w_MVTIPDOC2 = space(5)
  w_CODMAG2 = space(5)
  w_MVFLPROV8 = space(1)
  w_CENCOS2 = space(15)
  w_TDDESDOC2 = space(35)
  w_CCDESPIA2 = space(40)
  w_FLVEAC2 = space(1)
  w_MVPRD2 = space(2)
  w_MVCLADOC2 = space(2)
  w_MVFLACCO2 = space(1)
  w_MVFLINTE2 = space(1)
  w_MVTCAMAG2 = space(5)
  w_MVCAUCON2 = space(5)
  w_MVTFRAGG2 = space(1)
  w_FLPPRO2 = space(1)
  w_DESMAG2 = space(30)
  w_MVFLPROV6 = space(1)
  w_MVFLPROV4 = space(1)
  w_MVFLPROV2 = space(1)
  w_FLAGEN2 = space(1)
  w_MVTIPDOC4 = space(5)
  w_CODMAG4 = space(5)
  w_CENCOS4 = space(15)
  w_TDDESDOC4 = space(35)
  w_CCDESPIA4 = space(40)
  w_DESMAG4 = space(30)
  w_FLVEAC4 = space(1)
  w_MVPRD4 = space(2)
  w_MVCLADOC4 = space(2)
  w_MVFLACCO4 = space(1)
  w_MVFLINTE4 = space(1)
  w_MVTCAMAG4 = space(5)
  w_MVCAUCON4 = space(5)
  w_MVTFRAGG4 = space(1)
  w_FLPPRO4 = space(1)
  w_FLAGEN4 = space(1)
  w_MVTIPDOC6 = space(5)
  w_CODMAG6 = space(5)
  w_CENCOS6 = space(15)
  w_TDDESDOC6 = space(35)
  w_CCDESPIA6 = space(40)
  w_DESMAG6 = space(30)
  w_MVTIPDOC8 = space(5)
  w_CODMAG8 = space(5)
  w_CENCOS8 = space(15)
  w_TDDESDOC8 = space(35)
  w_CCDESPIA8 = space(40)
  w_DESMAG8 = space(30)
  w_FLVEAC6 = space(1)
  w_MVPRD6 = space(2)
  w_MVCLADOC6 = space(2)
  w_MVFLACCO6 = space(1)
  w_MVFLINTE6 = space(1)
  w_MVTCAMAG6 = space(5)
  w_MVCAUCON6 = space(5)
  w_MVTFRAGG6 = space(1)
  w_FLPPRO6 = space(1)
  w_FLAGEN6 = space(1)
  w_FLVEAC8 = space(1)
  w_MVPRD8 = space(2)
  w_MVCLADOC8 = space(2)
  w_MVFLACCO8 = space(1)
  w_MVFLINTE8 = space(1)
  w_MVTCAMAG8 = space(5)
  w_MVCAUCON8 = space(5)
  w_MVTFRAGG8 = space(1)
  w_FLPPRO8 = space(1)
  w_FLAGEN8 = space(1)
  w_EMERIC2 = space(1)
  w_EMERIC4 = space(1)
  w_EMERIC6 = space(1)
  w_EMERIC8 = space(1)
  w_DOSERIAL = space(10)
  w_ZoomSel = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscp_kgoPag1","gscp_kgo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Elenco ordini web")
      .Pages(2).addobject("oPag","tgscp_kgoPag2","gscp_kgo",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Filtro")
      .Pages(3).addobject("oPag","tgscp_kgoPag3","gscp_kgo",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Parametri per generazione")
      .Pages(4).addobject("oPag","tgscp_kgoPag4","gscp_kgo",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Visualizzazione parametri aziendali")
      .Pages(4).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELEZI_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gscp_kgo
    if VARTYPE(g_SCHEDULER)='C' AND g_SCHEDULER='S'
       this.parent.left=_screen.width+1
    endif
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomSel = this.oPgFrm.Pages(1).oPag.ZoomSel
    DoDefault()
    proc Destroy()
      this.w_ZoomSel = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='ZREGVALI'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='AGENTI'
    this.cWorkTables[4]='ZONE'
    this.cWorkTables[5]='BUSIUNIT'
    this.cWorkTables[6]='CENCOST'
    this.cWorkTables[7]='TIP_DOCU'
    this.cWorkTables[8]='CAN_TIER'
    this.cWorkTables[9]='ESERCIZI'
    this.cWorkTables[10]='MAGAZZIN'
    this.cWorkTables[11]='ATTIVITA'
    this.cWorkTables[12]='ZPARAMORD'
    return(this.OpenAllTables(12))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_ORSERIAL=space(10)
      .w_ORLIVCON=0
      .w_SELEZI=space(3)
      .w_DESELEZI=space(3)
      .w_LIVELLO=0
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_CODINI=space(15)
      .w_CODFIN=space(15)
      .w_DESCL1=space(40)
      .w_DESCL2=space(40)
      .w_CODAGE=space(5)
      .w_CODZON=space(5)
      .w_AGDESAGE=space(35)
      .w_ZODESZON=space(35)
      .w_A=space(1)
      .w_MVDATREG=ctod("  /  /  ")
      .w_MVCODESE=space(4)
      .w_MVDATCIV=ctod("  /  /  ")
      .w_DATDIV=ctod("  /  /  ")
      .w_MVCODBUN=space(10)
      .w_MVTCOMME=space(10)
      .w_MVTCOATT=space(15)
      .w_BUDESCRI=space(40)
      .w_CNDESCAN=space(30)
      .w_MVTIPCON=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_DTOBSO=ctod("  /  /  ")
      .w_FLANAL=space(1)
      .w_MVCODUTE=0
      .w_MVSERIAL=space(10)
      .w_DESATT=space(30)
      .w_VALNAZ=space(3)
      .w_FLANBU=space(1)
      .w_MVNUMREG=0
      .w_OR__NOTE=space(0)
      .w_CCTIPREG=space(1)
      .w_NOTECONT=space(0)
      .w_ORMERISP=space(0)
      .w_PAMODALI=space(1)
      .w_LNKPARAM=space(10)
      .w_MVTIPDOC2=space(5)
      .w_CODMAG2=space(5)
      .w_MVFLPROV8=space(1)
      .w_CENCOS2=space(15)
      .w_TDDESDOC2=space(35)
      .w_CCDESPIA2=space(40)
      .w_FLVEAC2=space(1)
      .w_MVPRD2=space(2)
      .w_MVCLADOC2=space(2)
      .w_MVFLACCO2=space(1)
      .w_MVFLINTE2=space(1)
      .w_MVTCAMAG2=space(5)
      .w_MVCAUCON2=space(5)
      .w_MVTFRAGG2=space(1)
      .w_FLPPRO2=space(1)
      .w_DESMAG2=space(30)
      .w_MVFLPROV6=space(1)
      .w_MVFLPROV4=space(1)
      .w_MVFLPROV2=space(1)
      .w_FLAGEN2=space(1)
      .w_MVTIPDOC4=space(5)
      .w_CODMAG4=space(5)
      .w_CENCOS4=space(15)
      .w_TDDESDOC4=space(35)
      .w_CCDESPIA4=space(40)
      .w_DESMAG4=space(30)
      .w_FLVEAC4=space(1)
      .w_MVPRD4=space(2)
      .w_MVCLADOC4=space(2)
      .w_MVFLACCO4=space(1)
      .w_MVFLINTE4=space(1)
      .w_MVTCAMAG4=space(5)
      .w_MVCAUCON4=space(5)
      .w_MVTFRAGG4=space(1)
      .w_FLPPRO4=space(1)
      .w_FLAGEN4=space(1)
      .w_MVTIPDOC6=space(5)
      .w_CODMAG6=space(5)
      .w_CENCOS6=space(15)
      .w_TDDESDOC6=space(35)
      .w_CCDESPIA6=space(40)
      .w_DESMAG6=space(30)
      .w_MVTIPDOC8=space(5)
      .w_CODMAG8=space(5)
      .w_CENCOS8=space(15)
      .w_TDDESDOC8=space(35)
      .w_CCDESPIA8=space(40)
      .w_DESMAG8=space(30)
      .w_FLVEAC6=space(1)
      .w_MVPRD6=space(2)
      .w_MVCLADOC6=space(2)
      .w_MVFLACCO6=space(1)
      .w_MVFLINTE6=space(1)
      .w_MVTCAMAG6=space(5)
      .w_MVCAUCON6=space(5)
      .w_MVTFRAGG6=space(1)
      .w_FLPPRO6=space(1)
      .w_FLAGEN6=space(1)
      .w_FLVEAC8=space(1)
      .w_MVPRD8=space(2)
      .w_MVCLADOC8=space(2)
      .w_MVFLACCO8=space(1)
      .w_MVFLINTE8=space(1)
      .w_MVTCAMAG8=space(5)
      .w_MVCAUCON8=space(5)
      .w_MVTFRAGG8=space(1)
      .w_FLPPRO8=space(1)
      .w_FLAGEN8=space(1)
      .w_EMERIC2=space(1)
      .w_EMERIC4=space(1)
      .w_EMERIC6=space(1)
      .w_EMERIC8=space(1)
      .w_DOSERIAL=space(10)
        .w_CODAZI = i_codazi
      .oPgFrm.Page1.oPag.ZoomSel.Calculate()
        .w_ORSERIAL = .w_ZOOMSEL.GetVar("ORSERIAL")
        .w_ORLIVCON = .w_ZOOMSEL.GetVar("ORLIVCON")
        .w_SELEZI = "ALL"
        .w_DESELEZI = "ALL"
        .w_LIVELLO = .w_ORLIVCON
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .DoRTCalc(7,9,.f.)
        if not(empty(.w_CODINI))
          .link_2_3('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CODFIN))
          .link_2_4('Full')
        endif
        .DoRTCalc(11,13,.f.)
        if not(empty(.w_CODAGE))
          .link_2_11('Full')
        endif
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CODZON))
          .link_2_12('Full')
        endif
          .DoRTCalc(15,16,.f.)
        .w_A = 'A'
        .w_MVDATREG = i_DATSYS
        .w_MVCODESE = g_CODESE
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_MVCODESE))
          .link_3_3('Full')
        endif
        .w_MVDATCIV = i_DATSYS
          .DoRTCalc(21,21,.f.)
        .w_MVCODBUN = g_CODBUN
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_MVCODBUN))
          .link_3_6('Full')
        endif
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_MVTCOMME))
          .link_3_7('Full')
        endif
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_MVTCOATT))
          .link_3_8('Full')
        endif
          .DoRTCalc(25,26,.f.)
        .w_MVTIPCON = 'C'
          .DoRTCalc(28,28,.f.)
        .w_OBTEST = i_DATSYS
          .DoRTCalc(30,31,.f.)
        .w_MVCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
          .DoRTCalc(33,37,.f.)
        .w_OR__NOTE = .w_ZOOMSEL.GetVar("OR__NOTE")
        .w_CCTIPREG = ' '
        .w_NOTECONT = .w_ZOOMSEL.GetVar("NOTECONT")
        .w_ORMERISP = .w_ZOOMSEL.GetVar("ORMERISP")
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
          .DoRTCalc(42,42,.f.)
        .w_LNKPARAM = i_codazi
        .DoRTCalc(43,43,.f.)
        if not(empty(.w_LNKPARAM))
          .link_4_1('Full')
        endif
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_MVTIPDOC2))
          .link_4_2('Full')
        endif
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_CODMAG2))
          .link_4_3('Full')
        endif
        .DoRTCalc(46,47,.f.)
        if not(empty(.w_CENCOS2))
          .link_4_5('Full')
        endif
        .DoRTCalc(48,64,.f.)
        if not(empty(.w_MVTIPDOC4))
          .link_4_29('Full')
        endif
        .DoRTCalc(65,65,.f.)
        if not(empty(.w_CODMAG4))
          .link_4_30('Full')
        endif
        .DoRTCalc(66,66,.f.)
        if not(empty(.w_CENCOS4))
          .link_4_31('Full')
        endif
        .DoRTCalc(67,80,.f.)
        if not(empty(.w_MVTIPDOC6))
          .link_4_48('Full')
        endif
        .DoRTCalc(81,81,.f.)
        if not(empty(.w_CODMAG6))
          .link_4_49('Full')
        endif
        .DoRTCalc(82,82,.f.)
        if not(empty(.w_CENCOS6))
          .link_4_50('Full')
        endif
        .DoRTCalc(83,86,.f.)
        if not(empty(.w_MVTIPDOC8))
          .link_4_57('Full')
        endif
        .DoRTCalc(87,87,.f.)
        if not(empty(.w_CODMAG8))
          .link_4_58('Full')
        endif
        .DoRTCalc(88,88,.f.)
        if not(empty(.w_CENCOS8))
          .link_4_59('Full')
        endif
      .oPgFrm.Page4.oPag.oObj_4_86.Calculate(iif(.w_MVFLPROV2='R','Rifiuta ordine',IIF(.w_MVFLPROV2='S','Accetta in provvisorio','Accetta')),'0','')
      .oPgFrm.Page4.oPag.oObj_4_87.Calculate(iif(.w_MVFLPROV4='R','Rifiuta ordine',IIF(.w_MVFLPROV4='S','Accetta in provvisorio','Accetta')),'0','')
      .oPgFrm.Page4.oPag.oObj_4_88.Calculate(iif(.w_MVFLPROV6='R','Rifiuta ordine',IIF(.w_MVFLPROV6='S','Accetta in provvisorio','Accetta')),'0','')
      .oPgFrm.Page4.oPag.oObj_4_89.Calculate(iif(.w_MVFLPROV8='R','Rifiuta ordine',IIF(.w_MVFLPROV8='S','Accetta in provvisorio','Accetta')),'0','')
      .oPgFrm.Page4.oPag.oObj_4_90.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
    endwith
    this.DoRTCalc(89,116,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_17.enabled = this.oPgFrm.Page2.oPag.oBtn_2_17.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomSel.Calculate()
        .DoRTCalc(1,1,.t.)
            .w_ORSERIAL = .w_ZOOMSEL.GetVar("ORSERIAL")
            .w_ORLIVCON = .w_ZOOMSEL.GetVar("ORLIVCON")
        .DoRTCalc(4,5,.t.)
        if .o_ORSERIAL<>.w_ORSERIAL.or. .o_ORLIVCON<>.w_ORLIVCON
            .w_LIVELLO = .w_ORLIVCON
        endif
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .DoRTCalc(7,26,.t.)
            .w_MVTIPCON = 'C'
        .DoRTCalc(28,37,.t.)
            .w_OR__NOTE = .w_ZOOMSEL.GetVar("OR__NOTE")
        .DoRTCalc(39,39,.t.)
            .w_NOTECONT = .w_ZOOMSEL.GetVar("NOTECONT")
            .w_ORMERISP = .w_ZOOMSEL.GetVar("ORMERISP")
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .DoRTCalc(42,42,.t.)
        if .o_LNKPARAM<>.w_LNKPARAM
          .link_4_1('Full')
        endif
          .link_4_2('Full')
          .link_4_3('Full')
        .DoRTCalc(46,46,.t.)
          .link_4_5('Full')
        .DoRTCalc(48,63,.t.)
          .link_4_29('Full')
          .link_4_30('Full')
          .link_4_31('Full')
        .DoRTCalc(67,79,.t.)
          .link_4_48('Full')
          .link_4_49('Full')
          .link_4_50('Full')
        .DoRTCalc(83,85,.t.)
          .link_4_57('Full')
          .link_4_58('Full')
          .link_4_59('Full')
        .oPgFrm.Page4.oPag.oObj_4_86.Calculate(iif(.w_MVFLPROV2='R','Rifiuta ordine',IIF(.w_MVFLPROV2='S','Accetta in provvisorio','Accetta')),'0','')
        .oPgFrm.Page4.oPag.oObj_4_87.Calculate(iif(.w_MVFLPROV4='R','Rifiuta ordine',IIF(.w_MVFLPROV4='S','Accetta in provvisorio','Accetta')),'0','')
        .oPgFrm.Page4.oPag.oObj_4_88.Calculate(iif(.w_MVFLPROV6='R','Rifiuta ordine',IIF(.w_MVFLPROV6='S','Accetta in provvisorio','Accetta')),'0','')
        .oPgFrm.Page4.oPag.oObj_4_89.Calculate(iif(.w_MVFLPROV8='R','Rifiuta ordine',IIF(.w_MVFLPROV8='S','Accetta in provvisorio','Accetta')),'0','')
        .oPgFrm.Page4.oPag.oObj_4_90.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(89,116,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomSel.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .oPgFrm.Page4.oPag.oObj_4_86.Calculate(iif(.w_MVFLPROV2='R','Rifiuta ordine',IIF(.w_MVFLPROV2='S','Accetta in provvisorio','Accetta')),'0','')
        .oPgFrm.Page4.oPag.oObj_4_87.Calculate(iif(.w_MVFLPROV4='R','Rifiuta ordine',IIF(.w_MVFLPROV4='S','Accetta in provvisorio','Accetta')),'0','')
        .oPgFrm.Page4.oPag.oObj_4_88.Calculate(iif(.w_MVFLPROV6='R','Rifiuta ordine',IIF(.w_MVFLPROV6='S','Accetta in provvisorio','Accetta')),'0','')
        .oPgFrm.Page4.oPag.oObj_4_89.Calculate(iif(.w_MVFLPROV8='R','Rifiuta ordine',IIF(.w_MVFLPROV8='S','Accetta in provvisorio','Accetta')),'0','')
        .oPgFrm.Page4.oPag.oObj_4_90.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page3.oPag.oMVCODBUN_3_6.enabled = this.oPgFrm.Page3.oPag.oMVCODBUN_3_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page3.oPag.oMVCODBUN_3_6.visible=!this.oPgFrm.Page3.oPag.oMVCODBUN_3_6.mHide()
    this.oPgFrm.Page3.oPag.oMVTCOATT_3_8.visible=!this.oPgFrm.Page3.oPag.oMVTCOATT_3_8.mHide()
    this.oPgFrm.Page3.oPag.oBUDESCRI_3_11.visible=!this.oPgFrm.Page3.oPag.oBUDESCRI_3_11.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_12.visible=!this.oPgFrm.Page3.oPag.oStr_3_12.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_19.visible=!this.oPgFrm.Page3.oPag.oStr_3_19.mHide()
    this.oPgFrm.Page3.oPag.oDESATT_3_20.visible=!this.oPgFrm.Page3.oPag.oDESATT_3_20.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page4.oPag.oCENCOS2_4_5.visible=!this.oPgFrm.Page4.oPag.oCENCOS2_4_5.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_8.visible=!this.oPgFrm.Page4.oPag.oStr_4_8.mHide()
    this.oPgFrm.Page4.oPag.oCCDESPIA2_4_9.visible=!this.oPgFrm.Page4.oPag.oCCDESPIA2_4_9.mHide()
    this.oPgFrm.Page4.oPag.oCENCOS4_4_31.visible=!this.oPgFrm.Page4.oPag.oCENCOS4_4_31.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_34.visible=!this.oPgFrm.Page4.oPag.oStr_4_34.mHide()
    this.oPgFrm.Page4.oPag.oCCDESPIA4_4_35.visible=!this.oPgFrm.Page4.oPag.oCCDESPIA4_4_35.mHide()
    this.oPgFrm.Page4.oPag.oCENCOS6_4_50.visible=!this.oPgFrm.Page4.oPag.oCENCOS6_4_50.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_53.visible=!this.oPgFrm.Page4.oPag.oStr_4_53.mHide()
    this.oPgFrm.Page4.oPag.oCCDESPIA6_4_54.visible=!this.oPgFrm.Page4.oPag.oCCDESPIA6_4_54.mHide()
    this.oPgFrm.Page4.oPag.oCENCOS8_4_59.visible=!this.oPgFrm.Page4.oPag.oCENCOS8_4_59.mHide()
    this.oPgFrm.Page4.oPag.oStr_4_62.visible=!this.oPgFrm.Page4.oPag.oStr_4_62.mHide()
    this.oPgFrm.Page4.oPag.oCCDESPIA8_4_63.visible=!this.oPgFrm.Page4.oPag.oCCDESPIA8_4_63.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomSel.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_86.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_87.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_88.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_89.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_90.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_MVTIPCON;
                     ,'ANCODICE',trim(this.w_CODINI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODINI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_MVTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODINI_2_3'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale deve essere minore o uguale al codice finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODINI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MVTIPCON;
                       ,'ANCODICE',this.w_CODINI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCL1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(15)
      endif
      this.w_DESCL1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CODFIN) OR .w_CODINI<=.w_CODFIN
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale deve essere minore o uguale al codice finale")
        endif
        this.w_CODINI = space(15)
        this.w_DESCL1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_MVTIPCON;
                     ,'ANCODICE',trim(this.w_CODFIN))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_MVTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFIN_2_4'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice finale deve essere maggiore o uguale al codice iniziale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MVTIPCON;
                       ,'ANCODICE',this.w_CODFIN)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCL2 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(15)
      endif
      this.w_DESCL2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CODINI) OR .w_CODFIN>=.w_CODINI
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice finale deve essere maggiore o uguale al codice iniziale")
        endif
        this.w_CODFIN = space(15)
        this.w_DESCL2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE_2_11'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_AGDESAGE = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE = space(5)
      endif
      this.w_AGDESAGE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODZON
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_CODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_CODZON))
          select ZOCODZON,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oCODZON_2_12'),i_cWhere,'',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_CODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_CODZON)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODZON = NVL(_Link_.ZOCODZON,space(5))
      this.w_ZODESZON = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODZON = space(5)
      endif
      this.w_ZODESZON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODESE
  func Link_3_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_MVCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_MVCODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oMVCODESE_3_3'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_MVCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_MVCODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODESE = space(4)
      endif
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODBUN
  func Link_3_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_MVCODBUN)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI,BUFLANAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_CODAZI;
                     ,'BUCODICE',trim(this.w_MVCODBUN))
          select BUCODAZI,BUCODICE,BUDESCRI,BUFLANAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVCODBUN)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVCODBUN) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oMVCODBUN_3_6'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI,BUFLANAL";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI,BUFLANAL;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI,BUFLANAL";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI,BUFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI,BUFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_MVCODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_CODAZI;
                       ,'BUCODICE',this.w_MVCODBUN)
            select BUCODAZI,BUCODICE,BUDESCRI,BUFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODBUN = NVL(_Link_.BUCODICE,space(10))
      this.w_BUDESCRI = NVL(_Link_.BUDESCRI,space(40))
      this.w_FLANBU = NVL(_Link_.BUFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCODBUN = space(10)
      endif
      this.w_BUDESCRI = space(40)
      this.w_FLANBU = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVTCOMME
  func Link_3_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTCOMME) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_MVTCOMME)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_MVTCOMME))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVTCOMME)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVTCOMME) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oMVTCOMME_3_7'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTCOMME)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_MVTCOMME);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_MVTCOMME)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTCOMME = NVL(_Link_.CNCODCAN,space(10))
      this.w_CNDESCAN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MVTCOMME = space(10)
      endif
      this.w_CNDESCAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTCOMME Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVTCOATT
  func Link_3_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTCOATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_MVTCOATT)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_MVTCOMME);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_A);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_MVTCOMME;
                     ,'ATTIPATT',this.w_A;
                     ,'ATCODATT',trim(this.w_MVTCOATT))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MVTCOATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MVTCOATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oMVTCOATT_3_8'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MVTCOMME<>oSource.xKey(1);
           .or. this.w_A<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_MVTCOMME);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_A);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTCOATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_MVTCOATT);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_MVTCOMME);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_A);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_MVTCOMME;
                       ,'ATTIPATT',this.w_A;
                       ,'ATCODATT',this.w_MVTCOATT)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTCOATT = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATT = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MVTCOATT = space(15)
      endif
      this.w_DESATT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTCOATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LNKPARAM
  func Link_4_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZPARAMORD_IDX,3]
    i_lTable = "ZPARAMORD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZPARAMORD_IDX,2], .t., this.ZPARAMORD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZPARAMORD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LNKPARAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LNKPARAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PAFLPRO8,PATIPDOC8,PACODMAG8,PACENCOS8,PAFLPRO6,PATIPDOC6,PACODMAG6,PACENCOS6,PAFLPRO4,PATIPDOC4,PACODMAG4,PACENCOS4,PAFLPRO2,PATIPDOC2,PACODMAG2,PACENCOS2";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_LNKPARAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_LNKPARAM)
            select PACODICE,PAFLPRO8,PATIPDOC8,PACODMAG8,PACENCOS8,PAFLPRO6,PATIPDOC6,PACODMAG6,PACENCOS6,PAFLPRO4,PATIPDOC4,PACODMAG4,PACENCOS4,PAFLPRO2,PATIPDOC2,PACODMAG2,PACENCOS2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LNKPARAM = NVL(_Link_.PACODICE,space(10))
      this.w_MVFLPROV8 = NVL(_Link_.PAFLPRO8,space(1))
      this.w_MVTIPDOC8 = NVL(_Link_.PATIPDOC8,space(5))
      this.w_CODMAG8 = NVL(_Link_.PACODMAG8,space(5))
      this.w_CENCOS8 = NVL(_Link_.PACENCOS8,space(15))
      this.w_MVFLPROV6 = NVL(_Link_.PAFLPRO6,space(1))
      this.w_MVTIPDOC6 = NVL(_Link_.PATIPDOC6,space(5))
      this.w_CODMAG6 = NVL(_Link_.PACODMAG6,space(5))
      this.w_CENCOS6 = NVL(_Link_.PACENCOS6,space(15))
      this.w_MVFLPROV4 = NVL(_Link_.PAFLPRO4,space(1))
      this.w_MVTIPDOC4 = NVL(_Link_.PATIPDOC4,space(5))
      this.w_CODMAG4 = NVL(_Link_.PACODMAG4,space(5))
      this.w_CENCOS4 = NVL(_Link_.PACENCOS4,space(15))
      this.w_MVFLPROV2 = NVL(_Link_.PAFLPRO2,space(1))
      this.w_MVTIPDOC2 = NVL(_Link_.PATIPDOC2,space(5))
      this.w_CODMAG2 = NVL(_Link_.PACODMAG2,space(5))
      this.w_CENCOS2 = NVL(_Link_.PACENCOS2,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_LNKPARAM = space(10)
      endif
      this.w_MVFLPROV8 = space(1)
      this.w_MVTIPDOC8 = space(5)
      this.w_CODMAG8 = space(5)
      this.w_CENCOS8 = space(15)
      this.w_MVFLPROV6 = space(1)
      this.w_MVTIPDOC6 = space(5)
      this.w_CODMAG6 = space(5)
      this.w_CENCOS6 = space(15)
      this.w_MVFLPROV4 = space(1)
      this.w_MVTIPDOC4 = space(5)
      this.w_CODMAG4 = space(5)
      this.w_CENCOS4 = space(15)
      this.w_MVFLPROV2 = space(1)
      this.w_MVTIPDOC2 = space(5)
      this.w_CODMAG2 = space(5)
      this.w_CENCOS2 = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZPARAMORD_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.ZPARAMORD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LNKPARAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVTIPDOC2
  func Link_4_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTIPDOC2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTIPDOC2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDFLPROV,TDEMERIC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MVTIPDOC2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MVTIPDOC2)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDFLPROV,TDEMERIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTIPDOC2 = NVL(_Link_.TDTIPDOC,space(5))
      this.w_TDDESDOC2 = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAC2 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_MVPRD2 = NVL(_Link_.TDPRODOC,space(2))
      this.w_MVCLADOC2 = NVL(_Link_.TDCATDOC,space(2))
      this.w_MVFLACCO2 = NVL(_Link_.TDFLACCO,space(1))
      this.w_MVFLINTE2 = NVL(_Link_.TDFLINTE,space(1))
      this.w_MVTCAMAG2 = NVL(_Link_.TDCAUMAG,space(5))
      this.w_MVTFRAGG2 = NVL(_Link_.TFFLRAGG,space(1))
      this.w_MVCAUCON2 = NVL(_Link_.TDCAUCON,space(5))
      this.w_FLPPRO2 = NVL(_Link_.TDFLPPRO,space(1))
      this.w_FLAGEN2 = NVL(_Link_.TDFLPROV,space(1))
      this.w_EMERIC2 = NVL(_Link_.TDEMERIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVTIPDOC2 = space(5)
      endif
      this.w_TDDESDOC2 = space(35)
      this.w_FLVEAC2 = space(1)
      this.w_MVPRD2 = space(2)
      this.w_MVCLADOC2 = space(2)
      this.w_MVFLACCO2 = space(1)
      this.w_MVFLINTE2 = space(1)
      this.w_MVTCAMAG2 = space(5)
      this.w_MVTFRAGG2 = space(1)
      this.w_MVCAUCON2 = space(5)
      this.w_FLPPRO2 = space(1)
      this.w_FLAGEN2 = space(1)
      this.w_EMERIC2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTIPDOC2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG2
  func Link_4_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG2)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG2 = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG2 = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG2 = space(5)
      endif
      this.w_DESMAG2 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CENCOS2
  func Link_4_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CENCOS2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CENCOS2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CENCOS2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CENCOS2)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CENCOS2 = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA2 = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CENCOS2 = space(15)
      endif
      this.w_CCDESPIA2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CENCOS2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVTIPDOC4
  func Link_4_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTIPDOC4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTIPDOC4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDFLPROV,TDEMERIC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MVTIPDOC4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MVTIPDOC4)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDFLPROV,TDEMERIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTIPDOC4 = NVL(_Link_.TDTIPDOC,space(5))
      this.w_TDDESDOC4 = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAC4 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_MVPRD4 = NVL(_Link_.TDPRODOC,space(2))
      this.w_MVCLADOC4 = NVL(_Link_.TDCATDOC,space(2))
      this.w_MVFLACCO4 = NVL(_Link_.TDFLACCO,space(1))
      this.w_MVFLINTE4 = NVL(_Link_.TDFLINTE,space(1))
      this.w_MVTCAMAG4 = NVL(_Link_.TDCAUMAG,space(5))
      this.w_MVTFRAGG4 = NVL(_Link_.TFFLRAGG,space(1))
      this.w_MVCAUCON4 = NVL(_Link_.TDCAUCON,space(5))
      this.w_FLPPRO4 = NVL(_Link_.TDFLPPRO,space(1))
      this.w_FLAGEN4 = NVL(_Link_.TDFLPROV,space(1))
      this.w_EMERIC4 = NVL(_Link_.TDEMERIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVTIPDOC4 = space(5)
      endif
      this.w_TDDESDOC4 = space(35)
      this.w_FLVEAC4 = space(1)
      this.w_MVPRD4 = space(2)
      this.w_MVCLADOC4 = space(2)
      this.w_MVFLACCO4 = space(1)
      this.w_MVFLINTE4 = space(1)
      this.w_MVTCAMAG4 = space(5)
      this.w_MVTFRAGG4 = space(1)
      this.w_MVCAUCON4 = space(5)
      this.w_FLPPRO4 = space(1)
      this.w_FLAGEN4 = space(1)
      this.w_EMERIC4 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTIPDOC4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG4
  func Link_4_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG4)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG4 = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG4 = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG4 = space(5)
      endif
      this.w_DESMAG4 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CENCOS4
  func Link_4_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CENCOS4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CENCOS4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CENCOS4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CENCOS4)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CENCOS4 = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA4 = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CENCOS4 = space(15)
      endif
      this.w_CCDESPIA4 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CENCOS4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVTIPDOC6
  func Link_4_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTIPDOC6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTIPDOC6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDFLPROV,TDEMERIC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MVTIPDOC6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MVTIPDOC6)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDFLPROV,TDEMERIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTIPDOC6 = NVL(_Link_.TDTIPDOC,space(5))
      this.w_TDDESDOC6 = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAC6 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_MVPRD6 = NVL(_Link_.TDPRODOC,space(2))
      this.w_MVCLADOC6 = NVL(_Link_.TDCATDOC,space(2))
      this.w_MVFLACCO6 = NVL(_Link_.TDFLACCO,space(1))
      this.w_MVFLINTE6 = NVL(_Link_.TDFLINTE,space(1))
      this.w_MVTCAMAG6 = NVL(_Link_.TDCAUMAG,space(5))
      this.w_MVTFRAGG6 = NVL(_Link_.TFFLRAGG,space(1))
      this.w_MVCAUCON6 = NVL(_Link_.TDCAUCON,space(5))
      this.w_FLPPRO6 = NVL(_Link_.TDFLPPRO,space(1))
      this.w_FLAGEN6 = NVL(_Link_.TDFLPROV,space(1))
      this.w_EMERIC6 = NVL(_Link_.TDEMERIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVTIPDOC6 = space(5)
      endif
      this.w_TDDESDOC6 = space(35)
      this.w_FLVEAC6 = space(1)
      this.w_MVPRD6 = space(2)
      this.w_MVCLADOC6 = space(2)
      this.w_MVFLACCO6 = space(1)
      this.w_MVFLINTE6 = space(1)
      this.w_MVTCAMAG6 = space(5)
      this.w_MVTFRAGG6 = space(1)
      this.w_MVCAUCON6 = space(5)
      this.w_FLPPRO6 = space(1)
      this.w_FLAGEN6 = space(1)
      this.w_EMERIC6 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTIPDOC6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG6
  func Link_4_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG6)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG6 = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG6 = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG6 = space(5)
      endif
      this.w_DESMAG6 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CENCOS6
  func Link_4_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CENCOS6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CENCOS6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CENCOS6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CENCOS6)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CENCOS6 = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA6 = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CENCOS6 = space(15)
      endif
      this.w_CCDESPIA6 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CENCOS6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVTIPDOC8
  func Link_4_57(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVTIPDOC8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVTIPDOC8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDFLPROV,TDEMERIC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_MVTIPDOC8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_MVTIPDOC8)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDPRODOC,TDCATDOC,TDFLACCO,TDFLINTE,TDCAUMAG,TFFLRAGG,TDCAUCON,TDFLPPRO,TDFLPROV,TDEMERIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVTIPDOC8 = NVL(_Link_.TDTIPDOC,space(5))
      this.w_TDDESDOC8 = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAC8 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_MVPRD8 = NVL(_Link_.TDPRODOC,space(2))
      this.w_MVCLADOC8 = NVL(_Link_.TDCATDOC,space(2))
      this.w_MVFLACCO8 = NVL(_Link_.TDFLACCO,space(1))
      this.w_MVFLINTE8 = NVL(_Link_.TDFLINTE,space(1))
      this.w_MVTCAMAG8 = NVL(_Link_.TDCAUMAG,space(5))
      this.w_MVTFRAGG8 = NVL(_Link_.TFFLRAGG,space(1))
      this.w_MVCAUCON8 = NVL(_Link_.TDCAUCON,space(5))
      this.w_FLPPRO8 = NVL(_Link_.TDFLPPRO,space(1))
      this.w_FLAGEN8 = NVL(_Link_.TDFLPROV,space(1))
      this.w_EMERIC8 = NVL(_Link_.TDEMERIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVTIPDOC8 = space(5)
      endif
      this.w_TDDESDOC8 = space(35)
      this.w_FLVEAC8 = space(1)
      this.w_MVPRD8 = space(2)
      this.w_MVCLADOC8 = space(2)
      this.w_MVFLACCO8 = space(1)
      this.w_MVFLINTE8 = space(1)
      this.w_MVTCAMAG8 = space(5)
      this.w_MVTFRAGG8 = space(1)
      this.w_MVCAUCON8 = space(5)
      this.w_FLPPRO8 = space(1)
      this.w_FLAGEN8 = space(1)
      this.w_EMERIC8 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVTIPDOC8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG8
  func Link_4_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG8)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG8 = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG8 = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG8 = space(5)
      endif
      this.w_DESMAG8 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CENCOS8
  func Link_4_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CENCOS8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CENCOS8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_CENCOS8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_CENCOS8)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CENCOS8 = NVL(_Link_.CC_CONTO,space(15))
      this.w_CCDESPIA8 = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CENCOS8 = space(15)
      endif
      this.w_CCDESPIA8 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CENCOS8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_5.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESELEZI_1_6.RadioValue()==this.w_DESELEZI)
      this.oPgFrm.Page1.oPag.oDESELEZI_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLIVELLO_1_7.RadioValue()==this.w_LIVELLO)
      this.oPgFrm.Page1.oPag.oLIVELLO_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDATINI_2_1.value==this.w_DATINI)
      this.oPgFrm.Page2.oPag.oDATINI_2_1.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDATFIN_2_2.value==this.w_DATFIN)
      this.oPgFrm.Page2.oPag.oDATFIN_2_2.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCODINI_2_3.value==this.w_CODINI)
      this.oPgFrm.Page2.oPag.oCODINI_2_3.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODFIN_2_4.value==this.w_CODFIN)
      this.oPgFrm.Page2.oPag.oCODFIN_2_4.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCL1_2_9.value==this.w_DESCL1)
      this.oPgFrm.Page2.oPag.oDESCL1_2_9.value=this.w_DESCL1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCL2_2_10.value==this.w_DESCL2)
      this.oPgFrm.Page2.oPag.oDESCL2_2_10.value=this.w_DESCL2
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGE_2_11.value==this.w_CODAGE)
      this.oPgFrm.Page2.oPag.oCODAGE_2_11.value=this.w_CODAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oCODZON_2_12.value==this.w_CODZON)
      this.oPgFrm.Page2.oPag.oCODZON_2_12.value=this.w_CODZON
    endif
    if not(this.oPgFrm.Page2.oPag.oAGDESAGE_2_13.value==this.w_AGDESAGE)
      this.oPgFrm.Page2.oPag.oAGDESAGE_2_13.value=this.w_AGDESAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oZODESZON_2_15.value==this.w_ZODESZON)
      this.oPgFrm.Page2.oPag.oZODESZON_2_15.value=this.w_ZODESZON
    endif
    if not(this.oPgFrm.Page3.oPag.oMVDATREG_3_2.value==this.w_MVDATREG)
      this.oPgFrm.Page3.oPag.oMVDATREG_3_2.value=this.w_MVDATREG
    endif
    if not(this.oPgFrm.Page3.oPag.oMVCODESE_3_3.value==this.w_MVCODESE)
      this.oPgFrm.Page3.oPag.oMVCODESE_3_3.value=this.w_MVCODESE
    endif
    if not(this.oPgFrm.Page3.oPag.oMVDATCIV_3_4.value==this.w_MVDATCIV)
      this.oPgFrm.Page3.oPag.oMVDATCIV_3_4.value=this.w_MVDATCIV
    endif
    if not(this.oPgFrm.Page3.oPag.oDATDIV_3_5.value==this.w_DATDIV)
      this.oPgFrm.Page3.oPag.oDATDIV_3_5.value=this.w_DATDIV
    endif
    if not(this.oPgFrm.Page3.oPag.oMVCODBUN_3_6.value==this.w_MVCODBUN)
      this.oPgFrm.Page3.oPag.oMVCODBUN_3_6.value=this.w_MVCODBUN
    endif
    if not(this.oPgFrm.Page3.oPag.oMVTCOMME_3_7.value==this.w_MVTCOMME)
      this.oPgFrm.Page3.oPag.oMVTCOMME_3_7.value=this.w_MVTCOMME
    endif
    if not(this.oPgFrm.Page3.oPag.oMVTCOATT_3_8.value==this.w_MVTCOATT)
      this.oPgFrm.Page3.oPag.oMVTCOATT_3_8.value=this.w_MVTCOATT
    endif
    if not(this.oPgFrm.Page3.oPag.oBUDESCRI_3_11.value==this.w_BUDESCRI)
      this.oPgFrm.Page3.oPag.oBUDESCRI_3_11.value=this.w_BUDESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oCNDESCAN_3_14.value==this.w_CNDESCAN)
      this.oPgFrm.Page3.oPag.oCNDESCAN_3_14.value=this.w_CNDESCAN
    endif
    if not(this.oPgFrm.Page3.oPag.oDESATT_3_20.value==this.w_DESATT)
      this.oPgFrm.Page3.oPag.oDESATT_3_20.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page3.oPag.oVALNAZ_3_21.value==this.w_VALNAZ)
      this.oPgFrm.Page3.oPag.oVALNAZ_3_21.value=this.w_VALNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTECONT_1_17.value==this.w_NOTECONT)
      this.oPgFrm.Page1.oPag.oNOTECONT_1_17.value=this.w_NOTECONT
    endif
    if not(this.oPgFrm.Page4.oPag.oMVTIPDOC2_4_2.value==this.w_MVTIPDOC2)
      this.oPgFrm.Page4.oPag.oMVTIPDOC2_4_2.value=this.w_MVTIPDOC2
    endif
    if not(this.oPgFrm.Page4.oPag.oCODMAG2_4_3.value==this.w_CODMAG2)
      this.oPgFrm.Page4.oPag.oCODMAG2_4_3.value=this.w_CODMAG2
    endif
    if not(this.oPgFrm.Page4.oPag.oCENCOS2_4_5.value==this.w_CENCOS2)
      this.oPgFrm.Page4.oPag.oCENCOS2_4_5.value=this.w_CENCOS2
    endif
    if not(this.oPgFrm.Page4.oPag.oTDDESDOC2_4_6.value==this.w_TDDESDOC2)
      this.oPgFrm.Page4.oPag.oTDDESDOC2_4_6.value=this.w_TDDESDOC2
    endif
    if not(this.oPgFrm.Page4.oPag.oCCDESPIA2_4_9.value==this.w_CCDESPIA2)
      this.oPgFrm.Page4.oPag.oCCDESPIA2_4_9.value=this.w_CCDESPIA2
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMAG2_4_24.value==this.w_DESMAG2)
      this.oPgFrm.Page4.oPag.oDESMAG2_4_24.value=this.w_DESMAG2
    endif
    if not(this.oPgFrm.Page4.oPag.oMVTIPDOC4_4_29.value==this.w_MVTIPDOC4)
      this.oPgFrm.Page4.oPag.oMVTIPDOC4_4_29.value=this.w_MVTIPDOC4
    endif
    if not(this.oPgFrm.Page4.oPag.oCODMAG4_4_30.value==this.w_CODMAG4)
      this.oPgFrm.Page4.oPag.oCODMAG4_4_30.value=this.w_CODMAG4
    endif
    if not(this.oPgFrm.Page4.oPag.oCENCOS4_4_31.value==this.w_CENCOS4)
      this.oPgFrm.Page4.oPag.oCENCOS4_4_31.value=this.w_CENCOS4
    endif
    if not(this.oPgFrm.Page4.oPag.oTDDESDOC4_4_32.value==this.w_TDDESDOC4)
      this.oPgFrm.Page4.oPag.oTDDESDOC4_4_32.value=this.w_TDDESDOC4
    endif
    if not(this.oPgFrm.Page4.oPag.oCCDESPIA4_4_35.value==this.w_CCDESPIA4)
      this.oPgFrm.Page4.oPag.oCCDESPIA4_4_35.value=this.w_CCDESPIA4
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMAG4_4_37.value==this.w_DESMAG4)
      this.oPgFrm.Page4.oPag.oDESMAG4_4_37.value=this.w_DESMAG4
    endif
    if not(this.oPgFrm.Page4.oPag.oMVTIPDOC6_4_48.value==this.w_MVTIPDOC6)
      this.oPgFrm.Page4.oPag.oMVTIPDOC6_4_48.value=this.w_MVTIPDOC6
    endif
    if not(this.oPgFrm.Page4.oPag.oCODMAG6_4_49.value==this.w_CODMAG6)
      this.oPgFrm.Page4.oPag.oCODMAG6_4_49.value=this.w_CODMAG6
    endif
    if not(this.oPgFrm.Page4.oPag.oCENCOS6_4_50.value==this.w_CENCOS6)
      this.oPgFrm.Page4.oPag.oCENCOS6_4_50.value=this.w_CENCOS6
    endif
    if not(this.oPgFrm.Page4.oPag.oTDDESDOC6_4_51.value==this.w_TDDESDOC6)
      this.oPgFrm.Page4.oPag.oTDDESDOC6_4_51.value=this.w_TDDESDOC6
    endif
    if not(this.oPgFrm.Page4.oPag.oCCDESPIA6_4_54.value==this.w_CCDESPIA6)
      this.oPgFrm.Page4.oPag.oCCDESPIA6_4_54.value=this.w_CCDESPIA6
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMAG6_4_56.value==this.w_DESMAG6)
      this.oPgFrm.Page4.oPag.oDESMAG6_4_56.value=this.w_DESMAG6
    endif
    if not(this.oPgFrm.Page4.oPag.oMVTIPDOC8_4_57.value==this.w_MVTIPDOC8)
      this.oPgFrm.Page4.oPag.oMVTIPDOC8_4_57.value=this.w_MVTIPDOC8
    endif
    if not(this.oPgFrm.Page4.oPag.oCODMAG8_4_58.value==this.w_CODMAG8)
      this.oPgFrm.Page4.oPag.oCODMAG8_4_58.value=this.w_CODMAG8
    endif
    if not(this.oPgFrm.Page4.oPag.oCENCOS8_4_59.value==this.w_CENCOS8)
      this.oPgFrm.Page4.oPag.oCENCOS8_4_59.value=this.w_CENCOS8
    endif
    if not(this.oPgFrm.Page4.oPag.oTDDESDOC8_4_60.value==this.w_TDDESDOC8)
      this.oPgFrm.Page4.oPag.oTDDESDOC8_4_60.value=this.w_TDDESDOC8
    endif
    if not(this.oPgFrm.Page4.oPag.oCCDESPIA8_4_63.value==this.w_CCDESPIA8)
      this.oPgFrm.Page4.oPag.oCCDESPIA8_4_63.value=this.w_CCDESPIA8
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMAG8_4_65.value==this.w_DESMAG8)
      this.oPgFrm.Page4.oPag.oDESMAG8_4_65.value=this.w_DESMAG8
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_CODFIN) OR .w_CODINI<=.w_CODFIN)  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODINI_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale deve essere minore o uguale al codice finale")
          case   not(EMPTY(.w_CODINI) OR .w_CODFIN>=.w_CODINI)  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODFIN_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice finale deve essere maggiore o uguale al codice iniziale")
          case   (empty(.w_MVDATREG))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMVDATREG_3_2.SetFocus()
            i_bnoObbl = !empty(.w_MVDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MVCODESE))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMVCODESE_3_3.SetFocus()
            i_bnoObbl = !empty(.w_MVCODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MVDATCIV))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMVDATCIV_3_4.SetFocus()
            i_bnoObbl = !empty(.w_MVDATCIV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MVCODBUN))  and not(g_APPLICATION='ADHOC REVOLUTION')  and (g_PERBUN<>'N')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMVCODBUN_3_6.SetFocus()
            i_bnoObbl = !empty(.w_MVCODBUN)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ORSERIAL = this.w_ORSERIAL
    this.o_ORLIVCON = this.w_ORLIVCON
    this.o_LNKPARAM = this.w_LNKPARAM
    return

enddefine

* --- Define pages as container
define class tgscp_kgoPag1 as StdContainer
  Width  = 886
  height = 442
  stdWidth  = 886
  stdheight = 442
  resizeXpos=717
  resizeYpos=318
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomSel as cp_szoombox with uid="TRALQPJGZU",left=-6, top=3, width=778,height=331,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="ZORDWEBM",cZoomFile="GSCP_KGO",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 122523162


  add object oSELEZI_1_5 as StdCombo with uid="BXZNLWLWYK",rtseq=4,rtrep=.f.,left=256,top=416,width=115,height=22;
    , ToolTipText = "Tipo di ordini da selezionare";
    , HelpContextID = 50301658;
    , cFormVar="w_SELEZI",RowSource=""+"Tutti,"+"Solo i verdi,"+"Solo i gialli,"+"Solo i rossi,"+"Solo i neri", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELEZI_1_5.RadioValue()
    return(iif(this.value =1,"ALL",;
    iif(this.value =2,"008",;
    iif(this.value =3,"006",;
    iif(this.value =4,"004",;
    iif(this.value =5,"002",;
    space(3)))))))
  endfunc
  func oSELEZI_1_5.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_5.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="ALL",1,;
      iif(this.Parent.oContained.w_SELEZI=="008",2,;
      iif(this.Parent.oContained.w_SELEZI=="006",3,;
      iif(this.Parent.oContained.w_SELEZI=="004",4,;
      iif(this.Parent.oContained.w_SELEZI=="002",5,;
      0)))))
  endfunc


  add object oDESELEZI_1_6 as StdCombo with uid="ANWBFQCGXK",rtseq=5,rtrep=.f.,left=480,top=416,width=115,height=22;
    , ToolTipText = "Tipo di ordini da deselezionare";
    , HelpContextID = 136373375;
    , cFormVar="w_DESELEZI",RowSource=""+"Tutti,"+"Solo i verdi,"+"Solo i gialli,"+"Solo i rossi,"+"Solo i neri", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDESELEZI_1_6.RadioValue()
    return(iif(this.value =1,"ALL",;
    iif(this.value =2,"008",;
    iif(this.value =3,"006",;
    iif(this.value =4,"004",;
    iif(this.value =5,"002",;
    space(3)))))))
  endfunc
  func oDESELEZI_1_6.GetRadio()
    this.Parent.oContained.w_DESELEZI = this.RadioValue()
    return .t.
  endfunc

  func oDESELEZI_1_6.SetRadio()
    this.Parent.oContained.w_DESELEZI=trim(this.Parent.oContained.w_DESELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_DESELEZI=="ALL",1,;
      iif(this.Parent.oContained.w_DESELEZI=="008",2,;
      iif(this.Parent.oContained.w_DESELEZI=="006",3,;
      iif(this.Parent.oContained.w_DESELEZI=="004",4,;
      iif(this.Parent.oContained.w_DESELEZI=="002",5,;
      0)))))
  endfunc

  add object oLIVELLO_1_7 as StdRadio with uid="FYGVOWEDZG",rtseq=6,rtrep=.f.,left=791, top=150, width=95,height=96;
    , cFormVar="w_LIVELLO", ButtonCount=5, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oLIVELLO_1_7.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Verde"
      this.Buttons(1).HelpContextID = 14608202
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Giallo"
      this.Buttons(2).HelpContextID = 14608202
      this.Buttons(2).Top=18
      this.Buttons(3).Caption="Rosso"
      this.Buttons(3).HelpContextID = 14608202
      this.Buttons(3).Top=36
      this.Buttons(4).Caption="Nero"
      this.Buttons(4).HelpContextID = 14608202
      this.Buttons(4).Top=54
      this.Buttons(5).Caption="Non definito"
      this.Buttons(5).HelpContextID = 14608202
      this.Buttons(5).Top=72
      this.SetAll("Width",93)
      this.SetAll("Height",20)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oLIVELLO_1_7.RadioValue()
    return(iif(this.value =1,8,;
    iif(this.value =2,6,;
    iif(this.value =3,4,;
    iif(this.value =4,2,;
    iif(this.value =5,-1,;
    0))))))
  endfunc
  func oLIVELLO_1_7.GetRadio()
    this.Parent.oContained.w_LIVELLO = this.RadioValue()
    return .t.
  endfunc

  func oLIVELLO_1_7.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_LIVELLO==8,1,;
      iif(this.Parent.oContained.w_LIVELLO==6,2,;
      iif(this.Parent.oContained.w_LIVELLO==4,3,;
      iif(this.Parent.oContained.w_LIVELLO==2,4,;
      iif(this.Parent.oContained.w_LIVELLO==-1,5,;
      0)))))
  endfunc


  add object oObj_1_8 as cp_runprogram with uid="ETCXXYJAVX",left=1, top=460, width=298,height=19,;
    caption='GSCP_BG1(LIV_CHANGE)',;
   bGlobalFont=.t.,;
    prg="GSCP_BG1('LIV_CHANGE')",;
    cEvent = "w_LIVELLO Changed",;
    nPag=1;
    , HelpContextID = 2921160

  add object oNOTECONT_1_17 as StdMemo with uid="WMJGGAVWVN",rtseq=40,rtrep=.f.,;
    cFormVar = "w_NOTECONT", cQueryName = "NOTECONT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Resoconto controllo",;
    HelpContextID = 242155734,;
   bGlobalFont=.t.,;
    Height=56, Width=592, Left=7, Top=336, Enabled=.T., ReadOnly=.T.


  add object oObj_1_19 as cp_runprogram with uid="INOSBKBWDJ",left=1, top=486, width=298,height=19,;
    caption='GSCP_BG1(AFTER_QUERY)',;
   bGlobalFont=.t.,;
    prg="GSCP_BG1('AFTER_QUERY')",;
    cEvent = "Blank, Interroga",;
    nPag=1;
    , ToolTipText = "L'evento after login non funziona in CP b43";
    , HelpContextID = 267518136


  add object oObj_1_20 as cp_runprogram with uid="PGFDSWNCPQ",left=1, top=512, width=463,height=19,;
    caption='GSCP_BG1(ROW_CHECKED)',;
   bGlobalFont=.t.,;
    prg="GSCP_BG1('ROW_CHECKED')",;
    cEvent = "w_ZoomSel row checked,ZOOMSEL row checked",;
    nPag=1;
    , HelpContextID = 226503028


  add object oBtn_1_21 as StdButton with uid="TWGVRNQCMX",left=791, top=14, width=48,height=45,;
    CpPicture="bmp\doc.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza ordine selezionato";
    , HelpContextID = 244631590;
    , Caption='\<Visualiz';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        GSCP_BG1(this.Parent.oContained,"SHOW_ORDI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_ORSERIAL))
      endwith
    endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="RIQPANXCNZ",left=791, top=69, width=48,height=45,;
    CpPicture="bmp\NOTE.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza / edita note ordine";
    , HelpContextID = 243474134;
    , Caption='\<Note';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      do gscp_kno with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_CPIN='S')
     endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="ASZABIWNPH",left=791, top=250, width=48,height=45,;
    CpPicture="bmp\Controlla.bmp", caption="", nPag=1;
    , ToolTipText = "Controllo automatico ordine web / seleziona regole";
    , HelpContextID = 189676158;
    , Caption='\<Controllo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      do gscp_kco with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_24 as StdButton with uid="WSTFHZNPWH",left=75, top=397, width=48,height=45,;
    CpPicture="BMP\GENERA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare gli ordini selezionati";
    , HelpContextID = 142286420;
    , Caption='E\<labora';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      this.parent.oContained.NotifyEvent("elabora")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_25 as StdButton with uid="SESTEYHVOC",left=202, top=397, width=48,height=45,;
    CpPicture="BMP\parametri.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare";
    , HelpContextID = 14509148;
    , Caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      this.parent.oContained.NotifyEvent("Selezionatutto")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_26 as StdButton with uid="NTRJJJEFYN",left=428, top=397, width=48,height=45,;
    CpPicture="BMP\deselez.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare";
    , HelpContextID = 172591503;
    , Caption='\<Desel';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        GSCP_BG1(this.Parent.oContained,"DESELE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_27 as StdButton with uid="APRCFTBFHJ",left=791, top=397, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 243667526;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_29 as cp_runprogram with uid="OTRKNQCWFQ",left=304, top=459, width=148,height=28,;
    caption='gscp_bgo',;
   bGlobalFont=.t.,;
    prg="gscp_bgo",;
    cEvent = "elabora",;
    nPag=1;
    , HelpContextID = 108860629


  add object oObj_1_30 as cp_runprogram with uid="TADTEJKYIU",left=462, top=463, width=214,height=24,;
    caption='GSCP_BG1(SELE)',;
   bGlobalFont=.t.,;
    prg="GSCP_BG1('SELE')",;
    cEvent = "Selezionatutto",;
    nPag=1;
    , HelpContextID = 66820119

  add object oStr_1_9 as StdString with uid="XPHZJQZNEH",Visible=.t., Left=770, Top=127,;
    Alignment=2, Width=112, Height=19,;
    Caption="Livello conformit�"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .f., FontItalic=.f., FontUnderline=.t., FontStrikeThru=.f.
enddefine
define class tgscp_kgoPag2 as StdContainer
  Width  = 886
  height = 442
  stdWidth  = 886
  stdheight = 442
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_2_1 as StdField with uid="ESJWWSEESP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio evasione di selezione (vuota=nessuna selezione)",;
    HelpContextID = 62590922,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=119, Top=63

  add object oDATFIN_2_2 as StdField with uid="CSPFXUGMMR",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine evasione di selezione (vuota=nessuna selezione)",;
    HelpContextID = 252579786,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=255, Top=63

  add object oCODINI_2_3 as StdField with uid="MUFCPEQKPM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale deve essere minore o uguale al codice finale",;
    ToolTipText = "Codice cliente di inizio selezione (spazio=no selezione)",;
    HelpContextID = 62652890,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=119, Top=114, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MVTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODINI"

  func oCODINI_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_MVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_MVTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODINI_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oCODINI_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_MVTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODINI
     i_obj.ecpSave()
  endproc

  add object oCODFIN_2_4 as StdField with uid="TINZHQJRQP",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice finale deve essere maggiore o uguale al codice iniziale",;
    ToolTipText = "Codice cliente di fine selezione (spazio=no selezione)",;
    HelpContextID = 252641754,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=119, Top=141, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MVTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFIN"

  func oCODFIN_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_MVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_MVTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFIN_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oCODFIN_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_MVTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODFIN
     i_obj.ecpSave()
  endproc

  add object oDESCL1_2_9 as StdField with uid="UINDRETMJH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCL1", cQueryName = "DESCL1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 199302090,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=255, Top=114, InputMask=replicate('X',40)

  add object oDESCL2_2_10 as StdField with uid="FRZKIOCAYP",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCL2", cQueryName = "DESCL2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 182524874,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=255, Top=141, InputMask=replicate('X',40)

  add object oCODAGE_2_11 as StdField with uid="PKTWKDYTRG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODAGE", cQueryName = "CODAGE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 137626074,;
   bGlobalFont=.t.,;
    Height=21, Width=73, Left=119, Top=200, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE"

  func oCODAGE_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oCODZON_2_12 as StdField with uid="VQIIKJKSYG",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CODZON", cQueryName = "CODZON",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 245039578,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=119, Top=238, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ZONE", oKey_1_1="ZOCODZON", oKey_1_2="this.w_CODZON"

  func oCODZON_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODZON_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODZON_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oCODZON_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Zone",'',this.parent.oContained
  endproc

  add object oAGDESAGE_2_13 as StdField with uid="ZZYEVVWWSA",rtseq=15,rtrep=.f.,;
    cFormVar = "w_AGDESAGE", cQueryName = "AGDESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 76543563,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=202, Top=200, InputMask=replicate('X',35)

  add object oZODESZON_2_15 as StdField with uid="AHWYWTPENT",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ZODESZON", cQueryName = "ZODESZON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 40894492,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=164, Top=238, InputMask=replicate('X',35)


  add object oBtn_2_17 as StdButton with uid="MXRYYILHXM",left=621, top=47, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per eseguire l'interrogazione";
    , HelpContextID = 140303363;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_17.Click()
      with this.Parent.oContained
        GSCP_BG1(this.Parent.oContained,"INTERROGA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_5 as StdString with uid="JMEGJREIFI",Visible=.t., Left=12, Top=65,;
    Alignment=1, Width=102, Height=22,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="BSXDCPCNAM",Visible=.t., Left=202, Top=65,;
    Alignment=1, Width=46, Height=22,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="RMYIYFSGBS",Visible=.t., Left=49, Top=116,;
    Alignment=1, Width=65, Height=22,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="SZAIBFYRJU",Visible=.t., Left=40, Top=143,;
    Alignment=1, Width=74, Height=22,;
    Caption="A cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="JBKOIROIPD",Visible=.t., Left=73, Top=202,;
    Alignment=1, Width=41, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="ZWKRXFDKLG",Visible=.t., Left=83, Top=240,;
    Alignment=1, Width=31, Height=18,;
    Caption="Zona:"  ;
  , bGlobalFont=.t.
enddefine
define class tgscp_kgoPag3 as StdContainer
  Width  = 886
  height = 442
  stdWidth  = 886
  stdheight = 442
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMVDATREG_3_2 as StdField with uid="MZHWUSZVJN",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MVDATREG", cQueryName = "MVDATREG",;
    bObbl = .t. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione dei documenti generati",;
    HelpContextID = 94111245,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=150, Top=37

  add object oMVCODESE_3_3 as StdField with uid="FDVMXBDOAS",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MVCODESE", cQueryName = "MVCODESE",;
    bObbl = .t. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio",;
    HelpContextID = 139856373,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=349, Top=37, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_MVCODESE"

  func oMVCODESE_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODESE_3_3.ecpDrop(oSource)
    this.Parent.oContained.link_3_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODESE_3_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oMVCODESE_3_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oMVDATCIV_3_4 as StdField with uid="NTJZTDVRDP",rtseq=20,rtrep=.f.,;
    cFormVar = "w_MVDATCIV", cQueryName = "MVDATCIV",;
    bObbl = .t. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di competenza IVA",;
    HelpContextID = 110888476,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=150, Top=64

  add object oDATDIV_3_5 as StdField with uid="HRERSSBLJE",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DATDIV", cQueryName = "DATDIV",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale data di inizio scadenze per pagamenti in data diversa",;
    HelpContextID = 118493130,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=406, Top=64

  add object oMVCODBUN_3_6 as StdField with uid="WHFMPXMEKV",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MVCODBUN", cQueryName = "MVCODBUN",;
    bObbl = .t. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Businnes Unit",;
    HelpContextID = 190188012,;
   bGlobalFont=.t.,;
    Height=21, Width=38, Left=150, Top=92, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BUSIUNIT", oKey_1_1="BUCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="BUCODICE", oKey_2_2="this.w_MVCODBUN"

  func oMVCODBUN_3_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERBUN<>'N')
    endwith
   endif
  endfunc

  func oMVCODBUN_3_6.mHide()
    with this.Parent.oContained
      return (g_APPLICATION='ADHOC REVOLUTION')
    endwith
  endfunc

  func oMVCODBUN_3_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVCODBUN_3_6.ecpDrop(oSource)
    this.Parent.oContained.link_3_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVCODBUN_3_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oMVCODBUN_3_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oMVTCOMME_3_7 as StdField with uid="NAOCJUVPIF",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MVTCOMME", cQueryName = "MVTCOMME",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 263256565,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=150, Top=120, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_MVTCOMME"

  func oMVTCOMME_3_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_7('Part',this)
      if .not. empty(.w_MVTCOATT)
        bRes2=.link_3_8('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMVTCOMME_3_7.ecpDrop(oSource)
    this.Parent.oContained.link_3_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVTCOMME_3_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oMVTCOMME_3_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oMVTCOATT_3_8 as StdField with uid="RICWPZWAUP",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MVTCOATT", cQueryName = "MVTCOATT",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 196147686,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=150, Top=147, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", oKey_1_1="ATCODCOM", oKey_1_2="this.w_MVTCOMME", oKey_2_1="ATTIPATT", oKey_2_2="this.w_A", oKey_3_1="ATCODATT", oKey_3_2="this.w_MVTCOATT"

  func oMVTCOATT_3_8.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  func oMVTCOATT_3_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oMVTCOATT_3_8.ecpDrop(oSource)
    this.Parent.oContained.link_3_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMVTCOATT_3_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_MVTCOMME)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_A)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_MVTCOMME)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_A)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oMVTCOATT_3_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oBUDESCRI_3_11 as StdField with uid="AMJFAWIIAZ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_BUDESCRI", cQueryName = "BUDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 158333857,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=193, Top=92, InputMask=replicate('X',40)

  func oBUDESCRI_3_11.mHide()
    with this.Parent.oContained
      return (g_APPLICATION='ADHOC REVOLUTION')
    endwith
  endfunc

  add object oCNDESCAN_3_14 as StdField with uid="IPMPJWOLXE",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CNDESCAN", cQueryName = "CNDESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 158335628,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=269, Top=120, InputMask=replicate('X',30)

  add object oDESATT_3_20 as StdField with uid="FOEXWMJTUO",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 140712906,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=269, Top=147, InputMask=replicate('X',30)

  func oDESATT_3_20.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oVALNAZ_3_21 as StdField with uid="SPQXZBHVDT",rtseq=35,rtrep=.f.,;
    cFormVar = "w_VALNAZ", cQueryName = "VALNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta di conto esercizio",;
    HelpContextID = 59149994,;
   bGlobalFont=.t.,;
    Height=22, Width=62, Left=420, Top=37, InputMask=replicate('X',3)

  add object oStr_3_9 as StdString with uid="EYAUUNNDLN",Visible=.t., Left=12, Top=67,;
    Alignment=1, Width=133, Height=15,;
    Caption="Data competenza IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_3_10 as StdString with uid="IEFMNXNWZI",Visible=.t., Left=253, Top=66,;
    Alignment=1, Width=144, Height=15,;
    Caption="Scadenza in data diversa:"  ;
  , bGlobalFont=.t.

  add object oStr_3_12 as StdString with uid="SYHGCMZXMO",Visible=.t., Left=44, Top=95,;
    Alignment=1, Width=101, Height=18,;
    Caption="Business Unit:"  ;
  , bGlobalFont=.t.

  func oStr_3_12.mHide()
    with this.Parent.oContained
      return (g_APPLICATION='ADHOC REVOLUTION')
    endwith
  endfunc

  add object oStr_3_13 as StdString with uid="FDESGSKJIC",Visible=.t., Left=76, Top=120,;
    Alignment=1, Width=69, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  add object oStr_3_15 as StdString with uid="FCXKNRHESK",Visible=.t., Left=37, Top=40,;
    Alignment=1, Width=108, Height=18,;
    Caption="Data registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_16 as StdString with uid="SBMXPADKAG",Visible=.t., Left=289, Top=39,;
    Alignment=1, Width=53, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_3_19 as StdString with uid="LFWGCSAHSA",Visible=.t., Left=93, Top=147,;
    Alignment=1, Width=52, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_3_19.mHide()
    with this.Parent.oContained
      return (g_COMM<>'S')
    endwith
  endfunc

  add object oStr_3_24 as StdString with uid="LWJQMMNYGO",Visible=.t., Left=402, Top=39,;
    Alignment=0, Width=18, Height=19,;
    Caption="in"  ;
  , bGlobalFont=.t.
enddefine
define class tgscp_kgoPag4 as StdContainer
  Width  = 886
  height = 442
  stdWidth  = 886
  stdheight = 442
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

    add object oBmp_4_10 as image with uid="GFPOVOTKBL",left=40, top=337, width=25,height=28,;
      Picture="bmp\green.ico"

    add object oBmp_4_11 as image with uid="WDMPEQJEET",left=40, top=122, width=25,height=28,;
      Picture="bmp\red.ico"

    add object oBmp_4_12 as image with uid="TJODCAIDVQ",left=40, top=223, width=25,height=28,;
      Picture="bmp\yellow.ico"

    add object oBmp_4_13 as image with uid="HQATJDSKXJ",left=40, top=14, width=25,height=28,;
      Picture="bmp\black.ico"

  add object oMVTIPDOC2_4_2 as StdField with uid="OTQXMXWEBN",rtseq=44,rtrep=.f.,;
    cFormVar = "w_MVTIPDOC2", cQueryName = "MVTIPDOC2",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale documenti di vendita",;
    HelpContextID = 144373463,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=162, Top=46, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MVTIPDOC2"

  func oMVTIPDOC2_4_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCODMAG2_4_3 as StdField with uid="EUUUNSCPBK",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CODMAG2", cQueryName = "CODMAG2",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 109576666,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=162, Top=68, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG2"

  func oCODMAG2_4_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCENCOS2_4_5 as StdField with uid="AYLLNAXJVY",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CENCOS2", cQueryName = "CENCOS2",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo",;
    HelpContextID = 162622426,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=162, Top=91, InputMask=replicate('X',15), cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CENCOS2"

  func oCENCOS2_4_5.mHide()
    with this.Parent.oContained
      return (!((.w_FLANBU='S' Or g_PERBUN='N') And g_PERCCM='S'))
    endwith
  endfunc

  func oCENCOS2_4_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oTDDESDOC2_4_6 as StdField with uid="CSOVVINJYH",rtseq=48,rtrep=.f.,;
    cFormVar = "w_TDDESDOC2", cQueryName = "TDDESDOC2",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 141559911,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=230, Top=46, InputMask=replicate('X',35)

  add object oCCDESPIA2_4_9 as StdField with uid="VSBFLVEZXX",rtseq=49,rtrep=.f.,;
    cFormVar = "w_CCDESPIA2", cQueryName = "CCDESPIA2",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 59766151,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=280, Top=91, InputMask=replicate('X',40)

  func oCCDESPIA2_4_9.mHide()
    with this.Parent.oContained
      return (!((.w_FLANBU='S' Or g_PERBUN='N') And g_PERCCM='S'))
    endwith
  endfunc

  add object oDESMAG2_4_24 as StdField with uid="QMCZXBAJYT",rtseq=59,rtrep=.f.,;
    cFormVar = "w_DESMAG2", cQueryName = "DESMAG2",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 109517770,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=230, Top=68, InputMask=replicate('X',30)

  add object oMVTIPDOC4_4_29 as StdField with uid="NYONNVTHQK",rtseq=64,rtrep=.f.,;
    cFormVar = "w_MVTIPDOC4", cQueryName = "MVTIPDOC4",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale documenti di vendita",;
    HelpContextID = 144373431,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=162, Top=152, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MVTIPDOC4"

  func oMVTIPDOC4_4_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCODMAG4_4_30 as StdField with uid="CLRDPAWYPR",rtseq=65,rtrep=.f.,;
    cFormVar = "w_CODMAG4", cQueryName = "CODMAG4",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 109576666,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=162, Top=174, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG4"

  func oCODMAG4_4_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCENCOS4_4_31 as StdField with uid="QEBJGIMFXZ",rtseq=66,rtrep=.f.,;
    cFormVar = "w_CENCOS4", cQueryName = "CENCOS4",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo",;
    HelpContextID = 162622426,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=162, Top=197, InputMask=replicate('X',15), cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CENCOS4"

  func oCENCOS4_4_31.mHide()
    with this.Parent.oContained
      return (!((.w_FLANBU='S' Or g_PERBUN='N') And g_PERCCM='S'))
    endwith
  endfunc

  func oCENCOS4_4_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oTDDESDOC4_4_32 as StdField with uid="YFQUZQTMLL",rtseq=67,rtrep=.f.,;
    cFormVar = "w_TDDESDOC4", cQueryName = "TDDESDOC4",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 141559879,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=230, Top=152, InputMask=replicate('X',35)

  add object oCCDESPIA4_4_35 as StdField with uid="PNEIMDEIOY",rtseq=68,rtrep=.f.,;
    cFormVar = "w_CCDESPIA4", cQueryName = "CCDESPIA4",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 59766183,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=280, Top=197, InputMask=replicate('X',40)

  func oCCDESPIA4_4_35.mHide()
    with this.Parent.oContained
      return (!((.w_FLANBU='S' Or g_PERBUN='N') And g_PERCCM='S'))
    endwith
  endfunc

  add object oDESMAG4_4_37 as StdField with uid="SLWBDURCEP",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DESMAG4", cQueryName = "DESMAG4",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 109517770,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=230, Top=174, InputMask=replicate('X',30)

  add object oMVTIPDOC6_4_48 as StdField with uid="XMHWDEKTVB",rtseq=80,rtrep=.f.,;
    cFormVar = "w_MVTIPDOC6", cQueryName = "MVTIPDOC6",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale documenti di vendita",;
    HelpContextID = 144373399,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=162, Top=265, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MVTIPDOC6"

  func oMVTIPDOC6_4_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCODMAG6_4_49 as StdField with uid="RNTNGFNPPC",rtseq=81,rtrep=.f.,;
    cFormVar = "w_CODMAG6", cQueryName = "CODMAG6",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 158858790,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=162, Top=287, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG6"

  func oCODMAG6_4_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCENCOS6_4_50 as StdField with uid="PZKIQPTPGI",rtseq=82,rtrep=.f.,;
    cFormVar = "w_CENCOS6", cQueryName = "CENCOS6",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo",;
    HelpContextID = 105813030,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=162, Top=310, InputMask=replicate('X',15), cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CENCOS6"

  func oCENCOS6_4_50.mHide()
    with this.Parent.oContained
      return (!((.w_FLANBU='S' Or g_PERBUN='N') And g_PERCCM='S'))
    endwith
  endfunc

  func oCENCOS6_4_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oTDDESDOC6_4_51 as StdField with uid="VBYNNEETNZ",rtseq=83,rtrep=.f.,;
    cFormVar = "w_TDDESDOC6", cQueryName = "TDDESDOC6",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 141559847,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=230, Top=265, InputMask=replicate('X',35)

  add object oCCDESPIA6_4_54 as StdField with uid="JRIATHBHOY",rtseq=84,rtrep=.f.,;
    cFormVar = "w_CCDESPIA6", cQueryName = "CCDESPIA6",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 59766215,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=280, Top=310, InputMask=replicate('X',40)

  func oCCDESPIA6_4_54.mHide()
    with this.Parent.oContained
      return (!((.w_FLANBU='S' Or g_PERBUN='N') And g_PERCCM='S'))
    endwith
  endfunc

  add object oDESMAG6_4_56 as StdField with uid="ZNOMYSEEJF",rtseq=85,rtrep=.f.,;
    cFormVar = "w_DESMAG6", cQueryName = "DESMAG6",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 158917686,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=230, Top=287, InputMask=replicate('X',30)

  add object oMVTIPDOC8_4_57 as StdField with uid="JLHNTKNWXN",rtseq=86,rtrep=.f.,;
    cFormVar = "w_MVTIPDOC8", cQueryName = "MVTIPDOC8",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale documenti di vendita",;
    HelpContextID = 144373367,;
   bGlobalFont=.t.,;
    Height=21, Width=64, Left=162, Top=372, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_MVTIPDOC8"

  func oMVTIPDOC8_4_57.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCODMAG8_4_58 as StdField with uid="CLSYCVAUAK",rtseq=87,rtrep=.f.,;
    cFormVar = "w_CODMAG8", cQueryName = "CODMAG8",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 158858790,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=162, Top=394, InputMask=replicate('X',5), cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG8"

  func oCODMAG8_4_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCENCOS8_4_59 as StdField with uid="OZSKRRWXKY",rtseq=88,rtrep=.f.,;
    cFormVar = "w_CENCOS8", cQueryName = "CENCOS8",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Centro di costo",;
    HelpContextID = 105813030,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=162, Top=417, InputMask=replicate('X',15), cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_CENCOS8"

  func oCENCOS8_4_59.mHide()
    with this.Parent.oContained
      return (!((.w_FLANBU='S' Or g_PERBUN='N') And g_PERCCM='S'))
    endwith
  endfunc

  func oCENCOS8_4_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oTDDESDOC8_4_60 as StdField with uid="FNLIKAUGCS",rtseq=89,rtrep=.f.,;
    cFormVar = "w_TDDESDOC8", cQueryName = "TDDESDOC8",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 141559815,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=230, Top=372, InputMask=replicate('X',35)

  add object oCCDESPIA8_4_63 as StdField with uid="TQJVLGFDLL",rtseq=90,rtrep=.f.,;
    cFormVar = "w_CCDESPIA8", cQueryName = "CCDESPIA8",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 59766247,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=280, Top=417, InputMask=replicate('X',40)

  func oCCDESPIA8_4_63.mHide()
    with this.Parent.oContained
      return (!((.w_FLANBU='S' Or g_PERBUN='N') And g_PERCCM='S'))
    endwith
  endfunc

  add object oDESMAG8_4_65 as StdField with uid="EXGDNTGIWB",rtseq=91,rtrep=.f.,;
    cFormVar = "w_DESMAG8", cQueryName = "DESMAG8",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 158917686,;
   bGlobalFont=.t.,;
    Height=21, Width=273, Left=230, Top=394, InputMask=replicate('X',30)


  add object oObj_4_86 as cp_calclbl with uid="YIHRGIZLES",left=87, top=17, width=139,height=19,;
    caption='',;
   bGlobalFont=.t.,;
    caption="",Alignment=0,BackStyle=0,;
    nPag=4;
    , HelpContextID = 236350198


  add object oObj_4_87 as cp_calclbl with uid="WCGUAAZWGA",left=87, top=124, width=139,height=19,;
    caption='',;
   bGlobalFont=.t.,;
    caption="",Alignment=0,BackStyle=0,;
    nPag=4;
    , HelpContextID = 236350198


  add object oObj_4_88 as cp_calclbl with uid="CSKNVFOFVI",left=87, top=227, width=139,height=19,;
    caption='',;
   bGlobalFont=.t.,;
    caption="",Alignment=0,BackStyle=0,;
    nPag=4;
    , HelpContextID = 236350198


  add object oObj_4_89 as cp_calclbl with uid="XVIYKGHCZO",left=87, top=340, width=139,height=19,;
    caption='',;
   bGlobalFont=.t.,;
    caption="",Alignment=0,BackStyle=0,;
    nPag=4;
    , HelpContextID = 236350198


  add object oObj_4_90 as cp_runprogram with uid="HNDCIDOZCI",left=23, top=462, width=237,height=19,;
    caption='GSCP_BGK',;
   bGlobalFont=.t.,;
    prg="GSCP_BGK",;
    cEvent = "Init",;
    nPag=4;
    , HelpContextID = 106623665

  add object oStr_4_7 as StdString with uid="YUENVVUUOK",Visible=.t., Left=40, Top=49,;
    Alignment=1, Width=117, Height=15,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_8 as StdString with uid="EZKEOUNSFV",Visible=.t., Left=65, Top=91,;
    Alignment=1, Width=92, Height=15,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_4_8.mHide()
    with this.Parent.oContained
      return (!((.w_FLANBU='S' Or g_PERBUN='N') And g_PERCCM='S'))
    endwith
  endfunc

  add object oStr_4_23 as StdString with uid="JVBNVLSCBA",Visible=.t., Left=88, Top=69,;
    Alignment=1, Width=69, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_4_33 as StdString with uid="TGHXYESPMQ",Visible=.t., Left=40, Top=155,;
    Alignment=1, Width=117, Height=15,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_34 as StdString with uid="GWMRFIODVZ",Visible=.t., Left=65, Top=197,;
    Alignment=1, Width=92, Height=15,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_4_34.mHide()
    with this.Parent.oContained
      return (!((.w_FLANBU='S' Or g_PERBUN='N') And g_PERCCM='S'))
    endwith
  endfunc

  add object oStr_4_36 as StdString with uid="JKUQCWISYY",Visible=.t., Left=88, Top=175,;
    Alignment=1, Width=69, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_4_52 as StdString with uid="CALLNOTKSJ",Visible=.t., Left=40, Top=268,;
    Alignment=1, Width=117, Height=15,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_53 as StdString with uid="LHPYMPJLOS",Visible=.t., Left=65, Top=310,;
    Alignment=1, Width=92, Height=15,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_4_53.mHide()
    with this.Parent.oContained
      return (!((.w_FLANBU='S' Or g_PERBUN='N') And g_PERCCM='S'))
    endwith
  endfunc

  add object oStr_4_55 as StdString with uid="NVNOJXJWWM",Visible=.t., Left=88, Top=288,;
    Alignment=1, Width=69, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_4_61 as StdString with uid="YUSOWXDHAJ",Visible=.t., Left=40, Top=375,;
    Alignment=1, Width=117, Height=15,;
    Caption="Causale documento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_62 as StdString with uid="ERBMPTRYYX",Visible=.t., Left=65, Top=417,;
    Alignment=1, Width=92, Height=15,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_4_62.mHide()
    with this.Parent.oContained
      return (!((.w_FLANBU='S' Or g_PERBUN='N') And g_PERCCM='S'))
    endwith
  endfunc

  add object oStr_4_64 as StdString with uid="YNXXULCUWG",Visible=.t., Left=88, Top=395,;
    Alignment=1, Width=69, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscp_kgo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
