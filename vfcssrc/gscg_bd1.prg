* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bd1                                                        *
*              Carica dati attivit�                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_17]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-03                                                      *
* Last revis.: 2002-09-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bd1",oParentObject)
return(i_retval)

define class tgscg_bd1 as StdBatch
  * --- Local variables
  w_ULTDAT = ctod("  /  /  ")
  w_WHERE = space(5)
  * --- WorkFile variables
  ATTIDETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura dati stampe registri dalle Attivit� (da GSCG_SDI e GSCG_SDA)
    if this.oParentObject.w_MTIPLIQ="S"
      this.w_WHERE = IIF(EMPTY(this.oParentObject.w_CODATT),g_CATAZI,this.oParentObject.w_CODATT)
    else
      this.w_WHERE = g_CATAZI
    endif
    this.oParentObject.w_PRPARI = 0
    this.oParentObject.w_PREFIS = " "
    this.w_ULTDAT = cp_CharToDate("  -  -    ")
    this.oParentObject.w_INTLIG = " "
    this.oParentObject.w_ATTIPREG = " "
    this.oParentObject.w_ATNUMREG = 0
    * --- Select from ATTIDETT
    i_nConn=i_TableProp[this.ATTIDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIDETT ";
          +" where ATCODATT="+cp_ToStrODBC(this.w_WHERE)+" AND ATFLREGI='S'";
           ,"_Curs_ATTIDETT")
    else
      select * from (i_cTable);
       where ATCODATT=this.w_WHERE AND ATFLREGI="S";
        into cursor _Curs_ATTIDETT
    endif
    if used('_Curs_ATTIDETT')
      select _Curs_ATTIDETT
      locate for 1=1
      do while not(eof())
      this.w_ULTDAT = CP_TODATE(_Curs_ATTIDETT.ATDATSTA)
      this.oParentObject.w_INTLIG = NVL(_Curs_ATTIDETT.ATSTAINT,SPACE(1))
      this.oParentObject.w_PREFIS = NVL(_Curs_ATTIDETT.ATPREFIS,SPACE(1))
      this.oParentObject.w_ATTIPREG = _Curs_ATTIDETT.ATTIPREG
      this.oParentObject.w_ATNUMREG = _Curs_ATTIDETT.ATNUMREG
      if (VAL(this.oParentObject.w_ANNO) > YEAR(this.w_ULTDAT)) AND NOT EMPTY(this.w_ULTDAT)
        this.oParentObject.w_PRPARI = 0
      else
        this.oParentObject.w_PRPARI = NVL(_Curs_ATTIDETT.ATPRPARI, 0)
      endif
        select _Curs_ATTIDETT
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ATTIDETT'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
