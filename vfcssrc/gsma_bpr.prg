* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bpr                                                        *
*              Pubblicazione codici ricerca                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_35]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-11                                                      *
* Last revis.: 2002-03-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_SCELTA
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bpr",oParentObject,m.w_SCELTA)
return(i_retval)

define class tgsma_bpr as StdBatch
  * --- Local variables
  w_SCELTA = space(3)
  CURSORE = space(10)
  w_MODIFICATO = .f.
  w_CODICE = space(20)
  w_DATOBSO = ctod("  /  /  ")
  w_TIPART2 = space(2)
  * --- WorkFile variables
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine richiamata dalla maschera gsma_kpu
    * --- Variabili Caller
    * --- Variabili locali
    do case
      case this.w_SCELTA = "OPT"
        * --- Selezione o deselezione di tutti i programmi visualizzati nella maschera.
        this.CURSORE = this.oParentObject.w_ZOOMPRO.cCursor
        if this.oParentObject.w_OPZIONI = "SEL"
          UPDATE (this.CURSORE) SET XCHK = 1
        else
          UPDATE (this.CURSORE) SET XCHK = 0
        endif
        SELECT(this.oParentObject.w_ZOOMPRO.cCursor)
        GO TOP
        this.oParentObject.w_ZOOMPRO.refresh()
      case this.w_SCELTA = "MOD"
        * --- Aggiornamento prodotti pubblicati
        this.w_MODIFICATO = .F.
        this.CURSORE = this.oParentObject.w_ZOOMPRO.cCursor
        Select (this.CURSORE)
        go top
        SCAN FOR XCHK=1
        this.w_MODIFICATO = .T.
        this.w_CODICE = ARCODART
        * --- Write into KEY_ARTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CAPUBWEB ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PUBBLICA),'KEY_ARTI','CAPUBWEB');
              +i_ccchkf ;
          +" where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CODICE);
                 )
        else
          update (i_cTable) set;
              CAPUBWEB = this.oParentObject.w_PUBBLICA;
              &i_ccchkf. ;
           where;
              CACODICE = this.w_CODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        ENDSCAN
        if this.w_MODIFICATO = .F.
          ah_ErrorMsg("� necessario selezionare almeno un codice di ricerca","!")
        else
          this.oParentObject.notifyevent("Interroga")
          do case
            case this.oParentObject.w_PUBBLICA="S"
              ah_ErrorMsg("I codici di ricerca selezionati sono stati marcati come <pubblicati>","i")
            case this.oParentObject.w_PUBBLICA="T"
              ah_ErrorMsg("I codici di ricerca selezionati sono stati marcati come <trasferiti>","i")
            case this.oParentObject.w_PUBBLICA="N"
              ah_ErrorMsg("I codici di ricerca selezionati sono stati marcati come <non pubblicati>","i")
          endcase
        endif
      case this.w_SCELTA = "CCD"
        if NOT EMPTY(this.oParentObject.w_DACODICE)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARTIPART,ARDTOBSO"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_DACODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARTIPART,ARDTOBSO;
              from (i_cTable) where;
                  ARCODART = this.oParentObject.w_DACODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPART2 = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
            this.w_DATOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if not ((EMPTY(this.w_DATOBSO) OR this.w_DATOBSO>i_DATSYS) and ((this.w_TIPART2 $ "FM-FO-DE" AND this.oParentObject.w_TIPART="S") or (this.w_TIPART2 $ "PF-SE-MP-MC-MA-IM-PM" AND this.oParentObject.w_TIPART="A") or (this.w_TIPART2="AC" AND this.oParentObject.w_TIPART="C")) and (this.oParentObject.w_DACODART<=this.oParentObject.w_ACODART or empty(this.oParentObject.w_ACODART)))
            ah_ErrorMsg("Prodotto obsoleto, di tipologia differente da quella selezionata o maggiore di quello finale","stop")
            this.oParentObject.w_DACODART = SPACE(20)
            this.oParentObject.w_DACODICE = SPACE(20)
            this.oParentObject.w_DADESART = SPACE(40)
          endif
        endif
      case this.w_SCELTA = "CCA"
        if NOT EMPTY(this.oParentObject.w_ACODICE)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARTIPART,ARDTOBSO"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_ACODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARTIPART,ARDTOBSO;
              from (i_cTable) where;
                  ARCODART = this.oParentObject.w_ACODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPART2 = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
            this.w_DATOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT( (EMPTY(this.w_DATOBSO) OR this.w_DATOBSO>i_DATSYS) and ((this.w_TIPART2 $ "FM-FO-DE" AND this.oParentObject.w_TIPART="S") or (this.w_TIPART2 $ "PF-SE-MP-MC-MA-IM-PM" AND this.oParentObject.w_TIPART="A") or (this.w_TIPART2="AC" AND this.oParentObject.w_TIPART="C")) and (this.oParentObject.w_ACODART>=this.oParentObject.w_DACODART or empty(this.oParentObject.w_DACODART)))
            ah_ErrorMsg("Prodotto obsoleto, di tipologia differente da quella selezionata o minore di quello iniziale","stop")
            this.oParentObject.w_ACODART = SPACE(20)
            this.oParentObject.w_ACODICE = SPACE(20)
            this.oParentObject.w_ADESART = SPACE(40)
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,w_SCELTA)
    this.w_SCELTA=w_SCELTA
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='KEY_ARTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_SCELTA"
endproc
