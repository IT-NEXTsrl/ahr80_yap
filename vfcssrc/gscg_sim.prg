* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_sim                                                        *
*              Riepilogo dichiarazione IVA periodica                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_361]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-21                                                      *
* Last revis.: 2010-06-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_sim",oParentObject))

* --- Class definition
define class tgscg_sim as StdForm
  Top    = 4
  Left   = 94

  * --- Standard Properties
  Width  = 642
  Height = 430+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-06-21"
  HelpContextID=9820823
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=82

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  COC_MAST_IDX = 0
  COD_ABI_IDX = 0
  COD_CAB_IDX = 0
  cPrg = "gscg_sim"
  cComment = "Riepilogo dichiarazione IVA periodica"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_VP__ANNO = space(4)
  w_VPPERIOD = 0
  w_VPCODVAL = space(1)
  w_COLSHOW = 0
  w_COLEDIT = 0
  w_MAGTRI = 0
  w_MINACC = 0
  w_MINVER = 0
  w_F2DEFI = space(1)
  w_IMPVAL = space(3)
  w_VPVARIMP = space(1)
  w_VPCORTER = space(1)
  w_RAGAZI = space(30)
  w_TIPDIC = space(1)
  w_VPDICGRU = space(1)
  w_VPDICSOC = space(1)
  w_FLTEST = space(1)
  w_VISPDF = space(1)
  w_VPIMPOR1 = 0
  w_VPCESINT = 0
  w_VPIMPOR2 = 0
  w_VPACQINT = 0
  w_VPIMPON3 = 0
  w_VPIMPOS3 = 0
  w_VPIMPOR5 = 0
  w_VPIMPOR6 = 0
  w_VPIMPOR7 = 0
  w_IMPDEB7 = 0
  w_IMPCRE7 = 0
  w_IMPDEB8 = 0
  w_IMPCRE8 = 0
  w_VPIMPOR8 = 0
  w_IMPDEB9 = 0
  w_IMPCRE9 = 0
  w_VPIMPOR9 = 0
  w_IMPDEB10 = 0
  w_IMPCRE10 = 0
  w_VPIMPO10 = 0
  w_TOTALE = 0
  w_TOTDEB = 0
  w_TOTCRE = 0
  w_VPIMPO11 = 0
  o_VPIMPO11 = 0
  w_VPIMPO12 = 0
  o_VPIMPO12 = 0
  w_IMPDEB12 = 0
  w_IMPCRE12 = 0
  w_VPIMPO13 = 0
  o_VPIMPO13 = 0
  w_VPIMPO14 = 0
  w_VPIMPO15 = 0
  w_VPIMPO16 = 0
  o_VPIMPO16 = 0
  w_VPIMPVER = 0
  o_VPIMPVER = 0
  w_VPVEREUR = space(1)
  w_CRERES = 0
  w_VPVERNEF = space(1)
  w_VPAR74C5 = space(1)
  w_VPAR74C4 = space(1)
  w_VPOPMEDI = space(1)
  w_VPOPNOIM = space(1)
  w_VPACQBEA = space(1)
  w_VPCRERIM = 0
  w_VPEURO19 = space(1)
  o_VPEURO19 = space(1)
  w_VPCREUTI = 0
  w_VPCODCAR = space(1)
  w_VPCODFIS = space(16)
  w_CONFER = space(10)
  w_VPDATVER = ctod('  /  /  ')
  w_CODBAN = space(15)
  w_VPCODAZI = space(5)
  w_VPCODCAB = space(5)
  w_DESBAN = space(35)
  w_COFAZI = space(16)
  w_PIVAZI = space(12)
  w_TIPLIQ = space(1)
  w_CONCES = space(3)
  w_VPDATPRE = ctod('  /  /  ')
  w_TITOLO = space(65)
  w_FLTEST = space(1)
  w_VPIMPOR6C = 0
  w_DESABI = space(80)
  w_DESFIL = space(40)
  w_INDIRI = space(50)
  w_CAP = space(5)
  w_VPSTAREG = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_simPag1","gscg_sim",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati IVA")
      .Pages(2).addobject("oPag","tgscg_simPag2","gscg_sim",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati presentazione")
      .Pages(3).addobject("oPag","tgscg_simPag3","gscg_sim",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Dati versamento")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oVPVARIMP_1_13
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='COC_MAST'
    this.cWorkTables[4]='COD_ABI'
    this.cWorkTables[5]='COD_CAB'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_sim
    *This.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VP__ANNO=space(4)
      .w_VPPERIOD=0
      .w_VPCODVAL=space(1)
      .w_COLSHOW=0
      .w_COLEDIT=0
      .w_MAGTRI=0
      .w_MINACC=0
      .w_MINVER=0
      .w_F2DEFI=space(1)
      .w_IMPVAL=space(3)
      .w_VPVARIMP=space(1)
      .w_VPCORTER=space(1)
      .w_RAGAZI=space(30)
      .w_TIPDIC=space(1)
      .w_VPDICGRU=space(1)
      .w_VPDICSOC=space(1)
      .w_FLTEST=space(1)
      .w_VISPDF=space(1)
      .w_VPIMPOR1=0
      .w_VPCESINT=0
      .w_VPIMPOR2=0
      .w_VPACQINT=0
      .w_VPIMPON3=0
      .w_VPIMPOS3=0
      .w_VPIMPOR5=0
      .w_VPIMPOR6=0
      .w_VPIMPOR7=0
      .w_IMPDEB7=0
      .w_IMPCRE7=0
      .w_IMPDEB8=0
      .w_IMPCRE8=0
      .w_VPIMPOR8=0
      .w_IMPDEB9=0
      .w_IMPCRE9=0
      .w_VPIMPOR9=0
      .w_IMPDEB10=0
      .w_IMPCRE10=0
      .w_VPIMPO10=0
      .w_TOTALE=0
      .w_TOTDEB=0
      .w_TOTCRE=0
      .w_VPIMPO11=0
      .w_VPIMPO12=0
      .w_IMPDEB12=0
      .w_IMPCRE12=0
      .w_VPIMPO13=0
      .w_VPIMPO14=0
      .w_VPIMPO15=0
      .w_VPIMPO16=0
      .w_VPIMPVER=0
      .w_VPVEREUR=space(1)
      .w_CRERES=0
      .w_VPVERNEF=space(1)
      .w_VPAR74C5=space(1)
      .w_VPAR74C4=space(1)
      .w_VPOPMEDI=space(1)
      .w_VPOPNOIM=space(1)
      .w_VPACQBEA=space(1)
      .w_VPCRERIM=0
      .w_VPEURO19=space(1)
      .w_VPCREUTI=0
      .w_VPCODCAR=space(1)
      .w_VPCODFIS=space(16)
      .w_CONFER=space(10)
      .w_VPDATVER=ctod("  /  /  ")
      .w_CODBAN=space(15)
      .w_VPCODAZI=space(5)
      .w_VPCODCAB=space(5)
      .w_DESBAN=space(35)
      .w_COFAZI=space(16)
      .w_PIVAZI=space(12)
      .w_TIPLIQ=space(1)
      .w_CONCES=space(3)
      .w_VPDATPRE=ctod("  /  /  ")
      .w_TITOLO=space(65)
      .w_FLTEST=space(1)
      .w_VPIMPOR6C=0
      .w_DESABI=space(80)
      .w_DESFIL=space(40)
      .w_INDIRI=space(50)
      .w_CAP=space(5)
      .w_VPSTAREG=space(1)
      .w_VP__ANNO=oParentObject.w_VP__ANNO
      .w_VPPERIOD=oParentObject.w_VPPERIOD
      .w_VPCODVAL=oParentObject.w_VPCODVAL
      .w_MAGTRI=oParentObject.w_MAGTRI
      .w_MINACC=oParentObject.w_MINACC
      .w_MINVER=oParentObject.w_MINVER
      .w_F2DEFI=oParentObject.w_F2DEFI
      .w_VPVARIMP=oParentObject.w_VPVARIMP
      .w_VPCORTER=oParentObject.w_VPCORTER
      .w_TIPDIC=oParentObject.w_TIPDIC
      .w_VPDICGRU=oParentObject.w_VPDICGRU
      .w_VPDICSOC=oParentObject.w_VPDICSOC
      .w_FLTEST=oParentObject.w_FLTEST
      .w_VPIMPOR1=oParentObject.w_VPIMPOR1
      .w_VPCESINT=oParentObject.w_VPCESINT
      .w_VPIMPOR2=oParentObject.w_VPIMPOR2
      .w_VPACQINT=oParentObject.w_VPACQINT
      .w_VPIMPON3=oParentObject.w_VPIMPON3
      .w_VPIMPOS3=oParentObject.w_VPIMPOS3
      .w_VPIMPOR5=oParentObject.w_VPIMPOR5
      .w_VPIMPOR6=oParentObject.w_VPIMPOR6
      .w_VPIMPOR7=oParentObject.w_VPIMPOR7
      .w_VPIMPOR8=oParentObject.w_VPIMPOR8
      .w_VPIMPOR9=oParentObject.w_VPIMPOR9
      .w_VPIMPO10=oParentObject.w_VPIMPO10
      .w_VPIMPO11=oParentObject.w_VPIMPO11
      .w_VPIMPO12=oParentObject.w_VPIMPO12
      .w_VPIMPO13=oParentObject.w_VPIMPO13
      .w_VPIMPO14=oParentObject.w_VPIMPO14
      .w_VPIMPO15=oParentObject.w_VPIMPO15
      .w_VPIMPO16=oParentObject.w_VPIMPO16
      .w_VPIMPVER=oParentObject.w_VPIMPVER
      .w_VPVEREUR=oParentObject.w_VPVEREUR
      .w_CRERES=oParentObject.w_CRERES
      .w_VPVERNEF=oParentObject.w_VPVERNEF
      .w_VPAR74C5=oParentObject.w_VPAR74C5
      .w_VPAR74C4=oParentObject.w_VPAR74C4
      .w_VPOPMEDI=oParentObject.w_VPOPMEDI
      .w_VPOPNOIM=oParentObject.w_VPOPNOIM
      .w_VPACQBEA=oParentObject.w_VPACQBEA
      .w_VPCRERIM=oParentObject.w_VPCRERIM
      .w_VPEURO19=oParentObject.w_VPEURO19
      .w_VPCREUTI=oParentObject.w_VPCREUTI
      .w_VPCODCAR=oParentObject.w_VPCODCAR
      .w_VPCODFIS=oParentObject.w_VPCODFIS
      .w_CONFER=oParentObject.w_CONFER
      .w_VPDATVER=oParentObject.w_VPDATVER
      .w_VPCODAZI=oParentObject.w_VPCODAZI
      .w_VPCODCAB=oParentObject.w_VPCODCAB
      .w_COFAZI=oParentObject.w_COFAZI
      .w_PIVAZI=oParentObject.w_PIVAZI
      .w_TIPLIQ=oParentObject.w_TIPLIQ
      .w_CONCES=oParentObject.w_CONCES
      .w_VPIMPOR6C=oParentObject.w_VPIMPOR6C
      .w_VPSTAREG=oParentObject.w_VPSTAREG
          .DoRTCalc(1,3,.f.)
        .w_COLSHOW = 0
        .w_COLEDIT = RGB(27,80,180)
          .DoRTCalc(6,9,.f.)
        .w_IMPVAL = IIF(.w_VPCODVAL='S', g_CODEUR, g_CODLIR)
          .DoRTCalc(11,12,.f.)
        .w_RAGAZI = g_RAGAZI
          .DoRTCalc(14,16,.f.)
        .w_FLTEST = ' '
          .DoRTCalc(18,27,.f.)
        .w_IMPDEB7 = IIF( .w_VPIMPOR7>0 ,  .w_VPIMPOR7, 0)
        .w_IMPCRE7 = IIF( .w_VPIMPOR7<0 ,  ABS(.w_VPIMPOR7) ,0 )
          .DoRTCalc(30,31,.f.)
        .w_VPIMPOR8 = .w_IMPDEB8-.w_IMPCRE8
          .DoRTCalc(33,34,.f.)
        .w_VPIMPOR9 = .w_IMPDEB9-.w_IMPCRE9
        .w_IMPDEB10 = IIF( .w_VPIMPO10>0,  .w_VPIMPO10 ,0)
        .w_IMPCRE10 = IIF( .w_VPIMPO10<0 ,  ABS(.w_VPIMPO10) ,0)
        .w_VPIMPO10 = .w_IMPDEB10-.w_IMPCRE10
        .w_TOTALE = .w_VPIMPOR7+.w_VPIMPOR8+.w_VPIMPOR9+.w_VPIMPO10
        .w_TOTDEB = .w_TOTALE
        .w_TOTCRE = .w_TOTALE
          .DoRTCalc(42,42,.f.)
        .w_VPIMPO12 = (.w_VPIMPOR7+.w_VPIMPOR8+.w_VPIMPOR9+.w_VPIMPO10)-.w_VPIMPO11
        .w_IMPDEB12 = IIF( .w_VPIMPO12>0,  .w_VPIMPO12 ,0)
        .w_IMPCRE12 = IIF( .w_VPIMPO12<0 ,  ABS(.w_VPIMPO12) ,0)
          .DoRTCalc(46,48,.f.)
        .w_VPIMPO16 = IIF((g_TIPDEN<>'M' AND .w_VPPERIOD=4) OR ((.w_VPIMPO12 + .w_VPIMPO14) - (.w_VPIMPO13 + .w_VPIMPO15))<0, 0, (.w_VPIMPO12 + .w_VPIMPO14) - (.w_VPIMPO13 + .w_VPIMPO15))
      .oPgFrm.Page2.oPag.oObj_2_70.Calculate('VP1 - Op.Attive',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page2.oPag.oObj_2_71.Calculate('VP2 - Op.Passive',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page2.oPag.oObj_2_72.Calculate('di cui Cessioni Intra',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page2.oPag.oObj_2_73.Calculate('di cui Acquisti Intra',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page2.oPag.oObj_2_74.Calculate('VP10 - Iva Esigibile per il Periodo',IIF(.w_VPDICGRU='S',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page2.oPag.oObj_2_75.Calculate('VP11 - Iva che si detrae per il Periodo',IIF(.w_VPDICGRU='S',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page2.oPag.oObj_2_76.Calculate('VP13 - Variazioni di Imposta',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page2.oPag.oObj_2_77.Calculate('VP16 - Credito Iva Compensabile',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page2.oPag.oObj_2_78.Calculate('VP22 - Versamento',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO16<>0 AND NOT (g_TIPDEN='T' AND .w_VPPERIOD=4), .w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page3.oPag.oObj_3_20.Calculate('VP31 - Credito chiesto a rimborso:',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND ((G_TIPDEN='M' AND (.w_VPPERIOD=3 or .w_VPPERIOD=6  or .w_VPPERIOD=9)) OR (G_TIPDEN='T' AND .w_VPPERIOD<4)),.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page3.oPag.oObj_3_21.Calculate('VP32 - Credito da utilizzare in compensazione mod. F24',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND ((G_TIPDEN='M' AND (.w_VPPERIOD=3 or .w_VPPERIOD=6  or .w_VPPERIOD=9)) OR (G_TIPDEN='T' AND .w_VPPERIOD<4)),.w_COLEDIT,.w_COLSHOW))
          .DoRTCalc(50,63,.f.)
        .w_CONFER = 'S'
        .DoRTCalc(65,66,.f.)
        if not(empty(.w_CODBAN))
          .link_3_24('Full')
        endif
        .DoRTCalc(67,67,.f.)
        if not(empty(.w_VPCODAZI))
          .link_3_25('Full')
        endif
        .DoRTCalc(68,68,.f.)
        if not(empty(.w_VPCODCAB))
          .link_3_26('Full')
        endif
          .DoRTCalc(69,73,.f.)
        .w_VPDATPRE = cp_CharToDate('  -  -  ')
      .oPgFrm.Page2.oPag.oObj_2_80.Calculate('VP3 - Imponibile',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
      .oPgFrm.Page2.oPag.oObj_2_81.Calculate('Imposta',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .w_TITOLO = 'Dichiarazione Iva '+IIF(.w_F2DEFI = 'S' ,'Definitiva','In Prova')+' del '+.w_VP__ANNO+iif(g_TIPDEN='T',' Trimestre ',' Mese di ')+CALCPER(.w_VPPERIOD, g_TIPDEN)
      .oPgFrm.Page2.oPag.oObj_2_84.Calculate('VP14 - Iva non versata/in eccesso',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
        .w_FLTEST = ' '
    endwith
    this.DoRTCalc(77,82,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_32.enabled = this.oPgFrm.Page1.oPag.oBtn_1_32.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_VP__ANNO=.w_VP__ANNO
      .oParentObject.w_VPPERIOD=.w_VPPERIOD
      .oParentObject.w_VPCODVAL=.w_VPCODVAL
      .oParentObject.w_MAGTRI=.w_MAGTRI
      .oParentObject.w_MINACC=.w_MINACC
      .oParentObject.w_MINVER=.w_MINVER
      .oParentObject.w_F2DEFI=.w_F2DEFI
      .oParentObject.w_VPVARIMP=.w_VPVARIMP
      .oParentObject.w_VPCORTER=.w_VPCORTER
      .oParentObject.w_TIPDIC=.w_TIPDIC
      .oParentObject.w_VPDICGRU=.w_VPDICGRU
      .oParentObject.w_VPDICSOC=.w_VPDICSOC
      .oParentObject.w_FLTEST=.w_FLTEST
      .oParentObject.w_VPIMPOR1=.w_VPIMPOR1
      .oParentObject.w_VPCESINT=.w_VPCESINT
      .oParentObject.w_VPIMPOR2=.w_VPIMPOR2
      .oParentObject.w_VPACQINT=.w_VPACQINT
      .oParentObject.w_VPIMPON3=.w_VPIMPON3
      .oParentObject.w_VPIMPOS3=.w_VPIMPOS3
      .oParentObject.w_VPIMPOR5=.w_VPIMPOR5
      .oParentObject.w_VPIMPOR6=.w_VPIMPOR6
      .oParentObject.w_VPIMPOR7=.w_VPIMPOR7
      .oParentObject.w_VPIMPOR8=.w_VPIMPOR8
      .oParentObject.w_VPIMPOR9=.w_VPIMPOR9
      .oParentObject.w_VPIMPO10=.w_VPIMPO10
      .oParentObject.w_VPIMPO11=.w_VPIMPO11
      .oParentObject.w_VPIMPO12=.w_VPIMPO12
      .oParentObject.w_VPIMPO13=.w_VPIMPO13
      .oParentObject.w_VPIMPO14=.w_VPIMPO14
      .oParentObject.w_VPIMPO15=.w_VPIMPO15
      .oParentObject.w_VPIMPO16=.w_VPIMPO16
      .oParentObject.w_VPIMPVER=.w_VPIMPVER
      .oParentObject.w_VPVEREUR=.w_VPVEREUR
      .oParentObject.w_CRERES=.w_CRERES
      .oParentObject.w_VPVERNEF=.w_VPVERNEF
      .oParentObject.w_VPAR74C5=.w_VPAR74C5
      .oParentObject.w_VPAR74C4=.w_VPAR74C4
      .oParentObject.w_VPOPMEDI=.w_VPOPMEDI
      .oParentObject.w_VPOPNOIM=.w_VPOPNOIM
      .oParentObject.w_VPACQBEA=.w_VPACQBEA
      .oParentObject.w_VPCRERIM=.w_VPCRERIM
      .oParentObject.w_VPEURO19=.w_VPEURO19
      .oParentObject.w_VPCREUTI=.w_VPCREUTI
      .oParentObject.w_VPCODCAR=.w_VPCODCAR
      .oParentObject.w_VPCODFIS=.w_VPCODFIS
      .oParentObject.w_CONFER=.w_CONFER
      .oParentObject.w_VPDATVER=.w_VPDATVER
      .oParentObject.w_VPCODAZI=.w_VPCODAZI
      .oParentObject.w_VPCODCAB=.w_VPCODCAB
      .oParentObject.w_COFAZI=.w_COFAZI
      .oParentObject.w_PIVAZI=.w_PIVAZI
      .oParentObject.w_TIPLIQ=.w_TIPLIQ
      .oParentObject.w_CONCES=.w_CONCES
      .oParentObject.w_VPIMPOR6C=.w_VPIMPOR6C
      .oParentObject.w_VPSTAREG=.w_VPSTAREG
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,27,.t.)
            .w_IMPDEB7 = IIF( .w_VPIMPOR7>0 ,  .w_VPIMPOR7, 0)
            .w_IMPCRE7 = IIF( .w_VPIMPOR7<0 ,  ABS(.w_VPIMPOR7) ,0 )
        .DoRTCalc(30,31,.t.)
            .w_VPIMPOR8 = .w_IMPDEB8-.w_IMPCRE8
        .DoRTCalc(33,34,.t.)
            .w_VPIMPOR9 = .w_IMPDEB9-.w_IMPCRE9
        .DoRTCalc(36,37,.t.)
            .w_VPIMPO10 = .w_IMPDEB10-.w_IMPCRE10
            .w_TOTALE = .w_VPIMPOR7+.w_VPIMPOR8+.w_VPIMPOR9+.w_VPIMPO10
            .w_TOTDEB = .w_TOTALE
            .w_TOTCRE = .w_TOTALE
        .DoRTCalc(42,42,.t.)
            .w_VPIMPO12 = (.w_VPIMPOR7+.w_VPIMPOR8+.w_VPIMPOR9+.w_VPIMPO10)-.w_VPIMPO11
            .w_IMPDEB12 = IIF( .w_VPIMPO12>0,  .w_VPIMPO12 ,0)
            .w_IMPCRE12 = IIF( .w_VPIMPO12<0 ,  ABS(.w_VPIMPO12) ,0)
        .DoRTCalc(46,46,.t.)
        if .o_VPIMPO11<>.w_VPIMPO11.or. .o_VPIMPO12<>.w_VPIMPO12.or. .o_VPIMPO13<>.w_VPIMPO13
            .w_VPIMPO14 = IIF(g_TIPDEN<>'M' AND .w_VPPERIOD<>4 AND (.w_VPIMPO12 - .w_VPIMPO13)>0, cp_ROUND(((.w_VPIMPO12 - .w_VPIMPO13) * .w_MAGTRI) / 100, 0), 0)
        endif
        .DoRTCalc(48,48,.t.)
            .w_VPIMPO16 = IIF((g_TIPDEN<>'M' AND .w_VPPERIOD=4) OR ((.w_VPIMPO12 + .w_VPIMPO14) - (.w_VPIMPO13 + .w_VPIMPO15))<0, 0, (.w_VPIMPO12 + .w_VPIMPO14) - (.w_VPIMPO13 + .w_VPIMPO15))
        if .o_VPIMPO16<>.w_VPIMPO16
            .w_VPIMPVER = IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO16>.w_MINVER,.w_VPIMPO16,0)
        endif
        .oPgFrm.Page2.oPag.oObj_2_70.Calculate('VP1 - Op.Attive',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_71.Calculate('VP2 - Op.Passive',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_72.Calculate('di cui Cessioni Intra',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_73.Calculate('di cui Acquisti Intra',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_74.Calculate('VP10 - Iva Esigibile per il Periodo',IIF(.w_VPDICGRU='S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_75.Calculate('VP11 - Iva che si detrae per il Periodo',IIF(.w_VPDICGRU='S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_76.Calculate('VP13 - Variazioni di Imposta',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_77.Calculate('VP16 - Credito Iva Compensabile',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_78.Calculate('VP22 - Versamento',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO16<>0 AND NOT (g_TIPDEN='T' AND .w_VPPERIOD=4), .w_COLEDIT,.w_COLSHOW))
        .DoRTCalc(51,52,.t.)
        if .o_VPIMPVER<>.w_VPIMPVER
            .w_VPVERNEF = ' '
        endif
        .DoRTCalc(54,58,.t.)
        if .o_VPEURO19<>.w_VPEURO19
            .w_VPCRERIM = 0
        endif
        .DoRTCalc(60,60,.t.)
        if .o_VPEURO19<>.w_VPEURO19
            .w_VPCREUTI = 0
        endif
        .oPgFrm.Page3.oPag.oObj_3_20.Calculate('VP31 - Credito chiesto a rimborso:',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND ((G_TIPDEN='M' AND (.w_VPPERIOD=3 or .w_VPPERIOD=6  or .w_VPPERIOD=9)) OR (G_TIPDEN='T' AND .w_VPPERIOD<4)),.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page3.oPag.oObj_3_21.Calculate('VP32 - Credito da utilizzare in compensazione mod. F24',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND ((G_TIPDEN='M' AND (.w_VPPERIOD=3 or .w_VPPERIOD=6  or .w_VPPERIOD=9)) OR (G_TIPDEN='T' AND .w_VPPERIOD<4)),.w_COLEDIT,.w_COLSHOW))
        .DoRTCalc(62,66,.t.)
          .link_3_25('Full')
          .link_3_26('Full')
        .oPgFrm.Page2.oPag.oObj_2_80.Calculate('VP3 - Imponibile',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_81.Calculate('Imposta',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .DoRTCalc(69,74,.t.)
            .w_TITOLO = 'Dichiarazione Iva '+IIF(.w_F2DEFI = 'S' ,'Definitiva','In Prova')+' del '+.w_VP__ANNO+iif(g_TIPDEN='T',' Trimestre ',' Mese di ')+CALCPER(.w_VPPERIOD, g_TIPDEN)
        .oPgFrm.Page2.oPag.oObj_2_84.Calculate('VP14 - Iva non versata/in eccesso',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(76,82,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.oObj_2_70.Calculate('VP1 - Op.Attive',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_71.Calculate('VP2 - Op.Passive',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_72.Calculate('di cui Cessioni Intra',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_73.Calculate('di cui Acquisti Intra',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_74.Calculate('VP10 - Iva Esigibile per il Periodo',IIF(.w_VPDICGRU='S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_75.Calculate('VP11 - Iva che si detrae per il Periodo',IIF(.w_VPDICGRU='S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_76.Calculate('VP13 - Variazioni di Imposta',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_77.Calculate('VP16 - Credito Iva Compensabile',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_78.Calculate('VP22 - Versamento',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO16<>0 AND NOT (g_TIPDEN='T' AND .w_VPPERIOD=4), .w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page3.oPag.oObj_3_20.Calculate('VP31 - Credito chiesto a rimborso:',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND ((G_TIPDEN='M' AND (.w_VPPERIOD=3 or .w_VPPERIOD=6  or .w_VPPERIOD=9)) OR (G_TIPDEN='T' AND .w_VPPERIOD<4)),.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page3.oPag.oObj_3_21.Calculate('VP32 - Credito da utilizzare in compensazione mod. F24',IIF(.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND ((G_TIPDEN='M' AND (.w_VPPERIOD=3 or .w_VPPERIOD=6  or .w_VPPERIOD=9)) OR (G_TIPDEN='T' AND .w_VPPERIOD<4)),.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_80.Calculate('VP3 - Imponibile',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_81.Calculate('Imposta',IIF(.w_VPVARIMP='S' And .w_VPDICGRU<>'S',.w_COLEDIT,.w_COLSHOW))
        .oPgFrm.Page2.oPag.oObj_2_84.Calculate('VP14 - Iva non versata/in eccesso',IIF(1=1,.w_COLEDIT,.w_COLSHOW))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oVPVARIMP_1_13.enabled = this.oPgFrm.Page1.oPag.oVPVARIMP_1_13.mCond()
    this.oPgFrm.Page2.oPag.oVPIMPOR1_2_1.enabled = this.oPgFrm.Page2.oPag.oVPIMPOR1_2_1.mCond()
    this.oPgFrm.Page2.oPag.oVPCESINT_2_2.enabled = this.oPgFrm.Page2.oPag.oVPCESINT_2_2.mCond()
    this.oPgFrm.Page2.oPag.oVPIMPOR2_2_3.enabled = this.oPgFrm.Page2.oPag.oVPIMPOR2_2_3.mCond()
    this.oPgFrm.Page2.oPag.oVPACQINT_2_4.enabled = this.oPgFrm.Page2.oPag.oVPACQINT_2_4.mCond()
    this.oPgFrm.Page2.oPag.oVPIMPON3_2_5.enabled = this.oPgFrm.Page2.oPag.oVPIMPON3_2_5.mCond()
    this.oPgFrm.Page2.oPag.oVPIMPOS3_2_6.enabled = this.oPgFrm.Page2.oPag.oVPIMPOS3_2_6.mCond()
    this.oPgFrm.Page2.oPag.oVPIMPOR5_2_7.enabled = this.oPgFrm.Page2.oPag.oVPIMPOR5_2_7.mCond()
    this.oPgFrm.Page2.oPag.oVPIMPOR6_2_8.enabled = this.oPgFrm.Page2.oPag.oVPIMPOR6_2_8.mCond()
    this.oPgFrm.Page2.oPag.oIMPDEB8_2_15.enabled = this.oPgFrm.Page2.oPag.oIMPDEB8_2_15.mCond()
    this.oPgFrm.Page2.oPag.oIMPCRE8_2_16.enabled = this.oPgFrm.Page2.oPag.oIMPCRE8_2_16.mCond()
    this.oPgFrm.Page2.oPag.oIMPDEB9_2_18.enabled = this.oPgFrm.Page2.oPag.oIMPDEB9_2_18.mCond()
    this.oPgFrm.Page2.oPag.oIMPCRE9_2_19.enabled = this.oPgFrm.Page2.oPag.oIMPCRE9_2_19.mCond()
    this.oPgFrm.Page2.oPag.oIMPDEB10_2_21.enabled = this.oPgFrm.Page2.oPag.oIMPDEB10_2_21.mCond()
    this.oPgFrm.Page2.oPag.oIMPCRE10_2_22.enabled = this.oPgFrm.Page2.oPag.oIMPCRE10_2_22.mCond()
    this.oPgFrm.Page2.oPag.oVPIMPO13_2_35.enabled = this.oPgFrm.Page2.oPag.oVPIMPO13_2_35.mCond()
    this.oPgFrm.Page2.oPag.oVPIMPO14_2_37.enabled = this.oPgFrm.Page2.oPag.oVPIMPO14_2_37.mCond()
    this.oPgFrm.Page2.oPag.oVPIMPVER_2_41.enabled = this.oPgFrm.Page2.oPag.oVPIMPVER_2_41.mCond()
    this.oPgFrm.Page3.oPag.oVPVERNEF_3_1.enabled = this.oPgFrm.Page3.oPag.oVPVERNEF_3_1.mCond()
    this.oPgFrm.Page3.oPag.oVPAR74C5_3_3.enabled = this.oPgFrm.Page3.oPag.oVPAR74C5_3_3.mCond()
    this.oPgFrm.Page3.oPag.oVPAR74C4_3_4.enabled = this.oPgFrm.Page3.oPag.oVPAR74C4_3_4.mCond()
    this.oPgFrm.Page3.oPag.oVPCRERIM_3_10.enabled = this.oPgFrm.Page3.oPag.oVPCRERIM_3_10.mCond()
    this.oPgFrm.Page3.oPag.oVPCREUTI_3_13.enabled = this.oPgFrm.Page3.oPag.oVPCREUTI_3_13.mCond()
    this.oPgFrm.Page3.oPag.oVPCODCAR_3_14.enabled = this.oPgFrm.Page3.oPag.oVPCODCAR_3_14.mCond()
    this.oPgFrm.Page3.oPag.oVPCODFIS_3_16.enabled = this.oPgFrm.Page3.oPag.oVPCODFIS_3_16.mCond()
    this.oPgFrm.Page3.oPag.oVPDATVER_3_23.enabled = this.oPgFrm.Page3.oPag.oVPDATVER_3_23.mCond()
    this.oPgFrm.Page3.oPag.oCODBAN_3_24.enabled = this.oPgFrm.Page3.oPag.oCODBAN_3_24.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oVISPDF_1_23.visible=!this.oPgFrm.Page1.oPag.oVISPDF_1_23.mHide()
    this.oPgFrm.Page2.oPag.oIMPDEB7_2_12.visible=!this.oPgFrm.Page2.oPag.oIMPDEB7_2_12.mHide()
    this.oPgFrm.Page2.oPag.oIMPCRE7_2_14.visible=!this.oPgFrm.Page2.oPag.oIMPCRE7_2_14.mHide()
    this.oPgFrm.Page2.oPag.oIMPDEB12_2_32.visible=!this.oPgFrm.Page2.oPag.oIMPDEB12_2_32.mHide()
    this.oPgFrm.Page2.oPag.oIMPCRE12_2_33.visible=!this.oPgFrm.Page2.oPag.oIMPCRE12_2_33.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_43.visible=!this.oPgFrm.Page2.oPag.oStr_2_43.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_44.visible=!this.oPgFrm.Page2.oPag.oStr_2_44.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_45.visible=!this.oPgFrm.Page2.oPag.oStr_2_45.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_46.visible=!this.oPgFrm.Page2.oPag.oStr_2_46.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_47.visible=!this.oPgFrm.Page2.oPag.oStr_2_47.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_48.visible=!this.oPgFrm.Page2.oPag.oStr_2_48.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_49.visible=!this.oPgFrm.Page2.oPag.oStr_2_49.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_50.visible=!this.oPgFrm.Page2.oPag.oStr_2_50.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_51.visible=!this.oPgFrm.Page2.oPag.oStr_2_51.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_52.visible=!this.oPgFrm.Page2.oPag.oStr_2_52.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_53.visible=!this.oPgFrm.Page2.oPag.oStr_2_53.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_54.visible=!this.oPgFrm.Page2.oPag.oStr_2_54.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_55.visible=!this.oPgFrm.Page2.oPag.oStr_2_55.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_56.visible=!this.oPgFrm.Page2.oPag.oStr_2_56.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_57.visible=!this.oPgFrm.Page2.oPag.oStr_2_57.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_58.visible=!this.oPgFrm.Page2.oPag.oStr_2_58.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_59.visible=!this.oPgFrm.Page2.oPag.oStr_2_59.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_60.visible=!this.oPgFrm.Page2.oPag.oStr_2_60.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_61.visible=!this.oPgFrm.Page2.oPag.oStr_2_61.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_62.visible=!this.oPgFrm.Page2.oPag.oStr_2_62.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_63.visible=!this.oPgFrm.Page2.oPag.oStr_2_63.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_64.visible=!this.oPgFrm.Page2.oPag.oStr_2_64.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_69.visible=!this.oPgFrm.Page2.oPag.oStr_2_69.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_11.visible=!this.oPgFrm.Page3.oPag.oStr_3_11.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_22.visible=!this.oPgFrm.Page3.oPag.oStr_3_22.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_82.visible=!this.oPgFrm.Page2.oPag.oStr_2_82.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_83.visible=!this.oPgFrm.Page2.oPag.oStr_2_83.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.oObj_2_70.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_71.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_72.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_73.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_74.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_75.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_76.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_77.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_78.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_20.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_21.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_80.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_81.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_84.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODBAN
  func Link_3_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CODBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CODBAN))
          select BACODBAN,BADESCRI,BACODABI,BACODCAB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_CODBAN)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_CODBAN)+"%");

            select BACODBAN,BADESCRI,BACODABI,BACODCAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODBAN) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCODBAN_3_24'),i_cWhere,'',"Conti correnti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BACODABI,BACODCAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CODBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CODBAN)
            select BACODBAN,BADESCRI,BACODABI,BACODCAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBAN = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBAN = NVL(_Link_.BADESCRI,space(35))
      this.w_VPCODAZI = NVL(_Link_.BACODABI,space(5))
      this.w_VPCODCAB = NVL(_Link_.BACODCAB,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODBAN = space(15)
      endif
      this.w_DESBAN = space(35)
      this.w_VPCODAZI = space(5)
      this.w_VPCODCAB = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPCODAZI
  func Link_3_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_ABI_IDX,3]
    i_lTable = "COD_ABI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2], .t., this.COD_ABI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI,ABDESABI";
                   +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(this.w_VPCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',this.w_VPCODAZI)
            select ABCODABI,ABDESABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODAZI = NVL(_Link_.ABCODABI,space(5))
      this.w_DESABI = NVL(_Link_.ABDESABI,space(80))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODAZI = space(5)
      endif
      this.w_DESABI = space(80)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])+'\'+cp_ToStr(_Link_.ABCODABI,1)
      cp_ShowWarn(i_cKey,this.COD_ABI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPCODCAB
  func Link_3_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_CAB_IDX,3]
    i_lTable = "COD_CAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2], .t., this.COD_CAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPCODCAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPCODCAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB,FIDESFIL,FIINDIRI,FI___CAP";
                   +" from "+i_cTable+" "+i_lTable+" where FICODCAB="+cp_ToStrODBC(this.w_VPCODCAB);
                   +" and FICODABI="+cp_ToStrODBC(this.w_VPCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODABI',this.w_VPCODAZI;
                       ,'FICODCAB',this.w_VPCODCAB)
            select FICODABI,FICODCAB,FIDESFIL,FIINDIRI,FI___CAP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPCODCAB = NVL(_Link_.FICODCAB,space(5))
      this.w_DESFIL = NVL(_Link_.FIDESFIL,space(40))
      this.w_INDIRI = NVL(_Link_.FIINDIRI,space(50))
      this.w_CAP = NVL(_Link_.FI___CAP,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VPCODCAB = space(5)
      endif
      this.w_DESFIL = space(40)
      this.w_INDIRI = space(50)
      this.w_CAP = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])+'\'+cp_ToStr(_Link_.FICODABI,1)+'\'+cp_ToStr(_Link_.FICODCAB,1)
      cp_ShowWarn(i_cKey,this.COD_CAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPCODCAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIMPVAL_1_12.value==this.w_IMPVAL)
      this.oPgFrm.Page1.oPag.oIMPVAL_1_12.value=this.w_IMPVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oVPVARIMP_1_13.RadioValue()==this.w_VPVARIMP)
      this.oPgFrm.Page1.oPag.oVPVARIMP_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVPCORTER_1_14.RadioValue()==this.w_VPCORTER)
      this.oPgFrm.Page1.oPag.oVPCORTER_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGAZI_1_16.value==this.w_RAGAZI)
      this.oPgFrm.Page1.oPag.oRAGAZI_1_16.value=this.w_RAGAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDIC_1_17.RadioValue()==this.w_TIPDIC)
      this.oPgFrm.Page1.oPag.oTIPDIC_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVISPDF_1_23.RadioValue()==this.w_VISPDF)
      this.oPgFrm.Page1.oPag.oVISPDF_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPOR1_2_1.value==this.w_VPIMPOR1)
      this.oPgFrm.Page2.oPag.oVPIMPOR1_2_1.value=this.w_VPIMPOR1
    endif
    if not(this.oPgFrm.Page2.oPag.oVPCESINT_2_2.value==this.w_VPCESINT)
      this.oPgFrm.Page2.oPag.oVPCESINT_2_2.value=this.w_VPCESINT
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPOR2_2_3.value==this.w_VPIMPOR2)
      this.oPgFrm.Page2.oPag.oVPIMPOR2_2_3.value=this.w_VPIMPOR2
    endif
    if not(this.oPgFrm.Page2.oPag.oVPACQINT_2_4.value==this.w_VPACQINT)
      this.oPgFrm.Page2.oPag.oVPACQINT_2_4.value=this.w_VPACQINT
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPON3_2_5.value==this.w_VPIMPON3)
      this.oPgFrm.Page2.oPag.oVPIMPON3_2_5.value=this.w_VPIMPON3
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPOS3_2_6.value==this.w_VPIMPOS3)
      this.oPgFrm.Page2.oPag.oVPIMPOS3_2_6.value=this.w_VPIMPOS3
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPOR5_2_7.value==this.w_VPIMPOR5)
      this.oPgFrm.Page2.oPag.oVPIMPOR5_2_7.value=this.w_VPIMPOR5
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPOR6_2_8.value==this.w_VPIMPOR6)
      this.oPgFrm.Page2.oPag.oVPIMPOR6_2_8.value=this.w_VPIMPOR6
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPDEB7_2_12.value==this.w_IMPDEB7)
      this.oPgFrm.Page2.oPag.oIMPDEB7_2_12.value=this.w_IMPDEB7
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPCRE7_2_14.value==this.w_IMPCRE7)
      this.oPgFrm.Page2.oPag.oIMPCRE7_2_14.value=this.w_IMPCRE7
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPDEB8_2_15.value==this.w_IMPDEB8)
      this.oPgFrm.Page2.oPag.oIMPDEB8_2_15.value=this.w_IMPDEB8
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPCRE8_2_16.value==this.w_IMPCRE8)
      this.oPgFrm.Page2.oPag.oIMPCRE8_2_16.value=this.w_IMPCRE8
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPDEB9_2_18.value==this.w_IMPDEB9)
      this.oPgFrm.Page2.oPag.oIMPDEB9_2_18.value=this.w_IMPDEB9
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPCRE9_2_19.value==this.w_IMPCRE9)
      this.oPgFrm.Page2.oPag.oIMPCRE9_2_19.value=this.w_IMPCRE9
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPDEB10_2_21.value==this.w_IMPDEB10)
      this.oPgFrm.Page2.oPag.oIMPDEB10_2_21.value=this.w_IMPDEB10
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPCRE10_2_22.value==this.w_IMPCRE10)
      this.oPgFrm.Page2.oPag.oIMPCRE10_2_22.value=this.w_IMPCRE10
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPO11_2_29.value==this.w_VPIMPO11)
      this.oPgFrm.Page2.oPag.oVPIMPO11_2_29.value=this.w_VPIMPO11
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPDEB12_2_32.value==this.w_IMPDEB12)
      this.oPgFrm.Page2.oPag.oIMPDEB12_2_32.value=this.w_IMPDEB12
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPCRE12_2_33.value==this.w_IMPCRE12)
      this.oPgFrm.Page2.oPag.oIMPCRE12_2_33.value=this.w_IMPCRE12
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPO13_2_35.value==this.w_VPIMPO13)
      this.oPgFrm.Page2.oPag.oVPIMPO13_2_35.value=this.w_VPIMPO13
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPO14_2_37.value==this.w_VPIMPO14)
      this.oPgFrm.Page2.oPag.oVPIMPO14_2_37.value=this.w_VPIMPO14
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPO15_2_38.value==this.w_VPIMPO15)
      this.oPgFrm.Page2.oPag.oVPIMPO15_2_38.value=this.w_VPIMPO15
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPO16_2_39.value==this.w_VPIMPO16)
      this.oPgFrm.Page2.oPag.oVPIMPO16_2_39.value=this.w_VPIMPO16
    endif
    if not(this.oPgFrm.Page2.oPag.oVPIMPVER_2_41.value==this.w_VPIMPVER)
      this.oPgFrm.Page2.oPag.oVPIMPVER_2_41.value=this.w_VPIMPVER
    endif
    if not(this.oPgFrm.Page2.oPag.oVPVEREUR_2_42.RadioValue()==this.w_VPVEREUR)
      this.oPgFrm.Page2.oPag.oVPVEREUR_2_42.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCRERES_2_65.value==this.w_CRERES)
      this.oPgFrm.Page2.oPag.oCRERES_2_65.value=this.w_CRERES
    endif
    if not(this.oPgFrm.Page3.oPag.oVPVERNEF_3_1.RadioValue()==this.w_VPVERNEF)
      this.oPgFrm.Page3.oPag.oVPVERNEF_3_1.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVPAR74C5_3_3.RadioValue()==this.w_VPAR74C5)
      this.oPgFrm.Page3.oPag.oVPAR74C5_3_3.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVPAR74C4_3_4.RadioValue()==this.w_VPAR74C4)
      this.oPgFrm.Page3.oPag.oVPAR74C4_3_4.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVPOPMEDI_3_7.RadioValue()==this.w_VPOPMEDI)
      this.oPgFrm.Page3.oPag.oVPOPMEDI_3_7.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVPOPNOIM_3_8.RadioValue()==this.w_VPOPNOIM)
      this.oPgFrm.Page3.oPag.oVPOPNOIM_3_8.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVPACQBEA_3_9.RadioValue()==this.w_VPACQBEA)
      this.oPgFrm.Page3.oPag.oVPACQBEA_3_9.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVPCRERIM_3_10.value==this.w_VPCRERIM)
      this.oPgFrm.Page3.oPag.oVPCRERIM_3_10.value=this.w_VPCRERIM
    endif
    if not(this.oPgFrm.Page3.oPag.oVPEURO19_3_12.RadioValue()==this.w_VPEURO19)
      this.oPgFrm.Page3.oPag.oVPEURO19_3_12.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVPCREUTI_3_13.value==this.w_VPCREUTI)
      this.oPgFrm.Page3.oPag.oVPCREUTI_3_13.value=this.w_VPCREUTI
    endif
    if not(this.oPgFrm.Page3.oPag.oVPCODCAR_3_14.RadioValue()==this.w_VPCODCAR)
      this.oPgFrm.Page3.oPag.oVPCODCAR_3_14.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oVPCODFIS_3_16.value==this.w_VPCODFIS)
      this.oPgFrm.Page3.oPag.oVPCODFIS_3_16.value=this.w_VPCODFIS
    endif
    if not(this.oPgFrm.Page3.oPag.oVPDATVER_3_23.value==this.w_VPDATVER)
      this.oPgFrm.Page3.oPag.oVPDATVER_3_23.value=this.w_VPDATVER
    endif
    if not(this.oPgFrm.Page3.oPag.oCODBAN_3_24.value==this.w_CODBAN)
      this.oPgFrm.Page3.oPag.oCODBAN_3_24.value=this.w_CODBAN
    endif
    if not(this.oPgFrm.Page3.oPag.oVPCODAZI_3_25.value==this.w_VPCODAZI)
      this.oPgFrm.Page3.oPag.oVPCODAZI_3_25.value=this.w_VPCODAZI
    endif
    if not(this.oPgFrm.Page3.oPag.oVPCODCAB_3_26.value==this.w_VPCODCAB)
      this.oPgFrm.Page3.oPag.oVPCODCAB_3_26.value=this.w_VPCODCAB
    endif
    if not(this.oPgFrm.Page1.oPag.oTITOLO_1_39.value==this.w_TITOLO)
      this.oPgFrm.Page1.oPag.oTITOLO_1_39.value=this.w_TITOLO
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTEST_1_40.RadioValue()==this.w_FLTEST)
      this.oPgFrm.Page1.oPag.oFLTEST_1_40.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_IMPDEB8>=0)  and (.w_IMPCRE8=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oIMPDEB8_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IMPCRE8>=0)  and (.w_IMPDEB8=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oIMPCRE8_2_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IMPDEB9>=0)  and (.w_IMPCRE9=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oIMPDEB9_2_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IMPCRE9>=0)  and (.w_IMPDEB9=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oIMPCRE9_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IMPDEB10>=0)  and (.w_F2DEFI<>'S' AND .w_IMPCRE10=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oIMPDEB10_2_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_IMPCRE10>=0)  and (.w_F2DEFI<>'S' AND .w_IMPDEB10=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oIMPCRE10_2_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_VPIMPO11<=.w_CRERES And .w_VPIMPO11>=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVPIMPO11_2_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il credito deve essere minore del credito disponibile e maggiore di zero")
          case   not(.w_VPIMPO13>=0 And .w_VPIMPO13<=.w_IMPDEB12)  and (.w_IMPDEB12>0 And Not (g_TIPDEN='T' And .w_VPPERIOD=4) And .w_VPDICSOC<>'S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVPIMPO13_2_35.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il credito non pu� superare il debito per il periodo")
          case   not(.w_VPIMPO14=0 OR (.w_VPIMPO14>0 AND (.w_VPIMPO12 - .w_VPIMPO13)>0))  and (g_TIPDEN<>'M')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVPIMPO14_2_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Importo interessi per le liquidazioni trimestrali non congruente")
          case   not(.w_VPIMPVER>=0)  and (.w_VPDICSOC<>'S' AND .w_VPIMPO16<>0 AND NOT (g_TIPDEN='T' AND .w_VPPERIOD=4))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oVPIMPVER_2_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'importo versato non pu� essere negativo")
          case   not(.w_VPCRERIM+.w_VPCREUTI<=ABS(.w_VPIMPO12))  and (.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND (.w_VPOPMEDI='S' OR .w_VPOPNOIM='S' OR .w_VPACQBEA='S'))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oVPCRERIM_3_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La somma chiesta a rimborso non deve superare il credito del periodo")
          case   not(.w_VPCRERIM+.w_VPCREUTI<=ABS(.w_VPIMPO12))  and (.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND (.w_VPOPMEDI='S' OR .w_VPOPNOIM='S' OR .w_VPACQBEA='S'))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oVPCREUTI_3_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La somma chiesta a rimborso non deve superare il credito del periodo")
          case   (empty(.w_VPCODCAR))  and (.w_VPDICSOC<>'S')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oVPCODCAR_3_14.SetFocus()
            i_bnoObbl = !empty(.w_VPCODCAR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_VPCODFIS)) or not(CHKCFP(.w_VPCODFIS,'CF','',1,'', g_CODNAZ )))  and (.w_VPDICSOC<>'S')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oVPCODFIS_3_16.SetFocus()
            i_bnoObbl = !empty(.w_VPCODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_VPDATVER))  and (.w_VPDICSOC<>'S' AND .w_VPIMPVER>0)
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oVPDATVER_3_23.SetFocus()
            i_bnoObbl = !empty(.w_VPDATVER)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_VPIMPO11 = this.w_VPIMPO11
    this.o_VPIMPO12 = this.w_VPIMPO12
    this.o_VPIMPO13 = this.w_VPIMPO13
    this.o_VPIMPO16 = this.w_VPIMPO16
    this.o_VPIMPVER = this.w_VPIMPVER
    this.o_VPEURO19 = this.w_VPEURO19
    return

enddefine

* --- Define pages as container
define class tgscg_simPag1 as StdContainer
  Width  = 638
  height = 430
  stdWidth  = 638
  stdheight = 430
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

    add object oBmp_1_10 as image with uid="DQJRMUSOWA",left=20, top=21, width=15,height=15,;
      Picture="D:\IVA\EXE\BMP\IVAMODP1.BMP"

    add object oBmp_1_25 as image with uid="ZIXYMJEJCC",left=20, top=20, width=151,height=132,;
      Picture="BMP\IVAMODP1.BMP"

  add object oIMPVAL_1_12 as StdField with uid="EYEJIARBVQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_IMPVAL", cQueryName = "IMPVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta importi",;
    HelpContextID = 251581306,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=37, Left=561, Top=133, InputMask=replicate('X',3)

  add object oVPVARIMP_1_13 as StdCheck with uid="IRWVKWSNAW",rtseq=11,rtrep=.f.,left=18, top=189, caption="Nel periodo sono comprese variazioni di imponibile relative a periodi precedenti",;
    ToolTipText = "Variazione di imponibile periodi precedenti comprese nel periodo",;
    HelpContextID = 251433126,;
    cFormVar="w_VPVARIMP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVPVARIMP_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oVPVARIMP_1_13.GetRadio()
    this.Parent.oContained.w_VPVARIMP = this.RadioValue()
    return .t.
  endfunc

  func oVPVARIMP_1_13.SetRadio()
    this.Parent.oContained.w_VPVARIMP=trim(this.Parent.oContained.w_VPVARIMP)
    this.value = ;
      iif(this.Parent.oContained.w_VPVARIMP=='S',1,;
      0)
  endfunc

  func oVPVARIMP_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICGRU<>'S')
    endwith
   endif
  endfunc

  add object oVPCORTER_1_14 as StdCheck with uid="WGLLEOVLFL",rtseq=12,rtrep=.f.,left=18, top=210, caption="Correttiva nei termini",;
    ToolTipText = "Se attivo: dichiarazione correttiva nei termini",;
    HelpContextID = 100048728,;
    cFormVar="w_VPCORTER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVPCORTER_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVPCORTER_1_14.GetRadio()
    this.Parent.oContained.w_VPCORTER = this.RadioValue()
    return .t.
  endfunc

  func oVPCORTER_1_14.SetRadio()
    this.Parent.oContained.w_VPCORTER=trim(this.Parent.oContained.w_VPCORTER)
    this.value = ;
      iif(this.Parent.oContained.w_VPCORTER=='S',1,;
      0)
  endfunc

  add object oRAGAZI_1_16 as StdField with uid="MZRWMPRHFH",rtseq=13,rtrep=.f.,;
    cFormVar = "w_RAGAZI", cQueryName = "RAGAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale",;
    HelpContextID = 8679146,;
   bGlobalFont=.t.,;
    Height=21, Width=404, Left=194, Top=73, InputMask=replicate('X',30)


  add object oTIPDIC_1_17 as StdCombo with uid="VROCLXNEXK",rtseq=14,rtrep=.f.,left=194,top=133,width=245,height=21, enabled=.f.;
    , HelpContextID = 126932682;
    , cFormVar="w_TIPDIC",RowSource=""+"Dichiarazione di societ� controllata dal gruppo,"+"Normale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPDIC_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oTIPDIC_1_17.GetRadio()
    this.Parent.oContained.w_TIPDIC = this.RadioValue()
    return .t.
  endfunc

  func oTIPDIC_1_17.SetRadio()
    this.Parent.oContained.w_TIPDIC=trim(this.Parent.oContained.w_TIPDIC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDIC=='S',1,;
      iif(this.Parent.oContained.w_TIPDIC=='N',2,;
      0))
  endfunc

  add object oVISPDF_1_23 as StdCheck with uid="XTCQFDSZVP",rtseq=18,rtrep=.f.,left=295, top=378, caption="Visualizza modulo PDF",;
    ToolTipText = "Se attivo: carica il modulo della dichiarazione in Acrobat Reader",;
    HelpContextID = 81045162,;
    cFormVar="w_VISPDF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVISPDF_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVISPDF_1_23.GetRadio()
    this.Parent.oContained.w_VISPDF = this.RadioValue()
    return .t.
  endfunc

  func oVISPDF_1_23.SetRadio()
    this.Parent.oContained.w_VISPDF=trim(this.Parent.oContained.w_VISPDF)
    this.value = ;
      iif(this.Parent.oContained.w_VISPDF=='S',1,;
      0)
  endfunc

  func oVISPDF_1_23.mHide()
    with this.Parent.oContained
      return (.w_FLTEST='S')
    endwith
  endfunc


  add object oBtn_1_24 as StdButton with uid="OBZRZEORUH",left=535, top=380, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare i risultati";
    , HelpContextID = 9849574;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_32 as StdButton with uid="LQHYDIVSGS",left=394, top=265, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Stampa liquidazione IVA";
    , HelpContextID = 116825050;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_32.Click()
      with this.Parent.oContained
        GSCG_BDV(this.Parent.oContained,"L")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_33 as StdButton with uid="JAEBERYJHB",left=394, top=319, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Stampa dichiarazione IVA periodica";
    , HelpContextID = 116825050;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        GSCG_BDV(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_36 as StdButton with uid="FWTZHDZJVZ",left=584, top=380, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare l'elaborazione";
    , HelpContextID = 17138246;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oTITOLO_1_39 as StdField with uid="XDPXPBCAQQ",rtseq=75,rtrep=.f.,;
    cFormVar = "w_TITOLO", cQueryName = "TITOLO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(65), bMultilanguage =  .f.,;
    HelpContextID = 190158538,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=402, Left=194, Top=19, InputMask=replicate('X',65)

  add object oFLTEST_1_40 as StdCheck with uid="DXYVVIOBTQ",rtseq=76,rtrep=.f.,left=480, top=294, caption="Stampa solo testo",;
    ToolTipText = "Se attivo: stampa su modulo solo testo",;
    HelpContextID = 99587242,;
    cFormVar="w_FLTEST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLTEST_1_40.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLTEST_1_40.GetRadio()
    this.Parent.oContained.w_FLTEST = this.RadioValue()
    return .t.
  endfunc

  func oFLTEST_1_40.SetRadio()
    this.Parent.oContained.w_FLTEST=trim(this.Parent.oContained.w_FLTEST)
    this.value = ;
      iif(this.Parent.oContained.w_FLTEST=='S',1,;
      0)
  endfunc

  add object oStr_1_11 as StdString with uid="UFQIFDPDWE",Visible=.t., Left=194, Top=110,;
    Alignment=0, Width=399, Height=15,;
    Caption="Dati per liquidazione IVA di gruppo (art. 73)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="JEBBYBRLBD",Visible=.t., Left=194, Top=58,;
    Alignment=0, Width=403, Height=15,;
    Caption="Ragione sociale"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="LVQXKABGUH",Visible=.t., Left=477, Top=133,;
    Alignment=1, Width=81, Height=15,;
    Caption="Importi in:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="NMVBPZGNEB",Visible=.t., Left=229, Top=234,;
    Alignment=0, Width=239, Height=15,;
    Caption="Opzioni di stampa"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="YIIRLIAGVU",Visible=.t., Left=209, Top=265,;
    Alignment=0, Width=176, Height=18,;
    Caption="Liquidazione normale =>"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="ITFHKLHGMY",Visible=.t., Left=205, Top=319,;
    Alignment=0, Width=180, Height=18,;
    Caption="Dichiarazione periodica =>"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_15 as StdBox with uid="WQCBFEQVKL",left=17, top=17, width=157,height=139

  add object oBox_1_28 as StdBox with uid="SQJOKOETQI",left=229, top=254, width=238,height=0
enddefine
define class tgscg_simPag2 as StdContainer
  Width  = 638
  height = 430
  stdWidth  = 638
  stdheight = 430
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVPIMPOR1_2_1 as StdField with uid="YCEHNWHNGR",rtseq=19,rtrep=.f.,;
    cFormVar = "w_VPIMPOR1", cQueryName = "VPIMPOR1",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operazioni attive al netto d'IVA ("+g_perval+")",;
    HelpContextID = 186138489,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=118, Top=8, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oVPIMPOR1_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPVARIMP='S' And .w_VPDICGRU<>'S')
    endwith
   endif
  endfunc

  add object oVPCESINT_2_2 as StdField with uid="NXGSJKTQLA",rtseq=20,rtrep=.f.,;
    cFormVar = "w_VPCESINT", cQueryName = "VPCESINT",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operazioni attive derivanti da cessioni intracomunitarie",;
    HelpContextID = 252666026,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=441, Top=8, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oVPCESINT_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPVARIMP='S' And .w_VPDICGRU<>'S')
    endwith
   endif
  endfunc

  add object oVPIMPOR2_2_3 as StdField with uid="JERCBUZYIT",rtseq=21,rtrep=.f.,;
    cFormVar = "w_VPIMPOR2", cQueryName = "VPIMPOR2",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operazioni passive al netto d'IVA ("+g_perval+")",;
    HelpContextID = 186138488,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=118, Top=28, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oVPIMPOR2_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPVARIMP='S' And .w_VPDICGRU<>'S')
    endwith
   endif
  endfunc

  add object oVPACQINT_2_4 as StdField with uid="GFBRSCZXTY",rtseq=22,rtrep=.f.,;
    cFormVar = "w_VPACQINT", cQueryName = "VPACQINT",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operazioni passive derivanti da acquisti intracomunitari",;
    HelpContextID = 250429610,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=441, Top=28, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oVPACQINT_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPVARIMP='S' And .w_VPDICGRU<>'S')
    endwith
   endif
  endfunc

  add object oVPIMPON3_2_5 as StdField with uid="BDMALWJPUT",rtseq=23,rtrep=.f.,;
    cFormVar = "w_VPIMPON3", cQueryName = "VPIMPON3",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imponibile delle importazioni di oro e argento",;
    HelpContextID = 82296969,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=118, Top=64, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oVPIMPON3_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPVARIMP='S' And .w_VPDICGRU<>'S')
    endwith
   endif
  endfunc

  add object oVPIMPOS3_2_6 as StdField with uid="GMQWPTAUJT",rtseq=24,rtrep=.f.,;
    cFormVar = "w_VPIMPOS3", cQueryName = "VPIMPOS3",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imposta delle importazioni di oro e argento",;
    HelpContextID = 186138487,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=441, Top=64, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oVPIMPOS3_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPVARIMP='S' And .w_VPDICGRU<>'S')
    endwith
   endif
  endfunc

  add object oVPIMPOR5_2_7 as StdField with uid="DDJORJMKPR",rtseq=25,rtrep=.f.,;
    cFormVar = "w_VPIMPOR5", cQueryName = "VPIMPOR5",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ammontare dell'IVA sulle vendite",;
    HelpContextID = 186138485,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=251, Top=109, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oVPIMPOR5_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICGRU='S')
    endwith
   endif
  endfunc

  add object oVPIMPOR6_2_8 as StdField with uid="LGFISEIKFP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_VPIMPOR6", cQueryName = "VPIMPOR6",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ammontare complessivo dell'IVA sugli acquisti",;
    HelpContextID = 186138484,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=441, Top=129, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oVPIMPOR6_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICGRU='S')
    endwith
   endif
  endfunc

  add object oIMPDEB7_2_12 as StdField with uid="SPFZLMSAKN",rtseq=28,rtrep=.f.,;
    cFormVar = "w_IMPDEB7", cQueryName = "IMPDEB7",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Differenza tra IVA esigibile e IVA detraibile",;
    HelpContextID = 147903354,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=251, Top=149, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oIMPDEB7_2_12.mHide()
    with this.Parent.oContained
      return (.w_VPIMPOR7<0)
    endwith
  endfunc

  add object oIMPCRE7_2_14 as StdField with uid="SEAPHSAMZQ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_IMPCRE7", cQueryName = "IMPCRE7",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Differenza tra IVA esigibile e IVA detraibile",;
    HelpContextID = 84005754,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=441, Top=149, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oIMPCRE7_2_14.mHide()
    with this.Parent.oContained
      return (.w_VPIMPOR7>=0)
    endwith
  endfunc

  add object oIMPDEB8_2_15 as StdField with uid="CSFNYRMAWZ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_IMPDEB8", cQueryName = "IMPDEB8",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Variazioni d'imposta non comprese nei righi VP10 e VP11",;
    HelpContextID = 120532102,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=251, Top=169, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oIMPDEB8_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPCRE8=0)
    endwith
   endif
  endfunc

  func oIMPDEB8_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPDEB8>=0)
    endwith
    return bRes
  endfunc

  add object oIMPCRE8_2_16 as StdField with uid="EHOLMASDFC",rtseq=31,rtrep=.f.,;
    cFormVar = "w_IMPCRE8", cQueryName = "IMPCRE8",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Variazioni d'imposta non comprese nei righi VP10 e VP11",;
    HelpContextID = 184429702,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=441, Top=169, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oIMPCRE8_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPDEB8=0)
    endwith
   endif
  endfunc

  func oIMPCRE8_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPCRE8>=0)
    endwith
    return bRes
  endfunc

  add object oIMPDEB9_2_18 as StdField with uid="HWBRXQYOHA",rtseq=33,rtrep=.f.,;
    cFormVar = "w_IMPDEB9", cQueryName = "IMPDEB9",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA non versata gi� indicata nel rigo VP21 di una dichiarazione precedente",;
    HelpContextID = 120532102,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=251, Top=189, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oIMPDEB9_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPCRE9=0)
    endwith
   endif
  endfunc

  func oIMPDEB9_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPDEB9>=0)
    endwith
    return bRes
  endfunc

  add object oIMPCRE9_2_19 as StdField with uid="WNWUBHSMJK",rtseq=34,rtrep=.f.,;
    cFormVar = "w_IMPCRE9", cQueryName = "IMPCRE9",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "IVA versata in eccesso gi� indicata nel rigo VP21 di una dichiarazione precedente",;
    HelpContextID = 184429702,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=441, Top=189, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oIMPCRE9_2_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPDEB9=0)
    endwith
   endif
  endfunc

  func oIMPCRE9_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPCRE9>=0)
    endwith
    return bRes
  endfunc

  add object oIMPDEB10_2_21 as StdField with uid="KRAZPMOFLH",rtseq=36,rtrep=.f.,;
    cFormVar = "w_IMPDEB10", cQueryName = "IMPDEB10",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Debito riportato dal periodo precedente",;
    HelpContextID = 147903306,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=251, Top=210, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oIMPDEB10_2_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_F2DEFI<>'S' AND .w_IMPCRE10=0)
    endwith
   endif
  endfunc

  func oIMPDEB10_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPDEB10>=0)
    endwith
    return bRes
  endfunc

  add object oIMPCRE10_2_22 as StdField with uid="KTOBCQHFAQ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_IMPCRE10", cQueryName = "IMPCRE10",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Credito riportato dal periodo precedente",;
    HelpContextID = 84005706,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=441, Top=210, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oIMPCRE10_2_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_F2DEFI<>'S' AND .w_IMPDEB10=0)
    endwith
   endif
  endfunc

  func oIMPCRE10_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_IMPCRE10>=0)
    endwith
    return bRes
  endfunc

  add object oVPIMPO11_2_29 as StdField with uid="GRJCBCYOES",rtseq=42,rtrep=.f.,;
    cFormVar = "w_VPIMPO11", cQueryName = "VPIMPO11",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il credito deve essere minore del credito disponibile e maggiore di zero",;
    ToolTipText = "Credito IVA dell'anno precedente compensato nel periodo",;
    HelpContextID = 186138489,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=441, Top=231, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  proc oVPIMPO11_2_29.mBefore
      with this.Parent.oContained
        GSCG_BDV(this.Parent.oContained,"_BEFOREVP11")
      endwith
  endproc

  proc oVPIMPO11_2_29.mAfter
      with this.Parent.oContained
        GSCG_BDV(this.Parent.oContained,"_AFTERVP11")
      endwith
  endproc

  func oVPIMPO11_2_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPIMPO11<=.w_CRERES And .w_VPIMPO11>=0)
    endwith
    return bRes
  endfunc

  add object oIMPDEB12_2_32 as StdField with uid="YLYANSOVPA",rtseq=44,rtrep=.f.,;
    cFormVar = "w_IMPDEB12", cQueryName = "IMPDEB12",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Somma algebrica dei righi da VP12 al VP16",;
    HelpContextID = 147903304,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=251, Top=251, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oIMPDEB12_2_32.mHide()
    with this.Parent.oContained
      return (.w_VPIMPO12<0)
    endwith
  endfunc

  add object oIMPCRE12_2_33 as StdField with uid="EZZRCODZHX",rtseq=45,rtrep=.f.,;
    cFormVar = "w_IMPCRE12", cQueryName = "IMPCRE12",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Somma algebrica dei righi da VP12 al VP16",;
    HelpContextID = 84005704,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=441, Top=251, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oIMPCRE12_2_33.mHide()
    with this.Parent.oContained
      return (.w_VPIMPO12>=0)
    endwith
  endfunc

  add object oVPIMPO13_2_35 as StdField with uid="ALXFZKRLSF",rtseq=46,rtrep=.f.,;
    cFormVar = "w_VPIMPO13", cQueryName = "VPIMPO13",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il credito non pu� superare il debito per il periodo",;
    ToolTipText = "Ammontare dei particolari crediti di imposta utilizzati nel periodo",;
    HelpContextID = 186138487,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=441, Top=271, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oVPIMPO13_2_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_IMPDEB12>0 And Not (g_TIPDEN='T' And .w_VPPERIOD=4) And .w_VPDICSOC<>'S')
    endwith
   endif
  endfunc

  func oVPIMPO13_2_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPIMPO13>=0 And .w_VPIMPO13<=.w_IMPDEB12)
    endwith
    return bRes
  endfunc

  add object oVPIMPO14_2_37 as StdField with uid="VAHMDBVVQB",rtseq=47,rtrep=.f.,;
    cFormVar = "w_VPIMPO14", cQueryName = "VPIMPO14",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Importo interessi per le liquidazioni trimestrali non congruente",;
    ToolTipText = "Interessi dovuti per liquidazioni trimestrali",;
    HelpContextID = 186138486,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=251, Top=291, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oVPIMPO14_2_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_TIPDEN<>'M')
    endwith
   endif
  endfunc

  func oVPIMPO14_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPIMPO14=0 OR (.w_VPIMPO14>0 AND (.w_VPIMPO12 - .w_VPIMPO13)>0))
    endwith
    return bRes
  endfunc

  add object oVPIMPO15_2_38 as StdField with uid="ONJGXRIWMI",rtseq=48,rtrep=.f.,;
    cFormVar = "w_VPIMPO15", cQueryName = "VPIMPO15",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Acconto versato relativo alla liquidazione dell'ultimo periodo dell'anno",;
    HelpContextID = 186138485,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=441, Top=311, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVPIMPO16_2_39 as StdField with uid="IZWVTDCWON",rtseq=49,rtrep=.f.,;
    cFormVar = "w_VPIMPO16", cQueryName = "VPIMPO16",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo da versare o da trasferire (VP17 col.1-VP18+vp19-VP20)",;
    HelpContextID = 186138484,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=251, Top=341, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  add object oVPIMPVER_2_41 as StdField with uid="IPSCJIFUTX",rtseq=50,rtrep=.f.,;
    cFormVar = "w_VPIMPVER", cQueryName = "VPIMPVER",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "L'importo versato non pu� essere negativo",;
    ToolTipText = "Importo del versamento riportato sul modello F24",;
    HelpContextID = 68697944,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=127, Top=370, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oVPIMPVER_2_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' AND .w_VPIMPO16<>0 AND NOT (g_TIPDEN='T' AND .w_VPPERIOD=4))
    endwith
   endif
  endfunc

  func oVPIMPVER_2_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPIMPVER>=0)
    endwith
    return bRes
  endfunc

  add object oVPVEREUR_2_42 as StdCheck with uid="ZTZVYWTCXY",rtseq=51,rtrep=.f.,left=333, top=370, caption="Versamento in Euro", enabled=.f.,;
    ToolTipText = "Valuta con cui viene espresso il versamento",;
    HelpContextID = 83849048,;
    cFormVar="w_VPVEREUR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oVPVEREUR_2_42.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVPVEREUR_2_42.GetRadio()
    this.Parent.oContained.w_VPVEREUR = this.RadioValue()
    return .t.
  endfunc

  func oVPVEREUR_2_42.SetRadio()
    this.Parent.oContained.w_VPVEREUR=trim(this.Parent.oContained.w_VPVEREUR)
    this.value = ;
      iif(this.Parent.oContained.w_VPVEREUR=='S',1,;
      0)
  endfunc

  proc oVPVEREUR_2_42.mAfter
    with this.Parent.oContained
      .w_VPIMPVER= 0
    endwith
  endproc

  add object oCRERES_2_65 as StdField with uid="BOEQBQAHYJ",rtseq=52,rtrep=.f.,;
    cFormVar = "w_CRERES", cQueryName = "CRERES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Credito IVA dell'anno precedente ancora utilizzabile",;
    HelpContextID = 130252506,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=441, Top=396, cSayPict="v_PV(40)", cGetPict="v_GV(40)"


  add object oObj_2_70 as cp_calclbl with uid="LPMLOQSWLH",left=7, top=8, width=109,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="VP1 - Op.Attive",Fontbold=.t.,;
    nPag=2;
    , HelpContextID = 80616986


  add object oObj_2_71 as cp_calclbl with uid="URUAFHFTOZ",left=7, top=28, width=109,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption='VP2 - Op.Passive',Fontbold=.t.,;
    nPag=2;
    , HelpContextID = 80616986


  add object oObj_2_72 as cp_calclbl with uid="KMAMMMQTPJ",left=323, top=8, width=116,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="di cui Cessioni Intra",Fontbold=.t.,;
    nPag=2;
    , HelpContextID = 80616986


  add object oObj_2_73 as cp_calclbl with uid="ZCFPYDSKNS",left=323, top=28, width=113,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption='di cui Acquisti Intra',Fontbold=.t.,;
    nPag=2;
    , HelpContextID = 80616986


  add object oObj_2_74 as cp_calclbl with uid="KIUYFSPXAZ",left=7, top=109, width=239,height=13,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="VP10 - Iva Esigibile per il Periodo",Fontbold=.t.,;
    nPag=2;
    , HelpContextID = 80616986


  add object oObj_2_75 as cp_calclbl with uid="ZSMRFWQNFZ",left=7, top=129, width=239,height=13,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="VP11 - Iva che si detrae per il Periodo",Fontbold=.t.,;
    nPag=2;
    , HelpContextID = 80616986


  add object oObj_2_76 as cp_calclbl with uid="UAXEQEIZSK",left=7, top=169, width=239,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="VP13 - Variazioni di Imposta",Fontbold=.t.,;
    nPag=2;
    , HelpContextID = 80616986


  add object oObj_2_77 as cp_calclbl with uid="NTRPYVCLYF",left=7, top=231, width=239,height=13,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="VP16 - Credito Iva Compensabile",Fontbold=.t.,;
    nPag=2;
    , HelpContextID = 80616986


  add object oObj_2_78 as cp_calclbl with uid="TUDVQIAFYW",left=7, top=370, width=119,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="VP22 - Versamento",Fontbold=.t.,;
    nPag=2;
    , HelpContextID = 80616986


  add object oObj_2_80 as cp_calclbl with uid="ENBQUAESLN",left=7, top=64, width=109,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="VP2 - Op.Passive",Fontbold=.t.,;
    nPag=2;
    , HelpContextID = 80616986


  add object oObj_2_81 as cp_calclbl with uid="XSOCYONAUC",left=359, top=64, width=78,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="VP2 - Op.Passive",Fontbold=.t.,;
    nPag=2;
    , HelpContextID = 80616986


  add object oObj_2_84 as cp_calclbl with uid="TZJBAKOIBK",left=7, top=189, width=239,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="VP14 - Iva non versata/in eccesso",Fontbold=.t.,;
    nPag=2;
    , HelpContextID = 80616986

  add object oStr_2_10 as StdString with uid="GMZESXGIRV",Visible=.t., Left=251, Top=86,;
    Alignment=0, Width=163, Height=15,;
    Caption="DEBITI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_11 as StdString with uid="KFQADRSQUB",Visible=.t., Left=441, Top=85,;
    Alignment=0, Width=160, Height=15,;
    Caption="CREDITI"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_13 as StdString with uid="REXHGNVSAQ",Visible=.t., Left=7, Top=149,;
    Alignment=0, Width=239, Height=18,;
    Caption="VP12 - debito o credito per il periodo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_24 as StdString with uid="ZBMNXRNWAF",Visible=.t., Left=7, Top=210,;
    Alignment=0, Width=239, Height=18,;
    Caption="VP15 - debito/credito riport."  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_26 as StdString with uid="URZOGZDSTS",Visible=.t., Left=7, Top=251,;
    Alignment=0, Width=239, Height=18,;
    Caption="VP17 - IVA dovuta/credito per il per."  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_31 as StdString with uid="UVPQJQHTSV",Visible=.t., Left=7, Top=271,;
    Alignment=0, Width=239, Height=18,;
    Caption="VP18 - crediti speciali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_34 as StdString with uid="HSEQFDOYWY",Visible=.t., Left=7, Top=291,;
    Alignment=0, Width=239, Height=18,;
    Caption="VP19 - interessi dovuti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_36 as StdString with uid="HHCDJUDDZE",Visible=.t., Left=7, Top=311,;
    Alignment=0, Width=239, Height=18,;
    Caption="VP20 - acconto versato"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_40 as StdString with uid="WKEWYLEUTZ",Visible=.t., Left=7, Top=341,;
    Alignment=0, Width=212, Height=18,;
    Caption="VP21 - importo da versare"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_43 as StdString with uid="PJJIKFYCUY",Visible=.t., Left=278, Top=370,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_43.mHide()
    with this.Parent.oContained
      return (.w_VPVEREUR='S')
    endwith
  endfunc

  add object oStr_2_44 as StdString with uid="WENXHMYUQB",Visible=.t., Left=270, Top=8,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_44.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_45 as StdString with uid="RTKDBUXTZA",Visible=.t., Left=270, Top=28,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_45.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_46 as StdString with uid="LZNNYECVKR",Visible=.t., Left=270, Top=64,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_46.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_47 as StdString with uid="MJSTZVEUPG",Visible=.t., Left=403, Top=109,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_47.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_48 as StdString with uid="DOUOHRPNMW",Visible=.t., Left=595, Top=8,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_48.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_49 as StdString with uid="SGMVNGJHRU",Visible=.t., Left=595, Top=28,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_49.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_50 as StdString with uid="XAENRTHRFF",Visible=.t., Left=595, Top=64,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_50.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_51 as StdString with uid="GLVEHDADHY",Visible=.t., Left=595, Top=169,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_51.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_52 as StdString with uid="YOESMVFOUA",Visible=.t., Left=403, Top=149,;
    Alignment=0, Width=28, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_52.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S' Or .w_VPIMPOR7<0)
    endwith
  endfunc

  add object oStr_2_53 as StdString with uid="BUTOBWUZOE",Visible=.t., Left=403, Top=169,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_53.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_54 as StdString with uid="OJHZRSXPNT",Visible=.t., Left=403, Top=210,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_54.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S' Or .w_VPIMPO10<0)
    endwith
  endfunc

  add object oStr_2_55 as StdString with uid="RMSOAXRUJG",Visible=.t., Left=403, Top=251,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_55.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S' OR .w_VPIMPO12<0)
    endwith
  endfunc

  add object oStr_2_56 as StdString with uid="SIQNJNOSIO",Visible=.t., Left=403, Top=291,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_56.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_57 as StdString with uid="KHLYPJMKHJ",Visible=.t., Left=595, Top=129,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_57.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_58 as StdString with uid="KBUWXXVOZZ",Visible=.t., Left=595, Top=149,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_58.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S' Or .w_VPIMPOR7>=0)
    endwith
  endfunc

  add object oStr_2_59 as StdString with uid="GTFNQQIGFZ",Visible=.t., Left=595, Top=210,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_59.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S' Or .w_VPIMPO10>=0)
    endwith
  endfunc

  add object oStr_2_60 as StdString with uid="KFOXIPZFGC",Visible=.t., Left=595, Top=231,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_60.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_61 as StdString with uid="ISDSTRMZWF",Visible=.t., Left=595, Top=251,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_61.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S' OR .w_VPIMPO12>=0)
    endwith
  endfunc

  add object oStr_2_62 as StdString with uid="MVWUSVTODK",Visible=.t., Left=595, Top=271,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_62.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_63 as StdString with uid="GZSWGWACLO",Visible=.t., Left=595, Top=311,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_63.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_64 as StdString with uid="GESBJZKERN",Visible=.t., Left=403, Top=341,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_64.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_66 as StdString with uid="HUYVXDWZNZ",Visible=.t., Left=489, Top=376,;
    Alignment=0, Width=114, Height=15,;
    Caption="Credito utilizzabile"  ;
  , bGlobalFont=.t.

  add object oStr_2_69 as StdString with uid="CZAPHOJFCI",Visible=.t., Left=595, Top=396,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_69.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_79 as StdString with uid="UBHQNKODBV",Visible=.t., Left=6, Top=396,;
    Alignment=0, Width=401, Height=13,;
    Caption="In blue le descrizioni associate ad importi editabili."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_82 as StdString with uid="MRSSCXETVV",Visible=.t., Left=595, Top=189,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_82.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oStr_2_83 as StdString with uid="NZQUTIMQEA",Visible=.t., Left=403, Top=189,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_2_83.mHide()
    with this.Parent.oContained
      return (.w_VPCODVAL='S')
    endwith
  endfunc

  add object oBox_2_67 as StdBox with uid="ZIMOQCIOTB",left=12, top=334, width=621,height=2

  add object oBox_2_68 as StdBox with uid="COOYXIQIBU",left=14, top=103, width=618,height=1
enddefine
define class tgscg_simPag3 as StdContainer
  Width  = 638
  height = 430
  stdWidth  = 638
  stdheight = 430
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVPVERNEF_3_1 as StdCheck with uid="XWETPYPOMX",rtseq=53,rtrep=.f.,left=55, top=21, caption="Versamento non effettuato a seguito di agevolazioni per eventi eccezionali",;
    HelpContextID = 201289572,;
    cFormVar="w_VPVERNEF", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVPVERNEF_3_1.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVPVERNEF_3_1.GetRadio()
    this.Parent.oContained.w_VPVERNEF = this.RadioValue()
    return .t.
  endfunc

  func oVPVERNEF_3_1.SetRadio()
    this.Parent.oContained.w_VPVERNEF=trim(this.Parent.oContained.w_VPVERNEF)
    this.value = ;
      iif(this.Parent.oContained.w_VPVERNEF=='S',1,;
      0)
  endfunc

  func oVPVERNEF_3_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' And .w_VPIMPVER=0)
    endwith
   endif
  endfunc

  add object oVPAR74C5_3_3 as StdCheck with uid="UATPFQYAGN",rtseq=54,rtrep=.f.,left=55, top=45, caption="Subfornitori che si sono avvalsi delle agevolazioni di cui all'art.74, comma 5",;
    HelpContextID = 128171893,;
    cFormVar="w_VPAR74C5", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVPAR74C5_3_3.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPAR74C5_3_3.GetRadio()
    this.Parent.oContained.w_VPAR74C5 = this.RadioValue()
    return .t.
  endfunc

  func oVPAR74C5_3_3.SetRadio()
    this.Parent.oContained.w_VPAR74C5=trim(this.Parent.oContained.w_VPAR74C5)
    this.value = ;
      iif(this.Parent.oContained.w_VPAR74C5=='S',1,;
      0)
  endfunc

  func oVPAR74C5_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S')
    endwith
   endif
  endfunc

  add object oVPAR74C4_3_4 as StdCheck with uid="CZFHSSAWDI",rtseq=55,rtrep=.f.,left=55, top=69, caption="Contribuenti con liquidazioni trimestrali (art.74, comma 4)",;
    HelpContextID = 128171894,;
    cFormVar="w_VPAR74C4", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVPAR74C4_3_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPAR74C4_3_4.GetRadio()
    this.Parent.oContained.w_VPAR74C4 = this.RadioValue()
    return .t.
  endfunc

  func oVPAR74C4_3_4.SetRadio()
    this.Parent.oContained.w_VPAR74C4=trim(this.Parent.oContained.w_VPAR74C4)
    this.value = ;
      iif(this.Parent.oContained.w_VPAR74C4=='S',1,;
      0)
  endfunc

  func oVPAR74C4_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S')
    endwith
   endif
  endfunc

  add object oVPOPMEDI_3_7 as StdCheck with uid="YFAOBVTFPB",rtseq=56,rtrep=.f.,left=66, top=138, caption="Aliquota media",;
    HelpContextID = 88399713,;
    cFormVar="w_VPOPMEDI", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVPOPMEDI_3_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPOPMEDI_3_7.GetRadio()
    this.Parent.oContained.w_VPOPMEDI = this.RadioValue()
    return .t.
  endfunc

  func oVPOPMEDI_3_7.SetRadio()
    this.Parent.oContained.w_VPOPMEDI=trim(this.Parent.oContained.w_VPOPMEDI)
    this.value = ;
      iif(this.Parent.oContained.w_VPOPMEDI=='S',1,;
      0)
  endfunc

  add object oVPOPNOIM_3_8 as StdCheck with uid="KUTTQVXLCF",rtseq=57,rtrep=.f.,left=194, top=138, caption="Operazioni non imponibili",;
    HelpContextID = 80421027,;
    cFormVar="w_VPOPNOIM", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVPOPNOIM_3_8.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oVPOPNOIM_3_8.GetRadio()
    this.Parent.oContained.w_VPOPNOIM = this.RadioValue()
    return .t.
  endfunc

  func oVPOPNOIM_3_8.SetRadio()
    this.Parent.oContained.w_VPOPNOIM=trim(this.Parent.oContained.w_VPOPNOIM)
    this.value = ;
      iif(this.Parent.oContained.w_VPOPNOIM=='S',1,;
      0)
  endfunc

  add object oVPACQBEA_3_9 as StdCheck with uid="PGQTKYERVP",rtseq=58,rtrep=.f.,left=379, top=138, caption="Acquisto di beni ammortizzabili",;
    HelpContextID = 135446377,;
    cFormVar="w_VPACQBEA", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oVPACQBEA_3_9.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oVPACQBEA_3_9.GetRadio()
    this.Parent.oContained.w_VPACQBEA = this.RadioValue()
    return .t.
  endfunc

  func oVPACQBEA_3_9.SetRadio()
    this.Parent.oContained.w_VPACQBEA=trim(this.Parent.oContained.w_VPACQBEA)
    this.value = ;
      iif(this.Parent.oContained.w_VPACQBEA=='S',1,;
      0)
  endfunc

  add object oVPCRERIM_3_10 as StdField with uid="EIKUPNXXDW",rtseq=59,rtrep=.f.,;
    cFormVar = "w_VPCRERIM", cQueryName = "VPCRERIM",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La somma chiesta a rimborso non deve superare il credito del periodo",;
    ToolTipText = "Importo rimborsato",;
    HelpContextID = 121397411,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=230, Top=177, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oVPCRERIM_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND (.w_VPOPMEDI='S' OR .w_VPOPNOIM='S' OR .w_VPACQBEA='S'))
    endwith
   endif
  endfunc

  func oVPCRERIM_3_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPCRERIM+.w_VPCREUTI<=ABS(.w_VPIMPO12))
    endwith
    return bRes
  endfunc


  add object oVPEURO19_3_12 as StdCombo with uid="IHMZDWZTMN",rtseq=60,rtrep=.f.,left=429,top=176,width=71,height=21, enabled=.f.;
    , ToolTipText = "Valuta con cui viene espresso il versamento";
    , HelpContextID = 183533425;
    , cFormVar="w_VPEURO19",RowSource=""+"Lire,"+"Euro", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oVPEURO19_3_12.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oVPEURO19_3_12.GetRadio()
    this.Parent.oContained.w_VPEURO19 = this.RadioValue()
    return .t.
  endfunc

  func oVPEURO19_3_12.SetRadio()
    this.Parent.oContained.w_VPEURO19=trim(this.Parent.oContained.w_VPEURO19)
    this.value = ;
      iif(this.Parent.oContained.w_VPEURO19=='N',1,;
      iif(this.Parent.oContained.w_VPEURO19=='S',2,;
      0))
  endfunc

  add object oVPCREUTI_3_13 as StdField with uid="MGUUQKOBGU",rtseq=61,rtrep=.f.,;
    cFormVar = "w_VPCREUTI", cQueryName = "VPCREUTI",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "La somma chiesta a rimborso non deve superare il credito del periodo",;
    ToolTipText = "Credito da utilizzare in compensazione con mod. F24",;
    HelpContextID = 96706401,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=372, Top=207, cSayPict="v_PV(40)", cGetPict="v_GV(40)"

  func oVPCREUTI_3_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' AND .w_VPIMPO12<0 AND (.w_VPOPMEDI='S' OR .w_VPOPNOIM='S' OR .w_VPACQBEA='S'))
    endwith
   endif
  endfunc

  func oVPCREUTI_3_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VPCRERIM+.w_VPCREUTI<=ABS(.w_VPIMPO12))
    endwith
    return bRes
  endfunc


  add object oVPCODCAR_3_14 as StdCombo with uid="WZBXXEGJPJ",rtseq=62,rtrep=.f.,left=104,top=271,width=242,height=21;
    , ToolTipText = "Codice carica dichiarante";
    , HelpContextID = 131506008;
    , cFormVar="w_VPCODCAR",RowSource=""+"1) Rappresentante legale o negoziale,"+"2) Socio amministratore,"+"3) Curatore fallimentare,"+"4) Commissario liquidatore,"+"5) Commissario giudiziale,"+"6) Rappresentante fiscale,"+"7) Eredi del contribuente,"+"8) Liquidatore,"+"9) Societ� beneficiaria o incorporante", bObbl = .t. , nPag = 3;
  , bGlobalFont=.t.


  func oVPCODCAR_3_14.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    iif(this.value =8,'8',;
    iif(this.value =9,'9',;
    space(1)))))))))))
  endfunc
  func oVPCODCAR_3_14.GetRadio()
    this.Parent.oContained.w_VPCODCAR = this.RadioValue()
    return .t.
  endfunc

  func oVPCODCAR_3_14.SetRadio()
    this.Parent.oContained.w_VPCODCAR=trim(this.Parent.oContained.w_VPCODCAR)
    this.value = ;
      iif(this.Parent.oContained.w_VPCODCAR=='1',1,;
      iif(this.Parent.oContained.w_VPCODCAR=='2',2,;
      iif(this.Parent.oContained.w_VPCODCAR=='3',3,;
      iif(this.Parent.oContained.w_VPCODCAR=='4',4,;
      iif(this.Parent.oContained.w_VPCODCAR=='5',5,;
      iif(this.Parent.oContained.w_VPCODCAR=='6',6,;
      iif(this.Parent.oContained.w_VPCODCAR=='7',7,;
      iif(this.Parent.oContained.w_VPCODCAR=='8',8,;
      iif(this.Parent.oContained.w_VPCODCAR=='9',9,;
      0)))))))))
  endfunc

  func oVPCODCAR_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S')
    endwith
   endif
  endfunc

  add object oVPCODFIS_3_16 as StdField with uid="DNHKQHBKJL",rtseq=63,rtrep=.f.,;
    cFormVar = "w_VPCODFIS", cQueryName = "VPCODFIS",;
    bObbl = .t. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del dichiarante",;
    HelpContextID = 187261097,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=456, Top=271, InputMask=replicate('X',16)

  func oVPCODFIS_3_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S')
    endwith
   endif
  endfunc

  func oVPCODFIS_3_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_VPCODFIS,'CF','',1,'', g_CODNAZ ))
    endwith
    return bRes
  endfunc


  add object oObj_3_20 as cp_calclbl with uid="RFYWSQPBKK",left=16, top=181, width=209,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="VP31 - Credito chiesto a rimborso:",Fontbold=.t.,;
    nPag=3;
    , HelpContextID = 80616986


  add object oObj_3_21 as cp_calclbl with uid="EMTDOYLAAL",left=16, top=211, width=351,height=15,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="VP32 - Credito da utilizzare in compensazione mod. F24",Fontbold=.t.,;
    nPag=3;
    , HelpContextID = 80616986

  add object oVPDATVER_3_23 as StdField with uid="OKHRTOXIAR",rtseq=65,rtrep=.f.,;
    cFormVar = "w_VPDATVER", cQueryName = "VPDATVER",;
    bObbl = .t. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del versamento",;
    HelpContextID = 65310552,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=116, Top=345

  func oVPDATVER_3_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' AND .w_VPIMPVER>0)
    endwith
   endif
  endfunc

  add object oCODBAN_3_24 as StdField with uid="PEPLDOWAVR",rtseq=66,rtrep=.f.,;
    cFormVar = "w_CODBAN", cQueryName = "CODBAN",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "C.corrente su cui appoggiare il versamento",;
    HelpContextID = 219386330,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=333, Top=345, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", oKey_1_1="BACODBAN", oKey_1_2="this.w_CODBAN"

  func oCODBAN_3_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VPDICSOC<>'S' AND .w_VPIMPVER>0)
    endwith
   endif
  endfunc

  func oCODBAN_3_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODBAN_3_24.ecpDrop(oSource)
    this.Parent.oContained.link_3_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODBAN_3_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCODBAN_3_24'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Conti correnti",'',this.parent.oContained
  endproc

  add object oVPCODAZI_3_25 as StdField with uid="VKIDGSGODN",rtseq=67,rtrep=.f.,;
    cFormVar = "w_VPCODAZI", cQueryName = "VPCODAZI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice ABI del conto",;
    HelpContextID = 103375007,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=279, Top=380, InputMask=replicate('X',5), cLinkFile="COD_ABI", cZoomOnZoom="GSAR_ABI", oKey_1_1="ABCODABI", oKey_1_2="this.w_VPCODAZI"

  func oVPCODAZI_3_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_VPCODCAB)
        bRes2=.link_3_26('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oVPCODCAB_3_26 as StdField with uid="PMFZTAFFHO",rtseq=68,rtrep=.f.,;
    cFormVar = "w_VPCODCAB", cQueryName = "VPCODCAB",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice CAB del conto",;
    HelpContextID = 131506024,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=425, Top=380, InputMask=replicate('X',5), cLinkFile="COD_CAB", cZoomOnZoom="GSAR_AFI", oKey_1_1="FICODABI", oKey_1_2="this.w_VPCODAZI", oKey_2_1="FICODCAB", oKey_2_2="this.w_VPCODCAB"

  func oVPCODCAB_3_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oStr_3_2 as StdString with uid="ENWQITWWAF",Visible=.t., Left=16, Top=18,;
    Alignment=0, Width=30, Height=18,;
    Caption="VP23"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_5 as StdString with uid="OJNNRAWCKI",Visible=.t., Left=16, Top=114,;
    Alignment=0, Width=30, Height=18,;
    Caption="VP30"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_6 as StdString with uid="WEPSJZJNXM",Visible=.t., Left=66, Top=114,;
    Alignment=0, Width=535, Height=15,;
    Caption="Dati per rimborso o compensazione infrannuale (art.30, 3)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_11 as StdString with uid="NBWNDDAUVC",Visible=.t., Left=361, Top=179,;
    Alignment=0, Width=24, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_11.mHide()
    with this.Parent.oContained
      return (.w_VPEURO19='S')
    endwith
  endfunc

  add object oStr_3_15 as StdString with uid="OJUFIPYAPL",Visible=.t., Left=18, Top=271,;
    Alignment=1, Width=82, Height=15,;
    Caption="Codice carica:"  ;
  , bGlobalFont=.t.

  add object oStr_3_17 as StdString with uid="KJGRQDDQFE",Visible=.t., Left=16, Top=239,;
    Alignment=0, Width=571, Height=15,;
    Caption="Dichiarante"  ;
  , bGlobalFont=.t.

  add object oStr_3_18 as StdString with uid="AJWZLPDSUC",Visible=.t., Left=364, Top=271,;
    Alignment=1, Width=88, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_3_22 as StdString with uid="KNEBTNDHIJ",Visible=.t., Left=505, Top=209,;
    Alignment=0, Width=28, Height=15,;
    Caption=".000"  ;
  , bGlobalFont=.t.

  func oStr_3_22.mHide()
    with this.Parent.oContained
      return (.w_VPEURO19='S')
    endwith
  endfunc

  add object oStr_3_27 as StdString with uid="SXAFPUPNED",Visible=.t., Left=207, Top=345,;
    Alignment=1, Width=123, Height=18,;
    Caption="Conto corrente:"  ;
  , bGlobalFont=.t.

  add object oStr_3_28 as StdString with uid="XFZRNSKFYY",Visible=.t., Left=117, Top=380,;
    Alignment=1, Width=159, Height=18,;
    Caption="Codice azienda:"  ;
  , bGlobalFont=.t.

  add object oStr_3_29 as StdString with uid="VPPWPKUTCR",Visible=.t., Left=372, Top=380,;
    Alignment=1, Width=50, Height=18,;
    Caption="CAB:"  ;
  , bGlobalFont=.t.

  add object oStr_3_30 as StdString with uid="DCECZMFIBK",Visible=.t., Left=4, Top=345,;
    Alignment=1, Width=109, Height=18,;
    Caption="Data versamento:"  ;
  , bGlobalFont=.t.

  add object oStr_3_31 as StdString with uid="PEAEHKDLJV",Visible=.t., Left=16, Top=313,;
    Alignment=0, Width=570, Height=18,;
    Caption="Estremi del versamento"  ;
  , bGlobalFont=.t.

  add object oStr_3_34 as StdString with uid="XRSWXJXZLM",Visible=.t., Left=398, Top=176,;
    Alignment=1, Width=27, Height=18,;
    Caption="in:"  ;
  , bGlobalFont=.t.

  add object oBox_3_19 as StdBox with uid="EMRKBEKMOZ",left=13, top=256, width=575,height=2

  add object oBox_3_32 as StdBox with uid="KJUOOZNRVU",left=16, top=332, width=575,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_sim','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
