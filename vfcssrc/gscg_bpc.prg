* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bpc                                                        *
*              Stampa bilancio verifica                                        *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][55]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-12-10                                                      *
* Last revis.: 2014-03-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bpc",oParentObject)
return(i_retval)

define class tgscg_bpc as StdBatch
  * --- Local variables
  w_DATEUR = ctod("  /  /  ")
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_QATT = 0
  w_QSAL = 0
  w_QRIC = 0
  w_QPAS = 0
  w_QCOS = 0
  w_MESS = space(50)
  w_QORD = 0
  w_QTRA = 0
  w_QPRE = 0
  w_QRIS = 0
  w_QRISP = 0
  w_OBTEST = ctod("  /  /  ")
  w_APERTURA = .f.
  w_MessError = space(200)
  w_QUADRATU = .f.
  w_MAXUTDV = ctod("  /  /  ")
  w_CHIUSURA = .f.
  w_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  w_PRIMO = space(15)
  w_DESCRI1 = space(40)
  w_STA1 = 0
  w_SECONDO = space(15)
  w_DESCRI2 = space(40)
  w_STA2 = 0
  w_TERZO = space(15)
  w_DESCRI3 = space(40)
  w_STA3 = 0
  w_QUARTO = space(15)
  w_DESCRI4 = space(40)
  w_STA4 = 0
  w_QUINTO = space(15)
  w_DESCRI5 = space(40)
  w_STA5 = 0
  w_SESTO = space(15)
  w_DESCRI6 = space(40)
  w_STA6 = 0
  w_SETTIMO = space(15)
  w_DESCRI7 = space(40)
  w_STA7 = 0
  w_OTTAVO = space(15)
  w_DESCRI8 = space(40)
  w_STA8 = 0
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_OLDMASTR = space(15)
  w_OLDSEZBIL = space(1)
  w_OR_DINE = 0
  w_NEWMASTR = space(15)
  w_DES_AT = space(15)
  w_POS_REC = 0
  ACODMAS = space(15)
  ADESMAS = space(40)
  ASEZBIL = space(1)
  PCODMAS = space(15)
  PDESMAS = 0
  PSEZBIL = space(1)
  ACODCON = space(15)
  ADESCON = space(40)
  ATIPCON = space(1)
  PCODCON = space(15)
  PDESCON = space(40)
  PTIPCON = space(1)
  AVALMAS = 0
  AVALCON = 0
  ADESCRI = space(40)
  PVALMAS = 0
  PVALCON = 0
  PDESCRI = space(40)
  contaA = 0
  contaR = 0
  TOTMAS = 0
  contaP = 0
  contaO = 0
  appocod = space(15)
  POSIZ = space(10)
  contaC = 0
  contaT = 0
  appodes = space(40)
  * --- WorkFile variables
  VALUTE_idx=0
  ESERCIZI_idx=0
  TMPMOVIMAST_idx=0
  TMPMOVIDETT_idx=0
  TMPPNT_MAST_idx=0
  CONTI_idx=0
  MASTRI_idx=0
  PIANOCON_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- STAMPA BILANCIO DI VERIFICA (da GSCG_SPC)
    * --- La stampa � effettuata nella valuta di conto dell'esercizio
    *     ATTENZIONE: LA STAMPA NON LEGGE PIU' I SALDI DELL'ESERCIZIO
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_DATEUR = GETVALUT(this.oParentObject.w_DIVISA,"VADATEUR")
    * --- L_molti e q sono i due camib, q non � significativo, L_molti ho prende il cambio presente
    *     sulla maschera se stampa bilancio in valuta, altrimenti sempre 1
    *     (perch �questo cambio si applica sempre ad una valuta di conto attuale o del passato, cmq valuta di conto del movimento)
    L_MOLTI=iif(this.oParentObject.w_monete<>"a", 1 ,iif(this.oParentObject.w_CAMVAL=0,1,this.oParentObject.w_CAMVAL)) 
 q=1 
 L_EUROPEA=IIF(this.oParentObject.w_EXTRAEUR=0,.F.,.T.) 
 s=20*( this.oParentObject.w_decimi +2) 
 L_VAPP=this.oParentObject.w_VALAPP
    L_ESE= this.oParentObject.w_ESE 
 L_ESPREC=this.oParentObject.w_ESPREC 
 L_ATTIVI=this.oParentObject.w_ATTIVITA 
 L_PASSI=this.oParentObject.w_PASSIVI 
 L_COST=this.oParentObject.w_COSTI 
 L_RICA=this.oParentObject.w_RICAVI 
 L_ORDI=this.oParentObject.w_ORDINE 
 L_TRANS=this.oParentObject.w_TRANSI 
 L_TUTTI=this.oParentObject.w_TUTTI 
 L_TUTTI_Z=this.oParentObject.w_TUTTI_Z
    L_TOTDAT=this.oParentObject.w_TOTDAT 
 L_DATSTA= this.oParentObject.w_DATA2 
 L_DATA2=this.oParentObject.w_DATA2 
 L_DATA1=this.oParentObject.w_DATA1 
 L_MONETE=this.oParentObject.w_MONETE
    L_CODBUN=this.oParentObject.w_CODBUN 
 L_SUPERBU=this.oParentObject.w_SUPERBU
    L_DIVISA=this.oParentObject.w_DIVISA 
 L_CAMVAL=this.oParentObject.w_CAMVAL 
 L_DECIMI=this.oParentObject.w_DECIMI 
 L_TIPSTA=this.oParentObject.w_TIPSTA 
 L_PERIODO=this.oParentObject.w_PERIODO 
 L_ESCINFR=this.oParentObject.w_INFRAAN
    * --- Problema con variabile PROVVI
    L_PROVVI=This.oParentObject.w_PROVVI
    * --- Lancio il logo assocciato all'azienda se esiste
    L_LOGO=GETBMP("BILVER"+I_CODAZI)
    L_QUADRATU=.F. 
 L_RISULPRE = 0
    * --- LEGGO I MOVIMENTI COMPRESI FRA LE DATE DI STAMPA E DI COMPETENZA DELL'ESERCIZIO
    *     SE SPECIFICATI, APPLICO I FILTRI PER B.U. E MOVIMENTI PROVVISORI
    * --- LEGGO I MOVIMENTI PRECEDENTI ALLA DATA DI INIZIO STAMPA CON COMPETENZA DELL'ESERCIZIO IN CORSO
    *     QUESTA QUERY SI PUO' EVITARE SE LA DATA DI INIZIO STAMPA COINCIDE CON L'INIZIO DELL'ESERCIZIO
    ah_msg("Lettura movimenti compresi fra le date di stampa",.t.,.t.)
    if this.oParentObject.w_ESSALDI="S"
      * --- Create temporary table TMPMOVIMAST
      i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_DOKVTLCAOB[2]
      indexes_DOKVTLCAOB[1]='PNTIPCON,PNCODCON'
      indexes_DOKVTLCAOB[2]='MCCODICE'
      vq_exec('query\b_movimenti1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_DOKVTLCAOB,.f.)
      this.TMPMOVIMAST_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    else
      if this.oParentObject.w_MASABI="S"
        * --- Create temporary table TMPMOVIMAST
        i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_QWFQYERVLW[2]
        indexes_QWFQYERVLW[1]='PNTIPCON,PNCODCON'
        indexes_QWFQYERVLW[2]='MCCODICE'
        vq_exec('query\b_movimenti2',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_QWFQYERVLW,.f.)
        this.TMPMOVIMAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        * --- Create temporary table TMPMOVIMAST
        i_nIdx=cp_AddTableDef('TMPMOVIMAST') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_JOTWYIJLLC[2]
        indexes_JOTWYIJLLC[1]='PNTIPCON,PNCODCON'
        indexes_JOTWYIJLLC[2]='MCCODICE'
        vq_exec('query\b_movimenti',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_JOTWYIJLLC,.f.)
        this.TMPMOVIMAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    endif
    if this.oParentObject.w_ESPREC="S" and this.oParentObject.w_PROVVI<>"S"
      * --- INIZIO LA LETTURA DEI SALDI DELL'ESERCIZIO PRECEDENTE
      *     1- VERIFICO SE PER L'ESERCIZIO IN CORSO E' STATA FATTA L'APERTURA
      * --- Entro a verificare se c'� apertura anche nel caso che sto 
      *     stampando tutti i mvimenti ma non esiste esercizio precedente
      *     QUESTO CASO SI VERIFICA SOLO NELLA STAMPA CONFRONTO
      ah_msg("Verifica effettuazione apertura conti",.t.,.t.)
      if this.oParentObject.w_MASABI="S"
        * --- Insert into TMPMOVIMAST
        i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_apertura1",this.TMPMOVIMAST_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      else
        * --- Insert into TMPMOVIMAST
        i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_apertura",this.TMPMOVIMAST_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      if i_Rows > 0
        this.w_APERTURA = .T.
      endif
      if NOT this.w_APERTURA 
        * --- 2- SE NON E' STATA FATTA L'APERTURA DEI CONTI, LEGGIAMO I SALDI DAI MOVIMENTI DELL'ESERCIZIO PRECEDENDE
        *     DALLA LETTURA ESCLUDIAMO I MOVIMENTI DI CHIUSURA
        if NOT EMPTY(NVL(this.oParentObject.w_ESEPRE,""))
          ah_msg("Lettura movimenti dell'esercizio precedente",.t.,.t.)
          if this.oParentObject.w_MASABI="S"
            if this.oParentObject.w_VALAPP=this.oParentObject.w_PRVALNAZ
              * --- Insert into TMPMOVIMAST
              i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_saldo2",this.TMPMOVIMAST_idx)
              else
                error "not yet implemented!"
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            else
              * --- Insert into TMPMOVIMAST
              i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_saldo3",this.TMPMOVIMAST_idx)
              else
                error "not yet implemented!"
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
          else
            if this.oParentObject.w_VALAPP=this.oParentObject.w_PRVALNAZ
              * --- Insert into TMPMOVIMAST
              i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_saldo",this.TMPMOVIMAST_idx)
              else
                error "not yet implemented!"
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            else
              * --- Insert into TMPMOVIMAST
              i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_saldo1",this.TMPMOVIMAST_idx)
              else
                error "not yet implemented!"
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
          endif
        endif
        if i_Rows=0
          * --- non ci sono movimenti nell'esercizio precedente per cui provo a leggere il movimento di apertura
          ah_msg("Verifica effettuazione apertura conti",.t.,.t.)
          if this.oParentObject.w_MASABI="S"
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_apertura1",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_apertura",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
        if EMPTY(this.oParentObject.w_CODBUN) AND EMPTY(this.oParentObject.w_SUPERBU)
          * --- LEGGIAMO ANCHE I MOVIMENTI FUORI LINEA CHE DEVONO POI ESSERE SOMMATI.
          ah_msg("Lettura saldi fuori linea",.t.,.t.)
          if this.oParentObject.w_VALAPP=this.oParentObject.w_PRVALNAZ
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_movfuori",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          else
            * --- Insert into TMPMOVIMAST
            i_nConn=i_TableProp[this.TMPMOVIMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIMAST_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_movfuori1",this.TMPMOVIMAST_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
        if EMPTY(this.oParentObject.w_CODBUN) AND EMPTY(this.oParentObject.w_SUPERBU)
          * --- Select from B_RISESEPRE
          do vq_exec with 'B_RISESEPRE',this,'_Curs_B_RISESEPRE','',.f.,.t.
          if used('_Curs_B_RISESEPRE')
            select _Curs_B_RISESEPRE
            locate for 1=1
            do while not(eof())
            if this.oParentObject.w_VALAPP=this.oParentObject.w_PRVALNAZ
              L_RISULPRE = mon2val (nvl(RISESEPRE,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI)
            else
              L_RISULPRE = VAL2CAM(nvl(RISESEPRE,0),this.oParentObject.w_PRVALNAZ,this.oParentObject.w_DIVISA,L_MOLTI,i_DATSYS,L_DECIMI)
            endif
              select _Curs_B_RISESEPRE
              continue
            enddo
            use
          endif
        endif
      endif
    endif
    * --- Select from B_CHIUSURA
    do vq_exec with 'B_CHIUSURA',this,'_Curs_B_CHIUSURA','',.f.,.t.
    if used('_Curs_B_CHIUSURA')
      select _Curs_B_CHIUSURA
      locate for 1=1
      do while not(eof())
      this.w_CHIUSURA = IIF(NVL(NUMERO,0)>0,.T.,.F.)
        select _Curs_B_CHIUSURA
        continue
      enddo
      use
    endif
    * --- Segnalo alla stampa se � stata fatta apertura e quindi di conseguenza chiusura nell'esercizio precedente
    L_CHIUSURA = this.w_APERTURA
    ah_msg("Costruisco la struttura del bilancio",.t.,.t.)
    * --- Select from b_pianomastri3
    do vq_exec with 'b_pianomastri3',this,'_Curs_b_pianomastri3','',.f.,.t.
    if used('_Curs_b_pianomastri3')
      select _Curs_b_pianomastri3
      locate for 1=1
      do while not(eof())
      this.w_MAXUTDV = IIF(_Curs_b_pianomastri3.UTDV>this.w_MAXUTDV,_Curs_b_pianomastri3.UTDV,this.w_MAXUTDV)
        select _Curs_b_pianomastri3
        continue
      enddo
      use
    endif
    if this.oParentObject.w_CALCSTR="S"
      this.Pag11()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      VQ_EXEC("b_pianomastri4",this,"Cursb_pianomastri4")
      if USED("Cursb_pianomastri4")
        SELECT Cursb_pianomastri4 
 GO TOP 
 SCAN
        if NVL(UTDV,cp_CharToDate("  -  -    "))<this.w_MAXUTDV
          this.Pag11()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        ENDSCAN 
 SELECT Cursb_pianomastri4 
 USE
      else
        this.Pag11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    ah_msg("Genero dati di stampa bilancio",.t.,.t.)
    if this.oParentObject.w_TUTTI="S"
      * --- per estrarre i conti movimentati prendo le tabelle in inner
      if this.oParentObject.w_TIPSTA = "T" OR this.oParentObject.w_TIPSTA = "M"
        * --- Create temporary table TMPPNT_MAST
        i_nIdx=cp_AddTableDef('TMPPNT_MAST') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_TZISXFWTTS[1]
        indexes_TZISXFWTTS[1]='CODICE,TIPCON'
        vq_exec('query\b_bilancio5A',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_TZISXFWTTS,.f.)
        this.TMPPNT_MAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        * --- Create temporary table TMPPNT_MAST
        i_nIdx=cp_AddTableDef('TMPPNT_MAST') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_WLSNQODDJZ[1]
        indexes_WLSNQODDJZ[1]='CODICE,TIPCON'
        vq_exec('query\b_bilancio5',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_WLSNQODDJZ,.f.)
        this.TMPPNT_MAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
    else
      * --- per estrarre tutto prendo la query in left
      if this.oParentObject.w_TIPSTA = "T" OR this.oParentObject.w_TIPSTA = "M"
        * --- Create temporary table TMPPNT_MAST
        i_nIdx=cp_AddTableDef('TMPPNT_MAST') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_MXSMIYXWRL[1]
        indexes_MXSMIYXWRL[1]='CODICE,TIPCON'
        vq_exec('query\b_bilancio4A',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_MXSMIYXWRL,.f.)
        this.TMPPNT_MAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      else
        * --- Create temporary table TMPPNT_MAST
        i_nIdx=cp_AddTableDef('TMPPNT_MAST') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        declare indexes_FWZQOOHKJC[1]
        indexes_FWZQOOHKJC[1]='CODICE,TIPCON'
        vq_exec('query\b_bilancio4',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_FWZQOOHKJC,.f.)
        this.TMPPNT_MAST_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      endif
      * --- Aggiungo eventuali Conti (Bilancio Verifica) o Clienti/Fornitori
      *     se non movimentati e non attivo flag Saldi a zero / Non movimentati
      * --- Insert into TMPPNT_MAST
      i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"Query\b_Esclusi",this.TMPPNT_MAST_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Delete from TMPPNT_MAST
      i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"TIPCON is null ";
              +" and CODICE is null ";
               )
      else
        delete from (i_cTable) where;
              TIPCON is null ;
              and CODICE is null ;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from TMPPNT_MAST
      i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
              +" and "+i_cTable+".CODICE = "+i_cQueryTable+".CODICE";
              +" and "+i_cTable+".PRIMO = "+i_cQueryTable+".PRIMO";
      
        do vq_exec with 'query\b_bilanc10',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Drop temporary table TMPMOVIMAST
    i_nIdx=cp_GetTableDefIdx('TMPMOVIMAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMOVIMAST')
    endif
    if this.oParentObject.w_SEZVAR="S" 
      * --- Write into TMPPNT_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="TIPCON,CODICE,PRIMO"
        do vq_exec with 'b_sezvar',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_MAST_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPPNT_MAST.TIPCON = _t2.TIPCON";
                +" and "+"TMPPNT_MAST.CODICE = _t2.CODICE";
                +" and "+"TMPPNT_MAST.PRIMO = _t2.PRIMO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SEZBIL = _t2.SEZBIL ";
            +",SEZORD = _t2.SEZORD ";
            +i_ccchkf;
            +" from "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPPNT_MAST.TIPCON = _t2.TIPCON";
                +" and "+"TMPPNT_MAST.CODICE = _t2.CODICE";
                +" and "+"TMPPNT_MAST.PRIMO = _t2.PRIMO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 set ";
            +"TMPPNT_MAST.SEZBIL = _t2.SEZBIL ";
            +",TMPPNT_MAST.SEZORD = _t2.SEZORD ";
            +Iif(Empty(i_ccchkf),"",",TMPPNT_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPPNT_MAST.TIPCON = t2.TIPCON";
                +" and "+"TMPPNT_MAST.CODICE = t2.CODICE";
                +" and "+"TMPPNT_MAST.PRIMO = t2.PRIMO";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set (";
            +"SEZBIL,";
            +"SEZORD";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.SEZBIL ,";
            +"t2.SEZORD ";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPPNT_MAST.TIPCON = _t2.TIPCON";
                +" and "+"TMPPNT_MAST.CODICE = _t2.CODICE";
                +" and "+"TMPPNT_MAST.PRIMO = _t2.PRIMO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set ";
            +"SEZBIL = _t2.SEZBIL ";
            +",SEZORD = _t2.SEZORD ";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
                +" and "+i_cTable+".CODICE = "+i_cQueryTable+".CODICE";
                +" and "+i_cTable+".PRIMO = "+i_cQueryTable+".PRIMO";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SEZBIL = (select SEZBIL  from "+i_cQueryTable+" where "+i_cWhere+")";
            +",SEZORD = (select SEZORD  from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      if this.oParentObject.w_SEZVAR1="S" 
        * --- Write into TMPPNT_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="TIPCON,CODICE,PRIMO"
          do vq_exec with 'b_sezvar1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPNT_MAST_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="TMPPNT_MAST.TIPCON = _t2.TIPCON";
                  +" and "+"TMPPNT_MAST.CODICE = _t2.CODICE";
                  +" and "+"TMPPNT_MAST.PRIMO = _t2.PRIMO";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SEZBIL = _t2.SEZBIL ";
              +",SEZORD = _t2.SEZORD ";
              +i_ccchkf;
              +" from "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="TMPPNT_MAST.TIPCON = _t2.TIPCON";
                  +" and "+"TMPPNT_MAST.CODICE = _t2.CODICE";
                  +" and "+"TMPPNT_MAST.PRIMO = _t2.PRIMO";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST, "+i_cQueryTable+" _t2 set ";
              +"TMPPNT_MAST.SEZBIL = _t2.SEZBIL ";
              +",TMPPNT_MAST.SEZORD = _t2.SEZORD ";
              +Iif(Empty(i_ccchkf),"",",TMPPNT_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="TMPPNT_MAST.TIPCON = t2.TIPCON";
                  +" and "+"TMPPNT_MAST.CODICE = t2.CODICE";
                  +" and "+"TMPPNT_MAST.PRIMO = t2.PRIMO";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set (";
              +"SEZBIL,";
              +"SEZORD";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.SEZBIL ,";
              +"t2.SEZORD ";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="TMPPNT_MAST.TIPCON = _t2.TIPCON";
                  +" and "+"TMPPNT_MAST.CODICE = _t2.CODICE";
                  +" and "+"TMPPNT_MAST.PRIMO = _t2.PRIMO";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPPNT_MAST set ";
              +"SEZBIL = _t2.SEZBIL ";
              +",SEZORD = _t2.SEZORD ";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
                  +" and "+i_cTable+".CODICE = "+i_cQueryTable+".CODICE";
                  +" and "+i_cTable+".PRIMO = "+i_cQueryTable+".PRIMO";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SEZBIL = (select SEZBIL  from "+i_cQueryTable+" where "+i_cWhere+")";
              +",SEZORD = (select SEZORD  from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    if this.oParentObject.w_TIPSTA<>"C" OR this.oParentObject.w_TIPSTA<>"F"
      * --- Select from B_TOTALI
      do vq_exec with 'B_TOTALI',this,'_Curs_B_TOTALI','',.f.,.t.
      if used('_Curs_B_TOTALI')
        select _Curs_B_TOTALI
        locate for 1=1
        do while not(eof())
        L_ATTIVITA = mon2val(nvl(ATTIVITA,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI) 
 L_PASSIVI = mon2val(nvl(PASSIVITA,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI) 
 L_COSTI = mon2val(nvl(COSTI,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI) 
 L_RICAVI = mon2val(nvl(RICAVI,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI) 
 L_ORDINE = mon2val(nvl(ORDINE,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI) 
 L_TRANSI = mon2val(nvl(TRANSI,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI) 
 L_PATTIVITA = mon2val(nvl(PATTIVITA,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI) 
 L_PPASSIVI = mon2val(nvl(PPASSIVITA,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI) 
 L_PCOSTI = mon2val(nvl(PCOSTI,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI) 
 L_PRICAVI = mon2val(nvl(PRICAVI,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI) 
 L_PORDINE = mon2val(nvl(PORDINE,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI) 
 L_PTRANSI = mon2val(nvl(PTRANSI,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI) 
 L_RISULTATO = mon2val(nvl(RISULTATO,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI) 
          select _Curs_B_TOTALI
          continue
        enddo
        use
      endif
      L_TIPSTA= this.oParentObject.w_TIPSTA 
 L_SEZVAR= this.oParentObject.w_SEZVAR
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.oParentObject.w_TUTTI_Z="S"
      * --- Cancello i conti la cui somma tra
      *     IMPORTO, MOVIPRIMA, MOVIFUORI, APERTURA E SALDO
      *     � uguale a zero
      * --- Delete from TMPPNT_MAST
      i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        local i_cQueryTable,i_cWhere
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_cWhere=i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
              +" and "+i_cTable+".CODICE = "+i_cQueryTable+".CODICE";
              +" and "+i_cTable+".PRIMO = "+i_cQueryTable+".PRIMO";
      
        do vq_exec with 'query\b_bilancio8',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
              +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      if NOT (this.oParentObject.w_TIPSTA="C" OR this.oParentObject.w_TIPSTA="F")
        * --- Nel caso di stampa no clienti fornitori
        * --- Delete from TMPPNT_MAST
        i_nConn=i_TableProp[this.TMPPNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPNT_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          local i_cQueryTable,i_cWhere
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_cWhere=i_cTable+".TIPCON = "+i_cQueryTable+".TIPCON";
                +" and "+i_cTable+".PRIMO = "+i_cQueryTable+".PRIMO";
        
          do vq_exec with 'query\b_bilancio9',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
    endif
    * --- Lancio la stampa a sezioni unite o contrapposte
    ah_msg("Organizzo dati di stampa",.t.,.t.)
    VQ_EXEC("query\b_stampa",this,"__TMP__")
    Select __TMP__ 
 GO TOP
    Scan
    this.w_OTTAVO = SUBSTR(NVL(OTTAVO,"")+NVL(SETTIMO,"")+NVL(SESTO,"")+NVL(QUINTO,"")+NVL(QUARTO,"")+NVL(TERZO,"")+NVL(SECONDO,"")+NVL(PRIMO,""),1,15)
    this.w_DESCRI8 = SUBSTR(NVL(DESCRI8,"")+NVL(DESCRI7,"")+NVL(DESCRI6,"")+NVL(DESCRI5,"")+NVL(DESCRI4,"")+NVL(DESCRI3,"")+NVL(DESCRI2,"")+NVL(DESCRI1,""),1,40)
    this.w_STA8 = VAL(SUBSTR(NVL(STR(STA8,3),"")+NVL(STR(STA7,3),"")+NVL(STR(STA6,3),"")+NVL(STR(STA5,3),"")+NVL(STR(STA4,3),"")+NVL(STR(STA3,3),"")+NVL(STR(STA2,3),"")+NVL(STR(STA1,3),""),1,3))
    this.w_SETTIMO = SUBSTR(NVL(OTTAVO,"")+NVL(SETTIMO,"")+NVL(SESTO,"")+NVL(QUINTO,"")+NVL(QUARTO,"")+NVL(TERZO,"")+NVL(SECONDO,"")+NVL(PRIMO,""),16,15)
    this.w_DESCRI7 = SUBSTR(NVL(DESCRI8,"")+NVL(DESCRI7,"")+NVL(DESCRI6,"")+NVL(DESCRI5,"")+NVL(DESCRI4,"")+NVL(DESCRI3,"")+NVL(DESCRI2,"")+NVL(DESCRI1,""),41,40)
    this.w_STA7 = VAL(SUBSTR(NVL(STR(STA8,3),"")+NVL(STR(STA7,3),"")+NVL(STR(STA6,3),"")+NVL(STR(STA5,3),"")+NVL(STR(STA4,3),"")+NVL(STR(STA3,3),"")+NVL(STR(STA2,3),"")+NVL(STR(STA1,3),""),4,3))
    this.w_SESTO = SUBSTR(NVL(OTTAVO,"")+NVL(SETTIMO,"")+NVL(SESTO,"")+NVL(QUINTO,"")+NVL(QUARTO,"")+NVL(TERZO,"")+NVL(SECONDO,"")+NVL(PRIMO,""),31,15)
    this.w_DESCRI6 = SUBSTR(NVL(DESCRI8,"")+NVL(DESCRI7,"")+NVL(DESCRI6,"")+NVL(DESCRI5,"")+NVL(DESCRI4,"")+NVL(DESCRI3,"")+NVL(DESCRI2,"")+NVL(DESCRI1,""),81,40)
    this.w_STA6 = VAL(SUBSTR(NVL(STR(STA8,3),"")+NVL(STR(STA7,3),"")+NVL(STR(STA6,3),"")+NVL(STR(STA5,3),"")+NVL(STR(STA4,3),"")+NVL(STR(STA3,3),"")+NVL(STR(STA2,3),"")+NVL(STR(STA1,3),""),7,3))
    this.w_QUINTO = SUBSTR(NVL(OTTAVO,"")+NVL(SETTIMO,"")+NVL(SESTO,"")+NVL(QUINTO,"")+NVL(QUARTO,"")+NVL(TERZO,"")+NVL(SECONDO,"")+NVL(PRIMO,""),46,15)
    this.w_DESCRI5 = SUBSTR(NVL(DESCRI8,"")+NVL(DESCRI7,"")+NVL(DESCRI6,"")+NVL(DESCRI5,"")+NVL(DESCRI4,"")+NVL(DESCRI3,"")+NVL(DESCRI2,"")+NVL(DESCRI1,""),121,40)
    this.w_STA5 = VAL(SUBSTR(NVL(STR(STA8,3),"")+NVL(STR(STA7,3),"")+NVL(STR(STA6,3),"")+NVL(STR(STA5,3),"")+NVL(STR(STA4,3),"")+NVL(STR(STA3,3),"")+NVL(STR(STA2,3),"")+NVL(STR(STA1,3),""),10,3))
    this.w_QUARTO = SUBSTR(NVL(OTTAVO,"")+NVL(SETTIMO,"")+NVL(SESTO,"")+NVL(QUINTO,"")+NVL(QUARTO,"")+NVL(TERZO,"")+NVL(SECONDO,"")+NVL(PRIMO,""),61,15)
    this.w_DESCRI4 = SUBSTR(NVL(DESCRI8,"")+NVL(DESCRI7,"")+NVL(DESCRI6,"")+NVL(DESCRI5,"")+NVL(DESCRI4,"")+NVL(DESCRI3,"")+NVL(DESCRI2,"")+NVL(DESCRI1,""),161,40)
    this.w_STA4 = VAL(SUBSTR(NVL(STR(STA8,3),"")+NVL(STR(STA7,3),"")+NVL(STR(STA6,3),"")+NVL(STR(STA5,3),"")+NVL(STR(STA4,3),"")+NVL(STR(STA3,3),"")+NVL(STR(STA2,3),"")+NVL(STR(STA1,3),""),13,3))
    this.w_TERZO = SUBSTR(NVL(OTTAVO,"")+NVL(SETTIMO,"")+NVL(SESTO,"")+NVL(QUINTO,"")+NVL(QUARTO,"")+NVL(TERZO,"")+NVL(SECONDO,"")+NVL(PRIMO,""),76,15)
    this.w_DESCRI3 = SUBSTR(NVL(DESCRI8,"")+NVL(DESCRI7,"")+NVL(DESCRI6,"")+NVL(DESCRI5,"")+NVL(DESCRI4,"")+NVL(DESCRI3,"")+NVL(DESCRI2,"")+NVL(DESCRI1,""),201,40)
    this.w_STA3 = VAL(SUBSTR(NVL(STR(STA8,3),"")+NVL(STR(STA7,3),"")+NVL(STR(STA6,3),"")+NVL(STR(STA5,3),"")+NVL(STR(STA4,3),"")+NVL(STR(STA3,3),"")+NVL(STR(STA2,3),"")+NVL(STR(STA1,3),""),16,3))
    this.w_SECONDO = SUBSTR(NVL(OTTAVO,"")+NVL(SETTIMO,"")+NVL(SESTO,"")+NVL(QUINTO,"")+NVL(QUARTO,"")+NVL(TERZO,"")+NVL(SECONDO,"")+NVL(PRIMO,""),91,15)
    this.w_DESCRI2 = SUBSTR(NVL(DESCRI8,"")+NVL(DESCRI7,"")+NVL(DESCRI6,"")+NVL(DESCRI5,"")+NVL(DESCRI4,"")+NVL(DESCRI3,"")+NVL(DESCRI2,"")+NVL(DESCRI1,""),241,40)
    this.w_STA2 = VAL(SUBSTR(NVL(STR(STA8,3),"")+NVL(STR(STA7,3),"")+NVL(STR(STA6,3),"")+NVL(STR(STA5,3),"")+NVL(STR(STA4,3),"")+NVL(STR(STA3,3),"")+NVL(STR(STA2,3)+NVL(STR(STA1,3),""),""),19,3))
    this.w_PRIMO = SUBSTR(NVL(OTTAVO,"")+NVL(SETTIMO,"")+NVL(SESTO,"")+NVL(QUINTO,"")+NVL(QUARTO,"")+NVL(TERZO,"")+NVL(SECONDO,"")+NVL(PRIMO,""),106,15)
    this.w_DESCRI1 = SUBSTR(NVL(DESCRI8,"")+NVL(DESCRI7,"")+NVL(DESCRI6,"")+NVL(DESCRI5,"")+NVL(DESCRI4,"")+NVL(DESCRI3,"")+NVL(DESCRI2,"")+NVL(DESCRI1,""),281,40)
    this.w_STA1 = VAL(SUBSTR(NVL(STR(STA8,3),"")+NVL(STR(STA7,3),"")+NVL(STR(STA6,3),"")+NVL(STR(STA5,3),"")+NVL(STR(STA4,3),"")+NVL(STR(STA3,3),"")+NVL(STR(STA2,3),"")+NVL(STR(STA1,3),""),22,3))
    Replace PRIMO With this.w_PRIMO 
 Replace SECONDO With this.w_SECONDO 
 Replace TERZO With this.w_TERZO 
 Replace QUARTO With this.w_QUARTO 
 Replace QUINTO With this.w_QUINTO 
 Replace SESTO With this.w_SESTO 
 Replace SETTIMO With this.w_SETTIMO 
 Replace OTTAVO With this.w_OTTAVO 
 Replace DESCRI1 With this.w_DESCRI1 
 Replace DESCRI2 With this.w_DESCRI2 
 Replace DESCRI3 With this.w_DESCRI3 
 Replace DESCRI4 With this.w_DESCRI4 
 Replace DESCRI5 With this.w_DESCRI5 
 Replace DESCRI6 With this.w_DESCRI6 
 Replace DESCRI7 With this.w_DESCRI7 
 Replace DESCRI8 With this.w_DESCRI8 
 Replace STA1 With this.w_STA1 
 Replace STA2 With this.w_STA2 
 Replace STA3 With this.w_STA3 
 Replace STA4 With this.w_STA4 
 Replace STA5 With this.w_STA5 
 Replace STA6 With this.w_STA6 
 Replace STA7 With this.w_STA7 
 Replace STA8 With this.w_STA8
    EndScan
    if this.oParentObject.w_TIPORD="C"
      Select * From __TMP__ ; 
 order by SEZORD,STA8 DESC,OTTAVO,STA7 DESC,SETTIMO,STA6 DESC,SESTO,STA5 DESC,QUINTO, ; 
 STA4 DESC,QUARTO,STA3 DESC,TERZO,STA2 DESC,SECONDO,STA1 DESC,PRIMO,CODICE ; 
 into cursor __TMP__
    else
      Select * From __TMP__ ; 
 order by SEZORD,STA8 DESC,OTTAVO,STA7 DESC,SETTIMO,STA6 DESC,SESTO,STA5 DESC,QUINTO, ; 
 STA4 DESC,QUARTO,STA3 DESC,TERZO,STA2 DESC,SECONDO,STA1 DESC,PRIMO,DESCRI,CODICE ; 
 into cursor __TMP__
    endif
    if this.oParentObject.w_MODSTA="C"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Azzera variabili CPZ
    if g_IZCP$"SA"
      g_ZCPTINTEST = " "
      g_ZCPCINTEST = 0
      g_ZCPALLENAME = " "
      g_ZCPALLETITLE = " "
    endif
    * --- Drop temporary table TMPPNT_MAST
    i_nIdx=cp_GetTableDefIdx('TMPPNT_MAST')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPPNT_MAST')
    endif
    * --- Drop temporary table TMPMOVIDETT
    i_nIdx=cp_GetTableDefIdx('TMPMOVIDETT')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMOVIDETT')
    endif
    if used("TRA")
      select TRA
      use
    endif
    if used("ORD")
      select ORD
      use
    endif
    if used("RIC")
      select RIC
      use
    endif
    if used("COS")
      select COS
      use
    endif
    if used("PAS")
      select PAS
      use
    endif
    if used("ATT")
      select ATT
      use
    endif
    if used("__TMP__")
      select __TMP__
      use
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CALCOLO IL TOTALE PER SEZIONI PER VERIFICARE LA MANCATA QUADRATURA.
    *     UTILIZZIAMO LA SEZIONE DI BILANCIO PRESENTE SUI MASTRI
    ah_msg("Verifica quadratura conti",.t.,.t.)
    * --- Controllo se:
    *     - Attivit� - Passivit� = Costi - Ricavi
    *     - Saldo Transitori (Dare - Avere) = zero
    *     - Saldo Ordine (Dare - Avere) = zero
    * --- Controllo se ho settato tutte le sezioni di bilancio sulla maschera.
    * --- Se non ho settato una di queste non effettuo il controllo di mancata quadratura
    if this.oParentObject.w_ATTIVITA+this.oParentObject.w_PASSIVI+this.oParentObject.w_COSTI+this.oParentObject.w_RICAVI<>"APCR"
      this.w_QUADRATU = .F.
      L_QUADRATU=.F.
    else
      this.w_QUADRATU = .T.
      L_QUADRATU=.T.
    endif
    * --- se la valuta dell'esercizio precedente � diversa da quella attuale
    *     e non ho fatto l'apertura
    *     il controllo di quadratura lo faccio solo sull'esercizio attuale
    if this.oParentObject.w_VALAPP=this.oParentObject.w_PRVALNAZ OR (this.oParentObject.w_VALAPP<>this.oParentObject.w_PRVALNAZ AND this.w_APERTURA)
      this.w_QSAL = (L_ATTIVITA+L_PATTIVITA)+(L_PASSIVI+L_PPASSIVI)-IIF(this.w_CHIUSURA,L_RISULTATO,0)+L_RISULPRE
      this.w_QSAL = this.w_QSAL + (L_RICAVI+L_PRICAVI) + (L_COSTI+L_PCOSTI)
    else
      this.w_QSAL = L_ATTIVITA+L_PASSIVI+IIF(this.w_CHIUSURA,-L_RISULTATO,0)
      this.w_QSAL = this.w_QSAL + L_RICAVI + L_COSTI
    endif
    * --- Avvisa se manca quadratura
    L_QUADRATU=(this.w_QSAL<>0 OR (L_ORDINE+L_PORDINE)<>0 OR (L_TRANSI+L_PTRANSI)<>0) AND this.w_QUADRATU
    if (this.w_QSAL<>0 OR (L_ORDINE+L_PORDINE)<>0 OR (L_TRANSI+L_PTRANSI)<>0) AND this.w_QUADRATU
      this.w_QATT = 0
      this.w_QPAS = 0
      this.w_QSAL = 0
      this.w_QCOS = 0
      this.w_QRIC = 0
      this.w_QORD = 0
      this.w_QTRA = 0
      this.w_QPRE = 0
      this.w_QRIS = 0
      this.w_QRISP = 0
      this.w_QATT = cp_ROUND(L_ATTIVITA,L_DECIMI) + cp_ROUND(L_PATTIVITA,L_DECIMI)
      this.w_QPAS = cp_ROUND(L_PASSIVI,L_DECIMI) + cp_ROUND(L_PPASSIVI,L_DECIMI)
      this.w_QCOS = -1 * (cp_ROUND(L_COSTI,L_DECIMI) + cp_ROUND(L_PCOSTI,L_DECIMI))
      this.w_QRIC = -1 * (cp_ROUND(L_RICAVI,L_DECIMI) + cp_ROUND(L_PRICAVI,L_DECIMI))
      this.w_QORD = cp_ROUND(L_ORDINE,L_DECIMI) + cp_ROUND(L_PORDINE,L_DECIMI)
      this.w_QTRA = cp_ROUND(L_TRANSI,L_DECIMI) + cp_ROUND(L_PTRANSI,L_DECIMI)
      this.w_QRIS = IIF(this.w_CHIUSURA,-1*cp_ROUND(L_RISULTATO,L_DECIMI),0)
      this.w_QRISP = IIF(this.w_CHIUSURA,-1*cp_ROUND(L_RISULPRE,L_DECIMI),0)
      this.w_QSAL = cp_ROUND(L_ATTIVITA,L_DECIMI)+cp_ROUND(L_PASSIVI,L_DECIMI)+cp_ROUND(this.w_QRIS,L_DECIMI)+cp_ROUND(L_RICAVI,L_DECIMI)+cp_ROUND(L_COSTI,L_DECIMI)
      this.w_QPRE = cp_ROUND(L_PATTIVITA,L_DECIMI)+cp_ROUND(L_PPASSIVI,L_DECIMI)+cp_ROUND(L_PRICAVI,L_DECIMI)+cp_ROUND(L_PCOSTI,L_DECIMI)+cp_ROUND(this.w_QRISP,L_DECIMI)
      if TYPE("g_SCHEDULER")="C" AND g_SCHEDULER="S"
        this.w_oMESS=createobject("AH_Message")
        this.w_oPART = this.w_oMESS.AddMsgPartNL("%1Manca quadratura")
        this.w_oPART.AddParam(space(10))     
        this.w_oMESS.AddMsgPartNL("-----------------------------------------------------------")     
        this.w_oPART = this.w_oMESS.AddMsgPartNL("Attivit�:%1")
        this.w_oPART.AddParam(chr(32)+TRAN(this.w_QATT," "+v_PV[S]))     
        this.w_oPART = this.w_oMESS.AddMsgPartNL("Passivit�:%1")
        this.w_oPART.AddParam(chr(32)+TRAN(this.w_QPAS," "+v_PV[S]))     
        if this.w_QRIS<>0
          this.w_oPART = this.w_oMESS.AddMsgPartNL("Risultato esercizio:%1")
          this.w_oPART.AddParam(chr(32)+TRAN(this.w_QRIS," "+v_PV[S]))     
        endif
        if this.w_QRISP<>0 AND this.oParentObject.w_VALAPP=this.oParentObject.w_PRVALNAZ
          this.w_oPART = this.w_oMESS.AddMsgPartNL("Risultato esercizio prec.:%1")
          this.w_oPART.AddParam(chr(32)+TRAN(this.w_QRISP," "+v_PV[S]))     
        endif
        this.w_oPART = this.w_oMESS.AddMsgPartNL("Ricavi:%1")
        this.w_oPART.AddParam(chr(32)+TRAN(this.w_QRIC," "+v_PV[S]))     
        this.w_oPART = this.w_oMESS.AddMsgPartNL("Costi:%1")
        this.w_oPART.AddParam(chr(32)+TRAN(this.w_QCOS," "+v_PV[S]))     
        this.w_oMESS.AddMsgPartNL("-----------------------------------------------------------")     
        this.w_oPART = this.w_oMESS.AddMsgPartNL("Differenza esercizio:%1")
        this.w_oPART.AddParam(chr(32)+TRAN(this.w_QSAL," "+v_PV[S]))     
        if this.w_QPRE<>0 AND this.oParentObject.w_VALAPP=this.oParentObject.w_PRVALNAZ
          this.w_oPART = this.w_oMESS.AddMsgPartNL("Differenza esercizio prec.:%1")
          this.w_oPART.AddParam(chr(32)+TRAN(this.w_QPRE," "+v_PV[S]))     
        endif
        if this.w_QORD<>0
          this.w_oPART = this.w_oMESS.AddMsgPartNL("Ordine:%1")
          this.w_oPART.AddParam(chr(32)+TRAN(this.w_QORD," "+v_PV[S]))     
        endif
        if this.w_QTRA<>0
          this.w_oPART = this.w_oMESS.AddMsgPartNL("Transitori:%1")
          this.w_oPART.AddParam(chr(32)+TRAN(this.w_QTRA," "+v_PV[S]))     
        endif
        this.w_oMESS.AH_ErrorMsg()     
      else
        do GSCG_KRE with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      this.w_QUADRATU = .F.
      L_QUADRATU=.F.
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- calcolo valorizzazione VALUTA
    * --- Leggo le valute dell'esercizio in corso e di quello precedente
    *     Tutti i calcoli vengono effettuati nella moneta di conto
    *     Se ho scelto di effettuare la stampa in altra valuta, traduco tutto alla fine prima di passare alla stampa
    ah_msg("LETTURA VARIABILI PER CAMBI",.t.,.t.)
    if this.oParentObject.w_MONETE="c"
      * --- Leggo informazioni della moneta di conto esercizio scelto (DECIMALI)
      * --- Leggo la valuta dell'esercizio
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESVALNAZ"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.oParentObject.w_ESE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESVALNAZ;
          from (i_cTable) where;
              ESCODAZI = i_CODAZI;
              and ESCODESE = this.oParentObject.w_ESE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_DIVISA = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VADECTOT"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_DIVISA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VADECTOT;
          from (i_cTable) where;
              VACODVAL = this.oParentObject.w_DIVISA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_DECIMI = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Dichiarazione Variabili di stampa.
    *     Dichiarazioni variabili per verifica mancata quadratura.
    *     Dichiarazione variabili per mantenere il cambio tra le valute dell'esercizio in corso e quello precedente.
    *     -- Siamo nel 2002, quindi consideriamo comunque solo il caso di triangolazione
    this.w_OBTEST = i_DATSYS
    * --- Inizia esecuzione controlli prima di lanciare la stampa vera e propria.
    *     Controlla essenzialmente che l'intervallo di date sia corretto
    ah_msg("CONTROLLO DATE IMPOSTATE",.t.,.t.)
    if (this.oParentObject.w_DATA1 > this.oParentObject.W_DATA2 AND ( NOT EMPTY( this.oParentObject.w_DATA2 ) OR NOT EMPTY( this.oParentObject.w_DATA1 ) )) 
      ah_errormsg("Intervallo di date errato",48)
      i_retcode = 'stop'
      return
    endif
    * --- Variabile controllo apertura
    this.w_APERTURA = .F.
    * --- Stampa Provvisori
    if g_IZCP$"SA"
      * --- Propone intestatario di tipo GRUPPO
      g_ZCPTINTEST = "G"
      g_ZCPCINTEST = 0
      * --- Determina descrizione
      if this.oParentObject.w_totdat="T"
        if g_PERBUN="T" and Not(Empty(this.oParentObject.w_CODBUN))
          g_ZCPALLENAME = Ah_MsgFormat("BilancioVerifica_%1_%2_%3.PDF",alltrim(this.oParentObject.w_Ese),alltrim(i_CODAZI),alltrim(this.oParentObject.w_CODBUN))
        else
          g_ZCPALLENAME = Ah_Msgformat("BilancioVerifica_%1_%2.PDF",alltrim(this.oParentObject.w_Ese),alltrim(i_CODAZI))
        endif
      else
        if g_PERBUN="T" and Not(Empty(this.oParentObject.w_CODBUN))
          g_ZCPALLENAME = Ah_MsgFormat("BilancioVerifica_%1_%2_%3_%4.PDF",dtoc(this.oParentObject.w_Data1),dtoc(this.oParentObject.w_Data2),alltrim(i_CODAZI),alltrim(this.oParentObject.w_CODBUN))
        else
          g_ZCPALLENAME = Ah_MsgFormat("BilancioVerifica_%1_%2_%3.PDF",dtoc(this.oParentObject.w_Data1),dtoc(this.oParentObject.w_Data2),alltrim(i_CODAZI))
        endif
      endif
      if this.oParentObject.w_totdat="T"
        if g_PERBUN="T" and Not(Empty(this.oParentObject.w_CODBUN))
          g_ZCPALLETITLE = Ah_MsgFormat("Bilancio di verifica es. %1 - Az.: %2  - B.U.: %3",alltrim(this.oParentObject.w_Ese),alltrim(i_CODAZI),alltrim(this.oParentObject.w_CODBUN))
        else
          g_ZCPALLETITLE = Ah_MsgFormat("Bilancio di verifica es. %1 - Az.: %2",alltrim(this.oParentObject.w_Ese),alltrim(i_CODAZI))
        endif
      else
        if g_PERBUN="T" and Not(Empty(this.oParentObject.w_CODBUN))
          g_ZCPALLETITLE = Ah_MsgFormat("Bilancio di verifica dal %1 al %2 - Az.: %3  - B.U.: %4",dtoc(this.oParentObject.w_Data1),dtoc(this.oParentObject.w_Data2),alltrim(i_CODAZI),alltrim(this.oParentObject.w_CODBUN))
        else
          g_ZCPALLETITLE = Ah_MsgFormat("Bilancio di verifica dal %1 al %2 - Az.: %3",dtoc(this.oParentObject.w_Data1),dtoc(this.oParentObject.w_Data2),alltrim(i_CODAZI))
        endif
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- STAMPA SEZIONI CONTRAPPOSTE
    * --- Le pagine dalla 6 (Pagina 6,7,8,9 e 10)
    *     Sono le pagine che prendono il cursore di stampa e lo ridefiniscono
    *     per fare la stampa a sezioni contrapposte.
    *     In caso di errori nei calcoli non sono da modificare.
    this.Pag6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Sostituisco il temporaneo della stampa con quello creato in Pag.6
    SELECT __TMP__
    USE
    SELECT * FROM APPOBIL INTO CURSOR __TMP__
    if USED("APPOBIL")
      SELECT APPOBIL
      USE
    endif
    if RECCOUNT("__TMP__")>0
      CP_CHPRN("QUERY\b_bilancioC.FRX","",this.oParentObject)
    else
      ah_ErrorMsg("Non ci sono dati da stampare",,"")
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- STAMPA SEZIONI UNITE
    * --- LANCIO I REPORT
    if RECCOUNT("__TMP__")>0
      if this.oParentObject.w_EXCEL<>"S"
        if this.oParentObject.w_PERIODO="S"
          do case
            case this.oParentObject.w_TIPSTA="T"
              CP_CHPRN("QUERY\b_bilancio2.FRX","",this.oParentObject)
            case this.oParentObject.w_TIPSTA="M"
              CP_CHPRN("QUERY\b_bilancio6.FRX","",this.oParentObject)
            case this.oParentObject.w_TIPSTA="C" OR this.oParentObject.w_TIPSTA="F"
              CP_CHPRN("QUERY\b_bilancio4.FRX","",this.oParentObject)
          endcase
        else
          * --- Controllo se ho selezionato il flag Tutti i mastri. Nella stampa inserisco tutti i mastri di qualsiasi livello
          do case
            case this.oParentObject.w_TIPSTA="T"
              if this.oParentObject.w_Livelli="S"
                CP_CHPRN("QUERY\b_bilancio11.FRX","",this.oParentObject)
              else
                if this.oParentObject.w_CONPRO="S"
                  CP_CHPRN("QUERY\b_bilancio7.FRX","",this.oParentObject)
                else
                  CP_CHPRN("QUERY\b_bilancio.FRX","",this.oParentObject)
                endif
              endif
            case this.oParentObject.w_TIPSTA="M"
              CP_CHPRN("QUERY\b_bilancio5.FRX","",this.oParentObject)
            case this.oParentObject.w_TIPSTA="C" OR this.oParentObject.w_TIPSTA="F"
              * --- Controllo se ho selezionato il flag Tutti i mastri.
              * --- Nella stampa inserisco tutti i mastri di qualsiasi livello
              CP_CHPRN("QUERY\b_bilancio3.FRX","",this.oParentObject)
          endcase
        endif
      else
        * --- Lancio la Stampa Su Excel
        * --- Costruisco il Cursore con i campi necessari. Il Campo Ordine mi serve per avere un ordinamento
        Select ((mon2val (nvl(dare,0)-nvl(avere,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI)+ ; 
 mon2val (nvl(moviprima,0)+nvl(apertura,0)+nvl(saldo,0)+nvl(movfuori,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI) ) * IIF(SezBil $ "PR" ,-1,1 )) As Importo , ; 
 TIPCON AS TipoConto, Codice , Descri , SezBil, ; 
 IIF(not empty(primo),primo,IIF(not empty(secondo),secondo,IIF(not empty(terzo),terzo, ; 
 IIF(not empty(quarto),quarto,IIF(not empty(quinto),quinto,IIF(not empty(sesto),sesto, ; 
 IIF(not empty(settimo),settimo,ottavo))))))) as Mastro, ; 
 IIF(not empty(primo),descri1,IIF(not empty(secondo),descri2,IIF(not empty(terzo),descri3, ; 
 IIF(not empty(quarto),descri4,IIF(not empty(quinto),descri5,IIF(not empty(sesto),descri6, ; 
 IIF(not empty(settimo),descri7,ottavo))))))) as McDescri, ; 
 RecNo()+.5-.5 As Ordine, ; 
 mon2val (nvl(moviprima,0)+nvl(apertura,0)+nvl(saldo,0)+nvl(movfuori,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI) * IIF(SezBil $ "PR" ,-1,1 ) As SaldoIn , ; 
 mon2val (nvl(dare,0),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI)+9.9999-9.9999 As Dare , ; 
 mon2val (abs(nvl(avere,0)),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI)+9.9999-9.9999 As Avere, MCFLDETT ; 
 From __TMP__ Order By Ordine Into Cursor __EX3__ NoFilter
        Cur = WrCursor ( "__EX3__" )
        * --- Calcolo i totali per sezione di bilancio TipoConto ='Z'
         
 Select __EX3__.* From __EX3__ ; 
 Union ; 
 Select Sum(Importo) As Importo,; 
 "Z" As TipoConto,"" As Codice,"" As Descri,SezBil,"" As Mastro,; 
 Ah_MsgFormat("Totale") As McDescri , ; 
 Max(Ordine+.6) As Ordine,Sum (SaldoIn) As SaldoIn , Sum (Dare) As Dare, Sum (Avere ) As Avere ,"N" as MCFLDETT; 
 From __EX3__ Group By SezBil ; 
 Union ; 
 Select IIF(SezBil="A",L_ATTIVITA+L_PATTIVITA,IIF(SezBil="P",-L_PASSIVI-L_PPASSIVI,; 
 IIF(SezBil="C",L_COSTI+L_PCOSTI,IIF(SezBil="R",-L_RICAVI-L_PRICAVI,; 
 IIF(SezBil="T",L_TRANSI+L_PTRANSI,L_ORDINE+L_PORDINE)))))-Sum(Importo) As Importo,; 
 "Z" As TipoConto,"" As Codice,"" As Descri,SezBil,"" As Mastro,; 
 Ah_MsgFormat("Differenze di conversione") As McDescri , ; 
 Max(Ordine+.7) As Ordine,0 As SaldoIn , 0 As Dare, 0 As Avere , Min(MCFLDETT) as MCFLDETT ; 
 From __EX3__ Group By SezBil having ; 
 Sum(Importo)-IIF(SezBil="A",L_ATTIVITA+L_PATTIVITA,IIF(SezBil="P",-L_PASSIVI-L_PPASSIVI,; 
 IIF(SezBil="C",L_COSTI+L_PCOSTI,IIF(SezBil="R",-L_RICAVI-L_PRICAVI,; 
 IIF(SezBil="T",L_TRANSI+L_PTRANSI,L_ORDINE+L_PORDINE)))))<>0 ; 
 Union ; 
 Select IIF(SezBil="A",L_ATTIVITA+L_PATTIVITA,IIF(SezBil="P",-L_PASSIVI-L_PPASSIVI,; 
 IIF(SezBil="C",L_COSTI+L_PCOSTI,IIF(SezBil="R",-L_RICAVI-L_PRICAVI,; 
 IIF(SezBil="T",L_TRANSI+L_PTRANSI,L_ORDINE+L_PORDINE))))) As Importo,; 
 "Z" As TipoConto,"" As Codice,"" As Descri,SezBil,"" As Mastro,; 
 IIF(SezBil="A",Ah_MsgFormat("Totale attivit�"),IIF(SezBil="P",Ah_MsgFormat("Totale passivit�"),IIF(SezBil="C",Ah_MsgFormat("Totale costi"),; 
 IIF(SezBil="R",Ah_MsgFormat("Totale ricavi"),IIF(SezBil="T",Ah_MsgFormat("Totale transitori"),Ah_MsgFormat("Totale ordine")))))) As McDescri , ; 
 Max(Ordine+.7) As Ordine,Sum (SaldoIn) As SaldoIn , Sum (Dare) As Dare, Sum (Avere ) As Avere, Min(MCFLDETT) as MCFLDETT ; 
 From __EX3__ Group By SezBil into Cursor __EXC__ NoFilter
        * --- Calcolo la differenza di conversione
        Cur = WrCursor ( "__EXC__" )
        do case
          case this.oParentObject.w_TIPSTA="T"
            * --- Completo - Raggruppo i Clienti e i Fornitori e calcolo i vari totali I totali sono aggiunti in coda come conti di tipo 'T'
             
 Select __EXC__.* From __EXC__ Where MCFLDETT="N" Or TipoConto="Z"; 
 Union ; 
 Select Sum(Importo) As Importo , "T" As TipoConto,"" As Codice,"" As Descri,SezBil,Mastro,; 
 Min(Left ( Ah_MsgFormat("Totale %1",McDescri) , 40 )) As McDescri ,Max(Ordine+.5) As Ordine, ; 
 Sum (SaldoIn) As SaldoIn , Sum (Dare) As Dare, Sum (Avere ) As Avere,MIN(MCFLDETT) AS MCFLDETT ; 
 From __EXC__ Where MCFLDETT="S" And TipoConto<>"Z" Group By Mastro, SezBil into Cursor __EX1__ NoFilter
            * --- Calcolo i totali per ogni mastro (solo per conti di tipo 'G'). A Ordine aggiungo .5 per farlo seguire al mastro
             
 Select __EX1__.* From __EX1__ ; 
 Union ; 
 Select Sum(Importo) As Importo , "T" As TipoConto,"" As Codice,"" As Descri,SezBil,"" As Mastro,; 
 Min(Left ( Ah_MsgFormat("Totale %1",McDescri) , 40 )) As McDescri ,Max(Ordine+.5) As Ordine, ; 
 Sum (SaldoIn) As SaldoIn , Sum (Dare) As Dare, Sum (Avere ) As Avere,MIN(MCFLDETT) AS MCFLDETT ; 
 From __EX1__ Where MCFLDETT="N" AND TipoConto <>"Z" Group By Mastro,SezBil Order By 8 Into Cursor __EX2__ NoFilter
          case this.oParentObject.w_TIPSTA$"C/F"
            * --- Completo - Raggruppo i Clienti e i Fornitori e calcolo i vari totali I totali sono aggiunti in coda come conti di tipo 'T'
             
 Select __EXC__.* From __EXC__ Where TipoConto="C" Or TipoConto="F" Or TipoConto="Z"; 
 Union ; 
 Select Sum(Importo) As Importo , "T" As TipoConto,"" As Codice,"" As Descri,SezBil,Mastro,; 
 Min(Left ( Ah_MsgFormat("Totale %1",McDescri) , 40 )) As McDescri ,Max(Ordine+.5) As Ordine, ; 
 Sum (SaldoIn) As SaldoIn , Sum (Dare) As Dare, Sum (Avere ) As Avere,MIN(MCFLDETT) AS MCFLDETT ; 
 From __EXC__ Where MCFLDETT="S" And TipoConto<>"Z" Group By Mastro, SezBil into Cursor __EX1__ NoFilter
            * --- Calcolo i totali per ogni mastro (solo per conti di tipo 'G'). A Ordine aggiungo .5 per farlo seguire al mastro
             
 Select __EX1__.* From __EX1__ ; 
 Union ; 
 Select Sum(Importo) As Importo , "T" As TipoConto,"" As Codice,"" As Descri,SezBil,"" As Mastro,; 
 Min(Left ( Ah_MsgFormat("Totale %1",McDescri) , 40 )) As McDescri ,Max(Ordine+.5) As Ordine, ; 
 Sum (SaldoIn) As SaldoIn , Sum (Dare) As Dare, Sum (Avere ) As Avere,MIN(MCFLDETT) AS MCFLDETT ; 
 From __EX1__ Where MCFLDETT="N" AND TipoConto <>"Z" Group By Mastro,SezBil Order By 8 Into Cursor __EX2__ NoFilter
          case this.oParentObject.w_TIPSTA="M"
            * --- Stampa solo totali Mastri (raggruppo per TipoConto e Sezbile per preservare i totali per Sezione di Bilancio)
             
 Select Sum(Importo) As Importo , TipoConto,"" As Codice,"" As Descri,SezBil,Mastro,McDescri ,Ordine, ; 
 Sum (SaldoIn) As SaldoIn , Sum (Dare) As Dare, Sum (Avere ) As Avere ; 
 From __EXC__ Where TipoConto<>"Z" Group By Mastro,TipoConto,SezBil ; 
 Union ; 
 Select Importo As Importo , TipoConto,"" As Codice,"" As Descri,SezBil,Mastro,McDescri ,Ordine, ; 
 SaldoIn As SaldoIn , Dare As Dare, Avere As Avere ; 
 From __EXC__ Where TipoConto="Z" ; 
 Order By 8 Into Cursor __EX2__ NoFilter
        endcase
        Cur =WrCursor( "__EX2__")
        * --- Sbianco i Mastri "doppi"
        Select "__EX2__"
        this.w_OLDMASTR = Space(15)
        this.w_OLDSEZBIL = "Z"
        this.w_NEWMASTR = Space(15)
        Scan For Nvl(TipoConto,"")<>"D" And Nvl(TipoConto,"")<>"Z"
        * --- Se non stampo solo i mastri allora sbianco i mastri duplicati e aggiungo i titoli pe le varie sezioni
        if this.oParentObject.w_TIPSTA<>"M"
          this.w_NEWMASTR = __EX2__.MASTRO
          if this.w_OLDMASTR<>this.w_NEWMASTR or __EX2__.SezBil <> this.w_OLDSEZBIL
            this.w_OLDMASTR = __EX2__.MASTRO
          else
            Replace Mastro With ""
            Replace McDescri With ""
          endif
        endif
        if this.oParentObject.w_TIPSTA$"M/T"
          * --- Al Cambio di Sezione di Bilancio aggiungo una riga con la descrizione e Tipo D
          if __EX2__.SezBil <> this.w_OLDSEZBIL
            this.w_POS_REC = Recno()
            this.w_OR_DINE = __EX2__.Ordine
            this.w_OLDSEZBIL = __EX2__.SezBil
            do case
              case this.w_OLDSEZBIL="A"
                this.w_DES_AT = ah_msgformat("Attivit�")
              case this.w_OLDSEZBIL="P"
                this.w_DES_AT = ah_msgformat("Passivit�")
              case this.w_OLDSEZBIL="C"
                this.w_DES_AT = ah_msgformat("Costi")
              case this.w_OLDSEZBIL="R"
                this.w_DES_AT = ah_msgformat("Ricavi")
              case this.w_OLDSEZBIL="O"
                this.w_DES_AT = ah_msgformat("Ordine")
              case this.w_OLDSEZBIL="T"
                this.w_DES_AT = ah_msgformat("Transitori")
            endcase
            Append Blank
            Replace Mastro With this.w_DES_AT
            Replace TipoConto With "D"
            Replace Ordine With this.w_OR_DINE-.3
            Go this.w_POS_REC
          endif
        endif
        EndScan
        Select *,IIF(TIPOCONTO="T" ,SEZBIL, " ") AS FLAG,IIF(TIPOCONTO<>"T" and TIPOCONTO<>"Z" ,SEZBIL, " ") AS FLAG1 from __EX2__ Order By Ordine Into Cursor __Tmp__ NoFilter
        if used("__EXC__")
          select __EXC__
          use
        endif
        if used("__EX1__")
          select __EX1__
          use
        endif
        if used("__EX2__")
          select __EX2__
          use
        endif
        if used("__EX3__")
          select __EX3__
          use
        endif
        if this.oParentObject.w_PERIODO="S"
          * --- Sezione Dare e Avere
          if this.oParentObject.w_TIPSTA="T"
            * --- Completo
            CP_CHPRN("QUERY\EXCELBIL2.XLT","",this.oParentObject)
          else
            * --- Solo mastri
            CP_CHPRN("QUERY\EXCELBIL3.XLT","",this.oParentObject)
          endif
        else
          * --- Solo Importo
          if this.oParentObject.w_TIPSTA="T"
            * --- Completo
            CP_CHPRN("QUERY\EXCELBIL.XLT","",this.oParentObject)
          else
            * --- Solo mastri
            CP_CHPRN("QUERY\EXCELBIL4.XLT","",this.oParentObject)
          endif
        endif
      endif
    else
      ah_ErrorMsg("Non ci sono dati da stampare",,"")
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili Sezione Attivo
    * --- Variabili Sezione Passivo
* --- Area Manuale = Crea_Cur
* --- gscg_bpc
CREATE CURSOR APPOBIL;
(RIGA N(5), SEZIONE C(1), ;
ATCODMAS C(15), ATDESMAS C(40), ATSEZBIL C(1),;
ATCODCON C(15), ATDESCON C(40), ATTIPCON C(1),;
ATVALMAS N(20,2), ATVALCON N(20,2),;
PACODMAS C(15), PADESMAS C(40),PASEZBIL C(1),;
PACODCON C(15), PADESCON C(40), PATIPCON C(1),;
PAVALMAS N(20,2), PAVALCON N(20,2))
* --- Fine Area Manuale
    * --- variabile per scrivere i mastri senza conto 1 volta sola
    * --- variabile che identifica da quele codice mastro � stato derivato quello corrente
    this.contaA = 1
    this.contaP = 1
    Select * From __tmp__ Where SEZBIL="A" And Not Empty(Nvl( CODICE , "" )) Into Cursor ATT NoFilter
    Select * From __tmp__ Where SEZBIL="P" And Not Empty(Nvl( CODICE , "" )) Into Cursor PAS NoFilter
    Select * From __tmp__ Where SEZBIL="C" And Not Empty(Nvl( CODICE , "" )) Into Cursor COS NOFilter
    Select * From __tmp__ Where SEZBIL="R" And Not Empty(Nvl( CODICE , "" )) Into Cursor RIC NoFilter
    Select * From __tmp__ Where SEZBIL="O" And Not Empty(Nvl( CODICE , "" )) Into Cursor ORD NoFilter
    Select * From __tmp__ Where SEZBIL="T" And Not Empty(Nvl( CODICE , "" )) Into Cursor TRA NoFilter
    * --- Scrittura Dati Attivit�
    ah_msg("Scrittura dati attivit�",.t.,.t.)
    this.TOTMAS = 0
    Select ATT
    Go Top
    Scan
    this.Pag10()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.ASEZBIL = nvl(att.sezbil,0)
    this.ATIPCON = nvl(att.tipcon,space(1))
    this.AVALCON = mon2val ((nvl(importo,0)+nvl(moviprima,0)+nvl(apertura,0)+nvl(saldo,0)+nvl(movfuori,0)),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI)
    this.ACODCON = ATT.CODICE
    this.ADESCON = att.DESCRI
    if ATT.MCFLDETT="N"
      Select ATT
      Skip
      CampoTest=This.POSIZ
      if &CampoTest=this.ACODMAS
        * --- Inserisco il conto
        this.ACODMAS = space(15)
        this.ADESMAS = space(15)
        this.AVALMAS = 0
        this.TOTMAS = this.TOTMAS+this.AVALCON
* --- Area Manuale = Insatt
* --- gscg_bpc
INSERT INTO APPOBIL (RIGA,SEZIONE,;
ATCODMAS,ATDESMAS,ATSEZBIL,;
ATCODCON,ATDESCON,ATTIPCON,ATVALMAS,;
ATVALCON) ;
VALUES ;
(THIS.CONTAA,"1",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,;
THIS.AVALMAS,THIS.AVALCON)
* --- Fine Area Manuale
      else
        * --- Inserisco il Conto
        this.appocod = this.ACODMAS
        this.appodes = this.ADESMAS
        this.ACODMAS = space(15)
        this.ADESMAS = space(15)
        this.AVALMAS = 0
        this.TOTMAS = this.TOTMAS+this.AVALCON
* --- Area Manuale = Insatt
* --- gscg_bpc
INSERT INTO APPOBIL (RIGA,SEZIONE,;
ATCODMAS,ATDESMAS,ATSEZBIL,;
ATCODCON,ATDESCON,ATTIPCON,ATVALMAS,;
ATVALCON) ;
VALUES ;
(THIS.CONTAA,"1",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,;
THIS.AVALMAS,THIS.AVALCON)
* --- Fine Area Manuale
        * --- Inserisco il Nuovo Mastro
        this.ACODMAS = this.appocod
        this.ADESMAS = this.appodes
        this.AVALMAS = this.TOTMAS
        this.ACODCON = space(15)
        this.ADESCON = space(15)
        this.AVALCON = 0
        this.contaA = this.contaA+1
* --- Area Manuale = Insatt
* --- gscg_bpc
INSERT INTO APPOBIL (RIGA,SEZIONE,;
ATCODMAS,ATDESMAS,ATSEZBIL,;
ATCODCON,ATDESCON,ATTIPCON,ATVALMAS,;
ATVALCON) ;
VALUES ;
(THIS.CONTAA,"1",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,;
THIS.AVALMAS,THIS.AVALCON)
* --- Fine Area Manuale
        this.TOTMAS = 0
      endif
      Skip -1
      this.contaA = this.contaA+1
    else
      this.ACODCON = SPACE(15)
      this.ADESCON = SPACE(40)
      this.TOTMAS = this.TOTMAS+this.AVALCON
      Select ATT
      Skip
      CampoTest=this.POSIZ
      if &CampoTest<>this.ACODMAS
        * --- Inserisco il Mastro
        INSERT INTO APPOBIL (RIGA,SEZIONE,;
        ATCODMAS,ATDESMAS,ATSEZBIL,;
        ATCODCON,ATDESCON,ATTIPCON,ATVALMAS) ;
        VALUES ;
        (THIS.CONTAA,"1",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
        THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,this.TOTMAS)
        this.TOTMAS = 0
        this.contaA = this.contaA+1
      endif
      Select ATT
      Skip -1
    endif
    Select ATT
    EndScan
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.CONTAA>this.CONTAP
      this.contaC = this.contaA
      this.contaR = this.contaA
    else
      this.contaC = this.contaP
      this.contaR = this.contaP
    endif
    * --- Scrittura Dati Costi
    ah_msg("Scrittura dati costi",.t.,.t.)
    this.TOTMAS = 0
    Select COS
    Go Top
    Scan
    this.Pag10()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.ASEZBIL = nvl(COS.sezbil,0)
    this.ATIPCON = nvl(COS.tipcon,space(1))
    this.AVALCON = mon2val ((nvl(importo,0)+nvl(moviprima,0)+nvl(apertura,0)+nvl(saldo,0)+nvl(movfuori,0)),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI)
    this.ACODCON = COS.CODICE
    this.ADESCON = COS.DESCRI
    if COS.MCFLDETT="N"
      SELECT COS
      SKIP
      CampoTest=this.POSIZ
      if &CampoTest=this.ACODMAS
        this.ACODMAS = space(15)
        this.ADESMAS = space(15)
        this.AVALMAS = 0
        this.TOTMAS = this.TOTMAS+this.AVALCON
* --- Area Manuale = Inscos
* --- gscg_bpc
INSERT INTO APPOBIL (RIGA,SEZIONE,;
ATCODMAS,ATDESMAS,ATSEZBIL,;
ATCODCON,ATDESCON,ATTIPCON,ATVALMAS,;
ATVALCON) ;
VALUES ;
(THIS.CONTAC,"2",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,;
THIS.AVALMAS,THIS.AVALCON)
* --- Fine Area Manuale
      else
        this.appocod = this.ACODMAS
        this.appodes = this.ADESMAS
        this.ACODMAS = space(15)
        this.ADESMAS = space(15)
        this.AVALMAS = 0
        this.TOTMAS = this.TOTMAS+this.AVALCON
* --- Area Manuale = Inscos
* --- gscg_bpc
INSERT INTO APPOBIL (RIGA,SEZIONE,;
ATCODMAS,ATDESMAS,ATSEZBIL,;
ATCODCON,ATDESCON,ATTIPCON,ATVALMAS,;
ATVALCON) ;
VALUES ;
(THIS.CONTAC,"2",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,;
THIS.AVALMAS,THIS.AVALCON)
* --- Fine Area Manuale
        this.ACODMAS = this.appocod
        this.ADESMAS = this.appodes
        this.AVALMAS = this.TOTMAS
        this.ACODCON = space(15)
        this.ADESCON = space(15)
        this.AVALCON = 0
        this.contaC = this.contaC+1
* --- Area Manuale = Inscos
* --- gscg_bpc
INSERT INTO APPOBIL (RIGA,SEZIONE,;
ATCODMAS,ATDESMAS,ATSEZBIL,;
ATCODCON,ATDESCON,ATTIPCON,ATVALMAS,;
ATVALCON) ;
VALUES ;
(THIS.CONTAC,"2",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,;
THIS.AVALMAS,THIS.AVALCON)
* --- Fine Area Manuale
        this.TOTMAS = 0
      endif
      SKIP-1
      this.contaC = this.contaC+1
    else
      * --- Se sono qui significa che il conto e' vuoto ed e' il secondo
      * --- AREA MANUALE CON REPLACE DEGLI IMPORTI CON +
      this.ACODCON = space(15)
      this.ADESCON = space(15)
      this.TOTMAS = this.TOTMAS+this.AVALCON
      Select COS
      Skip
      CampoTest=this.POSIZ
      if &CampoTest<>this.ACODMAS
        * --- Inserisco il Mastro
        INSERT INTO APPOBIL (RIGA,SEZIONE,;
        ATCODMAS,ATDESMAS,ATSEZBIL,;
        ATCODCON,ATDESCON,ATTIPCON,ATVALMAS) ;
        VALUES ;
        (THIS.CONTAC,"2",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
        THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,this.TOTMAS)
        this.TOTMAS = 0
        this.contaC = this.contaC+1
      endif
      Select COS
      Skip -1
    endif
    SELECT COS
    ENDSCAN
    this.Pag8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag9()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura Dati Passivit�
    ah_msg("Scrittura dati passivit�",.t.,.t.)
    this.TOTMAS = 0
    SELECT APPOBIL
    GO TOP
    select PAS
    go top
    this.contaP = 1
    SCAN
    select PAS
    this.POSIZ = space(1)
    this.Pag10()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.PCODMAS = this.ACODMAS
    this.PDESMAS = this.ADESMAS
    this.PSEZBIL = nvl(pas.sezbil,0)
    this.PTIPCON = nvl(pas.tipcon,space(1))
    this.PVALCON = mon2val ((nvl(importo,0)+nvl(moviprima,0)+nvl(apertura,0)+nvl(saldo,0)+nvl(movfuori,0)),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI)
    this.PCODCON = PAS.CODICE
    this.PDESCON = PAS.DESCRI
    SELECT APPOBIL
    if PAS.MCFLDETT="N"
      LOCATE FOR APPOBIL.RIGA=THIS.CONTAP
      if FOUND()
        SELECT PAS
        SKIP
        CampoTest=This.POSIZ
        if &CampoTest=this.PCODMAS
          this.PCODMAS = space(15)
          this.PDESMAS = space(15)
          this.PVALMAS = 0
          this.TOTMAS = this.TOTMAS+this.PVALCON
          SELECT APPOBIL
* --- Area Manuale = Inspas
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALMAS WITH THIS.PVALMAS,PAVALCON WITH THIS.PVALCON
* --- Fine Area Manuale
        else
          SELECT APPOBIL
          this.appocod = this.PCODMAS
          this.appodes = this.PDESMAS
          this.PCODMAS = space(15)
          this.PDESMAS = space(15)
          this.AVALMAS = 0
          this.TOTMAS = this.TOTMAS+this.PVALCON
* --- Area Manuale = Inspas
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALMAS WITH THIS.PVALMAS,PAVALCON WITH THIS.PVALCON
* --- Fine Area Manuale
          this.contaP = this.contaP+1
          SKIP
          if eof()
            APPEND BLANK
            REPLACE RIGA WITH THIS.CONTAP, SEZIONE WITH "1"
          endif
          this.PCODMAS = this.appocod
          this.PDESMAS = this.appodes
          this.PVALMAS = this.TOTMAS
          this.PCODCON = space(15)
          this.PDESCON = space(40)
          this.PVALCON = 0
* --- Area Manuale = Inspas
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALMAS WITH THIS.PVALMAS,PAVALCON WITH THIS.PVALCON
* --- Fine Area Manuale
          this.TOTMAS = 0
        endif
        SELECT PAS
        SKIP-1
      else
        SELECT APPOBIL
        APPEND BLANK
        REPLACE RIGA WITH THIS.CONTAP, SEZIONE WITH "1"
        SELECT PAS
        SKIP
        CampoTest=This.POSIZ
        if &CampoTest=this.PCODMAS
          SELECT APPOBIL
          this.PCODMAS = space(15)
          this.PDESMAS = space(15)
          this.PVALMAS = 0
          this.TOTMAS = this.TOTMAS+this.PVALCON
* --- Area Manuale = Inspas
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALMAS WITH THIS.PVALMAS,PAVALCON WITH THIS.PVALCON
* --- Fine Area Manuale
        else
          SELECT APPOBIL
          this.appocod = this.PCODMAS
          this.appodes = this.PDESMAS
          this.PCODMAS = space(15)
          this.PDESMAS = space(15)
          this.AVALMAS = 0
          this.TOTMAS = this.TOTMAS+this.PVALCON
* --- Area Manuale = Inspas
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALMAS WITH THIS.PVALMAS,PAVALCON WITH THIS.PVALCON
* --- Fine Area Manuale
          this.contaP = this.contaP+1
          SKIP
          if eof()
            APPEND BLANK
            REPLACE RIGA WITH THIS.CONTAP, SEZIONE WITH "1"
          endif
          this.PCODMAS = this.appocod
          this.PDESMAS = this.appodes
          this.PVALMAS = this.TOTMAS
          this.PCODCON = space(15)
          this.PDESCON = space(40)
          this.PVALCON = 0
* --- Area Manuale = Inspas
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALMAS WITH THIS.PVALMAS,PAVALCON WITH THIS.PVALCON
* --- Fine Area Manuale
          this.TOTMAS = 0
        endif
        SELECT PAS
        SKIP-1
      endif
      this.contaP = this.contaP+1
    else
      * --- Se sono qui significa che il conto e' vuoto ed e' il secondo
      this.PCODCON = SPACE(15)
      this.PDESCON = SPACE(40)
      this.TOTMAS = this.TOTMAS+this.PVALCON
      SELECT PAS
      SKIP
      CampoTest=This.POSIZ
      if &CampoTest<>this.PCODMAS
        this.PVALMAS = this.TOTMAS
        SELECT APPOBIL
        LOCATE FOR APPOBIL.RIGA= this.CONTAP
        if FOUND()
          if PACODMAS=Space(15)
* --- Area Manuale = Inspas
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALMAS WITH THIS.PVALMAS,PAVALCON WITH THIS.PVALCON
* --- Fine Area Manuale
          else
* --- Area Manuale = Replpass
* --- gscg_bpc
REPLACE PAVALMAS WITH (PAVALMAS+THIS.PVALCON)
* --- Fine Area Manuale
          endif
        else
          * --- Inserisco il Mastro
          APPEND BLANK
          REPLACE RIGA WITH THIS.CONTAP, SEZIONE WITH "1"
* --- Area Manuale = Inspas
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALMAS WITH THIS.PVALMAS,PAVALCON WITH THIS.PVALCON
* --- Fine Area Manuale
        endif
        this.TOTMAS = 0
        this.contaP = this.contaP+1
      endif
      SELECT PAS
      SKIP-1
    endif
    SELECT PAS
    ENDSCAN
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura Dati Ricavi
    ah_msg("Scrittura dati ricavi",.t.,.t.)
    this.TOTMAS = 0
    SELECT APPOBIL
    GO TOP
    select RIC
    go top
    SCAN
    this.POSIZ = space(1)
    this.Pag10()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.PCODMAS = this.ACODMAS
    this.PDESMAS = this.ADESMAS
    this.PSEZBIL = nvl(RIC.sezbil,0)
    this.PTIPCON = nvl(RIC.tipcon,space(1))
    this.PVALCON = mon2val ((nvl(importo,0)+nvl(moviprima,0)+nvl(apertura,0)+nvl(saldo,0)+nvl(movfuori,0)),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI)
    this.PCODCON = RIC.CODICE
    this.PDESCON = RIC.DESCRI
    SELECT APPOBIL
    if RIC.MCFLDETT="N"
      LOCATE FOR APPOBIL.RIGA=THIS.CONTAR
      if FOUND()
        SELECT RIC
        SKIP
        CampoTest=This.POSIZ
        if &CampoTest=this.PCODMAS
          this.PCODMAS = space(15)
          this.PDESMAS = space(15)
          this.PVALMAS = 0
          this.TOTMAS = this.TOTMAS+this.PVALCON
          SELECT APPOBIL
* --- Area Manuale = Insric
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALCON WITH THIS.PVALCON,PAVALMAS WITH THIS.PVALMAS
* --- Fine Area Manuale
        else
          SELECT APPOBIL
          this.appocod = this.PCODMAS
          this.appodes = this.PDESMAS
          this.PCODMAS = space(15)
          this.PDESMAS = space(15)
          this.AVALMAS = 0
          this.TOTMAS = this.TOTMAS+this.PVALCON
* --- Area Manuale = Insric
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALCON WITH THIS.PVALCON,PAVALMAS WITH THIS.PVALMAS
* --- Fine Area Manuale
          this.contaR = this.contaR+1
          SKIP
          if eof()
            APPEND BLANK
            REPLACE RIGA WITH THIS.CONTAR, SEZIONE WITH "2"
          endif
          this.PCODMAS = this.appocod
          this.PDESMAS = this.appodes
          this.PVALMAS = this.TOTMAS
          this.PCODCON = space(15)
          this.PDESCON = space(40)
          this.PVALCON = 0
* --- Area Manuale = Insric
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALCON WITH THIS.PVALCON,PAVALMAS WITH THIS.PVALMAS
* --- Fine Area Manuale
          this.TOTMAS = 0
        endif
        SELECT RIC
        SKIP-1
      else
        SELECT APPOBIL
        APPEND BLANK
        REPLACE RIGA WITH THIS.CONTAR, SEZIONE WITH "2"
        SELECT RIC
        SKIP
        CampoTest=this.POSIZ
        if &CampoTest=this.PCODMAS
          SELECT APPOBIL
          this.PCODMAS = space(15)
          this.PDESMAS = space(15)
          this.PVALMAS = 0
          this.TOTMAS = this.TOTMAS+this.PVALCON
* --- Area Manuale = Insric
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALCON WITH THIS.PVALCON,PAVALMAS WITH THIS.PVALMAS
* --- Fine Area Manuale
        else
          SELECT APPOBIL
          this.appocod = this.PCODMAS
          this.appodes = this.PDESMAS
          this.PCODMAS = space(15)
          this.PDESMAS = space(15)
          this.AVALMAS = 0
          this.TOTMAS = this.TOTMAS+this.PVALCON
* --- Area Manuale = Insric
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALCON WITH THIS.PVALCON,PAVALMAS WITH THIS.PVALMAS
* --- Fine Area Manuale
          this.contaR = this.contaR+1
          SKIP
          if eof()
            APPEND BLANK
            REPLACE RIGA WITH THIS.CONTAR, SEZIONE WITH "2"
          endif
          this.PCODMAS = this.appocod
          this.PDESMAS = this.appodes
          this.PVALMAS = this.TOTMAS
          this.PCODCON = space(15)
          this.PDESCON = space(40)
          this.PVALCON = 0
* --- Area Manuale = Insric
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALCON WITH THIS.PVALCON,PAVALMAS WITH THIS.PVALMAS
* --- Fine Area Manuale
          this.TOTMAS = 0
        endif
        SELECT RIC
        SKIP-1
      endif
      this.contaR = this.contaR+1
    else
      * --- Se sono qui significa che il conto e' vuoto ed e' il secondo
      * --- AREA MANUALE CON REPLACE DEGLI IMPORTI CON +
      this.PCODCON = space(15)
      this.PDESCON = space(40)
      this.TOTMAS = this.TOTMAS+this.PVALCON
      SELECT RIC
      SKIP
      CampoTest=This.POSIZ
      if &CampoTest<>this.PCODMAS
        this.PVALMAS = this.TOTMAS
        SELECT APPOBIL
        LOCATE FOR APPOBIL.RIGA= this.CONTAR
        if FOUND()
          if PACODMAS=Space(15)
* --- Area Manuale = Insric
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALCON WITH THIS.PVALCON,PAVALMAS WITH THIS.PVALMAS
* --- Fine Area Manuale
          else
* --- Area Manuale = Replpass
* --- gscg_bpc
REPLACE PAVALMAS WITH (PAVALMAS+THIS.PVALCON)
* --- Fine Area Manuale
          endif
        else
          * --- Inserisco il Mastro
          APPEND BLANK
          REPLACE RIGA WITH THIS.CONTAR, SEZIONE WITH "2"
* --- Area Manuale = Insric
* --- gscg_bpc
REPLACE PACODMAS  WITH  THIS.PCODMAS, PADESMAS  WITH  THIS.PDESMAS,;
PACODCON WITH THIS.PCODCON, PADESCON WITH THIS.PDESCON,;
PASEZBIL WITH THIS.PSEZBIL, PATIPCON WITH THIS.PTIPCON,;
PAVALCON WITH THIS.PVALCON,PAVALMAS WITH THIS.PVALMAS
* --- Fine Area Manuale
        endif
        this.TOTMAS = 0
        this.contaR = this.contaR+1
      endif
      SELECT RIC
      SKIP-1
    endif
    SELECT RIC
    ENDSCAN
  endproc


  procedure Pag9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.CONTAC>this.CONTAR
      this.contaO = this.contaC
    else
      this.contaO = this.contaR
    endif
    * --- Scrittura Dati Ordine
    ah_msg("Scrittura dati ordine",.t.,.t.)
    Select ORD
    Go Top
    Scan
    this.Pag10()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.ASEZBIL = nvl(ORD.sezbil,0)
    this.ATIPCON = nvl(ORD.tipcon,space(1))
    this.AVALCON = mon2val ((nvl(importo,0)+nvl(moviprima,0)+nvl(apertura,0)+nvl(saldo,0)+nvl(movfuori,0)),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI)
    this.ACODCON = ORD.CODICE
    this.ADESCON = ORD.DESCRI
    if ORD.MCFLDETT="N"
      SELECT ORD
      SKIP
      CampoTest=This.POSIZ
      if &CampoTest=this.ACODMAS
        this.ACODMAS = space(15)
        this.ADESMAS = space(15)
        this.AVALMAS = 0
        this.TOTMAS = this.TOTMAS+this.AVALCON
* --- Area Manuale = Insord
* --- gscg_bpc
INSERT INTO APPOBIL (RIGA,SEZIONE,;
ATCODMAS,ATDESMAS,ATSEZBIL,;
ATCODCON,ATDESCON,ATTIPCON,ATVALCON,;
ATVALMAS) ;
VALUES ;
(THIS.CONTAO,"3",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,;
THIS.AVALCON,THIS.AVALMAS)
* --- Fine Area Manuale
      else
        this.appocod = this.ACODMAS
        this.appodes = this.ADESMAS
        this.ACODMAS = space(15)
        this.ADESMAS = space(15)
        this.AVALMAS = 0
        this.TOTMAS = this.TOTMAS+this.AVALCON
* --- Area Manuale = Insord
* --- gscg_bpc
INSERT INTO APPOBIL (RIGA,SEZIONE,;
ATCODMAS,ATDESMAS,ATSEZBIL,;
ATCODCON,ATDESCON,ATTIPCON,ATVALCON,;
ATVALMAS) ;
VALUES ;
(THIS.CONTAO,"3",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,;
THIS.AVALCON,THIS.AVALMAS)
* --- Fine Area Manuale
        this.ACODMAS = this.appocod
        this.ADESMAS = this.appodes
        this.AVALMAS = this.TOTMAS
        this.ACODCON = space(15)
        this.ADESCON = space(15)
        this.AVALCON = 0
        this.contaO = this.contaO+1
* --- Area Manuale = Insord
* --- gscg_bpc
INSERT INTO APPOBIL (RIGA,SEZIONE,;
ATCODMAS,ATDESMAS,ATSEZBIL,;
ATCODCON,ATDESCON,ATTIPCON,ATVALCON,;
ATVALMAS) ;
VALUES ;
(THIS.CONTAO,"3",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,;
THIS.AVALCON,THIS.AVALMAS)
* --- Fine Area Manuale
        this.TOTMAS = 0
      endif
      SKIP-1
      this.contaO = this.contaO+1
    else
      * --- Se sono qui significa che il conto e' vuoto ed e' il secondo
      * --- AREA MANUALE CON REPLACE DEGLI IMPORTI CON +
      this.ACODCON = space(15)
      this.ADESCON = space(15)
      this.TOTMAS = this.TOTMAS+this.AVALCON
      Select ORD
      Skip
      CampoTest=this.POSIZ
      if &CampoTest<>this.ACODMAS
        * --- Inserisco il Mastro
        INSERT INTO APPOBIL (RIGA,SEZIONE,;
        ATCODMAS,ATDESMAS,ATSEZBIL,;
        ATCODCON,ATDESCON,ATTIPCON,ATVALMAS) ;
        VALUES ;
        (THIS.CONTAO,"3",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
        THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,THIS.TOTMAS)
        this.TOTMAS = 0
        this.contaO = this.contaO+1
      endif
      Select ORD
      Skip -1
    endif
    SELECT ORD
    ENDSCAN
    this.contaT = this.contaO+1
    * --- Scrittura Dati Conti Transitori
    ah_msg("Scrittura dati transitori")
    Select TRA
    Go Top
    Scan
    this.Pag10()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.ASEZBIL = nvl(TRA.sezbil,0)
    this.ATIPCON = nvl(TRA.tipcon,space(1))
    this.AVALCON = mon2val ((nvl(importo,0)+nvl(moviprima,0)+nvl(apertura,0)+nvl(saldo,0)+nvl(movfuori,0)),L_MOLTI,Q,i_DATSYS,g_PERVAL,L_DECIMI)
    this.ACODCON = TRA.CODICE
    this.ADESCON = TRA.DESCRI
    if TRA.MCFLDETT="N"
      SELECT TRA
      SKIP
      CampoTest=This.POSIZ
      if &CampoTest=this.ACODMAS
        this.ACODMAS = space(15)
        this.ADESMAS = space(15)
        this.AVALMAS = 0
        this.TOTMAS = this.TOTMAS+this.AVALCON
* --- Area Manuale = Instra
* --- gscg_bpc
INSERT INTO APPOBIL (RIGA,SEZIONE,;
ATCODMAS,ATDESMAS,ATSEZBIL,;
ATCODCON,ATDESCON,ATTIPCON,ATVALCON,;
ATVALMAS) ;
VALUES ;
(THIS.CONTAT,"4",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,;
THIS.AVALCON,THIS.AVALMAS)
* --- Fine Area Manuale
      else
        this.appocod = this.ACODMAS
        this.appodes = this.ADESMAS
        this.ACODMAS = space(15)
        this.ADESMAS = space(15)
        this.AVALMAS = 0
        this.TOTMAS = this.TOTMAS+this.AVALCON
* --- Area Manuale = Instra
* --- gscg_bpc
INSERT INTO APPOBIL (RIGA,SEZIONE,;
ATCODMAS,ATDESMAS,ATSEZBIL,;
ATCODCON,ATDESCON,ATTIPCON,ATVALCON,;
ATVALMAS) ;
VALUES ;
(THIS.CONTAT,"4",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,;
THIS.AVALCON,THIS.AVALMAS)
* --- Fine Area Manuale
        this.ACODMAS = this.appocod
        this.ADESMAS = this.appodes
        this.AVALMAS = this.TOTMAS
        this.ACODCON = space(15)
        this.ADESCON = space(15)
        this.AVALCON = 0
        this.contaT = this.contaT+1
* --- Area Manuale = Instra
* --- gscg_bpc
INSERT INTO APPOBIL (RIGA,SEZIONE,;
ATCODMAS,ATDESMAS,ATSEZBIL,;
ATCODCON,ATDESCON,ATTIPCON,ATVALCON,;
ATVALMAS) ;
VALUES ;
(THIS.CONTAT,"4",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,;
THIS.AVALCON,THIS.AVALMAS)
* --- Fine Area Manuale
        this.TOTMAS = 0
      endif
      SKIP-1
      this.contaT = this.contaT+1
    else
      * --- Se sono qui significa che il conto e' vuoto ed e' il secondo
      * --- AREA MANUALE CON REPLACE DEGLI IMPORTI CON +
      this.ACODCON = space(15)
      this.ADESCON = space(15)
      this.TOTMAS = this.TOTMAS+this.AVALCON
      Select TRA
      Skip
      CampoTest=this.POSIZ
      if &CampoTest<>this.ACODMAS
        * --- Inserisco il Mastro
        INSERT INTO APPOBIL (RIGA,SEZIONE,;
        ATCODMAS,ATDESMAS,ATSEZBIL,;
        ATCODCON,ATDESCON,ATTIPCON,ATVALMAS) ;
        VALUES ;
        (THIS.CONTAT,"4",THIS.ACODMAS,THIS.ADESMAS,THIS.ASEZBIL,;
        THIS.ACODCON,THIS.ADESCON,THIS.ATIPCON,THIS.TOTMAS)
        this.TOTMAS = 0
        this.contaT = this.contaT+1
      endif
      Select TRA
      Skip -1
    endif
    SELECT TRA
    ENDSCAN
  endproc


  procedure Pag10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Case sul Cursore
    do case
      case not empty(nvl(settimo,space(15))) And empty(nvl(sesto,space(15)))
        this.ACODMAS = settimo
        this.ADESMAS = descri7
        this.POSIZ = "settimo"
      case not empty(nvl(sesto,space(15))) And empty(nvl(quinto,space(15)))
        this.ACODMAS = sesto
        this.ADESMAS = descri6
        this.POSIZ = "sesto"
      case not empty(nvl(quinto,space(15))) And empty(nvl(quarto,space(15)))
        this.ACODMAS = quinto
        this.ADESMAS = descri5
        this.POSIZ = "quinto"
      case not empty(nvl(quarto,space(15))) And empty(nvl(terzo,space(15)))
        this.ACODMAS = quarto
        this.ADESMAS = descri4
        this.POSIZ = "quarto"
      case not empty(nvl(terzo,space(15))) And empty(nvl(secondo,space(15)))
        this.ACODMAS = terzo
        this.ADESMAS = descri3
        this.POSIZ = "terzo"
      case not empty(nvl(secondo,space(15))) And empty(nvl(primo,space(15)))
        this.ACODMAS = secondo
        this.ADESMAS = descri2
        this.POSIZ = "secondo"
      case not empty(nvl(primo,space(15)))
        this.ACODMAS = primo
        this.ADESMAS = descri1
        this.POSIZ = "primo"
    endcase
  endproc


  procedure Pag11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Struttura di Bilancio
    * --- Create temporary table TMPMOVIDETT
    i_nIdx=cp_AddTableDef('TMPMOVIDETT') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    declare indexes_TZQRSHQSTM[2]
    indexes_TZQRSHQSTM[1]='ANCODICE,ANTIPCON'
    indexes_TZQRSHQSTM[2]='PRIMO'
    vq_exec('query\b_pianomastri',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_TZQRSHQSTM,.f.)
    this.TMPMOVIDETT_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMPMOVIDETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPMOVIDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVIDETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMOVIDETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"UTDV ="+cp_NullLink(cp_ToStrODBC(this.w_MAXUTDV),'TMPMOVIDETT','UTDV');
          +i_ccchkf ;
             )
    else
      update (i_cTable) set;
          UTDV = this.w_MAXUTDV;
          &i_ccchkf. ;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Delete from PIANOCON
    i_nConn=i_TableProp[this.PIANOCON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PIANOCON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Insert into PIANOCON
    i_nConn=i_TableProp[this.PIANOCON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PIANOCON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\b_pianomastri7",this.PIANOCON_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='*TMPMOVIMAST'
    this.cWorkTables[4]='*TMPMOVIDETT'
    this.cWorkTables[5]='*TMPPNT_MAST'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='MASTRI'
    this.cWorkTables[8]='PIANOCON'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_B_RISESEPRE')
      use in _Curs_B_RISESEPRE
    endif
    if used('_Curs_B_CHIUSURA')
      use in _Curs_B_CHIUSURA
    endif
    if used('_Curs_b_pianomastri3')
      use in _Curs_b_pianomastri3
    endif
    if used('_Curs_B_TOTALI')
      use in _Curs_B_TOTALI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
