* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_brm                                                        *
*              VALORIZZAZIONE EMAIL                                            *
*                                                                              *
*      Author: Zucchetti S.p.A                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_51]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-06-03                                                      *
* Last revis.: 2014-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPERAZ,pTIPCON,pCODCON,pTIPO,pRESULT,pCODSED,pNOVERSEDI
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_brm",oParentObject,m.pOPERAZ,m.pTIPCON,m.pCODCON,m.pTIPO,m.pRESULT,m.pCODSED,m.pNOVERSEDI)
return(i_retval)

define class tgsar_brm as StdBatch
  * --- Local variables
  pOPERAZ = space(3)
  pTIPCON = space(1)
  pCODCON = space(15)
  pTIPO = space(2)
  pRESULT = space(1)
  pCODSED = space(5)
  pNOVERSEDI = .f.
  w_INDMAIL = space(254)
  w_INDMPEC = space(254)
  w_FAXNUM = space(254)
  w_CODSED = space(5)
  w_NUMFAX = space(20)
  w_OLDFAX = space(18)
  * --- WorkFile variables
  CONTI_idx=0
  DES_DIVE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per la determinazione dell'indirizzo di posta elettronica in base a:
    *     - Tipo Conto  -> pTIPCON
    *     - Codice conto -> PCODCON
    *     - Tipologia sede e/o contatto -> pTIPO
    * --- Tipo di operazione richiesta (Email/Fax 'E_M' o Fax 'FAX' o PEC 'PEC') -> pOPERAZ 
    *     Tipo di risultato voluto -> pRESULT
    *      Legenda pRESULT:
    *         'V' -> valorizza le variabili pubbliche i_EMAIL, i_EMAIL_PEC  e i_FAXNO con i valori trovati;
    *         'R' -> ritorna la lista degli indirizzi Email trovati (mail standard o PEC)
    *         'F' ->  ritorna la lista degli indirizzi Email trovati e valorizza la variabile pubblica i_FAXNO con il valore trovato
    *         'X' -> ritorna il numero di fax Email trovato
    *     - Codice della sede -> pCODSED
    *     - Check controllo indirizzo email nelle sedi (.T. attivo, .F. disattivo)
    do case
      case this.pOPERAZ="E_M" OR this.pOPERAZ="PEC"
        * --- Verifico se ho specificato il codice di una sede
        if VARTYPE(this.pCODSED)="C"
          this.w_CODSED = this.pCODSED
        else
          this.w_CODSED = SPACE(5)
        endif
        this.w_INDMAIL = ""
        this.w_INDMPEC = ""
        this.w_FAXNUM = ""
        * --- Ricerco gli indirizzi eMail e i numeri di Fax che soddisfano i requisiti specificati nei parametri
        *                                             --- Diversifico in base al tipo documento ---
        if ! this.pNOVERSEDI 
          if this.pTIPO = "FA"
            * --- In caso di fattura leggo direttamente i dati dalla sede di fatturazione (se presente) 
            if g_APPLICATION = "ad hoc ENTERPRISE"
              * --- Read from DES_DIVE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DES_DIVE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DD_EMAIL,DD_EMPEC,DDTELFAX"+;
                  " from "+i_cTable+" DES_DIVE where ";
                      +"DDTIPCON = "+cp_ToStrODBC(this.pTIPCON);
                      +" and DDCODICE = "+cp_ToStrODBC(this.pCODCON);
                      +" and DDTIPRIF = "+cp_ToStrODBC(this.pTIPO);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DD_EMAIL,DD_EMPEC,DDTELFAX;
                  from (i_cTable) where;
                      DDTIPCON = this.pTIPCON;
                      and DDCODICE = this.pCODCON;
                      and DDTIPRIF = this.pTIPO;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_INDMAIL = NVL(cp_ToDate(_read_.DD_EMAIL),cp_NullValue(_read_.DD_EMAIL))
                this.w_INDMPEC = NVL(cp_ToDate(_read_.DD_EMPEC),cp_NullValue(_read_.DD_EMPEC))
                this.w_FAXNUM = NVL(cp_ToDate(_read_.DDTELFAX),cp_NullValue(_read_.DDTELFAX))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            else
              * --- Read from DES_DIVE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DES_DIVE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DD_EMAIL,DD_EMPEC,DDNUMFAX"+;
                  " from "+i_cTable+" DES_DIVE where ";
                      +"DDTIPCON = "+cp_ToStrODBC(this.pTIPCON);
                      +" and DDCODICE = "+cp_ToStrODBC(this.pCODCON);
                      +" and DDTIPRIF = "+cp_ToStrODBC(this.pTIPO);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DD_EMAIL,DD_EMPEC,DDNUMFAX;
                  from (i_cTable) where;
                      DDTIPCON = this.pTIPCON;
                      and DDCODICE = this.pCODCON;
                      and DDTIPRIF = this.pTIPO;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_INDMAIL = NVL(cp_ToDate(_read_.DD_EMAIL),cp_NullValue(_read_.DD_EMAIL))
                this.w_INDMPEC = NVL(cp_ToDate(_read_.DD_EMPEC),cp_NullValue(_read_.DD_EMPEC))
                this.w_FAXNUM = NVL(cp_ToDate(_read_.DDNUMFAX),cp_NullValue(_read_.DDNUMFAX))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          else
            do case
              case not empty(this.w_CODSED)
                * --- Ricerca per codice sede
                * --- Select from QUERY\GSAR_BRM
                do vq_exec with 'QUERY\GSAR_BRM',this,'_Curs_QUERY_GSAR_BRM','',.f.,.t.
                if used('_Curs_QUERY_GSAR_BRM')
                  select _Curs_QUERY_GSAR_BRM
                  locate for 1=1
                  do while not(eof())
                  this.w_INDMAIL = this.w_INDMAIL + IIF(EMPTY(ALLTRIM(_Curs_QUERY_GSAR_BRM.INDEMAIL)),"",ALLTRIM(_Curs_QUERY_GSAR_BRM.INDEMAIL)+"; ")
                  this.w_INDMPEC = this.w_INDMPEC + IIF(EMPTY(ALLTRIM(_Curs_QUERY_GSAR_BRM.INDEMPEC)),"",ALLTRIM(_Curs_QUERY_GSAR_BRM.INDEMPEC)+"; ")
                  this.w_FAXNUM = this.w_FAXNUM + IIF(EMPTY(ALLTRIM(_Curs_QUERY_GSAR_BRM.NUMTEL)),"",ALLTRIM(_Curs_QUERY_GSAR_BRM.NUMTEL)+"; ")
                    select _Curs_QUERY_GSAR_BRM
                    continue
                  enddo
                  use
                endif
              case this.pTIPO="CO"
                * --- Ricerca ind. email sede di consegna predefinita, se presente.
                * --- Select from QUERY\GSAR3BRM
                do vq_exec with 'QUERY\GSAR3BRM',this,'_Curs_QUERY_GSAR3BRM','',.f.,.t.
                if used('_Curs_QUERY_GSAR3BRM')
                  select _Curs_QUERY_GSAR3BRM
                  locate for 1=1
                  do while not(eof())
                  this.w_INDMAIL = this.w_INDMAIL + IIF(EMPTY(ALLTRIM(_Curs_QUERY_GSAR3BRM.INDEMAIL)),"",ALLTRIM(_Curs_QUERY_GSAR3BRM.INDEMAIL)+"; ")
                  this.w_INDMPEC = this.w_INDMPEC + IIF(EMPTY(ALLTRIM(_Curs_QUERY_GSAR3BRM.INDEMPEC)),"",ALLTRIM(_Curs_QUERY_GSAR3BRM.INDEMPEC)+"; ")
                  this.w_FAXNUM = this.w_FAXNUM + IIF(EMPTY(ALLTRIM(_Curs_QUERY_GSAR3BRM.NUMTEL)),"",ALLTRIM(_Curs_QUERY_GSAR3BRM.NUMTEL)+"; ")
                    select _Curs_QUERY_GSAR3BRM
                    continue
                  enddo
                  use
                endif
            endcase
            if empty(this.w_INDMAIL) and not empty(this.pTIPO)
              * --- Ricerca per tipo sede
              * --- Select from QUERY\GSAR2BRM
              do vq_exec with 'QUERY\GSAR2BRM',this,'_Curs_QUERY_GSAR2BRM','',.f.,.t.
              if used('_Curs_QUERY_GSAR2BRM')
                select _Curs_QUERY_GSAR2BRM
                locate for 1=1
                do while not(eof())
                this.w_INDMAIL = this.w_INDMAIL + IIF(EMPTY(ALLTRIM(_Curs_QUERY_GSAR2BRM.INDEMAIL)),"",ALLTRIM(_Curs_QUERY_GSAR2BRM.INDEMAIL)+"; ")
                this.w_INDMPEC = this.w_INDMPEC + IIF(EMPTY(ALLTRIM(_Curs_QUERY_GSAR2BRM.INDEMPEC)),"",ALLTRIM(_Curs_QUERY_GSAR2BRM.INDEMPEC)+"; ")
                this.w_FAXNUM = this.w_FAXNUM + IIF(EMPTY(ALLTRIM(_Curs_QUERY_GSAR2BRM.NUMTEL)),"",ALLTRIM(_Curs_QUERY_GSAR2BRM.NUMTEL)+"; ")
                  select _Curs_QUERY_GSAR2BRM
                  continue
                enddo
                use
              endif
            endif
          endif
        endif
        * --- Verifico se ho almeno un indirizzo Email
        if empty(this.w_INDMAIL) 
          * --- Leggo l'indirizzo Email inserito nell'anagrafica
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AN_EMAIL"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.pTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.pCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AN_EMAIL;
              from (i_cTable) where;
                  ANTIPCON = this.pTIPCON;
                  and ANCODICE = this.pCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_INDMAIL = NVL(cp_ToDate(_read_.AN_EMAIL),cp_NullValue(_read_.AN_EMAIL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Elimino il ';' finale
          this.w_INDMAIL = SUBSTR(this.w_INDMAIL,1,LEN(this.w_INDMAIL)-2)
        endif
        * --- Verifico se ho almeno un indirizzo Email PEC
        if empty(this.w_INDMPEC) 
          * --- Leggo l'indirizzo PEC inserito nell'anagrafica
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AN_EMPEC"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.pTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.pCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AN_EMPEC;
              from (i_cTable) where;
                  ANTIPCON = this.pTIPCON;
                  and ANCODICE = this.pCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_INDMPEC = NVL(cp_ToDate(_read_.AN_EMPEC),cp_NullValue(_read_.AN_EMPEC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Elimino il ';' finale
          this.w_INDMPEC = SUBSTR(this.w_INDMPEC,1,LEN(this.w_INDMPEC)-2)
        endif
        * --- Verifico se ho almeno un numero di Fax
        if empty(this.w_FAXNUM) 
          * --- Leggo l'indirizzo Email inserito nell'anagrafica
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANTELFAX"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.pTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.pCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANTELFAX;
              from (i_cTable) where;
                  ANTIPCON = this.pTIPCON;
                  and ANCODICE = this.pCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FAXNUM = NVL(cp_ToDate(_read_.ANTELFAX),cp_NullValue(_read_.ANTELFAX))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Elimino il ';' finale
          this.w_FAXNUM = SUBSTR(this.w_FAXNUM,1,LEN(this.w_FAXNUM)-2)
        endif
        * --- Verifico se ho pi� di un numero per il fax, in questo caso sbianco la variabile. Lascaindo cos�
        *     l'incarico di selezione del numero appropriato all'utente.
        if at(";",this.w_FAXNUM) > 0
          this.w_FAXNUM = ""
        endif
        if TYPE("pRESULT") <> "C"
          this.pRESULT="A"
        endif
        * --- Gestisco il risultato in base all'opzione scelta
        do case
          case this.pRESULT = "R"
            i_retcode = 'stop'
            i_retval = IIF(this.pOPERAZ="PEC", this.w_INDMPEC, this.w_INDMAIL)
            return
          case this.pRESULT = "F"
            i_FAXNO = this.w_FAXNUM
            i_retcode = 'stop'
            i_retval = this.w_INDMAIL
            return
          case this.pRESULT = "V"
            i_EMAIL = this.w_INDMAIL
            i_EMAIL_PEC = this.w_INDMPEC
            i_FAXNO = this.w_FAXNUM
          case this.pRESULT = "X"
            i_retcode = 'stop'
            i_retval = this.w_FAXNUM
            return
          otherwise
            ah_ErrorMsg("(Gsar_brm) - non � stato indicato il parametro relativo al tipo di risultato da restituire","!","")
        endcase
      case this.pOPERAZ="FAX"
        if g_APPLICATION = "ad hoc ENTERPRISE"
          this.w_OLDFAX = this.oParentObject.w_FAXA
          vx_exec("query\gsut_kax.vzm",this)
          this.oParentObject.w_FAXA = this.w_NUMFAX
          if EMPTY(NVL(this.w_NUMFAX," " ))
            ah_Msg("Attenzione: la tipologia selezionata non ha un numero di FAX",.T.)
          endif
          * --- Se vuoto ripristino il valore iniziale
          if EMPTY(this.oParentObject.w_FAXA) AND NOT EMPTY(this.w_OLDFAX)
            this.oParentObject.w_FAXA = this.w_OLDFAX
          endif
        else
          * --- Legge prima dalla sede
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPERAZ,pTIPCON,pCODCON,pTIPO,pRESULT,pCODSED,pNOVERSEDI)
    this.pOPERAZ=pOPERAZ
    this.pTIPCON=pTIPCON
    this.pCODCON=pCODCON
    this.pTIPO=pTIPO
    this.pRESULT=pRESULT
    this.pCODSED=pCODSED
    this.pNOVERSEDI=pNOVERSEDI
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='DES_DIVE'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_QUERY_GSAR_BRM')
      use in _Curs_QUERY_GSAR_BRM
    endif
    if used('_Curs_QUERY_GSAR3BRM')
      use in _Curs_QUERY_GSAR3BRM
    endif
    if used('_Curs_QUERY_GSAR2BRM')
      use in _Curs_QUERY_GSAR2BRM
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPERAZ,pTIPCON,pCODCON,pTIPO,pRESULT,pCODSED,pNOVERSEDI"
endproc
