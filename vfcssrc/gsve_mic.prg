* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_mic                                                        *
*              Incasso ricevute fiscali                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_171]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-10                                                      *
* Last revis.: 2012-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_mic"))

* --- Class definition
define class tgsve_mic as StdTrsForm
  Top    = 1
  Left   = 4

  * --- Standard Properties
  Width  = 698
  Height = 434+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-07-17"
  HelpContextID=264909417
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=53

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  INC_CORR_IDX = 0
  INCDCORR_IDX = 0
  CAU_CONT_IDX = 0
  DOC_MAST_IDX = 0
  VALUTE_IDX = 0
  CONTROPA_IDX = 0
  CONTI_IDX = 0
  cFile = "INC_CORR"
  cFileDetail = "INCDCORR"
  cKeySelect = "INSERIAL"
  cKeyWhere  = "INSERIAL=this.w_INSERIAL"
  cKeyDetail  = "INSERIAL=this.w_INSERIAL"
  cKeyWhereODBC = '"INSERIAL="+cp_ToStrODBC(this.w_INSERIAL)';

  cKeyDetailWhereODBC = '"INSERIAL="+cp_ToStrODBC(this.w_INSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"INCDCORR.INSERIAL="+cp_ToStrODBC(this.w_INSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'INCDCORR.CPROWORD '
  cPrg = "gsve_mic"
  cComment = "Incasso ricevute fiscali"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 13
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_INSERIAL = space(10)
  o_INSERIAL = space(10)
  w_CODAZI = space(5)
  w_INNUMDOC = 0
  w_INALFDOC = space(10)
  w_INDATDOC = ctod('  /  /  ')
  w_CAUINI = space(10)
  w_INCODCAU = space(5)
  w_DESCAU = space(35)
  w_FILCLI = space(40)
  w_INFLCONT = space(1)
  w_INRIFCON = space(10)
  w_TIPREG = space(1)
  w_FLPART = space(1)
  w_CCOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_INCODESE = space(4)
  w_INVALNAZ = space(3)
  w_VALSIM = space(5)
  w_FLBESE = space(1)
  w_NUMREG = 0
  w_CPROWORD = 0
  w_INCODVAL = space(3)
  w_TIPCON = space(1)
  w_CAOVAL = 0
  w_INTOTIMP = 0
  w_INCAOAPE = 0
  w_INDATAPE = ctod('  /  /  ')
  w_INCAOVAL = 0
  w_INIMPSAL = 0
  o_INIMPSAL = 0
  w_INFLSALD = space(1)
  o_INFLSALD = space(1)
  w_INIMPABB = 0
  w_INORICON = space(10)
  w_INORIDOC = space(10)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod('  /  /  ')
  w_DARIMP = 0
  w_DARSAL = 0
  w_INTOTINC = 0
  w_NOTE = space(0)
  w_RAGSOC = space(60)
  w_TOTIMP = 0
  w_TOTSAL = 0
  w_TOTIMP1 = 0
  w_TOTSAL1 = 0
  w_DECTOT = 0
  w_CALCPICT = 0
  w_IMPABB = 0
  w_SIMVAL = space(5)
  w_IN_FLCON = space(1)
  w_IN_RIFCO = space(10)
  w_CODCON = space(15)
  w_ANDESCRI = space(60)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_INSERIAL = this.W_INSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_INCODESE = this.W_INCODESE
  op_INALFDOC = this.W_INALFDOC
  op_INNUMDOC = this.W_INNUMDOC
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'INC_CORR','gsve_mic')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_micPag1","gsve_mic",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Incasso")
      .Pages(1).HelpContextID = 181235846
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='CONTROPA'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='INC_CORR'
    this.cWorkTables[7]='INCDCORR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INC_CORR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INC_CORR_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_INSERIAL = NVL(INSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_13_joined
    link_2_13_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from INC_CORR where INSERIAL=KeySet.INSERIAL
    *
    i_nConn = i_TableProp[this.INC_CORR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INC_CORR_IDX,2],this.bLoadRecFilter,this.INC_CORR_IDX,"gsve_mic")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INC_CORR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INC_CORR.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"INCDCORR.","INC_CORR.")
      i_cTable = i_cTable+' INC_CORR '
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'INSERIAL',this.w_INSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_CAUINI = space(10)
        .w_DESCAU = space(35)
        .w_FILCLI = space(40)
        .w_TIPREG = space(1)
        .w_FLPART = space(1)
        .w_CCOBSO = ctod("  /  /  ")
        .w_OBTEST = .w_INDATDOC
        .w_FLBESE = space(1)
        .w_NUMREG = 0
        .w_TOTIMP = 0
        .w_TOTSAL = 0
        .w_INSERIAL = NVL(INSERIAL,space(10))
        .op_INSERIAL = .w_INSERIAL
          .link_1_2('Load')
        .w_INNUMDOC = NVL(INNUMDOC,0)
        .op_INNUMDOC = .w_INNUMDOC
        .w_INALFDOC = NVL(INALFDOC,space(10))
        .op_INALFDOC = .w_INALFDOC
        .w_INDATDOC = NVL(cp_ToDate(INDATDOC),ctod("  /  /  "))
        .w_INCODCAU = NVL(INCODCAU,space(5))
          if link_1_10_joined
            this.w_INCODCAU = NVL(CCCODICE110,NVL(this.w_INCODCAU,space(5)))
            this.w_DESCAU = NVL(CCDESCRI110,space(35))
            this.w_TIPREG = NVL(CCTIPREG110,space(1))
            this.w_FLPART = NVL(CCFLPART110,space(1))
            this.w_CCOBSO = NVL(cp_ToDate(CCDTOBSO110),ctod("  /  /  "))
            this.w_FLBESE = NVL(CCFLBESE110,space(1))
            this.w_NUMREG = NVL(CCNUMREG110,0)
          else
          .link_1_10('Load')
          endif
        .w_INFLCONT = NVL(INFLCONT,space(1))
        .w_INRIFCON = NVL(INRIFCON,space(10))
        .w_INCODESE = NVL(INCODESE,space(4))
        .op_INCODESE = .w_INCODESE
        .w_INVALNAZ = NVL(INVALNAZ,space(3))
        .w_VALSIM = g_VALSIM
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .w_TOTIMP1 = .w_TOTIMP
        .w_TOTSAL1 = .w_TOTSAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'INC_CORR')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from INCDCORR where INSERIAL=KeySet.INSERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.INCDCORR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INCDCORR_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('INCDCORR')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "INCDCORR.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" INCDCORR"
        link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_13_joined=this.AddJoinedLink_2_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'INSERIAL',this.w_INSERIAL  )
        select * from (i_cTable) INCDCORR where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TOTIMP = 0
      this.w_TOTSAL = 0
      scan
        with this
        .w_TIPCON = 'C'
          .w_CAOVAL = 0
          .w_NUMDOC = 0
          .w_ALFDOC = space(10)
          .w_DATDOC = ctod("  /  /  ")
          .w_NOTE = space(0)
          .w_DECTOT = 0
          .w_SIMVAL = space(5)
          .w_CODCON = space(15)
          .w_ANDESCRI = space(60)
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_INCODVAL = NVL(INCODVAL,space(3))
          if link_2_2_joined
            this.w_INCODVAL = NVL(VACODVAL202,NVL(this.w_INCODVAL,space(3)))
            this.w_CAOVAL = NVL(VACAOVAL202,0)
            this.w_DECTOT = NVL(VADECTOT202,0)
            this.w_SIMVAL = NVL(VASIMVAL202,space(5))
          else
          .link_2_2('Load')
          endif
          .w_INTOTIMP = NVL(INTOTIMP,0)
          .w_INCAOAPE = NVL(INCAOAPE,0)
          .w_INDATAPE = NVL(cp_ToDate(INDATAPE),ctod("  /  /  "))
          .w_INCAOVAL = NVL(INCAOVAL,0)
          .w_INIMPSAL = NVL(INIMPSAL,0)
          .w_INFLSALD = NVL(INFLSALD,space(1))
          .w_INIMPABB = NVL(INIMPABB,0)
          .w_INORICON = NVL(INORICON,space(10))
          .w_INORIDOC = NVL(INORIDOC,space(10))
          if link_2_13_joined
            this.w_INORIDOC = NVL(MVSERIAL213,NVL(this.w_INORIDOC,space(10)))
            this.w_NUMDOC = NVL(MVNUMDOC213,0)
            this.w_ALFDOC = NVL(MVALFDOC213,space(10))
            this.w_DATDOC = NVL(cp_ToDate(MVDATDOC213),ctod("  /  /  "))
            this.w_NOTE = NVL(MV__NOTE213,space(0))
            this.w_CODCON = NVL(MVCODCON213,space(15))
          else
          .link_2_13('Load')
          endif
        .w_DARIMP = IIF(.w_INCODVAL=.w_INVALNAZ, .w_INTOTIMP, VAL2MON(.w_INTOTIMP, .w_INCAOVAL, 1, .w_INDATAPE, .w_INVALNAZ, g_PERPVL))
        .w_DARSAL = IIF(.w_INCODVAL=.w_INVALNAZ, .w_INIMPSAL, cp_ROUND(VAL2MON(.w_INIMPSAL, .w_INCAOVAL, g_CAOVAL, .w_INDATAPE, .w_INVALNAZ), g_PERPVL))
          .w_INTOTINC = NVL(INTOTINC,0)
        .w_RAGSOC = SUBSTR(.w_NOTE, 1, 40)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_25.Calculate(IIF(.w_INTOTIMP<>0 AND .w_CAOVAL=0, 'Cambio:',''))
        .oPgFrm.Page1.oPag.oObj_2_26.Calculate(IIF(.w_INFLSALD<>'S' OR .w_INTOTIMP=.w_INIMPSAL,'', 'Abbuono:'))
        .w_IMPABB = IIF(.w_INCODVAL=.w_INVALNAZ, .w_INIMPABB, VAL2MON(.w_INIMPABB, .w_INCAOVAL, 1, .w_INDATAPE, .w_INVALNAZ, g_PERPVL))
          .w_IN_FLCON = NVL(IN_FLCON,space(1))
          .w_IN_RIFCO = NVL(IN_RIFCO,space(10))
          .link_2_31('Load')
          select (this.cTrsName)
          append blank
          replace INORIDOC with .w_INORIDOC
          replace INTOTINC with .w_INTOTINC
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTIMP = .w_TOTIMP+.w_DARIMP
          .w_TOTSAL = .w_TOTSAL+.w_DARSAL
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_VALSIM = g_VALSIM
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .w_TOTIMP1 = .w_TOTIMP
        .w_TOTSAL1 = .w_TOTSAL
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_29.enabled = .oPgFrm.Page1.oPag.oBtn_1_29.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_33.enabled = .oPgFrm.Page1.oPag.oBtn_1_33.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_INSERIAL=space(10)
      .w_CODAZI=space(5)
      .w_INNUMDOC=0
      .w_INALFDOC=space(10)
      .w_INDATDOC=ctod("  /  /  ")
      .w_CAUINI=space(10)
      .w_INCODCAU=space(5)
      .w_DESCAU=space(35)
      .w_FILCLI=space(40)
      .w_INFLCONT=space(1)
      .w_INRIFCON=space(10)
      .w_TIPREG=space(1)
      .w_FLPART=space(1)
      .w_CCOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_INCODESE=space(4)
      .w_INVALNAZ=space(3)
      .w_VALSIM=space(5)
      .w_FLBESE=space(1)
      .w_NUMREG=0
      .w_CPROWORD=10
      .w_INCODVAL=space(3)
      .w_TIPCON=space(1)
      .w_CAOVAL=0
      .w_INTOTIMP=0
      .w_INCAOAPE=0
      .w_INDATAPE=ctod("  /  /  ")
      .w_INCAOVAL=0
      .w_INIMPSAL=0
      .w_INFLSALD=space(1)
      .w_INIMPABB=0
      .w_INORICON=space(10)
      .w_INORIDOC=space(10)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_DATDOC=ctod("  /  /  ")
      .w_DARIMP=0
      .w_DARSAL=0
      .w_INTOTINC=0
      .w_NOTE=space(0)
      .w_RAGSOC=space(60)
      .w_TOTIMP=0
      .w_TOTSAL=0
      .w_TOTIMP1=0
      .w_TOTSAL1=0
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_IMPABB=0
      .w_SIMVAL=space(5)
      .w_IN_FLCON=space(1)
      .w_IN_RIFCO=space(10)
      .w_CODCON=space(15)
      .w_ANDESCRI=space(60)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODAZI))
         .link_1_2('Full')
        endif
        .DoRTCalc(3,4,.f.)
        .w_INDATDOC = i_DATSYS
        .DoRTCalc(6,6,.f.)
        .w_INCODCAU = .w_CAUINI
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_INCODCAU))
         .link_1_10('Full')
        endif
        .DoRTCalc(8,14,.f.)
        .w_OBTEST = .w_INDATDOC
        .w_INCODESE = g_CODESE
        .w_INVALNAZ = g_PERVAL
        .w_VALSIM = g_VALSIM
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .DoRTCalc(19,22,.f.)
        if not(empty(.w_INCODVAL))
         .link_2_2('Full')
        endif
        .w_TIPCON = 'C'
        .DoRTCalc(24,30,.f.)
        .w_INIMPABB = IIF(.w_INFLSALD='S', .w_INTOTIMP-.w_INIMPSAL, 0)
        .DoRTCalc(32,33,.f.)
        if not(empty(.w_INORIDOC))
         .link_2_13('Full')
        endif
        .DoRTCalc(34,36,.f.)
        .w_DARIMP = IIF(.w_INCODVAL=.w_INVALNAZ, .w_INTOTIMP, VAL2MON(.w_INTOTIMP, .w_INCAOVAL, 1, .w_INDATAPE, .w_INVALNAZ, g_PERPVL))
        .w_DARSAL = IIF(.w_INCODVAL=.w_INVALNAZ, .w_INIMPSAL, cp_ROUND(VAL2MON(.w_INIMPSAL, .w_INCAOVAL, g_CAOVAL, .w_INDATAPE, .w_INVALNAZ), g_PERPVL))
        .w_INTOTINC = .w_INIMPSAL + .w_INIMPABB
        .DoRTCalc(40,40,.f.)
        .w_RAGSOC = SUBSTR(.w_NOTE, 1, 40)
        .DoRTCalc(42,43,.f.)
        .w_TOTIMP1 = .w_TOTIMP
        .w_TOTSAL1 = .w_TOTSAL
        .DoRTCalc(46,46,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_25.Calculate(IIF(.w_INTOTIMP<>0 AND .w_CAOVAL=0, 'Cambio:',''))
        .oPgFrm.Page1.oPag.oObj_2_26.Calculate(IIF(.w_INFLSALD<>'S' OR .w_INTOTIMP=.w_INIMPSAL,'', 'Abbuono:'))
        .w_IMPABB = IIF(.w_INCODVAL=.w_INVALNAZ, .w_INIMPABB, VAL2MON(.w_INIMPABB, .w_INCAOVAL, 1, .w_INDATAPE, .w_INVALNAZ, g_PERPVL))
        .DoRTCalc(49,52,.f.)
        if not(empty(.w_CODCON))
         .link_2_31('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'INC_CORR')
    this.DoRTCalc(53,53,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oINNUMDOC_1_3.enabled = i_bVal
      .Page1.oPag.oINALFDOC_1_4.enabled = i_bVal
      .Page1.oPag.oINDATDOC_1_5.enabled = i_bVal
      .Page1.oPag.oINCODCAU_1_10.enabled = i_bVal
      .Page1.oPag.oFILCLI_1_13.enabled = i_bVal
      .Page1.oPag.oINCAOVAL_2_8.enabled = i_bVal
      .Page1.oPag.oBtn_1_24.enabled = i_bVal
      .Page1.oPag.oBtn_1_29.enabled = .Page1.oPag.oBtn_1_29.mCond()
      .Page1.oPag.oBtn_1_33.enabled = .Page1.oPag.oBtn_1_33.mCond()
      .Page1.oPag.oObj_1_27.enabled = i_bVal
      .Page1.oPag.oObj_1_28.enabled = i_bVal
      .Page1.oPag.oObj_1_30.enabled = i_bVal
      .Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.enabled = i_bVal
      .Page1.oPag.oObj_1_35.enabled = i_bVal
      .Page1.oPag.oObj_1_36.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oINNUMDOC_1_3.enabled = .t.
        .Page1.oPag.oINDATDOC_1_5.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'INC_CORR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INC_CORR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INC_CORR_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SAINCASS","i_codazi,w_INSERIAL")
      cp_AskTableProg(this,i_nConn,"PRINCASS","i_codazi,w_INCODESE,w_INALFDOC,w_INNUMDOC")
      .op_codazi = .w_codazi
      .op_INSERIAL = .w_INSERIAL
      .op_codazi = .w_codazi
      .op_INCODESE = .w_INCODESE
      .op_INALFDOC = .w_INALFDOC
      .op_INNUMDOC = .w_INNUMDOC
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INC_CORR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INSERIAL,"INSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INNUMDOC,"INNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INALFDOC,"INALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INDATDOC,"INDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INCODCAU,"INCODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INFLCONT,"INFLCONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INRIFCON,"INRIFCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INCODESE,"INCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_INVALNAZ,"INVALNAZ",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INC_CORR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INC_CORR_IDX,2])
    i_lTable = "INC_CORR"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.INC_CORR_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        do GSVE_BSC with this
      endwith
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_INTOTIMP N(18,4);
      ,t_INCAOVAL N(12,6);
      ,t_INIMPSAL N(18,4);
      ,t_INFLSALD N(3);
      ,t_NUMDOC N(15);
      ,t_ALFDOC C(10);
      ,t_DATDOC D(8);
      ,t_RAGSOC C(60);
      ,t_IMPABB N(18,4);
      ,t_SIMVAL C(5);
      ,t_ANDESCRI C(60);
      ,INORIDOC C(10);
      ,INTOTINC N(18,4);
      ,CPROWNUM N(10);
      ,t_INCODVAL C(3);
      ,t_TIPCON C(1);
      ,t_CAOVAL N(12,6);
      ,t_INCAOAPE N(12,7);
      ,t_INDATAPE D(8);
      ,t_INIMPABB N(18,4);
      ,t_INORICON C(10);
      ,t_INORIDOC C(10);
      ,t_DARIMP N(18,4);
      ,t_DARSAL N(18,4);
      ,t_INTOTINC N(18,4);
      ,t_NOTE M(10);
      ,t_DECTOT N(1);
      ,t_CALCPICT N(1);
      ,t_IN_FLCON C(1);
      ,t_IN_RIFCO C(10);
      ,t_CODCON C(15);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsve_micbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oINTOTIMP_2_5.controlsource=this.cTrsName+'.t_INTOTIMP'
    this.oPgFRm.Page1.oPag.oINCAOVAL_2_8.controlsource=this.cTrsName+'.t_INCAOVAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oINIMPSAL_2_9.controlsource=this.cTrsName+'.t_INIMPSAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oINFLSALD_2_10.controlsource=this.cTrsName+'.t_INFLSALD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_14.controlsource=this.cTrsName+'.t_NUMDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_15.controlsource=this.cTrsName+'.t_ALFDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDATDOC_2_16.controlsource=this.cTrsName+'.t_DATDOC'
    this.oPgFRm.Page1.oPag.oRAGSOC_2_21.controlsource=this.cTrsName+'.t_RAGSOC'
    this.oPgFRm.Page1.oPag.oIMPABB_2_27.controlsource=this.cTrsName+'.t_IMPABB'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSIMVAL_2_28.controlsource=this.cTrsName+'.t_SIMVAL'
    this.oPgFRm.Page1.oPag.oANDESCRI_2_32.controlsource=this.cTrsName+'.t_ANDESCRI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(45)
    this.AddVLine(166)
    this.AddVLine(276)
    this.AddVLine(359)
    this.AddVLine(403)
    this.AddVLine(526)
    this.AddVLine(652)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INC_CORR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INC_CORR_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SAINCASS","i_codazi,w_INSERIAL")
          cp_NextTableProg(this,i_nConn,"PRINCASS","i_codazi,w_INCODESE,w_INALFDOC,w_INNUMDOC")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into INC_CORR
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INC_CORR')
        i_extval=cp_InsertValODBCExtFlds(this,'INC_CORR')
        local i_cFld
        i_cFld=" "+;
                  "(INSERIAL,INNUMDOC,INALFDOC,INDATDOC,INCODCAU"+;
                  ",INFLCONT,INRIFCON,INCODESE,INVALNAZ"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_INSERIAL)+;
                    ","+cp_ToStrODBC(this.w_INNUMDOC)+;
                    ","+cp_ToStrODBC(this.w_INALFDOC)+;
                    ","+cp_ToStrODBC(this.w_INDATDOC)+;
                    ","+cp_ToStrODBCNull(this.w_INCODCAU)+;
                    ","+cp_ToStrODBC(this.w_INFLCONT)+;
                    ","+cp_ToStrODBC(this.w_INRIFCON)+;
                    ","+cp_ToStrODBC(this.w_INCODESE)+;
                    ","+cp_ToStrODBC(this.w_INVALNAZ)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INC_CORR')
        i_extval=cp_InsertValVFPExtFlds(this,'INC_CORR')
        cp_CheckDeletedKey(i_cTable,0,'INSERIAL',this.w_INSERIAL)
        INSERT INTO (i_cTable);
              (INSERIAL,INNUMDOC,INALFDOC,INDATDOC,INCODCAU,INFLCONT,INRIFCON,INCODESE,INVALNAZ &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_INSERIAL;
                  ,this.w_INNUMDOC;
                  ,this.w_INALFDOC;
                  ,this.w_INDATDOC;
                  ,this.w_INCODCAU;
                  ,this.w_INFLCONT;
                  ,this.w_INRIFCON;
                  ,this.w_INCODESE;
                  ,this.w_INVALNAZ;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INCDCORR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INCDCORR_IDX,2])
      *
      * insert into INCDCORR
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(INSERIAL,CPROWORD,INCODVAL,INTOTIMP,INCAOAPE"+;
                  ",INDATAPE,INCAOVAL,INIMPSAL,INFLSALD,INIMPABB"+;
                  ",INORICON,INORIDOC,INTOTINC,IN_FLCON,IN_RIFCO,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_INSERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_INCODVAL)+","+cp_ToStrODBC(this.w_INTOTIMP)+","+cp_ToStrODBC(this.w_INCAOAPE)+;
             ","+cp_ToStrODBC(this.w_INDATAPE)+","+cp_ToStrODBC(this.w_INCAOVAL)+","+cp_ToStrODBC(this.w_INIMPSAL)+","+cp_ToStrODBC(this.w_INFLSALD)+","+cp_ToStrODBC(this.w_INIMPABB)+;
             ","+cp_ToStrODBC(this.w_INORICON)+","+cp_ToStrODBCNull(this.w_INORIDOC)+","+cp_ToStrODBC(this.w_INTOTINC)+","+cp_ToStrODBC(this.w_IN_FLCON)+","+cp_ToStrODBC(this.w_IN_RIFCO)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'INSERIAL',this.w_INSERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_INSERIAL,this.w_CPROWORD,this.w_INCODVAL,this.w_INTOTIMP,this.w_INCAOAPE"+;
                ",this.w_INDATAPE,this.w_INCAOVAL,this.w_INIMPSAL,this.w_INFLSALD,this.w_INIMPABB"+;
                ",this.w_INORICON,this.w_INORIDOC,this.w_INTOTINC,this.w_IN_FLCON,this.w_IN_RIFCO,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.INC_CORR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INC_CORR_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update INC_CORR
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'INC_CORR')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " INNUMDOC="+cp_ToStrODBC(this.w_INNUMDOC)+;
             ",INALFDOC="+cp_ToStrODBC(this.w_INALFDOC)+;
             ",INDATDOC="+cp_ToStrODBC(this.w_INDATDOC)+;
             ",INCODCAU="+cp_ToStrODBCNull(this.w_INCODCAU)+;
             ",INFLCONT="+cp_ToStrODBC(this.w_INFLCONT)+;
             ",INRIFCON="+cp_ToStrODBC(this.w_INRIFCON)+;
             ",INCODESE="+cp_ToStrODBC(this.w_INCODESE)+;
             ",INVALNAZ="+cp_ToStrODBC(this.w_INVALNAZ)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'INC_CORR')
          i_cWhere = cp_PKFox(i_cTable  ,'INSERIAL',this.w_INSERIAL  )
          UPDATE (i_cTable) SET;
              INNUMDOC=this.w_INNUMDOC;
             ,INALFDOC=this.w_INALFDOC;
             ,INDATDOC=this.w_INDATDOC;
             ,INCODCAU=this.w_INCODCAU;
             ,INFLCONT=this.w_INFLCONT;
             ,INRIFCON=this.w_INRIFCON;
             ,INCODESE=this.w_INCODESE;
             ,INVALNAZ=this.w_INVALNAZ;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_INTOTIMP<>0 AND (t_INIMPSAL<>0 OR t_INIMPABB<>0)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.INCDCORR_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.INCDCORR_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from INCDCORR
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update INCDCORR
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",INCODVAL="+cp_ToStrODBCNull(this.w_INCODVAL)+;
                     ",INTOTIMP="+cp_ToStrODBC(this.w_INTOTIMP)+;
                     ",INCAOAPE="+cp_ToStrODBC(this.w_INCAOAPE)+;
                     ",INDATAPE="+cp_ToStrODBC(this.w_INDATAPE)+;
                     ",INCAOVAL="+cp_ToStrODBC(this.w_INCAOVAL)+;
                     ",INIMPSAL="+cp_ToStrODBC(this.w_INIMPSAL)+;
                     ",INFLSALD="+cp_ToStrODBC(this.w_INFLSALD)+;
                     ",INIMPABB="+cp_ToStrODBC(this.w_INIMPABB)+;
                     ",INORICON="+cp_ToStrODBC(this.w_INORICON)+;
                     ",INORIDOC="+cp_ToStrODBCNull(this.w_INORIDOC)+;
                     ",INTOTINC="+cp_ToStrODBC(this.w_INTOTINC)+;
                     ",IN_FLCON="+cp_ToStrODBC(this.w_IN_FLCON)+;
                     ",IN_RIFCO="+cp_ToStrODBC(this.w_IN_RIFCO)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,INCODVAL=this.w_INCODVAL;
                     ,INTOTIMP=this.w_INTOTIMP;
                     ,INCAOAPE=this.w_INCAOAPE;
                     ,INDATAPE=this.w_INDATAPE;
                     ,INCAOVAL=this.w_INCAOVAL;
                     ,INIMPSAL=this.w_INIMPSAL;
                     ,INFLSALD=this.w_INFLSALD;
                     ,INIMPABB=this.w_INIMPABB;
                     ,INORICON=this.w_INORICON;
                     ,INORIDOC=this.w_INORIDOC;
                     ,INTOTINC=this.w_INTOTINC;
                     ,IN_FLCON=this.w_IN_FLCON;
                     ,IN_RIFCO=this.w_IN_RIFCO;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsve_mic
    if not(bTrsErr)
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        * --- Controlli Finali
        this.NotifyEvent('ControlliFinali')
        * --- Riposiziona sul Primo record del Temporaneo
        * --- Perche' in caso di errore il Puntatore non si Riposiziona giusto
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        this.SaveDependsOn()
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Detail Transaction
  proc mRestoreTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nConn,i_cTable,i_oRow
    local i_cOp1

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..INTOTINC,0)==this.w_INTOTINC;
              and NVL(&i_cF..INORIDOC,space(10))==this.w_INORIDOC;

      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..INORIDOC,space(10))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" MVACCSUC=MVACCSUC - "+cp_ToStrODBC(NVL(&i_cF..INTOTINC,0))+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE MVSERIAL="+cp_ToStrODBC(NVL(&i_cF..INORIDOC,space(10)));
             )
      endif
    else
      i_cWhere = cp_PKFox(i_cTable;
                 ,'MVSERIAL',&i_cF..INORIDOC)
      UPDATE (i_cTable) SET ;
           MVACCSUC=MVACCSUC-&i_cF..INTOTINC  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Detail Transaction
  proc mUpdateTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nModRow,i_nConn,i_cTable,i_oRow
    local i_cOp1

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..INTOTINC,0)==this.w_INTOTINC;
              and NVL(&i_cF..INORIDOC,space(10))==this.w_INORIDOC;

      if !i_bSkip and !Empty(this.w_INORIDOC)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" MVACCSUC=MVACCSUC + "+cp_ToStrODBC(this.w_INTOTINC)  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE MVSERIAL="+cp_ToStrODBC(this.w_INORIDOC);
           )
      endif
    else
      i_cWhere = cp_PKFox(i_cTable;
                 ,'MVSERIAL',this.w_INORIDOC)
      UPDATE (i_cTable) SET;
           MVACCSUC=MVACCSUC+this.w_INTOTINC  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_INTOTIMP<>0 AND (t_INIMPSAL<>0 OR t_INIMPABB<>0)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.INCDCORR_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.INCDCORR_IDX,2])
        *
        * delete INCDCORR
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.INC_CORR_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.INC_CORR_IDX,2])
        *
        * delete INC_CORR
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_INTOTIMP<>0 AND (t_INIMPSAL<>0 OR t_INIMPABB<>0)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsve_mic
    if not(bTrsErr)
        select (this.cTrsName)
        go top
        this.WorkFromTrs()
        * --- Controlli Finali
        this.NotifyEvent('Cancella')
        * --- Riposiziona sul Primo record del Temporaneo
        * --- Perche' in caso di errore il Puntatore non si Riposiziona giusto
        if not(bTrsErr)
            select (this.cTrsName)
            go top
            this.WorkFromTrs()
        endif
    endif
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INC_CORR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INC_CORR_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,17,.t.)
          .w_VALSIM = g_VALSIM
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .DoRTCalc(19,21,.t.)
          .link_2_2('Full')
        .DoRTCalc(23,30,.t.)
        if .o_INFLSALD<>.w_INFLSALD.or. .o_INIMPSAL<>.w_INIMPSAL
          .w_INIMPABB = IIF(.w_INFLSALD='S', .w_INTOTIMP-.w_INIMPSAL, 0)
        endif
        .DoRTCalc(32,32,.t.)
        if .o_INSERIAL<>.w_INSERIAL
          .link_2_13('Full')
        endif
        .DoRTCalc(34,36,.t.)
          .w_TOTIMP = .w_TOTIMP-.w_darimp
          .w_DARIMP = IIF(.w_INCODVAL=.w_INVALNAZ, .w_INTOTIMP, VAL2MON(.w_INTOTIMP, .w_INCAOVAL, 1, .w_INDATAPE, .w_INVALNAZ, g_PERPVL))
          .w_TOTIMP = .w_TOTIMP+.w_darimp
          .w_TOTSAL = .w_TOTSAL-.w_darsal
          .w_DARSAL = IIF(.w_INCODVAL=.w_INVALNAZ, .w_INIMPSAL, cp_ROUND(VAL2MON(.w_INIMPSAL, .w_INCAOVAL, g_CAOVAL, .w_INDATAPE, .w_INVALNAZ), g_PERPVL))
          .w_TOTSAL = .w_TOTSAL+.w_darsal
          .w_INTOTINC = .w_INIMPSAL + .w_INIMPABB
        .DoRTCalc(40,40,.t.)
          .w_RAGSOC = SUBSTR(.w_NOTE, 1, 40)
        .DoRTCalc(42,43,.t.)
          .w_TOTIMP1 = .w_TOTIMP
          .w_TOTSAL1 = .w_TOTSAL
        .DoRTCalc(46,46,.t.)
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_25.Calculate(IIF(.w_INTOTIMP<>0 AND .w_CAOVAL=0, 'Cambio:',''))
        .oPgFrm.Page1.oPag.oObj_2_26.Calculate(IIF(.w_INFLSALD<>'S' OR .w_INTOTIMP=.w_INIMPSAL,'', 'Abbuono:'))
          .w_IMPABB = IIF(.w_INCODVAL=.w_INVALNAZ, .w_INIMPABB, VAL2MON(.w_INIMPABB, .w_INCAOVAL, 1, .w_INDATAPE, .w_INVALNAZ, g_PERPVL))
        .DoRTCalc(49,51,.t.)
          .link_2_31('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SAINCASS","i_codazi,w_INSERIAL")
          .op_INSERIAL = .w_INSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_INCODESE<>.w_INCODESE .or. .op_INALFDOC<>.w_INALFDOC
           cp_AskTableProg(this,i_nConn,"PRINCASS","i_codazi,w_INCODESE,w_INALFDOC,w_INNUMDOC")
          .op_INNUMDOC = .w_INNUMDOC
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_INCODESE = .w_INCODESE
        .op_INALFDOC = .w_INALFDOC
      endwith
      this.DoRTCalc(53,53,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_INCODVAL with this.w_INCODVAL
      replace t_TIPCON with this.w_TIPCON
      replace t_CAOVAL with this.w_CAOVAL
      replace t_INCAOAPE with this.w_INCAOAPE
      replace t_INDATAPE with this.w_INDATAPE
      replace t_INIMPABB with this.w_INIMPABB
      replace t_INORICON with this.w_INORICON
      replace t_INORIDOC with this.w_INORIDOC
      replace t_DARIMP with this.w_DARIMP
      replace t_DARSAL with this.w_DARSAL
      replace t_INTOTINC with this.w_INTOTINC
      replace t_NOTE with this.w_NOTE
      replace t_DECTOT with this.w_DECTOT
      replace t_CALCPICT with this.w_CALCPICT
      replace t_IN_FLCON with this.w_IN_FLCON
      replace t_IN_RIFCO with this.w_IN_RIFCO
      replace t_CODCON with this.w_CODCON
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_25.Calculate(IIF(.w_INTOTIMP<>0 AND .w_CAOVAL=0, 'Cambio:',''))
        .oPgFrm.Page1.oPag.oObj_2_26.Calculate(IIF(.w_INFLSALD<>'S' OR .w_INTOTIMP=.w_INIMPSAL,'', 'Abbuono:'))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_25.Calculate(IIF(.w_INTOTIMP<>0 AND .w_CAOVAL=0, 'Cambio:',''))
        .oPgFrm.Page1.oPag.oObj_2_26.Calculate(IIF(.w_INFLSALD<>'S' OR .w_INTOTIMP=.w_INIMPSAL,'', 'Abbuono:'))
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oINCODCAU_1_10.enabled = this.oPgFrm.Page1.oPag.oINCODCAU_1_10.mCond()
    this.oPgFrm.Page1.oPag.oFILCLI_1_13.enabled = this.oPgFrm.Page1.oPag.oFILCLI_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oINCAOVAL_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oINCAOVAL_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oINIMPSAL_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oINIMPSAL_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oINFLSALD_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oINFLSALD_2_10.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oINCODCAU_1_10.visible=!this.oPgFrm.Page1.oPag.oINCODCAU_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oDESCAU_1_12.visible=!this.oPgFrm.Page1.oPag.oDESCAU_1_12.mHide()
    this.oPgFrm.Page1.oPag.oFILCLI_1_13.visible=!this.oPgFrm.Page1.oPag.oFILCLI_1_13.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_24.visible=!this.oPgFrm.Page1.oPag.oBtn_1_24.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_29.visible=!this.oPgFrm.Page1.oPag.oBtn_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_33.visible=!this.oPgFrm.Page1.oPag.oBtn_1_33.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oINCAOVAL_2_8.visible=!this.oPgFrm.Page1.oPag.oINCAOVAL_2_8.mHide()
    this.oPgFrm.Page1.oPag.oRAGSOC_2_21.visible=!this.oPgFrm.Page1.oPag.oRAGSOC_2_21.mHide()
    this.oPgFrm.Page1.oPag.oIMPABB_2_27.visible=!this.oPgFrm.Page1.oPag.oIMPABB_2_27.mHide()
    this.oPgFrm.Page1.oPag.oANDESCRI_2_32.visible=!this.oPgFrm.Page1.oPag.oANDESCRI_2_32.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_25.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_26.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COCAUICO";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,COCAUICO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_CAUINI = NVL(_Link_.COCAUICO,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_CAUINI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INCODCAU
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INCODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_INCODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLPART,CCDTOBSO,CCFLBESE,CCNUMREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_INCODCAU))
          select CCCODICE,CCDESCRI,CCTIPREG,CCFLPART,CCDTOBSO,CCFLBESE,CCNUMREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INCODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_INCODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oINCODCAU_1_10'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSVE_MIC.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLPART,CCDTOBSO,CCFLBESE,CCNUMREG";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG,CCFLPART,CCDTOBSO,CCFLBESE,CCNUMREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INCODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLPART,CCDTOBSO,CCFLBESE,CCNUMREG";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_INCODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_INCODCAU)
            select CCCODICE,CCDESCRI,CCTIPREG,CCFLPART,CCDTOBSO,CCFLBESE,CCNUMREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INCODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_FLPART = NVL(_Link_.CCFLPART,space(1))
      this.w_CCOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_FLBESE = NVL(_Link_.CCFLBESE,space(1))
      this.w_NUMREG = NVL(_Link_.CCNUMREG,0)
    else
      if i_cCtrl<>'Load'
        this.w_INCODCAU = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_TIPREG = space(1)
      this.w_FLPART = space(1)
      this.w_CCOBSO = ctod("  /  /  ")
      this.w_FLBESE = space(1)
      this.w_NUMREG = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPREG $ 'CN' AND .w_FLPART='S' AND (.w_CCOBSO>.w_OBTEST OR EMPTY(.w_CCOBSO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale incongruente o obsoleta")
        endif
        this.w_INCODCAU = space(5)
        this.w_DESCAU = space(35)
        this.w_TIPREG = space(1)
        this.w_FLPART = space(1)
        this.w_CCOBSO = ctod("  /  /  ")
        this.w_FLBESE = space(1)
        this.w_NUMREG = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INCODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.CCCODICE as CCCODICE110"+ ",link_1_10.CCDESCRI as CCDESCRI110"+ ",link_1_10.CCTIPREG as CCTIPREG110"+ ",link_1_10.CCFLPART as CCFLPART110"+ ",link_1_10.CCDTOBSO as CCDTOBSO110"+ ",link_1_10.CCFLBESE as CCFLBESE110"+ ",link_1_10.CCNUMREG as CCNUMREG110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on INC_CORR.INCODCAU=link_1_10.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and INC_CORR.INCODCAU=link_1_10.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=INCODVAL
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_INCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_INCODVAL)
            select VACODVAL,VACAOVAL,VADECTOT,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_INCODVAL = space(3)
      endif
      this.w_CAOVAL = 0
      this.w_DECTOT = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.VACODVAL as VACODVAL202"+ ",link_2_2.VACAOVAL as VACAOVAL202"+ ",link_2_2.VADECTOT as VADECTOT202"+ ",link_2_2.VASIMVAL as VASIMVAL202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on INCDCORR.INCODVAL=link_2_2.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and INCDCORR.INCODVAL=link_2_2.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=INORIDOC
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INORIDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INORIDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVNUMDOC,MVALFDOC,MVDATDOC,MV__NOTE,MVCODCON";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_INORIDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_INORIDOC)
            select MVSERIAL,MVNUMDOC,MVALFDOC,MVDATDOC,MV__NOTE,MVCODCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INORIDOC = NVL(_Link_.MVSERIAL,space(10))
      this.w_NUMDOC = NVL(_Link_.MVNUMDOC,0)
      this.w_ALFDOC = NVL(_Link_.MVALFDOC,space(10))
      this.w_DATDOC = NVL(cp_ToDate(_Link_.MVDATDOC),ctod("  /  /  "))
      this.w_NOTE = NVL(_Link_.MV__NOTE,space(0))
      this.w_CODCON = NVL(_Link_.MVCODCON,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_INORIDOC = space(10)
      endif
      this.w_NUMDOC = 0
      this.w_ALFDOC = space(10)
      this.w_DATDOC = ctod("  /  /  ")
      this.w_NOTE = space(0)
      this.w_CODCON = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INORIDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DOC_MAST_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_13.MVSERIAL as MVSERIAL213"+ ",link_2_13.MVNUMDOC as MVNUMDOC213"+ ",link_2_13.MVALFDOC as MVALFDOC213"+ ",link_2_13.MVDATDOC as MVDATDOC213"+ ",link_2_13.MV__NOTE as MV__NOTE213"+ ",link_2_13.MVCODCON as MVCODCON213"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_13 on INCDCORR.INORIDOC=link_2_13.MVSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_13"
          i_cKey=i_cKey+'+" and INCDCORR.INORIDOC=link_2_13.MVSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_ANDESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oINNUMDOC_1_3.value==this.w_INNUMDOC)
      this.oPgFrm.Page1.oPag.oINNUMDOC_1_3.value=this.w_INNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oINALFDOC_1_4.value==this.w_INALFDOC)
      this.oPgFrm.Page1.oPag.oINALFDOC_1_4.value=this.w_INALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oINDATDOC_1_5.value==this.w_INDATDOC)
      this.oPgFrm.Page1.oPag.oINDATDOC_1_5.value=this.w_INDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oINCODCAU_1_10.value==this.w_INCODCAU)
      this.oPgFrm.Page1.oPag.oINCODCAU_1_10.value=this.w_INCODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_12.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_12.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oFILCLI_1_13.value==this.w_FILCLI)
      this.oPgFrm.Page1.oPag.oFILCLI_1_13.value=this.w_FILCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oVALSIM_1_23.value==this.w_VALSIM)
      this.oPgFrm.Page1.oPag.oVALSIM_1_23.value=this.w_VALSIM
    endif
    if not(this.oPgFrm.Page1.oPag.oINCAOVAL_2_8.value==this.w_INCAOVAL)
      this.oPgFrm.Page1.oPag.oINCAOVAL_2_8.value=this.w_INCAOVAL
      replace t_INCAOVAL with this.oPgFrm.Page1.oPag.oINCAOVAL_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGSOC_2_21.value==this.w_RAGSOC)
      this.oPgFrm.Page1.oPag.oRAGSOC_2_21.value=this.w_RAGSOC
      replace t_RAGSOC with this.oPgFrm.Page1.oPag.oRAGSOC_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIMP1_3_3.value==this.w_TOTIMP1)
      this.oPgFrm.Page1.oPag.oTOTIMP1_3_3.value=this.w_TOTIMP1
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTSAL1_3_4.value==this.w_TOTSAL1)
      this.oPgFrm.Page1.oPag.oTOTSAL1_3_4.value=this.w_TOTSAL1
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPABB_2_27.value==this.w_IMPABB)
      this.oPgFrm.Page1.oPag.oIMPABB_2_27.value=this.w_IMPABB
      replace t_IMPABB with this.oPgFrm.Page1.oPag.oIMPABB_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_2_32.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_2_32.value=this.w_ANDESCRI
      replace t_ANDESCRI with this.oPgFrm.Page1.oPag.oANDESCRI_2_32.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oINTOTIMP_2_5.value==this.w_INTOTIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oINTOTIMP_2_5.value=this.w_INTOTIMP
      replace t_INTOTIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oINTOTIMP_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oINIMPSAL_2_9.value==this.w_INIMPSAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oINIMPSAL_2_9.value=this.w_INIMPSAL
      replace t_INIMPSAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oINIMPSAL_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oINFLSALD_2_10.RadioValue()==this.w_INFLSALD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oINFLSALD_2_10.SetRadio()
      replace t_INFLSALD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oINFLSALD_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_14.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_14.value=this.w_NUMDOC
      replace t_NUMDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNUMDOC_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_15.value==this.w_ALFDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_15.value=this.w_ALFDOC
      replace t_ALFDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oALFDOC_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDATDOC_2_16.value==this.w_DATDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDATDOC_2_16.value=this.w_DATDOC
      replace t_DATDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDATDOC_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSIMVAL_2_28.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSIMVAL_2_28.value=this.w_SIMVAL
      replace t_SIMVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSIMVAL_2_28.value
    endif
    cp_SetControlsValueExtFlds(this,'INC_CORR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_INNUMDOC))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oINNUMDOC_1_3.SetFocus()
            i_bnoObbl = !empty(.w_INNUMDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_INDATDOC))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oINDATDOC_1_5.SetFocus()
            i_bnoObbl = !empty(.w_INDATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_INCODCAU) or not(.w_TIPREG $ 'CN' AND .w_FLPART='S' AND (.w_CCOBSO>.w_OBTEST OR EMPTY(.w_CCOBSO))))  and not(g_COGE<>'S')  and (g_COGE='S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oINCODCAU_1_10.SetFocus()
            i_bnoObbl = !empty(.w_INCODCAU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale incongruente o obsoleta")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_INCAOVAL) and (.w_INTOTIMP<>0 AND .w_CAOVAL=0) and (.w_INTOTIMP<>0 AND (.w_INIMPSAL<>0 OR .w_INIMPABB<>0))
          .oNewFocus=.oPgFrm.Page1.oPag.oINCAOVAL_2_8
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(SIGN(.w_INTOTIMP)=SIGN(.w_INIMPSAL) OR .w_INIMPSAL=0) and (.w_INTOTIMP<>0) and (.w_INTOTIMP<>0 AND (.w_INIMPSAL<>0 OR .w_INIMPABB<>0))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oINIMPSAL_2_9
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("L'importo incassato deve essere dello stesso segno dell'importo da incassare")
      endcase
      if .w_INTOTIMP<>0 AND (.w_INIMPSAL<>0 OR .w_INIMPABB<>0)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_INSERIAL = this.w_INSERIAL
    this.o_INIMPSAL = this.w_INIMPSAL
    this.o_INFLSALD = this.w_INFLSALD
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_INTOTIMP<>0 AND (t_INIMPSAL<>0 OR t_INIMPABB<>0))
    select(i_nArea)
    return(i_bRes)
  Endfunc
  Func CanDeleteRow()
    local i_bRes
    i_bRes=Empty(t_IN_RIFCO) and t_IN_FLCON<>'S'
    if !i_bRes
      cp_ErrorMsg("Incasso contabilizzato, impossibile eliminare","stop","")
    endif
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_INCODVAL=space(3)
      .w_TIPCON=space(1)
      .w_CAOVAL=0
      .w_INTOTIMP=0
      .w_INCAOAPE=0
      .w_INDATAPE=ctod("  /  /  ")
      .w_INCAOVAL=0
      .w_INIMPSAL=0
      .w_INFLSALD=space(1)
      .w_INIMPABB=0
      .w_INORICON=space(10)
      .w_INORIDOC=space(10)
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_DATDOC=ctod("  /  /  ")
      .w_DARIMP=0
      .w_DARSAL=0
      .w_INTOTINC=0
      .w_NOTE=space(0)
      .w_RAGSOC=space(60)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_IMPABB=0
      .w_SIMVAL=space(5)
      .w_IN_FLCON=space(1)
      .w_IN_RIFCO=space(10)
      .w_CODCON=space(15)
      .w_ANDESCRI=space(60)
      .DoRTCalc(1,22,.f.)
      if not(empty(.w_INCODVAL))
        .link_2_2('Full')
      endif
        .w_TIPCON = 'C'
      .DoRTCalc(24,30,.f.)
        .w_INIMPABB = IIF(.w_INFLSALD='S', .w_INTOTIMP-.w_INIMPSAL, 0)
      .DoRTCalc(32,33,.f.)
      if not(empty(.w_INORIDOC))
        .link_2_13('Full')
      endif
      .DoRTCalc(34,36,.f.)
        .w_DARIMP = IIF(.w_INCODVAL=.w_INVALNAZ, .w_INTOTIMP, VAL2MON(.w_INTOTIMP, .w_INCAOVAL, 1, .w_INDATAPE, .w_INVALNAZ, g_PERPVL))
        .w_DARSAL = IIF(.w_INCODVAL=.w_INVALNAZ, .w_INIMPSAL, cp_ROUND(VAL2MON(.w_INIMPSAL, .w_INCAOVAL, g_CAOVAL, .w_INDATAPE, .w_INVALNAZ), g_PERPVL))
        .w_INTOTINC = .w_INIMPSAL + .w_INIMPABB
      .DoRTCalc(40,40,.f.)
        .w_RAGSOC = SUBSTR(.w_NOTE, 1, 40)
      .DoRTCalc(42,46,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oObj_2_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_25.Calculate(IIF(.w_INTOTIMP<>0 AND .w_CAOVAL=0, 'Cambio:',''))
        .oPgFrm.Page1.oPag.oObj_2_26.Calculate(IIF(.w_INFLSALD<>'S' OR .w_INTOTIMP=.w_INIMPSAL,'', 'Abbuono:'))
        .w_IMPABB = IIF(.w_INCODVAL=.w_INVALNAZ, .w_INIMPABB, VAL2MON(.w_INIMPABB, .w_INCAOVAL, 1, .w_INDATAPE, .w_INVALNAZ, g_PERPVL))
      .DoRTCalc(49,52,.f.)
      if not(empty(.w_CODCON))
        .link_2_31('Full')
      endif
    endwith
    this.DoRTCalc(53,53,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_INCODVAL = t_INCODVAL
    this.w_TIPCON = t_TIPCON
    this.w_CAOVAL = t_CAOVAL
    this.w_INTOTIMP = t_INTOTIMP
    this.w_INCAOAPE = t_INCAOAPE
    this.w_INDATAPE = t_INDATAPE
    this.w_INCAOVAL = t_INCAOVAL
    this.w_INIMPSAL = t_INIMPSAL
    this.w_INFLSALD = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oINFLSALD_2_10.RadioValue(.t.)
    this.w_INIMPABB = t_INIMPABB
    this.w_INORICON = t_INORICON
    this.w_INORIDOC = t_INORIDOC
    this.w_NUMDOC = t_NUMDOC
    this.w_ALFDOC = t_ALFDOC
    this.w_DATDOC = t_DATDOC
    this.w_DARIMP = t_DARIMP
    this.w_DARSAL = t_DARSAL
    this.w_INTOTINC = t_INTOTINC
    this.w_NOTE = t_NOTE
    this.w_RAGSOC = t_RAGSOC
    this.w_DECTOT = t_DECTOT
    this.w_CALCPICT = t_CALCPICT
    this.w_IMPABB = t_IMPABB
    this.w_SIMVAL = t_SIMVAL
    this.w_IN_FLCON = t_IN_FLCON
    this.w_IN_RIFCO = t_IN_RIFCO
    this.w_CODCON = t_CODCON
    this.w_ANDESCRI = t_ANDESCRI
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_INCODVAL with this.w_INCODVAL
    replace t_TIPCON with this.w_TIPCON
    replace t_CAOVAL with this.w_CAOVAL
    replace t_INTOTIMP with this.w_INTOTIMP
    replace t_INCAOAPE with this.w_INCAOAPE
    replace t_INDATAPE with this.w_INDATAPE
    replace t_INCAOVAL with this.w_INCAOVAL
    replace t_INIMPSAL with this.w_INIMPSAL
    replace t_INFLSALD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oINFLSALD_2_10.ToRadio()
    replace t_INIMPABB with this.w_INIMPABB
    replace t_INORICON with this.w_INORICON
    replace t_INORIDOC with this.w_INORIDOC
    replace t_NUMDOC with this.w_NUMDOC
    replace t_ALFDOC with this.w_ALFDOC
    replace t_DATDOC with this.w_DATDOC
    replace t_DARIMP with this.w_DARIMP
    replace t_DARSAL with this.w_DARSAL
    replace t_INTOTINC with this.w_INTOTINC
    replace t_NOTE with this.w_NOTE
    replace t_RAGSOC with this.w_RAGSOC
    replace t_DECTOT with this.w_DECTOT
    replace t_CALCPICT with this.w_CALCPICT
    replace t_IMPABB with this.w_IMPABB
    replace t_SIMVAL with this.w_SIMVAL
    replace t_IN_FLCON with this.w_IN_FLCON
    replace t_IN_RIFCO with this.w_IN_RIFCO
    replace t_CODCON with this.w_CODCON
    replace t_ANDESCRI with this.w_ANDESCRI
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTIMP = .w_TOTIMP-.w_darimp
        .w_TOTSAL = .w_TOTSAL-.w_darsal
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsve_micPag1 as StdContainer
  Width  = 694
  height = 435
  stdWidth  = 694
  stdheight = 435
  resizeXpos=244
  resizeYpos=262
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oINNUMDOC_1_3 as StdField with uid="OJMHJKDJAS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_INNUMDOC", cQueryName = "INNUMDOC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Progressivo numero documento",;
    HelpContextID = 111148855,;
   bGlobalFont=.t.,;
    Height=21, Width=120, Left=91, Top=12, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oINALFDOC_1_4 as StdField with uid="XNWNEDTWNG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_INALFDOC", cQueryName = "INALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 119131959,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=225, Top=12, cSayPict="'!!!!!!!!!'", cGetPict="'!!!!!!!!!'", InputMask=replicate('X',10)

  add object oINDATDOC_1_5 as StdField with uid="ZDUNJXJXET",rtseq=5,rtrep=.f.,;
    cFormVar = "w_INDATDOC", cQueryName = "INDATDOC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 105160503,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=410, Top=12

  add object oINCODCAU_1_10 as StdField with uid="OVEBAHFDSI",rtseq=7,rtrep=.f.,;
    cFormVar = "w_INCODCAU", cQueryName = "INCODCAU",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale incongruente o obsoleta",;
    ToolTipText = "Causale di contabilizzazione incassi",;
    HelpContextID = 137801509,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=91, Top=41, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , tabstop=.f., cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_INCODCAU"

  func oINCODCAU_1_10.mCond()
    with this.Parent.oContained
      return (g_COGE='S')
    endwith
  endfunc

  func oINCODCAU_1_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
    endif
  endfunc

  func oINCODCAU_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oINCODCAU_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINCODCAU_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oINCODCAU_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSVE_MIC.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oINCODCAU_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_INCODCAU
    i_obj.ecpSave()
  endproc

  add object oDESCAU_1_12 as StdField with uid="WISDMIFBSF",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 108116170,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=159, Top=41, InputMask=replicate('X',35)

  func oDESCAU_1_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
    endif
  endfunc

  add object oFILCLI_1_13 as StdField with uid="LRURTRGCMI",rtseq=9,rtrep=.f.,;
    cFormVar = "w_FILCLI", cQueryName = "FILCLI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale filtro su intestatario",;
    HelpContextID = 29500586,;
   bGlobalFont=.t.,;
    Height=21, Width=298, Left=91, Top=67, InputMask=replicate('X',40)

  func oFILCLI_1_13.mCond()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  func oFILCLI_1_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
    endif
  endfunc

  add object oVALSIM_1_23 as StdField with uid="YVSYAXGQKZ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_VALSIM", cQueryName = "VALSIM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 232926122,;
   bGlobalFont=.t.,;
    Height=21, Width=57, Left=342, Top=388, InputMask=replicate('X',5)


  add object oBtn_1_24 as StdButton with uid="MZOBBTSDHH",left=636, top=41, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per ricercare i corrispettivi da incassare";
    , HelpContextID = 180448278;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        do GSVE_BIC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_INCODCAU) AND .cFunction='Load')
    endwith
  endfunc

  func oBtn_1_24.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
   endif
  endfunc


  add object oObj_1_27 as cp_runprogram with uid="EPSNTOWVXE",left=68, top=444, width=168,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSVE_BS9("FLAG")',;
    cEvent = "w_INFLSALD Changed",;
    nPag=1;
    , HelpContextID = 181523686


  add object oObj_1_28 as cp_runprogram with uid="EQNYDBEEQL",left=68, top=463, width=202,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSVE_BS9("IMPO")',;
    cEvent = "w_INIMPSAL Changed",;
    nPag=1;
    , HelpContextID = 181523686


  add object oBtn_1_29 as StdButton with uid="SDUYSAPTKJ",left=58, top=390, width=48,height=45,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare la registrazione contabile generata";
    , HelpContextID = 79289210;
    , Tabstop=.f., Caption='\<Primanota';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_IN_RIFCO)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mCond()
    with this.Parent.oContained
      return (g_COGE='S' AND NOT EMPTY(.w_IN_RIFCO) AND .cFunction<>'Load')
    endwith
  endfunc

  func oBtn_1_29.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_IN_RIFCO))
    endwith
   endif
  endfunc


  add object oObj_1_30 as cp_runprogram with uid="EFUDSQFIXA",left=279, top=465, width=245,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSVE_BSC',;
    cEvent = "Record Updated,Record Inserted",;
    nPag=1;
    , ToolTipText = "Stampa del documento";
    , HelpContextID = 181523686


  add object oBtn_1_33 as StdButton with uid="FUGDWESDHV",left=6, top=389, width=48,height=45,;
    CpPicture="BMP\DOCUMENTI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il documento di origine dell'incasso";
    , HelpContextID = 58623846;
    , Tabstop=.f., Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_INORIDOC, -20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_INORIDOC))
    endwith
  endfunc

  func oBtn_1_33.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_INORIDOC))
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=91, width=689,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CPROWORD",Label1="Riga",Field2="NUMDOC",Label2="Doc.num.",Field3="ALFDOC",Label3="Serie",Field4="DATDOC",Label4="Del",Field5="SIMVAL",Label5="Val.",Field6="INTOTIMP",Label6="Importo da incassare",Field7="INIMPSAL",Label7="Incassato",Field8="INFLSALD",Label8="Saldo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 51181434


  add object oObj_1_35 as cp_runprogram with uid="EDHQPXQMPW",left=239, top=444, width=197,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSVE_BKC ('U')",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 181523686


  add object oObj_1_36 as cp_runprogram with uid="CZRNTFIWYH",left=68, top=482, width=197,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSVE_BKC ('C')",;
    cEvent = "Cancella",;
    nPag=1;
    , HelpContextID = 181523686

  add object oStr_1_6 as StdString with uid="IZBETIBTOT",Visible=.t., Left=1, Top=12,;
    Alignment=1, Width=89, Height=15,;
    Caption="Documento n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="GLWMMTRXDD",Visible=.t., Left=214, Top=12,;
    Alignment=2, Width=8, Height=15,;
    Caption="/"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="WAQOJUBWFB",Visible=.t., Left=340, Top=12,;
    Alignment=1, Width=66, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="CKVHVPVPTH",Visible=.t., Left=1, Top=41,;
    Alignment=1, Width=89, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="LESJRVZFOW",Visible=.t., Left=295, Top=389,;
    Alignment=1, Width=44, Height=15,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="NEZINVIQUZ",Visible=.t., Left=0, Top=362,;
    Alignment=1, Width=90, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="HIRFTQQKLW",Visible=.t., Left=13, Top=67,;
    Alignment=1, Width=77, Height=15,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=110,;
    width=685+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*13*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=111,width=684+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*13*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oINCAOVAL_2_8.Refresh()
      this.Parent.oRAGSOC_2_21.Refresh()
      this.Parent.oIMPABB_2_27.Refresh()
      this.Parent.oANDESCRI_2_32.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oINCAOVAL_2_8 as StdTrsField with uid="PVCGSQSNCL",rtseq=28,rtrep=.t.,;
    cFormVar="w_INCAOVAL",value=0,;
    ToolTipText = "Cambio attuale della scadenza",;
    HelpContextID = 191582418,;
    cTotal="", bFixedPos=.t., cQueryName = "INCAOVAL",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=314, Top=412, cSayPict=["99999.999999"], cGetPict=["99999.999999"]

  func oINCAOVAL_2_8.mCond()
    with this.Parent.oContained
      return (.w_INTOTIMP<>0 AND .w_CAOVAL=0)
    endwith
  endfunc

  func oINCAOVAL_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL<>0 OR .w_INTOTIMP=0)
    endwith
    endif
  endfunc

  add object oRAGSOC_2_21 as StdTrsField with uid="PNCBESXLEN",rtseq=41,rtrep=.t.,;
    cFormVar="w_RAGSOC",value=space(60),enabled=.f.,;
    HelpContextID = 125991914,;
    cTotal="", bFixedPos=.t., cQueryName = "RAGSOC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=284, Left=91, Top=361, InputMask=replicate('X',60)

  func oRAGSOC_2_21.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_ANDESCRI))
    endwith
    endif
  endfunc

  add object oObj_2_25 as cp_calclbl with uid="EWCRTTXCZG",width=96,height=17,;
   left=216, top=414,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=2;
    , HelpContextID = 181523686

  add object oObj_2_26 as cp_calclbl with uid="BLYGOHZQNY",width=96,height=17,;
   left=427, top=414,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=2;
    , HelpContextID = 181523686

  add object oIMPABB_2_27 as StdTrsField with uid="ZDSUOTZYYN",rtseq=48,rtrep=.t.,;
    cFormVar="w_IMPABB",value=0,enabled=.f.,;
    HelpContextID = 157540474,;
    cTotal="", bFixedPos=.t., cQueryName = "IMPABB",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=526, Top=412, cSayPict=[v_PV(20)], cGetPict=[v_GV(20)]

  func oIMPABB_2_27.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INFLSALD<>'S' OR .w_INTOTIMP=.w_INIMPSAL)
    endwith
    endif
  endfunc

  add object oANDESCRI_2_32 as StdTrsField with uid="CRGQPNDBUC",rtseq=53,rtrep=.t.,;
    cFormVar="w_ANDESCRI",value=space(60),enabled=.f.,;
    HelpContextID = 145711183,;
    cTotal="", bFixedPos=.t., cQueryName = "ANDESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=91, Top=361, InputMask=replicate('X',60)

  func oANDESCRI_2_32.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_ANDESCRI))
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTIMP1_3_3 as StdField with uid="CROPZZEPPS",rtseq=44,rtrep=.f.,;
    cFormVar="w_TOTIMP1",value=0,enabled=.f.,;
    HelpContextID = 89416246,;
    cQueryName = "TOTIMP1",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=401, Top=388, cSayPict=[v_PV(20)], cGetPict=[v_GV(20)]

  add object oTOTSAL1_3_4 as StdField with uid="MRAXXYSNXT",rtseq=45,rtrep=.f.,;
    cFormVar="w_TOTSAL1",value=0,enabled=.f.,;
    HelpContextID = 10379830,;
    cQueryName = "TOTSAL1",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=527, Top=388, cSayPict=[v_PV(20)], cGetPict=[v_GV(20)]
enddefine

* --- Defining Body row
define class tgsve_micBodyRow as CPBodyRowCnt
  Width=675
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="GZLVJSIYPU",rtseq=21,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 83509866,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oINTOTIMP_2_5 as StdTrsField with uid="XDEQPBUMWG",rtseq=25,rtrep=.t.,;
    cFormVar="w_INTOTIMP",value=0,enabled=.f.,;
    ToolTipText = "Totale importo da incassare",;
    HelpContextID = 20291370,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=121, Left=396, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oINIMPSAL_2_9 as StdTrsField with uid="VCYWKMBIWZ",rtseq=29,rtrep=.t.,;
    cFormVar="w_INIMPSAL",value=0,;
    ToolTipText = "Importo incassato",;
    HelpContextID = 143110354,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "L'importo incassato deve essere dello stesso segno dell'importo da incassare",;
   bGlobalFont=.t.,;
    Height=17, Width=121, Left=519, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  func oINIMPSAL_2_9.mCond()
    with this.Parent.oContained
      return (.w_INTOTIMP<>0)
    endwith
  endfunc

  func oINIMPSAL_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (SIGN(.w_INTOTIMP)=SIGN(.w_INIMPSAL) OR .w_INIMPSAL=0)
    endwith
    return bRes
  endfunc

  add object oINFLSALD_2_10 as StdTrsCheck with uid="XJEQVSGUZG",rtrep=.t.,;
    cFormVar="w_INFLSALD",  caption="",;
    ToolTipText = "Se attivo: partita saldata",;
    HelpContextID = 155811638,;
    Left=648, Top=0, Width=22,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
   , bGlobalFont=.t.


  func oINFLSALD_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..INFLSALD,&i_cF..t_INFLSALD),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oINFLSALD_2_10.GetRadio()
    this.Parent.oContained.w_INFLSALD = this.RadioValue()
    return .t.
  endfunc

  func oINFLSALD_2_10.ToRadio()
    this.Parent.oContained.w_INFLSALD=trim(this.Parent.oContained.w_INFLSALD)
    return(;
      iif(this.Parent.oContained.w_INFLSALD=='S',1,;
      0))
  endfunc

  func oINFLSALD_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oINFLSALD_2_10.mCond()
    with this.Parent.oContained
      return (.w_INTOTIMP<>0)
    endwith
  endfunc

  add object oNUMDOC_2_14 as StdTrsField with uid="DEKPDRDGYF",rtseq=34,rtrep=.t.,;
    cFormVar="w_NUMDOC",value=0,enabled=.f.,;
    ToolTipText = "Numero documento di origine della scadenza",;
    HelpContextID = 126945322,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=39, Top=0

  add object oALFDOC_2_15 as StdTrsField with uid="BNIQIKEOWI",rtseq=35,rtrep=.t.,;
    cFormVar="w_ALFDOC",value=space(10),enabled=.f.,;
    ToolTipText = "Numero documento di origine della scadenza",;
    HelpContextID = 126976506,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=107, Left=160, Top=0, InputMask=replicate('X',10)

  add object oDATDOC_2_16 as StdTrsField with uid="UPSVPVLDGE",rtseq=36,rtrep=.t.,;
    cFormVar="w_DATDOC",value=ctod("  /  /  "),enabled=.f.,;
    ToolTipText = "Data documento di origine della scadenza",;
    HelpContextID = 126921930,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=270, Top=0

  add object oObj_2_24 as cp_runprogram with uid="NTDGJXQLUM",width=216,height=21,;
   left=435, top=332,;
    caption='gsve_bmi',;
   bGlobalFont=.t.,;
    prg="GSVE_BMI",;
    cEvent = "w_INCAOVAL Changed",;
    nPag=2;
    , HelpContextID = 124606513

  add object oSIMVAL_2_28 as StdTrsField with uid="KAGWHWUBMU",rtseq=49,rtrep=.t.,;
    cFormVar="w_SIMVAL",value=space(5),enabled=.f.,;
    ToolTipText = "Valuta",;
    HelpContextID = 257889242,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=353, Top=0, InputMask=replicate('X',5)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=12
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_mic','INC_CORR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".INSERIAL=INC_CORR.INSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
