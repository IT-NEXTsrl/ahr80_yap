* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bhe                                                        *
*              Gestione hascpevent prima nota                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_68]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-12-05                                                      *
* Last revis.: 2016-05-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bhe",oParentObject)
return(i_retval)

define class tgscg_bhe as StdBatch
  * --- Local variables
  w_MESS = space(100)
  w_RITEGEN = .f.
  w_SOLLGEN = .f.
  w_IVFLSTOD = space(1)
  w_oMess = .NULL.
  w_RIFDIS = space(10)
  w_PNUMPAR = space(31)
  w_PTDATSCA = ctod("  /  /  ")
  w_PTTIPCON = space(1)
  w_PCODCON = space(15)
  w_PSEGNO = space(1)
  w_PTOTIMP = 0
  w_PCODVAL = space(3)
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_TIPGES = space(1)
  w_SERDIF = space(10)
  w_NUMERO = 0
  w_DATGEN = ctod("  /  /  ")
  w_DVSERIAL = space(10)
  w_APDESCRI = space(50)
  w_APDATREG = ctod("  /  /  ")
  * --- WorkFile variables
  ESI_DETT_idx=0
  PAR_TITE_idx=0
  INCDCORR_idx=0
  VALDATPA_idx=0
  VAL_ATPA_idx=0
  SALMDACO_idx=0
  ESI_DIF_idx=0
  PNT_IVA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Abilita / dsabilita le operazioni di cancellazione/modifica di registrazioni
    *     di prima nota (hasCpEvent area manuale declare..)
    this.oParentObject.w_HASEVENT = .t.
    if (Upper( this.oParentObject.w_HASEVCOP )="ECPDELETE")or(Upper( this.oParentObject.w_HASEVCOP )="ECPEDIT")
      if this.oParentObject.w_STAMPRCV<>"S"
        if g_RITE="S"
          * --- Verifica se la registrazione ha generato ritenute
          * --- Select from gsri_kcp
          do vq_exec with 'gsri_kcp',this,'_Curs_gsri_kcp','',.f.,.t.
          if used('_Curs_gsri_kcp')
            select _Curs_gsri_kcp
            locate for 1=1
            do while not(eof())
            this.w_RITEGEN = .T.
            this.w_MESS = "La registrazione ha generato movimenti di ritenute%0Impossibile modificare/cancellare"
            EXIT
              select _Curs_gsri_kcp
              continue
            enddo
            use
          endif
          if NOT this.w_RITEGEN
            * --- Select from gsri1kcp
            do vq_exec with 'gsri1kcp',this,'_Curs_gsri1kcp','',.f.,.t.
            if used('_Curs_gsri1kcp')
              select _Curs_gsri1kcp
              locate for 1=1
              do while not(eof())
              this.w_RITEGEN = .T.
              this.w_MESS = "La registrazione � collegata ad incassi che hanno generato movimenti di ritenute%0Impossibile modificare/cancellare"
              EXIT
                select _Curs_gsri1kcp
                continue
              enddo
              use
            endif
          endif
        endif
        if g_SOLL="S" AND NOT this.w_RITEGEN
          * --- Select from gscg4bhe
          do vq_exec with 'gscg4bhe',this,'_Curs_gscg4bhe','',.f.,.t.
          if used('_Curs_gscg4bhe')
            select _Curs_gscg4bhe
            locate for 1=1
            do while not(eof())
            this.w_MESS = "Il movimento di primanota contiene partite legate ad un contenzioso.%0Impossibile modificare/cancellare"
            this.w_SOLLGEN = .T.
            EXIT
              select _Curs_gscg4bhe
              continue
            enddo
            use
          endif
        endif
        * --- Select from PNT_IVA
        i_nConn=i_TableProp[this.PNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2],.t.,this.PNT_IVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select IVSERIAL,IVFLSTOD  from "+i_cTable+" PNT_IVA ";
              +" where IVSERIAL="+cp_ToStrODBC(this.oParentObject.w_PNSERIAL)+" AND IVFLSTOD='S'";
               ,"_Curs_PNT_IVA")
        else
          select IVSERIAL,IVFLSTOD from (i_cTable);
           where IVSERIAL=this.oParentObject.w_PNSERIAL AND IVFLSTOD="S";
            into cursor _Curs_PNT_IVA
        endif
        if used('_Curs_PNT_IVA')
          select _Curs_PNT_IVA
          locate for 1=1
          do while not(eof())
          this.w_IVFLSTOD = _Curs_PNT_IVA.IVFLSTOD
            select _Curs_PNT_IVA
            continue
          enddo
          use
        endif
        do case
          case this.w_RITEGEN
            ah_ErrorMsg(this.w_MESS,,"")
            this.oParentObject.w_HASEVENT = .F.
          case this.w_SOLLGEN
            ah_ErrorMsg(this.w_MESS,,"")
            this.oParentObject.w_HASEVENT = .F.
          case NOT EMPTY(NVL(this.oParentObject.w_PNRIFINC, " "))
            this.oParentObject.w_HASEVENT = ah_YesNo("Registrazione generata da incasso corrispettivi.%0Confermi ugualmente?")
          case Not Empty(CHKCONS(IIF(this.oParentObject.w_FLANAL="S","PC","P"),this.oParentObject.w_PNDATREG,"B","N"))
            this.w_MESS = CHKCONS(IIF(this.oParentObject.w_FLANAL="S","PC","P"),this.oParentObject.w_PNDATREG,"B","S")
            this.oParentObject.w_HASEVENT = .F.
          case NOT EMPTY(NVL(this.oParentObject.w_PNRIFDOC, " "))
            do case
              case NVL(this.oParentObject.w_PNRIFACC," ")="S"
                this.w_MESS = "Registrazione di acconto generata dai documenti%0Confermi ugualmente?"
              case this.oParentObject.w_PNRIFDOC="CORRISP"
                this.w_MESS = "Registrazione generata dai corrispettivi%0Confermi ugualmente?"
              otherwise
                this.w_MESS = "Registrazione generata dai documenti%0Confermi ugualmente?"
            endcase
            this.oParentObject.w_HASEVENT = ah_YesNo(this.w_MESS)
          case NOT EMPTY(NVL(this.oParentObject.w_PNFLGDIF, " ")) OR this.w_IVFLSTOD="S"
            this.w_oMess=createobject("Ah_Message")
            this.w_oMess.AddMsgPartNL("La registrazione � associata ad un movimento di giroconto IVA ad esigibilit� differita.")     
            this.w_oMess.AddMsgPartNL("Per cancellare o modificare questa registrazione � necessario eliminare il riferimento")     
            this.w_oMess.AddMsgPartNL("alla stessa dal piano di generazione movimenti ad esig. differita.")     
            this.w_oMess.Ah_ErrorMsg()     
            this.oParentObject.w_HASEVENT = .F.
          case NOT EMPTY(NVL(this.oParentObject.w_PNRIFDIS, " "))
            if VAL(this.oParentObject.w_PNRIFDIS)<0
              * --- Verfiico se la contabilizzazione indiretta ha tra le sue scadenze delle scadenze
              *     in distinte contabilizzabili, se � cos� inbisco la cancellazione / modifica
              this.w_RIFDIS = "0"+RIGHT(this.oParentObject.w_PNRIFDIS,9)
              * --- Recupero l'elenco delle partite associate alla contabilizzazione indiretta
              *     e una per una valuto se esiste una distinta contabilizzabile che le riferisce
              * --- Questa Query potrebbe essere una SELECT se non avesse al suo interno un'altra SELECT sulle PARTITE
              *     Filtro su PTRIFIND per selezionare le sole partite della contabilizzazione
              *     indiretta che hannoa generato la reg. di prima nota (nel caso di
              *     Generazione singola partita una contabilizzazione pi� registrazioni di prima nota)
              * --- Select from gscg_bhe
              do vq_exec with 'gscg_bhe',this,'_Curs_gscg_bhe','',.f.,.t.
              if used('_Curs_gscg_bhe')
                select _Curs_gscg_bhe
                locate for 1=1
                do while not(eof())
                * --- Aseegno i valori letti sulle scadenze
                this.w_PNUMPAR = _Curs_gscg_bhe.PTNUMPAR
                this.w_PTDATSCA = _Curs_gscg_bhe.PTDATSCA
                this.w_PTTIPCON = _Curs_gscg_bhe.PTTIPCON
                this.w_PCODCON = _Curs_gscg_bhe.PTCODCON
                this.w_PTOTIMP = _Curs_gscg_bhe.PTTOTIMP
                this.w_PCODVAL = _Curs_gscg_bhe.PTCODVAL
                this.w_PSEGNO = IIF(_Curs_gscg_bhe.PT_SEGNO="D","A","D")
                this.w_PTSERRIF = _Curs_gscg_bhe.PTSERIAL
                this.w_PTORDRIF = _Curs_gscg_bhe.PTROWORD
                this.w_PTNUMRIF = _Curs_gscg_bhe.CPROWNUM
                * --- Cerco una partita in distinta con stesso numero partita, data scadenza, 
                *     tipo e codice conto, valuta e di segno opposto 
                *     Se PTFLIMPE<>DI e quindi distinta che sia contabilizzabile inibisco ogni
                *     operazione altrimenti chiedo conferma
                * --- Select from GSCG1BHE
                do vq_exec with 'GSCG1BHE',this,'_Curs_GSCG1BHE','',.f.,.t.
                if used('_Curs_GSCG1BHE')
                  select _Curs_GSCG1BHE
                  locate for 1=1
                  do while not(eof())
                  if _Curs_GSCG1BHE.PTFLIMPE<>"DI"
                    ah_ErrorMsg("La registrazione di contabilizzazione indiretta effetti � gi� stata inserita in distinta.%0Impossibile cancellare",,"")
                    this.oParentObject.w_HASEVENT = .F.
                  else
                    this.oParentObject.w_HASEVENT = ah_YesNo("La registrazione di contabilizzazione indiretta effetti � gi� stata inserita in distinta al dopo incasso.%0Confermi ugualmente?")
                  endif
                  i_retcode = 'stop'
                  return
                    select _Curs_GSCG1BHE
                    continue
                  enddo
                  use
                endif
                  select _Curs_gscg_bhe
                  continue
                enddo
                use
              endif
              * --- Se cancello da contabilizzazione indiretta non do il messaggio
              if this.oParentObject.w_HASEVENT And Not TYPE("THIS.oparentobject.w_RIFGSCGACI.caption")="C"
                this.oParentObject.w_HASEVENT = ah_YesNo("Registrazione generata da contabilizzazione indiretta effetti.%0Confermi ugualmente?")
              endif
            else
              this.w_MESS = ""
              this.oParentObject.w_HASEVENT = ah_YesNo("Registrazione generata dalle distinte.%0Confermi ugualmente?")
            endif
          case "SOLL" $ UPPER(i_cModules) And this.oParentObject.w_PNRIFCES="C"
            this.oParentObject.w_HASEVENT = ah_YesNo("Registrazione generata dai contenziosi. Confermi ugualmente?")
          case NOT EMPTY(this.oParentObject.w_PNRIFSAL)
            * --- Read from SALMDACO
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.SALMDACO_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALMDACO_idx,2],.t.,this.SALMDACO_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "SCTIPGES"+;
                " from "+i_cTable+" SALMDACO where ";
                    +"SCSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNRIFSAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                SCTIPGES;
                from (i_cTable) where;
                    SCSERIAL = this.oParentObject.w_PNRIFSAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TIPGES = NVL(cp_ToDate(_read_.SCTIPGES),cp_NullValue(_read_.SCTIPGES))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if vartype(THIS.OPARENTOBJECT.w_RIFGSCGACI) = "O"
              if this.w_TIPGES="S" 
                * --- Evito di dare lo stesso messaggio pi� volte nel caso di cancellazione reg. di saldaconto 
                if lower(THIS.OPARENTOBJECT.w_RIFGSCGACI.class) <> "tgscg_msc" 
                  this.oParentObject.w_HASEVENT = ah_YesNo("Registrazione generata da saldaconto. Confermi ugualmente?")
                endif
              else
                if lower(THIS.OPARENTOBJECT.w_RIFGSCGACI.class) <> "tgscg_msc" 
                  this.oParentObject.w_HASEVENT = ah_YesNo("Registrazione generata da storno conti SBF. Confermi ugualmente?")
                endif
              endif
            else
              if this.w_TIPGES="S" 
                this.oParentObject.w_HASEVENT = ah_YesNo("Registrazione generata da saldaconto. Confermi ugualmente?")
              else
                this.oParentObject.w_HASEVENT = ah_YesNo("Registrazione generata da storno conti SBF. Confermi ugualmente?")
              endif
            endif
        endcase
        if this.oParentObject.w_HASEVENT and Upper( this.oParentObject.w_HASEVCOP )="ECPDELETE"
          * --- Controllo presenza di Incassi Ricevute Fiscali legate alla registrazione
          * --- Select from INCDCORR
          i_nConn=i_TableProp[this.INCDCORR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.INCDCORR_idx,2],.t.,this.INCDCORR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select INSERIAL  from "+i_cTable+" INCDCORR ";
                +" where INORICON = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL)+"";
                 ,"_Curs_INCDCORR")
          else
            select INSERIAL from (i_cTable);
             where INORICON = this.oParentObject.w_PNSERIAL;
              into cursor _Curs_INCDCORR
          endif
          if used('_Curs_INCDCORR')
            select _Curs_INCDCORR
            locate for 1=1
            do while not(eof())
            ah_errormsg("Per i corrispettivi sono presenti movimenti di incasso ricevute fiscali.%0Cancellazione non consentita!","","")
            this.oParentObject.w_HASEVENT = .F.
            EXIT
              select _Curs_INCDCORR
              continue
            enddo
            use
          endif
        endif
        if this.oParentObject.w_HASEVENT
          * --- Controllo cancellazione/modifica registrazione di Storno Iva ad esigibilit� differita
          * --- Read from ESI_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ESI_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ESI_DETT_idx,2],.t.,this.ESI_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DPSERIAL"+;
              " from "+i_cTable+" ESI_DETT where ";
                  +"DPSERMOV = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DPSERIAL;
              from (i_cTable) where;
                  DPSERMOV = this.oParentObject.w_PNSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SERDIF = NVL(cp_ToDate(_read_.DPSERIAL),cp_NullValue(_read_.DPSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from ESI_DIF
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ESI_DIF_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ESI_DIF_idx,2],.t.,this.ESI_DIF_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "EDNUMERO,EDDATGEN"+;
              " from "+i_cTable+" ESI_DIF where ";
                  +"EDSERIAL = "+cp_ToStrODBC(this.w_SERDIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              EDNUMERO,EDDATGEN;
              from (i_cTable) where;
                  EDSERIAL = this.w_SERDIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NUMERO = NVL(cp_ToDate(_read_.EDNUMERO),cp_NullValue(_read_.EDNUMERO))
            this.w_DATGEN = NVL(cp_ToDate(_read_.EDDATGEN),cp_NullValue(_read_.EDDATGEN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows<>0
            * --- Vado in veriazione solo nel caso di lemco attivo
            if (Upper( this.oParentObject.w_HASEVCOP )="ECPEDIT") AND G_LEMC="S"
              this.oParentObject.w_HASEVENT = ah_YesNo("Il movimento di storno � stato generato dal piano di generazione movimenti ad esig. differita n.%1 del %2. Confermi ugualmente?",,this.w_NUMERO,Dtoc(this.w_DATGEN))
            else
              this.w_MESS = "Il movimento di storno � stato generato.%0Per cancellarlo o modificarlo, eliminare il riferimento all'incasso/fattura in sospensione IVA relativo%0dal piano di generazione movimenti ad esig. differita n.%1 del %2"
              this.oParentObject.w_HASEVENT = .F.
              ah_ErrorMsg(this.w_MESS,,"",this.w_NUMERO,Dtoc(this.w_DATGEN))
            endif
          endif
        endif
        if g_PERPAR="S" And this.oParentObject.w_FLPART<>"N" And this.oParentObject.w_HASEVENT
          this.oParentObject.w_HASEVENT = gsar_bvc( this , this.oParentObject.w_PNSERIAL , "P" )
        endif
        if g_PERPAR="S" And this.oParentObject.w_HASEVENT And Upper( this.oParentObject.w_HASEVCOP )="ECPDELETE"
          this.w_DVSERIAL = Space(10)
          * --- Read from VALDATPA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALDATPA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALDATPA_idx,2],.t.,this.VALDATPA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DVSERIAL"+;
              " from "+i_cTable+" VALDATPA where ";
                  +"DVPNSERI = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DVSERIAL;
              from (i_cTable) where;
                  DVPNSERI = this.oParentObject.w_PNSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DVSERIAL = NVL(cp_ToDate(_read_.DVSERIAL),cp_NullValue(_read_.DVSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if not Empty( this.w_DVSERIAL )
            * --- Read from VAL_ATPA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VAL_ATPA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VAL_ATPA_idx,2],.t.,this.VAL_ATPA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "APDATREG,APDESCRI"+;
                " from "+i_cTable+" VAL_ATPA where ";
                    +"APSERIAL = "+cp_ToStrODBC(this.w_DVSERIAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                APDATREG,APDESCRI;
                from (i_cTable) where;
                    APSERIAL = this.w_DVSERIAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_APDATREG = NVL(cp_ToDate(_read_.APDATREG),cp_NullValue(_read_.APDATREG))
              this.w_APDESCRI = NVL(cp_ToDate(_read_.APDESCRI),cp_NullValue(_read_.APDESCRI))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_MESS = "La registrazione � stata generata da una valutazione attivit� e passivit� in divisa.%0Per cancellare eliminare la corrispondente valutazione:%0Seriale [%1] %2 del %3"
            ah_ErrorMsg(this.w_MESS,,"",this.w_DVSERIAL,Alltrim(this.w_APDESCRI),Dtoc(this.w_APDATREG))
            this.oParentObject.w_HASEVENT = .F.
          endif
        endif
        if this.oParentObject.w_HASEVENT and Upper( this.oParentObject.w_HASEVCOP )="ECPDELETE" and Isalt() and this.oParentObject.w_FLPART="S"
          gsal_baf(this,this.oParentObject.w_PNSERIAL,"P",Cp_chartodate("  -  -    "),Cp_chartodate("  -  -    "),"D")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        ah_ErrorMsg("Errore: data registrazione inferiore o uguale a ultima stampa registri IVA",,"")
        this.oParentObject.w_HASEVENT = .F.
      endif
    endif
    if g_CESP="S" AND this.oParentObject.w_MOVCES<>"S"
      * --- verifica se la tabella pnt_cesp � piena e se s�, abilita il bottone  movimenti cespiti anche se 
      *     il flag cespiti sulla causale contabile non � attivo
      * --- Select from gscg6bck
      do vq_exec with 'gscg6bck',this,'_Curs_gscg6bck','',.f.,.t.
      if used('_Curs_gscg6bck')
        select _Curs_gscg6bck
        locate for 1=1
        do while not(eof())
        this.oParentObject.w_ABILITA = .T.
        EXIT
          select _Curs_gscg6bck
          continue
        enddo
        use
      endif
    endif
    if this.oParentObject.w_ABILITA
      L_ctrl=THIS.OPARENTOBJECT.GetCtrl(Ah_MsgFormat("Mo\<v.Cespiti"))
      L_ctrl.VISIBLE=.T.
    else
      L_ctrl=THIS.OPARENTOBJECT.GetCtrl(Ah_MsgFormat("Mo\<v.Cespiti"))
      L_ctrl.VISIBLE=.F.
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='ESI_DETT'
    this.cWorkTables[2]='PAR_TITE'
    this.cWorkTables[3]='INCDCORR'
    this.cWorkTables[4]='VALDATPA'
    this.cWorkTables[5]='VAL_ATPA'
    this.cWorkTables[6]='SALMDACO'
    this.cWorkTables[7]='ESI_DIF'
    this.cWorkTables[8]='PNT_IVA'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_gsri_kcp')
      use in _Curs_gsri_kcp
    endif
    if used('_Curs_gsri1kcp')
      use in _Curs_gsri1kcp
    endif
    if used('_Curs_gscg4bhe')
      use in _Curs_gscg4bhe
    endif
    if used('_Curs_PNT_IVA')
      use in _Curs_PNT_IVA
    endif
    if used('_Curs_gscg_bhe')
      use in _Curs_gscg_bhe
    endif
    if used('_Curs_GSCG1BHE')
      use in _Curs_GSCG1BHE
    endif
    if used('_Curs_INCDCORR')
      use in _Curs_INCDCORR
    endif
    if used('_Curs_gscg6bck')
      use in _Curs_gscg6bck
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
