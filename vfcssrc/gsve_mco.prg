* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_mco                                                        *
*              Contratti di vendita                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_153]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-09                                                      *
* Last revis.: 2015-07-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_mco"))

* --- Class definition
define class tgsve_mco as StdTrsForm
  Top    = 1
  Left   = 4

  * --- Standard Properties
  Width  = 732
  Height = 498+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-07-09"
  HelpContextID=171298199
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=52

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  CON_TRAM_IDX = 0
  CON_TRAD_IDX = 0
  GRUMERC_IDX = 0
  ART_ICOL_IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  CON_COSC_IDX = 0
  CATECOMM_IDX = 0
  UNIMIS_IDX = 0
  cFile = "CON_TRAM"
  cFileDetail = "CON_TRAD"
  cKeySelect = "CONUMERO"
  cQueryFilter="COTIPCLF='C'"
  cKeyWhere  = "CONUMERO=this.w_CONUMERO"
  cKeyDetail  = "CONUMERO=this.w_CONUMERO and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"CONUMERO="+cp_ToStrODBC(this.w_CONUMERO)';

  cKeyDetailWhereODBC = '"CONUMERO="+cp_ToStrODBC(this.w_CONUMERO)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"CON_TRAD.CONUMERO="+cp_ToStrODBC(this.w_CONUMERO)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CON_TRAD.CPROWNUM '
  cPrg = "gsve_mco"
  cComment = "Contratti di vendita"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  cAutoZoom = 'GSVE_MCO'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_COTIPCLF = space(1)
  w_CO__TIPO = space(1)
  w_CONUMERO = space(15)
  w_CODATCON = ctod('  /  /  ')
  w_CODATINI = ctod('  /  /  ')
  w_CODATFIN = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_COCODCLF = space(15)
  o_COCODCLF = space(15)
  w_DESCLF = space(40)
  w_COCATCOM = space(3)
  o_COCATCOM = space(3)
  w_DESCAT = space(35)
  w_CLFVAL = space(3)
  w_CODESCON = space(50)
  w_CO__NOTE = space(0)
  w_COCODVAL = space(3)
  w_DESVAL = space(35)
  w_DECTOT = 0
  o_DECTOT = 0
  w_COIVALIS = space(1)
  w_COQUANTI = space(1)
  o_COQUANTI = space(1)
  w_COFLUCOA = space(1)
  o_COFLUCOA = space(1)
  w_CALCPICT = 0
  w_CPROWNUM = 0
  w_COGRUMER = space(5)
  o_COGRUMER = space(5)
  w_COCODART = space(20)
  o_COCODART = space(20)
  w_COPREZZO = 0
  w_COPERPRO = 0
  w_COPERCAP = 0
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_CODART = space(20)
  w_FLSERG = space(1)
  w_COUNIMIS = space(3)
  w_COSCONT1 = 0
  o_COSCONT1 = 0
  w_COSCONT2 = 0
  o_COSCONT2 = 0
  w_COSCONT3 = 0
  o_COSCONT3 = 0
  w_COSCONT4 = 0
  w_DESGRU = space(35)
  w_DESART = space(40)
  w_DESGRAR = space(45)
  w_DTOBSO = ctod('  /  /  ')
  w_COFLARTI = space(1)
  w_NORIGHE = .F.
  w_SCORPO = space(1)
  w_OBSART = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_RIGHERIP = .F.
  w_CONTRNUMROW = 0
  w_QUANTI = 0
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0

  * --- Children pointers
  GSVE_MSO = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CON_TRAM','gsve_mco')
    stdPageFrame::Init()
    *set procedure to GSVE_MSO additive
    with this
      .Pages(1).addobject("oPag","tgsve_mcoPag1","gsve_mco",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Contratti di vendita")
      .Pages(1).HelpContextID = 72796200
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCONUMERO_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSVE_MSO
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='GRUMERC'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='CON_COSC'
    this.cWorkTables[6]='CATECOMM'
    this.cWorkTables[7]='UNIMIS'
    this.cWorkTables[8]='CON_TRAM'
    this.cWorkTables[9]='CON_TRAD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(9))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CON_TRAM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CON_TRAM_IDX,3]
  return

  function CreateChildren()
    this.GSVE_MSO = CREATEOBJECT('stdDynamicChild',this,'GSVE_MSO',this.oPgFrm.Page1.oPag.oLinkPC_2_16)
    this.GSVE_MSO.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSVE_MSO)
      this.GSVE_MSO.DestroyChildrenChain()
      this.GSVE_MSO=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_16')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSVE_MSO.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSVE_MSO.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSVE_MSO.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSVE_MSO.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_CONUMERO,"COCODICE";
             ,.w_CPROWNUM,"CONUMROW";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_CONUMERO = NVL(CONUMERO,space(15))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_8_joined
    link_1_8_joined=.f.
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_1_15_joined
    link_1_15_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from CON_TRAM where CONUMERO=KeySet.CONUMERO
    *
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2],this.bLoadRecFilter,this.CON_TRAM_IDX,"gsve_mco")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CON_TRAM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CON_TRAM.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CON_TRAD.","CON_TRAM.")
      i_cTable = i_cTable+' CON_TRAM '
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CONUMERO',this.w_CONUMERO  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_DESCLF = space(40)
        .w_DESCAT = space(35)
        .w_CLFVAL = space(3)
        .w_DESVAL = space(35)
        .w_DECTOT = 0
        .w_FLSERG = space(1)
        .w_DTOBSO = ctod("  /  /  ")
        .w_NORIGHE = .F.
        .w_SCORPO = space(1)
        .w_RIGHERIP = .F.
        .w_CONTRNUMROW = 0
        .w_QUANTI = 0
        .w_COTIPCLF = NVL(COTIPCLF,space(1))
        .w_CO__TIPO = NVL(CO__TIPO,space(1))
        .w_CONUMERO = NVL(CONUMERO,space(15))
        .w_CODATCON = NVL(cp_ToDate(CODATCON),ctod("  /  /  "))
        .w_CODATINI = NVL(cp_ToDate(CODATINI),ctod("  /  /  "))
        .w_CODATFIN = NVL(cp_ToDate(CODATFIN),ctod("  /  /  "))
        .w_OBTEST = .w_CODATFIN
        .w_COCODCLF = NVL(COCODCLF,space(15))
          if link_1_8_joined
            this.w_COCODCLF = NVL(ANCODICE108,NVL(this.w_COCODCLF,space(15)))
            this.w_DESCLF = NVL(ANDESCRI108,space(40))
            this.w_CLFVAL = NVL(ANCODVAL108,space(3))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO108),ctod("  /  /  "))
            this.w_SCORPO = NVL(ANSCORPO108,space(1))
          else
          .link_1_8('Load')
          endif
        .w_COCATCOM = NVL(COCATCOM,space(3))
          if link_1_10_joined
            this.w_COCATCOM = NVL(CTCODICE110,NVL(this.w_COCATCOM,space(3)))
            this.w_DESCAT = NVL(CTDESCRI110,space(35))
          else
          .link_1_10('Load')
          endif
        .w_CODESCON = NVL(CODESCON,space(50))
        .w_CO__NOTE = NVL(CO__NOTE,space(0))
        .w_COCODVAL = NVL(COCODVAL,space(3))
          if link_1_15_joined
            this.w_COCODVAL = NVL(VACODVAL115,NVL(this.w_COCODVAL,space(3)))
            this.w_DESVAL = NVL(VADESVAL115,space(35))
            this.w_DECTOT = NVL(VADECUNI115,0)
            this.w_DTOBSO = NVL(cp_ToDate(VADTOBSO115),ctod("  /  /  "))
          else
          .link_1_15('Load')
          endif
        .w_COIVALIS = NVL(COIVALIS,space(1))
        .w_COQUANTI = NVL(COQUANTI,space(1))
        .w_COFLUCOA = NVL(COFLUCOA,space(1))
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_COFLARTI = NVL(COFLARTI,space(1))
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        cp_LoadRecExtFlds(this,'CON_TRAM')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CON_TRAD where CONUMERO=KeySet.CONUMERO
      *                            and CPROWNUM=KeySet.CPROWNUM
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsve_mco
      * --- Setta Ordine per Gr.Merceologico, Articolo, Variante
      i_cOrder = 'order by COCODART, COGRUMER '
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CON_TRAD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAD_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CON_TRAD')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CON_TRAD.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CON_TRAD"
        link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'CONUMERO',this.w_CONUMERO  )
        select * from (i_cTable) CON_TRAD where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_CODART = space(20)
          .w_DESGRU = space(35)
          .w_DESART = space(40)
          .w_OBSART = ctod("  /  /  ")
          .w_DATOBSO = ctod("  /  /  ")
          .w_CPROWNUM = CPROWNUM
          .w_CPROWNUM = NVL(CPROWNUM,0)
          .w_COGRUMER = NVL(COGRUMER,space(5))
          if link_2_2_joined
            this.w_COGRUMER = NVL(GMCODICE202,NVL(this.w_COGRUMER,space(5)))
            this.w_DESGRU = NVL(GMDESCRI202,space(35))
          else
          .link_2_2('Load')
          endif
          .w_COCODART = NVL(COCODART,space(20))
          if link_2_3_joined
            this.w_COCODART = NVL(ARCODART203,NVL(this.w_COCODART,space(20)))
            this.w_DESART = NVL(ARDESART203,space(40))
            this.w_OBSART = NVL(cp_ToDate(ARDTOBSO203),ctod("  /  /  "))
            this.w_UNMIS1 = NVL(ARUNMIS1203,space(3))
            this.w_UNMIS2 = NVL(ARUNMIS2203,space(3))
            this.w_FLSERG = NVL(ARFLSERG203,space(1))
            this.w_COUNIMIS = NVL(ARUNMIS1203,space(3))
            this.w_CODART = NVL(ARCODART203,space(20))
          else
          .link_2_3('Load')
          endif
          .w_COPREZZO = NVL(COPREZZO,0)
          .w_COPERPRO = NVL(COPERPRO,0)
          .w_COPERCAP = NVL(COPERCAP,0)
          .w_COUNIMIS = NVL(COUNIMIS,space(3))
          * evitabile
          *.link_2_11('Load')
          .w_COSCONT1 = NVL(COSCONT1,0)
          .w_COSCONT2 = NVL(COSCONT2,0)
          .w_COSCONT3 = NVL(COSCONT3,0)
          .w_COSCONT4 = NVL(COSCONT4,0)
        .w_DESGRAR = IIF(EMPTY(.w_COGRUMER), .w_DESART, .w_DESGRU)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_OBTEST = .w_CODATFIN
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsve_mco
    if this.w_COTIPCLF<>'C'
      this.BlankRec()
    endif
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_COTIPCLF=space(1)
      .w_CO__TIPO=space(1)
      .w_CONUMERO=space(15)
      .w_CODATCON=ctod("  /  /  ")
      .w_CODATINI=ctod("  /  /  ")
      .w_CODATFIN=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_COCODCLF=space(15)
      .w_DESCLF=space(40)
      .w_COCATCOM=space(3)
      .w_DESCAT=space(35)
      .w_CLFVAL=space(3)
      .w_CODESCON=space(50)
      .w_CO__NOTE=space(0)
      .w_COCODVAL=space(3)
      .w_DESVAL=space(35)
      .w_DECTOT=0
      .w_COIVALIS=space(1)
      .w_COQUANTI=space(1)
      .w_COFLUCOA=space(1)
      .w_CALCPICT=0
      .w_CPROWNUM=0
      .w_COGRUMER=space(5)
      .w_COCODART=space(20)
      .w_COPREZZO=0
      .w_COPERPRO=0
      .w_COPERCAP=0
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_CODART=space(20)
      .w_FLSERG=space(1)
      .w_COUNIMIS=space(3)
      .w_COSCONT1=0
      .w_COSCONT2=0
      .w_COSCONT3=0
      .w_COSCONT4=0
      .w_DESGRU=space(35)
      .w_DESART=space(40)
      .w_DESGRAR=space(45)
      .w_DTOBSO=ctod("  /  /  ")
      .w_COFLARTI=space(1)
      .w_NORIGHE=.f.
      .w_SCORPO=space(1)
      .w_OBSART=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_RIGHERIP=.f.
      .w_CONTRNUMROW=0
      .w_QUANTI=0
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      if .cFunction<>"Filter"
        .w_COTIPCLF = 'C'
        .w_CO__TIPO = 'C'
        .DoRTCalc(3,3,.f.)
        .w_CODATCON = i_datsys
        .w_CODATINI = I_DATSYS
        .DoRTCalc(6,6,.f.)
        .w_OBTEST = .w_CODATFIN
        .w_COCODCLF = SPACE(15)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_COCODCLF))
         .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        .w_COCATCOM = SPACE(3)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_COCATCOM))
         .link_1_10('Full')
        endif
        .DoRTCalc(11,14,.f.)
        .w_COCODVAL = IIF(EMPTY(.w_CLFVAL), g_PERVAL, .w_CLFVAL)
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_COCODVAL))
         .link_1_15('Full')
        endif
        .DoRTCalc(16,17,.f.)
        .w_COIVALIS = IIF( .w_SCORPO = 'S', 'L', 'N')
        .DoRTCalc(19,19,.f.)
        .w_COFLUCOA = 'N'
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .DoRTCalc(22,22,.f.)
        .w_COGRUMER = SPACE(5)
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_COGRUMER))
         .link_2_2('Full')
        endif
        .w_COCODART = SPACE(20)
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_COCODART))
         .link_2_3('Full')
        endif
        .w_COPREZZO = 0
        .w_COPERPRO = 0
        .w_COPERCAP = 0
        .DoRTCalc(28,31,.f.)
        .w_COUNIMIS = .w_UNMIS1
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_COUNIMIS))
         .link_2_11('Full')
        endif
        .DoRTCalc(33,33,.f.)
        .w_COSCONT2 = 0
        .w_COSCONT3 = 0
        .w_COSCONT4 = 0
        .DoRTCalc(37,38,.f.)
        .w_DESGRAR = IIF(EMPTY(.w_COGRUMER), .w_DESART, .w_DESGRU)
        .DoRTCalc(40,40,.f.)
        .w_COFLARTI = 'N'
        .w_NORIGHE = .F.
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(43,45,.f.)
        .w_RIGHERIP = .F.
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CON_TRAM')
    this.DoRTCalc(47,52,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCONUMERO_1_3.enabled = i_bVal
      .Page1.oPag.oCODATCON_1_4.enabled = i_bVal
      .Page1.oPag.oCODATINI_1_5.enabled = i_bVal
      .Page1.oPag.oCODATFIN_1_6.enabled = i_bVal
      .Page1.oPag.oCOCODCLF_1_8.enabled = i_bVal
      .Page1.oPag.oCOCATCOM_1_10.enabled = i_bVal
      .Page1.oPag.oCODESCON_1_13.enabled = i_bVal
      .Page1.oPag.oCO__NOTE_1_14.enabled = i_bVal
      .Page1.oPag.oCOCODVAL_1_15.enabled = i_bVal
      .Page1.oPag.oCOIVALIS_1_18.enabled = i_bVal
      .Page1.oPag.oCOQUANTI_1_19.enabled = i_bVal
      .Page1.oPag.oCOFLUCOA_1_20.enabled = i_bVal
      .Page1.oPag.oCOUNIMIS_2_11.enabled = i_bVal
      .Page1.oPag.oCOSCONT1_2_12.enabled = i_bVal
      .Page1.oPag.oCOSCONT2_2_13.enabled = i_bVal
      .Page1.oPag.oCOSCONT3_2_14.enabled = i_bVal
      .Page1.oPag.oCOSCONT4_2_15.enabled = i_bVal
      .Page1.oPag.oCOFLARTI_1_35.enabled = i_bVal
      .Page1.oPag.oObj_1_38.enabled = i_bVal
      .Page1.oPag.oObj_1_39.enabled = i_bVal
      .Page1.oPag.oObj_1_40.enabled = i_bVal
      .Page1.oPag.oObj_1_41.enabled = i_bVal
      .Page1.oPag.oObj_1_44.enabled = i_bVal
      .Page1.oPag.oObj_1_49.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCONUMERO_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCONUMERO_1_3.enabled = .t.
        .Page1.oPag.oCODATCON_1_4.enabled = .t.
        .Page1.oPag.oCODATINI_1_5.enabled = .t.
        .Page1.oPag.oCODATFIN_1_6.enabled = .t.
        .Page1.oPag.oCOCODCLF_1_8.enabled = .t.
        .Page1.oPag.oCOCATCOM_1_10.enabled = .t.
      endif
    endwith
    this.GSVE_MSO.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CON_TRAM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSVE_MSO.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPCLF,"COTIPCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CO__TIPO,"CO__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CONUMERO,"CONUMERO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATCON,"CODATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATINI,"CODATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATFIN,"CODATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODCLF,"COCODCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCATCOM,"COCATCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODESCON,"CODESCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CO__NOTE,"CO__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODVAL,"COCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COIVALIS,"COIVALIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COQUANTI,"COQUANTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLUCOA,"COFLUCOA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLARTI,"COFLARTI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsve_mco
    * --- aggiunge alla Chiave ulteriore filtro su Ciclo
    IF NOT EMPTY(i_cWhere)
       IF AT('COTIPCLF', UPPER(i_cWhere))=0
          i_cWhere=i_cWhere+" and COTIPCLF='C'"
       ENDIF
    ENDIF
    
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    i_lTable = "CON_TRAM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CON_TRAM_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSVE_SCO with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWNUM N(4);
      ,t_COGRUMER C(5);
      ,t_COCODART C(20);
      ,t_COPREZZO N(18,5);
      ,t_COPERPRO N(5,2);
      ,t_COPERCAP N(5,2);
      ,t_UNMIS1 C(3);
      ,t_COUNIMIS C(3);
      ,t_COSCONT1 N(6,2);
      ,t_COSCONT2 N(6,2);
      ,t_COSCONT3 N(6,2);
      ,t_COSCONT4 N(6,2);
      ,t_DESGRAR C(45);
      ,CPROWNUM N(10);
      ,t_UNMIS2 C(3);
      ,t_CODART C(20);
      ,t_DESGRU C(35);
      ,t_DESART C(40);
      ,t_OBSART D(8);
      ,t_DATOBSO D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsve_mcobodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWNUM_2_1.controlsource=this.cTrsName+'.t_CPROWNUM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOGRUMER_2_2.controlsource=this.cTrsName+'.t_COGRUMER'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOCODART_2_3.controlsource=this.cTrsName+'.t_COCODART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOPREZZO_2_4.controlsource=this.cTrsName+'.t_COPREZZO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOPERPRO_2_5.controlsource=this.cTrsName+'.t_COPERPRO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOPERCAP_2_6.controlsource=this.cTrsName+'.t_COPERCAP'
    this.oPgFRm.Page1.oPag.oUNMIS1_2_7.controlsource=this.cTrsName+'.t_UNMIS1'
    this.oPgFRm.Page1.oPag.oCOUNIMIS_2_11.controlsource=this.cTrsName+'.t_COUNIMIS'
    this.oPgFRm.Page1.oPag.oCOSCONT1_2_12.controlsource=this.cTrsName+'.t_COSCONT1'
    this.oPgFRm.Page1.oPag.oCOSCONT2_2_13.controlsource=this.cTrsName+'.t_COSCONT2'
    this.oPgFRm.Page1.oPag.oCOSCONT3_2_14.controlsource=this.cTrsName+'.t_COSCONT3'
    this.oPgFRm.Page1.oPag.oCOSCONT4_2_15.controlsource=this.cTrsName+'.t_COSCONT4'
    this.oPgFRm.Page1.oPag.oDESGRAR_2_19.controlsource=this.cTrsName+'.t_DESGRAR'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(62)
    this.AddVLine(223)
    this.AddVLine(338)
    this.AddVLine(401)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWNUM_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CON_TRAM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CON_TRAM')
        i_extval=cp_InsertValODBCExtFlds(this,'CON_TRAM')
        local i_cFld
        i_cFld=" "+;
                  "(COTIPCLF,CO__TIPO,CONUMERO,CODATCON,CODATINI"+;
                  ",CODATFIN,COCODCLF,COCATCOM,CODESCON,CO__NOTE"+;
                  ",COCODVAL,COIVALIS,COQUANTI,COFLUCOA,COFLARTI"+;
                  ",UTCC,UTDC,UTDV,UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_COTIPCLF)+;
                    ","+cp_ToStrODBC(this.w_CO__TIPO)+;
                    ","+cp_ToStrODBC(this.w_CONUMERO)+;
                    ","+cp_ToStrODBC(this.w_CODATCON)+;
                    ","+cp_ToStrODBC(this.w_CODATINI)+;
                    ","+cp_ToStrODBC(this.w_CODATFIN)+;
                    ","+cp_ToStrODBCNull(this.w_COCODCLF)+;
                    ","+cp_ToStrODBCNull(this.w_COCATCOM)+;
                    ","+cp_ToStrODBC(this.w_CODESCON)+;
                    ","+cp_ToStrODBC(this.w_CO__NOTE)+;
                    ","+cp_ToStrODBCNull(this.w_COCODVAL)+;
                    ","+cp_ToStrODBC(this.w_COIVALIS)+;
                    ","+cp_ToStrODBC(this.w_COQUANTI)+;
                    ","+cp_ToStrODBC(this.w_COFLUCOA)+;
                    ","+cp_ToStrODBC(this.w_COFLARTI)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CON_TRAM')
        i_extval=cp_InsertValVFPExtFlds(this,'CON_TRAM')
        cp_CheckDeletedKey(i_cTable,0,'CONUMERO',this.w_CONUMERO)
        INSERT INTO (i_cTable);
              (COTIPCLF,CO__TIPO,CONUMERO,CODATCON,CODATINI,CODATFIN,COCODCLF,COCATCOM,CODESCON,CO__NOTE,COCODVAL,COIVALIS,COQUANTI,COFLUCOA,COFLARTI,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_COTIPCLF;
                  ,this.w_CO__TIPO;
                  ,this.w_CONUMERO;
                  ,this.w_CODATCON;
                  ,this.w_CODATINI;
                  ,this.w_CODATFIN;
                  ,this.w_COCODCLF;
                  ,this.w_COCATCOM;
                  ,this.w_CODESCON;
                  ,this.w_CO__NOTE;
                  ,this.w_COCODVAL;
                  ,this.w_COIVALIS;
                  ,this.w_COQUANTI;
                  ,this.w_COFLUCOA;
                  ,this.w_COFLARTI;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CON_TRAD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAD_IDX,2])
      *
      * insert into CON_TRAD
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(CONUMERO,COGRUMER,COCODART,COPREZZO,COPERPRO"+;
                  ",COPERCAP,COUNIMIS,COSCONT1,COSCONT2,COSCONT3"+;
                  ",COSCONT4,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CONUMERO)+","+cp_ToStrODBCNull(this.w_COGRUMER)+","+cp_ToStrODBCNull(this.w_COCODART)+","+cp_ToStrODBC(this.w_COPREZZO)+","+cp_ToStrODBC(this.w_COPERPRO)+;
             ","+cp_ToStrODBC(this.w_COPERCAP)+","+cp_ToStrODBCNull(this.w_COUNIMIS)+","+cp_ToStrODBC(this.w_COSCONT1)+","+cp_ToStrODBC(this.w_COSCONT2)+","+cp_ToStrODBC(this.w_COSCONT3)+;
             ","+cp_ToStrODBC(this.w_COSCONT4)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CONUMERO',this.w_CONUMERO,'CPROWNUM',this.w_CPROWNUM)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_CONUMERO,this.w_COGRUMER,this.w_COCODART,this.w_COPREZZO,this.w_COPERPRO"+;
                ",this.w_COPERCAP,this.w_COUNIMIS,this.w_COSCONT1,this.w_COSCONT2,this.w_COSCONT3"+;
                ",this.w_COSCONT4,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update CON_TRAM
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'CON_TRAM')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " COTIPCLF="+cp_ToStrODBC(this.w_COTIPCLF)+;
             ",CO__TIPO="+cp_ToStrODBC(this.w_CO__TIPO)+;
             ",CODATCON="+cp_ToStrODBC(this.w_CODATCON)+;
             ",CODATINI="+cp_ToStrODBC(this.w_CODATINI)+;
             ",CODATFIN="+cp_ToStrODBC(this.w_CODATFIN)+;
             ",COCODCLF="+cp_ToStrODBCNull(this.w_COCODCLF)+;
             ",COCATCOM="+cp_ToStrODBCNull(this.w_COCATCOM)+;
             ",CODESCON="+cp_ToStrODBC(this.w_CODESCON)+;
             ",CO__NOTE="+cp_ToStrODBC(this.w_CO__NOTE)+;
             ",COCODVAL="+cp_ToStrODBCNull(this.w_COCODVAL)+;
             ",COIVALIS="+cp_ToStrODBC(this.w_COIVALIS)+;
             ",COQUANTI="+cp_ToStrODBC(this.w_COQUANTI)+;
             ",COFLUCOA="+cp_ToStrODBC(this.w_COFLUCOA)+;
             ",COFLARTI="+cp_ToStrODBC(this.w_COFLARTI)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'CON_TRAM')
          i_cWhere = cp_PKFox(i_cTable  ,'CONUMERO',this.w_CONUMERO  )
          UPDATE (i_cTable) SET;
              COTIPCLF=this.w_COTIPCLF;
             ,CO__TIPO=this.w_CO__TIPO;
             ,CODATCON=this.w_CODATCON;
             ,CODATINI=this.w_CODATINI;
             ,CODATFIN=this.w_CODATFIN;
             ,COCODCLF=this.w_COCODCLF;
             ,COCATCOM=this.w_COCATCOM;
             ,CODESCON=this.w_CODESCON;
             ,CO__NOTE=this.w_CO__NOTE;
             ,COCODVAL=this.w_COCODVAL;
             ,COIVALIS=this.w_COIVALIS;
             ,COQUANTI=this.w_COQUANTI;
             ,COFLUCOA=this.w_COFLUCOA;
             ,COFLARTI=this.w_COFLARTI;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_COGRUMER) OR NOT EMPTY(t_COCODART)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CON_TRAD_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAD_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Deleting row children
              this.GSVE_MSO.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_CONUMERO,"COCODICE";
                     ,this.w_CPROWNUM,"CONUMROW";
                     )
              this.GSVE_MSO.mDelete()
              *
              * delete from CON_TRAD
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CON_TRAD
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " COGRUMER="+cp_ToStrODBCNull(this.w_COGRUMER)+;
                     ",COCODART="+cp_ToStrODBCNull(this.w_COCODART)+;
                     ",COPREZZO="+cp_ToStrODBC(this.w_COPREZZO)+;
                     ",COPERPRO="+cp_ToStrODBC(this.w_COPERPRO)+;
                     ",COPERCAP="+cp_ToStrODBC(this.w_COPERCAP)+;
                     ",COUNIMIS="+cp_ToStrODBCNull(this.w_COUNIMIS)+;
                     ",COSCONT1="+cp_ToStrODBC(this.w_COSCONT1)+;
                     ",COSCONT2="+cp_ToStrODBC(this.w_COSCONT2)+;
                     ",COSCONT3="+cp_ToStrODBC(this.w_COSCONT3)+;
                     ",COSCONT4="+cp_ToStrODBC(this.w_COSCONT4)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      COGRUMER=this.w_COGRUMER;
                     ,COCODART=this.w_COCODART;
                     ,COPREZZO=this.w_COPREZZO;
                     ,COPERPRO=this.w_COPERPRO;
                     ,COPERCAP=this.w_COPERCAP;
                     ,COUNIMIS=this.w_COUNIMIS;
                     ,COSCONT1=this.w_COSCONT1;
                     ,COSCONT2=this.w_COSCONT2;
                     ,COSCONT3=this.w_COSCONT3;
                     ,COSCONT4=this.w_COSCONT4;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask children belonging to rows to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (NOT EMPTY(t_COGRUMER) OR NOT EMPTY(t_COCODART))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        this.GSVE_MSO.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_CONUMERO,"COCODICE";
             ,this.w_CPROWNUM,"CONUMROW";
             )
        this.GSVE_MSO.mReplace()
        this.GSVE_MSO.bSaveContext=.f.
      endscan
     this.GSVE_MSO.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)2
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_COGRUMER) OR NOT EMPTY(t_COCODART)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSVE_MSO : Deleting
        this.GSVE_MSO.bSaveContext=.f.
        this.GSVE_MSO.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_CONUMERO,"COCODICE";
               ,this.w_CPROWNUM,"CONUMROW";
               )
        this.GSVE_MSO.bSaveContext=.t.
        this.GSVE_MSO.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CON_TRAD_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAD_IDX,2])
        *
        * delete CON_TRAD
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
        *
        * delete CON_TRAM
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_COGRUMER) OR NOT EMPTY(t_COCODART)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    if i_bUpd
      with this
          .w_COTIPCLF = 'C'
          .w_CO__TIPO = 'C'
        .DoRTCalc(3,6,.t.)
          .w_OBTEST = .w_CODATFIN
        if .o_COCATCOM<>.w_COCATCOM
          .w_COCODCLF = SPACE(15)
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.t.)
        if .o_COCODCLF<>.w_COCODCLF
          .w_COCATCOM = SPACE(3)
          .link_1_10('Full')
        endif
        .DoRTCalc(11,14,.t.)
        if .o_COCODCLF<>.w_COCODCLF
          .w_COCODVAL = IIF(EMPTY(.w_CLFVAL), g_PERVAL, .w_CLFVAL)
          .link_1_15('Full')
        endif
        .DoRTCalc(16,17,.t.)
        if .o_COCODCLF<>.w_COCODCLF
          .w_COIVALIS = IIF( .w_SCORPO = 'S', 'L', 'N')
        endif
        .DoRTCalc(19,19,.t.)
        if .o_COQUANTI<>.w_COQUANTI
          .w_COFLUCOA = 'N'
        endif
        if .o_DECTOT<>.w_DECTOT
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(22,22,.t.)
        if .o_COCODART<>.w_COCODART
          .w_COGRUMER = SPACE(5)
          .link_2_2('Full')
        endif
        if .o_COGRUMER<>.w_COGRUMER
          .w_COCODART = SPACE(20)
          .link_2_3('Full')
        endif
        if .o_COQUANTI<>.w_COQUANTI.or. .o_COGRUMER<>.w_COGRUMER.or. .o_COFLUCOA<>.w_COFLUCOA
          .w_COPREZZO = 0
        endif
        if .o_COQUANTI<>.w_COQUANTI
          .w_COPERPRO = 0
        endif
        if .o_COQUANTI<>.w_COQUANTI
          .w_COPERCAP = 0
        endif
        .DoRTCalc(28,31,.t.)
        if .o_COCODART<>.w_COCODART
          .w_COUNIMIS = .w_UNMIS1
          .link_2_11('Full')
        endif
        .DoRTCalc(33,33,.t.)
        if .o_COSCONT1<>.w_COSCONT1
          .w_COSCONT2 = 0
        endif
        if .o_COSCONT2<>.w_COSCONT2
          .w_COSCONT3 = 0
        endif
        if .o_COSCONT3<>.w_COSCONT3
          .w_COSCONT4 = 0
        endif
        .DoRTCalc(37,38,.t.)
          .w_DESGRAR = IIF(EMPTY(.w_COGRUMER), .w_DESART, .w_DESGRU)
        .DoRTCalc(40,40,.t.)
        if .o_COFLUCOA<>.w_COFLUCOA
          .w_COFLARTI = 'N'
        endif
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(42,52,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_CODART with this.w_CODART
      replace t_DESGRU with this.w_DESGRU
      replace t_DESART with this.w_DESART
      replace t_OBSART with this.w_OBSART
      replace t_DATOBSO with this.w_DATOBSO
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCOCODCLF_1_8.enabled = this.oPgFrm.Page1.oPag.oCOCODCLF_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCOCATCOM_1_10.enabled = this.oPgFrm.Page1.oPag.oCOCATCOM_1_10.mCond()
    this.oPgFrm.Page1.oPag.oCOFLUCOA_1_20.enabled = this.oPgFrm.Page1.oPag.oCOFLUCOA_1_20.mCond()
    this.oPgFrm.Page1.oPag.oCOFLARTI_1_35.enabled = this.oPgFrm.Page1.oPag.oCOFLARTI_1_35.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOGRUMER_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOGRUMER_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOCODART_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOCODART_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOPREZZO_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOPREZZO_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOPERPRO_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOPERPRO_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOPERCAP_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOPERCAP_2_6.mCond()
    this.oPgFrm.Page1.oPag.oCOUNIMIS_2_11.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCOUNIMIS_2_11.mCond()
    this.oPgFrm.Page1.oPag.oCOSCONT1_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCOSCONT1_2_12.mCond()
    this.oPgFrm.Page1.oPag.oCOSCONT2_2_13.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCOSCONT2_2_13.mCond()
    this.oPgFrm.Page1.oPag.oCOSCONT3_2_14.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCOSCONT3_2_14.mCond()
    this.oPgFrm.Page1.oPag.oCOSCONT4_2_15.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oCOSCONT4_2_15.mCond()
    this.GSVE_MSO.enabled = this.oPgFrm.Page1.oPag.oLinkPC_2_16.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCOFLARTI_1_35.visible=!this.oPgFrm.Page1.oPag.oCOFLARTI_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oCOUNIMIS_2_11.visible=!this.oPgFrm.Page1.oPag.oCOUNIMIS_2_11.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COCODCLF
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANSCORPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCLF;
                     ,'ANCODICE',trim(this.w_COCODCLF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANSCORPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANSCORPO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANSCORPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCODCLF_1_8'),i_cWhere,'GSAR_BZC',"Elenco clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANSCORPO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANSCORPO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Inserire il cliente (non obsoleto) o la categoria commerciale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANSCORPO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANSCORPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANSCORPO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCLF;
                       ,'ANCODICE',this.w_COCODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCODVAL,ANDTOBSO,ANSCORPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(40))
      this.w_CLFVAL = NVL(_Link_.ANCODVAL,space(3))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_SCORPO = NVL(_Link_.ANSCORPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCODCLF = space(15)
      endif
      this.w_DESCLF = space(40)
      this.w_CLFVAL = space(3)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_SCORPO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(NOT EMPTY(.w_COCATCOM) OR NOT EMPTY(.w_COCODCLF)) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST<=.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire il cliente (non obsoleto) o la categoria commerciale")
        endif
        this.w_COCODCLF = space(15)
        this.w_DESCLF = space(40)
        this.w_CLFVAL = space(3)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_SCORPO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.ANCODICE as ANCODICE108"+ ",link_1_8.ANDESCRI as ANDESCRI108"+ ",link_1_8.ANCODVAL as ANCODVAL108"+ ",link_1_8.ANDTOBSO as ANDTOBSO108"+ ",link_1_8.ANSCORPO as ANSCORPO108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on CON_TRAM.COCODCLF=link_1_8.ANCODICE"+" and CON_TRAM.COTIPCLF=link_1_8.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and CON_TRAM.COCODCLF=link_1_8.ANCODICE(+)"'+'+" and CON_TRAM.COTIPCLF=link_1_8.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCATCOM
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_COCATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_COCATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCOCATCOM_1_10'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_COCATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_COCATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCAT = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_COCATCOM = space(3)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=NOT EMPTY(.w_COCATCOM) OR NOT EMPTY(.w_COCODCLF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire il cliente o la categoria commerciale")
        endif
        this.w_COCATCOM = space(3)
        this.w_DESCAT = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATECOMM_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.CTCODICE as CTCODICE110"+ ",link_1_10.CTDESCRI as CTDESCRI110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on CON_TRAM.COCATCOM=link_1_10.CTCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and CON_TRAM.COCATCOM=link_1_10.CTCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCODVAL
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_COCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECUNI,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_COCODVAL))
          select VACODVAL,VADESVAL,VADECUNI,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_COCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECUNI,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_COCODVAL)+"%");

            select VACODVAL,VADESVAL,VADECUNI,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCOCODVAL_1_15'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECUNI,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECUNI,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECUNI,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_COCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_COCODVAL)
            select VACODVAL,VADESVAL,VADECUNI,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_DECTOT = NVL(_Link_.VADECUNI,0)
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_DECTOT = 0
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
        endif
        this.w_COCODVAL = space(3)
        this.w_DESVAL = space(35)
        this.w_DECTOT = 0
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.VACODVAL as VACODVAL115"+ ",link_1_15.VADESVAL as VADESVAL115"+ ",link_1_15.VADECUNI as VADECUNI115"+ ",link_1_15.VADTOBSO as VADTOBSO115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on CON_TRAM.COCODVAL=link_1_15.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and CON_TRAM.COCODVAL=link_1_15.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COGRUMER
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COGRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_COGRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_COGRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COGRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COGRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oCOGRUMER_2_2'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COGRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_COGRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_COGRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COGRUMER = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_COGRUMER = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COGRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUMERC_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.GMCODICE as GMCODICE202"+ ","+cp_TransLinkFldName('link_2_2.GMDESCRI')+" as GMDESCRI202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on CON_TRAD.COGRUMER=link_2_2.GMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and CON_TRAD.COGRUMER=link_2_2.GMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCODART
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_COCODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_COCODART))
          select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_COCODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_COCODART)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCOCODART_2_3'),i_cWhere,'GSMA_BZA',"Articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_COCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_COCODART)
            select ARCODART,ARDESART,ARDTOBSO,ARUNMIS1,ARUNMIS2,ARFLSERG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_OBSART = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_FLSERG = NVL(_Link_.ARFLSERG,space(1))
      this.w_COUNIMIS = NVL(_Link_.ARUNMIS1,space(3))
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_COCODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_OBSART = ctod("  /  /  ")
      this.w_UNMIS1 = space(3)
      this.w_UNMIS2 = space(3)
      this.w_FLSERG = space(1)
      this.w_COUNIMIS = space(3)
      this.w_CODART = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_OBSART,.w_OBTEST,'Articolo Obsoleto!')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        endif
        this.w_COCODART = space(20)
        this.w_DESART = space(40)
        this.w_OBSART = ctod("  /  /  ")
        this.w_UNMIS1 = space(3)
        this.w_UNMIS2 = space(3)
        this.w_FLSERG = space(1)
        this.w_COUNIMIS = space(3)
        this.w_CODART = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.ARCODART as ARCODART203"+ ",link_2_3.ARDESART as ARDESART203"+ ",link_2_3.ARDTOBSO as ARDTOBSO203"+ ",link_2_3.ARUNMIS1 as ARUNMIS1203"+ ",link_2_3.ARUNMIS2 as ARUNMIS2203"+ ",link_2_3.ARFLSERG as ARFLSERG203"+ ",link_2_3.ARUNMIS1 as ARUNMIS1203"+ ",link_2_3.ARCODART as ARCODART203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on CON_TRAD.COCODART=link_2_3.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and CON_TRAD.COCODART=link_2_3.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COUNIMIS
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_COUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_COUNIMIS))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oCOUNIMIS_2_11'),i_cWhere,'GSAR_AUM',"Unit� di misura",'GSMA1QUM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_COUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_COUNIMIS)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COUNIMIS = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_COUNIMIS = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUMLIS(.w_COCODART, IIF(.w_FLSERG='S', '***', .w_COUNIMIS), .w_UNMIS1, .w_UNMIS2) AND NOT EMPTY(.w_COUNIMIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_COUNIMIS = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCONUMERO_1_3.value==this.w_CONUMERO)
      this.oPgFrm.Page1.oPag.oCONUMERO_1_3.value=this.w_CONUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATCON_1_4.value==this.w_CODATCON)
      this.oPgFrm.Page1.oPag.oCODATCON_1_4.value=this.w_CODATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATINI_1_5.value==this.w_CODATINI)
      this.oPgFrm.Page1.oPag.oCODATINI_1_5.value=this.w_CODATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATFIN_1_6.value==this.w_CODATFIN)
      this.oPgFrm.Page1.oPag.oCODATFIN_1_6.value=this.w_CODATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODCLF_1_8.value==this.w_COCODCLF)
      this.oPgFrm.Page1.oPag.oCOCODCLF_1_8.value=this.w_COCODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_9.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_9.value=this.w_DESCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCATCOM_1_10.value==this.w_COCATCOM)
      this.oPgFrm.Page1.oPag.oCOCATCOM_1_10.value=this.w_COCATCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_11.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_11.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESCON_1_13.value==this.w_CODESCON)
      this.oPgFrm.Page1.oPag.oCODESCON_1_13.value=this.w_CODESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCO__NOTE_1_14.value==this.w_CO__NOTE)
      this.oPgFrm.Page1.oPag.oCO__NOTE_1_14.value=this.w_CO__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODVAL_1_15.value==this.w_COCODVAL)
      this.oPgFrm.Page1.oPag.oCOCODVAL_1_15.value=this.w_COCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_16.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_16.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOIVALIS_1_18.RadioValue()==this.w_COIVALIS)
      this.oPgFrm.Page1.oPag.oCOIVALIS_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOQUANTI_1_19.RadioValue()==this.w_COQUANTI)
      this.oPgFrm.Page1.oPag.oCOQUANTI_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOFLUCOA_1_20.RadioValue()==this.w_COFLUCOA)
      this.oPgFrm.Page1.oPag.oCOFLUCOA_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUNMIS1_2_7.value==this.w_UNMIS1)
      this.oPgFrm.Page1.oPag.oUNMIS1_2_7.value=this.w_UNMIS1
      replace t_UNMIS1 with this.oPgFrm.Page1.oPag.oUNMIS1_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCOUNIMIS_2_11.value==this.w_COUNIMIS)
      this.oPgFrm.Page1.oPag.oCOUNIMIS_2_11.value=this.w_COUNIMIS
      replace t_COUNIMIS with this.oPgFrm.Page1.oPag.oCOUNIMIS_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSCONT1_2_12.value==this.w_COSCONT1)
      this.oPgFrm.Page1.oPag.oCOSCONT1_2_12.value=this.w_COSCONT1
      replace t_COSCONT1 with this.oPgFrm.Page1.oPag.oCOSCONT1_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSCONT2_2_13.value==this.w_COSCONT2)
      this.oPgFrm.Page1.oPag.oCOSCONT2_2_13.value=this.w_COSCONT2
      replace t_COSCONT2 with this.oPgFrm.Page1.oPag.oCOSCONT2_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSCONT3_2_14.value==this.w_COSCONT3)
      this.oPgFrm.Page1.oPag.oCOSCONT3_2_14.value=this.w_COSCONT3
      replace t_COSCONT3 with this.oPgFrm.Page1.oPag.oCOSCONT3_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSCONT4_2_15.value==this.w_COSCONT4)
      this.oPgFrm.Page1.oPag.oCOSCONT4_2_15.value=this.w_COSCONT4
      replace t_COSCONT4 with this.oPgFrm.Page1.oPag.oCOSCONT4_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRAR_2_19.value==this.w_DESGRAR)
      this.oPgFrm.Page1.oPag.oDESGRAR_2_19.value=this.w_DESGRAR
      replace t_DESGRAR with this.oPgFrm.Page1.oPag.oDESGRAR_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCOFLARTI_1_35.RadioValue()==this.w_COFLARTI)
      this.oPgFrm.Page1.oPag.oCOFLARTI_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWNUM_2_1.value==this.w_CPROWNUM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWNUM_2_1.value=this.w_CPROWNUM
      replace t_CPROWNUM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWNUM_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOGRUMER_2_2.value==this.w_COGRUMER)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOGRUMER_2_2.value=this.w_COGRUMER
      replace t_COGRUMER with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOGRUMER_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCODART_2_3.value==this.w_COCODART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCODART_2_3.value=this.w_COCODART
      replace t_COCODART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCODART_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOPREZZO_2_4.value==this.w_COPREZZO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOPREZZO_2_4.value=this.w_COPREZZO
      replace t_COPREZZO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOPREZZO_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOPERPRO_2_5.value==this.w_COPERPRO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOPERPRO_2_5.value=this.w_COPERPRO
      replace t_COPERPRO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOPERPRO_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOPERCAP_2_6.value==this.w_COPERCAP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOPERCAP_2_6.value=this.w_COPERCAP
      replace t_COPERCAP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOPERCAP_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'CON_TRAM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CONUMERO))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCONUMERO_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CONUMERO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODATCON))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCODATCON_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CODATCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODATINI))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCODATINI_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CODATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODATFIN) or not(NOT .w_CODATFIN<.w_CODATINI))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCODATFIN_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CODATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data fine contratto inferiore alla data di inizio")
          case   not((NOT EMPTY(.w_COCATCOM) OR NOT EMPTY(.w_COCODCLF)) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST<=.w_DATOBSO))  and (EMPTY(.w_COCATCOM))  and not(empty(.w_COCODCLF))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCOCODCLF_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il cliente (non obsoleto) o la categoria commerciale")
          case   not(NOT EMPTY(.w_COCATCOM) OR NOT EMPTY(.w_COCODCLF))  and (EMPTY(.w_COCODCLF))  and not(empty(.w_COCATCOM))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCOCATCOM_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire il cliente o la categoria commerciale")
          case   (empty(.w_COCODVAL) or not(CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCOCODVAL_1_15.SetFocus()
            i_bnoObbl = !empty(.w_COCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsve_mco
      
      if i_bRes= .t. AND EMPTY(.w_COCODCLF) AND EMPTY(.w_COCATCOM)
         * -- Errore
         i_bnoChk = .f.
         i_bRes = .f.
         i_cErrorMsg = Ah_MsgFormat("Inserire Il cliente o la categoria commerciale")
      endif
      this.NotifyEvent('CheckRighe')
      If i_bRes And this.w_NORIGHE
         * -- Errore
         i_bnoChk = .f.
         i_bRes = .f.
         i_cErrorMsg =Ah_MsgFormat("Impossibile salvare il contratto senza righe di dettaglio")
      Endif
      If i_bRes And this.w_RIGHERIP
         * -- Errore
         i_bnoChk = .f.
         i_bRes = .f.
         i_cErrorMsg = ah_msgformat("Esiste gi� una riga di scaglione con la stessa quantit� (quantit� %1).%0Impossibile salvare il contratto.",alltrim(str(this.w_QUANTI)))
      Endif
      *--- Controllo congruit� tra scorporo piede fattura e il flag iva inclusa
      If i_bRes AND .w_COIVALIS <> IIF( .w_SCORPO = 'S', 'L', 'N') and Not Empty(.w_COCODCLF)
      
         * --- Errore
         i_bnoChk = .f.
         i_bRes = .f.
         i_cErrorMsg = Ah_MsgFormat("Impossibile salvare il contratto, incongruenza tra il check scorporo piede fattura e il check IVA inclusa (lordo)")
      Endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(CHKDTOBS(.w_OBSART,.w_OBTEST,'Articolo Obsoleto!')) and (EMPTY(.w_COGRUMER) AND (.w_COFLARTI<>'S' OR .w_COFLUCOA $ 'SN')) and not(empty(.w_COCODART)) and (NOT EMPTY(.w_COGRUMER) OR NOT EMPTY(.w_COCODART))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCODART_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice articolo inesistente oppure obsoleto")
        case   (empty(.w_COUNIMIS) or not(CHKUMLIS(.w_COCODART, IIF(.w_FLSERG='S', '***', .w_COUNIMIS), .w_UNMIS1, .w_UNMIS2) AND NOT EMPTY(.w_COUNIMIS))) and (!Empty(.w_COCODART) and .w_COFLUCOA <>'S') and (NOT EMPTY(.w_COGRUMER) OR NOT EMPTY(.w_COCODART))
          .oNewFocus=.oPgFrm.Page1.oPag.oCOUNIMIS_2_11
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
      endcase
      i_bRes = i_bRes .and. .GSVE_MSO.CheckForm()
      if NOT EMPTY(.w_COGRUMER) OR NOT EMPTY(.w_COCODART)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COCODCLF = this.w_COCODCLF
    this.o_COCATCOM = this.w_COCATCOM
    this.o_DECTOT = this.w_DECTOT
    this.o_COQUANTI = this.w_COQUANTI
    this.o_COFLUCOA = this.w_COFLUCOA
    this.o_COGRUMER = this.w_COGRUMER
    this.o_COCODART = this.w_COCODART
    this.o_COSCONT1 = this.w_COSCONT1
    this.o_COSCONT2 = this.w_COSCONT2
    this.o_COSCONT3 = this.w_COSCONT3
    * --- GSVE_MSO : Depends On
    this.GSVE_MSO.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_COGRUMER) OR NOT EMPTY(t_COCODART))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_COGRUMER=space(5)
      .w_COCODART=space(20)
      .w_COPREZZO=0
      .w_COPERPRO=0
      .w_COPERCAP=0
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_CODART=space(20)
      .w_COUNIMIS=space(3)
      .w_COSCONT1=0
      .w_COSCONT2=0
      .w_COSCONT3=0
      .w_COSCONT4=0
      .w_DESGRU=space(35)
      .w_DESART=space(40)
      .w_DESGRAR=space(45)
      .w_OBSART=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .DoRTCalc(1,22,.f.)
        .w_COGRUMER = SPACE(5)
      .DoRTCalc(23,23,.f.)
      if not(empty(.w_COGRUMER))
        .link_2_2('Full')
      endif
        .w_COCODART = SPACE(20)
      .DoRTCalc(24,24,.f.)
      if not(empty(.w_COCODART))
        .link_2_3('Full')
      endif
        .w_COPREZZO = 0
        .w_COPERPRO = 0
        .w_COPERCAP = 0
      .DoRTCalc(28,31,.f.)
        .w_COUNIMIS = .w_UNMIS1
      .DoRTCalc(32,32,.f.)
      if not(empty(.w_COUNIMIS))
        .link_2_11('Full')
      endif
      .DoRTCalc(33,33,.f.)
        .w_COSCONT2 = 0
        .w_COSCONT3 = 0
        .w_COSCONT4 = 0
      .DoRTCalc(37,38,.f.)
        .w_DESGRAR = IIF(EMPTY(.w_COGRUMER), .w_DESART, .w_DESGRU)
    endwith
    this.DoRTCalc(40,52,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWNUM = t_CPROWNUM
    this.w_COGRUMER = t_COGRUMER
    this.w_COCODART = t_COCODART
    this.w_COPREZZO = t_COPREZZO
    this.w_COPERPRO = t_COPERPRO
    this.w_COPERCAP = t_COPERCAP
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_CODART = t_CODART
    this.w_COUNIMIS = t_COUNIMIS
    this.w_COSCONT1 = t_COSCONT1
    this.w_COSCONT2 = t_COSCONT2
    this.w_COSCONT3 = t_COSCONT3
    this.w_COSCONT4 = t_COSCONT4
    this.w_DESGRU = t_DESGRU
    this.w_DESART = t_DESART
    this.w_DESGRAR = t_DESGRAR
    this.w_OBSART = t_OBSART
    this.w_DATOBSO = t_DATOBSO
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWNUM with this.w_CPROWNUM
    replace t_COGRUMER with this.w_COGRUMER
    replace t_COCODART with this.w_COCODART
    replace t_COPREZZO with this.w_COPREZZO
    replace t_COPERPRO with this.w_COPERPRO
    replace t_COPERCAP with this.w_COPERCAP
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_CODART with this.w_CODART
    replace t_COUNIMIS with this.w_COUNIMIS
    replace t_COSCONT1 with this.w_COSCONT1
    replace t_COSCONT2 with this.w_COSCONT2
    replace t_COSCONT3 with this.w_COSCONT3
    replace t_COSCONT4 with this.w_COSCONT4
    replace t_DESGRU with this.w_DESGRU
    replace t_DESART with this.w_DESART
    replace t_DESGRAR with this.w_DESGRAR
    replace t_OBSART with this.w_OBSART
    replace t_DATOBSO with this.w_DATOBSO
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsve_mcoPag1 as StdContainer
  Width  = 728
  height = 498
  stdWidth  = 728
  stdheight = 498
  resizeXpos=146
  resizeYpos=372
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCONUMERO_1_3 as StdField with uid="UOURHEFMCD",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CONUMERO", cQueryName = "CONUMERO",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice identificativo del contratto",;
    HelpContextID = 195034763,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=98, Top=11, InputMask=replicate('X',15)

  add object oCODATCON_1_4 as StdField with uid="UOCPWJCGJQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODATCON", cQueryName = "CODATCON",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di stipulazione del contratto",;
    HelpContextID = 222600844,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=282, Top=11

  add object oCODATINI_1_5 as StdField with uid="AKZXWBXBOX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODATINI", cQueryName = "CODATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit� del contratto",;
    HelpContextID = 121937553,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=497, Top=11

  add object oCODATFIN_1_6 as StdField with uid="GBWXRPYZKG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODATFIN", cQueryName = "CODATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data fine contratto inferiore alla data di inizio",;
    ToolTipText = "Data di termine validit� del contratto",;
    HelpContextID = 96166260,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=603, Top=11

  func oCODATFIN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT .w_CODATFIN<.w_CODATINI)
    endwith
    return bRes
  endfunc

  add object oCOCODCLF_1_8 as StdField with uid="CRZYQRFSIV",rtseq=8,rtrep=.f.,;
    cFormVar = "w_COCODCLF", cQueryName = "COCODCLF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il cliente (non obsoleto) o la categoria commerciale",;
    ToolTipText = "Codice del cliente associato al contratto",;
    HelpContextID = 29970796,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=98, Top=39, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCODCLF"

  func oCOCODCLF_1_8.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_COCATCOM))
    endwith
  endfunc

  func oCOCODCLF_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODCLF_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODCLF_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCODCLF_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti",'',this.parent.oContained
  endproc
  proc oCOCODCLF_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCLF
     i_obj.w_ANCODICE=this.parent.oContained.w_COCODCLF
    i_obj.ecpSave()
  endproc

  add object oDESCLF_1_9 as StdField with uid="DDPAMPGLMY",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 180467914,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=234, Top=39, InputMask=replicate('X',40)

  add object oCOCATCOM_1_10 as StdField with uid="XJOILOZHVK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_COCATCOM", cQueryName = "CODATINI,COCATCOM",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il cliente o la categoria commerciale",;
    ToolTipText = "Categoria commerciale associata al contratto",;
    HelpContextID = 222604941,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=98, Top=67, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_COCATCOM"

  func oCOCATCOM_1_10.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_COCODCLF))
    endwith
  endfunc

  func oCOCATCOM_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCATCOM_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCATCOM_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCOCATCOM_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oCOCATCOM_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_COCATCOM
    i_obj.ecpSave()
  endproc

  add object oDESCAT_1_11 as StdField with uid="OXBLFSFDYK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 225556682,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=150, Top=67, InputMask=replicate('X',35)

  add object oCODESCON_1_13 as StdField with uid="QNXPRAWNXD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODESCON", cQueryName = "CODESCON",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del contratto",;
    HelpContextID = 223387276,;
   bGlobalFont=.t.,;
    Height=21, Width=422, Left=98, Top=95, InputMask=replicate('X',50)

  add object oCO__NOTE_1_14 as StdMemo with uid="ARMKJBDECW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CO__NOTE", cQueryName = "CO__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali ulteriori note descrittive del contratto",;
    HelpContextID = 25489045,;
   bGlobalFont=.t.,;
    Height=60, Width=422, Left=98, Top=123

  add object oCOCODVAL_1_15 as StdField with uid="BRVTRTWDFM",rtseq=15,rtrep=.f.,;
    cFormVar = "w_COCODVAL", cQueryName = "COCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o incongruente o obsoleta",;
    ToolTipText = "Codice della valuta a cui � riferito il contratto",;
    HelpContextID = 188133006,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=98, Top=191, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_COCODVAL"

  func oCOCODVAL_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODVAL_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODVAL_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCOCODVAL_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oCOCODVAL_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_COCODVAL
    i_obj.ecpSave()
  endproc

  add object oDESVAL_1_16 as StdField with uid="MXYZEFSRIV",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 90093770,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=150, Top=191, InputMask=replicate('X',35)

  add object oCOIVALIS_1_18 as StdCheck with uid="XUBUOXCQKQ",rtseq=18,rtrep=.f.,left=533, top=146, caption="IVA inclusa (lordo)",;
    ToolTipText = "Se attivo: gli importo sono comprensivi di IVA",;
    HelpContextID = 178303353,;
    cFormVar="w_COIVALIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOIVALIS_1_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COIVALIS,&i_cF..t_COIVALIS),this.value)
    return(iif(xVal =1,'L',;
    'N'))
  endfunc
  func oCOIVALIS_1_18.GetRadio()
    this.Parent.oContained.w_COIVALIS = this.RadioValue()
    return .t.
  endfunc

  func oCOIVALIS_1_18.ToRadio()
    this.Parent.oContained.w_COIVALIS=trim(this.Parent.oContained.w_COIVALIS)
    return(;
      iif(this.Parent.oContained.w_COIVALIS=='L',1,;
      0))
  endfunc

  func oCOIVALIS_1_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCOQUANTI_1_19 as StdCheck with uid="AINOMAYAXG",rtseq=19,rtrep=.f.,left=533, top=167, caption="Scaglioni",;
    ToolTipText = "Se attivo: il contratto prevede gli scaglioni prezzi per le quantit�",;
    HelpContextID = 56610449,;
    cFormVar="w_COQUANTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOQUANTI_1_19.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COQUANTI,&i_cF..t_COQUANTI),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oCOQUANTI_1_19.GetRadio()
    this.Parent.oContained.w_COQUANTI = this.RadioValue()
    return .t.
  endfunc

  func oCOQUANTI_1_19.ToRadio()
    this.Parent.oContained.w_COQUANTI=trim(this.Parent.oContained.w_COQUANTI)
    return(;
      iif(this.Parent.oContained.w_COQUANTI=='S',1,;
      0))
  endfunc

  func oCOQUANTI_1_19.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oCOFLUCOA_1_20 as StdCombo with uid="PUMCTHOIVP",rtseq=20,rtrep=.f.,left=99,top=218,width=136,height=21;
    , ToolTipText = "Tipo di calcolo del prezzo";
    , HelpContextID = 220823193;
    , cFormVar="w_COFLUCOA",RowSource=""+"Ricarico su U.C.A.,"+"Ultimo prezzo cliente,"+"Classe ric. su U.C.A.,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOFLUCOA_1_20.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COFLUCOA,&i_cF..t_COFLUCOA),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'P',;
    iif(xVal =3,'R',;
    iif(xVal =4,'N',;
    ' ')))))
  endfunc
  func oCOFLUCOA_1_20.GetRadio()
    this.Parent.oContained.w_COFLUCOA = this.RadioValue()
    return .t.
  endfunc

  func oCOFLUCOA_1_20.ToRadio()
    this.Parent.oContained.w_COFLUCOA=trim(this.Parent.oContained.w_COFLUCOA)
    return(;
      iif(this.Parent.oContained.w_COFLUCOA=='S',1,;
      iif(this.Parent.oContained.w_COFLUCOA=='P',2,;
      iif(this.Parent.oContained.w_COFLUCOA=='R',3,;
      iif(this.Parent.oContained.w_COFLUCOA=='N',4,;
      0)))))
  endfunc

  func oCOFLUCOA_1_20.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOFLUCOA_1_20.mCond()
    with this.Parent.oContained
      return (.w_COQUANTI<>'S')
    endwith
  endfunc

  add object oCOFLARTI_1_35 as StdCheck with uid="BMJCXSNGTN",rtseq=41,rtrep=.f.,left=265, top=220, caption="Tutti gli articoli",;
    ToolTipText = "Se attivo applica ultimo prezzo cliente e classe ric. a qualunque articolo presente sul documento",;
    HelpContextID = 258571921,;
    cFormVar="w_COFLARTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOFLARTI_1_35.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COFLARTI,&i_cF..t_COFLARTI),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCOFLARTI_1_35.GetRadio()
    this.Parent.oContained.w_COFLARTI = this.RadioValue()
    return .t.
  endfunc

  func oCOFLARTI_1_35.ToRadio()
    this.Parent.oContained.w_COFLARTI=trim(this.Parent.oContained.w_COFLARTI)
    return(;
      iif(this.Parent.oContained.w_COFLARTI=='S',1,;
      0))
  endfunc

  func oCOFLARTI_1_35.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOFLARTI_1_35.mCond()
    with this.Parent.oContained
      return ((.w_COFLUCOA='P' OR .w_COFLUCOA='R') AND .w_COQUANTI<>'S')
    endwith
  endfunc

  func oCOFLARTI_1_35.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(.w_COFLUCOA='P' OR .w_COFLUCOA='R'))
    endwith
    endif
  endfunc


  add object oObj_1_38 as cp_runprogram with uid="FBHIKALFKA",left=0, top=511, width=227,height=23,;
    caption='GSVE_BRN(R)',;
   bGlobalFont=.t.,;
    prg="GSVE_BRN('R')",;
    cEvent = "CheckRighe",;
    nPag=1;
    , HelpContextID = 227317196


  add object oObj_1_39 as cp_runprogram with uid="MBFXUHBLVN",left=2, top=535, width=226,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSVE_BRN('N')",;
    cEvent = "w_COCODART Changed",;
    nPag=1;
    , HelpContextID = 187575066


  add object oObj_1_40 as cp_runprogram with uid="ZKOMECJMYE",left=239, top=535, width=230,height=19,;
    caption='GSVE_BRND)',;
   bGlobalFont=.t.,;
    prg="GSVE_BRN('D')",;
    cEvent = "w_CODATFIN Changed",;
    nPag=1;
    , HelpContextID = 227495180


  add object oObj_1_41 as cp_runprogram with uid="PPXGESGGTB",left=239, top=512, width=226,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSVE_BRN('C')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 187575066


  add object oObj_1_44 as cp_runprogram with uid="UUXBPKXIXW",left=4, top=557, width=226,height=21,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSVE_BRN('G')",;
    cEvent = "w_COGRUMER Changed",;
    nPag=1;
    , HelpContextID = 187575066


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=2, top=245, width=476,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="COGRUMER",Label1="Gr.merc.",Field2="COCODART",Label2="Articolo",Field3="COPREZZO",Label3="Prezzo",Field4="COPERPRO",Label4="Provv.ag.",Field5="COPERCAP",Label5="Provv.C.A.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49481862


  add object oObj_1_49 as cp_runprogram with uid="ADXAPZFBBA",left=239, top=554, width=230,height=19,;
    caption='GSVE_BRN(S)',;
   bGlobalFont=.t.,;
    prg="GSVE_BRN('S')",;
    cEvent = "w_COQUANTI Changed",;
    nPag=1;
    , HelpContextID = 227316940

  add object oStr_1_21 as StdString with uid="XFZRNTZFKU",Visible=.t., Left=221, Top=11,;
    Alignment=1, Width=59, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_22 as StdString with uid="QVPAIQSFPW",Visible=.t., Left=20, Top=95,;
    Alignment=1, Width=76, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="ESFJOMEFVE",Visible=.t., Left=20, Top=123,;
    Alignment=1, Width=76, Height=15,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="PDFYHSNFBL",Visible=.t., Left=364, Top=11,;
    Alignment=1, Width=129, Height=15,;
    Caption="Validit� dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="UUEOAHVCNB",Visible=.t., Left=575, Top=11,;
    Alignment=1, Width=26, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="UMPFHDRAYC",Visible=.t., Left=20, Top=191,;
    Alignment=1, Width=76, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="PBKCKHUNDL",Visible=.t., Left=522, Top=227,;
    Alignment=0, Width=196, Height=15,;
    Caption="Scaglioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_29 as StdString with uid="APXFVUMPKR",Visible=.t., Left=20, Top=67,;
    Alignment=1, Width=76, Height=15,;
    Caption="Cat.comm.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="KWUKIRFVAD",Visible=.t., Left=20, Top=11,;
    Alignment=1, Width=76, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_31 as StdString with uid="BQRVAYJCOE",Visible=.t., Left=20, Top=39,;
    Alignment=1, Width=76, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="WBDDCJARZU",Visible=.t., Left=23, Top=459,;
    Alignment=1, Width=128, Height=15,;
    Caption="Sconti/maggiorazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="YNYINYQECH",Visible=.t., Left=4, Top=218,;
    Alignment=1, Width=95, Height=18,;
    Caption="Calcolo prezzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="CVREWCNBFD",Visible=.t., Left=381, Top=433,;
    Alignment=1, Width=27, Height=16,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (Empty(.w_COCODART))
    endwith
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsve_mso",lower(this.oContained.GSVE_MSO.class))=0
        this.oContained.GSVE_MSO.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-8,top=265,;
    width=472+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-7,top=266,width=471+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='GRUMERC|ART_ICOL|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oUNMIS1_2_7.Refresh()
      this.Parent.oCOUNIMIS_2_11.Refresh()
      this.Parent.oCOSCONT1_2_12.Refresh()
      this.Parent.oCOSCONT2_2_13.Refresh()
      this.Parent.oCOSCONT3_2_14.Refresh()
      this.Parent.oCOSCONT4_2_15.Refresh()
      this.Parent.oDESGRAR_2_19.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='GRUMERC'
        oDropInto=this.oBodyCol.oRow.oCOGRUMER_2_2
      case cFile='ART_ICOL'
        oDropInto=this.oBodyCol.oRow.oCOCODART_2_3
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oUNMIS1_2_7 as StdTrsField with uid="MTDKHSUBWY",rtseq=28,rtrep=.t.,;
    cFormVar="w_UNMIS1",value=space(3),enabled=.f.,;
    HelpContextID = 256642746,;
    cTotal="", bFixedPos=.t., cQueryName = "UNMIS1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=73, Left=777, Top=243, InputMask=replicate('X',3)

  add object oCOUNIMIS_2_11 as StdTrsField with uid="LBRPRSXVDT",rtseq=32,rtrep=.t.,;
    cFormVar="w_COUNIMIS",value=space(3),;
    ToolTipText = "Unit� di misura a cui il contratto si riferisce",;
    HelpContextID = 202994041,;
    cTotal="", bFixedPos=.t., cQueryName = "COUNIMIS",;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=21, Width=53, Left=414, Top=432, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_COUNIMIS"

  func oCOUNIMIS_2_11.mCond()
    with this.Parent.oContained
      return (!Empty(.w_COCODART) and .w_COFLUCOA <>'S')
    endwith
  endfunc

  func oCOUNIMIS_2_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_COCODART))
    endwith
    endif
  endfunc

  func oCOUNIMIS_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOUNIMIS_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oCOUNIMIS_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oCOUNIMIS_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'GSMA1QUM.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oCOUNIMIS_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_COUNIMIS
    i_obj.ecpSave()
  endproc

  add object oCOSCONT1_2_12 as StdTrsField with uid="KBXROPZFHQ",rtseq=33,rtrep=.t.,;
    cFormVar="w_COSCONT1",value=0,;
    ToolTipText = "1^ % di maggiorazione (se positiva) o sconto (se negativa)",;
    HelpContextID = 43101865,;
    cTotal="", bFixedPos=.t., cQueryName = "COSCONT1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=152, Top=456, cSayPict=["999.99"], cGetPict=["999.99"]

  func oCOSCONT1_2_12.mCond()
    with this.Parent.oContained
      return ((g_NUMSCO>0 AND .w_COQUANTI<>'S') AND (.w_COFLUCOA= 'S' OR .w_COFLUCOA='N'))
    endwith
  endfunc

  add object oCOSCONT2_2_13 as StdTrsField with uid="VAPBMCTTKO",rtseq=34,rtrep=.t.,;
    cFormVar="w_COSCONT2",value=0,;
    ToolTipText = "2^ % di maggiorazione (se positiva) o sconto (se negativa)",;
    HelpContextID = 43101864,;
    cTotal="", bFixedPos=.t., cQueryName = "COSCONT2",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=207, Top=456, cSayPict=["999.99"], cGetPict=["999.99"]

  func oCOSCONT2_2_13.mCond()
    with this.Parent.oContained
      return ((.w_COSCONT1<>0 AND g_NUMSCO>1 AND .w_COQUANTI<>'S') AND (.w_COFLUCOA= 'S' OR .w_COFLUCOA='N'))
    endwith
  endfunc

  add object oCOSCONT3_2_14 as StdTrsField with uid="XZTUKIFOOS",rtseq=35,rtrep=.t.,;
    cFormVar="w_COSCONT3",value=0,;
    ToolTipText = "3^ % di maggiorazione (se positiva) o sconto (se negativa)",;
    HelpContextID = 43101863,;
    cTotal="", bFixedPos=.t., cQueryName = "COSCONT3",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=266, Top=456, cSayPict=["999.99"], cGetPict=["999.99"]

  func oCOSCONT3_2_14.mCond()
    with this.Parent.oContained
      return ((.w_COSCONT2<>0 AND g_NUMSCO>2 AND .w_COQUANTI<>'S') AND (.w_COFLUCOA= 'S' OR .w_COFLUCOA='N'))
    endwith
  endfunc

  add object oCOSCONT4_2_15 as StdTrsField with uid="FBIIEATDUH",rtseq=36,rtrep=.t.,;
    cFormVar="w_COSCONT4",value=0,;
    ToolTipText = "4^ % di maggiorazione (se positiva) o sconto (se negativa)",;
    HelpContextID = 43101862,;
    cTotal="", bFixedPos=.t., cQueryName = "COSCONT4",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=323, Top=456, cSayPict=["999.99"], cGetPict=["999.99"]

  func oCOSCONT4_2_15.mCond()
    with this.Parent.oContained
      return ((.w_COSCONT3<>0 AND g_NUMSCO>3 AND .w_COQUANTI<>'S') AND (.w_COFLUCOA= 'S' OR .w_COFLUCOA='N'))
    endwith
  endfunc

  add object oLinkPC_2_16 as stdDynamicChildContainer with uid="YWUFZBHAWO",bOnScreen=.t.,width=246,height=252,;
   left=481, top=246;


  func oLinkPC_2_16.mCond()
    with this.Parent.oContained
      return (.w_COQUANTI='S' AND .w_COFLUCOA<>'S' AND .cFunction<>'Query')
    endwith
  endfunc

  add object oDESGRAR_2_19 as StdTrsField with uid="PDETMTJAUQ",rtseq=39,rtrep=.t.,;
    cFormVar="w_DESGRAR",value=space(45),enabled=.f.,;
    HelpContextID = 257800394,;
    cTotal="", bFixedPos=.t., cQueryName = "DESGRAR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=366, Left=8, Top=431, InputMask=replicate('X',45)

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsve_mcoBodyRow as CPBodyRowCnt
  Width=462
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWNUM_2_1 as StdTrsField with uid="KGKBFAGPWL",rtseq=22,rtrep=.t.,;
    cFormVar="w_CPROWNUM",value=0,isprimarykey=.t.,;
    HelpContextID = 33930637,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=2, Width=1, Left=-2, Top=0

  add object oCOGRUMER_2_2 as StdTrsField with uid="IHLKLHDWFN",rtseq=23,rtrep=.t.,;
    cFormVar="w_COGRUMER",value=space(5),;
    ToolTipText = "Gruppo merceologico associato al contratto",;
    HelpContextID = 52653704,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=59, Left=-2, Top=-1, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_COGRUMER"

  func oCOGRUMER_2_2.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_COCODART) AND .w_COFLARTI<>'S')
    endwith
  endfunc

  func oCOGRUMER_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOGRUMER_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCOGRUMER_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oCOGRUMER_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oCOGRUMER_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_COGRUMER
    i_obj.ecpSave()
  endproc

  add object oCOCODART_2_3 as StdTrsField with uid="BJCWCPHMVA",rtseq=24,rtrep=.t.,;
    cFormVar="w_COCODART",value=space(20),;
    ToolTipText = "Codice articolo associato al contratto",;
    HelpContextID = 3583622,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=158, Left=60, Top=0, cSayPict=[p_ART], cGetPict=[p_ART], InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_COCODART"

  func oCOCODART_2_3.mCond()
    with this.Parent.oContained
      return (EMPTY(.w_COGRUMER) AND (.w_COFLARTI<>'S' OR .w_COFLUCOA $ 'SN'))
    endwith
  endfunc

  func oCOCODART_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODART_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCOCODART_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCOCODART_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'',this.parent.oContained
  endproc
  proc oCOCODART_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_COCODART
    i_obj.ecpSave()
  endproc

  add object oCOPREZZO_2_4 as StdTrsField with uid="KQREYPFWSI",rtseq=25,rtrep=.t.,;
    cFormVar="w_COPREZZO",value=0,;
    ToolTipText = "Prezzo",;
    HelpContextID = 148709749,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=114, Left=220, Top=0, cSayPict=[v_PU(38+VVU)], cGetPict=[v_GU(38+VVU)]

  func oCOPREZZO_2_4.mCond()
    with this.Parent.oContained
      return (.w_COQUANTI<>'S' AND .w_COFLUCOA='N')
    endwith
  endfunc

  add object oCOPERPRO_2_5 as StdTrsField with uid="YUYDWWFERI",rtseq=26,rtrep=.t.,;
    cFormVar="w_COPERPRO",value=0,;
    ToolTipText = "Percentuale provvigioni applicata all'agente",;
    HelpContextID = 6282891,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=61, Left=335, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  func oCOPERPRO_2_5.mCond()
    with this.Parent.oContained
      return (.w_COQUANTI<>'S' AND .w_COFLUCOA<>'R')
    endwith
  endfunc

  add object oCOPERCAP_2_6 as StdTrsField with uid="LDMEWIEABU",rtseq=27,rtrep=.t.,;
    cFormVar="w_COPERCAP",value=0,;
    ToolTipText = "Percentuale provvigioni applicata al capoarea",;
    HelpContextID = 224386698,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=399, Top=-1

  func oCOPERCAP_2_6.mCond()
    with this.Parent.oContained
      return (.w_COQUANTI<>'S' AND .w_COFLUCOA<>'R')
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWNUM_2_1.When()
    return(.t.)
  proc oCPROWNUM_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWNUM_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="COTIPCLF='C'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".CONUMERO=CON_TRAD.CONUMERO";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsve_mco','CON_TRAM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CONUMERO=CON_TRAM.CONUMERO";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
