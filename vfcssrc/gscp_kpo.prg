* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_kpo                                                        *
*              Parametri                                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_12]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-09                                                      *
* Last revis.: 2008-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscp_kpo",oParentObject))

* --- Class definition
define class tgscp_kpo as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 554
  Height = 269
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-08"
  HelpContextID=118909591
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=2

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gscp_kpo"
  cComment = "Parametri"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_RAGAZI = space(40)

  * --- Children pointers
  GSCP_APO = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSCP_APO additive
    with this
      .Pages(1).addobject("oPag","tgscp_kpoPag1","gscp_kpo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSCP_APO
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSCP_APO = CREATEOBJECT('stdDynamicChild',this,'GSCP_APO',this.oPgFrm.Page1.oPag.oLinkPC_1_2)
    this.GSCP_APO.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCP_APO)
      this.GSCP_APO.DestroyChildrenChain()
      this.GSCP_APO=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_2')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCP_APO.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCP_APO.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCP_APO.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCP_APO.SetKey(;
            .w_CODAZI,"PACODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCP_APO.ChangeRow(this.cRowID+'      1',1;
             ,.w_CODAZI,"PACODICE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCP_APO)
        i_f=.GSCP_APO.BuildFilter()
        if !(i_f==.GSCP_APO.cQueryFilter)
          i_fnidx=.GSCP_APO.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCP_APO.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCP_APO.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCP_APO.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCP_APO.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSCP_APO(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSCP_APO.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSCP_APO(i_ask)
    if this.GSCP_APO.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP))
      cp_BeginTrs()
      this.GSCP_APO.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_RAGAZI=space(40)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
      .GSCP_APO.NewDocument()
      .GSCP_APO.ChangeRow('1',1,.w_CODAZI,"PACODICE")
      if not(.GSCP_APO.bLoaded)
        .GSCP_APO.SetKey(.w_CODAZI,"PACODICE")
      endif
    endwith
    this.DoRTCalc(2,2,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSCP_APO.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCP_APO.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_CODAZI<>.o_CODAZI
          .Save_GSCP_APO(.t.)
          .GSCP_APO.NewDocument()
          .GSCP_APO.ChangeRow('1',1,.w_CODAZI,"PACODICE")
          if not(.GSCP_APO.bLoaded)
            .GSCP_APO.SetKey(.w_CODAZI,"PACODICE")
          endif
        endif
      endwith
      this.DoRTCalc(2,2,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODAZI_1_1.value==this.w_CODAZI)
      this.oPgFrm.Page1.oPag.oCODAZI_1_1.value=this.w_CODAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGAZI_1_4.value==this.w_RAGAZI)
      this.oPgFrm.Page1.oPag.oRAGAZI_1_4.value=this.w_RAGAZI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSCP_APO.CheckForm()
      if i_bres
        i_bres=  .GSCP_APO.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI = this.w_CODAZI
    * --- GSCP_APO : Depends On
    this.GSCP_APO.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscp_kpoPag1 as StdContainer
  Width  = 550
  height = 269
  stdWidth  = 550
  stdheight = 269
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODAZI_1_1 as StdField with uid="BLAAJDSDSQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODAZI", cQueryName = "CODAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 168034778,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=72, Top=13, InputMask=replicate('X',5), cLinkFile="AZIENDA", oKey_1_1="AZCODAZI", oKey_1_2="this.w_CODAZI"

  func oCODAZI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oLinkPC_1_2 as stdDynamicChildContainer with uid="YKUPNYCYIL",left=15, top=38, width=528, height=223, bOnScreen=.t.;


  add object oRAGAZI_1_4 as StdField with uid="JSRRKSNTIX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RAGAZI", cQueryName = "RAGAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 168025834,;
   bGlobalFont=.t.,;
    Height=21, Width=305, Left=129, Top=13, InputMask=replicate('X',40)

  add object oStr_1_3 as StdString with uid="HUKVATIPCL",Visible=.t., Left=9, Top=15,;
    Alignment=1, Width=58, Height=18,;
    Caption="Azienda:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscp_kpo','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
