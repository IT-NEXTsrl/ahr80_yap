* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bpr                                                        *
*              Gestione tabella progressivi                                    *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-01-19                                                      *
* Last revis.: 2005-12-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bpr",oParentObject)
return(i_retval)

define class tgsar_bpr as StdBatch
  * --- Local variables
  w_TMPC = space(100)
  * --- WorkFile variables
  CPWARN_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna tabella progressivi (chiamato da GSAR_KPR)
    this.w_TMPC = "Confermi aggiornamento progressivo %1 a %2?%0[Codice progressivo = %3]"
    if ah_YESNO(this.w_TMPC,,alltrim(this.oParentObject.w_CODPROG),alltrim(str(this.oParentObject.w_NUMDOC1,15,0)),alltrim(this.oParentObject.w_TABLECODE))
      * --- Try
      local bErr_03889180
      bErr_03889180=bTrsErr
      this.Try_03889180()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_ErrorMsg("Aggiornamento tabella CPWARN fallito",48,"")
      endif
      bTrsErr=bTrsErr or bErr_03889180
      * --- End
    else
      ah_ErrorMsg("Aggiornamento sospeso come richiesto",64,"")
    endif
  endproc
  proc Try_03889180()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CPWARN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CPWARN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CPWARN_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CPWARN_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"autonum ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUMDOC1),'CPWARN','autonum');
          +i_ccchkf ;
      +" where ";
          +"tablecode = "+cp_ToStrODBC(this.oParentObject.w_TABLECODE);
          +" and warncode = "+cp_ToStrODBC(this.oParentObject.w_WARNCODE);
             )
    else
      update (i_cTable) set;
          autonum = this.oParentObject.w_NUMDOC1;
          &i_ccchkf. ;
       where;
          tablecode = this.oParentObject.w_TABLECODE;
          and warncode = this.oParentObject.w_WARNCODE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_ErrorMsg("Aggiornamento eseguito con successo",64,"")
    this.oParentObject.NotifyEvent("Aggiorna")
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CPWARN'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
