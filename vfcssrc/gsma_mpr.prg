* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_mpr                                                        *
*              Dati articolo per magazzino                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_19]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-15                                                      *
* Last revis.: 2015-01-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsma_mpr")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsma_mpr")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsma_mpr")
  return

* --- Class definition
define class tgsma_mpr as StdPCForm
  Width  = 734
  Height = 394
  Top    = 22
  Left   = 13
  cComment = "Dati articolo per magazzino"
  cPrg = "gsma_mpr"
  HelpContextID=147487593
  add object cnt as tcgsma_mpr
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsma_mpr as PCContext
  w_PRCODART = space(20)
  w_OBTEST = space(8)
  w_PROVEN = space(1)
  w_CODDIS = space(20)
  w_OGGMPS = space(1)
  w_TIPGES = space(1)
  w_FLCOMM = space(1)
  w_FLFRAZ = space(1)
  w_TIPART = space(1)
  w_PRCODMAG = space(5)
  w_DESMAG = space(30)
  w_PRSCOMIN = 0
  w_PRSCOMAX = 0
  w_PRQTAMIN = 0
  w_PRDISMIN = 0
  w_PRQTAMAX = 0
  w_PRPUNRIO = 0
  w_PRLOTRIO = 0
  w_PRCLAABC = space(1)
  w_PRPERABC = 0
  w_PRDATABC = space(8)
  w_PRCOSSTA = 0
  w_PRGIOINV = 0
  w_PRGIOAPP = 0
  w_PRGIOCOP = 0
  w_PRTIPCON = space(1)
  w_PRCODFOR = space(15)
  w_DESFOR = space(40)
  w_DATOBSO = space(10)
  w_PRCOERSS = 0
  w_PRCOEFLT = 0
  w_PRSAFELT = 0
  proc Save(i_oFrom)
    this.w_PRCODART = i_oFrom.w_PRCODART
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_PROVEN = i_oFrom.w_PROVEN
    this.w_CODDIS = i_oFrom.w_CODDIS
    this.w_OGGMPS = i_oFrom.w_OGGMPS
    this.w_TIPGES = i_oFrom.w_TIPGES
    this.w_FLCOMM = i_oFrom.w_FLCOMM
    this.w_FLFRAZ = i_oFrom.w_FLFRAZ
    this.w_TIPART = i_oFrom.w_TIPART
    this.w_PRCODMAG = i_oFrom.w_PRCODMAG
    this.w_DESMAG = i_oFrom.w_DESMAG
    this.w_PRSCOMIN = i_oFrom.w_PRSCOMIN
    this.w_PRSCOMAX = i_oFrom.w_PRSCOMAX
    this.w_PRQTAMIN = i_oFrom.w_PRQTAMIN
    this.w_PRDISMIN = i_oFrom.w_PRDISMIN
    this.w_PRQTAMAX = i_oFrom.w_PRQTAMAX
    this.w_PRPUNRIO = i_oFrom.w_PRPUNRIO
    this.w_PRLOTRIO = i_oFrom.w_PRLOTRIO
    this.w_PRCLAABC = i_oFrom.w_PRCLAABC
    this.w_PRPERABC = i_oFrom.w_PRPERABC
    this.w_PRDATABC = i_oFrom.w_PRDATABC
    this.w_PRCOSSTA = i_oFrom.w_PRCOSSTA
    this.w_PRGIOINV = i_oFrom.w_PRGIOINV
    this.w_PRGIOAPP = i_oFrom.w_PRGIOAPP
    this.w_PRGIOCOP = i_oFrom.w_PRGIOCOP
    this.w_PRTIPCON = i_oFrom.w_PRTIPCON
    this.w_PRCODFOR = i_oFrom.w_PRCODFOR
    this.w_DESFOR = i_oFrom.w_DESFOR
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_PRCOERSS = i_oFrom.w_PRCOERSS
    this.w_PRCOEFLT = i_oFrom.w_PRCOEFLT
    this.w_PRSAFELT = i_oFrom.w_PRSAFELT
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PRCODART = this.w_PRCODART
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_PROVEN = this.w_PROVEN
    i_oTo.w_CODDIS = this.w_CODDIS
    i_oTo.w_OGGMPS = this.w_OGGMPS
    i_oTo.w_TIPGES = this.w_TIPGES
    i_oTo.w_FLCOMM = this.w_FLCOMM
    i_oTo.w_FLFRAZ = this.w_FLFRAZ
    i_oTo.w_TIPART = this.w_TIPART
    i_oTo.w_PRCODMAG = this.w_PRCODMAG
    i_oTo.w_DESMAG = this.w_DESMAG
    i_oTo.w_PRSCOMIN = this.w_PRSCOMIN
    i_oTo.w_PRSCOMAX = this.w_PRSCOMAX
    i_oTo.w_PRQTAMIN = this.w_PRQTAMIN
    i_oTo.w_PRDISMIN = this.w_PRDISMIN
    i_oTo.w_PRQTAMAX = this.w_PRQTAMAX
    i_oTo.w_PRPUNRIO = this.w_PRPUNRIO
    i_oTo.w_PRLOTRIO = this.w_PRLOTRIO
    i_oTo.w_PRCLAABC = this.w_PRCLAABC
    i_oTo.w_PRPERABC = this.w_PRPERABC
    i_oTo.w_PRDATABC = this.w_PRDATABC
    i_oTo.w_PRCOSSTA = this.w_PRCOSSTA
    i_oTo.w_PRGIOINV = this.w_PRGIOINV
    i_oTo.w_PRGIOAPP = this.w_PRGIOAPP
    i_oTo.w_PRGIOCOP = this.w_PRGIOCOP
    i_oTo.w_PRTIPCON = this.w_PRTIPCON
    i_oTo.w_PRCODFOR = this.w_PRCODFOR
    i_oTo.w_DESFOR = this.w_DESFOR
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_PRCOERSS = this.w_PRCOERSS
    i_oTo.w_PRCOEFLT = this.w_PRCOEFLT
    i_oTo.w_PRSAFELT = this.w_PRSAFELT
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsma_mpr as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 734
  Height = 394
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-01-22"
  HelpContextID=147487593
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PAR_RIMA_IDX = 0
  MAGAZZIN_IDX = 0
  CONTI_IDX = 0
  cFile = "PAR_RIMA"
  cKeySelect = "PRCODART"
  cKeyWhere  = "PRCODART=this.w_PRCODART"
  cKeyDetail  = "PRCODART=this.w_PRCODART and PRCODMAG=this.w_PRCODMAG"
  cKeyWhereODBC = '"PRCODART="+cp_ToStrODBC(this.w_PRCODART)';

  cKeyDetailWhereODBC = '"PRCODART="+cp_ToStrODBC(this.w_PRCODART)';
      +'+" and PRCODMAG="+cp_ToStrODBC(this.w_PRCODMAG)';

  cKeyWhereODBCqualified = '"PAR_RIMA.PRCODART="+cp_ToStrODBC(this.w_PRCODART)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsma_mpr"
  cComment = "Dati articolo per magazzino"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PRCODART = space(20)
  w_OBTEST = ctod('  /  /  ')
  w_PROVEN = space(1)
  w_CODDIS = space(20)
  w_OGGMPS = space(1)
  w_TIPGES = space(1)
  o_TIPGES = space(1)
  w_FLCOMM = space(1)
  w_FLFRAZ = space(1)
  o_FLFRAZ = space(1)
  w_TIPART = space(1)
  w_PRCODMAG = space(5)
  w_DESMAG = space(30)
  w_PRSCOMIN = 0
  w_PRSCOMAX = 0
  w_PRQTAMIN = 0
  w_PRDISMIN = 0
  w_PRQTAMAX = 0
  w_PRPUNRIO = 0
  w_PRLOTRIO = 0
  w_PRCLAABC = space(1)
  w_PRPERABC = 0
  w_PRDATABC = ctod('  /  /  ')
  w_PRCOSSTA = 0
  w_PRGIOINV = 0
  w_PRGIOAPP = 0
  w_PRGIOCOP = 0
  w_PRTIPCON = space(1)
  w_PRCODFOR = space(15)
  w_DESFOR = space(40)
  w_DATOBSO = space(10)
  w_PRCOERSS = 0
  w_PRCOEFLT = 0
  w_PRSAFELT = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_mprPag1","gsma_mpr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='PAR_RIMA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_RIMA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_RIMA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsma_mpr'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PAR_RIMA where PRCODART=KeySet.PRCODART
    *                            and PRCODMAG=KeySet.PRCODMAG
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PAR_RIMA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIMA_IDX,2],this.bLoadRecFilter,this.PAR_RIMA_IDX,"gsma_mpr")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_RIMA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_RIMA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_RIMA '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PRCODART',this.w_PRCODART  )
      select * from (i_cTable) PAR_RIMA where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PRCODART = NVL(PRCODART,space(20))
        .w_PROVEN = this.oParentObject.oParentObject .w_ARPROPRE
        .w_CODDIS = this.oParentObject.oParentObject .w_ARCODDIS
        .w_OGGMPS = this.oParentObject .w_PROGGMPS
        .w_TIPGES = this.oParentObject.oParentObject .w_ARTIPGES
        .w_FLCOMM = this.oParentObject.oParentObject .w_ARFLCOMM
        .w_FLFRAZ = this.oParentObject.oParentObject .w_FLFRAZ
        .w_TIPART = this.oParentObject.oParentObject .w_ARTIPART
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_PRTIPCON = NVL(PRTIPCON,space(1))
        cp_LoadRecExtFlds(this,'PAR_RIMA')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
        .w_OBTEST = i_DATSYS
          .w_DESMAG = space(30)
          .w_DESFOR = space(40)
          .w_DATOBSO = space(10)
          .w_PRCODMAG = NVL(PRCODMAG,space(5))
          if link_2_1_joined
            this.w_PRCODMAG = NVL(MGCODMAG201,NVL(this.w_PRCODMAG,space(5)))
            this.w_DESMAG = NVL(MGDESMAG201,space(30))
          else
          .link_2_1('Load')
          endif
          .w_PRSCOMIN = NVL(PRSCOMIN,0)
          .w_PRSCOMAX = NVL(PRSCOMAX,0)
          .w_PRQTAMIN = NVL(PRQTAMIN,0)
          .w_PRDISMIN = NVL(PRDISMIN,0)
          .w_PRQTAMAX = NVL(PRQTAMAX,0)
          .w_PRPUNRIO = NVL(PRPUNRIO,0)
          .w_PRLOTRIO = NVL(PRLOTRIO,0)
          .w_PRCLAABC = NVL(PRCLAABC,space(1))
          .w_PRPERABC = NVL(PRPERABC,0)
          .w_PRDATABC = NVL(cp_ToDate(PRDATABC),ctod("  /  /  "))
          .w_PRCOSSTA = NVL(PRCOSSTA,0)
          .w_PRGIOINV = NVL(PRGIOINV,0)
          .w_PRGIOAPP = NVL(PRGIOAPP,0)
          .w_PRGIOCOP = NVL(PRGIOCOP,0)
          .w_PRCODFOR = NVL(PRCODFOR,space(15))
          .link_2_18('Load')
          .w_PRCOERSS = NVL(PRCOERSS,0)
          .w_PRCOEFLT = NVL(PRCOEFLT,0)
          .w_PRSAFELT = NVL(PRSAFELT,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace PRCODMAG with .w_PRCODMAG
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_PROVEN = this.oParentObject.oParentObject .w_ARPROPRE
        .w_CODDIS = this.oParentObject.oParentObject .w_ARCODDIS
        .w_OGGMPS = this.oParentObject .w_PROGGMPS
        .w_TIPGES = this.oParentObject.oParentObject .w_ARTIPGES
        .w_FLCOMM = this.oParentObject.oParentObject .w_ARFLCOMM
        .w_FLFRAZ = this.oParentObject.oParentObject .w_FLFRAZ
        .w_TIPART = this.oParentObject.oParentObject .w_ARTIPART
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_PRCODART=space(20)
      .w_OBTEST=ctod("  /  /  ")
      .w_PROVEN=space(1)
      .w_CODDIS=space(20)
      .w_OGGMPS=space(1)
      .w_TIPGES=space(1)
      .w_FLCOMM=space(1)
      .w_FLFRAZ=space(1)
      .w_TIPART=space(1)
      .w_PRCODMAG=space(5)
      .w_DESMAG=space(30)
      .w_PRSCOMIN=0
      .w_PRSCOMAX=0
      .w_PRQTAMIN=0
      .w_PRDISMIN=0
      .w_PRQTAMAX=0
      .w_PRPUNRIO=0
      .w_PRLOTRIO=0
      .w_PRCLAABC=space(1)
      .w_PRPERABC=0
      .w_PRDATABC=ctod("  /  /  ")
      .w_PRCOSSTA=0
      .w_PRGIOINV=0
      .w_PRGIOAPP=0
      .w_PRGIOCOP=0
      .w_PRTIPCON=space(1)
      .w_PRCODFOR=space(15)
      .w_DESFOR=space(40)
      .w_DATOBSO=space(10)
      .w_PRCOERSS=0
      .w_PRCOEFLT=0
      .w_PRSAFELT=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_OBTEST = i_DATSYS
        .w_PROVEN = this.oParentObject.oParentObject .w_ARPROPRE
        .w_CODDIS = this.oParentObject.oParentObject .w_ARCODDIS
        .w_OGGMPS = this.oParentObject .w_PROGGMPS
        .w_TIPGES = this.oParentObject.oParentObject .w_ARTIPGES
        .w_FLCOMM = this.oParentObject.oParentObject .w_ARFLCOMM
        .w_FLFRAZ = this.oParentObject.oParentObject .w_FLFRAZ
        .w_TIPART = this.oParentObject.oParentObject .w_ARTIPART
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_PRCODMAG))
         .link_2_1('Full')
        endif
        .DoRTCalc(11,13,.f.)
        .w_PRQTAMIN = IIF(.w_TIPGES<>'S', .w_PRQTAMIN , 0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(15,25,.f.)
        .w_PRTIPCON = 'F'
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_PRCODFOR))
         .link_2_18('Full')
        endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_RIMA')
    this.DoRTCalc(28,32,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPRDISMIN_2_6.enabled = i_bVal
      .Page1.oPag.oPRCLAABC_2_10.enabled = i_bVal
      .Page1.oPag.oPRPERABC_2_11.enabled = i_bVal
      .Page1.oPag.oPRDATABC_2_12.enabled = i_bVal
      .Page1.oPag.oPRCOSSTA_2_13.enabled = i_bVal
      .Page1.oPag.oPRGIOINV_2_14.enabled = i_bVal
      .Page1.oPag.oPRGIOAPP_2_15.enabled = i_bVal
      .Page1.oPag.oPRGIOCOP_2_16.enabled = i_bVal
      .Page1.oPag.oPRCODFOR_2_18.enabled = i_bVal
      .Page1.oPag.oPRCOERSS_2_21.enabled = i_bVal
      .Page1.oPag.oPRCOEFLT_2_22.enabled = i_bVal
      .Page1.oPag.oPRSAFELT_2_23.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PAR_RIMA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_RIMA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRCODART,"PRCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PRTIPCON,"PRTIPCON",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PRCODMAG C(5);
      ,t_DESMAG C(30);
      ,t_PRSCOMIN N(12,3);
      ,t_PRSCOMAX N(12,3);
      ,t_PRQTAMIN N(12,3);
      ,t_PRDISMIN N(12,3);
      ,t_PRQTAMAX N(12,3);
      ,t_PRPUNRIO N(12,3);
      ,t_PRLOTRIO N(12,3);
      ,t_PRCLAABC C(1);
      ,t_PRPERABC N(8,5);
      ,t_PRDATABC D(8);
      ,t_PRCOSSTA N(18,5);
      ,t_PRGIOINV N(3);
      ,t_PRGIOAPP N(3);
      ,t_PRGIOCOP N(3);
      ,t_PRCODFOR C(15);
      ,t_DESFOR C(40);
      ,t_PRCOERSS N(6,4);
      ,t_PRCOEFLT N(6,4);
      ,t_PRSAFELT N(6,2);
      ,PRCODMAG C(5);
      ,t_OBTEST D(8);
      ,t_DATOBSO C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsma_mprbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRCODMAG_2_1.controlsource=this.cTrsName+'.t_PRCODMAG'
    this.oPgFRm.Page1.oPag.oDESMAG_2_2.controlsource=this.cTrsName+'.t_DESMAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRSCOMIN_2_3.controlsource=this.cTrsName+'.t_PRSCOMIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRSCOMAX_2_4.controlsource=this.cTrsName+'.t_PRSCOMAX'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRQTAMIN_2_5.controlsource=this.cTrsName+'.t_PRQTAMIN'
    this.oPgFRm.Page1.oPag.oPRDISMIN_2_6.controlsource=this.cTrsName+'.t_PRDISMIN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRQTAMAX_2_7.controlsource=this.cTrsName+'.t_PRQTAMAX'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRPUNRIO_2_8.controlsource=this.cTrsName+'.t_PRPUNRIO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPRLOTRIO_2_9.controlsource=this.cTrsName+'.t_PRLOTRIO'
    this.oPgFRm.Page1.oPag.oPRCLAABC_2_10.controlsource=this.cTrsName+'.t_PRCLAABC'
    this.oPgFRm.Page1.oPag.oPRPERABC_2_11.controlsource=this.cTrsName+'.t_PRPERABC'
    this.oPgFRm.Page1.oPag.oPRDATABC_2_12.controlsource=this.cTrsName+'.t_PRDATABC'
    this.oPgFRm.Page1.oPag.oPRCOSSTA_2_13.controlsource=this.cTrsName+'.t_PRCOSSTA'
    this.oPgFRm.Page1.oPag.oPRGIOINV_2_14.controlsource=this.cTrsName+'.t_PRGIOINV'
    this.oPgFRm.Page1.oPag.oPRGIOAPP_2_15.controlsource=this.cTrsName+'.t_PRGIOAPP'
    this.oPgFRm.Page1.oPag.oPRGIOCOP_2_16.controlsource=this.cTrsName+'.t_PRGIOCOP'
    this.oPgFRm.Page1.oPag.oPRCODFOR_2_18.controlsource=this.cTrsName+'.t_PRCODFOR'
    this.oPgFRm.Page1.oPag.oDESFOR_2_19.controlsource=this.cTrsName+'.t_DESFOR'
    this.oPgFRm.Page1.oPag.oPRCOERSS_2_21.controlsource=this.cTrsName+'.t_PRCOERSS'
    this.oPgFRm.Page1.oPag.oPRCOEFLT_2_22.controlsource=this.cTrsName+'.t_PRCOEFLT'
    this.oPgFRm.Page1.oPag.oPRSAFELT_2_23.controlsource=this.cTrsName+'.t_PRSAFELT'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(79)
    this.AddVLine(184)
    this.AddVLine(289)
    this.AddVLine(391)
    this.AddVLine(493)
    this.AddVLine(598)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRCODMAG_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAR_RIMA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIMA_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_RIMA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIMA_IDX,2])
      *
      * insert into PAR_RIMA
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_RIMA')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_RIMA')
        i_cFldBody=" "+;
                  "(PRCODART,PRCODMAG,PRSCOMIN,PRSCOMAX,PRQTAMIN"+;
                  ",PRDISMIN,PRQTAMAX,PRPUNRIO,PRLOTRIO,PRCLAABC"+;
                  ",PRPERABC,PRDATABC,PRCOSSTA,PRGIOINV,PRGIOAPP"+;
                  ",PRGIOCOP,PRTIPCON,PRCODFOR,PRCOERSS,PRCOEFLT"+;
                  ",PRSAFELT,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PRCODART)+","+cp_ToStrODBCNull(this.w_PRCODMAG)+","+cp_ToStrODBC(this.w_PRSCOMIN)+","+cp_ToStrODBC(this.w_PRSCOMAX)+","+cp_ToStrODBC(this.w_PRQTAMIN)+;
             ","+cp_ToStrODBC(this.w_PRDISMIN)+","+cp_ToStrODBC(this.w_PRQTAMAX)+","+cp_ToStrODBC(this.w_PRPUNRIO)+","+cp_ToStrODBC(this.w_PRLOTRIO)+","+cp_ToStrODBC(this.w_PRCLAABC)+;
             ","+cp_ToStrODBC(this.w_PRPERABC)+","+cp_ToStrODBC(this.w_PRDATABC)+","+cp_ToStrODBC(this.w_PRCOSSTA)+","+cp_ToStrODBC(this.w_PRGIOINV)+","+cp_ToStrODBC(this.w_PRGIOAPP)+;
             ","+cp_ToStrODBC(this.w_PRGIOCOP)+","+cp_ToStrODBC(this.w_PRTIPCON)+","+cp_ToStrODBCNull(this.w_PRCODFOR)+","+cp_ToStrODBC(this.w_PRCOERSS)+","+cp_ToStrODBC(this.w_PRCOEFLT)+;
             ","+cp_ToStrODBC(this.w_PRSAFELT)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_RIMA')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_RIMA')
        cp_CheckDeletedKey(i_cTable,0,'PRCODART',this.w_PRCODART,'PRCODMAG',this.w_PRCODMAG)
        INSERT INTO (i_cTable) (;
                   PRCODART;
                  ,PRCODMAG;
                  ,PRSCOMIN;
                  ,PRSCOMAX;
                  ,PRQTAMIN;
                  ,PRDISMIN;
                  ,PRQTAMAX;
                  ,PRPUNRIO;
                  ,PRLOTRIO;
                  ,PRCLAABC;
                  ,PRPERABC;
                  ,PRDATABC;
                  ,PRCOSSTA;
                  ,PRGIOINV;
                  ,PRGIOAPP;
                  ,PRGIOCOP;
                  ,PRTIPCON;
                  ,PRCODFOR;
                  ,PRCOERSS;
                  ,PRCOEFLT;
                  ,PRSAFELT;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PRCODART;
                  ,this.w_PRCODMAG;
                  ,this.w_PRSCOMIN;
                  ,this.w_PRSCOMAX;
                  ,this.w_PRQTAMIN;
                  ,this.w_PRDISMIN;
                  ,this.w_PRQTAMAX;
                  ,this.w_PRPUNRIO;
                  ,this.w_PRLOTRIO;
                  ,this.w_PRCLAABC;
                  ,this.w_PRPERABC;
                  ,this.w_PRDATABC;
                  ,this.w_PRCOSSTA;
                  ,this.w_PRGIOINV;
                  ,this.w_PRGIOAPP;
                  ,this.w_PRGIOCOP;
                  ,this.w_PRTIPCON;
                  ,this.w_PRCODFOR;
                  ,this.w_PRCOERSS;
                  ,this.w_PRCOEFLT;
                  ,this.w_PRSAFELT;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PAR_RIMA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIMA_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_PRCODMAG<>space(5)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_RIMA')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " PRTIPCON="+cp_ToStrODBC(this.w_PRTIPCON)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and PRCODMAG="+cp_ToStrODBC(&i_TN.->PRCODMAG)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_RIMA')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  PRTIPCON=this.w_PRTIPCON;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and PRCODMAG=&i_TN.->PRCODMAG;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_PRCODMAG<>space(5)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and PRCODMAG="+cp_ToStrODBC(&i_TN.->PRCODMAG)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and PRCODMAG=&i_TN.->PRCODMAG;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace PRCODMAG with this.w_PRCODMAG
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PAR_RIMA
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_RIMA')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PRSCOMIN="+cp_ToStrODBC(this.w_PRSCOMIN)+;
                     ",PRSCOMAX="+cp_ToStrODBC(this.w_PRSCOMAX)+;
                     ",PRQTAMIN="+cp_ToStrODBC(this.w_PRQTAMIN)+;
                     ",PRDISMIN="+cp_ToStrODBC(this.w_PRDISMIN)+;
                     ",PRQTAMAX="+cp_ToStrODBC(this.w_PRQTAMAX)+;
                     ",PRPUNRIO="+cp_ToStrODBC(this.w_PRPUNRIO)+;
                     ",PRLOTRIO="+cp_ToStrODBC(this.w_PRLOTRIO)+;
                     ",PRCLAABC="+cp_ToStrODBC(this.w_PRCLAABC)+;
                     ",PRPERABC="+cp_ToStrODBC(this.w_PRPERABC)+;
                     ",PRDATABC="+cp_ToStrODBC(this.w_PRDATABC)+;
                     ",PRCOSSTA="+cp_ToStrODBC(this.w_PRCOSSTA)+;
                     ",PRGIOINV="+cp_ToStrODBC(this.w_PRGIOINV)+;
                     ",PRGIOAPP="+cp_ToStrODBC(this.w_PRGIOAPP)+;
                     ",PRGIOCOP="+cp_ToStrODBC(this.w_PRGIOCOP)+;
                     ",PRTIPCON="+cp_ToStrODBC(this.w_PRTIPCON)+;
                     ",PRCODFOR="+cp_ToStrODBCNull(this.w_PRCODFOR)+;
                     ",PRCOERSS="+cp_ToStrODBC(this.w_PRCOERSS)+;
                     ",PRCOEFLT="+cp_ToStrODBC(this.w_PRCOEFLT)+;
                     ",PRSAFELT="+cp_ToStrODBC(this.w_PRSAFELT)+;
                     ",PRCODMAG="+cp_ToStrODBC(this.w_PRCODMAG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and PRCODMAG="+cp_ToStrODBC(PRCODMAG)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_RIMA')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PRSCOMIN=this.w_PRSCOMIN;
                     ,PRSCOMAX=this.w_PRSCOMAX;
                     ,PRQTAMIN=this.w_PRQTAMIN;
                     ,PRDISMIN=this.w_PRDISMIN;
                     ,PRQTAMAX=this.w_PRQTAMAX;
                     ,PRPUNRIO=this.w_PRPUNRIO;
                     ,PRLOTRIO=this.w_PRLOTRIO;
                     ,PRCLAABC=this.w_PRCLAABC;
                     ,PRPERABC=this.w_PRPERABC;
                     ,PRDATABC=this.w_PRDATABC;
                     ,PRCOSSTA=this.w_PRCOSSTA;
                     ,PRGIOINV=this.w_PRGIOINV;
                     ,PRGIOAPP=this.w_PRGIOAPP;
                     ,PRGIOCOP=this.w_PRGIOCOP;
                     ,PRTIPCON=this.w_PRTIPCON;
                     ,PRCODFOR=this.w_PRCODFOR;
                     ,PRCOERSS=this.w_PRCOERSS;
                     ,PRCOEFLT=this.w_PRCOEFLT;
                     ,PRSAFELT=this.w_PRSAFELT;
                     ,PRCODMAG=this.w_PRCODMAG;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and PRCODMAG=&i_TN.->PRCODMAG;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_RIMA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIMA_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_PRCODMAG<>space(5)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PAR_RIMA
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and PRCODMAG="+cp_ToStrODBC(&i_TN.->PRCODMAG)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and PRCODMAG=&i_TN.->PRCODMAG;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_PRCODMAG<>space(5)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_RIMA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_RIMA_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .w_PROVEN = this.oParentObject.oParentObject .w_ARPROPRE
          .w_CODDIS = this.oParentObject.oParentObject .w_ARCODDIS
          .w_OGGMPS = this.oParentObject .w_PROGGMPS
          .w_TIPGES = this.oParentObject.oParentObject .w_ARTIPGES
          .w_FLCOMM = this.oParentObject.oParentObject .w_ARFLCOMM
          .w_FLFRAZ = this.oParentObject.oParentObject .w_FLFRAZ
          .w_TIPART = this.oParentObject.oParentObject .w_ARTIPART
        .DoRTCalc(10,13,.t.)
        if .o_FLFRAZ<>.w_FLFRAZ.or. .o_TIPGES<>.w_TIPGES
          .w_PRQTAMIN = IIF(.w_TIPGES<>'S', .w_PRQTAMIN , 0)
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(15,25,.t.)
          .w_PRTIPCON = 'F'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(27,32,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_OBTEST with this.w_OBTEST
      replace t_DATOBSO with this.w_DATOBSO
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRQTAMIN_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRQTAMIN_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRPUNRIO_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPRPUNRIO_2_8.mCond()
    this.oPgFrm.Page1.oPag.oPRGIOCOP_2_16.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPRGIOCOP_2_16.mCond()
    this.oPgFrm.Page1.oPag.oPRCOEFLT_2_22.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPRCOEFLT_2_22.mCond()
    this.oPgFrm.Page1.oPag.oPRSAFELT_2_23.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPRSAFELT_2_23.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oPRCOERSS_2_21.visible=!this.oPgFrm.Page1.oPag.oPRCOERSS_2_21.mHide()
    this.oPgFrm.Page1.oPag.oPRCOEFLT_2_22.visible=!this.oPgFrm.Page1.oPag.oPRCOEFLT_2_22.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PRCODMAG
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PRCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PRCODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_PRCODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_PRCODMAG)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PRCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPRCODMAG_2_1'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PRCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PRCODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.MGCODMAG as MGCODMAG201"+ ",link_2_1.MGDESMAG as MGDESMAG201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on PAR_RIMA.PRCODMAG=link_2_1.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and PAR_RIMA.PRCODMAG=link_2_1.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PRCODFOR
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRCODFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PRCODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PRTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_PRTIPCON;
                     ,'ANCODICE',trim(this.w_PRCODFOR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PRCODFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PRCODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PRTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PRCODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_PRTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PRCODFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPRCODFOR_2_18'),i_cWhere,'GSAR_BZC',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PRTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Fornitore inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_PRTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRCODFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PRCODFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PRTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PRTIPCON;
                       ,'ANCODICE',this.w_PRCODFOR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRCODFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(_Link_.ANDTOBSO,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_PRCODFOR = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DATOBSO = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Fornitore inesistente o obsoleto")
        endif
        this.w_PRCODFOR = space(15)
        this.w_DESFOR = space(40)
        this.w_DATOBSO = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRCODFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESMAG_2_2.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_2_2.value=this.w_DESMAG
      replace t_DESMAG with this.oPgFrm.Page1.oPag.oDESMAG_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDISMIN_2_6.value==this.w_PRDISMIN)
      this.oPgFrm.Page1.oPag.oPRDISMIN_2_6.value=this.w_PRDISMIN
      replace t_PRDISMIN with this.oPgFrm.Page1.oPag.oPRDISMIN_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCLAABC_2_10.value==this.w_PRCLAABC)
      this.oPgFrm.Page1.oPag.oPRCLAABC_2_10.value=this.w_PRCLAABC
      replace t_PRCLAABC with this.oPgFrm.Page1.oPag.oPRCLAABC_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPERABC_2_11.value==this.w_PRPERABC)
      this.oPgFrm.Page1.oPag.oPRPERABC_2_11.value=this.w_PRPERABC
      replace t_PRPERABC with this.oPgFrm.Page1.oPag.oPRPERABC_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDATABC_2_12.value==this.w_PRDATABC)
      this.oPgFrm.Page1.oPag.oPRDATABC_2_12.value=this.w_PRDATABC
      replace t_PRDATABC with this.oPgFrm.Page1.oPag.oPRDATABC_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCOSSTA_2_13.value==this.w_PRCOSSTA)
      this.oPgFrm.Page1.oPag.oPRCOSSTA_2_13.value=this.w_PRCOSSTA
      replace t_PRCOSSTA with this.oPgFrm.Page1.oPag.oPRCOSSTA_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRGIOINV_2_14.value==this.w_PRGIOINV)
      this.oPgFrm.Page1.oPag.oPRGIOINV_2_14.value=this.w_PRGIOINV
      replace t_PRGIOINV with this.oPgFrm.Page1.oPag.oPRGIOINV_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRGIOAPP_2_15.value==this.w_PRGIOAPP)
      this.oPgFrm.Page1.oPag.oPRGIOAPP_2_15.value=this.w_PRGIOAPP
      replace t_PRGIOAPP with this.oPgFrm.Page1.oPag.oPRGIOAPP_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRGIOCOP_2_16.value==this.w_PRGIOCOP)
      this.oPgFrm.Page1.oPag.oPRGIOCOP_2_16.value=this.w_PRGIOCOP
      replace t_PRGIOCOP with this.oPgFrm.Page1.oPag.oPRGIOCOP_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCODFOR_2_18.value==this.w_PRCODFOR)
      this.oPgFrm.Page1.oPag.oPRCODFOR_2_18.value=this.w_PRCODFOR
      replace t_PRCODFOR with this.oPgFrm.Page1.oPag.oPRCODFOR_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_2_19.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_2_19.value=this.w_DESFOR
      replace t_DESFOR with this.oPgFrm.Page1.oPag.oDESFOR_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCOERSS_2_21.value==this.w_PRCOERSS)
      this.oPgFrm.Page1.oPag.oPRCOERSS_2_21.value=this.w_PRCOERSS
      replace t_PRCOERSS with this.oPgFrm.Page1.oPag.oPRCOERSS_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRCOEFLT_2_22.value==this.w_PRCOEFLT)
      this.oPgFrm.Page1.oPag.oPRCOEFLT_2_22.value=this.w_PRCOEFLT
      replace t_PRCOEFLT with this.oPgFrm.Page1.oPag.oPRCOEFLT_2_22.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPRSAFELT_2_23.value==this.w_PRSAFELT)
      this.oPgFrm.Page1.oPag.oPRSAFELT_2_23.value=this.w_PRSAFELT
      replace t_PRSAFELT with this.oPgFrm.Page1.oPag.oPRSAFELT_2_23.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRCODMAG_2_1.value==this.w_PRCODMAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRCODMAG_2_1.value=this.w_PRCODMAG
      replace t_PRCODMAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRCODMAG_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRSCOMIN_2_3.value==this.w_PRSCOMIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRSCOMIN_2_3.value=this.w_PRSCOMIN
      replace t_PRSCOMIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRSCOMIN_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRSCOMAX_2_4.value==this.w_PRSCOMAX)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRSCOMAX_2_4.value=this.w_PRSCOMAX
      replace t_PRSCOMAX with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRSCOMAX_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRQTAMIN_2_5.value==this.w_PRQTAMIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRQTAMIN_2_5.value=this.w_PRQTAMIN
      replace t_PRQTAMIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRQTAMIN_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRQTAMAX_2_7.value==this.w_PRQTAMAX)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRQTAMAX_2_7.value=this.w_PRQTAMAX
      replace t_PRQTAMAX with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRQTAMAX_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRPUNRIO_2_8.value==this.w_PRPUNRIO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRPUNRIO_2_8.value=this.w_PRPUNRIO
      replace t_PRPUNRIO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRPUNRIO_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRLOTRIO_2_9.value==this.w_PRLOTRIO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRLOTRIO_2_9.value=this.w_PRLOTRIO
      replace t_PRLOTRIO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRLOTRIO_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'PAR_RIMA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not((.w_PRSCOMIN<=.w_PRSCOMAX OR .w_PRSCOMAX=0) AND (.w_FLFRAZ<>'S' OR .w_PRSCOMIN=INT(.w_PRSCOMIN))) and (.w_PRCODMAG<>space(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRSCOMIN_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� non frazionabile o non corretta rispetto alla scorta massima")
        case   not((.w_PRSCOMIN<=.w_PRSCOMAX OR .w_PRSCOMAX=0) and (.w_FLFRAZ<>'S' OR .w_PRSCOMAX=INT(.w_PRSCOMAX)) and .w_PRSCOMAX>=0) and (.w_PRCODMAG<>space(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRSCOMAX_2_4
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� non frazionabile o non corretta rispetto alla scorta di sicurezza")
        case   not((.w_FLFRAZ<>'S' OR .w_PRQTAMIN=INT(.w_PRQTAMIN)) and .w_PRQTAMIN>=0) and (.w_TIPGES<>'S') and (.w_PRCODMAG<>space(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRQTAMIN_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� non frazionabile")
        case   not((.w_FLFRAZ<>'S' OR .w_PRDISMIN=INT(.w_PRDISMIN)) and .w_PRDISMIN>=0) and (.w_PRCODMAG<>space(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oPRDISMIN_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� non frazionabile")
        case   not((.w_FLFRAZ<>'S' OR .w_PRQTAMAX=INT(.w_PRQTAMAX)) and .w_PRQTAMAX>=0) and (.w_PRCODMAG<>space(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRQTAMAX_2_7
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Quantit� non frazionabile")
        case   (empty(.w_PRPUNRIO) or not((.w_TIPGES<>'S'  or .w_PRPUNRIO>0) and (.w_FLFRAZ<>'S' OR .w_PRPUNRIO=INT(.w_PRPUNRIO)) and .w_PRPUNRIO>=0)) and (.w_TIPGES='S') and (.w_PRCODMAG<>space(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRPUNRIO_2_8
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Q.t� non frazionabile oppure uguale a zero con gestione a scorta e prov.esterna")
        case   not((.w_TIPGES<>'S' or .w_PRLOTRIO>0) and (.w_FLFRAZ<>'S' OR .w_PRLOTRIO=INT(.w_PRLOTRIO)) and .w_PRLOTRIO>=0) and (.w_PRCODMAG<>space(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPRLOTRIO_2_9
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Q.t� non frazionabile oppure uguale a zero con gestione a scorta e prov.esterna")
        case   not(.w_PRGIOCOP>=0) and (.w_TIPGES = 'F' and .w_FLCOMM='N') and (.w_PRCODMAG<>space(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oPRGIOCOP_2_16
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and not(empty(.w_PRCODFOR)) and (.w_PRCODMAG<>space(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oPRCODFOR_2_18
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Fornitore inesistente o obsoleto")
        case   not(.w_PRCOERSS>=0 and .w_PRCOERSS<=1) and (.w_PRCODMAG<>space(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oPRCOERSS_2_21
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire un valore compreso tra zero e uno")
        case   not(.w_PRCOEFLT>=0 and .w_PRCOEFLT<=1) and (.w_PRGIOAPP>0 AND .w_PROVEN='I' AND NOT EMPTY(.w_CODDIS)) and (.w_PRCODMAG<>space(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oPRCOEFLT_2_22
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Indicare un valore tra 0 e 1")
        case   not(.w_PRSAFELT>=0) and (!.w_TIPGES$"SI" and .w_OGGMPS='N') and (.w_PRCODMAG<>space(5))
          .oNewFocus=.oPgFrm.Page1.oPag.oPRSAFELT_2_23
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if .w_PRCODMAG<>space(5)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPGES = this.w_TIPGES
    this.o_FLFRAZ = this.w_FLFRAZ
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_PRCODMAG<>space(5))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_PRCODMAG=space(5)
      .w_DESMAG=space(30)
      .w_PRSCOMIN=0
      .w_PRSCOMAX=0
      .w_PRQTAMIN=0
      .w_PRDISMIN=0
      .w_PRQTAMAX=0
      .w_PRPUNRIO=0
      .w_PRLOTRIO=0
      .w_PRCLAABC=space(1)
      .w_PRPERABC=0
      .w_PRDATABC=ctod("  /  /  ")
      .w_PRCOSSTA=0
      .w_PRGIOINV=0
      .w_PRGIOAPP=0
      .w_PRGIOCOP=0
      .w_PRCODFOR=space(15)
      .w_DESFOR=space(40)
      .w_DATOBSO=space(10)
      .w_PRCOERSS=0
      .w_PRCOEFLT=0
      .w_PRSAFELT=0
      .DoRTCalc(1,1,.f.)
        .w_OBTEST = i_DATSYS
      .DoRTCalc(3,10,.f.)
      if not(empty(.w_PRCODMAG))
        .link_2_1('Full')
      endif
      .DoRTCalc(11,13,.f.)
        .w_PRQTAMIN = IIF(.w_TIPGES<>'S', .w_PRQTAMIN , 0)
      .DoRTCalc(15,27,.f.)
      if not(empty(.w_PRCODFOR))
        .link_2_18('Full')
      endif
    endwith
    this.DoRTCalc(28,32,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_OBTEST = t_OBTEST
    this.w_PRCODMAG = t_PRCODMAG
    this.w_DESMAG = t_DESMAG
    this.w_PRSCOMIN = t_PRSCOMIN
    this.w_PRSCOMAX = t_PRSCOMAX
    this.w_PRQTAMIN = t_PRQTAMIN
    this.w_PRDISMIN = t_PRDISMIN
    this.w_PRQTAMAX = t_PRQTAMAX
    this.w_PRPUNRIO = t_PRPUNRIO
    this.w_PRLOTRIO = t_PRLOTRIO
    this.w_PRCLAABC = t_PRCLAABC
    this.w_PRPERABC = t_PRPERABC
    this.w_PRDATABC = t_PRDATABC
    this.w_PRCOSSTA = t_PRCOSSTA
    this.w_PRGIOINV = t_PRGIOINV
    this.w_PRGIOAPP = t_PRGIOAPP
    this.w_PRGIOCOP = t_PRGIOCOP
    this.w_PRCODFOR = t_PRCODFOR
    this.w_DESFOR = t_DESFOR
    this.w_DATOBSO = t_DATOBSO
    this.w_PRCOERSS = t_PRCOERSS
    this.w_PRCOEFLT = t_PRCOEFLT
    this.w_PRSAFELT = t_PRSAFELT
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_OBTEST with this.w_OBTEST
    replace t_PRCODMAG with this.w_PRCODMAG
    replace t_DESMAG with this.w_DESMAG
    replace t_PRSCOMIN with this.w_PRSCOMIN
    replace t_PRSCOMAX with this.w_PRSCOMAX
    replace t_PRQTAMIN with this.w_PRQTAMIN
    replace t_PRDISMIN with this.w_PRDISMIN
    replace t_PRQTAMAX with this.w_PRQTAMAX
    replace t_PRPUNRIO with this.w_PRPUNRIO
    replace t_PRLOTRIO with this.w_PRLOTRIO
    replace t_PRCLAABC with this.w_PRCLAABC
    replace t_PRPERABC with this.w_PRPERABC
    replace t_PRDATABC with this.w_PRDATABC
    replace t_PRCOSSTA with this.w_PRCOSSTA
    replace t_PRGIOINV with this.w_PRGIOINV
    replace t_PRGIOAPP with this.w_PRGIOAPP
    replace t_PRGIOCOP with this.w_PRGIOCOP
    replace t_PRCODFOR with this.w_PRCODFOR
    replace t_DESFOR with this.w_DESFOR
    replace t_DATOBSO with this.w_DATOBSO
    replace t_PRCOERSS with this.w_PRCOERSS
    replace t_PRCOEFLT with this.w_PRCOEFLT
    replace t_PRSAFELT with this.w_PRSAFELT
    if i_srv='A'
      replace PRCODMAG with this.w_PRCODMAG
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsma_mprPag1 as StdContainer
  Width  = 730
  height = 394
  stdWidth  = 730
  stdheight = 394
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="BMNZDJMPKL",left=4, top=3, width=719,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="PRCODMAG",Label1="Magazzino",Field2="PRSCOMIN",Label2="Scorta minima",Field3="PRSCOMAX",Label3="Scorta massima",Field4="PRQTAMIN",Label4="Q.t� min di riordino",Field5="PRQTAMAX",Label5="Q.t� max di riordino",Field6="PRPUNRIO",Label6="Punto di riordino",Field7="PRLOTRIO",Label7="Lotto di riordino",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168603258

  add object oStr_1_10 as StdString with uid="KMJUHWNALO",Visible=.t., Left=26, Top=247,;
    Alignment=1, Width=119, Height=15,;
    Caption="Classe ABC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="RVPUPGSFQA",Visible=.t., Left=180, Top=247,;
    Alignment=1, Width=15, Height=15,;
    Caption="%:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="KNJABZGGSR",Visible=.t., Left=22, Top=272,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data ultima analisi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="DTBWQPQDWE",Visible=.t., Left=390, Top=272,;
    Alignment=1, Width=132, Height=18,;
    Caption="Max.gg.invenduto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="RYOVVFPYKK",Visible=.t., Left=26, Top=222,;
    Alignment=1, Width=119, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="WUISOGFLOU",Visible=.t., Left=390, Top=295,;
    Alignment=1, Width=132, Height=18,;
    Caption="N.gg. approv.:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_PROVEN='I' AND NOT EMPTY(.w_CODDIS))
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="KFTHEZQCFH",Visible=.t., Left=390, Top=248,;
    Alignment=1, Width=132, Height=15,;
    Caption="Costo standard:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="RXUZXHFKYS",Visible=.t., Left=36, Top=368,;
    Alignment=1, Width=109, Height=15,;
    Caption="Fornitore abituale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="VMPIHOBGLN",Visible=.t., Left=44, Top=344,;
    Alignment=1, Width=101, Height=15,;
    Caption="Giorni copertura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="LCVPGURDNX",Visible=.t., Left=28, Top=296,;
    Alignment=1, Width=117, Height=15,;
    Caption="Disponibilit� minima:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="CNTYFRZDXN",Visible=.t., Left=400, Top=320,;
    Alignment=1, Width=122, Height=15,;
    Caption="Coeff. x LT variabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (NOT (.w_PROVEN='I' AND NOT EMPTY(.w_CODDIS)))
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="KSWQHGIGXR",Visible=.t., Left=28, Top=320,;
    Alignment=1, Width=117, Height=15,;
    Caption="Coeff. ripristino SS:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_TIPGES <> 'F' or ! .w_PRSCOMIN>0)
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="EOOWBKNHBS",Visible=.t., Left=401, Top=344,;
    Alignment=1, Width=121, Height=15,;
    Caption="Lead-time sicurezza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="YVRCAYUATH",Visible=.t., Left=409, Top=295,;
    Alignment=1, Width=113, Height=18,;
    Caption="Lead-time fisso:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (NOT (.w_PROVEN='I' AND NOT EMPTY(.w_CODDIS)))
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=22,;
    width=714+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=23,width=713+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='MAGAZZIN|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESMAG_2_2.Refresh()
      this.Parent.oPRDISMIN_2_6.Refresh()
      this.Parent.oPRCLAABC_2_10.Refresh()
      this.Parent.oPRPERABC_2_11.Refresh()
      this.Parent.oPRDATABC_2_12.Refresh()
      this.Parent.oPRCOSSTA_2_13.Refresh()
      this.Parent.oPRGIOINV_2_14.Refresh()
      this.Parent.oPRGIOAPP_2_15.Refresh()
      this.Parent.oPRGIOCOP_2_16.Refresh()
      this.Parent.oPRCODFOR_2_18.Refresh()
      this.Parent.oDESFOR_2_19.Refresh()
      this.Parent.oPRCOERSS_2_21.Refresh()
      this.Parent.oPRCOEFLT_2_22.Refresh()
      this.Parent.oPRSAFELT_2_23.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='MAGAZZIN'
        oDropInto=this.oBodyCol.oRow.oPRCODMAG_2_1
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDESMAG_2_2 as StdTrsField with uid="RVAYWXJCHF",rtseq=11,rtrep=.t.,;
    cFormVar="w_DESMAG",value=space(30),enabled=.f.,;
    HelpContextID = 224920010,;
    cTotal="", bFixedPos=.t., cQueryName = "DESMAG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=506, Left=148, Top=220, InputMask=replicate('X',30)

  add object oPRDISMIN_2_6 as StdTrsField with uid="KODGCYWRQU",rtseq=15,rtrep=.t.,;
    cFormVar="w_PRDISMIN",value=0,;
    ToolTipText = "Disponibilit� minima dell'articolo",;
    HelpContextID = 162733124,;
    cTotal="", bFixedPos=.t., cQueryName = "PRDISMIN",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� non frazionabile",;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=148, Top=292, cSayPict=[v_PQ(14)], cGetPict=[v_GQ(14)]

  func oPRDISMIN_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_FLFRAZ<>'S' OR .w_PRDISMIN=INT(.w_PRDISMIN)) and .w_PRDISMIN>=0)
    endwith
    return bRes
  endfunc

  add object oPRCLAABC_2_10 as StdTrsField with uid="SUCMRNKMDB",rtseq=19,rtrep=.t.,;
    cFormVar="w_PRCLAABC",value=space(1),;
    ToolTipText = "Classe ABC di appartenenza dell'articolo",;
    HelpContextID = 57275335,;
    cTotal="", bFixedPos=.t., cQueryName = "PRCLAABC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=148, Top=244, cSayPict=['!'], cGetPict=['!'], InputMask=replicate('X',1)

  add object oPRPERABC_2_11 as StdTrsField with uid="PPXDGIEJEP",rtseq=20,rtrep=.t.,;
    cFormVar="w_PRPERABC",value=0,;
    ToolTipText = "Percentuale di incidenza dell'articolo sul costo globale di acquisto",;
    HelpContextID = 39855047,;
    cTotal="", bFixedPos=.t., cQueryName = "PRPERABC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=197, Top=244, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oPRDATABC_2_12 as StdTrsField with uid="OJLDUMMEAN",rtseq=21,rtrep=.t.,;
    cFormVar="w_PRDATABC",value=ctod("  /  /  "),;
    ToolTipText = "Data dell'ultima elaborazione relativa all'analisi ABC",;
    HelpContextID = 38069191,;
    cTotal="", bFixedPos=.t., cQueryName = "PRDATABC",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=148, Top=268

  add object oPRCOSSTA_2_13 as StdTrsField with uid="MNLWYSLSAY",rtseq=22,rtrep=.t.,;
    cFormVar="w_PRCOSSTA",value=0,;
    ToolTipText = "Costo standard",;
    HelpContextID = 263785527,;
    cTotal="", bFixedPos=.t., cQueryName = "PRCOSSTA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=525, Top=245, cSayPict=[v_PU(18)], cGetPict=[v_GU(18)]

  add object oPRGIOINV_2_14 as StdTrsField with uid="ZSYPDNSYMN",rtseq=23,rtrep=.t.,;
    cFormVar="w_PRGIOINV",value=0,;
    ToolTipText = "Numero massimo di giorni dopo i quali l'articolo compare nella stampa invenduto",;
    HelpContextID = 176993204,;
    cTotal="", bFixedPos=.t., cQueryName = "PRGIOINV",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=525, Top=269, cSayPict=["999"], cGetPict=["999"]

  add object oPRGIOAPP_2_15 as StdTrsField with uid="HOVXTDTUUG",rtseq=24,rtrep=.t.,;
    cFormVar="w_PRGIOAPP",value=0,;
    ToolTipText = "Numero di giorni necessario all'approvvigionamento dell'articolo",;
    HelpContextID = 42775482,;
    cTotal="", bFixedPos=.t., cQueryName = "PRGIOAPP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=525, Top=293, cSayPict=["999"], cGetPict=["999"]

  add object oPRGIOCOP_2_16 as StdTrsField with uid="MVMKQYHOZS",rtseq=25,rtrep=.t.,;
    cFormVar="w_PRGIOCOP",value=0,;
    HelpContextID = 9221050,;
    cTotal="", bFixedPos=.t., cQueryName = "PRGIOCOP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=148, Top=340, cSayPict=["999"], cGetPict=["999"]

  func oPRGIOCOP_2_16.mCond()
    with this.Parent.oContained
      return (.w_TIPGES = 'F' and .w_FLCOMM='N')
    endwith
  endfunc

  func oPRGIOCOP_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRGIOCOP>=0)
    endwith
    return bRes
  endfunc

  add object oPRCODFOR_2_18 as StdTrsField with uid="YKPCIKLAKU",rtseq=27,rtrep=.t.,;
    cFormVar="w_PRCODFOR",value=space(15),;
    ToolTipText = "Codice del fornitore abituale",;
    HelpContextID = 238482360,;
    cTotal="", bFixedPos=.t., cQueryName = "PRCODFOR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Fornitore inesistente o obsoleto",;
   bGlobalFont=.t.,;
    Height=21, Width=138, Left=148, Top=364, cSayPict=[p_FOR], cGetPict=[p_FOR], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_PRTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PRCODFOR"

  func oPRCODFOR_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCODFOR_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPRCODFOR_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PRTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_PRTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPRCODFOR_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oPRCODFOR_2_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_PRTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PRCODFOR
    i_obj.ecpSave()
  endproc

  add object oDESFOR_2_19 as StdTrsField with uid="NWNPNKWAHA",rtseq=28,rtrep=.t.,;
    cFormVar="w_DESFOR",value=space(40),enabled=.f.,;
    HelpContextID = 26149322,;
    cTotal="", bFixedPos=.t., cQueryName = "DESFOR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=291, Top=364, InputMask=replicate('X',40)

  add object oPRCOERSS_2_21 as StdTrsField with uid="QYMTXMPAOD",rtseq=30,rtrep=.t.,;
    cFormVar="w_PRCOERSS",value=0,;
    ToolTipText = "Coeff. per la determinazione della data di consegna per il reintegro della SS",;
    HelpContextID = 232328265,;
    cTotal="", bFixedPos=.t., cQueryName = "PRCOERSS",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un valore compreso tra zero e uno",;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=148, Top=316, cSayPict=["9.99"], cGetPict=["9.99"]

  func oPRCOERSS_2_21.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPGES <> 'F' or ! .w_PRSCOMIN>0)
    endwith
    endif
  endfunc

  func oPRCOERSS_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRCOERSS>=0 and .w_PRCOERSS<=1)
    endwith
    return bRes
  endfunc

  add object oPRCOEFLT_2_22 as StdTrsField with uid="VTNWXROFNH",rtseq=31,rtrep=.t.,;
    cFormVar="w_PRCOEFLT",value=0,;
    ToolTipText = "Coefficiente per calcolo del lead-time variabile",;
    HelpContextID = 237433782,;
    cTotal="", bFixedPos=.t., cQueryName = "PRCOEFLT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Indicare un valore tra 0 e 1",;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=525, Top=317, cSayPict=["9.9999"], cGetPict=["9.9999"]

  func oPRCOEFLT_2_22.mCond()
    with this.Parent.oContained
      return (.w_PRGIOAPP>0 AND .w_PROVEN='I' AND NOT EMPTY(.w_CODDIS))
    endwith
  endfunc

  func oPRCOEFLT_2_22.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT (.w_PROVEN='I' AND NOT EMPTY(.w_CODDIS)))
    endwith
    endif
  endfunc

  func oPRCOEFLT_2_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRCOEFLT>=0 and .w_PRCOEFLT<=1)
    endwith
    return bRes
  endfunc

  add object oPRSAFELT_2_23 as StdTrsField with uid="HQCHLJXLVV",rtseq=32,rtrep=.t.,;
    cFormVar="w_PRSAFELT",value=0,;
    ToolTipText = "Lead-time sicurezza",;
    HelpContextID = 254014390,;
    cTotal="", bFixedPos=.t., cQueryName = "PRSAFELT",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=525, Top=341, cSayPict=["999.99"], cGetPict=["999.99"]

  func oPRSAFELT_2_23.mCond()
    with this.Parent.oContained
      return (!.w_TIPGES$"SI" and .w_OGGMPS='N')
    endwith
  endfunc

  func oPRSAFELT_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PRSAFELT>=0)
    endwith
    return bRes
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsma_mprBodyRow as CPBodyRowCnt
  Width=704
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPRCODMAG_2_1 as StdTrsField with uid="GDQWNMTFND",rtseq=10,rtrep=.t.,;
    cFormVar="w_PRCODMAG",value=space(5),isprimarykey=.t.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 121041859,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=71, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PRCODMAG"

  func oPRCODMAG_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPRCODMAG_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPRCODMAG_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oPRCODMAG_2_1.readonly and this.parent.oPRCODMAG_2_1.isprimarykey)
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPRCODMAG_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
   endif
  endproc
  proc oPRCODMAG_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_PRCODMAG
    i_obj.ecpSave()
  endproc

  add object oPRSCOMIN_2_3 as StdTrsField with uid="HMOGPIGBTR",rtseq=12,rtrep=.t.,;
    cFormVar="w_PRSCOMIN",value=0,;
    ToolTipText = "Scorta minima dell'articolo",;
    HelpContextID = 158207044,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� non frazionabile o non corretta rispetto alla scorta massima",;
   bGlobalFont=.t.,;
    Height=17, Width=100, Left=74, Top=0, cSayPict=[v_PQ(14)], cGetPict=[v_GQ(14)]

  func oPRSCOMIN_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PRSCOMIN<=.w_PRSCOMAX OR .w_PRSCOMAX=0) AND (.w_FLFRAZ<>'S' OR .w_PRSCOMIN=INT(.w_PRSCOMIN)))
    endwith
    return bRes
  endfunc

  add object oPRSCOMAX_2_4 as StdTrsField with uid="KLGHTRXUEJ",rtseq=13,rtrep=.t.,;
    cFormVar="w_PRSCOMAX",value=0,;
    ToolTipText = "Scorta massima dell'articolo",;
    HelpContextID = 110228402,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� non frazionabile o non corretta rispetto alla scorta di sicurezza",;
   bGlobalFont=.t.,;
    Height=17, Width=100, Left=179, Top=0, cSayPict=[v_PQ(14)], cGetPict=[v_GQ(14)]

  func oPRSCOMAX_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PRSCOMIN<=.w_PRSCOMAX OR .w_PRSCOMAX=0) and (.w_FLFRAZ<>'S' OR .w_PRSCOMAX=INT(.w_PRSCOMAX)) and .w_PRSCOMAX>=0)
    endwith
    return bRes
  endfunc

  add object oPRQTAMIN_2_5 as StdTrsField with uid="ZWHJEITMTD",rtseq=14,rtrep=.t.,;
    cFormVar="w_PRQTAMIN",value=0,;
    HelpContextID = 144632900,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� non frazionabile",;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=284, Top=0

  func oPRQTAMIN_2_5.mCond()
    with this.Parent.oContained
      return (.w_TIPGES<>'S')
    endwith
  endfunc

  func oPRQTAMIN_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_FLFRAZ<>'S' OR .w_PRQTAMIN=INT(.w_PRQTAMIN)) and .w_PRQTAMIN>=0)
    endwith
    return bRes
  endfunc

  add object oPRQTAMAX_2_7 as StdTrsField with uid="LDZZMSYCSP",rtseq=16,rtrep=.t.,;
    cFormVar="w_PRQTAMAX",value=0,;
    HelpContextID = 123802546,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� non frazionabile",;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=386, Top=0

  func oPRQTAMAX_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_FLFRAZ<>'S' OR .w_PRQTAMAX=INT(.w_PRQTAMAX)) and .w_PRQTAMAX>=0)
    endwith
    return bRes
  endfunc

  add object oPRPUNRIO_2_8 as StdTrsField with uid="KXOAPTEMNH",rtseq=17,rtrep=.t.,;
    cFormVar="w_PRPUNRIO",value=0,;
    ToolTipText = "Punto di riordino dell'articolo",;
    HelpContextID = 242211909,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Q.t� non frazionabile oppure uguale a zero con gestione a scorta e prov.esterna",;
   bGlobalFont=.t.,;
    Height=17, Width=100, Left=488, Top=0, cSayPict=[v_PQ(14)], cGetPict=[v_GQ(14)]

  func oPRPUNRIO_2_8.mCond()
    with this.Parent.oContained
      return (.w_TIPGES='S')
    endwith
  endfunc

  func oPRPUNRIO_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_TIPGES<>'S'  or .w_PRPUNRIO>0) and (.w_FLFRAZ<>'S' OR .w_PRPUNRIO=INT(.w_PRPUNRIO)) and .w_PRPUNRIO>=0)
    endwith
    return bRes
  endfunc

  add object oPRLOTRIO_2_9 as StdTrsField with uid="ZHAPUEPVZN",rtseq=18,rtrep=.t.,;
    cFormVar="w_PRLOTRIO",value=0,;
    ToolTipText = "Lotto di riordino dell'articolo",;
    HelpContextID = 248093765,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Q.t� non frazionabile oppure uguale a zero con gestione a scorta e prov.esterna",;
   bGlobalFont=.t.,;
    Height=17, Width=106, Left=593, Top=0, cSayPict=[v_PQ(14)], cGetPict=[v_GQ(14)]

  func oPRLOTRIO_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_TIPGES<>'S' or .w_PRLOTRIO>0) and (.w_FLFRAZ<>'S' OR .w_PRLOTRIO=INT(.w_PRLOTRIO)) and .w_PRLOTRIO>=0)
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oPRCODMAG_2_1.When()
    return(.t.)
  proc oPRCODMAG_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPRCODMAG_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_mpr','PAR_RIMA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PRCODART=PAR_RIMA.PRCODART";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
