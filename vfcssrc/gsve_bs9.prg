* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bs9                                                        *
*              Control righe incasso corr.                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_21]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-25                                                      *
* Last revis.: 2000-04-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bs9",oParentObject,m.pOPER)
return(i_retval)

define class tgsve_bs9 as StdBatch
  * --- Local variables
  pOPER = space(4)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue Calcoli/Controlli dopo l'input Importo Saldato e Flag Saldato (GSVE_MIC)
    * --- Parametro pOPER:
    * --- 'IMPO' input eseguito su Importo Saldato
    * --- 'FLAG' input eseguito su Flag Saldato
    do case
      case this.pOPER="IMPO"
        do case
          case ABS(this.oParentObject.w_INIMPSAL)>=ABS(this.oParentObject.w_INTOTIMP)
            this.oParentObject.w_INFLSALD = "S"
            this.oParentObject.w_INIMPABB = 0
            this.oParentObject.w_INIMPSAL = this.oParentObject.w_INTOTIMP
          case this.oParentObject.w_INFLSALD="S"
            this.oParentObject.w_INIMPABB = this.oParentObject.w_INTOTIMP - this.oParentObject.w_INIMPSAL
        endcase
      case this.pOPER="FLAG"
        do case
          case this.oParentObject.w_INFLSALD="S" AND this.oParentObject.w_INIMPSAL=0
            this.oParentObject.w_INIMPSAL = this.oParentObject.w_INTOTIMP
            this.oParentObject.w_INIMPABB = 0
          case this.oParentObject.w_INFLSALD="S" AND ABS(this.oParentObject.w_INIMPSAL)<=ABS(this.oParentObject.w_INTOTIMP)
            this.oParentObject.w_INIMPABB = this.oParentObject.w_INTOTIMP - this.oParentObject.w_INIMPSAL
          case this.oParentObject.w_INFLSALD="S" AND ABS(this.oParentObject.w_INIMPSAL)>ABS(this.oParentObject.w_INTOTIMP)
            this.oParentObject.w_INIMPSAL = this.oParentObject.w_INTOTIMP
            this.oParentObject.w_INIMPABB = 0
          case this.oParentObject.w_INFLSALD<>"S"
            this.oParentObject.w_INIMPABB = 0
            if ABS(this.oParentObject.w_INIMPSAL)>=ABS(this.oParentObject.w_INTOTIMP)
              this.oParentObject.w_TOTSAL = this.oParentObject.w_TOTSAL - this.oParentObject.w_DARSAL
              this.oParentObject.w_DARSAL = 0
              this.oParentObject.w_INIMPSAL = 0
            endif
        endcase
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
