* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_apl                                                        *
*              Parametri PostaLite                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-06-13                                                      *
* Last revis.: 2011-05-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsut_apl")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsut_apl")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsut_apl")
  return

* --- Class definition
define class tgsut_apl as StdPCForm
  Width  = 508
  Height = 219
  Top    = 10
  Left   = 66
  cComment = "Parametri PostaLite"
  cPrg = "gsut_apl"
  HelpContextID=108444823
  add object cnt as tcgsut_apl
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsut_apl as PCContext
  w_PLSERIAL = 0
  w_PDF = space(3)
  w_PC_NAME = space(20)
  w_COSTANTE = space(13)
  w_CHEKXFRX = space(10)
  w_CHEKXFRX_b = space(10)
  w_SELECTED_A = space(1)
  w_CHEKXFRX_A = space(20)
  w_PLTIPSTA = space(3)
  w_PLURLPLT = space(50)
  w_PLFILAFP = space(50)
  w_PLDEVICE = space(50)
  w_PLPATINC = space(50)
  w_PLPATOUT = space(50)
  w_PLPATTMP = space(50)
  w_SELECTED = space(1)
  w_SELECTED_b = space(1)
  w_TYPECLAS_A = space(20)
  proc Save(oFrom)
    this.w_PLSERIAL = oFrom.w_PLSERIAL
    this.w_PDF = oFrom.w_PDF
    this.w_PC_NAME = oFrom.w_PC_NAME
    this.w_COSTANTE = oFrom.w_COSTANTE
    this.w_CHEKXFRX = oFrom.w_CHEKXFRX
    this.w_CHEKXFRX_b = oFrom.w_CHEKXFRX_b
    this.w_SELECTED_A = oFrom.w_SELECTED_A
    this.w_CHEKXFRX_A = oFrom.w_CHEKXFRX_A
    this.w_PLTIPSTA = oFrom.w_PLTIPSTA
    this.w_PLURLPLT = oFrom.w_PLURLPLT
    this.w_PLFILAFP = oFrom.w_PLFILAFP
    this.w_PLDEVICE = oFrom.w_PLDEVICE
    this.w_PLPATINC = oFrom.w_PLPATINC
    this.w_PLPATOUT = oFrom.w_PLPATOUT
    this.w_PLPATTMP = oFrom.w_PLPATTMP
    this.w_SELECTED = oFrom.w_SELECTED
    this.w_SELECTED_b = oFrom.w_SELECTED_b
    this.w_TYPECLAS_A = oFrom.w_TYPECLAS_A
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_PLSERIAL = this.w_PLSERIAL
    oTo.w_PDF = this.w_PDF
    oTo.w_PC_NAME = this.w_PC_NAME
    oTo.w_COSTANTE = this.w_COSTANTE
    oTo.w_CHEKXFRX = this.w_CHEKXFRX
    oTo.w_CHEKXFRX_b = this.w_CHEKXFRX_b
    oTo.w_SELECTED_A = this.w_SELECTED_A
    oTo.w_CHEKXFRX_A = this.w_CHEKXFRX_A
    oTo.w_PLTIPSTA = this.w_PLTIPSTA
    oTo.w_PLURLPLT = this.w_PLURLPLT
    oTo.w_PLFILAFP = this.w_PLFILAFP
    oTo.w_PLDEVICE = this.w_PLDEVICE
    oTo.w_PLPATINC = this.w_PLPATINC
    oTo.w_PLPATOUT = this.w_PLPATOUT
    oTo.w_PLPATTMP = this.w_PLPATTMP
    oTo.w_SELECTED = this.w_SELECTED
    oTo.w_SELECTED_b = this.w_SELECTED_b
    oTo.w_TYPECLAS_A = this.w_TYPECLAS_A
    PCContext::Load(oTo)
enddefine

define class tcgsut_apl as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 508
  Height = 219
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-05"
  HelpContextID=108444823
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  POS_LITE_IDX = 0
  STMPFILE_IDX = 0
  cFile = "POS_LITE"
  cKeySelect = "PLSERIAL"
  cKeyWhere  = "PLSERIAL=this.w_PLSERIAL"
  cKeyWhereODBC = '"PLSERIAL="+cp_ToStrODBC(this.w_PLSERIAL)';

  cKeyWhereODBCqualified = '"POS_LITE.PLSERIAL="+cp_ToStrODBC(this.w_PLSERIAL)';

  cPrg = "gsut_apl"
  cComment = "Parametri PostaLite"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PLSERIAL = 0
  w_PDF = space(3)
  w_PC_NAME = space(20)
  w_COSTANTE = space(13)
  w_CHEKXFRX = space(10)
  w_CHEKXFRX_b = space(10)
  w_SELECTED_A = space(1)
  w_CHEKXFRX_A = space(20)
  w_PLTIPSTA = space(3)
  o_PLTIPSTA = space(3)
  w_PLURLPLT = space(50)
  w_PLFILAFP = space(50)
  w_PLDEVICE = space(50)
  w_PLPATINC = space(50)
  w_PLPATOUT = space(50)
  w_PLPATTMP = space(50)
  w_SELECTED = space(1)
  w_SELECTED_b = space(1)
  w_TYPECLAS_A = space(20)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_aplPag1","gsut_apl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 166892278
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPLTIPSTA_1_9
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='STMPFILE'
    this.cWorkTables[2]='POS_LITE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.POS_LITE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.POS_LITE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsut_apl'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from POS_LITE where PLSERIAL=KeySet.PLSERIAL
    *
    i_nConn = i_TableProp[this.POS_LITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.POS_LITE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('POS_LITE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "POS_LITE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' POS_LITE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PLSERIAL',this.w_PLSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PDF = 'PDF'
        .w_PC_NAME = ALLTRIM(LEFT(SYS(0),AT('#',SYS(0))-1))
        .w_COSTANTE = 'INSTALLAZIONE'
        .w_CHEKXFRX = 'XFRX'
        .w_CHEKXFRX_b = 'XFRX'
        .w_SELECTED_A = 'S'
        .w_CHEKXFRX_A = .w_PC_NAME
        .w_SELECTED = space(1)
        .w_SELECTED_b = space(1)
        .w_TYPECLAS_A = space(20)
        .w_PLSERIAL = NVL(PLSERIAL,0)
          .link_1_5('Load')
          .link_1_6('Load')
          .link_1_8('Load')
        .w_PLTIPSTA = NVL(PLTIPSTA,space(3))
        .w_PLURLPLT = NVL(PLURLPLT,space(50))
        .w_PLFILAFP = NVL(PLFILAFP,space(50))
        .w_PLDEVICE = NVL(PLDEVICE,space(50))
        .w_PLPATINC = NVL(PLPATINC,space(50))
        .w_PLPATOUT = NVL(PLPATOUT,space(50))
        .w_PLPATTMP = NVL(PLPATTMP,space(50))
        cp_LoadRecExtFlds(this,'POS_LITE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_PLSERIAL = 0
      .w_PDF = space(3)
      .w_PC_NAME = space(20)
      .w_COSTANTE = space(13)
      .w_CHEKXFRX = space(10)
      .w_CHEKXFRX_b = space(10)
      .w_SELECTED_A = space(1)
      .w_CHEKXFRX_A = space(20)
      .w_PLTIPSTA = space(3)
      .w_PLURLPLT = space(50)
      .w_PLFILAFP = space(50)
      .w_PLDEVICE = space(50)
      .w_PLPATINC = space(50)
      .w_PLPATOUT = space(50)
      .w_PLPATTMP = space(50)
      .w_SELECTED = space(1)
      .w_SELECTED_b = space(1)
      .w_TYPECLAS_A = space(20)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_PDF = 'PDF'
        .w_PC_NAME = ALLTRIM(LEFT(SYS(0),AT('#',SYS(0))-1))
        .w_COSTANTE = 'INSTALLAZIONE'
        .w_CHEKXFRX = 'XFRX'
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_CHEKXFRX))
          .link_1_5('Full')
          endif
        .w_CHEKXFRX_b = 'XFRX'
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_CHEKXFRX_b))
          .link_1_6('Full')
          endif
        .w_SELECTED_A = 'S'
        .w_CHEKXFRX_A = .w_PC_NAME
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_CHEKXFRX_A))
          .link_1_8('Full')
          endif
        .w_PLTIPSTA = 'AFP'
          .DoRTCalc(10,10,.f.)
        .w_PLFILAFP = iif(.w_PLTIPSTA=='AFP','Postalite.AFP','Postalite.pdf')
      endif
    endwith
    cp_BlankRecExtFlds(this,'POS_LITE')
    this.DoRTCalc(12,18,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPLTIPSTA_1_9.enabled = i_bVal
      .Page1.oPag.oPLURLPLT_1_10.enabled = i_bVal
      .Page1.oPag.oPLFILAFP_1_11.enabled = i_bVal
      .Page1.oPag.oPLDEVICE_1_12.enabled = i_bVal
      .Page1.oPag.oPLPATINC_1_14.enabled = i_bVal
      .Page1.oPag.oPLPATOUT_1_17.enabled = i_bVal
      .Page1.oPag.oPLPATTMP_1_20.enabled = i_bVal
      .Page1.oPag.oBtn_1_13.enabled = i_bVal
      .Page1.oPag.oBtn_1_16.enabled = i_bVal
      .Page1.oPag.oBtn_1_19.enabled = i_bVal
      .Page1.oPag.oBtn_1_22.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'POS_LITE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.POS_LITE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLSERIAL,"PLSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLTIPSTA,"PLTIPSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLURLPLT,"PLURLPLT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLFILAFP,"PLFILAFP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLDEVICE,"PLDEVICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLPATINC,"PLPATINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLPATOUT,"PLPATOUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLPATTMP,"PLPATTMP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.POS_LITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.POS_LITE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.POS_LITE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into POS_LITE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'POS_LITE')
        i_extval=cp_InsertValODBCExtFlds(this,'POS_LITE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PLSERIAL,PLTIPSTA,PLURLPLT,PLFILAFP,PLDEVICE"+;
                  ",PLPATINC,PLPATOUT,PLPATTMP "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PLSERIAL)+;
                  ","+cp_ToStrODBC(this.w_PLTIPSTA)+;
                  ","+cp_ToStrODBC(this.w_PLURLPLT)+;
                  ","+cp_ToStrODBC(this.w_PLFILAFP)+;
                  ","+cp_ToStrODBC(this.w_PLDEVICE)+;
                  ","+cp_ToStrODBC(this.w_PLPATINC)+;
                  ","+cp_ToStrODBC(this.w_PLPATOUT)+;
                  ","+cp_ToStrODBC(this.w_PLPATTMP)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'POS_LITE')
        i_extval=cp_InsertValVFPExtFlds(this,'POS_LITE')
        cp_CheckDeletedKey(i_cTable,0,'PLSERIAL',this.w_PLSERIAL)
        INSERT INTO (i_cTable);
              (PLSERIAL,PLTIPSTA,PLURLPLT,PLFILAFP,PLDEVICE,PLPATINC,PLPATOUT,PLPATTMP  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PLSERIAL;
                  ,this.w_PLTIPSTA;
                  ,this.w_PLURLPLT;
                  ,this.w_PLFILAFP;
                  ,this.w_PLDEVICE;
                  ,this.w_PLPATINC;
                  ,this.w_PLPATOUT;
                  ,this.w_PLPATTMP;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.POS_LITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.POS_LITE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.POS_LITE_IDX,i_nConn)
      *
      * update POS_LITE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'POS_LITE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PLTIPSTA="+cp_ToStrODBC(this.w_PLTIPSTA)+;
             ",PLURLPLT="+cp_ToStrODBC(this.w_PLURLPLT)+;
             ",PLFILAFP="+cp_ToStrODBC(this.w_PLFILAFP)+;
             ",PLDEVICE="+cp_ToStrODBC(this.w_PLDEVICE)+;
             ",PLPATINC="+cp_ToStrODBC(this.w_PLPATINC)+;
             ",PLPATOUT="+cp_ToStrODBC(this.w_PLPATOUT)+;
             ",PLPATTMP="+cp_ToStrODBC(this.w_PLPATTMP)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'POS_LITE')
        i_cWhere = cp_PKFox(i_cTable  ,'PLSERIAL',this.w_PLSERIAL  )
        UPDATE (i_cTable) SET;
              PLTIPSTA=this.w_PLTIPSTA;
             ,PLURLPLT=this.w_PLURLPLT;
             ,PLFILAFP=this.w_PLFILAFP;
             ,PLDEVICE=this.w_PLDEVICE;
             ,PLPATINC=this.w_PLPATINC;
             ,PLPATOUT=this.w_PLPATOUT;
             ,PLPATTMP=this.w_PLPATTMP;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.POS_LITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.POS_LITE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.POS_LITE_IDX,i_nConn)
      *
      * delete POS_LITE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PLSERIAL',this.w_PLSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.POS_LITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.POS_LITE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .link_1_5('Full')
          .link_1_6('Full')
        .DoRTCalc(7,7,.t.)
          .link_1_8('Full')
        .DoRTCalc(9,10,.t.)
        if .o_PLTIPSTA<>.w_PLTIPSTA
            .w_PLFILAFP = iif(.w_PLTIPSTA=='AFP','Postalite.AFP','Postalite.pdf')
        endif
        if .o_PLTIPSTA<>.w_PLTIPSTA
          .Calculate_COYQHIDORD()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_COYQHIDORD()
    with this
          * --- Compila il campo device di stampa
          selectdev(this;
              ,'.w_PLDEVICE';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPLDEVICE_1_12.enabled = this.oPgFrm.Page1.oPag.oPLDEVICE_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CHEKXFRX
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STMPFILE_IDX,3]
    i_lTable = "STMPFILE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STMPFILE_IDX,2], .t., this.STMPFILE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STMPFILE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CHEKXFRX) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CHEKXFRX)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SFFORMAT,SFPCNAME,SFTYPECL,SFFLGSEL";
                   +" from "+i_cTable+" "+i_lTable+" where SFTYPECL="+cp_ToStrODBC(this.w_CHEKXFRX);
                   +" and SFFORMAT="+cp_ToStrODBC(this.w_PDF);
                   +" and SFPCNAME="+cp_ToStrODBC(this.w_PC_NAME);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SFFORMAT',this.w_PDF;
                       ,'SFPCNAME',this.w_PC_NAME;
                       ,'SFTYPECL',this.w_CHEKXFRX)
            select SFFORMAT,SFPCNAME,SFTYPECL,SFFLGSEL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CHEKXFRX = NVL(_Link_.SFTYPECL,space(10))
      this.w_SELECTED = NVL(_Link_.SFFLGSEL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CHEKXFRX = space(10)
      endif
      this.w_SELECTED = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STMPFILE_IDX,2])+'\'+cp_ToStr(_Link_.SFFORMAT,1)+'\'+cp_ToStr(_Link_.SFPCNAME,1)+'\'+cp_ToStr(_Link_.SFTYPECL,1)
      cp_ShowWarn(i_cKey,this.STMPFILE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CHEKXFRX Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CHEKXFRX_b
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STMPFILE_IDX,3]
    i_lTable = "STMPFILE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STMPFILE_IDX,2], .t., this.STMPFILE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STMPFILE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CHEKXFRX_b) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CHEKXFRX_b)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SFFORMAT,SFPCNAME,SFTYPECL,SFFLGSEL";
                   +" from "+i_cTable+" "+i_lTable+" where SFTYPECL="+cp_ToStrODBC(this.w_CHEKXFRX_b);
                   +" and SFFORMAT="+cp_ToStrODBC(this.w_PDF);
                   +" and SFPCNAME="+cp_ToStrODBC(this.w_COSTANTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SFFORMAT',this.w_PDF;
                       ,'SFPCNAME',this.w_COSTANTE;
                       ,'SFTYPECL',this.w_CHEKXFRX_b)
            select SFFORMAT,SFPCNAME,SFTYPECL,SFFLGSEL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CHEKXFRX_b = NVL(_Link_.SFTYPECL,space(10))
      this.w_SELECTED_b = NVL(_Link_.SFFLGSEL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CHEKXFRX_b = space(10)
      endif
      this.w_SELECTED_b = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STMPFILE_IDX,2])+'\'+cp_ToStr(_Link_.SFFORMAT,1)+'\'+cp_ToStr(_Link_.SFPCNAME,1)+'\'+cp_ToStr(_Link_.SFTYPECL,1)
      cp_ShowWarn(i_cKey,this.STMPFILE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CHEKXFRX_b Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CHEKXFRX_A
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STMPFILE_IDX,3]
    i_lTable = "STMPFILE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STMPFILE_IDX,2], .t., this.STMPFILE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STMPFILE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CHEKXFRX_A) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CHEKXFRX_A)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SFFORMAT,SFFLGSEL,SFPCNAME,SFTYPECL";
                   +" from "+i_cTable+" "+i_lTable+" where SFPCNAME="+cp_ToStrODBC(this.w_CHEKXFRX_A);
                   +" and SFFORMAT="+cp_ToStrODBC(this.w_PDF);
                   +" and SFFLGSEL="+cp_ToStrODBC(this.w_SELECTED_A);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SFFORMAT',this.w_PDF;
                       ,'SFFLGSEL',this.w_SELECTED_A;
                       ,'SFPCNAME',this.w_CHEKXFRX_A)
            select SFFORMAT,SFFLGSEL,SFPCNAME,SFTYPECL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CHEKXFRX_A = NVL(_Link_.SFPCNAME,space(20))
      this.w_TYPECLAS_A = NVL(_Link_.SFTYPECL,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CHEKXFRX_A = space(20)
      endif
      this.w_TYPECLAS_A = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STMPFILE_IDX,2])+'\'+cp_ToStr(_Link_.SFFORMAT,1)+'\'+cp_ToStr(_Link_.SFFLGSEL,1)+'\'+cp_ToStr(_Link_.SFPCNAME,1)
      cp_ShowWarn(i_cKey,this.STMPFILE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CHEKXFRX_A Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPLTIPSTA_1_9.RadioValue()==this.w_PLTIPSTA)
      this.oPgFrm.Page1.oPag.oPLTIPSTA_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPLURLPLT_1_10.value==this.w_PLURLPLT)
      this.oPgFrm.Page1.oPag.oPLURLPLT_1_10.value=this.w_PLURLPLT
    endif
    if not(this.oPgFrm.Page1.oPag.oPLFILAFP_1_11.value==this.w_PLFILAFP)
      this.oPgFrm.Page1.oPag.oPLFILAFP_1_11.value=this.w_PLFILAFP
    endif
    if not(this.oPgFrm.Page1.oPag.oPLDEVICE_1_12.value==this.w_PLDEVICE)
      this.oPgFrm.Page1.oPag.oPLDEVICE_1_12.value=this.w_PLDEVICE
    endif
    if not(this.oPgFrm.Page1.oPag.oPLPATINC_1_14.value==this.w_PLPATINC)
      this.oPgFrm.Page1.oPag.oPLPATINC_1_14.value=this.w_PLPATINC
    endif
    if not(this.oPgFrm.Page1.oPag.oPLPATOUT_1_17.value==this.w_PLPATOUT)
      this.oPgFrm.Page1.oPag.oPLPATOUT_1_17.value=this.w_PLPATOUT
    endif
    if not(this.oPgFrm.Page1.oPag.oPLPATTMP_1_20.value==this.w_PLPATTMP)
      this.oPgFrm.Page1.oPag.oPLPATTMP_1_20.value=this.w_PLPATTMP
    endif
    cp_SetControlsValueExtFlds(this,'POS_LITE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(AT(':',.w_PLPATINC)=0 and AT('\\',.w_PLPATINC)=0 and left(.w_PLPATINC,1)='\')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPLPATINC_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Indicare un percorso senza riferimento alla unit� fisica (es.: \webprint\incoming\ senza C: o D: o \\)")
          case   not(AT(':',.w_PLPATOUT)=0 and AT('\\',.w_PLPATOUT)=0 and left(.w_PLPATOUT,1)='\')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPLPATOUT_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Indicare un percorso senza riferimento alla unit� fisica (es.: \webprint\outcoming\ senza C: o D: o \\)")
          case   not(AT(':',.w_PLPATTMP)=0 and AT('\\',.w_PLPATTMP)=0 and left(.w_PLPATOUT,1)='\')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPLPATTMP_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Indicare un percorso senza riferimento alla unit� fisica (es.: \webprint\temp\ senza C: o D: o \\)")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PLTIPSTA = this.w_PLTIPSTA
    return

enddefine

* --- Define pages as container
define class tgsut_aplPag1 as StdContainer
  Width  = 504
  height = 219
  stdWidth  = 504
  stdheight = 219
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oPLTIPSTA_1_9 as StdCombo with uid="WPUFOTHVYN",rtseq=9,rtrep=.f.,left=168,top=31,width=169,height=22;
    , ToolTipText = "Tipologia del file generato dalla stampante virtuale";
    , HelpContextID = 20623817;
    , cFormVar="w_PLTIPSTA",RowSource=""+"AFP,"+"PDF - XFRX,"+"PDF - PDFCREATOR", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPLTIPSTA_1_9.RadioValue()
    return(iif(this.value =1,'AFP',;
    iif(this.value =2,'PDF',;
    iif(this.value =3,'PDC',;
    space(3)))))
  endfunc
  func oPLTIPSTA_1_9.GetRadio()
    this.Parent.oContained.w_PLTIPSTA = this.RadioValue()
    return .t.
  endfunc

  func oPLTIPSTA_1_9.SetRadio()
    this.Parent.oContained.w_PLTIPSTA=trim(this.Parent.oContained.w_PLTIPSTA)
    this.value = ;
      iif(this.Parent.oContained.w_PLTIPSTA=='AFP',1,;
      iif(this.Parent.oContained.w_PLTIPSTA=='PDF',2,;
      iif(this.Parent.oContained.w_PLTIPSTA=='PDC',3,;
      0)))
  endfunc

  add object oPLURLPLT_1_10 as StdField with uid="EGRRHFXXXM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PLURLPLT", cQueryName = "PLURLPLT",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 193879626,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=168, Top=60, InputMask=replicate('X',50)

  proc oPLURLPLT_1_10.mDefault
    with this.Parent.oContained
      if empty(.w_PLURLPLT)
        .w_PLURLPLT = 'http://www.Postalite.it'
      endif
    endwith
  endproc

  add object oPLFILAFP_1_11 as StdField with uid="DHTPKZGHCO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PLFILAFP", cQueryName = "PLFILAFP",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 58429882,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=168, Top=87, InputMask=replicate('X',50)

  add object oPLDEVICE_1_12 as StdField with uid="JKJSBBHFJB",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PLDEVICE", cQueryName = "PLDEVICE",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 182432197,;
   bGlobalFont=.t.,;
    Height=21, Width=231, Left=168, Top=114, InputMask=replicate('X',50)

  func oPLDEVICE_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PLTIPSTA#'PDF')
    endwith
   endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="IDYWOAAKXN",left=403, top=116, width=20,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 108645846;
    , tabstop=.f.;
  , bGlobalFont=.t.

    proc oBtn_1_13.Click()
      with this.Parent.oContained
        do selectdev with this.Parent.oContained, '.w_PLDEVICE'
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPLPATINC_1_14 as StdField with uid="GYGYHBIOJT",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PLPATINC", cQueryName = "PLPATINC",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    sErrorMsg = "Indicare un percorso senza riferimento alla unit� fisica (es.: \webprint\incoming\ senza C: o D: o \\)",;
    ToolTipText = "Cartella incoming (indicare senza riferimento all'unit� fisica)",;
    HelpContextID = 83693113,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=167, Top=141, InputMask=replicate('X',50)

  proc oPLPATINC_1_14.mDefault
    with this.Parent.oContained
      if empty(.w_PLPATINC)
        .w_PLPATINC = '\WEBPRINT\INCOMING\'
      endif
    endwith
  endproc

  func oPLPATINC_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (AT(':',.w_PLPATINC)=0 and AT('\\',.w_PLPATINC)=0 and left(.w_PLPATINC,1)='\')
    endwith
    return bRes
  endfunc


  add object oBtn_1_16 as StdButton with uid="YNTXTLFHTQ",left=476, top=143, width=20,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 108645846;
    , tabstop=.f.;
  , bGlobalFont=.t.

    proc oBtn_1_16.Click()
      with this.Parent.oContained
        do selectdir with this.Parent.oContained,'.w_PLPATINC'
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPLPATOUT_1_17 as StdField with uid="HJBATXOKJR",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PLPATOUT", cQueryName = "PLPATOUT",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    sErrorMsg = "Indicare un percorso senza riferimento alla unit� fisica (es.: \webprint\outcoming\ senza C: o D: o \\)",;
    ToolTipText = "Cartella outcoming (indicare senza riferimento all'unit� fisica)",;
    HelpContextID = 84079030,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=167, Top=166, InputMask=replicate('X',50)

  proc oPLPATOUT_1_17.mDefault
    with this.Parent.oContained
      if empty(.w_PLPATOUT)
        .w_PLPATOUT = '\WEBPRINT\OUTCOMING\'
      endif
    endwith
  endproc

  func oPLPATOUT_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (AT(':',.w_PLPATOUT)=0 and AT('\\',.w_PLPATOUT)=0 and left(.w_PLPATOUT,1)='\')
    endwith
    return bRes
  endfunc


  add object oBtn_1_19 as StdButton with uid="YIDGUQOLGN",left=476, top=168, width=20,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 108645846;
    , tabstop=.f.;
  , bGlobalFont=.t.

    proc oBtn_1_19.Click()
      with this.Parent.oContained
        do selectdir with this.Parent.oContained,'.w_PLPATOUT'
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPLPATTMP_1_20 as StdField with uid="WPYDITOEUW",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PLPATTMP", cQueryName = "PLPATTMP",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    sErrorMsg = "Indicare un percorso senza riferimento alla unit� fisica (es.: \webprint\temp\ senza C: o D: o \\)",;
    ToolTipText = "Cartella temp (indicare senza riferimento all'unit� fisica)",;
    HelpContextID = 268242502,;
   bGlobalFont=.t.,;
    Height=21, Width=303, Left=167, Top=191, InputMask=replicate('X',50)

  proc oPLPATTMP_1_20.mDefault
    with this.Parent.oContained
      if empty(.w_PLPATTMP)
        .w_PLPATTMP = '\WEBPRINT\TEMP\'
      endif
    endwith
  endproc

  func oPLPATTMP_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (AT(':',.w_PLPATTMP)=0 and AT('\\',.w_PLPATTMP)=0 and left(.w_PLPATOUT,1)='\')
    endwith
    return bRes
  endfunc


  add object oBtn_1_22 as StdButton with uid="WQGSWHLPNQ",left=476, top=193, width=20,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 108645846;
    , tabstop=.f.;
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      with this.Parent.oContained
        do selectdir with this.Parent.oContained,'.w_PLPATTMP'
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_15 as StdString with uid="SORFEUEWFU",Visible=.t., Left=38, Top=143,;
    Alignment=1, Width=126, Height=18,;
    Caption="Files in uscita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="SSIJYLRCMM",Visible=.t., Left=47, Top=168,;
    Alignment=1, Width=117, Height=18,;
    Caption="Files inviati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="QAYWNBFJTW",Visible=.t., Left=23, Top=193,;
    Alignment=1, Width=141, Height=18,;
    Caption="Files temporanei:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="EWLONWKOBC",Visible=.t., Left=59, Top=60,;
    Alignment=1, Width=106, Height=18,;
    Caption="URL PostaLite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="SARCJIDVLM",Visible=.t., Left=7, Top=115,;
    Alignment=1, Width=158, Height=18,;
    Caption="Device di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="RFJOFMPSXA",Visible=.t., Left=11, Top=89,;
    Alignment=1, Width=154, Height=18,;
    Caption="Nome file standard:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="SLSBRECDDC",Visible=.t., Left=11, Top=4,;
    Alignment=0, Width=130, Height=18,;
    Caption="Parametri generali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_28 as StdString with uid="MEWVWXUFMG",Visible=.t., Left=56, Top=31,;
    Alignment=1, Width=106, Height=18,;
    Caption="Mod. PostaLite:"  ;
  , bGlobalFont=.t.

  add object oBox_1_27 as StdBox with uid="GKMEFBCSXZ",left=10, top=20, width=483,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_apl','POS_LITE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PLSERIAL=POS_LITE.PLSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_apl
procedure selectdev

  para i_obj,i_vname

  local i_dev
  if INLIST(i_obj.w_PLTIPSTA,'AFP','PDC')
     i_dev=GETPRINTER()

    if not empty(i_dev)

      i_obj&i_vname = i_dev

    endif
   else 
     if i_obj.w_SELECTED ='S'  or (i_obj.w_SELECTED_b ='S'  and empty(i_obj.w_TYPECLAS_A)) 
        i_obj&i_vname = 'XFRX'
     else
       Ah_ErrorMsg('Formato PDF (Tipo classe XFRX) non definito in <Configurazione stampa su file>',48)
       i_obj.w_PLTIPSTA='XXX'
       i_obj&i_vname=' '
     endif   
   endif 

  return



procedure selectdir

  para i_obj,i_vname

  local i_prg,i_cbase

  i_prg = ''

  i_prg=Cp_GETDIR()

  if not empty(i_prg)

    if AT(':',i_prg)>0

       i_prg=substr(i_prg,AT(':',i_prg)+1)

    endif

    i_obj&i_vname = i_prg

  endif

  return
* --- Fine Area Manuale
