* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kws                                                        *
*              Wizard gadget shortcut                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-12-16                                                      *
* Last revis.: 2015-11-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kws",oParentObject))

* --- Class definition
define class tgsut_kws as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 502
  Height = 413
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-27"
  HelpContextID=32064361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  _IDX = 0
  MOD_GADG_IDX = 0
  GRP_GADG_IDX = 0
  MYG_MAST_IDX = 0
  GADCOLOR_IDX = 0
  cPrg = "gsut_kws"
  cComment = "Wizard gadget shortcut"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_GGACTGRP = space(5)
  w_TEST_F10 = space(1)
  w_GAUTEESC = 0
  w_CONFUTE = 0
  w_DEFTHEME = space(5)
  w_GA__NOME = space(50)
  w_GADESCRI = space(0)
  w_GASRCPRG = space(50)
  w_GAGRPCOD = space(5)
  w_GCCODICE = space(5)
  w_DIMENSION = space(5)
  w_LABEL = space(252)
  w_TOOLTIP = space(252)
  w_FONTCLR = 0
  o_FONTCLR = 0
  w_IMAGE = space(252)
  o_IMAGE = space(252)
  w_APPLYTHEMEIMG = .F.
  o_APPLYTHEMEIMG = .F.
  w_IMAGETMP = space(252)
  w_PROGRAM = space(252)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kwsPag1","gsut_kws",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGA__NOME_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='MOD_GADG'
    this.cWorkTables[2]='GRP_GADG'
    this.cWorkTables[3]='MYG_MAST'
    this.cWorkTables[4]='GADCOLOR'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_GGACTGRP=space(5)
      .w_TEST_F10=space(1)
      .w_GAUTEESC=0
      .w_CONFUTE=0
      .w_DEFTHEME=space(5)
      .w_GA__NOME=space(50)
      .w_GADESCRI=space(0)
      .w_GASRCPRG=space(50)
      .w_GAGRPCOD=space(5)
      .w_GCCODICE=space(5)
      .w_DIMENSION=space(5)
      .w_LABEL=space(252)
      .w_TOOLTIP=space(252)
      .w_FONTCLR=0
      .w_IMAGE=space(252)
      .w_APPLYTHEMEIMG=.f.
      .w_IMAGETMP=space(252)
      .w_PROGRAM=space(252)
      .w_GGACTGRP=oParentObject.w_GGACTGRP
      .w_TEST_F10=oParentObject.w_TEST_F10
      .w_GAUTEESC=oParentObject.w_GAUTEESC
      .w_GA__NOME=oParentObject.w_GA__NOME
      .w_GADESCRI=oParentObject.w_GADESCRI
      .w_GASRCPRG=oParentObject.w_GASRCPRG
      .w_GAGRPCOD=oParentObject.w_GAGRPCOD
      .w_GCCODICE=oParentObject.w_GCCODICE
      .w_DIMENSION=oParentObject.w_DIMENSION
      .w_LABEL=oParentObject.w_LABEL
      .w_TOOLTIP=oParentObject.w_TOOLTIP
      .w_FONTCLR=oParentObject.w_FONTCLR
      .w_IMAGE=oParentObject.w_IMAGE
      .w_APPLYTHEMEIMG=oParentObject.w_APPLYTHEMEIMG
      .w_PROGRAM=oParentObject.w_PROGRAM
          .DoRTCalc(1,1,.f.)
        .w_TEST_F10 = 'S'
        .w_GAUTEESC = i_CODUTE
        .w_CONFUTE = .w_GAUTEESC
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CONFUTE))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,8,.f.)
        if not(empty(.w_GASRCPRG))
          .link_1_10('Full')
        endif
        .w_GAGRPCOD = EVL(.w_GGACTGRP,'')
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_GAGRPCOD))
          .link_1_12('Full')
        endif
        .w_GCCODICE = EVL(.w_DEFTHEME,'')
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_GCCODICE))
          .link_1_13('Full')
        endif
          .DoRTCalc(11,16,.f.)
        .w_IMAGETMP = Iif(.w_APPLYTHEMEIMG, cp_TmpColorizeImg(.w_IMAGE, .w_FONTCLR), .w_IMAGE)
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate(.w_IMAGETMP)
    endwith
    this.DoRTCalc(18,18,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_GGACTGRP=.w_GGACTGRP
      .oParentObject.w_TEST_F10=.w_TEST_F10
      .oParentObject.w_GAUTEESC=.w_GAUTEESC
      .oParentObject.w_GA__NOME=.w_GA__NOME
      .oParentObject.w_GADESCRI=.w_GADESCRI
      .oParentObject.w_GASRCPRG=.w_GASRCPRG
      .oParentObject.w_GAGRPCOD=.w_GAGRPCOD
      .oParentObject.w_GCCODICE=.w_GCCODICE
      .oParentObject.w_DIMENSION=.w_DIMENSION
      .oParentObject.w_LABEL=.w_LABEL
      .oParentObject.w_TOOLTIP=.w_TOOLTIP
      .oParentObject.w_FONTCLR=.w_FONTCLR
      .oParentObject.w_IMAGE=.w_IMAGE
      .oParentObject.w_APPLYTHEMEIMG=.w_APPLYTHEMEIMG
      .oParentObject.w_PROGRAM=.w_PROGRAM
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_GAUTEESC = i_CODUTE
          .link_1_4('Full')
        .DoRTCalc(5,7,.t.)
          .link_1_10('Full')
        .DoRTCalc(9,16,.t.)
        if .o_IMAGE<>.w_IMAGE.or. .o_APPLYTHEMEIMG<>.w_APPLYTHEMEIMG.or. .o_FONTCLR<>.w_FONTCLR
            .w_IMAGETMP = Iif(.w_APPLYTHEMEIMG, cp_TmpColorizeImg(.w_IMAGE, .w_FONTCLR), .w_IMAGE)
        endif
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(.w_IMAGETMP)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(18,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(.w_IMAGETMP)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CONFUTE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MYG_MAST_IDX,3]
    i_lTable = "MYG_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MYG_MAST_IDX,2], .t., this.MYG_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MYG_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONFUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONFUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODUTE,MGDTHEME";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODUTE="+cp_ToStrODBC(this.w_CONFUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODUTE',this.w_CONFUTE)
            select MGCODUTE,MGDTHEME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONFUTE = NVL(_Link_.MGCODUTE,0)
      this.w_DEFTHEME = NVL(_Link_.MGDTHEME,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CONFUTE = 0
      endif
      this.w_DEFTHEME = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MYG_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MGCODUTE,1)
      cp_ShowWarn(i_cKey,this.MYG_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONFUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GASRCPRG
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_GADG_IDX,3]
    i_lTable = "MOD_GADG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2], .t., this.MOD_GADG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GASRCPRG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GASRCPRG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGSRCPRG";
                   +" from "+i_cTable+" "+i_lTable+" where MGSRCPRG="+cp_ToStrODBC(this.w_GASRCPRG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGSRCPRG',this.w_GASRCPRG)
            select MGSRCPRG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GASRCPRG = NVL(_Link_.MGSRCPRG,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_GASRCPRG = space(50)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_GADG_IDX,2])+'\'+cp_ToStr(_Link_.MGSRCPRG,1)
      cp_ShowWarn(i_cKey,this.MOD_GADG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GASRCPRG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GAGRPCOD
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRP_GADG_IDX,3]
    i_lTable = "GRP_GADG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRP_GADG_IDX,2], .t., this.GRP_GADG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRP_GADG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GAGRPCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRP_GADG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GGCODICE like "+cp_ToStrODBC(trim(this.w_GAGRPCOD)+"%");

          i_ret=cp_SQL(i_nConn,"select GGCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GGCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GGCODICE',trim(this.w_GAGRPCOD))
          select GGCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GGCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GAGRPCOD)==trim(_Link_.GGCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GAGRPCOD) and !this.bDontReportError
            deferred_cp_zoom('GRP_GADG','*','GGCODICE',cp_AbsName(oSource.parent,'oGAGRPCOD_1_12'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GGCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where GGCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GGCODICE',oSource.xKey(1))
            select GGCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GAGRPCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GGCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where GGCODICE="+cp_ToStrODBC(this.w_GAGRPCOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GGCODICE',this.w_GAGRPCOD)
            select GGCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GAGRPCOD = NVL(_Link_.GGCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_GAGRPCOD = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRP_GADG_IDX,2])+'\'+cp_ToStr(_Link_.GGCODICE,1)
      cp_ShowWarn(i_cKey,this.GRP_GADG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GAGRPCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GCCODICE
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GADCOLOR_IDX,3]
    i_lTable = "GADCOLOR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GADCOLOR_IDX,2], .t., this.GADCOLOR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GADCOLOR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GCCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MGC',True,'GADCOLOR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GCCODICE like "+cp_ToStrODBC(trim(this.w_GCCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select GCCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GCCODICE',trim(this.w_GCCODICE))
          select GCCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GCCODICE)==trim(_Link_.GCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GCCODICE) and !this.bDontReportError
            deferred_cp_zoom('GADCOLOR','*','GCCODICE',cp_AbsName(oSource.parent,'oGCCODICE_1_13'),i_cWhere,'GSUT_MGC',"Elenco temi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GCCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where GCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GCCODICE',oSource.xKey(1))
            select GCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GCCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where GCCODICE="+cp_ToStrODBC(this.w_GCCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GCCODICE',this.w_GCCODICE)
            select GCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GCCODICE = NVL(_Link_.GCCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_GCCODICE = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GADCOLOR_IDX,2])+'\'+cp_ToStr(_Link_.GCCODICE,1)
      cp_ShowWarn(i_cKey,this.GADCOLOR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GCCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGA__NOME_1_6.value==this.w_GA__NOME)
      this.oPgFrm.Page1.oPag.oGA__NOME_1_6.value=this.w_GA__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oGADESCRI_1_8.value==this.w_GADESCRI)
      this.oPgFrm.Page1.oPag.oGADESCRI_1_8.value=this.w_GADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oGASRCPRG_1_10.RadioValue()==this.w_GASRCPRG)
      this.oPgFrm.Page1.oPag.oGASRCPRG_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGAGRPCOD_1_12.RadioValue()==this.w_GAGRPCOD)
      this.oPgFrm.Page1.oPag.oGAGRPCOD_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGCCODICE_1_13.RadioValue()==this.w_GCCODICE)
      this.oPgFrm.Page1.oPag.oGCCODICE_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIMENSION_1_14.RadioValue()==this.w_DIMENSION)
      this.oPgFrm.Page1.oPag.oDIMENSION_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLABEL_1_17.value==this.w_LABEL)
      this.oPgFrm.Page1.oPag.oLABEL_1_17.value=this.w_LABEL
    endif
    if not(this.oPgFrm.Page1.oPag.oTOOLTIP_1_18.value==this.w_TOOLTIP)
      this.oPgFrm.Page1.oPag.oTOOLTIP_1_18.value=this.w_TOOLTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oIMAGE_1_21.value==this.w_IMAGE)
      this.oPgFrm.Page1.oPag.oIMAGE_1_21.value=this.w_IMAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oAPPLYTHEMEIMG_1_22.RadioValue()==this.w_APPLYTHEMEIMG)
      this.oPgFrm.Page1.oPag.oAPPLYTHEMEIMG_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROGRAM_1_26.value==this.w_PROGRAM)
      this.oPgFrm.Page1.oPag.oPROGRAM_1_26.value=this.w_PROGRAM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FONTCLR = this.w_FONTCLR
    this.o_IMAGE = this.w_IMAGE
    this.o_APPLYTHEMEIMG = this.w_APPLYTHEMEIMG
    return

enddefine

* --- Define pages as container
define class tgsut_kwsPag1 as StdContainer
  Width  = 498
  height = 413
  stdWidth  = 498
  stdheight = 413
  resizeXpos=263
  resizeYpos=298
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGA__NOME_1_6 as StdField with uid="WNZSKKDGZP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_GA__NOME", cQueryName = "GA__NOME",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome del Gadget",;
    HelpContextID = 228855125,;
   bGlobalFont=.t.,;
    Height=21, Width=405, Left=83, Top=17, InputMask=replicate('X',50)

  add object oGADESCRI_1_8 as StdMemo with uid="MKKJPXKEYB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_GADESCRI", cQueryName = "GADESCRI",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della funzionalit� del gadget",;
    HelpContextID = 110117551,;
   bGlobalFont=.t.,;
    Height=84, Width=405, Left=83, Top=40


  add object oGASRCPRG_1_10 as StdTableCombo with uid="ZFPBHOVQPK",rtseq=8,rtrep=.f.,left=83,top=128,width=150,height=22, enabled=.f.;
    , ToolTipText = "Modello del gadget";
    , HelpContextID = 43922093;
    , cFormVar="w_GASRCPRG",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="MOD_GADG";
    , cTable='MOD_GADG',cKey='MGSRCPRG',cValue='MGDESCRI',cOrderBy='',xDefault=space(50);
  , bGlobalFont=.t.


  func oGASRCPRG_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oGAGRPCOD_1_12 as StdTableCombo with uid="EMYRNJKKIJ",rtseq=9,rtrep=.f.,left=338,top=128,width=150,height=22;
    , cDescEmptyElement = "Nessuna";
    , ToolTipText = "Area di appartenenza";
    , HelpContextID = 160599382;
    , cFormVar="w_GAGRPCOD",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="GRP_GADG";
    , cTable='QUERY\GSUT_KWS.VQR',cKey='GGCODICE',cValue='GGDESCRI',cOrderBy='GGCODICE',xDefault=space(5);
  , bGlobalFont=.t.


  func oGAGRPCOD_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oGAGRPCOD_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oGCCODICE_1_13 as StdTableCombo with uid="KXNHPAYQHF",rtseq=10,rtrep=.f.,left=83,top=153,width=150,height=22;
    , cDescEmptyElement = "Nessuno";
    , ToolTipText = "Tema del gadget";
    , HelpContextID = 195703979;
    , cFormVar="w_GCCODICE",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="GADCOLOR";
    , cTable='query/gsut_mgc.vqr',cKey='GCCODICE',cValue='GCDESCRI',cOrderBy='',xDefault=space(5);
  , bGlobalFont=.t.


  func oGCCODICE_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oGCCODICE_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oDIMENSION_1_14 as StdCombo with uid="OMGWQISROH",rtseq=11,rtrep=.f.,left=338,top=153,width=150,height=22;
    , cDescEmptyElement = "Nessuno";
    , ToolTipText = "Dimensioni del gadget";
    , HelpContextID = 163520667;
    , cFormVar="w_DIMENSION",RowSource=""+"Piccolo,"+"Medio,"+"Grande", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDIMENSION_1_14.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'M',;
    iif(this.value =3,'G',;
    space(5)))))
  endfunc
  func oDIMENSION_1_14.GetRadio()
    this.Parent.oContained.w_DIMENSION = this.RadioValue()
    return .t.
  endfunc

  func oDIMENSION_1_14.SetRadio()
    this.Parent.oContained.w_DIMENSION=trim(this.Parent.oContained.w_DIMENSION)
    this.value = ;
      iif(this.Parent.oContained.w_DIMENSION=='P',1,;
      iif(this.Parent.oContained.w_DIMENSION=='M',2,;
      iif(this.Parent.oContained.w_DIMENSION=='G',3,;
      0)))
  endfunc

  add object oLABEL_1_17 as StdField with uid="OBXQEZQUHD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_LABEL", cQueryName = "LABEL",;
    bObbl = .f. , nPag = 1, value=space(252), bMultilanguage =  .f.,;
    ToolTipText = "Etichetta del link",;
    HelpContextID = 215997770,;
   bGlobalFont=.t.,;
    Height=21, Width=405, Left=83, Top=196, InputMask=replicate('X',252)

  add object oTOOLTIP_1_18 as StdField with uid="EEWOPPUAGO",rtseq=13,rtrep=.f.,;
    cFormVar = "w_TOOLTIP", cQueryName = "TOOLTIP",;
    bObbl = .f. , nPag = 1, value=space(252), bMultilanguage =  .f.,;
    ToolTipText = "Etichetta del link",;
    HelpContextID = 56098506,;
   bGlobalFont=.t.,;
    Height=21, Width=405, Left=83, Top=219, InputMask=replicate('X',252)

  add object oIMAGE_1_21 as StdField with uid="ZQPDPPPTRS",rtseq=15,rtrep=.f.,;
    cFormVar = "w_IMAGE", cQueryName = "IMAGE",;
    bObbl = .f. , nPag = 1, value=space(252), bMultilanguage =  .f.,;
    ToolTipText = "Immagine associata al link",;
    HelpContextID = 223207802,;
   bGlobalFont=.t.,;
    Height=21, Width=309, Left=83, Top=242, InputMask=replicate('X',252), bHasZoom = .t. 

  proc oIMAGE_1_21.mZoom
    This.Parent.oContained.w_IMAGE = GetFile()
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAPPLYTHEMEIMG_1_22 as StdCheck with uid="GUKQVBMTSJ",rtseq=16,rtrep=.f.,left=400, top=245, caption="Applica tema",;
    ToolTipText = "Modifica i colori dell'immagine in base al tema",;
    HelpContextID = 54924517,;
    cFormVar="w_APPLYTHEMEIMG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAPPLYTHEMEIMG_1_22.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oAPPLYTHEMEIMG_1_22.GetRadio()
    this.Parent.oContained.w_APPLYTHEMEIMG = this.RadioValue()
    return .t.
  endfunc

  func oAPPLYTHEMEIMG_1_22.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_APPLYTHEMEIMG==.T.,1,;
      0)
  endfunc

  add object oPROGRAM_1_26 as StdField with uid="ZKDLNKBORJ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PROGRAM", cQueryName = "PROGRAM",;
    bObbl = .f. , nPag = 1, value=space(252), bMultilanguage =  .f.,;
    ToolTipText = "Programma da eseguire al click",;
    HelpContextID = 192740362,;
   bGlobalFont=.t.,;
    Height=90, Width=309, Left=83, Top=265, InputMask=replicate('X',252)


  add object oBtn_1_27 as StdButton with uid="MRBRWTXHBW",left=83, top=360, width=48,height=45,;
    CpPicture="BMP\RUN.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per testare l'espressione programma";
    , HelpContextID = 23963850;
    , Caption='\<Test';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        TestPrg(ThisForm)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_28 as StdButton with uid="ZMIZZNTUDJ",left=390, top=360, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 32035610;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_29 as StdButton with uid="JWIBHYOYEX",left=440, top=360, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 24746938;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_33 as cp_showimage with uid="DYWSEMRHRC",left=400, top=274, width=83,height=82,;
    caption='Object',;
   bGlobalFont=.t.,;
    default="default bitmap",stretch=2,;
    nPag=1;
    , HelpContextID = 145933286

  add object oStr_1_7 as StdString with uid="BHGUCQUBTC",Visible=.t., Left=42, Top=19,;
    Alignment=1, Width=40, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="VSPQKIQYQG",Visible=.t., Left=8, Top=40,;
    Alignment=1, Width=74, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="KMZGSQNRGB",Visible=.t., Left=32, Top=130,;
    Alignment=1, Width=50, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="GCBCPRPPKJ",Visible=.t., Left=286, Top=130,;
    Alignment=1, Width=50, Height=18,;
    Caption="Area:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="PUYPJVNLWT",Visible=.t., Left=28, Top=198,;
    Alignment=1, Width=55, Height=18,;
    Caption="Etichetta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="DPMDUQFFLW",Visible=.t., Left=19, Top=244,;
    Alignment=1, Width=64, Height=18,;
    Caption="Immagine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="XKEOSFCVXJ",Visible=.t., Left=8, Top=265,;
    Alignment=1, Width=75, Height=18,;
    Caption="Programma:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="QKOQHORACK",Visible=.t., Left=28, Top=221,;
    Alignment=1, Width=55, Height=18,;
    Caption="Tooltip:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="GEDJRQNZJI",Visible=.t., Left=263, Top=155,;
    Alignment=1, Width=73, Height=18,;
    Caption="Dimensioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="VBSITKZFGX",Visible=.t., Left=32, Top=155,;
    Alignment=1, Width=50, Height=18,;
    Caption="Tema:"  ;
  , bGlobalFont=.t.

  add object oBox_1_16 as StdBox with uid="NRCQZGZDRI",left=11, top=185, width=479,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kws','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_kws
Proc TestPrg(pParent)
  Local OldErr, bErr
  bErr = .F.
  OldErr = On("Error")
  On Error bErr=.T.
  Eval(m.pParent.w_PROGRAM)
  If m.bErr
    Ah_ErrorMsg([Si � verificato un errore durante il test dell'espressione "Programma"])
  EndIf
  On Error &OldErr
EndProc
* --- Fine Area Manuale
