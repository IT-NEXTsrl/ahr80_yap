* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_ban                                                        *
*              Assegna nazione e provincia                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-06                                                      *
* Last revis.: 2016-01-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_ban",oParentObject)
return(i_retval)

define class tgsve_ban as StdBatch
  * --- Local variables
  w_VARIABILE = space(10)
  w_VALORE = space(10)
  w_DOCUMENTI = .NULL.
  w_NAZIONE = space(3)
  w_PROVINCIA = space(2)
  w_AGGDATPRO = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ASSEGNA IL CODICE NAZIONE E LA PROVINCIA NEI DATI INTRA
    * --- w_NAZCLF
    *     Nazione letta da intestatario documento
    * --- w_CODNAZ1
    *     Nazione letta dalla sede dell'intestatario
    * --- w_PROSED � la provincia della sede azienda nel documento
    this.w_DOCUMENTI = THIS.OPARENTOBJECT
    do case
      case this.oParentObject.currentEvent = "w_MVCODCON Changed"
        * --- E' CAMBIATO L'INTESTATARIO
        this.w_NAZIONE = NVL( this.oParentObject.w_NAZCLF , SPACE( 3 ) )
        * --- ASSEGNA IL VALORE PER INIZIALIZZARE LE NUOVE RIGHE
        this.oParentObject.w_ONAZPRO = this.w_NAZIONE
        * --- Variabile e valore sono i parametri della SET a pag 2
        this.w_VARIABILE = "w_MVNAZPRO"
        this.w_VALORE = this.oParentObject.w_ONAZPRO
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.oParentObject.w_MVTIPCON = "F"
          * --- ASSEGNA IL VALORE PER INIZIALIZZARE LE NUOVE RIGHE
          this.oParentObject.w_OPAEFOR = this.w_NAZIONE
          * --- Variabile e valore sono i parametri della SET a pag 2
          this.w_VARIABILE = "w_MVPAEFOR"
          this.w_VALORE = this.oParentObject.w_OPAEFOR
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.currentEvent = "w_MVCODDES Changed"
        * --- E' CAMBIATA LA SEDE
        if EMPTY( NVL( this.oParentObject.w_CODNAZ1 , "" ) )
          this.w_NAZIONE = NVL( this.oParentObject.w_NAZCLF , SPACE( 3 ) )
        else
          this.w_NAZIONE = NVL( this.oParentObject.w_CODNAZ1 , SPACE( 3 ) )
        endif
        * --- ASSEGNA IL VALORE PER INIZIALIZZARE LE NUOVE RIGHE
        this.oParentObject.w_ONAZPRO = this.w_NAZIONE
        * --- Variabile e valore sono i parametri della SET a pag 2
        this.w_VARIABILE = "w_MVNAZPRO"
        this.w_VALORE = this.oParentObject.w_ONAZPRO
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.currentEvent = "w_DDCODNAZ Changed"
        * --- NAZIONE MESSA A MANO IN CARICAMENTO AUTOMATICO
        if !EMPTY( this.oParentObject.w_DDCODNAZ )
          this.w_NAZIONE = NVL( this.oParentObject.w_DDCODNAZ , SPACE( 3 ) )
        else
          this.w_NAZIONE = NVL( this.oParentObject.w_NAZCLF , SPACE( 3 ) )
        endif
        * --- ASSEGNA IL VALORE PER INIZIALIZZARE LE NUOVE RIGHE
        this.oParentObject.w_ONAZPRO = this.w_NAZIONE
        * --- Variabile e valore sono i parametri della SET a pag 2
        this.w_VARIABILE = "w_MVNAZPRO"
        this.w_VALORE = this.oParentObject.w_ONAZPRO
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.currentEvent = "w_CARICAU Changed"
        * --- FLAG CARICAMENTO AUTOMATICO
        if !EMPTY( this.oParentObject.w_CARICAU ) AND !EMPTY( this.oParentObject.w_DDCODNAZ )
          this.w_NAZIONE = NVL( this.oParentObject.w_DDCODNAZ , SPACE( 3 ) )
        else
          this.w_NAZIONE = NVL( this.oParentObject.w_NAZCLF , SPACE( 3 ) )
        endif
        * --- ASSEGNA IL VALORE PER INIZIALIZZARE LE NUOVE RIGHE
        this.oParentObject.w_ONAZPRO = this.w_NAZIONE
        * --- Variabile e valore sono i parametri della SET a pag 2
        this.w_VARIABILE = "w_MVNAZPRO"
        this.w_VALORE = this.oParentObject.w_ONAZPRO
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.currentEvent = "w_MVCODSED Changed"
        * --- SEDE AZIENDA
        if !EMPTY( this.oParentObject.w_PROSED )
          this.oParentObject.w_OPROORD = NVL( this.oParentObject.w_PROSED , SPACE( 2 ) )
        else
          this.oParentObject.w_OPROORD = g_PROAZI
        endif
        * --- Variabile e valore sono i parametri della SET a pag 2
        this.w_VARIABILE = "w_MVPROORD"
        this.w_VALORE = this.oParentObject.w_OPROORD
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.currentEvent = "w_MVCODORN Changed"
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.currentEvent = "Edit Started"
        * --- Tenuto volontariamente separato dall'evento:  "w_MVCODCON Changed"
        this.w_NAZIONE = NVL( this.oParentObject.w_NAZCLF , SPACE( 3 ) )
        * --- ASSEGNA IL VALORE PER INIZIALIZZARE LE NUOVE RIGHE
        this.oParentObject.w_ONAZPRO = this.w_NAZIONE
        * --- Variabile e valore sono i parametri della SET a pag 2
        this.w_VARIABILE = "w_MVNAZPRO"
        this.w_VALORE = this.oParentObject.w_ONAZPRO
        if NOT EMPTY(this.w_NAZIONE) AND EMPTY(this.w_VALORE)
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.oParentObject.w_MVTIPCON = "F"
          * --- ASSEGNA IL VALORE PER INIZIALIZZARE LE NUOVE RIGHE
          this.oParentObject.w_OPAEFOR = this.w_NAZIONE
          * --- Variabile e valore sono i parametri della SET a pag 2
          this.w_VARIABILE = "w_MVPAEFOR"
          this.w_VALORE = this.oParentObject.w_OPAEFOR
          if NOT EMPTY(this.w_NAZIONE) AND EMPTY(this.w_VALORE)
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
    endcase
    this.bUpdateParentObject = .F.
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ASSEGNA LA NAZIONE SU TUTTE LE RIGHE
    this.w_DOCUMENTI.MARKPOS()     
    this.w_DOCUMENTI.FIRSTROW()     
    do while NOT this.w_DOCUMENTI.EOF_TRS()
      this.w_DOCUMENTI.SET(this.w_VARIABILE , this.w_VALORE)     
      this.w_DOCUMENTI.NEXTROW()     
    enddo
    this.w_DOCUMENTI.REPOS(.T.)     
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ordini aperti, scorro il transitorio per aggiornare le date di fatturazione
    if this.oParentObject.w_MVCLADOC="OR" AND this.oParentObject.w_FLORDAPE="S"
      this.w_DOCUMENTI = this.oParentObject
      this.w_AGGDATPRO = .F.
      if this.w_DOCUMENTI.NUMROW() > 0
        this.w_AGGDATPRO = ah_YesNo("Aggiorno le date di prossima fatturazione?")
      endif
      this.w_DOCUMENTI.MarkPos()     
      this.w_DOCUMENTI.FirstRow()     
      do while Not this.w_DOCUMENTI.Eof_Trs()
        this.w_DOCUMENTI.SetRow()     
        * --- Se Ordine Aperto ricalcolo la data prossima fatturazione
        if NOT this.w_DOCUMENTI.FullRow()
          * --- Nel caso non sia soddisfatta la condizione di riga piena, il metodo di calcolo viene posto secondo il valore dell'init
          this.oParentObject.w_MVMC_PER = IIF( NOT EMPTY( this.oParentObject.w_ANMTDCL2 ), this.oParentObject.w_ANMTDCL2, IIF( NOT EMPTY( this.oParentObject.w_ANMTDCLC ), this.oParentObject.w_ANMTDCLC, this.oParentObject.w_TDMTDCLC ) )
        endif
        if this.w_DOCUMENTI.FullRow() AND this.w_AGGDATPRO
          this.oParentObject.w_MVMC_PER = IIF( NOT EMPTY( this.oParentObject.w_ANMTDCL2 ), this.oParentObject.w_ANMTDCL2, IIF( NOT EMPTY( this.oParentObject.w_ANMTDCLC ), this.oParentObject.w_ANMTDCLC, this.oParentObject.w_TDMTDCLC ) )
          this.oParentObject.w_MVDATEVA = NextTime(this.oParentObject.w_MVMC_PER, MAX(this.w_DOCUMENTI.w_MVDATOAI, i_DATSYS))
        endif
        * --- Carica il Temporaneo dei Dati
        this.w_DOCUMENTI.SaveRow()     
        this.w_DOCUMENTI.NextRow()     
      enddo
      * --- Riposizionamento sul Transitorio senza effettuare la SaveDependsOn()
      this.w_DOCUMENTI.RePos(.T.)     
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
