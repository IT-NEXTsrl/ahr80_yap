* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_abk                                                        *
*              Dati backup                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-01-19                                                      *
* Last revis.: 2012-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_abk")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_abk")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_abk")
  return

* --- Class definition
define class tgsar_abk as StdPCForm
  Width  = 654
  Height = 292
  Top    = 10
  Left   = 10
  cComment = "Dati backup"
  cPrg = "gsar_abk"
  HelpContextID=141985943
  add object cnt as tcgsar_abk
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_abk as PCContext
  w_AZLUNBCK = space(1)
  w_AZMARBCK = space(1)
  w_AZMERBCK = space(1)
  w_AZGIOBCK = space(1)
  w_AZVENBCK = space(1)
  w_AZSABBCK = space(1)
  w_AZDOMBCK = space(1)
  w_AZORABCK = 0
  w_ORABCK = space(2)
  w_AZMINBCK = 0
  w_MINBCK = space(2)
  w_NOTE_BCK = space(10)
  w_AZBCKLOG = space(200)
  w_AZCODAZI = space(5)
  w_AZBCKDAY = 0
  w_AZBCKNUM = 0
  proc Save(oFrom)
    this.w_AZLUNBCK = oFrom.w_AZLUNBCK
    this.w_AZMARBCK = oFrom.w_AZMARBCK
    this.w_AZMERBCK = oFrom.w_AZMERBCK
    this.w_AZGIOBCK = oFrom.w_AZGIOBCK
    this.w_AZVENBCK = oFrom.w_AZVENBCK
    this.w_AZSABBCK = oFrom.w_AZSABBCK
    this.w_AZDOMBCK = oFrom.w_AZDOMBCK
    this.w_AZORABCK = oFrom.w_AZORABCK
    this.w_ORABCK = oFrom.w_ORABCK
    this.w_AZMINBCK = oFrom.w_AZMINBCK
    this.w_MINBCK = oFrom.w_MINBCK
    this.w_NOTE_BCK = oFrom.w_NOTE_BCK
    this.w_AZBCKLOG = oFrom.w_AZBCKLOG
    this.w_AZCODAZI = oFrom.w_AZCODAZI
    this.w_AZBCKDAY = oFrom.w_AZBCKDAY
    this.w_AZBCKNUM = oFrom.w_AZBCKNUM
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_AZLUNBCK = this.w_AZLUNBCK
    oTo.w_AZMARBCK = this.w_AZMARBCK
    oTo.w_AZMERBCK = this.w_AZMERBCK
    oTo.w_AZGIOBCK = this.w_AZGIOBCK
    oTo.w_AZVENBCK = this.w_AZVENBCK
    oTo.w_AZSABBCK = this.w_AZSABBCK
    oTo.w_AZDOMBCK = this.w_AZDOMBCK
    oTo.w_AZORABCK = this.w_AZORABCK
    oTo.w_ORABCK = this.w_ORABCK
    oTo.w_AZMINBCK = this.w_AZMINBCK
    oTo.w_MINBCK = this.w_MINBCK
    oTo.w_NOTE_BCK = this.w_NOTE_BCK
    oTo.w_AZBCKLOG = this.w_AZBCKLOG
    oTo.w_AZCODAZI = this.w_AZCODAZI
    oTo.w_AZBCKDAY = this.w_AZBCKDAY
    oTo.w_AZBCKNUM = this.w_AZBCKNUM
    PCContext::Load(oTo)
enddefine

define class tcgsar_abk as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 654
  Height = 292
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-05"
  HelpContextID=141985943
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  AZBACKUP_IDX = 0
  cFile = "AZBACKUP"
  cKeySelect = "AZCODAZI"
  cKeyWhere  = "AZCODAZI=this.w_AZCODAZI"
  cKeyWhereODBC = '"AZCODAZI="+cp_ToStrODBC(this.w_AZCODAZI)';

  cKeyWhereODBCqualified = '"AZBACKUP.AZCODAZI="+cp_ToStrODBC(this.w_AZCODAZI)';

  cPrg = "gsar_abk"
  cComment = "Dati backup"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AZLUNBCK = space(1)
  w_AZMARBCK = space(1)
  w_AZMERBCK = space(1)
  w_AZGIOBCK = space(1)
  w_AZVENBCK = space(1)
  w_AZSABBCK = space(1)
  w_AZDOMBCK = space(1)
  w_AZORABCK = 0
  w_ORABCK = space(2)
  o_ORABCK = space(2)
  w_AZMINBCK = 0
  w_MINBCK = space(2)
  o_MINBCK = space(2)
  w_NOTE_BCK = space(0)
  w_AZBCKLOG = space(200)
  w_AZCODAZI = space(5)
  w_AZBCKDAY = 0
  w_AZBCKNUM = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_abkPag1","gsar_abk",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 200433398
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAZLUNBCK_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='AZBACKUP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.AZBACKUP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.AZBACKUP_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_abk'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from AZBACKUP where AZCODAZI=KeySet.AZCODAZI
    *
    i_nConn = i_TableProp[this.AZBACKUP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AZBACKUP_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('AZBACKUP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "AZBACKUP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' AZBACKUP '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AZCODAZI',this.w_AZCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ORABCK = space(2)
        .w_MINBCK = space(2)
        .w_NOTE_BCK = "E' possibile attivare in automatico il Back Up del database all'uscita di ad hoc. Per farlo occorre impostare un'ora a partire dalla quale, nei giorni marcati, la procedura all'uscita dell'ultimo utente proporr� la creazione del Back Up."
        .w_AZLUNBCK = NVL(AZLUNBCK,space(1))
        .w_AZMARBCK = NVL(AZMARBCK,space(1))
        .w_AZMERBCK = NVL(AZMERBCK,space(1))
        .w_AZGIOBCK = NVL(AZGIOBCK,space(1))
        .w_AZVENBCK = NVL(AZVENBCK,space(1))
        .w_AZSABBCK = NVL(AZSABBCK,space(1))
        .w_AZDOMBCK = NVL(AZDOMBCK,space(1))
        .w_AZORABCK = NVL(AZORABCK,0)
        .w_AZMINBCK = NVL(AZMINBCK,0)
        .w_AZBCKLOG = NVL(AZBCKLOG,space(200))
        .w_AZCODAZI = NVL(AZCODAZI,space(5))
        .w_AZBCKDAY = NVL(AZBCKDAY,0)
        .w_AZBCKNUM = NVL(AZBCKNUM,0)
        cp_LoadRecExtFlds(this,'AZBACKUP')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_abk
    this.w_ORABCK=RIGHT('00'+ALLTRIM(STR(this.w_AZORABCK)),2)
    this.w_MINBCK=RIGHT('00'+ALLTRIM(STR(this.w_AZMINBCK)),2)
    
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_AZLUNBCK = space(1)
      .w_AZMARBCK = space(1)
      .w_AZMERBCK = space(1)
      .w_AZGIOBCK = space(1)
      .w_AZVENBCK = space(1)
      .w_AZSABBCK = space(1)
      .w_AZDOMBCK = space(1)
      .w_AZORABCK = 0
      .w_ORABCK = space(2)
      .w_AZMINBCK = 0
      .w_MINBCK = space(2)
      .w_NOTE_BCK = space(0)
      .w_AZBCKLOG = space(200)
      .w_AZCODAZI = space(5)
      .w_AZBCKDAY = 0
      .w_AZBCKNUM = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,7,.f.)
        .w_AZORABCK = VAL(.w_ORABCK)
          .DoRTCalc(9,9,.f.)
        .w_AZMINBCK = VAL(.w_MINBCK)
          .DoRTCalc(11,11,.f.)
        .w_NOTE_BCK = "E' possibile attivare in automatico il Back Up del database all'uscita di ad hoc. Per farlo occorre impostare un'ora a partire dalla quale, nei giorni marcati, la procedura all'uscita dell'ultimo utente proporr� la creazione del Back Up."
      endif
    endwith
    cp_BlankRecExtFlds(this,'AZBACKUP')
    this.DoRTCalc(13,16,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oAZLUNBCK_1_1.enabled = i_bVal
      .Page1.oPag.oAZMARBCK_1_2.enabled = i_bVal
      .Page1.oPag.oAZMERBCK_1_3.enabled = i_bVal
      .Page1.oPag.oAZGIOBCK_1_4.enabled = i_bVal
      .Page1.oPag.oAZVENBCK_1_5.enabled = i_bVal
      .Page1.oPag.oAZSABBCK_1_6.enabled = i_bVal
      .Page1.oPag.oAZDOMBCK_1_7.enabled = i_bVal
      .Page1.oPag.oORABCK_1_11.enabled = i_bVal
      .Page1.oPag.oMINBCK_1_15.enabled = i_bVal
      .Page1.oPag.oAZBCKLOG_1_17.enabled = i_bVal
      .Page1.oPag.oAZBCKDAY_1_20.enabled = i_bVal
      .Page1.oPag.oAZBCKNUM_1_21.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'AZBACKUP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.AZBACKUP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZLUNBCK,"AZLUNBCK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMARBCK,"AZMARBCK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMERBCK,"AZMERBCK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZGIOBCK,"AZGIOBCK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZVENBCK,"AZVENBCK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZSABBCK,"AZSABBCK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDOMBCK,"AZDOMBCK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZORABCK,"AZORABCK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZMINBCK,"AZMINBCK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZBCKLOG,"AZBCKLOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCODAZI,"AZCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZBCKDAY,"AZBCKDAY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZBCKNUM,"AZBCKNUM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.AZBACKUP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AZBACKUP_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.AZBACKUP_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into AZBACKUP
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'AZBACKUP')
        i_extval=cp_InsertValODBCExtFlds(this,'AZBACKUP')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(AZLUNBCK,AZMARBCK,AZMERBCK,AZGIOBCK,AZVENBCK"+;
                  ",AZSABBCK,AZDOMBCK,AZORABCK,AZMINBCK,AZBCKLOG"+;
                  ",AZCODAZI,AZBCKDAY,AZBCKNUM "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_AZLUNBCK)+;
                  ","+cp_ToStrODBC(this.w_AZMARBCK)+;
                  ","+cp_ToStrODBC(this.w_AZMERBCK)+;
                  ","+cp_ToStrODBC(this.w_AZGIOBCK)+;
                  ","+cp_ToStrODBC(this.w_AZVENBCK)+;
                  ","+cp_ToStrODBC(this.w_AZSABBCK)+;
                  ","+cp_ToStrODBC(this.w_AZDOMBCK)+;
                  ","+cp_ToStrODBC(this.w_AZORABCK)+;
                  ","+cp_ToStrODBC(this.w_AZMINBCK)+;
                  ","+cp_ToStrODBC(this.w_AZBCKLOG)+;
                  ","+cp_ToStrODBC(this.w_AZCODAZI)+;
                  ","+cp_ToStrODBC(this.w_AZBCKDAY)+;
                  ","+cp_ToStrODBC(this.w_AZBCKNUM)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'AZBACKUP')
        i_extval=cp_InsertValVFPExtFlds(this,'AZBACKUP')
        cp_CheckDeletedKey(i_cTable,0,'AZCODAZI',this.w_AZCODAZI)
        INSERT INTO (i_cTable);
              (AZLUNBCK,AZMARBCK,AZMERBCK,AZGIOBCK,AZVENBCK,AZSABBCK,AZDOMBCK,AZORABCK,AZMINBCK,AZBCKLOG,AZCODAZI,AZBCKDAY,AZBCKNUM  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_AZLUNBCK;
                  ,this.w_AZMARBCK;
                  ,this.w_AZMERBCK;
                  ,this.w_AZGIOBCK;
                  ,this.w_AZVENBCK;
                  ,this.w_AZSABBCK;
                  ,this.w_AZDOMBCK;
                  ,this.w_AZORABCK;
                  ,this.w_AZMINBCK;
                  ,this.w_AZBCKLOG;
                  ,this.w_AZCODAZI;
                  ,this.w_AZBCKDAY;
                  ,this.w_AZBCKNUM;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.AZBACKUP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AZBACKUP_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.AZBACKUP_IDX,i_nConn)
      *
      * update AZBACKUP
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'AZBACKUP')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " AZLUNBCK="+cp_ToStrODBC(this.w_AZLUNBCK)+;
             ",AZMARBCK="+cp_ToStrODBC(this.w_AZMARBCK)+;
             ",AZMERBCK="+cp_ToStrODBC(this.w_AZMERBCK)+;
             ",AZGIOBCK="+cp_ToStrODBC(this.w_AZGIOBCK)+;
             ",AZVENBCK="+cp_ToStrODBC(this.w_AZVENBCK)+;
             ",AZSABBCK="+cp_ToStrODBC(this.w_AZSABBCK)+;
             ",AZDOMBCK="+cp_ToStrODBC(this.w_AZDOMBCK)+;
             ",AZORABCK="+cp_ToStrODBC(this.w_AZORABCK)+;
             ",AZMINBCK="+cp_ToStrODBC(this.w_AZMINBCK)+;
             ",AZBCKLOG="+cp_ToStrODBC(this.w_AZBCKLOG)+;
             ",AZBCKDAY="+cp_ToStrODBC(this.w_AZBCKDAY)+;
             ",AZBCKNUM="+cp_ToStrODBC(this.w_AZBCKNUM)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'AZBACKUP')
        i_cWhere = cp_PKFox(i_cTable  ,'AZCODAZI',this.w_AZCODAZI  )
        UPDATE (i_cTable) SET;
              AZLUNBCK=this.w_AZLUNBCK;
             ,AZMARBCK=this.w_AZMARBCK;
             ,AZMERBCK=this.w_AZMERBCK;
             ,AZGIOBCK=this.w_AZGIOBCK;
             ,AZVENBCK=this.w_AZVENBCK;
             ,AZSABBCK=this.w_AZSABBCK;
             ,AZDOMBCK=this.w_AZDOMBCK;
             ,AZORABCK=this.w_AZORABCK;
             ,AZMINBCK=this.w_AZMINBCK;
             ,AZBCKLOG=this.w_AZBCKLOG;
             ,AZBCKDAY=this.w_AZBCKDAY;
             ,AZBCKNUM=this.w_AZBCKNUM;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.AZBACKUP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AZBACKUP_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.AZBACKUP_IDX,i_nConn)
      *
      * delete AZBACKUP
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'AZCODAZI',this.w_AZCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.AZBACKUP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AZBACKUP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
        if .o_ORABCK<>.w_ORABCK
            .w_AZORABCK = VAL(.w_ORABCK)
        endif
        .DoRTCalc(9,9,.t.)
        if .o_MINBCK<>.w_MINBCK
            .w_AZMINBCK = VAL(.w_MINBCK)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,16,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oAZBCKDAY_1_20.visible=!this.oPgFrm.Page1.oPag.oAZBCKDAY_1_20.mHide()
    this.oPgFrm.Page1.oPag.oAZBCKNUM_1_21.visible=!this.oPgFrm.Page1.oPag.oAZBCKNUM_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAZLUNBCK_1_1.RadioValue()==this.w_AZLUNBCK)
      this.oPgFrm.Page1.oPag.oAZLUNBCK_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAZMARBCK_1_2.RadioValue()==this.w_AZMARBCK)
      this.oPgFrm.Page1.oPag.oAZMARBCK_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAZMERBCK_1_3.RadioValue()==this.w_AZMERBCK)
      this.oPgFrm.Page1.oPag.oAZMERBCK_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAZGIOBCK_1_4.RadioValue()==this.w_AZGIOBCK)
      this.oPgFrm.Page1.oPag.oAZGIOBCK_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAZVENBCK_1_5.RadioValue()==this.w_AZVENBCK)
      this.oPgFrm.Page1.oPag.oAZVENBCK_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAZSABBCK_1_6.RadioValue()==this.w_AZSABBCK)
      this.oPgFrm.Page1.oPag.oAZSABBCK_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAZDOMBCK_1_7.RadioValue()==this.w_AZDOMBCK)
      this.oPgFrm.Page1.oPag.oAZDOMBCK_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oORABCK_1_11.value==this.w_ORABCK)
      this.oPgFrm.Page1.oPag.oORABCK_1_11.value=this.w_ORABCK
    endif
    if not(this.oPgFrm.Page1.oPag.oMINBCK_1_15.value==this.w_MINBCK)
      this.oPgFrm.Page1.oPag.oMINBCK_1_15.value=this.w_MINBCK
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE_BCK_1_16.value==this.w_NOTE_BCK)
      this.oPgFrm.Page1.oPag.oNOTE_BCK_1_16.value=this.w_NOTE_BCK
    endif
    if not(this.oPgFrm.Page1.oPag.oAZBCKLOG_1_17.value==this.w_AZBCKLOG)
      this.oPgFrm.Page1.oPag.oAZBCKLOG_1_17.value=this.w_AZBCKLOG
    endif
    if not(this.oPgFrm.Page1.oPag.oAZBCKDAY_1_20.value==this.w_AZBCKDAY)
      this.oPgFrm.Page1.oPag.oAZBCKDAY_1_20.value=this.w_AZBCKDAY
    endif
    if not(this.oPgFrm.Page1.oPag.oAZBCKNUM_1_21.value==this.w_AZBCKNUM)
      this.oPgFrm.Page1.oPag.oAZBCKNUM_1_21.value=this.w_AZBCKNUM
    endif
    cp_SetControlsValueExtFlds(this,'AZBACKUP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(VAL(.w_ORABCK)>=0 And VAL(.w_ORABCK)<=23)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oORABCK_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un numero compreso tra 0 e 23")
          case   not(VAL(.w_MINBCK)>=0 And VAL(.w_MINBCK)<=59)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMINBCK_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un numero compreso tra 0 e 59")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ORABCK = this.w_ORABCK
    this.o_MINBCK = this.w_MINBCK
    return

enddefine

* --- Define pages as container
define class tgsar_abkPag1 as StdContainer
  Width  = 650
  height = 292
  stdWidth  = 650
  stdheight = 292
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAZLUNBCK_1_1 as StdCheck with uid="BMLYQJUZBH",rtseq=1,rtrep=.f.,left=22, top=37, caption="Luned�",;
    HelpContextID = 5200047,;
    cFormVar="w_AZLUNBCK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAZLUNBCK_1_1.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAZLUNBCK_1_1.GetRadio()
    this.Parent.oContained.w_AZLUNBCK = this.RadioValue()
    return .t.
  endfunc

  func oAZLUNBCK_1_1.SetRadio()
    this.Parent.oContained.w_AZLUNBCK=trim(this.Parent.oContained.w_AZLUNBCK)
    this.value = ;
      iif(this.Parent.oContained.w_AZLUNBCK=='S',1,;
      0)
  endfunc

  add object oAZMARBCK_1_2 as StdCheck with uid="IXOYNMVXXC",rtseq=2,rtrep=.f.,left=96, top=37, caption="Marted�",;
    HelpContextID = 2312367,;
    cFormVar="w_AZMARBCK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAZMARBCK_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAZMARBCK_1_2.GetRadio()
    this.Parent.oContained.w_AZMARBCK = this.RadioValue()
    return .t.
  endfunc

  func oAZMARBCK_1_2.SetRadio()
    this.Parent.oContained.w_AZMARBCK=trim(this.Parent.oContained.w_AZMARBCK)
    this.value = ;
      iif(this.Parent.oContained.w_AZMARBCK=='S',1,;
      0)
  endfunc

  add object oAZMERBCK_1_3 as StdCheck with uid="JIOJQGTUHV",rtseq=3,rtrep=.f.,left=170, top=37, caption="Mercoled�",;
    HelpContextID = 2050223,;
    cFormVar="w_AZMERBCK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAZMERBCK_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAZMERBCK_1_3.GetRadio()
    this.Parent.oContained.w_AZMERBCK = this.RadioValue()
    return .t.
  endfunc

  func oAZMERBCK_1_3.SetRadio()
    this.Parent.oContained.w_AZMERBCK=trim(this.Parent.oContained.w_AZMERBCK)
    this.value = ;
      iif(this.Parent.oContained.w_AZMERBCK=='S',1,;
      0)
  endfunc

  add object oAZGIOBCK_1_4 as StdCheck with uid="AJNDHBNILO",rtseq=4,rtrep=.f.,left=262, top=37, caption="Gioved�",;
    HelpContextID = 4958383,;
    cFormVar="w_AZGIOBCK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAZGIOBCK_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAZGIOBCK_1_4.GetRadio()
    this.Parent.oContained.w_AZGIOBCK = this.RadioValue()
    return .t.
  endfunc

  func oAZGIOBCK_1_4.SetRadio()
    this.Parent.oContained.w_AZGIOBCK=trim(this.Parent.oContained.w_AZGIOBCK)
    this.value = ;
      iif(this.Parent.oContained.w_AZGIOBCK=='S',1,;
      0)
  endfunc

  add object oAZVENBCK_1_5 as StdCheck with uid="OHCKMZFNBV",rtseq=5,rtrep=.f.,left=351, top=37, caption="Venerd�",;
    HelpContextID = 6207663,;
    cFormVar="w_AZVENBCK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAZVENBCK_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAZVENBCK_1_5.GetRadio()
    this.Parent.oContained.w_AZVENBCK = this.RadioValue()
    return .t.
  endfunc

  func oAZVENBCK_1_5.SetRadio()
    this.Parent.oContained.w_AZVENBCK=trim(this.Parent.oContained.w_AZVENBCK)
    this.value = ;
      iif(this.Parent.oContained.w_AZVENBCK=='S',1,;
      0)
  endfunc

  add object oAZSABBCK_1_6 as StdCheck with uid="BKZWCXZAPF",rtseq=6,rtrep=.f.,left=428, top=37, caption="Sabato",;
    HelpContextID = 19065007,;
    cFormVar="w_AZSABBCK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAZSABBCK_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAZSABBCK_1_6.GetRadio()
    this.Parent.oContained.w_AZSABBCK = this.RadioValue()
    return .t.
  endfunc

  func oAZSABBCK_1_6.SetRadio()
    this.Parent.oContained.w_AZSABBCK=trim(this.Parent.oContained.w_AZSABBCK)
    this.value = ;
      iif(this.Parent.oContained.w_AZSABBCK=='S',1,;
      0)
  endfunc

  add object oAZDOMBCK_1_7 as StdCheck with uid="XWWQGAPTII",rtseq=7,rtrep=.f.,left=502, top=37, caption="Domenica",;
    HelpContextID = 6674607,;
    cFormVar="w_AZDOMBCK", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAZDOMBCK_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAZDOMBCK_1_7.GetRadio()
    this.Parent.oContained.w_AZDOMBCK = this.RadioValue()
    return .t.
  endfunc

  func oAZDOMBCK_1_7.SetRadio()
    this.Parent.oContained.w_AZDOMBCK=trim(this.Parent.oContained.w_AZDOMBCK)
    this.value = ;
      iif(this.Parent.oContained.w_AZDOMBCK=='S',1,;
      0)
  endfunc

  add object oORABCK_1_11 as StdField with uid="WLRLBHYLAB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ORABCK", cQueryName = "ORABCK",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un numero compreso tra 0 e 23",;
    ToolTipText = "Ora inizio back up",;
    HelpContextID = 132968422,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=49, Top=88, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oORABCK_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ORABCK)>=0 And VAL(.w_ORABCK)<=23)
    endwith
    return bRes
  endfunc

  add object oMINBCK_1_15 as StdField with uid="DQMIGUJGVU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MINBCK", cQueryName = "MINBCK",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un numero compreso tra 0 e 59",;
    ToolTipText = "Minuto inizio back up",;
    HelpContextID = 133019334,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=99, Top=88, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMINBCK_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MINBCK)>=0 And VAL(.w_MINBCK)<=59)
    endwith
    return bRes
  endfunc

  add object oNOTE_BCK_1_16 as StdMemo with uid="FMIKGOFRHI",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NOTE_BCK", cQueryName = "NOTE_BCK",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 256828127,;
   bGlobalFont=.t.,;
    Height=95, Width=473, Left=137, Top=94

  add object oAZBCKLOG_1_17 as StdField with uid="BWEKNGLTHL",rtseq=13,rtrep=.f.,;
    cFormVar = "w_AZBCKLOG", cQueryName = "AZBCKLOG",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso / nome ultimo back up eseguito",;
    HelpContextID = 158205773,;
   bGlobalFont=.t.,;
    Height=21, Width=473, Left=134, Top=258, InputMask=replicate('X',200)

  add object oAZBCKDAY_1_20 as StdField with uid="HZDCSGJLRZ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_AZBCKDAY", cQueryName = "AZBCKDAY",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di giorni di back up da mantenere attivi",;
    HelpContextID = 23988063,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=323, Top=197

  func oAZBCKDAY_1_20.mHide()
    with this.Parent.oContained
      return (cp_dbtype='DB2')
    endwith
  endfunc

  add object oAZBCKNUM_1_21 as StdField with uid="UXYUZWESSN",rtseq=16,rtrep=.f.,;
    cFormVar = "w_AZBCKNUM", cQueryName = "AZBCKNUM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di back up da mantenere attivi",;
    HelpContextID = 76675245,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=323, Top=226

  func oAZBCKNUM_1_21.mHide()
    with this.Parent.oContained
      return (cp_dbtype='DB2')
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="RXQHNFLBHG",Visible=.t., Left=14, Top=13,;
    Alignment=0, Width=82, Height=18,;
    Caption="Giorni Backup"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="YLZEUUZJSO",Visible=.t., Left=1, Top=89,;
    Alignment=1, Width=43, Height=18,;
    Caption="Ora:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="UKDSBUXMKV",Visible=.t., Left=80, Top=90,;
    Alignment=1, Width=10, Height=18,;
    Caption=":"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="HUUNIIKNVU",Visible=.t., Left=38, Top=258,;
    Alignment=1, Width=88, Height=18,;
    Caption="Ultimo Backup:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="CTUKJXMLVB",Visible=.t., Left=137, Top=197,;
    Alignment=1, Width=182, Height=18,;
    Caption="Numero giorni back up in linea:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (cp_dbtype='DB2')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="GZEZBHEAWV",Visible=.t., Left=163, Top=226,;
    Alignment=1, Width=156, Height=18,;
    Caption="Numero back up in linea:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (cp_dbtype='DB2')
    endwith
  endfunc

  add object oBox_1_9 as StdBox with uid="PYQVRPEFPR",left=11, top=32, width=592,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_abk','AZBACKUP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AZCODAZI=AZBACKUP.AZCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
