* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bno                                                        *
*              GESTIONE NOMINATIVO SUI DOCUMENTI                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-02-27                                                      *
* Last revis.: 2014-02-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bno",oParentObject)
return(i_retval)

define class tgsve_bno as StdBatch
  * --- Local variables
  w_GESTIONE = .NULL.
  w_NOTIPO = space(1)
  w_NOTIPCLI = space(1)
  * --- WorkFile variables
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_GESTIONE = THIS.OPARENTOBJECT
    do case
      case this.oParentObject.CURRENTEVENT = "w_NOCODICE Changed"
        * --- Read from OFF_NOMI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NOCODCLI,NOTIPNOM,NOTIPCLI"+;
            " from "+i_cTable+" OFF_NOMI where ";
                +"NOCODICE = "+cp_ToStrODBC(this.oParentObject.w_NOCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NOCODCLI,NOTIPNOM,NOTIPCLI;
            from (i_cTable) where;
                NOCODICE = this.oParentObject.w_NOCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_MVCODCON = NVL(cp_ToDate(_read_.NOCODCLI),cp_NullValue(_read_.NOCODCLI))
          this.w_NOTIPO = NVL(cp_ToDate(_read_.NOTIPNOM),cp_NullValue(_read_.NOTIPNOM))
          this.w_NOTIPCLI = NVL(cp_ToDate(_read_.NOTIPCLI),cp_NullValue(_read_.NOTIPCLI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_MVCODCON = NVL( this.oParentObject.w_MVCODCON , SPACE( 15 ) )
        if EMPTY( this.oParentObject.w_MVCODCON )
          * --- Se il nominativo non � associato ad un ciente, occorre crearne uno SE PREVISTO DALA CAUSALE DEL DOCUMENTO
          * --- CREA CLIENTE
          if this.oParentObject.w_TDRICNOM = "A" AND NOT EMPTY( this.oParentObject.w_NOCODICE )
            if AH_YESNO( "Il nominativo selezionato non � cliente%0Si desidera trasformarlo?" )
              * --- Write into OFF_NOMI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"NOTIPNOM ="+cp_NullLink(cp_ToStrODBC("C"),'OFF_NOMI','NOTIPNOM');
                    +i_ccchkf ;
                +" where ";
                    +"NOCODICE = "+cp_ToStrODBC(this.oParentObject.w_NOCODICE);
                       )
              else
                update (i_cTable) set;
                    NOTIPNOM = "C";
                    &i_ccchkf. ;
                 where;
                    NOCODICE = this.oParentObject.w_NOCODICE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              this.oParentObject.w_MVCODCON = GSAR_BO1( THIS , this.oParentObject.w_NOCODICE , this.w_NOTIPO )
            endif
          endif
        endif
        if NOT EMPTY( this.oParentObject.w_MVCODCON )
          if this.w_NOTIPCLI="F"
            * --- Fornitore - nessuna selezione
            this.oParentObject.w_MVCODCON = ""
          else
            SETVALUELINKED("M", this.w_GESTIONE , "w_MVCODCON", this.oParentObject.w_MVCODCON , this.oParentObject.w_MVTIPCON )
          endif
        endif
        if EMPTY( this.oParentObject.w_MVCODCON )
          * --- Se l'assegnamento sul codice del cliente fallisce, occorre svuotare anche il codice nominativo
          this.oParentObject.w_NOCODICE = SPACE( 20 )
          this.oParentObject.w_DESCLF = SPACE( 40 )
        endif
      case this.oParentObject.CURRENTEVENT = "CaricaNominativo"
        * --- Read from OFF_NOMI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NOCODICE"+;
            " from "+i_cTable+" OFF_NOMI where ";
                +"NOCODCLI = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                +" and NOTIPCLI = "+cp_ToStrODBC("C");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NOCODICE;
            from (i_cTable) where;
                NOCODCLI = this.oParentObject.w_MVCODCON;
                and NOTIPCLI = "C";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_NOCODICE = NVL(cp_ToDate(_read_.NOCODICE),cp_NullValue(_read_.NOCODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OFF_NOMI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
