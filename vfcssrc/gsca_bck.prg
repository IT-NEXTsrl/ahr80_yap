* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_bck                                                        *
*              Verifica righe valorizzate                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_8]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-11                                                      *
* Last revis.: 2008-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_bck",oParentObject,m.pOPER)
return(i_retval)

define class tgsca_bck as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_OREC = 0
  w_CONTA = 0
  w_MESS = space(10)
  w_OK = .f.
  w_ATOGGETT = space(254)
  w_DATATT = space(10)
  w_CFUNC = space(10)
  * --- WorkFile variables
  CDC_MANU_idx=0
  OFF_ATTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Vertifica che esistano righe Valorizzate all'Atto dell' Inserimento (da GSCA_ACM)
    * --- C Controlli finali
    *     R controlli all'insert\update end
    *     W messaggi di warning
    this.w_OK = .T.
    do case
      case this.pOPER="C"
        if Not Empty(CHKCONS("C",this.oParentObject.w_CMDATREG,"B","N"))
          this.w_MESS = substr(CHKCONS("C",this.oParentObject.w_CMDATREG,"B","N"),12)
          this.w_OK = .F.
        endif
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOPER="R"
        SELECT (this.oParentObject.GSCA_MMC.cTrsName)
        this.w_OREC = RECNO()
        GO TOP
        * --- Conto le righe corrette (con centro voce e importo <> da 0 )
        COUNT FOR (NOT EMPTY(NVL(T_MRCODVOC," "))) AND (NOT EMPTY(NVL(T_MRCODICE," "))) AND (t_MRTOTIMP<>0) TO this.w_CONTA
        if this.w_OREC>0 AND this.w_OREC<=RECCOUNT()
          GOTO this.w_OREC
        endif
        if this.w_CONTA=0
          this.w_MESS = ah_Msgformat("Inserire almeno una riga di centri di costo")
          this.w_OK = .F.
        else
          this.w_CONTA = 0
          this.w_MESS = " "
          * --- Select from CDC_MANU
          i_nConn=i_TableProp[this.CDC_MANU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CDC_MANU_idx,2],.t.,this.CDC_MANU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" CDC_MANU ";
                +" where CMNUMREG="+cp_ToStrODBC(this.oParentObject.w_CMNUMREG)+" AND CMCOMPET="+cp_ToStrODBC(this.oParentObject.w_CMCOMPET)+"";
                 ,"_Curs_CDC_MANU")
          else
            select * from (i_cTable);
             where CMNUMREG=this.oParentObject.w_CMNUMREG AND CMCOMPET=this.oParentObject.w_CMCOMPET;
              into cursor _Curs_CDC_MANU
          endif
          if used('_Curs_CDC_MANU')
            select _Curs_CDC_MANU
            locate for 1=1
            do while not(eof())
            if NVL(_Curs_CDC_MANU.CMCODICE,"  ")<>this.oParentObject.w_CMCODICE
              this.w_MESS = ah_Msgformat("Esistono delle registrazioni con lo stesso numero (%1)",DTOC(CP_TODATE(_Curs_CDC_MANU.CMDATREG)) )
              this.w_CONTA = this.w_CONTA+1
            endif
              select _Curs_CDC_MANU
              continue
            enddo
            use
          endif
          if this.w_CONTA>0
            if NOT ah_YesNo("ATTENZIONE%0%1%0Confermi ugualmente?","", this.w_MESS)
              this.w_MESS = ah_Msgformat("Operazione annullata")
              this.w_OK = .F.
            endif
          endif
        endif
      case this.pOPER="W"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    if Not this.w_OK
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_AGEN="S" AND this.w_OK
      * --- Se il documento � generato da un'attivit� blocco la modifica e la cancellazione
      this.w_CFUNC = This.oparentobject.cFunction
      * --- Select from OFF_ATTI
      i_nConn=i_TableProp[this.OFF_ATTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_ATTI_idx,2],.t.,this.OFF_ATTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ATSERIAL,ATOGGETT,ATDATINI  from "+i_cTable+" OFF_ATTI ";
            +" where ATRIFMOV = "+cp_ToStrODBC(this.oParentObject.w_CMCODICE)+"";
             ,"_Curs_OFF_ATTI")
      else
        select ATSERIAL,ATOGGETT,ATDATINI from (i_cTable);
         where ATRIFMOV = this.oParentObject.w_CMCODICE;
          into cursor _Curs_OFF_ATTI
      endif
      if used('_Curs_OFF_ATTI')
        select _Curs_OFF_ATTI
        locate for 1=1
        do while not(eof())
        this.w_ATOGGETT = ALLTRIM(_Curs_OFF_ATTI.ATOGGETT)
        this.w_DATATT = LEFT(TTOC(_Curs_OFF_ATTI.ATDATINI),10)
        if this.w_CFUNC="Edit"
          this.w_MESS = ah_Errormsg("Movimento generato da un'attivit� (%1 del %2) ", "i"," ",this.w_ATOGGETT,this.w_DATATT,)
        else
          this.w_MESS = ah_Msgformat("Movimento generato da un'attivit� (%1 del %2)  impossibile eliminare",this.w_ATOGGETT,this.w_DATATT)
          this.w_OK = .f.
        endif
        EXIT
          select _Curs_OFF_ATTI
          continue
        enddo
        use
      endif
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CDC_MANU'
    this.cWorkTables[2]='OFF_ATTI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_CDC_MANU')
      use in _Curs_CDC_MANU
    endif
    if used('_Curs_OFF_ATTI')
      use in _Curs_OFF_ATTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
