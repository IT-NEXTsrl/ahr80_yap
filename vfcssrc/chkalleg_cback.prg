* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: chkalleg_cback                                                  *
*              CallBack per ChkAlleg asincrona                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2016-01-11                                                      *
* Last revis.: 2016-05-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFrmName,pTaskId
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tchkalleg_cback",oParentObject,m.pFrmName,m.pTaskId)
return(i_retval)

define class tchkalleg_cback as StdBatch
  * --- Local variables
  pFrmName = space(50)
  pTaskId = space(10)
  w_TaskResult = space(0)
  w_TaskState = space(1)
  w_oForm = .NULL.
  w_bAlleg = .f.
  w_GESTG = space(200)
  w_Msg = space(254)
  w_Return = space(200)
  * --- WorkFile variables
  ASYNTASK_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CallBack per ChkAlleg asincrona, legge dalla tabella di confine il risultato della chkalleg
    *     e lo visualizza sulla maschera (di cui ha ricevuto il nome)
    this.pTaskId = Alltrim(this.pTaskId)
    * --- Read from ASYNTASK
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ASYNTASK_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASYNTASK_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATRETURN,AT_STATO"+;
        " from "+i_cTable+" ASYNTASK where ";
            +"ATCODICE = "+cp_ToStrODBC(this.pTaskId);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATRETURN,AT_STATO;
        from (i_cTable) where;
            ATCODICE = this.pTaskId;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TaskResult = NVL(cp_ToDate(_read_.ATRETURN),cp_NullValue(_read_.ATRETURN))
      this.w_TaskState = NVL(cp_ToDate(_read_.AT_STATO),cp_NullValue(_read_.AT_STATO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    Local l_Form 
 m.l_Form = "frm"+Alltrim(this.pFrmName)
    if Type(l_Form)="O"
      * --- Utilizzo l'On Error per non avere un messaggio di errore nel caso in cui la maschera venga chiusa durante l'esecuzione della funzione di callback
      Local l_OnError, bCbackErr 
 m.l_OnError = On("Error") 
 m.bCbackErr = .F. 
 On Error bCbackErr = .T.
      this.w_oForm = &l_Form
      this.w_TaskResult = Alltrim(this.w_TaskResult)
      if this.w_TaskState<>"S" Or !Empty(this.w_TaskResult)
        this.w_bAlleg = .T.
        if Vartype(this.w_oForm.oAlleg)<>"O"
          this.w_oForm.AddObject("oAlleg","Image")     
          if Vartype(i_ThemesManager)=="O"
            this.w_oForm.oAlleg.Picture = Forceext(i_ThemesManager.RetBmpFromIco("bmp\attach.bmp", 24),"bmp")
          else
            this.w_oForm.oAlleg.Picture = "bmp\attach.bmp"
          endif
          this.w_oForm.oAlleg.Top = Objtoclient(this.w_oForm.oPgFrm.Page1, 1) - this.w_oForm.oAlleg.Height - 1
          this.w_oForm.oAlleg.Left = this.w_oForm.Width-25
          this.w_oForm.oAlleg.BackStyle = 0
          this.w_oForm.AddObject("oAlnum","Label")     
          this.w_oForm.oAlnum.Top = this.w_oForm.oAlleg.Top
          this.w_oForm.oAlnum.Left = this.w_oForm.Width-45
          this.w_oForm.oAlnum.Width = 25
          this.w_oForm.oAlnum.BackStyle = 0
          this.w_oForm.oAlnum.Alignment = 1
          this.w_oForm.oAlnum.ForeColor = i_ThemesManager.GetProp(98)
        endif
        if !bCbackErr
          if this.w_TaskState<>"S"
            if Vartype(i_ThemesManager)=="O"
              this.w_oForm.oAlleg.Picture = Forceext(i_ThemesManager.RetBmpFromIco("bmp\attach_alert.bmp", 24),"bmp")
            else
              this.w_oForm.oAlleg.Picture = "bmp\attach_alert.bmp"
            endif
          endif
          this.w_oForm.oAlleg.ToolTipText = Iif( this.w_TaskState=="S", cp_Translate(MSG_ARC_DOC), cp_Translate("Non � stato possibile recuperare i dati"))
          this.w_oForm.oAlleg.Visible = .T.
          this.w_oForm.oAlnum.Caption = this.w_TaskResult
          this.w_oForm.oAlnum.ToolTipText = cp_Translate(MSG_NUM_ARC_DOC)
          this.w_oForm.oAlnum.Visible = this.w_TaskState=="S"
        endif
      else
        * --- Rilascia flags allegati
        if Vartype(this.w_oForm.oAlleg)="O"
          this.w_oForm.oAlleg.Visible = .F.
          this.w_oForm.oAlnum.Visible = .F.
        endif
        * --- Zucchetti Aulla Inizio - iRevolution
        if Type("g_REVI")="C" And g_REVI="S" And Pemstatus(this.w_oForm,"oRevInfSync",5)
          this.w_oForm.oRevInfSync.Visible = .T.
        endif
        * --- Zucchetti Aulla Fine - iRevolution
      endif
      if !bCbackErr
        Select (this.w_oForm.cCursor) 
 Go Top
        if Type("GESTGUID")<>"U"
          this.w_GESTG = Nvl(GESTGUID,"")
          if !Empty(this.w_GESTG) And g_APPLICATION<>"ADHOC REVOLUTION"
            this.w_Return = Alltrim(gsma_bta( this.w_oForm, this.w_GESTG ))
            if !Empty(this.w_Return)
              if Vartype(this.w_oForm.oAttrib)<>"O"
                this.w_oForm.AddObject("oAttrib","Image")     
                if Vartype(i_ThemesManager)=="O"
                  this.w_oForm.oAttrib.Picture = Forceext(i_ThemesManager.RetBmpFromIco("bmp\tattrib.bmp", 24),"bmp")
                else
                  this.w_oForm.oAttrib.Picture = "bmp\tattrib.bmp"
                endif
                this.w_oForm.oAttrib.ToolTipText = cp_Translate(MSG_ATTRIBUTE)
                this.w_oForm.oAttrib.Top = Objtoclient(this.w_oForm.oPgFrm.Page1, 1) - this.w_oForm.oAttrib.Height - 1
                this.w_oForm.oAttrib.BackStyle = 0
                this.w_oForm.AddObject("oAtnum","Label")     
                this.w_oForm.oAtnum.Top = this.w_oForm.oAttrib.Top
                this.w_oForm.oAtnum.Width = 25
                this.w_oForm.oAtnum.BackStyle = 0
                this.w_oForm.oAtnum.Alignment = 1
              endif
              this.w_oForm.oAttrib.Left = this.w_oForm.Width-(25+Iif(this.w_bAlleg,40,0))
              this.w_oForm.oAtnum.Left = this.w_oForm.Width-(55+Iif(this.w_bAlleg,40,0))
              this.w_oForm.oAttrib.Visible = .T.
              this.w_oForm.oAtnum.Caption = this.w_Return
              this.w_oForm.oAtnum.ToolTipText = cp_Translate(MSG_NUM_ATTRIBUTE)
              this.w_oForm.oAtnum.Visible = .T.
              this.w_oForm.oAtnum.ForeColor = i_ThemesManager.GetProp(98)
            else
              if Vartype(this.w_oForm.oAttrib) = "O"
                this.w_oForm.oAttrib.Visible = .F.
                this.w_oForm.oAtnum.Visible = .F.
                * --- Zucchetti Aulla Inizio - iRevolution
                if Type("g_REVI")="C" And g_REVI="S" And Pemstatus(this.w_oForm,"oRevInfSync",5)
                  this.w_oForm.oRevInfSync.Visible = .F.
                endif
                * --- Zucchetti Aulla Fine - iRevolution
              endif
            endif
          endif
        endif
        * --- Zucchetti Aulla Inizio - iRevolution
        *     A causa del ritardo dovuto all'esecuzione asincrona potrei
        *     dover riposizionare l'icona della sincronizzazione
        if Type("g_REVI")="C" And g_REVI="S" And Pemstatus(this.w_oForm,"oRevInfSync",5) And this.w_oForm.oRevInfSync.Visible
          this.w_oForm.oRevInfSync.Left = this.w_oForm.Width-(28+Iif(PemStatus(this.w_oForm,"oAlleg",5) and this.w_oForm.oAlleg.visible,40,Iif(PemStatus(this.w_oForm,"oAttrib",5) and this.w_oForm.oAttrib.visible,40,0)))
        endif
      else
        this.w_oForm = .null.
      endif
      * --- Ripristino On Error
      On Error &l_OnError
      * --- Zucchetti Aulla Fine - iRevolution
    endif
  endproc


  proc Init(oParentObject,pFrmName,pTaskId)
    this.pFrmName=pFrmName
    this.pTaskId=pTaskId
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ASYNTASK'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFrmName,pTaskId"
endproc
