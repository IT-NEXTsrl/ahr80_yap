* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bak                                                        *
*              Controlli F24                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][98]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-20                                                      *
* Last revis.: 2005-12-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPROV,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bak",oParentObject,m.pPROV,m.pTIPO)
return(i_retval)

define class tgscg_bak as StdBatch
  * --- Local variables
  pPROV = space(1)
  pTIPO = space(3)
  w_INIPER = ctod("  /  /  ")
  w_FINEPER = ctod("  /  /  ")
  w_OBJFIGLIO = .NULL.
  w_VALUTA = space(3)
  w_MESS = space(50)
  w_PUNPAD = .NULL.
  w_DATARIF = ctod("  /  /  ")
  w_CODESE = space(4)
  * --- WorkFile variables
  ESERCIZI_idx=0
  MOD_PAG_idx=0
  SEDIAZIE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla se gli estremi dell'esercizio concidono con l'inizio e la fine dell'anno solare
    * --- Controlla Data Presentazione Modello
    * --- Aggiorna la valuta della sezione erario (da GSCG_AMF)
    * --- Puntatore al Padre (GSCG_AMF, GSCG_ANF)
    this.w_PUNPAD = this.oParentObject
    do case
      case this.pTIPO="ANN"
        * --- Read from SEDIAZIE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SECODDES,SE___CAP,SEINDIRI,SELOCALI,SEPROVIN,SEDESSTA"+;
            " from "+i_cTable+" SEDIAZIE where ";
                +"SECODAZI = "+cp_ToStrODBC(i_CODAZI);
                +" and SEDESSTA = "+cp_ToStrODBC("S");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SECODDES,SE___CAP,SEINDIRI,SELOCALI,SEPROVIN,SEDESSTA;
            from (i_cTable) where;
                SECODAZI = i_CODAZI;
                and SEDESSTA = "S";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_MFDESSTA = NVL(cp_ToDate(_read_.SECODDES),cp_NullValue(_read_.SECODDES))
          this.oParentObject.w_CAP = NVL(cp_ToDate(_read_.SE___CAP),cp_NullValue(_read_.SE___CAP))
          this.oParentObject.w_CAP1 = NVL(cp_ToDate(_read_.SE___CAP),cp_NullValue(_read_.SE___CAP))
          this.oParentObject.w_INDIRI = NVL(cp_ToDate(_read_.SEINDIRI),cp_NullValue(_read_.SEINDIRI))
          this.oParentObject.w_INDIRI1 = NVL(cp_ToDate(_read_.SEINDIRI),cp_NullValue(_read_.SEINDIRI))
          this.oParentObject.w_LOCALI = NVL(cp_ToDate(_read_.SELOCALI),cp_NullValue(_read_.SELOCALI))
          this.oParentObject.w_LOCALI1 = NVL(cp_ToDate(_read_.SELOCALI),cp_NullValue(_read_.SELOCALI))
          this.oParentObject.w_PROVIN = NVL(cp_ToDate(_read_.SEPROVIN),cp_NullValue(_read_.SEPROVIN))
          this.oParentObject.w_PROVIN1 = NVL(cp_ToDate(_read_.SEPROVIN),cp_NullValue(_read_.SEPROVIN))
          this.oParentObject.w_DESSTA = NVL(cp_ToDate(_read_.SEDESSTA),cp_NullValue(_read_.SEDESSTA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(NVL(this.oParentObject.w_MFDESSTA,""))
          * --- Se sede stampa non definita prende la prima sede fiscale
          * --- Read from SEDIAZIE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "SECODDES,SE___CAP,SEINDIRI,SELOCALI,SEPROVIN,SEDESSTA"+;
              " from "+i_cTable+" SEDIAZIE where ";
                  +"SECODAZI = "+cp_ToStrODBC(i_CODAZI);
                  +" and SETIPRIF = "+cp_ToStrODBC("DF");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              SECODDES,SE___CAP,SEINDIRI,SELOCALI,SEPROVIN,SEDESSTA;
              from (i_cTable) where;
                  SECODAZI = i_CODAZI;
                  and SETIPRIF = "DF";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_MFDESSTA = NVL(cp_ToDate(_read_.SECODDES),cp_NullValue(_read_.SECODDES))
            this.oParentObject.w_CAP = NVL(cp_ToDate(_read_.SE___CAP),cp_NullValue(_read_.SE___CAP))
            this.oParentObject.w_CAP1 = NVL(cp_ToDate(_read_.SE___CAP),cp_NullValue(_read_.SE___CAP))
            this.oParentObject.w_INDIRI = NVL(cp_ToDate(_read_.SEINDIRI),cp_NullValue(_read_.SEINDIRI))
            this.oParentObject.w_INDIRI1 = NVL(cp_ToDate(_read_.SEINDIRI),cp_NullValue(_read_.SEINDIRI))
            this.oParentObject.w_LOCALI = NVL(cp_ToDate(_read_.SELOCALI),cp_NullValue(_read_.SELOCALI))
            this.oParentObject.w_LOCALI1 = NVL(cp_ToDate(_read_.SELOCALI),cp_NullValue(_read_.SELOCALI))
            this.oParentObject.w_PROVIN = NVL(cp_ToDate(_read_.SEPROVIN),cp_NullValue(_read_.SEPROVIN))
            this.oParentObject.w_PROVIN1 = NVL(cp_ToDate(_read_.SEPROVIN),cp_NullValue(_read_.SEPROVIN))
            this.oParentObject.w_DESSTA = NVL(cp_ToDate(_read_.SEDESSTA),cp_NullValue(_read_.SEDESSTA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Iniczializza il campo sede destinatario stampa
        do case
          case this.pPROV="N"
            if this.w_PUNPAD.GSCG_AVF.w_VFDTPRES < cp_CharToDate("01-01-2002")
              this.w_MESS = "La data di presentazione del modello deve essere successiva al 01-01-2002%0I dati inseriti non potranno essere salvati"
              ah_ErrorMsg(this.w_MESS,48)
            endif
          case this.pPROV="M"
            if this.w_PUNPAD.GSCG_AVF.w_VFDTPRES >= cp_CharToDate("01-01-2002")
              this.w_MESS = "La data di presentazione del modello deve essere antecedente al 01-01-2002%0I dati inseriti non potranno essere salvati"
              ah_ErrorMsg(this.w_MESS,48)
            endif
        endcase
        * --- FLAG Anno di imposta non coincidente con anno solare
        this.w_DATARIF = cp_CharToDate("01-"+this.oParentObject.w_MFMESRIF+"-"+this.oParentObject.w_MFANNRIF)
        this.w_CODESE = CALCESER(this.w_DATARIF,g_CODESE)
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ESINIESE,ESFINESE"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(i_codazi);
                +" and ESCODESE = "+cp_ToStrODBC(this.w_CODESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ESINIESE,ESFINESE;
            from (i_cTable) where;
                ESCODAZI = i_codazi;
                and ESCODESE = this.w_CODESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INIPER = NVL(cp_ToDate(_read_.ESINIESE),cp_NullValue(_read_.ESINIESE))
          this.w_FINEPER = NVL(cp_ToDate(_read_.ESFINESE),cp_NullValue(_read_.ESFINESE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if right(dtoc(this.w_INIPER,1),4)<>"0101" or right(dtoc(this.w_FINEPER,1),4)<>"1231"
          this.oParentObject.w_APPCOIN = "N"
        else
          this.oParentObject.w_APPCOIN = "S"
        endif
      case this.pTIPO="VAL"
        this.w_OBJFIGLIO = this.w_PUNPAD.GSCG_AEF
        this.w_OBJFIGLIO.w_VALUTA = this.oParentObject.w_MFVALUTA
        this.w_OBJFIGLIO.SetControlsValue()     
        this.w_OBJFIGLIO.mHideControls()     
        this.w_MESS = "Gli importi non verranno automaticamente riconvertiti"
        ah_ErrorMsg(this.w_MESS,48)
    endcase
  endproc


  proc Init(oParentObject,pPROV,pTIPO)
    this.pPROV=pPROV
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='MOD_PAG'
    this.cWorkTables[3]='SEDIAZIE'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPROV,pTIPO"
endproc
