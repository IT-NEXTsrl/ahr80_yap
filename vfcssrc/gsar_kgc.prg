* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kgc                                                        *
*              Generazione calendari aziendali                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-06-26                                                      *
* Last revis.: 2012-05-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kgc",oParentObject))

* --- Class definition
define class tgsar_kgc as StdForm
  Top    = 12
  Left   = 47

  * --- Standard Properties
  Width  = 701
  Height = 421
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-05-24"
  HelpContextID=32077673
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=74

  * --- Constant Properties
  _IDX = 0
  TAB_CALE_IDX = 0
  FES_MAST_IDX = 0
  cPrg = "gsar_kgc"
  cComment = "Generazione calendari aziendali"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODCAL = space(5)
  o_CODCAL = space(5)
  w_CKDA1STD = space(10)
  w_DCAL = space(40)
  w_COD_FEST = space(3)
  w_FEDESCRI = space(35)
  w_ANNO = 0
  o_ANNO = 0
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_DA1STD = space(5)
  o_DA1STD = space(5)
  w_AL1STD = space(5)
  o_AL1STD = space(5)
  w_DA2STD = space(5)
  o_DA2STD = space(5)
  w_AL2STD = space(5)
  o_AL2STD = space(5)
  w_NUMORE = 0
  w_DAY2 = space(1)
  o_DAY2 = space(1)
  w_DA1DAY2 = space(5)
  o_DA1DAY2 = space(5)
  w_AL1DAY2 = space(5)
  o_AL1DAY2 = space(5)
  w_DA2DAY2 = space(5)
  o_DA2DAY2 = space(5)
  w_AL2DAY2 = space(5)
  o_AL2DAY2 = space(5)
  w_DAY3 = space(1)
  o_DAY3 = space(1)
  w_DA1DAY3 = space(5)
  o_DA1DAY3 = space(5)
  w_AL1DAY3 = space(5)
  o_AL1DAY3 = space(5)
  w_DA2DAY3 = space(5)
  o_DA2DAY3 = space(5)
  w_AL2DAY3 = space(5)
  o_AL2DAY3 = space(5)
  w_DAY4 = space(1)
  o_DAY4 = space(1)
  w_DA1DAY4 = space(5)
  o_DA1DAY4 = space(5)
  w_AL1DAY4 = space(5)
  o_AL1DAY4 = space(5)
  w_DA2DAY4 = space(5)
  o_DA2DAY4 = space(5)
  w_AL2DAY4 = space(5)
  o_AL2DAY4 = space(5)
  w_DAY5 = space(1)
  o_DAY5 = space(1)
  w_DA1DAY5 = space(5)
  o_DA1DAY5 = space(5)
  w_AL1DAY5 = space(5)
  o_AL1DAY5 = space(5)
  w_DA2DAY5 = space(5)
  o_DA2DAY5 = space(5)
  w_AL2DAY5 = space(5)
  o_AL2DAY5 = space(5)
  w_DAY6 = space(1)
  o_DAY6 = space(1)
  w_DA1DAY6 = space(5)
  o_DA1DAY6 = space(5)
  w_AL1DAY6 = space(5)
  o_AL1DAY6 = space(5)
  w_DA2DAY6 = space(5)
  o_DA2DAY6 = space(5)
  w_AL2DAY6 = space(5)
  o_AL2DAY6 = space(5)
  w_DAY7 = space(1)
  o_DAY7 = space(1)
  w_DA1DAY7 = space(5)
  o_DA1DAY7 = space(5)
  w_AL1DAY7 = space(5)
  o_AL1DAY7 = space(5)
  w_DA2DAY7 = space(5)
  o_DA2DAY7 = space(5)
  w_AL2DAY7 = space(5)
  o_AL2DAY7 = space(5)
  w_DAY1 = space(1)
  o_DAY1 = space(1)
  w_DA1DAY1 = space(5)
  o_DA1DAY1 = space(5)
  w_AL1DAY1 = space(5)
  o_AL1DAY1 = space(5)
  w_DA2DAY1 = space(5)
  o_DA2DAY1 = space(5)
  w_AL2DAY1 = space(5)
  o_AL2DAY1 = space(5)
  w_OREDAY2 = 0
  w_OREDAY3 = 0
  w_OREDAY4 = 0
  w_OREDAY5 = 0
  w_OREDAY6 = 0
  w_OREDAY7 = 0
  w_OREDAY1 = 0
  w_INPER1 = ctod('  /  /  ')
  w_FIPER1 = ctod('  /  /  ')
  w_INPER2 = ctod('  /  /  ')
  w_FIPER2 = ctod('  /  /  ')
  w_INPER3 = ctod('  /  /  ')
  w_FIPER3 = ctod('  /  /  ')
  w_INPER4 = ctod('  /  /  ')
  w_FIPER4 = ctod('  /  /  ')
  w_INPER5 = ctod('  /  /  ')
  w_FIPER5 = ctod('  /  /  ')
  w_INPER6 = ctod('  /  /  ')
  w_FIPER6 = ctod('  /  /  ')
  w_INPER7 = ctod('  /  /  ')
  w_FIPER7 = ctod('  /  /  ')
  w_GIOSO1 = space(2)
  w_MESO1 = space(2)
  w_GIOSO2 = space(2)
  w_MESO2 = space(2)
  w_DESCHIPE = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kgcPag1","gsar_kgc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODCAL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TAB_CALE'
    this.cWorkTables[2]='FES_MAST'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSAR_BGC with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODCAL=space(5)
      .w_CKDA1STD=space(10)
      .w_DCAL=space(40)
      .w_COD_FEST=space(3)
      .w_FEDESCRI=space(35)
      .w_ANNO=0
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DA1STD=space(5)
      .w_AL1STD=space(5)
      .w_DA2STD=space(5)
      .w_AL2STD=space(5)
      .w_NUMORE=0
      .w_DAY2=space(1)
      .w_DA1DAY2=space(5)
      .w_AL1DAY2=space(5)
      .w_DA2DAY2=space(5)
      .w_AL2DAY2=space(5)
      .w_DAY3=space(1)
      .w_DA1DAY3=space(5)
      .w_AL1DAY3=space(5)
      .w_DA2DAY3=space(5)
      .w_AL2DAY3=space(5)
      .w_DAY4=space(1)
      .w_DA1DAY4=space(5)
      .w_AL1DAY4=space(5)
      .w_DA2DAY4=space(5)
      .w_AL2DAY4=space(5)
      .w_DAY5=space(1)
      .w_DA1DAY5=space(5)
      .w_AL1DAY5=space(5)
      .w_DA2DAY5=space(5)
      .w_AL2DAY5=space(5)
      .w_DAY6=space(1)
      .w_DA1DAY6=space(5)
      .w_AL1DAY6=space(5)
      .w_DA2DAY6=space(5)
      .w_AL2DAY6=space(5)
      .w_DAY7=space(1)
      .w_DA1DAY7=space(5)
      .w_AL1DAY7=space(5)
      .w_DA2DAY7=space(5)
      .w_AL2DAY7=space(5)
      .w_DAY1=space(1)
      .w_DA1DAY1=space(5)
      .w_AL1DAY1=space(5)
      .w_DA2DAY1=space(5)
      .w_AL2DAY1=space(5)
      .w_OREDAY2=0
      .w_OREDAY3=0
      .w_OREDAY4=0
      .w_OREDAY5=0
      .w_OREDAY6=0
      .w_OREDAY7=0
      .w_OREDAY1=0
      .w_INPER1=ctod("  /  /  ")
      .w_FIPER1=ctod("  /  /  ")
      .w_INPER2=ctod("  /  /  ")
      .w_FIPER2=ctod("  /  /  ")
      .w_INPER3=ctod("  /  /  ")
      .w_FIPER3=ctod("  /  /  ")
      .w_INPER4=ctod("  /  /  ")
      .w_FIPER4=ctod("  /  /  ")
      .w_INPER5=ctod("  /  /  ")
      .w_FIPER5=ctod("  /  /  ")
      .w_INPER6=ctod("  /  /  ")
      .w_FIPER6=ctod("  /  /  ")
      .w_INPER7=ctod("  /  /  ")
      .w_FIPER7=ctod("  /  /  ")
      .w_GIOSO1=space(2)
      .w_MESO1=space(2)
      .w_GIOSO2=space(2)
      .w_MESO2=space(2)
      .w_DESCHIPE=space(35)
        .w_CODCAL = getpredcal()
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODCAL))
          .link_1_2('Full')
        endif
        .DoRTCalc(2,4,.f.)
        if not(empty(.w_COD_FEST))
          .link_1_6('Full')
        endif
          .DoRTCalc(5,5,.f.)
        .w_ANNO = year(i_DATSYS)+1
        .w_DATINI = IIF(EMPTY(.w_ANNO), cp_CharToDate('  -  -    '), cp_CharToDate('01-01-'+ALLTRIM(STR(.w_ANNO,4,0))))
        .w_DATFIN = IIF(EMPTY(.w_ANNO), cp_CharToDate('  -  -    '), cp_CharToDate('31-12-'+ALLTRIM(STR(.w_ANNO,4,0))))
        .w_DA1STD = IIF(EMPTY(.w_DA1STD), "09:00", formattime(.w_DA1STD, ":"))
        .w_AL1STD = IIF(EMPTY(.w_AL1STD), "13:00", formattime(.w_AL1STD, ":"))
        .w_DA2STD = formattime(.w_DA2STD, ":")
        .w_AL2STD = IIF(EMPTY(.w_DA2STD),"", formattime(.w_AL2STD, ":" ) )
          .DoRTCalc(13,13,.f.)
        .w_DAY2 = IIF(EMPTY(.w_DAY2), "S", .w_DAY2)
        .w_DA1DAY2 = IIF(.w_DAY2<>'S', "", IIF(EMPTY(.w_DA1DAY2), .w_DA1STD, formattime(.w_DA1DAY2, ":") ))
        .w_AL1DAY2 = IIF(.w_DAY2<>'S', "", IIF( EMPTY(.w_AL1DAY2), .w_AL1STD, formattime(.w_AL1DAY2, ":") ))
        .w_DA2DAY2 = IIF(.w_DAY2<>'S' OR EMPTY(.w_AL1DAY2) , "", formattime(.w_DA2DAY2, ":") )
        .w_AL2DAY2 = IIF(.w_DAY2<>'S' OR EMPTY(.w_DA2DAY2), "", formattime(.w_AL2DAY2, ":") )
        .w_DAY3 = IIF(EMPTY(.w_DAY3), "S", .w_DAY3)
        .w_DA1DAY3 = IIF(.w_DAY3<>'S', "", IIF(EMPTY(.w_DA1DAY3), .w_DA1STD, formattime(.w_DA1DAY3, ":") ))
        .w_AL1DAY3 = IIF(.w_DAY3<>'S', "", IIF( EMPTY(.w_AL1DAY3), .w_AL1STD, formattime(.w_AL1DAY3, ":") ))
        .w_DA2DAY3 = IIF(.w_DAY3<>'S' OR EMPTY(.w_AL1DAY3) , "", formattime(.w_DA2DAY3, ":") )
        .w_AL2DAY3 = IIF(.w_DAY3<>'S' OR EMPTY(.w_DA2DAY3), "", formattime(.w_AL2DAY3, ":") )
        .w_DAY4 = IIF(EMPTY(.w_DAY4), "S", .w_DAY4)
        .w_DA1DAY4 = IIF(.w_DAY4<>'S', "", IIF(EMPTY(.w_DA1DAY4), .w_DA1STD, formattime(.w_DA1DAY4, ":") ))
        .w_AL1DAY4 = IIF(.w_DAY4<>'S', "", IIF( EMPTY(.w_AL1DAY4), .w_AL1STD, formattime(.w_AL1DAY4, ":") ))
        .w_DA2DAY4 = IIF(.w_DAY4<>'S' OR EMPTY(.w_AL1DAY4) , "", formattime(.w_DA2DAY4, ":") )
        .w_AL2DAY4 = IIF(.w_DAY4<>'S' OR EMPTY(.w_DA2DAY4), "", formattime(.w_AL2DAY4, ":") )
        .w_DAY5 = IIF(EMPTY(.w_DAY5), "S", .w_DAY5)
        .w_DA1DAY5 = IIF(.w_DAY5<>'S', "", IIF(EMPTY(.w_DA1DAY5), .w_DA1STD, formattime(.w_DA1DAY5, ":") ))
        .w_AL1DAY5 = IIF(.w_DAY5<>'S', "", IIF( EMPTY(.w_AL1DAY5), .w_AL1STD, formattime(.w_AL1DAY5, ":") ))
        .w_DA2DAY5 = IIF(.w_DAY5<>'S' OR EMPTY(.w_AL1DAY5) , "", formattime(.w_DA2DAY5, ":") )
        .w_AL2DAY5 = IIF(.w_DAY5<>'S' OR EMPTY(.w_DA2DAY5), "", formattime(.w_AL2DAY5, ":") )
        .w_DAY6 = IIF(EMPTY(.w_DAY6), "S", .w_DAY6)
        .w_DA1DAY6 = IIF(.w_DAY6<>'S', "", IIF(EMPTY(.w_DA1DAY6), .w_DA1STD, formattime(.w_DA1DAY6, ":") ))
        .w_AL1DAY6 = IIF(.w_DAY6<>'S', "", IIF( EMPTY(.w_AL1DAY6), .w_AL1STD, formattime(.w_AL1DAY6, ":") ))
        .w_DA2DAY6 = IIF(.w_DAY6<>'S' OR EMPTY(.w_AL1DAY6) , "", formattime(.w_DA2DAY6, ":") )
        .w_AL2DAY6 = IIF(.w_DAY6<>'S' OR EMPTY(.w_DA2DAY6), "", formattime(.w_AL2DAY6, ":") )
        .w_DAY7 = IIF(EMPTY(.w_DAY7), "N", .w_DAY7)
        .w_DA1DAY7 = IIF(.w_DAY7<>'S', "", IIF(EMPTY(.w_DA1DAY7), .w_DA1STD, formattime(.w_DA1DAY7, ":") ))
        .w_AL1DAY7 = IIF(.w_DAY7<>'S', "", IIF( EMPTY(.w_AL1DAY7), .w_AL1STD, formattime(.w_AL1DAY7, ":") ))
        .w_DA2DAY7 = IIF(.w_DAY7<>'S' OR EMPTY(.w_AL1DAY7) , "", formattime(.w_DA2DAY7, ":") )
        .w_AL2DAY7 = IIF(.w_DAY7<>'S' OR EMPTY(.w_DA2DAY7), "", formattime(.w_AL2DAY7, ":") )
        .w_DAY1 = IIF(EMPTY(.w_DAY1), "N", .w_DAY1)
        .w_DA1DAY1 = IIF(.w_DAY1<>'S', "", IIF(EMPTY(.w_DA1DAY1), .w_DA1STD, formattime(.w_DA1DAY1, ":") ))
        .w_AL1DAY1 = IIF(.w_DAY1<>'S', "", IIF( EMPTY(.w_AL1DAY1), .w_AL1STD, formattime(.w_AL1DAY1, ":") ))
        .w_DA2DAY1 = IIF(.w_DAY1<>'S' OR EMPTY(.w_AL1DAY1) , "", formattime(.w_DA2DAY1, ":") )
        .w_AL2DAY1 = IIF(.w_DAY1<>'S' OR EMPTY(.w_DA2DAY1), "", formattime(.w_AL2DAY1, ":") )
        .w_OREDAY2 = IIF(.w_NUMORE<>0,.w_NUMORE,(cp_ROUND((cp_ROUND(CTOT(.w_AL1DAY2)-CTOT(.w_DA1DAY2),0))/3600,2)+cp_ROUND((cp_ROUND(CTOT(.w_AL2DAY2)-CTOT(.w_DA2DAY2),0))/3600,2)))
        .w_OREDAY3 = IIF(.w_NUMORE<>0,.w_NUMORE,(cp_ROUND((cp_ROUND(CTOT(.w_AL1DAY3)-CTOT(.w_DA1DAY3),0))/3600,2)+cp_ROUND((cp_ROUND(CTOT(.w_AL2DAY3)-CTOT(.w_DA2DAY3),0))/3600,2)))
        .w_OREDAY4 = IIF(.w_NUMORE<>0,.w_NUMORE,(cp_ROUND((cp_ROUND(CTOT(.w_AL1DAY4)-CTOT(.w_DA1DAY4),0))/3600,2)+cp_ROUND((cp_ROUND(CTOT(.w_AL2DAY4)-CTOT(.w_DA2DAY4),0))/3600,2)))
        .w_OREDAY5 = IIF(.w_NUMORE<>0,.w_NUMORE,(cp_ROUND((cp_ROUND(CTOT(.w_AL1DAY5)-CTOT(.w_DA1DAY5),0))/3600,2)+cp_ROUND((cp_ROUND(CTOT(.w_AL2DAY5)-CTOT(.w_DA2DAY5),0))/3600,2)))
        .w_OREDAY6 = IIF(.w_NUMORE<>0,.w_NUMORE,(cp_ROUND((cp_ROUND(CTOT(.w_AL1DAY6)-CTOT(.w_DA1DAY6),0))/3600,2)+cp_ROUND((cp_ROUND(CTOT(.w_AL2DAY6)-CTOT(.w_DA2DAY6),0))/3600,2)))
        .w_OREDAY7 = IIF(.w_NUMORE<>0,.w_NUMORE,(cp_ROUND((cp_ROUND(CTOT(.w_AL1DAY7)-CTOT(.w_DA1DAY7),0))/3600,2)+cp_ROUND((cp_ROUND(CTOT(.w_AL2DAY7)-CTOT(.w_DA2DAY7),0))/3600,2)))
        .w_OREDAY1 = IIF(.w_NUMORE<>0,.w_NUMORE,(cp_ROUND((cp_ROUND(CTOT(.w_AL1DAY1)-CTOT(.w_DA1DAY1),0))/3600,2)+cp_ROUND((cp_ROUND(CTOT(.w_AL2DAY1)-CTOT(.w_DA2DAY1),0))/3600,2)))
          .DoRTCalc(56,69,.f.)
        .w_GIOSO1 = '01'
        .w_MESO1 = '08'
        .w_GIOSO2 = '15'
        .w_MESO2 = '09'
        .w_DESCHIPE = "Chiuso (ferie)"
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_94.enabled = this.oPgFrm.Page1.oPag.oBtn_1_94.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_95.enabled = this.oPgFrm.Page1.oPag.oBtn_1_95.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_114.enabled = this.oPgFrm.Page1.oPag.oBtn_1_114.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsar_kgc
    * Effetua correttamente il calcolo delle ore lavorative
    this.mCalc(.T.)
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_ANNO<>.w_ANNO
            .w_DATINI = IIF(EMPTY(.w_ANNO), cp_CharToDate('  -  -    '), cp_CharToDate('01-01-'+ALLTRIM(STR(.w_ANNO,4,0))))
        endif
        if .o_ANNO<>.w_ANNO
            .w_DATFIN = IIF(EMPTY(.w_ANNO), cp_CharToDate('  -  -    '), cp_CharToDate('31-12-'+ALLTRIM(STR(.w_ANNO,4,0))))
        endif
        if .o_DA1STD<>.w_DA1STD
            .w_DA1STD = IIF(EMPTY(.w_DA1STD), "09:00", formattime(.w_DA1STD, ":"))
        endif
        if .o_AL1STD<>.w_AL1STD.or. .o_DA1STD<>.w_DA1STD
            .w_AL1STD = IIF(EMPTY(.w_AL1STD), "13:00", formattime(.w_AL1STD, ":"))
        endif
        if .o_DA2STD<>.w_DA2STD.or. .o_DA1STD<>.w_DA1STD.or. .o_AL1STD<>.w_AL1STD
            .w_DA2STD = formattime(.w_DA2STD, ":")
        endif
        if .o_DA1STD<>.w_DA1STD.or. .o_AL1STD<>.w_AL1STD.or. .o_DA2STD<>.w_DA2STD.or. .o_AL2STD<>.w_AL2STD
            .w_AL2STD = IIF(EMPTY(.w_DA2STD),"", formattime(.w_AL2STD, ":" ) )
        endif
        if .o_CODCAL<>.w_CODCAL
          .Calculate_PGBTIUOPZP()
        endif
        .DoRTCalc(13,14,.t.)
        if .o_DA1DAY2<>.w_DA1DAY2.or. .o_DAY2<>.w_DAY2
            .w_DA1DAY2 = IIF(.w_DAY2<>'S', "", IIF(EMPTY(.w_DA1DAY2), .w_DA1STD, formattime(.w_DA1DAY2, ":") ))
        endif
        if .o_DA1DAY2<>.w_DA1DAY2.or. .o_AL1DAY2<>.w_AL1DAY2.or. .o_DAY2<>.w_DAY2
            .w_AL1DAY2 = IIF(.w_DAY2<>'S', "", IIF( EMPTY(.w_AL1DAY2), .w_AL1STD, formattime(.w_AL1DAY2, ":") ))
        endif
        if .o_DA1DAY2<>.w_DA1DAY2.or. .o_DA2DAY2<>.w_DA2DAY2.or. .o_AL1DAY2<>.w_AL1DAY2.or. .o_DAY2<>.w_DAY2
            .w_DA2DAY2 = IIF(.w_DAY2<>'S' OR EMPTY(.w_AL1DAY2) , "", formattime(.w_DA2DAY2, ":") )
        endif
        if .o_DA1DAY2<>.w_DA1DAY2.or. .o_DA2DAY2<>.w_DA2DAY2.or. .o_AL2DAY2<>.w_AL2DAY2.or. .o_AL1DAY2<>.w_AL1DAY2.or. .o_DAY2<>.w_DAY2
            .w_AL2DAY2 = IIF(.w_DAY2<>'S' OR EMPTY(.w_DA2DAY2), "", formattime(.w_AL2DAY2, ":") )
        endif
        .DoRTCalc(19,19,.t.)
        if .o_DA1DAY3<>.w_DA1DAY3.or. .o_DAY3<>.w_DAY3
            .w_DA1DAY3 = IIF(.w_DAY3<>'S', "", IIF(EMPTY(.w_DA1DAY3), .w_DA1STD, formattime(.w_DA1DAY3, ":") ))
        endif
        if .o_DA1DAY3<>.w_DA1DAY3.or. .o_AL1DAY3<>.w_AL1DAY3.or. .o_DAY3<>.w_DAY3
            .w_AL1DAY3 = IIF(.w_DAY3<>'S', "", IIF( EMPTY(.w_AL1DAY3), .w_AL1STD, formattime(.w_AL1DAY3, ":") ))
        endif
        if .o_DA1DAY3<>.w_DA1DAY3.or. .o_DA2DAY3<>.w_DA2DAY3.or. .o_AL1DAY3<>.w_AL1DAY3.or. .o_DAY3<>.w_DAY3
            .w_DA2DAY3 = IIF(.w_DAY3<>'S' OR EMPTY(.w_AL1DAY3) , "", formattime(.w_DA2DAY3, ":") )
        endif
        if .o_DA1DAY3<>.w_DA1DAY3.or. .o_DA2DAY3<>.w_DA2DAY3.or. .o_AL2DAY3<>.w_AL2DAY3.or. .o_AL1DAY3<>.w_AL1DAY3.or. .o_DAY3<>.w_DAY3
            .w_AL2DAY3 = IIF(.w_DAY3<>'S' OR EMPTY(.w_DA2DAY3), "", formattime(.w_AL2DAY3, ":") )
        endif
        .DoRTCalc(24,24,.t.)
        if .o_DA1DAY4<>.w_DA1DAY4.or. .o_DAY4<>.w_DAY4
            .w_DA1DAY4 = IIF(.w_DAY4<>'S', "", IIF(EMPTY(.w_DA1DAY4), .w_DA1STD, formattime(.w_DA1DAY4, ":") ))
        endif
        if .o_DA1DAY4<>.w_DA1DAY4.or. .o_AL1DAY4<>.w_AL1DAY4.or. .o_DAY4<>.w_DAY4
            .w_AL1DAY4 = IIF(.w_DAY4<>'S', "", IIF( EMPTY(.w_AL1DAY4), .w_AL1STD, formattime(.w_AL1DAY4, ":") ))
        endif
        if .o_DA1DAY4<>.w_DA1DAY4.or. .o_DA2DAY4<>.w_DA2DAY4.or. .o_AL1DAY4<>.w_AL1DAY4.or. .o_DAY4<>.w_DAY4
            .w_DA2DAY4 = IIF(.w_DAY4<>'S' OR EMPTY(.w_AL1DAY4) , "", formattime(.w_DA2DAY4, ":") )
        endif
        if .o_DA1DAY4<>.w_DA1DAY4.or. .o_DA2DAY4<>.w_DA2DAY4.or. .o_AL2DAY4<>.w_AL2DAY4.or. .o_AL1DAY4<>.w_AL1DAY4.or. .o_DAY4<>.w_DAY4
            .w_AL2DAY4 = IIF(.w_DAY4<>'S' OR EMPTY(.w_DA2DAY4), "", formattime(.w_AL2DAY4, ":") )
        endif
        .DoRTCalc(29,29,.t.)
        if .o_DA1DAY5<>.w_DA1DAY5.or. .o_DAY5<>.w_DAY5
            .w_DA1DAY5 = IIF(.w_DAY5<>'S', "", IIF(EMPTY(.w_DA1DAY5), .w_DA1STD, formattime(.w_DA1DAY5, ":") ))
        endif
        if .o_DA1DAY5<>.w_DA1DAY5.or. .o_AL1DAY5<>.w_AL1DAY5.or. .o_DAY5<>.w_DAY5
            .w_AL1DAY5 = IIF(.w_DAY5<>'S', "", IIF( EMPTY(.w_AL1DAY5), .w_AL1STD, formattime(.w_AL1DAY5, ":") ))
        endif
        if .o_DA1DAY5<>.w_DA1DAY5.or. .o_DA2DAY5<>.w_DA2DAY5.or. .o_AL1DAY5<>.w_AL1DAY5.or. .o_DAY5<>.w_DAY5
            .w_DA2DAY5 = IIF(.w_DAY5<>'S' OR EMPTY(.w_AL1DAY5) , "", formattime(.w_DA2DAY5, ":") )
        endif
        if .o_DA1DAY5<>.w_DA1DAY5.or. .o_DA2DAY5<>.w_DA2DAY5.or. .o_AL2DAY5<>.w_AL2DAY5.or. .o_AL1DAY5<>.w_AL1DAY5.or. .o_DAY5<>.w_DAY5
            .w_AL2DAY5 = IIF(.w_DAY5<>'S' OR EMPTY(.w_DA2DAY5), "", formattime(.w_AL2DAY5, ":") )
        endif
        .DoRTCalc(34,34,.t.)
        if .o_DA1DAY6<>.w_DA1DAY6.or. .o_DAY6<>.w_DAY6
            .w_DA1DAY6 = IIF(.w_DAY6<>'S', "", IIF(EMPTY(.w_DA1DAY6), .w_DA1STD, formattime(.w_DA1DAY6, ":") ))
        endif
        if .o_DA1DAY6<>.w_DA1DAY6.or. .o_AL1DAY6<>.w_AL1DAY6.or. .o_DAY6<>.w_DAY6
            .w_AL1DAY6 = IIF(.w_DAY6<>'S', "", IIF( EMPTY(.w_AL1DAY6), .w_AL1STD, formattime(.w_AL1DAY6, ":") ))
        endif
        if .o_DA1DAY6<>.w_DA1DAY6.or. .o_DA2DAY6<>.w_DA2DAY6.or. .o_AL1DAY6<>.w_AL1DAY6.or. .o_DAY6<>.w_DAY6
            .w_DA2DAY6 = IIF(.w_DAY6<>'S' OR EMPTY(.w_AL1DAY6) , "", formattime(.w_DA2DAY6, ":") )
        endif
        if .o_DA1DAY6<>.w_DA1DAY6.or. .o_DA2DAY6<>.w_DA2DAY6.or. .o_AL2DAY6<>.w_AL2DAY6.or. .o_AL1DAY6<>.w_AL1DAY6.or. .o_DAY6<>.w_DAY6
            .w_AL2DAY6 = IIF(.w_DAY6<>'S' OR EMPTY(.w_DA2DAY6), "", formattime(.w_AL2DAY6, ":") )
        endif
        .DoRTCalc(39,39,.t.)
        if .o_DA1DAY7<>.w_DA1DAY7.or. .o_DAY7<>.w_DAY7
            .w_DA1DAY7 = IIF(.w_DAY7<>'S', "", IIF(EMPTY(.w_DA1DAY7), .w_DA1STD, formattime(.w_DA1DAY7, ":") ))
        endif
        if .o_DA1DAY7<>.w_DA1DAY7.or. .o_AL1DAY7<>.w_AL1DAY7.or. .o_DAY7<>.w_DAY7
            .w_AL1DAY7 = IIF(.w_DAY7<>'S', "", IIF( EMPTY(.w_AL1DAY7), .w_AL1STD, formattime(.w_AL1DAY7, ":") ))
        endif
        if .o_DA1DAY7<>.w_DA1DAY7.or. .o_DA2DAY7<>.w_DA2DAY7.or. .o_AL1DAY7<>.w_AL1DAY7.or. .o_DAY7<>.w_DAY7
            .w_DA2DAY7 = IIF(.w_DAY7<>'S' OR EMPTY(.w_AL1DAY7) , "", formattime(.w_DA2DAY7, ":") )
        endif
        if .o_DA1DAY7<>.w_DA1DAY7.or. .o_DA2DAY7<>.w_DA2DAY7.or. .o_AL2DAY7<>.w_AL2DAY7.or. .o_AL1DAY7<>.w_AL1DAY7.or. .o_DAY7<>.w_DAY7
            .w_AL2DAY7 = IIF(.w_DAY7<>'S' OR EMPTY(.w_DA2DAY7), "", formattime(.w_AL2DAY7, ":") )
        endif
        .DoRTCalc(44,44,.t.)
        if .o_DA1DAY1<>.w_DA1DAY1.or. .o_DAY1<>.w_DAY1
            .w_DA1DAY1 = IIF(.w_DAY1<>'S', "", IIF(EMPTY(.w_DA1DAY1), .w_DA1STD, formattime(.w_DA1DAY1, ":") ))
        endif
        if .o_DA1DAY1<>.w_DA1DAY1.or. .o_AL1DAY1<>.w_AL1DAY1.or. .o_DAY1<>.w_DAY1
            .w_AL1DAY1 = IIF(.w_DAY1<>'S', "", IIF( EMPTY(.w_AL1DAY1), .w_AL1STD, formattime(.w_AL1DAY1, ":") ))
        endif
        if .o_DA1DAY1<>.w_DA1DAY1.or. .o_DA2DAY1<>.w_DA2DAY1.or. .o_AL1DAY1<>.w_AL1DAY1.or. .o_DAY1<>.w_DAY1
            .w_DA2DAY1 = IIF(.w_DAY1<>'S' OR EMPTY(.w_AL1DAY1) , "", formattime(.w_DA2DAY1, ":") )
        endif
        if .o_DA1DAY1<>.w_DA1DAY1.or. .o_DA2DAY1<>.w_DA2DAY1.or. .o_AL2DAY1<>.w_AL2DAY1.or. .o_AL1DAY1<>.w_AL1DAY1.or. .o_DAY1<>.w_DAY1
            .w_AL2DAY1 = IIF(.w_DAY1<>'S' OR EMPTY(.w_DA2DAY1), "", formattime(.w_AL2DAY1, ":") )
        endif
        if .o_DAY2<>.w_DAY2.or. .o_DAY3<>.w_DAY3.or. .o_DAY4<>.w_DAY4.or. .o_DAY5<>.w_DAY5.or. .o_DAY6<>.w_DAY6.or. .o_DAY7<>.w_DAY7.or. .o_DAY1<>.w_DAY1
          .Calculate_VETGRUBTZH()
        endif
        if .o_DA1DAY2<>.w_DA1DAY2.or. .o_AL1DAY2<>.w_AL1DAY2.or. .o_DA2DAY2<>.w_DA2DAY2.or. .o_AL2DAY2<>.w_AL2DAY2.or. .o_CODCAL<>.w_CODCAL
            .w_OREDAY2 = IIF(.w_NUMORE<>0,.w_NUMORE,(cp_ROUND((cp_ROUND(CTOT(.w_AL1DAY2)-CTOT(.w_DA1DAY2),0))/3600,2)+cp_ROUND((cp_ROUND(CTOT(.w_AL2DAY2)-CTOT(.w_DA2DAY2),0))/3600,2)))
        endif
        if .o_DA1DAY3<>.w_DA1DAY3.or. .o_AL1DAY3<>.w_AL1DAY3.or. .o_DA2DAY3<>.w_DA2DAY3.or. .o_AL2DAY3<>.w_AL2DAY3.or. .o_CODCAL<>.w_CODCAL
            .w_OREDAY3 = IIF(.w_NUMORE<>0,.w_NUMORE,(cp_ROUND((cp_ROUND(CTOT(.w_AL1DAY3)-CTOT(.w_DA1DAY3),0))/3600,2)+cp_ROUND((cp_ROUND(CTOT(.w_AL2DAY3)-CTOT(.w_DA2DAY3),0))/3600,2)))
        endif
        if .o_DA1DAY4<>.w_DA1DAY4.or. .o_AL1DAY4<>.w_AL1DAY4.or. .o_AL2DAY4<>.w_AL2DAY4.or. .o_DA2DAY4<>.w_DA2DAY4.or. .o_CODCAL<>.w_CODCAL
            .w_OREDAY4 = IIF(.w_NUMORE<>0,.w_NUMORE,(cp_ROUND((cp_ROUND(CTOT(.w_AL1DAY4)-CTOT(.w_DA1DAY4),0))/3600,2)+cp_ROUND((cp_ROUND(CTOT(.w_AL2DAY4)-CTOT(.w_DA2DAY4),0))/3600,2)))
        endif
        if .o_DA1DAY5<>.w_DA1DAY5.or. .o_AL1DAY5<>.w_AL1DAY5.or. .o_DA2DAY5<>.w_DA2DAY5.or. .o_AL2DAY5<>.w_AL2DAY5.or. .o_CODCAL<>.w_CODCAL
            .w_OREDAY5 = IIF(.w_NUMORE<>0,.w_NUMORE,(cp_ROUND((cp_ROUND(CTOT(.w_AL1DAY5)-CTOT(.w_DA1DAY5),0))/3600,2)+cp_ROUND((cp_ROUND(CTOT(.w_AL2DAY5)-CTOT(.w_DA2DAY5),0))/3600,2)))
        endif
        if .o_DA1DAY6<>.w_DA1DAY6.or. .o_AL1DAY6<>.w_AL1DAY6.or. .o_DA2DAY6<>.w_DA2DAY6.or. .o_AL2DAY6<>.w_AL2DAY6.or. .o_CODCAL<>.w_CODCAL
            .w_OREDAY6 = IIF(.w_NUMORE<>0,.w_NUMORE,(cp_ROUND((cp_ROUND(CTOT(.w_AL1DAY6)-CTOT(.w_DA1DAY6),0))/3600,2)+cp_ROUND((cp_ROUND(CTOT(.w_AL2DAY6)-CTOT(.w_DA2DAY6),0))/3600,2)))
        endif
        if .o_DA1DAY7<>.w_DA1DAY7.or. .o_AL1DAY7<>.w_AL1DAY7.or. .o_DA2DAY7<>.w_DA2DAY7.or. .o_AL2DAY7<>.w_AL2DAY7.or. .o_CODCAL<>.w_CODCAL
            .w_OREDAY7 = IIF(.w_NUMORE<>0,.w_NUMORE,(cp_ROUND((cp_ROUND(CTOT(.w_AL1DAY7)-CTOT(.w_DA1DAY7),0))/3600,2)+cp_ROUND((cp_ROUND(CTOT(.w_AL2DAY7)-CTOT(.w_DA2DAY7),0))/3600,2)))
        endif
        if .o_DA1DAY1<>.w_DA1DAY1.or. .o_AL1DAY1<>.w_AL1DAY1.or. .o_DA2DAY1<>.w_DA2DAY1.or. .o_AL2DAY1<>.w_AL2DAY1.or. .o_CODCAL<>.w_CODCAL
            .w_OREDAY1 = IIF(.w_NUMORE<>0,.w_NUMORE,(cp_ROUND((cp_ROUND(CTOT(.w_AL1DAY1)-CTOT(.w_DA1DAY1),0))/3600,2)+cp_ROUND((cp_ROUND(CTOT(.w_AL2DAY1)-CTOT(.w_DA2DAY1),0))/3600,2)))
        endif
        if .o_ANNO<>.w_ANNO
          .Calculate_ZDIGWWRFLZ()
        endif
        if .o_DATINI<>.w_DATINI.or. .o_DATFIN<>.w_DATFIN
          .Calculate_VBJAURIKDE()
        endif
        if .o_DA1DAY2<>.w_DA1DAY2.or. .o_AL1DAY2<>.w_AL1DAY2.or. .o_DA2DAY2<>.w_DA2DAY2.or. .o_AL2DAY2<>.w_AL2DAY2
          .Calculate_OHNZBAYEST()
        endif
        if .o_DA1DAY3<>.w_DA1DAY3.or. .o_AL1DAY3<>.w_AL1DAY3.or. .o_DA2DAY3<>.w_DA2DAY3.or. .o_AL2DAY3<>.w_AL2DAY3
          .Calculate_CYVQEYIEGQ()
        endif
        if .o_DA1DAY4<>.w_DA1DAY4.or. .o_AL1DAY4<>.w_AL1DAY4.or. .o_DA2DAY4<>.w_DA2DAY4.or. .o_AL2DAY4<>.w_AL2DAY4
          .Calculate_TYHGBFFKRM()
        endif
        if .o_DA2DAY5<>.w_DA2DAY5.or. .o_DA1DAY5<>.w_DA1DAY5.or. .o_AL2DAY5<>.w_AL2DAY5.or. .o_AL1DAY5<>.w_AL1DAY5
          .Calculate_AQNJEZOECH()
        endif
        if .o_DA1DAY6<>.w_DA1DAY6.or. .o_AL1DAY6<>.w_AL1DAY6.or. .o_DA2DAY6<>.w_DA2DAY6.or. .o_AL2DAY6<>.w_AL2DAY6
          .Calculate_ZGPMYKSFQY()
        endif
        if .o_DA1DAY7<>.w_DA1DAY7.or. .o_AL1DAY7<>.w_AL1DAY7.or. .o_DA2DAY7<>.w_DA2DAY7.or. .o_AL2DAY7<>.w_AL2DAY7
          .Calculate_DDERLVYKFB()
        endif
        if .o_DA1DAY1<>.w_DA1DAY1.or. .o_AL1DAY1<>.w_AL1DAY1.or. .o_DA2DAY1<>.w_DA2DAY1.or. .o_AL2DAY1<>.w_AL2DAY1
          .Calculate_WDKPGSZEEK()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(56,74,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_PGBTIUOPZP()
    with this
          * --- INIZIALIZZA ORARI
          .w_DA2STD = IIF(EMPTY(.w_CKDA1STD), "14:00", .w_DA2STD)
          .w_AL2STD = IIF(EMPTY(.w_CKDA1STD), "18:00", .w_AL2STD)
          .w_DA2DAY2 = .w_DA2STD
          .w_AL2DAY2 = .w_AL2STD
          .w_DA2DAY3 = .w_DA2STD
          .w_AL2DAY3 = .w_AL2STD
          .w_DA2DAY4 = .w_DA2STD
          .w_AL2DAY4 = .w_AL2STD
          .w_DA2DAY5 = .w_DA2STD
          .w_AL2DAY5 = .w_AL2STD
          .w_DA2DAY6 = .w_DA2STD
          .w_AL2DAY6 = .w_AL2STD
          .w_DA1DAY1 = .w_DA1STD
          .w_DA1DAY2 = .w_DA1STD
          .w_DA1DAY3 = .w_DA1STD
          .w_DA1DAY4 = .w_DA1STD
          .w_DA1DAY5 = .w_DA1STD
          .w_DA1DAY6 = .w_DA1STD
          .w_DA1DAY7 = .w_DA1STD
          .w_AL1DAY1 = .w_AL1STD
          .w_AL1DAY2 = .w_AL1STD
          .w_AL1DAY3 = .w_AL1STD
          .w_AL1DAY4 = .w_AL1STD
          .w_AL1DAY5 = .w_AL1STD
          .w_AL1DAY6 = .w_AL1STD
          .w_AL1DAY7 = .w_AL1STD
    endwith
  endproc
  proc Calculate_VETGRUBTZH()
    with this
          * --- REIMPOSTA ORARI SECONDA FASCIA
          .w_DA2DAY2 = IIF(.w_DAY2='S', IIF(EMPTY(.w_DA2DAY2), .w_DA2STD, .w_DA2DAY2),"")
          .w_AL2DAY2 = IIF(.w_DAY2='S', IIF(EMPTY(.w_AL2DAY2), .w_AL2STD, .w_AL2DAY2), "")
          .w_DA2DAY3 = IIF(.w_DAY3='S', IIF(EMPTY(.w_DA2DAY3), .w_DA2STD, .w_DA2DAY3),"")
          .w_AL2DAY3 = IIF(.w_DAY3='S', IIF(EMPTY(.w_AL2DAY3), .w_AL2STD, .w_AL2DAY3), "")
          .w_DA2DAY4 = IIF(.w_DAY4='S', IIF(EMPTY(.w_DA2DAY4), .w_DA2STD, .w_DA2DAY4),"")
          .w_AL2DAY4 = IIF(.w_DAY4='S', IIF(EMPTY(.w_AL2DAY4), .w_AL2STD, .w_AL2DAY4), "")
          .w_DA2DAY5 = IIF(.w_DAY5='S', IIF(EMPTY(.w_DA2DAY5), .w_DA2STD, .w_DA2DAY5),"")
          .w_AL2DAY5 = IIF(.w_DAY5='S', IIF(EMPTY(.w_AL2DAY5), .w_AL2STD, .w_AL2DAY5), "")
          .w_DA2DAY6 = IIF(.w_DAY6='S', IIF(EMPTY(.w_DA2DAY6), .w_DA2STD, .w_DA2DAY6),"")
          .w_AL2DAY6 = IIF(.w_DAY6='S', IIF(EMPTY(.w_AL2DAY6), .w_AL2STD, .w_AL2DAY6), "")
          .w_DA2DAY7 = IIF(.w_DAY7='S', IIF(EMPTY(.w_DA2DAY7), .w_DA2STD, .w_DA2DAY7),"")
          .w_AL2DAY7 = IIF(.w_DAY7='S', IIF(EMPTY(.w_AL2DAY7), .w_AL2STD, .w_AL2DAY7), "")
          .w_DA2DAY1 = IIF(.w_DAY1='S', IIF(EMPTY(.w_DA2DAY1), .w_DA2STD, .w_DA2DAY1),"")
          .w_AL2DAY1 = IIF(.w_DAY1='S', IIF(EMPTY(.w_AL2DAY1), .w_AL2STD, .w_AL2DAY1), "")
    endwith
  endproc
  proc Calculate_ZDIGWWRFLZ()
    with this
          * --- Valorizza festivit� predefinita
          GSAR_BGC(this;
              ,'FESPRED';
             )
    endwith
  endproc
  proc Calculate_VBJAURIKDE()
    with this
          * --- Azzeramento variabili
          .w_ANNO = IIF(YEAR(.w_DATINI) # YEAR(.w_DATFIN) , 0, .w_ANNO)
          .w_COD_FEST = IIF(YEAR(.w_DATINI) # YEAR(.w_DATFIN) , space(3), .w_COD_FEST)
          .w_FEDESCRI = IIF(YEAR(.w_DATINI) # YEAR(.w_DATFIN) , space(35), .w_FEDESCRI)
    endwith
  endproc
  proc Calculate_OHNZBAYEST()
    with this
          * --- Calcola ore lavorative2
          .w_OREDAY2 = IIF(.w_OREDAY2>=(23.98),24,.w_OREDAY2)
    endwith
  endproc
  proc Calculate_CYVQEYIEGQ()
    with this
          * --- Calcola ore lavorative3
          .w_OREDAY3 = IIF(.w_OREDAY3>=(23.98),24,.w_OREDAY3)
    endwith
  endproc
  proc Calculate_TYHGBFFKRM()
    with this
          * --- Calcola ore lavorative4
          .w_OREDAY4 = IIF(.w_OREDAY4>=(23.98),24,.w_OREDAY4)
    endwith
  endproc
  proc Calculate_AQNJEZOECH()
    with this
          * --- Calcola ore lavorative5
          .w_OREDAY5 = IIF(.w_OREDAY5>=(23.98),24,.w_OREDAY5)
    endwith
  endproc
  proc Calculate_ZGPMYKSFQY()
    with this
          * --- Calcola ore lavorative6
          .w_OREDAY6 = IIF(.w_OREDAY6>=(23.98),24,.w_OREDAY6)
    endwith
  endproc
  proc Calculate_DDERLVYKFB()
    with this
          * --- Calcola ore lavorative7
          .w_OREDAY7 = IIF(.w_OREDAY7>=(23.98),24,.w_OREDAY7)
    endwith
  endproc
  proc Calculate_WDKPGSZEEK()
    with this
          * --- Calcola ore lavorative1
          .w_OREDAY1 = IIF(.w_OREDAY1>=(23.98),24,.w_OREDAY1)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAL1STD_1_13.enabled = this.oPgFrm.Page1.oPag.oAL1STD_1_13.mCond()
    this.oPgFrm.Page1.oPag.oDA2STD_1_14.enabled = this.oPgFrm.Page1.oPag.oDA2STD_1_14.mCond()
    this.oPgFrm.Page1.oPag.oAL2STD_1_15.enabled = this.oPgFrm.Page1.oPag.oAL2STD_1_15.mCond()
    this.oPgFrm.Page1.oPag.oDA1DAY2_1_25.enabled = this.oPgFrm.Page1.oPag.oDA1DAY2_1_25.mCond()
    this.oPgFrm.Page1.oPag.oAL1DAY2_1_26.enabled = this.oPgFrm.Page1.oPag.oAL1DAY2_1_26.mCond()
    this.oPgFrm.Page1.oPag.oDA2DAY2_1_27.enabled = this.oPgFrm.Page1.oPag.oDA2DAY2_1_27.mCond()
    this.oPgFrm.Page1.oPag.oAL2DAY2_1_28.enabled = this.oPgFrm.Page1.oPag.oAL2DAY2_1_28.mCond()
    this.oPgFrm.Page1.oPag.oDA1DAY3_1_30.enabled = this.oPgFrm.Page1.oPag.oDA1DAY3_1_30.mCond()
    this.oPgFrm.Page1.oPag.oAL1DAY3_1_31.enabled = this.oPgFrm.Page1.oPag.oAL1DAY3_1_31.mCond()
    this.oPgFrm.Page1.oPag.oDA2DAY3_1_32.enabled = this.oPgFrm.Page1.oPag.oDA2DAY3_1_32.mCond()
    this.oPgFrm.Page1.oPag.oAL2DAY3_1_33.enabled = this.oPgFrm.Page1.oPag.oAL2DAY3_1_33.mCond()
    this.oPgFrm.Page1.oPag.oDA1DAY4_1_35.enabled = this.oPgFrm.Page1.oPag.oDA1DAY4_1_35.mCond()
    this.oPgFrm.Page1.oPag.oAL1DAY4_1_36.enabled = this.oPgFrm.Page1.oPag.oAL1DAY4_1_36.mCond()
    this.oPgFrm.Page1.oPag.oDA2DAY4_1_37.enabled = this.oPgFrm.Page1.oPag.oDA2DAY4_1_37.mCond()
    this.oPgFrm.Page1.oPag.oAL2DAY4_1_38.enabled = this.oPgFrm.Page1.oPag.oAL2DAY4_1_38.mCond()
    this.oPgFrm.Page1.oPag.oDA1DAY5_1_40.enabled = this.oPgFrm.Page1.oPag.oDA1DAY5_1_40.mCond()
    this.oPgFrm.Page1.oPag.oAL1DAY5_1_41.enabled = this.oPgFrm.Page1.oPag.oAL1DAY5_1_41.mCond()
    this.oPgFrm.Page1.oPag.oDA2DAY5_1_42.enabled = this.oPgFrm.Page1.oPag.oDA2DAY5_1_42.mCond()
    this.oPgFrm.Page1.oPag.oAL2DAY5_1_43.enabled = this.oPgFrm.Page1.oPag.oAL2DAY5_1_43.mCond()
    this.oPgFrm.Page1.oPag.oDA1DAY6_1_45.enabled = this.oPgFrm.Page1.oPag.oDA1DAY6_1_45.mCond()
    this.oPgFrm.Page1.oPag.oAL1DAY6_1_46.enabled = this.oPgFrm.Page1.oPag.oAL1DAY6_1_46.mCond()
    this.oPgFrm.Page1.oPag.oDA2DAY6_1_47.enabled = this.oPgFrm.Page1.oPag.oDA2DAY6_1_47.mCond()
    this.oPgFrm.Page1.oPag.oAL2DAY6_1_48.enabled = this.oPgFrm.Page1.oPag.oAL2DAY6_1_48.mCond()
    this.oPgFrm.Page1.oPag.oDA1DAY7_1_50.enabled = this.oPgFrm.Page1.oPag.oDA1DAY7_1_50.mCond()
    this.oPgFrm.Page1.oPag.oAL1DAY7_1_51.enabled = this.oPgFrm.Page1.oPag.oAL1DAY7_1_51.mCond()
    this.oPgFrm.Page1.oPag.oDA2DAY7_1_52.enabled = this.oPgFrm.Page1.oPag.oDA2DAY7_1_52.mCond()
    this.oPgFrm.Page1.oPag.oAL2DAY7_1_53.enabled = this.oPgFrm.Page1.oPag.oAL2DAY7_1_53.mCond()
    this.oPgFrm.Page1.oPag.oDA1DAY1_1_55.enabled = this.oPgFrm.Page1.oPag.oDA1DAY1_1_55.mCond()
    this.oPgFrm.Page1.oPag.oAL1DAY1_1_56.enabled = this.oPgFrm.Page1.oPag.oAL1DAY1_1_56.mCond()
    this.oPgFrm.Page1.oPag.oDA2DAY1_1_57.enabled = this.oPgFrm.Page1.oPag.oDA2DAY1_1_57.mCond()
    this.oPgFrm.Page1.oPag.oAL2DAY1_1_58.enabled = this.oPgFrm.Page1.oPag.oAL2DAY1_1_58.mCond()
    this.oPgFrm.Page1.oPag.oFIPER1_1_68.enabled = this.oPgFrm.Page1.oPag.oFIPER1_1_68.mCond()
    this.oPgFrm.Page1.oPag.oINPER2_1_69.enabled = this.oPgFrm.Page1.oPag.oINPER2_1_69.mCond()
    this.oPgFrm.Page1.oPag.oFIPER2_1_70.enabled = this.oPgFrm.Page1.oPag.oFIPER2_1_70.mCond()
    this.oPgFrm.Page1.oPag.oINPER3_1_71.enabled = this.oPgFrm.Page1.oPag.oINPER3_1_71.mCond()
    this.oPgFrm.Page1.oPag.oFIPER3_1_72.enabled = this.oPgFrm.Page1.oPag.oFIPER3_1_72.mCond()
    this.oPgFrm.Page1.oPag.oINPER4_1_73.enabled = this.oPgFrm.Page1.oPag.oINPER4_1_73.mCond()
    this.oPgFrm.Page1.oPag.oFIPER4_1_74.enabled = this.oPgFrm.Page1.oPag.oFIPER4_1_74.mCond()
    this.oPgFrm.Page1.oPag.oINPER5_1_75.enabled = this.oPgFrm.Page1.oPag.oINPER5_1_75.mCond()
    this.oPgFrm.Page1.oPag.oFIPER5_1_76.enabled = this.oPgFrm.Page1.oPag.oFIPER5_1_76.mCond()
    this.oPgFrm.Page1.oPag.oINPER6_1_77.enabled = this.oPgFrm.Page1.oPag.oINPER6_1_77.mCond()
    this.oPgFrm.Page1.oPag.oFIPER6_1_78.enabled = this.oPgFrm.Page1.oPag.oFIPER6_1_78.mCond()
    this.oPgFrm.Page1.oPag.oINPER7_1_79.enabled = this.oPgFrm.Page1.oPag.oINPER7_1_79.mCond()
    this.oPgFrm.Page1.oPag.oFIPER7_1_80.enabled = this.oPgFrm.Page1.oPag.oFIPER7_1_80.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_5.visible=!this.oPgFrm.Page1.oPag.oStr_1_5.mHide()
    this.oPgFrm.Page1.oPag.oCOD_FEST_1_6.visible=!this.oPgFrm.Page1.oPag.oCOD_FEST_1_6.mHide()
    this.oPgFrm.Page1.oPag.oFEDESCRI_1_7.visible=!this.oPgFrm.Page1.oPag.oFEDESCRI_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oANNO_1_9.visible=!this.oPgFrm.Page1.oPag.oANNO_1_9.mHide()
    this.oPgFrm.Page1.oPag.oGIOSO1_1_81.visible=!this.oPgFrm.Page1.oPag.oGIOSO1_1_81.mHide()
    this.oPgFrm.Page1.oPag.oMESO1_1_82.visible=!this.oPgFrm.Page1.oPag.oMESO1_1_82.mHide()
    this.oPgFrm.Page1.oPag.oGIOSO2_1_83.visible=!this.oPgFrm.Page1.oPag.oGIOSO2_1_83.mHide()
    this.oPgFrm.Page1.oPag.oMESO2_1_84.visible=!this.oPgFrm.Page1.oPag.oMESO2_1_84.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_115.visible=!this.oPgFrm.Page1.oPag.oStr_1_115.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_118.visible=!this.oPgFrm.Page1.oPag.oStr_1_118.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_119.visible=!this.oPgFrm.Page1.oPag.oStr_1_119.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_120.visible=!this.oPgFrm.Page1.oPag.oStr_1_120.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_121.visible=!this.oPgFrm.Page1.oPag.oStr_1_121.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_PGBTIUOPZP()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_ZDIGWWRFLZ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCAL
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATL',True,'TAB_CALE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CODCAL)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI,TCDA1STD,TCAL1STD,TCDA2STD,TCAL2STD,TC__DAY1,TC__DAY2,TC__DAY3,TC__DAY4,TC__DAY5,TC__DAY6,TC__DAY7,TCNUMORE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CODCAL))
          select TCCODICE,TCDESCRI,TCDA1STD,TCAL1STD,TCDA2STD,TCAL2STD,TC__DAY1,TC__DAY2,TC__DAY3,TC__DAY4,TC__DAY5,TC__DAY6,TC__DAY7,TCNUMORE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAL)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAL) and !this.bDontReportError
            deferred_cp_zoom('TAB_CALE','*','TCCODICE',cp_AbsName(oSource.parent,'oCODCAL_1_2'),i_cWhere,'GSAR_ATL',"Calendari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI,TCDA1STD,TCAL1STD,TCDA2STD,TCAL2STD,TC__DAY1,TC__DAY2,TC__DAY3,TC__DAY4,TC__DAY5,TC__DAY6,TC__DAY7,TCNUMORE";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI,TCDA1STD,TCAL1STD,TCDA2STD,TCAL2STD,TC__DAY1,TC__DAY2,TC__DAY3,TC__DAY4,TC__DAY5,TC__DAY6,TC__DAY7,TCNUMORE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI,TCDA1STD,TCAL1STD,TCDA2STD,TCAL2STD,TC__DAY1,TC__DAY2,TC__DAY3,TC__DAY4,TC__DAY5,TC__DAY6,TC__DAY7,TCNUMORE";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CODCAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CODCAL)
            select TCCODICE,TCDESCRI,TCDA1STD,TCAL1STD,TCDA2STD,TCAL2STD,TC__DAY1,TC__DAY2,TC__DAY3,TC__DAY4,TC__DAY5,TC__DAY6,TC__DAY7,TCNUMORE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAL = NVL(_Link_.TCCODICE,space(5))
      this.w_DCAL = NVL(_Link_.TCDESCRI,space(40))
      this.w_DA1STD = NVL(_Link_.TCDA1STD,space(5))
      this.w_DA1STD = NVL(_Link_.TCDA1STD,space(5))
      this.w_AL1STD = NVL(_Link_.TCAL1STD,space(5))
      this.w_DA2STD = NVL(_Link_.TCDA2STD,space(5))
      this.w_AL2STD = NVL(_Link_.TCAL2STD,space(5))
      this.w_DAY1 = NVL(_Link_.TC__DAY1,space(1))
      this.w_DAY2 = NVL(_Link_.TC__DAY2,space(1))
      this.w_DAY3 = NVL(_Link_.TC__DAY3,space(1))
      this.w_DAY4 = NVL(_Link_.TC__DAY4,space(1))
      this.w_DAY5 = NVL(_Link_.TC__DAY5,space(1))
      this.w_DAY6 = NVL(_Link_.TC__DAY6,space(1))
      this.w_DAY7 = NVL(_Link_.TC__DAY7,space(1))
      this.w_CKDA1STD = NVL(_Link_.TCDA1STD,space(10))
      this.w_NUMORE = NVL(_Link_.TCNUMORE,0)
    else
      if i_cCtrl<>'Load'
        this.w_CODCAL = space(5)
      endif
      this.w_DCAL = space(40)
      this.w_DA1STD = space(5)
      this.w_DA1STD = space(5)
      this.w_AL1STD = space(5)
      this.w_DA2STD = space(5)
      this.w_AL2STD = space(5)
      this.w_DAY1 = space(1)
      this.w_DAY2 = space(1)
      this.w_DAY3 = space(1)
      this.w_DAY4 = space(1)
      this.w_DAY5 = space(1)
      this.w_DAY6 = space(1)
      this.w_DAY7 = space(1)
      this.w_CKDA1STD = space(10)
      this.w_NUMORE = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COD_FEST
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FES_MAST_IDX,3]
    i_lTable = "FES_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2], .t., this.FES_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_FEST) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_mfe',True,'FES_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FECODICE like "+cp_ToStrODBC(trim(this.w_COD_FEST)+"%");

          i_ret=cp_SQL(i_nConn,"select FECODICE,FEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FECODICE',trim(this.w_COD_FEST))
          select FECODICE,FEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COD_FEST)==trim(_Link_.FECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COD_FEST) and !this.bDontReportError
            deferred_cp_zoom('FES_MAST','*','FECODICE',cp_AbsName(oSource.parent,'oCOD_FEST_1_6'),i_cWhere,'gsar_mfe',"Festivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FECODICE,FEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FECODICE',oSource.xKey(1))
            select FECODICE,FEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_FEST)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FECODICE,FEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FECODICE="+cp_ToStrODBC(this.w_COD_FEST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FECODICE',this.w_COD_FEST)
            select FECODICE,FEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_FEST = NVL(_Link_.FECODICE,space(3))
      this.w_FEDESCRI = NVL(_Link_.FEDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_COD_FEST = space(3)
      endif
      this.w_FEDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2])+'\'+cp_ToStr(_Link_.FECODICE,1)
      cp_ShowWarn(i_cKey,this.FES_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_FEST Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODCAL_1_2.value==this.w_CODCAL)
      this.oPgFrm.Page1.oPag.oCODCAL_1_2.value=this.w_CODCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDCAL_1_4.value==this.w_DCAL)
      this.oPgFrm.Page1.oPag.oDCAL_1_4.value=this.w_DCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOD_FEST_1_6.value==this.w_COD_FEST)
      this.oPgFrm.Page1.oPag.oCOD_FEST_1_6.value=this.w_COD_FEST
    endif
    if not(this.oPgFrm.Page1.oPag.oFEDESCRI_1_7.value==this.w_FEDESCRI)
      this.oPgFrm.Page1.oPag.oFEDESCRI_1_7.value=this.w_FEDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_9.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_9.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_10.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_10.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_11.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_11.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDA1STD_1_12.value==this.w_DA1STD)
      this.oPgFrm.Page1.oPag.oDA1STD_1_12.value=this.w_DA1STD
    endif
    if not(this.oPgFrm.Page1.oPag.oAL1STD_1_13.value==this.w_AL1STD)
      this.oPgFrm.Page1.oPag.oAL1STD_1_13.value=this.w_AL1STD
    endif
    if not(this.oPgFrm.Page1.oPag.oDA2STD_1_14.value==this.w_DA2STD)
      this.oPgFrm.Page1.oPag.oDA2STD_1_14.value=this.w_DA2STD
    endif
    if not(this.oPgFrm.Page1.oPag.oAL2STD_1_15.value==this.w_AL2STD)
      this.oPgFrm.Page1.oPag.oAL2STD_1_15.value=this.w_AL2STD
    endif
    if not(this.oPgFrm.Page1.oPag.oDAY2_1_24.RadioValue()==this.w_DAY2)
      this.oPgFrm.Page1.oPag.oDAY2_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDA1DAY2_1_25.value==this.w_DA1DAY2)
      this.oPgFrm.Page1.oPag.oDA1DAY2_1_25.value=this.w_DA1DAY2
    endif
    if not(this.oPgFrm.Page1.oPag.oAL1DAY2_1_26.value==this.w_AL1DAY2)
      this.oPgFrm.Page1.oPag.oAL1DAY2_1_26.value=this.w_AL1DAY2
    endif
    if not(this.oPgFrm.Page1.oPag.oDA2DAY2_1_27.value==this.w_DA2DAY2)
      this.oPgFrm.Page1.oPag.oDA2DAY2_1_27.value=this.w_DA2DAY2
    endif
    if not(this.oPgFrm.Page1.oPag.oAL2DAY2_1_28.value==this.w_AL2DAY2)
      this.oPgFrm.Page1.oPag.oAL2DAY2_1_28.value=this.w_AL2DAY2
    endif
    if not(this.oPgFrm.Page1.oPag.oDAY3_1_29.RadioValue()==this.w_DAY3)
      this.oPgFrm.Page1.oPag.oDAY3_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDA1DAY3_1_30.value==this.w_DA1DAY3)
      this.oPgFrm.Page1.oPag.oDA1DAY3_1_30.value=this.w_DA1DAY3
    endif
    if not(this.oPgFrm.Page1.oPag.oAL1DAY3_1_31.value==this.w_AL1DAY3)
      this.oPgFrm.Page1.oPag.oAL1DAY3_1_31.value=this.w_AL1DAY3
    endif
    if not(this.oPgFrm.Page1.oPag.oDA2DAY3_1_32.value==this.w_DA2DAY3)
      this.oPgFrm.Page1.oPag.oDA2DAY3_1_32.value=this.w_DA2DAY3
    endif
    if not(this.oPgFrm.Page1.oPag.oAL2DAY3_1_33.value==this.w_AL2DAY3)
      this.oPgFrm.Page1.oPag.oAL2DAY3_1_33.value=this.w_AL2DAY3
    endif
    if not(this.oPgFrm.Page1.oPag.oDAY4_1_34.RadioValue()==this.w_DAY4)
      this.oPgFrm.Page1.oPag.oDAY4_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDA1DAY4_1_35.value==this.w_DA1DAY4)
      this.oPgFrm.Page1.oPag.oDA1DAY4_1_35.value=this.w_DA1DAY4
    endif
    if not(this.oPgFrm.Page1.oPag.oAL1DAY4_1_36.value==this.w_AL1DAY4)
      this.oPgFrm.Page1.oPag.oAL1DAY4_1_36.value=this.w_AL1DAY4
    endif
    if not(this.oPgFrm.Page1.oPag.oDA2DAY4_1_37.value==this.w_DA2DAY4)
      this.oPgFrm.Page1.oPag.oDA2DAY4_1_37.value=this.w_DA2DAY4
    endif
    if not(this.oPgFrm.Page1.oPag.oAL2DAY4_1_38.value==this.w_AL2DAY4)
      this.oPgFrm.Page1.oPag.oAL2DAY4_1_38.value=this.w_AL2DAY4
    endif
    if not(this.oPgFrm.Page1.oPag.oDAY5_1_39.RadioValue()==this.w_DAY5)
      this.oPgFrm.Page1.oPag.oDAY5_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDA1DAY5_1_40.value==this.w_DA1DAY5)
      this.oPgFrm.Page1.oPag.oDA1DAY5_1_40.value=this.w_DA1DAY5
    endif
    if not(this.oPgFrm.Page1.oPag.oAL1DAY5_1_41.value==this.w_AL1DAY5)
      this.oPgFrm.Page1.oPag.oAL1DAY5_1_41.value=this.w_AL1DAY5
    endif
    if not(this.oPgFrm.Page1.oPag.oDA2DAY5_1_42.value==this.w_DA2DAY5)
      this.oPgFrm.Page1.oPag.oDA2DAY5_1_42.value=this.w_DA2DAY5
    endif
    if not(this.oPgFrm.Page1.oPag.oAL2DAY5_1_43.value==this.w_AL2DAY5)
      this.oPgFrm.Page1.oPag.oAL2DAY5_1_43.value=this.w_AL2DAY5
    endif
    if not(this.oPgFrm.Page1.oPag.oDAY6_1_44.RadioValue()==this.w_DAY6)
      this.oPgFrm.Page1.oPag.oDAY6_1_44.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDA1DAY6_1_45.value==this.w_DA1DAY6)
      this.oPgFrm.Page1.oPag.oDA1DAY6_1_45.value=this.w_DA1DAY6
    endif
    if not(this.oPgFrm.Page1.oPag.oAL1DAY6_1_46.value==this.w_AL1DAY6)
      this.oPgFrm.Page1.oPag.oAL1DAY6_1_46.value=this.w_AL1DAY6
    endif
    if not(this.oPgFrm.Page1.oPag.oDA2DAY6_1_47.value==this.w_DA2DAY6)
      this.oPgFrm.Page1.oPag.oDA2DAY6_1_47.value=this.w_DA2DAY6
    endif
    if not(this.oPgFrm.Page1.oPag.oAL2DAY6_1_48.value==this.w_AL2DAY6)
      this.oPgFrm.Page1.oPag.oAL2DAY6_1_48.value=this.w_AL2DAY6
    endif
    if not(this.oPgFrm.Page1.oPag.oDAY7_1_49.RadioValue()==this.w_DAY7)
      this.oPgFrm.Page1.oPag.oDAY7_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDA1DAY7_1_50.value==this.w_DA1DAY7)
      this.oPgFrm.Page1.oPag.oDA1DAY7_1_50.value=this.w_DA1DAY7
    endif
    if not(this.oPgFrm.Page1.oPag.oAL1DAY7_1_51.value==this.w_AL1DAY7)
      this.oPgFrm.Page1.oPag.oAL1DAY7_1_51.value=this.w_AL1DAY7
    endif
    if not(this.oPgFrm.Page1.oPag.oDA2DAY7_1_52.value==this.w_DA2DAY7)
      this.oPgFrm.Page1.oPag.oDA2DAY7_1_52.value=this.w_DA2DAY7
    endif
    if not(this.oPgFrm.Page1.oPag.oAL2DAY7_1_53.value==this.w_AL2DAY7)
      this.oPgFrm.Page1.oPag.oAL2DAY7_1_53.value=this.w_AL2DAY7
    endif
    if not(this.oPgFrm.Page1.oPag.oDAY1_1_54.RadioValue()==this.w_DAY1)
      this.oPgFrm.Page1.oPag.oDAY1_1_54.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDA1DAY1_1_55.value==this.w_DA1DAY1)
      this.oPgFrm.Page1.oPag.oDA1DAY1_1_55.value=this.w_DA1DAY1
    endif
    if not(this.oPgFrm.Page1.oPag.oAL1DAY1_1_56.value==this.w_AL1DAY1)
      this.oPgFrm.Page1.oPag.oAL1DAY1_1_56.value=this.w_AL1DAY1
    endif
    if not(this.oPgFrm.Page1.oPag.oDA2DAY1_1_57.value==this.w_DA2DAY1)
      this.oPgFrm.Page1.oPag.oDA2DAY1_1_57.value=this.w_DA2DAY1
    endif
    if not(this.oPgFrm.Page1.oPag.oAL2DAY1_1_58.value==this.w_AL2DAY1)
      this.oPgFrm.Page1.oPag.oAL2DAY1_1_58.value=this.w_AL2DAY1
    endif
    if not(this.oPgFrm.Page1.oPag.oOREDAY2_1_60.value==this.w_OREDAY2)
      this.oPgFrm.Page1.oPag.oOREDAY2_1_60.value=this.w_OREDAY2
    endif
    if not(this.oPgFrm.Page1.oPag.oOREDAY3_1_61.value==this.w_OREDAY3)
      this.oPgFrm.Page1.oPag.oOREDAY3_1_61.value=this.w_OREDAY3
    endif
    if not(this.oPgFrm.Page1.oPag.oOREDAY4_1_62.value==this.w_OREDAY4)
      this.oPgFrm.Page1.oPag.oOREDAY4_1_62.value=this.w_OREDAY4
    endif
    if not(this.oPgFrm.Page1.oPag.oOREDAY5_1_63.value==this.w_OREDAY5)
      this.oPgFrm.Page1.oPag.oOREDAY5_1_63.value=this.w_OREDAY5
    endif
    if not(this.oPgFrm.Page1.oPag.oOREDAY6_1_64.value==this.w_OREDAY6)
      this.oPgFrm.Page1.oPag.oOREDAY6_1_64.value=this.w_OREDAY6
    endif
    if not(this.oPgFrm.Page1.oPag.oOREDAY7_1_65.value==this.w_OREDAY7)
      this.oPgFrm.Page1.oPag.oOREDAY7_1_65.value=this.w_OREDAY7
    endif
    if not(this.oPgFrm.Page1.oPag.oOREDAY1_1_66.value==this.w_OREDAY1)
      this.oPgFrm.Page1.oPag.oOREDAY1_1_66.value=this.w_OREDAY1
    endif
    if not(this.oPgFrm.Page1.oPag.oINPER1_1_67.value==this.w_INPER1)
      this.oPgFrm.Page1.oPag.oINPER1_1_67.value=this.w_INPER1
    endif
    if not(this.oPgFrm.Page1.oPag.oFIPER1_1_68.value==this.w_FIPER1)
      this.oPgFrm.Page1.oPag.oFIPER1_1_68.value=this.w_FIPER1
    endif
    if not(this.oPgFrm.Page1.oPag.oINPER2_1_69.value==this.w_INPER2)
      this.oPgFrm.Page1.oPag.oINPER2_1_69.value=this.w_INPER2
    endif
    if not(this.oPgFrm.Page1.oPag.oFIPER2_1_70.value==this.w_FIPER2)
      this.oPgFrm.Page1.oPag.oFIPER2_1_70.value=this.w_FIPER2
    endif
    if not(this.oPgFrm.Page1.oPag.oINPER3_1_71.value==this.w_INPER3)
      this.oPgFrm.Page1.oPag.oINPER3_1_71.value=this.w_INPER3
    endif
    if not(this.oPgFrm.Page1.oPag.oFIPER3_1_72.value==this.w_FIPER3)
      this.oPgFrm.Page1.oPag.oFIPER3_1_72.value=this.w_FIPER3
    endif
    if not(this.oPgFrm.Page1.oPag.oINPER4_1_73.value==this.w_INPER4)
      this.oPgFrm.Page1.oPag.oINPER4_1_73.value=this.w_INPER4
    endif
    if not(this.oPgFrm.Page1.oPag.oFIPER4_1_74.value==this.w_FIPER4)
      this.oPgFrm.Page1.oPag.oFIPER4_1_74.value=this.w_FIPER4
    endif
    if not(this.oPgFrm.Page1.oPag.oINPER5_1_75.value==this.w_INPER5)
      this.oPgFrm.Page1.oPag.oINPER5_1_75.value=this.w_INPER5
    endif
    if not(this.oPgFrm.Page1.oPag.oFIPER5_1_76.value==this.w_FIPER5)
      this.oPgFrm.Page1.oPag.oFIPER5_1_76.value=this.w_FIPER5
    endif
    if not(this.oPgFrm.Page1.oPag.oINPER6_1_77.value==this.w_INPER6)
      this.oPgFrm.Page1.oPag.oINPER6_1_77.value=this.w_INPER6
    endif
    if not(this.oPgFrm.Page1.oPag.oFIPER6_1_78.value==this.w_FIPER6)
      this.oPgFrm.Page1.oPag.oFIPER6_1_78.value=this.w_FIPER6
    endif
    if not(this.oPgFrm.Page1.oPag.oINPER7_1_79.value==this.w_INPER7)
      this.oPgFrm.Page1.oPag.oINPER7_1_79.value=this.w_INPER7
    endif
    if not(this.oPgFrm.Page1.oPag.oFIPER7_1_80.value==this.w_FIPER7)
      this.oPgFrm.Page1.oPag.oFIPER7_1_80.value=this.w_FIPER7
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOSO1_1_81.value==this.w_GIOSO1)
      this.oPgFrm.Page1.oPag.oGIOSO1_1_81.value=this.w_GIOSO1
    endif
    if not(this.oPgFrm.Page1.oPag.oMESO1_1_82.value==this.w_MESO1)
      this.oPgFrm.Page1.oPag.oMESO1_1_82.value=this.w_MESO1
    endif
    if not(this.oPgFrm.Page1.oPag.oGIOSO2_1_83.value==this.w_GIOSO2)
      this.oPgFrm.Page1.oPag.oGIOSO2_1_83.value=this.w_GIOSO2
    endif
    if not(this.oPgFrm.Page1.oPag.oMESO2_1_84.value==this.w_MESO2)
      this.oPgFrm.Page1.oPag.oMESO2_1_84.value=this.w_MESO2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCHIPE_1_110.value==this.w_DESCHIPE)
      this.oPgFrm.Page1.oPag.oDESCHIPE_1_110.value=this.w_DESCHIPE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_ANNO>1900)  and not(YEAR(.w_DATINI) # YEAR(.w_DATFIN) AND !EMPTY(.w_DATINI) AND !EMPTY(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATFIN) OR .w_DATINI<=.w_DATFIN) )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di generazione iniziale deve essere minore o uguale alla data di generazione finale")
          case   not((EMPTY(.w_DATINI) OR .w_DATINI<=.w_DATFIN) )
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di generazione finale deve essere maggiore o uguale alla data di generazione iniziale")
          case   ((empty(.w_DA1STD)) or not(chktimewnd(1, .w_DA1STD, .w_AL1STD, .w_DA2STD, .w_AL2STD)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA1STD_1_12.SetFocus()
            i_bnoObbl = !empty(.w_DA1STD)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido oppure orario iniziale maggiore dell'orario finale")
          case   ((empty(.w_AL1STD)) or not(chktimewnd(2, .w_DA1STD, .w_AL1STD, .w_DA2STD, .w_AL2STD)))  and (!EMPTY(.w_DA1STD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL1STD_1_13.SetFocus()
            i_bnoObbl = !empty(.w_AL1STD)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido o orario finale minore dell'orario iniziale")
          case   not(chktimewnd(3, .w_DA1STD, .w_AL1STD, .w_DA2STD, .w_AL2STD))  and (!EMPTY(.w_DA1STD) AND !EMPTY(.w_AL1STD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA2STD_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale")
          case   not(chktimewnd(4, .w_DA1STD, .w_AL1STD, .w_DA2STD, .w_AL2STD))  and (!EMPTY(.w_DA2STD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL2STD_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido o orario finale minore dell'orario iniziale")
          case   ((empty(.w_DA1DAY2)) or not(chktimewnd(1, .w_DA1DAY2, .w_AL1DAY2, .w_DA2DAY2, .w_AL2DAY2)))  and (.w_DAY2="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA1DAY2_1_25.SetFocus()
            i_bnoObbl = !empty(.w_DA1DAY2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AL1DAY2)) or not(chktimewnd(2, .w_DA1DAY2, .w_AL1DAY2, .w_DA2DAY2, .w_AL2DAY2)))  and (.w_DAY2="S" AND !EMPTY(.w_DA1DAY2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL1DAY2_1_26.SetFocus()
            i_bnoObbl = !empty(.w_AL1DAY2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(chktimewnd(3, .w_DA1DAY2, .w_AL1DAY2, .w_DA2DAY2, .w_AL2DAY2))  and (.w_DAY2="S" AND !EMPTY(.w_DA1DAY2) AND !EMPTY(.w_AL1DAY2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA2DAY2_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale")
          case   not(chktimewnd(4, .w_DA1DAY2, .w_AL1DAY2, .w_DA2DAY2, .w_AL2DAY2))  and (.w_DAY2="S" AND !EMPTY(.w_DA2DAY2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL2DAY2_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DA1DAY3)) or not(chktimewnd(1, .w_DA1DAY3, .w_AL1DAY3, .w_DA2DAY3, .w_AL2DAY3)))  and (.w_DAY3="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA1DAY3_1_30.SetFocus()
            i_bnoObbl = !empty(.w_DA1DAY3)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AL1DAY3)) or not(chktimewnd(2, .w_DA1DAY3, .w_AL1DAY3, .w_DA2DAY3, .w_AL2DAY3)))  and (.w_DAY3="S" AND !EMPTY(.w_DA1DAY3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL1DAY3_1_31.SetFocus()
            i_bnoObbl = !empty(.w_AL1DAY3)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(chktimewnd(3, .w_DA1DAY3, .w_AL1DAY3, .w_DA2DAY3, .w_AL2DAY3))  and (.w_DAY3="S" AND !EMPTY(.w_DA1DAY3) AND !EMPTY(.w_AL1DAY3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA2DAY3_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale")
          case   not(chktimewnd(4, .w_DA1DAY3, .w_AL1DAY3, .w_DA2DAY3, .w_AL2DAY3))  and (.w_DAY3="S" AND !EMPTY(.w_DA2DAY3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL2DAY3_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DA1DAY4)) or not(chktimewnd(1, .w_DA1DAY4, .w_AL1DAY4, .w_DA2DAY4, .w_AL2DAY4)))  and (.w_DAY4="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA1DAY4_1_35.SetFocus()
            i_bnoObbl = !empty(.w_DA1DAY4)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AL1DAY4)) or not(chktimewnd(2, .w_DA1DAY4, .w_AL1DAY4, .w_DA2DAY4, .w_AL2DAY4)))  and (.w_DAY4="S" AND !EMPTY(.w_DA1DAY4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL1DAY4_1_36.SetFocus()
            i_bnoObbl = !empty(.w_AL1DAY4)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(chktimewnd(3, .w_DA1DAY4, .w_AL1DAY4, .w_DA2DAY4, .w_AL2DAY4))  and (.w_DAY4="S" AND !EMPTY(.w_DA1DAY4) AND !EMPTY(.w_AL1DAY4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA2DAY4_1_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale")
          case   not(chktimewnd(4, .w_DA1DAY4, .w_AL1DAY4, .w_DA2DAY4, .w_AL2DAY4))  and (.w_DAY4="S" AND !EMPTY(.w_DA2DAY4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL2DAY4_1_38.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DA1DAY5)) or not(chktimewnd(1, .w_DA1DAY5, .w_AL1DAY5, .w_DA2DAY5, .w_AL2DAY5)))  and (.w_DAY5="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA1DAY5_1_40.SetFocus()
            i_bnoObbl = !empty(.w_DA1DAY5)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AL1DAY5)) or not(chktimewnd(2, .w_DA1DAY5, .w_AL1DAY5, .w_DA2DAY5, .w_AL2DAY5)))  and (.w_DAY5="S" AND !EMPTY(.w_DA1DAY5))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL1DAY5_1_41.SetFocus()
            i_bnoObbl = !empty(.w_AL1DAY5)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(chktimewnd(3, .w_DA1DAY5, .w_AL1DAY5, .w_DA2DAY5, .w_AL2DAY5))  and (.w_DAY5="S" AND !EMPTY(.w_DA1DAY5) AND !EMPTY(.w_AL1DAY5))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA2DAY5_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale")
          case   not(chktimewnd(4, .w_DA1DAY5, .w_AL1DAY5, .w_DA2DAY5, .w_AL2DAY5))  and (.w_DAY5="S" AND !EMPTY(.w_DA2DAY5))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL2DAY5_1_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DA1DAY6)) or not(chktimewnd(1, .w_DA1DAY6, .w_AL1DAY6, .w_DA2DAY6, .w_AL2DAY6)))  and (.w_DAY6="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA1DAY6_1_45.SetFocus()
            i_bnoObbl = !empty(.w_DA1DAY6)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AL1DAY6)) or not(chktimewnd(2, .w_DA1DAY6, .w_AL1DAY6, .w_DA2DAY6, .w_AL2DAY6)))  and (.w_DAY6="S" AND !EMPTY(.w_DA1DAY6))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL1DAY6_1_46.SetFocus()
            i_bnoObbl = !empty(.w_AL1DAY6)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(chktimewnd(3, .w_DA1DAY6, .w_AL1DAY6, .w_DA2DAY6, .w_AL2DAY6))  and (.w_DAY6="S" AND !EMPTY(.w_DA1DAY6) AND !EMPTY(.w_AL1DAY6))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA2DAY6_1_47.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale")
          case   not(chktimewnd(4, .w_DA1DAY6, .w_AL1DAY6, .w_DA2DAY6, .w_AL2DAY6))  and (.w_DAY6="S" AND !EMPTY(.w_DA2DAY6))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL2DAY6_1_48.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DA1DAY7)) or not(chktimewnd(1, .w_DA1DAY7, .w_AL1DAY7, .w_DA2DAY7, .w_AL2DAY7)))  and (.w_DAY7="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA1DAY7_1_50.SetFocus()
            i_bnoObbl = !empty(.w_DA1DAY7)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AL1DAY7)) or not(chktimewnd(2, .w_DA1DAY7, .w_AL1DAY7, .w_DA2DAY7, .w_AL2DAY7)))  and (.w_DAY7="S" AND !EMPTY(.w_DA1DAY7))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL1DAY7_1_51.SetFocus()
            i_bnoObbl = !empty(.w_AL1DAY7)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(chktimewnd(3, .w_DA1DAY7, .w_AL1DAY7, .w_DA2DAY7, .w_AL2DAY7))  and (.w_DAY7="S" AND !EMPTY(.w_DA1DAY7) AND !EMPTY(.w_AL1DAY7))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA2DAY7_1_52.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale")
          case   not(chktimewnd(4, .w_DA1DAY7, .w_AL1DAY7, .w_DA2DAY7, .w_AL2DAY7))  and (.w_DAY7="S" AND !EMPTY(.w_DA2DAY7))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL2DAY7_1_53.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DA1DAY1)) or not(chktimewnd(1, .w_DA1DAY1, .w_AL1DAY1, .w_DA2DAY1, .w_AL2DAY1)))  and (.w_DAY1="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA1DAY1_1_55.SetFocus()
            i_bnoObbl = !empty(.w_DA1DAY1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AL1DAY1)) or not(chktimewnd(2, .w_DA1DAY1, .w_AL1DAY1, .w_DA2DAY1, .w_AL2DAY1)))  and (.w_DAY1="S" AND !EMPTY(.w_DA1DAY1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL1DAY1_1_56.SetFocus()
            i_bnoObbl = !empty(.w_AL1DAY1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(chktimewnd(3, .w_DA1DAY1, .w_AL1DAY1, .w_DA2DAY1, .w_AL2DAY1))  and (.w_DAY1="S" AND !EMPTY(.w_DA1DAY1) AND !EMPTY(.w_AL1DAY1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDA2DAY1_1_57.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale")
          case   not(chktimewnd(4, .w_DA1DAY1, .w_AL1DAY1, .w_DA2DAY1, .w_AL2DAY1))  and (.w_DAY1="S" AND !EMPTY(.w_DA2DAY1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAL2DAY1_1_58.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_OREDAY2>=0 AND .w_OREDAY2<=24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREDAY2_1_60.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(0<=.w_OREDAY3 AND .w_OREDAY3<=24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREDAY3_1_61.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(0<=.w_OREDAY4 AND .w_OREDAY4<=24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREDAY4_1_62.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(0<=.w_OREDAY5 AND .w_OREDAY5<=24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREDAY5_1_63.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(0<=.w_OREDAY6 AND .w_OREDAY6<=24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREDAY6_1_64.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(0<=.w_OREDAY7 AND .w_OREDAY7<=24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREDAY7_1_65.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(0<=.w_OREDAY1 AND .w_OREDAY1<=24)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOREDAY1_1_66.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_INPER1) or (.w_INPER1>=.w_DATINI and .w_INPER1<=.w_DATFIN and (empty(.w_FIPER1) or .w_INPER1<=.w_FIPER1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINPER1_1_67.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio periodo di chiusura deve essere minore o uguale alla data di fine periodo di chiusura ed appartenere all'intervallo di date specificato")
          case   not(empty(.w_FIPER1) or (.w_FIPER1>=.w_DATINI and .w_FIPER1<=.w_DATFIN and .w_INPER1<=.w_FIPER1))  and (!empty(.w_INPER1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFIPER1_1_68.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di periodo finale deve essere maggiore o uguale alla data di periodo iniziale ed appartenere all'intervallo di date specificato")
          case   not(empty(.w_INPER2) or (.w_INPER2>=.w_DATINI and .w_INPER2<=.w_DATFIN and (empty(.w_FIPER2) or .w_INPER2<=.w_FIPER2)))  and (!empty(.w_FIPER1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINPER2_1_69.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio periodo di chiusura deve essere minore o uguale alla data di fine periodo di chiusura ed appartenere all'intervallo di date specificato")
          case   not(empty(.w_FIPER2) or (.w_FIPER2>=.w_DATINI and .w_FIPER2<=.w_DATFIN and .w_INPER2<=.w_FIPER2))  and (!empty(.w_INPER2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFIPER2_1_70.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di periodo finale deve essere maggiore o uguale alla data di periodo iniziale ed appartenere all'intervallo di date specificato")
          case   not(empty(.w_INPER3) or (.w_INPER3>=.w_DATINI and .w_INPER3<=.w_DATFIN and (empty(.w_FIPER3) or .w_INPER3<=.w_FIPER3)))  and (!empty(.w_FIPER2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINPER3_1_71.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio periodo di chiusura deve essere minore o uguale alla data di fine periodo di chiusura ed appartenere all'intervallo di date specificato")
          case   not(empty(.w_FIPER3) or (.w_FIPER3>=.w_DATINI and .w_FIPER3<=.w_DATFIN and .w_INPER3<=.w_FIPER3))  and (!empty(.w_INPER3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFIPER3_1_72.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di periodo finale deve essere maggiore o uguale alla data di periodo iniziale ed appartenere all'intervallo di date specificato")
          case   not(empty(.w_INPER4) or (.w_INPER4>=.w_DATINI and .w_INPER4<=.w_DATFIN and (empty(.w_FIPER4) or .w_INPER4<=.w_FIPER4)))  and (!empty(.w_FIPER3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINPER4_1_73.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio periodo di chiusura deve essere minore o uguale alla data di fine periodo di chiusura ed appartenere all'intervallo di date specificato")
          case   not(empty(.w_FIPER4) or (.w_FIPER4>=.w_DATINI and .w_FIPER4<=.w_DATFIN and .w_INPER4<=.w_FIPER4))  and (!empty(.w_INPER4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFIPER4_1_74.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di periodo finale deve essere maggiore o uguale alla data di periodo iniziale ed appartenere all'intervallo di date specificato")
          case   not(empty(.w_INPER5) or (.w_INPER5>=.w_DATINI and .w_INPER5<=.w_DATFIN and (empty(.w_FIPER5) or .w_INPER5<=.w_FIPER5)))  and (!empty(.w_FIPER4))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINPER5_1_75.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio periodo di chiusura deve essere minore o uguale alla data di fine periodo di chiusura ed appartenere all'intervallo di date specificato")
          case   not(empty(.w_FIPER5) or (.w_FIPER5>=.w_DATINI and .w_FIPER5<=.w_DATFIN and .w_INPER5<=.w_FIPER5))  and (!empty(.w_INPER5))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFIPER5_1_76.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di periodo finale deve essere maggiore o uguale alla data di periodo iniziale ed appartenere all'intervallo di date specificato")
          case   not(empty(.w_INPER5) or (.w_INPER6>=.w_DATINI and .w_INPER6<=.w_DATFIN and (empty(.w_FIPER6) or .w_INPER6<=.w_FIPER6)))  and (!empty(.w_FIPER5))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINPER6_1_77.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio periodo di chiusura deve essere minore o uguale alla data di fine periodo di chiusura ed appartenere all'intervallo di date specificato")
          case   not(empty(.w_FIPER6) or (.w_FIPER6>=.w_DATINI and .w_FIPER6<=.w_DATFIN and .w_INPER6<=.w_FIPER6))  and (!empty(.w_INPER6))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFIPER6_1_78.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di periodo finale deve essere maggiore o uguale alla data di periodo iniziale ed appartenere all'intervallo di date specificato")
          case   not(empty(.w_INPER7) or (.w_INPER7>=.w_DATINI and .w_INPER7<=.w_DATFIN and (empty(.w_FIPER7) or .w_INPER7<=.w_FIPER7)))  and (!empty(.w_FIPER6))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oINPER7_1_79.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio periodo di chiusura deve essere minore o uguale alla data di fine periodo di chiusura ed appartenere all'intervallo di date specificato")
          case   not(empty(.w_FIPER7) or (.w_FIPER7>=.w_DATINI and .w_FIPER7<=.w_DATFIN and .w_INPER7<=.w_FIPER7))  and (!empty(.w_INPER7))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFIPER7_1_80.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di periodo finale deve essere maggiore o uguale alla data di periodo iniziale ed appartenere all'intervallo di date specificato")
          case   not(VAL(.w_GIOSO1)>0 AND VAL(.w_GIOSO1)<=31)  and not(Not Isalt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGIOSO1_1_81.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_MESO1)>0 AND VAL(.w_MESO1)<=12)  and not(Not Isalt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMESO1_1_82.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_GIOSO2)>0 AND VAL(.w_GIOSO2)<=31)  and not(Not Isalt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGIOSO2_1_83.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(VAL(.w_MESO2)>0 AND VAL(.w_MESO2)<=12)  and not(Not Isalt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMESO2_1_84.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODCAL = this.w_CODCAL
    this.o_ANNO = this.w_ANNO
    this.o_DATINI = this.w_DATINI
    this.o_DATFIN = this.w_DATFIN
    this.o_DA1STD = this.w_DA1STD
    this.o_AL1STD = this.w_AL1STD
    this.o_DA2STD = this.w_DA2STD
    this.o_AL2STD = this.w_AL2STD
    this.o_DAY2 = this.w_DAY2
    this.o_DA1DAY2 = this.w_DA1DAY2
    this.o_AL1DAY2 = this.w_AL1DAY2
    this.o_DA2DAY2 = this.w_DA2DAY2
    this.o_AL2DAY2 = this.w_AL2DAY2
    this.o_DAY3 = this.w_DAY3
    this.o_DA1DAY3 = this.w_DA1DAY3
    this.o_AL1DAY3 = this.w_AL1DAY3
    this.o_DA2DAY3 = this.w_DA2DAY3
    this.o_AL2DAY3 = this.w_AL2DAY3
    this.o_DAY4 = this.w_DAY4
    this.o_DA1DAY4 = this.w_DA1DAY4
    this.o_AL1DAY4 = this.w_AL1DAY4
    this.o_DA2DAY4 = this.w_DA2DAY4
    this.o_AL2DAY4 = this.w_AL2DAY4
    this.o_DAY5 = this.w_DAY5
    this.o_DA1DAY5 = this.w_DA1DAY5
    this.o_AL1DAY5 = this.w_AL1DAY5
    this.o_DA2DAY5 = this.w_DA2DAY5
    this.o_AL2DAY5 = this.w_AL2DAY5
    this.o_DAY6 = this.w_DAY6
    this.o_DA1DAY6 = this.w_DA1DAY6
    this.o_AL1DAY6 = this.w_AL1DAY6
    this.o_DA2DAY6 = this.w_DA2DAY6
    this.o_AL2DAY6 = this.w_AL2DAY6
    this.o_DAY7 = this.w_DAY7
    this.o_DA1DAY7 = this.w_DA1DAY7
    this.o_AL1DAY7 = this.w_AL1DAY7
    this.o_DA2DAY7 = this.w_DA2DAY7
    this.o_AL2DAY7 = this.w_AL2DAY7
    this.o_DAY1 = this.w_DAY1
    this.o_DA1DAY1 = this.w_DA1DAY1
    this.o_AL1DAY1 = this.w_AL1DAY1
    this.o_DA2DAY1 = this.w_DA2DAY1
    this.o_AL2DAY1 = this.w_AL2DAY1
    return

enddefine

* --- Define pages as container
define class tgsar_kgcPag1 as StdContainer
  Width  = 697
  height = 421
  stdWidth  = 697
  stdheight = 421
  resizeXpos=435
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCAL_1_2 as StdField with uid="ZVBWYSFXCH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODCAL", cQueryName = "CODCAL",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice calendario di riferimento",;
    HelpContextID = 26338266,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=92, Top=20, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TAB_CALE", cZoomOnZoom="GSAR_ATL", oKey_1_1="TCCODICE", oKey_1_2="this.w_CODCAL"

  func oCODCAL_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAL_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAL_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_CALE','*','TCCODICE',cp_AbsName(this.parent,'oCODCAL_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATL',"Calendari",'',this.parent.oContained
  endproc
  proc oCODCAL_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_CODCAL
     i_obj.ecpSave()
  endproc

  add object oDCAL_1_4 as StdField with uid="JTELLQNSEZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DCAL", cQueryName = "DCAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 26812362,;
   bGlobalFont=.t.,;
    Height=21, Width=305, Left=161, Top=20, InputMask=replicate('X',40)

  add object oCOD_FEST_1_6 as StdField with uid="AWIYGBPQRA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_COD_FEST", cQueryName = "COD_FEST",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice calendario festivit� da utilizzare durante la generazione del calendario",;
    HelpContextID = 131734650,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=92, Top=44, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="FES_MAST", cZoomOnZoom="gsar_mfe", oKey_1_1="FECODICE", oKey_1_2="this.w_COD_FEST"

  func oCOD_FEST_1_6.mHide()
    with this.Parent.oContained
      return (YEAR(.w_DATINI) # YEAR(.w_DATFIN) AND !EMPTY(.w_DATINI) AND !EMPTY(.w_DATFIN))
    endwith
  endfunc

  func oCOD_FEST_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOD_FEST_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOD_FEST_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FES_MAST','*','FECODICE',cp_AbsName(this.parent,'oCOD_FEST_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_mfe',"Festivit�",'',this.parent.oContained
  endproc
  proc oCOD_FEST_1_6.mZoomOnZoom
    local i_obj
    i_obj=gsar_mfe()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FECODICE=this.parent.oContained.w_COD_FEST
     i_obj.ecpSave()
  endproc

  add object oFEDESCRI_1_7 as StdField with uid="QVBMPGPEIE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_FEDESCRI", cQueryName = "FEDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 110105247,;
   bGlobalFont=.t.,;
    Height=21, Width=305, Left=161, Top=44, InputMask=replicate('X',35)

  func oFEDESCRI_1_7.mHide()
    with this.Parent.oContained
      return (YEAR(.w_DATINI) # YEAR(.w_DATFIN) AND !EMPTY(.w_DATINI) AND !EMPTY(.w_DATFIN))
    endwith
  endfunc

  add object oANNO_1_9 as StdField with uid="FFZDYIEWMA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno solare per il quale si vuole generare il calendario",;
    HelpContextID = 26559738,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=92, Top=68, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"'

  func oANNO_1_9.mHide()
    with this.Parent.oContained
      return (YEAR(.w_DATINI) # YEAR(.w_DATFIN) AND !EMPTY(.w_DATINI) AND !EMPTY(.w_DATFIN))
    endwith
  endfunc

  func oANNO_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANNO>1900)
    endwith
    return bRes
  endfunc

  add object oDATINI_1_10 as StdField with uid="CHCFDHMZAS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di generazione iniziale deve essere minore o uguale alla data di generazione finale",;
    ToolTipText = "Data iniziale generazione calendario",;
    HelpContextID = 62583242,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=219, Top=68

  func oDATINI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((EMPTY(.w_DATFIN) OR .w_DATINI<=.w_DATFIN) )
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_11 as StdField with uid="EHWIGOHFHK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di generazione finale deve essere maggiore o uguale alla data di generazione iniziale",;
    ToolTipText = "Data finale generazione calendario",;
    HelpContextID = 252572106,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=376, Top=69

  func oDATFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((EMPTY(.w_DATINI) OR .w_DATINI<=.w_DATFIN) )
    endwith
    return bRes
  endfunc

  add object oDA1STD_1_12 as StdField with uid="NUAPRZIBJR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DA1STD", cQueryName = "DA1STD",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido oppure orario iniziale maggiore dell'orario finale",;
    ToolTipText = "Orario lavorativo iniziale predefinito prima fascia",;
    HelpContextID = 139665866,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=529, Top=29, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA1STD_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(1, .w_DA1STD, .w_AL1STD, .w_DA2STD, .w_AL2STD))
    endwith
    return bRes
  endfunc

  add object oAL1STD_1_13 as StdField with uid="YTZMGKVDSE",rtseq=10,rtrep=.f.,;
    cFormVar = "w_AL1STD", cQueryName = "AL1STD",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido o orario finale minore dell'orario iniziale",;
    ToolTipText = "Orario lavorativo finale predefinito prima fascia",;
    HelpContextID = 139663098,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=634, Top=29, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL1STD_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_DA1STD))
    endwith
   endif
  endfunc

  func oAL1STD_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(2, .w_DA1STD, .w_AL1STD, .w_DA2STD, .w_AL2STD))
    endwith
    return bRes
  endfunc

  add object oDA2STD_1_14 as StdField with uid="ETGYOAKQGL",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DA2STD", cQueryName = "DA2STD",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale",;
    ToolTipText = "Orario lavorativo iniziale predefinito seconda fascia",;
    HelpContextID = 139661770,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=529, Top=55, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA2STD_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_DA1STD) AND !EMPTY(.w_AL1STD))
    endwith
   endif
  endfunc

  func oDA2STD_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(3, .w_DA1STD, .w_AL1STD, .w_DA2STD, .w_AL2STD))
    endwith
    return bRes
  endfunc

  add object oAL2STD_1_15 as StdField with uid="WLATQCYMWR",rtseq=12,rtrep=.f.,;
    cFormVar = "w_AL2STD", cQueryName = "AL2STD",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido o orario finale minore dell'orario iniziale",;
    ToolTipText = "Orario lavorativo finale predefinito seconda fascia",;
    HelpContextID = 139659002,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=634, Top=54, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL2STD_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_DA2STD))
    endwith
   endif
  endfunc

  func oAL2STD_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(4, .w_DA1STD, .w_AL1STD, .w_DA2STD, .w_AL2STD))
    endwith
    return bRes
  endfunc

  add object oDAY2_1_24 as StdCheck with uid="VNIGJLRHOB",rtseq=14,rtrep=.f.,left=19, top=148, caption="Lunedi",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 28418506,;
    cFormVar="w_DAY2", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDAY2_1_24.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oDAY2_1_24.GetRadio()
    this.Parent.oContained.w_DAY2 = this.RadioValue()
    return .t.
  endfunc

  func oDAY2_1_24.SetRadio()
    this.Parent.oContained.w_DAY2=trim(this.Parent.oContained.w_DAY2)
    this.value = ;
      iif(this.Parent.oContained.w_DAY2=="S",1,;
      0)
  endfunc

  add object oDA1DAY2_1_25 as StdField with uid="WGXCNHDVHC",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DA1DAY2", cQueryName = "DA1DAY2",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo iniziale prima fascia",;
    HelpContextID = 191749686,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=149, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA1DAY2_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY2="S")
    endwith
   endif
  endfunc

  func oDA1DAY2_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(1, .w_DA1DAY2, .w_AL1DAY2, .w_DA2DAY2, .w_AL2DAY2))
    endwith
    return bRes
  endfunc

  add object oAL1DAY2_1_26 as StdField with uid="HUHYFZJILD",rtseq=16,rtrep=.f.,;
    cFormVar = "w_AL1DAY2", cQueryName = "AL1DAY2",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo finale prima fascia",;
    HelpContextID = 191752454,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=184, Top=149, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL1DAY2_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY2="S" AND !EMPTY(.w_DA1DAY2))
    endwith
   endif
  endfunc

  func oAL1DAY2_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(2, .w_DA1DAY2, .w_AL1DAY2, .w_DA2DAY2, .w_AL2DAY2))
    endwith
    return bRes
  endfunc

  add object oDA2DAY2_1_27 as StdField with uid="GZRVDHARLW",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DA2DAY2", cQueryName = "DA2DAY2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale",;
    ToolTipText = "Orario lavorativo iniziale seconda fascia",;
    HelpContextID = 191753782,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=243, Top=149, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA2DAY2_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY2="S" AND !EMPTY(.w_DA1DAY2) AND !EMPTY(.w_AL1DAY2))
    endwith
   endif
  endfunc

  func oDA2DAY2_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(3, .w_DA1DAY2, .w_AL1DAY2, .w_DA2DAY2, .w_AL2DAY2))
    endwith
    return bRes
  endfunc

  add object oAL2DAY2_1_28 as StdField with uid="ZGUOAUJDUF",rtseq=18,rtrep=.f.,;
    cFormVar = "w_AL2DAY2", cQueryName = "AL2DAY2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo finale seconda fascia",;
    HelpContextID = 191756550,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=300, Top=149, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL2DAY2_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY2="S" AND !EMPTY(.w_DA2DAY2))
    endwith
   endif
  endfunc

  func oAL2DAY2_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(4, .w_DA1DAY2, .w_AL1DAY2, .w_DA2DAY2, .w_AL2DAY2))
    endwith
    return bRes
  endfunc

  add object oDAY3_1_29 as StdCheck with uid="NLBFVWVBVV",rtseq=19,rtrep=.f.,left=19, top=171, caption="Martedi",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 28352970,;
    cFormVar="w_DAY3", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDAY3_1_29.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oDAY3_1_29.GetRadio()
    this.Parent.oContained.w_DAY3 = this.RadioValue()
    return .t.
  endfunc

  func oDAY3_1_29.SetRadio()
    this.Parent.oContained.w_DAY3=trim(this.Parent.oContained.w_DAY3)
    this.value = ;
      iif(this.Parent.oContained.w_DAY3=="S",1,;
      0)
  endfunc

  add object oDA1DAY3_1_30 as StdField with uid="ARUQPYXAAA",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DA1DAY3", cQueryName = "DA1DAY3",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo iniziale prima fascia",;
    HelpContextID = 191749686,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=172, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA1DAY3_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY3="S")
    endwith
   endif
  endfunc

  func oDA1DAY3_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(1, .w_DA1DAY3, .w_AL1DAY3, .w_DA2DAY3, .w_AL2DAY3))
    endwith
    return bRes
  endfunc

  add object oAL1DAY3_1_31 as StdField with uid="LIGSWJTWXH",rtseq=21,rtrep=.f.,;
    cFormVar = "w_AL1DAY3", cQueryName = "AL1DAY3",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo finale prima fascia",;
    HelpContextID = 191752454,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=184, Top=172, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL1DAY3_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY3="S" AND !EMPTY(.w_DA1DAY3))
    endwith
   endif
  endfunc

  func oAL1DAY3_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(2, .w_DA1DAY3, .w_AL1DAY3, .w_DA2DAY3, .w_AL2DAY3))
    endwith
    return bRes
  endfunc

  add object oDA2DAY3_1_32 as StdField with uid="DGORMLUQND",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DA2DAY3", cQueryName = "DA2DAY3",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale",;
    ToolTipText = "Orario lavorativo iniziale seconda fascia",;
    HelpContextID = 191753782,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=243, Top=172, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA2DAY3_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY3="S" AND !EMPTY(.w_DA1DAY3) AND !EMPTY(.w_AL1DAY3))
    endwith
   endif
  endfunc

  func oDA2DAY3_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(3, .w_DA1DAY3, .w_AL1DAY3, .w_DA2DAY3, .w_AL2DAY3))
    endwith
    return bRes
  endfunc

  add object oAL2DAY3_1_33 as StdField with uid="OPMGJGLYIV",rtseq=23,rtrep=.f.,;
    cFormVar = "w_AL2DAY3", cQueryName = "AL2DAY3",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo finale seconda fascia",;
    HelpContextID = 191756550,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=300, Top=172, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL2DAY3_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY3="S" AND !EMPTY(.w_DA2DAY3))
    endwith
   endif
  endfunc

  func oAL2DAY3_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(4, .w_DA1DAY3, .w_AL1DAY3, .w_DA2DAY3, .w_AL2DAY3))
    endwith
    return bRes
  endfunc

  add object oDAY4_1_34 as StdCheck with uid="KLIIYIJSZN",rtseq=24,rtrep=.f.,left=19, top=194, caption="Mercoledi",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 28287434,;
    cFormVar="w_DAY4", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDAY4_1_34.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oDAY4_1_34.GetRadio()
    this.Parent.oContained.w_DAY4 = this.RadioValue()
    return .t.
  endfunc

  func oDAY4_1_34.SetRadio()
    this.Parent.oContained.w_DAY4=trim(this.Parent.oContained.w_DAY4)
    this.value = ;
      iif(this.Parent.oContained.w_DAY4=="S",1,;
      0)
  endfunc

  add object oDA1DAY4_1_35 as StdField with uid="MWEBYKWTKU",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DA1DAY4", cQueryName = "DA1DAY4",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo iniziale prima fascia",;
    HelpContextID = 191749686,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=195, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA1DAY4_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY4="S")
    endwith
   endif
  endfunc

  func oDA1DAY4_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(1, .w_DA1DAY4, .w_AL1DAY4, .w_DA2DAY4, .w_AL2DAY4))
    endwith
    return bRes
  endfunc

  add object oAL1DAY4_1_36 as StdField with uid="MNRSTEXLDJ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_AL1DAY4", cQueryName = "AL1DAY4",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo finale prima fascia",;
    HelpContextID = 191752454,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=184, Top=195, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL1DAY4_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY4="S" AND !EMPTY(.w_DA1DAY4))
    endwith
   endif
  endfunc

  func oAL1DAY4_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(2, .w_DA1DAY4, .w_AL1DAY4, .w_DA2DAY4, .w_AL2DAY4))
    endwith
    return bRes
  endfunc

  add object oDA2DAY4_1_37 as StdField with uid="FXWTWMTTDB",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DA2DAY4", cQueryName = "DA2DAY4",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale",;
    ToolTipText = "Orario lavorativo iniziale seconda fascia",;
    HelpContextID = 191753782,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=243, Top=195, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA2DAY4_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY4="S" AND !EMPTY(.w_DA1DAY4) AND !EMPTY(.w_AL1DAY4))
    endwith
   endif
  endfunc

  func oDA2DAY4_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(3, .w_DA1DAY4, .w_AL1DAY4, .w_DA2DAY4, .w_AL2DAY4))
    endwith
    return bRes
  endfunc

  add object oAL2DAY4_1_38 as StdField with uid="KPQKLTIJYK",rtseq=28,rtrep=.f.,;
    cFormVar = "w_AL2DAY4", cQueryName = "AL2DAY4",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo finale seconda fascia",;
    HelpContextID = 191756550,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=300, Top=195, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL2DAY4_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY4="S" AND !EMPTY(.w_DA2DAY4))
    endwith
   endif
  endfunc

  func oAL2DAY4_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(4, .w_DA1DAY4, .w_AL1DAY4, .w_DA2DAY4, .w_AL2DAY4))
    endwith
    return bRes
  endfunc

  add object oDAY5_1_39 as StdCheck with uid="BBNCKTANRO",rtseq=29,rtrep=.f.,left=19, top=217, caption="Giovedi",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 28221898,;
    cFormVar="w_DAY5", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDAY5_1_39.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oDAY5_1_39.GetRadio()
    this.Parent.oContained.w_DAY5 = this.RadioValue()
    return .t.
  endfunc

  func oDAY5_1_39.SetRadio()
    this.Parent.oContained.w_DAY5=trim(this.Parent.oContained.w_DAY5)
    this.value = ;
      iif(this.Parent.oContained.w_DAY5=="S",1,;
      0)
  endfunc

  add object oDA1DAY5_1_40 as StdField with uid="BYGGNRSPTA",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DA1DAY5", cQueryName = "DA1DAY5",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo iniziale prima fascia",;
    HelpContextID = 191749686,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=218, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA1DAY5_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY5="S")
    endwith
   endif
  endfunc

  func oDA1DAY5_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(1, .w_DA1DAY5, .w_AL1DAY5, .w_DA2DAY5, .w_AL2DAY5))
    endwith
    return bRes
  endfunc

  add object oAL1DAY5_1_41 as StdField with uid="UTGFQIVCCV",rtseq=31,rtrep=.f.,;
    cFormVar = "w_AL1DAY5", cQueryName = "AL1DAY5",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo finale prima fascia",;
    HelpContextID = 191752454,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=184, Top=218, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL1DAY5_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY5="S" AND !EMPTY(.w_DA1DAY5))
    endwith
   endif
  endfunc

  func oAL1DAY5_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(2, .w_DA1DAY5, .w_AL1DAY5, .w_DA2DAY5, .w_AL2DAY5))
    endwith
    return bRes
  endfunc

  add object oDA2DAY5_1_42 as StdField with uid="NEPFSYLGWE",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DA2DAY5", cQueryName = "DA2DAY5",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale",;
    ToolTipText = "Orario lavorativo iniziale seconda fascia",;
    HelpContextID = 191753782,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=243, Top=218, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA2DAY5_1_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY5="S" AND !EMPTY(.w_DA1DAY5) AND !EMPTY(.w_AL1DAY5))
    endwith
   endif
  endfunc

  func oDA2DAY5_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(3, .w_DA1DAY5, .w_AL1DAY5, .w_DA2DAY5, .w_AL2DAY5))
    endwith
    return bRes
  endfunc

  add object oAL2DAY5_1_43 as StdField with uid="GOTDQQPGMX",rtseq=33,rtrep=.f.,;
    cFormVar = "w_AL2DAY5", cQueryName = "AL2DAY5",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo finale seconda fascia",;
    HelpContextID = 191756550,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=300, Top=218, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL2DAY5_1_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY5="S" AND !EMPTY(.w_DA2DAY5))
    endwith
   endif
  endfunc

  func oAL2DAY5_1_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(4, .w_DA1DAY5, .w_AL1DAY5, .w_DA2DAY5, .w_AL2DAY5))
    endwith
    return bRes
  endfunc

  add object oDAY6_1_44 as StdCheck with uid="CZJAIHHKMX",rtseq=34,rtrep=.f.,left=19, top=240, caption="Venerdi",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 28156362,;
    cFormVar="w_DAY6", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDAY6_1_44.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oDAY6_1_44.GetRadio()
    this.Parent.oContained.w_DAY6 = this.RadioValue()
    return .t.
  endfunc

  func oDAY6_1_44.SetRadio()
    this.Parent.oContained.w_DAY6=trim(this.Parent.oContained.w_DAY6)
    this.value = ;
      iif(this.Parent.oContained.w_DAY6=="S",1,;
      0)
  endfunc

  add object oDA1DAY6_1_45 as StdField with uid="FVADZPDJAB",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DA1DAY6", cQueryName = "DA1DAY6",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo iniziale prima fascia",;
    HelpContextID = 191749686,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=241, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA1DAY6_1_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY6="S")
    endwith
   endif
  endfunc

  func oDA1DAY6_1_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(1, .w_DA1DAY6, .w_AL1DAY6, .w_DA2DAY6, .w_AL2DAY6))
    endwith
    return bRes
  endfunc

  add object oAL1DAY6_1_46 as StdField with uid="UOOVSQSUED",rtseq=36,rtrep=.f.,;
    cFormVar = "w_AL1DAY6", cQueryName = "AL1DAY6",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo finale prima fascia",;
    HelpContextID = 191752454,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=184, Top=241, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL1DAY6_1_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY6="S" AND !EMPTY(.w_DA1DAY6))
    endwith
   endif
  endfunc

  func oAL1DAY6_1_46.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(2, .w_DA1DAY6, .w_AL1DAY6, .w_DA2DAY6, .w_AL2DAY6))
    endwith
    return bRes
  endfunc

  add object oDA2DAY6_1_47 as StdField with uid="TYJUPQAZJA",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DA2DAY6", cQueryName = "DA2DAY6",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale",;
    ToolTipText = "Orario lavorativo iniziale seconda fascia",;
    HelpContextID = 191753782,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=243, Top=241, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA2DAY6_1_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY6="S" AND !EMPTY(.w_DA1DAY6) AND !EMPTY(.w_AL1DAY6))
    endwith
   endif
  endfunc

  func oDA2DAY6_1_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(3, .w_DA1DAY6, .w_AL1DAY6, .w_DA2DAY6, .w_AL2DAY6))
    endwith
    return bRes
  endfunc

  add object oAL2DAY6_1_48 as StdField with uid="TKAFAKIPDV",rtseq=38,rtrep=.f.,;
    cFormVar = "w_AL2DAY6", cQueryName = "AL2DAY6",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo finale seconda fascia",;
    HelpContextID = 191756550,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=300, Top=241, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL2DAY6_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY6="S" AND !EMPTY(.w_DA2DAY6))
    endwith
   endif
  endfunc

  func oAL2DAY6_1_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(4, .w_DA1DAY6, .w_AL1DAY6, .w_DA2DAY6, .w_AL2DAY6))
    endwith
    return bRes
  endfunc

  add object oDAY7_1_49 as StdCheck with uid="LQSCKNXWAL",rtseq=39,rtrep=.f.,left=19, top=263, caption="Sabato",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 28090826,;
    cFormVar="w_DAY7", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDAY7_1_49.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oDAY7_1_49.GetRadio()
    this.Parent.oContained.w_DAY7 = this.RadioValue()
    return .t.
  endfunc

  func oDAY7_1_49.SetRadio()
    this.Parent.oContained.w_DAY7=trim(this.Parent.oContained.w_DAY7)
    this.value = ;
      iif(this.Parent.oContained.w_DAY7=="S",1,;
      0)
  endfunc

  add object oDA1DAY7_1_50 as StdField with uid="NLFAZTLMPB",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DA1DAY7", cQueryName = "DA1DAY7",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo iniziale prima fascia",;
    HelpContextID = 191749686,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=264, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA1DAY7_1_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY7="S")
    endwith
   endif
  endfunc

  func oDA1DAY7_1_50.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(1, .w_DA1DAY7, .w_AL1DAY7, .w_DA2DAY7, .w_AL2DAY7))
    endwith
    return bRes
  endfunc

  add object oAL1DAY7_1_51 as StdField with uid="DMPWBUICJW",rtseq=41,rtrep=.f.,;
    cFormVar = "w_AL1DAY7", cQueryName = "AL1DAY7",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo finale prima fascia",;
    HelpContextID = 191752454,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=184, Top=264, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL1DAY7_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY7="S" AND !EMPTY(.w_DA1DAY7))
    endwith
   endif
  endfunc

  func oAL1DAY7_1_51.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(2, .w_DA1DAY7, .w_AL1DAY7, .w_DA2DAY7, .w_AL2DAY7))
    endwith
    return bRes
  endfunc

  add object oDA2DAY7_1_52 as StdField with uid="QEVCMYYXGF",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DA2DAY7", cQueryName = "DA2DAY7",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale",;
    ToolTipText = "Orario lavorativo iniziale seconda fascia",;
    HelpContextID = 191753782,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=243, Top=264, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA2DAY7_1_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY7="S" AND !EMPTY(.w_DA1DAY7) AND !EMPTY(.w_AL1DAY7))
    endwith
   endif
  endfunc

  func oDA2DAY7_1_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(3, .w_DA1DAY7, .w_AL1DAY7, .w_DA2DAY7, .w_AL2DAY7))
    endwith
    return bRes
  endfunc

  add object oAL2DAY7_1_53 as StdField with uid="IXHTBBMVVW",rtseq=43,rtrep=.f.,;
    cFormVar = "w_AL2DAY7", cQueryName = "AL2DAY7",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo finale seconda fascia",;
    HelpContextID = 191756550,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=300, Top=264, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL2DAY7_1_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY7="S" AND !EMPTY(.w_DA2DAY7))
    endwith
   endif
  endfunc

  func oAL2DAY7_1_53.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(4, .w_DA1DAY7, .w_AL1DAY7, .w_DA2DAY7, .w_AL2DAY7))
    endwith
    return bRes
  endfunc

  add object oDAY1_1_54 as StdCheck with uid="ZVZVGLFUIG",rtseq=44,rtrep=.f.,left=19, top=286, caption="Domenica",;
    ToolTipText = "Se attivo: indica che la giornata � normalmente lavorativa",;
    HelpContextID = 28484042,;
    cFormVar="w_DAY1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDAY1_1_54.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oDAY1_1_54.GetRadio()
    this.Parent.oContained.w_DAY1 = this.RadioValue()
    return .t.
  endfunc

  func oDAY1_1_54.SetRadio()
    this.Parent.oContained.w_DAY1=trim(this.Parent.oContained.w_DAY1)
    this.value = ;
      iif(this.Parent.oContained.w_DAY1=="S",1,;
      0)
  endfunc

  add object oDA1DAY1_1_55 as StdField with uid="DIPQBYLHQN",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DA1DAY1", cQueryName = "DA1DAY1",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo iniziale prima fascia",;
    HelpContextID = 191749686,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=127, Top=288, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA1DAY1_1_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY1="S")
    endwith
   endif
  endfunc

  func oDA1DAY1_1_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(1, .w_DA1DAY1, .w_AL1DAY1, .w_DA2DAY1, .w_AL2DAY1))
    endwith
    return bRes
  endfunc

  add object oAL1DAY1_1_56 as StdField with uid="YHFWIRJPZH",rtseq=46,rtrep=.f.,;
    cFormVar = "w_AL1DAY1", cQueryName = "AL1DAY1",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo finale prima fascia",;
    HelpContextID = 191752454,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=184, Top=288, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL1DAY1_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY1="S" AND !EMPTY(.w_DA1DAY1))
    endwith
   endif
  endfunc

  func oAL1DAY1_1_56.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(2, .w_DA1DAY1, .w_AL1DAY1, .w_DA2DAY1, .w_AL2DAY1))
    endwith
    return bRes
  endfunc

  add object oDA2DAY1_1_57 as StdField with uid="TSYBSXUSNH",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DA2DAY1", cQueryName = "DA2DAY1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Orario specificato non valido, orario iniziale maggiore dell'orario finale oppure orario iniziale minore del precedente orario finale",;
    ToolTipText = "Orario lavorativo iniziale seconda fascia",;
    HelpContextID = 191753782,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=243, Top=288, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oDA2DAY1_1_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY1="S" AND !EMPTY(.w_DA1DAY1) AND !EMPTY(.w_AL1DAY1))
    endwith
   endif
  endfunc

  func oDA2DAY1_1_57.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(3, .w_DA1DAY1, .w_AL1DAY1, .w_DA2DAY1, .w_AL2DAY1))
    endwith
    return bRes
  endfunc

  add object oAL2DAY1_1_58 as StdField with uid="ROQJOOOVXD",rtseq=48,rtrep=.f.,;
    cFormVar = "w_AL2DAY1", cQueryName = "AL2DAY1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Orario lavorativo finale seconda fascia",;
    HelpContextID = 191756550,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=300, Top=288, cSayPict='"99:99"', cGetPict='"99:99"', InputMask=replicate('X',5)

  func oAL2DAY1_1_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DAY1="S" AND !EMPTY(.w_DA2DAY1))
    endwith
   endif
  endfunc

  func oAL2DAY1_1_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chktimewnd(4, .w_DA1DAY1, .w_AL1DAY1, .w_DA2DAY1, .w_AL2DAY1))
    endwith
    return bRes
  endfunc

  add object oOREDAY2_1_60 as StdField with uid="ACFWBGPTIY",rtseq=49,rtrep=.f.,;
    cFormVar = "w_OREDAY2", cQueryName = "OREDAY2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero ore lavorative",;
    HelpContextID = 191836134,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=358, Top=149, cSayPict='"@K 99.99"', cGetPict='"@K 99.99"'

  func oOREDAY2_1_60.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_OREDAY2>=0 AND .w_OREDAY2<=24)
    endwith
    return bRes
  endfunc

  add object oOREDAY3_1_61 as StdField with uid="FHUXXCRPUB",rtseq=50,rtrep=.f.,;
    cFormVar = "w_OREDAY3", cQueryName = "OREDAY3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero ore lavorative",;
    HelpContextID = 191836134,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=358, Top=172, cSayPict='"@K 99.99"', cGetPict='"@K 99.99"'

  func oOREDAY3_1_61.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (0<=.w_OREDAY3 AND .w_OREDAY3<=24)
    endwith
    return bRes
  endfunc

  add object oOREDAY4_1_62 as StdField with uid="FABEPMAYAJ",rtseq=51,rtrep=.f.,;
    cFormVar = "w_OREDAY4", cQueryName = "OREDAY4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero ore lavorative",;
    HelpContextID = 191836134,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=358, Top=195, cSayPict='"@K 99.99"', cGetPict='"@K 99.99"'

  func oOREDAY4_1_62.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (0<=.w_OREDAY4 AND .w_OREDAY4<=24)
    endwith
    return bRes
  endfunc

  add object oOREDAY5_1_63 as StdField with uid="BJGFLBEBHJ",rtseq=52,rtrep=.f.,;
    cFormVar = "w_OREDAY5", cQueryName = "OREDAY5",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero ore lavorative",;
    HelpContextID = 191836134,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=358, Top=218, cSayPict='"@K 99.99"', cGetPict='"@K 99.99"'

  func oOREDAY5_1_63.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (0<=.w_OREDAY5 AND .w_OREDAY5<=24)
    endwith
    return bRes
  endfunc

  add object oOREDAY6_1_64 as StdField with uid="SPUDZVGYZV",rtseq=53,rtrep=.f.,;
    cFormVar = "w_OREDAY6", cQueryName = "OREDAY6",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero ore lavorative",;
    HelpContextID = 191836134,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=358, Top=241, cSayPict='"@K 99.99"', cGetPict='"@K 99.99"'

  func oOREDAY6_1_64.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (0<=.w_OREDAY6 AND .w_OREDAY6<=24)
    endwith
    return bRes
  endfunc

  add object oOREDAY7_1_65 as StdField with uid="XUXZUBGWSA",rtseq=54,rtrep=.f.,;
    cFormVar = "w_OREDAY7", cQueryName = "OREDAY7",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero ore lavorative",;
    HelpContextID = 191836134,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=358, Top=264, cSayPict='"@K 99.99"', cGetPict='"@K 99.99"'

  func oOREDAY7_1_65.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (0<=.w_OREDAY7 AND .w_OREDAY7<=24)
    endwith
    return bRes
  endfunc

  add object oOREDAY1_1_66 as StdField with uid="SBKYMPUARI",rtseq=55,rtrep=.f.,;
    cFormVar = "w_OREDAY1", cQueryName = "OREDAY1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero ore lavorative",;
    HelpContextID = 191836134,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=358, Top=288, cSayPict='"@K 99.99"', cGetPict='"@K 99.99"'

  func oOREDAY1_1_66.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (0<=.w_OREDAY1 AND .w_OREDAY1<=24)
    endwith
    return bRes
  endfunc

  add object oINPER1_1_67 as StdField with uid="MLVMAQOKYJ",rtseq=56,rtrep=.f.,;
    cFormVar = "w_INPER1", cQueryName = "INPER1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio periodo di chiusura deve essere minore o uguale alla data di fine periodo di chiusura ed appartenere all'intervallo di date specificato",;
    ToolTipText = "Data inizio periodo di chiusura",;
    HelpContextID = 192881786,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=490, Top=149, cSayPict='"@K"', cGetPict='"@K"'

  func oINPER1_1_67.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_INPER1) or (.w_INPER1>=.w_DATINI and .w_INPER1<=.w_DATFIN and (empty(.w_FIPER1) or .w_INPER1<=.w_FIPER1)))
    endwith
    return bRes
  endfunc

  add object oFIPER1_1_68 as StdField with uid="WBXTTMXEMQ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_FIPER1", cQueryName = "FIPER1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di periodo finale deve essere maggiore o uguale alla data di periodo iniziale ed appartenere all'intervallo di date specificato",;
    ToolTipText = "Data fine periodo di chiusura",;
    HelpContextID = 192883114,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=610, Top=149, cSayPict='"@K"', cGetPict='"@K"'

  func oFIPER1_1_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_INPER1))
    endwith
   endif
  endfunc

  func oFIPER1_1_68.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_FIPER1) or (.w_FIPER1>=.w_DATINI and .w_FIPER1<=.w_DATFIN and .w_INPER1<=.w_FIPER1))
    endwith
    return bRes
  endfunc

  add object oINPER2_1_69 as StdField with uid="GMTZHTUDBH",rtseq=58,rtrep=.f.,;
    cFormVar = "w_INPER2", cQueryName = "INPER2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio periodo di chiusura deve essere minore o uguale alla data di fine periodo di chiusura ed appartenere all'intervallo di date specificato",;
    ToolTipText = "Data inizio periodo di chiusura",;
    HelpContextID = 176104570,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=490, Top=172, cSayPict='"@K"', cGetPict='"@K"'

  func oINPER2_1_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_FIPER1))
    endwith
   endif
  endfunc

  func oINPER2_1_69.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_INPER2) or (.w_INPER2>=.w_DATINI and .w_INPER2<=.w_DATFIN and (empty(.w_FIPER2) or .w_INPER2<=.w_FIPER2)))
    endwith
    return bRes
  endfunc

  add object oFIPER2_1_70 as StdField with uid="GCAMYKVALR",rtseq=59,rtrep=.f.,;
    cFormVar = "w_FIPER2", cQueryName = "FIPER2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di periodo finale deve essere maggiore o uguale alla data di periodo iniziale ed appartenere all'intervallo di date specificato",;
    ToolTipText = "Data fine periodo di chiusura",;
    HelpContextID = 176105898,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=610, Top=172, cSayPict='"@K"', cGetPict='"@K"'

  func oFIPER2_1_70.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_INPER2))
    endwith
   endif
  endfunc

  func oFIPER2_1_70.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_FIPER2) or (.w_FIPER2>=.w_DATINI and .w_FIPER2<=.w_DATFIN and .w_INPER2<=.w_FIPER2))
    endwith
    return bRes
  endfunc

  add object oINPER3_1_71 as StdField with uid="HUFQUDYASW",rtseq=60,rtrep=.f.,;
    cFormVar = "w_INPER3", cQueryName = "INPER3",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio periodo di chiusura deve essere minore o uguale alla data di fine periodo di chiusura ed appartenere all'intervallo di date specificato",;
    ToolTipText = "Data inizio periodo di chiusura",;
    HelpContextID = 159327354,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=490, Top=195, cSayPict='"@K"', cGetPict='"@K"'

  func oINPER3_1_71.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_FIPER2))
    endwith
   endif
  endfunc

  func oINPER3_1_71.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_INPER3) or (.w_INPER3>=.w_DATINI and .w_INPER3<=.w_DATFIN and (empty(.w_FIPER3) or .w_INPER3<=.w_FIPER3)))
    endwith
    return bRes
  endfunc

  add object oFIPER3_1_72 as StdField with uid="KVLIOAEZIO",rtseq=61,rtrep=.f.,;
    cFormVar = "w_FIPER3", cQueryName = "FIPER3",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di periodo finale deve essere maggiore o uguale alla data di periodo iniziale ed appartenere all'intervallo di date specificato",;
    ToolTipText = "Data fine periodo di chiusura",;
    HelpContextID = 159328682,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=610, Top=195, cSayPict='"@K"', cGetPict='"@K"'

  func oFIPER3_1_72.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_INPER3))
    endwith
   endif
  endfunc

  func oFIPER3_1_72.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_FIPER3) or (.w_FIPER3>=.w_DATINI and .w_FIPER3<=.w_DATFIN and .w_INPER3<=.w_FIPER3))
    endwith
    return bRes
  endfunc

  add object oINPER4_1_73 as StdField with uid="REFDMEFCMZ",rtseq=62,rtrep=.f.,;
    cFormVar = "w_INPER4", cQueryName = "INPER4",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio periodo di chiusura deve essere minore o uguale alla data di fine periodo di chiusura ed appartenere all'intervallo di date specificato",;
    ToolTipText = "Data inizio periodo di chiusura",;
    HelpContextID = 142550138,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=490, Top=218, cSayPict='"@K"', cGetPict='"@K"'

  func oINPER4_1_73.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_FIPER3))
    endwith
   endif
  endfunc

  func oINPER4_1_73.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_INPER4) or (.w_INPER4>=.w_DATINI and .w_INPER4<=.w_DATFIN and (empty(.w_FIPER4) or .w_INPER4<=.w_FIPER4)))
    endwith
    return bRes
  endfunc

  add object oFIPER4_1_74 as StdField with uid="NBMQLCBGIN",rtseq=63,rtrep=.f.,;
    cFormVar = "w_FIPER4", cQueryName = "FIPER4",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di periodo finale deve essere maggiore o uguale alla data di periodo iniziale ed appartenere all'intervallo di date specificato",;
    ToolTipText = "Data fine periodo di chiusura",;
    HelpContextID = 142551466,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=610, Top=218, cSayPict='"@K"', cGetPict='"@K"'

  func oFIPER4_1_74.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_INPER4))
    endwith
   endif
  endfunc

  func oFIPER4_1_74.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_FIPER4) or (.w_FIPER4>=.w_DATINI and .w_FIPER4<=.w_DATFIN and .w_INPER4<=.w_FIPER4))
    endwith
    return bRes
  endfunc

  add object oINPER5_1_75 as StdField with uid="UAJZSQRSFI",rtseq=64,rtrep=.f.,;
    cFormVar = "w_INPER5", cQueryName = "INPER5",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio periodo di chiusura deve essere minore o uguale alla data di fine periodo di chiusura ed appartenere all'intervallo di date specificato",;
    ToolTipText = "Data inizio periodo di chiusura",;
    HelpContextID = 125772922,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=490, Top=241, cSayPict='"@K"', cGetPict='"@K"'

  func oINPER5_1_75.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_FIPER4))
    endwith
   endif
  endfunc

  func oINPER5_1_75.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_INPER5) or (.w_INPER5>=.w_DATINI and .w_INPER5<=.w_DATFIN and (empty(.w_FIPER5) or .w_INPER5<=.w_FIPER5)))
    endwith
    return bRes
  endfunc

  add object oFIPER5_1_76 as StdField with uid="OLGARTPNFH",rtseq=65,rtrep=.f.,;
    cFormVar = "w_FIPER5", cQueryName = "FIPER5",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di periodo finale deve essere maggiore o uguale alla data di periodo iniziale ed appartenere all'intervallo di date specificato",;
    ToolTipText = "Data fine periodo di chiusura",;
    HelpContextID = 125774250,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=610, Top=241, cSayPict='"@K"', cGetPict='"@K"'

  func oFIPER5_1_76.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_INPER5))
    endwith
   endif
  endfunc

  func oFIPER5_1_76.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_FIPER5) or (.w_FIPER5>=.w_DATINI and .w_FIPER5<=.w_DATFIN and .w_INPER5<=.w_FIPER5))
    endwith
    return bRes
  endfunc

  add object oINPER6_1_77 as StdField with uid="FHJCBCSYRE",rtseq=66,rtrep=.f.,;
    cFormVar = "w_INPER6", cQueryName = "INPER6",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio periodo di chiusura deve essere minore o uguale alla data di fine periodo di chiusura ed appartenere all'intervallo di date specificato",;
    ToolTipText = "Data inizio periodo di chiusura",;
    HelpContextID = 108995706,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=490, Top=264, cSayPict='"@K"', cGetPict='"@K"'

  func oINPER6_1_77.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_FIPER5))
    endwith
   endif
  endfunc

  func oINPER6_1_77.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_INPER5) or (.w_INPER6>=.w_DATINI and .w_INPER6<=.w_DATFIN and (empty(.w_FIPER6) or .w_INPER6<=.w_FIPER6)))
    endwith
    return bRes
  endfunc

  add object oFIPER6_1_78 as StdField with uid="TOOOAWDRKI",rtseq=67,rtrep=.f.,;
    cFormVar = "w_FIPER6", cQueryName = "FIPER6",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di periodo finale deve essere maggiore o uguale alla data di periodo iniziale ed appartenere all'intervallo di date specificato",;
    ToolTipText = "Data fine periodo di chiusura",;
    HelpContextID = 108997034,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=610, Top=264, cSayPict='"@K"', cGetPict='"@K"'

  func oFIPER6_1_78.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_INPER6))
    endwith
   endif
  endfunc

  func oFIPER6_1_78.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_FIPER6) or (.w_FIPER6>=.w_DATINI and .w_FIPER6<=.w_DATFIN and .w_INPER6<=.w_FIPER6))
    endwith
    return bRes
  endfunc

  add object oINPER7_1_79 as StdField with uid="CYHCEVFOUT",rtseq=68,rtrep=.f.,;
    cFormVar = "w_INPER7", cQueryName = "INPER7",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio periodo di chiusura deve essere minore o uguale alla data di fine periodo di chiusura ed appartenere all'intervallo di date specificato",;
    ToolTipText = "Data inizio periodo di chiusura",;
    HelpContextID = 92218490,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=490, Top=287, cSayPict='"@K"', cGetPict='"@K"'

  func oINPER7_1_79.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_FIPER6))
    endwith
   endif
  endfunc

  func oINPER7_1_79.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_INPER7) or (.w_INPER7>=.w_DATINI and .w_INPER7<=.w_DATFIN and (empty(.w_FIPER7) or .w_INPER7<=.w_FIPER7)))
    endwith
    return bRes
  endfunc

  add object oFIPER7_1_80 as StdField with uid="AITQFQOEWA",rtseq=69,rtrep=.f.,;
    cFormVar = "w_FIPER7", cQueryName = "FIPER7",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di periodo finale deve essere maggiore o uguale alla data di periodo iniziale ed appartenere all'intervallo di date specificato",;
    ToolTipText = "Data fine periodo di chiusura",;
    HelpContextID = 92219818,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=610, Top=287, cSayPict='"@K"', cGetPict='"@K"'

  func oFIPER7_1_80.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_INPER7))
    endwith
   endif
  endfunc

  func oFIPER7_1_80.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_FIPER7) or (.w_FIPER7>=.w_DATINI and .w_FIPER7<=.w_DATFIN and .w_INPER7<=.w_FIPER7))
    endwith
    return bRes
  endfunc

  add object oGIOSO1_1_81 as StdField with uid="YVDQLOZTGX",rtseq=70,rtrep=.f.,;
    cFormVar = "w_GIOSO1", cQueryName = "GIOSO1",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Giorno inizio sospensione forense",;
    HelpContextID = 195115418,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=184, Top=348, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oGIOSO1_1_81.mHide()
    with this.Parent.oContained
      return (Not Isalt())
    endwith
  endfunc

  func oGIOSO1_1_81.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_GIOSO1)>0 AND VAL(.w_GIOSO1)<=31)
    endwith
    return bRes
  endfunc

  add object oMESO1_1_82 as StdField with uid="RXWXUVIZUL",rtseq=71,rtrep=.f.,;
    cFormVar = "w_MESO1", cQueryName = "MESO1",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Mese inizio sospensione forense",;
    HelpContextID = 243596602,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=263, Top=348, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMESO1_1_82.mHide()
    with this.Parent.oContained
      return (Not Isalt())
    endwith
  endfunc

  func oMESO1_1_82.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MESO1)>0 AND VAL(.w_MESO1)<=12)
    endwith
    return bRes
  endfunc

  add object oGIOSO2_1_83 as StdField with uid="OCMWAQIRBB",rtseq=72,rtrep=.f.,;
    cFormVar = "w_GIOSO2", cQueryName = "GIOSO2",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Giorno finale di sospensione forense",;
    HelpContextID = 178338202,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=184, Top=371, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oGIOSO2_1_83.mHide()
    with this.Parent.oContained
      return (Not Isalt())
    endwith
  endfunc

  func oGIOSO2_1_83.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_GIOSO2)>0 AND VAL(.w_GIOSO2)<=31)
    endwith
    return bRes
  endfunc

  add object oMESO2_1_84 as StdField with uid="XNDUAUAZRR",rtseq=73,rtrep=.f.,;
    cFormVar = "w_MESO2", cQueryName = "MESO2",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Mese finale di sospensione forense",;
    HelpContextID = 242548026,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=263, Top=371, cSayPict="'99'", cGetPict="'99'", InputMask=replicate('X',2)

  func oMESO2_1_84.mHide()
    with this.Parent.oContained
      return (Not Isalt())
    endwith
  endfunc

  func oMESO2_1_84.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_MESO2)>0 AND VAL(.w_MESO2)<=12)
    endwith
    return bRes
  endfunc


  add object oBtn_1_94 as StdButton with uid="ZMIZZNTUDJ",left=589, top=368, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 32057114;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_94.Click()
      with this.Parent.oContained
        GSAR_BGC(this.Parent.oContained,"GENER")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_95 as StdButton with uid="JWIBHYOYEX",left=639, top=368, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 24760250;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_95.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCHIPE_1_110 as StdField with uid="PRUQNAZQJK",rtseq=74,rtrep=.f.,;
    cFormVar = "w_DESCHIPE", cQueryName = "DESCHIPE",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione standard per le chiusure periodiche",;
    HelpContextID = 69270917,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=424, Top=102, InputMask=replicate('X',35)


  add object oBtn_1_114 as StdButton with uid="SFGPREFDEN",left=12, top=368, width=48,height=45,;
    CpPicture="BMP\DISPTEMP.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare gli orari dei giorni selezionati con gli orari standard";
    , HelpContextID = 143235993;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_114.Click()
      with this.Parent.oContained
        GSAR_BGC(this.Parent.oContained,"UPDHM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="SDCFWYTGZS",Visible=.t., Left=7, Top=21,;
    Alignment=1, Width=81, Height=18,;
    Caption="Calendario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="OELZAQFCBT",Visible=.t., Left=7, Top=44,;
    Alignment=1, Width=81, Height=15,;
    Caption="Festivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_5.mHide()
    with this.Parent.oContained
      return (YEAR(.w_DATINI) # YEAR(.w_DATFIN) AND !EMPTY(.w_DATINI) AND !EMPTY(.w_DATFIN))
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="UUINRNBKEV",Visible=.t., Left=7, Top=67,;
    Alignment=1, Width=81, Height=18,;
    Caption="Anno solare:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (YEAR(.w_DATINI) # YEAR(.w_DATFIN) AND !EMPTY(.w_DATINI) AND !EMPTY(.w_DATFIN))
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="GTFXAGPLKE",Visible=.t., Left=451, Top=152,;
    Alignment=1, Width=34, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="AYLHPSHDPF",Visible=.t., Left=570, Top=152,;
    Alignment=1, Width=34, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="VEUPGEDSMV",Visible=.t., Left=451, Top=175,;
    Alignment=1, Width=34, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="BKSPEMFIRR",Visible=.t., Left=570, Top=175,;
    Alignment=1, Width=34, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="YIXPZEVHIG",Visible=.t., Left=451, Top=198,;
    Alignment=1, Width=34, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="FJUGSILXXY",Visible=.t., Left=570, Top=198,;
    Alignment=1, Width=34, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="MCMVMALXRV",Visible=.t., Left=27, Top=128,;
    Alignment=0, Width=66, Height=15,;
    Caption="Giorni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_86 as StdString with uid="SQJSWOUBGF",Visible=.t., Left=358, Top=128,;
    Alignment=0, Width=56, Height=15,;
    Caption="N.ore"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_88 as StdString with uid="XDTEXXEWPU",Visible=.t., Left=519, Top=128,;
    Alignment=0, Width=149, Height=15,;
    Caption="Chiusure periodiche"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_89 as StdString with uid="HFOGQPZWVJ",Visible=.t., Left=451, Top=221,;
    Alignment=1, Width=34, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="SOGXYVWODH",Visible=.t., Left=570, Top=221,;
    Alignment=1, Width=34, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_91 as StdString with uid="ZCWUGGUQXZ",Visible=.t., Left=570, Top=244,;
    Alignment=1, Width=34, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_92 as StdString with uid="EVULMGOLFH",Visible=.t., Left=451, Top=244,;
    Alignment=1, Width=34, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_96 as StdString with uid="NWEFOLIKJN",Visible=.t., Left=475, Top=31,;
    Alignment=1, Width=50, Height=18,;
    Caption="Dalle:"  ;
  , bGlobalFont=.t.

  add object oStr_1_97 as StdString with uid="LSIYFWPHRP",Visible=.t., Left=580, Top=31,;
    Alignment=1, Width=53, Height=18,;
    Caption="Alle:"  ;
  , bGlobalFont=.t.

  add object oStr_1_98 as StdString with uid="YDSPWZYEXS",Visible=.t., Left=127, Top=128,;
    Alignment=0, Width=48, Height=15,;
    Caption="Dalle"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_99 as StdString with uid="QFWEZBSLMH",Visible=.t., Left=184, Top=128,;
    Alignment=0, Width=48, Height=15,;
    Caption="Alle"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_100 as StdString with uid="DSMJYABCJY",Visible=.t., Left=570, Top=267,;
    Alignment=1, Width=34, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_101 as StdString with uid="FLKGOZXTXQ",Visible=.t., Left=451, Top=267,;
    Alignment=1, Width=34, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_102 as StdString with uid="QWWHGCYVGK",Visible=.t., Left=570, Top=290,;
    Alignment=1, Width=34, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_103 as StdString with uid="OUZNMIQKFK",Visible=.t., Left=451, Top=290,;
    Alignment=1, Width=34, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_104 as StdString with uid="WTBWPVGLBY",Visible=.t., Left=475, Top=5,;
    Alignment=0, Width=209, Height=18,;
    Caption="Orario lavorativo predefinito"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_106 as StdString with uid="GHOMRYVSPO",Visible=.t., Left=479, Top=57,;
    Alignment=1, Width=46, Height=18,;
    Caption="Dalle:"  ;
  , bGlobalFont=.t.

  add object oStr_1_107 as StdString with uid="HIFFCAIMDU",Visible=.t., Left=580, Top=56,;
    Alignment=1, Width=53, Height=18,;
    Caption="Alle:"  ;
  , bGlobalFont=.t.

  add object oStr_1_108 as StdString with uid="GZIVCMHPTL",Visible=.t., Left=243, Top=128,;
    Alignment=0, Width=48, Height=15,;
    Caption="Dalle"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_109 as StdString with uid="WFPILSCFDT",Visible=.t., Left=300, Top=128,;
    Alignment=0, Width=48, Height=15,;
    Caption="Alle"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_111 as StdString with uid="TDFSARZSSK",Visible=.t., Left=126, Top=104,;
    Alignment=1, Width=294, Height=18,;
    Caption="Descrizione predefinita per le chiusure periodiche:"  ;
  , bGlobalFont=.t.

  add object oStr_1_112 as StdString with uid="SWHUXKCFJA",Visible=.t., Left=142, Top=70,;
    Alignment=1, Width=74, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_113 as StdString with uid="AEKLZTRTRL",Visible=.t., Left=311, Top=71,;
    Alignment=1, Width=63, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_115 as StdString with uid="RJYRZFRGOB",Visible=.t., Left=127, Top=328,;
    Alignment=1, Width=144, Height=18,;
    Caption="Intervallo di sospensione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_115.mHide()
    with this.Parent.oContained
      return (Not isalt())
    endwith
  endfunc

  add object oStr_1_118 as StdString with uid="XBCEBZJTPR",Visible=.t., Left=144, Top=350,;
    Alignment=1, Width=37, Height=18,;
    Caption="Giorno"  ;
  , bGlobalFont=.t.

  func oStr_1_118.mHide()
    with this.Parent.oContained
      return (Not isalt())
    endwith
  endfunc

  add object oStr_1_119 as StdString with uid="UWZQOTMIZI",Visible=.t., Left=223, Top=350,;
    Alignment=1, Width=37, Height=18,;
    Caption="Mese"  ;
  , bGlobalFont=.t.

  func oStr_1_119.mHide()
    with this.Parent.oContained
      return (Not isalt())
    endwith
  endfunc

  add object oStr_1_120 as StdString with uid="TPDJEMXYFG",Visible=.t., Left=144, Top=373,;
    Alignment=1, Width=37, Height=18,;
    Caption="Giorno"  ;
  , bGlobalFont=.t.

  func oStr_1_120.mHide()
    with this.Parent.oContained
      return (Not isalt())
    endwith
  endfunc

  add object oStr_1_121 as StdString with uid="WSBPOISQTX",Visible=.t., Left=223, Top=373,;
    Alignment=1, Width=37, Height=18,;
    Caption="Mese"  ;
  , bGlobalFont=.t.

  func oStr_1_121.mHide()
    with this.Parent.oContained
      return (Not isalt())
    endwith
  endfunc

  add object oBox_1_87 as StdBox with uid="GFBRHZECCA",left=9, top=142, width=398,height=175

  add object oBox_1_93 as StdBox with uid="TECWEIOSDO",left=444, top=142, width=246,height=175

  add object oBox_1_105 as StdBox with uid="SDLVSDVXRW",left=471, top=20, width=219,height=66
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kgc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
