* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bct                                                        *
*              Contabilizzazione distinte                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_556]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-08                                                      *
* Last revis.: 2013-04-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bct",oParentObject,m.pParam)
return(i_retval)

define class tgscg_bct as StdBatch
  * --- Local variables
  pParam = space(1)
  w_oERRORLOG = .NULL.
  w_DINUMDIS = space(10)
  w_PNCODVAL = space(3)
  w_IMPCLF = 0
  w_PTDATSCA = ctod("  /  /  ")
  w_DIBANRIF = space(15)
  w_PNCAOVAL = 0
  w_IMPBAN = 0
  w_PTNUMPAR = space(31)
  w_PNDATREG = ctod("  /  /  ")
  w_PNCODESE = space(4)
  w_PNCODPAG = space(5)
  w_IMPDCA = 0
  w_PTTIPCON = space(1)
  w_PNDATDOC = ctod("  /  /  ")
  w_PNCODUTE = 0
  w_IMPDCP = 0
  w_PTCODCON = space(15)
  w_PNNUMDOC = 0
  w_PNPRP = space(2)
  w_IMPBAN = 0
  w_PT_SEGNO = space(1)
  w_PNALFDOC = space(10)
  w_PNPRG = space(8)
  w_IMPCLF = 0
  w_PTTOTIMP = 0
  w_PNDESSUP = space(50)
  w_CAOVAC = 0
  w_IMPCOM = 0
  w_PTCODVAL = space(3)
  w_PNCODCAU = space(5)
  w_CAONAZ = 0
  w_IMPCOT = 0
  w_PTCAOVAL = 0
  w_PNIMPDAR = 0
  w_CODVAL = space(12)
  w_IMPEFF = 0
  w_PTCAOAPE = 0
  w_PNIMPAVE = 0
  w_CAOVAL = 0
  w_TDIS = 0
  w_PTMODPAG = space(10)
  w_PNNUMRER = 0
  w_PERVAL = space(3)
  w_TINS = 0
  w_PTIMPDOC = 0
  w_CPROWNUM = 0
  w_DATAPE = ctod("  /  /  ")
  w_NINS = 0
  w_PTNUMDOC = 0
  w_PTDESRIG = space(50)
  w_CPROWORD = 0
  w_DATCAM = ctod("  /  /  ")
  w_NDIS = 0
  w_PTALFDOC = space(10)
  w_PNNUMPRO = 0
  w_APPO = 0
  w_SQUAD = 0
  w_PTDATDOC = ctod("  /  /  ")
  w_PNTIPCON = space(1)
  w_APPO1 = 0
  w_UTCC = 0
  w_PTBANNOS = space(15)
  w_PNCODCON = space(15)
  w_EURVAL = 0
  w_UTDC = ctod("  /  /  ")
  w_PTBANAPP = space(10)
  w_PNVALNAZ = space(3)
  w_VAL1 = 0
  w_UTCV = 0
  w_PTFLCRSA = space(1)
  w_PNCOMPET = space(4)
  w_VAL2 = 0
  w_UTDV = ctod("  /  /  ")
  w_PTFLSOSP = space(1)
  w_PNSERIAL = space(10)
  w_PGEN = 0
  w_OK = .f.
  w_MAXDAT = ctod("  /  /  ")
  w_PTROWORD = 0
  w_PNFLPART = space(1)
  w_PARROW = 0
  w_MESS = space(10)
  w_PTROWNUM = 0
  w_PNTIPDOC = space(2)
  w_SALROW = 0
  w_CODAZI = space(5)
  w_PTNUMEFF = 0
  w_PNTIPREG = space(1)
  w_OLDORD = 0
  w_DATBLO = ctod("  /  /  ")
  w_MRINICOM = ctod("  /  /  ")
  w_PNNUMREG = 0
  w_SCOTRA = 0
  w_STALIG = ctod("  /  /  ")
  w_MRFINCOM = ctod("  /  /  ")
  w_PNALFPRO = space(10)
  w_TROV = .f.
  w_TIPCON = space(1)
  w_MRCODVOC = space(15)
  w_PNANNPRO = space(4)
  w_FLGERR = .f.
  w_CODCON = space(15)
  w_MRCODICE = space(15)
  w_PNFLSALI = space(1)
  w_AGGTES = .f.
  w_FLSALI = space(1)
  w_MRPARAME = 0
  w_PNFLSALF = space(1)
  w_OKANAL = .f.
  w_FLSALF = space(1)
  w_MRTOTIMP = 0
  w_PNFLSALD = space(1)
  w_ANALMAN = .f.
  w_FLPPRO = space(1)
  w_MR_SEGNO = space(1)
  w_PNINICOM = ctod("  /  /  ")
  w_CCFLANAL = space(1)
  w_TIPDIS = space(2)
  w_PNFINCOM = ctod("  /  /  ")
  w_CCFLPART = space(1)
  w_CODESE = space(4)
  w_PNFLIVDF = space(1)
  w_TIPO = space(1)
  w_PERPVL = 0
  w_RTOT = 0
  w_TOTPAR = 0
  w_FLINDI = space(1)
  w_ROWCEN = 0
  w_SEZBIL = space(1)
  w_OFLINDI = space(1)
  w_OKANAL = .f.
  w_CONSUP = space(15)
  w_IMPCES = 0
  w_IMPTOT = 0
  w_CCTAGG = space(1)
  w_VADTOBSO = ctod("  /  /  ")
  w_CODVAP = space(3)
  w_CAOVAP = 0
  w_PTFLRAGG = space(1)
  w_PNCODAGE = space(5)
  w_PNFLVABD = space(1)
  w_OSERIAL = space(10)
  w_OROWORD = 0
  w_OROWNUM = 0
  w_PNDESRIG = space(50)
  w_BACONASS = space(15)
  w_BACONSBF = space(1)
  w_DESSUP = space(50)
  w_FLERR = .f.
  w_NUMCOR = space(15)
  w_DATVAL = ctod("  /  /  ")
  w_CCFLCRED = space(1)
  w_CCFLDEBI = space(1)
  w_CCFLCOMM = space(1)
  w_COSCOM = 0
  w_COMVAL = 0
  w_IMPCRE = 0
  w_IMPDEB = 0
  w_TIPCAU = space(1)
  w_CONBANC = space(15)
  w_CAUMOV = space(5)
  w_CAUSBF = space(5)
  w_FLMOVC = space(1)
  w_FLCRDE = space(1)
  w_CALFES = space(3)
  w_IMPDEB = 0
  w_IMPCRE = 0
  w_ROWNUM = 0
  w_TESCAU = .f.
  w_DECTOT = 0
  w_BACODVAL = space(3)
  w_MESS_ERR = space(200)
  w_PTNUMCOR = space(25)
  w_PTCODRAG = space(10)
  w_LCONBAN = space(15)
  w_LCONTO = space(15)
  w_FLCONT = space(1)
  w_NUM_EFFETTO = 0
  w_NumMaxEffetti = 0
  w_NumEffettiSel = 0
  w_AGGCONTROPA = .f.
  w_OLDNUMDIS = space(10)
  w_MESSPN = space(10)
  w_SFLCRDE = space(1)
  w_SIMPCOM = 0
  w_CCDESRIG = space(254)
  w_CCDESSUP = space(254)
  w_NUMERO = 0
  w_DATSCA = ctod("  /  /  ")
  w_COND = .f.
  w_PSERIAL = space(10)
  w_PROWORD = 0
  w_LNUMPAR = space(14)
  w_LCODCON = space(15)
  w_LTIPCON = space(1)
  w_LCODVAL = space(3)
  w_LDATSCA = ctod("  /  /  ")
  w_LSERIAL = space(10)
  w_LCONEFA = space(15)
  w_LROWNUM = 0
  w_UPDATE = .f.
  w_LIMPAVE = 0
  w_LIMPDAR = 0
  w_PARTSN = space(1)
  w_BANRIF = space(15)
  w_ROWBAN = 0
  w_ORDBAN = 0
  w_LDATVAL = ctod("  /  /  ")
  w_LIMPCOM = 0
  w_TESTPAR = space(1)
  w_CONCOL = space(15)
  w_FLZERO = space(1)
  w_SLIMPDAR = 0
  w_SLIMPAVE = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  CAU_CONT_idx=0
  CCM_DETT_idx=0
  CONTI_idx=0
  DIS_TINT_idx=0
  ESERCIZI_idx=0
  MOVICOST_idx=0
  PAR_TITE_idx=0
  PNT_DETT_idx=0
  PNT_MAST_idx=0
  SALDICON_idx=0
  VALUTE_idx=0
  COC_MAST_idx=0
  CCC_MAST_idx=0
  CCC_DETT_idx=0
  BAN_CONTI_idx=0
  CON_INDI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Contabilizzazione Distinte (da GSCG_KCT,GSCG_KCD)
    *     pParam=S  Contabilizzazione Parziale
    *     pParam=I  Contabilizzazione Intera
    * --- Controlli Preliminari - non occorre rimuovere i cursori in caso di fallito check perch� non li ho ancora creati
    do case
      case EMPTY(this.oParentObject.w_CONBAN)
        ah_ErrorMsg("Contropartita spese bancarie non definita",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_CONSCO)
        ah_ErrorMsg("Contropartita sconti tratte non definita",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_CONDCA) OR EMPTY(this.oParentObject.w_CONDCP)
        ah_ErrorMsg("Contropartite differenze cambi non definite",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_CONEFA)
        ah_ErrorMsg("Contropartita effetti attivi non definita",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_CONCES)
        ah_ErrorMsg("Contropartita scarti da cessione non definita",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_DIFCON)
        ah_ErrorMsg("Contropartita differenze di conversione non definita",,"")
        i_retcode = 'stop'
        return
    endcase
     
 DIMENSION ARPARAM[12,2]
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.oParentObject.w_FLDATC="U"
      * --- Try
      local bErr_03F8CA50
      bErr_03F8CA50=bTrsErr
      this.Try_03F8CA50()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg(i_errmsg,,"")
        i_retcode = 'stop'
        return
      endif
      bTrsErr=bTrsErr or bErr_03F8CA50
      * --- End
    endif
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    this.w_OK = .F.
    this.w_NDIS = 0
    * --- Carica Le Distinte da Contabilizzare
    NC = this.oParentObject.w_ZoomCalc.cCursor
    * --- Carica le Partite da Contabilizzare
    if this.pParam="S"
      Select * from (NC) Where XCHK=1 into Cursor MOVIDETT
      * --- Verifico se � stato attivato il flag "Contabilizzazione per Singolo Effetto"
      if this.oParentObject.w_Contsineff="S"
        * --- La query Gscg3kcd raggruppa per seriale distinta e numero effetto
        vq_exec("QUERY\GSCG3KCD.VQR",this,"DETTDIS")
        * --- Calcolo il numero degli effetti presenti nella distinta selezionata
        Select Dinumdis, Count(*) As NumEffetti from Dettdis Group By Dinumdis into Cursor Effetti
        Select Effetti
        this.w_NumMaxEffetti = Nvl(Effetti.NumEffetti,0)
        * --- Effettuo una join tra il cursore Dettdis ed il cursore Movimast
        *     per avere l'elenco degli effetti selezionati dall'utente
        Select Max(Dettdis.Xchk) as Xchk,Dettdis.Dinumdis,Max(Dettdis.Didatdis) as Didatdis,; 
 Max(Dettdis.Didatval) as Didatval,Max(Dettdis.Dinumero) as Dinumero,Max(Dettdis.Dicodese) as Dicodese,; 
 Max(Dettdis.Didescri) as Didescri,Max(Dettdis.Dicaoval) as Dicaoval,Max(Dettdis.Discotra) as Discotra,; 
 Max(Dettdis.Dibanrif) as Dibanrif,Max(Dettdis.Dicoscom) as Dicoscom,Max(Dettdis.Dicodcau) as Dicodcau,; 
 Max(Dettdis.Dicodval) as Dicodval,Max(Dettdis.Di__Anno) as Di__Anno,; 
 Max(Dettdis.Ditipdis) as Ditipdis,Max(Dettdis.Discaces) as Discaces,Max(Dettdis.Vadtobso) Vadtobso,; 
 Max(Dettdis.Conban) as Conban,Max(Dettdis.Bacodval) Bacodval,Max(Dettdis.Didatcon) as Didatcon,; 
 Max(Dettdis.Ditipsca) as Ditipsca,Max(Dettdis.Baconass) as Baconass,Max(Dettdis.Baconsbf) as Baconsbf,; 
 Max(Dettdis.Mineff) as Mineff,Max(Dettdis.Maxeff) as Maxeff,Max(Dettdis.Caflcont) as Caflcont,Dettdis.Numeffetto; 
 from Dettdis inner join Movidett on Dettdis.Dinumdis=Movidett.Ptserial and Dettdis.NumEffetto=Movidett.Ptnumeff Into Cursor Movimast; 
 order by 3,2,27 Group by 2,27
      else
        * --- Nel caso di contabilizzazione distinte effetti parziali devo calcolare
        *     il numero degli effetti selezionati dall'utente
        Select Ptserial, Ptnumeff from Movidett Group By Ptserial,Ptnumeff into Cursor Dettdis
        Select Ptserial, Count(*) As NumEffetti from Dettdis Group By Ptserial into Cursor Effetti
        Select Effetti
        this.w_NumEffettiSel = Nvl(Effetti.NumEffetti,0)
        vq_exec("QUERY\GSCG2KCD.VQR",this,"MOVIMAST")
      endif
      NC="MOVIMAST"
      Select DettDis 
 Use
      Select Effetti 
 Use
    else
      vq_exec("QUERY\GSCG_KCT.VQR",this,"MOVIDETT")
    endif
    if RECCOUNT(NC)>0
      * --- Inizia la Contabilizzazione
      * --- Try
      local bErr_03B0EC08
      bErr_03B0EC08=bTrsErr
      this.Try_03B0EC08()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        if this.w_TROV=.F.
          ah_ErrorMsg(I_errmsg,,"")
        endif
      endif
      bTrsErr=bTrsErr or bErr_03B0EC08
      * --- End
    else
      ah_ErrorMsg("Per la selezione effettuata, non ci sono distinte da contabilizzare",,"")
    endif
    * --- Chiudo la Maschera
    This.bUpdateParentObject=.f.
    This.oParentObject.bUpdated=.f.
    This.oParentObject.ecpQuit()
    * --- Toglie <Blocco> per Primanota
    * --- Try
    local bErr_03B18FE8
    bErr_03B18FE8=bTrsErr
    this.Try_03B18FE8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Impossibile rimuovere il blocco della prima nota. Rimuoverlo dai dati azienda",,"")
    endif
    bTrsErr=bTrsErr or bErr_03B18FE8
    * --- End
    this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati")     
    * --- Rimozione Cursori dalla Memoria e Uscita
    if used("MOVIDETT")
      select MOVIDETT
      use
    endif
    if used("MOVIMAST")
      select MOVIMAST
      use
    endif
    if used("DETCEN")
      select DETCEN
      use
    endif
  endproc
  proc Try_03F8CA50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO,AZSTALIG"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO,AZSTALIG;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MAXDAT = this.oParentObject.w_DATCON
    do case
      case this.w_MAXDAT<=this.w_STALIG AND NOT EMPTY(this.w_STALIG)
        * --- Raise
        i_Error="Data di contabilizzazione inferiore a data ultima stampa L.G."
        return
      case EMPTY(this.w_DATBLO)
        * --- Inserisce <Blocco> per Primanota
        * --- Write into AZIENDA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATFIN),'AZIENDA','AZDATBLO');
              +i_ccchkf ;
          +" where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              AZDATBLO = this.oParentObject.w_DATFIN;
              &i_ccchkf. ;
           where;
              AZCODAZI = this.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case Not EMPTY(this.w_DATBLO)
        * --- Un altro utente ha impostato il blocco - controllo concorrenza
        * --- Raise
        i_Error="Prima nota bloccata - verificare semaforo bollati in dati azienda"
        return
    endcase
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03B0EC08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Cicla sui record Selezionati
    * --- begin transaction
    cp_BeginTrs()
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Possono esserci errori che prescindono dai controlli
    this.w_TROV = this.w_FLGERR
    this.w_TROV = IIF(this.w_NDIS<>this.w_TDIS, .T. , this.w_TROV)
    this.w_MESS = "Movimenti da distinta%0contabilizzati: %1 su %2 selezionati%0%3%0totale registrazioni di primanota generate: %4%0%5%0confermi la presente elaborazione?"
    WAIT CLEAR
    if NOT ah_YesNo(this.w_MESS,,ALLTRIM(STR(this.w_NDIS)),ALLTRIM(STR(this.w_TDIS)),REPL("_",40),ALLTRIM(STR(this.w_PGEN)),REPL("_",40))
      * --- Azzera tutto
      * --- Raise
      i_Error="Operazione annullata"
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03B18FE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inizializza variabili di Lavoro
    * --- Variabili C\C
    * --- Inizializza Valori
    this.w_MESS_ERR = SPACE(200)
    this.w_UTCC = i_CODUTE
    this.w_PNFLSALD = "+"
    this.w_AGGTES = .F.
    this.w_PNCAOVAL = GETCAM(g_PERVAL, i_DATSYS)
    this.w_OKANAL = .T.
    this.w_CCFLPART = " "
    this.w_CAONAZ = GETCAM(g_PERVAL, i_DATSYS)
    this.w_FLPPRO = "N"
    this.w_ANALMAN = .F.
    this.w_PNTIPDOC = "  "
    this.w_PNANNPRO = SPACE(4)
    this.w_FLGERR = .F.
    this.w_FLERR = .F.
    this.w_PNTIPREG = "N"
    this.w_PNCODESE = g_CODESE
    this.w_TINS = 0
    this.w_PNNUMREG = 0
    this.w_IMPDCA = 0
    this.w_NINS = 0
    this.w_PNALFPRO = Space(10)
    this.w_IMPDCP = 0
    this.w_SQUAD = 0
    this.w_TIPCON = "X"
    this.w_IMPEFF = 0
    this.w_PGEN = 0
    this.w_CODCON = SPACE(15)
    this.w_OKANAL = .T.
    this.w_IMPCES = 0
    this.w_IMPBAN = 0
    this.w_TDIS = 0
    this.w_IMPCLF = 0
    this.w_TESCAU = .F.
    this.w_TDIS = 0
    this.w_APPO = 0
    this.w_CODESE = g_CODESE
    this.w_CODAZI = i_CODAZI
    this.w_PERVAL = g_PERVAL
    this.w_DATBLO = cp_CharToDate("  -  -  ")
    this.w_PERPVL = g_PERPVL
    this.w_STALIG = cp_CharToDate("  -  -  ")
    this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
    this.w_PNCODPAG = SPACE(5)
    this.w_DATSCA = CTOD("  -  -  ")
    * --- Controllo che tutte le contropartite non siano obsolete alla data fine selezione
    this.w_TIPO = "G"
    if NOT EMPTY(this.oParentObject.w_CONBAN)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CONBAN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_CONBAN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if w_DATOBSO<this.oParentObject.w_DATFIN AND NOT EMPTY(w_DATOBSO)
        ah_ErrorMsg("Contropartita spese bancarie obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_CONSCO)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CONSCO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_CONSCO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if w_DATOBSO<this.oParentObject.w_DATFIN AND NOT EMPTY(w_DATOBSO)
        ah_ErrorMsg("Contropartita sconti tratte obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_CONDCA)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CONDCA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_CONDCA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if w_DATOBSO<this.oParentObject.w_DATFIN AND NOT EMPTY(w_DATOBSO)
        ah_ErrorMsg("Contropartite differenze cambi obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_CONDCP)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CONDCP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_CONDCP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if w_DATOBSO<this.oParentObject.w_DATFIN AND NOT EMPTY(w_DATOBSO)
        ah_ErrorMsg("Contropartite differenze cambi obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    if NOT EMPTY(this.oParentObject.w_CONEFA)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CONEFA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_CONEFA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if w_DATOBSO<this.oParentObject.w_DATFIN AND NOT EMPTY(w_DATOBSO)
        ah_ErrorMsg("Contropartita effetti attivi obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora e Aggiorna Primanota
    ah_Msg("Inizio contabilizzazione distinte...",.T.)
    this.w_NDIS = 0
    this.w_OLDNUMDIS = Space(10)
    SELECT (NC)
    GO TOP
    SCAN FOR XCHK=1 AND NOT EMPTY(NVL(DINUMDIS,"")) AND NOT EMPTY(NVL(DICODCAU,"")) AND NOT EMPTY(NVL(DIBANRIF,""))
    * --- Per ciascuna Riga, legge i dati di Testata
    this.w_DINUMDIS = DINUMDIS
    if this.w_OLDNUMDIS<>this.w_DINUMDIS
      this.w_TDIS = this.w_TDIS + 1
    endif
    if this.oParentObject.w_CONTSINEFF="S"
      this.w_NUM_EFFETTO = NVL(NUMEFFETTO,0)
    endif
    this.w_PNDATREG = IIF(EMPTY(CP_TODATE(DIDATVAL)), CP_TODATE(DIDATDIS), CP_TODATE(DIDATVAL))
    this.w_PNDATDOC = this.w_PNDATREG
    this.w_DATCAM = this.w_PNDATDOC
    * --- Se data reg.Unica
    this.w_PNDATREG = IIF(this.oParentObject.w_FLDATC="U" AND NOT EMPTY(this.oParentObject.w_DATCON), this.oParentObject.w_DATCON, this.w_PNDATREG)
    this.w_VADTOBSO = CP_TODATE(VADTOBSO)
    this.w_PNNUMDOC = NVL(DINUMERO, 0)
    this.w_PNALFDOC = Space(10)
    this.w_PNDESSUP = NVL(DIDESCRI, " ")
    this.w_PNCODCAU = DICODCAU
    this.w_DIBANRIF = DIBANRIF
    this.w_CONBANC = CONBAN
    this.w_BACODVAL = NVL(BACODVAL,SPACE(3))
    this.w_DATVAL = NVL(DIDATCON,cp_CharToDate("  -  -    "))
    this.w_PNTIPDOC = "  "
    this.w_PNTIPREG = "N"
    this.w_PNNUMREG = 0
    this.w_PNFLIVDF = " "
    this.w_PNALFPRO = Space(10)
    this.w_CCFLANAL = " "
    this.w_TIPDIS = NVL(DITIPDIS, "  ")
    this.w_SCOTRA = NVL(DISCOTRA, 0)
    * --- Nel caso di distinta parziale devo ripartire le commissione in Base al Numero effetto
    *     Nel caso di distinta intera il denominatore sar� pari a 1
    if this.oParentObject.w_CONTSINEFF="S"
      this.w_Impcom = cp_Round(Nvl(Dicoscom, 0)/this.w_NumMaxEffetti,this.w_Perpvl)
    else
      if this.pParam="S"
        this.w_NumMaxEffetti = Nvl(Maxeff,0)-Nvl(Mineff,0)+1
        this.w_Impcom = cp_Round((Nvl(Dicoscom, 0)/this.w_NumMaxEffetti)*this.w_NumEffettiSel,this.w_Perpvl)
      else
        this.w_Impcom = Nvl(Dicoscom, 0)
      endif
    endif
    this.w_IMPCES = IIF(this.w_TIPDIS="CE", NVL(DISCACES, 0), 0)
    this.w_CAOVAC = NVL(DICAOVAL, g_CAOVAL)
    this.w_PNCODVAL = NVL(DICODVAL, g_PERVAL)
    this.w_PNCAOVAL = this.w_CAOVAC
    this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
    this.w_BACONASS = NVL(BACONASS,SPACE(15))
    this.w_BACONSBF = NVL(BACONSBF, " ")
    * --- Valute
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_PNCODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_PNCODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Casale Contabile
    * --- Read from CAU_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCFLANAL,CCTIPDOC,CCTIPREG,CCNUMREG,CCFLIVDF,CCFLPART,CCFLPPRO,CCSERPRO,CCCONIVA,CCFLSALI,CCFLSALF,CCCAUMOV,CCFLMOVC,CCCAUSBF,CCDESSUP,CCDESRIG"+;
        " from "+i_cTable+" CAU_CONT where ";
            +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCFLANAL,CCTIPDOC,CCTIPREG,CCNUMREG,CCFLIVDF,CCFLPART,CCFLPPRO,CCSERPRO,CCCONIVA,CCFLSALI,CCFLSALF,CCCAUMOV,CCFLMOVC,CCCAUSBF,CCDESSUP,CCDESRIG;
        from (i_cTable) where;
            CCCODICE = this.w_PNCODCAU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CCFLANAL = NVL(cp_ToDate(_read_.CCFLANAL),cp_NullValue(_read_.CCFLANAL))
      this.w_PNTIPDOC = NVL(cp_ToDate(_read_.CCTIPDOC),cp_NullValue(_read_.CCTIPDOC))
      this.w_PNTIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
      this.w_PNNUMREG = NVL(cp_ToDate(_read_.CCNUMREG),cp_NullValue(_read_.CCNUMREG))
      this.w_PNFLIVDF = NVL(cp_ToDate(_read_.CCFLIVDF),cp_NullValue(_read_.CCFLIVDF))
      this.w_CCFLPART = NVL(cp_ToDate(_read_.CCFLPART),cp_NullValue(_read_.CCFLPART))
      this.w_FLPPRO = NVL(cp_ToDate(_read_.CCFLPPRO),cp_NullValue(_read_.CCFLPPRO))
      this.w_PNALFPRO = NVL(cp_ToDate(_read_.CCSERPRO),cp_NullValue(_read_.CCSERPRO))
      w_CCCONIVA = NVL(cp_ToDate(_read_.CCCONIVA),cp_NullValue(_read_.CCCONIVA))
      this.w_FLSALI = NVL(cp_ToDate(_read_.CCFLSALI),cp_NullValue(_read_.CCFLSALI))
      this.w_FLSALF = NVL(cp_ToDate(_read_.CCFLSALF),cp_NullValue(_read_.CCFLSALF))
      this.w_CAUMOV = NVL(cp_ToDate(_read_.CCCAUMOV),cp_NullValue(_read_.CCCAUMOV))
      this.w_FLMOVC = NVL(cp_ToDate(_read_.CCFLMOVC),cp_NullValue(_read_.CCFLMOVC))
      this.w_CAUSBF = NVL(cp_ToDate(_read_.CCCAUSBF),cp_NullValue(_read_.CCCAUSBF))
      this.w_CCDESSUP = NVL(cp_ToDate(_read_.CCDESSUP),cp_NullValue(_read_.CCDESSUP))
      this.w_CCDESRIG = NVL(cp_ToDate(_read_.CCDESRIG),cp_NullValue(_read_.CCDESRIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PNANNPRO = SPACE(4)
    do case
      case this.w_FLPPRO="D"
        this.w_PNANNPRO = STR(YEAR(this.w_PNDATREG), 4, 0)
      case this.w_FLPPRO="E"
        this.w_PNANNPRO = this.w_PNCODESE
    endcase
    this.w_PNFLSALI = IIF(this.w_FLSALI="S", "+", " ")
    this.w_PNFLSALF = IIF(this.w_FLSALF="S", "+", " ")
    this.w_PNINICOM = cp_CharToDate("  -  -  ")
    this.w_PNFINCOM = cp_CharToDate("  -  -  ")
    this.w_CCFLANAL = IIF(g_PERCCR="S", this.w_CCFLANAL, " ")
    ah_Msg("Inizio contabilizzazione distinta n. %1 del %2",.T.,.F.,.F.,STR(NVL(DINUMERO,0),6,0),DTOC(CP_TODATE(DIDATDIS)))
    this.w_FLPPRO = "N"
    this.w_SQUAD = 0
    this.w_PNCODESE = this.w_CODESE
    * --- Legge dati collegati alla Causale Contabile
    this.w_FLSALI = " "
    this.w_FLSALF = " "
    this.w_PNVALNAZ = this.w_PERVAL
    this.w_CAONAZ = GETCAM(this.w_PNVALNAZ, this.w_DATCAM, 0)
    this.w_CAONAZ = IIF(this.w_CAONAZ=0, 1, this.w_CAONAZ)
    this.w_PERPVL = g_PERPVL
    if this.w_PNDATREG<g_INIESE OR this.w_PNDATREG>g_FINESE
      * --- Se di un altro esercizio legge Codice e Divisa
      * --- Select from ESERCIZI
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ESCODESE, ESVALNAZ, ESINIESE, ESFINESE  from "+i_cTable+" ESERCIZI ";
            +" where ESINIESE<="+cp_ToStrODBC(this.w_PNDATREG)+" AND ESFINESE>="+cp_ToStrODBC(this.w_PNDATREG)+"";
             ,"_Curs_ESERCIZI")
      else
        select ESCODESE, ESVALNAZ, ESINIESE, ESFINESE from (i_cTable);
         where ESINIESE<=this.w_PNDATREG AND ESFINESE>=this.w_PNDATREG;
          into cursor _Curs_ESERCIZI
      endif
      if used('_Curs_ESERCIZI')
        select _Curs_ESERCIZI
        locate for 1=1
        do while not(eof())
        this.w_PNCODESE = ESCODESE
        this.w_PNVALNAZ = NVL(ESVALNAZ, this.w_PERVAL)
          select _Curs_ESERCIZI
          continue
        enddo
        use
      endif
      * --- Simbolo Valuta Nazionale e cambio Fisso EURO dell'esercizio Letto
      if this.w_PNVALNAZ<>this.w_PERVAL
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_PNVALNAZ);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_PNVALNAZ;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PERPVL = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_CAONAZ = GETCAM(this.w_PNVALNAZ, this.w_DATCAM, 0)
        this.w_CAONAZ = IIF(this.w_CAONAZ=0, 1, this.w_CAONAZ)
      endif
    endif
    if this.w_OLDNUMDIS<>this.w_DINUMDIS
      this.w_TESCAU = .F.
      * --- Controllo data consolidamento e libro giornale
      this.w_MESSPN = CHKINPNT(this.w_PNDATREG,this.w_CCFLANAL,"N",0,this.w_PNDATREG,"NO" ,"Load","SNSNN")
      if NOT EMPTY(this.w_MESSPN)
        this.w_TESCAU = .T.
        this.w_oERRORLOG.AddMsgLogNoTranslate(this.w_MESSPN)     
        this.w_FLERR = .T.
      endif
      if this.w_FLMOVC="S" AND g_BANC="S" AND Not this.w_TESCAU
        * --- Controlli Preliminari Sulle Causali Movimenti C\C
        if EMPTY(this.w_CAUMOV)
          this.w_MESS = ah_MsgFormat("Causale movimenti C\C non definita nella causale contabile")
          this.w_TESCAU = .T.
        else
          * --- Read from CCC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CCC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CCC_MAST_idx,2],.t.,this.CCC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CAFLCRDE,CATIPCON"+;
              " from "+i_cTable+" CCC_MAST where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_CAUMOV);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CAFLCRDE,CATIPCON;
              from (i_cTable) where;
                  CACODICE = this.w_CAUMOV;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLCRDE = NVL(cp_ToDate(_read_.CAFLCRDE),cp_NullValue(_read_.CAFLCRDE))
            this.w_TIPCAU = NVL(cp_ToDate(_read_.CATIPCON),cp_NullValue(_read_.CATIPCON))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from CCC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CCC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CCC_DETT_idx,2],.t.,this.CCC_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CAIMPCOM"+;
              " from "+i_cTable+" CCC_DETT where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_CAUMOV);
                  +" and CANUMCOR = "+cp_ToStrODBC(this.w_CONBANC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CAIMPCOM;
              from (i_cTable) where;
                  CACODICE = this.w_CAUMOV;
                  and CANUMCOR = this.w_CONBANC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_COSCOM = NVL(cp_ToDate(_read_.CAIMPCOM),cp_NullValue(_read_.CAIMPCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_ROWS=0
            this.w_MESS = ah_MsgFormat("Codice C\C: %1 non congruente alla causale movimenti C\C: %2 indicata nella causale contabile",ALLTRIM(this.w_CONBANC),ALLTRIM(this.w_CAUMOV))
            this.w_TESCAU = .T.
          else
            if this.w_BACODVAL<>this.w_PNCODVAL
              this.w_MESS = ah_MsgFormat("Valuta C\C: %1 non congruente alla valuta %2 della distinta",ALLTRIM(this.w_BACODVAL),ALLTRIM(this.w_PNCODVAL))
              this.w_TESCAU = .T.
            endif
          endif
        endif
        if Not Empty(this.w_BACONASS)
          * --- Esegue Controlli  dati SBF
          if EMPTY(this.w_CAUSBF)
            this.w_MESS = ah_MsgFormat("Causale movimenti C\C SBF per spese non definita nella causale contabile")
            this.w_TESCAU = .T.
          else
            * --- Read from CCC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CCC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CCC_MAST_idx,2],.t.,this.CCC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CAFLCRDE"+;
                " from "+i_cTable+" CCC_MAST where ";
                    +"CACODICE = "+cp_ToStrODBC(this.w_CAUSBF);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CAFLCRDE;
                from (i_cTable) where;
                    CACODICE = this.w_CAUSBF;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SFLCRDE = NVL(cp_ToDate(_read_.CAFLCRDE),cp_NullValue(_read_.CAFLCRDE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from CCC_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CCC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CCC_DETT_idx,2],.t.,this.CCC_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CAIMPCOM"+;
                " from "+i_cTable+" CCC_DETT where ";
                    +"CACODICE = "+cp_ToStrODBC(this.w_CAUSBF);
                    +" and CANUMCOR = "+cp_ToStrODBC(this.w_BACONASS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CAIMPCOM;
                from (i_cTable) where;
                    CACODICE = this.w_CAUSBF;
                    and CANUMCOR = this.w_BACONASS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SIMPCOM = NVL(cp_ToDate(_read_.CAIMPCOM),cp_NullValue(_read_.CAIMPCOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_ROWS=0
              this.w_MESS = ah_MsgFormat("Codice C\C: %1 non congruente alla causale movimenti C\C: %2 indicata nella causale contabile",ALLTRIM(this.w_BACONASS),ALLTRIM(this.w_CAUSBF))
              this.w_TESCAU = .T.
            else
              if this.w_SFLCRDE<>"D"
                this.w_MESS = ah_MsgFormat("Causale movimenti SBF per spese %1 non a debito",Alltrim(this.w_CAUSBF))
                this.w_TESCAU = .T.
              endif
            endif
          endif
        endif
        if this.w_TESCAU=.T.
          this.w_oERRORLOG.AddMsgLogNoTranslate(this.w_MESS)     
          this.w_FLERR = .T.
        endif
      endif
      this.w_PNCOMPET = this.w_PNCODESE
    endif
    this.w_OKANAL = .T.
    this.w_AGGTES = .F.
    * --- Try
    local bErr_026DBAA0
    bErr_026DBAA0=bTrsErr
    this.Try_026DBAA0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg(i_errmsg,,"")
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_026DBAA0
    * --- End
    SELECT (NC)
    * --- Aggiorna Totalizzatori Contabilizzato
    if this.w_AGGTES=.T.
      if this.w_OLDNUMDIS<>this.w_DINUMDIS
        this.w_NDIS = this.w_NDIS + 1
      endif
    endif
    this.w_OLDNUMDIS = this.w_DINUMDIS
    ENDSCAN
  endproc
  proc Try_026DBAA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if Not this.w_TESCAU
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_AGGTES=.T.
      if this.w_OKANAL=.F. OR this.w_SQUAD<>0
        this.w_oERRORLOG.AddMsgLog("Verificare registrazione primanota: %1",STR(this.w_PNNUMRER,6,0))     
        if this.w_OKANAL=.F.
          this.w_oERRORLOG.AddMsgLogPartNoTrans(SPACE(9), "Collegamenti ai c. di costo mancanti")     
        endif
        if this.w_SQUAD<>0
          this.w_oERRORLOG.AddMsgLogPartNoTrans(SPACE(9), "Errore di quadratura (%1 )", ALLTRIM(STR(this.w_SQUAD,18,4)) )     
        endif
        this.w_oERRORLOG.AddMsgLogNoTranslate(SPACE(50))     
        this.w_FLGERR = .T.
      endif
    else
      * --- Distinta non Contabilizzata
      SELECT (NC)
      if this.w_OLDNUMDIS<>this.w_DINUMDIS
        this.w_oERRORLOG.AddMsgLog("Distinta n.: %1/%2 del %3", STR(DINUMERO,6,0), DI__ANNO, DTOC(DIDATDIS))     
      endif
      if this.w_FLERR=.F.
        this.w_oERRORLOG.AddMsgLogPartNoTrans(SPACE(10), "Non esistono partite associate")     
        this.w_oERRORLOG.AddMsgLogNoTranslate(SPACE(50))     
      endif
      this.w_FLGERR = .T.
    endif
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scorre le Partite Della Distinta e Contabilizza
    this.w_AGGTES = .F.
    this.w_AGGCONTROPA = .F.
    SELECT MOVIDETT
    GO TOP
    SCAN FOR PTSERIAL=this.w_DINUMDIS AND ((this.oParentObject.w_CONTSINEFF="S" AND PTNUMEFF=this.w_NUM_EFFETTO) OR this.oParentObject.w_CONTSINEFF="N")
    this.w_AGGCONTROPA = .T.
    if this.w_AGGTES=.F.
      this.w_AGGTES = .T.
      * --- Calcola PNSERIAL e PNNUMRER
      this.w_PNSERIAL = SPACE(10)
      this.w_PNNUMRER = 0
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
      this.w_PNNUMPRO = 0
      this.w_PNPRP = "NN"
      i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
      cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
      cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
      * --- Protocollo (se Gestito)
      if NOT EMPTY(this.w_PNANNPRO)
        cp_NextTableProg(this, i_Conn, "PRPRO", "i_codazi,w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO")
      endif
      this.w_CODVAP = this.w_PNCODVAL
      this.w_CAOVAP = this.w_PNCAOVAL
      this.w_PTDATSCA = CP_TODATE(PTDATSCA)
      this.w_FLCONT = NVL(CAFLCONT,"")
      if NOT EMPTY(this.w_VADTOBSO) AND this.w_VADTOBSO<=this.w_DATCAM
        * --- Vauta EMU Obsoleta ; Converte in EURO
        this.w_CODVAP = g_CODEUR
        this.w_CAOVAP = 1
      endif
      this.w_DESSUP = this.w_PNDESSUP
      if Empty(this.w_DESSUP) and (Not Empty(this.w_CCDESSUP) OR Not Empty(this.w_CCDESRIG))
        * --- Array elenco parametri per descrizioni di riga e testata
         
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_PNNUMDOC,15,0)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=this.w_PNALFDOC 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.w_PNDATDOC) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]=this.w_PNALFPRO 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]="" 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]=ALLTRIM(STR(this.w_PNNUMPRO,15,0)) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=IIF(this.w_FLCONT="S",DTOC(this.w_PTDATSCA),"") 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=""
        this.w_DESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
      endif
      * --- Insert into PNT_MAST
      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PNALFDOC"+",PNALFPRO"+",PNANNDOC"+",PNANNPRO"+",PNCAOVAL"+",PNCODCAU"+",PNCODESE"+",PNCODUTE"+",PNCODVAL"+",PNCOMIVA"+",PNCOMPET"+",PNDATDOC"+",PNDATREG"+",PNDESSUP"+",PNFLIVDF"+",PNFLPROV"+",PNNUMDOC"+",PNNUMPRO"+",PNNUMREG"+",PNNUMRER"+",PNPRD"+",PNPRG"+",PNPRP"+",PNRIFDIS"+",PNSERIAL"+",PNTIPDOC"+",PNTIPREG"+",PNVALNAZ"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PNT_MAST','PNALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFPRO),'PNT_MAST','PNALFPRO');
        +","+cp_NullLink(cp_ToStrODBC("    "),'PNT_MAST','PNANNDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNPRO),'PNT_MAST','PNANNPRO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAOVAP),'PNT_MAST','PNCAOVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAP),'PNT_MAST','PNCODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNCOMIVA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PNT_MAST','PNDATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'PNT_MAST','PNDESSUP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLIVDF),'PNT_MAST','PNFLIVDF');
        +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNFLPROV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PNT_MAST','PNNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMPRO),'PNT_MAST','PNNUMPRO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_MAST','PNNUMREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
        +","+cp_NullLink(cp_ToStrODBC("NN"),'PNT_MAST','PNPRD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRP),'PNT_MAST','PNPRP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DINUMDIS),'PNT_MAST','PNRIFDIS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPDOC),'PNT_MAST','PNTIPDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'PNT_MAST','UTCC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTCV),'PNT_MAST','UTCV');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'PNT_MAST','UTDC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTDV),'PNT_MAST','UTDV');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PNALFDOC',this.w_PNALFDOC,'PNALFPRO',this.w_PNALFPRO,'PNANNDOC',"    ",'PNANNPRO',this.w_PNANNPRO,'PNCAOVAL',this.w_CAOVAP,'PNCODCAU',this.w_PNCODCAU,'PNCODESE',this.w_PNCODESE,'PNCODUTE',this.w_PNCODUTE,'PNCODVAL',this.w_CODVAP,'PNCOMIVA',this.w_PNDATREG,'PNCOMPET',this.w_PNCOMPET,'PNDATDOC',this.w_PNDATDOC)
        insert into (i_cTable) (PNALFDOC,PNALFPRO,PNANNDOC,PNANNPRO,PNCAOVAL,PNCODCAU,PNCODESE,PNCODUTE,PNCODVAL,PNCOMIVA,PNCOMPET,PNDATDOC,PNDATREG,PNDESSUP,PNFLIVDF,PNFLPROV,PNNUMDOC,PNNUMPRO,PNNUMREG,PNNUMRER,PNPRD,PNPRG,PNPRP,PNRIFDIS,PNSERIAL,PNTIPDOC,PNTIPREG,PNVALNAZ,UTCC,UTCV,UTDC,UTDV &i_ccchkf. );
           values (;
             this.w_PNALFDOC;
             ,this.w_PNALFPRO;
             ,"    ";
             ,this.w_PNANNPRO;
             ,this.w_CAOVAP;
             ,this.w_PNCODCAU;
             ,this.w_PNCODESE;
             ,this.w_PNCODUTE;
             ,this.w_CODVAP;
             ,this.w_PNDATREG;
             ,this.w_PNCOMPET;
             ,this.w_PNDATDOC;
             ,this.w_PNDATREG;
             ,this.w_DESSUP;
             ,this.w_PNFLIVDF;
             ,"N";
             ,this.w_PNNUMDOC;
             ,this.w_PNNUMPRO;
             ,this.w_PNNUMREG;
             ,this.w_PNNUMRER;
             ,"NN";
             ,this.w_PNPRG;
             ,this.w_PNPRP;
             ,this.w_DINUMDIS;
             ,this.w_PNSERIAL;
             ,this.w_PNTIPDOC;
             ,this.w_PNTIPREG;
             ,this.w_PNVALNAZ;
             ,this.w_UTCC;
             ,this.w_UTCV;
             ,this.w_UTDC;
             ,this.w_UTDV;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Aggiorna Riferimenti Contabilizzazione
      * --- Write into DIS_TINT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DIS_TINT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_TINT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DIRIFCON ="+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'DIS_TINT','DIRIFCON');
            +i_ccchkf ;
        +" where ";
            +"DINUMDIS = "+cp_ToStrODBC(this.w_DINUMDIS);
               )
      else
        update (i_cTable) set;
            DIRIFCON = this.w_PNSERIAL;
            &i_ccchkf. ;
         where;
            DINUMDIS = this.w_DINUMDIS;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_PGEN = this.w_PGEN + 1
      * --- Se Trovato Costruisce la Struttura:
      * --- Calcola i Totali dalla Distinta in Valuta P.N.
      this.w_IMPCLF = 0
      this.w_IMPBAN = 0
      this.w_IMPDCA = 0
      this.w_IMPDCP = 0
      this.w_IMPEFF = 0
      this.w_APPO = 0
      this.w_PARROW = 0
      this.w_SALROW = 0
      this.w_OLDORD = 100
      this.w_TIPCON = "X"
      this.w_CODCON = SPACE(15)
      this.w_CPROWORD = 100
      this.w_OFLINDI = " "
    endif
    * --- Scorre tutte le Partite Congruenti e Totalizza gli Importi
    SELECT MOVIDETT
    this.w_UPDATE = .F.
    this.w_NUMERO = CPROWNUM
    this.w_FLINDI = IIF(EMPTY(NVL(FLINDI, 0)), " ", "S")
    this.w_CODVAL = IIF(EMPTY(NVL(PTCODVAL," ")), g_PERVAL, PTCODVAL)
    this.w_CAOVAL = IIF(NVL(PTCAOVAL,0)=0, this.w_CAONAZ, PTCAOVAL)
    this.w_DATAPE = CP_TODATE(PTDATAPE)
    this.w_EURVAL = NVL(VACAOVAL, 0)
    this.w_APPO = NVL(PTTOTIMP,0)*IIF(PT_SEGNO="D" ,-1, 1)
    if this.w_CODVAL<>this.w_PNVALNAZ
      this.w_APPO = VAL2MON(this.w_APPO, this.w_CAOVAL, this.w_CAONAZ, this.w_DATAPE, this.w_PNVALNAZ)
    endif
    * --- Se Valuta EMU e data valuta successiva al 31-12-02 gli importi sono gia' automaticamente convertiti sulla distinta (compresi i decimali)
    this.w_IMPBAN = this.w_IMPBAN + cp_ROUND(this.w_APPO, IIF(this.w_EURVAL<>0 AND this.w_DATCAM>cp_CharToDate("31-12-2001"), g_PERPVL, 6))
    * --- Calcola Differenza Cambio (Passiva in Dare; Attiva in Avere) (PTCAOVAL=C.Apertura; w_CAOVAC=C.Chiusura)
    if this.w_CODVAL<>this.w_PNVALNAZ
      this.w_APPO1 = NVL(PTTOTIMP,0)
      if this.w_EURVAL=0 OR this.w_DATCAM<GETVALUT(g_PERVAL, "VADATEUR")
        * --- Fase Pre EURO o Valuta non EURO; Calcola Differenze Cambi
        this.w_VAL1 = VAL2MON(this.w_APPO1, this.w_CAOVAL, this.w_CAONAZ, this.w_DATAPE, this.w_PNVALNAZ)
        this.w_VAL2 = VAL2MON(this.w_APPO1, this.w_CAOVAC, this.w_CAONAZ, this.w_DATCAM, this.w_PNVALNAZ)
        this.w_APPO1 = cp_ROUND(this.w_VAL1 - this.w_VAL2, this.w_PERPVL)
      else
        * --- Differenze Cambi calcolati il 31-12-99
        this.w_APPO1 = 0
      endif
      if PT_SEGNO="A"
        this.w_IMPDCA = this.w_IMPDCA - IIF(this.w_APPO1>0, 0, ABS(this.w_APPO1))
        this.w_IMPDCP = this.w_IMPDCP + IIF(this.w_APPO1>0, ABS(this.w_APPO1), 0)
      else
        this.w_IMPDCA = this.w_IMPDCA - IIF(this.w_APPO1>0, ABS(this.w_APPO1), 0)
        this.w_IMPDCP = this.w_IMPDCP + IIF(this.w_APPO1>0, 0, ABS(this.w_APPO1))
      endif
    endif
    * --- Dettaglia Righe Cli/For
    if this.pParam="S"
      * --- Se distinta parziale eseguo rottura anche per data scadenza e numero effetto
      this.w_COND = this.w_TIPCON<>PTTIPCON OR this.w_CODCON<>PTCODCON OR this.w_OFLINDI<>this.w_FLINDI OR Cp_Todate(PTDATSCA)<>this.w_DATSCA 
    else
      this.w_COND = this.w_TIPCON<>PTTIPCON OR this.w_CODCON<>PTCODCON OR this.w_OFLINDI<>this.w_FLINDI
    endif
    if this.w_COND
      * --- Nuovo Conto o passo da Contab. Diretta a Indiretta
      * --- Aggiorna Importo Riga Clienti/Fornitori Precedente
      if this.w_IMPCLF<>0
        this.w_IMPCLF = cp_ROUND(this.w_IMPCLF, this.w_PERPVL)
        this.w_PNIMPDAR = IIF(this.w_IMPCLF>0, this.w_IMPCLF, 0)
        this.w_PNIMPAVE = IIF(this.w_IMPCLF<0, ABS(this.w_IMPCLF), 0)
        * --- Se importo a zero marco la riga (caso in cui effetto raggruppato attivo e passivo)
        this.w_FLZERO = IIF( this.w_IMPCLF=0 , "S", " " )
        * --- Write into PNT_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PNT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNIMPDAR ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
          +",PNIMPAVE ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
          +",PNDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
          +",PNFLZERO ="+cp_NullLink(cp_ToStrODBC(this.w_FLZERO),'PNT_DETT','PNFLZERO');
              +i_ccchkf ;
          +" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_SALROW);
                 )
        else
          update (i_cTable) set;
              PNIMPDAR = this.w_PNIMPDAR;
              ,PNIMPAVE = this.w_PNIMPAVE;
              ,PNDESRIG = this.w_PNDESRIG;
              ,PNFLZERO = this.w_FLZERO;
              &i_ccchkf. ;
           where;
              PNSERIAL = this.w_PNSERIAL;
              and CPROWNUM = this.w_SALROW;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_SQUAD = this.w_SQUAD + (this.w_PNIMPDAR-this.w_PNIMPAVE)
        * --- Aggiorna Saldo
        * --- Write into SALDICON
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
          i_cOp3=cp_SetTrsOp(this.w_PNFLSALI,'SLDARINI','this.w_PNIMPDAR',this.w_PNIMPDAR,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_PNFLSALI,'SLAVEINI','this.w_PNIMPAVE',this.w_PNIMPAVE,'update',i_nConn)
          i_cOp5=cp_SetTrsOp(this.w_PNFLSALF,'SLDARFIN','this.w_PNIMPDAR',this.w_PNIMPDAR,'update',i_nConn)
          i_cOp6=cp_SetTrsOp(this.w_PNFLSALF,'SLAVEFIN','this.w_PNIMPAVE',this.w_PNIMPAVE,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
          +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
          +",SLDARINI ="+cp_NullLink(i_cOp3,'SALDICON','SLDARINI');
          +",SLAVEINI ="+cp_NullLink(i_cOp4,'SALDICON','SLAVEINI');
          +",SLDARFIN ="+cp_NullLink(i_cOp5,'SALDICON','SLDARFIN');
          +",SLAVEFIN ="+cp_NullLink(i_cOp6,'SALDICON','SLAVEFIN');
              +i_ccchkf ;
          +" where ";
              +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
              +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
                 )
        else
          update (i_cTable) set;
              SLDARPER = SLDARPER + this.w_PNIMPDAR;
              ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
              ,SLDARINI = &i_cOp3.;
              ,SLAVEINI = &i_cOp4.;
              ,SLDARFIN = &i_cOp5.;
              ,SLAVEFIN = &i_cOp6.;
              &i_ccchkf. ;
           where;
              SLTIPCON = this.w_PNTIPCON;
              and SLCODICE = this.w_PNCODCON;
              and SLCODESE = this.w_PNCODESE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Inizializza Totali
      this.w_IMPCLF = 0
      this.w_OLDORD = this.w_CPROWORD
      SELECT MOVIDETT
      this.w_TIPCON = NVL(PTTIPCON,"")
      this.w_CODCON = NVL(PTCODCON,"")
      this.w_DATSCA = Cp_Todate(PTDATSCA)
      * --- Scrive nuova Riga Clienti/Fornitori
      this.w_PNTIPCON = NVL(PTTIPCON,"")
      this.w_PNCODCON = NVL(PTCODCON,"")
      this.w_PSERIAL = NVL(PTSERRIF,SPACE(10))
      this.w_PROWORD = NVL(PTORDRIF,0)
      if Empty(NVL(ANCODPAG, SPACE(5)))
        * --- Nel caso in cui nel conto non � stato valorizzato il pagamento
        *     lo ricerco nella registrazione di origine
        if Not Empty(this.w_PSERIAL)
          * --- Read from PNT_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PNT_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2],.t.,this.PNT_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PNCODPAG"+;
              " from "+i_cTable+" PNT_DETT where ";
                  +"PNSERIAL = "+cp_ToStrODBC(this.w_PSERIAL);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_PROWORD);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PNCODPAG;
              from (i_cTable) where;
                  PNSERIAL = this.w_PSERIAL;
                  and CPROWNUM = this.w_PROWORD;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PNCODPAG = NVL(cp_ToDate(_read_.PNCODPAG),cp_NullValue(_read_.PNCODPAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      else
        this.w_PNCODPAG = NVL(ANCODPAG, SPACE(5))
      endif
      this.w_PNCODAGE = NVL(PTCODAGE, SPACE(5))
      this.w_PNFLVABD = NVL(PTFLVABD, " ")
      this.w_PNFLPART = "S"
      this.w_APPO = 0
      if this.w_FLINDI<>"S"
        * --- Se contabilizzazione Indiretta non c'e' piu' ilCli/For da Inserire ma il Conto Effetti
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      SELECT MOVIDETT
      * --- Salva Il Puntatore alla Riga P.N. per aggiornare alla Fine il Totale di Riga
      this.w_SALROW = this.w_CPROWNUM
      * --- Riazzera Progressivo Partite
      this.w_PARROW = 0
    endif
    this.w_OFLINDI = this.w_FLINDI
    * --- Totalizza Importo Riga Cliente/Fornitore o Importo Effetti Attivi se da Distinta di Contab.Indiretta
    this.w_APPO = NVL(PTTOTIMP,0)*IIF(PT_SEGNO="D" ,1, -1)
    if this.w_CODVAL<>this.w_PNVALNAZ
      this.w_APPO = VAL2MON(this.w_APPO, this.w_CAOVAL, this.w_CAONAZ, this.w_DATAPE, this.w_PNVALNAZ)
      SELECT MOVIDETT
    endif
    if this.w_FLINDI="S"
      if this.oParentObject.w_FLCOEFF="S"
        * --- Importo Effetti Attivi (se Distinta Effetti a Contabilizzazione Indiretta)
        if this.w_APPO<>0
          this.w_LNUMPAR = PTNUMPAR
          this.w_LTIPCON = PTTIPCON
          this.w_LCODCON = PTCODCON
          this.w_LCODVAL = PTCODVAL
          this.w_LDATSCA = CP_TODATE(PTDATSCA)
          this.w_LSERIAL = NVL(PTSERRIF,SPACE(10))
          this.w_LIMPAVE = 0
          this.w_LIMPDAR = 0
          if Empty(this.w_LSERIAL)
            * --- Se partite del pregresso devo ricavare il seriale della partita di creazione
            * --- Read from PAR_TITE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PTSERIAL"+;
                " from "+i_cTable+" PAR_TITE where ";
                    +"PTNUMPAR = "+cp_ToStrODBC(this.w_LNUMPAR);
                    +" and PTDATSCA = "+cp_ToStrODBC(this.w_LDATSCA);
                    +" and PTTIPCON = "+cp_ToStrODBC(this.w_LTIPCON);
                    +" and PTCODCON = "+cp_ToStrODBC(this.w_LCODCON);
                    +" and PTCODVAL = "+cp_ToStrODBC(this.w_LCODVAL);
                    +" and PTROWORD = "+cp_ToStrODBC(-3);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PTSERIAL;
                from (i_cTable) where;
                    PTNUMPAR = this.w_LNUMPAR;
                    and PTDATSCA = this.w_LDATSCA;
                    and PTTIPCON = this.w_LTIPCON;
                    and PTCODCON = this.w_LCODCON;
                    and PTCODVAL = this.w_LCODVAL;
                    and PTROWORD = -3;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_LSERIAL = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          * --- Leggo conto Effetti specificato nella contabilizzazione Indiretta
          * --- Read from CON_INDI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CON_INDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CON_INDI_idx,2],.t.,this.CON_INDI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CICONEFA"+;
              " from "+i_cTable+" CON_INDI where ";
                  +"CISERIAL = "+cp_ToStrODBC(this.w_LSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CICONEFA;
              from (i_cTable) where;
                  CISERIAL = this.w_LSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LCONEFA = NVL(cp_ToDate(_read_.CICONEFA),cp_NullValue(_read_.CICONEFA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_LCONEFA = IIF(Not Empty(this.w_LCONEFA),this.w_LCONEFA,this.oParentObject.w_CONEFA)
          this.w_PNCODCON = this.w_LCONEFA
          this.w_PNTIPCON = "G"
          this.w_PNFLPART = "N"
          * --- Verifico la presenza di una stessa riga nella registrazione con lo stesso conto 
          *     in tal caso useguo un Update dell'importo e relativo aggiornamento dei Saldi
          * --- Read from PNT_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PNT_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2],.t.,this.PNT_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CPROWNUM,PNFLZERO,PNIMPDAR,PNIMPAVE"+;
              " from "+i_cTable+" PNT_DETT where ";
                  +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                  +" and PNCODCON = "+cp_ToStrODBC(this.w_PNCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CPROWNUM,PNFLZERO,PNIMPDAR,PNIMPAVE;
              from (i_cTable) where;
                  PNSERIAL = this.w_PNSERIAL;
                  and PNCODCON = this.w_PNCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
            this.w_FLZERO = NVL(cp_ToDate(_read_.PNFLZERO),cp_NullValue(_read_.PNFLZERO))
            this.w_LIMPDAR = NVL(cp_ToDate(_read_.PNIMPDAR),cp_NullValue(_read_.PNIMPDAR))
            this.w_LIMPAVE = NVL(cp_ToDate(_read_.PNIMPAVE),cp_NullValue(_read_.PNIMPAVE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_UPDATE = this.w_LROWNUM>0
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.w_PTROWNUM = this.w_LROWNUM+1
      else
        this.w_IMPEFF = this.w_IMPEFF + this.w_APPO
      endif
      * --- In realta' annulla le Scadenze associate alla Registrazione di Contab.Indiretta
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.w_IMPCLF = this.w_IMPCLF + this.w_APPO
      * --- Scrive la Partita di Chiusura Associata al Cliente/Fornitore
      this.w_PARROW = this.w_PARROW + 1
      this.w_PTROWORD = this.w_SALROW
      this.w_PTROWNUM = this.w_PARROW
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_PNCODAGE = SPACE(5)
      this.w_PNFLVABD = " "
    endif
    if Not EMPTY(this.w_CCDESRIG) and Empty(this.w_PNDESSUP)
       
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_PTNUMDOC,15,0)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=this.w_PTALFDOC 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.w_PTDATDOC) 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]=ALLTRIM(this.w_PNCODAGE) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]=Alltrim(this.w_PTNUMPAR) 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]=DTOC(this.w_PTDATSCA)
      this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
    else
      this.w_PNDESRIG = this.w_PNDESSUP
    endif
    * --- Aggiorno campo PTRIFIND nelle distinte
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTFLIMPE ="+cp_NullLink(cp_ToStrODBC("CG"),'PAR_TITE','PTFLIMPE');
      +",PTRIFIND ="+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTRIFIND');
          +i_ccchkf ;
      +" where ";
          +"PTSERIAL = "+cp_ToStrODBC(this.w_DINUMDIS);
          +" and PTROWORD = "+cp_ToStrODBC(-2);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMERO);
             )
    else
      update (i_cTable) set;
          PTFLIMPE = "CG";
          ,PTRIFIND = this.w_PNSERIAL;
          &i_ccchkf. ;
       where;
          PTSERIAL = this.w_DINUMDIS;
          and PTROWORD = -2;
          and CPROWNUM = this.w_NUMERO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    SELECT MOVIDETT
    ENDSCAN
    if this.w_AGGCONTROPA
      * --- Esegue la round alla Fine per non perdere scarti di arrotondamento
      this.w_IMPBAN = cp_ROUND(this.w_IMPBAN, this.w_PERPVL)
      this.w_IMPEFF = cp_ROUND(this.w_IMPEFF, this.w_PERPVL)
      * --- Dettaglia Righe Cli/For (Chiude ultima Riga)
      if this.w_IMPCLF<>0
        this.w_IMPCLF = cp_ROUND(this.w_IMPCLF, this.w_PERPVL)
        this.w_PNIMPDAR = IIF(this.w_IMPCLF>0, this.w_IMPCLF, 0)
        this.w_PNIMPAVE = IIF(this.w_IMPCLF<0, ABS(this.w_IMPCLF), 0)
        * --- Se importo a zero marco la riga (caso in cui effetto raggruppato attivo e passivo)
        this.w_FLZERO = IIF( this.w_IMPCLF=0 , "S", " " )
        * --- Write into PNT_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PNT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNIMPDAR ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
          +",PNIMPAVE ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
          +",PNDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
          +",PNFLZERO ="+cp_NullLink(cp_ToStrODBC(this.w_FLZERO),'PNT_DETT','PNFLZERO');
              +i_ccchkf ;
          +" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_SALROW);
                 )
        else
          update (i_cTable) set;
              PNIMPDAR = this.w_PNIMPDAR;
              ,PNIMPAVE = this.w_PNIMPAVE;
              ,PNDESRIG = this.w_PNDESRIG;
              ,PNFLZERO = this.w_FLZERO;
              &i_ccchkf. ;
           where;
              PNSERIAL = this.w_PNSERIAL;
              and CPROWNUM = this.w_SALROW;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_SQUAD = this.w_SQUAD + (this.w_PNIMPDAR-this.w_PNIMPAVE)
        * --- Aggiorna Saldi
        * --- Write into SALDICON
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
          i_cOp3=cp_SetTrsOp(this.w_PNFLSALI,'SLDARINI','this.w_PNIMPDAR',this.w_PNIMPDAR,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_PNFLSALI,'SLAVEINI','this.w_PNIMPAVE',this.w_PNIMPAVE,'update',i_nConn)
          i_cOp5=cp_SetTrsOp(this.w_PNFLSALF,'SLDARFIN','this.w_PNIMPDAR',this.w_PNIMPDAR,'update',i_nConn)
          i_cOp6=cp_SetTrsOp(this.w_PNFLSALF,'SLAVEFIN','this.w_PNIMPAVE',this.w_PNIMPAVE,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
          +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
          +",SLDARINI ="+cp_NullLink(i_cOp3,'SALDICON','SLDARINI');
          +",SLAVEINI ="+cp_NullLink(i_cOp4,'SALDICON','SLAVEINI');
          +",SLDARFIN ="+cp_NullLink(i_cOp5,'SALDICON','SLDARFIN');
          +",SLAVEFIN ="+cp_NullLink(i_cOp6,'SALDICON','SLAVEFIN');
              +i_ccchkf ;
          +" where ";
              +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
              +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
                 )
        else
          update (i_cTable) set;
              SLDARPER = SLDARPER + this.w_PNIMPDAR;
              ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
              ,SLDARINI = &i_cOp3.;
              ,SLAVEINI = &i_cOp4.;
              ,SLDARFIN = &i_cOp5.;
              ,SLAVEFIN = &i_cOp6.;
              &i_ccchkf. ;
           where;
              SLTIPCON = this.w_PNTIPCON;
              and SLCODICE = this.w_PNCODCON;
              and SLCODESE = this.w_PNCODESE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Scrive Reg.Contabili (Movimentazione)
      this.w_CPROWORD = 0
      this.w_PNFLPART = "N"
      this.w_PNTIPCON = "G"
      this.w_PNCODAGE = SPACE(5)
      this.w_PNFLVABD = " "
      this.w_PNCODPAG = SPACE(5)
      * --- Se Distinta Cambiale/Tratte calcola eventuale Sconto Tratte (Stessa sezione Banca)
      this.w_IMPCOT = 0
      if this.w_TIPDIS="CA" AND this.w_SCOTRA<>0
        this.w_IMPCOT = cp_ROUND((this.w_IMPBAN*this.w_SCOTRA)/100, this.w_PERPVL)
      endif
      * --- Eventuale Scarto da Cessione
      if this.w_IMPCES<>0
        if this.w_PNCODVAL<>this.w_PNVALNAZ
          this.w_IMPCES = VAL2MON(this.w_IMPCES, this.w_PNCAOVAL, this.w_CAONAZ, this.w_DATCAM, this.w_PNVALNAZ, this.w_PERPVL)
        endif
        * --- Stesso Segno della Banca
        this.w_IMPCES = ABS(this.w_IMPCES) * IIF(this.w_IMPBAN<0, -1, 1)
      endif
      * --- Importo Prima riga: Totale Distinta - (Commissioni + Sconto Tratte + Diff.Cambio Attiva e Passiva)
      this.w_IMPBAN = this.w_IMPBAN - (this.w_IMPCOM + this.w_IMPCOT + this.w_IMPDCA + this.w_IMPDCP + this.w_IMPCES)
      * --- Importo Effetti Attivi (se Distinta Effetti a Contabilizzazione Indiretta)
      if this.w_IMPEFF<>0 AND this.oParentObject.w_FLCOEFF<>"S"
        * --- Se non specificata opzione conto effetti inserisco riga unica con il conto effetti
        *     presente nei paramentri Distinte
        this.w_PNCODCON = this.oParentObject.w_CONEFA
        this.w_APPO = this.w_IMPEFF
        this.w_UPDATE = .F.
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- A Credito: Dare / a Debito: Avere
      if this.w_IMPBAN<>0
        this.w_PNCODCON = this.w_DIBANRIF
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANPARTSN,ANCODPAG"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANPARTSN,ANCODPAG;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCON;
                and ANCODICE = this.w_PNCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PARTSN = NVL(cp_ToDate(_read_.ANPARTSN),cp_NullValue(_read_.ANPARTSN))
          this.w_PNCODPAG = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Leggo il conto corrente associato al conto SBF.
        * --- Read from COC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BACONSBF,BACONASS"+;
            " from "+i_cTable+" COC_MAST where ";
                +"BACODBAN = "+cp_ToStrODBC(this.w_CONBANC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BACONSBF,BACONASS;
            from (i_cTable) where;
                BACODBAN = this.w_CONBANC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_BACONSBF = NVL(cp_ToDate(_read_.BACONSBF),cp_NullValue(_read_.BACONSBF))
          this.w_BACONASS = NVL(cp_ToDate(_read_.BACONASS),cp_NullValue(_read_.BACONASS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Se il conto � SBF, l'importo delle spese di commissioni � <>0 e gestisce le partite, aggiungo le spese al totimp.
        if this.w_BACONSBF="S" AND this.w_PARTSN="S"
          this.w_APPO = this.w_IMPBAN+this.w_IMPCOM
          this.w_PNFLPART = "C"
        else
          this.w_APPO = this.w_IMPBAN
          this.w_PNFLPART = "N"
        endif
        this.w_UPDATE = .F.
        this.w_PSERIAL = NVL(MOVIDETT.PTSERRIF,SPACE(10))
        this.w_PROWORD = NVL(MOVIDETT.PTORDRIF,0)
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_LCONBAN = this.w_CAUMOV
        this.w_LCONTO = this.w_CONBANC
        this.w_ORDBAN = 0
        this.w_ROWBAN = 0
        this.w_LIMPCOM = 0
        this.w_LIMPCOM = this.w_IMPCOM
        * --- Creo Tante righe di Conto Corrente in base alla Data Valuta Indicata nelle Partite generate con la Distinta
         
 Select PTDATVAL,SUM(iif(nvl(PT_SEGNO," ")="A",-1,1)*PTTOTIMP) AS TOTIMP FROM MOVIDETT where PTSERIAL=this.w_DINUMDIS AND ((this.oParentObject.w_CONTSINEFF="S" AND PTNUMEFF=this.w_NUM_EFFETTO) OR this.oParentObject.w_CONTSINEFF="N"); 
 Group By PTDATVAL Order By PTDATVAL INTO CURSOR CCORRENTI
         
 Select CCORRENTI 
 Go Top 
 Scan For Nvl(totimp,0)<>0
        this.w_LDATVAL = cp_todate(PTDATVAL)
        this.w_PNIMPDAR = IIF(Nvl(TOTIMP,0)<0,Abs(Nvl(TOTIMP,0)),0)
        this.w_PNIMPAVE = IIF(Nvl(TOTIMP,0)>0,Nvl(TOTIMP,0),0)
        this.w_CCFLCRED = IIF(this.w_FLCRDE="C", "+", " ")
        this.w_CCFLDEBI = IIF(this.w_FLCRDE="D", "+", " ")
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_LIMPCOM = 0
        * --- Le commissioni le addebito solo sulla prima riga di movimenti C\C
         
 endscan
         
 Select CCORRENTI 
 Use
        this.w_PNFLPART = "N"
        * --- Se il conto � di tipo Salvo Buon Fine, e gestito a partite vado a scrivere le partite.
        if this.w_PARTSN="S" and this.w_BACONSBF="S" 
          this.w_TESTPAR = "G"
          this.w_PTROWNUM = 0
          this.w_PTROWORD = this.w_CPROWNUM
          this.w_BANRIF = this.w_DIBANRIF
          * --- Leggo il conto contabile associato al conto corrente letto dalla precedente Read
          * --- Read from COC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "BACONCOL"+;
              " from "+i_cTable+" COC_MAST where ";
                  +"BACODBAN = "+cp_ToStrODBC(this.w_BACONASS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              BACONCOL;
              from (i_cTable) where;
                  BACODBAN = this.w_BACONASS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CONCOL = NVL(cp_ToDate(_read_.BACONCOL),cp_NullValue(_read_.BACONCOL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
           
 SELECT MOVIDETT 
 GO TOP 
 SCAN FOR PTSERIAL=this.w_DINUMDIS AND ((this.oParentObject.w_CONTSINEFF="S" AND PTNUMEFF=this.w_NUM_EFFETTO) OR this.oParentObject.w_CONTSINEFF="N")
          this.w_PTROWNUM = this.w_PTROWNUM+1
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          EndScan
          this.w_TESTPAR = " "
        endif
      endif
      this.w_PNCODPAG = SPACE(5)
      * --- Scarto da Cessione
      if this.w_IMPCES<>0
        this.w_PNCODCON = this.oParentObject.w_CONCES
        this.w_APPO = this.w_IMPCES
        this.w_UPDATE = .F.
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Sconto Tratte
      if this.w_IMPCOT<>0
        this.w_PNCODCON = this.oParentObject.w_CONSCO
        this.w_APPO = this.w_IMPCOT
        this.w_UPDATE = .F.
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Commissioni
      if this.w_IMPCOM<>0
        this.w_PNCODCON = this.oParentObject.w_CONBAN
        this.w_APPO = this.w_IMPCOM
        this.w_UPDATE = .F.
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- C/C Associato per compensazione spese in registrazioni con Conti Salvo Buon Fine.
      if this.w_BACONSBF="S" AND this.w_IMPCOM<>0 AND this.w_PARTSN="S"
        this.w_PNCODCON = this.w_CONCOL
        * --- Cambio il segno del conto salvo buon fine per avere le spese nella sezione opposta.
        this.w_APPO = -this.w_IMPCOM
        this.w_UPDATE = .F.
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_LCONBAN = this.w_CAUSBF
        this.w_LCONTO = this.w_BACONASS
        this.w_LDATVAL = cp_CharToDate("    -  -  ")
        this.w_ORDBAN = 0
        this.w_ROWBAN = 0
        this.w_LIMPCOM = this.w_IMPCOM
        this.w_CCFLCRED = IIF(this.w_SFLCRDE="C", "+", " ")
        this.w_CCFLDEBI = IIF(this.w_SFLCRDE="D", "+", " ")
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Differenza Cambi Attiva
      if this.w_IMPDCA<>0
        this.w_PNCODCON = this.oParentObject.w_CONDCA
        this.w_APPO = this.w_IMPDCA
        this.w_UPDATE = .F.
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Differenza Cambi Passiva
      if this.w_IMPDCP<>0
        this.w_PNCODCON = this.oParentObject.w_CONDCP
        this.w_APPO = this.w_IMPDCP
        this.w_UPDATE = .F.
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_SQUAD = cp_ROUND(this.w_SQUAD, this.w_PERPVL)
      if this.w_SQUAD<>0 AND ((this.w_PNVALNAZ=g_CODEUR AND ABS(this.w_SQUAD)<1) OR (this.w_PNVALNAZ=g_CODLIR AND ABS(this.w_SQUAD)<100))
        this.w_PNCODCON = this.oParentObject.w_DIFCON
        this.w_APPO = -this.w_SQUAD
        this.w_UPDATE = .F.
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if Isalt() and Not Empty(this.w_PNSERIAL)
        gsal_baf(this,this.w_PNSERIAL,"P",Cp_chartodate("  -  -    "),Cp_chartodate("  -  -    "),"A")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Dettaglio Primanota e Saldi
    this.w_SLIMPDAR = 0
    this.w_SLIMPAVE = 0
    if Not this.w_UPDATE
      this.w_PNIMPDAR = IIF(this.w_APPO>0, this.w_APPO, 0)
      this.w_PNIMPAVE = IIF(this.w_APPO<0, ABS(this.w_APPO), 0)
      * --- Se importo a zero marco la riga (caso in cui effetto raggruppato attivo e passivo)
      this.w_FLZERO = IIF( this.w_APPO=0 , "S", " " )
      this.w_CPROWNUM = this.w_CPROWNUM + 1
      this.w_CPROWORD = this.w_CPROWORD + 10
      * --- Try
      local bErr_03D9D350
      bErr_03D9D350=bTrsErr
      this.Try_03D9D350()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_Msg(MESSAGE(),.T.)
        * --- Raise
        i_Error="Impossibile inserire movimento in prima nota"
        return
      endif
      bTrsErr=bTrsErr or bErr_03D9D350
      * --- End
    else
      * --- Agli importi originali sommo il nuovo e ridetermino gli importi di riga..
      this.w_SLIMPDAR = IIF(this.w_APPO>0, this.w_APPO, 0)
      this.w_SLIMPAVE = IIF(this.w_APPO<0, ABS(this.w_APPO), 0)
      this.w_SQUAD = this.w_SQUAD + this.w_APPO
      this.w_APPO = this.w_APPO + (this.w_LIMPDAR-this.w_LIMPAVE)
      this.w_PNIMPDAR = IIF(this.w_APPO>0, this.w_APPO, 0)
      this.w_PNIMPAVE = IIF(this.w_APPO<0, ABS(this.w_APPO), 0)
      this.w_FLZERO = IIF( this.w_APPO=0 , "S", " " )
      * --- Write into PNT_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PNIMPAVE ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
        +",PNIMPDAR ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
        +",PNFLZERO ="+cp_NullLink(cp_ToStrODBC(this.w_FLZERO),'PNT_DETT','PNFLZERO');
            +i_ccchkf ;
        +" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_LROWNUM);
               )
      else
        update (i_cTable) set;
            PNIMPAVE = this.w_PNIMPAVE;
            ,PNIMPDAR = this.w_PNIMPDAR;
            ,PNFLZERO = this.w_FLZERO;
            &i_ccchkf. ;
         where;
            PNSERIAL = this.w_PNSERIAL;
            and CPROWNUM = this.w_LROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Importi per aggiornare i saldi
      this.w_PNIMPDAR = this.w_SLIMPDAR
      this.w_PNIMPAVE = this.w_SLIMPAVE
    endif
    * --- Aggiorna Saldo
    * --- Try
    local bErr_03D950A0
    bErr_03D950A0=bTrsErr
    this.Try_03D950A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03D950A0
    * --- End
    * --- Write into SALDICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
      +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
          +i_ccchkf ;
      +" where ";
          +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
          +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
          +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
             )
    else
      update (i_cTable) set;
          SLDARPER = SLDARPER + this.w_PNIMPDAR;
          ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
          &i_ccchkf. ;
       where;
          SLTIPCON = this.w_PNTIPCON;
          and SLCODICE = this.w_PNCODCON;
          and SLCODESE = this.w_PNCODESE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Gestione Analitica
    if this.w_CCFLANAL="S" And this.w_PNTIPCON="G" And g_PERCCR="S" AND Not this.w_UPDATE
      this.w_MESS_ERR = GSAR_BRA(This,this.w_PNSERIAL, this.w_CPROWNUM, this.w_PNTIPCON, this.w_PNCODCON, this.w_APPO)
      if Not EMPTY(this.w_MESS_ERR)
        this.w_oERRORLOG.AddMsgLogPartNoTrans(Alltrim(this.w_Mess_Err) )     
        this.w_oERRORLOG.AddMsgLogNoTranslate(Space(50))     
      endif
    endif
  endproc
  proc Try_03D9D350()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNCAURIG"+",PNFLSALD"+",PNFLSALF"+",PNINICOM"+",PNFINCOM"+",PNFLSALI"+",PNCODPAG"+",PNCODAGE"+",PNFLVABD"+",PNFLZERO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PNT_DETT','PNFLPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_DETT','PNCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNINICOM),'PNT_DETT','PNINICOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFINCOM),'PNT_DETT','PNFINCOM');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODPAG),'PNT_DETT','PNCODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODAGE),'PNT_DETT','PNCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLVABD),'PNT_DETT','PNFLVABD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLZERO),'PNT_DETT','PNFLZERO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_PNTIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLPART',this.w_PNFLPART,'PNCAURIG',this.w_PNCODCAU,'PNFLSALD',this.w_PNFLSALD,'PNFLSALF'," ",'PNINICOM',this.w_PNINICOM)
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNCAURIG,PNFLSALD,PNFLSALF,PNINICOM,PNFINCOM,PNFLSALI,PNCODPAG,PNCODAGE,PNFLVABD,PNFLZERO &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNIMPDAR;
           ,this.w_PNIMPAVE;
           ,this.w_PNFLPART;
           ,this.w_PNCODCAU;
           ,this.w_PNFLSALD;
           ," ";
           ,this.w_PNINICOM;
           ,this.w_PNFINCOM;
           ," ";
           ,this.w_PNCODPAG;
           ,this.w_PNCODAGE;
           ,this.w_PNFLVABD;
           ,this.w_FLZERO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_SQUAD = this.w_SQUAD + (this.w_PNIMPDAR-this.w_PNIMPAVE)
    return
  proc Try_03D950A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.w_PNCODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNCODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive le Partite associate alla Reg. di Primanota
    * --- Partite da Movimenti Distinta, Insoluto, Saldaconto (ATTENZIONE: Siamo Posizionati dentro il Cursore MOVIDETT)
    * --- Dati per Chusura Partite
    this.w_PTNUMPAR = PTNUMPAR
    if this.w_TESTPAR="G"
      this.w_PTTIPCON = this.w_PNTIPCON
      this.w_PTCODCON = this.w_BANRIF
      this.w_PTFLCRSA = "C"
      this.w_PT_SEGNO = IIF(PT_SEGNO="D", "A", "D")
      this.w_PTDATSCA = IIF(Not Empty(CP_TODATE(PTDATVAL)),CP_TODATE(PTDATVAL),CP_TODATE(PTDATSCA))
      this.w_PTBANNOS = IIF(Not Empty(this.w_BACONASS),this.w_BACONASS,PTBANNOS)
      this.w_OSERIAL = SPACE(10)
      this.w_OROWORD = 0
      this.w_OROWNUM = 0
    else
      this.w_PTDATSCA = CP_TODATE(PTDATSCA)
      this.w_PTTIPCON = PTTIPCON
      this.w_PTCODCON = PTCODCON
      this.w_PTFLCRSA = "S"
      this.w_PT_SEGNO = PT_SEGNO
      this.w_PTBANNOS = PTBANNOS
      this.w_OSERIAL = NVL(PTSERRIF,SPACE(10))
      this.w_OROWORD = NVL(PTORDRIF,0)
      this.w_OROWNUM = NVL(PTNUMRIF,0)
    endif
    this.w_PTTOTIMP = PTTOTIMP
    this.w_PTCODVAL = PTCODVAL
    this.w_PTCAOVAL = this.w_PNCAOVAL
    this.w_PTCAOAPE = PTCAOAPE
    this.w_PTMODPAG = PTMODPAG
    this.w_PTIMPDOC = PTIMPDOC
    this.w_PTNUMDOC = PTNUMDOC
    this.w_PTALFDOC = PTALFDOC
    this.w_PTDATDOC = CP_TODATE(PTDATDOC)
    this.w_PTBANAPP = PTBANAPP
    this.w_PTNUMEFF = PTNUMEFF
    this.w_PTDESRIG = PTDESRIG
    this.w_PNCODAGE = NVL(PTCODAGE,SPACE(5))
    this.w_PNFLVABD = NVL(PTFLVABD," ")
    this.w_PTFLSOSP = " "
    this.w_PTFLRAGG = NVL(PTFLRAGG," ")
    this.w_PTCODRAG = Nvl(PTCODRAG,Space(10))
    this.w_PTNUMCOR = NVL(PTNUMCOR," ")
    if Empty(this.w_PTNUMCOR)
      * --- Read from BAN_CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.BAN_CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.BAN_CONTI_idx,2],.t.,this.BAN_CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCCONCOR"+;
          " from "+i_cTable+" BAN_CONTI where ";
              +"CCTIPCON = "+cp_ToStrODBC(this.w_PTTIPCON);
              +" and CCCODCON = "+cp_ToStrODBC(this.w_PTCODCON);
              +" and CCCODBAN = "+cp_ToStrODBC(this.w_PTBANAPP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCCONCOR;
          from (i_cTable) where;
              CCTIPCON = this.w_PTTIPCON;
              and CCCODCON = this.w_PTCODCON;
              and CCCODBAN = this.w_PTBANAPP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PTNUMCOR = NVL(cp_ToDate(_read_.CCCONCOR),cp_NullValue(_read_.CCCONCOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if this.w_FLINDI="S" AND this.w_TESTPAR<>"G"
      * --- Annulla la Scadenza (Origine) della Contab.Indiretta inserita in Distinta
      * --- Try
      local bErr_037C8018
      bErr_037C8018=bTrsErr
      this.Try_037C8018()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_037C8018
      * --- End
    else
      * --- Scrive la Partita di Primanota
      * --- Insert into PAR_TITE
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTDATSCA"+",PTNUMPAR"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTMODPAG"+",PTFLCRSA"+",PTTIPCON"+",PTCODCON"+",PTCAOAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTFLSOSP"+",PTDATAPE"+",PTBANNOS"+",PTFLRAGG"+",PTBANAPP"+",PTNUMEFF"+",PTFLIMPE"+",PTNUMDIS"+",PTFLINDI"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTDATREG"+",PTNUMCOR"+",PTCODRAG"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'PAR_TITE','PTROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWNUM),'PAR_TITE','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_TITE','PTCODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLCRSA),'PAR_TITE','PTFLCRSA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTTIPCON),'PAR_TITE','PTTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODCON),'PAR_TITE','PTCODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOAPE),'PAR_TITE','PTCAOAPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDOC),'PAR_TITE','PTNUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTALFDOC),'PAR_TITE','PTALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATDOC),'PAR_TITE','PTDATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DATAPE),'PAR_TITE','PTDATAPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLRAGG),'PAR_TITE','PTFLRAGG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMEFF),'PAR_TITE','PTNUMEFF');
        +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DINUMDIS),'PAR_TITE','PTNUMDIS');
        +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODAGE),'PAR_TITE','PTCODAGE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLVABD),'PAR_TITE','PTFLVABD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OSERIAL),'PAR_TITE','PTSERRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OROWORD),'PAR_TITE','PTORDRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_OROWNUM),'PAR_TITE','PTNUMRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODRAG),'PAR_TITE','PTCODRAG');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_PTROWORD,'CPROWNUM',this.w_PTROWNUM,'PTDATSCA',this.w_PTDATSCA,'PTNUMPAR',this.w_PTNUMPAR,'PT_SEGNO',this.w_PT_SEGNO,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PTCODVAL,'PTCAOVAL',this.w_PTCAOVAL,'PTMODPAG',this.w_PTMODPAG,'PTFLCRSA',this.w_PTFLCRSA,'PTTIPCON',this.w_PTTIPCON)
        insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTDATSCA,PTNUMPAR,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTMODPAG,PTFLCRSA,PTTIPCON,PTCODCON,PTCAOAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTFLSOSP,PTDATAPE,PTBANNOS,PTFLRAGG,PTBANAPP,PTNUMEFF,PTFLIMPE,PTNUMDIS,PTFLINDI,PTDESRIG,PTCODAGE,PTFLVABD,PTSERRIF,PTORDRIF,PTNUMRIF,PTDATREG,PTNUMCOR,PTCODRAG &i_ccchkf. );
           values (;
             this.w_PNSERIAL;
             ,this.w_PTROWORD;
             ,this.w_PTROWNUM;
             ,this.w_PTDATSCA;
             ,this.w_PTNUMPAR;
             ,this.w_PT_SEGNO;
             ,this.w_PTTOTIMP;
             ,this.w_PTCODVAL;
             ,this.w_PTCAOVAL;
             ,this.w_PTMODPAG;
             ,this.w_PTFLCRSA;
             ,this.w_PTTIPCON;
             ,this.w_PTCODCON;
             ,this.w_PTCAOAPE;
             ,this.w_PTNUMDOC;
             ,this.w_PTALFDOC;
             ,this.w_PTDATDOC;
             ,this.w_PTIMPDOC;
             ,this.w_PTFLSOSP;
             ,this.w_DATAPE;
             ,this.w_PTBANNOS;
             ,this.w_PTFLRAGG;
             ,this.w_PTBANAPP;
             ,this.w_PTNUMEFF;
             ,"  ";
             ,this.w_DINUMDIS;
             ," ";
             ,this.w_PTDESRIG;
             ,this.w_PNCODAGE;
             ,this.w_PNFLVABD;
             ,this.w_OSERIAL;
             ,this.w_OROWORD;
             ,this.w_OROWNUM;
             ,this.w_PNDATREG;
             ,this.w_PTNUMCOR;
             ,this.w_PTCODRAG;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
  endproc
  proc Try_037C8018()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if Not Empty(this.w_OSERIAL)
      * --- Se non partite del pregresoo eseguo Write mirata sui riferimenti
      * --- Write into PAR_TITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PTFLIMPE ="+cp_NullLink(cp_ToStrODBC("CG"),'PAR_TITE','PTFLIMPE');
            +i_ccchkf ;
        +" where ";
            +"PTSERIAL = "+cp_ToStrODBC(this.w_OSERIAL);
            +" and PTROWORD = "+cp_ToStrODBC(-3);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_OROWNUM);
               )
      else
        update (i_cTable) set;
            PTFLIMPE = "CG";
            &i_ccchkf. ;
         where;
            PTSERIAL = this.w_OSERIAL;
            and PTROWORD = -3;
            and CPROWNUM = this.w_OROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into PAR_TITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PTFLIMPE ="+cp_NullLink(cp_ToStrODBC("CG"),'PAR_TITE','PTFLIMPE');
            +i_ccchkf ;
        +" where ";
            +"PTDATSCA = "+cp_ToStrODBC(this.w_PTDATSCA);
            +" and PTNUMPAR = "+cp_ToStrODBC(this.w_PTNUMPAR);
            +" and PTTIPCON = "+cp_ToStrODBC(this.w_PTTIPCON);
            +" and PTCODCON = "+cp_ToStrODBC(this.w_PTCODCON);
            +" and PTCODVAL = "+cp_ToStrODBC(this.w_PTCODVAL);
            +" and PTROWORD = "+cp_ToStrODBC(-3);
               )
      else
        update (i_cTable) set;
            PTFLIMPE = "CG";
            &i_ccchkf. ;
         where;
            PTDATSCA = this.w_PTDATSCA;
            and PTNUMPAR = this.w_PTNUMPAR;
            and PTTIPCON = this.w_PTTIPCON;
            and PTCODCON = this.w_PTCODCON;
            and PTCODVAL = this.w_PTCODVAL;
            and PTROWORD = -3;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea i movimenti di C/C.
    if g_BANC="S" AND this.w_FLMOVC="S"
      if NOT EMPTY(this.w_LCONBAN) 
        this.w_ROWNUM = this.w_CPROWNUM
        this.w_ROWBAN = this.w_ROWBAN+1
        this.w_ORDBAN = this.w_ORDBAN+10
        this.w_DATVAL = IIF(NOT EMPTY(this.w_LDATVAL),this.w_LDATVAL,CALCFEST(this.w_PNDATREG, this.w_CALFES, this.w_CONBANC, this.w_LCONBAN, this.w_TIPCAU, 1))
        this.w_COSCOM = IIF(this.w_BACONSBF="S",0, this.w_LIMPCOM)
        this.w_COMVAL = cp_ROUND(this.w_COSCOM*this.w_PNCAOVAL, this.w_DECTOT)
        this.w_IMPDEB = IIF(this.w_PNIMPAVE=0,0,this.w_PNIMPAVE)
        this.w_IMPCRE = IIF(this.w_PNIMPDAR=0,0,this.w_PNIMPDAR)
        this.w_CCFLCOMM = "+"
        * --- Insert into CCM_DETT
        i_nConn=i_TableProp[this.CCM_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CCM_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CCM_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CCSERIAL"+",CCROWRIF"+",CCCODCAU"+",CCNUMCOR"+",CCDATVAL"+",CCIMPCRE"+",CCIMPDEB"+",CPROWNUM"+",CPROWORD"+",CCFLDEBI"+",CCFLCRED"+",CCFLCOMM"+",CCCOSCOM"+",CCCOMVAL"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'CCM_DETT','CCSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'CCM_DETT','CCROWRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LCONBAN),'CCM_DETT','CCCODCAU');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LCONTO),'CCM_DETT','CCNUMCOR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DATVAL),'CCM_DETT','CCDATVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IMPCRE),'CCM_DETT','CCIMPCRE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDEB),'CCM_DETT','CCIMPDEB');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWBAN),'CCM_DETT','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ORDBAN),'CCM_DETT','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLDEBI),'CCM_DETT','CCFLDEBI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCRED),'CCM_DETT','CCFLCRED');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCOMM),'CCM_DETT','CCFLCOMM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_COSCOM),'CCM_DETT','CCCOSCOM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_COMVAL),'CCM_DETT','CCCOMVAL');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CCSERIAL',this.w_PNSERIAL,'CCROWRIF',this.w_ROWNUM,'CCCODCAU',this.w_LCONBAN,'CCNUMCOR',this.w_LCONTO,'CCDATVAL',this.w_DATVAL,'CCIMPCRE',this.w_IMPCRE,'CCIMPDEB',this.w_IMPDEB,'CPROWNUM',this.w_ROWBAN,'CPROWORD',this.w_ORDBAN,'CCFLDEBI',this.w_CCFLDEBI,'CCFLCRED',this.w_CCFLCRED,'CCFLCOMM',this.w_CCFLCOMM)
          insert into (i_cTable) (CCSERIAL,CCROWRIF,CCCODCAU,CCNUMCOR,CCDATVAL,CCIMPCRE,CCIMPDEB,CPROWNUM,CPROWORD,CCFLDEBI,CCFLCRED,CCFLCOMM,CCCOSCOM,CCCOMVAL &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_ROWNUM;
               ,this.w_LCONBAN;
               ,this.w_LCONTO;
               ,this.w_DATVAL;
               ,this.w_IMPCRE;
               ,this.w_IMPDEB;
               ,this.w_ROWBAN;
               ,this.w_ORDBAN;
               ,this.w_CCFLDEBI;
               ,this.w_CCFLCRED;
               ,this.w_CCFLCOMM;
               ,this.w_COSCOM;
               ,this.w_COMVAL;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Aggiorno Saldi C\C
        * --- Write into COC_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.COC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.COC_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"BASALCRE =BASALCRE+ "+cp_ToStrODBC(this.w_IMPCRE);
          +",BASALDEB =BASALDEB+ "+cp_ToStrODBC(this.w_PNIMPAVE);
              +i_ccchkf ;
          +" where ";
              +"BACODBAN = "+cp_ToStrODBC(this.w_LCONTO);
                 )
        else
          update (i_cTable) set;
              BASALCRE = BASALCRE + this.w_IMPCRE;
              ,BASALDEB = BASALDEB + this.w_PNIMPAVE;
              &i_ccchkf. ;
           where;
              BACODBAN = this.w_LCONTO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,17)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='CCM_DETT'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='DIS_TINT'
    this.cWorkTables[6]='ESERCIZI'
    this.cWorkTables[7]='MOVICOST'
    this.cWorkTables[8]='PAR_TITE'
    this.cWorkTables[9]='PNT_DETT'
    this.cWorkTables[10]='PNT_MAST'
    this.cWorkTables[11]='SALDICON'
    this.cWorkTables[12]='VALUTE'
    this.cWorkTables[13]='COC_MAST'
    this.cWorkTables[14]='CCC_MAST'
    this.cWorkTables[15]='CCC_DETT'
    this.cWorkTables[16]='BAN_CONTI'
    this.cWorkTables[17]='CON_INDI'
    return(this.OpenAllTables(17))

  proc CloseCursors()
    if used('_Curs_ESERCIZI')
      use in _Curs_ESERCIZI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
