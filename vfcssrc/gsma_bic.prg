* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bic                                                        *
*              Gestione immagini catalogo                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-25                                                      *
* Last revis.: 2005-12-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bic",oParentObject)
return(i_retval)

define class tgsma_bic as StdBatch
  * --- Local variables
  w_NewVal = space(2)
  w_Codice = space(10)
  w_curdir = space(50)
  w_PATH = space(254)
  w_NumFile = 0
  w_FILE = space(20)
  w_Estensione = space(5)
  w_posiz = 0
  w_indice = 0
  w_cont = 0
  w_OK = .f.
  w_FOLDER = space(254)
  * --- WorkFile variables
  LIB_IMMA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento zoom in maschera gsma_kic
    * --- Valore Si o No
    this.w_NewVal = space(2)
    * --- Codice
    this.w_Codice = space(10)
    * --- Directory corrente
    this.w_curdir = space(50)
    * --- Path della cartella contenente le immagini da cercare
    this.w_PATH = space(254)
    * --- Numero dei file individuati nella cartella indicata
    this.w_NumFile = 0
    this.w_FILE = space(20)
    this.w_Estensione = space(5)
    this.w_posiz = 1
    this.w_indice = 0
    this.w_cont = 0
    this.w_OK = .F.
    * --- Oggetto zoom presente nella maschera
    * --- Filtro di categoria presente nella maschera
    * --- Acquisizione della directory corrente
    this.w_curdir = sys(5) + sys(2003)
    * --- Si legge dalla tabella LIB_IMMA il path relativo all'archivio "AR"
    * --- Read from LIB_IMMA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.LIB_IMMA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIB_IMMA_idx,2],.t.,this.LIB_IMMA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LIPATIMG"+;
        " from "+i_cTable+" LIB_IMMA where ";
            +"LICODICE = "+cp_ToStrODBC("GESTFILE");
            +" and LITIPARC = "+cp_ToStrODBC("AR");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LIPATIMG;
        from (i_cTable) where;
            LICODICE = "GESTFILE";
            and LITIPARC = "AR";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PATH = NVL(cp_ToDate(_read_.LIPATIMG),cp_NullValue(_read_.LIPATIMG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_PATH <> space (254)
      this.w_PATH = alltrim(this.w_PATH)
      this.w_PATH = this.w_PATH + iif(right(this.w_path,1)="\","","\") + alltrim(i_codazi) +"\"
      if (.not.directory(alltrim(this.w_PATH)))
        this.w_FOLDER = left(this.w_PATH,len(this.w_PATH)-1)
        w_ERRORE = .F.
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if w_ERRORE
          AH_ERRORMSG("Impossibile creare la cartella %1",48,"",this.w_FOLDER)
          i_retcode = 'stop'
          return
        else
          ah_msg("Creata nuova cartella %1",.f.,.f.,1, this.w_FOLDER)
        endif
      endif
      cd (this.w_PATH)
    endif
    * --- Scansione cursore
    select (this.oParentObject.w_ZOOMIC.cCursor)
    go top
    scan
    this.w_Codice = CODICE
    this.w_FILE = alltrim(this.oParentObject.w_Categoria)+alltrim(this.w_Codice)+".*"
    this.w_NumFile = ADIR(w_ARRAY,this.w_FILE )
    if this.w_NumFile = 0
      this.w_OK = .F.
    endif
    this.w_cont = this.w_NumFile
    do while this.w_cont > 0
      this.w_OK = .F.
      this.w_FILE = w_ARRAY(this.w_posiz,1)
      this.w_indice = rat(".",this.w_FILE)
      this.w_Estensione = substr(this.w_FILE,this.w_indice,5)
      this.w_FILE = space(20)
      this.w_cont = this.w_cont - 1
      this.w_posiz = this.w_posiz + 1
      if (lower(this.w_Estensione)=".gif") or (lower(this.w_Estensione)=".jpg") or (lower(this.w_Estensione)=".jpeg") or (lower(this.w_Estensione)=".001") or (lower(this.w_Estensione)=".png")
        this.w_cont = 0
        this.w_OK = .T.
      endif
    enddo
    this.w_posiz = 1
    if this.w_OK
      this.w_NewVal = "Si"
    else
      this.w_NewVal = "No"
    endif
    replace IMG with this.w_NewVal
    endscan
    * --- Riposizionamento nella directory precedente all'elaborazione
    cd (this.w_curdir)
    this.oParentObject.w_ZOOMIC.refresh()     
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea la cartella specificata nella variabile w_FOLDER
    *     Imposta la variabile w_ERRORE in caso di errore.
    w_ERRORE = .F.
    w_ErrorHandler = on("ERROR")
    on error w_ERRORE = .T.
    md (this.w_FOLDER)
    on error &w_ErrorHandler
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LIB_IMMA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
