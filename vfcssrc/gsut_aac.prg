* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_aac                                                        *
*              Associazione Causali/Classi documentali                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-06-04                                                      *
* Last revis.: 2013-07-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_aac"))

* --- Class definition
define class tgsut_aac as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 675
  Height = 225+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-07-03"
  HelpContextID=143213417
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  ASSCAUDO_IDX = 0
  PROMCLAS_IDX = 0
  CAU_CONT_IDX = 0
  TIP_DOCU_IDX = 0
  cFile = "ASSCAUDO"
  cKeySelect = "AS__TIPO,ASCODCAU"
  cKeyWhere  = "AS__TIPO=this.w_AS__TIPO and ASCODCAU=this.w_ASCODCAU"
  cKeyWhereODBC = '"AS__TIPO="+cp_ToStrODBC(this.w_AS__TIPO)';
      +'+" and ASCODCAU="+cp_ToStrODBC(this.w_ASCODCAU)';

  cKeyWhereODBCqualified = '"ASSCAUDO.AS__TIPO="+cp_ToStrODBC(this.w_AS__TIPO)';
      +'+" and ASSCAUDO.ASCODCAU="+cp_ToStrODBC(this.w_ASCODCAU)';

  cPrg = "gsut_aac"
  cComment = "Associazione Causali/Classi documentali"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AS__TIPO = space(1)
  o_AS__TIPO = space(1)
  w_ASCODCAU = space(5)
  w_CCDESCRI = space(35)
  w_TDTIPDOC = space(10)
  o_TDTIPDOC = space(10)
  w_CCCODICE = space(10)
  o_CCCODICE = space(10)
  w_TDDESDOC = space(35)
  w_CDRIFTAB = space(20)
  w_ASCLADOC = space(15)
  o_ASCLADOC = space(15)
  w_CDDESCLA = space(50)
  w_CDMODALL = space(1)
  w_ASBAUTOM = space(1)
  o_ASBAUTOM = space(1)
  w_ASBARIMM = space(1)
  w_ASNOREPS = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_CDRIFDEF = space(1)
  w_ASCLAACQ = space(15)
  w_CDDESACQ = space(50)
  w_CDRIFACQ = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ASSCAUDO','gsut_aac')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_aacPag1","gsut_aac",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Associazione Causali/Classi documentali")
      .Pages(1).HelpContextID = 182204811
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAS__TIPO_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='PROMCLAS'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='TIP_DOCU'
    this.cWorkTables[4]='ASSCAUDO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ASSCAUDO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ASSCAUDO_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_AS__TIPO = NVL(AS__TIPO,space(1))
      .w_ASCODCAU = NVL(ASCODCAU,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ASSCAUDO where AS__TIPO=KeySet.AS__TIPO
    *                            and ASCODCAU=KeySet.ASCODCAU
    *
    i_nConn = i_TableProp[this.ASSCAUDO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ASSCAUDO_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ASSCAUDO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ASSCAUDO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ASSCAUDO '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AS__TIPO',this.w_AS__TIPO  ,'ASCODCAU',this.w_ASCODCAU  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CCDESCRI = space(35)
        .w_TDDESDOC = space(35)
        .w_CDDESCLA = space(50)
        .w_CDMODALL = space(1)
        .w_OBTEST = i_DATSYS
        .w_CDRIFDEF = space(1)
        .w_CDDESACQ = space(50)
        .w_CDRIFACQ = space(1)
        .w_AS__TIPO = NVL(AS__TIPO,space(1))
        .w_ASCODCAU = NVL(ASCODCAU,space(5))
        .w_TDTIPDOC = iif(.w_AS__TIPO="D", .w_ASCODCAU, space(5))
          .link_1_9('Load')
        .w_CCCODICE = iif(.w_AS__TIPO="C", .w_ASCODCAU, space(5))
          .link_1_10('Load')
        .w_CDRIFTAB = iif(.w_AS__TIPO="D", "DOC_MAST            ", "PNT_MAST            ")
        .w_ASCLADOC = NVL(ASCLADOC,space(15))
          .link_1_14('Load')
        .w_ASBAUTOM = NVL(ASBAUTOM,space(1))
        .w_ASBARIMM = NVL(ASBARIMM,space(1))
        .w_ASNOREPS = NVL(ASNOREPS,space(1))
        .w_ASCLAACQ = NVL(ASCLAACQ,space(15))
          .link_1_26('Load')
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(.T.)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(.T.)
        cp_LoadRecExtFlds(this,'ASSCAUDO')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AS__TIPO = space(1)
      .w_ASCODCAU = space(5)
      .w_CCDESCRI = space(35)
      .w_TDTIPDOC = space(10)
      .w_CCCODICE = space(10)
      .w_TDDESDOC = space(35)
      .w_CDRIFTAB = space(20)
      .w_ASCLADOC = space(15)
      .w_CDDESCLA = space(50)
      .w_CDMODALL = space(1)
      .w_ASBAUTOM = space(1)
      .w_ASBARIMM = space(1)
      .w_ASNOREPS = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_CDRIFDEF = space(1)
      .w_ASCLAACQ = space(15)
      .w_CDDESACQ = space(50)
      .w_CDRIFACQ = space(1)
      if .cFunction<>"Filter"
        .w_AS__TIPO = "D"
        .w_ASCODCAU = space(5)
          .DoRTCalc(3,3,.f.)
        .w_TDTIPDOC = iif(.w_AS__TIPO="D", .w_ASCODCAU, space(5))
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_TDTIPDOC))
          .link_1_9('Full')
          endif
        .w_CCCODICE = iif(.w_AS__TIPO="C", .w_ASCODCAU, space(5))
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_CCCODICE))
          .link_1_10('Full')
          endif
          .DoRTCalc(6,6,.f.)
        .w_CDRIFTAB = iif(.w_AS__TIPO="D", "DOC_MAST            ", "PNT_MAST            ")
        .w_ASCLADOC = space(15)
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_ASCLADOC))
          .link_1_14('Full')
          endif
          .DoRTCalc(9,10,.f.)
        .w_ASBAUTOM = iif(g_CPIN<>"S" OR g_DMIP="S" OR .w_CDMODALL="S", "N", "S")
        .w_ASBARIMM = "N"
        .w_ASNOREPS = iif(.w_ASBAUTOM $ "NO" and not empty(.w_ASNOREPS), .w_ASNOREPS, "N")
        .w_OBTEST = i_DATSYS
          .DoRTCalc(15,15,.f.)
        .w_ASCLAACQ = space(15)
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_ASCLAACQ))
          .link_1_26('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(.T.)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(.T.)
      endif
    endwith
    cp_BlankRecExtFlds(this,'ASSCAUDO')
    this.DoRTCalc(17,18,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oAS__TIPO_1_3.enabled = i_bVal
      .Page1.oPag.oASCODCAU_1_5.enabled = i_bVal
      .Page1.oPag.oASCLADOC_1_14.enabled = i_bVal
      .Page1.oPag.oASBAUTOM_1_19.enabled = i_bVal
      .Page1.oPag.oASBARIMM_1_20.enabled = i_bVal
      .Page1.oPag.oASNOREPS_1_21.enabled = i_bVal
      .Page1.oPag.oASCLAACQ_1_26.enabled = i_bVal
      .Page1.oPag.oObj_1_32.enabled = i_bVal
      .Page1.oPag.oObj_1_33.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oAS__TIPO_1_3.enabled = .f.
        .Page1.oPag.oASCODCAU_1_5.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oAS__TIPO_1_3.enabled = .t.
        .Page1.oPag.oASCODCAU_1_5.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'ASSCAUDO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ASSCAUDO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AS__TIPO,"AS__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASCODCAU,"ASCODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASCLADOC,"ASCLADOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASBAUTOM,"ASBAUTOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASBARIMM,"ASBARIMM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASNOREPS,"ASNOREPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ASCLAACQ,"ASCLAACQ",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ASSCAUDO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ASSCAUDO_IDX,2])
    i_lTable = "ASSCAUDO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ASSCAUDO_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ASSCAUDO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ASSCAUDO_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ASSCAUDO_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ASSCAUDO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ASSCAUDO')
        i_extval=cp_InsertValODBCExtFlds(this,'ASSCAUDO')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(AS__TIPO,ASCODCAU,ASCLADOC,ASBAUTOM,ASBARIMM"+;
                  ",ASNOREPS,ASCLAACQ "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_AS__TIPO)+;
                  ","+cp_ToStrODBC(this.w_ASCODCAU)+;
                  ","+cp_ToStrODBCNull(this.w_ASCLADOC)+;
                  ","+cp_ToStrODBC(this.w_ASBAUTOM)+;
                  ","+cp_ToStrODBC(this.w_ASBARIMM)+;
                  ","+cp_ToStrODBC(this.w_ASNOREPS)+;
                  ","+cp_ToStrODBCNull(this.w_ASCLAACQ)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ASSCAUDO')
        i_extval=cp_InsertValVFPExtFlds(this,'ASSCAUDO')
        cp_CheckDeletedKey(i_cTable,0,'AS__TIPO',this.w_AS__TIPO,'ASCODCAU',this.w_ASCODCAU)
        INSERT INTO (i_cTable);
              (AS__TIPO,ASCODCAU,ASCLADOC,ASBAUTOM,ASBARIMM,ASNOREPS,ASCLAACQ  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_AS__TIPO;
                  ,this.w_ASCODCAU;
                  ,this.w_ASCLADOC;
                  ,this.w_ASBAUTOM;
                  ,this.w_ASBARIMM;
                  ,this.w_ASNOREPS;
                  ,this.w_ASCLAACQ;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ASSCAUDO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ASSCAUDO_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ASSCAUDO_IDX,i_nConn)
      *
      * update ASSCAUDO
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ASSCAUDO')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ASCLADOC="+cp_ToStrODBCNull(this.w_ASCLADOC)+;
             ",ASBAUTOM="+cp_ToStrODBC(this.w_ASBAUTOM)+;
             ",ASBARIMM="+cp_ToStrODBC(this.w_ASBARIMM)+;
             ",ASNOREPS="+cp_ToStrODBC(this.w_ASNOREPS)+;
             ",ASCLAACQ="+cp_ToStrODBCNull(this.w_ASCLAACQ)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ASSCAUDO')
        i_cWhere = cp_PKFox(i_cTable  ,'AS__TIPO',this.w_AS__TIPO  ,'ASCODCAU',this.w_ASCODCAU  )
        UPDATE (i_cTable) SET;
              ASCLADOC=this.w_ASCLADOC;
             ,ASBAUTOM=this.w_ASBAUTOM;
             ,ASBARIMM=this.w_ASBARIMM;
             ,ASNOREPS=this.w_ASNOREPS;
             ,ASCLAACQ=this.w_ASCLAACQ;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ASSCAUDO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ASSCAUDO_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ASSCAUDO_IDX,i_nConn)
      *
      * delete ASSCAUDO
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'AS__TIPO',this.w_AS__TIPO  ,'ASCODCAU',this.w_ASCODCAU  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ASSCAUDO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ASSCAUDO_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_AS__TIPO<>.w_AS__TIPO
            .w_ASCODCAU = space(5)
        endif
        .DoRTCalc(3,3,.t.)
            .w_TDTIPDOC = iif(.w_AS__TIPO="D", .w_ASCODCAU, space(5))
          .link_1_9('Full')
            .w_CCCODICE = iif(.w_AS__TIPO="C", .w_ASCODCAU, space(5))
          .link_1_10('Full')
        if .o_AS__TIPO<>.w_AS__TIPO.or. .o_TDTIPDOC<>.w_TDTIPDOC.or. .o_CCCODICE<>.w_CCCODICE
          .Calculate_MIOORVFRGD()
        endif
        .DoRTCalc(6,6,.t.)
            .w_CDRIFTAB = iif(.w_AS__TIPO="D", "DOC_MAST            ", "PNT_MAST            ")
        if .o_AS__TIPO<>.w_AS__TIPO
            .w_ASCLADOC = space(15)
          .link_1_14('Full')
        endif
        .DoRTCalc(9,10,.t.)
        if .o_ASCLADOC<>.w_ASCLADOC
            .w_ASBAUTOM = iif(g_CPIN<>"S" OR g_DMIP="S" OR .w_CDMODALL="S", "N", "S")
        endif
        .DoRTCalc(12,12,.t.)
        if .o_ASBAUTOM<>.w_ASBAUTOM
            .w_ASNOREPS = iif(.w_ASBAUTOM $ "NO" and not empty(.w_ASNOREPS), .w_ASNOREPS, "N")
        endif
        .DoRTCalc(14,15,.t.)
        if .o_AS__TIPO<>.w_AS__TIPO
            .w_ASCLAACQ = space(15)
          .link_1_26('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(.T.)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(.T.)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(.T.)
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(.T.)
    endwith
  return

  proc Calculate_MIOORVFRGD()
    with this
          * --- link
          .w_ASCODCAU = iif(.w_AS__TIPO="D", .w_TDTIPDOC, .w_CCCODICE)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oASBAUTOM_1_19.enabled = this.oPgFrm.Page1.oPag.oASBAUTOM_1_19.mCond()
    this.oPgFrm.Page1.oPag.oASNOREPS_1_21.enabled = this.oPgFrm.Page1.oPag.oASNOREPS_1_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCCDESCRI_1_6.visible=!this.oPgFrm.Page1.oPag.oCCDESCRI_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oTDDESDOC_1_12.visible=!this.oPgFrm.Page1.oPag.oTDDESDOC_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oASBARIMM_1_20.visible=!this.oPgFrm.Page1.oPag.oASBARIMM_1_20.mHide()
    this.oPgFrm.Page1.oPag.oASNOREPS_1_21.visible=!this.oPgFrm.Page1.oPag.oASNOREPS_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TDTIPDOC
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TDTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TDTIPDOC)
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDTIPDOC = NVL(_Link_.TDTIPDOC,space(10))
      this.w_TDDESDOC = NVL(_Link_.TDDESDOC,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TDTIPDOC = space(10)
      endif
      this.w_TDDESDOC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CCCODICE
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CCCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CCCODICE)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODICE = NVL(_Link_.CCCODICE,space(10))
      this.w_CCDESCRI = NVL(_Link_.CCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODICE = space(10)
      endif
      this.w_CCDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ASCLADOC
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ASCLADOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MCD',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_ASCLADOC)+"%");
                   +" and CDRIFTAB="+cp_ToStrODBC(this.w_CDRIFTAB);

          i_ret=cp_SQL(i_nConn,"select CDRIFTAB,CDCODCLA,CDDESCLA,CDMODALL,CDRIFDEF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDRIFTAB,CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDRIFTAB',this.w_CDRIFTAB;
                     ,'CDCODCLA',trim(this.w_ASCLADOC))
          select CDRIFTAB,CDCODCLA,CDDESCLA,CDMODALL,CDRIFDEF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDRIFTAB,CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ASCLADOC)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CDDESCLA like "+cp_ToStrODBC(trim(this.w_ASCLADOC)+"%");
                   +" and CDRIFTAB="+cp_ToStrODBC(this.w_CDRIFTAB);

            i_ret=cp_SQL(i_nConn,"select CDRIFTAB,CDCODCLA,CDDESCLA,CDMODALL,CDRIFDEF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CDDESCLA like "+cp_ToStr(trim(this.w_ASCLADOC)+"%");
                   +" and CDRIFTAB="+cp_ToStr(this.w_CDRIFTAB);

            select CDRIFTAB,CDCODCLA,CDDESCLA,CDMODALL,CDRIFDEF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ASCLADOC) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDRIFTAB,CDCODCLA',cp_AbsName(oSource.parent,'oASCLADOC_1_14'),i_cWhere,'GSUT_MCD',"Classi documentali",'GSUT_AAC.PROMCLAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CDRIFTAB<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDRIFTAB,CDCODCLA,CDDESCLA,CDMODALL,CDRIFDEF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CDRIFTAB,CDCODCLA,CDDESCLA,CDMODALL,CDRIFDEF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non esistente o di tipo diverso da <Infinity D.M.S.> e <Infinity D.M.S. stand alone>")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDRIFTAB,CDCODCLA,CDDESCLA,CDMODALL,CDRIFDEF";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CDRIFTAB="+cp_ToStrODBC(this.w_CDRIFTAB);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDRIFTAB',oSource.xKey(1);
                       ,'CDCODCLA',oSource.xKey(2))
            select CDRIFTAB,CDCODCLA,CDDESCLA,CDMODALL,CDRIFDEF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ASCLADOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDRIFTAB,CDCODCLA,CDDESCLA,CDMODALL,CDRIFDEF";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_ASCLADOC);
                   +" and CDRIFTAB="+cp_ToStrODBC(this.w_CDRIFTAB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDRIFTAB',this.w_CDRIFTAB;
                       ,'CDCODCLA',this.w_ASCLADOC)
            select CDRIFTAB,CDCODCLA,CDDESCLA,CDMODALL,CDRIFDEF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ASCLADOC = NVL(_Link_.CDCODCLA,space(15))
      this.w_CDDESCLA = NVL(_Link_.CDDESCLA,space(50))
      this.w_CDMODALL = NVL(_Link_.CDMODALL,space(1))
      this.w_CDRIFDEF = NVL(_Link_.CDRIFDEF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ASCLADOC = space(15)
      endif
      this.w_CDDESCLA = space(50)
      this.w_CDMODALL = space(1)
      this.w_CDRIFDEF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CDMODALL $ "IS" and .w_CDRIFDEF<>"N"
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non esistente o di tipo diverso da <Infinity D.M.S.> e <Infinity D.M.S. stand alone>")
        endif
        this.w_ASCLADOC = space(15)
        this.w_CDDESCLA = space(50)
        this.w_CDMODALL = space(1)
        this.w_CDRIFDEF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDRIFTAB,1)+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ASCLADOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ASCLAACQ
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ASCLAACQ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MCD',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_ASCLAACQ)+"%");
                   +" and CDRIFTAB="+cp_ToStrODBC(this.w_CDRIFTAB);

          i_ret=cp_SQL(i_nConn,"select CDRIFTAB,CDCODCLA,CDDESCLA,CDRIFDEF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDRIFTAB,CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDRIFTAB',this.w_CDRIFTAB;
                     ,'CDCODCLA',trim(this.w_ASCLAACQ))
          select CDRIFTAB,CDCODCLA,CDDESCLA,CDRIFDEF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDRIFTAB,CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ASCLAACQ)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CDDESCLA like "+cp_ToStrODBC(trim(this.w_ASCLAACQ)+"%");
                   +" and CDRIFTAB="+cp_ToStrODBC(this.w_CDRIFTAB);

            i_ret=cp_SQL(i_nConn,"select CDRIFTAB,CDCODCLA,CDDESCLA,CDRIFDEF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CDDESCLA like "+cp_ToStr(trim(this.w_ASCLAACQ)+"%");
                   +" and CDRIFTAB="+cp_ToStr(this.w_CDRIFTAB);

            select CDRIFTAB,CDCODCLA,CDDESCLA,CDRIFDEF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ASCLAACQ) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDRIFTAB,CDCODCLA',cp_AbsName(oSource.parent,'oASCLAACQ_1_26'),i_cWhere,'GSUT_MCD',"Classi documentali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CDRIFTAB<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDRIFTAB,CDCODCLA,CDDESCLA,CDRIFDEF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CDRIFTAB,CDCODCLA,CDDESCLA,CDRIFDEF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non valida")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDRIFTAB,CDCODCLA,CDDESCLA,CDRIFDEF";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CDRIFTAB="+cp_ToStrODBC(this.w_CDRIFTAB);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDRIFTAB',oSource.xKey(1);
                       ,'CDCODCLA',oSource.xKey(2))
            select CDRIFTAB,CDCODCLA,CDDESCLA,CDRIFDEF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ASCLAACQ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDRIFTAB,CDCODCLA,CDDESCLA,CDRIFDEF";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_ASCLAACQ);
                   +" and CDRIFTAB="+cp_ToStrODBC(this.w_CDRIFTAB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDRIFTAB',this.w_CDRIFTAB;
                       ,'CDCODCLA',this.w_ASCLAACQ)
            select CDRIFTAB,CDCODCLA,CDDESCLA,CDRIFDEF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ASCLAACQ = NVL(_Link_.CDCODCLA,space(15))
      this.w_CDDESACQ = NVL(_Link_.CDDESCLA,space(50))
      this.w_CDRIFACQ = NVL(_Link_.CDRIFDEF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ASCLAACQ = space(15)
      endif
      this.w_CDDESACQ = space(50)
      this.w_CDRIFACQ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CDRIFACQ<>"N"
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non valida")
        endif
        this.w_ASCLAACQ = space(15)
        this.w_CDDESACQ = space(50)
        this.w_CDRIFACQ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDRIFTAB,1)+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ASCLAACQ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAS__TIPO_1_3.RadioValue()==this.w_AS__TIPO)
      this.oPgFrm.Page1.oPag.oAS__TIPO_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASCODCAU_1_5.value==this.w_ASCODCAU)
      this.oPgFrm.Page1.oPag.oASCODCAU_1_5.value=this.w_ASCODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_1_6.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_1_6.value=this.w_CCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTDDESDOC_1_12.value==this.w_TDDESDOC)
      this.oPgFrm.Page1.oPag.oTDDESDOC_1_12.value=this.w_TDDESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oASCLADOC_1_14.value==this.w_ASCLADOC)
      this.oPgFrm.Page1.oPag.oASCLADOC_1_14.value=this.w_ASCLADOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCDDESCLA_1_17.value==this.w_CDDESCLA)
      this.oPgFrm.Page1.oPag.oCDDESCLA_1_17.value=this.w_CDDESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oASBAUTOM_1_19.RadioValue()==this.w_ASBAUTOM)
      this.oPgFrm.Page1.oPag.oASBAUTOM_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASBARIMM_1_20.RadioValue()==this.w_ASBARIMM)
      this.oPgFrm.Page1.oPag.oASBARIMM_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASNOREPS_1_21.RadioValue()==this.w_ASNOREPS)
      this.oPgFrm.Page1.oPag.oASNOREPS_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oASCLAACQ_1_26.value==this.w_ASCLAACQ)
      this.oPgFrm.Page1.oPag.oASCLAACQ_1_26.value=this.w_ASCLAACQ
    endif
    if not(this.oPgFrm.Page1.oPag.oCDDESACQ_1_29.value==this.w_CDDESACQ)
      this.oPgFrm.Page1.oPag.oCDDESACQ_1_29.value=this.w_CDDESACQ
    endif
    cp_SetControlsValueExtFlds(this,'ASSCAUDO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(NOT(EMPTY(.w_ASCLADOC) AND EMPTY(.w_ASCLAACQ)))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare almeno una classe documentale")
          case   not(NOT EMPTY(iif(.w_AS__TIPO="D", looktab("TIP_DOCU","TDTIPDOC","TDTIPDOC",.w_ASCODCAU), looktab("CAU_CONT","CCCODICE","CCCODICE",.w_ASCODCAU))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oASCODCAU_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CDMODALL $ "IS" and .w_CDRIFDEF<>"N")  and not(empty(.w_ASCLADOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oASCLADOC_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe documentale non esistente o di tipo diverso da <Infinity D.M.S.> e <Infinity D.M.S. stand alone>")
          case   not(.w_CDRIFACQ<>"N")  and not(empty(.w_ASCLAACQ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oASCLAACQ_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe documentale non valida")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AS__TIPO = this.w_AS__TIPO
    this.o_TDTIPDOC = this.w_TDTIPDOC
    this.o_CCCODICE = this.w_CCCODICE
    this.o_ASCLADOC = this.w_ASCLADOC
    this.o_ASBAUTOM = this.w_ASBAUTOM
    return

enddefine

* --- Define pages as container
define class tgsut_aacPag1 as StdContainer
  Width  = 671
  height = 225
  stdWidth  = 671
  stdheight = 225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oAS__TIPO_1_3 as StdCombo with uid="XOGMZUHEGG",rtseq=1,rtrep=.f.,left=167,top=18,width=102,height=22;
    , ToolTipText = "Tipo causale Contabile/Documento";
    , HelpContextID = 165936043;
    , cFormVar="w_AS__TIPO",RowSource=""+"Documenti,"+"Contabile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAS__TIPO_1_3.RadioValue()
    return(iif(this.value =1,"D",;
    iif(this.value =2,"C",;
    space(1))))
  endfunc
  func oAS__TIPO_1_3.GetRadio()
    this.Parent.oContained.w_AS__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oAS__TIPO_1_3.SetRadio()
    this.Parent.oContained.w_AS__TIPO=trim(this.Parent.oContained.w_AS__TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_AS__TIPO=="D",1,;
      iif(this.Parent.oContained.w_AS__TIPO=="C",2,;
      0))
  endfunc

  add object oASCODCAU_1_5 as StdField with uid="YDKQKITJAG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ASCODCAU", cQueryName = "AS__TIPO,ASCODCAU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice causale contabile/documento",;
    HelpContextID = 16104357,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=167, Top=43, InputMask=replicate('X',5), bHasZoom = .t. , cMyzoom=""

  proc oASCODCAU_1_5.mBefore
    with this.Parent.oContained
      this.cMyzoom="QUERY\GSUT"+.w_AS__TIPO+"ACC.VZM"
    endwith
  endproc

  func oASCODCAU_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT EMPTY(iif(.w_AS__TIPO="D", looktab("TIP_DOCU","TDTIPDOC","TDTIPDOC",.w_ASCODCAU), looktab("CAU_CONT","CCCODICE","CCCODICE",.w_ASCODCAU))))
    endwith
    return bRes
  endfunc

  proc oASCODCAU_1_5.mZoom
    vx_exec(""+this.cMyzoom+"",this.Parent.oContained)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCCDESCRI_1_6 as StdField with uid="PTFLATCRCL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 1031057,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=234, Top=43, InputMask=replicate('X',35)

  func oCCDESCRI_1_6.mHide()
    with this.Parent.oContained
      return (.w_AS__TIPO<>"C")
    endwith
  endfunc

  add object oTDDESDOC_1_12 as StdField with uid="QQDEQLHTDD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_TDDESDOC", cQueryName = "TDDESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 252688775,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=234, Top=43, InputMask=replicate('X',35)

  func oTDDESDOC_1_12.mHide()
    with this.Parent.oContained
      return (.w_AS__TIPO<>"D")
    endwith
  endfunc

  add object oASCLADOC_1_14 as StdField with uid="UIZLXKQFFG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ASCLADOC", cQueryName = "ASCLADOC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe documentale non esistente o di tipo diverso da <Infinity D.M.S.> e <Infinity D.M.S. stand alone>",;
    ToolTipText = "Classe documentale per generazione barcode",;
    HelpContextID = 2669495,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=167, Top=101, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSUT_MCD", oKey_1_1="CDRIFTAB", oKey_1_2="this.w_CDRIFTAB", oKey_2_1="CDCODCLA", oKey_2_2="this.w_ASCLADOC"

  func oASCLADOC_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oASCLADOC_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oASCLADOC_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.PROMCLAS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CDRIFTAB="+cp_ToStrODBC(this.Parent.oContained.w_CDRIFTAB)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CDRIFTAB="+cp_ToStr(this.Parent.oContained.w_CDRIFTAB)
    endif
    do cp_zoom with 'PROMCLAS','*','CDRIFTAB,CDCODCLA',cp_AbsName(this.parent,'oASCLADOC_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MCD',"Classi documentali",'GSUT_AAC.PROMCLAS_VZM',this.parent.oContained
  endproc
  proc oASCLADOC_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CDRIFTAB=w_CDRIFTAB
     i_obj.w_CDCODCLA=this.parent.oContained.w_ASCLADOC
     i_obj.ecpSave()
  endproc

  add object oCDDESCLA_1_17 as StdField with uid="VMTUDHVSEM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CDDESCLA", cQueryName = "CDDESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 1030809,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=302, Top=101, InputMask=replicate('X',50)


  add object oASBAUTOM_1_19 as StdCombo with uid="WAIMMUTALV",rtseq=11,rtrep=.f.,left=167,top=133,width=132,height=22;
    , ToolTipText = "Modalit� di generazione barcode";
    , HelpContextID = 250858413;
    , cFormVar="w_ASBAUTOM",RowSource=""+"Automatica,"+"Manuale,"+"Man. obbligatoria", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oASBAUTOM_1_19.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    iif(this.value =3,"O",;
    space(1)))))
  endfunc
  func oASBAUTOM_1_19.GetRadio()
    this.Parent.oContained.w_ASBAUTOM = this.RadioValue()
    return .t.
  endfunc

  func oASBAUTOM_1_19.SetRadio()
    this.Parent.oContained.w_ASBAUTOM=trim(this.Parent.oContained.w_ASBAUTOM)
    this.value = ;
      iif(this.Parent.oContained.w_ASBAUTOM=="S",1,;
      iif(this.Parent.oContained.w_ASBAUTOM=="N",2,;
      iif(this.Parent.oContained.w_ASBAUTOM=="O",3,;
      0)))
  endfunc

  func oASBAUTOM_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_CPIN<>"S" OR g_DMIP="S" OR .w_CDMODALL="S")
    endwith
   endif
  endfunc

  add object oASBARIMM_1_20 as StdCheck with uid="USJSRLUDBL",rtseq=12,rtrep=.f.,left=338, top=132, caption="Barcode immediato",;
    ToolTipText = "Genera barcode contestualmente all'inserimento di una nuova registrazione",;
    HelpContextID = 170118061,;
    cFormVar="w_ASBARIMM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oASBARIMM_1_20.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oASBARIMM_1_20.GetRadio()
    this.Parent.oContained.w_ASBARIMM = this.RadioValue()
    return .t.
  endfunc

  func oASBARIMM_1_20.SetRadio()
    this.Parent.oContained.w_ASBARIMM=trim(this.Parent.oContained.w_ASBARIMM)
    this.value = ;
      iif(this.Parent.oContained.w_ASBARIMM=="S",1,;
      0)
  endfunc

  func oASBARIMM_1_20.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ASCLADOC))
    endwith
  endfunc

  add object oASNOREPS_1_21 as StdCheck with uid="BFCJHKUQIY",rtseq=13,rtrep=.f.,left=507, top=132, caption="No report di stampa",;
    ToolTipText = "Se il barcode � inserito manualmente non stampa l'etichetta o il frontespizio",;
    HelpContextID = 236260263,;
    cFormVar="w_ASNOREPS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oASNOREPS_1_21.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oASNOREPS_1_21.GetRadio()
    this.Parent.oContained.w_ASNOREPS = this.RadioValue()
    return .t.
  endfunc

  func oASNOREPS_1_21.SetRadio()
    this.Parent.oContained.w_ASNOREPS=trim(this.Parent.oContained.w_ASNOREPS)
    this.value = ;
      iif(this.Parent.oContained.w_ASNOREPS=="S",1,;
      0)
  endfunc

  func oASNOREPS_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ASBAUTOM $ "NO")
    endwith
   endif
  endfunc

  func oASNOREPS_1_21.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_ASCLADOC))
    endwith
  endfunc

  add object oASCLAACQ_1_26 as StdField with uid="EIQOPFNJMY",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ASCLAACQ", cQueryName = "ASCLAACQ",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe documentale non valida",;
    ToolTipText = "Classe documentale per archiviazione tramite cattura/scanner",;
    HelpContextID = 215434327,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=167, Top=198, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSUT_MCD", oKey_1_1="CDRIFTAB", oKey_1_2="this.w_CDRIFTAB", oKey_2_1="CDCODCLA", oKey_2_2="this.w_ASCLAACQ"

  func oASCLAACQ_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oASCLAACQ_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oASCLAACQ_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.PROMCLAS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CDRIFTAB="+cp_ToStrODBC(this.Parent.oContained.w_CDRIFTAB)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CDRIFTAB="+cp_ToStr(this.Parent.oContained.w_CDRIFTAB)
    endif
    do cp_zoom with 'PROMCLAS','*','CDRIFTAB,CDCODCLA',cp_AbsName(this.parent,'oASCLAACQ_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MCD',"Classi documentali",'',this.parent.oContained
  endproc
  proc oASCLAACQ_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.CDRIFTAB=w_CDRIFTAB
     i_obj.w_CDCODCLA=this.parent.oContained.w_ASCLAACQ
     i_obj.ecpSave()
  endproc

  add object oCDDESACQ_1_29 as StdField with uid="HXGLDDIJBO",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CDDESACQ", cQueryName = "CDDESACQ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 233850231,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=302, Top=198, InputMask=replicate('X',50)


  add object oObj_1_32 as cp_setobjprop with uid="TWILTJMNSZ",left=687, top=77, width=38,height=38,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Fontbold",cObj="CJEIFMASGX",;
    nPag=1;
    , HelpContextID = 34784230


  add object oObj_1_33 as cp_setobjprop with uid="XYOJOHXZZE",left=687, top=174, width=38,height=38,;
    caption='Object',;
   bGlobalFont=.t.,;
    cProp="Fontbold",cObj="VOXNNOCIPD",;
    nPag=1;
    , HelpContextID = 34784230

  add object oStr_1_1 as StdString with uid="CJEIFMASGX",Visible=.t., Left=10, Top=77,;
    Alignment=0, Width=478, Height=18,;
    Caption="Generazione barcode per acquisizione da Infinity"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="VOXNNOCIPD",Visible=.t., Left=10, Top=174,;
    Alignment=0, Width=478, Height=18,;
    Caption="Archiviazione da file/scanner di un documento"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="YVTXLGXTNU",Visible=.t., Left=5, Top=22,;
    Alignment=1, Width=160, Height=18,;
    Caption="Tipo causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="CUKNNPUPRJ",Visible=.t., Left=5, Top=47,;
    Alignment=1, Width=160, Height=18,;
    Caption="Codice causale contabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_AS__TIPO<>"C")
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="DGREBWBITY",Visible=.t., Left=5, Top=47,;
    Alignment=1, Width=160, Height=18,;
    Caption="Codice causale documento:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_AS__TIPO<>"D")
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="XAJCSBLWHQ",Visible=.t., Left=5, Top=105,;
    Alignment=1, Width=160, Height=18,;
    Caption="Classe documentale:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (g_DOCM <> "S")
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="AWRYXOMESN",Visible=.t., Left=5, Top=105,;
    Alignment=1, Width=160, Height=18,;
    Caption="Classe libreria allegati:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (g_DOCM = "S")
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="CTNATNLPZY",Visible=.t., Left=5, Top=137,;
    Alignment=1, Width=160, Height=18,;
    Caption="Generazione barcode:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="OZIUSQBCZG",Visible=.t., Left=5, Top=202,;
    Alignment=1, Width=160, Height=18,;
    Caption="Classe documentale:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (g_DOCM <> "S")
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="LTZENWZBKH",Visible=.t., Left=5, Top=202,;
    Alignment=1, Width=160, Height=18,;
    Caption="Classe libreria allegati:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (g_DOCM = "S")
    endwith
  endfunc

  add object oBox_1_25 as StdBox with uid="AEKEYWKVEP",left=5, top=94, width=663,height=1

  add object oBox_1_30 as StdBox with uid="ZSDIGASQFM",left=5, top=191, width=663,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_aac','ASSCAUDO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AS__TIPO=ASSCAUDO.AS__TIPO";
  +" and "+i_cAliasName2+".ASCODCAU=ASSCAUDO.ASCODCAU";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
