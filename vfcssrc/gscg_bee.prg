* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bee                                                        *
*              Estrazione dati per elenchi                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-26                                                      *
* Last revis.: 2007-09-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bee",oParentObject,m.pParam)
return(i_retval)

define class tgscg_bee as StdBatch
  * --- Local variables
  w_DESERIAL = space(10)
  w_DETIPSOG = space(1)
  w_CPROWNUM = 0
  w_DEPARIVA = space(11)
  w_DECODFIS = space(16)
  w_DETIPCON = space(1)
  w_DECODCON = space(15)
  w_DERIFPNT = space(10)
  w_DEIMPIMP = 0
  w_DEIMPAFF = 0
  w_DEIMPNOI = 0
  w_DEIMPESE = 0
  w_DEIMPNIN = 0
  w_DEIMPLOR = 0
  w_NUMMOV = 0
  w_DEDESCON = space(50)
  w_MESS = space(254)
  w_ERR = 0
  w_NUMMOVGEN = 0
  w_MESNAZ = .f.
  pParam = space(3)
  * --- WorkFile variables
  DAELCLFO_idx=0
  DAELCLFD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pParam="GEN"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pParam="AGG"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pParam="SVU"
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimino tutti i dati precedentemente caricati nel periodo
    * --- Delete from DAELCLFD
    i_nConn=i_TableProp[this.DAELCLFD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".DESERIAL = "+i_cQueryTable+".DESERIAL";
            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
      do vq_exec with 'query\gscg1bee',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Elimino le testate che non hanno pi� dettaglio
    * --- Delete from DAELCLFO
    i_nConn=i_TableProp[this.DAELCLFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".DESERIAL = "+i_cQueryTable+".DESERIAL";
    
      do vq_exec with 'query\gscg2bee',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    CREATE CURSOR MessErr (MSG C(254), DETIPSOG C(1), DEPARIVA C(11), DECODFIS C(16), DETIPCON C(1), DECODCON C(16), ORD N(2))
    VQ_EXEC("query\gscg3bee.vqr", this, "CursParametri")
    * --- estraggo tutti i dati supportati dalla procedura
    * --- elimino dati definiti come da non considerare nei parametri tramite la not exist sulla query
    SET PROCEDURE TO GSCG_BEC ADDITIVE
    * --- Select from GSCG_BEE
    do vq_exec with 'GSCG_BEE',this,'_Curs_GSCG_BEE','',.f.,.t.
    if used('_Curs_GSCG_BEE')
      select _Curs_GSCG_BEE
      locate for 1=1
      do while not(eof())
      if _Curs_GSCG_BEE.DERIFPNT<>this.w_DERIFPNT
        if not empty(this.w_DERIFPNT) AND (this.w_DEIMPIMP<>0 OR this.w_DEIMPAFF<>0 OR this.w_DEIMPNOI<>0 OR this.w_DEIMPESE<>0 OR this.w_DEIMPNIN<>0 OR this.w_DEIMPLOR<>0)
          this.w_CPROWNUM = this.w_CPROWNUM + 1
          if this.w_CPROWNUM=1
            this.w_NUMMOVGEN = this.w_NUMMOVGEN + 1
          endif
          * --- Insert into DAELCLFD
          i_nConn=i_TableProp[this.DAELCLFD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFD_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DAELCLFD_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DESERIAL"+",CPROWNUM"+",DEIMPIMP"+",DEIMPAFF"+",DEIMPNOI"+",DEIMPESE"+",DEIMPNIN"+",DEIMPLOR"+",DERIFPNT"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_DESERIAL),'DAELCLFD','DESERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DAELCLFD','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEIMPIMP),'DAELCLFD','DEIMPIMP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEIMPAFF),'DAELCLFD','DEIMPAFF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEIMPNOI),'DAELCLFD','DEIMPNOI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEIMPESE),'DAELCLFD','DEIMPESE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEIMPNIN),'DAELCLFD','DEIMPNIN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DEIMPLOR),'DAELCLFD','DEIMPLOR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DERIFPNT),'DAELCLFD','DERIFPNT');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DESERIAL',this.w_DESERIAL,'CPROWNUM',this.w_CPROWNUM,'DEIMPIMP',this.w_DEIMPIMP,'DEIMPAFF',this.w_DEIMPAFF,'DEIMPNOI',this.w_DEIMPNOI,'DEIMPESE',this.w_DEIMPESE,'DEIMPNIN',this.w_DEIMPNIN,'DEIMPLOR',this.w_DEIMPLOR,'DERIFPNT',this.w_DERIFPNT)
            insert into (i_cTable) (DESERIAL,CPROWNUM,DEIMPIMP,DEIMPAFF,DEIMPNOI,DEIMPESE,DEIMPNIN,DEIMPLOR,DERIFPNT &i_ccchkf. );
               values (;
                 this.w_DESERIAL;
                 ,this.w_CPROWNUM;
                 ,this.w_DEIMPIMP;
                 ,this.w_DEIMPAFF;
                 ,this.w_DEIMPNOI;
                 ,this.w_DEIMPESE;
                 ,this.w_DEIMPNIN;
                 ,this.w_DEIMPLOR;
                 ,this.w_DERIFPNT;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        this.w_DERIFPNT = _Curs_GSCG_BEE.DERIFPNT
        this.w_DEIMPIMP = 0
        this.w_DEIMPAFF = 0
        this.w_DEIMPNOI = 0
        this.w_DEIMPESE = 0
        this.w_DEIMPNIN = 0
        this.w_DEIMPLOR = 0
      endif
      if _Curs_GSCG_BEE.DETIPSOG<>this.w_DETIPSOG OR _Curs_GSCG_BEE.DEPARIVA<>this.w_DEPARIVA OR _Curs_GSCG_BEE.DECODFIS<>this.w_DECODFIS OR _Curs_GSCG_BEE.DETIPCON<>this.w_DETIPCON OR _Curs_GSCG_BEE.DECODCON<>this.w_DECODCON
        if this.w_CPROWNUM=0
          * --- non ho inserito nessun dettaglio per questo soggetto
          * --- Try
          local bErr_0501FEA0
          bErr_0501FEA0=bTrsErr
          this.Try_0501FEA0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0501FEA0
          * --- End
          DELETE FROM MessErr WHERE DETIPSOG=DETIPSOG AND DEPARIVA=this.w_DEPARIVA AND DECODFIS=this.w_DECODFIS AND DETIPCON=this.w_DETIPCON AND DECODCON=this.w_DECODCON
        endif
        cp_EndTrs(.f.)
        cp_BeginTrs()
        this.w_DETIPSOG = _Curs_GSCG_BEE.DETIPSOG
        this.w_MESNAZ = .f.
        this.w_CPROWNUM = 0
        this.w_NUMMOV = this.w_NUMMOV + 1
        this.w_DEPARIVA = _Curs_GSCG_BEE.DEPARIVA
        this.w_DECODFIS = _Curs_GSCG_BEE.DECODFIS
        this.w_DETIPCON = _Curs_GSCG_BEE.DETIPCON
        this.w_DECODCON = _Curs_GSCG_BEE.DECODCON
        this.w_MESS = iif(this.w_DETIPCON="C","Cliente <","Fornitore <")+this.w_DECODCON+">"
        this.w_ERR = 0
        if empty(this.w_DEPARIVA)
          this.w_MESS = this.w_MESS + " Partita IVA mancante"
          this.w_ERR = 40
        else
          if GSCG_BEC(this,this.w_DEPARIVA, "SPI", this.w_DETIPCON, this.w_DECODCON, SPACE(3))<>0
            this.w_MESS = this.w_MESS + " "+this.w_DEPARIVA+" Partita IVA errata"
            this.w_ERR = 30
          endif
        endif
        if empty(this.w_DECODFIS)
          this.w_MESS = this.w_MESS + " Codice fiscale mancante"
          this.w_ERR = iif(this.w_ERR=0,20,this.w_ERR)
        else
          if GSCG_BEC(this,this.w_DECODFIS, "SCF", this.w_DETIPCON, this.w_DECODCON, SPACE(3))<>0
            this.w_MESS = this.w_MESS + " "+this.w_DECODFIS+" Codice fiscale errato"
            this.w_ERR = iif(this.w_ERR=0,10,this.w_ERR)
          endif
        endif
        if this.w_ERR<>0
          INSERT INTO MessErr (MSG, DETIPSOG, DEPARIVA, DECODFIS, DETIPCON, DECODCON, ORD) VALUES (left(this.w_MESS,254), this.w_DETIPSOG, this.w_DEPARIVA, this.w_DECODFIS, this.w_DETIPCON, this.w_DECODCON, this.w_ERR)
        endif
        this.w_DEDESCON = _Curs_GSCG_BEE.DEDESCON
        this.w_DESERIAL = space(10)
        i_Conn=i_TableProp[this.DAELCLFO_IDX, 3]
        cp_NextTableProg(this, i_Conn, "SEECF", "i_codazi,w_DESERIAL")
        * --- Insert into DAELCLFO
        i_nConn=i_TableProp[this.DAELCLFO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DAELCLFO_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"DESERIAL"+",DE__ANNO"+",DEPARIVA"+",DECODFIS"+",DETIPCON"+",DECODCON"+",DETIPSOG"+",DEDESCON"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_DESERIAL),'DAELCLFO','DESERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANNO),'DAELCLFO','DE__ANNO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEPARIVA),'DAELCLFO','DEPARIVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DECODFIS),'DAELCLFO','DECODFIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DETIPCON),'DAELCLFO','DETIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DECODCON),'DAELCLFO','DECODCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DETIPSOG),'DAELCLFO','DETIPSOG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DEDESCON),'DAELCLFO','DEDESCON');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'DESERIAL',this.w_DESERIAL,'DE__ANNO',this.oParentObject.w_ANNO,'DEPARIVA',this.w_DEPARIVA,'DECODFIS',this.w_DECODFIS,'DETIPCON',this.w_DETIPCON,'DECODCON',this.w_DECODCON,'DETIPSOG',this.w_DETIPSOG,'DEDESCON',this.w_DEDESCON)
          insert into (i_cTable) (DESERIAL,DE__ANNO,DEPARIVA,DECODFIS,DETIPCON,DECODCON,DETIPSOG,DEDESCON &i_ccchkf. );
             values (;
               this.w_DESERIAL;
               ,this.oParentObject.w_ANNO;
               ,this.w_DEPARIVA;
               ,this.w_DECODFIS;
               ,this.w_DETIPCON;
               ,this.w_DECODCON;
               ,this.w_DETIPSOG;
               ,this.w_DEDESCON;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      * --- Controllo esculsione codice ISO
      Select CursParametri
      Locate for _Curs_GSCG_BEE.DETIPSOG=CursParametri.PETIPCOM AND NVL(PEFLGISO, " ")=" "
      if FOUND( ) OR "IT" $ upper(iif( empty(NVL(_Curs_GSCG_BEE.NACODISO," ")), "IT", NVL(_Curs_GSCG_BEE.NACODISO," ") ))
        * --- Controllo esclusione
        Select CursParametri
        Locate for _Curs_GSCG_BEE.DETIPSOG=CursParametri.PETIPCOM AND CursParametri.PECONDIZ="E" AND _Curs_GSCG_BEE.PNCODCAU=NVL(CursParametri.PECAUCON,space(5)) AND _Curs_GSCG_BEE.IVCODIVA=NVL(CursParametri.PECODIVA,space(5)) AND (CursParametri.PETIPREG="E" OR _Curs_GSCG_BEE.IVTIPREG=CursParametri.PETIPREG) AND (CursParametri.PECNDIZI="0" OR (CursParametri.PECNDIZI="1" AND _Curs_GSCG_BEE.TOTIMP>=CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="2" AND _Curs_GSCG_BEE.TOTIMP+_Curs_GSCG_BEE.TOTIVA>=CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="3" AND _Curs_GSCG_BEE.TOTIMP<CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="4" AND _Curs_GSCG_BEE.TOTIMP+_Curs_GSCG_BEE.TOTIVA<CursParametri.PEIMPCON))
        if NOT FOUND( )
          Locate for _Curs_GSCG_BEE.DETIPSOG=CursParametri.PETIPCOM AND CursParametri.PECONDIZ="E" AND _Curs_GSCG_BEE.PNCODCAU=NVL(CursParametri.PECAUCON,space(5)) AND space(5)=NVL(CursParametri.PECODIVA,space(5)) AND (CursParametri.PETIPREG="E" OR _Curs_GSCG_BEE.IVTIPREG=CursParametri.PETIPREG) AND (CursParametri.PECNDIZI="0" OR (CursParametri.PECNDIZI="1" AND _Curs_GSCG_BEE.TOTIMP>=CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="2" AND _Curs_GSCG_BEE.TOTIMP+_Curs_GSCG_BEE.TOTIVA>=CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="3" AND _Curs_GSCG_BEE.TOTIMP<CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="4" AND _Curs_GSCG_BEE.TOTIMP+_Curs_GSCG_BEE.TOTIVA<CursParametri.PEIMPCON))
          if NOT FOUND( )
            Locate for _Curs_GSCG_BEE.DETIPSOG=CursParametri.PETIPCOM AND CursParametri.PECONDIZ="E" AND space(5)=NVL(CursParametri.PECAUCON,space(5)) AND _Curs_GSCG_BEE.IVCODIVA=NVL(CursParametri.PECODIVA,space(5)) AND (CursParametri.PETIPREG="E" OR _Curs_GSCG_BEE.IVTIPREG=CursParametri.PETIPREG) AND (CursParametri.PECNDIZI="0" OR (CursParametri.PECNDIZI="1" AND _Curs_GSCG_BEE.TOTIMP>=CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="2" AND _Curs_GSCG_BEE.TOTIMP+_Curs_GSCG_BEE.TOTIVA>=CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="3" AND _Curs_GSCG_BEE.TOTIMP<CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="4" AND _Curs_GSCG_BEE.TOTIMP+_Curs_GSCG_BEE.TOTIVA<CursParametri.PEIMPCON))
          endif
        endif
        if not FOUND( )
          * --- Controllo attribuzione
          Select CursParametri
          Locate for _Curs_GSCG_BEE.DETIPSOG=CursParametri.PETIPCOM AND CursParametri.PECONDIZ="A" AND _Curs_GSCG_BEE.PNCODCAU=NVL(CursParametri.PECAUCON,space(5)) AND _Curs_GSCG_BEE.IVCODIVA=NVL(CursParametri.PECODIVA,space(5)) AND (CursParametri.PETIPREG="E" OR _Curs_GSCG_BEE.IVTIPREG=CursParametri.PETIPREG) AND (CursParametri.PECNDIZI="0" OR (CursParametri.PECNDIZI="1" AND _Curs_GSCG_BEE.TOTIMP>=CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="2" AND _Curs_GSCG_BEE.TOTIMP+_Curs_GSCG_BEE.TOTIVA>=CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="3" AND _Curs_GSCG_BEE.TOTIMP<CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="4" AND _Curs_GSCG_BEE.TOTIMP+_Curs_GSCG_BEE.TOTIVA<CursParametri.PEIMPCON))
          if NOT FOUND( )
            Locate for _Curs_GSCG_BEE.DETIPSOG=CursParametri.PETIPCOM AND CursParametri.PECONDIZ="A" AND _Curs_GSCG_BEE.PNCODCAU=NVL(CursParametri.PECAUCON,space(5)) AND space(5)=NVL(CursParametri.PECODIVA,space(5)) AND (CursParametri.PETIPREG="E" OR _Curs_GSCG_BEE.IVTIPREG=CursParametri.PETIPREG) AND (CursParametri.PECNDIZI="0" OR (CursParametri.PECNDIZI="1" AND _Curs_GSCG_BEE.TOTIMP>=CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="2" AND _Curs_GSCG_BEE.TOTIMP+_Curs_GSCG_BEE.TOTIVA>=CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="3" AND _Curs_GSCG_BEE.TOTIMP<CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="4" AND _Curs_GSCG_BEE.TOTIMP+_Curs_GSCG_BEE.TOTIVA<CursParametri.PEIMPCON))
            if NOT FOUND( )
              Locate for _Curs_GSCG_BEE.DETIPSOG=CursParametri.PETIPCOM AND CursParametri.PECONDIZ="A" AND space(5)=NVL(CursParametri.PECAUCON,space(5)) AND _Curs_GSCG_BEE.IVCODIVA=NVL(CursParametri.PECODIVA,space(5)) AND (CursParametri.PETIPREG="E" OR _Curs_GSCG_BEE.IVTIPREG=CursParametri.PETIPREG) AND (CursParametri.PECNDIZI="0" OR (CursParametri.PECNDIZI="1" AND _Curs_GSCG_BEE.TOTIMP>=CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="2" AND _Curs_GSCG_BEE.TOTIMP+_Curs_GSCG_BEE.TOTIVA>=CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="3" AND _Curs_GSCG_BEE.TOTIMP<CursParametri.PEIMPCON) OR (CursParametri.PECNDIZI="4" AND _Curs_GSCG_BEE.TOTIMP+_Curs_GSCG_BEE.TOTIVA<CursParametri.PEIMPCON))
            endif
          endif
          if NOT FOUND()
            if _Curs_GSCG_BEE.SCEENT="A"
              * --- Valorizzo importi
              if _Curs_GSCG_BEE.IVTIPREG="V"
                this.w_DEIMPIMP = this.w_DEIMPIMP + iif(_Curs_GSCG_BEE.IVDATANA<>1 AND _Curs_GSCG_BEE.IVDATANA<>2, _Curs_GSCG_BEE.IVIMPONI, 0)
                this.w_DEIMPAFF = this.w_DEIMPAFF + iif(_Curs_GSCG_BEE.IVDATANA<>1 AND _Curs_GSCG_BEE.IVDATANA<>2, _Curs_GSCG_BEE.IVIMPIVA, 0)
                this.w_DEIMPNOI = this.w_DEIMPNOI + iif(_Curs_GSCG_BEE.IVDATANA=1, _Curs_GSCG_BEE.IVIMPONI, 0)
                this.w_DEIMPESE = this.w_DEIMPESE + iif(_Curs_GSCG_BEE.IVDATANA=2, _Curs_GSCG_BEE.IVIMPONI, 0)
              else
                this.w_DEIMPIMP = this.w_DEIMPIMP + iif(_Curs_GSCG_BEE.IVDATANP<>1 AND _Curs_GSCG_BEE.IVDATANP<>2, _Curs_GSCG_BEE.IVIMPONI, 0)
                this.w_DEIMPAFF = this.w_DEIMPAFF + iif(_Curs_GSCG_BEE.IVDATANP<>1 AND _Curs_GSCG_BEE.IVDATANP<>2, _Curs_GSCG_BEE.IVIMPIVA, 0)
                this.w_DEIMPNOI = this.w_DEIMPNOI + iif(_Curs_GSCG_BEE.IVDATANP=1, _Curs_GSCG_BEE.IVIMPONI, 0)
                this.w_DEIMPESE = this.w_DEIMPESE + iif(_Curs_GSCG_BEE.IVDATANP=2, _Curs_GSCG_BEE.IVIMPONI, 0)
              endif
              this.w_DEIMPNIN = this.w_DEIMPNIN + 0
              this.w_DEIMPLOR = this.w_DEIMPLOR + 0
            endif
          else
            do case
              case CursParametri.PETIPOPE="I"
                this.w_DEIMPIMP = this.w_DEIMPIMP + _Curs_GSCG_BEE.IVIMPONI
                this.w_DEIMPAFF = this.w_DEIMPAFF + _Curs_GSCG_BEE.IVIMPIVA
              case CursParametri.PETIPOPE="X"
                this.w_DEIMPNOI = this.w_DEIMPNOI + _Curs_GSCG_BEE.IVIMPONI + iif(CursParametri.PEIMPDCO="E",_Curs_GSCG_BEE.IVIMPIVA,0)
              case CursParametri.PETIPOPE="E"
                this.w_DEIMPESE = this.w_DEIMPESE + _Curs_GSCG_BEE.IVIMPONI + iif(CursParametri.PEIMPDCO="E",_Curs_GSCG_BEE.IVIMPIVA,0)
              case CursParametri.PETIPOPE="A"
                this.w_DEIMPLOR = this.w_DEIMPLOR + _Curs_GSCG_BEE.IVIMPONI + iif(CursParametri.PEIMPDCO="E",_Curs_GSCG_BEE.IVIMPIVA,0)
              case CursParametri.PETIPOPE="S"
                this.w_DEIMPNIN = this.w_DEIMPNIN + _Curs_GSCG_BEE.IVIMPONI + iif(CursParametri.PEIMPDCO="E",_Curs_GSCG_BEE.IVIMPIVA,0)
            endcase
          endif
        else
          this.w_MESS = iif(this.w_DETIPCON="C","Cliente <","Fornitore <")+this.w_DECODCON+">"
          this.w_MESS = this.w_MESS + " Movimento reg. n� "+ALLTRIM(STR(_Curs_GSCG_BEE.PNNUMRER))+" del "+DTOC(_Curs_GSCG_BEE.PNDATREG)+" escluso per parametro ("+iif(not empty(nvl(CursParametri.PECAUCON,"")),"causale "+alltrim(CursParametri.PECAUCON)+" ","")+iif(not empty(nvl(CursParametri.PECODIVA,"")),"codice IVA "+alltrim(CursParametri.PECODIVA),"")+")"
          INSERT INTO MessErr (MSG, ORD) VALUES (left(this.w_MESS,254), 0)
        endif
      else
        if not this.w_MESNAZ
          this.w_MESS = iif(this.w_DETIPCON="C","Cliente <","Fornitore <")+this.w_DECODCON+">"
          this.w_MESS = this.w_MESS + " Escluso perch� nazione non Italia"
          INSERT INTO MessErr (MSG, ORD) VALUES (left(this.w_MESS,254), 0)
          this.w_MESNAZ = .t.
        endif
      endif
        select _Curs_GSCG_BEE
        continue
      enddo
      use
    endif
    RELEASE PROCEDURE GSCG_BEC
    * --- inserisco ultima riga
    if not empty(this.w_DERIFPNT) AND (this.w_DEIMPIMP<>0 OR this.w_DEIMPAFF<>0 OR this.w_DEIMPNOI<>0 OR this.w_DEIMPESE<>0 OR this.w_DEIMPNIN<>0 OR this.w_DEIMPLOR<>0)
      this.w_CPROWNUM = this.w_CPROWNUM + 1
      if this.w_CPROWNUM=1
        this.w_NUMMOVGEN = this.w_NUMMOVGEN + 1
      endif
      * --- Insert into DAELCLFD
      i_nConn=i_TableProp[this.DAELCLFD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFD_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DAELCLFD_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DESERIAL"+",CPROWNUM"+",DEIMPIMP"+",DEIMPAFF"+",DEIMPNOI"+",DEIMPESE"+",DEIMPNIN"+",DEIMPLOR"+",DERIFPNT"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_DESERIAL),'DAELCLFD','DESERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DAELCLFD','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DEIMPIMP),'DAELCLFD','DEIMPIMP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DEIMPAFF),'DAELCLFD','DEIMPAFF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DEIMPNOI),'DAELCLFD','DEIMPNOI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DEIMPESE),'DAELCLFD','DEIMPESE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DEIMPNIN),'DAELCLFD','DEIMPNIN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DEIMPLOR),'DAELCLFD','DEIMPLOR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DERIFPNT),'DAELCLFD','DERIFPNT');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DESERIAL',this.w_DESERIAL,'CPROWNUM',this.w_CPROWNUM,'DEIMPIMP',this.w_DEIMPIMP,'DEIMPAFF',this.w_DEIMPAFF,'DEIMPNOI',this.w_DEIMPNOI,'DEIMPESE',this.w_DEIMPESE,'DEIMPNIN',this.w_DEIMPNIN,'DEIMPLOR',this.w_DEIMPLOR,'DERIFPNT',this.w_DERIFPNT)
        insert into (i_cTable) (DESERIAL,CPROWNUM,DEIMPIMP,DEIMPAFF,DEIMPNOI,DEIMPESE,DEIMPNIN,DEIMPLOR,DERIFPNT &i_ccchkf. );
           values (;
             this.w_DESERIAL;
             ,this.w_CPROWNUM;
             ,this.w_DEIMPIMP;
             ,this.w_DEIMPAFF;
             ,this.w_DEIMPNOI;
             ,this.w_DEIMPESE;
             ,this.w_DEIMPNIN;
             ,this.w_DEIMPLOR;
             ,this.w_DERIFPNT;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    if this.w_CPROWNUM=0
      * --- non ho inserito nessun dettaglio per questo soggetto
      * --- Try
      local bErr_05019600
      bErr_05019600=bTrsErr
      this.Try_05019600()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_05019600
      * --- End
      DELETE FROM MessErr WHERE DETIPSOG=DETIPSOG AND DEPARIVA=this.w_DEPARIVA AND DECODFIS=this.w_DECODFIS AND DETIPCON=this.w_DETIPCON AND DECODCON=this.w_DECODCON
    endif
    cp_EndTrs(.f.)
    * --- Elimino le testate che non hanno pi� dettaglio
    * --- Delete from DAELCLFO
    i_nConn=i_TableProp[this.DAELCLFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".DESERIAL = "+i_cQueryTable+".DESERIAL";
    
      do vq_exec with 'query\gscg2bee',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Valorizzo il flag codifica non univoca
    * --- Write into DAELCLFO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DAELCLFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFO_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DAELCLFO_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DECONPLU ="+cp_NullLink(cp_ToStrODBC(" "),'DAELCLFO','DECONPLU');
          +i_ccchkf ;
      +" where ";
          +"DE__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
             )
    else
      update (i_cTable) set;
          DECONPLU = " ";
          &i_ccchkf. ;
       where;
          DE__ANNO = this.oParentObject.w_ANNO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into DAELCLFO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DAELCLFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFO_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DEPARIVA,DETIPSOG,DE__ANNO"
      do vq_exec with 'query\gscg5bee',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DAELCLFO_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DAELCLFO.DEPARIVA = _t2.DEPARIVA";
              +" and "+"DAELCLFO.DETIPSOG = _t2.DETIPSOG";
              +" and "+"DAELCLFO.DE__ANNO = _t2.DE__ANNO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DECONPLU ="+cp_NullLink(cp_ToStrODBC("S"),'DAELCLFO','DECONPLU');
          +i_ccchkf;
          +" from "+i_cTable+" DAELCLFO, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DAELCLFO.DEPARIVA = _t2.DEPARIVA";
              +" and "+"DAELCLFO.DETIPSOG = _t2.DETIPSOG";
              +" and "+"DAELCLFO.DE__ANNO = _t2.DE__ANNO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DAELCLFO, "+i_cQueryTable+" _t2 set ";
      +"DAELCLFO.DECONPLU ="+cp_NullLink(cp_ToStrODBC("S"),'DAELCLFO','DECONPLU');
          +Iif(Empty(i_ccchkf),"",",DAELCLFO.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DAELCLFO.DEPARIVA = t2.DEPARIVA";
              +" and "+"DAELCLFO.DETIPSOG = t2.DETIPSOG";
              +" and "+"DAELCLFO.DE__ANNO = t2.DE__ANNO";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DAELCLFO set (";
          +"DECONPLU";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'DAELCLFO','DECONPLU')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DAELCLFO.DEPARIVA = _t2.DEPARIVA";
              +" and "+"DAELCLFO.DETIPSOG = _t2.DETIPSOG";
              +" and "+"DAELCLFO.DE__ANNO = _t2.DE__ANNO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DAELCLFO set ";
      +"DECONPLU ="+cp_NullLink(cp_ToStrODBC("S"),'DAELCLFO','DECONPLU');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DEPARIVA = "+i_cQueryTable+".DEPARIVA";
              +" and "+i_cTable+".DETIPSOG = "+i_cQueryTable+".DETIPSOG";
              +" and "+i_cTable+".DE__ANNO = "+i_cQueryTable+".DE__ANNO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DECONPLU ="+cp_NullLink(cp_ToStrODBC("S"),'DAELCLFO','DECONPLU');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_ErrorMsg("Elaborazione terminata.%0Generati %1 movimenti.", , ,alltrim(str(this.w_NUMMOVGEN)))
    if RECCOUNT("MessErr")>0
      if ah_YESNO("Stampo situazione messaggi di errore?")
        SELECT DISTINCT MSG FROM MessErr INTO CURSOR __TMP__ order by ORD DESC
        CP_CHPRN("QUERY\GSVE_BCV.FRX","",this.oParentObject)
        if USED("__tmp__")
          Select __tmp__ 
 use
        endif
      endif
    endif
    Select MessErr 
 use
  endproc
  proc Try_0501FEA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from DAELCLFO
    i_nConn=i_TableProp[this.DAELCLFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DEPARIVA = "+cp_ToStrODBC(this.w_DEPARIVA);
            +" and DETIPSOG = "+cp_ToStrODBC(this.w_DETIPSOG);
            +" and DECODFIS = "+cp_ToStrODBC(this.w_DECODFIS);
            +" and DETIPCON = "+cp_ToStrODBC(this.w_DETIPCON);
            +" and DECODCON = "+cp_ToStrODBC(this.w_DECODCON);
             )
    else
      delete from (i_cTable) where;
            DEPARIVA = this.w_DEPARIVA;
            and DETIPSOG = this.w_DETIPSOG;
            and DECODFIS = this.w_DECODFIS;
            and DETIPCON = this.w_DETIPCON;
            and DECODCON = this.w_DECODCON;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_05019600()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from DAELCLFO
    i_nConn=i_TableProp[this.DAELCLFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DEPARIVA = "+cp_ToStrODBC(this.w_DEPARIVA);
            +" and DETIPSOG = "+cp_ToStrODBC(this.w_DETIPSOG);
            +" and DECODFIS = "+cp_ToStrODBC(this.w_DECODFIS);
            +" and DETIPCON = "+cp_ToStrODBC(this.w_DETIPCON);
            +" and DECODCON = "+cp_ToStrODBC(this.w_DECODCON);
             )
    else
      delete from (i_cTable) where;
            DEPARIVA = this.w_DEPARIVA;
            and DETIPSOG = this.w_DETIPSOG;
            and DECODFIS = this.w_DECODFIS;
            and DETIPCON = this.w_DETIPCON;
            and DECODCON = this.w_DECODCON;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorno la partita IVA e il codice fiscale con quelli trovati nel gestionale
    * --- Write into DAELCLFO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DAELCLFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFO_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DE__ANNO,DETIPCON,DECODCON"
      do vq_exec with 'gscg4bee',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DAELCLFO_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DAELCLFO.DE__ANNO = _t2.DE__ANNO";
              +" and "+"DAELCLFO.DETIPCON = _t2.DETIPCON";
              +" and "+"DAELCLFO.DECODCON = _t2.DECODCON";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DEPARIVA = _t2.ANPARIVA";
          +",DECODFIS = _t2.ANCODFIS";
          +i_ccchkf;
          +" from "+i_cTable+" DAELCLFO, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DAELCLFO.DE__ANNO = _t2.DE__ANNO";
              +" and "+"DAELCLFO.DETIPCON = _t2.DETIPCON";
              +" and "+"DAELCLFO.DECODCON = _t2.DECODCON";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DAELCLFO, "+i_cQueryTable+" _t2 set ";
          +"DAELCLFO.DEPARIVA = _t2.ANPARIVA";
          +",DAELCLFO.DECODFIS = _t2.ANCODFIS";
          +Iif(Empty(i_ccchkf),"",",DAELCLFO.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DAELCLFO.DE__ANNO = t2.DE__ANNO";
              +" and "+"DAELCLFO.DETIPCON = t2.DETIPCON";
              +" and "+"DAELCLFO.DECODCON = t2.DECODCON";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DAELCLFO set (";
          +"DEPARIVA,";
          +"DECODFIS";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.ANPARIVA,";
          +"t2.ANCODFIS";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DAELCLFO.DE__ANNO = _t2.DE__ANNO";
              +" and "+"DAELCLFO.DETIPCON = _t2.DETIPCON";
              +" and "+"DAELCLFO.DECODCON = _t2.DECODCON";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DAELCLFO set ";
          +"DEPARIVA = _t2.ANPARIVA";
          +",DECODFIS = _t2.ANCODFIS";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DE__ANNO = "+i_cQueryTable+".DE__ANNO";
              +" and "+i_cTable+".DETIPCON = "+i_cQueryTable+".DETIPCON";
              +" and "+i_cTable+".DECODCON = "+i_cQueryTable+".DECODCON";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DEPARIVA = (select ANPARIVA from "+i_cQueryTable+" where "+i_cWhere+")";
          +",DECODFIS = (select ANCODFIS from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Valorizzo il flag codifica non univoca
    * --- Write into DAELCLFO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DAELCLFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFO_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DAELCLFO_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DECONPLU ="+cp_NullLink(cp_ToStrODBC(" "),'DAELCLFO','DECONPLU');
          +i_ccchkf ;
      +" where ";
          +"DE__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
             )
    else
      update (i_cTable) set;
          DECONPLU = " ";
          &i_ccchkf. ;
       where;
          DE__ANNO = this.oParentObject.w_ANNO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into DAELCLFO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DAELCLFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFO_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="DEPARIVA,DETIPSOG,DE__ANNO"
      do vq_exec with 'query\gscg5bee',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DAELCLFO_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="DAELCLFO.DEPARIVA = _t2.DEPARIVA";
              +" and "+"DAELCLFO.DETIPSOG = _t2.DETIPSOG";
              +" and "+"DAELCLFO.DE__ANNO = _t2.DE__ANNO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DECONPLU ="+cp_NullLink(cp_ToStrODBC("S"),'DAELCLFO','DECONPLU');
          +i_ccchkf;
          +" from "+i_cTable+" DAELCLFO, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="DAELCLFO.DEPARIVA = _t2.DEPARIVA";
              +" and "+"DAELCLFO.DETIPSOG = _t2.DETIPSOG";
              +" and "+"DAELCLFO.DE__ANNO = _t2.DE__ANNO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DAELCLFO, "+i_cQueryTable+" _t2 set ";
      +"DAELCLFO.DECONPLU ="+cp_NullLink(cp_ToStrODBC("S"),'DAELCLFO','DECONPLU');
          +Iif(Empty(i_ccchkf),"",",DAELCLFO.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="DAELCLFO.DEPARIVA = t2.DEPARIVA";
              +" and "+"DAELCLFO.DETIPSOG = t2.DETIPSOG";
              +" and "+"DAELCLFO.DE__ANNO = t2.DE__ANNO";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DAELCLFO set (";
          +"DECONPLU";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'DAELCLFO','DECONPLU')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="DAELCLFO.DEPARIVA = _t2.DEPARIVA";
              +" and "+"DAELCLFO.DETIPSOG = _t2.DETIPSOG";
              +" and "+"DAELCLFO.DE__ANNO = _t2.DE__ANNO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DAELCLFO set ";
      +"DECONPLU ="+cp_NullLink(cp_ToStrODBC("S"),'DAELCLFO','DECONPLU');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".DEPARIVA = "+i_cQueryTable+".DEPARIVA";
              +" and "+i_cTable+".DETIPSOG = "+i_cQueryTable+".DETIPSOG";
              +" and "+i_cTable+".DE__ANNO = "+i_cQueryTable+".DE__ANNO";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DECONPLU ="+cp_NullLink(cp_ToStrODBC("S"),'DAELCLFO','DECONPLU');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_ErrorMsg("Elaborazione terminata.%0Aggiornati partita IVA e codice fiscale con dati gestionali")
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Svuota dati elenchi
    * --- Delete from DAELCLFD
    i_nConn=i_TableProp[this.DAELCLFD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".DESERIAL = "+i_cQueryTable+".DESERIAL";
    
      do vq_exec with 'query\gscg1beea',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Elimino le testate che non hanno pi� dettaglio
    * --- Delete from DAELCLFO
    i_nConn=i_TableProp[this.DAELCLFO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DAELCLFO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".DESERIAL = "+i_cQueryTable+".DESERIAL";
    
      do vq_exec with 'query\gscg2bee',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    ah_ErrorMsg("Elaborazione terminata.%0Eliminati dati elenchi clienti / fornitori")
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DAELCLFO'
    this.cWorkTables[2]='DAELCLFD'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSCG_BEE')
      use in _Curs_GSCG_BEE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
