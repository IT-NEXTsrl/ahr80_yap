* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_knd                                                        *
*              Nuovo documento - dati testata                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_70]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-11                                                      *
* Last revis.: 2008-07-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_knd",oParentObject))

* --- Class definition
define class tgsve_knd as StdForm
  Top    = 77
  Left   = 201

  * --- Standard Properties
  Width  = 529
  Height = 155
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-01"
  HelpContextID=183120489
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  cPrg = "gsve_knd"
  cComment = "Nuovo documento - dati testata"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PSTIPDOC = space(5)
  o_PSTIPDOC = space(5)
  w_FLINTE = space(1)
  w_DESCAU = space(35)
  w_PSNUMDOC = 0
  o_PSNUMDOC = 0
  w_PSALFDOC = space(10)
  o_PSALFDOC = space(10)
  w_PSDATDOC = ctod('  /  /  ')
  o_PSDATDOC = ctod('  /  /  ')
  w_MVTIPCON = space(1)
  w_CODCON = space(15)
  w_DESCLF = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DTOBSO = ctod('  /  /  ')
  w_MVFLVEAC = space(1)
  w_MVPRD = space(2)
  w_MVANNDOC = space(4)
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_PSSTATUS = space(1)
  w_FLSCOR = space(1)
  w_MVTCAMAG = space(5)
  w_NUMSCO = 0
  w_FLPACK = space(1)
  w_FLARCO = space(1)
  w_CAUPFI = space(5)
  w_CAUCOD = space(5)
  w_TDFLEXPL = space(1)
  w_FLANAL = space(1)
  w_FLELAN = space(1)
  w_FLGCOM = space(1)
  w_FLCASH = space(1)
  w_VOCTIP = space(1)
  w_TD_SEGNO = space(1)
  w_ASSCES = space(1)
  w_CAUCES = space(5)
  w_VALCOM = space(1)
  w_MVTIPDOC = space(5)
  w_MVSERIAL = space(10)
  w_GEST = space(1)
  w_CLADOC = space(2)
  w_OALFDOC = space(10)
  w_TDFLAPCA = space(1)
  w_TDNOSTCO = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kndPag1","gsve_knd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPSTIPDOC_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CONTI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSVE_BND(this,"K")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PSTIPDOC=space(5)
      .w_FLINTE=space(1)
      .w_DESCAU=space(35)
      .w_PSNUMDOC=0
      .w_PSALFDOC=space(10)
      .w_PSDATDOC=ctod("  /  /  ")
      .w_MVTIPCON=space(1)
      .w_CODCON=space(15)
      .w_DESCLF=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DTOBSO=ctod("  /  /  ")
      .w_MVFLVEAC=space(1)
      .w_MVPRD=space(2)
      .w_MVANNDOC=space(4)
      .w_MVNUMDOC=0
      .w_MVALFDOC=space(10)
      .w_PSSTATUS=space(1)
      .w_FLSCOR=space(1)
      .w_MVTCAMAG=space(5)
      .w_NUMSCO=0
      .w_FLPACK=space(1)
      .w_FLARCO=space(1)
      .w_CAUPFI=space(5)
      .w_CAUCOD=space(5)
      .w_TDFLEXPL=space(1)
      .w_FLANAL=space(1)
      .w_FLELAN=space(1)
      .w_FLGCOM=space(1)
      .w_FLCASH=space(1)
      .w_VOCTIP=space(1)
      .w_TD_SEGNO=space(1)
      .w_ASSCES=space(1)
      .w_CAUCES=space(5)
      .w_VALCOM=space(1)
      .w_MVTIPDOC=space(5)
      .w_MVSERIAL=space(10)
      .w_GEST=space(1)
      .w_CLADOC=space(2)
      .w_OALFDOC=space(10)
      .w_TDFLAPCA=space(1)
      .w_TDNOSTCO=space(1)
      .w_MVTIPCON=oParentObject.w_MVTIPCON
      .w_MVFLVEAC=oParentObject.w_MVFLVEAC
      .w_MVTCAMAG=oParentObject.w_MVTCAMAG
      .w_NUMSCO=oParentObject.w_NUMSCO
      .w_FLPACK=oParentObject.w_FLPACK
      .w_FLARCO=oParentObject.w_FLARCO
      .w_CAUPFI=oParentObject.w_CAUPFI
      .w_CAUCOD=oParentObject.w_CAUCOD
      .w_TDFLEXPL=oParentObject.w_TDFLEXPL
      .w_FLANAL=oParentObject.w_FLANAL
      .w_FLELAN=oParentObject.w_FLELAN
      .w_FLGCOM=oParentObject.w_FLGCOM
      .w_FLCASH=oParentObject.w_FLCASH
      .w_VOCTIP=oParentObject.w_VOCTIP
      .w_TD_SEGNO=oParentObject.w_TD_SEGNO
      .w_ASSCES=oParentObject.w_ASSCES
      .w_CAUCES=oParentObject.w_CAUCES
      .w_VALCOM=oParentObject.w_VALCOM
      .w_MVTIPDOC=oParentObject.w_MVTIPDOC
      .w_MVSERIAL=oParentObject.w_MVSERIAL
      .w_TDFLAPCA=oParentObject.w_TDFLAPCA
      .w_TDNOSTCO=oParentObject.w_TDNOSTCO
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PSTIPDOC))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,5,.f.)
        .w_PSDATDOC = i_DATSYS
        .w_MVTIPCON = IIF(.w_FLINTE $ "CF", .w_FLINTE, " ")
        .w_CODCON = IIF(.w_FLINTE='C',.w_CODCON,Space(15))
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODCON))
          .link_1_9('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_OBTEST = .w_PSDATDOC
          .DoRTCalc(11,13,.f.)
        .w_MVANNDOC = STR(YEAR(.w_PSDATDOC), 4, 0)
        .w_MVNUMDOC = .w_PSNUMDOC
        .w_MVALFDOC = .w_PSALFDOC
        .w_PSSTATUS = ' '
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .w_FLSCOR = 'S'
          .DoRTCalc(19,36,.f.)
        .w_GEST = this.oParentObject
      .oPgFrm.Page1.oPag.oObj_1_49.Calculate("Numero del Nuovo Documento. "+IIF(.w_MVPRD='NN',"(Numerazione Libera)",  "(Indicativo)"))
    endwith
    this.DoRTCalc(38,41,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_MVTIPCON=.w_MVTIPCON
      .oParentObject.w_MVFLVEAC=.w_MVFLVEAC
      .oParentObject.w_MVTCAMAG=.w_MVTCAMAG
      .oParentObject.w_NUMSCO=.w_NUMSCO
      .oParentObject.w_FLPACK=.w_FLPACK
      .oParentObject.w_FLARCO=.w_FLARCO
      .oParentObject.w_CAUPFI=.w_CAUPFI
      .oParentObject.w_CAUCOD=.w_CAUCOD
      .oParentObject.w_TDFLEXPL=.w_TDFLEXPL
      .oParentObject.w_FLANAL=.w_FLANAL
      .oParentObject.w_FLELAN=.w_FLELAN
      .oParentObject.w_FLGCOM=.w_FLGCOM
      .oParentObject.w_FLCASH=.w_FLCASH
      .oParentObject.w_VOCTIP=.w_VOCTIP
      .oParentObject.w_TD_SEGNO=.w_TD_SEGNO
      .oParentObject.w_ASSCES=.w_ASSCES
      .oParentObject.w_CAUCES=.w_CAUCES
      .oParentObject.w_VALCOM=.w_VALCOM
      .oParentObject.w_MVTIPDOC=.w_MVTIPDOC
      .oParentObject.w_MVSERIAL=.w_MVSERIAL
      .oParentObject.w_TDFLAPCA=.w_TDFLAPCA
      .oParentObject.w_TDNOSTCO=.w_TDNOSTCO
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_PSTIPDOC<>.w_PSTIPDOC
            .w_MVTIPCON = IIF(.w_FLINTE $ "CF", .w_FLINTE, " ")
        endif
        if .o_PSTIPDOC<>.w_PSTIPDOC
            .w_CODCON = IIF(.w_FLINTE='C',.w_CODCON,Space(15))
          .link_1_9('Full')
        endif
        .DoRTCalc(9,9,.t.)
        if .o_PSDATDOC<>.w_PSDATDOC
            .w_OBTEST = .w_PSDATDOC
        endif
        .DoRTCalc(11,13,.t.)
        if .o_PSDATDOC<>.w_PSDATDOC
            .w_MVANNDOC = STR(YEAR(.w_PSDATDOC), 4, 0)
        endif
        if .o_PSNUMDOC<>.w_PSNUMDOC
            .w_MVNUMDOC = .w_PSNUMDOC
        endif
        if .o_PSALFDOC<>.w_PSALFDOC
            .w_MVALFDOC = .w_PSALFDOC
        endif
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate("Numero del Nuovo Documento. "+IIF(.w_MVPRD='NN',"(Numerazione Libera)",  "(Indicativo)"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,41,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate("Numero del Nuovo Documento. "+IIF(.w_MVPRD='NN',"(Numerazione Libera)",  "(Indicativo)"))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPSNUMDOC_1_4.enabled = this.oPgFrm.Page1.oPag.oPSNUMDOC_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCODCON_1_9.enabled = this.oPgFrm.Page1.oPag.oCODCON_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODCON_1_9.visible=!this.oPgFrm.Page1.oPag.oCODCON_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oDESCLF_1_11.visible=!this.oPgFrm.Page1.oPag.oDESCLF_1_11.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PSTIPDOC
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PSTIPDOC)+"%");
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_MVFLVEAC);

          i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDTIPDOC,TDDESDOC,TDFLINTE,TDPRODOC,TDALFDOC,TDCATDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDFLVEAC,TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDFLVEAC',this.w_MVFLVEAC;
                     ,'TDTIPDOC',trim(this.w_PSTIPDOC))
          select TDFLVEAC,TDTIPDOC,TDDESDOC,TDFLINTE,TDPRODOC,TDALFDOC,TDCATDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDFLVEAC,TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDFLVEAC,TDTIPDOC',cp_AbsName(oSource.parent,'oPSTIPDOC_1_1'),i_cWhere,'GSVE_ATD',"Causali documento",'GSVE_BND.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MVFLVEAC<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDTIPDOC,TDDESDOC,TDFLINTE,TDPRODOC,TDALFDOC,TDCATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TDFLVEAC,TDTIPDOC,TDDESDOC,TDFLINTE,TDPRODOC,TDALFDOC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDTIPDOC,TDDESDOC,TDFLINTE,TDPRODOC,TDALFDOC,TDCATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TDFLVEAC="+cp_ToStrODBC(this.w_MVFLVEAC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',oSource.xKey(1);
                       ,'TDTIPDOC',oSource.xKey(2))
            select TDFLVEAC,TDTIPDOC,TDDESDOC,TDFLINTE,TDPRODOC,TDALFDOC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDFLVEAC,TDTIPDOC,TDDESDOC,TDFLINTE,TDPRODOC,TDALFDOC,TDCATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PSTIPDOC);
                   +" and TDFLVEAC="+cp_ToStrODBC(this.w_MVFLVEAC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDFLVEAC',this.w_MVFLVEAC;
                       ,'TDTIPDOC',this.w_PSTIPDOC)
            select TDFLVEAC,TDTIPDOC,TDDESDOC,TDFLINTE,TDPRODOC,TDALFDOC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAU = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_MVPRD = NVL(_Link_.TDPRODOC,space(2))
      this.w_PSALFDOC = NVL(_Link_.TDALFDOC,space(10))
      this.w_CLADOC = NVL(_Link_.TDCATDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PSTIPDOC = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_FLINTE = space(1)
      this.w_MVPRD = space(2)
      this.w_PSALFDOC = space(10)
      this.w_CLADOC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CKCHGCAU(.w_PSTIPDOC, .w_MVFLVEAC, .w_MVTCAMAG, .w_NUMSCO, .w_FLPACK, .w_FLARCO, .w_CAUPFI, .w_CAUCOD, .w_TDFLEXPL, .w_VALCOM, .w_FLGCOM, .w_FLCASH, .w_FLANAL, .w_FLELAN, .w_VOCTIP, .w_TD_SEGNO, .w_ASSCES, .w_CAUCES, .w_MVTIPDOC, .w_TDFLAPCA, .w_TDNOSTCO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PSTIPDOC = space(5)
        this.w_DESCAU = space(35)
        this.w_FLINTE = space(1)
        this.w_MVPRD = space(2)
        this.w_PSALFDOC = space(10)
        this.w_CLADOC = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDFLVEAC,1)+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_MVTIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_MVTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_9'),i_cWhere,'GSAR_BZC',"Elenco clienti",'GSVE1BND.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MVTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente o obsoleto o scorporo disattivo")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MVTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MVTIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCLF = space(40)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSO>.w_PSDATDOC OR EMPTY(.w_DTOBSO) And .w_FLSCOR='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente o obsoleto o scorporo disattivo")
        endif
        this.w_CODCON = space(15)
        this.w_DESCLF = space(40)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPSTIPDOC_1_1.value==this.w_PSTIPDOC)
      this.oPgFrm.Page1.oPag.oPSTIPDOC_1_1.value=this.w_PSTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_3.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_3.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oPSNUMDOC_1_4.value==this.w_PSNUMDOC)
      this.oPgFrm.Page1.oPag.oPSNUMDOC_1_4.value=this.w_PSNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPSALFDOC_1_5.value==this.w_PSALFDOC)
      this.oPgFrm.Page1.oPag.oPSALFDOC_1_5.value=this.w_PSALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPSDATDOC_1_6.value==this.w_PSDATDOC)
      this.oPgFrm.Page1.oPag.oPSDATDOC_1_6.value=this.w_PSDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_9.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_9.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_11.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_11.value=this.w_DESCLF
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(CKCHGCAU(.w_PSTIPDOC, .w_MVFLVEAC, .w_MVTCAMAG, .w_NUMSCO, .w_FLPACK, .w_FLARCO, .w_CAUPFI, .w_CAUCOD, .w_TDFLEXPL, .w_VALCOM, .w_FLGCOM, .w_FLCASH, .w_FLANAL, .w_FLELAN, .w_VOCTIP, .w_TD_SEGNO, .w_ASSCES, .w_CAUCES, .w_MVTIPDOC, .w_TDFLAPCA, .w_TDNOSTCO))  and not(empty(.w_PSTIPDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSTIPDOC_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PSDATDOC)) or not(NOT EMPTY(CALCESER(.w_PSDATDOC, '    '))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSDATDOC_1_6.SetFocus()
            i_bnoObbl = !empty(.w_PSDATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data documento inesistente o non compresa in nessun esercizio")
          case   not(.w_DTOBSO>.w_PSDATDOC OR EMPTY(.w_DTOBSO) And .w_FLSCOR='S')  and not(.w_FLINTE<>'C' OR .w_CLADOC='RF')  and (.w_FLINTE='C' AND .w_CLADOC<>'RF')  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice intestatario inesistente o obsoleto o scorporo disattivo")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsve_knd
      If Empty(.w_PSTIPDOC)
          i_bRes = .f.
          i_bnoChk = .f.
      	  i_cErrorMsg = Ah_MsgFormat("Inserire nuova causale documento")
      Endif
      If .w_FLINTE='C' AND .w_CLADOC<>'RF' And Empty(.w_CODCON)
          i_bRes = .f.
          i_bnoChk = .f.
      	  i_cErrorMsg = Ah_MsgFormat("Inserire codice intestatario")
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PSTIPDOC = this.w_PSTIPDOC
    this.o_PSNUMDOC = this.w_PSNUMDOC
    this.o_PSALFDOC = this.w_PSALFDOC
    this.o_PSDATDOC = this.w_PSDATDOC
    return

enddefine

* --- Define pages as container
define class tgsve_kndPag1 as StdContainer
  Width  = 525
  height = 155
  stdWidth  = 525
  stdheight = 155
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPSTIPDOC_1_1 as StdField with uid="UIPWHCSDIR",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PSTIPDOC", cQueryName = "PSTIPDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale del nuovo documento",;
    HelpContextID = 26974663,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=85, Top=12, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDFLVEAC", oKey_1_2="this.w_MVFLVEAC", oKey_2_1="TDTIPDOC", oKey_2_2="this.w_PSTIPDOC"

  func oPSTIPDOC_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSTIPDOC_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSTIPDOC_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIP_DOCU_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStrODBC(this.Parent.oContained.w_MVFLVEAC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TDFLVEAC="+cp_ToStr(this.Parent.oContained.w_MVFLVEAC)
    endif
    do cp_zoom with 'TIP_DOCU','*','TDFLVEAC,TDTIPDOC',cp_AbsName(this.parent,'oPSTIPDOC_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documento",'GSVE_BND.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oPSTIPDOC_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TDFLVEAC=w_MVFLVEAC
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PSTIPDOC
     i_obj.ecpSave()
  endproc

  add object oDESCAU_1_3 as StdField with uid="NANBXGBMGR",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione causale",;
    HelpContextID = 242108214,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=159, Top=12, InputMask=replicate('X',35)

  add object oPSNUMDOC_1_4 as StdField with uid="KXJWVVQBLG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PSNUMDOC", cQueryName = "PSNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero del nuovo documento",;
    HelpContextID = 29358535,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=85, Top=44, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oPSNUMDOC_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MVPRD='NN')
    endwith
   endif
  endfunc

  add object oPSALFDOC_1_5 as StdField with uid="ZYYQHRYKSU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PSALFDOC", cQueryName = "PSALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Parte alfanumerica del nuovo documento",;
    HelpContextID = 37341639,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=216, Top=44, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oPSDATDOC_1_6 as StdField with uid="ATGBJOPUKI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PSDATDOC", cQueryName = "PSDATDOC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data documento inesistente o non compresa in nessun esercizio",;
    ToolTipText = "Data del nuovo documento",;
    HelpContextID = 23370183,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=341, Top=44

  func oPSDATDOC_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT EMPTY(CALCESER(.w_PSDATDOC, '    ')))
    endwith
    return bRes
  endfunc

  add object oCODCON_1_9 as StdField with uid="ETCEUKOFIL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario inesistente o obsoleto o scorporo disattivo",;
    ToolTipText = "Intestatario del nuovo documento",;
    HelpContextID = 139288870,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=85, Top=77, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MVTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLINTE='C' AND .w_CLADOC<>'RF')
    endwith
   endif
  endfunc

  func oCODCON_1_9.mHide()
    with this.Parent.oContained
      return (.w_FLINTE<>'C' OR .w_CLADOC='RF')
    endwith
  endfunc

  func oCODCON_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_MVTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_MVTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti",'GSVE1BND.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_MVTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oDESCLF_1_11 as StdField with uid="CEJINNKNCR",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale cliente",;
    HelpContextID = 1984310,;
   bGlobalFont=.t.,;
    Height=21, Width=309, Left=205, Top=77, InputMask=replicate('X',40)

  func oDESCLF_1_11.mHide()
    with this.Parent.oContained
      return (.w_FLINTE<>'C' OR .w_CLADOC='RF')
    endwith
  endfunc


  add object oBtn_1_13 as StdButton with uid="XAOFRYAIID",left=414, top=105, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Genera nuovo documento";
    , HelpContextID = 183091738;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_PSTIPDOC))
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="KYQESPJULE",left=466, top=105, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 175803066;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_25 as cp_runprogram with uid="MBEWQEVRNB",left=-1, top=164, width=464,height=31,;
    caption='GSVE_BS3(D)',;
   bGlobalFont=.t.,;
    prg="GSVE_BS3('D')",;
    cEvent = "w_PSTIPDOC Changed,w_PSALFDOC Changed,w_PSDATDOC Changed",;
    nPag=1;
    , HelpContextID = 223566873


  add object oObj_1_49 as cp_setobjprop with uid="TMZDFGFLEK",left=-1, top=198, width=175,height=31,;
    caption='Object',;
   bGlobalFont=.t.,;
    cObj="w_PSNUMDOC",cProp="ToolTipText",;
    cEvent = "w_PSTIPDOC Changed",;
    nPag=1;
    , HelpContextID = 263312614

  add object oStr_1_8 as StdString with uid="REAKSHFWYN",Visible=.t., Left=7, Top=13,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="AOOELGBFME",Visible=.t., Left=23, Top=78,;
    Alignment=1, Width=60, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_FLINTE<>'C' OR .w_CLADOC='RF')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="XWLWNFVCPE",Visible=.t., Left=309, Top=45,;
    Alignment=1, Width=29, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="ZEOQRUIBOH",Visible=.t., Left=36, Top=45,;
    Alignment=1, Width=47, Height=18,;
    Caption="Doc.N.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="TEDRFIVGPC",Visible=.t., Left=205, Top=44,;
    Alignment=2, Width=11, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="TOWNWFXMRQ",Visible=.t., Left=-137, Top=236,;
    Alignment=0, Width=787, Height=19,;
    Caption="Attenzione: utilizzato il batch gsve_bs3 per numerazione documento che � lo stesso utilizzato nella fatturazione differita"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_44 as StdString with uid="DWKWEGMLHM",Visible=.t., Left=657, Top=-63,;
    Alignment=0, Width=97, Height=18,;
    Caption="Filtri per cusale"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_knd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
