* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_sab                                                        *
*              Stampa etichette articoli                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_176]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-28                                                      *
* Last revis.: 2014-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_sab",oParentObject))

* --- Class definition
define class tgsma_sab as StdForm
  Top    = 15
  Left   = 34

  * --- Standard Properties
  Width  = 589
  Height = 423+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-18"
  HelpContextID=124418921
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=69

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  CONTI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MARCHI_IDX = 0
  LISTINI_IDX = 0
  VALUTE_IDX = 0
  CAM_AGAZ_IDX = 0
  MAGAZZIN_IDX = 0
  AZIENDA_IDX = 0
  TIP_DOCU_IDX = 0
  cPrg = "gsma_sab"
  cComment = "Stampa etichette articoli"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PARAM = space(1)
  w_NOMPRG = space(10)
  w_AZIENDA = space(5)
  w_STPLOTTI = space(1)
  w_ARTINI = space(20)
  w_ARTINI = space(20)
  w_ARTFIN = space(20)
  w_DESINI = space(40)
  w_ARTFIN = space(20)
  w_CODLIS = space(5)
  o_CODLIS = space(5)
  w_CODVAL = space(3)
  o_CODVAL = space(3)
  w_TIPUMI = 0
  o_TIPUMI = 0
  w_CAOVAL = 0
  w_DATLIS = ctod('  /  /  ')
  w_TUTTIPRE = space(1)
  w_STAMPQTA = space(1)
  w_DESFIN = space(40)
  w_CODFAM = space(5)
  w_DESFAM = space(35)
  w_CODGRU = space(5)
  w_DESGRU = space(35)
  w_CODCAT = space(5)
  w_DESCAT = space(35)
  w_CODMAR = space(5)
  w_OBSOLETI = space(1)
  w_DATINS = ctod('  /  /  ')
  w_CODOBS = space(1)
  w_DESMAR = space(35)
  w_DATASTAM = ctod('  /  /  ')
  o_DATASTAM = ctod('  /  /  ')
  w_TIPBAR = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_SIMVAL = space(5)
  w_DECTOT = 0
  w_VALUTA = space(5)
  w_DECEUR = 0
  w_FUNIMI = space(1)
  w_PREMAS = space(1)
  w_PROVIEN = space(1)
  o_PROVIEN = space(1)
  w_CODCON = space(15)
  w_DESCRI = space(40)
  w_NUMCOP = space(1)
  o_NUMCOP = space(1)
  w_NUMCOP = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_CODINT = space(15)
  w_ININDOC = 0
  w_FINNDOC = 0
  w_TIPDOC = space(5)
  w_CODCA1 = space(5)
  w_CODCA2 = space(5)
  w_CODCA3 = space(5)
  w_CODCA4 = space(5)
  w_CODCA5 = space(5)
  w_CAUDESC1 = space(35)
  w_CAUDESC2 = space(35)
  w_CAUDESC3 = space(35)
  w_CAUDESC4 = space(35)
  w_CAUDESC5 = space(35)
  w_CODMAG = space(5)
  w_DESMAG = space(30)
  w_PERRIC = 0
  w_PERMUC = space(10)
  w_COPIE = 0
  w_DECUNI = 0
  w_NUMSKIP = 0
  w_CODESCRI = space(40)
  w_TIPDOCDES = space(35)
  * --- Area Manuale = Declare Variables
  * --- gsma_sab
  * --- Per gestione sicurezza (a seconda del parametro)
  * --- gestisco e mostro all'utente un nome di maschera diverso..
  func getSecuritycode()
    return(IIF( this.oParentObject='M', 'gsmd_sab', this.cPrg))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_sabPag1","gsma_sab",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principale")
      .Pages(2).addobject("oPag","tgsma_sabPag2","gsma_sab",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTPLOTTI_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='FAM_ARTI'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='CATEGOMO'
    this.cWorkTables[6]='MARCHI'
    this.cWorkTables[7]='LISTINI'
    this.cWorkTables[8]='VALUTE'
    this.cWorkTables[9]='CAM_AGAZ'
    this.cWorkTables[10]='MAGAZZIN'
    this.cWorkTables[11]='AZIENDA'
    this.cWorkTables[12]='TIP_DOCU'
    return(this.OpenAllTables(12))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSMA_BAB with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PARAM=space(1)
      .w_NOMPRG=space(10)
      .w_AZIENDA=space(5)
      .w_STPLOTTI=space(1)
      .w_ARTINI=space(20)
      .w_ARTINI=space(20)
      .w_ARTFIN=space(20)
      .w_DESINI=space(40)
      .w_ARTFIN=space(20)
      .w_CODLIS=space(5)
      .w_CODVAL=space(3)
      .w_TIPUMI=0
      .w_CAOVAL=0
      .w_DATLIS=ctod("  /  /  ")
      .w_TUTTIPRE=space(1)
      .w_STAMPQTA=space(1)
      .w_DESFIN=space(40)
      .w_CODFAM=space(5)
      .w_DESFAM=space(35)
      .w_CODGRU=space(5)
      .w_DESGRU=space(35)
      .w_CODCAT=space(5)
      .w_DESCAT=space(35)
      .w_CODMAR=space(5)
      .w_OBSOLETI=space(1)
      .w_DATINS=ctod("  /  /  ")
      .w_CODOBS=space(1)
      .w_DESMAR=space(35)
      .w_DATASTAM=ctod("  /  /  ")
      .w_TIPBAR=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_SIMVAL=space(5)
      .w_DECTOT=0
      .w_VALUTA=space(5)
      .w_DECEUR=0
      .w_FUNIMI=space(1)
      .w_PREMAS=space(1)
      .w_PROVIEN=space(1)
      .w_CODCON=space(15)
      .w_DESCRI=space(40)
      .w_NUMCOP=space(1)
      .w_NUMCOP=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_TIPCON=space(1)
      .w_CODINT=space(15)
      .w_ININDOC=0
      .w_FINNDOC=0
      .w_TIPDOC=space(5)
      .w_CODCA1=space(5)
      .w_CODCA2=space(5)
      .w_CODCA3=space(5)
      .w_CODCA4=space(5)
      .w_CODCA5=space(5)
      .w_CAUDESC1=space(35)
      .w_CAUDESC2=space(35)
      .w_CAUDESC3=space(35)
      .w_CAUDESC4=space(35)
      .w_CAUDESC5=space(35)
      .w_CODMAG=space(5)
      .w_DESMAG=space(30)
      .w_PERRIC=0
      .w_PERMUC=space(10)
      .w_COPIE=0
      .w_DECUNI=0
      .w_NUMSKIP=0
      .w_CODESCRI=space(40)
      .w_TIPDOCDES=space(35)
        .w_PARAM = this.oParentObject
        .w_NOMPRG = upper(IIF(.w_PARAM='M', 'GSMD_SAB', .cPrg))
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_AZIENDA))
          .link_1_3('Full')
        endif
        .w_STPLOTTI = 'N'
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_ARTINI))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_ARTINI))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_ARTFIN))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,9,.f.)
        if not(empty(.w_ARTFIN))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CODLIS))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CODVAL))
          .link_1_11('Full')
        endif
        .w_TIPUMI = 1
        .w_CAOVAL = GETCAM(.w_CODVAL, .w_DATLIS)
        .w_DATLIS = i_DATSYS
        .w_TUTTIPRE = 'N'
        .w_STAMPQTA = 'N'
        .DoRTCalc(17,18,.f.)
        if not(empty(.w_CODFAM))
          .link_1_18('Full')
        endif
        .DoRTCalc(19,20,.f.)
        if not(empty(.w_CODGRU))
          .link_1_20('Full')
        endif
        .DoRTCalc(21,22,.f.)
        if not(empty(.w_CODCAT))
          .link_1_22('Full')
        endif
        .DoRTCalc(23,24,.f.)
        if not(empty(.w_CODMAR))
          .link_1_24('Full')
        endif
        .w_OBSOLETI = 'N'
          .DoRTCalc(26,26,.f.)
        .w_CODOBS = 'N'
          .DoRTCalc(28,28,.f.)
        .w_DATASTAM = i_datsys
        .w_TIPBAR = '2'
      .oPgFrm.Page1.oPag.oObj_1_34.Calculate(.w_NOMPRG)
        .w_OBTEST = .w_DATASTAM
          .DoRTCalc(32,33,.f.)
        .w_DECTOT = GETVALUT(.w_CODVAL, 'VADECTOT')
        .w_VALUTA = g_PERVAL
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_VALUTA))
          .link_1_50('Full')
        endif
        .w_DECEUR = GETVALUT(.w_VALUTA, 'VADECUNI')
        .w_FUNIMI = 'P'
        .w_PREMAS = ' '
        .w_PROVIEN = 'R'
        .w_CODCON = SPACE(15)
        .DoRTCalc(40,40,.f.)
        if not(empty(.w_CODCON))
          .link_2_18('Full')
        endif
          .DoRTCalc(41,43,.f.)
        .w_DATINI = cp_CharToDate('  -  -  ')
        .w_DATFIN = cp_CharToDate('  -  -  ')
        .w_TIPCON = ' '
        .w_CODINT = IIF(EMPTY(.w_TIPCON), '', .w_CODINT)
        .DoRTCalc(47,47,.f.)
        if not(empty(.w_CODINT))
          .link_2_25('Full')
        endif
        .w_ININDOC = 1
        .w_FINNDOC = 999999999999999
        .DoRTCalc(50,50,.f.)
        if not(empty(.w_TIPDOC))
          .link_2_28('Full')
        endif
        .w_CODCA1 = ' '
        .DoRTCalc(51,51,.f.)
        if not(empty(.w_CODCA1))
          .link_2_29('Full')
        endif
        .w_CODCA2 = ' '
        .DoRTCalc(52,52,.f.)
        if not(empty(.w_CODCA2))
          .link_2_30('Full')
        endif
        .w_CODCA3 = ' '
        .DoRTCalc(53,53,.f.)
        if not(empty(.w_CODCA3))
          .link_2_31('Full')
        endif
        .w_CODCA4 = ' '
        .DoRTCalc(54,54,.f.)
        if not(empty(.w_CODCA4))
          .link_2_32('Full')
        endif
        .w_CODCA5 = ' '
        .DoRTCalc(55,55,.f.)
        if not(empty(.w_CODCA5))
          .link_2_33('Full')
        endif
          .DoRTCalc(56,60,.f.)
        .w_CODMAG = ' '
        .DoRTCalc(61,61,.f.)
        if not(empty(.w_CODMAG))
          .link_2_39('Full')
        endif
          .DoRTCalc(62,62,.f.)
        .w_PERRIC = 0
          .DoRTCalc(64,64,.f.)
        .w_COPIE = 1
        .w_DECUNI = GETVALUT(.w_CODVAL, 'VADECUNI')
    endwith
    this.DoRTCalc(67,69,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,10,.t.)
          .link_1_11('Full')
        .DoRTCalc(12,12,.t.)
            .w_CAOVAL = GETCAM(.w_CODVAL, .w_DATLIS)
        .DoRTCalc(14,14,.t.)
        if .o_CODLIS<>.w_CODLIS
            .w_TUTTIPRE = 'N'
        endif
        if .o_CODLIS<>.w_CODLIS
            .w_STAMPQTA = 'N'
        endif
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate(.w_NOMPRG)
        .DoRTCalc(17,30,.t.)
        if .o_DATASTAM<>.w_DATASTAM
            .w_OBTEST = .w_DATASTAM
        endif
        .DoRTCalc(32,33,.t.)
        if .o_CODVAL<>.w_CODVAL
            .w_DECTOT = GETVALUT(.w_CODVAL, 'VADECTOT')
        endif
          .link_1_50('Full')
            .w_DECEUR = GETVALUT(.w_VALUTA, 'VADECUNI')
        .DoRTCalc(37,39,.t.)
        if .o_PROVIEN<>.w_PROVIEN
            .w_CODCON = SPACE(15)
          .link_2_18('Full')
        endif
        .DoRTCalc(41,43,.t.)
        if .o_NUMCOP<>.w_NUMCOP
            .w_DATINI = cp_CharToDate('  -  -  ')
        endif
        if .o_NUMCOP<>.w_NUMCOP
            .w_DATFIN = cp_CharToDate('  -  -  ')
        endif
        .DoRTCalc(46,46,.t.)
        if .o_TIPCON<>.w_TIPCON
            .w_CODINT = IIF(EMPTY(.w_TIPCON), '', .w_CODINT)
          .link_2_25('Full')
        endif
        .DoRTCalc(48,50,.t.)
        if .o_NUMCOP<>.w_NUMCOP
            .w_CODCA1 = ' '
          .link_2_29('Full')
        endif
        if .o_NUMCOP<>.w_NUMCOP
            .w_CODCA2 = ' '
          .link_2_30('Full')
        endif
        if .o_NUMCOP<>.w_NUMCOP
            .w_CODCA3 = ' '
          .link_2_31('Full')
        endif
        if .o_NUMCOP<>.w_NUMCOP
            .w_CODCA4 = ' '
          .link_2_32('Full')
        endif
        if .o_NUMCOP<>.w_NUMCOP
            .w_CODCA5 = ' '
          .link_2_33('Full')
        endif
        .DoRTCalc(56,60,.t.)
        if .o_NUMCOP<>.w_NUMCOP
            .w_CODMAG = ' '
          .link_2_39('Full')
        endif
        .DoRTCalc(62,62,.t.)
        if .o_NUMCOP<>.w_NUMCOP
            .w_PERRIC = 0
        endif
        .DoRTCalc(64,64,.t.)
        if .o_NUMCOP<>.w_NUMCOP
            .w_COPIE = 1
        endif
        if .o_NUMCOP<>.w_NUMCOP
          .Calculate_JOZGNLXNPW()
        endif
        if .o_CODVAL<>.w_CODVAL
            .w_DECUNI = GETVALUT(.w_CODVAL, 'VADECUNI')
        endif
        if .o_TIPUMI<>.w_TIPUMI
          .Calculate_DDEQWNZYPV()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        this.Calculate_OVBGVWXVMA()
      endwith
      this.DoRTCalc(67,69,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate(.w_NOMPRG)
    endwith
  return

  proc Calculate_JOZGNLXNPW()
    with this
          * --- Valorizzo w_stplotti
          .w_STPLOTTI = IIF(.w_NUMCOP='F', 'N', .w_STPLOTTI)
    endwith
  endproc
  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
     if .w_PARAM='M'
          .cComment = AH_Msgformat("Stampa etichette matricole")
          .Caption = .cComment
     endif
    endwith
  endproc
  proc Calculate_TFDZTIDZUR()
    with this
          * --- Calcola numcop
          .w_NUMCOP = IIF(.w_PARAM='A','F','E')
    endwith
  endproc
  proc Calculate_DDEQWNZYPV()
    with this
          * --- calcola unit� di misura
          .w_FUNIMI = IIF(.w_TIPUMI<>1, 'P', .w_FUNIMI)
    endwith
  endproc
  proc Calculate_OVBGVWXVMA()
    with this
          * --- Cambia cprg
          .cPrg = lower(.w_NOMPRG)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSTPLOTTI_1_4.enabled = this.oPgFrm.Page1.oPag.oSTPLOTTI_1_4.mCond()
    this.oPgFrm.Page1.oPag.oTIPUMI_1_12.enabled = this.oPgFrm.Page1.oPag.oTIPUMI_1_12.mCond()
    this.oPgFrm.Page1.oPag.oTUTTIPRE_1_15.enabled = this.oPgFrm.Page1.oPag.oTUTTIPRE_1_15.mCond()
    this.oPgFrm.Page1.oPag.oSTAMPQTA_1_16.enabled = this.oPgFrm.Page1.oPag.oSTAMPQTA_1_16.mCond()
    this.oPgFrm.Page2.oPag.oFUNIMI_2_15.enabled = this.oPgFrm.Page2.oPag.oFUNIMI_2_15.mCond()
    this.oPgFrm.Page2.oPag.oCODCON_2_18.enabled = this.oPgFrm.Page2.oPag.oCODCON_2_18.mCond()
    this.oPgFrm.Page2.oPag.oDATINI_2_22.enabled = this.oPgFrm.Page2.oPag.oDATINI_2_22.mCond()
    this.oPgFrm.Page2.oPag.oDATFIN_2_23.enabled = this.oPgFrm.Page2.oPag.oDATFIN_2_23.mCond()
    this.oPgFrm.Page2.oPag.oTIPCON_2_24.enabled = this.oPgFrm.Page2.oPag.oTIPCON_2_24.mCond()
    this.oPgFrm.Page2.oPag.oCODINT_2_25.enabled = this.oPgFrm.Page2.oPag.oCODINT_2_25.mCond()
    this.oPgFrm.Page2.oPag.oININDOC_2_26.enabled = this.oPgFrm.Page2.oPag.oININDOC_2_26.mCond()
    this.oPgFrm.Page2.oPag.oFINNDOC_2_27.enabled = this.oPgFrm.Page2.oPag.oFINNDOC_2_27.mCond()
    this.oPgFrm.Page2.oPag.oTIPDOC_2_28.enabled = this.oPgFrm.Page2.oPag.oTIPDOC_2_28.mCond()
    this.oPgFrm.Page2.oPag.oCODCA1_2_29.enabled = this.oPgFrm.Page2.oPag.oCODCA1_2_29.mCond()
    this.oPgFrm.Page2.oPag.oCODCA2_2_30.enabled = this.oPgFrm.Page2.oPag.oCODCA2_2_30.mCond()
    this.oPgFrm.Page2.oPag.oCODCA3_2_31.enabled = this.oPgFrm.Page2.oPag.oCODCA3_2_31.mCond()
    this.oPgFrm.Page2.oPag.oCODCA4_2_32.enabled = this.oPgFrm.Page2.oPag.oCODCA4_2_32.mCond()
    this.oPgFrm.Page2.oPag.oCODCA5_2_33.enabled = this.oPgFrm.Page2.oPag.oCODCA5_2_33.mCond()
    this.oPgFrm.Page2.oPag.oCODMAG_2_39.enabled = this.oPgFrm.Page2.oPag.oCODMAG_2_39.mCond()
    this.oPgFrm.Page2.oPag.oPERRIC_2_41.enabled = this.oPgFrm.Page2.oPag.oPERRIC_2_41.mCond()
    this.oPgFrm.Page2.oPag.oCOPIE_2_46.enabled = this.oPgFrm.Page2.oPag.oCOPIE_2_46.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSTPLOTTI_1_4.visible=!this.oPgFrm.Page1.oPag.oSTPLOTTI_1_4.mHide()
    this.oPgFrm.Page1.oPag.oARTINI_1_5.visible=!this.oPgFrm.Page1.oPag.oARTINI_1_5.mHide()
    this.oPgFrm.Page1.oPag.oARTINI_1_6.visible=!this.oPgFrm.Page1.oPag.oARTINI_1_6.mHide()
    this.oPgFrm.Page1.oPag.oARTFIN_1_7.visible=!this.oPgFrm.Page1.oPag.oARTFIN_1_7.mHide()
    this.oPgFrm.Page1.oPag.oARTFIN_1_9.visible=!this.oPgFrm.Page1.oPag.oARTFIN_1_9.mHide()
    this.oPgFrm.Page1.oPag.oTIPUMI_1_12.visible=!this.oPgFrm.Page1.oPag.oTIPUMI_1_12.mHide()
    this.oPgFrm.Page1.oPag.oTUTTIPRE_1_15.visible=!this.oPgFrm.Page1.oPag.oTUTTIPRE_1_15.mHide()
    this.oPgFrm.Page1.oPag.oSTAMPQTA_1_16.visible=!this.oPgFrm.Page1.oPag.oSTAMPQTA_1_16.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_3.visible=!this.oPgFrm.Page2.oPag.oStr_2_3.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_4.visible=!this.oPgFrm.Page2.oPag.oStr_2_4.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_12.visible=!this.oPgFrm.Page2.oPag.oStr_2_12.mHide()
    this.oPgFrm.Page2.oPag.oNUMCOP_2_20.visible=!this.oPgFrm.Page2.oPag.oNUMCOP_2_20.mHide()
    this.oPgFrm.Page2.oPag.oNUMCOP_2_21.visible=!this.oPgFrm.Page2.oPag.oNUMCOP_2_21.mHide()
    this.oPgFrm.Page2.oPag.oPERRIC_2_41.visible=!this.oPgFrm.Page2.oPag.oPERRIC_2_41.mHide()
    this.oPgFrm.Page2.oPag.oCOPIE_2_46.visible=!this.oPgFrm.Page2.oPag.oCOPIE_2_46.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_47.visible=!this.oPgFrm.Page2.oPag.oStr_2_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_55.visible=!this.oPgFrm.Page1.oPag.oStr_1_55.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_48.visible=!this.oPgFrm.Page2.oPag.oStr_2_48.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_TFDZTIDZUR()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZPERMUC";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   +" and AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZPERMUC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(5))
      this.w_PERMUC = NVL(_Link_.AZPERMUC,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_PERMUC = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTINI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARTINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ARTINI))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_ARTINI)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_ARTINI)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARTINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oARTINI_1_5'),i_cWhere,'GSMA_BZA',"Articoli",'ARTMATR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTINI)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_artfin)) OR  (UPPER(.w_artini) <= UPPER(.w_artfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
        endif
        this.w_ARTINI = space(20)
        this.w_DESINI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTINI
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARTINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ARTINI))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_ARTINI)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_ARTINI)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARTINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oARTINI_1_6'),i_cWhere,'GSMA_BZA',"Articoli",'GSMA_SAR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTINI)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_artfin)) OR  (UPPER(.w_artini) <= UPPER(.w_artfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
        endif
        this.w_ARTINI = space(20)
        this.w_DESINI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTFIN
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARTFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ARTFIN))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_ARTFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_ARTFIN)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARTFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oARTFIN_1_7'),i_cWhere,'GSMA_BZA',"Articoli",'ARTMATR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTFIN)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((UPPER(.w_artini) <= UPPER(.w_artfin)) or (empty(.w_artfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
        endif
        this.w_ARTFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTFIN
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARTFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ARTFIN))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_ARTFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_ARTFIN)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ARTFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oARTFIN_1_9'),i_cWhere,'GSMA_BZA',"Articoli",'GSMA_SAR.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTFIN)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((UPPER(.w_artini) <= UPPER(.w_artfin)) or (empty(.w_artfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
        endif
        this.w_ARTFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODLIS
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_CODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_CODLIS))
          select LSCODLIS,LSVALLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oCODLIS_1_10'),i_cWhere,'GSAR_ALI',"Listini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSVALLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_CODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_CODLIS)
            select LSCODLIS,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_CODVAL = NVL(_Link_.LSVALLIS,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODLIS = space(5)
      endif
      this.w_CODVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAM_1_18'),i_cWhere,'GSAR_AFA',"Famiglie articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(5)
      endif
      this.w_DESFAM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_CODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_CODGRU))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODGRU)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oCODGRU_1_20'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_CODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_CODGRU)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CODCAT))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCODCAT_1_22'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CODCAT)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCAT = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAR
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_CODMAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_CODMAR))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAR) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oCODMAR_1_24'),i_cWhere,'GSAR_AMH',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_CODMAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_CODMAR)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAR = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAR = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAR = space(5)
      endif
      this.w_DESMAR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(5))
      this.w_DECEUR = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(5)
      endif
      this.w_DECEUR = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PROVIEN);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_PROVIEN;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PROVIEN);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_PROVIEN);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_2_18'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'GSCG1MPN.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PROVIEN<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice intestatario inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_PROVIEN);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PROVIEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PROVIEN;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODINT
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODINT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODINT))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODINT_2_25'),i_cWhere,'GSAR_BZC',"Elenco intestatari",'GSTE_KMS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODINT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODINT)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINT = NVL(_Link_.ANCODICE,space(15))
      this.w_CODESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODINT = space(15)
      endif
      this.w_CODESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPDOC
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZA',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPDOC))
          select TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPDOC_2_28'),i_cWhere,'GSAR_BZA',"Causali documenti",'GSVE9ATD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPDOC)
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_TIPDOCDES = NVL(_Link_.TDDESDOC,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TIPDOC = space(5)
      endif
      this.w_TIPDOCDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCA1
  func Link_2_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCA1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CODCA1)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CODCA1))
          select CMCODICE,CMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCA1)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCA1) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oCODCA1_2_29'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCA1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CODCA1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CODCA1)
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCA1 = NVL(_Link_.CMCODICE,space(5))
      this.w_CAUDESC1 = NVL(_Link_.CMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCA1 = space(5)
      endif
      this.w_CAUDESC1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCA1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCA2
  func Link_2_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCA2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CODCA2)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CODCA2))
          select CMCODICE,CMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCA2)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCA2) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oCODCA2_2_30'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCA2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CODCA2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CODCA2)
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCA2 = NVL(_Link_.CMCODICE,space(5))
      this.w_CAUDESC2 = NVL(_Link_.CMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCA2 = space(5)
      endif
      this.w_CAUDESC2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCA2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCA3
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCA3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CODCA3)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CODCA3))
          select CMCODICE,CMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCA3)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCA3) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oCODCA3_2_31'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCA3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CODCA3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CODCA3)
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCA3 = NVL(_Link_.CMCODICE,space(5))
      this.w_CAUDESC3 = NVL(_Link_.CMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCA3 = space(5)
      endif
      this.w_CAUDESC3 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCA3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCA4
  func Link_2_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCA4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CODCA4)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CODCA4))
          select CMCODICE,CMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCA4)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCA4) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oCODCA4_2_32'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCA4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CODCA4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CODCA4)
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCA4 = NVL(_Link_.CMCODICE,space(5))
      this.w_CAUDESC4 = NVL(_Link_.CMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCA4 = space(5)
      endif
      this.w_CAUDESC4 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCA4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCA5
  func Link_2_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCA5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CODCA5)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CODCA5))
          select CMCODICE,CMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCA5)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCA5) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oCODCA5_2_33'),i_cWhere,'GSMA_ACM',"Causali magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCA5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CODCA5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CODCA5)
            select CMCODICE,CMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCA5 = NVL(_Link_.CMCODICE,space(5))
      this.w_CAUDESC5 = NVL(_Link_.CMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCA5 = space(5)
      endif
      this.w_CAUDESC5 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCA5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_2_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_2_39'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTPLOTTI_1_4.RadioValue()==this.w_STPLOTTI)
      this.oPgFrm.Page1.oPag.oSTPLOTTI_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARTINI_1_5.value==this.w_ARTINI)
      this.oPgFrm.Page1.oPag.oARTINI_1_5.value=this.w_ARTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oARTINI_1_6.value==this.w_ARTINI)
      this.oPgFrm.Page1.oPag.oARTINI_1_6.value=this.w_ARTINI
    endif
    if not(this.oPgFrm.Page1.oPag.oARTFIN_1_7.value==this.w_ARTFIN)
      this.oPgFrm.Page1.oPag.oARTFIN_1_7.value=this.w_ARTFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_8.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_8.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oARTFIN_1_9.value==this.w_ARTFIN)
      this.oPgFrm.Page1.oPag.oARTFIN_1_9.value=this.w_ARTFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODLIS_1_10.value==this.w_CODLIS)
      this.oPgFrm.Page1.oPag.oCODLIS_1_10.value=this.w_CODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAL_1_11.value==this.w_CODVAL)
      this.oPgFrm.Page1.oPag.oCODVAL_1_11.value=this.w_CODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPUMI_1_12.RadioValue()==this.w_TIPUMI)
      this.oPgFrm.Page1.oPag.oTIPUMI_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATLIS_1_14.value==this.w_DATLIS)
      this.oPgFrm.Page1.oPag.oDATLIS_1_14.value=this.w_DATLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oTUTTIPRE_1_15.RadioValue()==this.w_TUTTIPRE)
      this.oPgFrm.Page1.oPag.oTUTTIPRE_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTAMPQTA_1_16.RadioValue()==this.w_STAMPQTA)
      this.oPgFrm.Page1.oPag.oSTAMPQTA_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_17.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_17.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFAM_1_18.value==this.w_CODFAM)
      this.oPgFrm.Page1.oPag.oCODFAM_1_18.value=this.w_CODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAM_1_19.value==this.w_DESFAM)
      this.oPgFrm.Page1.oPag.oDESFAM_1_19.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRU_1_20.value==this.w_CODGRU)
      this.oPgFrm.Page1.oPag.oCODGRU_1_20.value=this.w_CODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_21.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_21.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAT_1_22.value==this.w_CODCAT)
      this.oPgFrm.Page1.oPag.oCODCAT_1_22.value=this.w_CODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_23.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_23.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAR_1_24.value==this.w_CODMAR)
      this.oPgFrm.Page1.oPag.oCODMAR_1_24.value=this.w_CODMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oOBSOLETI_1_25.RadioValue()==this.w_OBSOLETI)
      this.oPgFrm.Page1.oPag.oOBSOLETI_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINS_1_26.value==this.w_DATINS)
      this.oPgFrm.Page1.oPag.oDATINS_1_26.value=this.w_DATINS
    endif
    if not(this.oPgFrm.Page1.oPag.oCODOBS_1_27.RadioValue()==this.w_CODOBS)
      this.oPgFrm.Page1.oPag.oCODOBS_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAR_1_28.value==this.w_DESMAR)
      this.oPgFrm.Page1.oPag.oDESMAR_1_28.value=this.w_DESMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDATASTAM_1_29.value==this.w_DATASTAM)
      this.oPgFrm.Page1.oPag.oDATASTAM_1_29.value=this.w_DATASTAM
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPBAR_1_31.RadioValue()==this.w_TIPBAR)
      this.oPgFrm.Page1.oPag.oTIPBAR_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFUNIMI_2_15.RadioValue()==this.w_FUNIMI)
      this.oPgFrm.Page2.oPag.oFUNIMI_2_15.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPREMAS_2_16.RadioValue()==this.w_PREMAS)
      this.oPgFrm.Page2.oPag.oPREMAS_2_16.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPROVIEN_2_17.RadioValue()==this.w_PROVIEN)
      this.oPgFrm.Page2.oPag.oPROVIEN_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON_2_18.value==this.w_CODCON)
      this.oPgFrm.Page2.oPag.oCODCON_2_18.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCRI_2_19.value==this.w_DESCRI)
      this.oPgFrm.Page2.oPag.oDESCRI_2_19.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMCOP_2_20.RadioValue()==this.w_NUMCOP)
      this.oPgFrm.Page2.oPag.oNUMCOP_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMCOP_2_21.RadioValue()==this.w_NUMCOP)
      this.oPgFrm.Page2.oPag.oNUMCOP_2_21.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDATINI_2_22.value==this.w_DATINI)
      this.oPgFrm.Page2.oPag.oDATINI_2_22.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDATFIN_2_23.value==this.w_DATFIN)
      this.oPgFrm.Page2.oPag.oDATFIN_2_23.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPCON_2_24.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page2.oPag.oTIPCON_2_24.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODINT_2_25.value==this.w_CODINT)
      this.oPgFrm.Page2.oPag.oCODINT_2_25.value=this.w_CODINT
    endif
    if not(this.oPgFrm.Page2.oPag.oININDOC_2_26.value==this.w_ININDOC)
      this.oPgFrm.Page2.oPag.oININDOC_2_26.value=this.w_ININDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oFINNDOC_2_27.value==this.w_FINNDOC)
      this.oPgFrm.Page2.oPag.oFINNDOC_2_27.value=this.w_FINNDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDOC_2_28.value==this.w_TIPDOC)
      this.oPgFrm.Page2.oPag.oTIPDOC_2_28.value=this.w_TIPDOC
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCA1_2_29.value==this.w_CODCA1)
      this.oPgFrm.Page2.oPag.oCODCA1_2_29.value=this.w_CODCA1
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCA2_2_30.value==this.w_CODCA2)
      this.oPgFrm.Page2.oPag.oCODCA2_2_30.value=this.w_CODCA2
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCA3_2_31.value==this.w_CODCA3)
      this.oPgFrm.Page2.oPag.oCODCA3_2_31.value=this.w_CODCA3
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCA4_2_32.value==this.w_CODCA4)
      this.oPgFrm.Page2.oPag.oCODCA4_2_32.value=this.w_CODCA4
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCA5_2_33.value==this.w_CODCA5)
      this.oPgFrm.Page2.oPag.oCODCA5_2_33.value=this.w_CODCA5
    endif
    if not(this.oPgFrm.Page2.oPag.oCAUDESC1_2_34.value==this.w_CAUDESC1)
      this.oPgFrm.Page2.oPag.oCAUDESC1_2_34.value=this.w_CAUDESC1
    endif
    if not(this.oPgFrm.Page2.oPag.oCAUDESC2_2_35.value==this.w_CAUDESC2)
      this.oPgFrm.Page2.oPag.oCAUDESC2_2_35.value=this.w_CAUDESC2
    endif
    if not(this.oPgFrm.Page2.oPag.oCAUDESC3_2_36.value==this.w_CAUDESC3)
      this.oPgFrm.Page2.oPag.oCAUDESC3_2_36.value=this.w_CAUDESC3
    endif
    if not(this.oPgFrm.Page2.oPag.oCAUDESC4_2_37.value==this.w_CAUDESC4)
      this.oPgFrm.Page2.oPag.oCAUDESC4_2_37.value=this.w_CAUDESC4
    endif
    if not(this.oPgFrm.Page2.oPag.oCAUDESC5_2_38.value==this.w_CAUDESC5)
      this.oPgFrm.Page2.oPag.oCAUDESC5_2_38.value=this.w_CAUDESC5
    endif
    if not(this.oPgFrm.Page2.oPag.oCODMAG_2_39.value==this.w_CODMAG)
      this.oPgFrm.Page2.oPag.oCODMAG_2_39.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAG_2_40.value==this.w_DESMAG)
      this.oPgFrm.Page2.oPag.oDESMAG_2_40.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oPERRIC_2_41.value==this.w_PERRIC)
      this.oPgFrm.Page2.oPag.oPERRIC_2_41.value=this.w_PERRIC
    endif
    if not(this.oPgFrm.Page2.oPag.oPERMUC_2_43.value==this.w_PERMUC)
      this.oPgFrm.Page2.oPag.oPERMUC_2_43.value=this.w_PERMUC
    endif
    if not(this.oPgFrm.Page2.oPag.oCOPIE_2_46.value==this.w_COPIE)
      this.oPgFrm.Page2.oPag.oCOPIE_2_46.value=this.w_COPIE
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMSKIP_2_49.value==this.w_NUMSKIP)
      this.oPgFrm.Page2.oPag.oNUMSKIP_2_49.value=this.w_NUMSKIP
    endif
    if not(this.oPgFrm.Page2.oPag.oCODESCRI_2_53.value==this.w_CODESCRI)
      this.oPgFrm.Page2.oPag.oCODESCRI_2_53.value=this.w_CODESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDOCDES_2_57.value==this.w_TIPDOCDES)
      this.oPgFrm.Page2.oPag.oTIPDOCDES_2_57.value=this.w_TIPDOCDES
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_artfin)) OR  (UPPER(.w_artini) <= UPPER(.w_artfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_PARAM <> 'M')  and not(empty(.w_ARTINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARTINI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
          case   not(((empty(.w_artfin)) OR  (UPPER(.w_artini) <= UPPER(.w_artfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_PARAM = 'M')  and not(empty(.w_ARTINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARTINI_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
          case   not(((UPPER(.w_artini) <= UPPER(.w_artfin)) or (empty(.w_artfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_PARAM <> 'M')  and not(empty(.w_ARTFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARTFIN_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
          case   not(((UPPER(.w_artini) <= UPPER(.w_artfin)) or (empty(.w_artfin))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_PARAM = 'M')  and not(empty(.w_ARTFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oARTFIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("L'articolo iniziale � maggiore di quello finale oppure obsoleto")
          case   (empty(.w_DATASTAM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATASTAM_1_29.SetFocus()
            i_bnoObbl = !empty(.w_DATASTAM)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsma_sab
      IF EMPTY(.w_CODMAG) AND .w_NUMCOP='E'
       i_bnoChk=.F.
       i_bRes=.F.
       i_cErrorMsg=Ah_MsgFormat("Inserire codice magazzino")
      ENDIF
      
      IF .w_NUMCOP='M' AND (EMPTY(.w_CODCA1) AND EMPTY(.w_CODCA2) AND EMPTY(.w_CODCA3) AND EMPTY(.w_CODCA1) AND EMPTY(.w_CODCA4) AND EMPTY(.w_CODCA5))
       i_bnoChk=.F.
       i_bRes=.F.
       i_cErrorMsg=Ah_MsgFormat("Inserire almeno una causale di magazzino")
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODLIS = this.w_CODLIS
    this.o_CODVAL = this.w_CODVAL
    this.o_TIPUMI = this.w_TIPUMI
    this.o_DATASTAM = this.w_DATASTAM
    this.o_PROVIEN = this.w_PROVIEN
    this.o_NUMCOP = this.w_NUMCOP
    this.o_TIPCON = this.w_TIPCON
    return

enddefine

* --- Define pages as container
define class tgsma_sabPag1 as StdContainer
  Width  = 585
  height = 424
  stdWidth  = 585
  stdheight = 424
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oSTPLOTTI_1_4 as StdCombo with uid="ZPLKSFEKBM",rtseq=4,rtrep=.f.,left=119,top=14,width=209,height=21;
    , ToolTipText = "Stampa etichette con lotto/ubicazione se il numero copie � diverso da fisso";
    , HelpContextID = 30858863;
    , cFormVar="w_STPLOTTI",RowSource=""+"Stampa etichette senza lotto,"+"Stampa etichette con lotto,"+"Stampe Etichette senza Ubicazione,"+"Stampe Etichette con Ubicazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTPLOTTI_1_4.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'X',;
    iif(this.value =4,'U',;
    space(1))))))
  endfunc
  func oSTPLOTTI_1_4.GetRadio()
    this.Parent.oContained.w_STPLOTTI = this.RadioValue()
    return .t.
  endfunc

  func oSTPLOTTI_1_4.SetRadio()
    this.Parent.oContained.w_STPLOTTI=trim(this.Parent.oContained.w_STPLOTTI)
    this.value = ;
      iif(this.Parent.oContained.w_STPLOTTI=='N',1,;
      iif(this.Parent.oContained.w_STPLOTTI=='S',2,;
      iif(this.Parent.oContained.w_STPLOTTI=='X',3,;
      iif(this.Parent.oContained.w_STPLOTTI=='U',4,;
      0))))
  endfunc

  func oSTPLOTTI_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP<>'F')
    endwith
   endif
  endfunc

  func oSTPLOTTI_1_4.mHide()
    with this.Parent.oContained
      return (.w_PARAM = 'M')
    endwith
  endfunc

  add object oARTINI_1_5 as StdField with uid="LOPMIFTCRS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ARTINI", cQueryName = "ARTINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "L'articolo iniziale � maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Codice articolo iniziale",;
    HelpContextID = 154920186,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=119, Top=46, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_ARTINI"

  func oARTINI_1_5.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> 'M')
    endwith
  endfunc

  func oARTINI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTINI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTINI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oARTINI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'ARTMATR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oARTINI_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ARTINI
     i_obj.ecpSave()
  endproc

  add object oARTINI_1_6 as StdField with uid="WBWJLGFCQZ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ARTINI", cQueryName = "ARTINI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "L'articolo iniziale � maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Codice articolo iniziale",;
    HelpContextID = 154920186,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=119, Top=46, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_ARTINI"

  func oARTINI_1_6.mHide()
    with this.Parent.oContained
      return (.w_PARAM = 'M')
    endwith
  endfunc

  func oARTINI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTINI_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTINI_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oARTINI_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'GSMA_SAR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oARTINI_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ARTINI
     i_obj.ecpSave()
  endproc

  add object oARTFIN_1_7 as StdField with uid="OQDMFYCAAG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ARTFIN", cQueryName = "ARTFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "L'articolo iniziale � maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Codice articolo finale",;
    HelpContextID = 76473594,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=119, Top=72, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_ARTFIN"

  func oARTFIN_1_7.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> 'M')
    endwith
  endfunc

  func oARTFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTFIN_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTFIN_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oARTFIN_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'ARTMATR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oARTFIN_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ARTFIN
     i_obj.ecpSave()
  endproc

  add object oDESINI_1_8 as StdField with uid="KNBVYXEHDK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Decrizione articolo iniziale",;
    HelpContextID = 154927562,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=277, Top=46, InputMask=replicate('X',40)

  add object oARTFIN_1_9 as StdField with uid="WDKCPPJCFG",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ARTFIN", cQueryName = "ARTFIN",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "L'articolo iniziale � maggiore di quello finale oppure obsoleto",;
    ToolTipText = "Codice articolo finale",;
    HelpContextID = 76473594,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=119, Top=72, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_ARTFIN"

  func oARTFIN_1_9.mHide()
    with this.Parent.oContained
      return (.w_PARAM = 'M')
    endwith
  endfunc

  func oARTFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTFIN_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTFIN_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oARTFIN_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'GSMA_SAR.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oARTFIN_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ARTFIN
     i_obj.ecpSave()
  endproc

  add object oCODLIS_1_10 as StdField with uid="UCEXJDRKGF",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODLIS", cQueryName = "CODLIS",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice listino inesistente o obsoleto",;
    ToolTipText = "Codice listino",;
    HelpContextID = 260696026,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=118, Top=101, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_CODLIS"

  func oCODLIS_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODLIS_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODLIS_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oCODLIS_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'',this.parent.oContained
  endproc
  proc oCODLIS_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_CODLIS
     i_obj.ecpSave()
  endproc

  add object oCODVAL_1_11 as StdField with uid="INEMLQIFQI",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta del listino selezionato",;
    HelpContextID = 117434330,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=230, Top=101, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CODVAL"

  func oCODVAL_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oTIPUMI_1_12 as StdCombo with uid="ZLVISPSMQV",value=3,rtseq=12,rtrep=.f.,left=277,top=101,width=99,height=21;
    , ToolTipText = "Cerca il listino nell'unit� di misura indicata (prima, seconda o tutte)";
    , HelpContextID = 155200714;
    , cFormVar="w_TIPUMI",RowSource=""+"Nella 1a U.m.,"+"Nella 2a U.m.,"+"In tutte le U.m.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPUMI_1_12.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,0,;
    0))))
  endfunc
  func oTIPUMI_1_12.GetRadio()
    this.Parent.oContained.w_TIPUMI = this.RadioValue()
    return .t.
  endfunc

  func oTIPUMI_1_12.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TIPUMI==1,1,;
      iif(this.Parent.oContained.w_TIPUMI==2,2,;
      iif(this.Parent.oContained.w_TIPUMI==0,3,;
      0)))
  endfunc

  func oTIPUMI_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODLIS))
    endwith
   endif
  endfunc

  func oTIPUMI_1_12.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CODLIS))
    endwith
  endfunc

  add object oDATLIS_1_14 as StdField with uid="QAORMUVFAH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATLIS", cQueryName = "DATLIS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data validit� listino",;
    HelpContextID = 260634058,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=118, Top=127

  add object oTUTTIPRE_1_15 as StdCheck with uid="WCZTYCQIXN",rtseq=15,rtrep=.f.,left=230, top=128, caption="Stampa tutti i prezzi",;
    ToolTipText = "Se attivo stampa le etichette per tutti gli scaglioni qta, altrimenti stampa le etichette relativa alla qta pi� piccola",;
    HelpContextID = 42000517,;
    cFormVar="w_TUTTIPRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTUTTIPRE_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTUTTIPRE_1_15.GetRadio()
    this.Parent.oContained.w_TUTTIPRE = this.RadioValue()
    return .t.
  endfunc

  func oTUTTIPRE_1_15.SetRadio()
    this.Parent.oContained.w_TUTTIPRE=trim(this.Parent.oContained.w_TUTTIPRE)
    this.value = ;
      iif(this.Parent.oContained.w_TUTTIPRE=='S',1,;
      0)
  endfunc

  func oTUTTIPRE_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODLIS))
    endwith
   endif
  endfunc

  func oTUTTIPRE_1_15.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CODLIS))
    endwith
  endfunc

  add object oSTAMPQTA_1_16 as StdCheck with uid="ICYYJQOFAX",rtseq=16,rtrep=.f.,left=390, top=128, caption="Stampa scaglioni quantit�",;
    ToolTipText = "Se attivo stampa gli scaglioni qta. Nelle etichette",;
    HelpContextID = 250015335,;
    cFormVar="w_STAMPQTA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTAMPQTA_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSTAMPQTA_1_16.GetRadio()
    this.Parent.oContained.w_STAMPQTA = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPQTA_1_16.SetRadio()
    this.Parent.oContained.w_STAMPQTA=trim(this.Parent.oContained.w_STAMPQTA)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPQTA=='S',1,;
      0)
  endfunc

  func oSTAMPQTA_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODLIS))
    endwith
   endif
  endfunc

  func oSTAMPQTA_1_16.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CODLIS))
    endwith
  endfunc

  add object oDESFIN_1_17 as StdField with uid="PWGASOLVUP",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Decrizione articolo finale",;
    HelpContextID = 76480970,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=277, Top=72, InputMask=replicate('X',40)

  add object oCODFAM_1_18 as StdField with uid="TPLOZOVNDK",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CODFAM", cQueryName = "CODFAM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice famiglia",;
    HelpContextID = 101705690,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=118, Top=156, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAM"

  func oCODFAM_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAM_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAM_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAM_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglie articoli",'',this.parent.oContained
  endproc
  proc oCODFAM_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_CODFAM
     i_obj.ecpSave()
  endproc

  add object oDESFAM_1_19 as StdField with uid="IVGOLHALFV",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 101646794,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=188, Top=156, InputMask=replicate('X',35)

  add object oCODGRU_1_20 as StdField with uid="PGIRHGZVWK",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODGRU", cQueryName = "CODGRU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo merceologico",;
    HelpContextID = 218032090,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=118, Top=182, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_CODGRU"

  func oCODGRU_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRU_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oCODGRU_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oCODGRU_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_CODGRU
     i_obj.ecpSave()
  endproc

  add object oDESGRU_1_21 as StdField with uid="EOSJWREDPZ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 217973194,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=188, Top=182, InputMask=replicate('X',35)

  add object oCODCAT_1_22 as StdField with uid="GZGNNSEBRR",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CODCAT", cQueryName = "CODCAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice categoria omogenea",;
    HelpContextID = 252897242,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=118, Top=208, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CODCAT"

  func oCODCAT_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAT_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCODCAT_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc oCODCAT_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CODCAT
     i_obj.ecpSave()
  endproc

  add object oDESCAT_1_23 as StdField with uid="AEYCFVOMXW",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 252838346,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=188, Top=208, InputMask=replicate('X',35)

  add object oCODMAR_1_24 as StdField with uid="TQUOVOZXDY",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CODMAR", cQueryName = "CODMAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice marca",;
    HelpContextID = 17360858,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=118, Top=234, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_CODMAR"

  func oCODMAR_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAR_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAR_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oCODMAR_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchi",'',this.parent.oContained
  endproc
  proc oCODMAR_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_CODMAR
     i_obj.ecpSave()
  endproc


  add object oOBSOLETI_1_25 as StdCombo with uid="RWINZUVBMH",rtseq=25,rtrep=.f.,left=119,top=264,width=96,height=21;
    , ToolTipText = "Stampa articoli solo validi, solo obsoleti o tutti";
    , HelpContextID = 44694575;
    , cFormVar="w_OBSOLETI",RowSource=""+"Non obsoleti,"+"Solo obsoleti,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOBSOLETI_1_25.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oOBSOLETI_1_25.GetRadio()
    this.Parent.oContained.w_OBSOLETI = this.RadioValue()
    return .t.
  endfunc

  func oOBSOLETI_1_25.SetRadio()
    this.Parent.oContained.w_OBSOLETI=trim(this.Parent.oContained.w_OBSOLETI)
    this.value = ;
      iif(this.Parent.oContained.w_OBSOLETI=='N',1,;
      iif(this.Parent.oContained.w_OBSOLETI=='S',2,;
      iif(this.Parent.oContained.w_OBSOLETI=='T',3,;
      0)))
  endfunc

  add object oDATINS_1_26 as StdField with uid="BNNZJYIOPC",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DATINS", cQueryName = "DATINS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio selezione",;
    HelpContextID = 255587786,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=363, Top=266


  add object oCODOBS_1_27 as StdCombo with uid="RRVSCLUWBB",rtseq=27,rtrep=.f.,left=119,top=293,width=96,height=21;
    , ToolTipText = "Stampa codici di ricerca solo validi, solo obsoleti o tutti";
    , HelpContextID = 267839450;
    , cFormVar="w_CODOBS",RowSource=""+"Non obsoleti,"+"Solo obsoleti,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCODOBS_1_27.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oCODOBS_1_27.GetRadio()
    this.Parent.oContained.w_CODOBS = this.RadioValue()
    return .t.
  endfunc

  func oCODOBS_1_27.SetRadio()
    this.Parent.oContained.w_CODOBS=trim(this.Parent.oContained.w_CODOBS)
    this.value = ;
      iif(this.Parent.oContained.w_CODOBS=='N',1,;
      iif(this.Parent.oContained.w_CODOBS=='S',2,;
      iif(this.Parent.oContained.w_CODOBS=='T',3,;
      0)))
  endfunc

  add object oDESMAR_1_28 as StdField with uid="EUHFXBJVSN",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESMAR", cQueryName = "DESMAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 17301962,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=188, Top=234, InputMask=replicate('X',35)

  add object oDATASTAM_1_29 as StdField with uid="LKYRVBTGIH",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DATASTAM", cQueryName = "DATASTAM",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 234091901,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=363, Top=293


  add object oTIPBAR_1_31 as StdCombo with uid="IROFBRNUTD",rtseq=30,rtrep=.f.,left=118,top=323,width=140,height=21;
    , ToolTipText = "Tipo codice a barre";
    , HelpContextID = 18033866;
    , cFormVar="w_TIPBAR",RowSource=""+"EAN 8,"+"EAN 13,"+"ALFA 39,"+"UPC A,"+"Tutti,"+"Tutti no barcode", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPBAR_1_31.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'T',;
    iif(this.value =6,'X',;
    space(1))))))))
  endfunc
  func oTIPBAR_1_31.GetRadio()
    this.Parent.oContained.w_TIPBAR = this.RadioValue()
    return .t.
  endfunc

  func oTIPBAR_1_31.SetRadio()
    this.Parent.oContained.w_TIPBAR=trim(this.Parent.oContained.w_TIPBAR)
    this.value = ;
      iif(this.Parent.oContained.w_TIPBAR=='1',1,;
      iif(this.Parent.oContained.w_TIPBAR=='2',2,;
      iif(this.Parent.oContained.w_TIPBAR=='3',3,;
      iif(this.Parent.oContained.w_TIPBAR=='4',4,;
      iif(this.Parent.oContained.w_TIPBAR=='T',5,;
      iif(this.Parent.oContained.w_TIPBAR=='X',6,;
      0))))))
  endfunc


  add object oBtn_1_33 as StdButton with uid="KKLSKOSXWN",left=474, top=379, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 251064794;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        do GSMA_BAB with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OREP) and not empty(.w_DATASTAM))
      endwith
    endif
  endfunc


  add object oObj_1_34 as cp_outputCombo with uid="ZLERXIVPWT",left=118, top=350, width=407,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,cNoDefSep="",cDefSep="",;
    nPag=1;
    , HelpContextID = 53578726


  add object oBtn_1_35 as StdButton with uid="BYHTZQMBKR",left=525, top=379, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 117101498;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_30 as StdString with uid="HZIELTBXDQ",Visible=.t., Left=17, Top=46,;
    Alignment=1, Width=99, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="LJDRQZRTIG",Visible=.t., Left=17, Top=72,;
    Alignment=1, Width=99, Height=15,;
    Caption="Ad articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="WCAEIDQCKM",Visible=.t., Left=17, Top=352,;
    Alignment=1, Width=99, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="ZFTEMDKZAH",Visible=.t., Left=17, Top=156,;
    Alignment=1, Width=99, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="FEURBKXRAG",Visible=.t., Left=7, Top=182,;
    Alignment=1, Width=109, Height=15,;
    Caption="Gr. merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="RHHWJFLIHG",Visible=.t., Left=261, Top=293,;
    Alignment=1, Width=100, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="GJZBLNKHGX",Visible=.t., Left=6, Top=208,;
    Alignment=1, Width=110, Height=15,;
    Caption="Cat. omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="HPWHCHUVEZ",Visible=.t., Left=17, Top=234,;
    Alignment=1, Width=99, Height=15,;
    Caption="Marca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="QYPBEUUKJI",Visible=.t., Left=17, Top=101,;
    Alignment=1, Width=99, Height=15,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="YBZYIWVCHV",Visible=.t., Left=17, Top=127,;
    Alignment=1, Width=99, Height=15,;
    Caption="Validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="TYWSLQADFA",Visible=.t., Left=17, Top=323,;
    Alignment=1, Width=99, Height=15,;
    Caption="Codice a barre:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="YZDJOGRAHL",Visible=.t., Left=170, Top=101,;
    Alignment=1, Width=59, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="DIMABSSJNQ",Visible=.t., Left=7, Top=264,;
    Alignment=1, Width=110, Height=18,;
    Caption="Articoli obsoleti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="RWIEWRBBGH",Visible=.t., Left=230, Top=266,;
    Alignment=1, Width=132, Height=18,;
    Caption="Variati/inseriti dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="JNVRGLMYCV",Visible=.t., Left=7, Top=293,;
    Alignment=1, Width=110, Height=18,;
    Caption="Codici obsoleti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="ZFCHNXLYAY",Visible=.t., Left=43, Top=16,;
    Alignment=1, Width=73, Height=18,;
    Caption="Tipo stampa:"  ;
  , bGlobalFont=.t.

  func oStr_1_55.mHide()
    with this.Parent.oContained
      return (.w_PARAM = 'M')
    endwith
  endfunc
enddefine
define class tgsma_sabPag2 as StdContainer
  Width  = 585
  height = 424
  stdWidth  = 585
  stdheight = 424
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFUNIMI_2_15 as StdCombo with uid="PAEHHPHJOV",rtseq=37,rtrep=.f.,left=127,top=24,width=85,height=21;
    , ToolTipText = "U.M. di riferimento per la stampa";
    , HelpContextID = 155992490;
    , cFormVar="w_FUNIMI",RowSource=""+"Principale,"+"Secondaria,"+"Alternativa", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFUNIMI_2_15.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'S',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oFUNIMI_2_15.GetRadio()
    this.Parent.oContained.w_FUNIMI = this.RadioValue()
    return .t.
  endfunc

  func oFUNIMI_2_15.SetRadio()
    this.Parent.oContained.w_FUNIMI=trim(this.Parent.oContained.w_FUNIMI)
    this.value = ;
      iif(this.Parent.oContained.w_FUNIMI=='P',1,;
      iif(this.Parent.oContained.w_FUNIMI=='S',2,;
      iif(this.Parent.oContained.w_FUNIMI=='A',3,;
      0)))
  endfunc

  func oFUNIMI_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPUMI=1)
    endwith
   endif
  endfunc

  add object oPREMAS_2_16 as StdCheck with uid="TEGKSFLVWR",rtseq=38,rtrep=.f.,left=222, top=24, caption="Prezzi mascherati",;
    ToolTipText = "Se attivo maschera la stampa del prezzo",;
    HelpContextID = 578570,;
    cFormVar="w_PREMAS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPREMAS_2_16.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPREMAS_2_16.GetRadio()
    this.Parent.oContained.w_PREMAS = this.RadioValue()
    return .t.
  endfunc

  func oPREMAS_2_16.SetRadio()
    this.Parent.oContained.w_PREMAS=trim(this.Parent.oContained.w_PREMAS)
    this.value = ;
      iif(this.Parent.oContained.w_PREMAS=='S',1,;
      0)
  endfunc


  add object oPROVIEN_2_17 as StdCombo with uid="LLFMQJWOET",rtseq=39,rtrep=.f.,left=127,top=56,width=85,height=21;
    , ToolTipText = "Indica la provenienza del codice: interna, cliente o fornitore o altro";
    , HelpContextID = 226440202;
    , cFormVar="w_PROVIEN",RowSource=""+"Interna,"+"Fornitore,"+"Cliente,"+"Tutte", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPROVIEN_2_17.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'F',;
    iif(this.value =3,'C',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oPROVIEN_2_17.GetRadio()
    this.Parent.oContained.w_PROVIEN = this.RadioValue()
    return .t.
  endfunc

  func oPROVIEN_2_17.SetRadio()
    this.Parent.oContained.w_PROVIEN=trim(this.Parent.oContained.w_PROVIEN)
    this.value = ;
      iif(this.Parent.oContained.w_PROVIEN=='R',1,;
      iif(this.Parent.oContained.w_PROVIEN=='F',2,;
      iif(this.Parent.oContained.w_PROVIEN=='C',3,;
      iif(this.Parent.oContained.w_PROVIEN=='T',4,;
      0))))
  endfunc

  func oPROVIEN_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_2_18('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCON_2_18 as StdField with uid="EDDONEAZYD",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice intestatario inesistente oppure obsoleto",;
    ToolTipText = "Eventuale cliente / fornitore di provenienza",;
    HelpContextID = 70445018,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=127, Top=88, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_PROVIEN", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PROVIEN $ 'CF')
    endwith
   endif
  endfunc

  func oCODCON_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PROVIEN)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_PROVIEN)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'GSCG1MPN.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_2_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_PROVIEN
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oDESCRI_2_19 as StdField with uid="OFSZWBAZUK",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cliente / fornitore",;
    HelpContextID = 151126474,;
   bGlobalFont=.t.,;
    Height=21, Width=302, Left=263, Top=88, InputMask=replicate('X',40)


  add object oNUMCOP_2_20 as StdCombo with uid="LLOWBWMZLN",rtseq=42,rtrep=.f.,left=127,top=117,width=305,height=21;
    , ToolTipText = "Criterio di calcolo per numero di copie delle stampe";
    , HelpContextID = 36852010;
    , cFormVar="w_NUMCOP",RowSource=""+"Copie calcolate sull'esistenza a magazzino,"+"Copie calcolate sui movimenti di magazzino/documenti,"+"Fisso", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oNUMCOP_2_20.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'M',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oNUMCOP_2_20.GetRadio()
    this.Parent.oContained.w_NUMCOP = this.RadioValue()
    return .t.
  endfunc

  func oNUMCOP_2_20.SetRadio()
    this.Parent.oContained.w_NUMCOP=trim(this.Parent.oContained.w_NUMCOP)
    this.value = ;
      iif(this.Parent.oContained.w_NUMCOP=='E',1,;
      iif(this.Parent.oContained.w_NUMCOP=='M',2,;
      iif(this.Parent.oContained.w_NUMCOP=='F',3,;
      0)))
  endfunc

  func oNUMCOP_2_20.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> 'A')
    endwith
  endfunc


  add object oNUMCOP_2_21 as StdCombo with uid="RDBECIBBPJ",rtseq=43,rtrep=.f.,left=127,top=117,width=305,height=21;
    , ToolTipText = "Criterio di calcolo per numero di copie delle stampe";
    , HelpContextID = 36852010;
    , cFormVar="w_NUMCOP",RowSource=""+"Copie calcolate sull'esistenza a magazzino,"+"Copie calcolate sui movimenti di magazzino/documenti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oNUMCOP_2_21.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'M',;
    space(1))))
  endfunc
  func oNUMCOP_2_21.GetRadio()
    this.Parent.oContained.w_NUMCOP = this.RadioValue()
    return .t.
  endfunc

  func oNUMCOP_2_21.SetRadio()
    this.Parent.oContained.w_NUMCOP=trim(this.Parent.oContained.w_NUMCOP)
    this.value = ;
      iif(this.Parent.oContained.w_NUMCOP=='E',1,;
      iif(this.Parent.oContained.w_NUMCOP=='M',2,;
      0))
  endfunc

  func oNUMCOP_2_21.mHide()
    with this.Parent.oContained
      return (.w_PARAM <> 'M')
    endwith
  endfunc

  add object oDATINI_2_22 as StdField with uid="DICVDRTMUL",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione per i documenti/movimenti di magazzino",;
    HelpContextID = 154924490,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=127, Top=146

  func oDATINI_2_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP='M')
    endwith
   endif
  endfunc

  add object oDATFIN_2_23 as StdField with uid="WBWPOGBTXF",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine selezione per i documenti/movimenti di magazzino",;
    HelpContextID = 76477898,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=356, Top=148

  func oDATFIN_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP='M')
    endwith
   endif
  endfunc


  add object oTIPCON_2_24 as StdCombo with uid="PVECWLBVQG",value=3,rtseq=46,rtrep=.f.,left=127,top=174,width=118,height=21;
    , ToolTipText = "Tipo intestatario";
    , HelpContextID = 70397130;
    , cFormVar="w_TIPCON",RowSource=""+"Cliente,"+"Fornitore,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPCON_2_24.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oTIPCON_2_24.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_2_24.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      iif(this.Parent.oContained.w_TIPCON=='',3,;
      0)))
  endfunc

  func oTIPCON_2_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP='M')
    endwith
   endif
  endfunc

  func oTIPCON_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODINT)
        bRes2=.link_2_25('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODINT_2_25 as StdField with uid="ZCPEEKMWJN",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CODINT", cQueryName = "CODINT",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice intestatario movimento/documento",;
    HelpContextID = 238872538,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=127, Top=206, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODINT"

  func oCODINT_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP='M' AND .w_TIPCON $ 'CF')
    endwith
   endif
  endfunc

  func oCODINT_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINT_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINT_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODINT_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco intestatari",'GSTE_KMS.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODINT_2_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODINT
     i_obj.ecpSave()
  endproc

  add object oININDOC_2_26 as StdField with uid="MKJKESKAYA",rtseq=48,rtrep=.f.,;
    cFormVar = "w_ININDOC", cQueryName = "ININDOC",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento iniziale",;
    HelpContextID = 203974534,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=127, Top=234

  func oININDOC_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP='M')
    endwith
   endif
  endfunc

  add object oFINNDOC_2_27 as StdField with uid="ARNAUDRIQE",rtseq=49,rtrep=.f.,;
    cFormVar = "w_FINNDOC", cQueryName = "FINNDOC",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento finale",;
    HelpContextID = 203993686,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=426, Top=236

  func oFINNDOC_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP='M')
    endwith
   endif
  endfunc

  add object oTIPDOC_2_28 as StdField with uid="FFHXDIIFVU",rtseq=50,rtrep=.f.,;
    cFormVar = "w_TIPDOC", cQueryName = "TIPDOC",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice causale documento",;
    HelpContextID = 254880970,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=127, Top=263, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAR_BZA", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPDOC"

  func oTIPDOC_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP='M')
    endwith
   endif
  endfunc

  func oTIPDOC_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPDOC_2_28.ecpDrop(oSource)
    this.Parent.oContained.link_2_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPDOC_2_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPDOC_2_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZA',"Causali documenti",'GSVE9ATD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPDOC_2_28.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPDOC
     i_obj.ecpSave()
  endproc

  add object oCODCA1_2_29 as StdField with uid="MIXQILJAVR",rtseq=51,rtrep=.f.,;
    cFormVar = "w_CODCA1", cQueryName = "CODCA1",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "1� Causale di magazzino selezionata",;
    HelpContextID = 34793434,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=98, Top=293, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_CODCA1"

  func oCODCA1_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP='M')
    endwith
   endif
  endfunc

  func oCODCA1_2_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCA1_2_29.ecpDrop(oSource)
    this.Parent.oContained.link_2_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCA1_2_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oCODCA1_2_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oCODCA1_2_29.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_CODCA1
     i_obj.ecpSave()
  endproc

  add object oCODCA2_2_30 as StdField with uid="AVQWWSIBNI",rtseq=52,rtrep=.f.,;
    cFormVar = "w_CODCA2", cQueryName = "CODCA2",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "2� Causale di magazzino selezionata",;
    HelpContextID = 18016218,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=98, Top=320, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_CODCA2"

  func oCODCA2_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP='M')
    endwith
   endif
  endfunc

  func oCODCA2_2_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCA2_2_30.ecpDrop(oSource)
    this.Parent.oContained.link_2_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCA2_2_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oCODCA2_2_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oCODCA2_2_30.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_CODCA2
     i_obj.ecpSave()
  endproc

  add object oCODCA3_2_31 as StdField with uid="ALXNOJBIIT",rtseq=53,rtrep=.f.,;
    cFormVar = "w_CODCA3", cQueryName = "CODCA3",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "3� Causale di magazzino selezionata",;
    HelpContextID = 1239002,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=98, Top=347, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_CODCA3"

  func oCODCA3_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP='M')
    endwith
   endif
  endfunc

  func oCODCA3_2_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCA3_2_31.ecpDrop(oSource)
    this.Parent.oContained.link_2_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCA3_2_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oCODCA3_2_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oCODCA3_2_31.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_CODCA3
     i_obj.ecpSave()
  endproc

  add object oCODCA4_2_32 as StdField with uid="WRTIOWYODH",rtseq=54,rtrep=.f.,;
    cFormVar = "w_CODCA4", cQueryName = "CODCA4",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "4� Causale di magazzino selezionata",;
    HelpContextID = 252897242,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=386, Top=295, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_CODCA4"

  func oCODCA4_2_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP='M')
    endwith
   endif
  endfunc

  func oCODCA4_2_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCA4_2_32.ecpDrop(oSource)
    this.Parent.oContained.link_2_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCA4_2_32.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oCODCA4_2_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oCODCA4_2_32.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_CODCA4
     i_obj.ecpSave()
  endproc

  add object oCODCA5_2_33 as StdField with uid="WJMKETNJEN",rtseq=55,rtrep=.f.,;
    cFormVar = "w_CODCA5", cQueryName = "CODCA5",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "5� Causale di magazzino selezionata",;
    HelpContextID = 236120026,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=386, Top=322, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_CODCA5"

  func oCODCA5_2_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP='M')
    endwith
   endif
  endfunc

  func oCODCA5_2_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCA5_2_33.ecpDrop(oSource)
    this.Parent.oContained.link_2_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCA5_2_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oCODCA5_2_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzino",'',this.parent.oContained
  endproc
  proc oCODCA5_2_33.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_CODCA5
     i_obj.ecpSave()
  endproc

  add object oCAUDESC1_2_34 as StdField with uid="PQBFFGFXDE",rtseq=56,rtrep=.f.,;
    cFormVar = "w_CAUDESC1", cQueryName = "CAUDESC1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione causale magazzino",;
    HelpContextID = 3086935,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=166, Top=293, InputMask=replicate('X',35)

  add object oCAUDESC2_2_35 as StdField with uid="FFWVCSLTZK",rtseq=57,rtrep=.f.,;
    cFormVar = "w_CAUDESC2", cQueryName = "CAUDESC2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione causale magazzino",;
    HelpContextID = 3086936,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=166, Top=320, InputMask=replicate('X',35)

  add object oCAUDESC3_2_36 as StdField with uid="JSPXXLSAXI",rtseq=58,rtrep=.f.,;
    cFormVar = "w_CAUDESC3", cQueryName = "CAUDESC3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione causale magazzino",;
    HelpContextID = 3086937,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=166, Top=345, InputMask=replicate('X',35)

  add object oCAUDESC4_2_37 as StdField with uid="LGSNJRRXLA",rtseq=59,rtrep=.f.,;
    cFormVar = "w_CAUDESC4", cQueryName = "CAUDESC4",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione causale magazzino",;
    HelpContextID = 3086938,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=455, Top=294, InputMask=replicate('X',35)

  add object oCAUDESC5_2_38 as StdField with uid="IZOYIFPDOZ",rtseq=60,rtrep=.f.,;
    cFormVar = "w_CAUDESC5", cQueryName = "CAUDESC5",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione causale magazzino",;
    HelpContextID = 3086939,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=455, Top=321, InputMask=replicate('X',35)

  add object oCODMAG_2_39 as StdField with uid="BEAREZOUPQ",rtseq=61,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino selezionato",;
    HelpContextID = 201910234,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=386, Top=349, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_2_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP$'EM')
    endwith
   endif
  endfunc

  func oCODMAG_2_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_2_39.ecpDrop(oSource)
    this.Parent.oContained.link_2_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_2_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_2_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_2_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_2_40 as StdField with uid="FOBTDUYCAE",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione magazzino selezionato",;
    HelpContextID = 201851338,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=455, Top=348, InputMask=replicate('X',30)

  add object oPERRIC_2_41 as StdField with uid="SGBTSIRLPF",rtseq=63,rtrep=.f.,;
    cFormVar = "w_PERRIC", cQueryName = "PERRIC",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Se il numero copie � calcolato sull'esistenza o sui movimenti aumenta il num. copie per articolo di una percentuale",;
    HelpContextID = 260247818,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=98, Top=378, cSayPict='"9999"', cGetPict='"9999"'

  func oPERRIC_2_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP $'EM')
    endwith
   endif
  endfunc

  func oPERRIC_2_41.mHide()
    with this.Parent.oContained
      return (.w_PARAM = 'M')
    endwith
  endfunc

  add object oPERMUC_2_43 as StdField with uid="KZPFQDMFKT",rtseq=64,rtrep=.f.,;
    cFormVar = "w_PERMUC", cQueryName = "PERMUC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codifica per mascherare i prezzi",;
    HelpContextID = 247992586,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=442, Top=53, InputMask=replicate('X',10)

  add object oCOPIE_2_46 as StdField with uid="FTAMHCBVIM",rtseq=65,rtrep=.f.,;
    cFormVar = "w_COPIE", cQueryName = "COPIE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Se il numero copie � calcolato sul fisso aumenta il num. copie per articolo",;
    HelpContextID = 46933978,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=245, Top=378, cSayPict='"99999"', cGetPict='"99999"'

  func oCOPIE_2_46.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMCOP='F')
    endwith
   endif
  endfunc

  func oCOPIE_2_46.mHide()
    with this.Parent.oContained
      return (.w_NUMCOP<>'F')
    endwith
  endfunc

  add object oNUMSKIP_2_49 as StdField with uid="IBMXVQRFIQ",rtseq=67,rtrep=.f.,;
    cFormVar = "w_NUMSKIP", cQueryName = "NUMSKIP",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di etichette da saltare",;
    HelpContextID = 157438250,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=455, Top=378

  add object oCODESCRI_2_53 as StdField with uid="WGMBEQJHUD",rtseq=68,rtrep=.f.,;
    cFormVar = "w_CODESCRI", cQueryName = "CODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione intestatario",;
    HelpContextID = 250668945,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=251, Top=206, InputMask=replicate('X',40)

  add object oTIPDOCDES_2_57 as StdField with uid="ZZYVRVOSFP",rtseq=69,rtrep=.f.,;
    cFormVar = "w_TIPDOCDES", cQueryName = "TIPDOCDES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 13555883,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=205, Top=263, InputMask=replicate('X',35)

  add object oStr_2_1 as StdString with uid="DHQMHNZIQD",Visible=.t., Left=15, Top=24,;
    Alignment=1, Width=107, Height=18,;
    Caption="Unit� di misura:"  ;
  , bGlobalFont=.t.

  add object oStr_2_2 as StdString with uid="NZYWHUFLLR",Visible=.t., Left=37, Top=57,;
    Alignment=1, Width=85, Height=18,;
    Caption="Provenienza:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="JFPQRLETPO",Visible=.t., Left=37, Top=88,;
    Alignment=1, Width=85, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_2_3.mHide()
    with this.Parent.oContained
      return (.w_PROVIEN<>'F')
    endwith
  endfunc

  add object oStr_2_4 as StdString with uid="EUJMAYUOHN",Visible=.t., Left=37, Top=88,;
    Alignment=1, Width=85, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_2_4.mHide()
    with this.Parent.oContained
      return (.w_PROVIEN<>'C')
    endwith
  endfunc

  add object oStr_2_5 as StdString with uid="ANHMCBNJFM",Visible=.t., Left=37, Top=118,;
    Alignment=1, Width=85, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="XHYHUZMUMX",Visible=.t., Left=28, Top=293,;
    Alignment=1, Width=65, Height=18,;
    Caption="1� Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="EKWGQJBCOO",Visible=.t., Left=296, Top=349,;
    Alignment=1, Width=85, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="IPOGLOYGWR",Visible=.t., Left=28, Top=321,;
    Alignment=1, Width=65, Height=18,;
    Caption="2� Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_9 as StdString with uid="USPEFMOPPC",Visible=.t., Left=28, Top=347,;
    Alignment=1, Width=65, Height=18,;
    Caption="3� Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="KNTMJUPDGW",Visible=.t., Left=316, Top=295,;
    Alignment=1, Width=65, Height=18,;
    Caption="4� Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="AGAODWOVSI",Visible=.t., Left=316, Top=322,;
    Alignment=1, Width=65, Height=18,;
    Caption="5� Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="SCBQHSDDSQ",Visible=.t., Left=7, Top=378,;
    Alignment=1, Width=86, Height=18,;
    Caption="% Ricarico:"  ;
  , bGlobalFont=.t.

  func oStr_2_12.mHide()
    with this.Parent.oContained
      return (.w_PARAM = 'M')
    endwith
  endfunc

  add object oStr_2_13 as StdString with uid="VTERMHAYZD",Visible=.t., Left=62, Top=147,;
    Alignment=1, Width=65, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="VRZHECYFTE",Visible=.t., Left=290, Top=147,;
    Alignment=1, Width=65, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_42 as StdString with uid="OSOLIRRWCG",Visible=.t., Left=443, Top=38,;
    Alignment=0, Width=70, Height=18,;
    Caption="0123456789"  ;
  , bGlobalFont=.t.

  add object oStr_2_44 as StdString with uid="PQGTCNCYJW",Visible=.t., Left=379, Top=19,;
    Alignment=1, Width=143, Height=18,;
    Caption="Codifica maschera prezzi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_47 as StdString with uid="XBZAXWSEEP",Visible=.t., Left=154, Top=378,;
    Alignment=1, Width=90, Height=18,;
    Caption="Numero copie:"  ;
  , bGlobalFont=.t.

  func oStr_2_47.mHide()
    with this.Parent.oContained
      return (.w_NUMCOP<>'F')
    endwith
  endfunc

  add object oStr_2_48 as StdString with uid="NZVXQBQMDX",Visible=.t., Left=-5, Top=428,;
    Alignment=0, Width=330, Height=19,;
    Caption="Attenzione: i campi w_numcop sono sovrapposti"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_48.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_2_50 as StdString with uid="CGEZOFVZWQ",Visible=.t., Left=307, Top=378,;
    Alignment=1, Width=146, Height=18,;
    Caption="Etichette da saltare:"  ;
  , bGlobalFont=.t.

  add object oStr_2_51 as StdString with uid="YCIVHTBTFT",Visible=.t., Left=76, Top=176,;
    Alignment=1, Width=50, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_52 as StdString with uid="AZJLQVAATV",Visible=.t., Left=37, Top=206,;
    Alignment=1, Width=90, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_2_54 as StdString with uid="HFHSZFGWHO",Visible=.t., Left=27, Top=234,;
    Alignment=1, Width=100, Height=18,;
    Caption="Da numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_55 as StdString with uid="IFPYKLQFIU",Visible=.t., Left=324, Top=236,;
    Alignment=1, Width=100, Height=18,;
    Caption="A numero doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_56 as StdString with uid="VCDVZFNNVK",Visible=.t., Left=51, Top=263,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale doc.:"  ;
  , bGlobalFont=.t.

  add object oBox_2_45 as StdBox with uid="FZAWFUVWKU",left=377, top=15, width=198,height=64
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_sab','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
