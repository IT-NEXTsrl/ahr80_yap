* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kcr                                                        *
*              Elenco voci del tariffario                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-09                                                      *
* Last revis.: 2014-04-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kcr",oParentObject))

* --- Class definition
define class tgsar_kcr as StdForm
  Top    = 2
  Left   = 2

  * --- Standard Properties
  Width  = 839
  Height = 567
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-04-11"
  HelpContextID=99186537
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  FAS_TARI_IDX = 0
  cPrg = "gsar_kcr"
  cComment = "Elenco voci del tariffario"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODICE = space(20)
  w_Descri = space(40)
  w_DesAgg = space(40)
  w_CLASSTARIF = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_CACODICE = space(20)
  w_CACODART = space(20)
  w_SOLOPAR = space(1)
  w_DATAOBSO = ctod('  /  /  ')
  w_AGKCR_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kcrPag1","gsar_kcr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_AGKCR_ZOOM = this.oPgFrm.Pages(1).oPag.AGKCR_ZOOM
    DoDefault()
    proc Destroy()
      this.w_AGKCR_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='FAS_TARI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODICE=space(20)
      .w_Descri=space(40)
      .w_DesAgg=space(40)
      .w_CLASSTARIF=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_CACODICE=space(20)
      .w_CACODART=space(20)
      .w_SOLOPAR=space(1)
      .w_DATAOBSO=ctod("  /  /  ")
          .DoRTCalc(1,3,.f.)
        .w_CLASSTARIF = 'K'
        .w_OBTEST = i_datsys
      .oPgFrm.Page1.oPag.AGKCR_ZOOM.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .w_CACODICE = .w_AGKCR_ZOOM.GetVar('CACODICE')
        .w_CACODART = .w_AGKCR_ZOOM.GetVar('CACODART')
        .w_SOLOPAR = 'T'
    endwith
    this.DoRTCalc(9,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.AGKCR_ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .DoRTCalc(1,5,.t.)
            .w_CACODICE = .w_AGKCR_ZOOM.GetVar('CACODICE')
            .w_CACODART = .w_AGKCR_ZOOM.GetVar('CACODART')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.AGKCR_ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_14.visible=!this.oPgFrm.Page1.oPag.oBtn_1_14.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_15.visible=!this.oPgFrm.Page1.oPag.oBtn_1_15.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_16.visible=!this.oPgFrm.Page1.oPag.oBtn_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDATAOBSO_1_23.visible=!this.oPgFrm.Page1.oPag.oDATAOBSO_1_23.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.AGKCR_ZOOM.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_1.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_1.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oDescri_1_2.value==this.w_Descri)
      this.oPgFrm.Page1.oPag.oDescri_1_2.value=this.w_Descri
    endif
    if not(this.oPgFrm.Page1.oPag.oDesAgg_1_3.value==this.w_DesAgg)
      this.oPgFrm.Page1.oPag.oDesAgg_1_3.value=this.w_DesAgg
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASSTARIF_1_5.RadioValue()==this.w_CLASSTARIF)
      this.oPgFrm.Page1.oPag.oCLASSTARIF_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOBTEST_1_6.value==this.w_OBTEST)
      this.oPgFrm.Page1.oPag.oOBTEST_1_6.value=this.w_OBTEST
    endif
    if not(this.oPgFrm.Page1.oPag.oSOLOPAR_1_21.RadioValue()==this.w_SOLOPAR)
      this.oPgFrm.Page1.oPag.oSOLOPAR_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAOBSO_1_23.value==this.w_DATAOBSO)
      this.oPgFrm.Page1.oPag.oDATAOBSO_1_23.value=this.w_DATAOBSO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_OBTEST))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOBTEST_1_6.SetFocus()
            i_bnoObbl = !empty(.w_OBTEST)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_kcrPag1 as StdContainer
  Width  = 835
  height = 567
  stdWidth  = 835
  stdheight = 567
  resizeXpos=364
  resizeYpos=241
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODICE_1_1 as AH_SEARCHFLD with uid="SFSNULZUHG",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice o parte di esso",;
    HelpContextID = 208397274,;
   bGlobalFont=.t.,;
    Height=21, Width=168, Left=76, Top=10, InputMask=replicate('X',20)

  add object oDescri_1_2 as AH_SEARCHFLD with uid="DLLGBSONMY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_Descri", cQueryName = "Descri",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione o parte di essa",;
    HelpContextID = 90103242,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=76, Top=40, InputMask=replicate('X',40)

  add object oDesAgg_1_3 as AH_SEARCHFLD with uid="JSACZFYPKJ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DesAgg", cQueryName = "DesAgg",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione aggiuntiva o parte di essa",;
    HelpContextID = 137420234,;
   bGlobalFont=.t.,;
    Height=21, Width=302, Left=464, Top=40, InputMask=replicate('X',40)


  add object oBtn_1_4 as StdButton with uid="PHEWMSSLEL",left=781, top=10, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Ricerca";
    , HelpContextID = 77735702;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricerca')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oCLASSTARIF_1_5 as StdCombo with uid="TCXAXUPRQG",rtseq=4,rtrep=.f.,left=343,top=10,width=168,height=22;
    , ToolTipText = "Classificazione";
    , HelpContextID = 207735800;
    , cFormVar="w_CLASSTARIF",RowSource=""+"Onorario civile,"+"Onorario penale,"+"Onorario stragiudiziale,"+"Diritto stragiudiziale,"+"Diritto civile,"+"Prestazione generica,"+"Prestazione a tempo,"+"Spesa,"+"Anticipazione,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLASSTARIF_1_5.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    iif(this.value =3,'T',;
    iif(this.value =4,'R',;
    iif(this.value =5,'C',;
    iif(this.value =6,'G',;
    iif(this.value =7,'P',;
    iif(this.value =8,'S',;
    iif(this.value =9,'A',;
    iif(this.value =10,'K',;
    space(1))))))))))))
  endfunc
  func oCLASSTARIF_1_5.GetRadio()
    this.Parent.oContained.w_CLASSTARIF = this.RadioValue()
    return .t.
  endfunc

  func oCLASSTARIF_1_5.SetRadio()
    this.Parent.oContained.w_CLASSTARIF=trim(this.Parent.oContained.w_CLASSTARIF)
    this.value = ;
      iif(this.Parent.oContained.w_CLASSTARIF=='I',1,;
      iif(this.Parent.oContained.w_CLASSTARIF=='E',2,;
      iif(this.Parent.oContained.w_CLASSTARIF=='T',3,;
      iif(this.Parent.oContained.w_CLASSTARIF=='R',4,;
      iif(this.Parent.oContained.w_CLASSTARIF=='C',5,;
      iif(this.Parent.oContained.w_CLASSTARIF=='G',6,;
      iif(this.Parent.oContained.w_CLASSTARIF=='P',7,;
      iif(this.Parent.oContained.w_CLASSTARIF=='S',8,;
      iif(this.Parent.oContained.w_CLASSTARIF=='A',9,;
      iif(this.Parent.oContained.w_CLASSTARIF=='K',10,;
      0))))))))))
  endfunc

  add object oOBTEST_1_6 as StdField with uid="KXZLKOTRZF",rtseq=5,rtrep=.f.,;
    cFormVar = "w_OBTEST", cQueryName = "OBTEST",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 208597018,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=694, Top=10


  add object AGKCR_ZOOM as cp_szoombox with uid="WSBRJLDGUZ",left=-2, top=64, width=846,height=444,;
    caption='AGKCR_ZOOM',;
   bGlobalFont=.t.,;
    bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.t.,bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",cTable="KEY_ARTI",bNoZoomGridShape=.f.,cZoomOnZoom="",cZoomFile="GSAG_KCR",;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 25242043


  add object oObj_1_11 as cp_runprogram with uid="UAIOGLNCRD",left=198, top=589, width=248,height=28,;
    caption='Doppio click',;
   bGlobalFont=.t.,;
    prg="GSAR_BCR('T')",;
    cEvent = "w_AGKCR_ZOOM selected",;
    nPag=1;
    , HelpContextID = 9411673


  add object oBtn_1_14 as StdButton with uid="RJYBUCOVMY",left=8, top=518, width=48,height=45,;
    CpPicture="bmp\SALDI.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza la tariffa";
    , HelpContextID = 115836216;
    , caption='\<Tariffario';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSAR_BCR(this.Parent.oContained,"T")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CACODART))
      endwith
    endif
  endfunc

  func oBtn_1_14.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_CACODICE))
     endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="ARHYBOCNYM",left=60, top=518, width=48,height=45,;
    CpPicture="bmp\CODICI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza il codice di ricerca voce del tariffario";
    , HelpContextID = 105497562;
    , caption='\<Codici';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSAR_BCR(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CACODICE))
      endwith
    endif
  endfunc

  func oBtn_1_15.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_CACODICE))
     endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="SOZKXQTEIP",left=112, top=518, width=48,height=45,;
    CpPicture="bmp\Genera.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare la data di obsolescenza di tutte le tariffe selezionate";
    , HelpContextID = 210344857;
    , caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSAR_BCR(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DATAOBSO))
      endwith
    endif
  endfunc

  func oBtn_1_16.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_CACODICE))
     endwith
    endif
  endfunc


  add object oBtn_1_17 as StdButton with uid="QJTUZLZBDL",left=729, top=518, width=48,height=45,;
    CpPicture="bmp\CARICA.BMP", caption="", nPag=1;
    , ToolTipText = "Nuova tariffa";
    , HelpContextID = 243010858;
    , caption='\<Nuovo';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSAR_BCR(this.Parent.oContained,"N")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="TKCMXZXILU",left=781, top=518, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91869114;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oSOLOPAR_1_21 as StdCombo with uid="IGGZYHQJOR",rtseq=8,rtrep=.f.,left=520,top=10,width=127,height=22;
    , ToolTipText = "Se attivo, vengono riportate solo le tariffe con il parametro di riferimento per la liquidazione dei compensi da parte del giudice";
    , HelpContextID = 261448410;
    , cFormVar="w_SOLOPAR",RowSource=""+"Solo parametri 2012,"+"Solo parametri 2014,"+"Escludi parametri,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSOLOPAR_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'4',;
    iif(this.value =3,'E',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oSOLOPAR_1_21.GetRadio()
    this.Parent.oContained.w_SOLOPAR = this.RadioValue()
    return .t.
  endfunc

  func oSOLOPAR_1_21.SetRadio()
    this.Parent.oContained.w_SOLOPAR=trim(this.Parent.oContained.w_SOLOPAR)
    this.value = ;
      iif(this.Parent.oContained.w_SOLOPAR=='S',1,;
      iif(this.Parent.oContained.w_SOLOPAR=='4',2,;
      iif(this.Parent.oContained.w_SOLOPAR=='E',3,;
      iif(this.Parent.oContained.w_SOLOPAR=='T',4,;
      0))))
  endfunc

  add object oDATAOBSO_1_23 as StdField with uid="LZIINKGSEB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATAOBSO", cQueryName = "DATAOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Inserire la data di obsolescenza da attribuire alle tariffe selezionate",;
    HelpContextID = 21827205,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=278, Top=529

  func oDATAOBSO_1_23.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACODICE))
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="NXVUNSZMCT",Visible=.t., Left=-4, Top=40,;
    Alignment=1, Width=76, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="IYNHQGCNVL",Visible=.t., Left=326, Top=40,;
    Alignment=1, Width=133, Height=18,;
    Caption="Descrizione aggiuntiva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="JUAYSURWTP",Visible=.t., Left=238, Top=10,;
    Alignment=1, Width=100, Height=18,;
    Caption="Classificazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="UGWUNQFGNY",Visible=.t., Left=26, Top=10,;
    Alignment=1, Width=46, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="OUHVGVZCAK",Visible=.t., Left=661, Top=10,;
    Alignment=1, Width=29, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="VPNLXEUQOF",Visible=.t., Left=164, Top=529,;
    Alignment=1, Width=109, Height=18,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CACODICE))
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kcr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
