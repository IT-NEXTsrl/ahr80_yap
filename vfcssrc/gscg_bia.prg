* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bia                                                        *
*              Ricerca registrazioni IVA autotrasportatori                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_2]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-31                                                      *
* Last revis.: 2002-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bia",oParentObject)
return(i_retval)

define class tgscg_bia as StdBatch
  * --- Local variables
  w_NOMCUR = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.oParentObject.w_TOTIMP = 0
    this.oParentObject.w_TOTIVA = 0
    this.w_NOMCUR = this.oParentObject.w_IVAAUTR.cCursor
    this.oParentObject.NotifyEvent("Ricerca")
    SELECT (this.w_NOMCUR)
    SUM IVIMPONI TO this.oParentObject.w_TOTIMP
    SUM IVIMPIVA TO this.oParentObject.w_TOTIVA
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
