* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bci                                                        *
*              Carica imballi                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-16                                                      *
* Last revis.: 2006-05-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bci",oParentObject,m.pTipo)
return(i_retval)

define class tgsar_bci as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_PADRE = .NULL.
  w_CODDIS = space(20)
  w_OBJCTRL = .NULL.
  w_QTAMOV = 0
  w_OSOURCE = .NULL.
  * --- WorkFile variables
  DISTBASE_idx=0
  DISMBASE_idx=0
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  PAR_RIOR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica nel temporaneo degli imballi a perdere la lista degli imballi legati all'erticolo
    *     inserito nella riga documento. (da GSAR_MIP)
    *     
    *     pTipo : A Automatico: lanciato da area manual BlankRecord End.
    *                 M Manuale : premuto bottone carica imballi
    this.w_PADRE = this.oParentObject
    this.w_CODDIS = this.oParentObject.w_OBJ_GEST.w_CODDIS
    this.w_QTAMOV = this.oParentObject.w_OBJ_GEST.w_MVQTAUM1
    * --- Read from DISMBASE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DISMBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2],.t.,this.DISMBASE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DBDISKIT"+;
        " from "+i_cTable+" DISMBASE where ";
            +"DBCODICE = "+cp_ToStrODBC(this.w_CODDIS);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DBDISKIT;
        from (i_cTable) where;
            DBCODICE = this.w_CODDIS;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      w_DISKIT = NVL(cp_ToDate(_read_.DBDISKIT),cp_NullValue(_read_.DBDISKIT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Solo se all'articolo ho associato un kit imballo
    *     Inoltre, solo se ho premuto effettivamente il bottone imballi (SHOWMMT = 'S') 
    *     oppure ho definito esplosione automatica sull'articolo
    if w_DISKIT="I" And this.w_PADRE.bOnScreen And (this.oParentObject.w_OBJ_GEST.w_FLESIM="S" Or this.oParentObject.w_OBJ_GEST.w_SHOWMMT="S")
      if this.pTipo = "M" And this.w_PADRE.NumRow() >0
        * --- Se premo il bottone chiedo se si vuole eliminare gli imballi precedente inseriti
        *     o se si vuole andare in append
        if Ah_YesNo("Si desidera eliminare gli imballi precedentemente inseriti?")
          * --- Nel caso si desidera eliminare le righe precedenti lancio la blanckrec in modo che sbianchi le righe
          *     Nel caso in cui sono in caricamento per� mi devo fermare poich� alla BlankRecordEnd viene rilanciato questo batch
          *     con parametro A.
          *     Esegue quindi la parte successiva.
          *     In modifica invece non mi devo fermare per continuare il caricamento
          this.w_PADRE.BlankRec()     
          if this.oParentObject.w_OBJ_GEST.cFunction="Load"
            i_retcode = 'stop'
            return
          endif
        endif
      endif
      * --- Select from DISTBASE
      i_nConn=i_TableProp[this.DISTBASE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2],.t.,this.DISTBASE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" DISTBASE ";
            +" where DBCODICE = "+cp_ToStrODBC(this.w_CODDIS)+"";
             ,"_Curs_DISTBASE")
      else
        select * from (i_cTable);
         where DBCODICE = this.w_CODDIS;
          into cursor _Curs_DISTBASE
      endif
      if used('_Curs_DISTBASE')
        select _Curs_DISTBASE
        locate for 1=1
        do while not(eof())
        this.w_PADRE.AddRow()     
        this.w_PADRE.w_MVCODICE = _Curs_DISTBASE.DBCODCOM
        * --- Invece delle Read sarebbe stato pi� preciso utilizzare la EcpDrop
        *     Dava per� problemi nella visualizzazione: in esplosione per ricalcolo cauzioni
        *     visualizzava la gestione comunque
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CADESART,CACODART"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_PADRE.w_MVCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CADESART,CACODART;
            from (i_cTable) where;
                CACODICE = this.w_PADRE.w_MVCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PADRE.w_MVDESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
          this.w_PADRE.w_MVCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          this.w_PADRE.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARUNMIS1,ARKITIMB"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_PADRE.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARUNMIS1,ARKITIMB;
            from (i_cTable) where;
                ARCODART = this.w_PADRE.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PADRE.w_MVUNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.w_PADRE.w_KITIMB = NVL(cp_ToDate(_read_.ARKITIMB),cp_NullValue(_read_.ARKITIMB))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from PAR_RIOR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PRCOSSTA"+;
            " from "+i_cTable+" PAR_RIOR where ";
                +"PRCODART = "+cp_ToStrODBC(this.w_PADRE.w_CODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PRCOSSTA;
            from (i_cTable) where;
                PRCODART = this.w_PADRE.w_CODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PADRE.w_COSSTA = NVL(cp_ToDate(_read_.PRCOSSTA),cp_NullValue(_read_.PRCOSSTA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PADRE.mCalc(.t.)     
        * --- La quantit� specificata nel kit indica il numero di pezzi che contiene l'imballo.
        *     Eseguo quindi una divisione e arrotondo all'unit� per eccesso
        this.w_PADRE.w_MVQTAUM1 = INT(this.w_QTAMOV / _Curs_DISTBASE.DBQTADIS) + IIF(cp_Round(this.w_QTAMOV / _Curs_DISTBASE.DBQTADIS,6) = INT(this.w_QTAMOV / _Curs_DISTBASE.DBQTADIS),0,1) 
        this.w_PADRE.w_COSSTA1 = IIF(this.w_PADRE.w_KITIMB="R",this.w_PADRE.w_COSSTA*this.w_PADRE.w_MVQTAUM1,0)
        this.w_PADRE.w_TOTCAU = this.w_PADRE.w_TOTCAU + this.w_PADRE.w_COSSTA1
        this.w_PADRE.w_COSSTA2 = this.w_PADRE.w_COSSTA*this.w_PADRE.w_MVQTAUM1
        this.w_PADRE.w_TOTCOS = this.w_PADRE.w_TOTCOS + this.w_PADRE.w_COSSTA2
        this.w_PADRE.SaveRow()     
          select _Curs_DISTBASE
          continue
        enddo
        use
      endif
      this.w_PADRE.oPgFrm.Page1.oPag.oBody.Refresh()     
    endif
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='DISTBASE'
    this.cWorkTables[2]='DISMBASE'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='PAR_RIOR'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_DISTBASE')
      use in _Curs_DISTBASE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
