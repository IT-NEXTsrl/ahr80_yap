* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kex                                                        *
*              Export contatti                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][134]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-09-03                                                      *
* Last revis.: 2014-07-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kex",oParentObject))

* --- Class definition
define class tgsar_kex as StdForm
  Top    = 50
  Left   = 118

  * --- Standard Properties
  Width  = 623
  Height = 372+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-07-24"
  HelpContextID=65632105
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=33

  * --- Constant Properties
  _IDX = 0
  CAT_ATTR_IDX = 0
  NOM_CONT_IDX = 0
  TIP_CATT_IDX = 0
  OFF_NOMI_IDX = 0
  PAR_OFFE_IDX = 0
  GRU_NOMI_IDX = 0
  ORI_NOMI_IDX = 0
  CPUSERS_IDX = 0
  ZONE_IDX = 0
  AGENTI_IDX = 0
  RUO_CONT_IDX = 0
  ANAG_PRO_IDX = 0
  cPrg = "gsar_kex"
  cComment = "Export contatti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_POCODAZI = space(5)
  w_POOUTEXP = space(40)
  w_OFNOME = space(40)
  w_OFRAGGR = space(15)
  w_FlPublic = space(1)
  w_OFCODCON = space(40)
  w_OFCODCON2 = space(40)
  w_OFDESNOM = space(40)
  w_OFDESNOM2 = space(40)
  w_OFCODNOM = space(20)
  w_OFCODNOM2 = space(20)
  w_OFTIPO = space(1)
  w_OFLOCALI = space(30)
  w_NODATNS = space(1)
  w_OFPROV = space(2)
  o_OFPROV = space(2)
  w_OFGRUNOM = space(5)
  w_OFORIGI = space(5)
  w_OFOPERAT = 0
  w_OFZONA = space(3)
  w_OFAGENTE = space(5)
  w_OFRUOLO = space(5)
  w_OFSOLOBSO = space(1)
  w_POOUTRAG = space(1)
  o_POOUTRAG = space(1)
  w_OFDESGRU = space(35)
  w_OFDESORI = space(35)
  w_OFDESUTE = space(20)
  w_OFDESZON = space(35)
  w_OFDESAGE = space(35)
  w_OFDESRUO = space(35)
  w_OFDATTEST = ctod('  /  /  ')
  w_OFDESPRO = space(30)
  w_OFTIPO = space(1)
  w_OBTEST = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kexPag1","gsar_kex",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsar_kexPag2","gsar_kex",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri aggiuntivi export")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oOFNOME_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='CAT_ATTR'
    this.cWorkTables[2]='NOM_CONT'
    this.cWorkTables[3]='TIP_CATT'
    this.cWorkTables[4]='OFF_NOMI'
    this.cWorkTables[5]='PAR_OFFE'
    this.cWorkTables[6]='GRU_NOMI'
    this.cWorkTables[7]='ORI_NOMI'
    this.cWorkTables[8]='CPUSERS'
    this.cWorkTables[9]='ZONE'
    this.cWorkTables[10]='AGENTI'
    this.cWorkTables[11]='RUO_CONT'
    this.cWorkTables[12]='ANAG_PRO'
    return(this.OpenAllTables(12))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSAR_BEO(this,"E")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_POCODAZI=space(5)
      .w_POOUTEXP=space(40)
      .w_OFNOME=space(40)
      .w_OFRAGGR=space(15)
      .w_FlPublic=space(1)
      .w_OFCODCON=space(40)
      .w_OFCODCON2=space(40)
      .w_OFDESNOM=space(40)
      .w_OFDESNOM2=space(40)
      .w_OFCODNOM=space(20)
      .w_OFCODNOM2=space(20)
      .w_OFTIPO=space(1)
      .w_OFLOCALI=space(30)
      .w_NODATNS=space(1)
      .w_OFPROV=space(2)
      .w_OFGRUNOM=space(5)
      .w_OFORIGI=space(5)
      .w_OFOPERAT=0
      .w_OFZONA=space(3)
      .w_OFAGENTE=space(5)
      .w_OFRUOLO=space(5)
      .w_OFSOLOBSO=space(1)
      .w_POOUTRAG=space(1)
      .w_OFDESGRU=space(35)
      .w_OFDESORI=space(35)
      .w_OFDESUTE=space(20)
      .w_OFDESZON=space(35)
      .w_OFDESAGE=space(35)
      .w_OFDESRUO=space(35)
      .w_OFDATTEST=ctod("  /  /  ")
      .w_OFDESPRO=space(30)
      .w_OFTIPO=space(1)
      .w_OBTEST=ctod("  /  /  ")
        .w_POCODAZI = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_POCODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_OFNOME = .w_POOUTEXP
        .w_OFRAGGR = IIF(NOT EMPTY(.w_POOUTRAG),.w_POOUTRAG,'N')
        .w_FlPublic = 'N'
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_OFCODCON))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_OFCODCON2))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_OFDESNOM))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_OFDESNOM2))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_OFCODNOM))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_OFCODNOM2))
          .link_1_11('Full')
        endif
        .w_OFTIPO = 'A'
          .DoRTCalc(13,13,.f.)
        .w_NODATNS = 'N'
        .DoRTCalc(15,16,.f.)
        if not(empty(.w_OFGRUNOM))
          .link_2_9('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_OFORIGI))
          .link_2_11('Full')
        endif
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_OFOPERAT))
          .link_2_13('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_OFZONA))
          .link_2_15('Full')
        endif
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_OFAGENTE))
          .link_2_17('Full')
        endif
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_OFRUOLO))
          .link_2_19('Full')
        endif
        .w_OFSOLOBSO = 'N'
          .DoRTCalc(23,29,.f.)
        .w_OFDATTEST = i_datsys
          .DoRTCalc(31,31,.f.)
        .w_OFTIPO = 'A'
        .w_OBTEST = i_DatSys
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,3,.t.)
        if .o_POOUTRAG<>.w_POOUTRAG
            .w_OFRAGGR = IIF(NOT EMPTY(.w_POOUTRAG),.w_POOUTRAG,'N')
        endif
        if .o_OFPROV<>.w_OFPROV
          .Calculate_SHFIXVWURX()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,33,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_SHFIXVWURX()
    with this
          * --- Eliminazione valore provincia
          .w_OFDESPRO = IIF(EMPTY(.w_OFPROV), "", .w_OFDESPRO)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFlPublic_1_5.enabled = this.oPgFrm.Page1.oPag.oFlPublic_1_5.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFlPublic_1_5.visible=!this.oPgFrm.Page1.oPag.oFlPublic_1_5.mHide()
    this.oPgFrm.Page2.oPag.oOFTIPO_2_2.visible=!this.oPgFrm.Page2.oPag.oOFTIPO_2_2.mHide()
    this.oPgFrm.Page2.oPag.oNODATNS_2_5.visible=!this.oPgFrm.Page2.oPag.oNODATNS_2_5.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_14.visible=!this.oPgFrm.Page2.oPag.oStr_2_14.mHide()
    this.oPgFrm.Page2.oPag.oOFZONA_2_15.visible=!this.oPgFrm.Page2.oPag.oOFZONA_2_15.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_16.visible=!this.oPgFrm.Page2.oPag.oStr_2_16.mHide()
    this.oPgFrm.Page2.oPag.oOFAGENTE_2_17.visible=!this.oPgFrm.Page2.oPag.oOFAGENTE_2_17.mHide()
    this.oPgFrm.Page2.oPag.oOFDESZON_2_24.visible=!this.oPgFrm.Page2.oPag.oOFDESZON_2_24.mHide()
    this.oPgFrm.Page2.oPag.oOFDESAGE_2_25.visible=!this.oPgFrm.Page2.oPag.oOFDESAGE_2_25.mHide()
    this.oPgFrm.Page2.oPag.oOFTIPO_2_29.visible=!this.oPgFrm.Page2.oPag.oOFTIPO_2_29.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=POCODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
    i_lTable = "PAR_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2], .t., this.PAR_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_POCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_POCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODAZI,POOUTEXP,POOUTRAG";
                   +" from "+i_cTable+" "+i_lTable+" where POCODAZI="+cp_ToStrODBC(this.w_POCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODAZI',this.w_POCODAZI)
            select POCODAZI,POOUTEXP,POOUTRAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_POCODAZI = NVL(_Link_.POCODAZI,space(5))
      this.w_POOUTEXP = NVL(_Link_.POOUTEXP,space(40))
      this.w_POOUTRAG = NVL(_Link_.POOUTRAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_POCODAZI = space(5)
      endif
      this.w_POOUTEXP = space(40)
      this.w_POOUTRAG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.POCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_POCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFCODCON
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_lTable = "NOM_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2], .t., this.NOM_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'NOM_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NCPERSON like "+cp_ToStrODBC(trim(this.w_OFCODCON)+"%");

          i_ret=cp_SQL(i_nConn,"select NCPERSON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NCPERSON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NCPERSON',trim(this.w_OFCODCON))
          select NCPERSON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NCPERSON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODCON)==trim(_Link_.NCPERSON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OFCODCON) and !this.bDontReportError
            deferred_cp_zoom('NOM_CONT','*','NCPERSON',cp_AbsName(oSource.parent,'oOFCODCON_1_6'),i_cWhere,'',"Codice contatto",'GSOF_CON.NOM_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCPERSON";
                     +" from "+i_cTable+" "+i_lTable+" where NCPERSON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCPERSON',oSource.xKey(1))
            select NCPERSON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCPERSON";
                   +" from "+i_cTable+" "+i_lTable+" where NCPERSON="+cp_ToStrODBC(this.w_OFCODCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCPERSON',this.w_OFCODCON)
            select NCPERSON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODCON = NVL(_Link_.NCPERSON,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODCON = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_OFCODCON2) OR  (UPPER(.w_OFCODCON)<= UPPER(.w_OFCODCON2))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice contatto iniziale � errato o maggiore di quello finale")
        endif
        this.w_OFCODCON = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])+'\'+cp_ToStr(_Link_.NCPERSON,1)
      cp_ShowWarn(i_cKey,this.NOM_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFCODCON2
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NOM_CONT_IDX,3]
    i_lTable = "NOM_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2], .t., this.NOM_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODCON2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'NOM_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NCPERSON like "+cp_ToStrODBC(trim(this.w_OFCODCON2)+"%");

          i_ret=cp_SQL(i_nConn,"select NCPERSON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NCPERSON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NCPERSON',trim(this.w_OFCODCON2))
          select NCPERSON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NCPERSON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODCON2)==trim(_Link_.NCPERSON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OFCODCON2) and !this.bDontReportError
            deferred_cp_zoom('NOM_CONT','*','NCPERSON',cp_AbsName(oSource.parent,'oOFCODCON2_1_7'),i_cWhere,'',"Codice contatto",'GSOF_CON.NOM_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCPERSON";
                     +" from "+i_cTable+" "+i_lTable+" where NCPERSON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCPERSON',oSource.xKey(1))
            select NCPERSON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODCON2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NCPERSON";
                   +" from "+i_cTable+" "+i_lTable+" where NCPERSON="+cp_ToStrODBC(this.w_OFCODCON2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NCPERSON',this.w_OFCODCON2)
            select NCPERSON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODCON2 = NVL(_Link_.NCPERSON,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODCON2 = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_OFCODCON) OR  (UPPER(.w_OFCODCON)<= UPPER(.w_OFCODCON2))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice contatto iniziale � errato o maggiore di quello finale")
        endif
        this.w_OFCODCON2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NOM_CONT_IDX,2])+'\'+cp_ToStr(_Link_.NCPERSON,1)
      cp_ShowWarn(i_cKey,this.NOM_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODCON2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFDESNOM
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFDESNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_OFDESNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NODESCRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NODESCRI',trim(this.w_OFDESNOM))
          select NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NODESCRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFDESNOM)==trim(_Link_.NODESCRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OFDESNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NODESCRI',cp_AbsName(oSource.parent,'oOFDESNOM_1_8'),i_cWhere,'',"Descrizione nominativo",'GSOF2CON.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NODESCRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NODESCRI',oSource.xKey(1))
            select NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFDESNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NODESCRI="+cp_ToStrODBC(this.w_OFDESNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NODESCRI',this.w_OFDESNOM)
            select NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFDESNOM = NVL(_Link_.NODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_OFDESNOM = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_OFDESNOM2) OR  (UPPER(.w_OFDESNOM)<= UPPER(.w_OFDESNOM2))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La descrizione nominativo iniziale � errata o maggiore di quella finale")
        endif
        this.w_OFDESNOM = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NODESCRI,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFDESNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFDESNOM2
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFDESNOM2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NODESCRI like "+cp_ToStrODBC(trim(this.w_OFDESNOM2)+"%");

          i_ret=cp_SQL(i_nConn,"select NODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NODESCRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NODESCRI',trim(this.w_OFDESNOM2))
          select NODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NODESCRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFDESNOM2)==trim(_Link_.NODESCRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OFDESNOM2) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NODESCRI',cp_AbsName(oSource.parent,'oOFDESNOM2_1_9'),i_cWhere,'',"Descrizione nominativo",'GSOF2CON.OFF_NOMI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where NODESCRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NODESCRI',oSource.xKey(1))
            select NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFDESNOM2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where NODESCRI="+cp_ToStrODBC(this.w_OFDESNOM2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NODESCRI',this.w_OFDESNOM2)
            select NODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFDESNOM2 = NVL(_Link_.NODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_OFDESNOM2 = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_OFDESNOM) OR  (UPPER(.w_OFDESNOM)<= UPPER(.w_OFDESNOM2))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("La descrizione nominativo iniziale � errata o maggiore di quella finale")
        endif
        this.w_OFDESNOM2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NODESCRI,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFDESNOM2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFCODNOM
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_OFCODNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_OFCODNOM))
          select NOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODNOM)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OFCODNOM) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oOFCODNOM_1_10'),i_cWhere,'',"Codice nominativo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_OFCODNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_OFCODNOM)
            select NOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODNOM = NVL(_Link_.NOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODNOM = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_OFCODNOM2) OR  (UPPER(.w_OFCODNOM)<= UPPER(.w_OFCODNOM2))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice nominativo iniziale � errato o maggiore di quello finale")
        endif
        this.w_OFCODNOM = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFCODNOM2
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OFF_NOMI_IDX,3]
    i_lTable = "OFF_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2], .t., this.OFF_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFCODNOM2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OFF_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NOCODICE like "+cp_ToStrODBC(trim(this.w_OFCODNOM2)+"%");

          i_ret=cp_SQL(i_nConn,"select NOCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NOCODICE',trim(this.w_OFCODNOM2))
          select NOCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFCODNOM2)==trim(_Link_.NOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OFCODNOM2) and !this.bDontReportError
            deferred_cp_zoom('OFF_NOMI','*','NOCODICE',cp_AbsName(oSource.parent,'oOFCODNOM2_1_11'),i_cWhere,'',"Codice nominativo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',oSource.xKey(1))
            select NOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFCODNOM2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NOCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where NOCODICE="+cp_ToStrODBC(this.w_OFCODNOM2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NOCODICE',this.w_OFCODNOM2)
            select NOCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFCODNOM2 = NVL(_Link_.NOCODICE,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OFCODNOM2 = space(20)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=empty(.w_OFCODNOM) OR  (UPPER(.w_OFCODNOM)<= UPPER(.w_OFCODNOM2))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice nominativo iniziale � errato o maggiore di quello finale")
        endif
        this.w_OFCODNOM2 = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OFF_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.NOCODICE,1)
      cp_ShowWarn(i_cKey,this.OFF_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFCODNOM2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFGRUNOM
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_NOMI_IDX,3]
    i_lTable = "GRU_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2], .t., this.GRU_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFGRUNOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRU_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GNCODICE like "+cp_ToStrODBC(trim(this.w_OFGRUNOM)+"%");

          i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GNCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GNCODICE',trim(this.w_OFGRUNOM))
          select GNCODICE,GNDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GNCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFGRUNOM)==trim(_Link_.GNCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" GNDESCRI like "+cp_ToStrODBC(trim(this.w_OFGRUNOM)+"%");

            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GNDESCRI like "+cp_ToStr(trim(this.w_OFGRUNOM)+"%");

            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_OFGRUNOM) and !this.bDontReportError
            deferred_cp_zoom('GRU_NOMI','*','GNCODICE',cp_AbsName(oSource.parent,'oOFGRUNOM_2_9'),i_cWhere,'',"Gruppi nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',oSource.xKey(1))
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFGRUNOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GNCODICE,GNDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GNCODICE="+cp_ToStrODBC(this.w_OFGRUNOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GNCODICE',this.w_OFGRUNOM)
            select GNCODICE,GNDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFGRUNOM = NVL(_Link_.GNCODICE,space(5))
      this.w_OFDESGRU = NVL(_Link_.GNDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_OFGRUNOM = space(5)
      endif
      this.w_OFDESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.GNCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFGRUNOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFORIGI
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ORI_NOMI_IDX,3]
    i_lTable = "ORI_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2], .t., this.ORI_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFORIGI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ORI_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ONCODICE like "+cp_ToStrODBC(trim(this.w_OFORIGI)+"%");

          i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ONCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ONCODICE',trim(this.w_OFORIGI))
          select ONCODICE,ONDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ONCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFORIGI)==trim(_Link_.ONCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ONDESCRI like "+cp_ToStrODBC(trim(this.w_OFORIGI)+"%");

            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ONDESCRI like "+cp_ToStr(trim(this.w_OFORIGI)+"%");

            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_OFORIGI) and !this.bDontReportError
            deferred_cp_zoom('ORI_NOMI','*','ONCODICE',cp_AbsName(oSource.parent,'oOFORIGI_2_11'),i_cWhere,'',"Codici origine nominativi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',oSource.xKey(1))
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFORIGI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ONCODICE,ONDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ONCODICE="+cp_ToStrODBC(this.w_OFORIGI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ONCODICE',this.w_OFORIGI)
            select ONCODICE,ONDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFORIGI = NVL(_Link_.ONCODICE,space(5))
      this.w_OFDESORI = NVL(_Link_.ONDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_OFORIGI = space(5)
      endif
      this.w_OFDESORI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ORI_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.ONCODICE,1)
      cp_ShowWarn(i_cKey,this.ORI_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFORIGI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFOPERAT
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFOPERAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CPUSERS')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_OFOPERAT);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_OFOPERAT)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_OFOPERAT) and !this.bDontReportError
            deferred_cp_zoom('CPUSERS','*','CODE',cp_AbsName(oSource.parent,'oOFOPERAT_2_13'),i_cWhere,'',"Operatori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFOPERAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_OFOPERAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_OFOPERAT)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFOPERAT = NVL(_Link_.CODE,0)
      this.w_OFDESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_OFOPERAT = 0
      endif
      this.w_OFDESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFOPERAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFZONA
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFZONA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_OFZONA)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_OFZONA))
          select ZOCODZON,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFZONA)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OFZONA) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oOFZONA_2_15'),i_cWhere,'',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFZONA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_OFZONA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_OFZONA)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFZONA = NVL(_Link_.ZOCODZON,space(3))
      this.w_OFDESZON = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_OFZONA = space(3)
      endif
      this.w_OFDESZON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFZONA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFAGENTE
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFAGENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_OFAGENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_OFAGENTE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFAGENTE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_OFAGENTE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oOFAGENTE_2_17'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFAGENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_OFAGENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_OFAGENTE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFAGENTE = NVL(_Link_.AGCODAGE,space(5))
      this.w_OFDESAGE = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_OFAGENTE = space(5)
      endif
      this.w_OFDESAGE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFAGENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OFRUOLO
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RUO_CONT_IDX,3]
    i_lTable = "RUO_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RUO_CONT_IDX,2], .t., this.RUO_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RUO_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OFRUOLO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RUO_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RCCODICE like "+cp_ToStrODBC(trim(this.w_OFRUOLO)+"%");

          i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RCCODICE',trim(this.w_OFRUOLO))
          select RCCODICE,RCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_OFRUOLO)==trim(_Link_.RCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" RCDESCRI like "+cp_ToStrODBC(trim(this.w_OFRUOLO)+"%");

            i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" RCDESCRI like "+cp_ToStr(trim(this.w_OFRUOLO)+"%");

            select RCCODICE,RCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_OFRUOLO) and !this.bDontReportError
            deferred_cp_zoom('RUO_CONT','*','RCCODICE',cp_AbsName(oSource.parent,'oOFRUOLO_2_19'),i_cWhere,'',"Codici ruolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RCCODICE',oSource.xKey(1))
            select RCCODICE,RCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OFRUOLO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RCCODICE,RCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RCCODICE="+cp_ToStrODBC(this.w_OFRUOLO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RCCODICE',this.w_OFRUOLO)
            select RCCODICE,RCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OFRUOLO = NVL(_Link_.RCCODICE,space(5))
      this.w_OFDESRUO = NVL(_Link_.RCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_OFRUOLO = space(5)
      endif
      this.w_OFDESRUO = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RUO_CONT_IDX,2])+'\'+cp_ToStr(_Link_.RCCODICE,1)
      cp_ShowWarn(i_cKey,this.RUO_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OFRUOLO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oOFNOME_1_3.value==this.w_OFNOME)
      this.oPgFrm.Page1.oPag.oOFNOME_1_3.value=this.w_OFNOME
    endif
    if not(this.oPgFrm.Page1.oPag.oOFRAGGR_1_4.RadioValue()==this.w_OFRAGGR)
      this.oPgFrm.Page1.oPag.oOFRAGGR_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFlPublic_1_5.RadioValue()==this.w_FlPublic)
      this.oPgFrm.Page1.oPag.oFlPublic_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOFCODCON_1_6.value==this.w_OFCODCON)
      this.oPgFrm.Page1.oPag.oOFCODCON_1_6.value=this.w_OFCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oOFCODCON2_1_7.value==this.w_OFCODCON2)
      this.oPgFrm.Page1.oPag.oOFCODCON2_1_7.value=this.w_OFCODCON2
    endif
    if not(this.oPgFrm.Page1.oPag.oOFDESNOM_1_8.value==this.w_OFDESNOM)
      this.oPgFrm.Page1.oPag.oOFDESNOM_1_8.value=this.w_OFDESNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oOFDESNOM2_1_9.value==this.w_OFDESNOM2)
      this.oPgFrm.Page1.oPag.oOFDESNOM2_1_9.value=this.w_OFDESNOM2
    endif
    if not(this.oPgFrm.Page1.oPag.oOFCODNOM_1_10.value==this.w_OFCODNOM)
      this.oPgFrm.Page1.oPag.oOFCODNOM_1_10.value=this.w_OFCODNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oOFCODNOM2_1_11.value==this.w_OFCODNOM2)
      this.oPgFrm.Page1.oPag.oOFCODNOM2_1_11.value=this.w_OFCODNOM2
    endif
    if not(this.oPgFrm.Page2.oPag.oOFTIPO_2_2.RadioValue()==this.w_OFTIPO)
      this.oPgFrm.Page2.oPag.oOFTIPO_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOFLOCALI_2_4.value==this.w_OFLOCALI)
      this.oPgFrm.Page2.oPag.oOFLOCALI_2_4.value=this.w_OFLOCALI
    endif
    if not(this.oPgFrm.Page2.oPag.oNODATNS_2_5.RadioValue()==this.w_NODATNS)
      this.oPgFrm.Page2.oPag.oNODATNS_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOFPROV_2_7.value==this.w_OFPROV)
      this.oPgFrm.Page2.oPag.oOFPROV_2_7.value=this.w_OFPROV
    endif
    if not(this.oPgFrm.Page2.oPag.oOFGRUNOM_2_9.value==this.w_OFGRUNOM)
      this.oPgFrm.Page2.oPag.oOFGRUNOM_2_9.value=this.w_OFGRUNOM
    endif
    if not(this.oPgFrm.Page2.oPag.oOFORIGI_2_11.value==this.w_OFORIGI)
      this.oPgFrm.Page2.oPag.oOFORIGI_2_11.value=this.w_OFORIGI
    endif
    if not(this.oPgFrm.Page2.oPag.oOFOPERAT_2_13.value==this.w_OFOPERAT)
      this.oPgFrm.Page2.oPag.oOFOPERAT_2_13.value=this.w_OFOPERAT
    endif
    if not(this.oPgFrm.Page2.oPag.oOFZONA_2_15.value==this.w_OFZONA)
      this.oPgFrm.Page2.oPag.oOFZONA_2_15.value=this.w_OFZONA
    endif
    if not(this.oPgFrm.Page2.oPag.oOFAGENTE_2_17.value==this.w_OFAGENTE)
      this.oPgFrm.Page2.oPag.oOFAGENTE_2_17.value=this.w_OFAGENTE
    endif
    if not(this.oPgFrm.Page2.oPag.oOFRUOLO_2_19.value==this.w_OFRUOLO)
      this.oPgFrm.Page2.oPag.oOFRUOLO_2_19.value=this.w_OFRUOLO
    endif
    if not(this.oPgFrm.Page2.oPag.oOFSOLOBSO_2_20.RadioValue()==this.w_OFSOLOBSO)
      this.oPgFrm.Page2.oPag.oOFSOLOBSO_2_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oOFDESGRU_2_21.value==this.w_OFDESGRU)
      this.oPgFrm.Page2.oPag.oOFDESGRU_2_21.value=this.w_OFDESGRU
    endif
    if not(this.oPgFrm.Page2.oPag.oOFDESORI_2_22.value==this.w_OFDESORI)
      this.oPgFrm.Page2.oPag.oOFDESORI_2_22.value=this.w_OFDESORI
    endif
    if not(this.oPgFrm.Page2.oPag.oOFDESUTE_2_23.value==this.w_OFDESUTE)
      this.oPgFrm.Page2.oPag.oOFDESUTE_2_23.value=this.w_OFDESUTE
    endif
    if not(this.oPgFrm.Page2.oPag.oOFDESZON_2_24.value==this.w_OFDESZON)
      this.oPgFrm.Page2.oPag.oOFDESZON_2_24.value=this.w_OFDESZON
    endif
    if not(this.oPgFrm.Page2.oPag.oOFDESAGE_2_25.value==this.w_OFDESAGE)
      this.oPgFrm.Page2.oPag.oOFDESAGE_2_25.value=this.w_OFDESAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oOFDESRUO_2_26.value==this.w_OFDESRUO)
      this.oPgFrm.Page2.oPag.oOFDESRUO_2_26.value=this.w_OFDESRUO
    endif
    if not(this.oPgFrm.Page2.oPag.oOFDESPRO_2_28.value==this.w_OFDESPRO)
      this.oPgFrm.Page2.oPag.oOFDESPRO_2_28.value=this.w_OFDESPRO
    endif
    if not(this.oPgFrm.Page2.oPag.oOFTIPO_2_29.RadioValue()==this.w_OFTIPO)
      this.oPgFrm.Page2.oPag.oOFTIPO_2_29.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_OFCODCON2) OR  (UPPER(.w_OFCODCON)<= UPPER(.w_OFCODCON2)))  and not(empty(.w_OFCODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFCODCON_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice contatto iniziale � errato o maggiore di quello finale")
          case   not(empty(.w_OFCODCON) OR  (UPPER(.w_OFCODCON)<= UPPER(.w_OFCODCON2)))  and not(empty(.w_OFCODCON2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFCODCON2_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice contatto iniziale � errato o maggiore di quello finale")
          case   not(empty(.w_OFDESNOM2) OR  (UPPER(.w_OFDESNOM)<= UPPER(.w_OFDESNOM2)))  and not(empty(.w_OFDESNOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFDESNOM_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La descrizione nominativo iniziale � errata o maggiore di quella finale")
          case   not(empty(.w_OFDESNOM) OR  (UPPER(.w_OFDESNOM)<= UPPER(.w_OFDESNOM2)))  and not(empty(.w_OFDESNOM2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFDESNOM2_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La descrizione nominativo iniziale � errata o maggiore di quella finale")
          case   not(empty(.w_OFCODNOM2) OR  (UPPER(.w_OFCODNOM)<= UPPER(.w_OFCODNOM2)))  and not(empty(.w_OFCODNOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFCODNOM_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice nominativo iniziale � errato o maggiore di quello finale")
          case   not(empty(.w_OFCODNOM) OR  (UPPER(.w_OFCODNOM)<= UPPER(.w_OFCODNOM2)))  and not(empty(.w_OFCODNOM2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOFCODNOM2_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice nominativo iniziale � errato o maggiore di quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_OFPROV = this.w_OFPROV
    this.o_POOUTRAG = this.w_POOUTRAG
    return

enddefine

* --- Define pages as container
define class tgsar_kexPag1 as StdContainer
  Width  = 619
  height = 372
  stdWidth  = 619
  stdheight = 372
  resizeXpos=296
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oOFNOME_1_3 as StdField with uid="HXMUFIGHAP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_OFNOME", cQueryName = "OFNOME",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Cartella di destinazione export contatti",;
    HelpContextID = 104510438,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=194, Top=23, InputMask=replicate('X',40)


  add object oOFRAGGR_1_4 as StdCombo with uid="FTKSOJLPQH",rtseq=4,rtrep=.f.,left=194,top=55,width=146,height=21, enabled=.f.;
    , ToolTipText = "Tipo raggruppamento per export";
    , HelpContextID = 130872294;
    , cFormVar="w_OFRAGGR",RowSource=""+"Nessuno,"+"Tipo,"+"Localit�,"+"Provincia,"+"Gruppo nominativi,"+"Origine,"+"Zona,"+"Agente,"+"Ruolo contatto,"+"Operatore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oOFRAGGR_1_4.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'T',;
    iif(this.value =3,'L',;
    iif(this.value =4,'P',;
    iif(this.value =5,'G',;
    iif(this.value =6,'O',;
    iif(this.value =7,'Z',;
    iif(this.value =8,'A',;
    iif(this.value =9,'R',;
    iif(this.value =10,'U',;
    space(15))))))))))))
  endfunc
  func oOFRAGGR_1_4.GetRadio()
    this.Parent.oContained.w_OFRAGGR = this.RadioValue()
    return .t.
  endfunc

  func oOFRAGGR_1_4.SetRadio()
    this.Parent.oContained.w_OFRAGGR=trim(this.Parent.oContained.w_OFRAGGR)
    this.value = ;
      iif(this.Parent.oContained.w_OFRAGGR=='N',1,;
      iif(this.Parent.oContained.w_OFRAGGR=='T',2,;
      iif(this.Parent.oContained.w_OFRAGGR=='L',3,;
      iif(this.Parent.oContained.w_OFRAGGR=='P',4,;
      iif(this.Parent.oContained.w_OFRAGGR=='G',5,;
      iif(this.Parent.oContained.w_OFRAGGR=='O',6,;
      iif(this.Parent.oContained.w_OFRAGGR=='Z',7,;
      iif(this.Parent.oContained.w_OFRAGGR=='A',8,;
      iif(this.Parent.oContained.w_OFRAGGR=='R',9,;
      iif(this.Parent.oContained.w_OFRAGGR=='U',10,;
      0))))))))))
  endfunc

  add object oFlPublic_1_5 as StdCheck with uid="JQJOWCEEOP",rtseq=5,rtrep=.f.,left=194, top=86, caption="Cartella pubblica",;
    ToolTipText = "Se attivo, la cartella di selezione viene ricercata tra le cartelle pubbliche",;
    HelpContextID = 21956167,;
    cFormVar="w_FlPublic", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFlPublic_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFlPublic_1_5.GetRadio()
    this.Parent.oContained.w_FlPublic = this.RadioValue()
    return .t.
  endfunc

  func oFlPublic_1_5.SetRadio()
    this.Parent.oContained.w_FlPublic=trim(this.Parent.oContained.w_FlPublic)
    this.value = ;
      iif(this.Parent.oContained.w_FlPublic=='S',1,;
      0)
  endfunc

  func oFlPublic_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_OFNOME))
    endwith
   endif
  endfunc

  func oFlPublic_1_5.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oOFCODCON_1_6 as StdField with uid="FLQRTXUTOU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_OFCODCON", cQueryName = "OFCODCON",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice contatto iniziale � errato o maggiore di quello finale",;
    ToolTipText = "Selezione dal codice contatto",;
    HelpContextID = 61473844,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=194, Top=150, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="NOM_CONT", oKey_1_1="NCPERSON", oKey_1_2="this.w_OFCODCON"

  func oOFCODCON_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODCON_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODCON_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NOM_CONT','*','NCPERSON',cp_AbsName(this.parent,'oOFCODCON_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codice contatto",'GSOF_CON.NOM_CONT_VZM',this.parent.oContained
  endproc

  add object oOFCODCON2_1_7 as StdField with uid="YCYQFBNSDB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_OFCODCON2", cQueryName = "OFCODCON2",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice contatto iniziale � errato o maggiore di quello finale",;
    ToolTipText = "Selezione al codice contatto",;
    HelpContextID = 61474644,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=194, Top=178, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="NOM_CONT", oKey_1_1="NCPERSON", oKey_1_2="this.w_OFCODCON2"

  func oOFCODCON2_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODCON2_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODCON2_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NOM_CONT','*','NCPERSON',cp_AbsName(this.parent,'oOFCODCON2_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codice contatto",'GSOF_CON.NOM_CONT_VZM',this.parent.oContained
  endproc

  add object oOFDESNOM_1_8 as StdField with uid="RBUMGJEWIB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_OFDESNOM", cQueryName = "OFDESNOM",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    sErrorMsg = "La descrizione nominativo iniziale � errata o maggiore di quella finale",;
    ToolTipText = "Selezione dalla descrizione nominativo",;
    HelpContextID = 261100595,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=194, Top=204, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="OFF_NOMI", oKey_1_1="NODESCRI", oKey_1_2="this.w_OFDESNOM"

  func oOFDESNOM_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFDESNOM_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFDESNOM_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NODESCRI',cp_AbsName(this.parent,'oOFDESNOM_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Descrizione nominativo",'GSOF2CON.OFF_NOMI_VZM',this.parent.oContained
  endproc

  add object oOFDESNOM2_1_9 as StdField with uid="PWVJWHBLFU",rtseq=9,rtrep=.f.,;
    cFormVar = "w_OFDESNOM2", cQueryName = "OFDESNOM2",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    sErrorMsg = "La descrizione nominativo iniziale � errata o maggiore di quella finale",;
    ToolTipText = "Selezione alla descrizione nominativo",;
    HelpContextID = 261101395,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=194, Top=232, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="OFF_NOMI", oKey_1_1="NODESCRI", oKey_1_2="this.w_OFDESNOM2"

  func oOFDESNOM2_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFDESNOM2_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFDESNOM2_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NODESCRI',cp_AbsName(this.parent,'oOFDESNOM2_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Descrizione nominativo",'GSOF2CON.OFF_NOMI_VZM',this.parent.oContained
  endproc

  add object oOFCODNOM_1_10 as StdField with uid="TCWRRAKJBJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_OFCODNOM", cQueryName = "OFCODNOM",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice nominativo iniziale � errato o maggiore di quello finale",;
    ToolTipText = "Selezione dal codice nominativo",;
    HelpContextID = 246023219,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=194, Top=262, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", oKey_1_1="NOCODICE", oKey_1_2="this.w_OFCODNOM"

  func oOFCODNOM_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODNOM_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODNOM_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oOFCODNOM_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codice nominativo",'',this.parent.oContained
  endproc

  add object oOFCODNOM2_1_11 as StdField with uid="CKDVRBAVYG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_OFCODNOM2", cQueryName = "OFCODNOM2",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice nominativo iniziale � errato o maggiore di quello finale",;
    ToolTipText = "Selezione al codice nominativo",;
    HelpContextID = 246024019,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=194, Top=290, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="OFF_NOMI", oKey_1_1="NOCODICE", oKey_1_2="this.w_OFCODNOM2"

  func oOFCODNOM2_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFCODNOM2_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFCODNOM2_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OFF_NOMI','*','NOCODICE',cp_AbsName(this.parent,'oOFCODNOM2_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codice nominativo",'',this.parent.oContained
  endproc


  add object oBtn_1_13 as StdButton with uid="ISLODYFBVX",left=513, top=325, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per esportare i contatti";
    , HelpContextID = 65611546;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        gsar_beo(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="WVKCRLCZSX",left=564, top=325, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 257088262;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_12 as StdString with uid="JJWDKLOCRK",Visible=.t., Left=57, Top=25,;
    Alignment=1, Width=131, Height=18,;
    Caption="Cartella principale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="GNEEKHLXTA",Visible=.t., Left=57, Top=55,;
    Alignment=1, Width=131, Height=18,;
    Caption="Raggruppamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="NUYXTEHMAB",Visible=.t., Left=16, Top=117,;
    Alignment=0, Width=161, Height=18,;
    Caption="Filtri per export contatti"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="VOXGBJFEJL",Visible=.t., Left=27, Top=152,;
    Alignment=1, Width=161, Height=18,;
    Caption="Da contatto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="BRSOYYEROT",Visible=.t., Left=27, Top=180,;
    Alignment=1, Width=161, Height=18,;
    Caption="A contatto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="UXSAGSXEYV",Visible=.t., Left=27, Top=208,;
    Alignment=1, Width=161, Height=18,;
    Caption="Da descr. nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="RWVKJZVNSM",Visible=.t., Left=27, Top=236,;
    Alignment=1, Width=161, Height=18,;
    Caption="A descr. nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="AYGRLKHXLW",Visible=.t., Left=27, Top=264,;
    Alignment=1, Width=161, Height=18,;
    Caption="Da codice nominativo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="CWBVQQDGXU",Visible=.t., Left=27, Top=292,;
    Alignment=1, Width=161, Height=18,;
    Caption="A codice nominativo:"  ;
  , bGlobalFont=.t.

  add object oBox_1_16 as StdBox with uid="RJVEKXAJIQ",left=16, top=136, width=598,height=184
enddefine
define class tgsar_kexPag2 as StdContainer
  Width  = 619
  height = 372
  stdWidth  = 619
  stdheight = 372
  resizeXpos=267
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oOFTIPO_2_2 as StdCombo with uid="GNJTDAXDDK",rtseq=12,rtrep=.f.,left=102,top=21,width=138,height=21;
    , ToolTipText = "Tipo nominativo";
    , HelpContextID = 6624230;
    , cFormVar="w_OFTIPO",RowSource=""+"Tutti,"+"Da valutare,"+"Potenziale,"+"Lead,"+"Prospect,"+"Cliente,"+"Congelato,"+"Non interessante", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oOFTIPO_2_2.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'T',;
    iif(this.value =3,'Z',;
    iif(this.value =4,'L',;
    iif(this.value =5,'P',;
    iif(this.value =6,'C',;
    iif(this.value =7,'G',;
    iif(this.value =8,'N',;
    space(1))))))))))
  endfunc
  func oOFTIPO_2_2.GetRadio()
    this.Parent.oContained.w_OFTIPO = this.RadioValue()
    return .t.
  endfunc

  func oOFTIPO_2_2.SetRadio()
    this.Parent.oContained.w_OFTIPO=trim(this.Parent.oContained.w_OFTIPO)
    this.value = ;
      iif(this.Parent.oContained.w_OFTIPO=='A',1,;
      iif(this.Parent.oContained.w_OFTIPO=='T',2,;
      iif(this.Parent.oContained.w_OFTIPO=='Z',3,;
      iif(this.Parent.oContained.w_OFTIPO=='L',4,;
      iif(this.Parent.oContained.w_OFTIPO=='P',5,;
      iif(this.Parent.oContained.w_OFTIPO=='C',6,;
      iif(this.Parent.oContained.w_OFTIPO=='G',7,;
      iif(this.Parent.oContained.w_OFTIPO=='N',8,;
      0))))))))
  endfunc

  func oOFTIPO_2_2.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oOFLOCALI_2_4 as StdField with uid="LVEFIBEOQT",rtseq=13,rtrep=.f.,;
    cFormVar = "w_OFLOCALI", cQueryName = "OFLOCALI",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� nominativo",;
    HelpContextID = 241527761,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=102, Top=53, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oOFLOCALI_2_4.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C","",".w_OFLOCALI",".w_OFPROV",".w_OFDESPRO")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNODATNS_2_5 as StdCheck with uid="OMOAUFNQHD",rtseq=14,rtrep=.f.,left=431, top=51, caption="No data di nascita",;
    ToolTipText = "Se attivo: non esporta data di nascita",;
    HelpContextID = 261889238,;
    cFormVar="w_NODATNS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oNODATNS_2_5.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oNODATNS_2_5.GetRadio()
    this.Parent.oContained.w_NODATNS = this.RadioValue()
    return .t.
  endfunc

  func oNODATNS_2_5.SetRadio()
    this.Parent.oContained.w_NODATNS=trim(this.Parent.oContained.w_NODATNS)
    this.value = ;
      iif(this.Parent.oContained.w_NODATNS=='S',1,;
      0)
  endfunc

  func oNODATNS_2_5.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oOFPROV_2_7 as StdField with uid="YXRMPWRRFY",rtseq=15,rtrep=.f.,;
    cFormVar = "w_OFPROV", cQueryName = "OFPROV",;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia nominativo",;
    HelpContextID = 123589606,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=102, Top=86, InputMask=replicate('X',2), bHasZoom = .t. 

  proc oOFPROV_2_7.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_OFPROV", ".w_OFDESPRO")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oOFGRUNOM_2_9 as StdField with uid="JALHIREXPT",rtseq=16,rtrep=.f.,;
    cFormVar = "w_OFGRUNOM", cQueryName = "OFGRUNOM",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo nominativo",;
    HelpContextID = 264062003,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=102, Top=119, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRU_NOMI", oKey_1_1="GNCODICE", oKey_1_2="this.w_OFGRUNOM"

  func oOFGRUNOM_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFGRUNOM_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFGRUNOM_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_NOMI','*','GNCODICE',cp_AbsName(this.parent,'oOFGRUNOM_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi nominativi",'',this.parent.oContained
  endproc

  add object oOFORIGI_2_11 as StdField with uid="NITLRQWFKI",rtseq=17,rtrep=.f.,;
    cFormVar = "w_OFORIGI", cQueryName = "OFORIGI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Origine nominativo",;
    HelpContextID = 134364186,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=102, Top=152, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ORI_NOMI", oKey_1_1="ONCODICE", oKey_1_2="this.w_OFORIGI"

  func oOFORIGI_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFORIGI_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFORIGI_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ORI_NOMI','*','ONCODICE',cp_AbsName(this.parent,'oOFORIGI_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici origine nominativi",'',this.parent.oContained
  endproc

  add object oOFOPERAT_2_13 as StdField with uid="MRPPMGTPAC",rtseq=18,rtrep=.f.,;
    cFormVar = "w_OFOPERAT", cQueryName = "OFOPERAT",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Operatore",;
    HelpContextID = 45859898,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=102, Top=185, bHasZoom = .t. , cLinkFile="CPUSERS", oKey_1_1="CODE", oKey_1_2="this.w_OFOPERAT"

  func oOFOPERAT_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFOPERAT_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFOPERAT_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CPUSERS','*','CODE',cp_AbsName(this.parent,'oOFOPERAT_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Operatori",'',this.parent.oContained
  endproc

  add object oOFZONA_2_15 as StdField with uid="YZVUKZAMXS",rtseq=19,rtrep=.f.,;
    cFormVar = "w_OFZONA", cQueryName = "OFZONA",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Zona",;
    HelpContextID = 38499302,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=102, Top=218, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", oKey_1_1="ZOCODZON", oKey_1_2="this.w_OFZONA"

  func oOFZONA_2_15.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oOFZONA_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFZONA_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFZONA_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oOFZONA_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Zone",'',this.parent.oContained
  endproc

  add object oOFAGENTE_2_17 as StdField with uid="DAJDLAPDWG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_OFAGENTE", cQueryName = "OFAGENTE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Agente",;
    HelpContextID = 246539307,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=102, Top=251, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_OFAGENTE"

  func oOFAGENTE_2_17.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oOFAGENTE_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFAGENTE_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFAGENTE_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oOFAGENTE_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oOFRUOLO_2_19 as StdField with uid="IVWEZUTGGL",rtseq=21,rtrep=.f.,;
    cFormVar = "w_OFRUOLO", cQueryName = "OFRUOLO",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Ruolo contatto",;
    HelpContextID = 224457702,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=102, Top=284, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="RUO_CONT", oKey_1_1="RCCODICE", oKey_1_2="this.w_OFRUOLO"

  func oOFRUOLO_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oOFRUOLO_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oOFRUOLO_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'RUO_CONT','*','RCCODICE',cp_AbsName(this.parent,'oOFRUOLO_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici ruolo",'',this.parent.oContained
  endproc


  add object oOFSOLOBSO_2_20 as StdCombo with uid="HHCWJAVOAJ",rtseq=22,rtrep=.f.,left=431,top=23,width=125,height=22;
    , ToolTipText = "Export solo obsoleti";
    , HelpContextID = 2820393;
    , cFormVar="w_OFSOLOBSO",RowSource=""+"Solo obsoleti,"+"Solo non obsoleti,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oOFSOLOBSO_2_20.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oOFSOLOBSO_2_20.GetRadio()
    this.Parent.oContained.w_OFSOLOBSO = this.RadioValue()
    return .t.
  endfunc

  func oOFSOLOBSO_2_20.SetRadio()
    this.Parent.oContained.w_OFSOLOBSO=trim(this.Parent.oContained.w_OFSOLOBSO)
    this.value = ;
      iif(this.Parent.oContained.w_OFSOLOBSO=='S',1,;
      iif(this.Parent.oContained.w_OFSOLOBSO=='N',2,;
      iif(this.Parent.oContained.w_OFSOLOBSO=='T',3,;
      0)))
  endfunc

  add object oOFDESGRU_2_21 as StdField with uid="HKDRTQOHWQ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_OFDESGRU", cQueryName = "OFDESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 143660091,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=119, InputMask=replicate('X',35)

  add object oOFDESORI_2_22 as StdField with uid="ODDISHYNJE",rtseq=25,rtrep=.f.,;
    cFormVar = "w_OFDESORI", cQueryName = "OFDESORI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 9442351,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=152, InputMask=replicate('X',35)

  add object oOFDESUTE_2_23 as StdField with uid="SKHJNHYYGW",rtseq=26,rtrep=.f.,;
    cFormVar = "w_OFDESUTE", cQueryName = "OFDESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 158329813,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=175, Top=185, InputMask=replicate('X',20)

  add object oOFDESZON_2_24 as StdField with uid="ITGOPOBJUW",rtseq=27,rtrep=.f.,;
    cFormVar = "w_OFDESZON", cQueryName = "OFDESZON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 193991732,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=218, InputMask=replicate('X',35)

  func oOFDESZON_2_24.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oOFDESAGE_2_25 as StdField with uid="XWRQWWVCJQ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_OFDESAGE", cQueryName = "OFDESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 225438677,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=251, InputMask=replicate('X',35)

  func oOFDESAGE_2_25.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oOFDESRUO_2_26 as StdField with uid="RIRCIZKXGT",rtseq=29,rtrep=.f.,;
    cFormVar = "w_OFDESRUO", cQueryName = "OFDESRUO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 208661451,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=284, InputMask=replicate('X',35)

  add object oOFDESPRO_2_28 as StdField with uid="TSLVUCAUWY",rtseq=31,rtrep=.f.,;
    cFormVar = "w_OFDESPRO", cQueryName = "OFDESPRO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 26219573,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=175, Top=86, InputMask=replicate('X',30)


  add object oOFTIPO_2_29 as StdCombo with uid="JOFQNZXGXA",rtseq=32,rtrep=.f.,left=102,top=21,width=138,height=22;
    , ToolTipText = "Tipo nominativo";
    , HelpContextID = 6624230;
    , cFormVar="w_OFTIPO",RowSource=""+"Tutti,"+"Nominativo,"+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oOFTIPO_2_29.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'T',;
    iif(this.value =3,'C',;
    iif(this.value =4,'F',;
    space(1))))))
  endfunc
  func oOFTIPO_2_29.GetRadio()
    this.Parent.oContained.w_OFTIPO = this.RadioValue()
    return .t.
  endfunc

  func oOFTIPO_2_29.SetRadio()
    this.Parent.oContained.w_OFTIPO=trim(this.Parent.oContained.w_OFTIPO)
    this.value = ;
      iif(this.Parent.oContained.w_OFTIPO=='A',1,;
      iif(this.Parent.oContained.w_OFTIPO=='T',2,;
      iif(this.Parent.oContained.w_OFTIPO=='C',3,;
      iif(this.Parent.oContained.w_OFTIPO=='F',4,;
      0))))
  endfunc

  func oOFTIPO_2_29.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oStr_2_1 as StdString with uid="MSBPBQIAXL",Visible=.t., Left=8, Top=22,;
    Alignment=1, Width=90, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="XMEJOFUGII",Visible=.t., Left=8, Top=55,;
    Alignment=1, Width=90, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="LWHSEIDPBX",Visible=.t., Left=8, Top=88,;
    Alignment=1, Width=90, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_8 as StdString with uid="RSGTMRLYJN",Visible=.t., Left=8, Top=121,;
    Alignment=1, Width=90, Height=18,;
    Caption="Gruppo nom.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="CUKMCSQNYH",Visible=.t., Left=8, Top=154,;
    Alignment=1, Width=90, Height=18,;
    Caption="Origine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="ENIIBTCOWI",Visible=.t., Left=8, Top=187,;
    Alignment=1, Width=90, Height=18,;
    Caption="Operatore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="GZLZLACXUR",Visible=.t., Left=8, Top=220,;
    Alignment=1, Width=90, Height=18,;
    Caption="Zona:"  ;
  , bGlobalFont=.t.

  func oStr_2_14.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_16 as StdString with uid="DXLQPFXSMP",Visible=.t., Left=8, Top=253,;
    Alignment=1, Width=90, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  func oStr_2_16.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_18 as StdString with uid="GCYBGTMDAH",Visible=.t., Left=8, Top=286,;
    Alignment=1, Width=90, Height=18,;
    Caption="Codice ruolo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kex','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
