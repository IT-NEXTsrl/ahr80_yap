* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gs___kbe                                                        *
*              Cambia contesto                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-12-07                                                      *
* Last revis.: 2015-03-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gs___kbe
LOCAL w_MSG
w_MSG=OpenForm(.T.)
IF Not Empty(w_Msg)
   ah_ErrorMsg('Gestione %1 aperta, chiudere prima tutte le gestioni', 48,'',w_Msg)
   RETURN
ENDIF
* --- Fine Area Manuale
return(createobject("tgs___kbe",oParentObject))

* --- Class definition
define class tgs___kbe as StdForm
  Top    = 59
  Left   = 115

  * --- Standard Properties
  Width  = 560
  Height = 193
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-20"
  HelpContextID=116042089
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  ESERCIZI_IDX = 0
  BUSIUNIT_IDX = 0
  cpazi_IDX = 0
  UTE_AZI_IDX = 0
  CPUSERS_IDX = 0
  cPrg = "gs___kbe"
  cComment = "Cambia contesto"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AZI = space(5)
  o_AZI = space(5)
  w_OLDAZI = space(5)
  w_CODAZI = space(5)
  w_GROUPDEF = 0
  w_CODBUN = space(3)
  w_DATSYS = ctod('  /  /  ')
  w_FLBUNI = space(1)
  w_DESBUN = space(40)
  w_CODESE = space(4)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_RAGAZI = space(40)
  w_NUMBGROUP = 0
  w_UTE = 0
  w_DESUTE = space(20)
  w_OUTE = 0
  w_MOBIMODE = .F.
  * --- Area Manuale = Declare Variables
  * --- gs___kbe
  * --- Centro la finestra
  Autocenter=.t.
  
  Proc CalcGROUPDEF(w_obj)
  local ComboGROUPDEF
     ComboGROUPDEF=w_obj.GetCtrl("w_GROUPDEF")
     ComboGROUPDEF.popola()
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgs___kbePag1","gs___kbe",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAZI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='BUSIUNIT'
    this.cWorkTables[4]='cpazi'
    this.cWorkTables[5]='UTE_AZI'
    this.cWorkTables[6]='CPUSERS'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZI=space(5)
      .w_OLDAZI=space(5)
      .w_CODAZI=space(5)
      .w_GROUPDEF=0
      .w_CODBUN=space(3)
      .w_DATSYS=ctod("  /  /  ")
      .w_FLBUNI=space(1)
      .w_DESBUN=space(40)
      .w_CODESE=space(4)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_RAGAZI=space(40)
      .w_NUMBGROUP=0
      .w_UTE=0
      .w_DESUTE=space(20)
      .w_OUTE=0
      .w_MOBIMODE=.f.
        .w_AZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_AZI))
          .link_1_1('Full')
        endif
        .w_OLDAZI = LEFT(.w_AZI,5)
        .w_CODAZI = LEFT(.w_AZI,5)
        .w_GROUPDEF = i_GROUPROLE
        .w_CODBUN = g_CODBUN
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODBUN))
          .link_1_5('Full')
        endif
        .w_DATSYS = date()
          .DoRTCalc(7,8,.f.)
        .w_CODESE = CALCESER(.w_DATSYS, g_CODESE)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODESE))
          .link_1_9('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
          .DoRTCalc(10,13,.f.)
        .w_UTE = i_CODUTE
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_UTE))
          .link_1_26('Full')
        endif
          .DoRTCalc(15,15,.f.)
        .w_OUTE = i_CODUTE
        .w_MOBIMODE = i_bMobileMode
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gs___kbe
    * --- Mi posiziono sul campo business unit
    this.GetCtrl("w_CODBUN")
      if VarType(i_oDeskmenu)="O"
        _Screen.RemoveObject("NavBar") 
        Release i_oDeskmenu 
        _Screen.RemoveObject("ImgBackground") 
        _Screen.RemoveObject("TBMDI")
      endif
      if i_cDeskmenu="O"
        * --- Lo lancio due volte, la prima crea l'oggetto la seconda carica il menu
        Do cp_NavBar
        i_oDeskmenu.Visible=.F. 
        Public i_OldDeskStatus
        i_OldDeskStatus = i_cDeskmenuStatus
        i_cDeskmenuStatus = 'C'
        i_oDeskmenu.ChangeSettings() 
        i_oDeskmenu.Visible=.T.
      endif    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- gs___kbe
    *** se il gruppo preferenziale � cambiato 
    *** ricalcolo il menu e la lista tabelle filtrate
    if this.w_GROUPDEF<>i_GROUPROLE
      i_GROUPROLE=this.w_GROUPDEF
      release i_aUsrGrps
      cp_FillGroupsArray()
      cp_menu()
      cp_LoadTableSecArray() 
    endif
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_Azi<>.w_Azi
            .w_CODAZI = LEFT(.w_AZI,5)
        endif
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .DoRTCalc(4,13,.t.)
          .link_1_26('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,17,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
    endwith
  return

  proc Calculate_JOXQQTHIPZ()
    with this
          * --- Calcolo GROUPDEF
          .CalcGROUPDEF(this;
             )
    endwith
  endproc
  proc Calculate_BDRLQAALAQ()
    with this
          * --- Update end
          GS___BAZ(this;
             )
          DeskMenuSetup(this;
             )
    endwith
  endproc
  proc Calculate_JIJCZFMGBJ()
    with this
          * --- Edit Aborted
          ripristinaazi(this;
             )
          DeskMenuSetup(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oGROUPDEF_1_4.enabled = this.oPgFrm.Page1.oPag.oGROUPDEF_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCODBUN_1_5.enabled = this.oPgFrm.Page1.oPag.oCODBUN_1_5.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODBUN_1_5.visible=!this.oPgFrm.Page1.oPag.oCODBUN_1_5.mHide()
    this.oPgFrm.Page1.oPag.oDESBUN_1_8.visible=!this.oPgFrm.Page1.oPag.oDESBUN_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_JOXQQTHIPZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Update end")
          .Calculate_BDRLQAALAQ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Edit Aborted")
          .Calculate_JIJCZFMGBJ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AZIENDA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AZCODAZI like "+cp_ToStrODBC(trim(this.w_AZI)+"%");

          i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI,AZFLBUNI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AZCODAZI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AZCODAZI',trim(this.w_AZI))
          select AZCODAZI,AZRAGAZI,AZFLBUNI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AZCODAZI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZI)==trim(_Link_.AZCODAZI) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AZRAGAZI like "+cp_ToStrODBC(trim(this.w_AZI)+"%");

            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI,AZFLBUNI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AZRAGAZI like "+cp_ToStr(trim(this.w_AZI)+"%");

            select AZCODAZI,AZRAGAZI,AZFLBUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AZI) and !this.bDontReportError
            deferred_cp_zoom('AZIENDA','*','AZCODAZI',cp_AbsName(oSource.parent,'oAZI_1_1'),i_cWhere,'',"",'LOGINAZI.AZIENDA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI,AZFLBUNI";
                     +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',oSource.xKey(1))
            select AZCODAZI,AZRAGAZI,AZFLBUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI,AZFLBUNI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZI)
            select AZCODAZI,AZRAGAZI,AZFLBUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
      this.w_FLBUNI = NVL(_Link_.AZFLBUNI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZI = space(5)
      endif
      this.w_RAGAZI = space(40)
      this.w_FLBUNI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODBUN
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KBU',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_CODBUN)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_CODAZI;
                     ,'BUCODICE',trim(this.w_CODBUN))
          select BUCODAZI,BUCODICE,BUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODBUN)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODBUN) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oCODBUN_1_5'),i_cWhere,'GSAR_KBU',"Business Unit",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_CODAZI;
                       ,'BUCODICE',this.w_CODBUN)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBUN = NVL(_Link_.BUCODICE,space(3))
      this.w_DESBUN = NVL(_Link_.BUDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODBUN = space(3)
      endif
      this.w_DESBUN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_9'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTE
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_UTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_UTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_UTE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAZI_1_1.value==this.w_AZI)
      this.oPgFrm.Page1.oPag.oAZI_1_1.value=this.w_AZI
    endif
    if not(this.oPgFrm.Page1.oPag.oGROUPDEF_1_4.RadioValue()==this.w_GROUPDEF)
      this.oPgFrm.Page1.oPag.oGROUPDEF_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODBUN_1_5.value==this.w_CODBUN)
      this.oPgFrm.Page1.oPag.oCODBUN_1_5.value=this.w_CODBUN
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSYS_1_6.value==this.w_DATSYS)
      this.oPgFrm.Page1.oPag.oDATSYS_1_6.value=this.w_DATSYS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBUN_1_8.value==this.w_DESBUN)
      this.oPgFrm.Page1.oPag.oDESBUN_1_8.value=this.w_DESBUN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_9.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_9.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oINIESE_1_12.value==this.w_INIESE)
      this.oPgFrm.Page1.oPag.oINIESE_1_12.value=this.w_INIESE
    endif
    if not(this.oPgFrm.Page1.oPag.oFINESE_1_13.value==this.w_FINESE)
      this.oPgFrm.Page1.oPag.oFINESE_1_13.value=this.w_FINESE
    endif
    if not(this.oPgFrm.Page1.oPag.oRAGAZI_1_20.value==this.w_RAGAZI)
      this.oPgFrm.Page1.oPag.oRAGAZI_1_20.value=this.w_RAGAZI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_AZI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAZI_1_1.SetFocus()
            i_bnoObbl = !empty(.w_AZI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Azienda non esistente")
          case   (empty(.w_CODBUN))  and not(g_APPLICATION <> "ad hoc ENTERPRISE")  and (.w_FLBUNI $ "ET")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODBUN_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CODBUN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATSYS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATSYS_1_6.SetFocus()
            i_bnoObbl = !empty(.w_DATSYS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODESE_1_9.SetFocus()
            i_bnoObbl = !empty(.w_CODESE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gs___kbe
      if empty(.w_CODAZI)
         i_cErrorMsg=Ah_MsgFormat("Codice azienda obbligatorio")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      if not chkuteaz(.w_CODAZI, i_CODUTE)
         i_bnoChk = .f.
         i_bRes = .f.
         i_cErrorMsg=Ah_MsgFormat("Codice utente non associato all'azienda")
         .w_CODAZI=space(5)
         .w_RAGAZI=space(40)
         .SetControlsValue()
      endif
      if empty(.w_CODBUN) and (.w_FLBUNI $ "ET")
         i_cErrorMsg=Ah_MsgFormat("Business Unit obbligatoria")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      if empty(.w_CODESE)
         i_cErrorMsg=Ah_MsgFormat("Codice esercizio obbligatorio")
         i_bRes = .f.
         i_bnoChk = .f.
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AZI = this.w_AZI
    return

enddefine

* --- Define pages as container
define class tgs___kbePag1 as StdContainer
  Width  = 556
  height = 193
  stdWidth  = 556
  stdheight = 193
  resizeXpos=349
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAZI_1_1 as StdField with uid="JFBMKJMAHO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_AZI", cQueryName = "AZI",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Azienda non esistente",;
    ToolTipText = "Codice azienda",;
    HelpContextID = 115718906,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=149, Top=6, cSayPict='"!!!!!"', cGetPict='"!!!!!"', InputMask=replicate('X',5), bHasZoom = .t. , TabStop=.f., cLinkFile="AZIENDA", oKey_1_1="AZCODAZI", oKey_1_2="this.w_AZI"

  func oAZI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZI_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZI_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AZIENDA','*','AZCODAZI',cp_AbsName(this.parent,'oAZI_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'LOGINAZI.AZIENDA_VZM',this.parent.oContained
  endproc


  add object oGROUPDEF_1_4 as StdZTamTableCombo with uid="RVXHRTQDCL",rtseq=4,rtrep=.f.,left=149,top=33,width=136,height=21;
    , ToolTipText = "Gruppo preferenziale di utilizzo della procedura";
    , HelpContextID = 40869292;
    , cFormVar="w_GROUPDEF",tablefilter="", bObbl = .f. , nPag = 1;
    , cTable='GROUPSROLE.VQR',cKey='code',cValue='name',cOrderBy='code',xDefault=0;
  , bGlobalFont=.t.


  func oGROUPDEF_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NUMBGROUP>1)
    endwith
   endif
  endfunc

  add object oCODBUN_1_5 as StdField with uid="XYOSHEHITQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODBUN", cQueryName = "CODBUN",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Nome della Business Unit",;
    HelpContextID = 212593190,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=149, Top=62, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", cZoomOnZoom="GSAR_KBU", oKey_1_1="BUCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="BUCODICE", oKey_2_2="this.w_CODBUN"

  func oCODBUN_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLBUNI $ "ET")
    endwith
   endif
  endfunc

  func oCODBUN_1_5.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  func oCODBUN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODBUN_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODBUN_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oCODBUN_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KBU',"Business Unit",'',this.parent.oContained
  endproc
  proc oCODBUN_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KBU()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.BUCODAZI=w_CODAZI
     i_obj.w_BUCODICE=this.parent.oContained.w_CODBUN
     i_obj.ecpSave()
  endproc

  add object oDATSYS_1_6 as StdField with uid="PQCURSVNSP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATSYS", cQueryName = "DATSYS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di sistema",;
    HelpContextID = 33414198,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=149, Top=90

  add object oDESBUN_1_8 as StdField with uid="ZRNPKTVPGX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESBUN", cQueryName = "DESBUN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 212652086,;
   bGlobalFont=.t.,;
    Height=21, Width=334, Left=217, Top=62, InputMask=replicate('X',40)

  func oDESBUN_1_8.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oCODESE_1_9 as StdField with uid="GXATUEVIOD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio da elaborare",;
    HelpContextID = 59697702,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=149, Top=118, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oINIESE_1_12 as StdField with uid="MJTDRCTOAP",rtseq=10,rtrep=.f.,;
    cFormVar = "w_INIESE", cQueryName = "INIESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio dell'esercizio selezionato",;
    HelpContextID = 59718022,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=261, Top=118

  add object oFINESE_1_13 as StdField with uid="DBULUWKCSC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FINESE", cQueryName = "FINESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine dell'esercizio selezionato",;
    HelpContextID = 59737174,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=385, Top=118


  add object oObj_1_17 as cp_runprogram with uid="KPRFLGVWDL",left=3, top=223, width=222,height=19,;
    caption='GS___BCK',;
   bGlobalFont=.t.,;
    prg="GS___BCK('D')",;
    cEvent = "w_DATSYS Changed",;
    nPag=1;
    , HelpContextID = 23764657


  add object oBtn_1_18 as StdButton with uid="ZMIZZNTUDJ",left=451, top=142, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 116013338;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="JWIBHYOYEX",left=501, top=142, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 108724666;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oRAGAZI_1_20 as StdField with uid="JCMYBQNXEN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_RAGAZI", cQueryName = "RAGAZI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 133893398,;
   bGlobalFont=.t.,;
    Height=21, Width=334, Left=217, Top=6, InputMask=replicate('X',40)


  add object oObj_1_22 as cp_runprogram with uid="RUBHLIOPVZ",left=3, top=200, width=222,height=19,;
    caption='GS___BCK(A)',;
   bGlobalFont=.t.,;
    prg='GS___BCK("A")',;
    cEvent = "w_AZI Changed",;
    nPag=1;
    , HelpContextID = 23949873

  add object oStr_1_10 as StdString with uid="IPETQIYEZL",Visible=.t., Left=41, Top=92,;
    Alignment=1, Width=104, Height=15,;
    Caption="Data di sistema:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="EFGZLMQMSW",Visible=.t., Left=92, Top=120,;
    Alignment=1, Width=53, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="YHTZVFWRXI",Visible=.t., Left=218, Top=118,;
    Alignment=1, Width=38, Height=18,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="TWXOTRZPFR",Visible=.t., Left=347, Top=118,;
    Alignment=1, Width=33, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="QFKMERYUTH",Visible=.t., Left=51, Top=64,;
    Alignment=1, Width=94, Height=15,;
    Caption="Business Unit:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_21 as StdString with uid="UHJIDNOZTK",Visible=.t., Left=58, Top=8,;
    Alignment=1, Width=87, Height=15,;
    Caption="Azienda:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_23 as StdString with uid="OGCHUJXVXU",Visible=.t., Left=8, Top=35,;
    Alignment=1, Width=138, Height=18,;
    Caption="Gruppo preferenziale:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gs___kbe','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gs___kbe
proc ripristinaazi(pParent)
   i_codazi=pParent.w_OLDAZI
endproc

Proc DeskMenuSetup(pParent)
   if Type("i_OldDeskStatus")<>"U"
      i_cDeskmenuStatus = i_OldDeskStatus
      i_oDeskmenu.ChangeSettings()
      Release i_OldDeskStatus
   Endif
EndProc

define class StdZTamTableCombo as StdTableCombo

proc Init()
  IF VARTYPE(this.bNoBackColor)='U'
		This.backcolor=i_EBackColor
	ENDIF
endproc

  proc Popola()
  * --- Zucchetti Aulla - Inizio Interfaccia
    IF VARTYPE(this.bNoBackColor)='U'
         This.backcolor=i_EBackColor
    ENDIF
  * --- Zucchetti Aulla - Fine Interfaccia	
    this.ToolTipText=cp_Translate(this.ToolTipText)	
    local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    if not empty(this.cTable) AND LOWER(i_codazi)<>'xxx'
       vq_exec(this.cTable,this.parent.oContained,i_curs)
    endif
    i_fk=this.cKey
    i_fd=this.cValue
    if used(i_curs)
        select (i_curs)
        this.nValues=iif(reccount()>1,reccount()+1,1)
        for i_j=alen(this.combovalues) to 1 step -1
          adel(this.combovalues,i_j)
          IF this.ListCount>0 AND i_j <= this.ListCount
         	 this.RemoveItem(i_j)
          ENDIF
        endfor
        dimension this.combovalues[MAX(1,this.nValues)]
        i_bCharKey=type(i_fk)='C'
        if this.nValues>1
            this.AddItem(' ')
            this.combovalues[1]=iif(i_bCharKey,Space(1),0)          
        else 
            i_GROUPROLE=code
            this.parent.oContained.w_GROUPDEF=i_GROUPROLE
        endif
        this.parent.oContained.w_NUMBGROUP=this.nValues
        do while !eof()
          this.AddItem(iif(type(i_fd)='C',ALLTRIM(&i_fd),ALLTRIM(str(&i_fd))))
          if i_bCharKey
            this.combovalues[iif(this.nValues=1,0,1)+recno()]=trim(&i_fk)
          else
            this.combovalues[iif(this.nValues=1,0,1)+recno()]=&i_fk
          endif
          skip
        enddo
        this.SetRadio()
        select (i_curs)
        use
    else
        this.enabled=.f.
    endif
    *--- Zucchetti Aulla Inizio - Interfaccia  
    IF This.bSetFont
   		This.SetFont()
    ENDIF
    *--- Zucchetti Aulla Fine - Interfaccia   

enddefine
* --- Fine Area Manuale
