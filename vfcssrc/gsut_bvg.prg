* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bvg                                                        *
*              Aggiorna le variabili globali per l'utilizzo di una mail non predefinita*
*                                                                              *
*      Author: Zucchetti s.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-05-03                                                      *
* Last revis.: 2016-04-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL,pPEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bvg",oParentObject,m.pSERIAL,m.pPEC)
return(i_retval)

define class tgsut_bvg as StdBatch
  * --- Local variables
  pSERIAL = space(5)
  pPEC = .f.
  w_EMAIL = space(254)
  w_ERR = .f.
  * --- WorkFile variables
  ACC_MAIL_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato dal bottone "E-mail" della print system (cp_chpr3)
    *     
    *     Aggiorna le variabili globali per l'utilizzo di una mail non predefinita
    *     
    *     pSERIAL : Account Mail scelto nel bottone di invio della print system
    if this.pPEC or g_AccountMail <> this.pSERIAL
      * --- Memorizzo le var. globali della configurazione mail
      this.w_ERR = ("KO" = CFGMAIL(i_CODUTE,"PUSH_CFG"))
      this.w_ERR = Empty(CFGMAIL(i_CODUTE, Iif(this.pPEC, "P", "S"), this.pSERIAL, .T.))
      if this.w_ERR or empty(g_AccountMail)
        ah_Errormsg("Configurazione %1 utente non valida",,,IIF(this.pPEC,"PEC","mail"))
      else
        CP_CHPFUN (this.oParentObject, "MailClick") 
      endif
      * --- ripristino le var. globali
      this.w_ERR = ("KO" = CFGMAIL(i_CODUTE,"POP_CFG"))
    else
      CP_CHPFUN (this.oParentObject, "MailClick") 
    endif
  endproc


  proc Init(oParentObject,pSERIAL,pPEC)
    this.pSERIAL=pSERIAL
    this.pPEC=pPEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ACC_MAIL'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL,pPEC"
endproc
