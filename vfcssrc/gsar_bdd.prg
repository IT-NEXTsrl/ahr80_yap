* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bdd                                                        *
*              Duplicazione distinta base                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_63]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-24                                                      *
* Last revis.: 2015-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bdd",oParentObject)
return(i_retval)

define class tgsar_bdd as StdBatch
  * --- Local variables
  w_DATELAB = ctod("  /  /  ")
  w_MESS = space(120)
  w_OK = .f.
  w_CODICE = space(10)
  w_oERRORLOG = .NULL.
  w_APPO = space(20)
  w_SOVRAS = space(1)
  w_UTCC = 0
  w_UTDC = ctod("  /  /  ")
  w_NUMRIG = 0
  w_ROWVAR = 0
  w_ROWORD = 0
  w_CODCOM = space(20)
  w_DTOBSO = ctod("  /  /  ")
  * --- WorkFile variables
  COM_VARI_idx=0
  DISMBASE_idx=0
  DISTBASE_idx=0
  ART_ICOL_idx=0
  CONF_DIS_idx=0
  CONFDDIS_idx=0
  MODE_DIS_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Duplicazione Distinta Base (da GSDS_KDD)
    this.w_DATELAB = i_Datsys
    this.w_OK = .F.
    if EMPTY(this.oParentObject.w_DISRIF)
      do case
        case this.oParentObject.w_CICLO="D"
          ah_ErrorMsg("Distinta di origine non definita",,"")
        case this.oParentObject.w_CICLO="K"
          ah_ErrorMsg("Articolo kit di origine non definito",,"")
        case this.oParentObject.w_CICLO="I"
          ah_ErrorMsg("Kit imballo di origine non definito",,"")
      endcase
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_DBCODICE)
      do case
        case this.oParentObject.w_CICLO="D"
          ah_ErrorMsg("Distinta da generare non definita",,"")
        case this.oParentObject.w_CICLO="K"
          ah_ErrorMsg("Articolo kit da generare non definito",,"")
        case this.oParentObject.w_CICLO="I"
          ah_ErrorMsg("Kit imballo da generare non definito",,"")
      endcase
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_DISRIF=this.oParentObject.w_DBCODICE
      do case
        case this.oParentObject.w_CICLO="D"
          ah_ErrorMsg("Distinta di origine e distinta destinazione coincidenti; impossibile generare",,"")
        case this.oParentObject.w_CICLO="K"
          ah_ErrorMsg("Articolo kit di origine e di destinazione coincidenti; impossibile generare",,"")
        case this.oParentObject.w_CICLO="I"
          ah_ErrorMsg("Kit imballo di origine e di destinazione coincidenti; impossibile generare",,"")
      endcase
      i_retcode = 'stop'
      return
    endif
    if Not Empty(this.oParentObject.w_DFIRIF) and this.oParentObject.w_DFIRIF<this.w_DATELAB
      * --- Negli articoli kit e kit imballi non ci sono le date di validit�
      if !ah_YesNo("Distinta di riferimento obsoleta continuare ugualmente?")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    this.w_APPO = SPACE(20)
    this.w_SOVRAS = "N"
    this.w_UTCC = i_CODUTE
    this.w_UTDC = SetInfoDate( g_CALUTD )
    * --- Read from DISMBASE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DISMBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2],.t.,this.DISMBASE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DBCODICE"+;
        " from "+i_cTable+" DISMBASE where ";
            +"DBCODICE = "+cp_ToStrODBC(this.oParentObject.w_DBCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DBCODICE;
        from (i_cTable) where;
            DBCODICE = this.oParentObject.w_DBCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_APPO = NVL(cp_ToDate(_read_.DBCODICE),cp_NullValue(_read_.DBCODICE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_APPO=this.oParentObject.w_DBCODICE
      if this.oParentObject.w_DISKIT <> this.oParentObject.w_CICLO
        do case
          case this.oParentObject.w_CICLO="D"
            this.w_MESS = Ah_MsgFormat("distinta base")
          case this.oParentObject.w_CICLO="K"
            this.w_MESS = Ah_MsgFormat("articolo kit")
          case this.oParentObject.w_CICLO="I"
            this.w_MESS = Ah_MsgFormat("kit imballo")
        endcase
        ah_ErrorMsg("Codice %1 gi� utilizzato per %2. Impossibile continuare",,"", Alltrim(this.oParentObject.w_DBCODICE), this.w_MESS)
        i_retcode = 'stop'
        return
      endif
      do case
        case this.oParentObject.w_CICLO="D"
          this.w_MESS = "Distinta da generare gi� esistente; sovrascrivo?"
        case this.oParentObject.w_CICLO="K"
          this.w_MESS = "Articolo kit da generare gi� esistente; sovrascrivo?"
        case this.oParentObject.w_CICLO="I"
          this.w_MESS = "Kit imballo da generare gi� esistente; sovrascrivo?"
      endcase
      if ah_YesNo(this.w_MESS)
        this.w_SOVRAS = "S"
      else
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Inizia a Procedere
    ah_Msg("Inizio duplicazione: %1",.T.,.F.,.F., ALLTRIM(this.oParentObject.w_DBCODICE) )
    * --- Try
    local bErr_03AA01F0
    bErr_03AA01F0=bTrsErr
    this.Try_03AA01F0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      if NOT EMPTY(this.w_MESS)
        ah_ErrorMsg("%1%0Operazione abbandonata",,"",this.w_MESS)
      else
        ah_ErrorMsg("Errore durante l'elaborazione%0Operazione abbandonata",,"")
      endif
    endif
    bTrsErr=bTrsErr or bErr_03AA01F0
    * --- End
    if this.w_OK
      this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati",.T., "Si vuole visualizzare righe non inserite?")     
    endif
  endproc
  proc Try_03AA01F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if this.w_SOVRAS="S"
      * --- Elimina i Dati della Distinta Origine
      if this.oParentObject.w_CICLO = "D"
        * --- Try
        local bErr_03A7B780
        bErr_03A7B780=bTrsErr
        this.Try_03A7B780()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_MESS = ah_Msgformat("Impossibile eliminare la distinta base da sovrascrivere (tabella: componenti varianti)")
        endif
        bTrsErr=bTrsErr or bErr_03A7B780
        * --- End
      endif
      * --- Try
      local bErr_03A7C860
      bErr_03A7C860=bTrsErr
      this.Try_03A7C860()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_MESS = ah_Msgformat("Impossibile eliminare il record da sovrascrivere (dettaglio)")
      endif
      bTrsErr=bTrsErr or bErr_03A7C860
      * --- End
      * --- Try
      local bErr_03A7C6E0
      bErr_03A7C6E0=bTrsErr
      this.Try_03A7C6E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_MESS = ah_Msgformat("Impossibile modificare il record da sovrascrivere (testata)")
      endif
      bTrsErr=bTrsErr or bErr_03A7C6E0
      * --- End
    else
      * --- Try
      local bErr_03A7AE50
      bErr_03A7AE50=bTrsErr
      this.Try_03A7AE50()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_MESS = ah_Msgformat("Impossibile inserire il nuovo record (testata)")
      endif
      bTrsErr=bTrsErr or bErr_03A7AE50
      * --- End
    endif
    * --- Select from DISTBASE
    i_nConn=i_TableProp[this.DISTBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2],.t.,this.DISTBASE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" DISTBASE ";
          +" where DBCODICE="+cp_ToStrODBC(this.oParentObject.w_DISRIF)+"";
          +" order by CPROWORD";
           ,"_Curs_DISTBASE")
    else
      select * from (i_cTable);
       where DBCODICE=this.oParentObject.w_DISRIF;
       order by CPROWORD;
        into cursor _Curs_DISTBASE
    endif
    if used('_Curs_DISTBASE')
      select _Curs_DISTBASE
      locate for 1=1
      do while not(eof())
      this.w_CODCOM = _Curs_DISTBASE.DBARTCOM
      if NVL(_Curs_DISTBASE.DBFLVARI," ")<>"S"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.w_DTOBSO = this.w_DATELAB+1
      endif
      this.w_ROWORD = _Curs_DISTBASE.CPROWORD 
      if EMPTY(this.w_DTOBSO) OR (this.w_DTOBSO>this.w_DATELAB)
        * --- Try
        local bErr_03A8B330
        bErr_03A8B330=bTrsErr
        this.Try_03A8B330()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_MESS = ah_Msgformat("Impossibile inserire il nuovo record (dettaglio)")
        endif
        bTrsErr=bTrsErr or bErr_03A8B330
        * --- End
        if NVL(_Curs_DISTBASE.DBFLVARI," ")="S"
          * --- DBFLVARI per Articoli kit e kit imballi � sempre vuoto quindi in questo if ci entra
          *     solo per le distinte basi
          this.w_NUMRIG = _Curs_DISTBASE.CPROWNUM
          ah_Msg("Inizio duplicazione %1 riga: %1",.T.,.F.,.F., ALLTRIM(this.oParentObject.w_DBCODICE), ALLTRIM(STR(this.w_NUMRIG)) )
          * --- Select from COM_VARI
          i_nConn=i_TableProp[this.COM_VARI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COM_VARI_idx,2],.t.,this.COM_VARI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" COM_VARI ";
                +" where CVCODICE="+cp_ToStrODBC(this.oParentObject.w_DISRIF)+" AND CVNUMRIF="+cp_ToStrODBC(this.w_NUMRIG)+"";
                +" order by CPROWORD";
                 ,"_Curs_COM_VARI")
          else
            select * from (i_cTable);
             where CVCODICE=this.oParentObject.w_DISRIF AND CVNUMRIF=this.w_NUMRIG;
             order by CPROWORD;
              into cursor _Curs_COM_VARI
          endif
          if used('_Curs_COM_VARI')
            select _Curs_COM_VARI
            locate for 1=1
            do while not(eof())
            this.w_CODCOM = _Curs_COM_VARI.CVARTCOM
            this.w_ROWVAR = _Curs_COM_VARI.CPROWORD
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if EMPTY(this.w_DTOBSO) OR (this.w_DTOBSO>this.w_DATELAB)
              * --- Try
              local bErr_03A894D0
              bErr_03A894D0=bTrsErr
              this.Try_03A894D0()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                this.w_MESS = ah_Msgformat("Impossibile inserire i componenti varianti da sovrascrivere (tabella: componenti distinta)")
              endif
              bTrsErr=bTrsErr or bErr_03A894D0
              * --- End
            else
              this.w_oERRORLOG.AddMsgLog("Riga variante %1 collegata alla riga %2 non inserita, perch� codice componente %3 obsoleto dal %4", alltrim(STR(this.w_ROWVAR)), alltrim(STR(this.w_ROWORD)) , Alltrim(this.w_CODCOM), DTOC(this.w_DTOBSO) )     
            endif
              select _Curs_COM_VARI
              continue
            enddo
            use
          endif
          * --- Read from COM_VARI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COM_VARI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COM_VARI_idx,2],.t.,this.COM_VARI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CVCODICE"+;
              " from "+i_cTable+" COM_VARI where ";
                  +"CVCODICE = "+cp_ToStrODBC(this.oParentObject.w_DBCODICE);
                  +" and CVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CVCODICE;
              from (i_cTable) where;
                  CVCODICE = this.oParentObject.w_DBCODICE;
                  and CVNUMRIF = this.w_NUMRIG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODICE = NVL(cp_ToDate(_read_.CVCODICE),cp_NullValue(_read_.CVCODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_ROWS=0
            * --- Elimion Riga Distinta Senza Dettaglio Varianti
            * --- Delete from DISTBASE
            i_nConn=i_TableProp[this.DISTBASE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"DBCODICE = "+cp_ToStrODBC(this.oParentObject.w_DBCODICE);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMRIG);
                     )
            else
              delete from (i_cTable) where;
                    DBCODICE = this.oParentObject.w_DBCODICE;
                    and CPROWNUM = this.w_NUMRIG;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
        endif
      else
        this.w_oERRORLOG.AddMsgLog("Riga %1 non inserita, perch� codice componente %2 obsoleto dal %3", alltrim(STR(this.w_ROWORD)) , Alltrim(this.w_CODCOM), DTOC(this.w_DTOBSO) )     
      endif
        select _Curs_DISTBASE
        continue
      enddo
      use
    endif
    * --- Read from DISTBASE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DISTBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2],.t.,this.DISTBASE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DBCODICE"+;
        " from "+i_cTable+" DISTBASE where ";
            +"DBCODICE = "+cp_ToStrODBC(this.oParentObject.w_DBCODICE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DBCODICE;
        from (i_cTable) where;
            DBCODICE = this.oParentObject.w_DBCODICE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODICE = NVL(cp_ToDate(_read_.DBCODICE),cp_NullValue(_read_.DBCODICE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_ROWS=0
      * --- Raise
      i_Error="Record non duplicato, tutti i componenti sono obsoleti"
      return
      this.w_MESS = ah_Msgformat(i_error)
    endif
    if this.oParentObject.w_CICLO="D" and g_CCAR="S" and this.oParentObject.w_DUPCAR="S"
      * --- Duplicazione legami caratteristiche
      * --- Insert into CONF_DIS
      i_nConn=i_TableProp[this.CONF_DIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_DIS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.CONF_DIS_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,""+cp_ToStrODBC(this.oParentObject.w_DBCODICE)+" AS CC__DIBA,CCROWORD,CCCOMPON, CCCODART,CCTIPOCA,CCCODICE,CCDATINI,CCDATFIN,CCDESCRI,CPROWORD,CPCCCHK"," from "+i_cTempTable+" where CC__DIBA="+cp_ToStrODBC(this.oParentObject.w_DISRIF)+"",this.CONF_DIS_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into CONFDDIS
      i_nConn=i_TableProp[this.CONFDDIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.CONFDDIS_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,""+cp_ToStrODBC(this.oParentObject.w_DBCODICE)+" AS CC__DIBA,CCROWORD,CCCOMPON,CCDATINI,CCDATFIN,CCTIPOCA,CCCODICE,CPROWNUM,CPROWORD,CCDETTAG,CC__DEFA,CCOPERAT,CC_COEFF,CCDESSUP,CPCCCHK"," from "+i_cTempTable+" where CC__DIBA="+cp_ToStrODBC(this.oParentObject.w_DISRIF)+"",this.CONFDDIS_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Insert into MODE_DIS
      i_nConn=i_TableProp[this.MODE_DIS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MODE_DIS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"QUERY\GSAR_BDD1",this.MODE_DIS_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    this.w_OK = .T.
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Duplicazione completata",,"")
    return
  proc Try_03A7B780()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from COM_VARI
    i_nConn=i_TableProp[this.COM_VARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COM_VARI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CVCODICE = "+cp_ToStrODBC(this.oParentObject.w_DBCODICE);
             )
    else
      delete from (i_cTable) where;
            CVCODICE = this.oParentObject.w_DBCODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_03A7C860()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from DISTBASE
    i_nConn=i_TableProp[this.DISTBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DBCODICE = "+cp_ToStrODBC(this.oParentObject.w_DBCODICE);
             )
    else
      delete from (i_cTable) where;
            DBCODICE = this.oParentObject.w_DBCODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_03A7C6E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DISMBASE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DISMBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DISMBASE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DBDESCRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBDESCRI),'DISMBASE','DBDESCRI');
      +",UTCV ="+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'DISMBASE','UTCV');
      +",UTDV ="+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'DISMBASE','UTDV');
      +",DBDTINVA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBDTINVA),'DISMBASE','DBDTINVA');
      +",DBDTOBSO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBDTOBSO),'DISMBASE','DBDTOBSO');
      +",DBNOTAGG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBNOTAGG),'DISMBASE','DBNOTAGG');
      +",DBFLSTAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBFLSTAT),'DISMBASE','DBFLSTAT');
      +",DBCODRIS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBCODRIS),'DISMBASE','DBCODRIS');
      +",DBPRIORI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPRIORI),'DISMBASE','DBPRIORI');
      +",DBTIPDIS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBTIPDIS),'DISMBASE','DBTIPDIS');
          +i_ccchkf ;
      +" where ";
          +"DBCODICE = "+cp_ToStrODBC(this.oParentObject.w_DBCODICE);
             )
    else
      update (i_cTable) set;
          DBDESCRI = this.oParentObject.w_DBDESCRI;
          ,UTCV = this.w_UTCC;
          ,UTDV = this.w_UTDC;
          ,DBDTINVA = this.oParentObject.w_DBDTINVA;
          ,DBDTOBSO = this.oParentObject.w_DBDTOBSO;
          ,DBNOTAGG = this.oParentObject.w_DBNOTAGG;
          ,DBFLSTAT = this.oParentObject.w_DBFLSTAT;
          ,DBCODRIS = this.oParentObject.w_DBCODRIS;
          ,DBPRIORI = this.oParentObject.w_DBPRIORI;
          ,DBTIPDIS = this.oParentObject.w_DBTIPDIS;
          &i_ccchkf. ;
       where;
          DBCODICE = this.oParentObject.w_DBCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_03A7AE50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DISMBASE
    i_nConn=i_TableProp[this.DISMBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DISMBASE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DBCODICE"+",DBDESCRI"+",UTCC"+",UTDC"+",DBDTINVA"+",DBDTOBSO"+",DBNOTAGG"+",DBCODRIS"+",DBFLSTAT"+",DBDISKIT"+",DBPRIORI"+",DBTIPDIS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBCODICE),'DISMBASE','DBCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBDESCRI),'DISMBASE','DBDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'DISMBASE','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'DISMBASE','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBDTINVA),'DISMBASE','DBDTINVA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBDTOBSO),'DISMBASE','DBDTOBSO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBNOTAGG),'DISMBASE','DBNOTAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBCODRIS),'DISMBASE','DBCODRIS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBFLSTAT),'DISMBASE','DBFLSTAT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CICLO),'DISMBASE','DBDISKIT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBPRIORI),'DISMBASE','DBPRIORI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBTIPDIS),'DISMBASE','DBTIPDIS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DBCODICE',this.oParentObject.w_DBCODICE,'DBDESCRI',this.oParentObject.w_DBDESCRI,'UTCC',this.w_UTCC,'UTDC',this.w_UTDC,'DBDTINVA',this.oParentObject.w_DBDTINVA,'DBDTOBSO',this.oParentObject.w_DBDTOBSO,'DBNOTAGG',this.oParentObject.w_DBNOTAGG,'DBCODRIS',this.oParentObject.w_DBCODRIS,'DBFLSTAT',this.oParentObject.w_DBFLSTAT,'DBDISKIT',this.oParentObject.w_CICLO,'DBPRIORI',this.oParentObject.w_DBPRIORI,'DBTIPDIS',this.oParentObject.w_DBTIPDIS)
      insert into (i_cTable) (DBCODICE,DBDESCRI,UTCC,UTDC,DBDTINVA,DBDTOBSO,DBNOTAGG,DBCODRIS,DBFLSTAT,DBDISKIT,DBPRIORI,DBTIPDIS &i_ccchkf. );
         values (;
           this.oParentObject.w_DBCODICE;
           ,this.oParentObject.w_DBDESCRI;
           ,this.w_UTCC;
           ,this.w_UTDC;
           ,this.oParentObject.w_DBDTINVA;
           ,this.oParentObject.w_DBDTOBSO;
           ,this.oParentObject.w_DBNOTAGG;
           ,this.oParentObject.w_DBCODRIS;
           ,this.oParentObject.w_DBFLSTAT;
           ,this.oParentObject.w_CICLO;
           ,this.oParentObject.w_DBPRIORI;
           ,this.oParentObject.w_DBTIPDIS;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03A8B330()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DISTBASE
    i_nConn=i_TableProp[this.DISTBASE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DISTBASE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CPROWNUM"+",CPROWORD"+",DB__NOTE"+",DBARTCOM"+",DBCODCOM"+",DBCODICE"+",DBCOEIMP"+",DBCOEUM1"+",DBDESCOM"+",DBFLESPL"+",DBFLVARC"+",DBFLVARI"+",DBPERRIC"+",DBPERSCA"+",DBPERSFR"+",DBQTADIS"+",DBRECSCA"+",DBRECSFR"+",DBUNIMIS"+",DBINIVAL"+",DBFINVAL"+",DBRIFFAS"+",DBFLOMAG"+",DBCORLEA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.CPROWNUM),'DISTBASE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.CPROWORD),'DISTBASE','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DB__NOTE),'DISTBASE','DB__NOTE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBARTCOM),'DISTBASE','DBARTCOM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBCODCOM),'DISTBASE','DBCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBCODICE),'DISTBASE','DBCODICE');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBCOEIMP),'DISTBASE','DBCOEIMP');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBCOEUM1),'DISTBASE','DBCOEUM1');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBDESCOM),'DISTBASE','DBDESCOM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBFLESPL),'DISTBASE','DBFLESPL');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBFLVARC),'DISTBASE','DBFLVARC');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBFLVARI),'DISTBASE','DBFLVARI');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBPERRIC),'DISTBASE','DBPERRIC');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBPERSCA),'DISTBASE','DBPERSCA');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBPERSFR),'DISTBASE','DBPERSFR');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBQTADIS),'DISTBASE','DBQTADIS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBRECSCA),'DISTBASE','DBRECSCA');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBRECSFR),'DISTBASE','DBRECSFR');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBUNIMIS),'DISTBASE','DBUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBINIVAL),'DISTBASE','DBINIVAL');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBFINVAL),'DISTBASE','DBFINVAL');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBRIFFAS),'DISTBASE','DBRIFFAS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBFLOMAG),'DISTBASE','DBFLOMAG');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_DISTBASE.DBCORLEA),'DISTBASE','DBCORLEA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CPROWNUM',_Curs_DISTBASE.CPROWNUM,'CPROWORD',_Curs_DISTBASE.CPROWORD,'DB__NOTE',_Curs_DISTBASE.DB__NOTE,'DBARTCOM',_Curs_DISTBASE.DBARTCOM,'DBCODCOM',_Curs_DISTBASE.DBCODCOM,'DBCODICE',this.oParentObject.w_DBCODICE,'DBCOEIMP',_Curs_DISTBASE.DBCOEIMP,'DBCOEUM1',_Curs_DISTBASE.DBCOEUM1,'DBDESCOM',_Curs_DISTBASE.DBDESCOM,'DBFLESPL',_Curs_DISTBASE.DBFLESPL,'DBFLVARC',_Curs_DISTBASE.DBFLVARC,'DBFLVARI',_Curs_DISTBASE.DBFLVARI)
      insert into (i_cTable) (CPROWNUM,CPROWORD,DB__NOTE,DBARTCOM,DBCODCOM,DBCODICE,DBCOEIMP,DBCOEUM1,DBDESCOM,DBFLESPL,DBFLVARC,DBFLVARI,DBPERRIC,DBPERSCA,DBPERSFR,DBQTADIS,DBRECSCA,DBRECSFR,DBUNIMIS,DBINIVAL,DBFINVAL,DBRIFFAS,DBFLOMAG,DBCORLEA &i_ccchkf. );
         values (;
           _Curs_DISTBASE.CPROWNUM;
           ,_Curs_DISTBASE.CPROWORD;
           ,_Curs_DISTBASE.DB__NOTE;
           ,_Curs_DISTBASE.DBARTCOM;
           ,_Curs_DISTBASE.DBCODCOM;
           ,this.oParentObject.w_DBCODICE;
           ,_Curs_DISTBASE.DBCOEIMP;
           ,_Curs_DISTBASE.DBCOEUM1;
           ,_Curs_DISTBASE.DBDESCOM;
           ,_Curs_DISTBASE.DBFLESPL;
           ,_Curs_DISTBASE.DBFLVARC;
           ,_Curs_DISTBASE.DBFLVARI;
           ,_Curs_DISTBASE.DBPERRIC;
           ,_Curs_DISTBASE.DBPERSCA;
           ,_Curs_DISTBASE.DBPERSFR;
           ,_Curs_DISTBASE.DBQTADIS;
           ,_Curs_DISTBASE.DBRECSCA;
           ,_Curs_DISTBASE.DBRECSFR;
           ,_Curs_DISTBASE.DBUNIMIS;
           ,_Curs_DISTBASE.DBINIVAL;
           ,_Curs_DISTBASE.DBFINVAL;
           ,_Curs_DISTBASE.DBRIFFAS;
           ,_Curs_DISTBASE.DBFLOMAG;
           ,_Curs_DISTBASE.DBCORLEA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03A894D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into COM_VARI
    i_nConn=i_TableProp[this.COM_VARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COM_VARI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COM_VARI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CVCODICE"+",CVNUMRIF"+",CPROWNUM"+",CPROWORD"+",CVCODART"+",CVCODCOM"+",CVUNIMIS"+",CVQTADIS"+",CVFLESPL"+",CVCOEIMP"+",CVARTCOM"+",CVCOEUM1"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DBCODICE),'COM_VARI','CVCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG),'COM_VARI','CVNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CPROWNUM),'COM_VARI','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CPROWORD),'COM_VARI','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CVCODART),'COM_VARI','CVCODART');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CVCODCOM),'COM_VARI','CVCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CVUNIMIS),'COM_VARI','CVUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CVQTADIS),'COM_VARI','CVQTADIS');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CVFLESPL),'COM_VARI','CVFLESPL');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CVCOEIMP),'COM_VARI','CVCOEIMP');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CVARTCOM),'COM_VARI','CVARTCOM');
      +","+cp_NullLink(cp_ToStrODBC(_Curs_COM_VARI.CVCOEUM1),'COM_VARI','CVCOEUM1');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CVCODICE',this.oParentObject.w_DBCODICE,'CVNUMRIF',this.w_NUMRIG,'CPROWNUM',_Curs_COM_VARI.CPROWNUM,'CPROWORD',_Curs_COM_VARI.CPROWORD,'CVCODART',_Curs_COM_VARI.CVCODART,'CVCODCOM',_Curs_COM_VARI.CVCODCOM,'CVUNIMIS',_Curs_COM_VARI.CVUNIMIS,'CVQTADIS',_Curs_COM_VARI.CVQTADIS,'CVFLESPL',_Curs_COM_VARI.CVFLESPL,'CVCOEIMP',_Curs_COM_VARI.CVCOEIMP,'CVARTCOM',_Curs_COM_VARI.CVARTCOM,'CVCOEUM1',_Curs_COM_VARI.CVCOEUM1)
      insert into (i_cTable) (CVCODICE,CVNUMRIF,CPROWNUM,CPROWORD,CVCODART,CVCODCOM,CVUNIMIS,CVQTADIS,CVFLESPL,CVCOEIMP,CVARTCOM,CVCOEUM1 &i_ccchkf. );
         values (;
           this.oParentObject.w_DBCODICE;
           ,this.w_NUMRIG;
           ,_Curs_COM_VARI.CPROWNUM;
           ,_Curs_COM_VARI.CPROWORD;
           ,_Curs_COM_VARI.CVCODART;
           ,_Curs_COM_VARI.CVCODCOM;
           ,_Curs_COM_VARI.CVUNIMIS;
           ,_Curs_COM_VARI.CVQTADIS;
           ,_Curs_COM_VARI.CVFLESPL;
           ,_Curs_COM_VARI.CVCOEIMP;
           ,_Curs_COM_VARI.CVARTCOM;
           ,_Curs_COM_VARI.CVCOEUM1;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARDTOBSO"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_CODCOM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARDTOBSO;
        from (i_cTable) where;
            ARCODART = this.w_CODCOM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DTOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='COM_VARI'
    this.cWorkTables[2]='DISMBASE'
    this.cWorkTables[3]='DISTBASE'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='CONF_DIS'
    this.cWorkTables[6]='CONFDDIS'
    this.cWorkTables[7]='MODE_DIS'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_DISTBASE')
      use in _Curs_DISTBASE
    endif
    if used('_Curs_COM_VARI')
      use in _Curs_COM_VARI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
