* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1bmg                                                        *
*              Controllo cancellazione dettaglio                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-21                                                      *
* Last revis.: 2008-12-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar1bmg",oParentObject,m.pOPER)
return(i_retval)

define class tgsar1bmg as StdBatch
  * --- Local variables
  pOPER = space(2)
  w_INIVAL = ctod("  /  /  ")
  w_FINVAL = ctod("  /  /  ")
  w_ATTR = space(10)
  w_MULTIP = space(1)
  w_OBBLIG = space(1)
  w_NUMREC = 0
  w_PADRE = .NULL.
  w_originale = 0
  w_ROWORD = 0
  * --- WorkFile variables
  ASS_ATTR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Disabilita f6
    this.w_PADRE = this.oparentobject
    this.w_INIVAL = this.oParentObject.w_GRINIVAL
    this.w_FINVAL = this.oParentObject.w_GRFINVAL
    this.w_ATTR = this.oParentObject.w_CODATTR
    this.w_MULTIP = this.oParentObject.w_GRMULTIP
    this.w_OBBLIG = this.oParentObject.w_GROBBLIG
    this.w_ROWORD = this.oParentObject.w_CPROWORD
    if this.pOPER="CH"
      * --- Sono in modifica e ho cambiato il codice
      if this.oParentObject.cFunction="Edit"
        * --- Select from ASS_ATTR
        i_nConn=i_TableProp[this.ASS_ATTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATTR_idx,2],.t.,this.ASS_ATTR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ASS_ATTR ";
              +" where ASCODATT="+cp_ToStrODBC(this.oParentObject.w_GRCODICE)+" And ASCODFAM="+cp_ToStrODBC(this.oParentObject.o_GRFAMATT)+"";
               ,"_Curs_ASS_ATTR")
        else
          select * from (i_cTable);
           where ASCODATT=this.oParentObject.w_GRCODICE And ASCODFAM=this.oParentObject.o_GRFAMATT;
            into cursor _Curs_ASS_ATTR
        endif
        if used('_Curs_ASS_ATTR')
          select _Curs_ASS_ATTR
          locate for 1=1
          do while not(eof())
          if !ah_yesno("Il codice: %1 � gi� stato associato%0Confermi la modifica?"," ",alltrim(this.oParentObject.o_GRFAMATT))
            this.oParentObject.w_GRFAMATT = this.oParentObject.o_GRFAMATT
          endif
          exit
            select _Curs_ASS_ATTR
            continue
          enddo
          use
        endif
      endif
      if NOT EMPTY(this.oParentObject.w_CODATTR)
        this.w_PADRE.Markpos()     
        * --- Cerco se esiste un attributo con codice attributo collegato:
        *     se non esiste lo inserisco in automatico, altrimenti se � gi� presente non faccio niente.
        this.w_NUMREC = this.w_PADRE.Search("nvl(t_GRFAMATT, space(10))="+cp_ToStrODBC(this.oParentObject.w_CODATTR)+"and not deleted()")
        this.w_PADRE.Repos()     
        if this.w_NUMREC=-1
          if NOT EMPTY(this.oParentObject.w_CODATTR)
            * --- Aggiungo una riga sul temporaneo
            this.w_PADRE.addRow()     
            this.oParentObject.w_GRFAMATT = this.w_ATTR
            this.oParentObject.w_GRMULTIP = this.w_MULTIP
            this.oParentObject.w_GROBBLIG = this.w_OBBLIG
            this.oParentObject.w_GRINIVAL = this.w_INIVAL
            this.oParentObject.w_GRFINVAL = this.w_FINVAL
            this.oParentObject.w_CPROWORD = this.w_ROWORD-1
            * --- Carica il temporaneo dei dati
            this.w_PADRE.SaveRow()     
          endif
        else
          i_retcode = 'stop'
          return
        endif
      endif
    else
      * --- Select from ASS_ATTR
      i_nConn=i_TableProp[this.ASS_ATTR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATTR_idx,2],.t.,this.ASS_ATTR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" ASS_ATTR ";
            +" where ASCODATT="+cp_ToStrODBC(this.oParentObject.w_GRCODICE)+" And ASCODFAM="+cp_ToStrODBC(this.oParentObject.w_GRFAMATT)+"";
             ,"_Curs_ASS_ATTR")
      else
        select * from (i_cTable);
         where ASCODATT=this.oParentObject.w_GRCODICE And ASCODFAM=this.oParentObject.w_GRFAMATT;
          into cursor _Curs_ASS_ATTR
      endif
      if used('_Curs_ASS_ATTR')
        select _Curs_ASS_ATTR
        locate for 1=1
        do while not(eof())
        if ah_yesno("Il codice: %1 � gi� stato associato%0Confermi la cancellazione?"," ",alltrim(this.oParentObject.w_GRFAMATT))
          bOK=.T.
        else
          bOK=.F.
        endif
        exit
          select _Curs_ASS_ATTR
          continue
        enddo
        use
      endif
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ASS_ATTR'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_ASS_ATTR')
      use in _Curs_ASS_ATTR
    endif
    if used('_Curs_ASS_ATTR')
      use in _Curs_ASS_ATTR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
