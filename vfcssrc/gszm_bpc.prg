* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bpc                                                        *
*              Tasto destro sui tipi attivit�                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-03                                                      *
* Last revis.: 2010-03-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFUNZ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bpc",oParentObject,m.pFUNZ)
return(i_retval)

define class tgszm_bpc as StdBatch
  * --- Local variables
  pFUNZ = space(1)
  w_OBJECT = .NULL.
  w_CACODICE = space(20)
  w_CACODSER = space(41)
  w_TEST = .f.
  w_PROG = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tasto destro sui tipi attivit�
    * --- Assegno i valori alle variabili
    this.w_CACODICE = g_oMenu.getbyindexkeyvalue(1)
    * --- Lancia maschera filtra per attivit�
    if this.pFUNZ="2"
      this.w_OBJECT = GSAG_KFP(this)
      this.w_CACODICE = ""
      this.w_TEST = .T.
      if empty(this.w_CACODSER)
        Ah_errormsg("Attenzione, nessuna prestazione selezionata")
        this.w_OBJECT = .Null.
        i_retcode = 'stop'
        return
      endif
    else
      if empty(this.w_CACODICE)
        Ah_errormsg("Attenzione, nessun tipo selezionato")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Lancia maschera mostra prestazioni
    this.w_PROG = g_oMenu.oParentobject
    this.w_OBJECT = GSAG_KMP(this)
    * --- Utilizzo la manual blocks Chiudo perch� non funziona il parent lanciato direttamente
    if this.pFUNZ="2"
      if Lower(g_oMenu.ocontained.class)<>"gsag_aat"
* --- Area Manuale = Chiudo
* --- gszm_bpc
if type('g_oMenu.ocontained.parent')='O'
  g_oMenu.ocontained.parent.ecpquit()
endif
* --- Fine Area Manuale
      endif
      * --- Con il doppio click carico l'attivit� che ho selezionato nello zoom.
      this.w_PROG.w_ATCAUATT = this.w_OBJECT.w_ATCAUATT
      = setvaluelinked ( "M" , this.w_PROG, "w_ATCAUATT", this.w_OBJECT.w_ATCAUATT)
    endif
    this.w_OBJECT = .Null.
    this.w_PROG = .NULL.
  endproc


  proc Init(oParentObject,pFUNZ)
    this.pFUNZ=pFUNZ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFUNZ"
endproc
