* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bnr                                                        *
*              Controlli in modifica movimento                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_33]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-26                                                      *
* Last revis.: 2004-01-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bnr",oParentObject)
return(i_retval)

define class tgsma_bnr as StdBatch
  * --- Local variables
  w_FLELGM = space(1)
  w_MESS = space(10)
  w_PADRE = .NULL.
  w_SALDO = 0
  w_STATO = space(100)
  w_OK = .f.
  * --- WorkFile variables
  MOVIMATR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Setta per non eseguire ricalcoli
    this.w_PADRE = this.oParentObject
    this.bUpdateParentObject=.f.
    this.oParentObject.w_HASEVENT = .T.
    do case
      case Upper(this.oParentObject.w_HASEVCOP)="ECPF6"
        if g_MATR="S" AND this.oParentObject.w_GESMAT="S"
          this.w_SALDO = 0
          * --- Read from MOVIMATR
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MOVIMATR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOVIMATR_idx,2],.t.,this.MOVIMATR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MT_SALDO"+;
              " from "+i_cTable+" MOVIMATR where ";
                  +"MTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MMSERIAL);
                  +" and MTROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                  +" and MTNUMRIF = "+cp_ToStrODBC(this.oParentObject.w_MMNUMRIF);
                  +" and MT_SALDO = "+cp_ToStrODBC(1);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MT_SALDO;
              from (i_cTable) where;
                  MTSERIAL = this.oParentObject.w_MMSERIAL;
                  and MTROWNUM = this.oParentObject.w_CPROWNUM;
                  and MTNUMRIF = this.oParentObject.w_MMNUMRIF;
                  and MT_SALDO = 1;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALDO = NVL(cp_ToDate(_read_.MT_SALDO),cp_NullValue(_read_.MT_SALDO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_SALDO<>0
            this.w_MESS = "La riga movimenta matricole successivamente utilizzate. Impossibile eliminare"
            this.oParentObject.w_HASEVENT = .F.
            ah_ErrorMsg(this.w_MESS)
          endif
        endif
      case Upper(this.oParentObject.w_HASEVCOP)="ECPEDIT"
        * --- Rettifiche Inventariali, Movimento non puo essere modificato
        if !Empty( this.oParentObject.w_MMFLRETT )
          this.w_MESS = "Impossibile modificare un movimento generato dalla procedura di rettifica inventariale"
          ah_ErrorMsg(this.w_MESS)
          this.oParentObject.w_HASEVENT = .F.
          i_retcode = 'stop'
          return
        endif
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if Not this.oParentObject.w_HASEVENT
          ah_ErrorMsg("%1",,"",this.w_MESS)
        endif
      case Upper(this.oParentObject.w_HASEVCOP)="ECPDELETE"
        * --- Verifica se Eliminabile
        if NOT EMPTY(this.oParentObject.w_MMRIFPRO) AND TYPE("THIS.oparentobject.w_DELCARPRO.caption")<>"C"
          if LEFT(this.oParentObject.w_MMRIFPRO,1)$ "IO"
            this.w_MESS = ah_msgformat("Registrazione associata a una distinta di impegno componenti (modulo distinta base)%0Impossibile cancellare")
          else
            this.w_MESS = ah_msgformat("Registrazione associata a una distinta di carico da produzione (modulo distinta base)%0Impossibile cancellare")
          endif
          this.oParentObject.w_HASEVENT = .F.
        endif
        if !Empty( this.oParentObject.w_MMFLRETT ) AND TYPE("THIS.oparentobject.w_DELCARPRO.caption")<>"C"
          this.w_MESS = "Impossibile cancellare un movimento generato dalla procedura di rettifica inventariale"
          ah_ErrorMsg(this.w_MESS)
          this.oParentObject.w_HASEVENT = .F.
          i_retcode = 'stop'
          return
        endif
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if Not this.oParentObject.w_HASEVENT
          ah_ErrorMsg("%1",,"",this.w_MESS)
        endif
        * --- Controllo disponibilitÓ articoli...
        if this.oParentObject.w_HASEVENT And g_PERDIS="S" 
          * --- Lancio il check disponibilitÓ articoli
          this.oParentObject.w_HASEVENT = GSAR_BDA( This , "S", this.w_PADRE )
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli comuni modifica / cancellazione..
    if this.oParentObject.w_HASEVENT AND NOT EMPTY(this.oParentObject.w_MMSERPOS) AND g_GPOS="S"
      * --- Verifica se Modificabile
      this.w_OK = GSPS_BKM(this.oParentObject.w_MMSERPOS)
      if Not this.w_OK
        if Upper( this.oParentObject.w_HASEVCOP ) ="ECPEDIT"
          this.w_MESS = ah_msgformat("Registrazione generata da documento di vendita negozio (P.O.S.); impossibile modificare")
        else
          this.w_MESS = ah_msgformat("Registrazione generata da documento di vendita negozio (P.O.S.); impossibile eliminare")
        endif
        this.oParentObject.w_HASEVENT = .F.
      endif
    endif
    if this.oParentObject.w_HASEVENT
      this.w_MESS = CHKCONS("M",this.oParentObject.w_MMDATREG,"B","N") 
      this.oParentObject.w_HASEVENT = Empty(this.w_MESS)
      if Upper( this.oParentObject.w_HASEVCOP ) ="ECPEDIT"
        this.w_MESS = ah_msgformat("%1%0Impossibile modificare", CHKCONS("M",this.oParentObject.w_MMDATREG,"B","N") )
      else
        this.w_MESS = ah_msgformat("%1%0Impossibile eliminare", CHKCONS("M",this.oParentObject.w_MMDATREG,"B","N") )
      endif
    endif
    if this.oParentObject.w_HASEVENT
      this.w_PADRE.MarkPos()     
      SELECT (this.oParentObject.cTrsName)
      GO TOP
      SCAN
      this.w_FLELGM = iif(t_MMFLELGM=1,"S","")
      if ! CHKGIOM(this.oParentObject.w_MMCODESE,t_mmCODMAG,this.oParentObject.w_MMDATREG,this.w_FLELGM)
        SELECT (this.oParentObject.cTrsName)
        this.w_MESS = ah_msgformat("Errore: documento stampato sul giornale magazzino [%1]; impossibile variare", t_mmCODMAG)
        this.oParentObject.w_HASEVENT = .F.
        Exit
      endif
      ENDSCAN
      this.w_PADRE.RePos()     
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MOVIMATR'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
