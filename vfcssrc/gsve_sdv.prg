* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_sdv                                                        *
*              Ristampa documenti                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_40]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2014-09-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_sdv",oParentObject))

* --- Class definition
define class tgsve_sdv as StdForm
  Top    = 25
  Left   = 73

  * --- Standard Properties
  Width  = 535
  Height = 512+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-19"
  HelpContextID=74068585
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=103

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  ESERCIZI_IDX = 0
  AGENTI_IDX = 0
  CAN_TIER_IDX = 0
  ATTIVITA_IDX = 0
  LINGUE_IDX = 0
  VETTORI_IDX = 0
  CATECOMM_IDX = 0
  COC_MAST_IDX = 0
  DES_DIVE_IDX = 0
  ZONE_IDX = 0
  PAG_AMEN_IDX = 0
  PAR_PARC_IDX = 0
  cPrg = "gsve_sdv"
  cComment = "Ristampa documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_HIDE = space(1)
  w_ACQU = space(1)
  w_CATDO2 = space(2)
  o_CATDO2 = space(2)
  w_CATDO2 = space(2)
  w_CATDO1 = space(2)
  o_CATDO1 = space(2)
  w_CATDO1 = space(2)
  w_CATDO3 = space(2)
  o_CATDO3 = space(2)
  w_SELVEAC = space(1)
  o_SELVEAC = space(1)
  w_TIPOVAO = space(1)
  o_TIPOVAO = space(1)
  w_EDITCAU = .F.
  w_EDIT = .F.
  w_EDITINT = .F.
  w_fc = space(1)
  o_fc = space(1)
  w_azi = space(5)
  w_DASPED = .F.
  w_FILVEAC = space(10)
  w_stampa = 0
  w_TIPOIN = space(5)
  o_TIPOIN = space(5)
  w_TIPOIN = space(5)
  w_TIPOIN = space(5)
  w_fcc = space(1)
  w_codese = space(4)
  o_codese = space(4)
  w_FLEVAS = space(1)
  w_data1 = ctod('  /  /  ')
  w_data2 = ctod('  /  /  ')
  w_datain = ctod('  /  /  ')
  o_datain = ctod('  /  /  ')
  w_datafi = ctod('  /  /  ')
  w_numini = 0
  w_serie1 = space(10)
  w_numfin = 0
  w_serie2 = space(10)
  w_clifor = space(15)
  o_clifor = space(15)
  w_REPSEC = space(1)
  w_DQ = space(40)
  w_TIPCON = space(1)
  w_catdoc = space(2)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_MVRIFFAD = space(10)
  w_MVRIFODL = space(10)
  w_FLVEAC2 = space(1)
  w_MVSERIAL = space(10)
  w_SELALL = space(1)
  o_SELALL = space(1)
  w_CODART = space(20)
  w_SERDOC = space(10)
  w_DT = space(35)
  w_FLPROV = space(1)
  w_CODAGE = space(5)
  w_LINGUA = space(3)
  w_CODVET = space(5)
  w_CODDES = space(5)
  w_CODZON = space(3)
  w_CATCOM = space(3)
  w_CODPAG = space(5)
  w_NOSBAN = space(15)
  w_DESAGE = space(35)
  w_CODCOM = space(15)
  o_CODCOM = space(15)
  w_CODATT1 = space(15)
  w_CODATT2 = space(15)
  w_DESCOM = space(30)
  w_DESAT1 = space(30)
  w_DESAT2 = space(30)
  w_dt = space(35)
  w_CICLO = space(1)
  w_TDFLVEAC = space(1)
  w_PROVE = space(10)
  w_TIPATT = space(1)
  w_DESLINGUA = space(30)
  w_RIFPRA = space(1)
  w_RIFOGG = space(1)
  w_RIFVAL = space(1)
  w_RIFDAT = space(1)
  w_RIFSUP = space(1)
  w_RIFRIGHE = space(1)
  w_FLGNOT = space(1)
  w_RIFGU = space(1)
  w_RIFMINMAX = space(1)
  w_RIFSCOFIN = space(1)
  w_RIFDATDIR = space(1)
  w_RIFDATONO = space(1)
  w_RIFDATTEM = space(1)
  w_RIFDATSPA = space(1)
  w_RIFQTA = space(1)
  w_RIFRESTIT = space(1)
  w_RIFPARTI = space(1)
  w_CATEGO = space(2)
  w_DESVET = space(30)
  w_DESDES = space(40)
  w_DESZON = space(35)
  w_DESPAG = space(30)
  w_DESCACM = space(35)
  w_DESBAN = space(30)
  w_CATEG1 = space(2)
  w_FLVEAC = space(1)
  w_CATDO3 = space(2)
  w_RIFNOIMPO = space(1)
  w_RIFCOEDIF = space(1)
  w_RIFIMP = space(1)
  w_NOSBAN = space(15)
  w_RIFDATGEN = space(1)
  w_PRTIPCAU = space(1)
  w_RIFRAGFAS = space(1)
  w_OLDFLVEAC = space(1)
  w_ZoomTD = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsve_sdv
  * --- Disabilita il salvataggio della posizione
  bchkpositionform=.f.
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    local l_CICLO
    l_CICLO=this.oParentObject
    return(lower('GSVE_SDV'+IIF(Type('l_CICLO')='C',','+l_CICLO,'')))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_sdvPag1","gsve_sdv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Stampa")
      .Pages(2).addobject("oPag","tgsve_sdvPag2","gsve_sdv",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Filtri aggiuntivi")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCATDO2_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomTD = this.oPgFrm.Pages(1).oPag.ZoomTD
    DoDefault()
    proc Destroy()
      this.w_ZoomTD = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='AGENTI'
    this.cWorkTables[5]='CAN_TIER'
    this.cWorkTables[6]='ATTIVITA'
    this.cWorkTables[7]='LINGUE'
    this.cWorkTables[8]='VETTORI'
    this.cWorkTables[9]='CATECOMM'
    this.cWorkTables[10]='COC_MAST'
    this.cWorkTables[11]='DES_DIVE'
    this.cWorkTables[12]='ZONE'
    this.cWorkTables[13]='PAG_AMEN'
    this.cWorkTables[14]='PAR_PARC'
    return(this.OpenAllTables(14))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSVE_BRD(this,"A")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_HIDE=space(1)
      .w_ACQU=space(1)
      .w_CATDO2=space(2)
      .w_CATDO2=space(2)
      .w_CATDO1=space(2)
      .w_CATDO1=space(2)
      .w_CATDO3=space(2)
      .w_SELVEAC=space(1)
      .w_TIPOVAO=space(1)
      .w_EDITCAU=.f.
      .w_EDIT=.f.
      .w_EDITINT=.f.
      .w_fc=space(1)
      .w_azi=space(5)
      .w_DASPED=.f.
      .w_FILVEAC=space(10)
      .w_stampa=0
      .w_TIPOIN=space(5)
      .w_TIPOIN=space(5)
      .w_TIPOIN=space(5)
      .w_fcc=space(1)
      .w_codese=space(4)
      .w_FLEVAS=space(1)
      .w_data1=ctod("  /  /  ")
      .w_data2=ctod("  /  /  ")
      .w_datain=ctod("  /  /  ")
      .w_datafi=ctod("  /  /  ")
      .w_numini=0
      .w_serie1=space(10)
      .w_numfin=0
      .w_serie2=space(10)
      .w_clifor=space(15)
      .w_REPSEC=space(1)
      .w_DQ=space(40)
      .w_TIPCON=space(1)
      .w_catdoc=space(2)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_MVRIFFAD=space(10)
      .w_MVRIFODL=space(10)
      .w_FLVEAC2=space(1)
      .w_MVSERIAL=space(10)
      .w_SELALL=space(1)
      .w_CODART=space(20)
      .w_SERDOC=space(10)
      .w_DT=space(35)
      .w_FLPROV=space(1)
      .w_CODAGE=space(5)
      .w_LINGUA=space(3)
      .w_CODVET=space(5)
      .w_CODDES=space(5)
      .w_CODZON=space(3)
      .w_CATCOM=space(3)
      .w_CODPAG=space(5)
      .w_NOSBAN=space(15)
      .w_DESAGE=space(35)
      .w_CODCOM=space(15)
      .w_CODATT1=space(15)
      .w_CODATT2=space(15)
      .w_DESCOM=space(30)
      .w_DESAT1=space(30)
      .w_DESAT2=space(30)
      .w_dt=space(35)
      .w_CICLO=space(1)
      .w_TDFLVEAC=space(1)
      .w_PROVE=space(10)
      .w_TIPATT=space(1)
      .w_DESLINGUA=space(30)
      .w_RIFPRA=space(1)
      .w_RIFOGG=space(1)
      .w_RIFVAL=space(1)
      .w_RIFDAT=space(1)
      .w_RIFSUP=space(1)
      .w_RIFRIGHE=space(1)
      .w_FLGNOT=space(1)
      .w_RIFGU=space(1)
      .w_RIFMINMAX=space(1)
      .w_RIFSCOFIN=space(1)
      .w_RIFDATDIR=space(1)
      .w_RIFDATONO=space(1)
      .w_RIFDATTEM=space(1)
      .w_RIFDATSPA=space(1)
      .w_RIFQTA=space(1)
      .w_RIFRESTIT=space(1)
      .w_RIFPARTI=space(1)
      .w_CATEGO=space(2)
      .w_DESVET=space(30)
      .w_DESDES=space(40)
      .w_DESZON=space(35)
      .w_DESPAG=space(30)
      .w_DESCACM=space(35)
      .w_DESBAN=space(30)
      .w_CATEG1=space(2)
      .w_FLVEAC=space(1)
      .w_CATDO3=space(2)
      .w_RIFNOIMPO=space(1)
      .w_RIFCOEDIF=space(1)
      .w_RIFIMP=space(1)
      .w_NOSBAN=space(15)
      .w_RIFDATGEN=space(1)
      .w_PRTIPCAU=space(1)
      .w_RIFRAGFAS=space(1)
      .w_OLDFLVEAC=space(1)
        .w_HIDE = IIF(Type('This.oParentObject')='C' And !This.oParentObject$'A-V','S','N')
        .w_ACQU = iif(!isAhe(), g_ACQU, 'S')
        .w_CATDO2 = 'XX'
        .w_CATDO2 = 'XX'
        .w_CATDO1 = 'XX'
        .w_CATDO1 = 'XX'
        .w_CATDO3 = 'XX'
        .w_SELVEAC = 'O'
        .w_TIPOVAO = IIF(!IsAhe() And g_ACQU<>'S' And this.oParentObject<>'O', ' ', this.oParentObject)
        .w_EDITCAU = .T.
        .w_EDIT = .T.
        .w_EDITINT = .T.
        .w_fc = IIF(.w_TIPOVAO='V', 'C', 'F')
        .w_azi = i_codazi
          .DoRTCalc(15,15,.f.)
        .w_FILVEAC = IIF(!IsAhe() And g_ACQU='S', 'V', ' ')
          .DoRTCalc(17,17,.f.)
        .w_TIPOIN = SPACE(5)
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_TIPOIN))
          .link_1_18('Full')
        endif
        .w_TIPOIN = SPACE(5)
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_TIPOIN))
          .link_1_19('Full')
        endif
        .w_TIPOIN = SPACE(5)
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_TIPOIN))
          .link_1_20('Full')
        endif
          .DoRTCalc(21,21,.f.)
        .w_codese = g_CODESE
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_codese))
          .link_1_22('Full')
        endif
        .w_FLEVAS = ' '
          .DoRTCalc(24,25,.f.)
        .w_datain = .w_data1
        .w_datafi = .w_data2
        .w_numini = 1
        .w_serie1 = ''
        .w_numfin = IIF(IsAhe(), 9999999999, 999999999999999)
        .w_serie2 = ''
        .w_clifor = space(15)
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_clifor))
          .link_1_32('Full')
        endif
        .w_REPSEC = 'S'
          .DoRTCalc(34,34,.f.)
        .w_TIPCON = IIF(.w_FC='C' OR .w_FC='F',.w_FC,'')
          .DoRTCalc(36,36,.f.)
        .w_OBTEST = .w_datain
          .DoRTCalc(38,38,.f.)
        .w_MVRIFFAD = SPACE(10)
        .w_MVRIFODL = SPACE(10)
        .w_FLVEAC2 = ''
        .w_MVSERIAL = SPACE(10)
      .oPgFrm.Page1.oPag.ZoomTD.Calculate()
        .w_SELALL = 'N'
          .DoRTCalc(44,44,.f.)
        .w_SERDOC = 'XXXXXXXXXX'
          .DoRTCalc(46,46,.f.)
        .w_FLPROV = 'T'
        .w_CODAGE = space(5)
        .DoRTCalc(48,48,.f.)
        if not(empty(.w_CODAGE))
          .link_2_2('Full')
        endif
        .DoRTCalc(49,49,.f.)
        if not(empty(.w_LINGUA))
          .link_2_4('Full')
        endif
        .DoRTCalc(50,50,.f.)
        if not(empty(.w_CODVET))
          .link_2_5('Full')
        endif
        .w_CODDES = SPACE(5)
        .DoRTCalc(51,51,.f.)
        if not(empty(.w_CODDES))
          .link_2_6('Full')
        endif
        .DoRTCalc(52,52,.f.)
        if not(empty(.w_CODZON))
          .link_2_7('Full')
        endif
        .DoRTCalc(53,53,.f.)
        if not(empty(.w_CATCOM))
          .link_2_8('Full')
        endif
        .DoRTCalc(54,54,.f.)
        if not(empty(.w_CODPAG))
          .link_2_9('Full')
        endif
        .DoRTCalc(55,55,.f.)
        if not(empty(.w_NOSBAN))
          .link_2_10('Full')
        endif
        .DoRTCalc(56,57,.f.)
        if not(empty(.w_CODCOM))
          .link_2_12('Full')
        endif
        .w_CODATT1 = SPACE(15)
        .DoRTCalc(58,58,.f.)
        if not(empty(.w_CODATT1))
          .link_2_13('Full')
        endif
        .w_CODATT2 = SPACE(15)
        .DoRTCalc(59,59,.f.)
        if not(empty(.w_CODATT2))
          .link_2_14('Full')
        endif
          .DoRTCalc(60,63,.f.)
        .w_CICLO = this.oParentObject
          .DoRTCalc(65,66,.f.)
        .w_TIPATT = "A"
          .DoRTCalc(68,74,.f.)
        .w_FLGNOT = 'S'
          .DoRTCalc(76,83,.f.)
        .w_RIFRESTIT = 'A'
          .DoRTCalc(85,85,.f.)
        .w_CATEGO = IIF(.w_CICLO='V',IIF(.w_ACQU='S', IIF(.w_CATDO1$'XX-TT' , '  ', .w_CATDO1), IIF(.w_CATDO2='IF', 'DI', IIF(.w_CATDO2='DF', 'DT', IIF(.w_CATDO2$'XX-TT', '  ', .w_CATDO2)))), IIF(.w_CATDO3$'XX-TT', '  ', .w_CATDO3))
          .DoRTCalc(87,92,.f.)
        .w_CATEG1 = IIF(.w_CICLO='V',IIF(.w_ACQU='S', IIF(.w_CATDO1='TT', 'TT', .w_CATDO1), IIF(.w_CATDO2='IF', 'DI', IIF(.w_CATDO2='DF', 'DT', IIF(.w_CATDO2='TT', 'TT', .w_CATDO2)))), IIF(.w_CATDO3='TT', 'TT', .w_CATDO3))
          .DoRTCalc(94,94,.f.)
        .w_CATDO3 = 'XX'
        .DoRTCalc(96,99,.f.)
        if not(empty(.w_NOSBAN))
          .link_2_37('Full')
        endif
    endwith
    this.DoRTCalc(100,103,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_60.enabled = this.oPgFrm.Page1.oPag.oBtn_1_60.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,15,.t.)
            .w_FILVEAC = IIF(!IsAhe() And g_ACQU='S', 'V', ' ')
        .DoRTCalc(17,17,.t.)
        if .o_CATDO1<>.w_CATDO1.or. .o_CATDO2<>.w_CATDO2.or. .o_CATDO3<>.w_CATDO3
            .w_TIPOIN = SPACE(5)
          .link_1_18('Full')
        endif
        if .o_CATDO3<>.w_CATDO3.or. .o_CATDO2<>.w_CATDO2.or. .o_CATDO1<>.w_CATDO1
            .w_TIPOIN = SPACE(5)
          .link_1_19('Full')
        endif
        if .o_CATDO3<>.w_CATDO3.or. .o_CATDO2<>.w_CATDO2.or. .o_CATDO1<>.w_CATDO1
            .w_TIPOIN = SPACE(5)
          .link_1_20('Full')
        endif
        .DoRTCalc(21,25,.t.)
        if .o_codese<>.w_codese
            .w_datain = .w_data1
        endif
        if .o_codese<>.w_codese
            .w_datafi = .w_data2
        endif
        .DoRTCalc(28,31,.t.)
        if .o_FC<>.w_FC
            .w_clifor = space(15)
          .link_1_32('Full')
        endif
        .DoRTCalc(33,34,.t.)
        if .o_FC<>.w_FC
            .w_TIPCON = IIF(.w_FC='C' OR .w_FC='F',.w_FC,'')
        endif
        .DoRTCalc(36,36,.t.)
        if .o_datain<>.w_datain
            .w_OBTEST = .w_datain
        endif
        .DoRTCalc(38,40,.t.)
            .w_FLVEAC2 = ''
        .oPgFrm.Page1.oPag.ZoomTD.Calculate()
        if .o_SELALL<>.w_SELALL
          .Calculate_HUXVGENZPI()
        endif
        .DoRTCalc(42,48,.t.)
        if .o_TIPOIN<>.w_TIPOIN.or. .o_clifor<>.w_clifor
          .link_2_4('Full')
        endif
        .DoRTCalc(50,50,.t.)
        if .o_CLIFOR<>.w_CLIFOR.or. .o_fc<>.w_fc
            .w_CODDES = SPACE(5)
          .link_2_6('Full')
        endif
        .DoRTCalc(52,57,.t.)
        if .o_CODCOM<>.w_CODCOM
            .w_CODATT1 = SPACE(15)
          .link_2_13('Full')
        endif
        if .o_CODCOM<>.w_CODCOM
            .w_CODATT2 = SPACE(15)
          .link_2_14('Full')
        endif
        if .o_TIPOIN<>.w_TIPOIN.or. .o_CATDO1<>.w_CATDO1.or. .o_CATDO2<>.w_CATDO2.or. .o_CATDO3<>.w_CATDO3.or. .o_SELVEAC<>.w_SELVEAC
          .Calculate_ZYDHNISHJF()
        endif
        .DoRTCalc(60,85,.t.)
        if .o_CATDO1<>.w_CATDO1.or. .o_CATDO2<>.w_CATDO2.or. .o_CATDO3<>.w_CATDO3
            .w_CATEGO = IIF(.w_CICLO='V',IIF(.w_ACQU='S', IIF(.w_CATDO1$'XX-TT' , '  ', .w_CATDO1), IIF(.w_CATDO2='IF', 'DI', IIF(.w_CATDO2='DF', 'DT', IIF(.w_CATDO2$'XX-TT', '  ', .w_CATDO2)))), IIF(.w_CATDO3$'XX-TT', '  ', .w_CATDO3))
        endif
        .DoRTCalc(87,92,.t.)
        if .o_CATDO1<>.w_CATDO1.or. .o_CATDO2<>.w_CATDO2.or. .o_CATDO3<>.w_CATDO3
            .w_CATEG1 = IIF(.w_CICLO='V',IIF(.w_ACQU='S', IIF(.w_CATDO1='TT', 'TT', .w_CATDO1), IIF(.w_CATDO2='IF', 'DI', IIF(.w_CATDO2='DF', 'DT', IIF(.w_CATDO2='TT', 'TT', .w_CATDO2)))), IIF(.w_CATDO3='TT', 'TT', .w_CATDO3))
        endif
        if .o_TIPOIN<>.w_TIPOIN
          .Calculate_VLEIJJOQGR()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(94,103,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomTD.Calculate()
    endwith
  return

  proc Calculate_HUXVGENZPI()
    with this
          * --- Sel/desel. tutto
          Seleziona_Deseleziona(this;
             )
          Controllo_Causale_Intestatario(this;
             )
    endwith
  endproc
  proc Calculate_EZAJIWWJGI()
    with this
          * --- Controllo causale intestatario
          Controllo_Causale_Intestatario(this;
             )
    endwith
  endproc
  proc Calculate_ZYDHNISHJF()
    with this
          * --- Cambio tipo documento
          .w_CATEGO = IIF(.w_CICLO='V',IIF(.w_ACQU='S', IIF(.w_CATDO1='XX', '  ', .w_CATDO1), IIF(.w_CATDO2='IF', 'DI', IIF(.w_CATDO2='DF', 'DT', IIF(.w_CATDO2='XX', '  ', .w_CATDO2)))), IIF(.w_CATDO3='XX', '  ', .w_CATDO3))
          .w_CATEG1 = IIF(.w_CICLO='V',IIF(.w_ACQU='S', IIF(.w_CATDO1='TT', 'TT', .w_CATDO1), IIF(.w_CATDO2='IF', 'DI', IIF(.w_CATDO2='DF', 'DT', IIF(.w_CATDO2='TT', 'TT', .w_CATDO2)))), IIF(.w_CATDO3='TT', 'TT', .w_CATDO3))
          CheckOnlyTipDoc(this;
             )
          Controllo_Causale_Intestatario(this;
             )
    endwith
  endproc
  proc Calculate_VLEIJJOQGR()
    with this
          * --- Cambio w_fc al cambio del tipo documento
          .w_fc = IIF(empty(nvl(.w_fcc,'')),.w_fc, .w_fcc)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCATDO2_1_3.enabled = this.oPgFrm.Page1.oPag.oCATDO2_1_3.mCond()
    this.oPgFrm.Page1.oPag.oCATDO2_1_4.enabled = this.oPgFrm.Page1.oPag.oCATDO2_1_4.mCond()
    this.oPgFrm.Page1.oPag.oCATDO1_1_5.enabled = this.oPgFrm.Page1.oPag.oCATDO1_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCATDO1_1_6.enabled = this.oPgFrm.Page1.oPag.oCATDO1_1_6.mCond()
    this.oPgFrm.Page1.oPag.oTIPOIN_1_18.enabled = this.oPgFrm.Page1.oPag.oTIPOIN_1_18.mCond()
    this.oPgFrm.Page1.oPag.oTIPOIN_1_19.enabled = this.oPgFrm.Page1.oPag.oTIPOIN_1_19.mCond()
    this.oPgFrm.Page1.oPag.oTIPOIN_1_20.enabled = this.oPgFrm.Page1.oPag.oTIPOIN_1_20.mCond()
    this.oPgFrm.Page1.oPag.ocodese_1_22.enabled = this.oPgFrm.Page1.oPag.ocodese_1_22.mCond()
    this.oPgFrm.Page1.oPag.odatain_1_26.enabled = this.oPgFrm.Page1.oPag.odatain_1_26.mCond()
    this.oPgFrm.Page1.oPag.odatafi_1_27.enabled = this.oPgFrm.Page1.oPag.odatafi_1_27.mCond()
    this.oPgFrm.Page1.oPag.oclifor_1_32.enabled = this.oPgFrm.Page1.oPag.oclifor_1_32.mCond()
    this.oPgFrm.Page2.oPag.oCODDES_2_6.enabled = this.oPgFrm.Page2.oPag.oCODDES_2_6.mCond()
    this.oPgFrm.Page2.oPag.oCODCOM_2_12.enabled = this.oPgFrm.Page2.oPag.oCODCOM_2_12.mCond()
    this.oPgFrm.Page2.oPag.oCODATT1_2_13.enabled = this.oPgFrm.Page2.oPag.oCODATT1_2_13.mCond()
    this.oPgFrm.Page2.oPag.oCODATT2_2_14.enabled = this.oPgFrm.Page2.oPag.oCODATT2_2_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_59.enabled = this.oPgFrm.Page1.oPag.oBtn_1_59.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show1
    i_show1=not(isAlt() OR (IsAhe() AND This.oParentObject$'ST'))
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Filtri aggiuntivi"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    this.oPgFrm.Page1.oPag.oCATDO2_1_3.visible=!this.oPgFrm.Page1.oPag.oCATDO2_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCATDO2_1_4.visible=!this.oPgFrm.Page1.oPag.oCATDO2_1_4.mHide()
    this.oPgFrm.Page1.oPag.oCATDO1_1_5.visible=!this.oPgFrm.Page1.oPag.oCATDO1_1_5.mHide()
    this.oPgFrm.Page1.oPag.oCATDO1_1_6.visible=!this.oPgFrm.Page1.oPag.oCATDO1_1_6.mHide()
    this.oPgFrm.Page1.oPag.oCATDO3_1_7.visible=!this.oPgFrm.Page1.oPag.oCATDO3_1_7.mHide()
    this.oPgFrm.Page1.oPag.oSELVEAC_1_8.visible=!this.oPgFrm.Page1.oPag.oSELVEAC_1_8.mHide()
    this.oPgFrm.Page1.oPag.oTIPOIN_1_18.visible=!this.oPgFrm.Page1.oPag.oTIPOIN_1_18.mHide()
    this.oPgFrm.Page1.oPag.oTIPOIN_1_19.visible=!this.oPgFrm.Page1.oPag.oTIPOIN_1_19.mHide()
    this.oPgFrm.Page1.oPag.oTIPOIN_1_20.visible=!this.oPgFrm.Page1.oPag.oTIPOIN_1_20.mHide()
    this.oPgFrm.Page1.oPag.oFLEVAS_1_23.visible=!this.oPgFrm.Page1.oPag.oFLEVAS_1_23.mHide()
    this.oPgFrm.Page1.oPag.oSELALL_1_53.visible=!this.oPgFrm.Page1.oPag.oSELALL_1_53.mHide()
    this.oPgFrm.Page2.oPag.oNOSBAN_2_10.visible=!this.oPgFrm.Page2.oPag.oNOSBAN_2_10.mHide()
    this.oPgFrm.Page2.oPag.oCODCOM_2_12.visible=!this.oPgFrm.Page2.oPag.oCODCOM_2_12.mHide()
    this.oPgFrm.Page2.oPag.oCODATT1_2_13.visible=!this.oPgFrm.Page2.oPag.oCODATT1_2_13.mHide()
    this.oPgFrm.Page2.oPag.oCODATT2_2_14.visible=!this.oPgFrm.Page2.oPag.oCODATT2_2_14.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_15.visible=!this.oPgFrm.Page2.oPag.oStr_2_15.mHide()
    this.oPgFrm.Page2.oPag.oDESCOM_2_16.visible=!this.oPgFrm.Page2.oPag.oDESCOM_2_16.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_17.visible=!this.oPgFrm.Page2.oPag.oStr_2_17.mHide()
    this.oPgFrm.Page2.oPag.oDESAT1_2_18.visible=!this.oPgFrm.Page2.oPag.oDESAT1_2_18.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_19.visible=!this.oPgFrm.Page2.oPag.oStr_2_19.mHide()
    this.oPgFrm.Page2.oPag.oDESAT2_2_20.visible=!this.oPgFrm.Page2.oPag.oDESAT2_2_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_64.visible=!this.oPgFrm.Page1.oPag.oStr_1_64.mHide()
    this.oPgFrm.Page1.oPag.oFLGNOT_1_74.visible=!this.oPgFrm.Page1.oPag.oFLGNOT_1_74.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_85.visible=!this.oPgFrm.Page1.oPag.oStr_1_85.mHide()
    this.oPgFrm.Page1.oPag.oCATDO3_1_89.visible=!this.oPgFrm.Page1.oPag.oCATDO3_1_89.mHide()
    this.oPgFrm.Page2.oPag.oNOSBAN_2_37.visible=!this.oPgFrm.Page2.oPag.oNOSBAN_2_37.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsve_sdv
    *--- Modifico Caption
    If cEvent=='Init'
       Do Case
       Case Type('This.oParentObject')='C' And This.oParentObject='V'
         If IsAhe() Or g_ACQU='S'
           This.cComment= Ah_MsgFormat("Ristampa documenti (vendite)")
         Else
           This.cComment= Ah_MsgFormat("Ristampa documenti")
         Endif
       Case Type('This.oParentObject')='C' And This.oParentObject='A'
         This.cComment = Ah_MsgFormat("Ristampa documenti (acquisti)")
       Case Type('This.oParentObject')='C' And This.oParentObject='O'
         This.cComment = Ah_MsgFormat("Ristampa ordini")
       Case Type('This.oParentObject')='C' And This.oParentObject='S'
         This.cComment = Ah_MsgFormat("Ristampa doc. da piano sped.")
       Case Type('This.oParentObject')='C' And This.oParentObject='T'
         This.cComment = Ah_MsgFormat("Ristampa documenti a terzisti")
       Case Type('This.oParentObject')='C' And This.oParentObject='L'
         This.cComment = Ah_MsgFormat("Ristampa documenti a fornitore")
       EndCase
       This.Caption = This.cComment
       *--- Resize maschera
       If Type('This.oParentObject')='C' And INLIST(This.oParentObject, 'T', 'S', 'L')
         This.w_ZoomTD.Visible=.F.
         Local oBtn
         oBtn = This.GetCtrl('S\<tampa')
         oBtn.Top = oBtn.Top - This.w_ZoomTD.Height
         oBtn = This.GetCtrl('\<Esci')
         oBtn.Top = oBtn.Top - This.w_ZoomTD.Height
         oBtn = This.GetCtrl('w_SELALL')
         oBtn = .null.
         This.Height = This.Height - This.w_ZoomTD.Height
       Endif
    EndIf
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomTD.Event(cEvent)
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("w_zoomtd row checked") or lower(cEvent)==lower("w_zoomtd row unchecked")
          .Calculate_EZAJIWWJGI()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TIPOIN
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPOIN)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPOIN))
          select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPOIN)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPOIN) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPOIN_1_18'),i_cWhere,'GSVE_ATD',"Causali documenti di vendita",'GSVE1SBD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPOIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPOIN)
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOIN = NVL(_Link_.TDTIPDOC,space(5))
      this.w_dt = NVL(_Link_.TDDESDOC,space(35))
      this.w_fcc = NVL(_Link_.TDFLINTE,space(1))
      this.w_stampa = NVL(_Link_.TDPRGSTA,0)
      this.w_catdoc = NVL(_Link_.TDCATDOC,space(2))
      this.w_TDFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOIN = space(5)
      endif
      this.w_dt = space(35)
      this.w_fcc = space(1)
      this.w_stampa = 0
      this.w_catdoc = space(2)
      this.w_TDFLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CICLO=.w_TDFLVEAC OR INLIST(.w_CICLO, 'T', 'S', 'L')) and (.w_CATEGO=.w_CATDOC OR EMPTY(.w_CATEGO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente")
        endif
        this.w_TIPOIN = space(5)
        this.w_dt = space(35)
        this.w_fcc = space(1)
        this.w_stampa = 0
        this.w_catdoc = space(2)
        this.w_TDFLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPOIN
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOR_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPOIN)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPOIN))
          select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPOIN)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPOIN) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPOIN_1_19'),i_cWhere,'GSOR_ATD',"Causali ordini",'GSVE1SBD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPOIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPOIN)
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOIN = NVL(_Link_.TDTIPDOC,space(5))
      this.w_dt = NVL(_Link_.TDDESDOC,space(35))
      this.w_fcc = NVL(_Link_.TDFLINTE,space(1))
      this.w_stampa = NVL(_Link_.TDPRGSTA,0)
      this.w_catdoc = NVL(_Link_.TDCATDOC,space(2))
      this.w_TDFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOIN = space(5)
      endif
      this.w_dt = space(35)
      this.w_fcc = space(1)
      this.w_stampa = 0
      this.w_catdoc = space(2)
      this.w_TDFLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CATDOC='OR' OR INLIST(.w_CICLO, 'T', 'S', 'L')) and (.w_CATEGO=.w_CATDOC OR EMPTY(.w_CATEGO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente")
        endif
        this.w_TIPOIN = space(5)
        this.w_dt = space(35)
        this.w_fcc = space(1)
        this.w_stampa = 0
        this.w_catdoc = space(2)
        this.w_TDFLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPOIN
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAC_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_TIPOIN)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_TIPOIN))
          select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPOIN)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPOIN) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oTIPOIN_1_20'),i_cWhere,'GSAC_ATD',"Causali documenti d'acquisto",'GSVE1SBD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_TIPOIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_TIPOIN)
            select TDTIPDOC,TDDESDOC,TDFLINTE,TDPRGSTA,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOIN = NVL(_Link_.TDTIPDOC,space(5))
      this.w_dt = NVL(_Link_.TDDESDOC,space(35))
      this.w_fcc = NVL(_Link_.TDFLINTE,space(1))
      this.w_stampa = NVL(_Link_.TDPRGSTA,0)
      this.w_catdoc = NVL(_Link_.TDCATDOC,space(2))
      this.w_TDFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOIN = space(5)
      endif
      this.w_dt = space(35)
      this.w_fcc = space(1)
      this.w_stampa = 0
      this.w_catdoc = space(2)
      this.w_TDFLVEAC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CICLO=.w_TDFLVEAC OR INLIST(.w_CICLO, 'T', 'S', 'L')) and (.w_CATEGO=.w_CATDOC OR EMPTY(.w_CATEGO))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento incongruente")
        endif
        this.w_TIPOIN = space(5)
        this.w_dt = space(35)
        this.w_fcc = space(1)
        this.w_stampa = 0
        this.w_catdoc = space(2)
        this.w_TDFLVEAC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=codese
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_codese) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_codese)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_azi);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_azi;
                     ,'ESCODESE',trim(this.w_codese))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_codese)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_codese) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'ocodese_1_22'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_azi<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_azi);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_codese)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_codese);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_azi);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_azi;
                       ,'ESCODESE',this.w_codese)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_codese = NVL(_Link_.ESCODESE,space(4))
      this.w_data1 = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_data2 = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_codese = space(4)
      endif
      this.w_data1 = ctod("  /  /  ")
      this.w_data2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_codese Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=clifor
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_clifor) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_clifor)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FC);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_FC;
                     ,'ANCODICE',trim(this.w_clifor))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_clifor)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_clifor)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FC);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_clifor)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_FC);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_clifor) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oclifor_1_32'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FC<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_FC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_clifor)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_clifor);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_FC;
                       ,'ANCODICE',this.w_clifor)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_clifor = NVL(_Link_.ANCODICE,space(15))
      this.w_DQ = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_clifor = space(15)
      endif
      this.w_DQ = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_clifor = space(15)
        this.w_DQ = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_clifor Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE_2_2'),i_cWhere,'GSAR_AGE',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LINGUA
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LINGUA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_LINGUA)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_LINGUA))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LINGUA)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LINGUA) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oLINGUA_2_4'),i_cWhere,'GSAR_ALG',"Lingue",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LINGUA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_LINGUA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_LINGUA)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LINGUA = NVL(_Link_.LUCODICE,space(3))
      this.w_DESLINGUA = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_LINGUA = space(3)
      endif
      this.w_DESLINGUA = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LINGUA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVET
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VETTORI_IDX,3]
    i_lTable = "VETTORI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2], .t., this.VETTORI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVT',True,'VETTORI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VTCODVET like "+cp_ToStrODBC(trim(this.w_CODVET)+"%");

          i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VTCODVET","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VTCODVET',trim(this.w_CODVET))
          select VTCODVET,VTDESVET;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VTCODVET into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVET)==trim(_Link_.VTCODVET) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStrODBC(trim(this.w_CODVET)+"%");

            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStr(trim(this.w_CODVET)+"%");

            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODVET) and !this.bDontReportError
            deferred_cp_zoom('VETTORI','*','VTCODVET',cp_AbsName(oSource.parent,'oCODVET_2_5'),i_cWhere,'GSAR_AVT',"Vettori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                     +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',oSource.xKey(1))
            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                   +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(this.w_CODVET);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',this.w_CODVET)
            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVET = NVL(_Link_.VTCODVET,space(5))
      this.w_DESVET = NVL(_Link_.VTDESVET,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODVET = space(5)
      endif
      this.w_DESVET = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])+'\'+cp_ToStr(_Link_.VTCODVET,1)
      cp_ShowWarn(i_cKey,this.VETTORI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODDES
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_CODDES)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_FC);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CLIFOR);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_FC;
                     ,'DDCODICE',this.w_CLIFOR;
                     ,'DDCODDES',trim(this.w_CODDES))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODDES)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODDES) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oCODDES_2_6'),i_cWhere,'',"Destinazioni diverse",'GSVE0KFD.DES_DIVE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FC<>oSource.xKey(1);
           .or. this.w_CLIFOR<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_FC);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_CLIFOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_CODDES);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_FC);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CLIFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_FC;
                       ,'DDCODICE',this.w_CLIFOR;
                       ,'DDCODDES',this.w_CODDES)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODDES = NVL(_Link_.DDCODDES,space(5))
      this.w_DESDES = NVL(_Link_.DDNOMDES,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODDES = space(5)
      endif
      this.w_DESDES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODZON
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_CODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_CODZON))
          select ZOCODZON,ZODESZON,ZODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oCODZON_2_7'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_CODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_CODZON)
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZON = NVL(_Link_.ZODESZON,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODZON = space(3)
      endif
      this.w_DESZON = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice zona inesistente oppure obsoleto")
        endif
        this.w_CODZON = space(3)
        this.w_DESZON = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCOM
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStrODBC(trim(this.w_CATCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStr(trim(this.w_CATCOM)+"%");

            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCATCOM_2_8'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCACM = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCOM = space(3)
      endif
      this.w_DESCACM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPAG
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_CODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_CODPAG))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_CODPAG)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_CODPAG))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_CODPAG)+"%");

            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oCODPAG_2_9'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_CODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_CODPAG)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_CODPAG = space(5)
        this.w_DESPAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOSBAN
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOSBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACC',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BANUMCOR like "+cp_ToStrODBC(trim(this.w_NOSBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BANUMCOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BANUMCOR',trim(this.w_NOSBAN))
          select BANUMCOR,BADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BANUMCOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOSBAN)==trim(_Link_.BANUMCOR) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_NOSBAN)+"%");

            i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_NOSBAN)+"%");

            select BANUMCOR,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NOSBAN) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BANUMCOR',cp_AbsName(oSource.parent,'oNOSBAN_2_10'),i_cWhere,'GSTE_ACC',"Conti banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BANUMCOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BANUMCOR',oSource.xKey(1))
            select BANUMCOR,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOSBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BANUMCOR,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BANUMCOR="+cp_ToStrODBC(this.w_NOSBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BANUMCOR',this.w_NOSBAN)
            select BANUMCOR,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOSBAN = NVL(_Link_.BANUMCOR,space(15))
      this.w_DESBAN = NVL(_Link_.BADESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_NOSBAN = space(15)
      endif
      this.w_DESBAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BANUMCOR,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOSBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN,CNDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_2_12'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN,CNDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN,CNDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CNDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODCOM = space(15)
        this.w_DESCOM = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT1
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT1)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT1))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT1)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_CODATT1)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_CODATT1)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODATT1) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT1_2_13'),i_cWhere,'GSPC_BZZ',"Elenco attivita",'GSPC_AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT1);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT1)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT1 = NVL(_Link_.ATCODATT,space(15))
      this.w_DESAT1 = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT1 = space(15)
      endif
      this.w_DESAT1 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Empty (.w_CODATT2) Or .w_CODATT1<=.w_CODATT2
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODATT1 = space(15)
        this.w_DESAT1 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT2
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT2)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

          i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODCOM,ATTIPATT,ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODCOM',this.w_CODCOM;
                     ,'ATTIPATT',this.w_TIPATT;
                     ,'ATCODATT',trim(this.w_CODATT2))
          select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODCOM,ATTIPATT,ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT2)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStrODBC(trim(this.w_CODATT2)+"%");
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);

            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ATDESCRI like "+cp_ToStr(trim(this.w_CODATT2)+"%");
                   +" and ATCODCOM="+cp_ToStr(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStr(this.w_TIPATT);

            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODATT2) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(oSource.parent,'oCODATT2_2_14'),i_cWhere,'GSPC_BZZ',"Elenco attivita",'GSPC_AAZ.ATTIVITA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODCOM<>oSource.xKey(1);
           .or. this.w_TIPATT<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(3));
                     +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                     +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',oSource.xKey(1);
                       ,'ATTIPATT',oSource.xKey(2);
                       ,'ATCODATT',oSource.xKey(3))
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT2);
                   +" and ATCODCOM="+cp_ToStrODBC(this.w_CODCOM);
                   +" and ATTIPATT="+cp_ToStrODBC(this.w_TIPATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODCOM',this.w_CODCOM;
                       ,'ATTIPATT',this.w_TIPATT;
                       ,'ATCODATT',this.w_CODATT2)
            select ATCODCOM,ATTIPATT,ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT2 = NVL(_Link_.ATCODATT,space(15))
      this.w_DESAT2 = NVL(_Link_.ATDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT2 = space(15)
      endif
      this.w_DESAT2 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Empty (.w_CODATT1) Or .w_CODATT1<=.w_CODATT2
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODATT2 = space(15)
        this.w_DESAT2 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODCOM,1)+'\'+cp_ToStr(_Link_.ATTIPATT,1)+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NOSBAN
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NOSBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_NOSBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_NOSBAN))
          select BACODBAN,BADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NOSBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_NOSBAN)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_NOSBAN)+"%");

            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NOSBAN) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oNOSBAN_2_37'),i_cWhere,'GSTE_ACB',"Conti banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NOSBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_NOSBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_NOSBAN)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NOSBAN = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBAN = NVL(_Link_.BADESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_NOSBAN = space(15)
      endif
      this.w_DESBAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NOSBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCATDO2_1_3.RadioValue()==this.w_CATDO2)
      this.oPgFrm.Page1.oPag.oCATDO2_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDO2_1_4.RadioValue()==this.w_CATDO2)
      this.oPgFrm.Page1.oPag.oCATDO2_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDO1_1_5.RadioValue()==this.w_CATDO1)
      this.oPgFrm.Page1.oPag.oCATDO1_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDO1_1_6.RadioValue()==this.w_CATDO1)
      this.oPgFrm.Page1.oPag.oCATDO1_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDO3_1_7.RadioValue()==this.w_CATDO3)
      this.oPgFrm.Page1.oPag.oCATDO3_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELVEAC_1_8.RadioValue()==this.w_SELVEAC)
      this.oPgFrm.Page1.oPag.oSELVEAC_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOIN_1_18.value==this.w_TIPOIN)
      this.oPgFrm.Page1.oPag.oTIPOIN_1_18.value=this.w_TIPOIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOIN_1_19.value==this.w_TIPOIN)
      this.oPgFrm.Page1.oPag.oTIPOIN_1_19.value=this.w_TIPOIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOIN_1_20.value==this.w_TIPOIN)
      this.oPgFrm.Page1.oPag.oTIPOIN_1_20.value=this.w_TIPOIN
    endif
    if not(this.oPgFrm.Page1.oPag.ocodese_1_22.value==this.w_codese)
      this.oPgFrm.Page1.oPag.ocodese_1_22.value=this.w_codese
    endif
    if not(this.oPgFrm.Page1.oPag.oFLEVAS_1_23.RadioValue()==this.w_FLEVAS)
      this.oPgFrm.Page1.oPag.oFLEVAS_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.odatain_1_26.value==this.w_datain)
      this.oPgFrm.Page1.oPag.odatain_1_26.value=this.w_datain
    endif
    if not(this.oPgFrm.Page1.oPag.odatafi_1_27.value==this.w_datafi)
      this.oPgFrm.Page1.oPag.odatafi_1_27.value=this.w_datafi
    endif
    if not(this.oPgFrm.Page1.oPag.onumini_1_28.value==this.w_numini)
      this.oPgFrm.Page1.oPag.onumini_1_28.value=this.w_numini
    endif
    if not(this.oPgFrm.Page1.oPag.oserie1_1_29.value==this.w_serie1)
      this.oPgFrm.Page1.oPag.oserie1_1_29.value=this.w_serie1
    endif
    if not(this.oPgFrm.Page1.oPag.onumfin_1_30.value==this.w_numfin)
      this.oPgFrm.Page1.oPag.onumfin_1_30.value=this.w_numfin
    endif
    if not(this.oPgFrm.Page1.oPag.oserie2_1_31.value==this.w_serie2)
      this.oPgFrm.Page1.oPag.oserie2_1_31.value=this.w_serie2
    endif
    if not(this.oPgFrm.Page1.oPag.oclifor_1_32.value==this.w_clifor)
      this.oPgFrm.Page1.oPag.oclifor_1_32.value=this.w_clifor
    endif
    if not(this.oPgFrm.Page1.oPag.oREPSEC_1_33.RadioValue()==this.w_REPSEC)
      this.oPgFrm.Page1.oPag.oREPSEC_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDQ_1_39.value==this.w_DQ)
      this.oPgFrm.Page1.oPag.oDQ_1_39.value=this.w_DQ
    endif
    if not(this.oPgFrm.Page1.oPag.oSELALL_1_53.RadioValue()==this.w_SELALL)
      this.oPgFrm.Page1.oPag.oSELALL_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFLPROV_2_1.RadioValue()==this.w_FLPROV)
      this.oPgFrm.Page2.oPag.oFLPROV_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGE_2_2.value==this.w_CODAGE)
      this.oPgFrm.Page2.oPag.oCODAGE_2_2.value=this.w_CODAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oLINGUA_2_4.value==this.w_LINGUA)
      this.oPgFrm.Page2.oPag.oLINGUA_2_4.value=this.w_LINGUA
    endif
    if not(this.oPgFrm.Page2.oPag.oCODVET_2_5.value==this.w_CODVET)
      this.oPgFrm.Page2.oPag.oCODVET_2_5.value=this.w_CODVET
    endif
    if not(this.oPgFrm.Page2.oPag.oCODDES_2_6.value==this.w_CODDES)
      this.oPgFrm.Page2.oPag.oCODDES_2_6.value=this.w_CODDES
    endif
    if not(this.oPgFrm.Page2.oPag.oCODZON_2_7.value==this.w_CODZON)
      this.oPgFrm.Page2.oPag.oCODZON_2_7.value=this.w_CODZON
    endif
    if not(this.oPgFrm.Page2.oPag.oCATCOM_2_8.value==this.w_CATCOM)
      this.oPgFrm.Page2.oPag.oCATCOM_2_8.value=this.w_CATCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODPAG_2_9.value==this.w_CODPAG)
      this.oPgFrm.Page2.oPag.oCODPAG_2_9.value=this.w_CODPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oNOSBAN_2_10.value==this.w_NOSBAN)
      this.oPgFrm.Page2.oPag.oNOSBAN_2_10.value=this.w_NOSBAN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE_2_11.value==this.w_DESAGE)
      this.oPgFrm.Page2.oPag.oDESAGE_2_11.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCOM_2_12.value==this.w_CODCOM)
      this.oPgFrm.Page2.oPag.oCODCOM_2_12.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODATT1_2_13.value==this.w_CODATT1)
      this.oPgFrm.Page2.oPag.oCODATT1_2_13.value=this.w_CODATT1
    endif
    if not(this.oPgFrm.Page2.oPag.oCODATT2_2_14.value==this.w_CODATT2)
      this.oPgFrm.Page2.oPag.oCODATT2_2_14.value=this.w_CODATT2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOM_2_16.value==this.w_DESCOM)
      this.oPgFrm.Page2.oPag.oDESCOM_2_16.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAT1_2_18.value==this.w_DESAT1)
      this.oPgFrm.Page2.oPag.oDESAT1_2_18.value=this.w_DESAT1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAT2_2_20.value==this.w_DESAT2)
      this.oPgFrm.Page2.oPag.oDESAT2_2_20.value=this.w_DESAT2
    endif
    if not(this.oPgFrm.Page1.oPag.odt_1_61.value==this.w_dt)
      this.oPgFrm.Page1.oPag.odt_1_61.value=this.w_dt
    endif
    if not(this.oPgFrm.Page2.oPag.oDESLINGUA_2_23.value==this.w_DESLINGUA)
      this.oPgFrm.Page2.oPag.oDESLINGUA_2_23.value=this.w_DESLINGUA
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGNOT_1_74.RadioValue()==this.w_FLGNOT)
      this.oPgFrm.Page1.oPag.oFLGNOT_1_74.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVET_2_26.value==this.w_DESVET)
      this.oPgFrm.Page2.oPag.oDESVET_2_26.value=this.w_DESVET
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDES_2_27.value==this.w_DESDES)
      this.oPgFrm.Page2.oPag.oDESDES_2_27.value=this.w_DESDES
    endif
    if not(this.oPgFrm.Page2.oPag.oDESZON_2_28.value==this.w_DESZON)
      this.oPgFrm.Page2.oPag.oDESZON_2_28.value=this.w_DESZON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPAG_2_32.value==this.w_DESPAG)
      this.oPgFrm.Page2.oPag.oDESPAG_2_32.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCACM_2_33.value==this.w_DESCACM)
      this.oPgFrm.Page2.oPag.oDESCACM_2_33.value=this.w_DESCACM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESBAN_2_36.value==this.w_DESBAN)
      this.oPgFrm.Page2.oPag.oDESBAN_2_36.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oCATDO3_1_89.RadioValue()==this.w_CATDO3)
      this.oPgFrm.Page1.oPag.oCATDO3_1_89.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNOSBAN_2_37.value==this.w_NOSBAN)
      this.oPgFrm.Page2.oPag.oNOSBAN_2_37.value=this.w_NOSBAN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_CICLO=.w_TDFLVEAC OR INLIST(.w_CICLO, 'T', 'S', 'L')) and (.w_CATEGO=.w_CATDOC OR EMPTY(.w_CATEGO)))  and not(.w_CICLO<>'V' AND .w_CICLO <> 'S')  and (.w_EDITCAU)  and not(empty(.w_TIPOIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPOIN_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente")
          case   not((.w_CATDOC='OR' OR INLIST(.w_CICLO, 'T', 'S', 'L')) and (.w_CATEGO=.w_CATDOC OR EMPTY(.w_CATEGO)))  and not(.w_CICLO<>'O' AND .w_CICLO<>'L' AND .w_CICLO<>'T')  and (.w_EDITCAU)  and not(empty(.w_TIPOIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPOIN_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente")
          case   not((.w_CICLO=.w_TDFLVEAC OR INLIST(.w_CICLO, 'T', 'S', 'L')) and (.w_CATEGO=.w_CATDOC OR EMPTY(.w_CATEGO)))  and not(.w_CICLO<>'A')  and (.w_EDITCAU)  and not(empty(.w_TIPOIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTIPOIN_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento incongruente")
          case   (empty(.w_codese))  and (.w_EDIT)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.ocodese_1_22.SetFocus()
            i_bnoObbl = !empty(.w_codese)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_datain)) or not((empty(.w_DATAFI)) OR  (.w_DATAIN<=.w_DATAFI)))  and (.w_EDIT)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odatain_1_26.SetFocus()
            i_bnoObbl = !empty(.w_datain)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_datafi)) or not((empty(.w_DATAin)) OR  (.w_DATAIN<=.w_DATAFI)))  and (.w_EDIT)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odatafi_1_27.SetFocus()
            i_bnoObbl = !empty(.w_datafi)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.onumini_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oserie1_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggiore della serie finale")
          case   not(.w_numini<=.w_numfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.onumfin_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oserie2_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggiore della serie finale")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_EDITINT)  and not(empty(.w_clifor))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oclifor_1_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODZON))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODZON_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice zona inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODPAG))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODPAG_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
          case   not(.w_DATOBSO>.w_OBTEST OR EMPTY(.w_DATOBSO))  and not(!isAhe())  and (g_COMM='S' Or  g_COMM='N' And g_PERCAN='S')  and not(empty(.w_CODCOM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODCOM_2_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(Empty (.w_CODATT2) Or .w_CODATT1<=.w_CODATT2)  and not(!isAhe())  and (g_COMM='S' And !Empty(.w_CODCOM))  and not(empty(.w_CODATT1))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODATT1_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(Empty (.w_CODATT1) Or .w_CODATT1<=.w_CODATT2)  and not(!isAhe())  and (g_COMM='S' And !Empty(.w_CODCOM))  and not(empty(.w_CODATT2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODATT2_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CATDO2 = this.w_CATDO2
    this.o_CATDO1 = this.w_CATDO1
    this.o_CATDO3 = this.w_CATDO3
    this.o_SELVEAC = this.w_SELVEAC
    this.o_TIPOVAO = this.w_TIPOVAO
    this.o_fc = this.w_fc
    this.o_TIPOIN = this.w_TIPOIN
    this.o_codese = this.w_codese
    this.o_datain = this.w_datain
    this.o_clifor = this.w_clifor
    this.o_SELALL = this.w_SELALL
    this.o_CODCOM = this.w_CODCOM
    return

enddefine

* --- Define pages as container
define class tgsve_sdvPag1 as StdContainer
  Width  = 531
  height = 512
  stdWidth  = 531
  stdheight = 512
  resizeXpos=249
  resizeYpos=296
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCATDO2_1_3 as StdCombo with uid="UPJNZIKQML",rtseq=3,rtrep=.f.,left=105,top=12,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 47141670;
    , cFormVar="w_CATDO2",RowSource=""+"Nessuna selezione,"+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ricevute fiscali,"+"DDT a fornitore,"+"Carichi a fornitore,"+"Tutti i documenti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO2_1_3.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    iif(this.value =6,'RF',;
    iif(this.value =7,'DF',;
    iif(this.value =8,'IF',;
    iif(this.value =9,'TT',;
    space(2)))))))))))
  endfunc
  func oCATDO2_1_3.GetRadio()
    this.Parent.oContained.w_CATDO2 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO2_1_3.SetRadio()
    this.Parent.oContained.w_CATDO2=trim(this.Parent.oContained.w_CATDO2)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO2=='XX',1,;
      iif(this.Parent.oContained.w_CATDO2=='DI',2,;
      iif(this.Parent.oContained.w_CATDO2=='DT',3,;
      iif(this.Parent.oContained.w_CATDO2=='FA',4,;
      iif(this.Parent.oContained.w_CATDO2=='NC',5,;
      iif(this.Parent.oContained.w_CATDO2=='RF',6,;
      iif(this.Parent.oContained.w_CATDO2=='DF',7,;
      iif(this.Parent.oContained.w_CATDO2=='IF',8,;
      iif(this.Parent.oContained.w_CATDO2=='TT',9,;
      0)))))))))
  endfunc

  func oCATDO2_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(.w_ACQU='S' AND .w_CICLO='V' OR .w_CICLO='A' OR ISALT()))
    endwith
   endif
  endfunc

  func oCATDO2_1_3.mHide()
    with this.Parent.oContained
      return ((.w_ACQU='S' AND .w_CICLO='V' ) OR .w_CICLO='A' OR ISALT() OR .w_HIDE='S')
    endwith
  endfunc


  add object oCATDO2_1_4 as StdCombo with uid="KLSHGSXUVJ",rtseq=4,rtrep=.f.,left=105,top=12,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 47141670;
    , cFormVar="w_CATDO2",RowSource=""+"Nessuna selezione,"+"Documenti interni,"+"Fatture,"+"Note di credito,"+"Tutti i documenti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO2_1_4.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'FA',;
    iif(this.value =4,'NC',;
    iif(this.value =5,'TT',;
    space(2)))))))
  endfunc
  func oCATDO2_1_4.GetRadio()
    this.Parent.oContained.w_CATDO2 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO2_1_4.SetRadio()
    this.Parent.oContained.w_CATDO2=trim(this.Parent.oContained.w_CATDO2)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO2=='XX',1,;
      iif(this.Parent.oContained.w_CATDO2=='DI',2,;
      iif(this.Parent.oContained.w_CATDO2=='FA',3,;
      iif(this.Parent.oContained.w_CATDO2=='NC',4,;
      iif(this.Parent.oContained.w_CATDO2=='TT',5,;
      0)))))
  endfunc

  func oCATDO2_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oCATDO2_1_4.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR  .w_HIDE='S')
    endwith
  endfunc


  add object oCATDO1_1_5 as StdCombo with uid="QDJUHAEQBR",rtseq=5,rtrep=.f.,left=105,top=12,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 30364454;
    , cFormVar="w_CATDO1",RowSource=""+"Nessuna selezione,"+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ricevute fiscali,"+"Tutti i documenti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO1_1_5.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    iif(this.value =6,'RF',;
    iif(this.value =7,'TT',;
    space(2)))))))))
  endfunc
  func oCATDO1_1_5.GetRadio()
    this.Parent.oContained.w_CATDO1 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO1_1_5.SetRadio()
    this.Parent.oContained.w_CATDO1=trim(this.Parent.oContained.w_CATDO1)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO1=='XX',1,;
      iif(this.Parent.oContained.w_CATDO1=='DI',2,;
      iif(this.Parent.oContained.w_CATDO1=='DT',3,;
      iif(this.Parent.oContained.w_CATDO1=='FA',4,;
      iif(this.Parent.oContained.w_CATDO1=='NC',5,;
      iif(this.Parent.oContained.w_CATDO1=='RF',6,;
      iif(this.Parent.oContained.w_CATDO1=='TT',7,;
      0)))))))
  endfunc

  func oCATDO1_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ACQU='S')
    endwith
   endif
  endfunc

  func oCATDO1_1_5.mHide()
    with this.Parent.oContained
      return ((.w_ACQU<>'S' AND .w_CICLO='V' ) OR .w_CICLO='A' OR .w_HIDE='S' or  isAhe() )
    endwith
  endfunc


  add object oCATDO1_1_6 as StdCombo with uid="MBDKNKUFGL",rtseq=6,rtrep=.f.,left=105,top=12,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 30364454;
    , cFormVar="w_CATDO1",RowSource=""+"Nessuna selezione,"+"Documenti interni,"+"Ordini,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ordini previsionali,"+"Tutti i documenti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO1_1_6.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'OR',;
    iif(this.value =4,'DT',;
    iif(this.value =5,'FA',;
    iif(this.value =6,'NC',;
    iif(this.value =7,'OP',;
    iif(this.value =8,'TT',;
    space(2))))))))))
  endfunc
  func oCATDO1_1_6.GetRadio()
    this.Parent.oContained.w_CATDO1 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO1_1_6.SetRadio()
    this.Parent.oContained.w_CATDO1=trim(this.Parent.oContained.w_CATDO1)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO1=='XX',1,;
      iif(this.Parent.oContained.w_CATDO1=='DI',2,;
      iif(this.Parent.oContained.w_CATDO1=='OR',3,;
      iif(this.Parent.oContained.w_CATDO1=='DT',4,;
      iif(this.Parent.oContained.w_CATDO1=='FA',5,;
      iif(this.Parent.oContained.w_CATDO1=='NC',6,;
      iif(this.Parent.oContained.w_CATDO1=='OP',7,;
      iif(this.Parent.oContained.w_CATDO1=='TT',8,;
      0))))))))
  endfunc

  func oCATDO1_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ACQU='S')
    endwith
   endif
  endfunc

  func oCATDO1_1_6.mHide()
    with this.Parent.oContained
      return (.w_ACQU<>'S' AND .w_CICLO='V' OR .w_CICLO='A' OR .w_HIDE='S' or !isAhe())
    endwith
  endfunc


  add object oCATDO3_1_7 as StdCombo with uid="HUPPWPBBJO",rtseq=7,rtrep=.f.,left=105,top=12,width=130,height=21;
    , ToolTipText = "Categoria di appartenenza del documento selezionata";
    , HelpContextID = 63918886;
    , cFormVar="w_CATDO3",RowSource=""+"Nessuna selezione,"+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Tutti i documenti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO3_1_7.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    iif(this.value =6,'TT',;
    space(2))))))))
  endfunc
  func oCATDO3_1_7.GetRadio()
    this.Parent.oContained.w_CATDO3 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO3_1_7.SetRadio()
    this.Parent.oContained.w_CATDO3=trim(this.Parent.oContained.w_CATDO3)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO3=='XX',1,;
      iif(this.Parent.oContained.w_CATDO3=='DI',2,;
      iif(this.Parent.oContained.w_CATDO3=='DT',3,;
      iif(this.Parent.oContained.w_CATDO3=='FA',4,;
      iif(this.Parent.oContained.w_CATDO3=='NC',5,;
      iif(this.Parent.oContained.w_CATDO3=='TT',6,;
      0))))))
  endfunc

  func oCATDO3_1_7.mHide()
    with this.Parent.oContained
      return (.w_CICLO='V' OR .w_HIDE='S' OR IsAhe())
    endwith
  endfunc


  add object oSELVEAC_1_8 as StdCombo with uid="VSJLNZYUDI",rtseq=8,rtrep=.f.,left=105,top=12,width=130,height=21;
    , ToolTipText = "Categoria di appartenenza del documento selezionata";
    , HelpContextID = 21026854;
    , cFormVar="w_SELVEAC",RowSource=""+"Impegni da cliente,"+"Ordini a fornitore,"+"Nessuna selezione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELVEAC_1_8.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    iif(this.value =3,'O',;
    space(1)))))
  endfunc
  func oSELVEAC_1_8.GetRadio()
    this.Parent.oContained.w_SELVEAC = this.RadioValue()
    return .t.
  endfunc

  func oSELVEAC_1_8.SetRadio()
    this.Parent.oContained.w_SELVEAC=trim(this.Parent.oContained.w_SELVEAC)
    this.value = ;
      iif(this.Parent.oContained.w_SELVEAC=='V',1,;
      iif(this.Parent.oContained.w_SELVEAC=='A',2,;
      iif(this.Parent.oContained.w_SELVEAC=='O',3,;
      0)))
  endfunc

  func oSELVEAC_1_8.mHide()
    with this.Parent.oContained
      return (.w_CICLO<>'O')
    endwith
  endfunc

  add object oTIPOIN_1_18 as StdField with uid="KQVRLAIRLD",rtseq=18,rtrep=.f.,;
    cFormVar = "w_TIPOIN", cQueryName = "TIPOIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente",;
    ToolTipText = "Tipo documento selezionato",;
    HelpContextID = 242883638,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=41, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPOIN"

  func oTIPOIN_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITCAU)
    endwith
   endif
  endfunc

  func oTIPOIN_1_18.mHide()
    with this.Parent.oContained
      return (.w_CICLO<>'V' AND .w_CICLO <> 'S')
    endwith
  endfunc

  func oTIPOIN_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPOIN_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPOIN_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPOIN_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti di vendita",'GSVE1SBD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPOIN_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPOIN
     i_obj.ecpSave()
  endproc

  add object oTIPOIN_1_19 as StdField with uid="KSFLDDVHTA",rtseq=19,rtrep=.f.,;
    cFormVar = "w_TIPOIN", cQueryName = "TIPOIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente",;
    ToolTipText = "Tipo documento selezionato",;
    HelpContextID = 242883638,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=41, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSOR_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPOIN"

  func oTIPOIN_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITCAU)
    endwith
   endif
  endfunc

  func oTIPOIN_1_19.mHide()
    with this.Parent.oContained
      return (.w_CICLO<>'O' AND .w_CICLO<>'L' AND .w_CICLO<>'T')
    endwith
  endfunc

  func oTIPOIN_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPOIN_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPOIN_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPOIN_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOR_ATD',"Causali ordini",'GSVE1SBD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPOIN_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSOR_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPOIN
     i_obj.ecpSave()
  endproc

  add object oTIPOIN_1_20 as StdField with uid="HSFKIVAPVY",rtseq=20,rtrep=.f.,;
    cFormVar = "w_TIPOIN", cQueryName = "TIPOIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento incongruente",;
    ToolTipText = "Tipo documento selezionato",;
    HelpContextID = 242883638,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=104, Top=41, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAC_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_TIPOIN"

  func oTIPOIN_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITCAU)
    endwith
   endif
  endfunc

  func oTIPOIN_1_20.mHide()
    with this.Parent.oContained
      return (.w_CICLO<>'A')
    endwith
  endfunc

  func oTIPOIN_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oTIPOIN_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPOIN_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oTIPOIN_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAC_ATD',"Causali documenti d'acquisto",'GSVE1SBD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oTIPOIN_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAC_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_TIPOIN
     i_obj.ecpSave()
  endproc

  add object ocodese_1_22 as StdField with uid="TDNJQIZCMZ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_codese", cQueryName = "codese",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio selezionato",;
    HelpContextID = 137462566,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=104, Top=70, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_azi", oKey_2_1="ESCODESE", oKey_2_2="this.w_codese"

  func ocodese_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDIT)
    endwith
   endif
  endfunc

  func ocodese_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc ocodese_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ocodese_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_azi)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_azi)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'ocodese_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oFLEVAS_1_23 as StdCheck with uid="AFBKADTJAT",rtseq=23,rtrep=.f.,left=296, top=69, caption="Righe evase",;
    ToolTipText = "Se attivo: stampa anche la parte gi� evasa sui documenti, altrimenti solo le qta/valori ancora da evadere",;
    HelpContextID = 50359894,;
    cFormVar="w_FLEVAS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLEVAS_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLEVAS_1_23.GetRadio()
    this.Parent.oContained.w_FLEVAS = this.RadioValue()
    return .t.
  endfunc

  func oFLEVAS_1_23.SetRadio()
    this.Parent.oContained.w_FLEVAS=trim(this.Parent.oContained.w_FLEVAS)
    this.value = ;
      iif(this.Parent.oContained.w_FLEVAS=='S',1,;
      0)
  endfunc

  func oFLEVAS_1_23.mHide()
    with this.Parent.oContained
      return (.w_TIPOVAO<>'O' Or IsAhe())
    endwith
  endfunc

  add object odatain_1_26 as StdField with uid="TLSMGPEYWG",rtseq=26,rtrep=.f.,;
    cFormVar = "w_datain", cQueryName = "datain",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento iniziale selezionata",;
    HelpContextID = 9336118,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=104, Top=98

  func odatain_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDIT)
    endwith
   endif
  endfunc

  func odatain_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_DATAFI)) OR  (.w_DATAIN<=.w_DATAFI))
    endwith
    return bRes
  endfunc

  add object odatafi_1_27 as StdField with uid="SJPNDJOWBM",rtseq=27,rtrep=.f.,;
    cFormVar = "w_datafi", cQueryName = "datafi",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data documento finale selezionata",;
    HelpContextID = 190739766,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=104, Top=125

  func odatafi_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDIT)
    endwith
   endif
  endfunc

  func odatafi_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_DATAin)) OR  (.w_DATAIN<=.w_DATAFI))
    endwith
    return bRes
  endfunc

  add object onumini_1_28 as StdField with uid="CGTAYVCLKJ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_numini", cQueryName = "numini",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento iniziale selezionato",;
    HelpContextID = 199629270,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=294, Top=98, cSayPict='IIF(IsAhe(), "9999999999","999999999999999")', cGetPict='IIF(IsAhe(), "9999999999","999999999999999")', nMaxValue = 999999999999999

  func onumini_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc

  add object oserie1_1_29 as StdField with uid="IKCHOCFECU",rtseq=29,rtrep=.f.,;
    cFormVar = "w_serie1", cQueryName = "serie1",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggiore della serie finale",;
    ToolTipText = "Serie del documento iniziale selezionato",;
    HelpContextID = 55990822,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=437, Top=98, cSayPict='IIF(!IsAhe(), "!!!!!!!!!!", "!!")', cGetPict='IIF(!IsAhe(), "!!!!!!!!!!", "!!")', InputMask=replicate('X',10)

  func oserie1_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_SERIE2)) OR  (.w_SERIE1<=.w_SERIE2))
    endwith
    return bRes
  endfunc

  add object onumfin_1_30 as StdField with uid="PPVPLBFEDU",rtseq=30,rtrep=.f.,;
    cFormVar = "w_numfin", cQueryName = "numfin",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento finale selezionato",;
    HelpContextID = 9640406,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=294, Top=125, cSayPict='IIF(IsAhe(), "9999999999","999999999999999")', cGetPict='IIF(IsAhe(), "9999999999","999999999999999")', nMaxValue = 999999999999999

  func onumfin_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numini<=.w_numfin)
    endwith
    return bRes
  endfunc

  add object oserie2_1_31 as StdField with uid="SNGKDXOYPG",rtseq=31,rtrep=.f.,;
    cFormVar = "w_serie2", cQueryName = "serie2",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggiore della serie finale",;
    ToolTipText = "Serie del documento finale selezionato",;
    HelpContextID = 72768038,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=437, Top=125, cSayPict='IIF(!IsAhe(), "!!!!!!!!!!", "!!")', cGetPict='IIF(!IsAhe(), "!!!!!!!!!!", "!!")', InputMask=replicate('X',10)

  func oserie2_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_serie2>=.w_serie1) or (empty(.w_serie1)))
    endwith
    return bRes
  endfunc

  add object oclifor_1_32 as StdField with uid="PVMPOOOOIB",rtseq=32,rtrep=.f.,;
    cFormVar = "w_clifor", cQueryName = "clifor",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Eventuale codice intestatario di selezione",;
    HelpContextID = 83021862,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=104, Top=152, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_FC", oKey_2_1="ANCODICE", oKey_2_2="this.w_clifor"

  func oclifor_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EDITINT)
    endwith
   endif
  endfunc

  func oclifor_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
      if .not. empty(.w_CODDES)
        bRes2=.link_2_6('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oclifor_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oclifor_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_FC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_FC)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oclifor_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc
  proc oclifor_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_FC
     i_obj.w_ANCODICE=this.parent.oContained.w_clifor
     i_obj.ecpSave()
  endproc


  add object oREPSEC_1_33 as StdCombo with uid="ILFBHAZQOE",rtseq=33,rtrep=.f.,left=104,top=178,width=170,height=21;
    , ToolTipText = "Stampa report secondari";
    , HelpContextID = 54401046;
    , cFormVar="w_REPSEC",RowSource=""+"Tutti i report secondari,"+"Nessun report secondario,"+"Escludi report opzionali,"+"Solo i report secondari", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oREPSEC_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'C',;
    iif(this.value =4,'2',;
    space(1))))))
  endfunc
  func oREPSEC_1_33.GetRadio()
    this.Parent.oContained.w_REPSEC = this.RadioValue()
    return .t.
  endfunc

  func oREPSEC_1_33.SetRadio()
    this.Parent.oContained.w_REPSEC=trim(this.Parent.oContained.w_REPSEC)
    this.value = ;
      iif(this.Parent.oContained.w_REPSEC=='S',1,;
      iif(this.Parent.oContained.w_REPSEC=='N',2,;
      iif(this.Parent.oContained.w_REPSEC=='C',3,;
      iif(this.Parent.oContained.w_REPSEC=='2',4,;
      0))))
  endfunc

  add object oDQ_1_39 as StdField with uid="JUPRVCCYCI",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DQ", cQueryName = "DQ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 74046666,;
   bGlobalFont=.t.,;
    Height=21, Width=277, Left=243, Top=152, InputMask=replicate('X',40)


  add object ZoomTD as cp_szoombox with uid="JXTVQZELFB",left=5, top=211, width=518,height=227,;
    caption='Object',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.t.,cMenuFile="",cTable="TIP_DOCU",cZoomOnZoom="",cZoomFile="GSVE_SDV",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 103929062

  add object oSELALL_1_53 as StdRadio with uid="ASHEAQLXZM",rtseq=43,rtrep=.f.,left=5, top=439, width=161,height=40;
    , cFormVar="w_SELALL", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELALL_1_53.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 211540006
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 211540006
      this.Buttons(2).Top=19
      this.SetAll("Width",159)
      this.SetAll("Height",21)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELALL_1_53.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oSELALL_1_53.GetRadio()
    this.Parent.oContained.w_SELALL = this.RadioValue()
    return .t.
  endfunc

  func oSELALL_1_53.SetRadio()
    this.Parent.oContained.w_SELALL=trim(this.Parent.oContained.w_SELALL)
    this.value = ;
      iif(this.Parent.oContained.w_SELALL=='S',1,;
      iif(this.Parent.oContained.w_SELALL=='N',2,;
      0))
  endfunc

  func oSELALL_1_53.mHide()
    with this.Parent.oContained
      return (INLIST(.oParentObject, 'T', 'S'))
    endwith
  endfunc


  add object oBtn_1_59 as StdButton with uid="MBXAIGGBZE",left=424, top=458, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la stampa";
    , HelpContextID = 74048026;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_59.Click()
      with this.Parent.oContained
        GSVE_BRD(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_59.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_codese))
      endwith
    endif
  endfunc


  add object oBtn_1_60 as StdButton with uid="ISDICPRYHO",left=475, top=458, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 248651782;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_60.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object odt_1_61 as StdField with uid="VKEZJASLEX",rtseq=63,rtrep=.f.,;
    cFormVar = "w_dt", cQueryName = "dt",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 74037194,;
   bGlobalFont=.t.,;
    Height=21, Width=347, Left=173, Top=41, InputMask=replicate('X',35)

  add object oFLGNOT_1_74 as StdCheck with uid="XFYXEROWJX",rtseq=75,rtrep=.f.,left=296, top=178, caption="Includi note",;
    ToolTipText = "Se attivo: include note del documento",;
    HelpContextID = 81301078,;
    cFormVar="w_FLGNOT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGNOT_1_74.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLGNOT_1_74.GetRadio()
    this.Parent.oContained.w_FLGNOT = this.RadioValue()
    return .t.
  endfunc

  func oFLGNOT_1_74.SetRadio()
    this.Parent.oContained.w_FLGNOT=trim(this.Parent.oContained.w_FLGNOT)
    this.value = ;
      iif(this.Parent.oContained.w_FLGNOT=='S',1,;
      0)
  endfunc

  func oFLGNOT_1_74.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oCATDO3_1_89 as StdCombo with uid="WNIZIHXNUH",rtseq=95,rtrep=.f.,left=105,top=12,width=130,height=21;
    , ToolTipText = "Categoria di appartenenza del documento selezionata";
    , HelpContextID = 63918886;
    , cFormVar="w_CATDO3",RowSource=""+"Nessuna selezione,"+"Documenti interni,"+"Ordini,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ordini previsionali,"+"Tutti i documenti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATDO3_1_89.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'OR',;
    iif(this.value =4,'DT',;
    iif(this.value =5,'FA',;
    iif(this.value =6,'NC',;
    iif(this.value =7,'OP',;
    iif(this.value =8,'TT',;
    space(2))))))))))
  endfunc
  func oCATDO3_1_89.GetRadio()
    this.Parent.oContained.w_CATDO3 = this.RadioValue()
    return .t.
  endfunc

  func oCATDO3_1_89.SetRadio()
    this.Parent.oContained.w_CATDO3=trim(this.Parent.oContained.w_CATDO3)
    this.value = ;
      iif(this.Parent.oContained.w_CATDO3=='XX',1,;
      iif(this.Parent.oContained.w_CATDO3=='DI',2,;
      iif(this.Parent.oContained.w_CATDO3=='OR',3,;
      iif(this.Parent.oContained.w_CATDO3=='DT',4,;
      iif(this.Parent.oContained.w_CATDO3=='FA',5,;
      iif(this.Parent.oContained.w_CATDO3=='NC',6,;
      iif(this.Parent.oContained.w_CATDO3=='OP',7,;
      iif(this.Parent.oContained.w_CATDO3=='TT',8,;
      0))))))))
  endfunc

  func oCATDO3_1_89.mHide()
    with this.Parent.oContained
      return (.w_CICLO='V' OR .w_HIDE='S' or !IsAhe())
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="RWHNHUCUAO",Visible=.t., Left=6, Top=98,;
    Alignment=1, Width=95, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="GFGSYQKGMB",Visible=.t., Left=6, Top=125,;
    Alignment=1, Width=95, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="JIPRXJTPCJ",Visible=.t., Left=207, Top=98,;
    Alignment=1, Width=85, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="TVMPXBRQTI",Visible=.t., Left=207, Top=125,;
    Alignment=1, Width=85, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="WEXYCECXGJ",Visible=.t., Left=6, Top=70,;
    Alignment=1, Width=95, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="SOIMYBQITY",Visible=.t., Left=6, Top=152,;
    Alignment=1, Width=95, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="RGJNSAKFNJ",Visible=.t., Left=418, Top=98,;
    Alignment=2, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="TQXDLWGEQQ",Visible=.t., Left=418, Top=125,;
    Alignment=2, Width=13, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="FVKQVLIYBJ",Visible=.t., Left=8, Top=178,;
    Alignment=1, Width=93, Height=18,;
    Caption="Stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="WVRVCARJRJ",Visible=.t., Left=7, Top=41,;
    Alignment=1, Width=94, Height=15,;
    Caption="Tipo documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="TXCNCSMVLA",Visible=.t., Left=542, Top=323,;
    Alignment=0, Width=57, Height=18,;
    Caption="Enterprise"  ;
  , bGlobalFont=.t.

  func oStr_1_64.mHide()
    with this.Parent.oContained
      return (.t.)
    endwith
  endfunc

  add object oStr_1_85 as StdString with uid="VSPPXMPCOS",Visible=.t., Left=9, Top=13,;
    Alignment=1, Width=92, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  func oStr_1_85.mHide()
    with this.Parent.oContained
      return (.w_DASPED)
    endwith
  endfunc
enddefine
define class tgsve_sdvPag2 as StdContainer
  Width  = 531
  height = 512
  stdWidth  = 531
  stdheight = 512
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFLPROV_2_1 as StdCombo with uid="MCAUYSEWPX",rtseq=47,rtrep=.f.,left=104,top=35,width=164,height=21;
    , HelpContextID = 115154518;
    , cFormVar="w_FLPROV",RowSource=""+"Provvisori,"+"Confermati,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFLPROV_2_1.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLPROV_2_1.GetRadio()
    this.Parent.oContained.w_FLPROV = this.RadioValue()
    return .t.
  endfunc

  func oFLPROV_2_1.SetRadio()
    this.Parent.oContained.w_FLPROV=trim(this.Parent.oContained.w_FLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_FLPROV=='S',1,;
      iif(this.Parent.oContained.w_FLPROV=='N',2,;
      iif(this.Parent.oContained.w_FLPROV=='T',3,;
      0)))
  endfunc

  add object oCODAGE_2_2 as StdField with uid="EKORLTTUHX",rtseq=48,rtrep=.f.,;
    cFormVar = "w_CODAGE", cQueryName = "CODAGE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente di selezione",;
    HelpContextID = 88826150,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=102, Top=65, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE"

  func oCODAGE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Elenco agenti",'',this.parent.oContained
  endproc
  proc oCODAGE_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_CODAGE
     i_obj.ecpSave()
  endproc

  add object oLINGUA_2_4 as StdField with uid="WWDQTNXXKR",rtseq=49,rtrep=.f.,;
    cFormVar = "w_LINGUA", cQueryName = "LINGUA",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o non associato al tipo documento",;
    ToolTipText = "Lingua dell'intestatario",;
    HelpContextID = 36830134,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=102, Top=95, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_LINGUA"

  func oLINGUA_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oLINGUA_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLINGUA_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oLINGUA_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"Lingue",'',this.parent.oContained
  endproc
  proc oLINGUA_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_LINGUA
     i_obj.ecpSave()
  endproc

  add object oCODVET_2_5 as StdField with uid="OJISALVLMV",rtseq=50,rtrep=.f.,;
    cFormVar = "w_CODVET", cQueryName = "CODVET",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del vettore di selezione",;
    HelpContextID = 71328038,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=102, Top=124, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VETTORI", cZoomOnZoom="GSAR_AVT", oKey_1_1="VTCODVET", oKey_1_2="this.w_CODVET"

  func oCODVET_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVET_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVET_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VETTORI','*','VTCODVET',cp_AbsName(this.parent,'oCODVET_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVT',"Vettori",'',this.parent.oContained
  endproc
  proc oCODVET_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VTCODVET=this.parent.oContained.w_CODVET
     i_obj.ecpSave()
  endproc

  add object oCODDES_2_6 as StdField with uid="RNXWONQJXX",rtseq=51,rtrep=.f.,;
    cFormVar = "w_CODDES", cQueryName = "CODDES",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale destinazione particolare di selezione (vuoto = no selezione)",;
    HelpContextID = 53371174,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=102, Top=153, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_FC", oKey_2_1="DDCODICE", oKey_2_2="this.w_CLIFOR", oKey_3_1="DDCODDES", oKey_3_2="this.w_CODDES"

  func oCODDES_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CLIFOR))
    endwith
   endif
  endfunc

  proc oCODDES_2_6.mAfter
    with this.Parent.oContained
      .w_CODDES=chkdesdiv(.w_CODDES, .w_FC, .w_CLIFOR)
    endwith
  endproc

  func oCODDES_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODDES_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODDES_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_FC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CLIFOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_FC)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_CLIFOR)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oCODDES_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Destinazioni diverse",'GSVE0KFD.DES_DIVE_VZM',this.parent.oContained
  endproc

  add object oCODZON_2_7 as StdField with uid="OGVYCYWNEX",rtseq=52,rtrep=.f.,;
    cFormVar = "w_CODZON", cQueryName = "CODZON",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente oppure obsoleto",;
    ToolTipText = "Codice zona di eventuale selezione (vuoto = no selezione)",;
    HelpContextID = 249848102,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=102, Top=182, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_CODZON"

  func oCODZON_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODZON_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODZON_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oCODZON_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oCODZON_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_CODZON
     i_obj.ecpSave()
  endproc

  add object oCATCOM_2_8 as StdField with uid="ZEPVTSOYRK",rtseq=53,rtrep=.f.,;
    cFormVar = "w_CATCOM", cQueryName = "CATCOM",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale di eventuale selezione (vuoto = no selezione)",;
    HelpContextID = 231625510,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=102, Top=213, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CATCOM"

  func oCATCOM_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCOM_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCOM_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCATCOM_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oCATCOM_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CATCOM
     i_obj.ecpSave()
  endproc

  add object oCODPAG_2_9 as StdField with uid="TGIYBOLWES",rtseq=54,rtrep=.f.,;
    cFormVar = "w_CODPAG", cQueryName = "CODPAG",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice pagamento selezionato (vuoto = no selezione)",;
    HelpContextID = 117072166,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=102, Top=244, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_CODPAG"

  func oCODPAG_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPAG_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPAG_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oCODPAG_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oCODPAG_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_CODPAG
     i_obj.ecpSave()
  endproc

  add object oNOSBAN_2_10 as StdField with uid="QXVRUKKZSE",rtseq=55,rtrep=.f.,;
    cFormVar = "w_NOSBAN", cQueryName = "NOSBAN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice banca inesistente ",;
    ToolTipText = "Codice nostra banca selezionato (vuoto = no selezione)",;
    HelpContextID = 233656790,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=102, Top=274, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACC", oKey_1_1="BANUMCOR", oKey_1_2="this.w_NOSBAN"

  func oNOSBAN_2_10.mHide()
    with this.Parent.oContained
      return (!upper(g_application)='AD HOC ENTERPRISE')
    endwith
  endfunc

  func oNOSBAN_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOSBAN_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOSBAN_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BANUMCOR',cp_AbsName(this.parent,'oNOSBAN_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACC',"Conti banche",'',this.parent.oContained
  endproc
  proc oNOSBAN_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BANUMCOR=this.parent.oContained.w_NOSBAN
     i_obj.ecpSave()
  endproc

  add object oDESAGE_2_11 as StdField with uid="XPSYMGPEPP",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 88885046,;
   bGlobalFont=.t.,;
    Height=21, Width=285, Left=168, Top=65, InputMask=replicate('X',35)

  add object oCODCOM_2_12 as StdField with uid="FYSATWLWOJ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 231563558,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=102, Top=305, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_2_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S' Or  g_COMM='N' And g_PERCAN='S')
    endwith
   endif
  endfunc

  func oCODCOM_2_12.mHide()
    with this.Parent.oContained
      return (!isAhe())
    endwith
  endfunc

  func oCODCOM_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
      if .not. empty(.w_CODATT1)
        bRes2=.link_2_13('Full')
      endif
      if .not. empty(.w_CODATT2)
        bRes2=.link_2_14('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCOM_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oCODATT1_2_13 as StdField with uid="MVWAWQQKDY",rtseq=58,rtrep=.f.,;
    cFormVar = "w_CODATT1", cQueryName = "CODATT1",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 85680422,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=102, Top=333, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT1"

  func oCODATT1_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S' And !Empty(.w_CODCOM))
    endwith
   endif
  endfunc

  func oCODATT1_2_13.mHide()
    with this.Parent.oContained
      return (!isAhe())
    endwith
  endfunc

  func oCODATT1_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT1_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT1_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT1_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivita",'GSPC_AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oCODATT1_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_CODCOM
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_CODATT1
     i_obj.ecpSave()
  endproc

  add object oCODATT2_2_14 as StdField with uid="EUHCSDVZXF",rtseq=59,rtrep=.f.,;
    cFormVar = "w_CODATT2", cQueryName = "CODATT2",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice attivit�",;
    HelpContextID = 85680422,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=102, Top=362, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODCOM", oKey_1_2="this.w_CODCOM", oKey_2_1="ATTIPATT", oKey_2_2="this.w_TIPATT", oKey_3_1="ATCODATT", oKey_3_2="this.w_CODATT2"

  func oCODATT2_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S' And !Empty(.w_CODCOM))
    endwith
   endif
  endfunc

  func oCODATT2_2_14.mHide()
    with this.Parent.oContained
      return (!isAhe())
    endwith
  endfunc

  func oCODATT2_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT2_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT2_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ATTIVITA_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStrODBC(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStrODBC(this.Parent.oContained.w_TIPATT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATCODCOM="+cp_ToStr(this.Parent.oContained.w_CODCOM)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ATTIPATT="+cp_ToStr(this.Parent.oContained.w_TIPATT)
    endif
    do cp_zoom with 'ATTIVITA','*','ATCODCOM,ATTIPATT,ATCODATT',cp_AbsName(this.parent,'oCODATT2_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Elenco attivita",'GSPC_AAZ.ATTIVITA_VZM',this.parent.oContained
  endproc
  proc oCODATT2_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ATCODCOM=w_CODCOM
    i_obj.ATTIPATT=w_TIPATT
     i_obj.w_ATCODATT=this.parent.oContained.w_CODATT2
     i_obj.ecpSave()
  endproc

  add object oDESCOM_2_16 as StdField with uid="IDFXZOJPNV",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 231622454,;
   bGlobalFont=.t.,;
    Height=21, Width=214, Left=242, Top=305, InputMask=replicate('X',30)

  func oDESCOM_2_16.mHide()
    with this.Parent.oContained
      return (!isAhe())
    endwith
  endfunc

  add object oDESAT1_2_18 as StdField with uid="QIZSBLASGC",rtseq=61,rtrep=.f.,;
    cFormVar = "w_DESAT1", cQueryName = "DESAT1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 35407670,;
   bGlobalFont=.t.,;
    Height=21, Width=214, Left=242, Top=333, InputMask=replicate('X',30)

  func oDESAT1_2_18.mHide()
    with this.Parent.oContained
      return (!isAhe())
    endwith
  endfunc

  add object oDESAT2_2_20 as StdField with uid="ZCBOYAMJVK",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DESAT2", cQueryName = "DESAT2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 52184886,;
   bGlobalFont=.t.,;
    Height=21, Width=214, Left=242, Top=362, InputMask=replicate('X',30)

  func oDESAT2_2_20.mHide()
    with this.Parent.oContained
      return (!isAhe())
    endwith
  endfunc

  add object oDESLINGUA_2_23 as StdField with uid="BPDRIHCADC",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DESLINGUA", cQueryName = "DESLINGUA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 25736293,;
   bGlobalFont=.t.,;
    Height=21, Width=214, Left=168, Top=95, InputMask=replicate('X',30)

  add object oDESVET_2_26 as StdField with uid="ZPTAEJWEWM",rtseq=87,rtrep=.f.,;
    cFormVar = "w_DESVET", cQueryName = "DESVET",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 71386934,;
   bGlobalFont=.t.,;
    Height=21, Width=214, Left=168, Top=121, InputMask=replicate('X',30)

  add object oDESDES_2_27 as StdField with uid="INPWQBPBRQ",rtseq=88,rtrep=.f.,;
    cFormVar = "w_DESDES", cQueryName = "DESDES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 53430070,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=168, Top=153, InputMask=replicate('X',40)

  add object oDESZON_2_28 as StdField with uid="YWZJCKTTOM",rtseq=89,rtrep=.f.,;
    cFormVar = "w_DESZON", cQueryName = "DESZON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 249906998,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=168, Top=182, InputMask=replicate('X',35)

  add object oDESPAG_2_32 as StdField with uid="CIURVSUYUU",rtseq=90,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 117131062,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=168, Top=244, InputMask=replicate('X',30)

  add object oDESCACM_2_33 as StdField with uid="UOTJOGHFLS",rtseq=91,rtrep=.f.,;
    cFormVar = "w_DESCACM", cQueryName = "DESCACM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 219265226,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=168, Top=213, InputMask=replicate('X',35)

  add object oDESBAN_2_36 as StdField with uid="CCRHFRMUDF",rtseq=92,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 233654070,;
   bGlobalFont=.t.,;
    Height=21, Width=214, Left=242, Top=274, InputMask=replicate('X',30)

  add object oNOSBAN_2_37 as StdField with uid="MQXLBXXNNE",rtseq=99,rtrep=.f.,;
    cFormVar = "w_NOSBAN", cQueryName = "NOSBAN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice banca inesistente ",;
    ToolTipText = "Codice nostra banca selezionato (vuoto = no selezione)",;
    HelpContextID = 233656790,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=102, Top=274, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_NOSBAN"

  func oNOSBAN_2_37.mHide()
    with this.Parent.oContained
      return (upper(g_application)='AD HOC ENTERPRISE')
    endwith
  endfunc

  func oNOSBAN_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oNOSBAN_2_37.ecpDrop(oSource)
    this.Parent.oContained.link_2_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNOSBAN_2_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oNOSBAN_2_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banche",'',this.parent.oContained
  endproc
  proc oNOSBAN_2_37.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_NOSBAN
     i_obj.ecpSave()
  endproc

  add object oStr_2_3 as StdString with uid="TMKURHOKPZ",Visible=.t., Left=59, Top=65,;
    Alignment=1, Width=41, Height=15,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="TDMXJIPLPI",Visible=.t., Left=31, Top=307,;
    Alignment=1, Width=69, Height=15,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_2_15.mHide()
    with this.Parent.oContained
      return (!isAhe())
    endwith
  endfunc

  add object oStr_2_17 as StdString with uid="QBWNFFMDZW",Visible=.t., Left=20, Top=335,;
    Alignment=1, Width=80, Height=15,;
    Caption="Da attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_17.mHide()
    with this.Parent.oContained
      return (!isAhe())
    endwith
  endfunc

  add object oStr_2_19 as StdString with uid="QJWZLHGRZQ",Visible=.t., Left=31, Top=364,;
    Alignment=1, Width=69, Height=15,;
    Caption="A attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_19.mHide()
    with this.Parent.oContained
      return (!isAhe())
    endwith
  endfunc

  add object oStr_2_22 as StdString with uid="FKFGFEIQGK",Visible=.t., Left=31, Top=95,;
    Alignment=1, Width=69, Height=18,;
    Caption="Lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="YLMSEDSQVV",Visible=.t., Left=5, Top=35,;
    Alignment=1, Width=95, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="XZGHZSEYZB",Visible=.t., Left=5, Top=124,;
    Alignment=1, Width=95, Height=18,;
    Caption="Vettore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="MDCFNFKCXO",Visible=.t., Left=8, Top=153,;
    Alignment=1, Width=92, Height=15,;
    Caption="Destinazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="AVWDZOWPHR",Visible=.t., Left=2, Top=182,;
    Alignment=1, Width=98, Height=18,;
    Caption="Zona:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="ZWRRXYXROA",Visible=.t., Left=8, Top=244,;
    Alignment=1, Width=92, Height=15,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="NZDSXGVUDX",Visible=.t., Left=2, Top=213,;
    Alignment=1, Width=98, Height=18,;
    Caption="Cat.Comm:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="ESEMDHTHFK",Visible=.t., Left=8, Top=274,;
    Alignment=1, Width=92, Height=18,;
    Caption="Ns. banca:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_sdv','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsve_sdv
*--- Seleziona/deseleziona record zoom
Procedure Seleziona_Deseleziona(pParent)
   local NC
   NC=pParent.w_ZoomTD.cCursor
   UPDATE (NC) SET XCHK=IIF(pParent.w_SELALL='S',1,0)
   SELECT(NC)
   GO TOP
   pParent.w_ZoomTD.Refresh()
EndProc

*--- Controllo_Causale_Intestatario
Procedure Controllo_Causale_Intestatario(pParent)
   local NC,l_NumVe,l_NumAc,l_RecNum, l_chk,l_NumCl,l_NumFo,l_NumNoInt
   l_NumNoInt=0
   l_NumFo=0
   l_NumCl=0
   NC=pParent.w_ZoomTD.cCursor
   SELECT(NC)
   l_RecNum=IIF(EOF(), -1, RECNO(NC))
   * conta le causali di vendita e di acquisto
   COUNT FOR XCHK=1 AND TDFLVEAC='V' TO l_NumVe
   COUNT FOR XCHK=1 AND TDFLVEAC='A' TO l_NumAc
   * conta le causali a cliente, a fornitore e senza intestatario
   COUNT FOR XCHK=1 AND TDFLINTE='C' AND TDRICNOM<>'A' TO l_NumCl
   COUNT FOR XCHK=1 AND TDFLINTE='F' AND TDRICNOM<>'A' TO l_NumFo
   COUNT FOR XCHK=1 AND (!TDFLINTE$'C-F' OR TDRICNOM='A') TO l_NumNoInt
   * conta le causali di ordini
   COUNT FOR XCHK=1 AND TDCATDOC='OR' TO l_NumOrdini
   * conta le causali di ddt
   COUNT FOR XCHK=1 AND TDCATDOC='DT' TO l_NumDdt
   * conta le causali di fatture
   COUNT FOR XCHK=1 AND TDCATDOC='FA' TO l_NumFatture
   * conta le causali di note di credito
   COUNT FOR XCHK=1 AND TDCATDOC='NC' TO l_NumNoteDiCredito
        pParent.w_EDITINT=.f.
     pParent.w_CATDOC=SPACE(2)
     pParent.w_FLVEAC=SPACE(1)
    If l_NumVe>0 and l_NumAc=0
       pParent.w_FLVEAC='V'
    Endif
    If l_NumAc>0 and l_NumVe=0
       pParent.w_FLVEAC='A'
     Endif
    If l_NumCl>0 and l_NumFo=0 and l_NumNoInt=0
        pParent.w_EDITINT=.t.
        pParent.w_FC='C'
    Endif
    If l_NumFo>0 and l_NumCl=0 and l_NumNoInt=0
        pParent.w_EDITINT=.t.
        pParent.w_FC='F'
    Endif
    If l_NumOrdini>0 and l_NumDdt=0 and l_NumFatture=0 and l_NumNoteDiCredito=0
       pParent.w_CATDOC='OR'
     Endif
    If l_NumDdt>0 and l_NumOrdini=0 and l_NumFatture=0 and l_NumNoteDiCredito=0
       pParent.w_CATDOC='DT'
     Endif
    If l_NumFatture>0 and l_NumOrdini=0 and l_NumDdt=0 and l_NumNoteDiCredito=0
       pParent.w_CATDOC='FA'
     Endif
    If l_NumNoteDiCredito>0 and l_NumOrdini=0 and l_NumDdt=0 and l_NumFatture=0
       pParent.w_CATDOC='NC'
     Endif
    IF IsAlt()
     * Deve poter essere possibile filtrare per intestatario, 
     * indipendentemente dal tipo di causale selezionata avremo sempre l_NumFo=0
     pParent.w_EDITINT=.t.
     pParent.w_FC='C'
     pParent.w_FLVEAC='V'
    ENDIF
    if ! pParent.w_EDITINT or pParent.w_TDFLVEAC <> pParent.w_OLDFLVEAC or pParent.w_CATEGO='XX'
      pParent.w_CLIFOR=SPACE(15)
      pParent.w_DQ=SPACE(40)
    endif
    pParent.w_OLDFLVEAC=pParent.w_TDFLVEAC
   *--- Causale Unica
   SELECT(NC)
   COUNT FOR XCHK=1 TO l_chk
   If l_chk>1
     pParent.w_EDITCAU=.f.
     pParent.w_TIPOIN=SPACE(5)
     pParent.w_dt = SPACE(35)
   Else
     pParent.w_EDITCAU=.t.
   Endif
   SELECT(NC)
   If l_RecNum<>-1
     GO (l_RecNum)
   Endif
   pParent.mEnableControls()
EndProc

Proc CheckOnlyTipDoc(pParent)
     local NC
     NC=pParent.w_ZoomTD.cCursor
     UPDATE (NC) SET XCHK = 0
     UPDATE (NC) SET XCHK = 1 WHERE TDTIPDOC = pParent.w_TIPOIN OR (TDCATDOC= pParent.w_CATEGO AND NOT EMPTY(pParent.w_CATEGO))OR pParent.w_CATEG1='TT' OR (TDFLVEAC=pParent.w_SELVEAC AND pParent.w_CICLO='O')
     SELECT(NC)
     GO TOP
   pParent.NotifyEvent("w_zoomtd row checked")
   pParent.w_ZoomTD.Refresh()
EndProc
* --- Fine Area Manuale
