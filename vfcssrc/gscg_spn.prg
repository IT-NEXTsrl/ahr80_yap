* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_spn                                                        *
*              Stampa movimenti contabili                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_95]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-20                                                      *
* Last revis.: 2008-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_spn",oParentObject))

* --- Class definition
define class tgscg_spn as StdForm
  Top    = 24
  Left   = 15

  * --- Standard Properties
  Width  = 484
  Height = 390
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-08"
  HelpContextID=127261335
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Constant Properties
  _IDX = 0
  CAU_CONT_IDX = 0
  ESERCIZI_IDX = 0
  cpusers_IDX = 0
  CONTI_IDX = 0
  PNT_IVA_IDX = 0
  VOCIIVA_IDX = 0
  cPrg = "gscg_spn"
  cComment = "Stampa movimenti contabili"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(5)
  w_ESE = space(4)
  w_data1 = ctod('  /  /  ')
  o_data1 = ctod('  /  /  ')
  w_data2 = ctod('  /  /  ')
  w_NUMER1 = 0
  w_NUMER2 = 0
  w_TIPCLF = space(1)
  w_CODCLF = space(15)
  w_UTENTE1 = 0
  o_UTENTE1 = 0
  w_CAUSA = space(5)
  w_TIPREG = space(1)
  o_TIPREG = space(1)
  w_NUMREG = 0
  w_DEFI = space(10)
  w_QUADRA = space(1)
  w_DESCRI = space(35)
  w_DESUTE = space(20)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_ONUME = 0
  w_TIPO = space(1)
  w_DESCON = space(30)
  w_TIPOCLF = space(1)
  w_CODIVA = space(5)
  w_DESIVA = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_spnPag1","gscg_spn",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oESE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='cpusers'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='PNT_IVA'
    this.cWorkTables[6]='VOCIIVA'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCG_SPN with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_spn
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(5)
      .w_ESE=space(4)
      .w_data1=ctod("  /  /  ")
      .w_data2=ctod("  /  /  ")
      .w_NUMER1=0
      .w_NUMER2=0
      .w_TIPCLF=space(1)
      .w_CODCLF=space(15)
      .w_UTENTE1=0
      .w_CAUSA=space(5)
      .w_TIPREG=space(1)
      .w_NUMREG=0
      .w_DEFI=space(10)
      .w_QUADRA=space(1)
      .w_DESCRI=space(35)
      .w_DESUTE=space(20)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_ONUME=0
      .w_TIPO=space(1)
      .w_DESCON=space(30)
      .w_TIPOCLF=space(1)
      .w_CODIVA=space(5)
      .w_DESIVA=space(35)
        .w_AZIENDA = i_CODAZI
        .w_ESE = g_CODESE
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ESE))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_NUMER1 = 1
        .w_NUMER2 = 999999
        .w_TIPCLF = 'T'
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODCLF))
          .link_1_8('Full')
        endif
        .w_UTENTE1 = IIF(g_UNIUTE $ 'UE', i_CODUTE,0)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_UTENTE1))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CAUSA))
          .link_1_10('Full')
        endif
        .w_TIPREG = 'T'
        .w_NUMREG = 0
        .w_DEFI = ''
        .w_QUADRA = 'T'
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
          .DoRTCalc(15,15,.f.)
        .w_DESUTE = IIF(.w_UTENTE1<>0,.w_DESUTE,'Tutti')
        .w_OBTEST = .w_data1
          .DoRTCalc(18,19,.f.)
        .w_TIPO = IIF(.w_tipreg='T',' ',.w_tipreg)
          .DoRTCalc(21,21,.f.)
        .w_TIPOCLF = IIF(.w_TIPCLF='T', ' ', .w_TIPCLF)
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_CODIVA))
          .link_1_39('Full')
        endif
    endwith
    this.DoRTCalc(24,24,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_AZIENDA = i_CODAZI
        .DoRTCalc(2,11,.t.)
        if .o_TIPREG<>.w_TIPREG
            .w_NUMREG = 0
        endif
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .DoRTCalc(13,15,.t.)
        if .o_UTENTE1<>.w_UTENTE1
            .w_DESUTE = IIF(.w_UTENTE1<>0,.w_DESUTE,'Tutti')
        endif
        if .o_data1<>.w_data1
            .w_OBTEST = .w_data1
        endif
        .DoRTCalc(18,19,.t.)
        if .o_TIPREG<>.w_TIPREG
            .w_TIPO = IIF(.w_tipreg='T',' ',.w_tipreg)
        endif
        .DoRTCalc(21,21,.t.)
            .w_TIPOCLF = IIF(.w_TIPCLF='T', ' ', .w_TIPCLF)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(23,24,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCLF_1_8.enabled = this.oPgFrm.Page1.oPag.oCODCLF_1_8.mCond()
    this.oPgFrm.Page1.oPag.oUTENTE1_1_9.enabled = this.oPgFrm.Page1.oPag.oUTENTE1_1_9.mCond()
    this.oPgFrm.Page1.oPag.oNUMREG_1_12.enabled = this.oPgFrm.Page1.oPag.oNUMREG_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDESCON_1_37.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_37.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ESE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_ESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESE_1_2'),i_cWhere,'GSAR_KES',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_ESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESE = NVL(_Link_.ESCODESE,space(4))
      this.w_data1 = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_data2 = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESE = space(4)
      endif
      this.w_data1 = ctod("  /  /  ")
      this.w_data2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCLF
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLF;
                     ,'ANCODICE',trim(this.w_CODCLF))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCLF_1_8'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLF;
                       ,'ANCODICE',this.w_CODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLF = space(15)
      endif
      this.w_DESCON = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTENTE1
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTENTE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_UTENTE1);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_UTENTE1)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_UTENTE1) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','CODE',cp_AbsName(oSource.parent,'oUTENTE1_1_9'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTENTE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_UTENTE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_UTENTE1)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTENTE1 = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_UTENTE1 = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTENTE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUSA
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUSA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CAUSA)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CAUSA))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUSA)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUSA) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCAUSA_1_10'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUSA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUSA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUSA)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUSA = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCRI = NVL(_Link_.CCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CAUSA = space(5)
      endif
      this.w_DESCRI = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente oppure obsoleta")
        endif
        this.w_CAUSA = space(5)
        this.w_DESCRI = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUSA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODIVA
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_CODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_CODIVA))
          select IVCODIVA,IVDESIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCODIVA_1_39'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_CODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_CODIVA)
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oESE_1_2.value==this.w_ESE)
      this.oPgFrm.Page1.oPag.oESE_1_2.value=this.w_ESE
    endif
    if not(this.oPgFrm.Page1.oPag.odata1_1_3.value==this.w_data1)
      this.oPgFrm.Page1.oPag.odata1_1_3.value=this.w_data1
    endif
    if not(this.oPgFrm.Page1.oPag.odata2_1_4.value==this.w_data2)
      this.oPgFrm.Page1.oPag.odata2_1_4.value=this.w_data2
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMER1_1_5.value==this.w_NUMER1)
      this.oPgFrm.Page1.oPag.oNUMER1_1_5.value=this.w_NUMER1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMER2_1_6.value==this.w_NUMER2)
      this.oPgFrm.Page1.oPag.oNUMER2_1_6.value=this.w_NUMER2
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCLF_1_7.RadioValue()==this.w_TIPCLF)
      this.oPgFrm.Page1.oPag.oTIPCLF_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCLF_1_8.value==this.w_CODCLF)
      this.oPgFrm.Page1.oPag.oCODCLF_1_8.value=this.w_CODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oUTENTE1_1_9.value==this.w_UTENTE1)
      this.oPgFrm.Page1.oPag.oUTENTE1_1_9.value=this.w_UTENTE1
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUSA_1_10.value==this.w_CAUSA)
      this.oPgFrm.Page1.oPag.oCAUSA_1_10.value=this.w_CAUSA
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPREG_1_11.RadioValue()==this.w_TIPREG)
      this.oPgFrm.Page1.oPag.oTIPREG_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMREG_1_12.value==this.w_NUMREG)
      this.oPgFrm.Page1.oPag.oNUMREG_1_12.value=this.w_NUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oDEFI_1_13.RadioValue()==this.w_DEFI)
      this.oPgFrm.Page1.oPag.oDEFI_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oQUADRA_1_14.RadioValue()==this.w_QUADRA)
      this.oPgFrm.Page1.oPag.oQUADRA_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_18.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_18.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_24.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_24.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_37.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_37.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCODIVA_1_39.value==this.w_CODIVA)
      this.oPgFrm.Page1.oPag.oCODIVA_1_39.value=this.w_CODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_41.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_41.value=this.w_DESIVA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_data2) or .w_data1<=.w_data2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odata1_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(empty(.w_data1) or .w_data1<=.w_data2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odata2_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(.w_numer1<=.w_numer2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMER1_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il primo numero � maggiore del secondo")
          case   not(.w_numer1<=.w_numer2)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMER2_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il primo numero � maggiore del secondo")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CAUSA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUSA_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente oppure obsoleta")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_data1 = this.w_data1
    this.o_UTENTE1 = this.w_UTENTE1
    this.o_TIPREG = this.w_TIPREG
    return

enddefine

* --- Define pages as container
define class tgscg_spnPag1 as StdContainer
  Width  = 480
  height = 390
  stdWidth  = 480
  stdheight = 390
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oESE_1_2 as StdField with uid="CTFGUWXZPD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ESE", cQueryName = "ESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza dei movimenti da stampare",;
    HelpContextID = 127566406,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=110, Top=7, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESE"

  func oESE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oESE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"Esercizi",'',this.parent.oContained
  endproc
  proc oESE_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_AZIENDA
     i_obj.w_ESCODESE=this.parent.oContained.w_ESE
     i_obj.ecpSave()
  endproc

  add object odata1_1_3 as StdField with uid="KQPPUKNLMW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_data1", cQueryName = "data1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data registrazione di inizio stampa",;
    HelpContextID = 185500214,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=110, Top=33

  func odata1_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data2) or .w_data1<=.w_data2)
    endwith
    return bRes
  endfunc

  add object odata2_1_4 as StdField with uid="JWAYBCEWUW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_data2", cQueryName = "data2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data registrazione di fine stampa",;
    HelpContextID = 186548790,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=110, Top=59

  func odata2_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data1) or .w_data1<=.w_data2)
    endwith
    return bRes
  endfunc

  add object oNUMER1_1_5 as StdField with uid="NIQAPTYNLS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NUMER1", cQueryName = "NUMER1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il primo numero � maggiore del secondo",;
    ToolTipText = "Numero di registrazione di inizio stampa",;
    HelpContextID = 33553194,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=382, Top=33, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMER1_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numer1<=.w_numer2)
    endwith
    return bRes
  endfunc

  add object oNUMER2_1_6 as StdField with uid="QYLWHTCKMO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_NUMER2", cQueryName = "NUMER2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il primo numero � maggiore del secondo",;
    ToolTipText = "Numero di registrazione di fine stampa",;
    HelpContextID = 16775978,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=382, Top=59, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMER2_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_numer1<=.w_numer2)
    endwith
    return bRes
  endfunc


  add object oTIPCLF_1_7 as StdCombo with uid="LXJVDJQCLV",rtseq=7,rtrep=.f.,left=110,top=85,width=79,height=21;
    , ToolTipText = "Tipo conto";
    , HelpContextID = 224515786;
    , cFormVar="w_TIPCLF",RowSource=""+"Cliente,"+"Fornitore,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCLF_1_7.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oTIPCLF_1_7.GetRadio()
    this.Parent.oContained.w_TIPCLF = this.RadioValue()
    return .t.
  endfunc

  func oTIPCLF_1_7.SetRadio()
    this.Parent.oContained.w_TIPCLF=trim(this.Parent.oContained.w_TIPCLF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCLF=='C',1,;
      iif(this.Parent.oContained.w_TIPCLF=='F',2,;
      iif(this.Parent.oContained.w_TIPCLF=='T',3,;
      0)))
  endfunc

  func oTIPCLF_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCLF)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCLF_1_8 as StdField with uid="EUSKVAOTAU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODCLF", cQueryName = "CODCLF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice intestatario selezionato",;
    HelpContextID = 224563674,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=110, Top=114, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCLF"

  func oCODCLF_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCLF<>'T')
    endwith
   endif
  endfunc

  func oCODCLF_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCLF_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLF_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCLF_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'',this.parent.oContained
  endproc
  proc oCODCLF_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCLF
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCLF
     i_obj.ecpSave()
  endproc

  add object oUTENTE1_1_9 as StdField with uid="LXFONUUCRT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_UTENTE1", cQueryName = "UTENTE1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente selezionato (0 tutti)",;
    HelpContextID = 232225722,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=110, Top=141, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="CODE", oKey_1_2="this.w_UTENTE1"

  func oUTENTE1_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_UNIUTE $ 'UE')
    endwith
   endif
  endfunc

  func oUTENTE1_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oUTENTE1_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUTENTE1_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','CODE',cp_AbsName(this.parent,'oUTENTE1_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oCAUSA_1_10 as StdField with uid="FATAEJJVYQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CAUSA", cQueryName = "CAUSA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente oppure obsoleta",;
    ToolTipText = "Causale contabile selezionata",;
    HelpContextID = 201224230,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=110, Top=168, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CAUSA"

  func oCAUSA_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUSA_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUSA_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCAUSA_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oCAUSA_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CAUSA
     i_obj.ecpSave()
  endproc


  add object oTIPREG_1_11 as StdCombo with uid="ARGSWPJTSO",rtseq=11,rtrep=.f.,left=110,top=223,width=157,height=21;
    , ToolTipText = "Tipo registro IVA selezionato";
    , HelpContextID = 214095562;
    , cFormVar="w_TIPREG",RowSource=""+"Vendite,"+"Acquisti,"+"Corr.scorporo,"+"Corr.ventilazione,"+"No IVA,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPREG_1_11.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'A',;
    iif(this.value =3,'C',;
    iif(this.value =4,'E',;
    iif(this.value =5,'N',;
    iif(this.value =6,'T',;
    space(1))))))))
  endfunc
  func oTIPREG_1_11.GetRadio()
    this.Parent.oContained.w_TIPREG = this.RadioValue()
    return .t.
  endfunc

  func oTIPREG_1_11.SetRadio()
    this.Parent.oContained.w_TIPREG=trim(this.Parent.oContained.w_TIPREG)
    this.value = ;
      iif(this.Parent.oContained.w_TIPREG=='V',1,;
      iif(this.Parent.oContained.w_TIPREG=='A',2,;
      iif(this.Parent.oContained.w_TIPREG=='C',3,;
      iif(this.Parent.oContained.w_TIPREG=='E',4,;
      iif(this.Parent.oContained.w_TIPREG=='N',5,;
      iif(this.Parent.oContained.w_TIPREG=='T',6,;
      0))))))
  endfunc

  add object oNUMREG_1_12 as StdField with uid="DRHWZSEZDP",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NUMREG", cQueryName = "NUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registro IVA selezionato (0 = nessuna selezione)",;
    HelpContextID = 214104874,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=410, Top=223, cSayPict='"99"', cGetPict='"99"'

  func oNUMREG_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPREG<>'N')
    endwith
   endif
  endfunc


  add object oDEFI_1_13 as StdCombo with uid="NAELGZRXQU",value=3,rtseq=13,rtrep=.f.,left=110,top=251,width=128,height=21;
    , ToolTipText = "Tipo registrazioni selezionate";
    , HelpContextID = 132351030;
    , cFormVar="w_DEFI",RowSource=""+"Confermate,"+"Provvisorie,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDEFI_1_13.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    space(10)))))
  endfunc
  func oDEFI_1_13.GetRadio()
    this.Parent.oContained.w_DEFI = this.RadioValue()
    return .t.
  endfunc

  func oDEFI_1_13.SetRadio()
    this.Parent.oContained.w_DEFI=trim(this.Parent.oContained.w_DEFI)
    this.value = ;
      iif(this.Parent.oContained.w_DEFI=='N',1,;
      iif(this.Parent.oContained.w_DEFI=='S',2,;
      iif(this.Parent.oContained.w_DEFI=='',3,;
      0)))
  endfunc


  add object oQUADRA_1_14 as StdCombo with uid="WUXXXJEVIL",rtseq=14,rtrep=.f.,left=110,top=280,width=277,height=21;
    , ToolTipText = "Tipo movimento";
    , HelpContextID = 33667834;
    , cFormVar="w_QUADRA",RowSource=""+"Solo movimenti senza quadratura,"+"Solo movimenti incongruenti,"+"Movimenti senza quadratura o incongruenti,"+"Movimenti corretti,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oQUADRA_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'R',;
    iif(this.value =3,'E',;
    iif(this.value =4,'C',;
    iif(this.value =5,'T',;
    space(1)))))))
  endfunc
  func oQUADRA_1_14.GetRadio()
    this.Parent.oContained.w_QUADRA = this.RadioValue()
    return .t.
  endfunc

  func oQUADRA_1_14.SetRadio()
    this.Parent.oContained.w_QUADRA=trim(this.Parent.oContained.w_QUADRA)
    this.value = ;
      iif(this.Parent.oContained.w_QUADRA=='S',1,;
      iif(this.Parent.oContained.w_QUADRA=='R',2,;
      iif(this.Parent.oContained.w_QUADRA=='E',3,;
      iif(this.Parent.oContained.w_QUADRA=='C',4,;
      iif(this.Parent.oContained.w_QUADRA=='T',5,;
      0)))))
  endfunc

  add object oDESCRI_1_18 as StdField with uid="WVGDDUOQEU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 167881674,;
   bGlobalFont=.t.,;
    Height=21, Width=266, Left=175, Top=168, InputMask=replicate('X',35)


  add object oObj_1_19 as cp_outputCombo with uid="ZLERXIVPWT",left=110, top=313, width=364,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 231611930


  add object oBtn_1_20 as StdButton with uid="DQLPESHZYX",left=376, top=338, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 267819994;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        do GSCG_BPN with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_21 as StdButton with uid="HOBTEUTDGP",left=426, top=338, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 134578758;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESUTE_1_24 as StdField with uid="NSPCPFCPVZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 231713738,;
   bGlobalFont=.t.,;
    Height=21, Width=199, Left=175, Top=141, InputMask=replicate('X',20)

  add object oDESCON_1_37 as StdField with uid="SNVHKSIWGF",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 87141322,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=230, Top=114, InputMask=replicate('X',30)

  func oDESCON_1_37.mHide()
    with this.Parent.oContained
      return (.w_TIPCLF='T')
    endwith
  endfunc

  add object oCODIVA_1_39 as StdField with uid="YRFZEUQXXF",rtseq=23,rtrep=.f.,;
    cFormVar = "w_CODIVA", cQueryName = "CODIVA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA selezionato (valido solo per stampe attinenti la parte IVA)",;
    HelpContextID = 29135322,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=110, Top=195, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_CODIVA"

  func oCODIVA_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIVA_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIVA_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCODIVA_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oCODIVA_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_CODIVA
     i_obj.ecpSave()
  endproc

  add object oDESIVA_1_41 as StdField with uid="UEJAXPZWXP",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 29076426,;
   bGlobalFont=.t.,;
    Height=21, Width=266, Left=175, Top=195, InputMask=replicate('X',35)

  add object oStr_1_15 as StdString with uid="WDNQZBBARJ",Visible=.t., Left=7, Top=33,;
    Alignment=1, Width=100, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="DLNOYWOPBH",Visible=.t., Left=7, Top=59,;
    Alignment=1, Width=100, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="OBWKODYJNV",Visible=.t., Left=7, Top=168,;
    Alignment=1, Width=100, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="XLYJAJWLJZ",Visible=.t., Left=7, Top=7,;
    Alignment=1, Width=100, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="XZHARHUSBV",Visible=.t., Left=7, Top=141,;
    Alignment=1, Width=100, Height=15,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="NAOBDZRIZI",Visible=.t., Left=7, Top=249,;
    Alignment=1, Width=100, Height=15,;
    Caption="Registrazioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="FXHLESQKFF",Visible=.t., Left=199, Top=33,;
    Alignment=1, Width=180, Height=15,;
    Caption="Da num.registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="OUFSXWIJBP",Visible=.t., Left=199, Top=59,;
    Alignment=1, Width=180, Height=15,;
    Caption="A num.registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="YBVGGYKCUF",Visible=.t., Left=7, Top=313,;
    Alignment=1, Width=100, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="WRMIESLCDW",Visible=.t., Left=351, Top=223,;
    Alignment=1, Width=55, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="JKCGCPZJDW",Visible=.t., Left=7, Top=221,;
    Alignment=1, Width=100, Height=15,;
    Caption="Tipo registro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="GRDWYGJWHT",Visible=.t., Left=7, Top=86,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="VWHMHWUMJL",Visible=.t., Left=7, Top=114,;
    Alignment=1, Width=100, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="HODXRUVTMY",Visible=.t., Left=7, Top=195,;
    Alignment=1, Width=100, Height=18,;
    Caption="Codice I.V.A.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="QLBLEYPZXG",Visible=.t., Left=7, Top=278,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo movimenti:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_spn','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
