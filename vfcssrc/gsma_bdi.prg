* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bdi                                                        *
*              Controlla inventari per dettagli                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_24]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-27                                                      *
* Last revis.: 2006-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bdi",oParentObject)
return(i_retval)

define class tgsma_bdi as StdBatch
  * --- Local variables
  w_APPO = space(6)
  * --- WorkFile variables
  INVENTAR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla se l'inventario � utilizzato come riferimento e in caso affermativo ne  impedisce la modifica o il caricamento
    * --- Read from INVENTAR
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.INVENTAR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2],.t.,this.INVENTAR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "INNUMPRE"+;
        " from "+i_cTable+" INVENTAR where ";
            +"INNUMPRE = "+cp_ToStrODBC(this.oParentObject.w_DINUMINV);
            +" and INESEPRE = "+cp_ToStrODBC(this.oParentObject.w_DICODESE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        INNUMPRE;
        from (i_cTable) where;
            INNUMPRE = this.oParentObject.w_DINUMINV;
            and INESEPRE = this.oParentObject.w_DICODESE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_APPO = NVL(cp_ToDate(_read_.INNUMPRE),cp_NullValue(_read_.INNUMPRE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_Rows<>0
      this.oParentObject.w_RESCHK = -1
    else
      this.oParentObject.w_RESCHK = 0
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='INVENTAR'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
