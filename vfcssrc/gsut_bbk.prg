* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bbk                                                        *
*              Routine per il Backup                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_866]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-25                                                      *
* Last revis.: 2016-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bbk",oParentObject)
return(i_retval)

define class tgsut_bbk as StdBatch
  * --- Local variables
  w_zipfile = space(200)
  w_SqlComando = space(100)
  w_TmpDBBCK = space(100)
  w_TmpN = 0
  w_From_Conn = 0
  w_NDBSRG = space(20)
  w_strwait = space(100)
  w_oMess = .NULL.
  w_OVERWRITE = .f.
  w_VERSIONE = space(10)
  w_RESCONTO = space(254)
  w_TABLENAME = space(240)
  w_TmpN2 = 0
  w_CMDDMP = space(254)
  w_DIRSQL2 = space(254)
  w_CARTELLADIAPPOGGIOPERZIP = space(250)
  w_FILESYSTEMRESULT = space(10)
  w_RESULT = 0
  w_NDAY = 0
  w_BACKUP = 0
  w_RES = space(254)
  * --- WorkFile variables
  AZIENDA_idx=0
  AZBACKUP_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- effettuo il backup del database inserito (da GSUT_KBK).
    * --- Path del Back Up
    this.oParentObject.w_PATBCK = alltrim(this.oParentObject.w_PATBCK)
    * --- La ricerca della directory ha senso solo se lancio il Backup sulla macchina
    *     in cui gira il server.
    *     
    *     Cerco l'ultimo '\' per determinare il nome della directory
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    if this.oParentObject.w_RiorgInd="S" OR Directory(iif(i_ServerConn[1,6]="DB2",this.oParentObject.w_PATBCK,substr(this.oParentObject.w_PATBCK,1,rat("\",this.oParentObject.w_PATBCK)))) Or ah_YesNo("Directory non presente localmente%0Vuoi comunque proseguire?")
      if ah_YesNo("Confermi operazione?")
        if i_ServerConn[1,6]="Oracle"
          this.w_strwait = "Export dei dati in corso..."
        else
          this.w_strwait = "Backup del database in corso..."
        endif
        * --- Se non su DB2 verifico eventuale presenza di precedenti backup
        this.w_OVERWRITE = .F.
        if i_ServerConn[1,6]<>"DB2"
          if file(alltrim(this.oParentObject.w_PATBCK))
            if not(ah_YesNo("Il file %1 esiste gi�%0Vuoi sovrascriverlo?","", alltrim(this.oParentObject.w_PATBCK) ))
              i_retcode = 'stop'
              i_retval = .f.
              return
            endif
            this.w_OVERWRITE = .T.
          endif
        endif
        * --- Nome del database
        this.w_NDBSRG = this.oParentObject.w_OriDat
        * --- Verifico l'attuale connessione (eventuali problemi...)
        this.w_From_Conn = i_ServerConn[1,2]
        if this.w_From_Conn<0
          ah_ErrorMsg("Connessione al database non attiva","!","")
          i_retcode = 'stop'
          i_retval = .f.
          return
        endif
        * --- Effettuo il Backup/export
        do case
          case i_ServerConn[1,6]="SQLServer"
            if this.oParentObject.w_CheckDB = "S"
              * --- Per eseguire il check occorre chiudere la connessione secondaria
               
 cp_CloseSecondConn()
              * --- Metto il database in Single user mode
              ah_Msg("Setto il flag <single user mode> sul database",.T.)
              this.w_VERSIONE = alltrim (SQLXGetDatabaseVersion( this.w_From_Conn ))
              if VAL(LEFT( this.w_VERSIONE ,3))>=11
                * --- In sql 2012 sp_dboptione � deprecato
                this.w_SqlComando = 'ALTER DATABASE "'+Alltrim(this.w_NDBSRG)+'" set SINGLE_USER WITH NO_WAIT'
              else
                this.w_SqlComando = "exec sp_dboption N'"+Alltrim(this.w_NDBSRG)+"', N'single', N'true' "
              endif
              this.w_TmpN = SqlExec(this.w_From_Conn,this.w_SqlComando)
              if this.w_TmpN=-1
                this.Page_2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                ah_ErrorMsg(Left("Errore check del database. Probabile presenza di altri utenti sul database",254),"!","")
                i_retcode = 'stop'
                i_retval = .f.
                return
              endif
              * --- Lancio il check del database...
              ah_Msg("Avvio check sul database",.T.)
              this.w_SqlComando = " dbcc checkdb(N'"+Alltrim(this.w_NDBSRG)+"',  REPAIR_FAST )  WITH TABLERESULTS "
              this.w_TmpN = SqlExec(this.w_From_Conn,this.w_SqlComando) * this.w_TmpN
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if Used( "SqlResult" )
                * --- Creo un nuovo temporaneo per la stampa
                 
 Select SqlResult 
 Go Bottom
                * --- Leggo il contenuto dell'ultimo record e lo mostro all'utente come resoconto
                *     Il risultato restituitomi da SQL Server potrebbe essere in formato UNICODE (SQl Server 2005 compatibilit� 90).
                *     In questo caso l'ASC del secondo carattere � 0 (non ho trovato altro per
                *     identificare se una stringa � UNICODE in VFP).
                if ASC(RIGHT(LEFT(SqlResult.messagetext,2),1))=0
                  * --- Converto l'UNICODE
                  this.w_RESCONTO = STRCONV(SqlResult.messagetext,6)
                else
                  this.w_RESCONTO = SqlResult.Messagetext
                endif
                if ah_YesNo(Left("Resoconto check database%0%1%0Si desidera stampare un resoconto dettagliato?",254),"", alltrim(this.w_RESCONTO ) )
                  Select * From SqlResult into Cursor __Tmp__ NoFilter
                  CP_CHPRN("QUERY\GSUT_BBK.FRX", " ", this)
                  if Used( "__tmp__" )
                     
 Select __Tmp__ 
 Use
                  endif
                  * --- Se l'utente decide di verificare in dettaglio il resoconto significa che ci potrebbero
                  *     essere dei problemi...
                  this.w_oMess.AddMsgPartNL("Se l'analisi del database ha evidenziato errori di consistenza e/o di allocazione si consiglia di conservare l'ultimo back up evitando di sovrascriverlo")     
                  this.w_oMess.AddMsgPartNL("Si desidera proseguire eventualmente sovrascrivendo il file di back up presente?")     
                  if this.w_OVERWRITE and Not this.w_oMESS.ah_YesNo()
                    if Used("SqlResult")
                       
 Select SqlResult 
 Use
                    endif
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    i_retcode = 'stop'
                    i_retval = .f.
                    return
                  endif
                endif
                if Used( "SQLRESULT" )
                   
 Select SqlResult 
 Use
                endif
              endif
              if this.w_TmpN<0
                ah_ErrorMsg(Left("Errore check del database%0%1",254),"!","", message() )
                i_retcode = 'stop'
                return
              else
                ah_Msg("Check sul database completato",.T.)
              endif
            endif
            if this.oParentObject.w_ReIndexDb="S" And this.w_TmpN>=0
               
 ah_Msg("Recupero elenco tabelle database...",.T.)
              * --- Non recupero le tabelle di sistema non aggiornabili
              if sqltables( this.w_From_Conn , "TABLE" )<>-1
                 
 Select SqlResult 
 Go Top
                do while Not Eof( "SqlResult" )
                  this.w_TABLENAME = Alltrim( SqlResult.Table_Name )
                  * --- Ricostruisce gli indici tranne per le tabelle di sistema
                  if LOWER (this.w_TABLENAME) <> "trace_xe_action_map" AND LOWER (this.w_TABLENAME) <>"trace_xe_event_map"
                    ah_Msg("Ricostruzione indici tabella %1 numero %2 su %3", .T.,.F.,.F., this.w_TABLENAME, alltrim(Str(Recno("Sqlresult"))), alltrim(Str(RecCount("Sqlresult"))))
                    this.w_SqlComando = "dbcc DBREINDEX (["+this.w_TABLENAME+"], '', 0)WITH NO_INFOMSGS "
                    this.w_TmpN = SqlExec(this.w_From_Conn,this.W_SqlComando)
                  endif
                  if Not Eof( "SqlResult" )
                     
 Select SqlResult 
 Skip
                  endif
                enddo
                 
 Select SqlResult 
 Use
              else
                ah_ErrorMsg(Left("Errore riorganizzazione indici%0%1",254),"!","", message() )
                i_retcode = 'stop'
                i_retval = .f.
                return
              endif
            endif
            * --- Se tutto ok eseguo il backup...
            if this.oParentObject.w_BckDB="S" AND this.w_TmpN>=0
              if this.w_OVERWRITE
                * --- Se il file di back Up esiste lo elimino dal disco...
                Delete File Alltrim(this.oParentObject.w_PATBCK)
              endif
              * --- Avvio Back Up - Cancello eventuale file logico di backup
              ah_Msg("Elimino eventuale device di Backup",.T.)
              this.w_TmpDBBCK = substr(this.oParentObject.w_PATBCK,rat("\",this.oParentObject.w_PATBCK)+1,rat(".",this.oParentObject.w_PATBCK)-(rat("\",this.oParentObject.w_PATBCK)+1))
              this.w_SqlComando = "exec sp_dropdevice '"+this.w_TmpDBBCK+"' "
              this.w_TmpN = SqlExec(this.w_From_Conn,this.w_SqlComando)
              * --- Anche se da errore il comando proseguo (caso in cui non ho nessun device)
              *     ad esempio st� creando il primo back Up
              * --- Creo un dispositivo logico di supporto
              ah_Msg("Creo un nuovo device di Backup",.T.)
              this.w_SqlComando = "exec sp_addumpdevice 'disk', '"+ this.w_TmpDBBCK +"', '"+this.oParentObject.w_PATBCK+"'"
              this.w_TmpN = SqlExec(this.w_From_Conn,this.W_SqlComando)
              if this.w_TmpN<0
                ah_ErrorMsg(Left("Errore nel Backup del database%0%1",254),"!","", message() )
                i_retcode = 'stop'
                i_retval = .f.
                return
              endif
              ah_Msg(this.w_strwait,.T.)
              this.w_SqlComando = "BackUp Database ["+ alltrim(this.w_NDBSRG) + "] to DISK = '"+ alltrim(this.oParentObject.w_PATBCK)+"'"+" WITH INIT"
              _Cliptext= this.w_SqlComando
              this.w_TmpN = SqlExec(this.w_From_Conn,this.w_SqlComando)
              * --- Avvio Back Up - Cancello eventuale file logico di backup
              ah_Msg("Elimino eventuale device di Backup",.T.)
              this.w_SqlComando = "exec sp_dropdevice '"+this.w_TmpDBBCK+"' "
              this.w_TmpN2 = SqlExec(this.w_From_Conn,this.w_SqlComando)
              if this.w_TmpN<=0
                * --- Backup fallito
                ah_ErrorMsg(Left("Errore nel Backup del database%0%1",254),"!","", message() )
                i_retcode = 'stop'
                i_retval = .f.
                return
              else
                ah_ErrorMsg("Backup effettuato correttamente","!","")
              endif
            endif
          case i_ServerConn[1,6]="DB2"
            * --- Su DB2 il back up fallisce se esiste una connessione attiva...
            if not(ah_YesNo("Per effettuare il Backup del database DB2 tutti gli altri utenti devono essere scollegati%0Tutti gli utenti sono scollegati?"))
              i_retcode = 'stop'
              i_retval = .f.
              return
            else
              ah_Msg(this.w_strwait,.T.)
              * --- Costruisco il comando DOS per lanciare il back-Up DB2
              Backdb2="db2cmd /c /w /i db2 backup db " + alltrim(this.w_NDBSRG) + " user " + alltrim(this.oParentObject.w_user) + " using " + alltrim(this.oParentObject.w_passwd) + " to '" + this.oParentObject.w_PATBCK + "'"
              * --- Il comando DB2 non consente comandi pi� lunghi di 95 caratteri..
              if len(backdb2) > 95
                ah_ErrorMsg("Comando di Backup troppo lungo, scegliere un nome directory pi� corto. (Max %1 caratteri)","!","", Alltrim(str(95-(Len(BackDb2)-Len(this.oParentObject.w_PATBCK)))) )
                i_retcode = 'stop'
                i_retval = .f.
                return
              else
                * --- Lancio il Back Up in modo asincrono (/N) a seguito di:
                *     
                *     a) Disabilito mCalc maschera chiamante
                *     b) Aggiorno il path di Back Up sull'anagrafica azienda
                *     c) Elimino dalla gestione utente l'entrata relativa a questa connessione
                *     d) Chiudio tutte le conenssioni (requisito indispensabile per eseguire il back up su DB2)
                *     e) lancio il comando DOS
                *     f) Dlear events per saltare all'istruzioen dopo la READ EVENTS nel CP3START, a tal scopo dichiaro la
                *     variabile g_DB2BCK per eviare di svolgere operazioni ulteriori sul database
                *     (darebbero errore in quanto ho chiuso la connessione)
                this.bUpdateParentObject =.f.
                This.oParentObject.NotifyEvent("ScriviPath")
                * --- Chiudo la maschera..
                This.oParentObject.EcpQuit()
                 
 Do GSUT_BCU 
 Public g_DB2Bck 
 g_DB2Bck="S"
                * --- Disconnetto tutte le connessioni...
                this.w_TmpN = Sqldisconnect(0)
                _ClipText=backdb2
                ! /N &backdb2
                * --- Esce da procedura
                 
 Clear Events
                i_retcode = 'stop'
                i_retval = .t.
                return
              endif
            endif
          case i_ServerConn[1,6]="Oracle"
            if this.w_OVERWRITE
              * --- Se il file di back Up esiste lo elimino dal disco...
              Delete File Alltrim(this.oParentObject.w_PATBCK)
            endif
            ah_Msg(this.w_strwait,.T.)
            this.w_CMDDMP = "exp"+Alltrim(this.oParentObject.w_USER)+"/"+Alltrim(this.oParentObject.w_PASSWD)+"@"+this.oParentObject.w_service+"OWNER='"+this.oParentObject.w_user+"' FILE='"
            oraexp="exp "+Alltrim(this.oParentObject.w_USER)+"/"+Alltrim( this.oParentObject.w_PASSWD )+"@"+this.oParentObject.w_service+" OWNER='"+this.oParentObject.w_user+"' FILE='"+this.oParentObject.w_PATBCK+"'"
            * --- Verifico la lunghezza se superiore a 107 Oracle va in errore
            if len(oraexp) > 107
              ah_ErrorMsg("Comando di export dati troppo lungo, scegliere un nome file dump pi� corto. (Max %1 caratteri)","!","", alltrim(str(107-len(this.w_CMDDMP)-34)) )
              i_retcode = 'stop'
              i_retval = .f.
              return
            else
              _ClipText=oraexp
              ! &oraexp
            endif
          case i_ServerConn[1,6]="PostgreSQL"
            if vartype(g_PATH_PGDUMP)<>"C" or empty(g_PATH_PGDUMP)
              ah_ErrorMsg("Impostare la variabile (g_PATH_PGDUMP) con il percorso al file pg_dump.exe","!","")
              i_retcode = 'stop'
              i_retval = .F.
              return
            endif
            if NOT FILE(ADDBS(g_PATH_PGDUMP)+"PG_DUMP.EXE") 
              ah_ErrorMsg("Applicazione pg_dump.exe non trovata nel percorso: "+ g_PATH_PGDUMP,"!","")
              i_retcode = 'stop'
              i_retval = .F.
              return
            endif
            if this.w_OVERWRITE
              * --- Se il file di back Up esiste lo elimino dal disco...
              Delete File Alltrim(this.oParentObject.w_PATBCK)
            endif
            ah_Msg(this.w_strwait,.T.)
            pgr_dump2 = '"'+ADDBS(g_PATH_PGDUMP)+"pg_dump.exe"+'"'+" --host=" +this.oParentObject.w_SERVERNAME+ " --port=" +this.oParentObject.w_PORT+; 
 " --username=" +this.oParentObject.w_user+ " --format=" +this.oParentObject.w_FORMATO+ " --encoding=WIN1252 --blobs --file="+'"'+this.oParentObject.w_PATBCK+'"' + " " + this.w_NDBSRG
            _ClipText=pgr_dump2
            pgr_dump = "@SET PGPASSWORD="+this.oParentObject.w_passwd+CHR(13)+CHR(10) + pgr_dump2
            dump_bat = addbs(tempadhoc()) + "PGR_DUMP.BAT"
            STRTOFILE(pgr_dump, dump_bat, 0)
            if FILE(dump_bat)
              ! &dump_bat
              delete file (dump_bat)
            else
              ah_ErrorMsg("Impossibile creare file BAT di esecuzione backup.","!","")
              i_retcode = 'stop'
              i_retval = .F.
              return
            endif
        endcase
        * --- Se attivo creo lo Zip del file di Back Up....
        if this.oParentObject.w_ZipDB="S" and ah_YesNo("La creazione dello zip avviene solo se l'applicativo � lanciato sul server di dati%0Proseguo con l'esecuzione dello zip?")
          this.w_DIRSQL2 = substr(this.oParentObject.w_PATBCK,1,rat("\",this.oParentObject.w_PATBCK))
          this.w_zipfile = substr(this.oParentObject.w_PATBCK,rat("\",this.oParentObject.w_PATBCK)+1,rat(".",this.oParentObject.w_PATBCK)-(rat("\",this.oParentObject.w_PATBCK)+1))+".7z"
          if File( this.w_DIRSQL2 +this.w_zipfile ) 
            if ah_YesNo("Lo zip � gia presente, per proseguire l'applicativo lo canceller�%0Proseguo con l'esecuzione dello zip?")
              delete file alltrim(this.w_DIRSQL2 +this.w_zipfile)
            else
              i_retcode = 'stop'
              i_retval = .f.
              return
            endif
          endif
          * --- Crea la cartella di appoggio w_CARTELLADIAPPOGGIOPERZIP
          if i_ServerConn[1,6]="PostgreSQL" and this.oParentObject.w_FORMATO="directory" and directory(this.oParentObject.w_PATBCK)
            * --- Backup postgres in formato directory
            this.w_CARTELLADIAPPOGGIOPERZIP = this.oParentObject.w_PATBCK
          else
            this.w_CARTELLADIAPPOGGIOPERZIP = ALLTRIM( this.w_DIRSQL2 ) + ALLTRIM( ADDBS(substr(this.oParentObject.w_PATBCK,rat("\",this.oParentObject.w_PATBCK)+1,rat(".",this.oParentObject.w_PATBCK)-(rat("\",this.oParentObject.w_PATBCK)+1))) )
            this.w_FILESYSTEMRESULT = FILESYSTEMOBJ("createfolder", this.w_CARTELLADIAPPOGGIOPERZIP)
            * --- Copia il file di backup w_PATBCK in w_DIRSQL2+w_CARTELLADIAPPOGGIOPERZIP+JUSTFNAME(w_PATBCK)
            this.w_FILESYSTEMRESULT = FILESYSTEMOBJ("copyfile", this.oParentObject.w_PATBCK, this.w_CARTELLADIAPPOGGIOPERZIP+JUSTFNAME(this.oParentObject.w_PATBCK) )
          endif
          * --- Esegue lo zip
          L_ERRORE = 0
          ZIPUNZIP("Z",this.w_DIRSQL2+this.w_ZIPFILE,this.w_CARTELLADIAPPOGGIOPERZIP,"",@L_ERRORE)
          * --- Elimina la cartella di appoggio w_CARTELLADIAPPOGGIOPERZIP
          this.w_FILESYSTEMRESULT = FILESYSTEMOBJ("deletefilesinfolder", this.w_CARTELLADIAPPOGGIOPERZIP, "*.BAK" )
          this.w_FILESYSTEMRESULT = FILESYSTEMOBJ("deletefolder", this.w_CARTELLADIAPPOGGIOPERZIP )
          this.w_RESULT = 0
          * --- Elimino il back up appena zippato...
          if FILESYSTEMOBJ("existfile", this.w_DIRSQL2 +this.w_zipfile )
            this.w_FILESYSTEMRESULT = FILESYSTEMOBJ("deletefile", alltrim(this.oParentObject.w_PATBCK) )
            ah_ErrorMsg("Zip completato","!","")
          else
            ah_ErrorMsg("Zip del database non eseguito. Probabile causa la mancata creazione del file di Backup","!","")
            i_retcode = 'stop'
            return
          endif
        endif
        if this.oParentObject.w_RiorgInd<>"S" AND Upper(This.oParentObject.BaseClass)="FORM"
          * --- Elimino i back up vecchi da non mantenere in linea...
          * --- Analizzo i file all'interno della cartella di back up, se richiesta la puliza
          *     dei back up storici...
          * --- Read from AZBACKUP
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZBACKUP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZBACKUP_idx,2],.t.,this.AZBACKUP_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZBCKDAY,AZBCKNUM"+;
              " from "+i_cTable+" AZBACKUP where ";
                  +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZBCKDAY,AZBCKNUM;
              from (i_cTable) where;
                  AZCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NDAY = NVL(cp_ToDate(_read_.AZBCKDAY),cp_NullValue(_read_.AZBCKDAY))
            this.w_BACKUP = NVL(cp_ToDate(_read_.AZBCKNUM),cp_NullValue(_read_.AZBCKNUM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_RES = GSBACKUP( ADDBS(JUSTPATH(this.oParentObject.w_PATBCK)) , this.w_NDAY, this.w_BACKUP )
          if Not Empty( this.w_RES )
            ah_ErrorMsg("%1","!","",this.w_RES)
            i_retcode = 'stop'
            i_retval = .F.
            return
          endif
          * --- Aggiorno il Path di back Up sull'anagrafica azienda
          This.oParentObject.NotifyEvent("ScriviPath")
        endif
        ah_ErrorMsg("Operazione completata")
        if Upper(This.oParentObject.BaseClass)="FORM"
          * --- Chiudo la maschera..
          This.oParentObject.EcpQuit()
        endif
      endif
    endif
    i_retcode = 'stop'
    i_retval = .t.
    return
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Disattivo sul database il Single user mode
    this.w_VERSIONE = alltrim (SQLXGetDatabaseVersion( this.w_From_Conn ))
    if VAL(LEFT( this.w_VERSIONE ,3))>=11
      * --- In sql 2012 sp_dboptione � deprecato
      this.w_SqlComando = 'ALTER DATABASE "'+Alltrim(this.w_NDBSRG)+'" set MULTI_USER WITH NO_WAIT'
    else
      this.w_SqlComando = "exec sp_dboption N'"+Alltrim(this.w_NDBSRG)+"', N'single', N'false' "
    endif
    ah_Msg("Disabilito <single user mode> sul database",.T.)
    this.w_TmpN = SqlExec(this.w_From_Conn,this.w_SqlComando) * this.w_TmpN
    * --- Riapro la connessione secondaria
     
 cp_OpenSecondConn( this.w_From_Conn )
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='AZBACKUP'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
