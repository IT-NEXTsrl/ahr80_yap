* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bie                                                        *
*              Eliminazione figli inventario                                   *
*                                                                              *
*      Author: TAM Software                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_10]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-08                                                      *
* Last revis.: 2000-09-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bie",oParentObject)
return(i_retval)

define class tgsma_bie as StdBatch
  * --- Local variables
  w_MESS = space(254)
  w_Messaggio = space(6)
  w_NUMINV = space(6)
  * --- WorkFile variables
  INVEDETT_idx=0
  LIFOSCAT_idx=0
  LIFOCONT_idx=0
  FIFOCONT_idx=0
  INVENTAR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione figli inventario
    * --- Variabili anagrafica
    * --- Variabili locali
    * --- Non � ammessa la cancellazione degli inventari Storici perche' sono indispensabili per poter eseguire la ricostruzione dei saldi
    if this.oParentObject.w_INSTAINV = "S"
      * --- Abbandona la Transazione
      ah_ErrorMsg("Non � possibile eliminare gli inventari storici")
      * --- Try
      local bErr_0386D398
      bErr_0386D398=bTrsErr
      this.Try_0386D398()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=ah_msgformat("Eliminazione annullata")
      endif
      bTrsErr=bTrsErr or bErr_0386D398
      * --- End
      i_retcode = 'stop'
      return
    endif
    * --- Non � ammessa la cancellazione di inventari utilizzati per calcolare altri inventari
    this.w_NUMINV = space(6)
    * --- Read from INVENTAR
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.INVENTAR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2],.t.,this.INVENTAR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "INNUMINV"+;
        " from "+i_cTable+" INVENTAR where ";
            +"INNUMPRE = "+cp_ToStrODBC(this.oParentObject.w_INNUMINV);
            +" and INESEPRE = "+cp_ToStrODBC(this.oParentObject.w_INCODESE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        INNUMINV;
        from (i_cTable) where;
            INNUMPRE = this.oParentObject.w_INNUMINV;
            and INESEPRE = this.oParentObject.w_INCODESE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NUMINV = NVL(cp_ToDate(_read_.INNUMINV),cp_NullValue(_read_.INNUMINV))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if not empty(this.w_NUMINV)
      this.w_MESS = "L'inventario non pu� essere eliminato perch� utilizzato per il calcolo di altri inventari"
      ah_ErrorMsg(this.w_MESS,"!")
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_magformat("Eliminazione annullata")
      i_retcode = 'stop'
      return
    endif
    * --- Richiesta della conferma per l'eliminazione dei figli
    if this.oParentObject.w_INELABOR = "S"
      this.w_Messaggio = "ATTENZIONE%0%0Verranno eliminati anche i dati calcolati con l'elaborazione dell'inventario%0%0Confermi la cancellazione?"
      if NOT ah_YesNo(this.w_Messaggio)
        * --- Abbandona la Transazione
        * --- Try
        local bErr_04026FF0
        bErr_04026FF0=bTrsErr
        this.Try_04026FF0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=ah_msgformat("Eliminazione annullata")
        endif
        bTrsErr=bTrsErr or bErr_04026FF0
        * --- End
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Messaggio di elaborazione in corso
    ah_Msg("Eliminazione archivi collegati...")
    * --- Eliminazione
    * --- Try
    local bErr_026DA420
    bErr_026DA420=bTrsErr
    this.Try_026DA420()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Abbandona la Transazione
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_msgformat("Eliminazione annullata")
    endif
    bTrsErr=bTrsErr or bErr_026DA420
    * --- End
  endproc
  proc Try_0386D398()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Raise
    i_Error="Non � possibile eliminare gli inventari storici"
    return
    return
  proc Try_04026FF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Raise
    i_Error="Eliminazione annullata"
    return
    return
  proc Try_026DA420()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- LIFO Continuo
    * --- Delete from LIFOCONT
    i_nConn=i_TableProp[this.LIFOCONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIFOCONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ISNUMINV = "+cp_ToStrODBC(this.oParentObject.w_INNUMINV);
            +" and ISCODESE = "+cp_ToStrODBC(this.oParentObject.w_INCODESE);
             )
    else
      delete from (i_cTable) where;
            ISNUMINV = this.oParentObject.w_INNUMINV;
            and ISCODESE = this.oParentObject.w_INCODESE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- LIFO a Scatti
    * --- Delete from LIFOSCAT
    i_nConn=i_TableProp[this.LIFOSCAT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIFOSCAT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ISNUMINV = "+cp_ToStrODBC(this.oParentObject.w_INNUMINV);
            +" and ISCODESE = "+cp_ToStrODBC(this.oParentObject.w_INCODESE);
             )
    else
      delete from (i_cTable) where;
            ISNUMINV = this.oParentObject.w_INNUMINV;
            and ISCODESE = this.oParentObject.w_INCODESE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- FIFO Continuo
    * --- Delete from FIFOCONT
    i_nConn=i_TableProp[this.FIFOCONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FIFOCONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ISNUMINV = "+cp_ToStrODBC(this.oParentObject.w_INNUMINV);
            +" and ISCODESE = "+cp_ToStrODBC(this.oParentObject.w_INCODESE);
             )
    else
      delete from (i_cTable) where;
            ISNUMINV = this.oParentObject.w_INNUMINV;
            and ISCODESE = this.oParentObject.w_INCODESE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Dettaglio articoli
    * --- Delete from INVEDETT
    i_nConn=i_TableProp[this.INVEDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DINUMINV = "+cp_ToStrODBC(this.oParentObject.w_INNUMINV);
            +" and DICODESE = "+cp_ToStrODBC(this.oParentObject.w_INCODESE);
             )
    else
      delete from (i_cTable) where;
            DINUMINV = this.oParentObject.w_INNUMINV;
            and DICODESE = this.oParentObject.w_INCODESE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='INVEDETT'
    this.cWorkTables[2]='LIFOSCAT'
    this.cWorkTables[3]='LIFOCONT'
    this.cWorkTables[4]='FIFOCONT'
    this.cWorkTables[5]='INVENTAR'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
