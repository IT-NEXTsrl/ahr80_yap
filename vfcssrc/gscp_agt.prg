* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_agt                                                        *
*              Gruppi utente portale                                           *
*                                                                              *
*      Author: Zuccheti S.p.A.                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_21]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-08-28                                                      *
* Last revis.: 2004-09-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscp_agt"))

* --- Class definition
define class tgscp_agt as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 435
  Height = 248+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2004-09-30"
  HelpContextID=42571113
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  ZGRUPPI_IDX = 0
  cFile = "ZGRUPPI"
  cKeySelect = "code"
  cKeyWhere  = "code=this.w_code"
  cKeyWhereODBC = '"code="+cp_ToStrODBC(this.w_code)';

  cKeyWhereODBCqualified = '"ZGRUPPI.code="+cp_ToStrODBC(this.w_code)';

  cPrg = "gscp_agt"
  cComment = "Gruppi utente portale"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- gscp_agt
  Height = 100
  * --- Fine Area Manuale

  * --- Local Variables
  w_code = 0
  w_name = space(50)
  w_grptype = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_code = this.W_code

  * --- Children pointers
  GSCP_MGT = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'ZGRUPPI','gscp_agt')
    stdPageFrame::Init()
    *set procedure to GSCP_MGT additive
    with this
      .Pages(1).addobject("oPag","tgscp_agtPag1","gscp_agt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Gruppi portale")
      .Pages(1).HelpContextID = 99161402
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.ocode_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCP_MGT
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ZGRUPPI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ZGRUPPI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ZGRUPPI_IDX,3]
  return

  function CreateChildren()
    this.GSCP_MGT = CREATEOBJECT('stdDynamicChild',this,'GSCP_MGT',this.oPgFrm.Page1.oPag.oLinkPC_1_6)
    this.GSCP_MGT.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCP_MGT)
      this.GSCP_MGT.DestroyChildrenChain()
      this.GSCP_MGT=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_6')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCP_MGT.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCP_MGT.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCP_MGT.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCP_MGT.SetKey(;
            .w_code,"GRGROUP";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCP_MGT.ChangeRow(this.cRowID+'      1',1;
             ,.w_code,"GRGROUP";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCP_MGT)
        i_f=.GSCP_MGT.BuildFilter()
        if !(i_f==.GSCP_MGT.cQueryFilter)
          i_fnidx=.GSCP_MGT.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCP_MGT.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCP_MGT.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCP_MGT.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCP_MGT.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_code = NVL(code,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from ZGRUPPI where code=KeySet.code
    *
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ZGRUPPI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ZGRUPPI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ZGRUPPI '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'code',this.w_code  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_code = NVL(code,0)
        .op_code = .w_code
        .w_name = NVL(name,space(50))
        .w_grptype = NVL(grptype,space(1))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'ZGRUPPI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_code = 0
      .w_name = space(50)
      .w_grptype = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_grptype = 'O'
      endif
    endwith
    cp_BlankRecExtFlds(this,'ZGRUPPI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"IZCPGRU","i_CODAZI,w_code")
      .op_CODAZI = .w_CODAZI
      .op_code = .w_code
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.ocode_1_1.enabled = i_bVal
      .Page1.oPag.oname_1_3.enabled = i_bVal
      .Page1.oPag.ogrptype_1_4.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.ocode_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.ocode_1_1.enabled = .t.
        .Page1.oPag.oname_1_3.enabled = .t.
      endif
    endwith
    this.GSCP_MGT.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'ZGRUPPI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCP_MGT.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_code,"code",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_name,"name",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_grptype,"grptype",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    i_lTable = "ZGRUPPI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.ZGRUPPI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.ZGRUPPI_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"IZCPGRU","i_CODAZI,w_code")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into ZGRUPPI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ZGRUPPI')
        i_extval=cp_InsertValODBCExtFlds(this,'ZGRUPPI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(code,name,grptype "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_code)+;
                  ","+cp_ToStrODBC(this.w_name)+;
                  ","+cp_ToStrODBC(this.w_grptype)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ZGRUPPI')
        i_extval=cp_InsertValVFPExtFlds(this,'ZGRUPPI')
        cp_CheckDeletedKey(i_cTable,0,'code',this.w_code)
        INSERT INTO (i_cTable);
              (code,name,grptype  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_code;
                  ,this.w_name;
                  ,this.w_grptype;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.ZGRUPPI_IDX,i_nConn)
      *
      * update ZGRUPPI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'ZGRUPPI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " name="+cp_ToStrODBC(this.w_name)+;
             ",grptype="+cp_ToStrODBC(this.w_grptype)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'ZGRUPPI')
        i_cWhere = cp_PKFox(i_cTable  ,'code',this.w_code  )
        UPDATE (i_cTable) SET;
              name=this.w_name;
             ,grptype=this.w_grptype;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCP_MGT : Saving
      this.GSCP_MGT.ChangeRow(this.cRowID+'      1',0;
             ,this.w_code,"GRGROUP";
             )
      this.GSCP_MGT.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCP_MGT : Deleting
    this.GSCP_MGT.ChangeRow(this.cRowID+'      1',0;
           ,this.w_code,"GRGROUP";
           )
    this.GSCP_MGT.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.ZGRUPPI_IDX,i_nConn)
      *
      * delete ZGRUPPI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'code',this.w_code  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ZGRUPPI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ZGRUPPI_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"IZCPGRU","i_CODAZI,w_code")
          .op_code = .w_code
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oLinkPC_1_6.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_6.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.ocode_1_1.value==this.w_code)
      this.oPgFrm.Page1.oPag.ocode_1_1.value=this.w_code
    endif
    if not(this.oPgFrm.Page1.oPag.oname_1_3.value==this.w_name)
      this.oPgFrm.Page1.oPag.oname_1_3.value=this.w_name
    endif
    if not(this.oPgFrm.Page1.oPag.ogrptype_1_4.RadioValue()==this.w_grptype)
      this.oPgFrm.Page1.oPag.ogrptype_1_4.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'ZGRUPPI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      *i_bRes = i_bRes .and. .GSCP_MGT.CheckForm()
      if i_bres
        i_bres=  .GSCP_MGT.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSCP_MGT : Depends On
    this.GSCP_MGT.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscp_agtPag1 as StdContainer
  Width  = 431
  height = 251
  stdWidth  = 431
  stdheight = 251
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object ocode_1_1 as StdField with uid="HJAVLDOHVA",rtseq=1,rtrep=.f.,;
    cFormVar = "w_code", cQueryName = "code",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 35512282,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=66, Top=14, cSayPict='"@Z 999999"', cGetPict='"@Z 999999"'

  add object oname_1_3 as StdField with uid="JPJGKTPMAK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_name", cQueryName = "name",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo...",;
    HelpContextID = 35478826,;
   bGlobalFont=.t.,;
    Height=21, Width=249, Left=126, Top=14, InputMask=replicate('X',50)


  add object ogrptype_1_4 as StdCombo with uid="TDPWZEJQZL",rtseq=3,rtrep=.f.,left=67,top=39,width=144,height=21;
    , ToolTipText = "Tipo gruppo (organizzativo o di condivisione)";
    , HelpContextID = 92398438;
    , cFormVar="w_grptype",RowSource=""+"Organizzativo,"+"Condivisione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func ogrptype_1_4.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func ogrptype_1_4.GetRadio()
    this.Parent.oContained.w_grptype = this.RadioValue()
    return .t.
  endfunc

  func ogrptype_1_4.SetRadio()
    this.Parent.oContained.w_grptype=trim(this.Parent.oContained.w_grptype)
    this.value = ;
      iif(this.Parent.oContained.w_grptype=='O',1,;
      iif(this.Parent.oContained.w_grptype=='C',2,;
      0))
  endfunc


  add object oLinkPC_1_6 as stdDynamicChildContainer with uid="RKPUGJIUEF",left=5, top=67, width=425, height=184, bOnScreen=.t.;


  func oLinkPC_1_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_grptype $ 'O-C')
     endwith
    endif
  endfunc

  add object oStr_1_2 as StdString with uid="MMSZUYDXXZ",Visible=.t., Left=-7, Top=14,;
    Alignment=1, Width=67, Height=18,;
    Caption="Gruppo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="QEOAFIQOMH",Visible=.t., Left=4, Top=39,;
    Alignment=1, Width=57, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscp_agt','ZGRUPPI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".code=ZGRUPPI.code";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
