* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gs___bnu                                                        *
*              New utente                                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-29                                                      *
* Last revis.: 2005-03-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODE,pMOD
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgs___bnu",oParentObject,m.pCODE,m.pMOD)
return(i_retval)

define class tgs___bnu as StdBatch
  * --- Local variables
  pCODE = 0
  pMOD = .f.
  w_DATAGG = ctod("  /  /  ")
  w_ATTIVO = space(1)
  w_MESS = space(100)
  * --- WorkFile variables
  POL_SIC_idx=0
  POL_UTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch scatta alla creazione di un nuovo utente, il batch � sotto transazione
    *     per bloccare la creazione in caso di errore occore utilizzare una 
    *     "Transaction Error"
    * --- Parametri:
    *     pCODE = Codice utente da cancellare
    *     pMOD = .t. modifica utente, .f. nuovo utente
    * --- --
    this.w_DATAGG = DATE()
    * --- Read from POL_SIC
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.POL_SIC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.POL_SIC_idx,2],.t.,this.POL_SIC_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PSATTIVO"+;
        " from "+i_cTable+" POL_SIC where ";
            +"PSSERIAL = "+cp_ToStrODBC("AHE");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PSATTIVO;
        from (i_cTable) where;
            PSSERIAL = "AHE";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ATTIVO = NVL(cp_ToDate(_read_.PSATTIVO),cp_NullValue(_read_.PSATTIVO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.pMOD
      * --- Modifica utente
      if this.w_ATTIVO = "S"
        * --- Try
        local bErr_00F3D188
        bErr_00F3D188=bTrsErr
        this.Try_00F3D188()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_MESS = AH_MSGFORMAT("Impossibile aggiornare l'utente della gestione politiche di sicurezza")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
        bTrsErr=bTrsErr or bErr_00F3D188
        * --- End
      endif
    else
      * --- Nuovo utente
      if this.w_ATTIVO = "S"
        * --- Try
        local bErr_035E9EB8
        bErr_035E9EB8=bTrsErr
        this.Try_035E9EB8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_MESS = AH_MSGFORMAT("'Impossibile aggiungere il nuovo utente nella gestione politiche di sicurezza")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
        bTrsErr=bTrsErr or bErr_035E9EB8
        * --- End
      endif
    endif
    * --- Affinch� il batch non ritorni errore "Property MCALLEDBATCHEND is not found"
    *     devo porre bUpdateParentObject a false
    this.bUpdateParentObject = .F.
  endproc
  proc Try_00F3D188()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into POL_UTE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.POL_UTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.POL_UTE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PUDATULT ="+cp_NullLink(cp_ToStrODBC(this.w_DATAGG),'POL_UTE','PUDATULT');
          +i_ccchkf ;
      +" where ";
          +"PUCODUTE = "+cp_ToStrODBC(this.pCODE);
             )
    else
      update (i_cTable) set;
          PUDATULT = this.w_DATAGG;
          &i_ccchkf. ;
       where;
          PUCODUTE = this.pCODE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_035E9EB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into POL_UTE
    i_nConn=i_TableProp[this.POL_UTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.POL_UTE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PUCODUTE"+",PUDATULT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.pCODE),'POL_UTE','PUCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATAGG),'POL_UTE','PUDATULT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PUCODUTE',this.pCODE,'PUDATULT',this.w_DATAGG)
      insert into (i_cTable) (PUCODUTE,PUDATULT &i_ccchkf. );
         values (;
           this.pCODE;
           ,this.w_DATAGG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pCODE,pMOD)
    this.pCODE=pCODE
    this.pMOD=pMOD
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='POL_SIC'
    this.cWorkTables[2]='POL_UTE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODE,pMOD"
endproc
