* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mpn                                                        *
*              Primanota                                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_653]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-20                                                      *
* Last revis.: 2016-11-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gscg_mpn
* --- Inizializza Picture
VVP=20*g_PERPVL

* --- Fine Area Manuale
return(createobject("tgscg_mpn"))

* --- Class definition
define class tgscg_mpn as StdTrsForm
  Top    = 2
  Left   = 8

  * --- Standard Properties
  Width  = 789
  Height = 535+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-11-24"
  HelpContextID=120969879
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=250

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  PNT_MAST_IDX = 0
  PNT_DETT_IDX = 0
  ESERCIZI_IDX = 0
  PAR_TITE_IDX = 0
  CAU_CONT_IDX = 0
  MOVICOST_IDX = 0
  VALUTE_IDX = 0
  CAM_BI_IDX = 0
  CONTI_IDX = 0
  PAG_AMEN_IDX = 0
  SALDICON_IDX = 0
  PNT_IVA_IDX = 0
  COC_MAST_IDX = 0
  AGENTI_IDX = 0
  AZIENDA_IDX = 0
  BAN_CONTI_IDX = 0
  OPERSUPE_IDX = 0
  DATI_AGG_IDX = 0
  cFile = "PNT_MAST"
  cFileDetail = "PNT_DETT"
  cKeySelect = "PNSERIAL"
  cKeyWhere  = "PNSERIAL=this.w_PNSERIAL"
  cKeyDetail  = "PNSERIAL=this.w_PNSERIAL"
  cKeyWhereODBC = '"PNSERIAL="+cp_ToStrODBC(this.w_PNSERIAL)';

  cKeyDetailWhereODBC = '"PNSERIAL="+cp_ToStrODBC(this.w_PNSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"PNT_DETT.PNSERIAL="+cp_ToStrODBC(this.w_PNSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PNT_DETT.CPROWORD '
  cPrg = "gscg_mpn"
  cComment = "Primanota"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  cAutoZoom = 'GSCG0MPN'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PNSERIAL = space(10)
  o_PNSERIAL = space(10)
  w_CHKAUT = 0
  w_PNCODESE = space(4)
  w_VALATT = space(10)
  w_TABKEY = space(8)
  w_DELIMM = .F.
  w_CODAZI = space(5)
  w_AZDATDOC = space(1)
  w_PNNUMRER = 0
  w_STAMPATO = space(1)
  w_STAMPRCV = space(1)
  w_PNCODUTE = 0
  w_PNDATREG = ctod('  /  /  ')
  o_PNDATREG = ctod('  /  /  ')
  w_FLCOMP = space(1)
  w_PNCODCAU = space(5)
  o_PNCODCAU = space(5)
  w_PNCOMPET = space(4)
  o_PNCOMPET = space(4)
  w_PNPRG = space(8)
  w_PNVALNAZ = space(3)
  o_PNVALNAZ = space(3)
  w_CAONAZ = 0
  w_DECTOP = 0
  w_CALCPICP = 0
  w_DESCAU = space(35)
  w_SALDOC = 0
  w_SALPRO = 0
  w_INIESE = space(8)
  w_FINESE = ctod('  /  /  ')
  w_FLSALI = space(1)
  w_FLSALF = space(1)
  w_FLPART = space(1)
  w_FLRIFE = space(1)
  w_PNTIPCLF = space(1)
  o_PNTIPCLF = space(1)
  w_PNFLIVDF = space(1)
  w_PNTIPREG = space(1)
  o_PNTIPREG = space(1)
  w_PNNUMREG = 0
  w_PNTIPDOC = space(2)
  w_FLNDOC = space(1)
  w_CALDOC = space(1)
  w_TESDOC = space(1)
  w_PNPRD = space(2)
  o_PNPRD = space(2)
  w_PNPRP = space(2)
  o_PNPRP = space(2)
  w_TESTPART = space(1)
  w_CFDAVE = space(1)
  w_FLPDOC = space(1)
  w_FLPPRO = space(1)
  w_PNALFDOC = space(10)
  o_PNALFDOC = space(10)
  w_PNNUMDOC = 0
  o_PNNUMDOC = 0
  w_PNDATDOC = ctod('  /  /  ')
  o_PNDATDOC = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  o_OBTEST = ctod('  /  /  ')
  w_PNANNDOC = space(4)
  o_PNANNDOC = space(4)
  w_PNANNPRO = space(4)
  o_PNANNPRO = space(4)
  w_PNFLREGI = space(1)
  w_PNDESSUP = space(50)
  o_PNDESSUP = space(50)
  w_PN__MESE = 0
  w_PN__ANNO = 0
  w_PNALFPRO = space(10)
  o_PNALFPRO = space(10)
  w_PNNUMPRO = 0
  o_PNNUMPRO = 0
  w_PNCODCLF = space(15)
  o_PNCODCLF = space(15)
  w_PNCOMIVA = ctod('  /  /  ')
  w_PNDATPLA = ctod('  /  /  ')
  w_PNCODVAL = space(3)
  o_PNCODVAL = space(3)
  w_CAOVAL = 0
  w_DECTOT = 0
  w_CALCPICT = 0
  w_PNCAOVAL = 0
  w_SIMVAL = space(5)
  w_DESCLF = space(36)
  w_CLFVAL = space(3)
  o_CLFVAL = space(3)
  w_PAGCLF = space(5)
  w_DATMOR = ctod('  /  /  ')
  w_PAGMOR = space(5)
  w_CODPAG = space(5)
  w_DTOBSO = ctod('  /  /  ')
  w_DESPAG = space(30)
  w_PNTOTDOC = 0
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_TOTFLAG = 0
  w_GIACAR = space(1)
  w_ABILITA = .F.
  w_MOVCES = space(1)
  w_ODATREG = ctod('  /  /  ')
  w_OCOMIVA = ctod('  /  /  ')
  w_PNFLPROV = space(1)
  o_PNFLPROV = space(1)
  w_IMPDCP = 0
  w_IMPDCA = 0
  w_IMPABB = 0
  w_IMPANT = 0
  w_CCOBSO = ctod('  /  /  ')
  w_FLANAL = space(1)
  w_DESAPP = space(35)
  w_PNRIFDOC = space(10)
  w_RESCHK = 0
  w_PNRIFDIS = space(10)
  w_FLAGAC = space(1)
  w_FLPDIF = space(1)
  w_PNFLGDIF = space(1)
  w_PNRIFINC = space(10)
  w_PNRIFCES = space(1)
  w_PNTIPCON = space(1)
  o_PNTIPCON = space(1)
  w_CPROWORD = 0
  w_PNDESRIG = space(50)
  w_PNCAURIG = space(5)
  o_PNCAURIG = space(5)
  w_RIGPAR = space(1)
  w_CAUDES = space(35)
  w_PNCODCON = space(15)
  o_PNCODCON = space(15)
  w_DESCON = space(30)
  w_MASTRO = space(15)
  w_SEZB = space(1)
  o_SEZB = space(1)
  w_DESCO2 = space(30)
  w_PNFLSALD = space(1)
  w_PARTSN = space(1)
  w_PNFLSALI = space(1)
  w_CLMES1 = 0
  w_PNFLSALF = space(1)
  w_CLMES2 = 0
  w_CLGIO1 = 0
  w_CLGIO2 = 0
  w_GIOFIS = 0
  w_PAGCLF1 = space(5)
  w_PAGMOR1 = space(5)
  w_DATMOR1 = ctod('  /  /  ')
  w_PNCODPAG = space(5)
  w_PNFLZERO = space(1)
  w_CCTAGG = space(1)
  w_PNIMPDAR = 0
  o_PNIMPDAR = 0
  w_PNIMPAVE = 0
  o_PNIMPAVE = 0
  w_IMPDAR = 0
  w_SLIMPDAR = 0
  w_IMPAVE = 0
  w_SLIMPAVE = 0
  w_FLAGTOT = space(1)
  w_FLDAVE = space(1)
  w_PNCODCON = space(15)
  w_BANAPP = space(10)
  w_PARSAL = space(1)
  w_FLAPAR = space(1)
  w_PNFLPART = space(1)
  o_PNFLPART = space(1)
  w_PARCAR = space(1)
  w_PNFLABAN = space(6)
  w_CDCSAL = space(1)
  w_PNINICOM = ctod('  /  /  ')
  w_NUMCOR = space(25)
  w_PNFINCOM = ctod('  /  /  ')
  w_TIPSOT = space(1)
  o_TIPSOT = space(1)
  w_OANAL = space(1)
  w_BANNOS = space(15)
  w_PNIMPIND = 0
  w_VARPAR = space(1)
  w_VARCEN = space(1)
  w_TOTDAR = 0
  w_TOTAVE = 0
  w_SBILAN = 0
  w_PNRIFACC = space(1)
  w_PARIVA = space(12)
  w_ONUMDOC = 0
  w_ONUMPRO = 0
  w_OALFDOC = space(10)
  w_OALFPRO = space(10)
  w_PNNUMTRA = 0
  w_DATOBSO = ctod('  /  /  ')
  w_DTVOBSO = ctod('  /  /  ')
  w_CAUMOV = space(5)
  w_CONBAN = space(15)
  w_CALFES = space(3)
  w_FLMOVC = space(1)
  o_FLMOVC = space(1)
  w_BAVAL = space(3)
  w_FLASMO = space(1)
  w_CONIVA = space(15)
  w_PNNUMTR2 = 0
  w_AGECLF = space(5)
  w_PNCODAGE = space(5)
  w_PNFLVABD = space(1)
  w_FLACBD = space(1)
  w_FLVEBD = space(1)
  w_FLAACC = space(1)
  w_SLAINI = 0
  w_SLDPRO = 0
  w_SLDPER = 0
  w_SLDFIN = 0
  w_SLAPRO = 0
  w_SLAPER = 0
  w_SLAFIN = 0
  w_SLDINI = 0
  w_PCODCON = space(10)
  o_PCODCON = space(10)
  w_SALDO = 0
  w_DESCRI = space(30)
  w_CAUCES = space(5)
  w_OLCUTE = 0
  w_OLCESE = space(4)
  w_OLPRG = space(8)
  w_MODNRE = .F.
  w_MODNDO = .F.
  w_OLANDO = space(4)
  w_OLPRD = space(2)
  w_OLALDO = space(2)
  w_HASEVCOP = space(15)
  w_HASEVENT = .F.
  w_PNRIFSAL = space(10)
  w_DETCON = space(1)
  w_PNFLGSTO = space(1)
  w_GESRIT = space(1)
  w_RITENU = space(1)
  w_PCCLICK = .F.
  w_FLGSTO = space(1)
  w_TOTFOR = 0
  w_NUMACC = 0
  w_TOTACC = 0
  w_IMPPDP = 0
  w_IMPPDA = 0
  w_FLVEAC = space(1)
  w_IMPFOR = 0
  w_PNSCRASS = space(1)
  w_NOACC = .F.
  w_PNTOTENA = 0
  w_PAGINS = space(5)
  w_FLINSO = space(1)
  w_CCDESSUP = space(254)
  w_CCDESRIG = space(254)
  w_TESTRITE = .F.
  w_FLCHKNUMD = .F.
  w_FLCHKPRO = .F.
  w_FLGRIT = space(1)
  w_CCCONIVA = space(15)
  w_REGMARG = space(1)
  w_IMPABP = 0
  w_IMPABA = 0
  w_TIPOOLD = space(10)
  w_ANFLSOAL = space(1)
  w_DATINITRA = ctod('  /  /  ')
  w_OSSERIAL = space(10)
  w_OSTIPORE = space(1)
  w_ANOPETRE = space(1)
  w_PNAGG_01 = space(15)
  w_PNAGG_02 = space(15)
  w_PNAGG_03 = space(15)
  w_PNAGG_04 = space(15)
  w_PNAGG_05 = ctod('  /  /  ')
  w_PNAGG_06 = ctod('  /  /  ')
  w_DASERIAL = space(10)
  w_DACAM_01 = space(30)
  w_DACAM_02 = space(30)
  w_DACAM_04 = space(30)
  w_DACAM_03 = space(30)
  w_DACAM_05 = space(30)
  w_DACAM_06 = space(30)
  w_SEARCHACC = space(1)
  w_OLDSERIAL = space(10)
  w_PNNUMFAT = space(50)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_PNSERIAL = this.W_PNSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_PNCODESE = this.W_PNCODESE
  op_PNCODUTE = this.W_PNCODUTE
  op_PNPRG = this.W_PNPRG
  op_PNNUMRER = this.W_PNNUMRER
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_PNANNDOC = this.W_PNANNDOC
  op_PNPRD = this.W_PNPRD
  op_PNALFDOC = this.W_PNALFDOC
  op_PNNUMDOC = this.W_PNNUMDOC
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_PNANNPRO = this.W_PNANNPRO
  op_PNPRP = this.W_PNPRP
  op_PNALFPRO = this.W_PNALFPRO
  op_PNNUMPRO = this.W_PNNUMPRO

  * --- Children pointers
  GSCG_MIV = .NULL.
  GSCG_MAC = .NULL.
  GSCG_MCA = .NULL.
  GSCG_MPA = .NULL.
  GSCG_MMC = .NULL.
  GSCG_MDR = .NULL.
  GSCG_ASA = .NULL.
  GSCG_AOS = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscg_mpn
  *--Riferimento Anagrafica Contabilizzazione Indiretta(GSCG_ACI)
  *--Utilizzata alla Record Inserted per lanciare il refresh della Maschera
  w_RIFGSCGACI=.NULL.
  w_AGGPRO=.F.
  DIMENSION AACC[1,10]
  *-- Inibisce la cancellazione delle Righe da Contab.Indiretta
  *--Riportare i controlli legati all'F6 nel batch del calcolo automatismo (GSCG_BP3)
  Proc F6()
      if NOT EMPTY(this.w_PNRIFDIS) AND VAL(this.w_PNRIFDIS)<0
        AH_ERRORMSG("Impossibile eliminare righe di registrazioni da contabilizzazione indiretta")
         return
      endif
      DoDefault()
  EndProc
  
  
  * ridefinisco la ECPDELETE per chiudere
  * la gestione nel caso dia stata invocata la cancellazione
  * da contabilizzazione indiretta
  Proc EcpDelete()
  DoDefault()
  *-- Propriet� inizializzata dopo la LOADREC in GSCG_BZP
  IF Type('THIS.w_RIFGSCGACI')='O'
   IF  upper(THIS.w_RIFGSCGACI.class)="TGSCG_ACI" &&upper(THIS.w_RIFGSCGACI.class)="TGSCG_MSC" or
    THIS.w_RIFGSCGACI.NOTIFYEVENT('Legge')
    this.ecpquit()
    return
   Endif
  Endif
  Endproc
  
    * Solo in cancellazione/modifica, disabilito se registrazione
    * non � modificabile (gestisco anche le richieste utente)
  
    Func ah_HasCPEvents(i_cOp)	
  	If (this.cFunction='Query' And (Upper(i_cop)='ECPDELETE' or Upper(i_cop)='ECPEDIT')) And Not Empty(this.w_PNSERIAL)
        if (Upper(i_cop)='ECPEDIT' And (seconds()>this.nLoadTime+60 or this.bupdated)) Or(Upper(i_cop)='ECPDELETE' And this.nLoadTime=0)
          this.LoadRecWarn()  && rilettura se sono passati piu' di 60 secondi dall' ultima lettura o il record � stato variato in Interroga
        ENDIF
  		This.w_HASEVCOP=i_cop	
  		this.NotifyEvent('HasEvent')
  		return(this.w_HASEVENT)
  	Else
  		return(.t.)	
  	endif
    EndFunc
  
    Proc Activate()
      DoDefault()
      =DEFPIP(THIS.w_DECTOP)
      =DEFPIC(THIS.w_DECTOT)
    EndProc
  
  * Modulo Ritenute
  * Azzero la variabile utilizzata per poter controllare
  * il totale del documento
  w_TOTMDR=0
  
  Proc Ecpquit()
  DoDefault()
  IF vartype (this.gscg_mpa)='O' AND vartype (this.gscg_mpa.cnt)='O'
  this.gscg_mpa.cnt.w_TIPPAG=''
  ENDIF
  Endproc
  
  
  
  ** Aggiunta la funzione AddSonsFilter per gestire i filtri F12 anche sui campi del castelletto
   function AddSonsFilter(i_cFlt,i_oTopObject)
      local i_f,i_fnidx,i_cDatabaseType
      with this
        if !IsNull(.GSCG_MIV)
          i_f=.GSCG_MIV.BuildFilter()
          if !(i_f==.GSCG_MIV.cQueryFilter)
            i_fnidx=.GSCG_MIV.cFile+'_IDX'
            i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_MIV.&i_fnidx.,5],6]
            i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_MIV.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_MIV.&i_fnidx.,2])+' where '+i_f+')'
          else
            i_f=''
          endif
          i_f=.GSCG_MIV.AddSonsFilter(i_f,i_oTopObject)
          if !empty(i_f)
            i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
          endif
        endif
      ENDWITH
      return(i_cFlt)
      endfunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PNT_MAST','gscg_mpn')
    stdPageFrame::Init()
    *set procedure to GSCG_MIV additive
    *set procedure to GSCG_MAC additive
    *set procedure to GSCG_MCA additive
    *set procedure to GSCG_MPA additive
    *set procedure to GSCG_MMC additive
    *set procedure to GSCG_MDR additive
    *set procedure to GSCG_ASA additive
    *set procedure to GSCG_AOS additive
    with this
      .Pages(1).addobject("oPag","tgscg_mpnPag1","gscg_mpn",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Primanota")
      .Pages(1).HelpContextID = 71702406
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCG_MIV
    *release procedure GSCG_MAC
    *release procedure GSCG_MCA
    *release procedure GSCG_MPA
    *release procedure GSCG_MMC
    *release procedure GSCG_MDR
    *release procedure GSCG_ASA
    *release procedure GSCG_AOS
    * --- Area Manuale = Init Page Frame
    * --- gscg_mpn
      if VARTYPE(g_SCHEDULER)='C' AND g_SCHEDULER='S'
       this.parent.left=_screen.width+1
      endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[18]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='PAR_TITE'
    this.cWorkTables[3]='CAU_CONT'
    this.cWorkTables[4]='MOVICOST'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='CAM_BI'
    this.cWorkTables[7]='CONTI'
    this.cWorkTables[8]='PAG_AMEN'
    this.cWorkTables[9]='SALDICON'
    this.cWorkTables[10]='PNT_IVA'
    this.cWorkTables[11]='COC_MAST'
    this.cWorkTables[12]='AGENTI'
    this.cWorkTables[13]='AZIENDA'
    this.cWorkTables[14]='BAN_CONTI'
    this.cWorkTables[15]='OPERSUPE'
    this.cWorkTables[16]='DATI_AGG'
    this.cWorkTables[17]='PNT_MAST'
    this.cWorkTables[18]='PNT_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(18))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PNT_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PNT_MAST_IDX,3]
  return

  function CreateChildren()
    this.GSCG_MIV = CREATEOBJECT('stdDynamicChild',this,'GSCG_MIV',this.oPgFrm.Page1.oPag.oLinkPC_1_95)
    this.GSCG_MIV.createrealchild()
    this.GSCG_MAC = CREATEOBJECT('stdLazyChild',this,'GSCG_MAC')
    this.GSCG_MCA = CREATEOBJECT('stdLazyChild',this,'GSCG_MCA')
    this.GSCG_MPA = CREATEOBJECT('stdLazyChild',this,'GSCG_MPA')
    this.GSCG_MMC = CREATEOBJECT('stdLazyChild',this,'GSCG_MMC')
    this.GSCG_MDR = CREATEOBJECT('stdLazyChild',this,'GSCG_MDR')
    this.GSCG_ASA = CREATEOBJECT('stdLazyChild',this,'GSCG_ASA')
    this.GSCG_AOS = CREATEOBJECT('stdLazyChild',this,'GSCG_AOS')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCG_MIV)
      this.GSCG_MIV.DestroyChildrenChain()
      this.GSCG_MIV=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_95')
    if !ISNULL(this.GSCG_MAC)
      this.GSCG_MAC.DestroyChildrenChain()
      this.GSCG_MAC=.NULL.
    endif
    if !ISNULL(this.GSCG_MCA)
      this.GSCG_MCA.DestroyChildrenChain()
      this.GSCG_MCA=.NULL.
    endif
    if !ISNULL(this.GSCG_MPA)
      this.GSCG_MPA.DestroyChildrenChain()
      this.GSCG_MPA=.NULL.
    endif
    if !ISNULL(this.GSCG_MMC)
      this.GSCG_MMC.DestroyChildrenChain()
      this.GSCG_MMC=.NULL.
    endif
    if !ISNULL(this.GSCG_MDR)
      this.GSCG_MDR.DestroyChildrenChain()
      this.GSCG_MDR=.NULL.
    endif
    if !ISNULL(this.GSCG_ASA)
      this.GSCG_ASA.DestroyChildrenChain()
      this.GSCG_ASA=.NULL.
    endif
    if !ISNULL(this.GSCG_AOS)
      this.GSCG_AOS.DestroyChildrenChain()
      this.GSCG_AOS=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_MIV.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_MAC.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_MCA.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_MPA.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_MMC.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_MDR.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_ASA.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_AOS.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_MIV.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_MAC.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_MCA.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_MPA.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_MMC.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_MDR.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_ASA.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_AOS.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_MIV.NewDocument()
    this.GSCG_MAC.NewDocument()
    this.GSCG_MCA.NewDocument()
    this.GSCG_MPA.NewDocument()
    this.GSCG_MMC.NewDocument()
    this.GSCG_MDR.NewDocument()
    this.GSCG_ASA.NewDocument()
    this.GSCG_AOS.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSCG_MIV.ChangeRow(this.cRowID+'      1',1;
             ,.w_PNSERIAL,"IVSERIAL";
             )
      .GSCG_MAC.ChangeRow(this.cRowID+'      1',1;
             ,.w_PNSERIAL,"ACSERIAL";
             )
      .GSCG_MCA.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_PNSERIAL,"MRSERIAL";
             ,.w_CPROWNUM,"MRROWORD";
             )
      .GSCG_MPA.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_PNSERIAL,"PTSERIAL";
             ,.w_CPROWNUM,"PTROWORD";
             )
      .GSCG_MMC.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_PNSERIAL,"CCSERIAL";
             ,.w_CPROWNUM,"CCROWRIF";
             )
      .GSCG_MDR.ChangeRow(this.cRowID+'      1',1;
             ,.w_PNSERIAL,"DRSERIAL";
             )
      .GSCG_ASA.ChangeRow(this.cRowID+'      1',1;
             ,.w_PNSERIAL,"SASERIAL";
             )
      .GSCG_AOS.ChangeRow(this.cRowID+'      1',1;
             ,.w_PNSERIAL,"OSSERIAL";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_PNSERIAL = NVL(PNSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_59_joined
    link_1_59_joined=.f.
    local link_1_62_joined
    link_1_62_joined=.f.
    local link_2_4_joined
    link_2_4_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    local link_2_24_joined
    link_2_24_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from PNT_MAST where PNSERIAL=KeySet.PNSERIAL
    *
    i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2],this.bLoadRecFilter,this.PNT_MAST_IDX,"gscg_mpn")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PNT_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PNT_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"PNT_DETT.","PNT_MAST.")
      i_cTable = i_cTable+' PNT_MAST '
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_59_joined=this.AddJoinedLink_1_59(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_62_joined=this.AddJoinedLink_1_62(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PNSERIAL',this.w_PNSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CHKAUT = 0
        .w_VALATT = .w_PNSERIAL
        .w_TABKEY = 'PNT_MAST'
        .w_DELIMM = .f.
        .w_AZDATDOC = space(1)
        .w_STAMPATO = 'N'
        .w_STAMPRCV = 'N'
        .w_FLCOMP = space(1)
        .w_CAONAZ = 0
        .w_DECTOP = 0
        .w_DESCAU = space(35)
        .w_SALDOC = 0
        .w_SALPRO = 0
        .w_INIESE = space(8)
        .w_FINESE = ctod("  /  /  ")
        .w_FLSALI = space(1)
        .w_FLSALF = space(1)
        .w_FLPART = space(1)
        .w_FLRIFE = space(1)
        .w_FLNDOC = space(1)
        .w_CALDOC = space(1)
        .w_TESDOC = space(1)
        .w_CFDAVE = space(1)
        .w_FLPDOC = space(1)
        .w_FLPPRO = space(1)
        .w_CAOVAL = 0
        .w_DECTOT = 0
        .w_SIMVAL = space(5)
        .w_DESCLF = space(36)
        .w_CLFVAL = space(3)
        .w_PAGCLF = space(5)
        .w_DATMOR = ctod("  /  /  ")
        .w_PAGMOR = space(5)
        .w_DTOBSO = ctod("  /  /  ")
        .w_DESPAG = space(30)
        .w_GIACAR = ' '
        .w_ABILITA = .f.
        .w_MOVCES = space(1)
        .w_IMPDCP = 0
        .w_IMPDCA = 0
        .w_IMPABB = 0
        .w_IMPANT = 0
        .w_CCOBSO = ctod("  /  /  ")
        .w_FLANAL = space(1)
        .w_DESAPP = space(35)
        .w_RESCHK = 0
        .w_FLPDIF = space(1)
        .w_TOTDAR = 0
        .w_TOTAVE = 0
        .w_PARIVA = space(12)
        .w_DATOBSO = ctod("  /  /  ")
        .w_DTVOBSO = ctod("  /  /  ")
        .w_FLMOVC = space(1)
        .w_CONIVA = space(15)
        .w_FLACBD = space(1)
        .w_FLVEBD = space(1)
        .w_FLAACC = space(1)
        .w_CAUCES = space(5)
        .w_OLCUTE = 0
        .w_OLCESE = space(4)
        .w_OLPRG = space(8)
        .w_MODNRE = .f.
        .w_MODNDO = .f.
        .w_OLANDO = space(4)
        .w_OLPRD = space(2)
        .w_OLALDO = space(2)
        .w_HASEVCOP = space(15)
        .w_HASEVENT = .f.
        .w_DETCON = space(1)
        .w_GESRIT = space(1)
        .w_RITENU = space(1)
        .w_PCCLICK = .F.
        .w_TOTFOR = 0
        .w_NUMACC = 0
        .w_TOTACC = 0
        .w_IMPPDP = 0
        .w_IMPPDA = 0
        .w_NOACC = .F.
        .w_PAGINS = space(5)
        .w_FLINSO = space(1)
        .w_CCDESSUP = space(254)
        .w_CCDESRIG = space(254)
        .w_TESTRITE = .T.
        .w_FLCHKNUMD = .f.
        .w_FLCHKPRO = .f.
        .w_FLGRIT = space(1)
        .w_CCCONIVA = space(15)
        .w_REGMARG = space(1)
        .w_IMPABP = 0
        .w_IMPABA = 0
        .w_TIPOOLD = space(10)
        .w_ANFLSOAL = space(1)
        .w_DATINITRA = ctod("  /  /  ")
        .w_OSTIPORE = space(1)
        .w_ANOPETRE = space(1)
        .w_DASERIAL = '0000000001'
        .w_DACAM_01 = space(30)
        .w_DACAM_02 = space(30)
        .w_DACAM_04 = space(30)
        .w_DACAM_03 = space(30)
        .w_DACAM_05 = space(30)
        .w_DACAM_06 = space(30)
        .w_SEARCHACC = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_PNSERIAL = NVL(PNSERIAL,space(10))
        .op_PNSERIAL = .w_PNSERIAL
        .w_PNCODESE = NVL(PNCODESE,space(4))
        .op_PNCODESE = .w_PNCODESE
        .w_CODAZI = i_CODAZI
          .link_1_8('Load')
        .w_PNNUMRER = NVL(PNNUMRER,0)
        .op_PNNUMRER = .w_PNNUMRER
        .w_PNCODUTE = NVL(PNCODUTE,0)
        .op_PNCODUTE = .w_PNCODUTE
        .w_PNDATREG = NVL(cp_ToDate(PNDATREG),ctod("  /  /  "))
        .w_PNCODCAU = NVL(PNCODCAU,space(5))
          if link_1_16_joined
            this.w_PNCODCAU = NVL(CCCODICE116,NVL(this.w_PNCODCAU,space(5)))
            this.w_DESCAU = NVL(CCDESCRI116,space(35))
            this.w_FLPART = NVL(CCFLPART116,space(1))
            this.w_FLSALI = NVL(CCFLSALI116,space(1))
            this.w_FLSALF = NVL(CCFLSALF116,space(1))
            this.w_PNFLIVDF = NVL(CCFLIVDF116,space(1))
            this.w_FLCOMP = NVL(CCFLCOMP116,space(1))
            this.w_PNTIPDOC = NVL(CCTIPDOC116,space(2))
            this.w_PNTIPREG = NVL(CCTIPREG116,space(1))
            this.w_PNNUMREG = NVL(CCNUMREG116,0)
            this.w_FLNDOC = NVL(CCNUMDOC116,space(1))
            this.w_PNALFPRO = NVL(CCSERPRO116,space(10))
            this.w_CFDAVE = NVL(CCCFDAVE116,space(1))
            this.w_CALDOC = NVL(CCCALDOC116,space(1))
            this.w_TESDOC = NVL(CCTESDOC116,space(1))
            this.w_FLANAL = NVL(CCFLANAL116,space(1))
            this.w_FLRIFE = NVL(CCFLRIFE116,space(1))
            this.w_CCOBSO = NVL(cp_ToDate(CCDTOBSO116),ctod("  /  /  "))
            this.w_PNALFDOC = NVL(CCSERDOC116,space(10))
            this.w_FLPDOC = NVL(CCFLPDOC116,space(1))
            this.w_FLPPRO = NVL(CCFLPPRO116,space(1))
            this.w_FLPDIF = NVL(CCFLPDIF116,space(1))
            this.w_FLMOVC = NVL(CCFLMOVC116,space(1))
            this.w_CONIVA = NVL(CCCONIVA116,space(15))
            this.w_MOVCES = NVL(CCMOVCES116,space(1))
            this.w_CAUCES = NVL(CCCAUCES116,space(5))
            this.w_GESRIT = NVL(CCGESRIT116,space(1))
            this.w_PAGINS = NVL(CCPAGINS116,space(5))
            this.w_FLINSO = NVL(CCFLINSO116,space(1))
            this.w_CCDESSUP = NVL(CCDESSUP116,space(254))
            this.w_CCDESRIG = NVL(CCDESRIG116,space(254))
            this.w_CCCONIVA = NVL(CCCONIVA116,space(15))
            this.w_REGMARG = NVL(CCREGMAR116,space(1))
          else
          .link_1_16('Load')
          endif
        .w_PNCOMPET = NVL(PNCOMPET,space(4))
          .link_1_17('Load')
        .w_PNPRG = NVL(PNPRG,space(8))
        .op_PNPRG = .w_PNPRG
        .w_PNVALNAZ = NVL(PNVALNAZ,space(3))
          if link_1_19_joined
            this.w_PNVALNAZ = NVL(VACODVAL119,NVL(this.w_PNVALNAZ,space(3)))
            this.w_CAONAZ = NVL(VACAOVAL119,0)
            this.w_DECTOP = NVL(VADECTOT119,0)
          else
          .link_1_19('Load')
          endif
        .w_CALCPICP = DEFPIP(.w_DECTOP)
        .w_PNTIPCLF = NVL(PNTIPCLF,space(1))
        .w_PNFLIVDF = NVL(PNFLIVDF,space(1))
        .w_PNTIPREG = NVL(PNTIPREG,space(1))
        .w_PNNUMREG = NVL(PNNUMREG,0)
        .w_PNTIPDOC = NVL(PNTIPDOC,space(2))
        .w_PNPRD = NVL(PNPRD,space(2))
        .op_PNPRD = .w_PNPRD
        .w_PNPRP = NVL(PNPRP,space(2))
        .op_PNPRP = .w_PNPRP
        .w_TESTPART = IIF(g_PERPAR="S", .w_FLPART, "N")
        .w_PNALFDOC = NVL(PNALFDOC,space(10))
        .op_PNALFDOC = .w_PNALFDOC
        .w_PNNUMDOC = NVL(PNNUMDOC,0)
        .op_PNNUMDOC = .w_PNNUMDOC
        .w_PNDATDOC = NVL(cp_ToDate(PNDATDOC),ctod("  /  /  "))
        .w_OBTEST = MIN(IIF(EMPTY(.w_PNDATDOC),i_FINDAT,.w_PNDATDOC), .w_PNDATREG, .w_FINESE)
        .w_PNANNDOC = NVL(PNANNDOC,space(4))
        .op_PNANNDOC = .w_PNANNDOC
        .w_PNANNPRO = NVL(PNANNPRO,space(4))
        .op_PNANNPRO = .w_PNANNPRO
        .w_PNFLREGI = NVL(PNFLREGI,space(1))
        .w_PNDESSUP = NVL(PNDESSUP,space(50))
        .w_PN__MESE = NVL(PN__MESE,0)
        .w_PN__ANNO = NVL(PN__ANNO,0)
        .w_PNALFPRO = NVL(PNALFPRO,space(10))
        .op_PNALFPRO = .w_PNALFPRO
        .w_PNNUMPRO = NVL(PNNUMPRO,0)
        .op_PNNUMPRO = .w_PNNUMPRO
        .w_PNCODCLF = NVL(PNCODCLF,space(15))
          if link_1_59_joined
            this.w_PNCODCLF = NVL(ANCODICE159,NVL(this.w_PNCODCLF,space(15)))
            this.w_DESCLF = NVL(ANDESCRI159,space(36))
            this.w_PARIVA = NVL(ANPARIVA159,space(12))
            this.w_DTOBSO = NVL(cp_ToDate(ANDTOBSO159),ctod("  /  /  "))
            this.w_PAGCLF = NVL(ANCODPAG159,space(5))
            this.w_CLFVAL = NVL(ANCODVAL159,space(3))
            this.w_PAGMOR = NVL(ANPAGPAR159,space(5))
            this.w_DATMOR = NVL(cp_ToDate(ANDATMOR159),ctod("  /  /  "))
            this.w_FLACBD = NVL(ANFLACBD159,space(1))
            this.w_FLAACC = NVL(ANFLAACC159,space(1))
            this.w_RITENU = NVL(ANRITENU159,space(1))
            this.w_FLGRIT = NVL(ANFLRITE159,space(1))
            this.w_ANFLSOAL = NVL(ANFLSOAL159,space(1))
            this.w_ANOPETRE = NVL(ANOPETRE159,space(1))
          else
          .link_1_59('Load')
          endif
        .w_PNCOMIVA = NVL(cp_ToDate(PNCOMIVA),ctod("  /  /  "))
        .w_PNDATPLA = NVL(cp_ToDate(PNDATPLA),ctod("  /  /  "))
        .w_PNCODVAL = NVL(PNCODVAL,space(3))
          if link_1_62_joined
            this.w_PNCODVAL = NVL(VACODVAL162,NVL(this.w_PNCODVAL,space(3)))
            this.w_DESAPP = NVL(VADESVAL162,space(35))
            this.w_SIMVAL = NVL(VASIMVAL162,space(5))
            this.w_DECTOT = NVL(VADECTOT162,0)
            this.w_CAOVAL = NVL(VACAOVAL162,0)
            this.w_DTVOBSO = NVL(cp_ToDate(VADTOBSO162),ctod("  /  /  "))
          else
          .link_1_62('Load')
          endif
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_PNCAOVAL = NVL(PNCAOVAL,0)
        .w_CODPAG = IIF(.w_DATMOR>=IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC) AND NOT EMPTY(.w_PAGMOR), .w_PAGMOR, .w_PAGCLF)
          .link_1_73('Load')
        .w_PNTOTDOC = NVL(PNTOTDOC,0)
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_TOTFLAG = AGGFLAG(.w_FLAGTOT, .o_PNIMPDAR+.o_PNIMPAVE, .w_PNIMPDAR+.w_PNIMPAVE, .w_TOTFLAG)
        .w_ODATREG = .w_PNDATREG
        .w_OCOMIVA = .w_PNCOMIVA
        .w_PNFLPROV = NVL(PNFLPROV,space(1))
        .w_PNRIFDOC = NVL(PNRIFDOC,space(10))
        .oPgFrm.Page1.oPag.oObj_1_121.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_122.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_123.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_124.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_125.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_126.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_127.Calculate()
        .w_PNRIFDIS = NVL(PNRIFDIS,space(10))
        .w_FLAGAC = 'N'
        .oPgFrm.Page1.oPag.oObj_1_131.Calculate()
        .w_PNFLGDIF = NVL(PNFLGDIF,space(1))
        .w_PNRIFINC = NVL(PNRIFINC,space(10))
        .w_PNRIFCES = NVL(PNRIFCES,space(1))
        .w_SBILAN = .w_TOTDAR-.w_TOTAVE
        .w_PNRIFACC = NVL(PNRIFACC,space(1))
        .oPgFrm.Page1.oPag.oObj_1_138.Calculate()
        .w_ONUMDOC = .w_PNNUMDOC
        .w_ONUMPRO = .w_PNNUMPRO
        .w_OALFDOC = .w_PNALFDOC
        .w_OALFPRO = .w_PNALFPRO
        .w_PNNUMTRA = NVL(PNNUMTRA,0)
        .w_PNNUMTR2 = NVL(PNNUMTR2,0)
        .oPgFrm.Page1.oPag.oObj_1_151.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_155.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_165.Calculate()
        .w_PNRIFSAL = NVL(PNRIFSAL,space(10))
        .oPgFrm.Page1.oPag.oObj_1_170.Calculate()
        .w_PNFLGSTO = NVL(PNFLGSTO,space(1))
        .w_FLGSTO = IIF(g_RITE='S',LOOKTAB("TAB_RITE",IIF(.w_PNTIPCLF='F',"TRFLGSTO","TRFLGST1"),"TRCODAZI",.w_CODAZI,"TRTIPRIT",IIF(.w_PNTIPCLF="F","A","V")),'S')
        .w_FLVEAC = IIF(.w_PNTIPREG='V' OR (.w_PNTIPREG='C' AND .w_PNTIPDOC='FC'), 'V', IIF(.w_PNTIPREG='A', 'A', ' '))
        .oPgFrm.Page1.oPag.oObj_1_185.Calculate()
        .w_PNSCRASS = NVL(PNSCRASS,space(1))
        .w_PNTOTENA = NVL(PNTOTENA,0)
        .w_OSSERIAL = .w_PNSERIAL
          .link_1_221('Load')
        .oPgFrm.Page1.oPag.oObj_1_224.Calculate()
        .w_PNAGG_01 = NVL(PNAGG_01,space(15))
        .w_PNAGG_02 = NVL(PNAGG_02,space(15))
        .w_PNAGG_03 = NVL(PNAGG_03,space(15))
        .w_PNAGG_04 = NVL(PNAGG_04,space(15))
        .w_PNAGG_05 = NVL(cp_ToDate(PNAGG_05),ctod("  /  /  "))
        .w_PNAGG_06 = NVL(cp_ToDate(PNAGG_06),ctod("  /  /  "))
          .link_1_232('Load')
        .oPgFrm.Page1.oPag.oObj_1_241.Calculate()
        .w_OLDSERIAL = .w_PNSERIAL
        .w_PNNUMFAT = NVL(PNNUMFAT,space(50))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'PNT_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from PNT_DETT where PNSERIAL=KeySet.PNSERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.PNT_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('PNT_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "PNT_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" PNT_DETT"
        link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_24_joined=this.AddJoinedLink_2_24(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'PNSERIAL',this.w_PNSERIAL  )
        select * from (i_cTable) PNT_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TOTDAR = 0
      this.w_TOTAVE = 0
      this.w_TOTFOR = 0
      scan
        with this
          .w_RIGPAR = space(1)
          .w_CAUDES = space(35)
          .w_DESCON = space(30)
          .w_MASTRO = space(15)
          .w_DESCO2 = space(30)
          .w_PARTSN = space(1)
          .w_CLMES1 = 0
          .w_CLMES2 = 0
          .w_CLGIO1 = 0
          .w_CLGIO2 = 0
          .w_GIOFIS = 0
          .w_PAGCLF1 = space(5)
          .w_PAGMOR1 = space(5)
          .w_DATMOR1 = ctod("  /  /  ")
          .w_CCTAGG = space(1)
          .w_FLAGTOT = space(1)
          .w_BANAPP = space(10)
          .w_PARCAR = space(1)
          .w_CDCSAL = space(1)
          .w_NUMCOR = space(25)
          .w_TIPSOT = space(1)
          .w_BANNOS = space(15)
          .w_CAUMOV = space(5)
          .w_CONBAN = space(15)
          .w_CALFES = space(3)
          .w_BAVAL = space(3)
        .w_FLASMO = ' '
          .w_AGECLF = space(5)
          .w_SLAINI = 0
          .w_SLDPRO = 0
          .w_SLDPER = 0
          .w_SLDFIN = 0
          .w_SLAPRO = 0
          .w_SLAPER = 0
          .w_SLAFIN = 0
          .w_SLDINI = 0
          .w_DESCRI = space(30)
          .w_CPROWNUM = CPROWNUM
          .w_PNTIPCON = NVL(PNTIPCON,space(1))
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_PNDESRIG = NVL(PNDESRIG,space(50))
          .w_PNCAURIG = NVL(PNCAURIG,space(5))
          if link_2_4_joined
            this.w_PNCAURIG = NVL(CCCODICE204,NVL(this.w_PNCAURIG,space(5)))
            this.w_CAUDES = NVL(CCDESCRI204,space(35))
            this.w_RIGPAR = NVL(CCFLPART204,space(1))
          else
          .link_2_4('Load')
          endif
          .w_PNCODCON = NVL(PNCODCON,space(15))
          if link_2_7_joined
            this.w_PNCODCON = NVL(ANCODICE207,NVL(this.w_PNCODCON,space(15)))
            this.w_DESCON = NVL(ANDESCRI207,space(30))
            this.w_DESCO2 = NVL(ANDESCR2207,space(30))
            this.w_PAGCLF1 = NVL(ANCODPAG207,space(5))
            this.w_PARTSN = NVL(ANPARTSN207,space(1))
            this.w_CLMES1 = NVL(AN1MESCL207,0)
            this.w_CLMES2 = NVL(AN2MESCL207,0)
            this.w_CLGIO1 = NVL(ANGIOSC1207,0)
            this.w_MASTRO = NVL(ANCONSUP207,space(15))
            this.w_DTOBSO = NVL(cp_ToDate(ANDTOBSO207),ctod("  /  /  "))
            this.w_CLGIO2 = NVL(ANGIOSC2207,0)
            this.w_CCTAGG = NVL(ANCCTAGG207,space(1))
            this.w_BANAPP = NVL(ANCODBAN207,space(10))
            this.w_TIPSOT = NVL(ANTIPSOT207,space(1))
            this.w_GIOFIS = NVL(ANGIOFIS207,0)
            this.w_BANNOS = NVL(ANCODBA2207,space(15))
            this.w_PAGMOR1 = NVL(ANPAGPAR207,space(5))
            this.w_DATMOR1 = NVL(cp_ToDate(ANDATMOR207),ctod("  /  /  "))
            this.w_TESTPART = NVL(ANPARTSN207,space(1))
            this.w_AGECLF = NVL(ANCODAG1207,space(5))
          else
          .link_2_7('Load')
          endif
        .w_SEZB = IIF(.w_PNTIPCON="G", CALCSEZ(.w_MASTRO), " ")
          .w_PNFLSALD = NVL(PNFLSALD,space(1))
          .w_PNFLSALI = NVL(PNFLSALI,space(1))
          .w_PNFLSALF = NVL(PNFLSALF,space(1))
          .w_PNCODPAG = NVL(PNCODPAG,space(5))
          if link_2_24_joined
            this.w_PNCODPAG = NVL(PACODICE224,NVL(this.w_PNCODPAG,space(5)))
            this.w_DESCRI = NVL(PADESCRI224,space(30))
          else
          .link_2_24('Load')
          endif
          .w_PNFLZERO = NVL(PNFLZERO,space(1))
          .w_PNIMPDAR = NVL(PNIMPDAR,0)
          .w_PNIMPAVE = NVL(PNIMPAVE,0)
        .w_IMPDAR = .w_PNIMPDAR
        .w_SLIMPDAR = IIF(.w_PNIMPDAR-.w_PNIMPAVE>=0,.w_PNIMPDAR-.w_PNIMPAVE,0)
        .w_IMPAVE = .w_PNIMPAVE
        .w_SLIMPAVE = IIF(.w_PNIMPDAR-.w_PNIMPAVE<0,ABS(.w_PNIMPDAR-.w_PNIMPAVE),0)
        .w_FLDAVE = IIF(.w_PNIMPDAR<>0, "D", IIF(.w_PNIMPAVE<>0, "A", " "))
          .w_PNCODCON = NVL(PNCODCON,space(15))
          * evitabile
          *.link_2_35('Load')
        .w_PARSAL = ' '
        .w_FLAPAR = IIF(EMPTY(.w_PNFLABAN) AND .cFunction='Load' AND .w_PARSAL<>'.'  AND NOT .w_FLPART $ 'CA', 'S', ' ')
          .w_PNFLPART = NVL(PNFLPART,space(1))
          .w_PNFLABAN = NVL(PNFLABAN,space(6))
          .w_PNINICOM = NVL(cp_ToDate(PNINICOM),ctod("  /  /  "))
          .link_2_45('Load')
          .w_PNFINCOM = NVL(cp_ToDate(PNFINCOM),ctod("  /  /  "))
        .w_OANAL = IIF(.w_PNTIPCON="G" AND .w_SEZB $ "CR" AND .w_CCTAGG $ "AM", 'S', 'N')
          .w_PNIMPIND = NVL(PNIMPIND,0)
        .w_VARPAR = IIF(.w_PNFLPART $ 'CSA' AND g_PERPAR="S", 'N', 'S')
        .w_VARCEN = IIF(.w_PNTIPCON="G" AND .w_SEZB $ "CR" AND .w_FLANAL $ "SN" AND g_PERCCR="S" AND .w_CCTAGG $ "AM", 'N', 'S')
          .link_2_57('Load')
        .oPgFrm.Page1.oPag.oObj_2_61.Calculate()
          .w_PNCODAGE = NVL(PNCODAGE,space(5))
          * evitabile
          *.link_2_63('Load')
          .w_PNFLVABD = NVL(PNFLVABD,space(1))
        .oPgFrm.Page1.oPag.oObj_2_65.Calculate(IIF((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN)>=0, AH_MSGFORMAT('Saldo DARE:'), IIF((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN)<0, AH_MSGFORMAT('Saldo AVERE:'), '')))
        .w_PCODCON = .w_PNCODCON
          .link_2_74('Load')
        .w_SALDO = ABS((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN))
        .w_IMPFOR = IIF(NOT EMPTY(.w_PNCODCON) AND .w_PNCAURIG=.w_PNCODCAU and g_Rite='S' AND .w_PNTIPCON $ 'F-C' ,iif(.w_PNTIPCON='F',.w_PNIMPAVE-.w_PNIMPDAR,.w_PNIMPDAR-.w_PNIMPAVE),0)
          select (this.cTrsName)
          append blank
          replace PNCOMPET with .w_PNCOMPET
          replace PNTIPCON with .w_PNTIPCON
          replace PNCODCON with .w_PNCODCON
          replace PNFLSALD with .w_PNFLSALD
          replace PNFLSALI with .w_PNFLSALI
          replace PNFLSALF with .w_PNFLSALF
          replace SLIMPDAR with .w_SLIMPDAR
          replace SLIMPAVE with .w_SLIMPAVE
          replace PNCODCON with .w_PNCODCON
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTDAR = .w_TOTDAR+.w_IMPDAR
          .w_TOTAVE = .w_TOTAVE+.w_IMPAVE
          .w_TOTFOR = .w_TOTFOR+.w_IMPFOR
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CODAZI = i_CODAZI
        .w_CALCPICP = DEFPIP(.w_DECTOP)
        .w_TESTPART = IIF(g_PERPAR="S", .w_FLPART, "N")
        .w_OBTEST = MIN(IIF(EMPTY(.w_PNDATDOC),i_FINDAT,.w_PNDATDOC), .w_PNDATREG, .w_FINESE)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CODPAG = IIF(.w_DATMOR>=IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC) AND NOT EMPTY(.w_PAGMOR), .w_PAGMOR, .w_PAGCLF)
        .w_TOTFLAG = AGGFLAG(.w_FLAGTOT, .o_PNIMPDAR+.o_PNIMPAVE, .w_PNIMPDAR+.w_PNIMPAVE, .w_TOTFLAG)
        .w_ODATREG = .w_PNDATREG
        .w_OCOMIVA = .w_PNCOMIVA
        .oPgFrm.Page1.oPag.oObj_1_121.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_122.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_123.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_124.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_125.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_126.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_127.Calculate()
        .w_FLAGAC = 'N'
        .oPgFrm.Page1.oPag.oObj_1_131.Calculate()
        .w_SBILAN = .w_TOTDAR-.w_TOTAVE
        .oPgFrm.Page1.oPag.oObj_1_138.Calculate()
        .w_ONUMDOC = .w_PNNUMDOC
        .w_ONUMPRO = .w_PNNUMPRO
        .w_OALFDOC = .w_PNALFDOC
        .w_OALFPRO = .w_PNALFPRO
        .oPgFrm.Page1.oPag.oObj_1_151.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_155.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_165.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_170.Calculate()
        .w_FLGSTO = IIF(g_RITE='S',LOOKTAB("TAB_RITE",IIF(.w_PNTIPCLF='F',"TRFLGSTO","TRFLGST1"),"TRCODAZI",.w_CODAZI,"TRTIPRIT",IIF(.w_PNTIPCLF="F","A","V")),'S')
        .w_FLVEAC = IIF(.w_PNTIPREG='V' OR (.w_PNTIPREG='C' AND .w_PNTIPDOC='FC'), 'V', IIF(.w_PNTIPREG='A', 'A', ' '))
        .oPgFrm.Page1.oPag.oObj_1_185.Calculate()
        .w_OSSERIAL = .w_PNSERIAL
        .oPgFrm.Page1.oPag.oObj_1_224.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_241.Calculate()
        .w_OLDSERIAL = .w_PNSERIAL
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_47.enabled = .oPgFrm.Page1.oPag.oBtn_1_47.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_90.enabled = .oPgFrm.Page1.oPag.oBtn_1_90.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_44.enabled = .oPgFrm.Page1.oPag.oBtn_2_44.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_51.enabled = .oPgFrm.Page1.oPag.oBtn_2_51.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_167.enabled = .oPgFrm.Page1.oPag.oBtn_1_167.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_190.enabled = .oPgFrm.Page1.oPag.oBtn_1_190.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_191.enabled = .oPgFrm.Page1.oPag.oBtn_1_191.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_192.enabled = .oPgFrm.Page1.oPag.oBtn_1_192.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_194.enabled = .oPgFrm.Page1.oPag.oBtn_1_194.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_212.enabled = .oPgFrm.Page1.oPag.oBtn_1_212.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_218.enabled = .oPgFrm.Page1.oPag.oBtn_1_218.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gscg_mpn
    this.w_OLCESE=this.w_PNCODESE
    this.w_OLCUTE=this.w_PNCODUTE
    this.w_OLPRG=this.w_PNPRG
    this.w_MODNRE=.F.
    this.w_OLANDO=this.w_PNANNDOC
    this.w_OLPRD=this.w_PNPRD
    this.w_OLALDO=this.w_PNALFDOC
    this.w_MODNDO=.F.
    
    * --- Esegue Controllo Stampa registri Iva
    this.NotifyEvent('ControlloStampa')
    
    * Modulo Ritenute
    * Assegno alla variabile Totmdr il contenuto della variabile
    * Totfor che contiene il totale delle righe con tipo conto uguale ad 'F'
    if g_Rite='S'
       This.w_Totmdr=This.w_Totfor
    endif
    w_AGGPRO=.F.
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_mpn
    * --- Propone l'ultima Causale e Data Reg. Inserita
    p_OLDCAU = SPACE(5)
    p_OLDDAT = i_datsys
    if type('this.w_PNCODCAU')='C' and APPPNT='Load'
       p_OLDCAU = this.w_PNCODCAU
    endif
    if type('this.w_PNDATREG')='D' and APPPNT='Load'
       p_OLDDAT = IIF(EMPTY(this.w_PNDATREG), i_datsys, this.w_PNDATREG)
    endif
    APPPNT=' '
    
    * Modulo Ritenute
    * Azzero la variabile utilizzata per poter controllare
    * il totale del documento
    if g_Rite='S'
       this.w_TOTMDR=0
    endif
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PNSERIAL=space(10)
      .w_CHKAUT=0
      .w_PNCODESE=space(4)
      .w_VALATT=space(10)
      .w_TABKEY=space(8)
      .w_DELIMM=.f.
      .w_CODAZI=space(5)
      .w_AZDATDOC=space(1)
      .w_PNNUMRER=0
      .w_STAMPATO=space(1)
      .w_STAMPRCV=space(1)
      .w_PNCODUTE=0
      .w_PNDATREG=ctod("  /  /  ")
      .w_FLCOMP=space(1)
      .w_PNCODCAU=space(5)
      .w_PNCOMPET=space(4)
      .w_PNPRG=space(8)
      .w_PNVALNAZ=space(3)
      .w_CAONAZ=0
      .w_DECTOP=0
      .w_CALCPICP=0
      .w_DESCAU=space(35)
      .w_SALDOC=0
      .w_SALPRO=0
      .w_INIESE=space(8)
      .w_FINESE=ctod("  /  /  ")
      .w_FLSALI=space(1)
      .w_FLSALF=space(1)
      .w_FLPART=space(1)
      .w_FLRIFE=space(1)
      .w_PNTIPCLF=space(1)
      .w_PNFLIVDF=space(1)
      .w_PNTIPREG=space(1)
      .w_PNNUMREG=0
      .w_PNTIPDOC=space(2)
      .w_FLNDOC=space(1)
      .w_CALDOC=space(1)
      .w_TESDOC=space(1)
      .w_PNPRD=space(2)
      .w_PNPRP=space(2)
      .w_TESTPART=space(1)
      .w_CFDAVE=space(1)
      .w_FLPDOC=space(1)
      .w_FLPPRO=space(1)
      .w_PNALFDOC=space(10)
      .w_PNNUMDOC=0
      .w_PNDATDOC=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_PNANNDOC=space(4)
      .w_PNANNPRO=space(4)
      .w_PNFLREGI=space(1)
      .w_PNDESSUP=space(50)
      .w_PN__MESE=0
      .w_PN__ANNO=0
      .w_PNALFPRO=space(10)
      .w_PNNUMPRO=0
      .w_PNCODCLF=space(15)
      .w_PNCOMIVA=ctod("  /  /  ")
      .w_PNDATPLA=ctod("  /  /  ")
      .w_PNCODVAL=space(3)
      .w_CAOVAL=0
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_PNCAOVAL=0
      .w_SIMVAL=space(5)
      .w_DESCLF=space(36)
      .w_CLFVAL=space(3)
      .w_PAGCLF=space(5)
      .w_DATMOR=ctod("  /  /  ")
      .w_PAGMOR=space(5)
      .w_CODPAG=space(5)
      .w_DTOBSO=ctod("  /  /  ")
      .w_DESPAG=space(30)
      .w_PNTOTDOC=0
      .w_UTCC=0
      .w_UTCV=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_TOTFLAG=0
      .w_GIACAR=space(1)
      .w_ABILITA=.f.
      .w_MOVCES=space(1)
      .w_ODATREG=ctod("  /  /  ")
      .w_OCOMIVA=ctod("  /  /  ")
      .w_PNFLPROV=space(1)
      .w_IMPDCP=0
      .w_IMPDCA=0
      .w_IMPABB=0
      .w_IMPANT=0
      .w_CCOBSO=ctod("  /  /  ")
      .w_FLANAL=space(1)
      .w_DESAPP=space(35)
      .w_PNRIFDOC=space(10)
      .w_RESCHK=0
      .w_PNRIFDIS=space(10)
      .w_FLAGAC=space(1)
      .w_FLPDIF=space(1)
      .w_PNFLGDIF=space(1)
      .w_PNRIFINC=space(10)
      .w_PNRIFCES=space(1)
      .w_PNTIPCON=space(1)
      .w_CPROWORD=10
      .w_PNDESRIG=space(50)
      .w_PNCAURIG=space(5)
      .w_RIGPAR=space(1)
      .w_CAUDES=space(35)
      .w_PNCODCON=space(15)
      .w_DESCON=space(30)
      .w_MASTRO=space(15)
      .w_SEZB=space(1)
      .w_DESCO2=space(30)
      .w_PNFLSALD=space(1)
      .w_PARTSN=space(1)
      .w_PNFLSALI=space(1)
      .w_CLMES1=0
      .w_PNFLSALF=space(1)
      .w_CLMES2=0
      .w_CLGIO1=0
      .w_CLGIO2=0
      .w_GIOFIS=0
      .w_PAGCLF1=space(5)
      .w_PAGMOR1=space(5)
      .w_DATMOR1=ctod("  /  /  ")
      .w_PNCODPAG=space(5)
      .w_PNFLZERO=space(1)
      .w_CCTAGG=space(1)
      .w_PNIMPDAR=0
      .w_PNIMPAVE=0
      .w_IMPDAR=0
      .w_SLIMPDAR=0
      .w_IMPAVE=0
      .w_SLIMPAVE=0
      .w_FLAGTOT=space(1)
      .w_FLDAVE=space(1)
      .w_PNCODCON=space(15)
      .w_BANAPP=space(10)
      .w_PARSAL=space(1)
      .w_FLAPAR=space(1)
      .w_PNFLPART=space(1)
      .w_PARCAR=space(1)
      .w_PNFLABAN=space(6)
      .w_CDCSAL=space(1)
      .w_PNINICOM=ctod("  /  /  ")
      .w_NUMCOR=space(25)
      .w_PNFINCOM=ctod("  /  /  ")
      .w_TIPSOT=space(1)
      .w_OANAL=space(1)
      .w_BANNOS=space(15)
      .w_PNIMPIND=0
      .w_VARPAR=space(1)
      .w_VARCEN=space(1)
      .w_TOTDAR=0
      .w_TOTAVE=0
      .w_SBILAN=0
      .w_PNRIFACC=space(1)
      .w_PARIVA=space(12)
      .w_ONUMDOC=0
      .w_ONUMPRO=0
      .w_OALFDOC=space(10)
      .w_OALFPRO=space(10)
      .w_PNNUMTRA=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_DTVOBSO=ctod("  /  /  ")
      .w_CAUMOV=space(5)
      .w_CONBAN=space(15)
      .w_CALFES=space(3)
      .w_FLMOVC=space(1)
      .w_BAVAL=space(3)
      .w_FLASMO=space(1)
      .w_CONIVA=space(15)
      .w_PNNUMTR2=0
      .w_AGECLF=space(5)
      .w_PNCODAGE=space(5)
      .w_PNFLVABD=space(1)
      .w_FLACBD=space(1)
      .w_FLVEBD=space(1)
      .w_FLAACC=space(1)
      .w_SLAINI=0
      .w_SLDPRO=0
      .w_SLDPER=0
      .w_SLDFIN=0
      .w_SLAPRO=0
      .w_SLAPER=0
      .w_SLAFIN=0
      .w_SLDINI=0
      .w_PCODCON=space(10)
      .w_SALDO=0
      .w_DESCRI=space(30)
      .w_CAUCES=space(5)
      .w_OLCUTE=0
      .w_OLCESE=space(4)
      .w_OLPRG=space(8)
      .w_MODNRE=.f.
      .w_MODNDO=.f.
      .w_OLANDO=space(4)
      .w_OLPRD=space(2)
      .w_OLALDO=space(2)
      .w_HASEVCOP=space(15)
      .w_HASEVENT=.f.
      .w_PNRIFSAL=space(10)
      .w_DETCON=space(1)
      .w_PNFLGSTO=space(1)
      .w_GESRIT=space(1)
      .w_RITENU=space(1)
      .w_PCCLICK=.f.
      .w_FLGSTO=space(1)
      .w_TOTFOR=0
      .w_NUMACC=0
      .w_TOTACC=0
      .w_IMPPDP=0
      .w_IMPPDA=0
      .w_FLVEAC=space(1)
      .w_IMPFOR=0
      .w_PNSCRASS=space(1)
      .w_NOACC=.f.
      .w_PNTOTENA=0
      .w_PAGINS=space(5)
      .w_FLINSO=space(1)
      .w_CCDESSUP=space(254)
      .w_CCDESRIG=space(254)
      .w_TESTRITE=.f.
      .w_FLCHKNUMD=.f.
      .w_FLCHKPRO=.f.
      .w_FLGRIT=space(1)
      .w_CCCONIVA=space(15)
      .w_REGMARG=space(1)
      .w_IMPABP=0
      .w_IMPABA=0
      .w_TIPOOLD=space(10)
      .w_ANFLSOAL=space(1)
      .w_DATINITRA=ctod("  /  /  ")
      .w_OSSERIAL=space(10)
      .w_OSTIPORE=space(1)
      .w_ANOPETRE=space(1)
      .w_PNAGG_01=space(15)
      .w_PNAGG_02=space(15)
      .w_PNAGG_03=space(15)
      .w_PNAGG_04=space(15)
      .w_PNAGG_05=ctod("  /  /  ")
      .w_PNAGG_06=ctod("  /  /  ")
      .w_DASERIAL=space(10)
      .w_DACAM_01=space(30)
      .w_DACAM_02=space(30)
      .w_DACAM_04=space(30)
      .w_DACAM_03=space(30)
      .w_DACAM_05=space(30)
      .w_DACAM_06=space(30)
      .w_SEARCHACC=space(1)
      .w_OLDSERIAL=space(10)
      .w_PNNUMFAT=space(50)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,1,.f.)
        .w_CHKAUT = 0
        .w_PNCODESE = g_CODESE
        .w_VALATT = .w_PNSERIAL
        .w_TABKEY = 'PNT_MAST'
        .w_DELIMM = .f.
        .w_CODAZI = i_CODAZI
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODAZI))
         .link_1_8('Full')
        endif
        .DoRTCalc(8,9,.f.)
        .w_STAMPATO = 'N'
        .w_STAMPRCV = 'N'
        .w_PNCODUTE = IIF(g_UNIUTE $ 'UE', i_CODUTE, 0)
        .w_PNDATREG = IIF(TYPE('p_OLDDAT')='D', IIF(EMPTY(p_OLDDAT), i_datsys, p_OLDDAT), i_datsys)
        .DoRTCalc(14,14,.f.)
        .w_PNCODCAU = IIF(TYPE('p_OLDCAU')='C', p_OLDCAU, SPACE(5))
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_PNCODCAU))
         .link_1_16('Full')
        endif
        .w_PNCOMPET = g_CODESE
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_PNCOMPET))
         .link_1_17('Full')
        endif
        .w_PNPRG = IIF(g_UNIUTE $ 'GE', DTOS(.w_PNDATREG), SPACE(8))
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_PNVALNAZ))
         .link_1_19('Full')
        endif
        .DoRTCalc(19,20,.f.)
        .w_CALCPICP = DEFPIP(.w_DECTOP)
        .DoRTCalc(22,30,.f.)
        .w_PNTIPCLF = .w_FLRIFE
        .DoRTCalc(32,38,.f.)
        .w_PNPRD = IIF(.w_PNTIPREG='V' OR (.w_PNTIPREG='C' AND .w_PNTIPDOC='FC'), 'FV', 'NN')
        .w_PNPRP = IIF(.w_PNTIPREG='A', 'AC', 'NN')
        .w_TESTPART = IIF(g_PERPAR="S", .w_FLPART, "N")
        .DoRTCalc(42,47,.f.)
        .w_OBTEST = MIN(IIF(EMPTY(.w_PNDATDOC),i_FINDAT,.w_PNDATDOC), .w_PNDATREG, .w_FINESE)
        .w_PNANNDOC = CALPRO(IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC), .w_PNCOMPET, .w_FLPDOC)
        .w_PNANNPRO = CALPRO(.w_PNDATREG, .w_PNCOMPET, .w_FLPPRO)
        .DoRTCalc(51,52,.f.)
        .w_PN__MESE = month(.w_PNDATDOC)
        .w_PN__ANNO = year(.w_PNDATDOC)
        .DoRTCalc(55,57,.f.)
        if not(empty(.w_PNCODCLF))
         .link_1_59('Full')
        endif
        .w_PNCOMIVA = .w_PNDATREG
        .w_PNDATPLA = .w_PNDATREG
        .w_PNCODVAL = iif(not empty( .w_PNCODVAL ) ,.w_PNCODVAL,iif( not empty( .w_CLFVAL ) , .w_CLFVAL , .w_PNVALNAZ ))
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_PNCODVAL))
         .link_1_62('Full')
        endif
        .DoRTCalc(61,62,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_PNCAOVAL = GETCAM(.w_PNCODVAL, IIF( .w_PNTIPREG='A', iif(.w_AZDATDOC='S' and Not Empty(.w_PNDATDOC) ,.w_PNDATDOC,.w_PNDATREG),IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC)), 7)
        .DoRTCalc(65,70,.f.)
        .w_CODPAG = IIF(.w_DATMOR>=IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC) AND NOT EMPTY(.w_PAGMOR), .w_PAGMOR, .w_PAGCLF)
        .DoRTCalc(71,71,.f.)
        if not(empty(.w_CODPAG))
         .link_1_73('Full')
        endif
        .DoRTCalc(72,78,.f.)
        .w_TOTFLAG = AGGFLAG(.w_FLAGTOT, .o_PNIMPDAR+.o_PNIMPAVE, .w_PNIMPDAR+.w_PNIMPAVE, .w_TOTFLAG)
        .w_GIACAR = ' '
        .DoRTCalc(81,82,.f.)
        .w_ODATREG = .w_PNDATREG
        .w_OCOMIVA = .w_PNCOMIVA
        .w_PNFLPROV = 'N'
        .DoRTCalc(86,93,.f.)
        .w_RESCHK = 0
        .oPgFrm.Page1.oPag.oObj_1_121.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_122.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_123.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_124.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_125.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_126.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_127.Calculate()
        .DoRTCalc(95,95,.f.)
        .w_FLAGAC = 'N'
        .oPgFrm.Page1.oPag.oObj_1_131.Calculate()
        .DoRTCalc(97,102,.f.)
        .w_PNDESRIG = IIF( EMPTY(.w_PNDESRIG), .w_PNDESSUP, .w_PNDESRIG)
        .w_PNCAURIG = .w_PNCODCAU
        .DoRTCalc(104,104,.f.)
        if not(empty(.w_PNCAURIG))
         .link_2_4('Full')
        endif
        .DoRTCalc(105,107,.f.)
        if not(empty(.w_PNCODCON))
         .link_2_7('Full')
        endif
        .DoRTCalc(108,109,.f.)
        .w_SEZB = IIF(.w_PNTIPCON="G", CALCSEZ(.w_MASTRO), " ")
        .DoRTCalc(111,111,.f.)
        .w_PNFLSALD = IIF(.w_PNFLPROV='S', ' ', '+')
        .DoRTCalc(113,113,.f.)
        .w_PNFLSALI = IIF(.w_FLSALI='S' AND .w_PNFLPROV<>'S' AND .w_SEZB<>'T', '+', ' ')
        .DoRTCalc(115,115,.f.)
        .w_PNFLSALF = IIF(.w_FLSALF='S' AND .w_PNFLPROV<>'S' AND .w_SEZB<>'T', '+', ' ')
        .DoRTCalc(117,123,.f.)
        .w_PNCODPAG = IIF(.w_DATMOR1>=IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC) AND NOT EMPTY(.w_PAGMOR1), .w_PAGMOR1, IIF((NOT EMPTY(g_SAPAGA) AND .w_FLPART='A'),g_SAPAGA,IIF(.w_FLINSO='S' AND NOT EMPTY(NVL(.w_PAGINS,'')), .w_PAGINS,.w_PAGCLF1)))
        .DoRTCalc(124,124,.f.)
        if not(empty(.w_PNCODPAG))
         .link_2_24('Full')
        endif
        .DoRTCalc(125,128,.f.)
        .w_IMPDAR = .w_PNIMPDAR
        .w_SLIMPDAR = IIF(.w_PNIMPDAR-.w_PNIMPAVE>=0,.w_PNIMPDAR-.w_PNIMPAVE,0)
        .w_IMPAVE = .w_PNIMPAVE
        .w_SLIMPAVE = IIF(.w_PNIMPDAR-.w_PNIMPAVE<0,ABS(.w_PNIMPDAR-.w_PNIMPAVE),0)
        .DoRTCalc(133,133,.f.)
        .w_FLDAVE = IIF(.w_PNIMPDAR<>0, "D", IIF(.w_PNIMPAVE<>0, "A", " "))
        .DoRTCalc(135,135,.f.)
        if not(empty(.w_PNCODCON))
         .link_2_35('Full')
        endif
        .DoRTCalc(136,136,.f.)
        .w_PARSAL = ' '
        .w_FLAPAR = IIF(EMPTY(.w_PNFLABAN) AND .cFunction='Load' AND .w_PARSAL<>'.'  AND NOT .w_FLPART $ 'CA', 'S', ' ')
        .w_PNFLPART = IIF(.w_PARTSN="S", .w_RIGPAR, "N")
        .DoRTCalc(140,144,.f.)
        if not(empty(.w_NUMCOR))
         .link_2_45('Full')
        endif
        .DoRTCalc(145,146,.f.)
        .w_OANAL = IIF(.w_PNTIPCON="G" AND .w_SEZB $ "CR" AND .w_CCTAGG $ "AM", 'S', 'N')
        .DoRTCalc(148,149,.f.)
        .w_VARPAR = IIF(.w_PNFLPART $ 'CSA' AND g_PERPAR="S", 'N', 'S')
        .w_VARCEN = IIF(.w_PNTIPCON="G" AND .w_SEZB $ "CR" AND .w_FLANAL $ "SN" AND g_PERCCR="S" AND .w_CCTAGG $ "AM", 'N', 'S')
        .DoRTCalc(152,153,.f.)
        .w_SBILAN = .w_TOTDAR-.w_TOTAVE
        .oPgFrm.Page1.oPag.oObj_1_138.Calculate()
        .DoRTCalc(155,156,.f.)
        .w_ONUMDOC = .w_PNNUMDOC
        .w_ONUMPRO = .w_PNNUMPRO
        .w_OALFDOC = .w_PNALFDOC
        .w_OALFPRO = .w_PNALFPRO
        .w_PNNUMTRA = 0
        .DoRTCalc(162,165,.f.)
        if not(empty(.w_CONBAN))
         .link_2_57('Full')
        endif
        .DoRTCalc(166,168,.f.)
        .w_FLASMO = ' '
        .DoRTCalc(170,170,.f.)
        .w_PNNUMTR2 = 0
        .oPgFrm.Page1.oPag.oObj_1_151.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_61.Calculate()
        .DoRTCalc(172,172,.f.)
        .w_PNCODAGE = .w_AGECLF
        .DoRTCalc(173,173,.f.)
        if not(empty(.w_PNCODAGE))
         .link_2_63('Full')
        endif
        .w_PNFLVABD = IIF(.w_PNFLPART $ 'AC', IIF(.w_PNTIPCLF='C', .w_FLVEBD, IIF(.w_PNTIPCLF='F', .w_FLACBD, ' ')), ' ')
        .oPgFrm.Page1.oPag.oObj_1_155.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_65.Calculate(IIF((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN)>=0, AH_MSGFORMAT('Saldo DARE:'), IIF((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN)<0, AH_MSGFORMAT('Saldo AVERE:'), '')))
        .DoRTCalc(175,185,.f.)
        .w_PCODCON = .w_PNCODCON
        .DoRTCalc(186,186,.f.)
        if not(empty(.w_PCODCON))
         .link_2_74('Full')
        endif
        .w_SALDO = ABS((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN))
        .oPgFrm.Page1.oPag.oObj_1_165.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_170.Calculate()
        .DoRTCalc(188,201,.f.)
        .w_PNFLGSTO = IIF(g_RITE='S',LOOKTAB("TAB_RITE",IIF(.w_PNTIPCLF='F',"TRFLGSTO","TRFLGST1"),"TRCODAZI",.w_CODAZI,"TRTIPRIT",IIF(.w_PNTIPCLF="F","A","V")),'S')
        .DoRTCalc(203,204,.f.)
        .w_PCCLICK = .F.
        .w_FLGSTO = IIF(g_RITE='S',LOOKTAB("TAB_RITE",IIF(.w_PNTIPCLF='F',"TRFLGSTO","TRFLGST1"),"TRCODAZI",.w_CODAZI,"TRTIPRIT",IIF(.w_PNTIPCLF="F","A","V")),'S')
        .DoRTCalc(207,211,.f.)
        .w_FLVEAC = IIF(.w_PNTIPREG='V' OR (.w_PNTIPREG='C' AND .w_PNTIPDOC='FC'), 'V', IIF(.w_PNTIPREG='A', 'A', ' '))
        .oPgFrm.Page1.oPag.oObj_1_185.Calculate()
        .w_IMPFOR = IIF(NOT EMPTY(.w_PNCODCON) AND .w_PNCAURIG=.w_PNCODCAU and g_Rite='S' AND .w_PNTIPCON $ 'F-C' ,iif(.w_PNTIPCON='F',.w_PNIMPAVE-.w_PNIMPDAR,.w_PNIMPDAR-.w_PNIMPAVE),0)
        .DoRTCalc(214,214,.f.)
        .w_NOACC = .F.
        .DoRTCalc(216,220,.f.)
        .w_TESTRITE = .T.
        .DoRTCalc(222,231,.f.)
        .w_OSSERIAL = .w_PNSERIAL
        .DoRTCalc(232,232,.f.)
        if not(empty(.w_OSSERIAL))
         .link_1_221('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_224.Calculate()
        .DoRTCalc(233,240,.f.)
        .w_DASERIAL = '0000000001'
        .DoRTCalc(241,241,.f.)
        if not(empty(.w_DASERIAL))
         .link_1_232('Full')
        endif
        .DoRTCalc(242,247,.f.)
        .w_SEARCHACC = 'N'
        .oPgFrm.Page1.oPag.oObj_1_241.Calculate()
        .w_OLDSERIAL = .w_PNSERIAL
      endif
    endwith
    cp_BlankRecExtFlds(this,'PNT_MAST')
    this.DoRTCalc(250,250,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_90.enabled = this.oPgFrm.Page1.oPag.oBtn_1_90.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_44.enabled = this.oPgFrm.Page1.oPag.oBtn_2_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_51.enabled = this.oPgFrm.Page1.oPag.oBtn_2_51.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_167.enabled = this.oPgFrm.Page1.oPag.oBtn_1_167.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_190.enabled = this.oPgFrm.Page1.oPag.oBtn_1_190.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_191.enabled = this.oPgFrm.Page1.oPag.oBtn_1_191.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_192.enabled = this.oPgFrm.Page1.oPag.oBtn_1_192.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_194.enabled = this.oPgFrm.Page1.oPag.oBtn_1_194.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_212.enabled = this.oPgFrm.Page1.oPag.oBtn_1_212.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_218.enabled = this.oPgFrm.Page1.oPag.oBtn_1_218.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_mpn
    * Variabile utilizzata per il calcolo delle Ritenute
    * Controllo se esiste la variabile Drtotdoc.
    * Verifico se � attivo il modulo Ritenute
    * Effettuo un duplice controllo:
    * 1) Sono entrato in caricamento (F4)
    * 2) La procedura si posiziona in caricamento (F10)
    if g_Rite='S'
        if type('this.GSCG_MDR.w_DRTOTDOC')<>'U'
           this.GSCG_MDR.w_DRTOTDOC=0
        Endif
        if type('this.GSCG_MDR.cnt.w_DRTOTDOC')<>'U'
         this.GSCG_MDR.cnt.w_DRTOTDOC=0
        Endif
    endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPNNUMRER_1_10.enabled = i_bVal
      .Page1.oPag.oPNDATREG_1_14.enabled = i_bVal
      .Page1.oPag.oPNCODCAU_1_16.enabled = i_bVal
      .Page1.oPag.oPNCOMPET_1_17.enabled = i_bVal
      .Page1.oPag.oPNALFDOC_1_46.enabled = i_bVal
      .Page1.oPag.oPNNUMDOC_1_48.enabled = i_bVal
      .Page1.oPag.oPNDATDOC_1_49.enabled = i_bVal
      .Page1.oPag.oPNDESSUP_1_54.enabled = i_bVal
      .Page1.oPag.oPN__MESE_1_55.enabled = i_bVal
      .Page1.oPag.oPN__ANNO_1_56.enabled = i_bVal
      .Page1.oPag.oPNALFPRO_1_57.enabled = i_bVal
      .Page1.oPag.oPNNUMPRO_1_58.enabled = i_bVal
      .Page1.oPag.oPNCODCLF_1_59.enabled = i_bVal
      .Page1.oPag.oPNCOMIVA_1_60.enabled = i_bVal
      .Page1.oPag.oPNDATPLA_1_61.enabled = i_bVal
      .Page1.oPag.oPNCODVAL_1_62.enabled = i_bVal
      .Page1.oPag.oPNCAOVAL_1_66.enabled = i_bVal
      .Page1.oPag.oPNTOTDOC_1_76.enabled = i_bVal
      .Page1.oPag.oPNFLPROV_1_107.enabled = i_bVal
      .Page1.oPag.oBtn_1_47.enabled = .Page1.oPag.oBtn_1_47.mCond()
      .Page1.oPag.oBtn_1_90.enabled = .Page1.oPag.oBtn_1_90.mCond()
      .Page1.oPag.oBtn_2_44.enabled = .Page1.oPag.oBtn_2_44.mCond()
      .Page1.oPag.oBtn_2_51.enabled = .Page1.oPag.oBtn_2_51.mCond()
      .Page1.oPag.oBtn_1_167.enabled = .Page1.oPag.oBtn_1_167.mCond()
      .Page1.oPag.oBtn_1_172.enabled = i_bVal
      .Page1.oPag.oBtn_1_190.enabled = .Page1.oPag.oBtn_1_190.mCond()
      .Page1.oPag.oBtn_1_191.enabled = .Page1.oPag.oBtn_1_191.mCond()
      .Page1.oPag.oBtn_1_192.enabled = .Page1.oPag.oBtn_1_192.mCond()
      .Page1.oPag.oBtn_1_194.enabled = .Page1.oPag.oBtn_1_194.mCond()
      .Page1.oPag.oBtn_1_212.enabled = .Page1.oPag.oBtn_1_212.mCond()
      .Page1.oPag.oBtn_1_218.enabled = .Page1.oPag.oBtn_1_218.mCond()
      .Page1.oPag.oObj_1_121.enabled = i_bVal
      .Page1.oPag.oObj_1_122.enabled = i_bVal
      .Page1.oPag.oObj_1_123.enabled = i_bVal
      .Page1.oPag.oObj_1_124.enabled = i_bVal
      .Page1.oPag.oObj_1_125.enabled = i_bVal
      .Page1.oPag.oObj_1_126.enabled = i_bVal
      .Page1.oPag.oObj_1_127.enabled = i_bVal
      .Page1.oPag.oObj_1_131.enabled = i_bVal
      .Page1.oPag.oObj_1_138.enabled = i_bVal
      .Page1.oPag.oObj_1_151.enabled = i_bVal
      .Page1.oPag.oObj_2_61.enabled = i_bVal
      .Page1.oPag.oObj_1_155.enabled = i_bVal
      .Page1.oPag.oObj_1_165.enabled = i_bVal
      .Page1.oPag.oObj_1_170.enabled = i_bVal
      .Page1.oPag.oObj_1_185.enabled = i_bVal
      .Page1.oPag.oObj_1_224.enabled = i_bVal
      .Page1.oPag.oObj_1_241.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oPNNUMRER_1_10.enabled = .t.
        .Page1.oPag.oPNDATREG_1_14.enabled = .t.
        .Page1.oPag.oPNCODCAU_1_16.enabled = .t.
        .Page1.oPag.oPNNUMDOC_1_48.enabled = .t.
        .Page1.oPag.oPNNUMPRO_1_58.enabled = .t.
      endif
    endwith
    this.GSCG_MIV.SetStatus(i_cOp)
    this.GSCG_MAC.SetStatus(i_cOp)
    this.GSCG_MCA.SetStatus(i_cOp)
    this.GSCG_MPA.SetStatus(i_cOp)
    this.GSCG_MMC.SetStatus(i_cOp)
    this.GSCG_MDR.SetStatus(i_cOp)
    this.GSCG_ASA.SetStatus(i_cOp)
    this.GSCG_AOS.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PNT_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEPNT","i_codazi,w_PNSERIAL")
      cp_AskTableProg(this,i_nConn,"PRPNT","i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
      if .w_PNANNDOC<>'    '
        cp_AskTableProg(this,i_nConn,"PRDOC","i_codazi,w_PNANNDOC,w_PNPRD,w_PNALFDOC,w_PNNUMDOC")
      endif
      if .w_PNANNPRO<>'    '
        cp_AskTableProg(this,i_nConn,"PRPRO","i_codazi,w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO")
      endif
      .op_codazi = .w_codazi
      .op_PNSERIAL = .w_PNSERIAL
      .op_codazi = .w_codazi
      .op_PNCODESE = .w_PNCODESE
      .op_PNCODUTE = .w_PNCODUTE
      .op_PNPRG = .w_PNPRG
      .op_PNNUMRER = .w_PNNUMRER
      .op_codazi = .w_codazi
      .op_PNANNDOC = .w_PNANNDOC
      .op_PNPRD = .w_PNPRD
      .op_PNALFDOC = .w_PNALFDOC
      .op_PNNUMDOC = .w_PNNUMDOC
      .op_codazi = .w_codazi
      .op_PNANNPRO = .w_PNANNPRO
      .op_PNPRP = .w_PNPRP
      .op_PNALFPRO = .w_PNALFPRO
      .op_PNNUMPRO = .w_PNNUMPRO
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_MIV.SetChildrenStatus(i_cOp)
  *  this.GSCG_MAC.SetChildrenStatus(i_cOp)
  *  this.GSCG_MCA.SetChildrenStatus(i_cOp)
  *  this.GSCG_MPA.SetChildrenStatus(i_cOp)
  *  this.GSCG_MMC.SetChildrenStatus(i_cOp)
  *  this.GSCG_MDR.SetChildrenStatus(i_cOp)
  *  this.GSCG_ASA.SetChildrenStatus(i_cOp)
  *  this.GSCG_AOS.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNSERIAL,"PNSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNCODESE,"PNCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNNUMRER,"PNNUMRER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNCODUTE,"PNCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNDATREG,"PNDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNCODCAU,"PNCODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNCOMPET,"PNCOMPET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNPRG,"PNPRG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNVALNAZ,"PNVALNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNTIPCLF,"PNTIPCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNFLIVDF,"PNFLIVDF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNTIPREG,"PNTIPREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNNUMREG,"PNNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNTIPDOC,"PNTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNPRD,"PNPRD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNPRP,"PNPRP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNALFDOC,"PNALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNNUMDOC,"PNNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNDATDOC,"PNDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNANNDOC,"PNANNDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNANNPRO,"PNANNPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNFLREGI,"PNFLREGI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNDESSUP,"PNDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PN__MESE,"PN__MESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PN__ANNO,"PN__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNALFPRO,"PNALFPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNNUMPRO,"PNNUMPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNCODCLF,"PNCODCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNCOMIVA,"PNCOMIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNDATPLA,"PNDATPLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNCODVAL,"PNCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNCAOVAL,"PNCAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNTOTDOC,"PNTOTDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNFLPROV,"PNFLPROV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNRIFDOC,"PNRIFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNRIFDIS,"PNRIFDIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNFLGDIF,"PNFLGDIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNRIFINC,"PNRIFINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNRIFCES,"PNRIFCES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNRIFACC,"PNRIFACC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNNUMTRA,"PNNUMTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNNUMTR2,"PNNUMTR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNRIFSAL,"PNRIFSAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNFLGSTO,"PNFLGSTO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNSCRASS,"PNSCRASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNTOTENA,"PNTOTENA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNAGG_01,"PNAGG_01",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNAGG_02,"PNAGG_02",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNAGG_03,"PNAGG_03",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNAGG_04,"PNAGG_04",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNAGG_05,"PNAGG_05",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNAGG_06,"PNAGG_06",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PNNUMFAT,"PNNUMFAT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
    i_lTable = "PNT_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PNT_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCG_SPN with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PNTIPCON C(1);
      ,t_CPROWORD N(5);
      ,t_PNCODCON C(15);
      ,t_DESCON C(30);
      ,t_PNIMPDAR N(18,4);
      ,t_PNIMPAVE N(18,4);
      ,t_PNFLPART C(1);
      ,t_CDCSAL C(1);
      ,t_NUMCOR C(25);
      ,t_SALDO N(18,4);
      ,PNCOMPET C(4);
      ,PNTIPCON C(1);
      ,PNCODCON C(15);
      ,PNFLSALD C(1);
      ,PNFLSALI C(1);
      ,PNFLSALF C(1);
      ,SLIMPDAR N(18,4);
      ,SLIMPAVE N(18,4);
      ,CPROWNUM N(10);
      ,t_PNDESRIG C(50);
      ,t_PNCAURIG C(5);
      ,t_RIGPAR C(1);
      ,t_CAUDES C(35);
      ,t_MASTRO C(15);
      ,t_SEZB C(1);
      ,t_DESCO2 C(30);
      ,t_PNFLSALD C(1);
      ,t_PARTSN C(1);
      ,t_PNFLSALI C(1);
      ,t_CLMES1 N(2);
      ,t_PNFLSALF C(1);
      ,t_CLMES2 N(2);
      ,t_CLGIO1 N(2);
      ,t_CLGIO2 N(2);
      ,t_GIOFIS N(2);
      ,t_PAGCLF1 C(5);
      ,t_PAGMOR1 C(5);
      ,t_DATMOR1 D(8);
      ,t_PNCODPAG C(5);
      ,t_PNFLZERO C(1);
      ,t_CCTAGG C(1);
      ,t_IMPDAR N(18,4);
      ,t_SLIMPDAR N(18,4);
      ,t_IMPAVE N(18,4);
      ,t_SLIMPAVE N(18,4);
      ,t_FLAGTOT C(1);
      ,t_FLDAVE C(1);
      ,t_BANAPP C(10);
      ,t_PARSAL C(1);
      ,t_FLAPAR C(1);
      ,t_PARCAR C(1);
      ,t_PNFLABAN C(6);
      ,t_PNINICOM D(8);
      ,t_PNFINCOM D(8);
      ,t_TIPSOT C(1);
      ,t_OANAL C(1);
      ,t_BANNOS C(15);
      ,t_PNIMPIND N(18,4);
      ,t_VARPAR C(1);
      ,t_VARCEN C(1);
      ,t_CAUMOV C(5);
      ,t_CONBAN C(15);
      ,t_CALFES C(3);
      ,t_BAVAL C(3);
      ,t_FLASMO C(1);
      ,t_AGECLF C(5);
      ,t_PNCODAGE C(5);
      ,t_PNFLVABD C(1);
      ,t_SLAINI N(18,4);
      ,t_SLDPRO N(18,4);
      ,t_SLDPER N(18,4);
      ,t_SLDFIN N(18,4);
      ,t_SLAPRO N(18,4);
      ,t_SLAPER N(18,4);
      ,t_SLAFIN N(18,4);
      ,t_SLDINI N(18,4);
      ,t_PCODCON C(10);
      ,t_DESCRI C(30);
      ,t_IMPFOR N(18,4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mpnbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPNTIPCON_2_1.controlsource=this.cTrsName+'.t_PNTIPCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPNCODCON_2_7.controlsource=this.cTrsName+'.t_PNCODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCON_2_8.controlsource=this.cTrsName+'.t_DESCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPNIMPDAR_2_27.controlsource=this.cTrsName+'.t_PNIMPDAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPNIMPAVE_2_28.controlsource=this.cTrsName+'.t_PNIMPAVE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPNFLPART_2_39.controlsource=this.cTrsName+'.t_PNFLPART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCDCSAL_2_42.controlsource=this.cTrsName+'.t_CDCSAL'
    this.oPgFRm.Page1.oPag.oNUMCOR_2_45.controlsource=this.cTrsName+'.t_NUMCOR'
    this.oPgFRm.Page1.oPag.oSALDO_2_75.controlsource=this.cTrsName+'.t_SALDO'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(42)
    this.AddVLine(72)
    this.AddVLine(203)
    this.AddVLine(429)
    this.AddVLine(560)
    this.AddVLine(691)
    this.AddVLine(713)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNTIPCON_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SEPNT","i_codazi,w_PNSERIAL")
          cp_NextTableProg(this,i_nConn,"PRPNT","i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
        if .w_PNANNDOC<>'    '
          cp_NextTableProg(this,i_nConn,"PRDOC","i_codazi,w_PNANNDOC,w_PNPRD,w_PNALFDOC,w_PNNUMDOC")
        endif
        if .w_PNANNPRO<>'    '
          cp_NextTableProg(this,i_nConn,"PRPRO","i_codazi,w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO")
        endif
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PNT_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PNT_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'PNT_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(PNSERIAL,PNCODESE,PNNUMRER,PNCODUTE,PNDATREG"+;
                  ",PNCODCAU,PNCOMPET,PNPRG,PNVALNAZ,PNTIPCLF"+;
                  ",PNFLIVDF,PNTIPREG,PNNUMREG,PNTIPDOC,PNPRD"+;
                  ",PNPRP,PNALFDOC,PNNUMDOC,PNDATDOC,PNANNDOC"+;
                  ",PNANNPRO,PNFLREGI,PNDESSUP,PN__MESE,PN__ANNO"+;
                  ",PNALFPRO,PNNUMPRO,PNCODCLF,PNCOMIVA,PNDATPLA"+;
                  ",PNCODVAL,PNCAOVAL,PNTOTDOC,UTCC,UTCV"+;
                  ",UTDC,UTDV,PNFLPROV,PNRIFDOC,PNRIFDIS"+;
                  ",PNFLGDIF,PNRIFINC,PNRIFCES,PNRIFACC,PNNUMTRA"+;
                  ",PNNUMTR2,PNRIFSAL,PNFLGSTO,PNSCRASS,PNTOTENA"+;
                  ",PNAGG_01,PNAGG_02,PNAGG_03,PNAGG_04,PNAGG_05"+;
                  ",PNAGG_06,PNNUMFAT"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_PNSERIAL)+;
                    ","+cp_ToStrODBC(this.w_PNCODESE)+;
                    ","+cp_ToStrODBC(this.w_PNNUMRER)+;
                    ","+cp_ToStrODBC(this.w_PNCODUTE)+;
                    ","+cp_ToStrODBC(this.w_PNDATREG)+;
                    ","+cp_ToStrODBCNull(this.w_PNCODCAU)+;
                    ","+cp_ToStrODBCNull(this.w_PNCOMPET)+;
                    ","+cp_ToStrODBC(this.w_PNPRG)+;
                    ","+cp_ToStrODBCNull(this.w_PNVALNAZ)+;
                    ","+cp_ToStrODBC(this.w_PNTIPCLF)+;
                    ","+cp_ToStrODBC(this.w_PNFLIVDF)+;
                    ","+cp_ToStrODBC(this.w_PNTIPREG)+;
                    ","+cp_ToStrODBC(this.w_PNNUMREG)+;
                    ","+cp_ToStrODBC(this.w_PNTIPDOC)+;
                    ","+cp_ToStrODBC(this.w_PNPRD)+;
                    ","+cp_ToStrODBC(this.w_PNPRP)+;
                    ","+cp_ToStrODBC(this.w_PNALFDOC)+;
                    ","+cp_ToStrODBC(this.w_PNNUMDOC)+;
                    ","+cp_ToStrODBC(this.w_PNDATDOC)+;
                    ","+cp_ToStrODBC(this.w_PNANNDOC)+;
                    ","+cp_ToStrODBC(this.w_PNANNPRO)+;
                    ","+cp_ToStrODBC(this.w_PNFLREGI)+;
                    ","+cp_ToStrODBC(this.w_PNDESSUP)+;
                    ","+cp_ToStrODBC(this.w_PN__MESE)+;
                    ","+cp_ToStrODBC(this.w_PN__ANNO)+;
                    ","+cp_ToStrODBC(this.w_PNALFPRO)+;
                    ","+cp_ToStrODBC(this.w_PNNUMPRO)+;
                    ","+cp_ToStrODBCNull(this.w_PNCODCLF)+;
                    ","+cp_ToStrODBC(this.w_PNCOMIVA)+;
                    ","+cp_ToStrODBC(this.w_PNDATPLA)+;
                    ","+cp_ToStrODBCNull(this.w_PNCODVAL)+;
                    ","+cp_ToStrODBC(this.w_PNCAOVAL)+;
                    ","+cp_ToStrODBC(this.w_PNTOTDOC)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_PNFLPROV)+;
                    ","+cp_ToStrODBC(this.w_PNRIFDOC)+;
                    ","+cp_ToStrODBC(this.w_PNRIFDIS)+;
                    ","+cp_ToStrODBC(this.w_PNFLGDIF)+;
                    ","+cp_ToStrODBC(this.w_PNRIFINC)+;
                    ","+cp_ToStrODBC(this.w_PNRIFCES)+;
                    ","+cp_ToStrODBC(this.w_PNRIFACC)+;
                    ","+cp_ToStrODBC(this.w_PNNUMTRA)+;
                    ","+cp_ToStrODBC(this.w_PNNUMTR2)+;
                    ","+cp_ToStrODBC(this.w_PNRIFSAL)+;
                    ","+cp_ToStrODBC(this.w_PNFLGSTO)+;
                    ","+cp_ToStrODBC(this.w_PNSCRASS)+;
                    ","+cp_ToStrODBC(this.w_PNTOTENA)+;
                    ","+cp_ToStrODBC(this.w_PNAGG_01)+;
                    ","+cp_ToStrODBC(this.w_PNAGG_02)+;
                    ","+cp_ToStrODBC(this.w_PNAGG_03)+;
                    ","+cp_ToStrODBC(this.w_PNAGG_04)+;
                    ","+cp_ToStrODBC(this.w_PNAGG_05)+;
                    ","+cp_ToStrODBC(this.w_PNAGG_06)+;
                    ","+cp_ToStrODBC(this.w_PNNUMFAT)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PNT_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'PNT_MAST')
        cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL)
        INSERT INTO (i_cTable);
              (PNSERIAL,PNCODESE,PNNUMRER,PNCODUTE,PNDATREG,PNCODCAU,PNCOMPET,PNPRG,PNVALNAZ,PNTIPCLF,PNFLIVDF,PNTIPREG,PNNUMREG,PNTIPDOC,PNPRD,PNPRP,PNALFDOC,PNNUMDOC,PNDATDOC,PNANNDOC,PNANNPRO,PNFLREGI,PNDESSUP,PN__MESE,PN__ANNO,PNALFPRO,PNNUMPRO,PNCODCLF,PNCOMIVA,PNDATPLA,PNCODVAL,PNCAOVAL,PNTOTDOC,UTCC,UTCV,UTDC,UTDV,PNFLPROV,PNRIFDOC,PNRIFDIS,PNFLGDIF,PNRIFINC,PNRIFCES,PNRIFACC,PNNUMTRA,PNNUMTR2,PNRIFSAL,PNFLGSTO,PNSCRASS,PNTOTENA,PNAGG_01,PNAGG_02,PNAGG_03,PNAGG_04,PNAGG_05,PNAGG_06,PNNUMFAT &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_PNSERIAL;
                  ,this.w_PNCODESE;
                  ,this.w_PNNUMRER;
                  ,this.w_PNCODUTE;
                  ,this.w_PNDATREG;
                  ,this.w_PNCODCAU;
                  ,this.w_PNCOMPET;
                  ,this.w_PNPRG;
                  ,this.w_PNVALNAZ;
                  ,this.w_PNTIPCLF;
                  ,this.w_PNFLIVDF;
                  ,this.w_PNTIPREG;
                  ,this.w_PNNUMREG;
                  ,this.w_PNTIPDOC;
                  ,this.w_PNPRD;
                  ,this.w_PNPRP;
                  ,this.w_PNALFDOC;
                  ,this.w_PNNUMDOC;
                  ,this.w_PNDATDOC;
                  ,this.w_PNANNDOC;
                  ,this.w_PNANNPRO;
                  ,this.w_PNFLREGI;
                  ,this.w_PNDESSUP;
                  ,this.w_PN__MESE;
                  ,this.w_PN__ANNO;
                  ,this.w_PNALFPRO;
                  ,this.w_PNNUMPRO;
                  ,this.w_PNCODCLF;
                  ,this.w_PNCOMIVA;
                  ,this.w_PNDATPLA;
                  ,this.w_PNCODVAL;
                  ,this.w_PNCAOVAL;
                  ,this.w_PNTOTDOC;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_PNFLPROV;
                  ,this.w_PNRIFDOC;
                  ,this.w_PNRIFDIS;
                  ,this.w_PNFLGDIF;
                  ,this.w_PNRIFINC;
                  ,this.w_PNRIFCES;
                  ,this.w_PNRIFACC;
                  ,this.w_PNNUMTRA;
                  ,this.w_PNNUMTR2;
                  ,this.w_PNRIFSAL;
                  ,this.w_PNFLGSTO;
                  ,this.w_PNSCRASS;
                  ,this.w_PNTOTENA;
                  ,this.w_PNAGG_01;
                  ,this.w_PNAGG_02;
                  ,this.w_PNAGG_03;
                  ,this.w_PNAGG_04;
                  ,this.w_PNAGG_05;
                  ,this.w_PNAGG_06;
                  ,this.w_PNNUMFAT;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PNT_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_DETT_IDX,2])
      *
      * insert into PNT_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(PNSERIAL,PNTIPCON,CPROWORD,PNDESRIG,PNCAURIG"+;
                  ",PNCODCON,PNFLSALD,PNFLSALI,PNFLSALF,PNCODPAG"+;
                  ",PNFLZERO,PNIMPDAR,PNIMPAVE,PNFLPART,PNFLABAN"+;
                  ",PNINICOM,PNFINCOM,PNIMPIND,PNCODAGE,PNFLVABD,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PNSERIAL)+","+cp_ToStrODBC(this.w_PNTIPCON)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_PNDESRIG)+","+cp_ToStrODBCNull(this.w_PNCAURIG)+;
             ","+cp_ToStrODBCNull(this.w_PNCODCON)+","+cp_ToStrODBC(this.w_PNFLSALD)+","+cp_ToStrODBC(this.w_PNFLSALI)+","+cp_ToStrODBC(this.w_PNFLSALF)+","+cp_ToStrODBCNull(this.w_PNCODPAG)+;
             ","+cp_ToStrODBC(this.w_PNFLZERO)+","+cp_ToStrODBC(this.w_PNIMPDAR)+","+cp_ToStrODBC(this.w_PNIMPAVE)+","+cp_ToStrODBC(this.w_PNFLPART)+","+cp_ToStrODBC(this.w_PNFLABAN)+;
             ","+cp_ToStrODBC(this.w_PNINICOM)+","+cp_ToStrODBC(this.w_PNFINCOM)+","+cp_ToStrODBC(this.w_PNIMPIND)+","+cp_ToStrODBCNull(this.w_PNCODAGE)+","+cp_ToStrODBC(this.w_PNFLVABD)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PNSERIAL',this.w_PNSERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_PNSERIAL,this.w_PNTIPCON,this.w_CPROWORD,this.w_PNDESRIG,this.w_PNCAURIG"+;
                ",this.w_PNCODCON,this.w_PNFLSALD,this.w_PNFLSALI,this.w_PNFLSALF,this.w_PNCODPAG"+;
                ",this.w_PNFLZERO,this.w_PNIMPDAR,this.w_PNIMPAVE,this.w_PNFLPART,this.w_PNFLABAN"+;
                ",this.w_PNINICOM,this.w_PNFINCOM,this.w_PNIMPIND,this.w_PNCODAGE,this.w_PNFLVABD,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- gscg_mpn
    * --- Forza l'Aggiornamento del Master
    * --- E' necessario per la modifica di alcuni alcuni campi  UTDC, UTDV
    this.bHeaderUpdated = .t.
    
    IF Type('this.GSCG_MPA.CNT')='O'
       IF this.GSCG_MPA.CNT.BoNsCREEN=.T.
          this.GSCG_MPA.EcpSave()
       ENDIF
    ENDIF
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update PNT_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'PNT_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " PNCODESE="+cp_ToStrODBC(this.w_PNCODESE)+;
             ",PNNUMRER="+cp_ToStrODBC(this.w_PNNUMRER)+;
             ",PNCODUTE="+cp_ToStrODBC(this.w_PNCODUTE)+;
             ",PNDATREG="+cp_ToStrODBC(this.w_PNDATREG)+;
             ",PNCODCAU="+cp_ToStrODBCNull(this.w_PNCODCAU)+;
             ",PNCOMPET="+cp_ToStrODBCNull(this.w_PNCOMPET)+;
             ",PNPRG="+cp_ToStrODBC(this.w_PNPRG)+;
             ",PNVALNAZ="+cp_ToStrODBCNull(this.w_PNVALNAZ)+;
             ",PNTIPCLF="+cp_ToStrODBC(this.w_PNTIPCLF)+;
             ",PNFLIVDF="+cp_ToStrODBC(this.w_PNFLIVDF)+;
             ",PNTIPREG="+cp_ToStrODBC(this.w_PNTIPREG)+;
             ",PNNUMREG="+cp_ToStrODBC(this.w_PNNUMREG)+;
             ",PNTIPDOC="+cp_ToStrODBC(this.w_PNTIPDOC)+;
             ",PNPRD="+cp_ToStrODBC(this.w_PNPRD)+;
             ",PNPRP="+cp_ToStrODBC(this.w_PNPRP)+;
             ",PNALFDOC="+cp_ToStrODBC(this.w_PNALFDOC)+;
             ",PNNUMDOC="+cp_ToStrODBC(this.w_PNNUMDOC)+;
             ",PNDATDOC="+cp_ToStrODBC(this.w_PNDATDOC)+;
             ",PNANNDOC="+cp_ToStrODBC(this.w_PNANNDOC)+;
             ",PNANNPRO="+cp_ToStrODBC(this.w_PNANNPRO)+;
             ",PNFLREGI="+cp_ToStrODBC(this.w_PNFLREGI)+;
             ",PNDESSUP="+cp_ToStrODBC(this.w_PNDESSUP)+;
             ",PN__MESE="+cp_ToStrODBC(this.w_PN__MESE)+;
             ",PN__ANNO="+cp_ToStrODBC(this.w_PN__ANNO)+;
             ",PNALFPRO="+cp_ToStrODBC(this.w_PNALFPRO)+;
             ",PNNUMPRO="+cp_ToStrODBC(this.w_PNNUMPRO)+;
             ",PNCODCLF="+cp_ToStrODBCNull(this.w_PNCODCLF)+;
             ",PNCOMIVA="+cp_ToStrODBC(this.w_PNCOMIVA)+;
             ",PNDATPLA="+cp_ToStrODBC(this.w_PNDATPLA)+;
             ",PNCODVAL="+cp_ToStrODBCNull(this.w_PNCODVAL)+;
             ",PNCAOVAL="+cp_ToStrODBC(this.w_PNCAOVAL)+;
             ",PNTOTDOC="+cp_ToStrODBC(this.w_PNTOTDOC)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",PNFLPROV="+cp_ToStrODBC(this.w_PNFLPROV)+;
             ",PNRIFDOC="+cp_ToStrODBC(this.w_PNRIFDOC)+;
             ",PNRIFDIS="+cp_ToStrODBC(this.w_PNRIFDIS)+;
             ",PNFLGDIF="+cp_ToStrODBC(this.w_PNFLGDIF)+;
             ",PNRIFINC="+cp_ToStrODBC(this.w_PNRIFINC)+;
             ",PNRIFCES="+cp_ToStrODBC(this.w_PNRIFCES)+;
             ",PNRIFACC="+cp_ToStrODBC(this.w_PNRIFACC)+;
             ",PNNUMTRA="+cp_ToStrODBC(this.w_PNNUMTRA)+;
             ",PNNUMTR2="+cp_ToStrODBC(this.w_PNNUMTR2)+;
             ",PNRIFSAL="+cp_ToStrODBC(this.w_PNRIFSAL)+;
             ",PNFLGSTO="+cp_ToStrODBC(this.w_PNFLGSTO)+;
             ",PNSCRASS="+cp_ToStrODBC(this.w_PNSCRASS)+;
             ",PNTOTENA="+cp_ToStrODBC(this.w_PNTOTENA)+;
             ",PNAGG_01="+cp_ToStrODBC(this.w_PNAGG_01)+;
             ",PNAGG_02="+cp_ToStrODBC(this.w_PNAGG_02)+;
             ",PNAGG_03="+cp_ToStrODBC(this.w_PNAGG_03)+;
             ",PNAGG_04="+cp_ToStrODBC(this.w_PNAGG_04)+;
             ",PNAGG_05="+cp_ToStrODBC(this.w_PNAGG_05)+;
             ",PNAGG_06="+cp_ToStrODBC(this.w_PNAGG_06)+;
             ",PNNUMFAT="+cp_ToStrODBC(this.w_PNNUMFAT)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'PNT_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'PNSERIAL',this.w_PNSERIAL  )
          UPDATE (i_cTable) SET;
              PNCODESE=this.w_PNCODESE;
             ,PNNUMRER=this.w_PNNUMRER;
             ,PNCODUTE=this.w_PNCODUTE;
             ,PNDATREG=this.w_PNDATREG;
             ,PNCODCAU=this.w_PNCODCAU;
             ,PNCOMPET=this.w_PNCOMPET;
             ,PNPRG=this.w_PNPRG;
             ,PNVALNAZ=this.w_PNVALNAZ;
             ,PNTIPCLF=this.w_PNTIPCLF;
             ,PNFLIVDF=this.w_PNFLIVDF;
             ,PNTIPREG=this.w_PNTIPREG;
             ,PNNUMREG=this.w_PNNUMREG;
             ,PNTIPDOC=this.w_PNTIPDOC;
             ,PNPRD=this.w_PNPRD;
             ,PNPRP=this.w_PNPRP;
             ,PNALFDOC=this.w_PNALFDOC;
             ,PNNUMDOC=this.w_PNNUMDOC;
             ,PNDATDOC=this.w_PNDATDOC;
             ,PNANNDOC=this.w_PNANNDOC;
             ,PNANNPRO=this.w_PNANNPRO;
             ,PNFLREGI=this.w_PNFLREGI;
             ,PNDESSUP=this.w_PNDESSUP;
             ,PN__MESE=this.w_PN__MESE;
             ,PN__ANNO=this.w_PN__ANNO;
             ,PNALFPRO=this.w_PNALFPRO;
             ,PNNUMPRO=this.w_PNNUMPRO;
             ,PNCODCLF=this.w_PNCODCLF;
             ,PNCOMIVA=this.w_PNCOMIVA;
             ,PNDATPLA=this.w_PNDATPLA;
             ,PNCODVAL=this.w_PNCODVAL;
             ,PNCAOVAL=this.w_PNCAOVAL;
             ,PNTOTDOC=this.w_PNTOTDOC;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,PNFLPROV=this.w_PNFLPROV;
             ,PNRIFDOC=this.w_PNRIFDOC;
             ,PNRIFDIS=this.w_PNRIFDIS;
             ,PNFLGDIF=this.w_PNFLGDIF;
             ,PNRIFINC=this.w_PNRIFINC;
             ,PNRIFCES=this.w_PNRIFCES;
             ,PNRIFACC=this.w_PNRIFACC;
             ,PNNUMTRA=this.w_PNNUMTRA;
             ,PNNUMTR2=this.w_PNNUMTR2;
             ,PNRIFSAL=this.w_PNRIFSAL;
             ,PNFLGSTO=this.w_PNFLGSTO;
             ,PNSCRASS=this.w_PNSCRASS;
             ,PNTOTENA=this.w_PNTOTENA;
             ,PNAGG_01=this.w_PNAGG_01;
             ,PNAGG_02=this.w_PNAGG_02;
             ,PNAGG_03=this.w_PNAGG_03;
             ,PNAGG_04=this.w_PNAGG_04;
             ,PNAGG_05=this.w_PNAGG_05;
             ,PNAGG_06=this.w_PNAGG_06;
             ,PNNUMFAT=this.w_PNNUMFAT;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for ((t_PNIMPDAR<>0 OR t_PNIMPAVE<>0 OR t_PNFLZERO="S") AND NOT EMPTY(t_PNCODCON)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.PNT_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.PNT_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Deleting row children
              this.GSCG_MCA.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_PNSERIAL,"MRSERIAL";
                     ,this.w_CPROWNUM,"MRROWORD";
                     )
              this.GSCG_MPA.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_PNSERIAL,"PTSERIAL";
                     ,this.w_CPROWNUM,"PTROWORD";
                     )
              this.GSCG_MMC.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_PNSERIAL,"CCSERIAL";
                     ,this.w_CPROWNUM,"CCROWRIF";
                     )
              this.GSCG_MCA.mDelete()
              this.GSCG_MPA.mDelete()
              this.GSCG_MMC.mDelete()
              *
              * delete from PNT_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PNT_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PNTIPCON="+cp_ToStrODBC(this.w_PNTIPCON)+;
                     ",CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",PNDESRIG="+cp_ToStrODBC(this.w_PNDESRIG)+;
                     ",PNCAURIG="+cp_ToStrODBCNull(this.w_PNCAURIG)+;
                     ",PNCODCON="+cp_ToStrODBCNull(this.w_PNCODCON)+;
                     ",PNFLSALD="+cp_ToStrODBC(this.w_PNFLSALD)+;
                     ",PNFLSALI="+cp_ToStrODBC(this.w_PNFLSALI)+;
                     ",PNFLSALF="+cp_ToStrODBC(this.w_PNFLSALF)+;
                     ",PNCODPAG="+cp_ToStrODBCNull(this.w_PNCODPAG)+;
                     ",PNFLZERO="+cp_ToStrODBC(this.w_PNFLZERO)+;
                     ",PNIMPDAR="+cp_ToStrODBC(this.w_PNIMPDAR)+;
                     ",PNIMPAVE="+cp_ToStrODBC(this.w_PNIMPAVE)+;
                     ",PNFLPART="+cp_ToStrODBC(this.w_PNFLPART)+;
                     ",PNFLABAN="+cp_ToStrODBC(this.w_PNFLABAN)+;
                     ",PNINICOM="+cp_ToStrODBC(this.w_PNINICOM)+;
                     ",PNFINCOM="+cp_ToStrODBC(this.w_PNFINCOM)+;
                     ",PNIMPIND="+cp_ToStrODBC(this.w_PNIMPIND)+;
                     ",PNCODAGE="+cp_ToStrODBCNull(this.w_PNCODAGE)+;
                     ",PNFLVABD="+cp_ToStrODBC(this.w_PNFLVABD)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PNTIPCON=this.w_PNTIPCON;
                     ,CPROWORD=this.w_CPROWORD;
                     ,PNDESRIG=this.w_PNDESRIG;
                     ,PNCAURIG=this.w_PNCAURIG;
                     ,PNCODCON=this.w_PNCODCON;
                     ,PNFLSALD=this.w_PNFLSALD;
                     ,PNFLSALI=this.w_PNFLSALI;
                     ,PNFLSALF=this.w_PNFLSALF;
                     ,PNCODPAG=this.w_PNCODPAG;
                     ,PNFLZERO=this.w_PNFLZERO;
                     ,PNIMPDAR=this.w_PNIMPDAR;
                     ,PNIMPAVE=this.w_PNIMPAVE;
                     ,PNFLPART=this.w_PNFLPART;
                     ,PNFLABAN=this.w_PNFLABAN;
                     ,PNINICOM=this.w_PNINICOM;
                     ,PNFINCOM=this.w_PNFINCOM;
                     ,PNIMPIND=this.w_PNIMPIND;
                     ,PNCODAGE=this.w_PNCODAGE;
                     ,PNFLVABD=this.w_PNFLVABD;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask header and footer children to save themselves
      * --- GSCG_MIV : Saving
      this.GSCG_MIV.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PNSERIAL,"IVSERIAL";
             )
      this.GSCG_MIV.mReplace()
      * --- GSCG_MAC : Saving
      this.GSCG_MAC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PNSERIAL,"ACSERIAL";
             )
      this.GSCG_MAC.mReplace()
      * --- GSCG_MDR : Saving
      this.GSCG_MDR.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PNSERIAL,"DRSERIAL";
             )
      this.GSCG_MDR.mReplace()
      * --- GSCG_ASA : Saving
      this.GSCG_ASA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PNSERIAL,"SASERIAL";
             )
      this.GSCG_ASA.mReplace()
      * --- GSCG_AOS : Saving
      this.GSCG_AOS.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PNSERIAL,"OSSERIAL";
             )
      this.GSCG_AOS.mReplace()
      * --- Ask children belonging to rows to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for ((t_PNIMPDAR<>0 OR t_PNIMPAVE<>0 OR t_PNFLZERO="S") AND NOT EMPTY(t_PNCODCON))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        this.GSCG_MCA.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_PNSERIAL,"MRSERIAL";
             ,this.w_CPROWNUM,"MRROWORD";
             )
        this.GSCG_MCA.mReplace()
        this.GSCG_MPA.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_PNSERIAL,"PTSERIAL";
             ,this.w_CPROWNUM,"PTROWORD";
             )
        this.GSCG_MPA.mReplace()
        this.GSCG_MMC.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_PNSERIAL,"CCSERIAL";
             ,this.w_CPROWNUM,"CCROWRIF";
             )
        this.GSCG_MMC.mReplace()
        this.GSCG_MCA.bSaveContext=.f.
        this.GSCG_MPA.bSaveContext=.f.
        this.GSCG_MMC.bSaveContext=.f.
      endscan
     this.GSCG_MCA.bSaveContext=.t.
     this.GSCG_MPA.bSaveContext=.t.
     this.GSCG_MMC.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)2
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- gscg_mpn
    * --- Esegue Controlli Finali se non ho errori...
     * --- temporaneo eseguita nella GSCG_MPN.mReplace
     this.WorkFromTrs()
    if not(bTrsErr)
       this.NotifyEvent('ControlliFinali')
       * --- Memorizza l'ultima Operazione
       APPPNT=this.cFunction
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Detail Transaction
  proc mRestoreTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2,i_cOp3,i_cOp4,i_cOp5,i_cOp6

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.SALDICON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDICON_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..PNFLSALD,space(1))==this.w_PNFLSALD;
              and NVL(&i_cF..SLIMPDAR,0)==this.w_SLIMPDAR;
              and NVL(&i_cF..PNFLSALD,space(1))==this.w_PNFLSALD;
              and NVL(&i_cF..SLIMPAVE,0)==this.w_SLIMPAVE;
              and NVL(&i_cF..PNFLSALI,space(1))==this.w_PNFLSALI;
              and NVL(&i_cF..SLIMPDAR,0)==this.w_SLIMPDAR;
              and NVL(&i_cF..PNFLSALI,space(1))==this.w_PNFLSALI;
              and NVL(&i_cF..SLIMPAVE,0)==this.w_SLIMPAVE;
              and NVL(&i_cF..PNFLSALF,space(1))==this.w_PNFLSALF;
              and NVL(&i_cF..SLIMPAVE,0)==this.w_SLIMPAVE;
              and NVL(&i_cF..PNFLSALF,space(1))==this.w_PNFLSALF;
              and NVL(&i_cF..SLIMPDAR,0)==this.w_SLIMPDAR;
              and NVL(&i_cF..PNCODCON,space(15))==this.w_PNCODCON;
              and NVL(&i_cF..PNTIPCON,space(1))==this.w_PNTIPCON;
              and NVL(&i_cF..PNCOMPET,space(4))==this.w_PNCOMPET;

      i_cOp1=cp_SetTrsOp(NVL(&i_cF..PNFLSALD,space(1)),'SLDARPER','',NVL(&i_cF..SLIMPDAR,0),'restore',i_nConn)
      i_cOp2=cp_SetTrsOp(NVL(&i_cF..PNFLSALD,space(1)),'SLAVEPER','',NVL(&i_cF..SLIMPAVE,0),'restore',i_nConn)
      i_cOp3=cp_SetTrsOp(NVL(&i_cF..PNFLSALI,space(1)),'SLDARINI','',NVL(&i_cF..SLIMPDAR,0),'restore',i_nConn)
      i_cOp4=cp_SetTrsOp(NVL(&i_cF..PNFLSALI,space(1)),'SLAVEINI','',NVL(&i_cF..SLIMPAVE,0),'restore',i_nConn)
      i_cOp5=cp_SetTrsOp(NVL(&i_cF..PNFLSALF,space(1)),'SLDARFIN','',NVL(&i_cF..SLIMPAVE,0),'restore',i_nConn)
      i_cOp6=cp_SetTrsOp(NVL(&i_cF..PNFLSALF,space(1)),'SLAVEFIN','',NVL(&i_cF..SLIMPDAR,0),'restore',i_nConn)
      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..PNCODCON,space(15))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
           +" SLDARPER="+i_cOp1+","           +" SLAVEPER="+i_cOp2+","           +" SLDARINI="+i_cOp3+","           +" SLAVEINI="+i_cOp4+","           +" SLDARFIN="+i_cOp5+","           +" SLAVEFIN="+i_cOp6+","  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SLCODICE="+cp_ToStrODBC(NVL(&i_cF..PNCODCON,space(15)));
             +" AND SLTIPCON="+cp_ToStrODBC(NVL(&i_cF..PNTIPCON,space(1)));
             +" AND SLCODESE="+cp_ToStrODBC(NVL(&i_cF..PNCOMPET,space(4)));
             )
      endif
    else
      i_cOp1=cp_SetTrsOp(&i_cF..PNFLSALD,'SLDARPER',i_cF+'.SLIMPDAR',&i_cF..SLIMPDAR,'restore',0)
      i_cOp2=cp_SetTrsOp(&i_cF..PNFLSALD,'SLAVEPER',i_cF+'.SLIMPAVE',&i_cF..SLIMPAVE,'restore',0)
      i_cOp3=cp_SetTrsOp(&i_cF..PNFLSALI,'SLDARINI',i_cF+'.SLIMPDAR',&i_cF..SLIMPDAR,'restore',0)
      i_cOp4=cp_SetTrsOp(&i_cF..PNFLSALI,'SLAVEINI',i_cF+'.SLIMPAVE',&i_cF..SLIMPAVE,'restore',0)
      i_cOp5=cp_SetTrsOp(&i_cF..PNFLSALF,'SLDARFIN',i_cF+'.SLIMPAVE',&i_cF..SLIMPAVE,'restore',0)
      i_cOp6=cp_SetTrsOp(&i_cF..PNFLSALF,'SLAVEFIN',i_cF+'.SLIMPDAR',&i_cF..SLIMPDAR,'restore',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SLTIPCON',&i_cF..PNTIPCON;
                 ,'SLCODESE',&i_cF..PNCOMPET;
                 ,'SLCODICE',&i_cF..PNCODCON)
      UPDATE (i_cTable) SET ;
           SLDARPER=&i_cOp1.  ,;
           SLAVEPER=&i_cOp2.  ,;
           SLDARINI=&i_cOp3.  ,;
           SLAVEINI=&i_cOp4.  ,;
           SLDARFIN=&i_cOp5.  ,;
           SLAVEFIN=&i_cOp6.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Detail Transaction
  proc mUpdateTrsDetail(i_bCanSkip)
    local i_cWherel,i_cF,i_nModRow,i_nConn,i_cTable,i_oRow
    local i_cOp1,i_cOp2,i_cOp3,i_cOp4,i_cOp5,i_cOp6

    i_cF=this.cTrsName
    i_nConn = i_TableProp[this.SALDICON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDICON_IDX,2])
    i_oRow=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..PNFLSALD,space(1))==this.w_PNFLSALD;
              and NVL(&i_cF..SLIMPDAR,0)==this.w_SLIMPDAR;
              and NVL(&i_cF..PNFLSALD,space(1))==this.w_PNFLSALD;
              and NVL(&i_cF..SLIMPAVE,0)==this.w_SLIMPAVE;
              and NVL(&i_cF..PNFLSALI,space(1))==this.w_PNFLSALI;
              and NVL(&i_cF..SLIMPDAR,0)==this.w_SLIMPDAR;
              and NVL(&i_cF..PNFLSALI,space(1))==this.w_PNFLSALI;
              and NVL(&i_cF..SLIMPAVE,0)==this.w_SLIMPAVE;
              and NVL(&i_cF..PNFLSALF,space(1))==this.w_PNFLSALF;
              and NVL(&i_cF..SLIMPAVE,0)==this.w_SLIMPAVE;
              and NVL(&i_cF..PNFLSALF,space(1))==this.w_PNFLSALF;
              and NVL(&i_cF..SLIMPDAR,0)==this.w_SLIMPDAR;
              and NVL(&i_cF..PNCODCON,space(15))==this.w_PNCODCON;
              and NVL(&i_cF..PNTIPCON,space(1))==this.w_PNTIPCON;
              and NVL(&i_cF..PNCOMPET,space(4))==this.w_PNCOMPET;

      i_cOp1=cp_SetTrsOp(this.w_PNFLSALD,'SLDARPER','this.w_SLIMPDAR',this.w_SLIMPDAR,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_PNFLSALD,'SLAVEPER','this.w_SLIMPAVE',this.w_SLIMPAVE,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_PNFLSALI,'SLDARINI','this.w_SLIMPDAR',this.w_SLIMPDAR,'update',i_nConn)
      i_cOp4=cp_SetTrsOp(this.w_PNFLSALI,'SLAVEINI','this.w_SLIMPAVE',this.w_SLIMPAVE,'update',i_nConn)
      i_cOp5=cp_SetTrsOp(this.w_PNFLSALF,'SLDARFIN','this.w_SLIMPAVE',this.w_SLIMPAVE,'update',i_nConn)
      i_cOp6=cp_SetTrsOp(this.w_PNFLSALF,'SLAVEFIN','this.w_SLIMPDAR',this.w_SLIMPDAR,'update',i_nConn)
      if !i_bSkip and !Empty(this.w_PNCODCON)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" SLDARPER="+i_cOp1  +",";
         +" SLAVEPER="+i_cOp2  +",";
         +" SLDARINI="+i_cOp3  +",";
         +" SLAVEINI="+i_cOp4  +",";
         +" SLDARFIN="+i_cOp5  +",";
         +" SLAVEFIN="+i_cOp6  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE SLCODICE="+cp_ToStrODBC(this.w_PNCODCON);
           +" AND SLTIPCON="+cp_ToStrODBC(this.w_PNTIPCON);
           +" AND SLCODESE="+cp_ToStrODBC(this.w_PNCOMPET);
           )
        if i_nModRow<1 .and. .not. empty(this.w_PNCODCON)
        i_cOp1=cp_SetTrsOp(this.w_PNFLSALD,'SLDARPER','this.w_SLIMPDAR',this.w_SLIMPDAR,'insert',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_PNFLSALD,'SLAVEPER','this.w_SLIMPAVE',this.w_SLIMPAVE,'insert',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_PNFLSALI,'SLDARINI','this.w_SLIMPDAR',this.w_SLIMPDAR,'insert',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_PNFLSALI,'SLAVEINI','this.w_SLIMPAVE',this.w_SLIMPAVE,'insert',i_nConn)
        i_cOp5=cp_SetTrsOp(this.w_PNFLSALF,'SLDARFIN','this.w_SLIMPAVE',this.w_SLIMPAVE,'insert',i_nConn)
        i_cOp6=cp_SetTrsOp(this.w_PNFLSALF,'SLAVEFIN','this.w_SLIMPDAR',this.w_SLIMPDAR,'insert',i_nConn)
          =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" (SLCODICE,SLTIPCON,SLCODESE  ;
             ,SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN,CPCCCHK) VALUES ("+cp_ToStrODBC(this.w_PNCODCON)+","+cp_ToStrODBC(this.w_PNTIPCON)+","+cp_ToStrODBC(this.w_PNCOMPET)  ;
             +","+i_cOp1;
             +","+i_cOp2;
             +","+i_cOp3;
             +","+i_cOp4;
             +","+i_cOp5;
             +","+i_cOp6;
             +","+cp_ToStrODBC(cp_NewCCChk())+")")
        endif
      endif
    else
      i_cOp1=cp_SetTrsOp(this.w_PNFLSALD,'SLDARPER','this.w_SLIMPDAR',this.w_SLIMPDAR,'update',0)
      i_cOp2=cp_SetTrsOp(this.w_PNFLSALD,'SLAVEPER','this.w_SLIMPAVE',this.w_SLIMPAVE,'update',0)
      i_cOp3=cp_SetTrsOp(this.w_PNFLSALI,'SLDARINI','this.w_SLIMPDAR',this.w_SLIMPDAR,'update',0)
      i_cOp4=cp_SetTrsOp(this.w_PNFLSALI,'SLAVEINI','this.w_SLIMPAVE',this.w_SLIMPAVE,'update',0)
      i_cOp5=cp_SetTrsOp(this.w_PNFLSALF,'SLDARFIN','this.w_SLIMPAVE',this.w_SLIMPAVE,'update',0)
      i_cOp6=cp_SetTrsOp(this.w_PNFLSALF,'SLAVEFIN','this.w_SLIMPDAR',this.w_SLIMPDAR,'update',0)
      i_cWhere = cp_PKFox(i_cTable;
                 ,'SLTIPCON',this.w_PNTIPCON;
                 ,'SLCODESE',this.w_PNCOMPET;
                 ,'SLCODICE',this.w_PNCODCON)
      UPDATE (i_cTable) SET;
           SLDARPER=&i_cOp1.  ,;
           SLAVEPER=&i_cOp2.  ,;
           SLDARINI=&i_cOp3.  ,;
           SLAVEINI=&i_cOp4.  ,;
           SLDARFIN=&i_cOp5.  ,;
           SLAVEFIN=&i_cOp6.  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
      i_nModRow = _tally
      if i_nModRow<1 .and. .not. empty(this.w_PNCODCON)
        i_cOp1=cp_SetTrsOp(this.w_PNFLSALD,'SLDARPER','this.w_SLIMPDAR',this.w_SLIMPDAR,'insert',0)
        i_cOp2=cp_SetTrsOp(this.w_PNFLSALD,'SLAVEPER','this.w_SLIMPAVE',this.w_SLIMPAVE,'insert',0)
        i_cOp3=cp_SetTrsOp(this.w_PNFLSALI,'SLDARINI','this.w_SLIMPDAR',this.w_SLIMPDAR,'insert',0)
        i_cOp4=cp_SetTrsOp(this.w_PNFLSALI,'SLAVEINI','this.w_SLIMPAVE',this.w_SLIMPAVE,'insert',0)
        i_cOp5=cp_SetTrsOp(this.w_PNFLSALF,'SLDARFIN','this.w_SLIMPAVE',this.w_SLIMPAVE,'insert',0)
        i_cOp6=cp_SetTrsOp(this.w_PNFLSALF,'SLAVEFIN','this.w_SLIMPDAR',this.w_SLIMPDAR,'insert',0)
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODESE',this.w_PNCOMPET,'SLCODICE',this.w_PNCODCON)
        INSERT INTO (i_cTable) (SLCODICE,SLTIPCON,SLCODESE  ;
         ,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN,CPCCCHK) VALUES (this.w_PNCODCON,this.w_PNTIPCON,this.w_PNCOMPET  ;
           ,&i_cOp1.  ;
           ,&i_cOp2.  ;
           ,&i_cOp3.  ;
           ,&i_cOp4.  ;
           ,&i_cOp5.  ;
           ,&i_cOp6.  ,cp_NewCCChk())
      endif
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCG_MIV : Deleting
    this.GSCG_MIV.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PNSERIAL,"IVSERIAL";
           )
    this.GSCG_MIV.mDelete()
    * --- GSCG_MAC : Deleting
    this.GSCG_MAC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PNSERIAL,"ACSERIAL";
           )
    this.GSCG_MAC.mDelete()
    * --- GSCG_MDR : Deleting
    this.GSCG_MDR.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PNSERIAL,"DRSERIAL";
           )
    this.GSCG_MDR.mDelete()
    * --- GSCG_ASA : Deleting
    this.GSCG_ASA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PNSERIAL,"SASERIAL";
           )
    this.GSCG_ASA.mDelete()
    * --- GSCG_AOS : Deleting
    this.GSCG_AOS.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PNSERIAL,"OSSERIAL";
           )
    this.GSCG_AOS.mDelete()
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for ((t_PNIMPDAR<>0 OR t_PNIMPAVE<>0 OR t_PNFLZERO="S") AND NOT EMPTY(t_PNCODCON)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSCG_MCA : Deleting
        this.GSCG_MCA.bSaveContext=.f.
        this.GSCG_MCA.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_PNSERIAL,"MRSERIAL";
               ,this.w_CPROWNUM,"MRROWORD";
               )
        this.GSCG_MCA.bSaveContext=.t.
        this.GSCG_MCA.mDelete()
        * --- GSCG_MPA : Deleting
        this.GSCG_MPA.bSaveContext=.f.
        this.GSCG_MPA.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_PNSERIAL,"PTSERIAL";
               ,this.w_CPROWNUM,"PTROWORD";
               )
        this.GSCG_MPA.bSaveContext=.t.
        this.GSCG_MPA.mDelete()
        * --- GSCG_MMC : Deleting
        this.GSCG_MMC.bSaveContext=.f.
        this.GSCG_MMC.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_PNSERIAL,"CCSERIAL";
               ,this.w_CPROWNUM,"CCROWRIF";
               )
        this.GSCG_MMC.bSaveContext=.t.
        this.GSCG_MMC.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.PNT_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.PNT_DETT_IDX,2])
        *
        * delete PNT_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
        *
        * delete PNT_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for ((t_PNIMPDAR<>0 OR t_PNIMPAVE<>0 OR t_PNFLZERO="S") AND NOT EMPTY(t_PNCODCON)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gscg_mpn
    * - Nel caso in cui non � una registrazione di contabilizzazione documento
    * - tento di recuperare il progressivo
    if not(bTrsErr) And Empty(this.w_PNRIFDOC)
        * --- Controlli Finali
        this.NotifyEvent('RecuperoProg')
    endif
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,6,.t.)
          .w_CODAZI = i_CODAZI
          .link_1_8('Full')
        .DoRTCalc(8,16,.t.)
        if .o_PNCODCAU<>.w_PNCODCAU.or. .o_PNDATREG<>.w_PNDATREG
          .w_PNPRG = IIF(g_UNIUTE $ 'GE', DTOS(.w_PNDATREG), SPACE(8))
        endif
        if .o_PNCOMPET<>.w_PNCOMPET
          .link_1_19('Full')
        endif
        .DoRTCalc(19,20,.t.)
        if .o_PNSERIAL<>.w_PNSERIAL.or. .o_PNVALNAZ<>.w_PNVALNAZ
          .w_CALCPICP = DEFPIP(.w_DECTOP)
        endif
        .DoRTCalc(22,30,.t.)
          .w_PNTIPCLF = .w_FLRIFE
        .DoRTCalc(32,38,.t.)
        if .o_PNCODCAU<>.w_PNCODCAU
          .w_PNPRD = IIF(.w_PNTIPREG='V' OR (.w_PNTIPREG='C' AND .w_PNTIPDOC='FC'), 'FV', 'NN')
        endif
        if .o_PNCODCAU<>.w_PNCODCAU
          .w_PNPRP = IIF(.w_PNTIPREG='A', 'AC', 'NN')
        endif
        if .o_PNCODCAU<>.w_PNCODCAU.or. .o_PNTIPCON<>.w_PNTIPCON.or. .o_PNCODCON<>.w_PNCODCON
          .w_TESTPART = IIF(g_PERPAR="S", .w_FLPART, "N")
        endif
        .DoRTCalc(42,47,.t.)
        if .o_PNDATREG<>.w_PNDATREG.or. .o_PNDATDOC<>.w_PNDATDOC.or. .o_PNCOMPET<>.w_PNCOMPET
          .w_OBTEST = MIN(IIF(EMPTY(.w_PNDATDOC),i_FINDAT,.w_PNDATDOC), .w_PNDATREG, .w_FINESE)
        endif
        if .o_PNDATDOC<>.w_PNDATDOC.or. .o_PNDATREG<>.w_PNDATREG.or. .o_PNCOMPET<>.w_PNCOMPET.or. .o_PNCODCAU<>.w_PNCODCAU
          .w_PNANNDOC = CALPRO(IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC), .w_PNCOMPET, .w_FLPDOC)
        endif
        if .o_PNDATREG<>.w_PNDATREG.or. .o_PNCOMPET<>.w_PNCOMPET.or. .o_PNCODCAU<>.w_PNCODCAU
          .w_PNANNPRO = CALPRO(.w_PNDATREG, .w_PNCOMPET, .w_FLPPRO)
        endif
        .DoRTCalc(51,52,.t.)
        if .o_PNDATDOC<>.w_PNDATDOC
          .w_PN__MESE = month(.w_PNDATDOC)
        endif
        if .o_PNDATDOC<>.w_PNDATDOC
          .w_PN__ANNO = year(.w_PNDATDOC)
        endif
        .DoRTCalc(55,57,.t.)
        if .o_PNDATREG<>.w_PNDATREG
          .w_PNCOMIVA = .w_PNDATREG
        endif
        if .o_PNDATREG<>.w_PNDATREG
          .w_PNDATPLA = .w_PNDATREG
        endif
        if .o_PNVALNAZ<>.w_PNVALNAZ.or. .o_PNCODVAL<>.w_PNCODVAL.or. .o_CLFVAL<>.w_CLFVAL
          .w_PNCODVAL = iif(not empty( .w_PNCODVAL ) ,.w_PNCODVAL,iif( not empty( .w_CLFVAL ) , .w_CLFVAL , .w_PNVALNAZ ))
          .link_1_62('Full')
        endif
        .DoRTCalc(61,62,.t.)
        if .o_PNCODVAL<>.w_PNCODVAL.or. .o_PNSERIAL<>.w_PNSERIAL
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        if .o_PNCODVAL<>.w_PNCODVAL.or. .o_PNDATREG<>.w_PNDATREG.or. .o_PNDATDOC<>.w_PNDATDOC.or. .o_PNTIPREG<>.w_PNTIPREG
          .w_PNCAOVAL = GETCAM(.w_PNCODVAL, IIF( .w_PNTIPREG='A', iif(.w_AZDATDOC='S' and Not Empty(.w_PNDATDOC) ,.w_PNDATDOC,.w_PNDATREG),IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC)), 7)
        endif
        .DoRTCalc(65,70,.t.)
        if .o_PNCODCLF<>.w_PNCODCLF.or. .o_PNTIPCLF<>.w_PNTIPCLF
          .w_CODPAG = IIF(.w_DATMOR>=IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC) AND NOT EMPTY(.w_PAGMOR), .w_PAGMOR, .w_PAGCLF)
          .link_1_73('Full')
        endif
        .DoRTCalc(72,78,.t.)
        if .o_PNIMPDAR<>.w_PNIMPDAR.or. .o_PNIMPAVE<>.w_PNIMPAVE
          .w_TOTFLAG = AGGFLAG(.w_FLAGTOT, .o_PNIMPDAR+.o_PNIMPAVE, .w_PNIMPDAR+.w_PNIMPAVE, .w_TOTFLAG)
        endif
        .DoRTCalc(80,82,.t.)
        if .o_PNSERIAL<>.w_PNSERIAL
          .w_ODATREG = .w_PNDATREG
        endif
        if .o_PNSERIAL<>.w_PNSERIAL
          .w_OCOMIVA = .w_PNCOMIVA
        endif
        .oPgFrm.Page1.oPag.oObj_1_121.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_122.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_123.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_124.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_125.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_126.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_127.Calculate()
        .DoRTCalc(85,95,.t.)
        if .o_PNCODCAU<>.w_PNCODCAU
          .w_FLAGAC = 'N'
        endif
        .oPgFrm.Page1.oPag.oObj_1_131.Calculate()
        .DoRTCalc(97,102,.t.)
        if .o_PNDESSUP<>.w_PNDESSUP.or. .o_PNCODCON<>.w_PNCODCON
          .w_PNDESRIG = IIF( EMPTY(.w_PNDESRIG), .w_PNDESSUP, .w_PNDESRIG)
        endif
        if .o_PNCODCAU<>.w_PNCODCAU
          .w_PNCAURIG = .w_PNCODCAU
          .link_2_4('Full')
        endif
        .DoRTCalc(105,106,.t.)
        if .o_PNTIPCON<>.w_PNTIPCON
          .link_2_7('Full')
        endif
        .DoRTCalc(108,109,.t.)
        if .o_PNTIPCON<>.w_PNTIPCON.or. .o_PNCODCON<>.w_PNCODCON
          .w_SEZB = IIF(.w_PNTIPCON="G", CALCSEZ(.w_MASTRO), " ")
        endif
        .DoRTCalc(111,111,.t.)
        if .o_PNFLPROV<>.w_PNFLPROV
          .w_PNFLSALD = IIF(.w_PNFLPROV='S', ' ', '+')
        endif
        .DoRTCalc(113,113,.t.)
        if .o_PNCODCAU<>.w_PNCODCAU.or. .o_PNFLPROV<>.w_PNFLPROV.or. .o_SEZB<>.w_SEZB
          .w_PNFLSALI = IIF(.w_FLSALI='S' AND .w_PNFLPROV<>'S' AND .w_SEZB<>'T', '+', ' ')
        endif
        .DoRTCalc(115,115,.t.)
        if .o_PNCODCAU<>.w_PNCODCAU.or. .o_PNFLPROV<>.w_PNFLPROV.or. .o_SEZB<>.w_SEZB
          .w_PNFLSALF = IIF(.w_FLSALF='S' AND .w_PNFLPROV<>'S' AND .w_SEZB<>'T', '+', ' ')
        endif
        .DoRTCalc(117,123,.t.)
        if .o_PNCODCON<>.w_PNCODCON
          .w_PNCODPAG = IIF(.w_DATMOR1>=IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC) AND NOT EMPTY(.w_PAGMOR1), .w_PAGMOR1, IIF((NOT EMPTY(g_SAPAGA) AND .w_FLPART='A'),g_SAPAGA,IIF(.w_FLINSO='S' AND NOT EMPTY(NVL(.w_PAGINS,'')), .w_PAGINS,.w_PAGCLF1)))
          .link_2_24('Full')
        endif
        .DoRTCalc(125,128,.t.)
        if .o_PNIMPDAR<>.w_PNIMPDAR.or. .o_PNIMPAVE<>.w_PNIMPAVE
          .w_TOTDAR = .w_TOTDAR-.w_impdar
          .w_IMPDAR = .w_PNIMPDAR
          .w_TOTDAR = .w_TOTDAR+.w_impdar
        endif
        if .o_PNIMPDAR<>.w_PNIMPDAR.or. .o_PNIMPAVE<>.w_PNIMPAVE
          .w_SLIMPDAR = IIF(.w_PNIMPDAR-.w_PNIMPAVE>=0,.w_PNIMPDAR-.w_PNIMPAVE,0)
        endif
        if .o_PNIMPAVE<>.w_PNIMPAVE.or. .o_PNIMPDAR<>.w_PNIMPDAR
          .w_TOTAVE = .w_TOTAVE-.w_impave
          .w_IMPAVE = .w_PNIMPAVE
          .w_TOTAVE = .w_TOTAVE+.w_impave
        endif
        if .o_PNIMPDAR<>.w_PNIMPDAR.or. .o_PNIMPAVE<>.w_PNIMPAVE
          .w_SLIMPAVE = IIF(.w_PNIMPDAR-.w_PNIMPAVE<0,ABS(.w_PNIMPDAR-.w_PNIMPAVE),0)
        endif
        .DoRTCalc(133,133,.t.)
        if .o_PNIMPDAR<>.w_PNIMPDAR.or. .o_PNIMPAVE<>.w_PNIMPAVE.or. .o_PNTIPCON<>.w_PNTIPCON
          .w_FLDAVE = IIF(.w_PNIMPDAR<>0, "D", IIF(.w_PNIMPAVE<>0, "A", " "))
        endif
        if .o_PNSERIAL<>.w_PNSERIAL
          .link_2_35('Full')
        endif
        .DoRTCalc(136,136,.t.)
        if .o_PNIMPAVE<>.w_PNIMPAVE.or. .o_PNIMPDAR<>.w_PNIMPDAR.or. .o_PNCODCON<>.w_PNCODCON.or. .o_PNTIPCON<>.w_PNTIPCON
          .w_PARSAL = ' '
        endif
          .w_FLAPAR = IIF(EMPTY(.w_PNFLABAN) AND .cFunction='Load' AND .w_PARSAL<>'.'  AND NOT .w_FLPART $ 'CA', 'S', ' ')
        if .o_PNTIPCON<>.w_PNTIPCON.or. .o_PNCODCON<>.w_PNCODCON
          .w_PNFLPART = IIF(.w_PARTSN="S", .w_RIGPAR, "N")
        endif
        .DoRTCalc(140,143,.t.)
          .link_2_45('Full')
        .DoRTCalc(145,146,.t.)
        if .o_PNSERIAL<>.w_PNSERIAL
          .w_OANAL = IIF(.w_PNTIPCON="G" AND .w_SEZB $ "CR" AND .w_CCTAGG $ "AM", 'S', 'N')
        endif
        .DoRTCalc(148,149,.t.)
          .w_VARPAR = IIF(.w_PNFLPART $ 'CSA' AND g_PERPAR="S", 'N', 'S')
          .w_VARCEN = IIF(.w_PNTIPCON="G" AND .w_SEZB $ "CR" AND .w_FLANAL $ "SN" AND g_PERCCR="S" AND .w_CCTAGG $ "AM", 'N', 'S')
        .DoRTCalc(152,153,.t.)
          .w_SBILAN = .w_TOTDAR-.w_TOTAVE
        .oPgFrm.Page1.oPag.oObj_1_138.Calculate()
        .DoRTCalc(155,156,.t.)
        if .o_PNSERIAL<>.w_PNSERIAL
          .w_ONUMDOC = .w_PNNUMDOC
        endif
        if .o_PNSERIAL<>.w_PNSERIAL
          .w_ONUMPRO = .w_PNNUMPRO
        endif
        if .o_PNSERIAL<>.w_PNSERIAL
          .w_OALFDOC = .w_PNALFDOC
        endif
        if .o_PNSERIAL<>.w_PNSERIAL
          .w_OALFPRO = .w_PNALFPRO
        endif
        .DoRTCalc(161,164,.t.)
          .link_2_57('Full')
        .oPgFrm.Page1.oPag.oObj_1_151.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_61.Calculate()
        .DoRTCalc(166,172,.t.)
        if .o_PNCODCON<>.w_PNCODCON.or. .o_PNFLPART<>.w_PNFLPART
          .w_PNCODAGE = .w_AGECLF
          .link_2_63('Full')
        endif
        if .o_PNTIPCON<>.w_PNTIPCON.or. .o_PNCODCON<>.w_PNCODCON.or. .o_PNFLPART<>.w_PNFLPART.or. .o_PNTIPCLF<>.w_PNTIPCLF.or. .o_PNCODCLF<>.w_PNCODCLF
          .w_PNFLVABD = IIF(.w_PNFLPART $ 'AC', IIF(.w_PNTIPCLF='C', .w_FLVEBD, IIF(.w_PNTIPCLF='F', .w_FLACBD, ' ')), ' ')
        endif
        .oPgFrm.Page1.oPag.oObj_1_155.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_65.Calculate(IIF((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN)>=0, AH_MSGFORMAT('Saldo DARE:'), IIF((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN)<0, AH_MSGFORMAT('Saldo AVERE:'), '')))
        .DoRTCalc(175,185,.t.)
        if .o_PNCODCON<>.w_PNCODCON
          .w_PCODCON = .w_PNCODCON
          .link_2_74('Full')
        endif
          .w_SALDO = ABS((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN))
        .oPgFrm.Page1.oPag.oObj_1_165.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_170.Calculate()
        .DoRTCalc(188,201,.t.)
        if .o_PNCODCAU<>.w_PNCODCAU
          .w_PNFLGSTO = IIF(g_RITE='S',LOOKTAB("TAB_RITE",IIF(.w_PNTIPCLF='F',"TRFLGSTO","TRFLGST1"),"TRCODAZI",.w_CODAZI,"TRTIPRIT",IIF(.w_PNTIPCLF="F","A","V")),'S')
        endif
        .DoRTCalc(203,205,.t.)
        if .o_PNCODCAU<>.w_PNCODCAU
          .w_FLGSTO = IIF(g_RITE='S',LOOKTAB("TAB_RITE",IIF(.w_PNTIPCLF='F',"TRFLGSTO","TRFLGST1"),"TRCODAZI",.w_CODAZI,"TRTIPRIT",IIF(.w_PNTIPCLF="F","A","V")),'S')
        endif
        .DoRTCalc(207,211,.t.)
          .w_FLVEAC = IIF(.w_PNTIPREG='V' OR (.w_PNTIPREG='C' AND .w_PNTIPDOC='FC'), 'V', IIF(.w_PNTIPREG='A', 'A', ' '))
        .oPgFrm.Page1.oPag.oObj_1_185.Calculate()
        if .o_PNCAURIG<>.w_PNCAURIG.or. .o_PNCODCAU<>.w_PNCODCAU.or. .o_PNTIPCON<>.w_PNTIPCON.or. .o_PNIMPDAR<>.w_PNIMPDAR.or. .o_PNIMPAVE<>.w_PNIMPAVE
          .w_TOTFOR = .w_TOTFOR-.w_impfor
          .w_IMPFOR = IIF(NOT EMPTY(.w_PNCODCON) AND .w_PNCAURIG=.w_PNCODCAU and g_Rite='S' AND .w_PNTIPCON $ 'F-C' ,iif(.w_PNTIPCON='F',.w_PNIMPAVE-.w_PNIMPDAR,.w_PNIMPDAR-.w_PNIMPAVE),0)
          .w_TOTFOR = .w_TOTFOR+.w_impfor
        endif
        if .o_PNDATDOC<>.w_PNDATDOC.or. .o_PNNUMDOC<>.w_PNNUMDOC.or. .o_PNALFDOC<>.w_PNALFDOC.or. .o_PNANNDOC<>.w_PNANNDOC.or. .o_PNPRD<>.w_PNPRD
          .Calculate_JRYONOWEQW()
        endif
        if .o_PNNUMPRO<>.w_PNNUMPRO.or. .o_PNALFPRO<>.w_PNALFPRO.or. .o_PNDATREG<>.w_PNDATREG.or. .o_PNANNPRO<>.w_PNANNPRO.or. .o_PNPRP<>.w_PNPRP
          .Calculate_RQVJJSUQFW()
        endif
        if .o_OBTEST<>.w_OBTEST
          .Calculate_JLWOLQTQAO()
        endif
        .DoRTCalc(214,231,.t.)
        if .o_PNSERIAL<>.w_PNSERIAL
          .w_OSSERIAL = .w_PNSERIAL
          .link_1_221('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_224.Calculate()
        .DoRTCalc(233,240,.t.)
          .link_1_232('Full')
        .oPgFrm.Page1.oPag.oObj_1_241.Calculate()
        .DoRTCalc(242,248,.t.)
        if .o_PNSERIAL<>.w_PNSERIAL
          .w_OLDSERIAL = .w_PNSERIAL
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        this.Calculate_QYQODPLTRV()
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEPNT","i_codazi,w_PNSERIAL")
          .op_PNSERIAL = .w_PNSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_PNCODESE<>.w_PNCODESE .or. .op_PNCODUTE<>.w_PNCODUTE .or. .op_PNPRG<>.w_PNPRG
           cp_AskTableProg(this,i_nConn,"PRPNT","i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
          .op_PNNUMRER = .w_PNNUMRER
        endif
        if .op_codazi<>.w_codazi .or. .op_PNANNDOC<>.w_PNANNDOC .or. .op_PNPRD<>.w_PNPRD .or. .op_PNALFDOC<>.w_PNALFDOC
          if .w_PNANNDOC<>'    '
             cp_AskTableProg(this,i_nConn,"PRDOC","i_codazi,w_PNANNDOC,w_PNPRD,w_PNALFDOC,w_PNNUMDOC")
          endif
          .op_PNNUMDOC = .w_PNNUMDOC
        endif
        if .op_codazi<>.w_codazi .or. .op_PNANNPRO<>.w_PNANNPRO .or. .op_PNPRP<>.w_PNPRP .or. .op_PNALFPRO<>.w_PNALFPRO
          if .w_PNANNPRO<>'    '
             cp_AskTableProg(this,i_nConn,"PRPRO","i_codazi,w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO")
          endif
          .op_PNNUMPRO = .w_PNNUMPRO
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_PNCODESE = .w_PNCODESE
        .op_PNCODUTE = .w_PNCODUTE
        .op_PNPRG = .w_PNPRG
        .op_codazi = .w_codazi
        .op_PNANNDOC = .w_PNANNDOC
        .op_PNPRD = .w_PNPRD
        .op_PNALFDOC = .w_PNALFDOC
        .op_codazi = .w_codazi
        .op_PNANNPRO = .w_PNANNPRO
        .op_PNPRP = .w_PNPRP
        .op_PNALFPRO = .w_PNALFPRO
      endwith
      this.DoRTCalc(250,250,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_PNDESRIG with this.w_PNDESRIG
      replace t_PNCAURIG with this.w_PNCAURIG
      replace t_RIGPAR with this.w_RIGPAR
      replace t_CAUDES with this.w_CAUDES
      replace t_MASTRO with this.w_MASTRO
      replace t_SEZB with this.w_SEZB
      replace t_DESCO2 with this.w_DESCO2
      replace t_PNFLSALD with this.w_PNFLSALD
      replace t_PARTSN with this.w_PARTSN
      replace t_PNFLSALI with this.w_PNFLSALI
      replace t_CLMES1 with this.w_CLMES1
      replace t_PNFLSALF with this.w_PNFLSALF
      replace t_CLMES2 with this.w_CLMES2
      replace t_CLGIO1 with this.w_CLGIO1
      replace t_CLGIO2 with this.w_CLGIO2
      replace t_GIOFIS with this.w_GIOFIS
      replace t_PAGCLF1 with this.w_PAGCLF1
      replace t_PAGMOR1 with this.w_PAGMOR1
      replace t_DATMOR1 with this.w_DATMOR1
      replace t_PNCODPAG with this.w_PNCODPAG
      replace t_PNFLZERO with this.w_PNFLZERO
      replace t_CCTAGG with this.w_CCTAGG
      replace t_IMPDAR with this.w_IMPDAR
      replace t_SLIMPDAR with this.w_SLIMPDAR
      replace t_IMPAVE with this.w_IMPAVE
      replace t_SLIMPAVE with this.w_SLIMPAVE
      replace t_FLAGTOT with this.w_FLAGTOT
      replace t_FLDAVE with this.w_FLDAVE
      replace t_PNCODCON with this.w_PNCODCON
      replace t_BANAPP with this.w_BANAPP
      replace t_PARSAL with this.w_PARSAL
      replace t_FLAPAR with this.w_FLAPAR
      replace t_PARCAR with this.w_PARCAR
      replace t_PNFLABAN with this.w_PNFLABAN
      replace t_PNINICOM with this.w_PNINICOM
      replace t_PNFINCOM with this.w_PNFINCOM
      replace t_TIPSOT with this.w_TIPSOT
      replace t_OANAL with this.w_OANAL
      replace t_BANNOS with this.w_BANNOS
      replace t_PNIMPIND with this.w_PNIMPIND
      replace t_VARPAR with this.w_VARPAR
      replace t_VARCEN with this.w_VARCEN
      replace t_CAUMOV with this.w_CAUMOV
      replace t_CONBAN with this.w_CONBAN
      replace t_CALFES with this.w_CALFES
      replace t_BAVAL with this.w_BAVAL
      replace t_FLASMO with this.w_FLASMO
      replace t_AGECLF with this.w_AGECLF
      replace t_PNCODAGE with this.w_PNCODAGE
      replace t_PNFLVABD with this.w_PNFLVABD
      replace t_SLAINI with this.w_SLAINI
      replace t_SLDPRO with this.w_SLDPRO
      replace t_SLDPER with this.w_SLDPER
      replace t_SLDFIN with this.w_SLDFIN
      replace t_SLAPRO with this.w_SLAPRO
      replace t_SLAPER with this.w_SLAPER
      replace t_SLAFIN with this.w_SLAFIN
      replace t_SLDINI with this.w_SLDINI
      replace t_PCODCON with this.w_PCODCON
      replace t_DESCRI with this.w_DESCRI
      replace t_IMPFOR with this.w_IMPFOR
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_121.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_122.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_123.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_124.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_125.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_126.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_127.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_131.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_138.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_151.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_61.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_155.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_65.Calculate(IIF((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN)>=0, AH_MSGFORMAT('Saldo DARE:'), IIF((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN)<0, AH_MSGFORMAT('Saldo AVERE:'), '')))
        .oPgFrm.Page1.oPag.oObj_1_165.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_170.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_185.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_224.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_241.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_61.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_65.Calculate(IIF((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN)>=0, AH_MSGFORMAT('Saldo DARE:'), IIF((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN)<0, AH_MSGFORMAT('Saldo AVERE:'), '')))
    endwith
  return
  proc Calculate_QYQODPLTRV()
    with this
          * --- Riassegno alfa documento\protocollo caso numerazione libera
          .op_PNALFDOC = iif(.w_FLPDOC $ 'N-L' ,.w_PNALFDOC,.op_PNALFDOC)
          .op_PNALFPRO = iif(.w_FLPPRO $ 'N-L' ,.w_PNALFPRO,.op_PNALFPRO)
    endwith
  endproc
  proc Calculate_JRYONOWEQW()
    with this
          * --- Flchknumd a true
          .w_FLCHKNUMD = .T.
    endwith
  endproc
  proc Calculate_YSGUZFFFDT()
    with this
          * --- Flchknumd a false
          .w_FLCHKNUMD = .F.
    endwith
  endproc
  proc Calculate_RQVJJSUQFW()
    with this
          * --- Flchkpro a true
          .w_FLCHKPRO = .T.
    endwith
  endproc
  proc Calculate_XXVTPWLQCO()
    with this
          * --- Flchkpro a false
          .w_FLCHKPRO = .F.
    endwith
  endproc
  proc Calculate_JLWOLQTQAO()
    with this
          * --- inizializza w_OBTEST del figlio
          .gscg_miv.w_obtest = .w_obtest
    endwith
  endproc
  proc Calculate_CZGHMRBITS()
    with this
          * --- Cancellazione allegati collegati
          gsma_bae(this;
              ,'I';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPNNUMRER_1_10.enabled = this.oPgFrm.Page1.oPag.oPNNUMRER_1_10.mCond()
    this.oPgFrm.Page1.oPag.oPNDATREG_1_14.enabled = this.oPgFrm.Page1.oPag.oPNDATREG_1_14.mCond()
    this.oPgFrm.Page1.oPag.oPNCODCAU_1_16.enabled = this.oPgFrm.Page1.oPag.oPNCODCAU_1_16.mCond()
    this.oPgFrm.Page1.oPag.oPNCOMPET_1_17.enabled = this.oPgFrm.Page1.oPag.oPNCOMPET_1_17.mCond()
    this.oPgFrm.Page1.oPag.oPNALFDOC_1_46.enabled = this.oPgFrm.Page1.oPag.oPNALFDOC_1_46.mCond()
    this.oPgFrm.Page1.oPag.oPNNUMDOC_1_48.enabled = this.oPgFrm.Page1.oPag.oPNNUMDOC_1_48.mCond()
    this.oPgFrm.Page1.oPag.oPNDATDOC_1_49.enabled = this.oPgFrm.Page1.oPag.oPNDATDOC_1_49.mCond()
    this.oPgFrm.Page1.oPag.oPNDESSUP_1_54.enabled = this.oPgFrm.Page1.oPag.oPNDESSUP_1_54.mCond()
    this.oPgFrm.Page1.oPag.oPNALFPRO_1_57.enabled = this.oPgFrm.Page1.oPag.oPNALFPRO_1_57.mCond()
    this.oPgFrm.Page1.oPag.oPNNUMPRO_1_58.enabled = this.oPgFrm.Page1.oPag.oPNNUMPRO_1_58.mCond()
    this.oPgFrm.Page1.oPag.oPNCODCLF_1_59.enabled = this.oPgFrm.Page1.oPag.oPNCODCLF_1_59.mCond()
    this.oPgFrm.Page1.oPag.oPNCOMIVA_1_60.enabled = this.oPgFrm.Page1.oPag.oPNCOMIVA_1_60.mCond()
    this.oPgFrm.Page1.oPag.oPNDATPLA_1_61.enabled = this.oPgFrm.Page1.oPag.oPNDATPLA_1_61.mCond()
    this.oPgFrm.Page1.oPag.oPNCODVAL_1_62.enabled = this.oPgFrm.Page1.oPag.oPNCODVAL_1_62.mCond()
    this.oPgFrm.Page1.oPag.oPNCAOVAL_1_66.enabled = this.oPgFrm.Page1.oPag.oPNCAOVAL_1_66.mCond()
    this.oPgFrm.Page1.oPag.oPNTOTDOC_1_76.enabled = this.oPgFrm.Page1.oPag.oPNTOTDOC_1_76.mCond()
    this.oPgFrm.Page1.oPag.oPNFLPROV_1_107.enabled = this.oPgFrm.Page1.oPag.oPNFLPROV_1_107.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_90.enabled = this.oPgFrm.Page1.oPag.oBtn_1_90.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_172.enabled = this.oPgFrm.Page1.oPag.oBtn_1_172.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_190.enabled = this.oPgFrm.Page1.oPag.oBtn_1_190.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_191.enabled = this.oPgFrm.Page1.oPag.oBtn_1_191.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_192.enabled = this.oPgFrm.Page1.oPag.oBtn_1_192.mCond()
    this.GSCG_MIV.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_95.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCPROWORD_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCPROWORD_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPNCODCON_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPNCODCON_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPNIMPDAR_2_27.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPNIMPDAR_2_27.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPNIMPAVE_2_28.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPNIMPAVE_2_28.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPNFLPART_2_39.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPNFLPART_2_39.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCDCSAL_2_42.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCDCSAL_2_42.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_44.enabled =this.oPgFrm.Page1.oPag.oBtn_2_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_51.enabled =this.oPgFrm.Page1.oPag.oBtn_2_51.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_2_47.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_47.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSCG_MCA.visible")=='L' And this.GSCG_MCA.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_47.enabled
      this.GSCG_MCA.HideChildrenChain()
    endif 
    this.oPgFrm.Page1.oPag.oLinkPC_2_49.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_49.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSCG_MPA.visible")=='L' And this.GSCG_MPA.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_49.enabled
      this.GSCG_MPA.HideChildrenChain()
    endif 
    this.oPgFrm.Page1.oPag.oLinkPC_2_59.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_59.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSCG_MMC.visible")=='L' And this.GSCG_MMC.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_59.enabled
      this.GSCG_MMC.HideChildrenChain()
    endif 
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_47.visible=!this.oPgFrm.Page1.oPag.oBtn_1_47.mHide()
    this.oPgFrm.Page1.oPag.oPN__MESE_1_55.visible=!this.oPgFrm.Page1.oPag.oPN__MESE_1_55.mHide()
    this.oPgFrm.Page1.oPag.oPN__ANNO_1_56.visible=!this.oPgFrm.Page1.oPag.oPN__ANNO_1_56.mHide()
    this.oPgFrm.Page1.oPag.oPNALFPRO_1_57.visible=!this.oPgFrm.Page1.oPag.oPNALFPRO_1_57.mHide()
    this.oPgFrm.Page1.oPag.oPNNUMPRO_1_58.visible=!this.oPgFrm.Page1.oPag.oPNNUMPRO_1_58.mHide()
    this.oPgFrm.Page1.oPag.oPNCODCLF_1_59.visible=!this.oPgFrm.Page1.oPag.oPNCODCLF_1_59.mHide()
    this.oPgFrm.Page1.oPag.oPNCOMIVA_1_60.visible=!this.oPgFrm.Page1.oPag.oPNCOMIVA_1_60.mHide()
    this.oPgFrm.Page1.oPag.oPNDATPLA_1_61.visible=!this.oPgFrm.Page1.oPag.oPNDATPLA_1_61.mHide()
    this.oPgFrm.Page1.oPag.oDESCLF_1_68.visible=!this.oPgFrm.Page1.oPag.oDESCLF_1_68.mHide()
    this.oPgFrm.Page1.oPag.oDESPAG_1_75.visible=!this.oPgFrm.Page1.oPag.oDESPAG_1_75.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_94.visible=!this.oPgFrm.Page1.oPag.oStr_1_94.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_97.visible=!this.oPgFrm.Page1.oPag.oStr_1_97.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_98.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_98.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_102.visible=!this.oPgFrm.Page1.oPag.oStr_1_102.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_115.visible=!this.oPgFrm.Page1.oPag.oStr_1_115.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_116.visible=!this.oPgFrm.Page1.oPag.oStr_1_116.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_117.visible=!this.oPgFrm.Page1.oPag.oStr_1_117.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_146.visible=!this.oPgFrm.Page1.oPag.oStr_1_146.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_167.visible=!this.oPgFrm.Page1.oPag.oBtn_1_167.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_172.visible=!this.oPgFrm.Page1.oPag.oBtn_1_172.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_179.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_179.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_186.visible=!this.oPgFrm.Page1.oPag.oStr_1_186.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_190.visible=!this.oPgFrm.Page1.oPag.oBtn_1_190.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_191.visible=!this.oPgFrm.Page1.oPag.oBtn_1_191.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_192.visible=!this.oPgFrm.Page1.oPag.oBtn_1_192.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_194.visible=!this.oPgFrm.Page1.oPag.oBtn_1_194.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_207.visible=!this.oPgFrm.Page1.oPag.oStr_1_207.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_212.visible=!this.oPgFrm.Page1.oPag.oBtn_1_212.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_215.visible=!this.oPgFrm.Page1.oPag.oStr_1_215.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_216.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_216.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_217.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_217.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_218.visible=!this.oPgFrm.Page1.oPag.oBtn_1_218.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oLinkPC_2_47.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_47.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.GSCG_MCA.visible")=='L' And this.GSCG_MCA.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_47.visible
      this.GSCG_MCA.HideChildrenChain()
    endif 
    this.oPgFrm.Page1.oPag.oLinkPC_2_49.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_49.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.GSCG_MPA.visible")=='L' And this.GSCG_MPA.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_49.visible
      this.GSCG_MPA.HideChildrenChain()
    endif 
    this.oPgFrm.Page1.oPag.oLinkPC_2_59.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_59.mHide()
    *--- Nascondo il figlio se non visibile
    if Type("this.GSCG_MMC.visible")=='L' And this.GSCG_MMC.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_59.visible
      this.GSCG_MMC.HideChildrenChain()
    endif 
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gscg_mpn
    IF cevent ='w_PNCODCON Changed'
     IF this.o_FLMOVC='S' AND g_BANC='S' AND  this.o_TIPSOT='B' and this.cfunction='Edit'
     this.w_PNCODCON =this.o_PNCODCON 
     this.w_TIPSOT=this.o_TIPSOT 
     this.w_FLMOVC=this.o_FLMOVC
     ah_ErrorMsg("Non � possibile modificare righe che movimentano i conti correnti. Bisoogna cancellare la riga (F6) e ricrearne una nuova",,'')
     ENDIF
    ENDIF
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_121.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_122.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_123.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_124.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_125.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_126.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_127.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_131.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_138.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_151.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_61.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_155.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_65.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_165.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_170.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_185.Event(cEvent)
        if lower(cEvent)==lower("New record")
          .Calculate_JRYONOWEQW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_YSGUZFFFDT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("New record")
          .Calculate_RQVJJSUQFW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_XXVTPWLQCO()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_224.Event(cEvent)
        if lower(cEvent)==lower("Delete start")
          .Calculate_CZGHMRBITS()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_241.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLVEBD,AZDETCON,AZDATTRA,AZDATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZFLVEBD,AZDETCON,AZDATTRA,AZDATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_FLVEBD = NVL(_Link_.AZFLVEBD,space(1))
      this.w_DETCON = NVL(_Link_.AZDETCON,space(1))
      this.w_DATINITRA = NVL(cp_ToDate(_Link_.AZDATTRA),ctod("  /  /  "))
      this.w_AZDATDOC = NVL(_Link_.AZDATDOC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_FLVEBD = space(1)
      this.w_DETCON = space(1)
      this.w_DATINITRA = ctod("  /  /  ")
      this.w_AZDATDOC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PNCODCAU
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_PNCODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select *";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_PNCODCAU))
          select *;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PNCODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_PNCODCAU)+"%");

            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_PNCODCAU)+"%");

            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PNCODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oPNCODCAU_1_16'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_PNCODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_PNCODCAU)
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLPART = NVL(_Link_.CCFLPART,space(1))
      this.w_FLSALI = NVL(_Link_.CCFLSALI,space(1))
      this.w_FLSALF = NVL(_Link_.CCFLSALF,space(1))
      this.w_PNFLIVDF = NVL(_Link_.CCFLIVDF,space(1))
      this.w_FLCOMP = NVL(_Link_.CCFLCOMP,space(1))
      this.w_PNTIPDOC = NVL(_Link_.CCTIPDOC,space(2))
      this.w_PNTIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_PNNUMREG = NVL(_Link_.CCNUMREG,0)
      this.w_FLNDOC = NVL(_Link_.CCNUMDOC,space(1))
      this.w_PNALFPRO = NVL(_Link_.CCSERPRO,space(10))
      this.w_CFDAVE = NVL(_Link_.CCCFDAVE,space(1))
      this.w_CALDOC = NVL(_Link_.CCCALDOC,space(1))
      this.w_TESDOC = NVL(_Link_.CCTESDOC,space(1))
      this.w_FLANAL = NVL(_Link_.CCFLANAL,space(1))
      this.w_FLRIFE = NVL(_Link_.CCFLRIFE,space(1))
      this.w_CCOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_PNALFDOC = NVL(_Link_.CCSERDOC,space(10))
      this.w_FLPDOC = NVL(_Link_.CCFLPDOC,space(1))
      this.w_FLPPRO = NVL(_Link_.CCFLPPRO,space(1))
      this.w_FLPDIF = NVL(_Link_.CCFLPDIF,space(1))
      this.w_FLMOVC = NVL(_Link_.CCFLMOVC,space(1))
      this.w_CONIVA = NVL(_Link_.CCCONIVA,space(15))
      this.w_MOVCES = NVL(_Link_.CCMOVCES,space(1))
      this.w_CAUCES = NVL(_Link_.CCCAUCES,space(5))
      this.w_GESRIT = NVL(_Link_.CCGESRIT,space(1))
      this.w_PAGINS = NVL(_Link_.CCPAGINS,space(5))
      this.w_FLINSO = NVL(_Link_.CCFLINSO,space(1))
      this.w_CCDESSUP = NVL(_Link_.CCDESSUP,space(254))
      this.w_CCDESRIG = NVL(_Link_.CCDESRIG,space(254))
      this.w_CCCONIVA = NVL(_Link_.CCCONIVA,space(15))
      this.w_REGMARG = NVL(_Link_.CCREGMAR,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PNCODCAU = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_FLPART = space(1)
      this.w_FLSALI = space(1)
      this.w_FLSALF = space(1)
      this.w_PNFLIVDF = space(1)
      this.w_FLCOMP = space(1)
      this.w_PNTIPDOC = space(2)
      this.w_PNTIPREG = space(1)
      this.w_PNNUMREG = 0
      this.w_FLNDOC = space(1)
      this.w_PNALFPRO = space(10)
      this.w_CFDAVE = space(1)
      this.w_CALDOC = space(1)
      this.w_TESDOC = space(1)
      this.w_FLANAL = space(1)
      this.w_FLRIFE = space(1)
      this.w_CCOBSO = ctod("  /  /  ")
      this.w_PNALFDOC = space(10)
      this.w_FLPDOC = space(1)
      this.w_FLPPRO = space(1)
      this.w_FLPDIF = space(1)
      this.w_FLMOVC = space(1)
      this.w_CONIVA = space(15)
      this.w_MOVCES = space(1)
      this.w_CAUCES = space(5)
      this.w_GESRIT = space(1)
      this.w_PAGINS = space(5)
      this.w_FLINSO = space(1)
      this.w_CCDESSUP = space(254)
      this.w_CCDESRIG = space(254)
      this.w_CCCONIVA = space(15)
      this.w_REGMARG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CCOBSO>.w_PNDATREG OR EMPTY(.w_CCOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_PNCODCAU = space(5)
        this.w_DESCAU = space(35)
        this.w_FLPART = space(1)
        this.w_FLSALI = space(1)
        this.w_FLSALF = space(1)
        this.w_PNFLIVDF = space(1)
        this.w_FLCOMP = space(1)
        this.w_PNTIPDOC = space(2)
        this.w_PNTIPREG = space(1)
        this.w_PNNUMREG = 0
        this.w_FLNDOC = space(1)
        this.w_PNALFPRO = space(10)
        this.w_CFDAVE = space(1)
        this.w_CALDOC = space(1)
        this.w_TESDOC = space(1)
        this.w_FLANAL = space(1)
        this.w_FLRIFE = space(1)
        this.w_CCOBSO = ctod("  /  /  ")
        this.w_PNALFDOC = space(10)
        this.w_FLPDOC = space(1)
        this.w_FLPPRO = space(1)
        this.w_FLPDIF = space(1)
        this.w_FLMOVC = space(1)
        this.w_CONIVA = space(15)
        this.w_MOVCES = space(1)
        this.w_CAUCES = space(5)
        this.w_GESRIT = space(1)
        this.w_PAGINS = space(5)
        this.w_FLINSO = space(1)
        this.w_CCDESSUP = space(254)
        this.w_CCDESRIG = space(254)
        this.w_CCCONIVA = space(15)
        this.w_REGMARG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 33 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+33<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.CCCODICE as CCCODICE116"+ ",link_1_16.CCDESCRI as CCDESCRI116"+ ",link_1_16.CCFLPART as CCFLPART116"+ ",link_1_16.CCFLSALI as CCFLSALI116"+ ",link_1_16.CCFLSALF as CCFLSALF116"+ ",link_1_16.CCFLIVDF as CCFLIVDF116"+ ",link_1_16.CCFLCOMP as CCFLCOMP116"+ ",link_1_16.CCTIPDOC as CCTIPDOC116"+ ",link_1_16.CCTIPREG as CCTIPREG116"+ ",link_1_16.CCNUMREG as CCNUMREG116"+ ",link_1_16.CCNUMDOC as CCNUMDOC116"+ ",link_1_16.CCSERPRO as CCSERPRO116"+ ",link_1_16.CCCFDAVE as CCCFDAVE116"+ ",link_1_16.CCCALDOC as CCCALDOC116"+ ",link_1_16.CCTESDOC as CCTESDOC116"+ ",link_1_16.CCFLANAL as CCFLANAL116"+ ",link_1_16.CCFLRIFE as CCFLRIFE116"+ ",link_1_16.CCDTOBSO as CCDTOBSO116"+ ",link_1_16.CCSERDOC as CCSERDOC116"+ ",link_1_16.CCFLPDOC as CCFLPDOC116"+ ",link_1_16.CCFLPPRO as CCFLPPRO116"+ ",link_1_16.CCFLPDIF as CCFLPDIF116"+ ",link_1_16.CCFLMOVC as CCFLMOVC116"+ ",link_1_16.CCCONIVA as CCCONIVA116"+ ",link_1_16.CCMOVCES as CCMOVCES116"+ ",link_1_16.CCCAUCES as CCCAUCES116"+ ",link_1_16.CCGESRIT as CCGESRIT116"+ ",link_1_16.CCPAGINS as CCPAGINS116"+ ",link_1_16.CCFLINSO as CCFLINSO116"+ ",link_1_16.CCDESSUP as CCDESSUP116"+ ",link_1_16.CCDESRIG as CCDESRIG116"+ ",link_1_16.CCCONIVA as CCCONIVA116"+ ",link_1_16.CCREGMAR as CCREGMAR116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on PNT_MAST.PNCODCAU=link_1_16.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+33
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and PNT_MAST.PNCODCAU=link_1_16.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+33
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PNCOMPET
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCOMPET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_PNCOMPET)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_PNCOMPET))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PNCOMPET)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PNCOMPET) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oPNCOMPET_1_17'),i_cWhere,'',"Elenco esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCOMPET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_PNCOMPET);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_PNCOMPET)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCOMPET = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(_Link_.ESINIESE,space(8))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_PNVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PNCOMPET = space(4)
      endif
      this.w_INIESE = space(8)
      this.w_FINESE = ctod("  /  /  ")
      this.w_PNVALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCOMPET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PNVALNAZ
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNVALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNVALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PNVALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PNVALNAZ)
            select VACODVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNVALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_CAONAZ = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOP = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PNVALNAZ = space(3)
      endif
      this.w_CAONAZ = 0
      this.w_DECTOP = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNVALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.VACODVAL as VACODVAL119"+ ",link_1_19.VACAOVAL as VACAOVAL119"+ ",link_1_19.VADECTOT as VADECTOT119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on PNT_MAST.PNVALNAZ=link_1_19.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and PNT_MAST.PNVALNAZ=link_1_19.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PNCODCLF
  func Link_1_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PNCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PNTIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANDTOBSO,ANCODPAG,ANCODVAL,ANPAGPAR,ANDATMOR,ANFLACBD,ANFLAACC,ANRITENU,ANFLRITE,ANFLSOAL,ANOPETRE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_PNTIPCLF;
                     ,'ANCODICE',trim(this.w_PNCODCLF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANDTOBSO,ANCODPAG,ANCODVAL,ANPAGPAR,ANDATMOR,ANFLACBD,ANFLAACC,ANRITENU,ANFLRITE,ANFLSOAL,ANOPETRE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PNCODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PNCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PNTIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANDTOBSO,ANCODPAG,ANCODVAL,ANPAGPAR,ANDATMOR,ANFLACBD,ANFLAACC,ANRITENU,ANFLRITE,ANFLSOAL,ANOPETRE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PNCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_PNTIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANDTOBSO,ANCODPAG,ANCODVAL,ANPAGPAR,ANDATMOR,ANFLACBD,ANFLAACC,ANRITENU,ANFLRITE,ANFLSOAL,ANOPETRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANPARIVA like "+cp_ToStrODBC(trim(this.w_PNCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PNTIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANDTOBSO,ANCODPAG,ANCODVAL,ANPAGPAR,ANDATMOR,ANFLACBD,ANFLAACC,ANRITENU,ANFLRITE,ANFLSOAL,ANOPETRE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANPARIVA like "+cp_ToStr(trim(this.w_PNCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_PNTIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANDTOBSO,ANCODPAG,ANCODVAL,ANPAGPAR,ANDATMOR,ANFLACBD,ANFLAACC,ANRITENU,ANFLRITE,ANFLSOAL,ANOPETRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PNCODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPNCODCLF_1_59'),i_cWhere,'GSAR_BZC',"Anagrafica clienti/fornitori",'GSCG1MPN.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PNTIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANDTOBSO,ANCODPAG,ANCODVAL,ANPAGPAR,ANDATMOR,ANFLACBD,ANFLAACC,ANRITENU,ANFLRITE,ANFLSOAL,ANOPETRE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANDTOBSO,ANCODPAG,ANCODVAL,ANPAGPAR,ANDATMOR,ANFLACBD,ANFLAACC,ANRITENU,ANFLRITE,ANFLSOAL,ANOPETRE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANDTOBSO,ANCODPAG,ANCODVAL,ANPAGPAR,ANDATMOR,ANFLACBD,ANFLAACC,ANRITENU,ANFLRITE,ANFLSOAL,ANOPETRE";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_PNTIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANDTOBSO,ANCODPAG,ANCODVAL,ANPAGPAR,ANDATMOR,ANFLACBD,ANFLAACC,ANRITENU,ANFLRITE,ANFLSOAL,ANOPETRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANDTOBSO,ANCODPAG,ANCODVAL,ANPAGPAR,ANDATMOR,ANFLACBD,ANFLAACC,ANRITENU,ANFLRITE,ANFLSOAL,ANOPETRE";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PNCODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PNTIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PNTIPCLF;
                       ,'ANCODICE',this.w_PNCODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARIVA,ANDTOBSO,ANCODPAG,ANCODVAL,ANPAGPAR,ANDATMOR,ANFLACBD,ANFLAACC,ANRITENU,ANFLRITE,ANFLSOAL,ANOPETRE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(36))
      this.w_PARIVA = NVL(_Link_.ANPARIVA,space(12))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PAGCLF = NVL(_Link_.ANCODPAG,space(5))
      this.w_CLFVAL = NVL(_Link_.ANCODVAL,space(3))
      this.w_PAGMOR = NVL(_Link_.ANPAGPAR,space(5))
      this.w_DATMOR = NVL(cp_ToDate(_Link_.ANDATMOR),ctod("  /  /  "))
      this.w_FLACBD = NVL(_Link_.ANFLACBD,space(1))
      this.w_FLAACC = NVL(_Link_.ANFLAACC,space(1))
      this.w_RITENU = NVL(_Link_.ANRITENU,space(1))
      this.w_FLGRIT = NVL(_Link_.ANFLRITE,space(1))
      this.w_ANFLSOAL = NVL(_Link_.ANFLSOAL,space(1))
      this.w_ANOPETRE = NVL(_Link_.ANOPETRE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PNCODCLF = space(15)
      endif
      this.w_DESCLF = space(36)
      this.w_PARIVA = space(12)
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_PAGCLF = space(5)
      this.w_CLFVAL = space(3)
      this.w_PAGMOR = space(5)
      this.w_DATMOR = ctod("  /  /  ")
      this.w_FLACBD = space(1)
      this.w_FLAACC = space(1)
      this.w_RITENU = space(1)
      this.w_FLGRIT = space(1)
      this.w_ANFLSOAL = space(1)
      this.w_ANOPETRE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSO>.w_PNDATREG OR EMPTY(.w_DTOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto")
        endif
        this.w_PNCODCLF = space(15)
        this.w_DESCLF = space(36)
        this.w_PARIVA = space(12)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_PAGCLF = space(5)
        this.w_CLFVAL = space(3)
        this.w_PAGMOR = space(5)
        this.w_DATMOR = ctod("  /  /  ")
        this.w_FLACBD = space(1)
        this.w_FLAACC = space(1)
        this.w_RITENU = space(1)
        this.w_FLGRIT = space(1)
        this.w_ANFLSOAL = space(1)
        this.w_ANOPETRE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_59(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 14 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+14<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_59.ANCODICE as ANCODICE159"+ ",link_1_59.ANDESCRI as ANDESCRI159"+ ",link_1_59.ANPARIVA as ANPARIVA159"+ ",link_1_59.ANDTOBSO as ANDTOBSO159"+ ",link_1_59.ANCODPAG as ANCODPAG159"+ ",link_1_59.ANCODVAL as ANCODVAL159"+ ",link_1_59.ANPAGPAR as ANPAGPAR159"+ ",link_1_59.ANDATMOR as ANDATMOR159"+ ",link_1_59.ANFLACBD as ANFLACBD159"+ ",link_1_59.ANFLAACC as ANFLAACC159"+ ",link_1_59.ANRITENU as ANRITENU159"+ ",link_1_59.ANFLRITE as ANFLRITE159"+ ",link_1_59.ANFLSOAL as ANFLSOAL159"+ ",link_1_59.ANOPETRE as ANOPETRE159"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_59 on PNT_MAST.PNCODCLF=link_1_59.ANCODICE"+" and PNT_MAST.PNTIPCLF=link_1_59.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_59"
          i_cKey=i_cKey+'+" and PNT_MAST.PNCODCLF=link_1_59.ANCODICE(+)"'+'+" and PNT_MAST.PNTIPCLF=link_1_59.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+14
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PNCODVAL
  func Link_1_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_PNCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VACAOVAL,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_PNCODVAL))
          select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VACAOVAL,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PNCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_PNCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VACAOVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_PNCODVAL)+"%");

            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VACAOVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PNCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oPNCODVAL_1_62'),i_cWhere,'GSAR_AVL',"Elenco valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VACAOVAL,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VACAOVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VACAOVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PNCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PNCODVAL)
            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VACAOVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESAPP = NVL(_Link_.VADESVAL,space(35))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_DTVOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PNCODVAL = space(3)
      endif
      this.w_DESAPP = space(35)
      this.w_SIMVAL = space(5)
      this.w_DECTOT = 0
      this.w_CAOVAL = 0
      this.w_DTVOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DTVOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o obsoleta o valuta esercizio non definita")
        endif
        this.w_PNCODVAL = space(3)
        this.w_DESAPP = space(35)
        this.w_SIMVAL = space(5)
        this.w_DECTOT = 0
        this.w_CAOVAL = 0
        this.w_DTVOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_62(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_62.VACODVAL as VACODVAL162"+ ",link_1_62.VADESVAL as VADESVAL162"+ ",link_1_62.VASIMVAL as VASIMVAL162"+ ",link_1_62.VADECTOT as VADECTOT162"+ ",link_1_62.VACAOVAL as VACAOVAL162"+ ",link_1_62.VADTOBSO as VADTOBSO162"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_62 on PNT_MAST.PNCODVAL=link_1_62.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_62"
          i_cKey=i_cKey+'+" and PNT_MAST.PNCODVAL=link_1_62.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODPAG
  func Link_1_73(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_CODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_CODPAG)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PNCAURIG
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCAURIG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCAURIG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPART";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_PNCAURIG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_PNCAURIG)
            select CCCODICE,CCDESCRI,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCAURIG = NVL(_Link_.CCCODICE,space(5))
      this.w_CAUDES = NVL(_Link_.CCDESCRI,space(35))
      this.w_RIGPAR = NVL(_Link_.CCFLPART,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PNCAURIG = space(5)
      endif
      this.w_CAUDES = space(35)
      this.w_RIGPAR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCAURIG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.CCCODICE as CCCODICE204"+ ",link_2_4.CCDESCRI as CCDESCRI204"+ ",link_2_4.CCFLPART as CCFLPART204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on PNT_DETT.PNCAURIG=link_2_4.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and PNT_DETT.PNCAURIG=link_2_4.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PNCODCON
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PNCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PNTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDESCR2,ANCODPAG,ANPARTSN,AN1MESCL,AN2MESCL,ANGIOSC1,ANCONSUP,ANDTOBSO,ANGIOSC2,ANCCTAGG,ANCODBAN,ANTIPSOT,ANGIOFIS,ANCODBA2,ANPAGPAR,ANDATMOR,ANCODAG1";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_PNTIPCON;
                     ,'ANCODICE',trim(this.w_PNCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDESCR2,ANCODPAG,ANPARTSN,AN1MESCL,AN2MESCL,ANGIOSC1,ANCONSUP,ANDTOBSO,ANGIOSC2,ANCCTAGG,ANCODBAN,ANTIPSOT,ANGIOFIS,ANCODBA2,ANPAGPAR,ANDATMOR,ANCODAG1;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PNCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PNCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PNTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDESCR2,ANCODPAG,ANPARTSN,AN1MESCL,AN2MESCL,ANGIOSC1,ANCONSUP,ANDTOBSO,ANGIOSC2,ANCCTAGG,ANCODBAN,ANTIPSOT,ANGIOFIS,ANCODBA2,ANPAGPAR,ANDATMOR,ANCODAG1";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PNCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_PNTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDESCR2,ANCODPAG,ANPARTSN,AN1MESCL,AN2MESCL,ANGIOSC1,ANCONSUP,ANDTOBSO,ANGIOSC2,ANCCTAGG,ANCODBAN,ANTIPSOT,ANGIOFIS,ANCODBA2,ANPAGPAR,ANDATMOR,ANCODAG1;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PNCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPNCODCON_2_7'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori/conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PNTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDESCR2,ANCODPAG,ANPARTSN,AN1MESCL,AN2MESCL,ANGIOSC1,ANCONSUP,ANDTOBSO,ANGIOSC2,ANCCTAGG,ANCODBAN,ANTIPSOT,ANGIOFIS,ANCODBA2,ANPAGPAR,ANDATMOR,ANCODAG1";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDESCR2,ANCODPAG,ANPARTSN,AN1MESCL,AN2MESCL,ANGIOSC1,ANCONSUP,ANDTOBSO,ANGIOSC2,ANCCTAGG,ANCODBAN,ANTIPSOT,ANGIOFIS,ANCODBA2,ANPAGPAR,ANDATMOR,ANCODAG1;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDESCR2,ANCODPAG,ANPARTSN,AN1MESCL,AN2MESCL,ANGIOSC1,ANCONSUP,ANDTOBSO,ANGIOSC2,ANCCTAGG,ANCODBAN,ANTIPSOT,ANGIOFIS,ANCODBA2,ANPAGPAR,ANDATMOR,ANCODAG1";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_PNTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDESCR2,ANCODPAG,ANPARTSN,AN1MESCL,AN2MESCL,ANGIOSC1,ANCONSUP,ANDTOBSO,ANGIOSC2,ANCCTAGG,ANCODBAN,ANTIPSOT,ANGIOFIS,ANCODBA2,ANPAGPAR,ANDATMOR,ANCODAG1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDESCR2,ANCODPAG,ANPARTSN,AN1MESCL,AN2MESCL,ANGIOSC1,ANCONSUP,ANDTOBSO,ANGIOSC2,ANCCTAGG,ANCODBAN,ANTIPSOT,ANGIOFIS,ANCODBA2,ANPAGPAR,ANDATMOR,ANCODAG1";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PNCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PNTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PNTIPCON;
                       ,'ANCODICE',this.w_PNCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDESCR2,ANCODPAG,ANPARTSN,AN1MESCL,AN2MESCL,ANGIOSC1,ANCONSUP,ANDTOBSO,ANGIOSC2,ANCCTAGG,ANCODBAN,ANTIPSOT,ANGIOFIS,ANCODBA2,ANPAGPAR,ANDATMOR,ANCODAG1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(30))
      this.w_DESCO2 = NVL(_Link_.ANDESCR2,space(30))
      this.w_PAGCLF1 = NVL(_Link_.ANCODPAG,space(5))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
      this.w_CLMES1 = NVL(_Link_.AN1MESCL,0)
      this.w_CLMES2 = NVL(_Link_.AN2MESCL,0)
      this.w_CLGIO1 = NVL(_Link_.ANGIOSC1,0)
      this.w_MASTRO = NVL(_Link_.ANCONSUP,space(15))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_CLGIO2 = NVL(_Link_.ANGIOSC2,0)
      this.w_CCTAGG = NVL(_Link_.ANCCTAGG,space(1))
      this.w_BANAPP = NVL(_Link_.ANCODBAN,space(10))
      this.w_TIPSOT = NVL(_Link_.ANTIPSOT,space(1))
      this.w_GIOFIS = NVL(_Link_.ANGIOFIS,0)
      this.w_BANNOS = NVL(_Link_.ANCODBA2,space(15))
      this.w_PAGMOR1 = NVL(_Link_.ANPAGPAR,space(5))
      this.w_DATMOR1 = NVL(cp_ToDate(_Link_.ANDATMOR),ctod("  /  /  "))
      this.w_TESTPART = NVL(_Link_.ANPARTSN,space(1))
      this.w_AGECLF = NVL(_Link_.ANCODAG1,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PNCODCON = space(15)
      endif
      this.w_DESCON = space(30)
      this.w_DESCO2 = space(30)
      this.w_PAGCLF1 = space(5)
      this.w_PARTSN = space(1)
      this.w_CLMES1 = 0
      this.w_CLMES2 = 0
      this.w_CLGIO1 = 0
      this.w_MASTRO = space(15)
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_CLGIO2 = 0
      this.w_CCTAGG = space(1)
      this.w_BANAPP = space(10)
      this.w_TIPSOT = space(1)
      this.w_GIOFIS = 0
      this.w_BANNOS = space(15)
      this.w_PAGMOR1 = space(5)
      this.w_DATMOR1 = ctod("  /  /  ")
      this.w_TESTPART = space(1)
      this.w_AGECLF = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DTOBSO>.w_PNDATREG OR EMPTY(.w_DTOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente o obsoleto")
        endif
        this.w_PNCODCON = space(15)
        this.w_DESCON = space(30)
        this.w_DESCO2 = space(30)
        this.w_PAGCLF1 = space(5)
        this.w_PARTSN = space(1)
        this.w_CLMES1 = 0
        this.w_CLMES2 = 0
        this.w_CLGIO1 = 0
        this.w_MASTRO = space(15)
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_CLGIO2 = 0
        this.w_CCTAGG = space(1)
        this.w_BANAPP = space(10)
        this.w_TIPSOT = space(1)
        this.w_GIOFIS = 0
        this.w_BANNOS = space(15)
        this.w_PAGMOR1 = space(5)
        this.w_DATMOR1 = ctod("  /  /  ")
        this.w_TESTPART = space(1)
        this.w_AGECLF = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 20 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+20<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.ANCODICE as ANCODICE207"+ ",link_2_7.ANDESCRI as ANDESCRI207"+ ",link_2_7.ANDESCR2 as ANDESCR2207"+ ",link_2_7.ANCODPAG as ANCODPAG207"+ ",link_2_7.ANPARTSN as ANPARTSN207"+ ",link_2_7.AN1MESCL as AN1MESCL207"+ ",link_2_7.AN2MESCL as AN2MESCL207"+ ",link_2_7.ANGIOSC1 as ANGIOSC1207"+ ",link_2_7.ANCONSUP as ANCONSUP207"+ ",link_2_7.ANDTOBSO as ANDTOBSO207"+ ",link_2_7.ANGIOSC2 as ANGIOSC2207"+ ",link_2_7.ANCCTAGG as ANCCTAGG207"+ ",link_2_7.ANCODBAN as ANCODBAN207"+ ",link_2_7.ANTIPSOT as ANTIPSOT207"+ ",link_2_7.ANGIOFIS as ANGIOFIS207"+ ",link_2_7.ANCODBA2 as ANCODBA2207"+ ",link_2_7.ANPAGPAR as ANPAGPAR207"+ ",link_2_7.ANDATMOR as ANDATMOR207"+ ",link_2_7.ANPARTSN as ANPARTSN207"+ ",link_2_7.ANCODAG1 as ANCODAG1207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on PNT_DETT.PNCODCON=link_2_7.ANCODICE"+" and PNT_DETT.PNTIPCON=link_2_7.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+20
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and PNT_DETT.PNCODCON=link_2_7.ANCODICE(+)"'+'+" and PNT_DETT.PNTIPCON=link_2_7.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+20
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PNCODPAG
  func Link_2_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_PNCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_PNCODPAG)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESCRI = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PNCODPAG = space(5)
      endif
      this.w_DESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_24(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_24.PACODICE as PACODICE224"+ ","+cp_TransLinkFldName('link_2_24.PADESCRI')+" as PADESCRI224"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_24 on PNT_DETT.PNCODPAG=link_2_24.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_24"
          i_cKey=i_cKey+'+" and PNT_DETT.PNCODPAG=link_2_24.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PNCODCON
  func Link_2_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SALDICON_IDX,3]
    i_lTable = "SALDICON"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SALDICON_IDX,2], .t., this.SALDICON_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SALDICON_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SLTIPCON,SLCODESE,SLCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SLCODICE="+cp_ToStrODBC(this.w_PNCODCON);
                   +" and SLTIPCON="+cp_ToStrODBC(this.w_PNTIPCON);
                   +" and SLCODESE="+cp_ToStrODBC(this.w_PNCOMPET);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SLTIPCON',this.w_PNTIPCON;
                       ,'SLCODESE',this.w_PNCOMPET;
                       ,'SLCODICE',this.w_PNCODCON)
            select SLTIPCON,SLCODESE,SLCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
    else
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SALDICON_IDX,2])+'\'+cp_ToStr(_Link_.SLTIPCON,1)+'\'+cp_ToStr(_Link_.SLCODESE,1)+'\'+cp_ToStr(_Link_.SLCODICE,1)
      cp_ShowWarn(i_cKey,this.SALDICON_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMCOR
  func Link_2_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CONTI_IDX,3]
    i_lTable = "BAN_CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2], .t., this.BAN_CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                   +" from "+i_cTable+" "+i_lTable+" where CCCONCOR="+cp_ToStrODBC(this.w_NUMCOR);
                   +" and CCTIPCON="+cp_ToStrODBC(this.w_PNTIPCON);
                   +" and CCCODCON="+cp_ToStrODBC(this.w_PNCODCON);
                   +" and CCCODBAN="+cp_ToStrODBC(this.w_BANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPCON',this.w_PNTIPCON;
                       ,'CCCODCON',this.w_PNCODCON;
                       ,'CCCODBAN',this.w_BANAPP;
                       ,'CCCONCOR',this.w_NUMCOR)
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMCOR = NVL(_Link_.CCCONCOR,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_NUMCOR = space(25)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPCON,1)+'\'+cp_ToStr(_Link_.CCCODCON,1)+'\'+cp_ToStr(_Link_.CCCODBAN,1)+'\'+cp_ToStr(_Link_.CCCONCOR,1)
      cp_ShowWarn(i_cKey,this.BAN_CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONBAN
  func Link_2_57(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACALFES,BACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CONBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CONBAN)
            select BACODBAN,BACALFES,BACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONBAN = NVL(_Link_.BACODBAN,space(15))
      this.w_CALFES = NVL(_Link_.BACALFES,space(3))
      this.w_BAVAL = NVL(_Link_.BACODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CONBAN = space(15)
      endif
      this.w_CALFES = space(3)
      this.w_BAVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PNCODAGE
  func Link_2_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PNCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PNCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_PNCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_PNCODAGE)
            select AGCODAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PNCODAGE = NVL(_Link_.AGCODAGE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PNCODAGE = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PNCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PCODCON
  func Link_2_74(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SALDICON_IDX,3]
    i_lTable = "SALDICON"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SALDICON_IDX,2], .t., this.SALDICON_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SALDICON_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SLTIPCON,SLCODESE,SLCODICE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN";
                   +" from "+i_cTable+" "+i_lTable+" where SLCODICE="+cp_ToStrODBC(this.w_PCODCON);
                   +" and SLTIPCON="+cp_ToStrODBC(this.w_PNTIPCON);
                   +" and SLCODESE="+cp_ToStrODBC(this.w_PNCOMPET);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SLTIPCON',this.w_PNTIPCON;
                       ,'SLCODESE',this.w_PNCOMPET;
                       ,'SLCODICE',this.w_PCODCON)
            select SLTIPCON,SLCODESE,SLCODICE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCODCON = NVL(_Link_.SLCODICE,space(10))
      this.w_SLDPRO = NVL(_Link_.SLDARPRO,0)
      this.w_SLAPRO = NVL(_Link_.SLAVEPRO,0)
      this.w_SLDPER = NVL(_Link_.SLDARPER,0)
      this.w_SLAPER = NVL(_Link_.SLAVEPER,0)
      this.w_SLDINI = NVL(_Link_.SLDARINI,0)
      this.w_SLAINI = NVL(_Link_.SLAVEINI,0)
      this.w_SLDFIN = NVL(_Link_.SLDARFIN,0)
      this.w_SLAFIN = NVL(_Link_.SLAVEFIN,0)
    else
      if i_cCtrl<>'Load'
        this.w_PCODCON = space(10)
      endif
      this.w_SLDPRO = 0
      this.w_SLAPRO = 0
      this.w_SLDPER = 0
      this.w_SLAPER = 0
      this.w_SLDINI = 0
      this.w_SLAINI = 0
      this.w_SLDFIN = 0
      this.w_SLAFIN = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SALDICON_IDX,2])+'\'+cp_ToStr(_Link_.SLTIPCON,1)+'\'+cp_ToStr(_Link_.SLCODESE,1)+'\'+cp_ToStr(_Link_.SLCODICE,1)
      cp_ShowWarn(i_cKey,this.SALDICON_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=OSSERIAL
  func Link_1_221(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OPERSUPE_IDX,3]
    i_lTable = "OPERSUPE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2], .t., this.OPERSUPE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_OSSERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_OSSERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OSSERIAL";
                   +" from "+i_cTable+" "+i_lTable+" where OSSERIAL="+cp_ToStrODBC(this.w_OSSERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OSSERIAL',this.w_OSSERIAL)
            select OSSERIAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_OSSERIAL = NVL(_Link_.OSSERIAL,space(10))
      this.w_OSTIPORE = NVL(_Link_.OSSERIAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_OSSERIAL = space(10)
      endif
      this.w_OSTIPORE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OPERSUPE_IDX,2])+'\'+cp_ToStr(_Link_.OSSERIAL,1)
      cp_ShowWarn(i_cKey,this.OPERSUPE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_OSSERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DASERIAL
  func Link_1_232(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DATI_AGG_IDX,3]
    i_lTable = "DATI_AGG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2], .t., this.DATI_AGG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DASERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DASERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DASERIAL,DACAM_01,DACAM_02,DACAM_03,DACAM_04,DACAM_05,DACAM_06";
                   +" from "+i_cTable+" "+i_lTable+" where DASERIAL="+cp_ToStrODBC(this.w_DASERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DASERIAL',this.w_DASERIAL)
            select DASERIAL,DACAM_01,DACAM_02,DACAM_03,DACAM_04,DACAM_05,DACAM_06;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DASERIAL = NVL(_Link_.DASERIAL,space(10))
      this.w_DACAM_01 = NVL(_Link_.DACAM_01,space(30))
      this.w_DACAM_02 = NVL(_Link_.DACAM_02,space(30))
      this.w_DACAM_03 = NVL(_Link_.DACAM_03,space(30))
      this.w_DACAM_04 = NVL(_Link_.DACAM_04,space(30))
      this.w_DACAM_05 = NVL(_Link_.DACAM_05,space(30))
      this.w_DACAM_06 = NVL(_Link_.DACAM_06,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_DASERIAL = space(10)
      endif
      this.w_DACAM_01 = space(30)
      this.w_DACAM_02 = space(30)
      this.w_DACAM_03 = space(30)
      this.w_DACAM_04 = space(30)
      this.w_DACAM_05 = space(30)
      this.w_DACAM_06 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2])+'\'+cp_ToStr(_Link_.DASERIAL,1)
      cp_ShowWarn(i_cKey,this.DATI_AGG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DASERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPNNUMRER_1_10.value==this.w_PNNUMRER)
      this.oPgFrm.Page1.oPag.oPNNUMRER_1_10.value=this.w_PNNUMRER
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCODUTE_1_13.value==this.w_PNCODUTE)
      this.oPgFrm.Page1.oPag.oPNCODUTE_1_13.value=this.w_PNCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oPNDATREG_1_14.value==this.w_PNDATREG)
      this.oPgFrm.Page1.oPag.oPNDATREG_1_14.value=this.w_PNDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCODCAU_1_16.value==this.w_PNCODCAU)
      this.oPgFrm.Page1.oPag.oPNCODCAU_1_16.value=this.w_PNCODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCOMPET_1_17.value==this.w_PNCOMPET)
      this.oPgFrm.Page1.oPag.oPNCOMPET_1_17.value=this.w_PNCOMPET
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_23.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_23.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oPNALFDOC_1_46.value==this.w_PNALFDOC)
      this.oPgFrm.Page1.oPag.oPNALFDOC_1_46.value=this.w_PNALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPNNUMDOC_1_48.value==this.w_PNNUMDOC)
      this.oPgFrm.Page1.oPag.oPNNUMDOC_1_48.value=this.w_PNNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPNDATDOC_1_49.value==this.w_PNDATDOC)
      this.oPgFrm.Page1.oPag.oPNDATDOC_1_49.value=this.w_PNDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPNDESSUP_1_54.value==this.w_PNDESSUP)
      this.oPgFrm.Page1.oPag.oPNDESSUP_1_54.value=this.w_PNDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oPN__MESE_1_55.value==this.w_PN__MESE)
      this.oPgFrm.Page1.oPag.oPN__MESE_1_55.value=this.w_PN__MESE
    endif
    if not(this.oPgFrm.Page1.oPag.oPN__ANNO_1_56.value==this.w_PN__ANNO)
      this.oPgFrm.Page1.oPag.oPN__ANNO_1_56.value=this.w_PN__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oPNALFPRO_1_57.value==this.w_PNALFPRO)
      this.oPgFrm.Page1.oPag.oPNALFPRO_1_57.value=this.w_PNALFPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oPNNUMPRO_1_58.value==this.w_PNNUMPRO)
      this.oPgFrm.Page1.oPag.oPNNUMPRO_1_58.value=this.w_PNNUMPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCODCLF_1_59.value==this.w_PNCODCLF)
      this.oPgFrm.Page1.oPag.oPNCODCLF_1_59.value=this.w_PNCODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCOMIVA_1_60.value==this.w_PNCOMIVA)
      this.oPgFrm.Page1.oPag.oPNCOMIVA_1_60.value=this.w_PNCOMIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oPNDATPLA_1_61.value==this.w_PNDATPLA)
      this.oPgFrm.Page1.oPag.oPNDATPLA_1_61.value=this.w_PNDATPLA
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCODVAL_1_62.value==this.w_PNCODVAL)
      this.oPgFrm.Page1.oPag.oPNCODVAL_1_62.value=this.w_PNCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPNCAOVAL_1_66.value==this.w_PNCAOVAL)
      this.oPgFrm.Page1.oPag.oPNCAOVAL_1_66.value=this.w_PNCAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_67.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_67.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_68.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_68.value=this.w_DESCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAG_1_75.value==this.w_DESPAG)
      this.oPgFrm.Page1.oPag.oDESPAG_1_75.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oPNTOTDOC_1_76.value==this.w_PNTOTDOC)
      this.oPgFrm.Page1.oPag.oPNTOTDOC_1_76.value=this.w_PNTOTDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPNFLPROV_1_107.RadioValue()==this.w_PNFLPROV)
      this.oPgFrm.Page1.oPag.oPNFLPROV_1_107.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMCOR_2_45.value==this.w_NUMCOR)
      this.oPgFrm.Page1.oPag.oNUMCOR_2_45.value=this.w_NUMCOR
      replace t_NUMCOR with this.oPgFrm.Page1.oPag.oNUMCOR_2_45.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDAR_3_1.value==this.w_TOTDAR)
      this.oPgFrm.Page1.oPag.oTOTDAR_3_1.value=this.w_TOTDAR
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTAVE_3_2.value==this.w_TOTAVE)
      this.oPgFrm.Page1.oPag.oTOTAVE_3_2.value=this.w_TOTAVE
    endif
    if not(this.oPgFrm.Page1.oPag.oSBILAN_3_3.value==this.w_SBILAN)
      this.oPgFrm.Page1.oPag.oSBILAN_3_3.value=this.w_SBILAN
    endif
    if not(this.oPgFrm.Page1.oPag.oSALDO_2_75.value==this.w_SALDO)
      this.oPgFrm.Page1.oPag.oSALDO_2_75.value=this.w_SALDO
      replace t_SALDO with this.oPgFrm.Page1.oPag.oSALDO_2_75.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNTIPCON_2_1.value==this.w_PNTIPCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNTIPCON_2_1.value=this.w_PNTIPCON
      replace t_PNTIPCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNTIPCON_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNCODCON_2_7.value==this.w_PNCODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNCODCON_2_7.value=this.w_PNCODCON
      replace t_PNCODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNCODCON_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCON_2_8.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCON_2_8.value=this.w_DESCON
      replace t_DESCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCON_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNIMPDAR_2_27.value==this.w_PNIMPDAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNIMPDAR_2_27.value=this.w_PNIMPDAR
      replace t_PNIMPDAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNIMPDAR_2_27.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNIMPAVE_2_28.value==this.w_PNIMPAVE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNIMPAVE_2_28.value=this.w_PNIMPAVE
      replace t_PNIMPAVE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNIMPAVE_2_28.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNFLPART_2_39.value==this.w_PNFLPART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNFLPART_2_39.value=this.w_PNFLPART
      replace t_PNFLPART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNFLPART_2_39.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCSAL_2_42.value==this.w_CDCSAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCSAL_2_42.value=this.w_CDCSAL
      replace t_CDCSAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCDCSAL_2_42.value
    endif
    cp_SetControlsValueExtFlds(this,'PNT_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PNDATREG))  and (.w_STAMPATO<>'S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPNDATREG_1_14.SetFocus()
            i_bnoObbl = !empty(.w_PNDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PNCODCAU) or not(.w_CCOBSO>.w_PNDATREG OR EMPTY(.w_CCOBSO)))  and (.w_STAMPATO<>'S' AND .cFunction<>'Edit')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPNCODCAU_1_16.SetFocus()
            i_bnoObbl = !empty(.w_PNCODCAU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   (empty(.w_PNCOMPET))  and (.w_FLCOMP='S' AND .w_STAMPATO<>'S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPNCOMPET_1_17.SetFocus()
            i_bnoObbl = !empty(.w_PNCOMPET)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHPNTDOC(.w_PNDATREG, .w_PNDATDOC))  and (.w_STAMPATO<>'S' AND .w_FLPDOC<>'N')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPNDATDOC_1_49.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_PN__MESE>=1 and .w_PN__MESE<=12) OR .w_PN__MESE=0)  and not(Not(.w_PNTIPREG<>'N' AND .w_PNTIPDOC $ 'FA-NC-FC-FE-NE-AU-NU' AND .w_PNTIPCLF $ 'C-F'))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPN__MESE_1_55.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Mese di riferimento: selezionare un mese compreso tra 1 e 12")
          case   not((.w_PN__ANNO>=1901 and .w_PN__ANNO <=2099) OR .w_PN__ANNO=0)  and not(Not(.w_PNTIPREG<>'N' AND .w_PNTIPDOC $ 'FA-NC-FC-FE-NE-AU-NU' AND .w_PNTIPCLF $ 'C-F'))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPN__ANNO_1_56.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Anno solare: selezionare un anno compreso tra 1901 e 2099")
          case   (empty(.w_PNCODCLF) or not(.w_DTOBSO>.w_PNDATREG OR EMPTY(.w_DTOBSO)))  and not(NOT .w_PNTIPCLF$"CF")  and (.w_PNTIPCLF$"CF" AND .w_STAMPATO<>'S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPNCODCLF_1_59.SetFocus()
            i_bnoObbl = !empty(.w_PNCODCLF)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto")
          case   (empty(.w_PNCOMIVA) or not(.w_PNCOMIVA<=.w_PNDATREG OR .w_PNTIPREG $ " N"))  and not(.w_PNTIPREG $ " N")  and (.w_STAMPATO<>'S' AND NOT .w_PNTIPREG $ "NE")
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPNCOMIVA_1_60.SetFocus()
            i_bnoObbl = !empty(.w_PNCOMIVA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data competenza IVA incongruente o superiore alla data di registrazione")
          case   (empty(.w_PNDATPLA))  and not(.w_PNTIPREG $ ' EN' AND NOT ( .w_PNTIPREG='C' AND .w_PNTIPDOC='FC'  ))  and ((.w_PNTIPREG='A' OR .w_PNTIPREG='V' OR .w_PNTIPREG='C') AND .w_STAMPATO<>'S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPNDATPLA_1_61.SetFocus()
            i_bnoObbl = !empty(.w_PNDATPLA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PNCODVAL) or not(CHKDTOBS(.w_DTVOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)))  and (.w_STAMPATO<>'S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPNCODVAL_1_62.SetFocus()
            i_bnoObbl = !empty(.w_PNCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o obsoleta o valuta esercizio non definita")
          case   (empty(.w_PNCAOVAL))  and ((GETVALUT(g_PERVAL, 'VADATEUR')>=IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC) OR .w_CAOVAL=0) AND .w_STAMPATO<>'S')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oPNCAOVAL_1_66.SetFocus()
            i_bnoObbl = !empty(.w_PNCAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCG_MIV.CheckForm()
      if i_bres
        i_bres=  .GSCG_MIV.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_MAC.CheckForm()
      if i_bres
        i_bres=  .GSCG_MAC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_MDR.CheckForm()
      if i_bres
        i_bres=  .GSCG_MDR.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_ASA.CheckForm()
      if i_bres
        i_bres=  .GSCG_ASA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_AOS.CheckForm()
      if i_bres
        i_bres=  .GSCG_AOS.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gscg_mpn
      * --- Controlli Finali
      if i_bRes=.t. and .cFunction<>'Query'
         .w_RESCHK=0
          Ah_Msg('Controlli finali...',.T.)
           .NotifyEvent('ChkFinali')
           WAIT CLEAR
           if .w_RESCHK<>0
              i_bRes=.f.
           endif
       IF i_bRes=.T. AND ( Empty(Nvl(.w_INIESE,CP_chartodate('    -  -  '))) or NOT ( .w_INIESE<=.w_PNDATREG AND .w_PNDATREG<=.w_FINESE ))
            IF NOT AH_YESNO("Data registrazione esterna all'esercizio della registrazione contabile%0Confermi ugualmente?")
               i_bRes=.f.
            ENDIF
         endif
      endif
      
      * Effettuo il lancio dell'evento 'CaricaRitenute' nelle seguenti condizioni:
      * Modulo Ritenute Attivo , Fornitore Soggetto a Ritenute, Causale di testata senza flag No ritenute, Codice Fornitore specificato
      if (this.cFunction='Load' or this.cFunction='Edit') and i_bRes And this.w_TOTFOR<>This.w_TOTMDR and g_RITE='S' ;
          and not empty(this.w_PNCODCLF) and nvl(this.w_GESRIT,' ')='S' and ((this.w_RITENU$'CS' AND this.w_PNTIPCLF='F')OR (this.w_FLGRIT ='S' AND this.w_PNTIPCLF='C'))
           this.GSCG_MDR.LinkPCClick()
           IF ((this.cFunction='Load'or this.cFunction='Edit') and this.w_TESTRITE )
              this.GSCG_MDR.cnt.Notifyevent("Aggiorna")
           ENDIF
          i_bRes=.f.
       * disabilito accorpamento acconti pela seconda riesecuzione del GSCG_BCK
          .w_NOACC=.t.
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_PNTIPCON$"CFG") and ((.w_PNIMPDAR<>0 OR .w_PNIMPAVE<>0 OR .w_PNFLZERO="S") AND NOT EMPTY(.w_PNCODCON))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNTIPCON_2_1
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(.w_DTOBSO>.w_PNDATREG OR EMPTY(.w_DTOBSO)) and (.cFunction<>'Edit' OR ( vartype (I_SRV)='C' AND I_SRV='A' ) OR (.w_VARPAR='S' AND .w_VARCEN='S')) and not(empty(.w_PNCODCON)) and ((.w_PNIMPDAR<>0 OR .w_PNIMPAVE<>0 OR .w_PNFLZERO="S") AND NOT EMPTY(.w_PNCODCON))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNCODCON_2_7
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice conto inesistente o obsoleto")
        case   not(.w_PNFLPART $ 'CSA' OR .w_PARTSN<>'S' OR NOT .w_RIGPAR $ 'CSA') and (.w_PARTSN='S' AND .w_RIGPAR $ 'CSA' AND .cFunction <> 'Edit') and ((.w_PNIMPDAR<>0 OR .w_PNIMPAVE<>0 OR .w_PNFLZERO="S") AND NOT EMPTY(.w_PNCODCON))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPNFLPART_2_39
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire: C=crea partite o S=salda partite o A=acconto")
      endcase
      i_bRes = i_bRes .and. .GSCG_MCA.CheckForm()
      i_bRes = i_bRes .and. .GSCG_MPA.CheckForm()
      i_bRes = i_bRes .and. .GSCG_MMC.CheckForm()
      if (.w_PNIMPDAR<>0 OR .w_PNIMPAVE<>0 OR .w_PNFLZERO="S") AND NOT EMPTY(.w_PNCODCON)
        * --- Area Manuale = Check Row
        * --- gscg_mpn
        * --- Controlla che se la Causale Contabile ' di Apertura, non deve inserire conti Economici
        if .w_FLSALI='S' and .w_SEZB $ 'CR'
           i_bRes = .f.
           i_bnoChk = .f.
           i_cErrorMsg = ah_MsgFormat("Causale contabile di apertura. Non si possono inserire conti economici")
        endif
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PNSERIAL = this.w_PNSERIAL
    this.o_PNDATREG = this.w_PNDATREG
    this.o_PNCODCAU = this.w_PNCODCAU
    this.o_PNCOMPET = this.w_PNCOMPET
    this.o_PNVALNAZ = this.w_PNVALNAZ
    this.o_PNTIPCLF = this.w_PNTIPCLF
    this.o_PNTIPREG = this.w_PNTIPREG
    this.o_PNPRD = this.w_PNPRD
    this.o_PNPRP = this.w_PNPRP
    this.o_PNALFDOC = this.w_PNALFDOC
    this.o_PNNUMDOC = this.w_PNNUMDOC
    this.o_PNDATDOC = this.w_PNDATDOC
    this.o_OBTEST = this.w_OBTEST
    this.o_PNANNDOC = this.w_PNANNDOC
    this.o_PNANNPRO = this.w_PNANNPRO
    this.o_PNDESSUP = this.w_PNDESSUP
    this.o_PNALFPRO = this.w_PNALFPRO
    this.o_PNNUMPRO = this.w_PNNUMPRO
    this.o_PNCODCLF = this.w_PNCODCLF
    this.o_PNCODVAL = this.w_PNCODVAL
    this.o_CLFVAL = this.w_CLFVAL
    this.o_PNFLPROV = this.w_PNFLPROV
    this.o_PNTIPCON = this.w_PNTIPCON
    this.o_PNCAURIG = this.w_PNCAURIG
    this.o_PNCODCON = this.w_PNCODCON
    this.o_SEZB = this.w_SEZB
    this.o_PNIMPDAR = this.w_PNIMPDAR
    this.o_PNIMPAVE = this.w_PNIMPAVE
    this.o_PNFLPART = this.w_PNFLPART
    this.o_TIPSOT = this.w_TIPSOT
    this.o_FLMOVC = this.w_FLMOVC
    this.o_PCODCON = this.w_PCODCON
    * --- GSCG_MIV : Depends On
    this.GSCG_MIV.SaveDependsOn()
    * --- GSCG_MAC : Depends On
    this.GSCG_MAC.SaveDependsOn()
    * --- GSCG_MCA : Depends On
    this.GSCG_MCA.SaveDependsOn()
    * --- GSCG_MPA : Depends On
    this.GSCG_MPA.SaveDependsOn()
    * --- GSCG_MMC : Depends On
    this.GSCG_MMC.SaveDependsOn()
    * --- GSCG_MDR : Depends On
    this.GSCG_MDR.SaveDependsOn()
    * --- GSCG_ASA : Depends On
    this.GSCG_ASA.SaveDependsOn()
    * --- GSCG_AOS : Depends On
    this.GSCG_AOS.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=((t_PNIMPDAR<>0 OR t_PNIMPAVE<>0 OR t_PNFLZERO="S") AND NOT EMPTY(t_PNCODCON))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PNTIPCON=space(1)
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_PNDESRIG=space(50)
      .w_PNCAURIG=space(5)
      .w_RIGPAR=space(1)
      .w_CAUDES=space(35)
      .w_PNCODCON=space(15)
      .w_DESCON=space(30)
      .w_MASTRO=space(15)
      .w_SEZB=space(1)
      .w_DESCO2=space(30)
      .w_PNFLSALD=space(1)
      .w_PARTSN=space(1)
      .w_PNFLSALI=space(1)
      .w_CLMES1=0
      .w_PNFLSALF=space(1)
      .w_CLMES2=0
      .w_CLGIO1=0
      .w_CLGIO2=0
      .w_GIOFIS=0
      .w_PAGCLF1=space(5)
      .w_PAGMOR1=space(5)
      .w_DATMOR1=ctod("  /  /  ")
      .w_PNCODPAG=space(5)
      .w_PNFLZERO=space(1)
      .w_CCTAGG=space(1)
      .w_PNIMPDAR=0
      .w_PNIMPAVE=0
      .w_IMPDAR=0
      .w_SLIMPDAR=0
      .w_IMPAVE=0
      .w_SLIMPAVE=0
      .w_FLAGTOT=space(1)
      .w_FLDAVE=space(1)
      .w_PNCODCON=space(15)
      .w_BANAPP=space(10)
      .w_PARSAL=space(1)
      .w_FLAPAR=space(1)
      .w_PNFLPART=space(1)
      .w_PARCAR=space(1)
      .w_PNFLABAN=space(6)
      .w_CDCSAL=space(1)
      .w_PNINICOM=ctod("  /  /  ")
      .w_NUMCOR=space(25)
      .w_PNFINCOM=ctod("  /  /  ")
      .w_TIPSOT=space(1)
      .w_OANAL=space(1)
      .w_BANNOS=space(15)
      .w_PNIMPIND=0
      .w_VARPAR=space(1)
      .w_VARCEN=space(1)
      .w_CAUMOV=space(5)
      .w_CONBAN=space(15)
      .w_CALFES=space(3)
      .w_BAVAL=space(3)
      .w_FLASMO=space(1)
      .w_AGECLF=space(5)
      .w_PNCODAGE=space(5)
      .w_PNFLVABD=space(1)
      .w_SLAINI=0
      .w_SLDPRO=0
      .w_SLDPER=0
      .w_SLDFIN=0
      .w_SLAPRO=0
      .w_SLAPER=0
      .w_SLAFIN=0
      .w_SLDINI=0
      .w_PCODCON=space(10)
      .w_SALDO=0
      .w_DESCRI=space(30)
      .w_IMPFOR=0
      .DoRTCalc(1,102,.f.)
        .w_PNDESRIG = IIF( EMPTY(.w_PNDESRIG), .w_PNDESSUP, .w_PNDESRIG)
        .w_PNCAURIG = .w_PNCODCAU
      .DoRTCalc(104,104,.f.)
      if not(empty(.w_PNCAURIG))
        .link_2_4('Full')
      endif
      .DoRTCalc(105,107,.f.)
      if not(empty(.w_PNCODCON))
        .link_2_7('Full')
      endif
      .DoRTCalc(108,109,.f.)
        .w_SEZB = IIF(.w_PNTIPCON="G", CALCSEZ(.w_MASTRO), " ")
      .DoRTCalc(111,111,.f.)
        .w_PNFLSALD = IIF(.w_PNFLPROV='S', ' ', '+')
      .DoRTCalc(113,113,.f.)
        .w_PNFLSALI = IIF(.w_FLSALI='S' AND .w_PNFLPROV<>'S' AND .w_SEZB<>'T', '+', ' ')
      .DoRTCalc(115,115,.f.)
        .w_PNFLSALF = IIF(.w_FLSALF='S' AND .w_PNFLPROV<>'S' AND .w_SEZB<>'T', '+', ' ')
      .DoRTCalc(117,123,.f.)
        .w_PNCODPAG = IIF(.w_DATMOR1>=IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC) AND NOT EMPTY(.w_PAGMOR1), .w_PAGMOR1, IIF((NOT EMPTY(g_SAPAGA) AND .w_FLPART='A'),g_SAPAGA,IIF(.w_FLINSO='S' AND NOT EMPTY(NVL(.w_PAGINS,'')), .w_PAGINS,.w_PAGCLF1)))
      .DoRTCalc(124,124,.f.)
      if not(empty(.w_PNCODPAG))
        .link_2_24('Full')
      endif
      .DoRTCalc(125,128,.f.)
        .w_IMPDAR = .w_PNIMPDAR
        .w_SLIMPDAR = IIF(.w_PNIMPDAR-.w_PNIMPAVE>=0,.w_PNIMPDAR-.w_PNIMPAVE,0)
        .w_IMPAVE = .w_PNIMPAVE
        .w_SLIMPAVE = IIF(.w_PNIMPDAR-.w_PNIMPAVE<0,ABS(.w_PNIMPDAR-.w_PNIMPAVE),0)
      .DoRTCalc(133,133,.f.)
        .w_FLDAVE = IIF(.w_PNIMPDAR<>0, "D", IIF(.w_PNIMPAVE<>0, "A", " "))
      .DoRTCalc(135,135,.f.)
      if not(empty(.w_PNCODCON))
        .link_2_35('Full')
      endif
      .DoRTCalc(136,136,.f.)
        .w_PARSAL = ' '
        .w_FLAPAR = IIF(EMPTY(.w_PNFLABAN) AND .cFunction='Load' AND .w_PARSAL<>'.'  AND NOT .w_FLPART $ 'CA', 'S', ' ')
        .w_PNFLPART = IIF(.w_PARTSN="S", .w_RIGPAR, "N")
      .DoRTCalc(140,144,.f.)
      if not(empty(.w_NUMCOR))
        .link_2_45('Full')
      endif
      .DoRTCalc(145,146,.f.)
        .w_OANAL = IIF(.w_PNTIPCON="G" AND .w_SEZB $ "CR" AND .w_CCTAGG $ "AM", 'S', 'N')
      .DoRTCalc(148,149,.f.)
        .w_VARPAR = IIF(.w_PNFLPART $ 'CSA' AND g_PERPAR="S", 'N', 'S')
        .w_VARCEN = IIF(.w_PNTIPCON="G" AND .w_SEZB $ "CR" AND .w_FLANAL $ "SN" AND g_PERCCR="S" AND .w_CCTAGG $ "AM", 'N', 'S')
      .DoRTCalc(152,165,.f.)
      if not(empty(.w_CONBAN))
        .link_2_57('Full')
      endif
      .DoRTCalc(166,168,.f.)
        .w_FLASMO = ' '
        .oPgFrm.Page1.oPag.oObj_2_61.Calculate()
      .DoRTCalc(170,172,.f.)
        .w_PNCODAGE = .w_AGECLF
      .DoRTCalc(173,173,.f.)
      if not(empty(.w_PNCODAGE))
        .link_2_63('Full')
      endif
        .w_PNFLVABD = IIF(.w_PNFLPART $ 'AC', IIF(.w_PNTIPCLF='C', .w_FLVEBD, IIF(.w_PNTIPCLF='F', .w_FLACBD, ' ')), ' ')
        .oPgFrm.Page1.oPag.oObj_2_65.Calculate(IIF((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN)>=0, AH_MSGFORMAT('Saldo DARE:'), IIF((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN)<0, AH_MSGFORMAT('Saldo AVERE:'), '')))
      .DoRTCalc(175,185,.f.)
        .w_PCODCON = .w_PNCODCON
      .DoRTCalc(186,186,.f.)
      if not(empty(.w_PCODCON))
        .link_2_74('Full')
      endif
        .w_SALDO = ABS((.w_SLDPRO+.w_SLDPER+.w_SLDFIN) - (.w_SLAPRO+.w_SLAPER+.w_SLAFIN))
      .DoRTCalc(188,212,.f.)
        .w_IMPFOR = IIF(NOT EMPTY(.w_PNCODCON) AND .w_PNCAURIG=.w_PNCODCAU and g_Rite='S' AND .w_PNTIPCON $ 'F-C' ,iif(.w_PNTIPCON='F',.w_PNIMPAVE-.w_PNIMPDAR,.w_PNIMPDAR-.w_PNIMPAVE),0)
    endwith
    this.DoRTCalc(214,250,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PNTIPCON = t_PNTIPCON
    this.w_CPROWORD = t_CPROWORD
    this.w_PNDESRIG = t_PNDESRIG
    this.w_PNCAURIG = t_PNCAURIG
    this.w_RIGPAR = t_RIGPAR
    this.w_CAUDES = t_CAUDES
    this.w_PNCODCON = t_PNCODCON
    this.w_DESCON = t_DESCON
    this.w_MASTRO = t_MASTRO
    this.w_SEZB = t_SEZB
    this.w_DESCO2 = t_DESCO2
    this.w_PNFLSALD = t_PNFLSALD
    this.w_PARTSN = t_PARTSN
    this.w_PNFLSALI = t_PNFLSALI
    this.w_CLMES1 = t_CLMES1
    this.w_PNFLSALF = t_PNFLSALF
    this.w_CLMES2 = t_CLMES2
    this.w_CLGIO1 = t_CLGIO1
    this.w_CLGIO2 = t_CLGIO2
    this.w_GIOFIS = t_GIOFIS
    this.w_PAGCLF1 = t_PAGCLF1
    this.w_PAGMOR1 = t_PAGMOR1
    this.w_DATMOR1 = t_DATMOR1
    this.w_PNCODPAG = t_PNCODPAG
    this.w_PNFLZERO = t_PNFLZERO
    this.w_CCTAGG = t_CCTAGG
    this.w_PNIMPDAR = t_PNIMPDAR
    this.w_PNIMPAVE = t_PNIMPAVE
    this.w_IMPDAR = t_IMPDAR
    this.w_SLIMPDAR = t_SLIMPDAR
    this.w_IMPAVE = t_IMPAVE
    this.w_SLIMPAVE = t_SLIMPAVE
    this.w_FLAGTOT = t_FLAGTOT
    this.w_FLDAVE = t_FLDAVE
    this.w_PNCODCON = t_PNCODCON
    this.w_BANAPP = t_BANAPP
    this.w_PARSAL = t_PARSAL
    this.w_FLAPAR = t_FLAPAR
    this.w_PNFLPART = t_PNFLPART
    this.w_PARCAR = t_PARCAR
    this.w_PNFLABAN = t_PNFLABAN
    this.w_CDCSAL = t_CDCSAL
    this.w_PNINICOM = t_PNINICOM
    this.w_NUMCOR = t_NUMCOR
    this.w_PNFINCOM = t_PNFINCOM
    this.w_TIPSOT = t_TIPSOT
    this.w_OANAL = t_OANAL
    this.w_BANNOS = t_BANNOS
    this.w_PNIMPIND = t_PNIMPIND
    this.w_VARPAR = t_VARPAR
    this.w_VARCEN = t_VARCEN
    this.w_CAUMOV = t_CAUMOV
    this.w_CONBAN = t_CONBAN
    this.w_CALFES = t_CALFES
    this.w_BAVAL = t_BAVAL
    this.w_FLASMO = t_FLASMO
    this.w_AGECLF = t_AGECLF
    this.w_PNCODAGE = t_PNCODAGE
    this.w_PNFLVABD = t_PNFLVABD
    this.w_SLAINI = t_SLAINI
    this.w_SLDPRO = t_SLDPRO
    this.w_SLDPER = t_SLDPER
    this.w_SLDFIN = t_SLDFIN
    this.w_SLAPRO = t_SLAPRO
    this.w_SLAPER = t_SLAPER
    this.w_SLAFIN = t_SLAFIN
    this.w_SLDINI = t_SLDINI
    this.w_PCODCON = t_PCODCON
    this.w_SALDO = t_SALDO
    this.w_DESCRI = t_DESCRI
    this.w_IMPFOR = t_IMPFOR
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PNTIPCON with this.w_PNTIPCON
    replace t_CPROWORD with this.w_CPROWORD
    replace t_PNDESRIG with this.w_PNDESRIG
    replace t_PNCAURIG with this.w_PNCAURIG
    replace t_RIGPAR with this.w_RIGPAR
    replace t_CAUDES with this.w_CAUDES
    replace t_PNCODCON with this.w_PNCODCON
    replace t_DESCON with this.w_DESCON
    replace t_MASTRO with this.w_MASTRO
    replace t_SEZB with this.w_SEZB
    replace t_DESCO2 with this.w_DESCO2
    replace t_PNFLSALD with this.w_PNFLSALD
    replace t_PARTSN with this.w_PARTSN
    replace t_PNFLSALI with this.w_PNFLSALI
    replace t_CLMES1 with this.w_CLMES1
    replace t_PNFLSALF with this.w_PNFLSALF
    replace t_CLMES2 with this.w_CLMES2
    replace t_CLGIO1 with this.w_CLGIO1
    replace t_CLGIO2 with this.w_CLGIO2
    replace t_GIOFIS with this.w_GIOFIS
    replace t_PAGCLF1 with this.w_PAGCLF1
    replace t_PAGMOR1 with this.w_PAGMOR1
    replace t_DATMOR1 with this.w_DATMOR1
    replace t_PNCODPAG with this.w_PNCODPAG
    replace t_PNFLZERO with this.w_PNFLZERO
    replace t_CCTAGG with this.w_CCTAGG
    replace t_PNIMPDAR with this.w_PNIMPDAR
    replace t_PNIMPAVE with this.w_PNIMPAVE
    replace t_IMPDAR with this.w_IMPDAR
    replace t_SLIMPDAR with this.w_SLIMPDAR
    replace t_IMPAVE with this.w_IMPAVE
    replace t_SLIMPAVE with this.w_SLIMPAVE
    replace t_FLAGTOT with this.w_FLAGTOT
    replace t_FLDAVE with this.w_FLDAVE
    replace t_PNCODCON with this.w_PNCODCON
    replace t_BANAPP with this.w_BANAPP
    replace t_PARSAL with this.w_PARSAL
    replace t_FLAPAR with this.w_FLAPAR
    replace t_PNFLPART with this.w_PNFLPART
    replace t_PARCAR with this.w_PARCAR
    replace t_PNFLABAN with this.w_PNFLABAN
    replace t_CDCSAL with this.w_CDCSAL
    replace t_PNINICOM with this.w_PNINICOM
    replace t_NUMCOR with this.w_NUMCOR
    replace t_PNFINCOM with this.w_PNFINCOM
    replace t_TIPSOT with this.w_TIPSOT
    replace t_OANAL with this.w_OANAL
    replace t_BANNOS with this.w_BANNOS
    replace t_PNIMPIND with this.w_PNIMPIND
    replace t_VARPAR with this.w_VARPAR
    replace t_VARCEN with this.w_VARCEN
    replace t_CAUMOV with this.w_CAUMOV
    replace t_CONBAN with this.w_CONBAN
    replace t_CALFES with this.w_CALFES
    replace t_BAVAL with this.w_BAVAL
    replace t_FLASMO with this.w_FLASMO
    replace t_AGECLF with this.w_AGECLF
    replace t_PNCODAGE with this.w_PNCODAGE
    replace t_PNFLVABD with this.w_PNFLVABD
    replace t_SLAINI with this.w_SLAINI
    replace t_SLDPRO with this.w_SLDPRO
    replace t_SLDPER with this.w_SLDPER
    replace t_SLDFIN with this.w_SLDFIN
    replace t_SLAPRO with this.w_SLAPRO
    replace t_SLAPER with this.w_SLAPER
    replace t_SLAFIN with this.w_SLAFIN
    replace t_SLDINI with this.w_SLDINI
    replace t_PCODCON with this.w_PCODCON
    replace t_SALDO with this.w_SALDO
    replace t_DESCRI with this.w_DESCRI
    replace t_IMPFOR with this.w_IMPFOR
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTDAR = .w_TOTDAR-.w_impdar
        .w_TOTAVE = .w_TOTAVE-.w_impave
        .w_TOTFOR = .w_TOTFOR-.w_impfor
        .SetControlsValue()
      endwith
  EndProc
  func CanDelete()
    local i_res
    i_res=GSMA_BAE(this,'G')
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione annullata"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgscg_mpnPag1 as StdContainer
  Width  = 786
  height = 535
  stdWidth  = 786
  stdheight = 535
  resizeXpos=231
  resizeYpos=408
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="HBNWIGPNUX",left=4, top=352, width=730,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="CPROWORD",Label1="Riga",Field2="PNTIPCON",Label2="Tipo",Field3="PNCODCON",Label3="Conto",Field4="DESCON",Label4="Descrizione",Field5="PNIMPDAR",Label5="DARE",Field6="PNIMPAVE",Label6="AVERE",Field7="PNFLPART",Label7="Partite",Field8="",Label8="",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 237903386

  add object oPNNUMRER_1_10 as StdField with uid="HIOPTUKWWJ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PNNUMRER", cQueryName = "PNNUMRER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 27259320,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=68, Top=5, cSayPict='"999999"', cGetPict='"999999"'

  func oPNNUMRER_1_10.mCond()
    with this.Parent.oContained
      return (.w_STAMPATO<>'S')
    endwith
  endfunc

  proc oPNNUMRER_1_10.mBefore
    with this.Parent.oContained
      this.cQueryName = "PNCOMPET,PNNUMRER"
    endwith
  endproc

  proc oPNNUMRER_1_10.mAfter
    with this.Parent.oContained
      .w_MODNRE=.T.
    endwith
  endproc

  add object oPNCODUTE_1_13 as StdField with uid="ZAABFLLFLQ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_PNCODUTE", cQueryName = "PNCODUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 255238597,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=140, Top=5, cSayPict='"9999"', cGetPict='"9999"'

  add object oPNDATREG_1_14 as StdField with uid="OQNQFWRVBW",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PNDATREG", cQueryName = "PNDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione",;
    HelpContextID = 21270979,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=225, Top=5

  func oPNDATREG_1_14.mCond()
    with this.Parent.oContained
      return (.w_STAMPATO<>'S')
    endwith
  endfunc

  add object oPNCODCAU_1_16 as StdField with uid="MPUKMVUALV",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PNCODCAU", cQueryName = "PNDATREG,PNCODCAU",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    ToolTipText = "Codice della causale contabile",;
    HelpContextID = 20357557,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=68, Top=32, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_PNCODCAU"

  func oPNCODCAU_1_16.mCond()
    with this.Parent.oContained
      return (.w_STAMPATO<>'S' AND .cFunction<>'Edit')
    endwith
  endfunc

  proc oPNCODCAU_1_16.mBefore
    with this.Parent.oContained
      this.cQueryName = "PNCOMPET,PNCODCAU"
    endwith
  endproc

  func oPNCODCAU_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oPNCODCAU_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPNCODCAU_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oPNCODCAU_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oPNCODCAU_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_PNCODCAU
    i_obj.ecpSave()
  endproc

  add object oPNCOMPET_1_17 as StdField with uid="MEOSZCLNMA",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PNCOMPET", cQueryName = "PNCOMPET",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza contabile della registrazione (imposta i saldi)",;
    HelpContextID = 61252022,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=380, Top=5, InputMask=replicate('X',4), bHasZoom = .t. , tabstop=.f., cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_PNCOMPET"

  func oPNCOMPET_1_17.mCond()
    with this.Parent.oContained
      if (.w_FLCOMP='S' AND .w_STAMPATO<>'S')
        if .nLastRow>1
          return (.f.)
        endif
      else
        return (.f.)
      endif
    endwith
  endfunc

  func oPNCOMPET_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
      if .not. empty(.w_PNCODCON)
        bRes2=.link_2_35('Full')
      endif
      if .not. empty(.w_PCODCON)
        bRes2=.link_2_74('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPNCOMPET_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPNCOMPET_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oPNCOMPET_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco esercizi",'',this.parent.oContained
  endproc

  add object oDESCAU_1_23 as StdField with uid="GYKUCEMLFQ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 259107786,;
   bGlobalFont=.t.,;
    Height=21, Width=294, Left=141, Top=32, InputMask=replicate('X',35)

  add object oPNALFDOC_1_46 as StdField with uid="ZTOBVTOCJO",rtseq=45,rtrep=.f.,;
    cFormVar = "w_PNALFDOC", cQueryName = "PNALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 1688007,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=218, Top=60, cSayPict="'!!!!!!!!!!'", cGetPict="'!!!!!!!!!!'", InputMask=replicate('X',10)

  func oPNALFDOC_1_46.mCond()
    with this.Parent.oContained
      return (.w_STAMPATO<>'S' AND .w_FLPDOC<>'N')
    endwith
  endfunc


  add object oBtn_1_47 as StdButton with uid="MPMCULIORF",left=304, top=58, width=24,height=22,;
    CpPicture="bmp\3points.bmp", caption="", nPag=1,tabstop=.f.;
    , ToolTipText = "Numerazione facoltativa per comunicazione delle fatture emesse e ricevute";
    , HelpContextID = 120969974;
    , ImgSize=16;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_47.Click()
      do GSCG_KNF with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_47.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PNTIPREG<>'A')
    endwith
   endif
  endfunc

  add object oPNNUMDOC_1_48 as StdField with uid="YXHCGCKRSQ",rtseq=46,rtrep=.f.,;
    cFormVar = "w_PNNUMDOC", cQueryName = "PNNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero del documento",;
    HelpContextID = 262140359,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=68, Top=60, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oPNNUMDOC_1_48.mCond()
    with this.Parent.oContained
      return (.w_STAMPATO<>'S' AND .w_FLPDOC<>'N')
    endwith
  endfunc

  proc oPNNUMDOC_1_48.mBefore
    with this.Parent.oContained
      this.cQueryName = "PNCOMPET,PNNUMDOC"
    endwith
  endproc

  proc oPNNUMDOC_1_48.mAfter
    with this.Parent.oContained
      .w_MODNDO=.T.
    endwith
  endproc

  add object oPNDATDOC_1_49 as StdField with uid="MYASTPPCJZ",rtseq=47,rtrep=.f.,;
    cFormVar = "w_PNDATDOC", cQueryName = "PNDATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del documento",;
    HelpContextID = 256152007,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=359, Top=60

  func oPNDATDOC_1_49.mCond()
    with this.Parent.oContained
      return (.w_STAMPATO<>'S' AND .w_FLPDOC<>'N')
    endwith
  endfunc

  func oPNDATDOC_1_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHPNTDOC(.w_PNDATREG, .w_PNDATDOC))
    endwith
    return bRes
  endfunc

  add object oPNDESSUP_1_54 as StdField with uid="ZZUJPBZCBC",rtseq=52,rtrep=.f.,;
    cFormVar = "w_PNDESSUP", cQueryName = "PNDESSUP",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione aggiuntiva dell'operazione",;
    HelpContextID = 5280186,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=367, Left=68, Top=84, InputMask=replicate('X',50)

  func oPNDESSUP_1_54.mCond()
    with this.Parent.oContained
      return (.w_PNFLREGI<>'S')
    endwith
  endfunc

  add object oPN__MESE_1_55 as StdField with uid="PJRMCXEJYU",rtseq=53,rtrep=.f.,;
    cFormVar = "w_PN__MESE", cQueryName = "PN__MESE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Mese di riferimento: selezionare un mese compreso tra 1 e 12",;
    ToolTipText = "Mese di riferimento dell'operazione",;
    HelpContextID = 244638149,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=479, Top=122, cSayPict='"@L"', cGetPict='"99"'

  func oPN__MESE_1_55.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not(.w_PNTIPREG<>'N' AND .w_PNTIPDOC $ 'FA-NC-FC-FE-NE-AU-NU' AND .w_PNTIPCLF $ 'C-F'))
    endwith
    endif
  endfunc

  func oPN__MESE_1_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PN__MESE>=1 and .w_PN__MESE<=12) OR .w_PN__MESE=0)
    endwith
    return bRes
  endfunc

  add object oPN__ANNO_1_56 as StdField with uid="CPVYILICVS",rtseq=54,rtrep=.f.,;
    cFormVar = "w_PN__ANNO", cQueryName = "PN__ANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Anno solare: selezionare un anno compreso tra 1901 e 2099",;
    ToolTipText = "Anno solare di riferimento dell'operazione",;
    HelpContextID = 106226107,;
   bGlobalFont=.t.,;
    Height=21, Width=59, Left=601, Top=122, cSayPict='"9999"', cGetPict='"9999"'

  func oPN__ANNO_1_56.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not(.w_PNTIPREG<>'N' AND .w_PNTIPDOC $ 'FA-NC-FC-FE-NE-AU-NU' AND .w_PNTIPCLF $ 'C-F'))
    endwith
    endif
  endfunc

  func oPN__ANNO_1_56.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PN__ANNO>=1901 and .w_PN__ANNO <=2099) OR .w_PN__ANNO=0)
    endwith
    return bRes
  endfunc

  add object oPNALFPRO_1_57 as StdField with uid="IMAMRIJLXG",rtseq=55,rtrep=.f.,;
    cFormVar = "w_PNALFPRO", cQueryName = "PNALFPRO",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie protocollo",;
    HelpContextID = 68796859,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=601, Top=60, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oPNALFPRO_1_57.mCond()
    with this.Parent.oContained
      return (.w_STAMPATO<>'S' AND NOT .w_FLPPRO $ ' N')
    endwith
  endfunc

  func oPNALFPRO_1_57.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLPPRO $ ' N')
    endwith
    endif
  endfunc

  add object oPNNUMPRO_1_58 as StdField with uid="CUZXPMRXEZ",rtseq=56,rtrep=.f.,;
    cFormVar = "w_PNNUMPRO", cQueryName = "PNNUMPRO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero protocollo",;
    HelpContextID = 60813755,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=479, Top=60, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oPNNUMPRO_1_58.mCond()
    with this.Parent.oContained
      return (.w_STAMPATO<>'S' AND NOT .w_FLPPRO $ ' N')
    endwith
  endfunc

  func oPNNUMPRO_1_58.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLPPRO $ ' N')
    endwith
    endif
  endfunc

  add object oPNCODCLF_1_59 as StdField with uid="SCQMEERZTO",rtseq=57,rtrep=.f.,;
    cFormVar = "w_PNCODCLF", cQueryName = "PNCODCLF",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto",;
    ToolTipText = "Cliente/fornitore intestatario del documento",;
    HelpContextID = 248077884,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=68, Top=133, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_PNTIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_PNCODCLF"

  func oPNCODCLF_1_59.mCond()
    with this.Parent.oContained
      return (.w_PNTIPCLF$"CF" AND .w_STAMPATO<>'S')
    endwith
  endfunc

  func oPNCODCLF_1_59.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_PNTIPCLF$"CF")
    endwith
    endif
  endfunc

  func oPNCODCLF_1_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_59('Part',this)
    endwith
    return bRes
  endfunc

  proc oPNCODCLF_1_59.ecpDrop(oSource)
    this.Parent.oContained.link_1_59('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPNCODCLF_1_59.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PNTIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_PNTIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPNCODCLF_1_59'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Anagrafica clienti/fornitori",'GSCG1MPN.CONTI_VZM',this.parent.oContained
  endproc
  proc oPNCODCLF_1_59.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_PNTIPCLF
     i_obj.w_ANCODICE=this.parent.oContained.w_PNCODCLF
    i_obj.ecpSave()
  endproc

  add object oPNCOMIVA_1_60 as StdField with uid="LYMKSDMTUU",rtseq=58,rtrep=.f.,;
    cFormVar = "w_PNCOMIVA", cQueryName = "PNCOMIVA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data competenza IVA incongruente o superiore alla data di registrazione",;
    ToolTipText = "Data competenza IVA",;
    HelpContextID = 89742903,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=479, Top=151, TabStop=.f.

  func oPNCOMIVA_1_60.mCond()
    with this.Parent.oContained
      return (.w_STAMPATO<>'S' AND NOT .w_PNTIPREG $ "NE")
    endwith
  endfunc

  func oPNCOMIVA_1_60.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PNTIPREG $ " N")
    endwith
    endif
  endfunc

  func oPNCOMIVA_1_60.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PNCOMIVA<=.w_PNDATREG OR .w_PNTIPREG $ " N")
    endwith
    return bRes
  endfunc

  add object oPNDATPLA_1_61 as StdField with uid="ZJQUCAMVAE",rtseq=59,rtrep=.f.,;
    cFormVar = "w_PNDATPLA", cQueryName = "PNDATPLA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data competenza plafond esenzione IVA",;
    HelpContextID = 213610039,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=601, Top=151, TabStop=.f.

  func oPNDATPLA_1_61.mCond()
    with this.Parent.oContained
      return ((.w_PNTIPREG='A' OR .w_PNTIPREG='V' OR .w_PNTIPREG='C') AND .w_STAMPATO<>'S')
    endwith
  endfunc

  func oPNDATPLA_1_61.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PNTIPREG $ ' EN' AND NOT ( .w_PNTIPREG='C' AND .w_PNTIPDOC='FC'  ))
    endwith
    endif
  endfunc

  add object oPNCODVAL_1_62 as StdField with uid="KKPQGOQSFH",rtseq=60,rtrep=.f.,;
    cFormVar = "w_PNCODVAL", cQueryName = "PNCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o obsoleta o valuta esercizio non definita",;
    ToolTipText = "Codice valuta della registrazione",;
    HelpContextID = 238461374,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=68, Top=225, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_PNCODVAL"

  func oPNCODVAL_1_62.mCond()
    with this.Parent.oContained
      return (.w_STAMPATO<>'S')
    endwith
  endfunc

  func oPNCODVAL_1_62.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_62('Part',this)
    endwith
    return bRes
  endfunc

  proc oPNCODVAL_1_62.ecpDrop(oSource)
    this.Parent.oContained.link_1_62('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPNCODVAL_1_62.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oPNCODVAL_1_62'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Elenco valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oPNCODVAL_1_62.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_PNCODVAL
    i_obj.ecpSave()
  endproc

  add object oPNCAOVAL_1_66 as StdField with uid="NOXRYIVNPE",rtseq=64,rtrep=.f.,;
    cFormVar = "w_PNCAOVAL", cQueryName = "PNCAOVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio del documento associato",;
    HelpContextID = 227844542,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=178, Top=225, cSayPict='"99999.999999"', cGetPict='"99999.999999"', tabstop=.f.

  func oPNCAOVAL_1_66.mCond()
    with this.Parent.oContained
      return ((GETVALUT(g_PERVAL, 'VADATEUR')>=IIF(EMPTY(.w_PNDATDOC), .w_PNDATREG, .w_PNDATDOC) OR .w_CAOVAL=0) AND .w_STAMPATO<>'S')
    endwith
  endfunc

  add object oSIMVAL_1_67 as StdField with uid="SDROEOLMPG",rtseq=65,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 140445402,;
   bGlobalFont=.t.,;
    Height=21, Width=57, Left=225, Top=252, InputMask=replicate('X',5)

  add object oDESCLF_1_68 as StdField with uid="OORFYQIKPV",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(36), bMultilanguage =  .f.,;
    HelpContextID = 230796234,;
    FontName = "Tahoma", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=214, Left=68, Top=163, InputMask=replicate('X',36)

  func oDESCLF_1_68.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_PNTIPCLF$"CF")
    endwith
    endif
  endfunc

  add object oDESPAG_1_75 as StdField with uid="USGZIYEQIK",rtseq=73,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Pagamento standard del cliente/fornitore definito in anagrafica",;
    HelpContextID = 224701386,;
    FontName = "Tahoma", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=213, Left=68, Top=279, InputMask=replicate('X',30)

  func oDESPAG_1_75.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_PNTIPCLF$"CF")
    endwith
    endif
  endfunc

  add object oPNTOTDOC_1_76 as StdField with uid="ZHNHHJGEYJ",rtseq=74,rtrep=.f.,;
    cFormVar = "w_PNTOTDOC", cQueryName = "PNTOTDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale del documento (nella valuta originale)",;
    HelpContextID = 255168967,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=68, Top=252, cSayPict="v_PV(38+VVL)", cGetPict="v_GV(38+VVL)"

  func oPNTOTDOC_1_76.mCond()
    with this.Parent.oContained
      return ((.w_PNTIPCLF$"CF" OR (.w_PNTIPCLF='N' And .w_PNCODVAL<>.w_PNVALNAZ)) AND  .w_STAMPATO<>'S')
    endwith
  endfunc


  add object oBtn_1_90 as StdButton with uid="PGSLXFJPDF",left=738, top=97, width=48,height=45,;
    CpPicture="bmp\modprot.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare automatismi";
    , HelpContextID = 149695526;
    , TabStop=.f., Caption='\<Modello';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_90.Click()
      with this.Parent.oContained
        do GSCG_BAU with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_90.mCond()
    with this.Parent.oContained
      return (.w_PNNUMRER<>0)
    endwith
  endfunc


  add object oLinkPC_1_95 as stdDynamicChildContainer with uid="KHELJKFRTQ",left=288, top=179, width=494, height=170, bOnScreen=.t.;


  func oLinkPC_1_95.mCond()
    with this.Parent.oContained
      return (.w_PNTIPREG<>"N" AND (.w_PNTIPREG<>"E" or .w_PNTIPDOC<>"CO") AND .w_STAMPATO<>'S')
    endwith
  endfunc


  add object oLinkPC_1_98 as StdButton with uid="CGUDSJKYIF",left=687, top=50, width=48,height=45,;
    CpPicture="bmp\CESP.bmp", caption="", nPag=1;
    , ToolTipText = "Dettagli movimenti cespiti collegati";
    , HelpContextID = 224216634;
    , TabStop=.f., Caption='\<Mov.Cespiti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_98.Click()
      this.Parent.oContained.GSCG_MAC.LinkPCClick()
    endproc

  func oLinkPC_1_98.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_CESP<>'S' OR .w_MOVCES<>'S')
    endwith
   endif
  endfunc


  add object oPNFLPROV_1_107 as StdCombo with uid="MMTGJJRZNV",rtseq=85,rtrep=.f.,left=479,top=5,width=118,height=21;
    , TabStop=.f.;
    , ToolTipText = "Stato della registrazione: (provvisorio o confermato)";
    , HelpContextID = 24736180;
    , cFormVar="w_PNFLPROV",RowSource=""+"Confermato,"+"Provvisorio", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPNFLPROV_1_107.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PNFLPROV,&i_cF..t_PNFLPROV),this.value)
    return(iif(xVal =1,'N',;
    iif(xVal =2,'S',;
    space(1))))
  endfunc
  func oPNFLPROV_1_107.GetRadio()
    this.Parent.oContained.w_PNFLPROV = this.RadioValue()
    return .t.
  endfunc

  func oPNFLPROV_1_107.ToRadio()
    this.Parent.oContained.w_PNFLPROV=trim(this.Parent.oContained.w_PNFLPROV)
    return(;
      iif(this.Parent.oContained.w_PNFLPROV=='N',1,;
      iif(this.Parent.oContained.w_PNFLPROV=='S',2,;
      0)))
  endfunc

  func oPNFLPROV_1_107.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPNFLPROV_1_107.mCond()
    with this.Parent.oContained
      return (.w_STAMPATO<>'S')
    endwith
  endfunc


  add object oObj_1_121 as cp_runprogram with uid="OJDBMJBAZA",left=-3, top=572, width=300,height=18,;
    caption='GSCG_BP1(1)',;
   bGlobalFont=.t.,;
    prg='GSCG_BP1("1")',;
    cEvent = "w_PNCODCAU Changed,New record",;
    nPag=1;
    , HelpContextID = 9165289


  add object oObj_1_122 as cp_runprogram with uid="IZNRKUQYGT",left=-3, top=591, width=300,height=18,;
    caption='GSCG_BP1(2)',;
   bGlobalFont=.t.,;
    prg='GSCG_BP1("2")',;
    cEvent = "w_PNCODCLF Changed",;
    nPag=1;
    , HelpContextID = 9165033


  add object oObj_1_123 as cp_runprogram with uid="GFBUDPYVVF",left=-3, top=648, width=300,height=18,;
    caption='GSCG_BCF(F)',;
   bGlobalFont=.t.,;
    prg='GSCG_BCF("F")',;
    cEvent = "w_PNFLPROV Changed",;
    nPag=1;
    , ToolTipText = "Cambia i flag saldi";
    , HelpContextID = 9159892


  add object oObj_1_124 as cp_runprogram with uid="GYCFJNZMMG",left=-3, top=629, width=300,height=18,;
    caption='GSCG_BCF(C)',;
   bGlobalFont=.t.,;
    prg='GSCG_BCF("C")',;
    cEvent = "w_PNCOMPET Changed",;
    nPag=1;
    , ToolTipText = "Cambia le date di competenza";
    , HelpContextID = 9160660


  add object oObj_1_125 as cp_runprogram with uid="TLLNQPOVQW",left=303, top=628, width=233,height=18,;
    caption='GSCG_BCK',;
   bGlobalFont=.t.,;
    prg="GSCG_BCK",;
    cEvent = "ChkFinali,Delete start",;
    nPag=1;
    , ToolTipText = "Controlli primanota";
    , HelpContextID = 9346383


  add object oObj_1_126 as cp_runprogram with uid="BEWVCCHCFW",left=303, top=571, width=233,height=18,;
    caption='GSCG_BPK',;
   bGlobalFont=.t.,;
    prg="GSCG_BPK",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , ToolTipText = "Controlli partite / centri di costo collegati";
    , HelpContextID = 9346383


  add object oObj_1_127 as cp_runprogram with uid="AXYJWAQPMF",left=303, top=590, width=233,height=18,;
    caption='GSCG_BRP',;
   bGlobalFont=.t.,;
    prg="GSCG_BRP",;
    cEvent = "RigheDaPartite",;
    nPag=1;
    , HelpContextID = 9346378


  add object oObj_1_131 as cp_runprogram with uid="DMWMLQMPNW",left=-3, top=610, width=300,height=18,;
    caption='GSCG_BP1(3)',;
   bGlobalFont=.t.,;
    prg='GSCG_BP1("3")',;
    cEvent = "Edit Aborted",;
    nPag=1;
    , HelpContextID = 9164777


  add object oObj_1_138 as cp_runprogram with uid="BMRFKCNUQP",left=303, top=609, width=233,height=18,;
    caption='GSCG_BDE',;
   bGlobalFont=.t.,;
    prg="GSCG_BDE",;
    cEvent = "w_PNDESSUP Changed",;
    nPag=1;
    , HelpContextID = 9346389


  add object oObj_1_151 as cp_runprogram with uid="ACFUDMTTLK",left=539, top=590, width=180,height=18,;
    caption='GSCG_BTK(T)',;
   bGlobalFont=.t.,;
    prg="GSCG_BTK('T')",;
    cEvent = "CambiaTipo",;
    nPag=1;
    , HelpContextID = 9156303


  add object oObj_1_155 as cp_runprogram with uid="ABNZKHUSCN",left=303, top=647, width=233,height=18,;
    caption='GSCG_BTK(D)',;
   bGlobalFont=.t.,;
    prg="GSCG_BTK('D')",;
    cEvent = "w_PNDATREG Changed",;
    nPag=1;
    , ToolTipText = "Avvisa ricalcolo num.reg. al cambio data reg.";
    , HelpContextID = 9160399


  add object oObj_1_165 as cp_runprogram with uid="LBZUIZXLQW",left=539, top=609, width=155,height=17,;
    caption='GSCG_BHE',;
   bGlobalFont=.t.,;
    prg="GSCG_BHE",;
    cEvent = "HasEvent,Load",;
    nPag=1;
    , ToolTipText = "Lancia check per hascpevent (area manuale declare)";
    , HelpContextID = 259089067


  add object oBtn_1_167 as StdButton with uid="DCKLQWRFKC",left=688, top=50, width=48,height=45,;
    CpPicture="bmp\CESP.bmp", caption="", nPag=1;
    , ToolTipText = "Dettagli movimenti cespiti collegati";
    , HelpContextID = 224216634;
    , TabStop=.f.,Caption='Mo\<v.Cespiti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_167.Click()
      do GSCG_KAC with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_167.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_CESP<>'S' OR .w_MOVCES='S' OR .Cfunction='Load' OR Not .w_ABILITA)
    endwith
   endif
  endfunc


  add object oObj_1_170 as cp_runprogram with uid="DOGSXQGCIX",left=539, top=628, width=151,height=17,;
    caption='GSCG_BRD',;
   bGlobalFont=.t.,;
    prg="GSCG_BRD",;
    cEvent = "ControlloStampa",;
    nPag=1;
    , ToolTipText = "Controllo se sono gi� stati stampati i registri IVA";
    , HelpContextID = 9346390


  add object oBtn_1_172 as Btn_Mpn with uid="SJYANKRLEU",left=183, top=303, width=48,height=45,;
    CpPicture="BMP\AUTO.BMP", caption="", nPag=1;
    , ToolTipText = "Applica eventuale automatismo contabile";
    , HelpContextID = 8128353;
    , Caption='Au\<tomat.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_172.Click()
      with this.Parent.oContained
        do GSCG_BP3 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_172.mCond()
    with this.Parent.oContained
      return (Empty(.w_GIACAR))
    endwith
  endfunc

  func oBtn_1_172.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not ( Empty(.w_GIACAR)  ) OR .cfunction='Query')
    endwith
   endif
  endfunc


  add object oLinkPC_1_179 as StdButton with uid="KTKBYYFEMR",left=738, top=50, width=48,height=45,;
    CpPicture="bmp\Ritenute.bmp", caption="", nPag=1;
    , HelpContextID = 58886523;
    , Caption='R\<itenute', tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_179.Click()
      this.Parent.oContained.GSCG_MDR.LinkPCClick()
    endproc

  func oLinkPC_1_179.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not(((.w_PNTIPCLF='F' AND .w_RITENU $ 'SC') or (.w_PNTIPCLF='C' AND .w_FLGRIT = 'S') ) And .w_GESRIT='S' AND NOT EMPTY(.w_PNCODCLF) AND g_RITE='S' ) OR (.cfunction='Load' And Not .w_PCCLICK ))
    endwith
   endif
  endfunc


  add object oObj_1_185 as cp_runprogram with uid="IKNMXXXCSP",left=303, top=666, width=233,height=18,;
    caption='GSAR_BRP',;
   bGlobalFont=.t.,;
    prg="GSAR_BRP(w_FLVEAC, w_PNANNDOC, w_PNPRD,  w_PNNUMDOC, w_PNALFDOC, w_PNPRP, w_PNNUMPRO, w_PNALFPRO, w_PNANNPRO, '  ')",;
    cEvent = "RecuperoProg",;
    nPag=1;
    , HelpContextID = 8633674


  add object oBtn_1_190 as StdButton with uid="UHLKEQDFSQ",left=638, top=3, width=48,height=45,;
    CpPicture="bmp\cattura.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da unit� disco";
    , HelpContextID = 251559898;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'CATTURA', disabledpicture = 'bmp\catturad.bmp',Caption='\<Cattura';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_190.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"PNT_MAST",.w_PNSERIAL,"GSCG_MPN","C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_190.mCond()
    with this.Parent.oContained
      return (.w_PNNUMRER<>0 And .cFunction='Query')
    endwith
  endfunc

  func oBtn_1_190.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_FLARDO<>'S' or isalt())
    endwith
   endif
  endfunc


  add object oBtn_1_191 as StdButton with uid="YYRCTQLCDE",left=688, top=3, width=48,height=45,;
    CpPicture="BMP\visualicat.ico", caption="", nPag=1;
    , ToolTipText = "Visualizza allegati";
    , HelpContextID = 163766896;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_191.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"PNT_MAST",.w_PNSERIAL,"GSCG_MPN","V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_191.mCond()
    with this.Parent.oContained
      return (.w_PNNUMRER<>0 And .cFunction='Query')
    endwith
  endfunc

  func oBtn_1_191.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_FLARDO<>'S' or isalt())
    endwith
   endif
  endfunc


  add object oBtn_1_192 as StdButton with uid="BJRFWTRDLX",left=738, top=3, width=48,height=45,;
    CpPicture="bmp\scanner.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da periferica di acquisizione immagini";
    , HelpContextID = 209038554;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'SCANNER', disabledpicture = 'bmp\scannerd.bmp',Caption='\<Scanner';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_192.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"PNT_MAST",.w_PNSERIAL,"GSCG_MPN","S",MROW(),MCOL(),.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_192.mCond()
    with this.Parent.oContained
      return (.w_PNNUMRER<>0 And .cFunction='Query')
    endwith
  endfunc

  func oBtn_1_192.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DOCM<>'S' OR g_FLARDO<>'S' or isalt())
    endwith
   endif
  endfunc


  add object oBtn_1_194 as StdButton with uid="AEKDJBHXKK",left=604, top=6, width=24,height=23,;
    caption="...", nPag=1;
    , ToolTipText = "Conferma registrazioni provvisorie";
    , HelpContextID = 121170902;
    , TabStop=.f.,Caption='...';
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_194.Click()
      do GSCG_KCP with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_194.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PNFLPROV='N' OR Not(.w_PNTIPCLF='F' AND .w_RITENU $ 'SC' And .w_GESRIT='S' AND NOT EMPTY(.w_PNCODCLF) AND g_RITE='S' ) OR .cfunction <>'Query')
    endwith
   endif
  endfunc


  add object oBtn_1_212 as StdButton with uid="XDOCUNIBLC",left=688, top=97, width=48,height=45,;
    CpPicture="BMP\NOTE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per modificare i dati descrittivi";
    , HelpContextID = 230084101;
    , TabStop=.f., Caption='Var.\<note';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_212.Click()
      with this.Parent.oContained
        gscg_bv1(this.Parent.oContained,"DOCUM")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_212.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query')
    endwith
   endif
  endfunc


  add object oLinkPC_1_216 as StdButton with uid="KXPNHDAZNS",left=207, top=133, width=24,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Visualizza intestatario effettivo blacklist";
    , HelpContextID = 121170902;
    , TabStop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_216.Click()
      this.Parent.oContained.GSCG_ASA.LinkPCClick()
    endproc

  func oLinkPC_1_216.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not(.w_ANFLSOAL="S" And .w_PNTIPREG<>"N"))
    endwith
   endif
  endfunc


  add object oLinkPC_1_217 as StdButton with uid="LEPANDXOLX",left=235, top=110, width=48,height=45,;
    CpPicture="bmp\pagat.ico", caption="", nPag=1;
    , ToolTipText = "Visualizza dati operazioni superiori a 3000 euro";
    , HelpContextID = 121170902;
    , Caption='\<3.000 �', TabStop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_217.Click()
      this.Parent.oContained.GSCG_AOS.LinkPCClick()
    endproc

  func oLinkPC_1_217.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_AIFT<>'S' Or .w_PNTIPREG="N" Or .w_FLRIFE='N' Or Empty(.w_PNCODCLF) Or  .w_PNTIPDOC='NO' Or Empty(.w_PNCODCAU) or (.cFunction='Query' and Empty(.w_OSTIPORE) ))
    endwith
   endif
  endfunc


  add object oBtn_1_218 as StdButton with uid="VSSBTZDFAV",left=284, top=110, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare dati aggiuntivi";
    , HelpContextID = 219389898;
    , TabStop=.f., Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_218.Click()
      with this.Parent.oContained
        do GSCG_KPD with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_218.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLPART<>'C')
    endwith
   endif
  endfunc


  add object oObj_1_224 as cp_runprogram with uid="TSKKIWVEJH",left=303, top=685, width=233,height=18,;
    caption='GSCG_BFN',;
   bGlobalFont=.t.,;
    prg="GSCG_BFN",;
    cEvent = "Delete row start",;
    nPag=1;
    , ToolTipText = "Controlli primanota";
    , HelpContextID = 9346380


  add object oObj_1_241 as cp_runprogram with uid="WXFYHVPRWZ",left=-3, top=667, width=300,height=18,;
    caption='GSCG_BP7',;
   bGlobalFont=.t.,;
    prg="GSCG_BP7",;
    cEvent = "Record Inserted",;
    nPag=1;
    , HelpContextID = 9346403

  add object oStr_1_81 as StdString with uid="NGTIUMKUNH",Visible=.t., Left=190, Top=5,;
    Alignment=1, Width=34, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_82 as StdString with uid="OMINARSWAZ",Visible=.t., Left=8, Top=5,;
    Alignment=1, Width=59, Height=15,;
    Caption="Reg. n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_83 as StdString with uid="EAAZAALHSU",Visible=.t., Left=306, Top=5,;
    Alignment=1, Width=73, Height=15,;
    Caption="Esercizio:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_84 as StdString with uid="JETJMPQRSQ",Visible=.t., Left=134, Top=6,;
    Alignment=2, Width=8, Height=15,;
    Caption="/"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_85 as StdString with uid="NTZUMFAHTD",Visible=.t., Left=8, Top=32,;
    Alignment=1, Width=59, Height=15,;
    Caption="Causale:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_86 as StdString with uid="NPJSUDYDXU",Visible=.t., Left=-3, Top=84,;
    Alignment=1, Width=70, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_87 as StdString with uid="QEWFSVHJQS",Visible=.t., Left=331, Top=60,;
    Alignment=1, Width=26, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_88 as StdString with uid="YLENQFQYAT",Visible=.t., Left=8, Top=225,;
    Alignment=1, Width=59, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="ZFRERFVTWC",Visible=.t., Left=124, Top=225,;
    Alignment=1, Width=51, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="TKFUDCRAAF",Visible=.t., Left=285, Top=161,;
    Alignment=1, Width=92, Height=15,;
    Caption="Primanota IVA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_94 as StdString with uid="HVVROYDGOG",Visible=.t., Left=375, Top=152,;
    Alignment=1, Width=104, Height=15,;
    Caption="Competenza IVA:"  ;
  , bGlobalFont=.t.

  func oStr_1_94.mHide()
    with this.Parent.oContained
      return (.w_PNTIPREG $ " N")
    endwith
  endfunc

  add object oStr_1_96 as StdString with uid="OSKULPNSKI",Visible=.t., Left=208, Top=60,;
    Alignment=2, Width=7, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_97 as StdString with uid="SPKUTBJETZ",Visible=.t., Left=596, Top=60,;
    Alignment=2, Width=7, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_97.mHide()
    with this.Parent.oContained
      return (NOT (.w_PNFLREGI<>'S' AND NOT .w_FLPPRO $ ' N'))
    endwith
  endfunc

  add object oStr_1_101 as StdString with uid="BSVRDNPWAL",Visible=.t., Left=16, Top=60,;
    Alignment=1, Width=51, Height=15,;
    Caption="Doc.N.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_102 as StdString with uid="VMOHKEYOZS",Visible=.t., Left=434, Top=60,;
    Alignment=1, Width=45, Height=15,;
    Caption="Prot.N.:"  ;
  , bGlobalFont=.t.

  func oStr_1_102.mHide()
    with this.Parent.oContained
      return (NOT (.w_PNFLREGI<>'S' AND NOT .w_FLPPRO $ ' N'))
    endwith
  endfunc

  add object oStr_1_103 as StdString with uid="RHBKPCTXZH",Visible=.t., Left=11, Top=334,;
    Alignment=0, Width=141, Height=15,;
    Caption="Primanota contabile"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_104 as StdString with uid="AJKRLVEOAG",Visible=.t., Left=218, Top=513,;
    Alignment=1, Width=73, Height=15,;
    Caption="Sbilancio:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_114 as StdString with uid="HZMJZXKGWU",Visible=.t., Left=440, Top=5,;
    Alignment=1, Width=39, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_115 as StdString with uid="DVKUQMBXBU",Visible=.t., Left=8, Top=279,;
    Alignment=1, Width=59, Height=15,;
    Caption="Pagam.:"  ;
  , bGlobalFont=.t.

  func oStr_1_115.mHide()
    with this.Parent.oContained
      return (NOT .w_PNTIPCLF$"CF")
    endwith
  endfunc

  add object oStr_1_116 as StdString with uid="LRYINRJYFL",Visible=.t., Left=7, Top=134,;
    Alignment=1, Width=60, Height=15,;
    Caption="Cliente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_116.mHide()
    with this.Parent.oContained
      return (.w_PNTIPCLF<>'C')
    endwith
  endfunc

  add object oStr_1_117 as StdString with uid="LLOPEGLFES",Visible=.t., Left=7, Top=134,;
    Alignment=1, Width=60, Height=15,;
    Caption="Fornitore:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_117.mHide()
    with this.Parent.oContained
      return (.w_PNTIPCLF<>'F')
    endwith
  endfunc

  add object oStr_1_129 as StdString with uid="SUWXNMYFQM",Visible=.t., Left=8, Top=252,;
    Alignment=1, Width=59, Height=15,;
    Caption="Importo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_146 as StdString with uid="RWMLJEATPL",Visible=.t., Left=556, Top=152,;
    Alignment=1, Width=45, Height=18,;
    Caption="Plafond:"  ;
  , bGlobalFont=.t.

  func oStr_1_146.mHide()
    with this.Parent.oContained
      return (.w_PNTIPREG $ ' EN' AND NOT ( .w_PNTIPREG='C' AND .w_PNTIPDOC='FC'  ))
    endwith
  endfunc

  add object oStr_1_186 as StdString with uid="KZEMRNKZFV",Visible=.t., Left=479, Top=32,;
    Alignment=0, Width=80, Height=17,;
    Caption="Assestamento"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_186.mHide()
    with this.Parent.oContained
      return (.w_PNSCRASS<>'S')
    endwith
  endfunc

  add object oStr_1_207 as StdString with uid="GATGPQHGCU",Visible=.t., Left=564, Top=125,;
    Alignment=1, Width=33, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  func oStr_1_207.mHide()
    with this.Parent.oContained
      return (Not(.w_PNTIPREG<>'N' AND .w_PNTIPDOC $ 'FA-NC-FC-FE-NE-AU-NU' AND .w_PNTIPCLF $ 'C-F'))
    endwith
  endfunc

  add object oStr_1_215 as StdString with uid="EDTJWLMMNS",Visible=.t., Left=430, Top=125,;
    Alignment=1, Width=49, Height=15,;
    Caption="Mese:"  ;
  , bGlobalFont=.t.

  func oStr_1_215.mHide()
    with this.Parent.oContained
      return (Not(.w_PNTIPREG<>'N' AND .w_PNTIPDOC $ 'FA-NC-FC-FE-NE-AU-NU' AND .w_PNTIPCLF $ 'C-F'))
    endwith
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_miv",lower(this.oContained.GSCG_MIV.class))=0
        this.oContained.GSCG_MIV.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=370,;
    width=726+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=371,width=725+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CONTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oNUMCOR_2_45.Refresh()
      this.Parent.oSALDO_2_75.Refresh()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CONTI'
        oDropInto=this.oBodyCol.oRow.oPNCODCON_2_7
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_44 as StdButton with uid="WPJWLTODMZ",width=48,height=45,;
   left=738, top=352,;
    CpPicture="BMP\CONCLUSI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai dati aggiuntivi di riga";
    , HelpContextID = 235823222;
    , TabStop=.f., Caption='\<Righe';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_44.Click()
      do GSCG_KPN with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_44.mCond()
    with this.Parent.oContained
      return (.w_PNNUMRER<>0)
    endwith
  endfunc

  add object oNUMCOR_2_45 as StdTrsField with uid="FMZWCIOPPR",rtseq=144,rtrep=.t.,;
    cFormVar="w_NUMCOR",value=space(25),enabled=.f.,;
    HelpContextID = 26344234,;
    cTotal="", bFixedPos=.t., cQueryName = "NUMCOR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=80, Left=1133, Top=702, InputMask=replicate('X',25), cLinkFile="BAN_CONTI", cZoomOnZoom="GSAR_MCC", oKey_1_1="CCTIPCON", oKey_1_2="this.w_PNTIPCON", oKey_2_1="CCCODCON", oKey_2_2="this.w_PNCODCON", oKey_3_1="CCCODBAN", oKey_3_2="this.w_BANAPP", oKey_4_1="CCCONCOR", oKey_4_2="this.w_NUMCOR"

  func oNUMCOR_2_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oLinkPC_2_47 as StdButton with uid="SQKSDWWALZ",width=48,height=45,;
   left=738, top=398,;
    CpPicture="BMP\CENCOST.BMP", caption="", nPag=2;
    , ToolTipText = "Centri di costo collegati";
    , HelpContextID = 37249913;
    , TabStop=.f., Caption='A\<nalitica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_47.Click()
      this.Parent.oContained.GSCG_MCA.LinkPCClick()
    endproc

  func oLinkPC_2_47.mCond()
    with this.Parent.oContained
      return (.w_PNTIPCON="G" AND .w_SEZB $ "CR" AND .w_FLANAL="S" AND g_PERCCR="S" AND .w_CCTAGG $ "AM" and (.w_PNIMPDAR<>0 or .w_PNIMPAVE<>0 ))
    endwith
  endfunc

  func oLinkPC_2_47.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(.w_PNTIPCON="G" AND .w_SEZB $ "CR" AND .w_FLANAL="S" AND g_PERCCR="S" AND .w_CCTAGG $ "AM"))
    endwith
   endif
  endfunc

  add object oLinkPC_2_49 as StdButton with uid="UOHNGDMHPQ",width=48,height=45,;
   left=738, top=444,;
    CpPicture="BMP\SOLDIT.BMP", caption="", nPag=2;
    , ToolTipText = "Dettagli partite collegate";
    , HelpContextID = 37839094;
    , TabStop=.f., Caption='\<Partite';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_49.Click()
      this.Parent.oContained.GSCG_MPA.LinkPCClick()
    endproc

  func oLinkPC_2_49.mCond()
    with this.Parent.oContained
      return (.w_PNFLPART $ 'CSA' AND g_PERPAR="S")
    endwith
  endfunc

  func oLinkPC_2_49.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT(.w_PNFLPART $ 'CSA' AND g_PERPAR="S"))
    endwith
   endif
  endfunc

  add object oBtn_2_51 as StdButton with uid="BRPWLCNQXQ",width=48,height=45,;
   left=234, top=303,;
    CpPicture="BMP\SCHEDE.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per visualizzare la scheda contabile";
    , HelpContextID = 220085466;
    , TabStop=.f., Caption='Sc\<hede';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_51.Click()
      with this.Parent.oContained
        do GSCG_BZM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_51.mCond()
    with this.Parent.oContained
      return (.w_PNNUMRER<>0)
    endwith
  endfunc

  add object oLinkPC_2_59 as StdButton with uid="HPEKRYGXRZ",width=48,height=45,;
   left=738, top=490,;
    CpPicture="BMP\CONT-COR.BMP", caption="", nPag=2;
    , HelpContextID = 59329710;
    , TabStop=.f., Caption='M\<ovim.C\C';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_59.Click()
      this.Parent.oContained.GSCG_MMC.LinkPCClick()
    endproc

  func oLinkPC_2_59.mCond()
    with this.Parent.oContained
      return ( .w_FLMOVC='S' AND g_BANC='S' AND  .w_TIPSOT='B')
    endwith
  endfunc

  func oLinkPC_2_59.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_BANC<>'S' OR .w_FLMOVC<>'S' OR .w_TIPSOT<>'B')
    endwith
   endif
  endfunc

  add object oObj_2_61 as cp_runprogram with uid="YUHSPSJORM",width=251,height=18,;
   left=539, top=571,;
    caption='GSCG_BTK(C)',;
   bGlobalFont=.t.,;
    prg="GSCG_BTK('C')",;
    cEvent = "w_PNCODCON Changed",;
    nPag=2;
    , HelpContextID = 9160655

  add object oObj_2_65 as cp_calclbl with uid="TDQGSXBWVT",width=77,height=17,;
   left=-1, top=512,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=2;
    , HelpContextID = 237903386

  add object oSALDO_2_75 as StdTrsField with uid="PVITTXDXSN",rtseq=187,rtrep=.t.,;
    cFormVar="w_SALDO",value=0,enabled=.f.,;
    HelpContextID = 208593190,;
    cTotal="", bFixedPos=.t., cQueryName = "SALDO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=79, Top=512, cSayPict=[v_PV(20)], cGetPict=[v_GV(20)]

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTDAR_3_1 as StdField with uid="JQHUIECZCX",rtseq=152,rtrep=.f.,;
    cFormVar="w_TOTDAR",value=0,enabled=.f.,;
    ToolTipText = "Totale dare",;
    HelpContextID = 40931530,;
    cQueryName = "TOTDAR",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=425, Top=512, cSayPict=[v_PV(38+VVP)], cGetPict=[v_GV(38+VVP)]

  add object oTOTAVE_3_2 as StdField with uid="YUSBIMWCII",rtseq=153,rtrep=.f.,;
    cFormVar="w_TOTAVE",value=0,enabled=.f.,;
    ToolTipText = "Totale avere",;
    HelpContextID = 237211850,;
    cQueryName = "TOTAVE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=556, Top=512, cSayPict=[v_PV(38+VVP)], cGetPict=[v_GV(38+VVP)]

  add object oSBILAN_3_3 as StdField with uid="MLYFRJISZE",rtseq=154,rtrep=.f.,;
    cFormVar="w_SBILAN",value=0,enabled=.f.,;
    ToolTipText = "Totale sbilancio",;
    HelpContextID = 107564506,;
    cQueryName = "SBILAN",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
    FontName="Tahoma", FontSize=9, FontBold=.t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=125, Left=295, Top=513, cSayPict=[v_PV(40+VVP)], cGetPict=[v_GV(40+VVP)]
enddefine

* --- Defining Body row
define class tgscg_mpnBodyRow as CPBodyRowCnt
  Width=716
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPNTIPCON_2_1 as StdTrsField with uid="SMIIWDJBEA",rtseq=101,rtrep=.t.,;
    cFormVar="w_PNTIPCON",value=space(1),;
    ToolTipText = "Tipo conto clienti, fornitori o generico (F9 =calcola automatismi)",;
    HelpContextID = 8098236,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=27, Left=38, Top=0, cSayPict=["!"], cGetPict=["!"], InputMask=replicate('X',1)

  proc oPNTIPCON_2_1.mDefault
    with this.Parent.oContained
      if empty(.w_PNTIPCON)
        .w_PNTIPCON = 'G'
      endif
    endwith
  endproc

  proc oPNTIPCON_2_1.mAfter
    with this.Parent.oContained
      .NotifyEvent('CambiaTipo')
    endwith
  endproc

  func oPNTIPCON_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PNTIPCON$"CFG")
      if .not. empty(.w_PNCODCON)
        bRes2=.link_2_7('Full')
      endif
      if .not. empty(.w_PNCODCON)
        bRes2=.link_2_35('Full')
      endif
      if .not. empty(.w_NUMCOR)
        bRes2=.link_2_45('Full')
      endif
      if .not. empty(.w_PCODCON)
        bRes2=.link_2_74('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCPROWORD_2_2 as StdTrsField with uid="LQJHTBZNMG",rtseq=102,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Progressivo riga primanota",;
    HelpContextID = 67481750,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"], TabStop=.f.

  func oCPROWORD_2_2.mCond()
    with this.Parent.oContained
      return ((.w_PNIMPDAR<>0 OR .w_PNIMPAVE<>0 OR .w_PNFLZERO="S") AND NOT EMPTY(.w_PNCODCON))
    endwith
  endfunc

  add object oPNCODCON_2_7 as StdTrsField with uid="YWHGXINQMB",rtseq=107,rtrep=.t.,;
    cFormVar="w_PNCODCON",value=space(15),;
    ToolTipText = "Codice conto (clienti, fornitori o generico)",;
    HelpContextID = 20357564,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente o obsoleto",;
   bGlobalFont=.t.,;
    Height=17, Width=129, Left=67, Top=0, cSayPict=[p_CCF], cGetPict=[p_CCF], InputMask=replicate('X',15), bHasZoom = .t. , bNotCalczer=.f., cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_PNTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PNCODCON"

  func oPNCODCON_2_7.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Edit' OR ( vartype (I_SRV)='C' AND I_SRV='A' ) OR (.w_VARPAR='S' AND .w_VARCEN='S'))
    endwith
  endfunc

  proc oPNCODCON_2_7.mAfter
    with this.Parent.oContained
      This.bNotCalczer = LEFT(.w_PNCODCON+SPACE(15),15)=LEFT(.o_PNCODCON+SPACE(15),15)
    endwith
  endproc

  func oPNCODCON_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
      if .not. empty(.w_NUMCOR)
        bRes2=.link_2_45('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPNCODCON_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPNCODCON_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PNTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_PNTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPNCODCON_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori/conti",'',this.parent.oContained
  endproc
  proc oPNCODCON_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_PNTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PNCODCON
    i_obj.ecpSave()
  endproc

  add object oDESCON_2_8 as StdTrsField with uid="PGMERUYXYH",rtseq=108,rtrep=.t.,;
    cFormVar="w_DESCON",value=space(30),enabled=.f.,;
    HelpContextID = 93432778,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=224, Left=198, Top=0, InputMask=replicate('X',30)

  add object oPNIMPDAR_2_27 as StdTrsField with uid="PFFFVKTSSM",rtseq=127,rtrep=.t.,;
    cFormVar="w_PNIMPDAR",value=0,;
    ToolTipText = "Importo dare (F9=converte l'importo inserito in valuta in base al cambio)",;
    HelpContextID = 259539384,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=425, Top=0, cSayPict=[v_PV(38+VVP)], cGetPict=[v_GV(38+VVP)], bHasZoom = .t. 

  proc oPNIMPDAR_2_27.mDefault
    with this.Parent.oContained
      if empty(.w_PNIMPDAR)
        .w_PNIMPDAR = IIF(.w_FLAGTOT $ "TPDIA",  IIF(.w_TOTDAR>.w_TOTAVE,-ABS(.w_TOTFLAG),Abs(.w_TOTFLAG)) ,0)
      endif
    endwith
  endproc

  func oPNIMPDAR_2_27.mCond()
    with this.Parent.oContained
      return (.w_PNIMPAVE=0 AND .w_FLDAVE<>"A")
    endwith
  endfunc

  proc oPNIMPDAR_2_27.mZoom
      with this.Parent.oContained
        GSCG_BIV(this.Parent.oContained,"DARE")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPNIMPAVE_2_28 as StdTrsField with uid="NQKUYSUQRC",rtseq=128,rtrep=.t.,;
    cFormVar="w_PNIMPAVE",value=0,;
    ToolTipText = "Importo avere (F9=converte l'importo inserito in valuta in base al cambio)",;
    HelpContextID = 41435589,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=129, Left=555, Top=0, cSayPict=[v_PV(38+VVP)], cGetPict=[v_GV(38+VVP)], bHasZoom = .t. 

  proc oPNIMPAVE_2_28.mDefault
    with this.Parent.oContained
      if empty(.w_PNIMPAVE)
        .w_PNIMPAVE = IIF(.w_FLAGTOT $ "TPDIA",IIF(.w_TOTAVE>.w_TOTDAR,-ABS(.w_TOTFLAG),Abs(.w_TOTFLAG)),0)
      endif
    endwith
  endproc

  func oPNIMPAVE_2_28.mCond()
    with this.Parent.oContained
      return (.w_PNIMPDAR=0 AND .w_FLDAVE<>"D")
    endwith
  endfunc

  proc oPNIMPAVE_2_28.mZoom
      with this.Parent.oContained
        GSCG_BIV(this.Parent.oContained,"AVERE")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPNFLPART_2_39 as StdTrsField with uid="RLMXZDZIGK",rtseq=139,rtrep=.t.,;
    cFormVar="w_PNFLPART",value=space(1),;
    ToolTipText = "Riga crea o salda partita",;
    HelpContextID = 41513398,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire: C=crea partite o S=salda partite o A=acconto",;
   bGlobalFont=.t.,;
    Height=17, Width=20, Left=687, Top=0, cSayPict=['!'], cGetPict=['!'], InputMask=replicate('X',1)

  func oPNFLPART_2_39.mCond()
    with this.Parent.oContained
      return (.w_PARTSN='S' AND .w_RIGPAR $ 'CSA' AND .cFunction <> 'Edit')
    endwith
  endfunc

  proc oPNFLPART_2_39.mBefore
    with this.Parent.oContained
      IIF(.w_PNFLPART='S' AND .w_FLAPAR='S', .GSCG_MPA.LinkPCClick(), '')
    endwith
  endproc

  func oPNFLPART_2_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PNFLPART $ 'CSA' OR .w_PARTSN<>'S' OR NOT .w_RIGPAR $ 'CSA')
    endwith
    return bRes
  endfunc

  add object oCDCSAL_2_42 as StdTrsField with uid="ZKWJGXIKXI",rtseq=142,rtrep=.t.,;
    cFormVar="w_CDCSAL",value=space(1),;
    HelpContextID = 140684506,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=3, Left=708, Top=0, InputMask=replicate('X',1)

  func oCDCSAL_2_42.mCond()
    with this.Parent.oContained
      return ((EMPTY(.w_CDCSAL) AND .cFunction='Load' AND .w_PNTIPCON='G' AND .w_SEZB $ 'CR' AND .w_FLANAL $ 'SN' AND g_PERCCR='S' AND .w_CCTAGG='M' AND (.w_PNIMPDAR<>0 or .w_PNIMPAVE<>0 )) OR( g_BANC='S' AND .w_FLMOVC='S' AND .w_TIPSOT='B' ))
    endwith
  endfunc

  proc oCDCSAL_2_42.mBefore
    with this.Parent.oContained
      IIF( .w_FLMOVC='S' AND g_BANC='S' AND  .w_TIPSOT='B',.GSCG_MMC.LinkPCClick(),.GSCG_MCA.LinkPCClick())
    endwith
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oPNTIPCON_2_1.When()
    return(.t.)
  proc oPNTIPCON_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPNTIPCON_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mpn','PNT_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PNSERIAL=PNT_MAST.PNSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_mpn
* --- Classe per bottone automatismi
* --- alla perdita del focus fa scattare
* --- il lancio del batch per creare il dettaglio
* --- in base all'eventuale automatismo

Define class Btn_Mpn as StdButton

 Proc LostFocus

  with this.Parent.oContained
   If Empty(.w_GIACAR) AND .w_CHKAUT>0 And .cFunction='Load'
    This.Click()
   endif
  Endwith
  DoDefault()
 Endproc

Enddefine
* --- Fine Area Manuale
