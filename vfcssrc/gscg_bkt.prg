* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bkt                                                        *
*              Controlli finali toatalizzatore IVA                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39][VRS_4]                                               *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-06                                                      *
* Last revis.: 2011-08-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bkt",oParentObject,m.pEXEC)
return(i_retval)

define class tgscg_bkt as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_CODICE = space(15)
  w_MESS = space(100)
  w_OK = .f.
  w_GSCG_MTI = .NULL.
  w_MISURA = space(15)
  w_LCODMIS = space(15)
  w_PADRE = .NULL.
  * --- WorkFile variables
  DET_DIME_idx=0
  TOT_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue Controlli Finali al salvataggio totalizzatore IVA
    this.w_PADRE = This.Oparentobject
    this.oParentObject.w_HASEVENT = .t.
    do case
      case this.pEXEC="A"
        * --- Esegue controlli finali di coerenza delle dimensioni
        this.w_OK = .t.
        this.w_MESS = " "
        if this.oParentObject.w_TITIPTOT="S" AND this.w_PADRE.cFunction<>"Query"
          * --- Read from DET_DIME
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DET_DIME_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DET_DIME_idx,2],.t.,this.DET_DIME_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DICODICE"+;
              " from "+i_cTable+" DET_DIME where ";
                  +"DICODTOT = "+cp_ToStrODBC(this.oParentObject.w_TICODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DICODICE;
              from (i_cTable) where;
                  DICODTOT = this.oParentObject.w_TICODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODICE = NVL(cp_ToDate(_read_.DICODICE),cp_NullValue(_read_.DICODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_ROWS=0 AND !ah_YesNo("Attenzione, non � stata inserita nessuna dimensione%0Procedere comunque?")
            this.w_OK = .f.
          endif
        endif
        if this.w_OK and this.oParentObject.w_TITIPTOT="C" AND this.w_PADRE.cFunction<>"Query"
          * --- Controllo imputazione della formula
          * --- Select from GSCG_BKT
          do vq_exec with 'GSCG_BKT',this,'_Curs_GSCG_BKT','',.f.,.t.
          if used('_Curs_GSCG_BKT')
            select _Curs_GSCG_BKT
            locate for 1=1
            do while not(eof())
            this.w_CODICE = Alltrim(Nvl(_Curs_GSCG_BKT.TICODCOM," "))
            this.w_MESS = AH_MSGFORMAT("Misura: %1 senza formula, impossibile confermare",this.w_CODICE)
            this.w_OK = .f.
              select _Curs_GSCG_BKT
              continue
            enddo
            use
          endif
        endif
        if this.w_OK and this.oParentObject.w_TITIPTOT="C" 
          * --- Controllo imputazione della formula
          * --- Select from GSCG1BKT
          do vq_exec with 'GSCG1BKT',this,'_Curs_GSCG1BKT','',.f.,.t.
          if used('_Curs_GSCG1BKT')
            select _Curs_GSCG1BKT
            locate for 1=1
            do while not(eof())
            this.w_CODICE = Alltrim(Nvl(_Curs_GSCG1BKT.TICODCOM," "))
            this.w_MESS = AH_MSGFORMAT("Attenzione, misura: %1 ripetuta, impossibile confermare",this.w_CODICE)
            this.w_OK = .f.
              select _Curs_GSCG1BKT
              continue
            enddo
            use
          endif
        endif
        if this.w_OK and this.oParentObject.w_TITIPTOT<>"D" AND this.w_PADRE.cFunction<>"Query"
          * --- Controllo imputazione della formula
          * --- Read from TOT_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TOT_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TOT_DETT_idx,2],.t.,this.TOT_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" TOT_DETT where ";
                  +"TICODICE = "+cp_ToStrODBC(this.oParentObject.w_TICODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  TICODICE = this.oParentObject.w_TICODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
            if i_Rows=0
              this.w_MESS = AH_MSGFORMAT("Nessuna misura specificata, impossibile confermare")
              this.w_OK = .f.
            endif
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if this.w_OK and this.oParentObject.w_TITIPTOT<>"D" AND this.w_PADRE.cFunction="Query"
          * --- Controllo imputazione della formula
          * --- Select from GSCG2BKT
          do vq_exec with 'GSCG2BKT',this,'_Curs_GSCG2BKT','',.f.,.t.
          if used('_Curs_GSCG2BKT')
            select _Curs_GSCG2BKT
            locate for 1=1
            do while not(eof())
            this.w_CODICE = Alltrim(Nvl(_Curs_GSCG2BKT.TICODICE," "))
            this.w_MESS = AH_MSGFORMAT("Attenzione, totalizzatore utilizzato nella formula del totalizzatore %1, impossibile eliminare",this.w_CODICE)
            this.w_OK = .f.
              select _Curs_GSCG2BKT
              continue
            enddo
            use
          endif
        endif
        if Not this.w_OK
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pEXEC="B"
        * --- Azzera dimensioni e Misure al cambiare della Fonte
        this.w_GSCG_MTI = this.w_PADRE
        Select (this.w_GSCG_MTI.ctrsname)
        if reccount()<>0
          ZAP
          * --- Nuova Riga del Temporaneo
          this.w_GSCG_MTI.InitRow()     
        endif
        if Type("this.w_GSCG_MTI.GSCG_MDD.CNT")="O"
          Select (this.w_GSCG_MTI.GSCG_MDD.CNT.ctrsname)
          if Reccount()<>0
            this.oParentObject.GSCG_MDD.BlankRec()     
          endif
        endif
      case this.pEXEC="D" AND this.w_PADRE.cfunction <>"Query"
        this.w_MISURA = IIF(this.oParentObject.w_TITIPTOT="S",this.oParentObject.w_CODMIS,this.oParentObject.w_CODCOM)
        * --- Controllo imputazione della formula
        * --- Select from GSCG3BKT
        do vq_exec with 'GSCG3BKT',this,'_Curs_GSCG3BKT','',.f.,.t.
        if used('_Curs_GSCG3BKT')
          select _Curs_GSCG3BKT
          locate for 1=1
          do while not(eof())
          this.w_CODICE = Alltrim(Nvl(_Curs_GSCG3BKT.TICODICE," "))
          this.w_LCODMIS = Alltrim(Nvl(_Curs_GSCG3BKT.TICODMIS," "))
          this.w_MESS = AH_ERRORMSG("Attenzione, misura utilizzata nella formula della misura %1 del totalizzatore %2, impossibile modificare",,"",this.w_LCODMIS,this.w_CODICE)
          if UPPER(this.oParentObject.w_HASEVCOP)="ECPF6" 
            * --- Blocco l'eliminazione della riga
            this.oParentObject.w_HASEVENT = .f.
          else
            * --- Rassegno con il vecchio valore
            if this.oParentObject.w_TITIPTOT="S"
              this.oParentObject.w_TICODMIS = this.oParentObject.w_CODMIS
            else
              this.oParentObject.w_TICODCOM = this.oParentObject.w_CODCOM
            endif
          endif
          Exit
            select _Curs_GSCG3BKT
            continue
          enddo
          use
        endif
    endcase
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DET_DIME'
    this.cWorkTables[2]='TOT_DETT'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSCG_BKT')
      use in _Curs_GSCG_BKT
    endif
    if used('_Curs_GSCG1BKT')
      use in _Curs_GSCG1BKT
    endif
    if used('_Curs_GSCG2BKT')
      use in _Curs_GSCG2BKT
    endif
    if used('_Curs_GSCG3BKT')
      use in _Curs_GSCG3BKT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
