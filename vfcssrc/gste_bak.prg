* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bak                                                        *
*              Seleziona tutti -cash                                           *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-11-27                                                      *
* Last revis.: 2005-12-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bak",oParentObject)
return(i_retval)

define class tgste_bak as StdBatch
  * --- Local variables
  siono = 0
  w_ZOOM = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona o meno tutte le partite/scadenze relative al bonifico(da GSTE_SAB)
    * --- seleziona o deseleziona tutti
    this.siono = IIF(this.oParentObject.w_SELEZI="S", 1, 0)
    this.w_ZOOM = this.oParentObject.w_Zoomcc.cCursor
    * --- Aggiorno il cursore
    UPDATE ( this.w_ZOOM ) SET XCHK = this.siono
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
