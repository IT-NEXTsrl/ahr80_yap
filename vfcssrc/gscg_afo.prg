* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_afo                                                        *
*              Fonti                                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-15                                                      *
* Last revis.: 2009-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_afo"))

* --- Class definition
define class tgscg_afo as StdForm
  Top    = 10
  Left   = 9

  * --- Standard Properties
  Width  = 418
  Height = 160+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-06-18"
  HelpContextID=209050263
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  TAB_FONT_IDX = 0
  XDC_TABLE_IDX = 0
  cFile = "TAB_FONT"
  cKeySelect = "FOCODICE"
  cKeyWhere  = "FOCODICE=this.w_FOCODICE"
  cKeyWhereODBC = '"FOCODICE="+cp_ToStrODBC(this.w_FOCODICE)';

  cKeyWhereODBCqualified = '"TAB_FONT.FOCODICE="+cp_ToStrODBC(this.w_FOCODICE)';

  cPrg = "gscg_afo"
  cComment = "Fonti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_FOCODICE = space(10)
  w_FODESCRI = space(40)
  w_FO__TIPO = space(1)
  o_FO__TIPO = space(1)
  w_FO__NOME = space(254)
  w_TABELLA = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TAB_FONT','gscg_afo')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_afoPag1","gscg_afo",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Fonti")
      .Pages(1).HelpContextID = 58797654
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFOCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='XDC_TABLE'
    this.cWorkTables[2]='TAB_FONT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TAB_FONT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TAB_FONT_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_FOCODICE = NVL(FOCODICE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TAB_FONT where FOCODICE=KeySet.FOCODICE
    *
    i_nConn = i_TableProp[this.TAB_FONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TAB_FONT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TAB_FONT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TAB_FONT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'FOCODICE',this.w_FOCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_FOCODICE = NVL(FOCODICE,space(10))
        .w_FODESCRI = NVL(FODESCRI,space(40))
        .w_FO__TIPO = NVL(FO__TIPO,space(1))
        .w_FO__NOME = NVL(FO__NOME,space(254))
        .w_TABELLA = ''
          .link_1_10('Load')
        cp_LoadRecExtFlds(this,'TAB_FONT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FOCODICE = space(10)
      .w_FODESCRI = space(40)
      .w_FO__TIPO = space(1)
      .w_FO__NOME = space(254)
      .w_TABELLA = space(30)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_FO__TIPO = 'Q'
        .w_FO__NOME = ''
        .w_TABELLA = ''
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_TABELLA))
          .link_1_10('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'TAB_FONT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oFOCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oFODESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oFO__TIPO_1_3.enabled = i_bVal
      .Page1.oPag.oFO__NOME_1_6.enabled = i_bVal
      .Page1.oPag.oTABELLA_1_10.enabled = i_bVal
      .Page1.oPag.oBtn_1_4.enabled = .Page1.oPag.oBtn_1_4.mCond()
      if i_cOp = "Edit"
        .Page1.oPag.oFOCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oFOCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TAB_FONT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TAB_FONT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FOCODICE,"FOCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FODESCRI,"FODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FO__TIPO,"FO__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_FO__NOME,"FO__NOME",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TAB_FONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])
    i_lTable = "TAB_FONT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TAB_FONT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TAB_FONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TAB_FONT_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TAB_FONT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TAB_FONT')
        i_extval=cp_InsertValODBCExtFlds(this,'TAB_FONT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(FOCODICE,FODESCRI,FO__TIPO,FO__NOME "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_FOCODICE)+;
                  ","+cp_ToStrODBC(this.w_FODESCRI)+;
                  ","+cp_ToStrODBC(this.w_FO__TIPO)+;
                  ","+cp_ToStrODBC(this.w_FO__NOME)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TAB_FONT')
        i_extval=cp_InsertValVFPExtFlds(this,'TAB_FONT')
        cp_CheckDeletedKey(i_cTable,0,'FOCODICE',this.w_FOCODICE)
        INSERT INTO (i_cTable);
              (FOCODICE,FODESCRI,FO__TIPO,FO__NOME  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_FOCODICE;
                  ,this.w_FODESCRI;
                  ,this.w_FO__TIPO;
                  ,this.w_FO__NOME;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TAB_FONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TAB_FONT_IDX,i_nConn)
      *
      * update TAB_FONT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TAB_FONT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " FODESCRI="+cp_ToStrODBC(this.w_FODESCRI)+;
             ",FO__TIPO="+cp_ToStrODBC(this.w_FO__TIPO)+;
             ",FO__NOME="+cp_ToStrODBC(this.w_FO__NOME)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TAB_FONT')
        i_cWhere = cp_PKFox(i_cTable  ,'FOCODICE',this.w_FOCODICE  )
        UPDATE (i_cTable) SET;
              FODESCRI=this.w_FODESCRI;
             ,FO__TIPO=this.w_FO__TIPO;
             ,FO__NOME=this.w_FO__NOME;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TAB_FONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TAB_FONT_IDX,i_nConn)
      *
      * delete TAB_FONT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'FOCODICE',this.w_FOCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TAB_FONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_FO__TIPO<>.w_FO__TIPO
            .w_FO__NOME = ''
        endif
        if .o_FO__TIPO<>.w_FO__TIPO
            .w_TABELLA = ''
          .link_1_10('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_GDDUPACQBI()
    with this
          * --- Tabella
          .w_FO__NOME = .w_TABELLA
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFO__TIPO_1_3.enabled = this.oPgFrm.Page1.oPag.oFO__TIPO_1_3.mCond()
    this.oPgFrm.Page1.oPag.oFO__NOME_1_6.enabled = this.oPgFrm.Page1.oPag.oFO__NOME_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.visible=!this.oPgFrm.Page1.oPag.oBtn_1_4.mHide()
    this.oPgFrm.Page1.oPag.oTABELLA_1_10.visible=!this.oPgFrm.Page1.oPag.oTABELLA_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_TABELLA Changed")
          .Calculate_GDDUPACQBI()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TABELLA
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TABELLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBNAME like "+cp_ToStrODBC(trim(this.w_TABELLA)+"%");

          i_ret=cp_SQL(i_nConn,"select TBNAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',trim(this.w_TABELLA))
          select TBNAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TABELLA)==trim(_Link_.TBNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TABELLA) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBNAME',cp_AbsName(oSource.parent,'oTABELLA_1_10'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME";
                     +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1))
            select TBNAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TABELLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME";
                   +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(this.w_TABELLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_TABELLA)
            select TBNAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TABELLA = NVL(_Link_.TBNAME,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_TABELLA = space(30)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TABELLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFOCODICE_1_1.value==this.w_FOCODICE)
      this.oPgFrm.Page1.oPag.oFOCODICE_1_1.value=this.w_FOCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oFODESCRI_1_2.value==this.w_FODESCRI)
      this.oPgFrm.Page1.oPag.oFODESCRI_1_2.value=this.w_FODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oFO__TIPO_1_3.RadioValue()==this.w_FO__TIPO)
      this.oPgFrm.Page1.oPag.oFO__TIPO_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFO__NOME_1_6.value==this.w_FO__NOME)
      this.oPgFrm.Page1.oPag.oFO__NOME_1_6.value=this.w_FO__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oTABELLA_1_10.value==this.w_TABELLA)
      this.oPgFrm.Page1.oPag.oTABELLA_1_10.value=this.w_TABELLA
    endif
    cp_SetControlsValueExtFlds(this,'TAB_FONT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_FOCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFOCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_FOCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_FO__NOME))  and (.cFunction='Load' and .w_FO__TIPO='Q')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFO__NOME_1_6.SetFocus()
            i_bnoObbl = !empty(.w_FO__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FO__TIPO = this.w_FO__TIPO
    return

enddefine

* --- Define pages as container
define class tgscg_afoPag1 as StdContainer
  Width  = 414
  height = 160
  stdWidth  = 414
  stdheight = 160
  resizeXpos=369
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFOCODICE_1_1 as StdField with uid="JRQGFEYMVQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_FOCODICE", cQueryName = "FOCODICE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice fonte",;
    HelpContextID = 100049253,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=95, Left=115, Top=15, InputMask=replicate('X',10)

  add object oFODESCRI_1_2 as StdField with uid="IAKZGSZEAF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FODESCRI", cQueryName = "FODESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione fonte",;
    HelpContextID = 185635169,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=115, Top=42, InputMask=replicate('X',40)


  add object oFO__TIPO_1_3 as StdCombo with uid="ZXMICMTJJV",rtseq=3,rtrep=.f.,left=117,top=69,width=114,height=21;
    , ToolTipText = "Tipo fonte (query\tabella)";
    , HelpContextID = 82108763;
    , cFormVar="w_FO__TIPO",RowSource=""+"Query,"+"Tabella", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFO__TIPO_1_3.RadioValue()
    return(iif(this.value =1,'Q',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oFO__TIPO_1_3.GetRadio()
    this.Parent.oContained.w_FO__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oFO__TIPO_1_3.SetRadio()
    this.Parent.oContained.w_FO__TIPO=trim(this.Parent.oContained.w_FO__TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_FO__TIPO=='Q',1,;
      iif(this.Parent.oContained.w_FO__TIPO=='T',2,;
      0))
  endfunc

  func oFO__TIPO_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc


  add object oBtn_1_4 as StdButton with uid="PSACVHWWYM",left=248, top=69, width=22,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare la query";
    , HelpContextID = 209251286;
  , bGlobalFont=.t.

    proc oBtn_1_4.Click()
      with this.Parent.oContained
        .w_FO__NOME=SYS(2014, GETFILE())
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FO__TIPO='T' OR .cFunction<>'Load')
     endwith
    endif
  endfunc

  add object oFO__NOME_1_6 as StdField with uid="BZICGCECHH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FO__NOME", cQueryName = "FO__NOME",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome fisico della tabella o della query",;
    HelpContextID = 256172389,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=115, Top=94, InputMask=replicate('X',254)

  func oFO__NOME_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' and .w_FO__TIPO='Q')
    endwith
   endif
  endfunc

  add object oTABELLA_1_10 as StdField with uid="ALNTPPQDVW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TABELLA", cQueryName = "TABELLA",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Nome fisico della tabella",;
    HelpContextID = 41991882,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=114, Top=123, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="XDC_TABLE", oKey_1_1="TBNAME", oKey_1_2="this.w_TABELLA"

  func oTABELLA_1_10.mHide()
    with this.Parent.oContained
      return (.w_FO__TIPO='Q' OR .cFunction<>'Load')
    endwith
  endfunc

  func oTABELLA_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oTABELLA_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTABELLA_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'XDC_TABLE','*','TBNAME',cp_AbsName(this.parent,'oTABELLA_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oStr_1_5 as StdString with uid="TEJGIRBGLB",Visible=.t., Left=18, Top=17,;
    Alignment=1, Width=92, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="MGILSHTNOP",Visible=.t., Left=7, Top=96,;
    Alignment=1, Width=103, Height=18,;
    Caption="Nome fisico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="IGQEXFVTSF",Visible=.t., Left=19, Top=43,;
    Alignment=1, Width=91, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="YEPBUEULBF",Visible=.t., Left=46, Top=71,;
    Alignment=1, Width=69, Height=18,;
    Caption="Tipo fonte:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_afo','TAB_FONT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".FOCODICE=TAB_FONT.FOCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
