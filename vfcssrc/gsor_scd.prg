* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_scd                                                        *
*              Stampa causali ordini                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_17]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-18                                                      *
* Last revis.: 2007-07-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsor_scd",oParentObject))

* --- Class definition
define class tgsor_scd as StdForm
  Top    = 34
  Left   = 74

  * --- Standard Properties
  Width  = 515
  Height = 239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-16"
  HelpContextID=90794345
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=16

  * --- Constant Properties
  _IDX = 0
  TIP_DOCU_IDX = 0
  CAM_AGAZ_IDX = 0
  CAU_CONT_IDX = 0
  cPrg = "gsor_scd"
  cComment = "Stampa causali ordini"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_FLVEAC1 = space(1)
  o_FLVEAC1 = space(1)
  w_FLVEAC = space(10)
  w_BCODICE = space(5)
  w_ECODICE = space(5)
  w_TDCAUMAG = space(5)
  w_TDCATDOC = space(2)
  w_descri = space(35)
  w_descri1 = space(35)
  w_DESCRI2 = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CAT1 = space(2)
  w_CAT2 = space(2)
  w_FLV1 = space(1)
  w_FLV2 = space(1)
  w_TDORDAPE = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsor_scdPag1","gsor_scd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLVEAC1_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='CAU_CONT'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsor_scd
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLVEAC1=space(1)
      .w_FLVEAC=space(10)
      .w_BCODICE=space(5)
      .w_ECODICE=space(5)
      .w_TDCAUMAG=space(5)
      .w_TDCATDOC=space(2)
      .w_descri=space(35)
      .w_descri1=space(35)
      .w_DESCRI2=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CAT1=space(2)
      .w_CAT2=space(2)
      .w_FLV1=space(1)
      .w_FLV2=space(1)
      .w_TDORDAPE=space(1)
        .w_FLVEAC1 = 'A'
        .w_FLVEAC = IIF(.w_FLVEAC1='E', ' ', .w_FLVEAC1)
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_BCODICE))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_ECODICE))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_TDCAUMAG))
          .link_1_5('Full')
        endif
        .w_TDCATDOC = 'OR'
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
          .DoRTCalc(7,9,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(11,15,.f.)
        .w_TDORDAPE = IIF(.w_FLVEAC1="A",' ', .w_TDORDAPE)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_FLVEAC = IIF(.w_FLVEAC1='E', ' ', .w_FLVEAC1)
        .DoRTCalc(3,5,.t.)
            .w_TDCATDOC = 'OR'
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .DoRTCalc(7,15,.t.)
        if .o_FLVEAC1<>.w_FLVEAC1
            .w_TDORDAPE = IIF(.w_FLVEAC1="A",' ', .w_TDORDAPE)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTDORDAPE_1_24.visible=!this.oPgFrm.Page1.oPag.oTDORDAPE_1_24.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=BCODICE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOR_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_BCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_BCODICE))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BCODICE)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BCODICE) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oBCODICE_1_3'),i_cWhere,'GSOR_ATD',"Causali ordini",'GSOR_SCD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_BCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_BCODICE)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BCODICE = NVL(_Link_.TDTIPDOC,space(5))
      this.w_descri = NVL(_Link_.TDDESDOC,space(35))
      this.w_CAT2 = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLV2 = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_BCODICE = space(5)
      endif
      this.w_descri = space(35)
      this.w_CAT2 = space(2)
      this.w_FLV2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_BCODICE) OR (.w_CAT2='OR' AND (.w_FLV2=.w_FLVEAC1 OR .w_FLVEAC1='E'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_BCODICE = space(5)
        this.w_descri = space(35)
        this.w_CAT2 = space(2)
        this.w_FLV2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ECODICE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ECODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSOR_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_ECODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_ECODICE))
          select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ECODICE)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ECODICE) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oECODICE_1_4'),i_cWhere,'GSOR_ATD',"Causali ordini",'GSOR_SCD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ECODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_ECODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_ECODICE)
            select TDTIPDOC,TDDESDOC,TDCATDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ECODICE = NVL(_Link_.TDTIPDOC,space(5))
      this.w_descri1 = NVL(_Link_.TDDESDOC,space(35))
      this.w_CAT1 = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLV1 = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ECODICE = space(5)
      endif
      this.w_descri1 = space(35)
      this.w_CAT1 = space(2)
      this.w_FLV1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_ECODICE) OR (.w_CAT1='OR' AND (.w_FLV1=.w_FLVEAC1 OR .w_FLVEAC1='E'))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ECODICE = space(5)
        this.w_descri1 = space(35)
        this.w_CAT1 = space(2)
        this.w_FLV1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ECODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TDCAUMAG
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TDCAUMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACM',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_TDCAUMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_TDCAUMAG))
          select CMCODICE,CMDESCRI,CMDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TDCAUMAG)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStrODBC(trim(this.w_TDCAUMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CMDESCRI like "+cp_ToStr(trim(this.w_TDCAUMAG)+"%");

            select CMCODICE,CMDESCRI,CMDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TDCAUMAG) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oTDCAUMAG_1_5'),i_cWhere,'GSMA_ACM',"Causali magazzini",'GSOR_ATD.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TDCAUMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_TDCAUMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_TDCAUMAG)
            select CMCODICE,CMDESCRI,CMDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TDCAUMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCRI2 = NVL(_Link_.CMDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CMDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_TDCAUMAG = space(5)
      endif
      this.w_DESCRI2 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale di magazzino inesistente oppure obsoleta")
        endif
        this.w_TDCAUMAG = space(5)
        this.w_DESCRI2 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TDCAUMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLVEAC1_1_1.RadioValue()==this.w_FLVEAC1)
      this.oPgFrm.Page1.oPag.oFLVEAC1_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBCODICE_1_3.value==this.w_BCODICE)
      this.oPgFrm.Page1.oPag.oBCODICE_1_3.value=this.w_BCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oECODICE_1_4.value==this.w_ECODICE)
      this.oPgFrm.Page1.oPag.oECODICE_1_4.value=this.w_ECODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTDCAUMAG_1_5.value==this.w_TDCAUMAG)
      this.oPgFrm.Page1.oPag.oTDCAUMAG_1_5.value=this.w_TDCAUMAG
    endif
    if not(this.oPgFrm.Page1.oPag.odescri_1_7.value==this.w_descri)
      this.oPgFrm.Page1.oPag.odescri_1_7.value=this.w_descri
    endif
    if not(this.oPgFrm.Page1.oPag.odescri1_1_8.value==this.w_descri1)
      this.oPgFrm.Page1.oPag.odescri1_1_8.value=this.w_descri1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI2_1_9.value==this.w_DESCRI2)
      this.oPgFrm.Page1.oPag.oDESCRI2_1_9.value=this.w_DESCRI2
    endif
    if not(this.oPgFrm.Page1.oPag.oTDORDAPE_1_24.RadioValue()==this.w_TDORDAPE)
      this.oPgFrm.Page1.oPag.oTDORDAPE_1_24.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_BCODICE) OR (.w_CAT2='OR' AND (.w_FLV2=.w_FLVEAC1 OR .w_FLVEAC1='E')))  and not(empty(.w_BCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBCODICE_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_ECODICE) OR (.w_CAT1='OR' AND (.w_FLV1=.w_FLVEAC1 OR .w_FLVEAC1='E')))  and not(empty(.w_ECODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oECODICE_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_TDCAUMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTDCAUMAG_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale di magazzino inesistente oppure obsoleta")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLVEAC1 = this.w_FLVEAC1
    return

enddefine

* --- Define pages as container
define class tgsor_scdPag1 as StdContainer
  Width  = 511
  height = 239
  stdWidth  = 511
  stdheight = 239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oFLVEAC1_1_1 as StdCombo with uid="OODHBPQMIF",rtseq=1,rtrep=.f.,left=124,top=15,width=134,height=21;
    , ToolTipText = "Tipo causali";
    , HelpContextID = 32589654;
    , cFormVar="w_FLVEAC1",RowSource=""+"Ordini a fornitore,"+"Impegni da cliente,"+"Entrambi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLVEAC1_1_1.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'V',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oFLVEAC1_1_1.GetRadio()
    this.Parent.oContained.w_FLVEAC1 = this.RadioValue()
    return .t.
  endfunc

  func oFLVEAC1_1_1.SetRadio()
    this.Parent.oContained.w_FLVEAC1=trim(this.Parent.oContained.w_FLVEAC1)
    this.value = ;
      iif(this.Parent.oContained.w_FLVEAC1=='A',1,;
      iif(this.Parent.oContained.w_FLVEAC1=='V',2,;
      iif(this.Parent.oContained.w_FLVEAC1=='E',3,;
      0)))
  endfunc

  add object oBCODICE_1_3 as StdField with uid="ETBLNPCCXB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_BCODICE", cQueryName = "BCODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Filtro sul tipo del documento",;
    HelpContextID = 40881686,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=124, Top=48, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSOR_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_BCODICE"

  func oBCODICE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oBCODICE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBCODICE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oBCODICE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOR_ATD',"Causali ordini",'GSOR_SCD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oBCODICE_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSOR_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_BCODICE
     i_obj.ecpSave()
  endproc

  add object oECODICE_1_4 as StdField with uid="PXRALWCDVN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ECODICE", cQueryName = "ECODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Filtro sul tipo del documento",;
    HelpContextID = 40881734,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=124, Top=78, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSOR_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_ECODICE"

  func oECODICE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oECODICE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oECODICE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oECODICE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSOR_ATD',"Causali ordini",'GSOR_SCD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oECODICE_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSOR_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_ECODICE
     i_obj.ecpSave()
  endproc

  add object oTDCAUMAG_1_5 as StdField with uid="FDYLTEIKLF",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TDCAUMAG", cQueryName = "TDCAUMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale di magazzino inesistente oppure obsoleta",;
    ToolTipText = "Filtro sulla causale magazzino",;
    HelpContextID = 220991613,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=124, Top=108, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="GSMA_ACM", oKey_1_1="CMCODICE", oKey_1_2="this.w_TDCAUMAG"

  func oTDCAUMAG_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oTDCAUMAG_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTDCAUMAG_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oTDCAUMAG_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACM',"Causali magazzini",'GSOR_ATD.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oTDCAUMAG_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_TDCAUMAG
     i_obj.ecpSave()
  endproc

  add object odescri_1_7 as StdField with uid="ICVPZTXTUR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_descri", cQueryName = "descri",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 186724918,;
   bGlobalFont=.t.,;
    Height=21, Width=245, Left=191, Top=48, InputMask=replicate('X',35)

  add object odescri1_1_8 as StdField with uid="DNKYBLTMFM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_descri1", cQueryName = "descri1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 186724918,;
   bGlobalFont=.t.,;
    Height=21, Width=245, Left=191, Top=78, InputMask=replicate('X',35)

  add object oDESCRI2_1_9 as StdField with uid="LFKPIQMBXC",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCRI2", cQueryName = "DESCRI2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 150933558,;
   bGlobalFont=.t.,;
    Height=21, Width=245, Left=191, Top=108, InputMask=replicate('X',35)


  add object oObj_1_10 as cp_outputCombo with uid="ZLERXIVPWT",left=124, top=160, width=382,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 87203302


  add object oBtn_1_11 as StdButton with uid="ZZLRXFHOBI",left=408, top=190, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 50995238;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="MCKALTEAGD",left=458, top=190, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 83476922;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oTDORDAPE_1_24 as StdCheck with uid="GGFBVSPGGB",rtseq=16,rtrep=.f.,left=124, top=134, caption="Ordini aperti",;
    ToolTipText = "Se attivo: stampa solo le causali con flag ordine aperto",;
    HelpContextID = 265432965,;
    cFormVar="w_TDORDAPE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTDORDAPE_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTDORDAPE_1_24.GetRadio()
    this.Parent.oContained.w_TDORDAPE = this.RadioValue()
    return .t.
  endfunc

  func oTDORDAPE_1_24.SetRadio()
    this.Parent.oContained.w_TDORDAPE=trim(this.Parent.oContained.w_TDORDAPE)
    this.value = ;
      iif(this.Parent.oContained.w_TDORDAPE=='S',1,;
      0)
  endfunc

  func oTDORDAPE_1_24.mHide()
    with this.Parent.oContained
      return (.w_FLVEAC1="A")
    endwith
  endfunc

  add object oStr_1_13 as StdString with uid="ICXUJOZIUV",Visible=.t., Left=5, Top=160,;
    Alignment=1, Width=116, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="APNMMEACMI",Visible=.t., Left=5, Top=48,;
    Alignment=1, Width=116, Height=15,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="JODUFYNGQA",Visible=.t., Left=5, Top=78,;
    Alignment=1, Width=116, Height=15,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="MVOXEPOXYR",Visible=.t., Left=5, Top=108,;
    Alignment=1, Width=116, Height=15,;
    Caption="Causale magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="HYIQNHKYDY",Visible=.t., Left=5, Top=15,;
    Alignment=1, Width=116, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsor_scd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
