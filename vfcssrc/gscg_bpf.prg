* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bpf                                                        *
*              Calcola/stampa plafond                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_507]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-05                                                      *
* Last revis.: 2008-01-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bpf",oParentObject,m.pOper)
return(i_retval)

define class tgscg_bpf as StdBatch
  * --- Local variables
  pOper = space(1)
  w_ANNPREC = space(4)
  w_DATCON = ctod("  /  /  ")
  w_INIPLA = 0
  w_CODVAL = space(3)
  w_CAOVAL = 0
  w_VALNAZ = space(3)
  w_IMPONI = 0
  w_ESANPREC = 0
  w_CAONAZ = 0
  w_DECTOP = 0
  w_PLADIS = 0
  w_PLAUTI = 0
  w_CODAZI = space(5)
  w_ANNPREC2 = space(4)
  w_ANNOINT = 0
  w_DITOTESP = 0
  w_DIPLAUTI = 0
  w_DIPLADIS = 0
  w_APPO = 0
  w_VALPLAPREC = space(3)
  w_TMPANNO = space(4)
  w_A = 0
  w_B = 0
  w_C = 0
  w_E = 0
  w_CESSIONI = 0
  w_UTILIZZI = 0
  w_INDAZI = space(25)
  w_LOCAZI = space(35)
  w_CAPAZI = space(5)
  w_PROAZI = space(2)
  w_COFAZI = space(16)
  w_PIVAZI = space(12)
  w_PAGINE = 0
  w_DITOTESP = 0
  w_DIPLAUTI = 0
  w_DIPLADIS = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  VALUTE_idx=0
  PLA_IVAN_idx=0
  DAT_IVAN_idx=0
  ATTIDETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Stampa PLAFOND (da GSCG_SPF)
    * --- Somma cessioni 12 mesi precedenti
    * --- Utilizzo Plafond al termine del mese precedente (w_H mese prima)
    * --- Cessioni estere del 13-esimo mese espungibili
    * --- Plafond disponibile all'inizio del periodo w_A - w_D
    * --- Numerazione Pagine
    this.w_CODAZI = i_CODAZI
    this.w_ANNPREC = IIF ( this.oParentObject.w_MOBPLA="S" , alltrim(str(val(this.oParentObject.w_ANNO)-1)) ,"XXXX")
    do case
      case this.pOper="A" OR this.pOper="M"
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="S"
        * --- Elaborazione del Plafond
        if this.oParentObject.w_FLBOLL="R"
          * --- Read from PLA_IVAN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PLA_IVAN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PLA_IVAN_idx,2],.t.,this.PLA_IVAN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DICODAZI"+;
              " from "+i_cTable+" PLA_IVAN where ";
                  +"DICODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                  +" and DI__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                  +" and DIPERIOD = "+cp_ToStrODBC(this.oParentObject.w_MESE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DICODAZI;
              from (i_cTable) where;
                  DICODAZI = this.w_CODAZI;
                  and DI__ANNO = this.oParentObject.w_ANNO;
                  and DIPERIOD = this.oParentObject.w_MESE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APPO = NVL(cp_ToDate(_read_.DICODAZI),cp_NullValue(_read_.DICODAZI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows=0
            ah_ErrorMsg("Periodo inesistente o non confermato; impossibile ristampare",,"")
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Segnalo se la valuta del Plafond � diversa da quella di conto
        if g_PERVAL<>this.oParentObject.w_VALPLA
          ah_ErrorMsg("Valuta stampa plafond differente dalla valuta di conto",,"")
        endif
        ah_Msg("Fase calcolo plafond....",.T.)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        Wait Clear
        * --- Stampa del Plafond
        L_UTIPRE = 0
        if this.oParentObject.w_MOBPLA<>"S"
          * --- Select from PLA_IVAN
          i_nConn=i_TableProp[this.PLA_IVAN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PLA_IVAN_idx,2],.t.,this.PLA_IVAN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select Sum (DIPLAUTI) As Somma  from "+i_cTable+" PLA_IVAN ";
                +" where DICODAZI="+cp_ToStrODBC(this.w_CODAZI)+" AND DI__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+" AND DIPERIOD<"+cp_ToStrODBC(this.oParentObject.w_MESE)+"";
                 ,"_Curs_PLA_IVAN")
          else
            select Sum (DIPLAUTI) As Somma from (i_cTable);
             where DICODAZI=this.w_CODAZI AND DI__ANNO=this.oParentObject.w_ANNO AND DIPERIOD<this.oParentObject.w_MESE;
              into cursor _Curs_PLA_IVAN
          endif
          if used('_Curs_PLA_IVAN')
            select _Curs_PLA_IVAN
            locate for 1=1
            do while not(eof())
            L_UTIPRE = Nvl( _Curs_PLA_IVAN.Somma , 0 ) 
              select _Curs_PLA_IVAN
              continue
            enddo
            use
          endif
          * --- Lettura Plafond a Inizio Anno
          * --- Read from DAT_IVAN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2],.t.,this.DAT_IVAN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IAPLAINI"+;
              " from "+i_cTable+" DAT_IVAN where ";
                  +"IACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                  +" and IA__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IAPLAINI;
              from (i_cTable) where;
                  IACODAZI = this.w_CODAZI;
                  and IA__ANNO = this.oParentObject.w_ANNO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_INIPLA = NVL(cp_ToDate(_read_.IAPLAINI),cp_NullValue(_read_.IAPLAINI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_E = this.w_INIPLA - L_UTIPRE
        endif
        L_UTILIZZI = this.w_UTILIZZI
        L_CESSIONI = this.w_CESSIONI
        L_PLAMOB = this.oParentObject.w_MOBPLA
        L_PLAINI = this.oParentObject.w_PLARES
        L_PLARES = this.w_E
        L_MESE = this.oParentObject.w_MESE
        L_ANNO = this.oParentObject.w_ANNO
        L_DESCRI = this.oParentObject.w_DESCRI
        L_DECTOT = this.oParentObject.w_DECTOT
        L_VALPLA = IIF(EMPTY(this.oParentObject.w_SIMVAL), this.oParentObject.w_VALPLA, this.oParentObject.w_SIMVAL)
        * --- PER NUMERAZIONE PAGINE IN TESTATA
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI;
            from (i_cTable) where;
                AZCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDAZI = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
          this.w_LOCAZI = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
          this.w_CAPAZI = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
          this.w_PROAZI = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
          this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
          this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        L_PAGINE=0
        L_PRPARI=this.oParentObject.w_PRPARI
        L_INTLIG=this.oParentObject.w_INTLIG
        L_PREFIS=this.oParentObject.w_PREFIS
        L_INDAZI=this.w_INDAZI
        L_LOCAZI=this.w_LOCAZI
        L_CAPAZI=this.w_CAPAZI
        L_PROAZI=this.w_PROAZI
        L_COFAZI=this.w_COFAZI
        L_PIVAZI=this.w_PIVAZI
        this.w_PAGINE = 0
        SELECT __TMP__
        if RECCOUNT()=0 
 
          * --- Stampa almeno il Plafond iniziale
          CREATE CURSOR __TMP__ (DICODAZI C(5), DI__ANNO C(4), DIPERIOD N(2,0), DITOTESP N(18,4), DIPLAUTI N(18,4),DIPLADIS N(18,4))
          INSERT INTO __TMP__ (DICODAZI, DI__ANNO, DIPERIOD, DITOTESP, DIPLAUTI,DIPLADIS) VALUES (this.w_CODAZI, this.oParentObject.w_ANNO, this.oParentObject.w_MESE, 0, 0 ,0)
        endif
        * --- Calcolo il plafond residuo se plafond mobile
        L_PLARES = this.w_E
        if this.oParentObject.w_TESTO="S"
          PUBLIC STPAG
          STPAG = this.oParentObject.w_PRPARI
          Cp_ChPrn("PLAF_V.FXP","S", this)
          this.w_PAGINE = STPAG
        else
          Cp_ChPrn("QUERY\GSCG_SPF.FRX", " ", this)
          this.w_PAGINE = L_PAGINE+this.oParentObject.w_PRPARI
        endif
        this.w_PAGINE = IIF(this.w_PAGINE=0,this.oParentObject.w_PRPARI,this.w_PAGINE)
        if USED("__TMP__")
          SELECT __TMP__
          USE
        endif
        if this.oParentObject.w_FLBOLL="B"
          this.oParentObject.w_PRPARI = this.w_PAGINE
          * --- Non serve la transazione perch� ho solo una operazione sul database,
          *     se va male si auto annulla (rollback)
          * --- Try
          local bErr_03D2F000
          bErr_03D2F000=bTrsErr
          this.Try_03D2F000()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            ah_ErrorMsg("Impossibile registrare i dati plafond del periodo.%0Verificare che non sia gi� stato inserito in precedenza.",,"")
            * --- Rimetto i dati della maschera a posto
            this.Pag4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_03D2F000
          * --- End
        endif
    endcase
  endproc
  proc Try_03D2F000()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PLA_IVAN
    i_nConn=i_TableProp[this.PLA_IVAN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PLA_IVAN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PLA_IVAN_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DICODAZI"+",DI__ANNO"+",DIPERIOD"+",DITOTESP"+",DIPLAUTI"+",DIPLADIS"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODAZI),'PLA_IVAN','DICODAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANNO),'PLA_IVAN','DI__ANNO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MESE),'PLA_IVAN','DIPERIOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CESSIONI),'PLA_IVAN','DITOTESP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTILIZZI),'PLA_IVAN','DIPLAUTI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_E),'PLA_IVAN','DIPLADIS');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DICODAZI',this.w_CODAZI,'DI__ANNO',this.oParentObject.w_ANNO,'DIPERIOD',this.oParentObject.w_MESE,'DITOTESP',this.w_CESSIONI,'DIPLAUTI',this.w_UTILIZZI,'DIPLADIS',this.w_E)
      insert into (i_cTable) (DICODAZI,DI__ANNO,DIPERIOD,DITOTESP,DIPLAUTI,DIPLADIS &i_ccchkf. );
         values (;
           this.w_CODAZI;
           ,this.oParentObject.w_ANNO;
           ,this.oParentObject.w_MESE;
           ,this.w_CESSIONI;
           ,this.w_UTILIZZI;
           ,this.w_E;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.oParentObject.w_FLBOLL = "S"
    this.oParentObject.w_MESE = this.oParentObject.w_MESE + 1
    if this.oParentObject.w_MESE=13
      * --- Passo all'anno successivo
      this.oParentObject.w_MESE = 1
      this.oParentObject.w_ANNO = Str(Val(this.oParentObject.w_ANNO) + 1, 4, 0)
    endif
    this.oParentObject.w_DESCRI = g_MESE[this.oParentObject.w_MESE]
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    ah_ErrorMsg("Elaborazione periodo plafond completata.",,"")
    if this.oParentObject.w_INTLIG="S"
      * --- inserisco il progressivo pagina
      * --- Write into ATTIDETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATPRPARI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PRPARI),'ATTIDETT','ATPRPARI');
            +i_ccchkf ;
        +" where ";
            +"ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
            +" and ATNUMREG = "+cp_ToStrODBC(this.oParentObject.w_NUMREG);
            +" and ATTIPREG = "+cp_ToStrODBC(this.oParentObject.w_TIPREG);
               )
      else
        update (i_cTable) set;
            ATPRPARI = this.oParentObject.w_PRPARI;
            &i_ccchkf. ;
         where;
            ATCODATT = this.oParentObject.w_CODATT;
            and ATNUMREG = this.oParentObject.w_NUMREG;
            and ATTIPREG = this.oParentObject.w_TIPREG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola il Plafond
    this.w_CESSIONI = 0
    this.w_UTILIZZI = 0
    * --- Se plafond fisso non leggo nessun dato dall'anno precedente
    this.w_ANNOINT = Val(this.oParentObject.w_ANNO)
    * --- Inizio Elaborazione
    * --- Calcola gli Utilizzi Plafond
    if this.oParentObject.w_PLNODO="S"
      * --- Se check Attivo eseguo solo l'ultima query della catena che preleva i dati dalla prima nota
      VQ_EXEC("QUERY\GSCG6KPF.VQR",this,"DOCPNT")
    else
      VQ_EXEC("QUERY\GSCG_KPF.VQR",this,"DOCPNT")
    endif
    ah_Msg("Lettura documenti di utilizzo plafond",.T.)
    if USED("DOCPNT")
      * --- Cicla Sul Cursore della Query e Calcola i valori per il Temporaneo di Elaborazione
      SELECT DOCPNT
      GO TOP
      do while Not Eof()
        this.w_DATCON = CP_TODATE(DATCON)
        this.w_CODVAL = NVL(CODVAL, g_PERVAL)
        this.w_CAOVAL = NVL(CAOVAL, GETCAM(G_PERVAL,i_DATSYS))
        this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
        this.w_IMPONI = IMPONI
        if this.w_IMPONI<>0
          * --- Anno e Mese di Competenza x Plafond
          this.w_CAONAZ = GETCAM(g_PERVAL, i_DATSYS)
          this.w_DECTOP = g_PERPVL
          * --- Se riga proveniente dai documenti allora potrei doverla convertire
          if ORDINE="D"
            if this.w_CODVAL=g_PERVAL
              * --- Se la Valuta Documento e' gia' Uguale alla Valuta Esercizio (Attuale) 
              *     considera come se il documento fosse inserito nell'esercizio attuale con la stessa valuta (per evitare doppia conversione)
              this.w_VALNAZ = g_PERVAL
            else
              if this.w_CODVAL<>this.w_VALNAZ
                if this.w_VALNAZ<>g_PERVAL
                  this.w_CAONAZ = GETCAM(this.w_VALNAZ, I_DATSYS)
                  this.w_DECTOP = GETVALUT(this.w_VALNAZ,"VADECTOT")
                endif
                * --- Se arriva un Importo in Valuta e il Documento e' in Valuta Converte in Moneta di Conto
                this.w_IMPONI = VAL2MON(this.w_IMPONI,this.w_CAOVAL,this.w_CAONAZ,this.w_DATCON,this.w_VALNAZ, this.w_DECTOP)
              endif
            endif
          endif
          * --- Se Valuta plafond Diversa dalla Valuta di Conto
          if this.w_VALNAZ<>this.oParentObject.w_VALPLA AND GETCAM(g_PERVAL,i_DATSYS)<>0
            * --- Prevediamo solo LIRE o EURO
            this.w_IMPONI = VAL2MON(this.w_IMPONI,this.w_CAOVAL,this.w_CAONAZ,this.w_DATCON,this.w_VALNAZ, this.w_DECTOP)
          endif
          this.w_UTILIZZI = this.w_UTILIZZI + this.w_IMPONI
        endif
        SELECT DOCPNT
        if Not Eof()
          Skip
        endif
      enddo
    endif
    * --- Dettaglio di stampa (la eseguo qua per avere a disposizione il totale dei 12 periorid precedenti utilizzato per il calcolo del plafond mobile)
    VQ_EXEC("QUERY\GSCG_SPF.VQR",this,"__TMP__")
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.oParentObject.w_MOBPLA="S"
      * --- Calcola le Esportazioni in esenzione Plafond
      ah_Msg("Lettura documenti di esportazione",.T.)
      VQ_EXEC("QUERY\GSCG_KPE.VQR",this,"DOCPNT")
      if USED("DOCPNT")
        * --- Cicla Sul Cursore della Query e Calcola i valori per il Temporaneo di Elaborazione (solo Primanota)
        SELECT DOCPNT
        GO TOP
        do while Not Eof()
          this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
          this.w_IMPONI = IMPONI
          * --- Se Valuta plafond Diversa dalla Valuta di Conto
          if this.w_VALNAZ<>this.oParentObject.w_VALPLA AND GETCAM(g_PERVAL,I_DATSYS)<>0
            * --- Prevediamo solo LIRE o EURO
            this.w_IMPONI = VAL2MON(this.w_IMPONI,this.w_CAOVAL,this.w_CAONAZ,this.w_DATCON,this.w_VALNAZ, this.w_DECTOP)
          endif
          this.w_CESSIONI = this.w_CESSIONI + this.w_IMPONI
          SELECT DOCPNT
          if Not Eof()
            Skip
          endif
        enddo
      endif
      * --- Calcolo utilizzo totale del plafond a fine mese
      * --- Somma cessioni 12 mesi precedenti
      Select __TMP__ 
      Calculate Sum ( DITOTESP ) To this.w_A 
      if this.oParentObject.w_MESE<>1
        * --- Cessioni estere del 13-esimo mese espungibili
        * --- Read from PLA_IVAN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PLA_IVAN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PLA_IVAN_idx,2],.t.,this.PLA_IVAN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DITOTESP"+;
            " from "+i_cTable+" PLA_IVAN where ";
                +"DICODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and DI__ANNO = "+cp_ToStrODBC(this.w_ANNPREC);
                +" and DIPERIOD = "+cp_ToStrODBC(this.oParentObject.w_MESE - 1);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DITOTESP;
            from (i_cTable) where;
                DICODAZI = this.w_CODAZI;
                and DI__ANNO = this.w_ANNPREC;
                and DIPERIOD = this.oParentObject.w_MESE - 1;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_C = NVL(cp_ToDate(_read_.DITOTESP),cp_NullValue(_read_.DITOTESP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Cessioni estere del 13-esimo mese espungibili
        this.w_ANNPREC2 = alltrim(str(val(this.w_ANNPREC)-1))
        * --- Read from PLA_IVAN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PLA_IVAN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PLA_IVAN_idx,2],.t.,this.PLA_IVAN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DITOTESP"+;
            " from "+i_cTable+" PLA_IVAN where ";
                +"DICODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and DI__ANNO = "+cp_ToStrODBC(this.w_ANNPREC2);
                +" and DIPERIOD = "+cp_ToStrODBC(12);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DITOTESP;
            from (i_cTable) where;
                DICODAZI = this.w_CODAZI;
                and DI__ANNO = this.w_ANNPREC2;
                and DIPERIOD = 12;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_C = NVL(cp_ToDate(_read_.DITOTESP),cp_NullValue(_read_.DITOTESP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Verifico se devo tradurre l'importo
      this.w_TMPANNO = IIF ( this.oParentObject.w_MESE<>1 , this.w_ANNPREC , this.w_ANNPREC2 )
      * --- Read from DAT_IVAN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2],.t.,this.DAT_IVAN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IAVALPLA"+;
          " from "+i_cTable+" DAT_IVAN where ";
              +"IACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
              +" and IA__ANNO = "+cp_ToStrODBC(this.w_TMPANNO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IAVALPLA;
          from (i_cTable) where;
              IACODAZI = this.w_CODAZI;
              and IA__ANNO = this.w_TMPANNO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_VALPLAPREC = NVL(cp_ToDate(_read_.IAVALPLA),cp_NullValue(_read_.IAVALPLA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_VALPLAPREC <> this.oParentObject.w_VALPLA and GETCAM(g_PERVAL, i_DATSYS)<>0
        * --- Prevediamo solo LIRE o EURO
        do case
          case this.w_VALPLAPREC=g_CODLIR
            this.w_C = VAL2MON(this.w_C,this.w_CAOVAL,this.w_CAONAZ,this.w_DATCON,this.w_VALNAZ, this.w_DECTOP)
          case this.w_VALPLAPREC=g_CODEUR
            this.w_C = VAL2MON(this.w_C,this.w_CAOVAL,this.w_CAONAZ,this.w_DATCON,this.w_VALNAZ, this.w_DECTOP)
        endcase
      endif
      * --- Utilizzo Plafond al termine del mese precedente (w_H mese prima)
      *     Lo ricavo dal Plafond disponibile inizio mese memorizzato con la formala:
      *     w_B = Somma cessioni periodo precedente (w_A PERIODO PRECEDENTE) -
      *     Plafond disponibile inizio mese +
      *     Utilizzi mese precedente
      Select __TMP__ 
      Go Bottom
      * --- Calcolo la somma delle cessioni periodo precedente, dal totale tolgo l'ultima cessione ed aggiungo l'ex dodicesima ora tredicesima (w_C)
      * --- - plafond disponibile inizio mese + Utilizzi mese precedente
      this.w_B = this.w_A - DITOTESP + this.w_C- Nvl ( DIPLADIS , 0 ) + Nvl ( DIPLAUTI , 0 )
      * --- Plafond disponibile all'inizio del periodo w_A - (w_B - w_C)
      this.w_E = this.w_A - IIF( this.w_B - this.w_C >0 , this.w_B - this.w_C , 0 )
    endif
    if USED("DOCPNT")
      SELECT DOCPNT
      USE
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo il plafond disponibile ad inizio periodo
    if this.oParentObject.w_MESE = 1
      if this.oParentObject.w_MOBPLA="S"
        * --- Leggo il 12 periodo dell'anno precedente
        * --- Read from PLA_IVAN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PLA_IVAN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PLA_IVAN_idx,2],.t.,this.PLA_IVAN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DIPLADIS"+;
            " from "+i_cTable+" PLA_IVAN where ";
                +"DICODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and DI__ANNO = "+cp_ToStrODBC(this.w_ANNPREC);
                +" and DIPERIOD = "+cp_ToStrODBC(12);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DIPLADIS;
            from (i_cTable) where;
                DICODAZI = this.w_CODAZI;
                and DI__ANNO = this.w_ANNPREC;
                and DIPERIOD = 12;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_PLARES = NVL(cp_ToDate(_read_.DIPLADIS),cp_NullValue(_read_.DIPLADIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Leggo i Decimali (all'Init w_DECTOT non � ancora valorizzata)
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALPLA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.oParentObject.w_VALPLA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Leggo il dato impostato dall'utente nei Dati Iva
        * --- Read from DAT_IVAN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DAT_IVAN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DAT_IVAN_idx,2],.t.,this.DAT_IVAN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IAPLAINI"+;
            " from "+i_cTable+" DAT_IVAN where ";
                +"IACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and IA__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IAPLAINI;
            from (i_cTable) where;
                IACODAZI = this.w_CODAZI;
                and IA__ANNO = this.oParentObject.w_ANNO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_PLARES = NVL(cp_ToDate(_read_.IAPLAINI),cp_NullValue(_read_.IAPLAINI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    else
      * --- Lo leggo direttamente dal periodo precedente 
      this.w_APPO = 0
      this.oParentObject.w_PLARES = 0
      * --- Read from PLA_IVAN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PLA_IVAN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PLA_IVAN_idx,2],.t.,this.PLA_IVAN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DIPLADIS,DIPLAUTI"+;
          " from "+i_cTable+" PLA_IVAN where ";
              +"DICODAZI = "+cp_ToStrODBC(this.w_CODAZI);
              +" and DI__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
              +" and DIPERIOD = "+cp_ToStrODBC(this.oParentObject.w_MESE - 1);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DIPLADIS,DIPLAUTI;
          from (i_cTable) where;
              DICODAZI = this.w_CODAZI;
              and DI__ANNO = this.oParentObject.w_ANNO;
              and DIPERIOD = this.oParentObject.w_MESE - 1;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_PLARES = NVL(cp_ToDate(_read_.DIPLADIS),cp_NullValue(_read_.DIPLADIS))
        this.w_APPO = NVL(cp_ToDate(_read_.DIPLAUTI),cp_NullValue(_read_.DIPLAUTI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Sottrae gli Utilizzi
      this.oParentObject.w_PLARES = this.oParentObject.w_PLARES - this.w_APPO
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo il prossimo mese da stampare
    if this.pOper<>"M"
      this.oParentObject.w_MESE = 0
      * --- Select from PLA_IVAN
      i_nConn=i_TableProp[this.PLA_IVAN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PLA_IVAN_idx,2],.t.,this.PLA_IVAN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Max ( DIPERIOD ) As Massimo  from "+i_cTable+" PLA_IVAN ";
            +" where DICODAZI = "+cp_ToStrODBC(this.w_CODAZI)+" AND DI__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO)+" ";
             ,"_Curs_PLA_IVAN")
      else
        select Max ( DIPERIOD ) As Massimo from (i_cTable);
         where DICODAZI = this.w_CODAZI AND DI__ANNO = this.oParentObject.w_ANNO ;
          into cursor _Curs_PLA_IVAN
      endif
      if used('_Curs_PLA_IVAN')
        select _Curs_PLA_IVAN
        locate for 1=1
        do while not(eof())
        this.oParentObject.w_MESE = Nvl ( _Curs_PLA_IVAN.MASSIMO , 0 )
          select _Curs_PLA_IVAN
          continue
        enddo
        use
      endif
      this.oParentObject.w_MESE = Nvl ( this.oParentObject.w_MESE , 0 ) + 1
      if this.oParentObject.w_MESE = 13
        this.oParentObject.w_DESCRI = "Anno gi� stampato completamente"
        this.oParentObject.w_MESE = 0
        this.oParentObject.w_PLARES = 0
        ah_ErrorMsg("L'anno selezionato � gia stato completamente stampato",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Imposto la descrizione
    if this.oParentObject.w_MESE>0 AND this.oParentObject.w_MESE<13
      this.oParentObject.w_DESCRI = g_MESE[this.oParentObject.w_MESE]
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converto gli importi nella valuta del plafond dell'anno in corso
    Select __Tmp__
    do while Not Eof()
      if IAVALPLA <> this.oParentObject.w_VALPLA And GETCAM(g_PERVAL, i_DATSYS)<>0
        this.w_DITOTESP = Nvl ( DITOTESP , 0 )
        this.w_DIPLAUTI = Nvl ( DIPLAUTI , 0 )
        this.w_DIPLADIS = Nvl ( DIPLADIS , 0 )
        * --- Prevediamo solo LIRE o EURO
        this.w_DITOTESP = VAL2MON(this.w_DITOTESP,this.w_CAOVAL,this.w_CAONAZ,this.w_DATCON,IAVALPLA, this.w_DECTOP)
        this.w_DIPLAUTI = VAL2MON(this.w_DIPLAUTI,this.w_CAOVAL,this.w_CAONAZ,this.w_DATCON,IAVALPLA, this.w_DECTOP)
        this.w_DIPLADIS = VAL2MON(this.w_DIPLADIS,this.w_CAOVAL,this.w_CAONAZ,this.w_DATCON,IAVALPLA, this.w_DECTOP)
        Select __Tmp__
         
 Replace DITOTESP With this.w_DITOTESP 
 Replace DIPLAUTI With this.w_DIPLAUTI 
 Replace DIPLADIS With this.w_DIPLADIS
      endif
      if Not Eof()
        Skip
      endif
    enddo
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='PLA_IVAN'
    this.cWorkTables[4]='DAT_IVAN'
    this.cWorkTables[5]='ATTIDETT'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_PLA_IVAN')
      use in _Curs_PLA_IVAN
    endif
    if used('_Curs_PLA_IVAN')
      use in _Curs_PLA_IVAN
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
