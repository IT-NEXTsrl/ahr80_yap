* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_klg                                                        *
*              Opzioni avanzate report                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_10]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-05                                                      *
* Last revis.: 2013-04-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_klg",oParentObject))

* --- Class definition
define class tgsar_klg as StdForm
  Top    = 43
  Left   = 48

  * --- Standard Properties
  Width  = 536
  Height = 148
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-04-17"
  HelpContextID=216627049
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsar_klg"
  cComment = "Opzioni avanzate report"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_LGTIPREP = space(1)
  w_LG_SPLIT = space(254)
  w_LG__LINK = space(254)
  w_LGNO_RST = space(1)
  w_LGFLPRSY = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_klgPag1","gsar_klg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLG_SPLIT_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LGTIPREP=space(1)
      .w_LG_SPLIT=space(254)
      .w_LG__LINK=space(254)
      .w_LGNO_RST=space(1)
      .w_LGFLPRSY=space(1)
      .w_LGTIPREP=oParentObject.w_LGTIPREP
      .w_LG_SPLIT=oParentObject.w_LG_SPLIT
      .w_LG__LINK=oParentObject.w_LG__LINK
      .w_LGNO_RST=oParentObject.w_LGNO_RST
      .w_LGFLPRSY=oParentObject.w_LGFLPRSY
    endwith
    this.DoRTCalc(1,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oLG_SPLIT_1_2.enabled = i_bVal
      .Page1.oPag.oLG__LINK_1_4.enabled = i_bVal
      .Page1.oPag.oLGNO_RST_1_5.enabled = i_bVal
      .Page1.oPag.oLGFLPRSY_1_7.enabled = i_bVal
      .Page1.oPag.oBtn_1_8.enabled = .Page1.oPag.oBtn_1_8.mCond()
      .Page1.oPag.oBtn_1_9.enabled = .Page1.oPag.oBtn_1_9.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_LGTIPREP=.w_LGTIPREP
      .oParentObject.w_LG_SPLIT=.w_LG_SPLIT
      .oParentObject.w_LG__LINK=.w_LG__LINK
      .oParentObject.w_LGNO_RST=.w_LGNO_RST
      .oParentObject.w_LGFLPRSY=.w_LGFLPRSY
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLG_SPLIT_1_2.enabled = this.oPgFrm.Page1.oPag.oLG_SPLIT_1_2.mCond()
    this.oPgFrm.Page1.oPag.oLG__LINK_1_4.enabled = this.oPgFrm.Page1.oPag.oLG__LINK_1_4.mCond()
    this.oPgFrm.Page1.oPag.oLGNO_RST_1_5.enabled = this.oPgFrm.Page1.oPag.oLGNO_RST_1_5.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLG_SPLIT_1_2.value==this.w_LG_SPLIT)
      this.oPgFrm.Page1.oPag.oLG_SPLIT_1_2.value=this.w_LG_SPLIT
    endif
    if not(this.oPgFrm.Page1.oPag.oLG__LINK_1_4.value==this.w_LG__LINK)
      this.oPgFrm.Page1.oPag.oLG__LINK_1_4.value=this.w_LG__LINK
    endif
    if not(this.oPgFrm.Page1.oPag.oLGNO_RST_1_5.RadioValue()==this.w_LGNO_RST)
      this.oPgFrm.Page1.oPag.oLGNO_RST_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLGFLPRSY_1_7.RadioValue()==this.w_LGFLPRSY)
      this.oPgFrm.Page1.oPag.oLGFLPRSY_1_7.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_klgPag1 as StdContainer
  Width  = 532
  height = 148
  stdWidth  = 532
  stdheight = 148
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLG_SPLIT_1_2 as StdField with uid="QQYUVZMRHK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LG_SPLIT", cQueryName = "LG_SPLIT",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Espressione di rottura per la fascicolazione dei report principali e secondari/produzione",;
    HelpContextID = 194001654,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=168, Top=17, InputMask=replicate('X',254)

  func oLG_SPLIT_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.oParentObject.cFunction<>'Query')
    endwith
   endif
  endfunc

  add object oLG__LINK_1_4 as StdField with uid="VDLNFXIKCZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LG__LINK", cQueryName = "LG__LINK",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Espressione di collegamento tra i dati del report secondario/produzione con il report principale",;
    HelpContextID = 20694273,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=168, Top=45, InputMask=replicate('X',254)

  func oLG__LINK_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_LGTIPREP<>'M' AND .oParentObject.cFunction<>'Query')
    endwith
   endif
  endfunc

  add object oLGNO_RST_1_5 as StdCheck with uid="CWNRGKVMAH",rtseq=4,rtrep=.f.,left=168, top=75, caption="Non azzerare il numero pagina",;
    ToolTipText = "Se attivo: non resetta il contatore delle pagine al momento del passaggio al nuovo report",;
    HelpContextID = 190493962,;
    cFormVar="w_LGNO_RST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLGNO_RST_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oLGNO_RST_1_5.GetRadio()
    this.Parent.oContained.w_LGNO_RST = this.RadioValue()
    return .t.
  endfunc

  func oLGNO_RST_1_5.SetRadio()
    this.Parent.oContained.w_LGNO_RST=trim(this.Parent.oContained.w_LGNO_RST)
    this.value = ;
      iif(this.Parent.oContained.w_LGNO_RST=='S',1,;
      0)
  endfunc

  func oLGNO_RST_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.oParentObject.cFunction<>'Query')
    endwith
   endif
  endfunc

  add object oLGFLPRSY_1_7 as StdCheck with uid="GQYIVKVVYB",rtseq=5,rtrep=.f.,left=168, top=108, caption="Cambia flusso di stampa",;
    ToolTipText = "Se abilitato apre una nuova print system per stampare il report",;
    HelpContextID = 174535951,;
    cFormVar="w_LGFLPRSY", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLGFLPRSY_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oLGFLPRSY_1_7.GetRadio()
    this.Parent.oContained.w_LGFLPRSY = this.RadioValue()
    return .t.
  endfunc

  func oLGFLPRSY_1_7.SetRadio()
    this.Parent.oContained.w_LGFLPRSY=trim(this.Parent.oContained.w_LGFLPRSY)
    this.value = ;
      iif(this.Parent.oContained.w_LGFLPRSY=='S',1,;
      0)
  endfunc


  add object oBtn_1_8 as StdButton with uid="AVEBAEPVRV",left=427, top=99, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 216598298;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="UGHEMFTEUT",left=477, top=99, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 209309626;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="OGEJQYBPPJ",Visible=.t., Left=11, Top=17,;
    Alignment=1, Width=155, Height=18,;
    Caption="Espressione di rottura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="FFIFTTYKNR",Visible=.t., Left=11, Top=45,;
    Alignment=1, Width=155, Height=18,;
    Caption="Espressione di link:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_klg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
