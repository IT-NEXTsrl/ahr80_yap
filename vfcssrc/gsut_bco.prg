* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bco                                                        *
*              Carica salva dati esterni                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_231]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2017-04-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bco",oParentObject,m.pTIPO)
return(i_retval)

define class tgsut_bco as StdBatch
  * --- Local variables
  pTIPO = space(10)
  w_MESS = space(50)
  w_CURSORE = 0
  w_TRICODI = space(5)
  w_TRIDES = space(200)
  w_TRITIP = space(10)
  w_PADRE = .NULL.
  w_NOMSG = .f.
  w_MSGERROR = space(1)
  w_TRMERIF = space(1)
  w_TRIFCODA = space(1)
  OldPoint = space(1)
  w_CANC_RAGGR = .f.
  w_DATRASCO = space(4)
  w_CAMPO = space(20)
  w_CAMPO1 = space(254)
  w_NATURA = space(1)
  w_FILTERCURSOR = space(10)
  w_ERRNUM = 0
  w_ERRDESCRI = space(250)
  w_ERRMESS = space(254)
  w_ERRNOTE = space(0)
  w_OUTSTR = space(0)
  w_CODABI = space(5)
  w_DESABI = space(80)
  w_CODCAB = space(5)
  w_DESFIL = space(40)
  w_INDIRI = space(50)
  w_CAP = space(9)
  w_NMCODI = space(8)
  w_NMDESC = space(51)
  w_NMUMSU = space(3)
  w_UMCODI = space(3)
  w_UMDESC = space(35)
  w_UMFRAZ = space(1)
  w_RECODI = space(4)
  w_REDES = space(30)
  w_AECODI = space(5)
  w_AEDES = space(30)
  w_CSCAU = space(5)
  w_CSDES = space(60)
  w_CSPINI = space(40)
  w_CSPFIN = space(40)
  w_CAENTE = space(5)
  w_CACAU = space(5)
  w_CADES = space(40)
  w_CAINI = space(30)
  w_CAFIN = space(30)
  w_CODICE = space(5)
  w_NOME = space(30)
  w_INDIRI = space(35)
  w_CAP = space(5)
  w_LOCAL = space(30)
  w_PROV = space(2)
  w_CODCAP = space(9)
  w_CODLOC = space(10)
  w_LOCALITA = space(50)
  w_FRAZIONE = space(50)
  w_INDIRIZZ = space(154)
  w_PROVINCIA = space(2)
  w_DESPROV = space(30)
  w_CCODFIS = space(4)
  w_ELIMINA = .f.
  w_FLUFFGIU = space(1)
  w_APPOCOD = 0
  w_NEWPROG = 0
  w_RECODICE = space(15)
  w_REDESCRI = space(45)
  w_RETIPMOD = space(2)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctod("  /  /  ")
  w_UTDV = ctod("  /  /  ")
  w_REDTINVA = ctod("  /  /  ")
  w_REDTOBSO = ctod("  /  /  ")
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_RETIPSTE = space(2)
  w_REPARAME = space(1)
  w_REDESOPE = space(100)
  w_RENUMCUR = space(8)
  w_TIPDEST = space(1)
  w_RECODICEP = space(15)
  w_ERRORCAR = .f.
  w_ATDAFILT = space(25)
  w_ATMULTAZ = space(1)
  w_ATCRIPTE = space(1)
  w_ATNTABLE = space(50)
  w_ATCOODBC = space(25)
  w_ATCOSTRI = space(250)
  w_ATCAMCON = space(150)
  w_GRCODICE = space(15)
  w_GRDESCRI = space(35)
  w_GRDTINVA = ctod("  /  /  ")
  w_GRDTOBSO = ctod("  /  /  ")
  w_GRNUMSEQ = 0
  w_GRCODREG = space(15)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_TIPODEST = space(1)
  w_GRCODICEP = space(15)
  w_ERRORGRU = .f.
  w_GSNOME = space(20)
  w_GSDESC = space(50)
  w_GSNOMVAL = space(20)
  w_GSNOMDES = space(254)
  w_GSFLGACT = space(1)
  w_GSTIPPAR = space(1)
  w_GSVALVAR = space(254)
  w_CPROWNUM = 0
  w_GSTIPESC = space(1)
  w_AGG = .f.
  w_FLMD = space(1)
  w_SFPCNAME = space(20)
  w_SFFORMAT = space(10)
  w_CPROWNUM = 0
  w_SFTYPECL = space(20)
  w_SFFLGSEL = space(1)
  w_SFMSKCFG = space(20)
  w_SFEXTFOR = space(10)
  w_CPROWORD = 0
  w_AGG = .f.
  w_FLMD = space(1)
  w_AQCODICE = space(20)
  w_AQDES = space(254)
  w_AQQRPATH = space(254)
  w_AQDTFILE = space(254)
  w_AQIMPATH = space(254)
  w_AQLOG = space(254)
  w_AQADOCS = space(254)
  w_AQMAILR = space(100)
  w_AQROWORD = 0
  w_AQUTENTE = 0
  w_AQFLUE = space(1)
  w_GIROWNUM = 0
  w_GIROWORD = 0
  w_GIPROGRA = space(254)
  w_GIPARAM = space(15)
  w_GITABNAM = space(15)
  w_GIDESC = space(50)
  w_GIDESGES = space(50)
  w_QAROWPRG = 0
  w_QAFLUTGR = space(1)
  w_QAUTEGRP = 0
  w_AQFORTYP = space(10)
  w_AQFLGZCP = space(1)
  w_AQCONTYP = space(15)
  w_AQCURLOC = space(1)
  w_AQCURTYP = space(20)
  w_AQENGTYP = space(1)
  w_AQTIMOUT = 0
  w_AQCMDTMO = 0
  w_AQQUEFIL = space(1)
  w_AQCRIPTE = space(1)
  w_FIPRGRN = 0
  w_FIROWNUM = 0
  w_FIFLNAME = space(30)
  w_FIROWORD = 0
  w_FITIPFIL = space(8)
  w_FIFILTRO = space(254)
  w_FIFLGAZI = space(1)
  w_FIFLGREM = space(1)
  w_FIVISMSK = space(254)
  w_FIROWNUM1 = 0
  w_FIFLNAME1 = space(30)
  w_FIROWORD1 = 0
  w_FITIPFIL1 = space(8)
  w_FIFILTRO1 = space(254)
  w_FIFLGAZI1 = space(1)
  w_FIFLGREM1 = space(1)
  w_FIVISMSK1 = space(254)
  w_DERISERV = space(1)
  w_DEROWORD = 0
  w_DEROWNUM = 0
  w_DETIPDES = space(1)
  w_DECODGRU = 0
  w_DETIPCON = space(1)
  w_DECODCON = space(15)
  w_DECODAGE = space(5)
  w_DERIFCFO = space(1)
  w_DEVALDES = space(25)
  w_DECODROL = 0
  w_DE__READ = space(1)
  w_DE_WRITE = space(1)
  w_DEDELETE = space(1)
  w_DEFOLDER = 0
  w_DEDESFIL = space(100)
  w_DE__NOTE = space(0)
  w_PFROWNUM = 0
  w_PFTIPVAR = space(1)
  w_PFVARNAM = space(20)
  w_PFDESVAR = space(50)
  w_PFLENVAR = 0
  w_PFDECVAR = 0
  w_PFROWORD = 0
  w_GI__MENU = space(10)
  w_MG__MENU = space(254)
  w_AQFLJOIN = space(1)
  w_GQCODICE = space(15)
  w_GQDESCRI = space(45)
  w_GQDTINVA = ctod("  /  /  ")
  w_GQDTOBSO = ctod("  /  /  ")
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_GQCODQUE = space(20)
  w_GQNUMSEQ = 0
  w_RECODICE = space(15)
  w_RECODICEP = space(15)
  w_REDESCRI = space(45)
  w_REDESSUP = space(0)
  w_RE__TIPO = space(1)
  w_REDTINVA = ctod("  /  /  ")
  w_REDTOBSO = ctod("  /  /  ")
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_RETIPSTE = space(2)
  w_REDESOPE = space(100)
  w_RENOMCUR = space(8)
  w_TIPINSERT = space(1)
  w_ERRORE = .f.
  w_CANCSTRUC = .f.
  w_TRCODICE = space(15)
  w_TRCODICP = space(15)
  w_TRDESCRI = space(40)
  w_TR__NOTE = space(254)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_CPROWNUMP = 0
  w_CPROWORDP = 0
  w_TRTIPDET = space(1)
  w_TRTIPVOC = space(1)
  w_TRDESDET = space(50)
  w_TR__COL1 = space(1)
  w_TR__COL2 = space(1)
  w_TR__COL3 = space(1)
  w_TC__COLO = 0
  w_TC__INDE = 0
  w_TC__FTST = space(1)
  w_TC__FONT = space(1)
  w_TR__FLAG = space(1)
  w_TRSEQCAL = 0
  w_TR_RIFUE = space(1)
  w_BGOON = .f.
  w_RTCODICE = space(15)
  w_RTNUMRIG = 0
  w_RTRIGTOT = 0
  w_RT_SEGNO = space(1)
  w_VRCODVOC = space(15)
  w_VRROWORD = 0
  w_VRDESVOC = space(50)
  w_VR__NOTE = space(254)
  w_DVCODVOC = space(15)
  w_DVROWORD = 0
  w_CPROWNUM2 = 0
  w_DVFLMACO = space(1)
  w_DVCODMAS = space(15)
  w_DVCODCON = space(15)
  w_DV_SEGNO = space(1)
  w_DV__DAVE = space(1)
  w_PERAZI = space(2)
  w_CODAZI = space(5)
  w_VBGRUPPO = space(15)
  w_VBMASTRO = space(15)
  w_VB_CONTO = space(15)
  w_VB__VOCE = space(15)
  w_VBNATURA = space(1)
  w_VBDESCRI = space(70)
  w_VBPARTEC = space(1)
  w_CHIAVE = space(65)
  w_OLDCHIAVE = space(63)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_VB__TIPO = space(1)
  w_VBCODCON = space(15)
  w_VBCODMAS = space(15)
  w_VBTIPVOC = space(1)
  w_VBCODVOC = space(15)
  w_VBROWORD = 0
  w_VBFORMUL = space(30)
  w_VB_SEGNO = 0
  w_VBCONDIZ = space(1)
  w_VB___NOTE = space(254)
  w_VBTIPESP = space(1)
  w_VBESPDET = space(1)
  w_CCCODICE = space(4)
  w_CCLOCALI = space(30)
  w_CC___CAP = space(5)
  w_CC____PR = space(2)
  w_CCREGION = space(3)
  w_CCAREAGE = space(2)
  w_FILENAME = space(50)
  w_OLDCHIAVE = space(5)
  w_CHIAVE = space(5)
  w_TACODICE = space(5)
  w_TADESCRI = space(40)
  w_TACODCLA = space(5)
  w_TACLADES = space(40)
  w_TAFLUNIC = space(1)
  w_TAFLDEFA = space(1)
  w_TDPUBWEB = space(1)
  w_EXCODICE = space(10)
  w_EXDESCRI = space(40)
  w_EXTIPALL = space(5)
  w_EXCLAALL = space(5)
  w_EXESTCBI = space(4)
  w_ERRORECLA = .f.
  w_CLCODICE = space(15)
  w_CLCODICEP = space(15)
  w_CLDESCRI = space(50)
  w_CDTIPARC = space(1)
  w_CDMOTARC = space(1)
  w_CDCLAAWE = space(15)
  w_CDRIFDEF = space(1)
  w_CDRIFTAB = space(20)
  w_CDMODALL = space(1)
  w_CDTIPRAG = space(1)
  w_CDCAMRAG = space(50)
  w_CDPATSTD = space(75)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_CDCODATT = space(15)
  w_CDDESATT = space(40)
  w_CDTIPATT = space(1)
  w_CDCAMCUR = space(254)
  w_CDCHKOBB = space(1)
  w_CDTABKEY = space(15)
  w_CDVLPRED = space(50)
  w_CDNATAWE = space(15)
  w_CDLUNATT = 0
  w_CDDECATT = 0
  w_CDKEYAWE = space(1)
  w_CDTIPALL = space(5)
  w_CDCLAALL = space(5)
  w_CDCOMTIP = space(1)
  w_CDCOMCLA = space(1)
  w_CDFOLDER = space(1)
  w_CDFIRDIG = space(1)
  w_CDCONSOS = space(1)
  w_CDCLASOS = space(10)
  w_CDAVVSOS = 0
  w_CDKEYUPD = space(1)
  w_CDGETDES = space(1)
  w_CDSERVER = space(20)
  w_CD__PORT = 0
  w_CD__USER = space(20)
  w_CDPASSWD = space(20)
  w_CDARCHIVE = space(20)
  w_CDFLCTRL = space(1)
  w_CDFLGSOS = space(2)
  w_CDRIFTEM = space(1)
  w_CDATTSOS = space(10)
  w_CDCLAPRA = space(1)
  w_CDATTPRI = space(1)
  w_CDALIASF = space(15)
  w_CDFLGDES = space(1)
  w_CDATTINF = space(15)
  w_CDTIPOBC = space(1)
  w_CDPUBWEB = space(1)
  w_CDNOMFIL = space(200)
  w_CDERASEF = space(1)
  w_CDREPOBC = space(254)
  w_ERROREPROC = .f.
  w_PDCODPROP = space(10)
  w_PDCODPRO = space(10)
  w_PDDESPRO = space(40)
  w_PDCLADOC = space(15)
  w_PDCHKOBB = space(1)
  w_PDCHKLOG = space(1)
  w_PDCHKSTA = space(1)
  w_PDCHKMAI = space(1)
  w_PDCHKFAX = space(1)
  w_PDCHKPTL = space(1)
  w_PDCHKWWP = space(1)
  w_PDCHKARC = space(1)
  w_PDCHKANT = 0
  w_PDRILSTA = space(1)
  w_PDRILMAI = space(1)
  w_PDRILFAX = space(1)
  w_PDRILPTL = space(1)
  w_PDRILWWP = space(1)
  w_PDRILARC = space(1)
  w_PDNMAXAL = 0
  w_PDREPORT = space(100)
  w_PDDESREP = space(50)
  w_PDKEYPRO = space(100)
  w_PDCONCON = space(60)
  w_PDOGGETT = space(254)
  w_PD_TESTO = space(254)
  w_PDFILTRO = space(254)
  w_PDCHKZCP = space(1)
  w_PDRILZCP = space(1)
  w_PDISOLAN = space(60)
  w_PDTIPRIF = space(2)
  w_PDCODSED = space(60)
  w_PDCODORD = space(60)
  w_PDCHKPEC = space(1)
  w_PDRILPEC = space(1)
  w_PDPROCES = space(3)
  w_CGDEFCFG = space(1)
  w_CGMODSAV = space(1)
  w_CGTIPCFG = space(1)
  w_CGCODAZI = space(5)
  w_CGUTEGRP = 0
  w_PRROWNUM = 0
  w_MOCODICE = space(10)
  w_MODESCRI = space(60)
  w_MOCLASSE = space(15)
  w_MOPATMOD = space(254)
  w_MOQUERY = space(60)
  w_MO__NOTE = space(0)
  w_MOPREFER = space(1)
  w_MOTIPPRA = space(10)
  w_MOMATPRA = space(10)
  w_MO__ENTE = space(10)
  w_MODTOBSO = ctod("  /  /  ")
  w_MODTINVA = ctod("  /  /  ")
  w_OK = .f.
  w_AggiornaTipo_Mate = .f.
  w_Procedi_Tipo_Mate = .f.
  w_ROWNUM = 0
  w_SERIAL = space(20)
  w_OLD_DESCOM = space(50)
  w_CCDESCOM = space(50)
  w_CCTIPUFF = space(1)
  w_CCLOCUFF = space(50)
  w_CCINDUFF = space(50)
  w_CCTELUFF = space(50)
  w_CCFAXUFF = space(50)
  w_CCMAILUF = space(50)
  w_CONTA = 0
  w_RMSERIAL = space(5)
  w_RMCODICE = space(5)
  w_RMDESCRI = space(60)
  w_RMDESAGG = space(60)
  w_RMVALMIN = 0
  w_RMVALMAX = 0
  w_RGCODICE = space(5)
  w_RGDESCRI = space(60)
  w_RGCODRIG = space(5)
  w_CPROWORD = 0
  w_OLD_COD = space(5)
  w_TCCODICE = space(5)
  w_TCDESCRI = space(60)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_TCCODRIG = space(5)
  w_TCDATINI = ctod("  /  /  ")
  w_TCDATFIN = ctod("  /  /  ")
  w_CUSERIAL = space(4)
  w_CUDESCRI = space(130)
  w_CUFLTIPO = space(1)
  w_CUVALPRA = 0
  w_CUIMPORT = 0
  w_CUIMPANT = 0
  w_CUTRIBUT = space(1)
  w_CTCODICE = space(5)
  w_CTDESCRI = space(80)
  w_CTFLCONT = space(1)
  w_COCODICE = space(2)
  w_CODESCRI = space(80)
  w_CACODICE = space(3)
  w_CADESCRI = space(80)
  w_UFCODICE = space(15)
  w_UFLOCALI = space(50)
  w_UF__NOME = space(80)
  w_UFTIPUFF = space(1)
  w_UFPROVIN = space(2)
  w_UFPRORIF = space(2)
  w_UFINDMAI = space(250)
  w_UFCODICE = space(15)
  w_UFDESCRI = space(60)
  w_UF_TIPOL = space(1)
  w_EP__TIPO = space(1)
  w_EPCALTAR = space(10)
  w_EPCODENT = space(5)
  w_EPCODICE = space(10)
  w_EPDESCRI = space(60)
  w_EPDESECT = space(60)
  w_EPTIPUFF = space(1)
  w_OPCODICE = space(10)
  w_OPDESCRI = space(250)
  w_OP_RUOLO = space(100)
  w_OPMATERI = space(100)
  w_OPGRUPPO = space(60)
  w_OPCLRUOL = space(1)
  w_OPCLMATE = space(2)
  w_OPCLOGGE = space(3)
  w_OPCODMAT = space(10)
  w_OLD_COD = space(5)
  w_MPCODICE = space(10)
  w_MPDESCRI = space(60)
  w_MP_TIPOL = space(1)
  w_TCDESCRI = space(60)
  w_SECODRIG = space(5)
  w_SEDATINI = ctod("  /  /  ")
  w_SEDATFIN = ctod("  /  /  ")
  w_OLD_COD = space(5)
  w_TPCODICE = space(10)
  w_TPDESCRI = space(60)
  w_TPFLGIUD = space(1)
  w_STCODRIG = space(5)
  w_STDATINI = ctod("  /  /  ")
  w_STDATFIN = ctod("  /  /  ")
  w_ITCODICE = space(10)
  w_ITDESCRI = space(60)
  w_ITCODPRE = space(20)
  w_ITCOD_PR = space(41)
  w_ITDESPRE = space(40)
  w_OLD_COD = space(5)
  w_CACODICE = space(20)
  w_CADESCRI = space(254)
  w_CADURORE = 0
  w_CADURMIN = 0
  w_CADATINI = ctot("")
  w_CAGGPREA = 0
  w_CAPROMEM = ctot("")
  w_CAPRIORI = 0
  w_CATIPATT = space(5)
  w_CACHKOBB = space(1)
  w_CASTAATT = space(1)
  w_CAFLNSAP = space(1)
  w_CAFLTRIS = space(1)
  w_CAFLRINV = space(1)
  w_CACAUDOC = space(5)
  w_CARAGGST = space(1)
  w_CADISPON = 0
  w_CACODSER = space(20)
  w_CAKEYART = space(41)
  w_CADESSER = space(40)
  w_CADESAGG = space(254)
  w_CAFLDEFF = space(1)
  w_CAFLRESP = space(1)
  w_CATIPRIS = space(1)
  w_CAFLNOTI = space(1)
  w_CAFLATRI = space(1)
  w_CACHKNOM = space(1)
  w_CADOCCLI = space(1)
  w_CA__NOTE = space(254)
  w_CAMINPRE = 0
  w_CAORASYS = space(1)
  w_CAFLPREA = space(1)
  w_CAFLANAL = space(1)
  w_CATIPRIG = space(1)
  w_CAPUBWEB = space(1)
  w_CAFLGLIS = space(1)
  w_CACAUACQ = space(5)
  w_CACHKINI = space(1)
  w_CACHKFOR = space(1)
  w_CAFLSTAT = space(1)
  w_CACOLORE = 0
  w_CAFLPRAT = space(1)
  w_CAFLGIUD = space(1)
  w_CAFLPART = space(1)
  w_CAFLSOGG = space(1)
  w_CAFLATTI = space(1)
  w_CAFLNOMI = space(1)
  w_CAFLPRAN = 0
  w_CAFLGIUN = 0
  w_CAFLPARN = 0
  w_CAFLSOGN = 0
  w_CAFLATTN = 0
  w_CAFLNOMN = 0
  w_CATIPPRA = space(10)
  w_CAMATPRA = space(10)
  w_CA__ENTE = space(10)
  w_CATIPRI2 = space(1)
  w_IPCODICE = space(10)
  w_IPDESCRI = space(60)
  w_IPCODCAU = space(20)
  w_IPDESCAU = space(254)
  w_IPNUMGIO = 0
  w_IPRIFPRO = 0
  w_NOMEMETODO = space(30)
  w_DESCMETODO = space(254)
  w_CODICESTATO = space(18)
  w_DESCSTATO = space(35)
  w_SUBSCRIBE = space(1)
  w_RFRSSNAM = space(15)
  w_RFDESCRI = space(254)
  w_RFRSSURL = space(254)
  w_RFMAXITM = 0
  w_RFFLGDSK = space(1)
  w_MOPRPRTY = space(50)
  w_MOTIPO_P = space(1)
  w_MODESCRI = space(50)
  w_MCNOMTAB = space(20)
  w_MCGESTIO = space(10)
  w_MC_VERSO = space(1)
  w_MCTIPO_P = space(1)
  w_MCMSOUTL = space(50)
  w_MCTIPEXP = space(1)
  w_MCEXPRES = space(250)
  w_MCFL_KEY = space(1)
  w_RESERIAL = space(10)
  w_REBUSOBJ = space(15)
  w_RETABMST = space(20)
  w_RETABDTL = space(20)
  w_REDESCRI = space(50)
  w_REPSTBTN = space(1)
  w_REKEYSEL = space(60)
  w_RE_TABLE = space(10)
  w_REFLDNAM = space(30)
  w_REKEYREC = space(1)
  w_RERIFREC = space(1)
  w_REMONINS = space(1)
  w_REMONMOD = space(1)
  w_REMONDEL = space(1)
  w_CPROWORD = 0
  w_CPROWNUM = 0
  w_REFLENAB = space(1)
  w_RESAVEDB = space(1)
  w_REUSEROP = space(30)
  w_REREGSUC = space(1)
  w_RETFLTEX = space(254)
  w_REPRIORI = 0
  w_REGG_LOG = 0
  w_REBLKINS = space(1)
  w_REOBBINS = space(1)
  w_REFLTEXP = space(254)
  w_RETDELEX = space(254)
  w_REFLDCOM = space(254)
  w_REKEYGES = space(1)
  w_REMSGERC = space(254)
  w_REMSGERR = space(254)
  w_SERIAL = space(10)
  w_RASERIAL = space(10)
  w_RADESCRI = space(40)
  w_RAESTENS = space(254)
  w_RATIPBST = space(1)
  w_MRSERIAL = space(10)
  w_MRPATMOD = space(254)
  w_MRDESCRI = space(60)
  w_MRIMGMOD = space(254)
  w_CHIAVE = space(3)
  w_OLDCHIAVE = space(3)
  w_FECODICE = space(3)
  w_FEDESCRI = space(35)
  w_FEPREDEF = space(1)
  w_FEDATFES = ctod("  /  /  ")
  w_FEDESFES = space(35)
  w_PRCODICE = space(5)
  w_PRDESCRI = space(80)
  w_PRINDIRI = space(254)
  w_GLCODICE = space(5)
  w_GLDESCRI = space(100)
  w_CCCODICE = space(5)
  w_CCDESCRI = space(35)
  w_CCFLPART = space(1)
  w_CCFLANAL = space(1)
  w_CCFLSALI = space(1)
  w_CCFLSALF = space(1)
  w_CCFLCOMP = space(1)
  w_CCFLRIFE = space(1)
  w_CCTIPDOC = space(2)
  w_CCTIPREG = space(1)
  w_CCNUMREG = 0
  w_CCNUMDOC = space(1)
  w_CCFLIVDF = space(1)
  w_CCFLPDIF = space(1)
  w_CCFLSTDA = space(1)
  w_CCCALDOC = space(1)
  w_CCTESDOC = space(1)
  w_CCFLRITE = space(1)
  w_CCDTINVA = ctod("  /  /  ")
  w_CCDTOBSO = ctod("  /  /  ")
  w_CCSERDOC = space(10)
  w_CCFLPDOC = space(1)
  w_CCSERPRO = space(10)
  w_CCFLPPRO = space(1)
  w_CCCFDAVE = space(1)
  w_CCCONIVA = space(15)
  w_CCFLBESE = space(1)
  w_CCFLMOVC = space(1)
  w_CCCAUMOV = space(5)
  w_CCFLAUTR = space(1)
  w_CCFLINSO = space(1)
  w_CCMOVCES = space(1)
  w_CCCAUCES = space(5)
  w_CCGESRIT = space(1)
  w_CCCAUSBF = space(5)
  w_CCFLASSE = space(1)
  w_CCDESSUP = space(254)
  w_CCDESRIG = space(254)
  w_CCPAGINS = space(5)
  w_CCREGMAR = space(1)
  w_CCGESCAS = space(1)
  w_CCFLCOSE = space(1)
  w_APTIPCON = space(1)
  w_APCODCON = space(15)
  w_APFLDAVE = space(1)
  w_APFLAUTO = space(1)
  w_APFLPART = space(1)
  w_APCAURIG = space(5)
  w_APCONBAN = space(15)
  w_APCAUMOV = space(5)
  w_AICODIVA = space(5)
  w_AITIPCON = space(1)
  w_AICONDET = space(15)
  w_AICONIND = space(15)
  w_AITIPREG = space(1)
  w_AINUMREG = 0
  w_AICONTRO = space(15)
  w_AITIPCOP = space(1)
  w_CODCAUR = space(5)
  w_DESCCAU = space(60)
  w_CAUTESR = space(5)
  w_TIPMOVR = space(3)
  w_CAUINS = space(5)
  w_FTCODICE = space(10)
  w_FTDESCRI = space(60)
  w_CODICE = space(20)
  w_CODPROGR = space(20)
  w_DESCR = space(30)
  w_NOMETAB = space(20)
  w_NOMECAMPO = space(20)
  w_DIPENDENZE = space(20)
  w_COMMENTO = space(10)
  w_CCFLRAGG = space(1)
  w_CCFLPRIU = space(1)
  w_CCFLGA01 = space(1)
  w_CCFLGA02 = space(1)
  w_CCFLGA03 = space(1)
  w_CCFLONFI = space(1)
  w_CCFLGA04 = space(1)
  w_CCFLGA05 = space(1)
  w_CCFLGA06 = space(1)
  w_CCFLGA07 = space(1)
  w_CCFLGA08 = space(1)
  w_CCFLGA09 = space(1)
  w_CCFLGA10 = space(1)
  w_CCFLGA11 = space(1)
  w_CCFLGA12 = space(1)
  w_CCFLGA13 = space(1)
  w_CCFLGA14 = space(1)
  w_CCDATAGG = space(1)
  w_CCFLCONT = space(1)
  w_CCCAUCON = space(5)
  w_CCFLGA15 = space(1)
  w_CCFLGA16 = space(1)
  w_CCFLGA17 = space(1)
  w_CCFLDADI = space(1)
  w_CCFLGOPE = space(1)
  w_CCRIVALU = space(1)
  w_CCFLGA18 = space(1)
  w_CCFLGA19 = space(1)
  w_CCFLGA20 = space(1)
  w_CCFLGA21 = space(1)
  w_CCFLGA22 = space(1)
  w_CCFLGA23 = space(1)
  w_CCFLGA24 = space(1)
  w_CCFLONFF = space(1)
  w_CCFLPRIC = space(1)
  w_FODESCRI = space(40)
  w_FORISULT = space(3)
  w_FOFORMUL = space(100)
  w_FOCONDIZ = space(50)
  w_MCCODICE = space(3)
  w_MCCONDAR = space(3)
  w_MCCONAVE = space(3)
  w_BTCODICE = space(5)
  w_BTDESCRI = space(120)
  w_BT_SCHEMA = space(90)
  w_BTTIPREC = space(1)
  w_SRCPRG = space(50)
  w_DESCRI = space(254)
  w_PREIMG = space(254)
  w_CODUTE = 0
  w_ROTTURA = 0
  w_CHKCOL = space(1)
  w_WIDTH = 0
  w_PIANO = space(1)
  w_COLOR = 0
  w_TRANSIZ = space(1)
  w_UTESAVE = space(1)
  w_STARTUP = space(1)
  w_TIMER = space(1)
  w_PIN = space(1)
  w_ORDGRP = space(100)
  w_DTHEME = space(5)
  w_HIDREF = space(1)
  w_GADGET = space(10)
  w_GRPCOD = space(5)
  w_CODICE = space(10)
  w_ROTTURA = space(10)
  w_GSERIAL = space(10)
  w_NOME = space(50)
  w_DESCRI = space(254)
  w_SRCPRG = space(50)
  w_GRPCOD = space(5)
  w_CNDATT = space(254)
  w_CHKATT = space(1)
  w_FORAZI = space(1)
  w_PROP = space(50)
  w_VALORE = space(254)
  w_CODUTE = 0
  w_RESET = space(1)
  w_UTEESC = 0
  w_Handle = 0
  w_CODICE = space(10)
  w_ROTTURA = space(10)
  w_DESCRI = space(254)
  w_TIPOUG = space(1)
  w_UTEGRP = 0
  w_CNDATT = space(254)
  w_CHKATT = space(1)
  w_QUERY = space(250)
  w_TIPRES = space(1)
  w_TIPSIN = space(3)
  w_BENCHM = space(3)
  w_BENFIX = 0
  w_BENCOD = space(10)
  w_VISIBI = space(1)
  w_WRTLOG = space(1)
  w_FRQEXE = 0
  w_NUMRES = 0
  w_PARAM = space(50)
  w_VALORE = space(254)
  w_MINUTI = 0
  w_AVVIO = space(1)
  w_ESCLUSIVO = space(1)
  * --- WorkFile variables
  ANAG_CAP_idx=0
  ANAG_PRO_idx=0
  AZIENDA_idx=0
  CAU_AEN_idx=0
  CAU_INPS_idx=0
  COD_ABI_idx=0
  COD_CAB_idx=0
  COD_CATA_idx=0
  COD_PREV_idx=0
  COD_TRIB_idx=0
  DETTRICL_idx=0
  ENTI_COM_idx=0
  NOMENCLA_idx=0
  OUT_PUTS_idx=0
  RACCBILA_idx=0
  RACDBILA_idx=0
  REG_PROV_idx=0
  RIGTOTAL_idx=0
  SE_INAIL_idx=0
  SED_INPS_idx=0
  STMPFILE_idx=0
  TIR_DETT_idx=0
  TIR_MAST_idx=0
  UNIMIS_idx=0
  VOCIRICL_idx=0
  TIP_ALLE_idx=0
  CLA_ALLE_idx=0
  EXT_ENS_idx=0
  TOT_MAST_idx=0
  TOT_DETT_idx=0
  DET_DIME_idx=0
  CFG_GEST_idx=0
  PRCFGGES_idx=0
  MODMCONF_idx=0
  MODDCONF_idx=0
  PROMODEL_idx=0
  UFF_COMP_idx=0
  SET_RIGH_idx=0
  SET_RAGG_idx=0
  TIP_CLIE_idx=0
  SET_TIPC_idx=0
  CON_UNIF_idx=0
  F23_TRIB_idx=0
  F23_CONT_idx=0
  F23_CAUS_idx=0
  UFF_GIUD_idx=0
  PRA_UFFI_idx=0
  PRA_ENTI_idx=0
  PRA_OGGE_idx=0
  TIP_MATE_idx=0
  PRA_MATE_idx=0
  SET_MATE_idx=0
  PRA_TIPI_idx=0
  SET_TIPI_idx=0
  PRE_ITER_idx=0
  CAUMATTI_idx=0
  CAU_ATTI_idx=0
  PRO_ITER_idx=0
  MET_SOS_idx=0
  STATISOS_idx=0
  RSS_FEED_idx=0
  MAP_OUTL_idx=0
  MAP_SINC_idx=0
  REF_MAST_idx=0
  REF_DETT_idx=0
  RAG_TIPI_idx=0
  WZMODREP_idx=0
  FES_MAST_idx=0
  FES_DETT_idx=0
  PUN_ACCE_idx=0
  PWB_GLOC_idx=0
  CAUPRI1_idx=0
  CAUIVA1_idx=0
  CAU_CONT_idx=0
  CONTROPA_idx=0
  FAS_TARI_idx=0
  TSDESPRO_idx=0
  FOR_CESP_idx=0
  MCO_CESP_idx=0
  CAU_CESP_idx=0
  BUS_TIPI_idx=0
  MOD_GADG_idx=0
  MYG_MAST_idx=0
  MYG_DETT_idx=0
  GAD_MAST_idx=0
  GAD_DETT_idx=0
  ELABKPIM_idx=0
  ELABKPID_idx=0
  TMRELKPI_idx=0
  GAD_ACCE_idx=0
  GRP_GADG_idx=0
  GADCOLOR_idx=0
  ELE_EXPR_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica o salva da file di testo  (da GSUT_SCO)
    * --- Nel caso di contributo unificato il separatore deve essere . dato che esportiamo ed importiamo valori numerici con decimali
    * --- Nel caso di esportazione e importazione Gadget e/o KPI si fa una trascodifica per il carattere doppio apice
    this.w_DATRASCO = "*DA*"
    this.w_PADRE = This.oParentObject
    * --- Se true lanciato da procedura di conversione non da messaggi a video al termine
    this.w_NOMSG = INLIST(Upper( this.w_PADRE.Class ), "TGSCV_BOU", "TGSRV_BCH_S")
    if this.pTIPO="CARICA"
      * --- Carica da file di testo il contenuto della tabella Selezionata
      if NOT file( this.oParentObject.w_PATH )
        * --- Se il file non esiste
        ah_ErrorMsg("Il file selezionato non esiste",,"")
        i_retcode = 'stop'
        return
      endif
      * --- Se siamo in fase di caricamento dei Raggruppamenti di attivit�
      if IsAlt() AND this.oParentObject.w_TIPARC="AC"
        this.w_CANC_RAGGR = AH_YESNO("Vuoi cancellare i raggruppamenti di attivit� esistenti prima di procedere con il caricamento di quelli nuovi?", " ", " ", " ", " ", " ", " ", .T.)
      endif
      * --- Try
      local bErr_04B94698
      bErr_04B94698=bTrsErr
      this.Try_04B94698()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("%1",,"", i_errmsg)
      endif
      bTrsErr=bTrsErr or bErr_04B94698
      * --- End
      if this.w_NOMSG and not Empty(this.w_MSGERROR)
        ah_ErrorMsg(this.w_MSGERROR)
      endif
    else
      * --- Salva su file di testo il contenuto dell'Output Utente
      * --- Controllo se il file c'� gi� - se si esco
      if file( this.oParentObject.w_PATH )
        * --- Se il file non esiste
        ah_ErrorMsg("File su cui si intende salvare la tabella gia esistente",,"")
        i_retcode = 'stop'
        return
      endif
      if !chknfile(this.oParentObject.w_PATH,"F")
        i_retcode = 'stop'
        return
      endif
      ah_Msg("Scrittura in corso...",.T.)
      do case
        case this.oParentObject.w_TIPARC="AB"
          * --- ABI Banche
          vq_exec("query\ABISAVE.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="CA"
          * --- CAB Filiali Banche
          vq_exec("query\CABSAVE.vqr",this,"dati")
          i_A = WRCURSOR("DATI")
          SELECT DATI
          REPLACE ALL FIDESFIL WITH STRTRAN(FIDESFIL,'"',"|"), ;
          FIINDIRI WITH STRTRAN(FIINDIRI,'"',"|")
        case this.oParentObject.w_TIPARC="NM"
          * --- Nomenclature
          vq_exec("query\NOMSAVE.vqr",this,"dati")
          i_A = WRCURSOR("DATI")
          SELECT DATI
          REPLACE ALL NMDESCRI WITH STRTRAN(NMDESCRI,'"',"|")
        case this.oParentObject.w_TIPARC="UM"
          * --- U.M. Supplementari
          vq_exec("query\UMSSAVE.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="TF"
          do case
            case this.oParentObject.w_TIPMDF="RP"
              * --- Regioni, Province autonome
              vq_exec("query\RPSSAVE.vqr",this,"dati")
            case this.oParentObject.w_TIPMDF="EL"
              * --- Enti Locali
              vq_exec("query\ELSSAVE.vqr",this,"dati")
            case this.oParentObject.w_TIPMDF="CI"
              * --- Causali Contributo INPS
              vq_exec("query\CISSAVE.vqr",this,"dati")
            case this.oParentObject.w_TIPMDF="SI"
              * --- Sedi INAIL
              vq_exec("query\SISSAVE.vqr",this,"dati")
            case this.oParentObject.w_TIPMDF="AE"
              * --- Altri Enti Previdenziali
              vq_exec("query\AESSAVE.vqr",this,"dati")
            case this.oParentObject.w_TIPMDF="CA"
              * --- Causali Altri Enti
              vq_exec("query\CASSAVE.vqr",this,"dati")
            case this.oParentObject.w_TIPMDF="SP"
              * --- Sedi INPS
              vq_exec("query\SPSSAVE.vqr",this,"dati")
            case this.oParentObject.w_TIPMDF="TR"
              * --- Sedi INPS
              vq_exec("query\TRISAVE.vqr",this,"dati")
              * --- Il campo descrizione e' un memo , prima di salvarlo devo convertirlo in carattere
              CREATE CURSOR TEMPO (TRICODI C(5), TRIDES C(200), TRITIP C(10),TRMERIF C(1), TRIFCODA C(1))
              SELECT DATI
              GO TOP
              SCAN FOR NOT EMPTY(NVL(TRCODICE,""))
              this.w_TRICODI = TRCODICE
              this.w_TRIDES = STRTRAN(LEFT(NVL(TRDESCRI,""), 200), CHR(10)+CHR(13), " ")
              this.w_TRIDES = STRTRAN(ALLTRIM(this.w_TRIDES), CHR(10), " ")
              this.w_TRIDES = STRTRAN(ALLTRIM(this.w_TRIDES), CHR(13), " ")
              this.w_TRITIP = NVL(TRTIPTRI, SPACE(10))
              this.w_TRMERIF = IIF(EMPTY(NVL(TRFLMERF,"N")),"N",NVL(TRFLMERF,"N"))
              this.w_TRIFCODA = IIF(EMPTY(NVL(TRFLCODA,"N")),"N",NVL(TRFLCODA,"N"))
              INSERT INTO TEMPO (TRICODI, TRIDES, TRITIP, TRMERIF, TRIFCODA) VALUES (this.w_TRICODI, this.w_TRIDES, this.w_TRITIP, this.w_TRMERIF, this.w_TRIFCODA)
              SELECT DATI
              ENDSCAN
              SELECT TEMPO
              GO TOP
            case this.oParentObject.w_TIPMDF="CU"
              * --- F24 - Contributo unificato
              vq_exec("query\CONUNIFSAVE.vqr",this,"dati")
            case this.oParentObject.w_TIPMDF="CT"
              vq_exec("query\TRIBUTISAVE.vqr",this,"dati")
            case this.oParentObject.w_TIPMDF="CC"
              vq_exec("query\CODICECONSAVE.vqr",this,"dati")
            case this.oParentObject.w_TIPMDF="CF"
              vq_exec("query\CAUSALISAVE.vqr",this,"dati")
          endcase
        case this.oParentObject.w_TIPARC="CP"
          * --- CAP e Provincie
          vq_exec("query\CAPSAVE.VQR",this,"dati")
        case this.oParentObject.w_TIPARC="RE"
          * --- Regole di importazione (INFOLINK)
          * --- E' stata eliminata la union dalla query 'REGOINFO' per ovviare a un' errata estrazione dei dati di tipo numerico in Oracle.
          *     Di conseguenza vengono ora creati due cursori 'Dati' e 'Dati2'.
          vq_exec("query\REGOINFO.vqr",this,"dati")
          vq_exec("query\REGOINF1.vqr",this,"dati2")
          Select * from dati union all Select * from dati2 order by 1,11 into cursor Dati 
        case this.oParentObject.w_TIPARC="GR"
          * --- Gruppi di regole (INFOLINK)
          vq_exec("query\GRUPINFO.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="CF"
          * --- Regole Controllo Flussi
          vq_exec("query\GSCF_BCO.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="GS"
          * --- Gestioni Schedulatore
          vq_exec("query\GSTSJOB.vqr",this,"dati")
          wrdati=WRCURSOR("dati")
          Scan
          this.w_CAMPO = NVL(GSNOME,"")
          this.w_CAMPO1 = NVL(GSVALVAR,"")
          this.w_CAMPO = STRTRAN(this.w_CAMPO, '"', "#")
          this.w_CAMPO1 = STRTRAN(this.w_CAMPO1, '"', "#")
          Replace GSNOME with this.w_CAMPO
          Replace GSVALVAR with this.w_CAMPO1
          EndScan
        case this.oParentObject.w_TIPARC="SF"
          * --- Configurazione Stampa su File
          vq_exec("query\CFGSTMP.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="IR"
          * --- Salvo anagrafica query InfoPublisher
          vq_exec("query\INFOPB_A.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="GI"
          * --- Salvo gruppi query InfoPublisher
          vq_exec("query\INFOPB_G.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="RB"
          * --- Regole relative all'analisi di bilancio
          vq_exec("query\REGSAVE.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="SB"
          * --- Struttura di Bilancio UE
          * --- Select from query\STRU_BIL_NOTE
          do vq_exec with 'query\STRU_BIL_NOTE',this,'_Curs_query_STRU_BIL_NOTE','',.f.,.t.
          if used('_Curs_query_STRU_BIL_NOTE')
            select _Curs_query_STRU_BIL_NOTE
            locate for 1=1
            do while not(eof())
            if _Curs_query_STRU_BIL_NOTE.NUMREC > 0
              ah_ErrorMsg("Attenzione:%0I dati presenti in alcune note saranno ridotti ai primi 254 caratteri","!","")
            endif
              select _Curs_query_STRU_BIL_NOTE
              continue
            enddo
            use
          endif
          vq_exec("query\TIPIBILC.vqr",this,"dati2")
          * --- Rimuovo eventuali caratteri di fine riga nei campi memo
          select TRCODICE,TRDESCRI,STRTRAN(TR__NOTE,chr(13)+chr(10)," ") as TR__NOTE,CPROWNUM,CPROWORD,TRTIPDET,TRTIPVOC,TRDESDET,; 
 TR__COL1,TR__COL2,TR__COL3,TC__COLO,TC__INDE,TC__FTST,TC__FONT,TR__FLAG,TRSEQCAL,; 
 TR_RIFUE,RTCODICE,RTNUMRIG,RTRIGTOT,RT_SEGNO,VRCODVOC,VRROWORD,VRDESVOC,; 
 STRTRAN(VR__NOTE,chr(13)+chr(10)," ") as VR__NOTE,DVCODVOC,DVROWORD,CPROWNUM1,; 
 DVFLMACO,DVCODMAS,DVCODCON,DV_SEGNO,DV__DAVE FROM dati2 INTO CURSOR dati
        case this.oParentObject.w_TIPARC$ "VB-VF-BO"
          * --- Salva Sd�-Basilea2, Voci di raccordo,Bilancio & oltre
          this.w_NATURA = ICASE(this.oParentObject.w_TIPARC="VF","M",this.oParentObject.w_TIPARC="VB","B","O")
          vq_exec("query\GSAR_AVB.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="CC"
          * --- Salva Codice Catastali
          vq_exec("query\GSAR_ACC_1.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="MC"
          * --- Salva messaggi controllo SSFA
          vq_exec("query\CTRLMESS.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="TA"
          * --- Salvataggio della tabella Tipologie Allegati
          vq_exec("query\SAVETIPALL.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="EA"
          * --- Salvataggio della tabella Estensioni Allegati
          vq_exec("query\SAVEEXTENS.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="PO"
          if g_ISONAZ="ESP"
            * --- Salvataggio della tabella Porti/Aeroporti
            vq_exec("query\COD_AREO.vqr",this,"dati")
          else
            ah_errormsg("Modulo localizzazione spagnola non installato")
          endif
        case this.oParentObject.w_TIPARC="PR"
          if g_ISONAZ="ESP"
            * --- Salvataggio della tabella Province
            vq_exec("query\ANAG_PRO.vqr",this,"dati")
          else
            ah_errormsg("Modulo localizzazione spagnola non installato")
          endif
        case this.oParentObject.w_TIPARC="TI"
          vq_exec("query\GSAR_ATI.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="CD" or this.oParentObject.w_TIPARC="CL"
          * --- Classi Documentali
          vq_exec("query\CLASAVE.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="PD"
          * --- Processi Documentali
          vq_exec("query\PROSAVE.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="CG"
          * --- Configurazione gestioni
          vq_exec("query\CFG_GEST.vqr",this,"dati")
          wrdati=WRCURSOR("dati")
          Scan
          this.w_CAMPO = NVL(CGNOMGES,"")
          this.w_CAMPO1 = NVL(PRVALVAR,"")
          this.w_CAMPO = STRTRAN(this.w_CAMPO, '"', "#")
          this.w_CAMPO1 = STRTRAN(this.w_CAMPO1, '"', "#")
          Replace CGNOMGES with this.w_CAMPO
          Replace PRVALVAR with this.w_CAMPO1
          Select("dati")
          EndScan
        case this.oParentObject.w_TIPARC="MG"
          * --- Modelli configurazione gestione
          vq_exec("query\MOD__CFG.vqr",this,"dati")
          wrdati=WRCURSOR("dati")
          Scan
          this.w_CAMPO = NVL(MCNOMGES,"")
          this.w_CAMPO = STRTRAN(this.w_CAMPO, '"', "#")
          Replace MCNOMGES with this.w_CAMPO
          Select("dati")
          EndScan
        case this.oParentObject.w_TIPARC="DM"
          * --- Modelli documenti
          vq_exec("query\PROMD_SAVE.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="UC"
          * --- Uffici competenti relativi al comune
          vq_exec("query\UFFC_SAVE.vqr",this,"dati")
      endcase
      do case
        case this.oParentObject.w_TIPARC="RM"
          vq_exec("query\gsar_qrm.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="RG"
          vq_exec("query\gsar_qrg.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="TC"
          vq_exec("query\gsar_qtc.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="UG"
          vq_exec("query\COUFGI_SAVE.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="UP"
          vq_exec("query\gsar_qup.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="EP"
          vq_exec("query\gsar_qep.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="OP"
          vq_exec("query\gsar_qop.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="MP"
          vq_exec("query\gsar_qmp.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="TP"
          vq_exec("query\gsar_qtp.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="RP"
          * --- Se � disattivo il flag Usa descrizione prestazioni raggruppamenti nei Parametri generali di AeTop
          if this.oParentObject.w_DesPreRagg = "N"
            vq_exec("query\gsar_qrp.vqr",this,"dati")
          else
            vq_exec("query\gsar2qrp.vqr",this,"dati")
          endif
        case this.oParentObject.w_TIPARC="TT"
          vq_exec("query\gsar_qtt.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="AC"
          vq_exec("query\gsar1qac.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="RF"
          * --- RSS Feed
          vq_exec("query\gsut_ars.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="DE"
          vq_exec("..\AGEN\exe\query\gsfa_qdr.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="TE"
          vq_exec("..\AGEN\exe\query\gsfa_qte.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="OU"
          * --- Campi Outlook
          vq_exec("query\CampiOutl.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="OM"
          * --- Mappatura campi per sincronizzazione con Outlook
          vq_exec("query\MappaSinc.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="RT"
          * --- Raggruppamenti tipi file
          vq_exec("query\GSUT_RTF.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="MR"
          * --- Modelli report wizard
          vq_exec("query\Modelli_report.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="FE"
          * --- Festivit�
          vq_exec("query\GSUT_FES.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="PU"
          * --- Punti di accesso
          vq_exec("..\alte\exe\query\PUNTI_ACCE.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="PG"
          * --- Gestori locali
          vq_exec("..\alte\exe\query\GESTORI_LOCALI.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="AU"
          * --- Automatismi per causali contabili
          vq_exec("..\alte\exe\query\AUTOMATISMI.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="SO"
          * --- Causali Remote Banking
          vq_exec("query\CAUSREBA.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="OR"
          GSVA_BCO(this,"EXPORT", "dati" )
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          SELECT "dati"
          GO TOP
      endcase
      do case
        case this.oParentObject.w_TIPARC="MS"
          * --- Metodi SOStitutiva
          vq_exec("..\docm\exe\query\METODISOS.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="SS"
          * --- Stati SOStitutiva
          vq_exec("..\docm\exe\query\STATISOS.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="FT"
          * --- Fasi tariffario
          vq_exec("query\FAS_TARI.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="CS"
          * --- Causali cespiti
          vq_exec("..\alte\exe\query\CAU_CESP.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="BU"
          * --- Tipologie atto per busta telematica
          vq_exec("query\BUS_TIPI.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="GP"
          vq_exec("query\GRP_GADGET.vqr",this,"dati") 
 Update "dati" Set GGDESCRI = Strtran(GGDESCRI, '"', (this.w_DATRASCO))
        case this.oParentObject.w_TIPARC="TG"
          vq_exec("query\TEMI_GADGET.vqr",this,"dati") 
 Update "dati" Set GCDESCRI = Strtran(GCDESCRI, '"', (this.w_DATRASCO)), GCVALORE = Strtran(GCVALORE, '"', (this.w_DATRASCO))
        case this.oParentObject.w_TIPARC="GM"
          vq_exec("query\MOD_GADGET.vqr",this,"dati") 
 Update "dati" Set MGDESCRI = Strtran(MGDESCRI, '"', (this.w_DATRASCO))
        case this.oParentObject.w_TIPARC="IG"
          vq_exec("query\IMP_GADGET.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="GG"
          vq_exec("query\ARC_GADGET.vqr",this,"dati")
          if this.w_RESET="S"
            UPDATE "dati" SET GAVALORE=".F." WHERE GA_RESET="S"
          endif
          * --- Se lanciato dalla manutenzione gadget esporto solo quelli selezionati
          if Upper( this.w_PADRE.Class )=="TGSUT_KGG"
            this.w_FILTERCURSOR = Sys(2015)
            l_oldArea = Select() 
 vq_exec("query\gsut1kgg.vqr",This,this.w_FILTERCURSOR) 
 Delete From "dati" where GACODICE not in (select GACODICE from (this.w_FILTERCURSOR)) 
 Use in (this.w_FILTERCURSOR) 
 Select(m.l_oldArea)
          endif
          * --- Pulisco la testata delle righe successive alla prima
          this.w_ROTTURA = "0000000000"
          l_oldArea = Select() 
 Select("dati") 
 Go Top 
 Scan
          if this.w_ROTTURA==GACODICE
            Replace GA__NOME with "", GADESCRI with "", GASRCPRG with "", GAGRPCOD with "", GACNDATT with "", GACHKATT with "", GAFORAZI with "", GAUTEESC with 0
          else
            this.w_ROTTURA = GACODICE
          endif
          Select("dati") 
 EndScan 
 Select(m.l_oldArea)
        case this.oParentObject.w_TIPARC="EK"
          vq_exec("query\ELABKPI.vqr",this,"dati") 
 Update "dati" Set EKDESCRI = Strtran(EKDESCRI, '"', (this.w_DATRASCO)), EKCNDATT = Strtran(EKCNDATT, '"', (this.w_DATRASCO)), EKVALORE = Strtran(EKVALORE, '"', (this.w_DATRASCO))
        case this.oParentObject.w_TIPARC="TK"
          vq_exec("query\TIMERKPI.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="EE"
          vq_exec("query\ELE_EXPR.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="IL"
          vq_exec("..\revi\exe\query\gsrv_bco.vqr",this,"dati")
        case this.oParentObject.w_TIPARC="IQ"
          vq_exec("..\revi\exe\query\gsrv_bcq.vqr",this,"dati")
      endcase
      * --- Salvo il cursore nel file
      do case
        case this.oParentObject.w_TIPARC="MC"
          FH=FCREATE(this.oParentObject.w_PATH)
          if FH>0
            SELECT "dati"
            GO TOP
            SCAN
            this.w_ERRNUM = ALLTRIM(STR(ercodice))
            this.w_ERRDESCRI = ALLTRIM(erdescri)
            this.w_ERRMESS = ALLTRIM(ermessag)
            this.w_ERRNOTE = ALLTRIM(er__note)
            this.w_ERRNOTE = STRTRAN(this.w_ERRNOTE,CHR(13),"+CHR(13)")
            this.w_ERRNOTE = STRTRAN(this.w_ERRNOTE,CHR(10),"+CHR(10)+")
            this.w_OUTSTR = ALLTRIM('"'+this.w_ERRNUM+'","'+this.w_ERRDESCRI+'","'+this.w_ERRMESS+'","'+this.w_ERRNOTE+'"')
            FW=FPUTS(FH,this.w_OUTSTR,LEN(this.w_OUTSTR))
            ENDSCAN
            FCLOSE(FH)
          else
            ah_ErrorMsg("Errore apertura file di output","!","")
          endif
        case this.oParentObject.w_TIPARC="TI"
          COPY TO (this.oParentObject.w_PATH ) TYPE DELIMITED WITH CHARACTER @
        case this.oParentObject.w_TIPARC="CD" OR this.oParentObject.w_TIPARC="PD" OR this.oParentObject.w_TIPARC="CL" 
          COPY TO (this.oParentObject.w_PATH ) TYPE DELIMITED WITH "*"
        case this.oParentObject.w_TIPARC="MO"
          GSOF_BIE(this,this.oParentObject.w_PATH,this.pTipo)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case inlist(this.oParentObject.w_TIPARC,"GG","EE","IL","IQ")
          StrToFile(cp_JsonEncodeCursor("dati"),this.oParentObject.w_PATH)
        otherwise
          if (this.oParentObject.w_TIPARC="TF" AND this.oParentObject.w_TIPMDF="CU") OR this.oParentObject.w_TIPARC="RM" OR this.oParentObject.w_TIPARC="EK"
            * --- Nel caso di contributo unificato il separatore deve essere . dato che esportiamo ed importiamo valori numerici con decimali
            this.OldPoint = SET("POINT")
            SET POINT TO "."
          endif
          COPY TO (this.oParentObject.w_PATH ) TYPE DELIMITED
          if (this.oParentObject.w_TIPARC="TF" AND this.oParentObject.w_TIPMDF="CU") OR this.oParentObject.w_TIPARC="RM" OR this.oParentObject.w_TIPARC="EK"
            SET POINT TO this.OldPoint
          endif
      endcase
      ah_ErrorMsg("Salvataggio completato",,"")
    endif
    USE IN SELECT("Dati")
    USE IN SELECT("Dati2")
    USE IN SELECT("TEMPO")
  endproc
  proc Try_04B94698()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    do case
      case this.oParentObject.w_TIPARC $ "AB-CA"
        * --- ABI-CAB Banche
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="NM"
        * --- Nomenclature
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="UM"
        * --- U.M. Supplementari
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="TF"
        * --- Tabelle F24
        do case
          case this.oParentObject.w_TIPMDF="TR"
            this.Page_6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_TIPMDF $ "RP-EL"
            this.Page_7()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_TIPMDF $ "AE-SP"
            this.Page_8()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_TIPMDF $ "CI-CA"
            this.Page_9()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_TIPMDF="SI"
            this.Page_10()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_TIPMDF="CU"
            * --- F23 - Contributo unificato
            this.Page_35()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_TIPMDF="CT"
            * --- F23 -Codici tributo
            this.Page_35()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_TIPMDF="CC"
            * --- F23 - Codici contenzioso
            this.Page_35()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          case this.oParentObject.w_TIPMDF="CF"
            * --- F23 Causali 
            this.Page_35()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
        endcase
      case this.oParentObject.w_TIPARC="CP"
        * --- CAP e provincie
        this.Page_11()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="RE"
        * --- Regole di importazioni (INFOLINK)
        this.Page_12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="GR"
        * --- Gruppi di Regole (INFOLINK)
        this.Page_13()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="GS"
        * --- Gestioni Schedulatore
        this.Page_14()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="SF"
        * --- Configurazione Stampa su File
        this.Page_15()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC$"IR-GI"
        this.Page_16()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="RB"
        * --- Regole analisi di bilancio
        if g_BILC="S"
          * --- Controllo se � gestito il bilancio
          this.Page_17()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          ah_ErrorMsg("Analisi di bilancio non gestita","!","")
          * --- Raise
          i_Error="Import non consentito"
          return
        endif
      case this.oParentObject.w_TIPARC="SB"
        * --- Impotazione Struttura di Bilancio UE
        this.Page_18()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC $ "VB-VF-BO"
        this.Page_19()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="CC"
        this.Page_20()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="CF"
        this.Page_49()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="MC"
        this.Page_21()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="TA"
        this.Page_22()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="EA"
        this.Page_23()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="PO"
        this.Page_24()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="PR"
        this.Page_25()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="TI"
        this.Page_26()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="CD" or this.oParentObject.w_TIPARC="CL"
        this.Page_27()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="PD"
        this.Page_28()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="CG"
        this.Page_29()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="MG"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="MO"
        GSOF_BIE(this,this.oParentObject.w_PATH,this.pTipo)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="DM"
        * --- Modelli documenti
        this.Page_30()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="UC"
        * --- Uffici competenti relativi al comune
        this.Page_31()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="RF"
        * --- RSS Feed
        this.Page_47()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    do case
      case this.oParentObject.w_TIPARC="RM"
        this.Page_32()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="RG"
        this.Page_33()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="TC"
        this.Page_34()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="UG"
        this.Page_36()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="UP"
        this.Page_37()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="EP"
        this.Page_38()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="OP"
        this.Page_39()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="MP"
        this.Page_40()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="TP"
        this.Page_41()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="RP"
        this.Page_42()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="TT"
        this.Page_43()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="AC"
        this.Page_44()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="MS"
        this.Page_45()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="SS"
        this.Page_46()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC$ "DE-TE"
        gsag_bsc(this,this.oParentObject.w_PATH,this.oParentObject.w_TIPARC)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="OU" OR this.oParentObject.w_TIPARC="OM"
        * --- Campi Outlook
        this.Page_48()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="RT"
        * --- Raggruppamenti tipi file
        this.Page_50()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="MR"
        * --- Modelli report wizard
        this.Page_51()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="FE"
        * --- Festivit�
        this.Page_52()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="PU"
        this.Page_53()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="PG"
        this.Page_54()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="AU"
        this.Page_55()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="SO"
        * --- Causali Remote Banking
        this.Page_56()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="OR"
        this.Page_57()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="FT"
        this.Page_58()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="DP"
        this.Page_59()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="CS"
        * --- Causali cespiti
        this.Page_60()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="BU"
        * --- Tipologie atto per busta telematica
        this.Page_61()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="GP"
        this.Page_67()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="TG"
        this.Page_68()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="GM"
        this.Page_62()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="IG"
        this.Page_63()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="GG"
        this.Page_64()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="EK"
        this.Page_65()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="TK"
        this.Page_66()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="EE"
        this.Page_69()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_TIPARC="IL"
        * --- w_TIPARC='IL' Infinity Link
        cp_JsonParseCursor(FileToStr(this.oParentObject.w_PATH), "TEMPO")
        if USED("TEMPO")
          INSUPDTABLE("TEMPO","GESTINFY","A",.F.,.T.)
        else
          * --- Raise
          i_Error="Impossibile aprire il file di origine"
          return
        endif
      case this.oParentObject.w_TIPARC="IQ"
        * --- w_TIPARC='IQ' I.Revolution Query di controllo dati
        cp_JsonParseCursor(FileToStr(this.oParentObject.w_PATH), "TEMPO")
        if USED("TEMPO")
          INSUPDTABLE("TEMPO","QERYCONT","A",.F.,.T.)
        else
          * --- Raise
          i_Error="Impossibile aprire il file di origine"
          return
        endif
    endcase
    * --- commit
    cp_EndTrs(.t.)
    * --- Se carico Output utente da procedura di conversione non do segnali a video..
    if Not this.w_NOMSG
      ah_ErrorMsg("Import completato",,"")
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Import Modelli gestioni
    * --- Caricamento cfg gestioni
    CREATE CURSOR TEMPO (MCNOMGES C(30), MCDESGES C(50), CPROWNUM N(6,0), MCNOMVAR C(20), MCDESVAR C(254))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_ERRORGRU = .F.
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE) 
 GO TOP 
 SCAN FOR NOT EMPTY(NVL(MCNOMGES," "))
    if nvl(TEMPO.MCNOMGES,"") = this.w_GSNOME
      this.w_FLMD = "D"
    else
      this.w_FLMD = "M"
    endif
    this.w_GSNOME = nvl(TEMPO.MCNOMGES,"")
    this.w_GSNOME = STRTRAN(this.w_GSNOME, "#", '"')
    this.w_GSDESC = nvl(TEMPO.MCDESGES ,"")
    this.w_GSNOMVAL = nvl(TEMPO.MCNOMVAR,"")
    this.w_GSNOMDES = nvl(TEMPO.MCDESVAR,"")
    this.w_CPROWNUM = nvl(TEMPO.CPROWNUM,0)
    if this.w_FLMD="M"
      * --- Testata
      * --- Try
      local bErr_04B03AD0
      bErr_04B03AD0=bTrsErr
      this.Try_04B03AD0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_04B02510
        bErr_04B02510=bTrsErr
        this.Try_04B02510()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_ERRORGRU = .T.
        endif
        bTrsErr=bTrsErr or bErr_04B02510
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_04B03AD0
      * --- End
      * --- Dettaglio
      * --- Try
      local bErr_04B03A10
      bErr_04B03A10=bTrsErr
      this.Try_04B03A10()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_04B037D0
        bErr_04B037D0=bTrsErr
        this.Try_04B037D0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_ERRORGRU = .T.
        endif
        bTrsErr=bTrsErr or bErr_04B037D0
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_04B03A10
      * --- End
    else
      * --- Dettaglio
      * --- Try
      local bErr_04B01850
      bErr_04B01850=bTrsErr
      this.Try_04B01850()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_04B01250
        bErr_04B01250=bTrsErr
        this.Try_04B01250()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_ERRORGRU = .T.
        endif
        bTrsErr=bTrsErr or bErr_04B01250
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_04B01850
      * --- End
    endif
    if this.w_ERRORGRU
      * --- Raise
      i_Error=ah_Msgformat("Import fallito")
      return
    endif
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_04B03AD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MODMCONF
    i_nConn=i_TableProp[this.MODMCONF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODMCONF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MODMCONF_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MCNOMGES"+",MCDESGES"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GSNOME),'MODMCONF','MCNOMGES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSDESC),'MODMCONF','MCDESGES');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MCNOMGES',this.w_GSNOME,'MCDESGES',this.w_GSDESC)
      insert into (i_cTable) (MCNOMGES,MCDESGES &i_ccchkf. );
         values (;
           this.w_GSNOME;
           ,this.w_GSDESC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04B02510()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into MODMCONF
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MODMCONF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODMCONF_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MODMCONF_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MCDESGES ="+cp_NullLink(cp_ToStrODBC(this.w_GSDESC),'MODMCONF','MCDESGES');
          +i_ccchkf ;
      +" where ";
          +"MCNOMGES = "+cp_ToStrODBC(this.w_GSNOME);
             )
    else
      update (i_cTable) set;
          MCDESGES = this.w_GSDESC;
          &i_ccchkf. ;
       where;
          MCNOMGES = this.w_GSNOME;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04B03A10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MODDCONF
    i_nConn=i_TableProp[this.MODDCONF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODDCONF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MODDCONF_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MCNOMGES"+",CPROWNUM"+",MCNOMVAR"+",MCDESVAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GSNOME),'MODDCONF','MCNOMGES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MODDCONF','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSNOMVAL),'MODDCONF','MCNOMVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSNOMDES),'MODDCONF','MCDESVAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MCNOMGES',this.w_GSNOME,'CPROWNUM',this.w_CPROWNUM,'MCNOMVAR',this.w_GSNOMVAL,'MCDESVAR',this.w_GSNOMDES)
      insert into (i_cTable) (MCNOMGES,CPROWNUM,MCNOMVAR,MCDESVAR &i_ccchkf. );
         values (;
           this.w_GSNOME;
           ,this.w_CPROWNUM;
           ,this.w_GSNOMVAL;
           ,this.w_GSNOMDES;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04B037D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into MODDCONF
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MODDCONF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODDCONF_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MODDCONF_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MCNOMVAR ="+cp_NullLink(cp_ToStrODBC(this.w_GSNOMVAL),'MODDCONF','MCNOMVAR');
      +",MCDESVAR ="+cp_NullLink(cp_ToStrODBC(this.w_GSNOMDES),'MODDCONF','MCDESVAR');
          +i_ccchkf ;
      +" where ";
          +"MCNOMGES = "+cp_ToStrODBC(this.w_GSNOME);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          MCNOMVAR = this.w_GSNOMVAL;
          ,MCDESVAR = this.w_GSNOMDES;
          &i_ccchkf. ;
       where;
          MCNOMGES = this.w_GSNOME;
          and CPROWNUM = this.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04B01850()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MODDCONF
    i_nConn=i_TableProp[this.MODDCONF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODDCONF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MODDCONF_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MCNOMGES"+",CPROWNUM"+",MCNOMVAR"+",MCDESVAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GSNOME),'MODDCONF','MCNOMGES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MODDCONF','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSNOMVAL),'MODDCONF','MCNOMVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSNOMDES),'MODDCONF','MCDESVAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MCNOMGES',this.w_GSNOME,'CPROWNUM',this.w_CPROWNUM,'MCNOMVAR',this.w_GSNOMVAL,'MCDESVAR',this.w_GSNOMDES)
      insert into (i_cTable) (MCNOMGES,CPROWNUM,MCNOMVAR,MCDESVAR &i_ccchkf. );
         values (;
           this.w_GSNOME;
           ,this.w_CPROWNUM;
           ,this.w_GSNOMVAL;
           ,this.w_GSNOMDES;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04B01250()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into MODDCONF
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MODDCONF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MODDCONF_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MODDCONF_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MCNOMVAR ="+cp_NullLink(cp_ToStrODBC(this.w_GSNOMVAL),'MODDCONF','MCNOMVAR');
      +",MCDESVAR ="+cp_NullLink(cp_ToStrODBC(this.w_GSNOMDES),'MODDCONF','MCDESVAR');
          +i_ccchkf ;
      +" where ";
          +"MCNOMGES = "+cp_ToStrODBC(this.w_GSNOME);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          MCNOMVAR = this.w_GSNOMVAL;
          ,MCDESVAR = this.w_GSNOMDES;
          &i_ccchkf. ;
       where;
          MCNOMGES = this.w_GSNOME;
          and CPROWNUM = this.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Codici ABI/CAB
    if this.oParentObject.w_TIPARC="AB"
      CREATE CURSOR TEMPO (CODABI C(5), DESABI C (80))
    else
      CREATE CURSOR TEMPO (CODABI C(5), CODCAB C (5), DESFIL C(40), INDIRI C(50), CAP C(8))
    endif
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_CODABI = SPACE(5)
    this.w_DESABI = SPACE(80)
    this.w_CODCAB = SPACE(5)
    this.w_DESFIL = SPACE(40)
    this.w_INDIRI = SPACE(50)
    this.w_CAP = SPACE(8)
    * --- Gestione obsolescenza...
    if this.oParentObject.w_ABICABOBSO="S"
      * --- Imposto alla data di oggi la data osbolescenza dei codice ABI/CAB che
      *     non hanno il campo valorizzato...
      if this.oParentObject.w_TIPARC="AB"
        * --- Write into COD_ABI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.COD_ABI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COD_ABI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.COD_ABI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ABDTOBSO ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'COD_ABI','ABDTOBSO');
              +i_ccchkf ;
          +" where ";
              +"ABDTOBSO is null ";
                 )
        else
          update (i_cTable) set;
              ABDTOBSO = i_DATSYS;
              &i_ccchkf. ;
           where;
              ABDTOBSO is null;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- Write into COD_CAB
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.COD_CAB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COD_CAB_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.COD_CAB_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FIDTOBSO ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'COD_CAB','FIDTOBSO');
              +i_ccchkf ;
          +" where ";
              +"FIDTOBSO is null ";
                 )
        else
          update (i_cTable) set;
              FIDTOBSO = i_DATSYS;
              &i_ccchkf. ;
           where;
              FIDTOBSO is null;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODABI," "))
    * --- Inserisco...
    this.w_CODABI = TEMPO.CODABI
    if this.oParentObject.w_TIPARC="AB"
      this.w_DESABI = NVL(TEMPO.DESABI, SPACE(80))
    else
      this.w_CODCAB = NVL(TEMPO.CODCAB, SPACE(5))
      this.w_DESFIL = STRTRAN(NVL(TEMPO.DESFIL, SPACE(40)), "|",'"')
      this.w_INDIRI = STRTRAN(NVL(TEMPO.INDIRI, SPACE(50)), "|",'"')
      this.w_CAP = NVL(TEMPO.CAP, SPACE(8))
    endif
    if this.oParentObject.w_TIPARC="AB" OR NOT EMPTY(this.w_CODCAB)
      * --- Try
      local bErr_04AFDCE0
      bErr_04AFDCE0=bTrsErr
      this.Try_04AFDCE0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_04AFC3F0
        bErr_04AFC3F0=bTrsErr
        this.Try_04AFC3F0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          if this.oParentObject.w_TIPARC="CA"
            this.w_MESS = "Errore di scrittura ABI: %1 CAB: %2%0Continuo?"
          else
            this.w_MESS = "Errore di scrittura ABI: %1%0Continuo?"
          endif
          if not AH_YESNO(this.w_MESS,,this.w_CODABI,this.w_CODCAB)
            * --- Raise
            i_Error="Import fallito"
            return
          else
            * --- accept error
            bTrsErr=.f.
          endif
        endif
        bTrsErr=bTrsErr or bErr_04AFC3F0
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_04AFDCE0
      * --- End
    endif
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_04AFDCE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_TIPARC="AB"
      * --- Insert into COD_ABI
      i_nConn=i_TableProp[this.COD_ABI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COD_ABI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COD_ABI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"ABCODABI"+",ABDESABI"+",ABDTOBSO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CODABI),'COD_ABI','ABCODABI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESABI),'COD_ABI','ABDESABI');
        +","+cp_NullLink(cp_ToStrODBC(Cp_CharTodate( "  -  -    " )),'COD_ABI','ABDTOBSO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'ABCODABI',this.w_CODABI,'ABDESABI',this.w_DESABI,'ABDTOBSO',Cp_CharTodate( "  -  -    " ))
        insert into (i_cTable) (ABCODABI,ABDESABI,ABDTOBSO &i_ccchkf. );
           values (;
             this.w_CODABI;
             ,this.w_DESABI;
             ,Cp_CharTodate( "  -  -    " );
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Insert into COD_CAB
      i_nConn=i_TableProp[this.COD_CAB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COD_CAB_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COD_CAB_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"FICODABI"+",FICODCAB"+",FIDESFIL"+",FIINDIRI"+",FI___CAP"+",FIDTOBSO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CODABI),'COD_CAB','FICODABI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODCAB),'COD_CAB','FICODCAB');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESFIL),'COD_CAB','FIDESFIL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_INDIRI),'COD_CAB','FIINDIRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAP),'COD_CAB','FI___CAP');
        +","+cp_NullLink(cp_ToStrODBC(Cp_CharTodate( "  -  -    " )),'COD_CAB','FIDTOBSO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'FICODABI',this.w_CODABI,'FICODCAB',this.w_CODCAB,'FIDESFIL',this.w_DESFIL,'FIINDIRI',this.w_INDIRI,'FI___CAP',this.w_CAP,'FIDTOBSO',Cp_CharTodate( "  -  -    " ))
        insert into (i_cTable) (FICODABI,FICODCAB,FIDESFIL,FIINDIRI,FI___CAP,FIDTOBSO &i_ccchkf. );
           values (;
             this.w_CODABI;
             ,this.w_CODCAB;
             ,this.w_DESFIL;
             ,this.w_INDIRI;
             ,this.w_CAP;
             ,Cp_CharTodate( "  -  -    " );
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    if this.oParentObject.w_TIPARC="CA"
      ah_msg("Inserisco ABI: %1 CAB: %2",.t.,,,this.w_CODABI,this.w_CODCAB)
    else
      ah_msg("Inserisco ABI: %1",.t.,,,this.w_CODABI)
    endif
    return
  proc Try_04AFC3F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_TIPARC="AB"
      * --- Write into COD_ABI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.COD_ABI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COD_ABI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.COD_ABI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ABDESABI ="+cp_NullLink(cp_ToStrODBC(this.w_DESABI),'COD_ABI','ABDESABI');
        +",ABDTOBSO ="+cp_NullLink(cp_ToStrODBC(Cp_CharTodate( "  -  -    " )),'COD_ABI','ABDTOBSO');
            +i_ccchkf ;
        +" where ";
            +"ABCODABI = "+cp_ToStrODBC(this.w_CODABI);
               )
      else
        update (i_cTable) set;
            ABDESABI = this.w_DESABI;
            ,ABDTOBSO = Cp_CharTodate( "  -  -    " );
            &i_ccchkf. ;
         where;
            ABCODABI = this.w_CODABI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into COD_CAB
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.COD_CAB_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COD_CAB_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.COD_CAB_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FIDESFIL ="+cp_NullLink(cp_ToStrODBC(this.w_DESFIL),'COD_CAB','FIDESFIL');
        +",FIINDIRI ="+cp_NullLink(cp_ToStrODBC(this.w_INDIRI),'COD_CAB','FIINDIRI');
        +",FI___CAP ="+cp_NullLink(cp_ToStrODBC(this.w_CAP),'COD_CAB','FI___CAP');
        +",FIDTOBSO ="+cp_NullLink(cp_ToStrODBC(Cp_CharTodate( "  -  -    " )),'COD_CAB','FIDTOBSO');
            +i_ccchkf ;
        +" where ";
            +"FICODABI = "+cp_ToStrODBC(this.w_CODABI);
            +" and FICODCAB = "+cp_ToStrODBC(this.w_CODCAB);
               )
      else
        update (i_cTable) set;
            FIDESFIL = this.w_DESFIL;
            ,FIINDIRI = this.w_INDIRI;
            ,FI___CAP = this.w_CAP;
            ,FIDTOBSO = Cp_CharTodate( "  -  -    " );
            &i_ccchkf. ;
         where;
            FICODABI = this.w_CODABI;
            and FICODCAB = this.w_CODCAB;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.oParentObject.w_TIPARC="CA"
      ah_msg("Scrivo ABI: %1 CAB: %2",.t.,,,this.w_CODABI,this.w_CODCAB)
    else
      ah_msg("Scrivo ABI: %1",.t.,,,this.w_CODABI)
    endif
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Nomenclature
    CREATE CURSOR TEMPO (NMCODI C(8), NMDESC C (51), NMUMSU C(3))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_NMCODI = SPACE(8)
    this.w_NMDESC = SPACE(51)
    this.w_NMUMSU = SPACE(3)
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(NMCODI," "))
    * --- Inserisco...
    this.w_NMCODI = TEMPO.NMCODI
    this.w_NMDESC = STRTRAN(NVL(TEMPO.NMDESC, SPACE(51)), "|",'"')
    this.w_NMUMSU = NVL(TEMPO.NMUMSU, SPACE(3))
    * --- Try
    local bErr_04ADFEC0
    bErr_04ADFEC0=bTrsErr
    this.Try_04ADFEC0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_04ADEED0
      bErr_04ADEED0=bTrsErr
      this.Try_04ADEED0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura in codice nomenclatura: %1%0Continuo?","", this.w_NMCODI)
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_04ADEED0
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_04ADFEC0
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_04ADFEC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into NOMENCLA
    i_nConn=i_TableProp[this.NOMENCLA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.NOMENCLA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.NOMENCLA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"NMCODICE"+",NMDESCRI"+",NMUNISUP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_NMCODI),'NOMENCLA','NMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NMDESC),'NOMENCLA','NMDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NMUMSU),'NOMENCLA','NMUNISUP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'NMCODICE',this.w_NMCODI,'NMDESCRI',this.w_NMDESC,'NMUNISUP',this.w_NMUMSU)
      insert into (i_cTable) (NMCODICE,NMDESCRI,NMUNISUP &i_ccchkf. );
         values (;
           this.w_NMCODI;
           ,this.w_NMDESC;
           ,this.w_NMUMSU;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco codice nomenclatura: %1",.t.,,,this.w_NMCODI )
    return
  proc Try_04ADEED0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into NOMENCLA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.NOMENCLA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.NOMENCLA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.NOMENCLA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"NMDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_NMDESC),'NOMENCLA','NMDESCRI');
      +",NMUNISUP ="+cp_NullLink(cp_ToStrODBC(this.w_NMUMSU),'NOMENCLA','NMUNISUP');
          +i_ccchkf ;
      +" where ";
          +"NMCODICE = "+cp_ToStrODBC(this.w_NMCODI);
             )
    else
      update (i_cTable) set;
          NMDESCRI = this.w_NMDESC;
          ,NMUNISUP = this.w_NMUMSU;
          &i_ccchkf. ;
       where;
          NMCODICE = this.w_NMCODI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Inserisco codice nomenclatura: %1",.t.,,,this.w_NMCODI )
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa UM Supplementari
    CREATE CURSOR TEMPO (UMCODI C(3), UMDESC C (35), UMFRAZ C(1))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_UMCODI = SPACE(3)
    this.w_UMDESC = SPACE(35)
    this.w_UMFRAZ = SPACE(1)
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(UMCODI," "))
    * --- Inserisco...
    this.w_UMCODI = TEMPO.UMCODI
    this.w_UMDESC = NVL(TEMPO.UMDESC, SPACE(35))
    this.w_UMFRAZ = NVL(TEMPO.UMFRAZ, " ")
    * --- Try
    local bErr_04ADBD50
    bErr_04ADBD50=bTrsErr
    this.Try_04ADBD50()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_04ADB720
      bErr_04ADB720=bTrsErr
      this.Try_04ADB720()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura in codice U.M.: %1%0Continuo?","", this.w_UMCODI )
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_04ADB720
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_04ADBD50
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_04ADBD50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into UNIMIS
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.UNIMIS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"UMCODICE"+",UMDESCRI"+",UMFLFRAZ"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_UMCODI),'UNIMIS','UMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UMDESC),'UNIMIS','UMDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UMFRAZ),'UNIMIS','UMFLFRAZ');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'UMCODICE',this.w_UMCODI,'UMDESCRI',this.w_UMDESC,'UMFLFRAZ',this.w_UMFRAZ)
      insert into (i_cTable) (UMCODICE,UMDESCRI,UMFLFRAZ &i_ccchkf. );
         values (;
           this.w_UMCODI;
           ,this.w_UMDESC;
           ,this.w_UMFRAZ;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco codice U.M.: %1",.t.,,,this.w_UMCODI)
    return
  proc Try_04ADB720()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into UNIMIS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.UNIMIS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"UMDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_UMDESC),'UNIMIS','UMDESCRI');
      +",UMFLFRAZ ="+cp_NullLink(cp_ToStrODBC(this.w_UMFRAZ),'UNIMIS','UMFLFRAZ');
          +i_ccchkf ;
      +" where ";
          +"UMCODICE = "+cp_ToStrODBC(this.w_UMCODI);
             )
    else
      update (i_cTable) set;
          UMDESCRI = this.w_UMDESC;
          ,UMFLFRAZ = this.w_UMFRAZ;
          &i_ccchkf. ;
       where;
          UMCODICE = this.w_UMCODI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Scrivo codice U.M.: %1",.t.,,,this.w_UMCODI)
    return


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Codici Tributo
    CREATE CURSOR TEMPO (TRICODI C(5), TRIDES C(200), TRITIP C(10), TRMERIF C(1), TRIFCODA C(1))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_TRICODI = SPACE(5)
    this.w_TRIDES = SPACE(200)
    this.w_TRITIP = SPACE(10)
    this.w_TRMERIF = SPACE(1)
    this.w_TRIFCODA = SPACE(1)
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(TRICODI," "))
    * --- Inserisco...
    this.w_TRICODI = TEMPO.TRICODI
    this.w_TRIDES = NVL(TEMPO.TRIDES," ")
    this.w_TRITIP = NVL(TEMPO.TRITIP, " ")
    this.w_TRMERIF = NVL(TEMPO.TRMERIF,"N")
    this.w_TRIFCODA = NVL(TEMPO.TRIFCODA,"N")
    * --- Try
    local bErr_04AD8120
    bErr_04AD8120=bTrsErr
    this.Try_04AD8120()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_04AD7010
      bErr_04AD7010=bTrsErr
      this.Try_04AD7010()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura in codice: %1%0Continuo?","",this.w_TRICODI)
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_04AD7010
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_04AD8120
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_04AD8120()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into COD_TRIB
    i_nConn=i_TableProp[this.COD_TRIB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COD_TRIB_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COD_TRIB_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESCRI"+",TRTIPTRI"+",TRFLMERF"+",TRFLCODA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TRICODI),'COD_TRIB','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TRIDES),'COD_TRIB','TRDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TRITIP),'COD_TRIB','TRTIPTRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TRMERIF),'COD_TRIB','TRFLMERF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TRIFCODA),'COD_TRIB','TRFLCODA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',this.w_TRICODI,'TRDESCRI',this.w_TRIDES,'TRTIPTRI',this.w_TRITIP,'TRFLMERF',this.w_TRMERIF,'TRFLCODA',this.w_TRIFCODA)
      insert into (i_cTable) (TRCODICE,TRDESCRI,TRTIPTRI,TRFLMERF,TRFLCODA &i_ccchkf. );
         values (;
           this.w_TRICODI;
           ,this.w_TRIDES;
           ,this.w_TRITIP;
           ,this.w_TRMERIF;
           ,this.w_TRIFCODA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco codice: %1",.t.,,,this.w_TRICODI)
    return
  proc Try_04AD7010()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into COD_TRIB
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.COD_TRIB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COD_TRIB_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.COD_TRIB_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TRDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_TRIDES),'COD_TRIB','TRDESCRI');
      +",TRTIPTRI ="+cp_NullLink(cp_ToStrODBC(this.w_TRITIP),'COD_TRIB','TRTIPTRI');
      +",TRFLMERF ="+cp_NullLink(cp_ToStrODBC(this.w_TRMERIF),'COD_TRIB','TRFLMERF');
      +",TRFLCODA ="+cp_NullLink(cp_ToStrODBC(this.w_TRIFCODA),'COD_TRIB','TRFLCODA');
          +i_ccchkf ;
      +" where ";
          +"TRCODICE = "+cp_ToStrODBC(this.w_TRICODI);
             )
    else
      update (i_cTable) set;
          TRDESCRI = this.w_TRIDES;
          ,TRTIPTRI = this.w_TRITIP;
          ,TRFLMERF = this.w_TRMERIF;
          ,TRFLCODA = this.w_TRIFCODA;
          &i_ccchkf. ;
       where;
          TRCODICE = this.w_TRICODI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Scrivo codice: %1",.t.,,,this.w_TRICODI)
    return


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Regioni e Province - Importa Enti Locali
    CREATE CURSOR TEMPO (RECODI C(4), REDES C(30))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_RECODI = SPACE(4)
    this.w_REDES = SPACE(30)
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(RECODI," "))
    * --- Inserisco...
    this.w_RECODI = ALLTRIM(TEMPO.RECODI)
    this.w_REDES = NVL(TEMPO.REDES," ")
    * --- Try
    local bErr_04AE4EA0
    bErr_04AE4EA0=bTrsErr
    this.Try_04AE4EA0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_04AE3A30
      bErr_04AE3A30=bTrsErr
      this.Try_04AE3A30()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura in codice: %1%0Continuo?","", this.w_RECODI)
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_04AE3A30
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_04AE4EA0
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_04AE4EA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_TIPMDF="RP"
      * --- Insert into REG_PROV
      i_nConn=i_TableProp[this.REG_PROV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.REG_PROV_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.REG_PROV_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"RPCODICE"+",RPDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_RECODI),'REG_PROV','RPCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_REDES),'REG_PROV','RPDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'RPCODICE',this.w_RECODI,'RPDESCRI',this.w_REDES)
        insert into (i_cTable) (RPCODICE,RPDESCRI &i_ccchkf. );
           values (;
             this.w_RECODI;
             ,this.w_REDES;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Insert into ENTI_COM
      i_nConn=i_TableProp[this.ENTI_COM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ENTI_COM_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ENTI_COM_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"ECCODICE"+",ECTERRIT"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_RECODI),'ENTI_COM','ECCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_REDES),'ENTI_COM','ECTERRIT');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'ECCODICE',this.w_RECODI,'ECTERRIT',this.w_REDES)
        insert into (i_cTable) (ECCODICE,ECTERRIT &i_ccchkf. );
           values (;
             this.w_RECODI;
             ,this.w_REDES;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    ah_msg("Inserisco codice: %1",.t.,,,this.w_RECODI)
    return
  proc Try_04AE3A30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_TIPMDF="RP"
      * --- Write into REG_PROV
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.REG_PROV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.REG_PROV_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.REG_PROV_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"RPDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_REDES),'REG_PROV','RPDESCRI');
            +i_ccchkf ;
        +" where ";
            +"RPCODICE = "+cp_ToStrODBC(this.w_RECODI);
               )
      else
        update (i_cTable) set;
            RPDESCRI = this.w_REDES;
            &i_ccchkf. ;
         where;
            RPCODICE = this.w_RECODI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into ENTI_COM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ENTI_COM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ENTI_COM_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ENTI_COM_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ECTERRIT ="+cp_NullLink(cp_ToStrODBC(this.w_REDES),'ENTI_COM','ECTERRIT');
            +i_ccchkf ;
        +" where ";
            +"ECCODICE = "+cp_ToStrODBC(this.w_RECODI);
               )
      else
        update (i_cTable) set;
            ECTERRIT = this.w_REDES;
            &i_ccchkf. ;
         where;
            ECCODICE = this.w_RECODI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    ah_msg("Scrivo codice: %1",.t.,,,this.w_RECODI)
    return


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Altri Enti Previdenziali - Sedi INPS
    CREATE CURSOR TEMPO (AECODI C(5), AEDES C(30))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_AECODI = SPACE(5)
    this.w_AEDES = SPACE(30)
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(AECODI," "))
    * --- Inserisco...
    this.w_AECODI = TEMPO.AECODI
    this.w_AEDES = NVL(TEMPO.AEDES," ")
    * --- Try
    local bErr_0301F158
    bErr_0301F158=bTrsErr
    this.Try_0301F158()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_0301DD18
      bErr_0301DD18=bTrsErr
      this.Try_0301DD18()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura in codice: %1%0Continuo?","",this.w_AECODI)
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_0301DD18
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_0301F158
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_0301F158()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_TIPMDF="AE"
      * --- Insert into COD_PREV
      i_nConn=i_TableProp[this.COD_PREV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COD_PREV_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COD_PREV_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"CPCODICE"+",CPDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_AECODI),'COD_PREV','CPCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_AEDES),'COD_PREV','CPDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'CPCODICE',this.w_AECODI,'CPDESCRI',this.w_AEDES)
        insert into (i_cTable) (CPCODICE,CPDESCRI &i_ccchkf. );
           values (;
             this.w_AECODI;
             ,this.w_AEDES;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Insert into SED_INPS
      i_nConn=i_TableProp[this.SED_INPS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SED_INPS_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SED_INPS_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PSCODICE"+",PSDESCRI"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_AECODI),'SED_INPS','PSCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_AEDES),'SED_INPS','PSDESCRI');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PSCODICE',this.w_AECODI,'PSDESCRI',this.w_AEDES)
        insert into (i_cTable) (PSCODICE,PSDESCRI &i_ccchkf. );
           values (;
             this.w_AECODI;
             ,this.w_AEDES;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    ah_msg("Inserisco codice: %1",.t.,,,this.w_AECODI)
    return
  proc Try_0301DD18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.oParentObject.w_TIPMDF="AE"
      * --- Write into COD_PREV
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.COD_PREV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COD_PREV_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.COD_PREV_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CPDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_AEDES),'COD_PREV','CPDESCRI');
            +i_ccchkf ;
        +" where ";
            +"CPCODICE = "+cp_ToStrODBC(this.w_AECODI);
               )
      else
        update (i_cTable) set;
            CPDESCRI = this.w_AEDES;
            &i_ccchkf. ;
         where;
            CPCODICE = this.w_AECODI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into SED_INPS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SED_INPS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SED_INPS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SED_INPS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PSDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_AEDES),'SED_INPS','PSDESCRI');
            +i_ccchkf ;
        +" where ";
            +"PSCODICE = "+cp_ToStrODBC(this.w_AECODI);
               )
      else
        update (i_cTable) set;
            PSDESCRI = this.w_AEDES;
            &i_ccchkf. ;
         where;
            PSCODICE = this.w_AECODI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    ah_msg("Scrivo codice: %1",.t.,,,this.w_AECODI)
    return


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Causali Contributo - INPS, Altri Enti -
    if this.oParentObject.w_TIPMDF="CI"
      CREATE CURSOR TEMPO (CSCAU C(5), CSDES C(60), CSPINI C(40), CSPFIN C(40))
    else
      CREATE CURSOR TEMPO (CAENTE C(5), CACAU C(5), CADES C(40), CAINI C(30), CAFIN C(30))
    endif
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    if this.oParentObject.w_TIPMDF="CI"
      this.w_CSCAU = SPACE(5)
      this.w_CSDES = SPACE(60)
      this.w_CSPINI = SPACE(40)
      this.w_CSPFIN = SPACE(40)
    else
      this.w_CAENTE = SPACE(5)
      this.w_CACAU = SPACE(5)
      this.w_CADES = SPACE(40)
      this.w_CAINI = SPACE(30)
      this.w_CAFIN = SPACE(30)
    endif
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    if this.oParentObject.w_TIPMDF="CI"
      SCAN FOR NOT EMPTY(NVL(CSCAU," "))
      * --- Inserisco...
      this.w_CSCAU = TEMPO.CSCAU
      this.w_CSDES = NVL(TEMPO.CSDES," ")
      this.w_CSPINI = NVL(TEMPO.CSPINI," ")
      this.w_CSPFIN = NVL(TEMPO.CSPFIN," ")
      * --- Try
      local bErr_03022968
      bErr_03022968=bTrsErr
      this.Try_03022968()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_03021D98
        bErr_03021D98=bTrsErr
        this.Try_03021D98()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          if not ah_YesNo("Errore di scrittura in codice: %1%0Continuo?","", this.w_CSCAU)
            * --- Raise
            i_Error=ah_Msgformat("Import fallito")
            return
          else
            * --- accept error
            bTrsErr=.f.
          endif
        endif
        bTrsErr=bTrsErr or bErr_03021D98
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_03022968
      * --- End
      SELECT (this.w_CURSORE)
      ENDSCAN
    else
      SCAN FOR NOT EMPTY(NVL(CAENTE," "))
      * --- Inserisco...
      this.w_CAENTE = TEMPO.CAENTE
      this.w_CACAU = TEMPO.CACAU
      this.w_CADES = NVL(TEMPO.CADES," ")
      this.w_CAINI = NVL(TEMPO.CAINI," ")
      this.w_CAFIN = NVL(TEMPO.CAFIN," ")
      * --- Try
      local bErr_04AB2CA0
      bErr_04AB2CA0=bTrsErr
      this.Try_04AB2CA0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_04AB2280
        bErr_04AB2280=bTrsErr
        this.Try_04AB2280()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          if not ah_YesNo("Errore di scrittura in codice: %1%0Continuo?","", this.w_CACAU)
            * --- Raise
            i_Error=ah_Msgformat("Import fallito")
            return
          else
            * --- accept error
            bTrsErr=.f.
          endif
        endif
        bTrsErr=bTrsErr or bErr_04AB2280
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_04AB2CA0
      * --- End
      SELECT (this.w_CURSORE)
      ENDSCAN
    endif
  endproc
  proc Try_03022968()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAU_INPS
    i_nConn=i_TableProp[this.CAU_INPS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_INPS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAU_INPS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CS_CAUSA"+",CSDESCRI"+",CSPERINI"+",CSPERFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CSCAU),'CAU_INPS','CS_CAUSA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CSDES),'CAU_INPS','CSDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CSPINI),'CAU_INPS','CSPERINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CSPFIN),'CAU_INPS','CSPERFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CS_CAUSA',this.w_CSCAU,'CSDESCRI',this.w_CSDES,'CSPERINI',this.w_CSPINI,'CSPERFIN',this.w_CSPFIN)
      insert into (i_cTable) (CS_CAUSA,CSDESCRI,CSPERINI,CSPERFIN &i_ccchkf. );
         values (;
           this.w_CSCAU;
           ,this.w_CSDES;
           ,this.w_CSPINI;
           ,this.w_CSPFIN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco codice: %1",.t.,,,this.w_CSCAU)
    return
  proc Try_03021D98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CAU_INPS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CAU_INPS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_INPS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CAU_INPS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CSDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_CSDES),'CAU_INPS','CSDESCRI');
      +",CSPERINI ="+cp_NullLink(cp_ToStrODBC(this.w_CSPINI),'CAU_INPS','CSPERINI');
      +",CSPERFIN ="+cp_NullLink(cp_ToStrODBC(this.w_CSPFIN),'CAU_INPS','CSPERFIN');
          +i_ccchkf ;
      +" where ";
          +"CS_CAUSA = "+cp_ToStrODBC(this.w_CSCAU);
             )
    else
      update (i_cTable) set;
          CSDESCRI = this.w_CSDES;
          ,CSPERINI = this.w_CSPINI;
          ,CSPERFIN = this.w_CSPFIN;
          &i_ccchkf. ;
       where;
          CS_CAUSA = this.w_CSCAU;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Scrivo codice: %1",.t.,,,this.w_CSCAU)
    return
  proc Try_04AB2CA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAU_AEN
    i_nConn=i_TableProp[this.CAU_AEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_AEN_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAU_AEN_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AECODENT"+",AECAUSEN"+",AEDESENT"+",AEPERINI"+",AEPERFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CAENTE),'CAU_AEN','AECODENT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CACAU),'CAU_AEN','AECAUSEN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CADES),'CAU_AEN','AEDESENT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAINI),'CAU_AEN','AEPERINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFIN),'CAU_AEN','AEPERFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AECODENT',this.w_CAENTE,'AECAUSEN',this.w_CACAU,'AEDESENT',this.w_CADES,'AEPERINI',this.w_CAINI,'AEPERFIN',this.w_CAFIN)
      insert into (i_cTable) (AECODENT,AECAUSEN,AEDESENT,AEPERINI,AEPERFIN &i_ccchkf. );
         values (;
           this.w_CAENTE;
           ,this.w_CACAU;
           ,this.w_CADES;
           ,this.w_CAINI;
           ,this.w_CAFIN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco codice: %1",.t.,,,this.w_CACAU)
    return
  proc Try_04AB2280()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CAU_AEN
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CAU_AEN_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_AEN_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CAU_AEN_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AEDESENT ="+cp_NullLink(cp_ToStrODBC(this.w_CADES),'CAU_AEN','AEDESENT');
      +",AEPERINI ="+cp_NullLink(cp_ToStrODBC(this.w_CAINI),'CAU_AEN','AEPERINI');
      +",AEPERFIN ="+cp_NullLink(cp_ToStrODBC(this.w_CAFIN),'CAU_AEN','AEPERFIN');
          +i_ccchkf ;
      +" where ";
          +"AECODENT = "+cp_ToStrODBC(this.w_CAENTE);
          +" and AECAUSEN = "+cp_ToStrODBC(this.w_CACAU);
             )
    else
      update (i_cTable) set;
          AEDESENT = this.w_CADES;
          ,AEPERINI = this.w_CAINI;
          ,AEPERFIN = this.w_CAFIN;
          &i_ccchkf. ;
       where;
          AECODENT = this.w_CAENTE;
          and AECAUSEN = this.w_CACAU;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Scrivo codice: %1",.t.,,,this.w_CACAU)
    return


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Sedi INAIL
    CREATE CURSOR TEMPO (CODICE C(5), NOME C(30), INDIRI C(35), CAP C(5), LOCAL C(30), PROV C(2))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_CODICE = SPACE(5)
    this.w_NOME = SPACE(30)
    this.w_INDIRI = SPACE(35)
    this.w_CAP = SPACE(5)
    this.w_LOCAL = SPACE(30)
    this.w_PROV = SPACE(2)
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODICE," "))
    * --- Inserisco...
    this.w_CODICE = TEMPO.CODICE
    this.w_NOME = NVL(TEMPO.NOME," ")
    this.w_INDIRI = NVL(TEMPO.INDIRI," ")
    this.w_CAP = NVL(TEMPO.CAP, " ")
    this.w_LOCAL = NVL(TEMPO.LOCAL, " ")
    this.w_PROV = NVL(TEMPO.PROV, " ")
    * --- Try
    local bErr_04ABCBA0
    bErr_04ABCBA0=bTrsErr
    this.Try_04ABCBA0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_04ABC030
      bErr_04ABC030=bTrsErr
      this.Try_04ABC030()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura in codice: %1%0Continuo?","", this.w_CODICE)
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_04ABC030
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_04ABCBA0
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_04ABCBA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SE_INAIL
    i_nConn=i_TableProp[this.SE_INAIL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SE_INAIL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SE_INAIL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SICODICE"+",SI__NOME"+",SI_INDIR"+",SI__CAP"+",SI_LOCAL"+",SI__PROV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODICE),'SE_INAIL','SICODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOME),'SE_INAIL','SI__NOME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INDIRI),'SE_INAIL','SI_INDIR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAP),'SE_INAIL','SI__CAP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOCAL),'SE_INAIL','SI_LOCAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PROV),'SE_INAIL','SI__PROV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SICODICE',this.w_CODICE,'SI__NOME',this.w_NOME,'SI_INDIR',this.w_INDIRI,'SI__CAP',this.w_CAP,'SI_LOCAL',this.w_LOCAL,'SI__PROV',this.w_PROV)
      insert into (i_cTable) (SICODICE,SI__NOME,SI_INDIR,SI__CAP,SI_LOCAL,SI__PROV &i_ccchkf. );
         values (;
           this.w_CODICE;
           ,this.w_NOME;
           ,this.w_INDIRI;
           ,this.w_CAP;
           ,this.w_LOCAL;
           ,this.w_PROV;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco codice: %1",.t.,,,this.w_CODICE)
    return
  proc Try_04ABC030()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SE_INAIL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SE_INAIL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SE_INAIL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SE_INAIL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SI__NOME ="+cp_NullLink(cp_ToStrODBC(this.w_NOME),'SE_INAIL','SI__NOME');
      +",SI_INDIR ="+cp_NullLink(cp_ToStrODBC(this.w_INDIRI),'SE_INAIL','SI_INDIR');
      +",SI__CAP ="+cp_NullLink(cp_ToStrODBC(this.w_CAP),'SE_INAIL','SI__CAP');
      +",SI_LOCAL ="+cp_NullLink(cp_ToStrODBC(this.w_LOCAL),'SE_INAIL','SI_LOCAL');
      +",SI__PROV ="+cp_NullLink(cp_ToStrODBC(this.w_PROV),'SE_INAIL','SI__PROV');
          +i_ccchkf ;
      +" where ";
          +"SICODICE = "+cp_ToStrODBC(this.w_CODICE);
             )
    else
      update (i_cTable) set;
          SI__NOME = this.w_NOME;
          ,SI_INDIR = this.w_INDIRI;
          ,SI__CAP = this.w_CAP;
          ,SI_LOCAL = this.w_LOCAL;
          ,SI__PROV = this.w_PROV;
          &i_ccchkf. ;
       where;
          SICODICE = this.w_CODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Scrivo codice: %1",.t.,,,this.w_CODICE)
    return


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Codici Avviamento Postale
    this.w_ELIMINA = AH_YESNO("Elimino i dati precedentemente inseriti?")
    if this.w_ELIMINA
      * --- Delete from ANAG_CAP
      i_nConn=i_TableProp[this.ANAG_CAP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANAG_CAP_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CPCODCAP <> "+cp_ToStrODBC("zxzxzxzxz");
               )
      else
        delete from (i_cTable) where;
              CPCODCAP <> "zxzxzxzxz";

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from ANAG_PRO
      i_nConn=i_TableProp[this.ANAG_PRO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANAG_PRO_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PRCODPRO <> "+cp_ToStrODBC("zx");
               )
      else
        delete from (i_cTable) where;
              PRCODPRO <> "zx";

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    CREATE CURSOR TEMPO (CODCAP C(8), LOCALITA C (50), FRAZIONE C (50), INDIRIZZ C (154), PROVINCIA C(2), DESPROV C(30), CCODFIS C(4), FLUFFGIU C(1))
    * --- Definisce il file di tipo testo delimitato 
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    * --- Ritorna il numero dell'area di lavoro utilizzata
    this.w_CURSORE = SELECT()
    this.w_CODCAP = SPACE(9)
    this.w_CODLOC = SPACE(10)
    this.w_LOCALITA = SPACE(30)
    this.w_FRAZIONE = SPACE(50)
    this.w_INDIRIZZ = SPACE(154)
    this.w_PROVINCIA = SPACE(2)
    this.w_DESPROV = SPACE(30)
    this.w_CCODFIS = SPACE(4)
    this.w_FLUFFGIU = SPACE(1)
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    this.w_APPOCOD = 0
    i_Conn=i_TableProp[this.ANAG_CAP_IDX, 3]
    cp_NextTableProg(this, i_Conn, "NUCAP", "w_APPOCOD")
    * --- Aggiorno il progressivo
    this.w_NEWPROG = this.w_APPOCOD + RecCount(this.w_CURSORE)
    cp_NextTableProg(this, i_Conn, "NUCAP", "w_NEWPROG")
    SELECT (this.w_CURSORE)
    SCAN FOR NOT EMPTY(NVL(CODCAP," "))
    * --- Inserisco...
    this.w_CODLOC = STR(this.w_APPOCOD,10,0)
    this.w_LOCALITA = NVL(TEMPO.LOCALITA, SPACE(30))
    this.w_FRAZIONE = NVL(TEMPO.FRAZIONE, SPACE(50))
    this.w_INDIRIZZ = NVL(TEMPO.INDIRIZZ, SPACE(154))
    this.w_CODCAP = TEMPO.CODCAP
    this.w_PROVINCIA = NVL(TEMPO.PROVINCIA, SPACE(2))
    this.w_DESPROV = NVL(TEMPO.DESPROV, SPACE(30))
    this.w_CCODFIS = NVL(TEMPO.CCODFIS, SPACE(4))
    this.w_FLUFFGIU = NVL(TEMPO.FLUFFGIU, SPACE(1))
    * --- Try
    local bErr_04AB5640
    bErr_04AB5640=bTrsErr
    this.Try_04AB5640()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error="Import fallito"
      return
    endif
    bTrsErr=bTrsErr or bErr_04AB5640
    * --- End
    SELECT (this.w_CURSORE)
    this.w_APPOCOD = this.w_APPOCOD+1
    ENDSCAN
    * --- Scrittura Provincie
    SELECT DISTINCT PROVINCIA,DESPROV FROM TEMPO INTO CURSOR PROVINCE
    SELECT PROVINCE
    GO TOP
    SCAN
    this.w_PROVINCIA = NVL(PROVINCE.PROVINCIA, SPACE(2))
    this.w_DESPROV = NVL(PROVINCE.DESPROV, SPACE(30))
    if NOT EMPTY(this.w_PROVINCIA)
      ah_msg("Scrivo provincia: %1",.t.,,,ALLTRIM(this.w_DESPROV))
      * --- Insert into ANAG_PRO
      i_nConn=i_TableProp[this.ANAG_PRO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ANAG_PRO_idx,2])
      i_commit = .f.
      local bErr_04AB5FD0
      bErr_04AB5FD0=bTrsErr
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ANAG_PRO_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PRCODPRO"+",PRDESPRO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PROVINCIA),'ANAG_PRO','PRCODPRO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESPROV),'ANAG_PRO','PRDESPRO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PRCODPRO',this.w_PROVINCIA,'PRDESPRO',this.w_DESPROV)
        insert into (i_cTable) (PRCODPRO,PRDESPRO &i_ccchkf. );
           values (;
             this.w_PROVINCIA;
             ,this.w_DESPROV;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        bTrsErr=bErr_04AB5FD0
        * --- accept error
        bTrsErr=.f.
        * --- Write into ANAG_PRO
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ANAG_PRO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANAG_PRO_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ANAG_PRO_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PRDESPRO ="+cp_NullLink(cp_ToStrODBC(this.w_DESPROV),'ANAG_PRO','PRDESPRO');
              +i_ccchkf ;
          +" where ";
              +"PRCODPRO = "+cp_ToStrODBC(this.w_PROVINCIA);
                 )
        else
          update (i_cTable) set;
              PRDESPRO = this.w_DESPROV;
              &i_ccchkf. ;
           where;
              PRCODPRO = this.w_PROVINCIA;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    ENDSCAN
    USE IN SELECT("PROVINCE")
  endproc
  proc Try_04AB5640()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Scrittura CAP
    * --- Try
    local bErr_04AB52B0
    bErr_04AB52B0=bTrsErr
    this.Try_04AB52B0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if this.w_ELIMINA=.F.
        * --- accept error
        bTrsErr=.f.
        * --- Write into ANAG_CAP
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.ANAG_CAP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANAG_CAP_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.ANAG_CAP_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPCODCAP ="+cp_NullLink(cp_ToStrODBC(this.w_CODCAP),'ANAG_CAP','CPCODCAP');
          +",CPDESLOC ="+cp_NullLink(cp_ToStrODBC(this.w_LOCALITA),'ANAG_CAP','CPDESLOC');
          +",CPDESFRA ="+cp_NullLink(cp_ToStrODBC(this.w_FRAZIONE),'ANAG_CAP','CPDESFRA');
          +",CPDESIND ="+cp_NullLink(cp_ToStrODBC(this.w_INDIRIZZ),'ANAG_CAP','CPDESIND');
          +",CPDESPRO ="+cp_NullLink(cp_ToStrODBC(this.w_DESPROV),'ANAG_CAP','CPDESPRO');
          +",CPCODPRO ="+cp_NullLink(cp_ToStrODBC(this.w_PROVINCIA),'ANAG_CAP','CPCODPRO');
          +",CPCODFIS ="+cp_NullLink(cp_ToStrODBC(this.w_CCODFIS),'ANAG_CAP','CPCODFIS');
          +",CPUFFGIU ="+cp_NullLink(cp_ToStrODBC(this.w_FLUFFGIU),'ANAG_CAP','CPUFFGIU');
              +i_ccchkf ;
          +" where ";
              +"CPCODLOC = "+cp_ToStrODBC(this.w_CODLOC);
                 )
        else
          update (i_cTable) set;
              CPCODCAP = this.w_CODCAP;
              ,CPDESLOC = this.w_LOCALITA;
              ,CPDESFRA = this.w_FRAZIONE;
              ,CPDESIND = this.w_INDIRIZZ;
              ,CPDESPRO = this.w_DESPROV;
              ,CPCODPRO = this.w_PROVINCIA;
              ,CPCODFIS = this.w_CCODFIS;
              ,CPUFFGIU = this.w_FLUFFGIU;
              &i_ccchkf. ;
           where;
              CPCODLOC = this.w_CODLOC;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    bTrsErr=bTrsErr or bErr_04AB52B0
    * --- End
    if mod(this.w_APPOCOD,10)=0
      ah_msg("Inserisco CAP: %1",.t.,,,ALLTRIM(this.w_LOCALITA))
    endif
    return
  proc Try_04AB52B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ANAG_CAP
    i_nConn=i_TableProp[this.ANAG_CAP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ANAG_CAP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ANAG_CAP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CPCODCAP"+",CPCODLOC"+",CPDESLOC"+",CPDESFRA"+",CPDESIND"+",CPDESPRO"+",CPCODPRO"+",CPCODFIS"+",CPUFFGIU"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODCAP),'ANAG_CAP','CPCODCAP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODLOC),'ANAG_CAP','CPCODLOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOCALITA),'ANAG_CAP','CPDESLOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FRAZIONE),'ANAG_CAP','CPDESFRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INDIRIZZ),'ANAG_CAP','CPDESIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESPROV),'ANAG_CAP','CPDESPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PROVINCIA),'ANAG_CAP','CPCODPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCODFIS),'ANAG_CAP','CPCODFIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLUFFGIU),'ANAG_CAP','CPUFFGIU');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CPCODCAP',this.w_CODCAP,'CPCODLOC',this.w_CODLOC,'CPDESLOC',this.w_LOCALITA,'CPDESFRA',this.w_FRAZIONE,'CPDESIND',this.w_INDIRIZZ,'CPDESPRO',this.w_DESPROV,'CPCODPRO',this.w_PROVINCIA,'CPCODFIS',this.w_CCODFIS,'CPUFFGIU',this.w_FLUFFGIU)
      insert into (i_cTable) (CPCODCAP,CPCODLOC,CPDESLOC,CPDESFRA,CPDESIND,CPDESPRO,CPCODPRO,CPCODFIS,CPUFFGIU &i_ccchkf. );
         values (;
           this.w_CODCAP;
           ,this.w_CODLOC;
           ,this.w_LOCALITA;
           ,this.w_FRAZIONE;
           ,this.w_INDIRIZZ;
           ,this.w_DESPROV;
           ,this.w_PROVINCIA;
           ,this.w_CCODFIS;
           ,this.w_FLUFFGIU;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento Regole di importazioni (INFOLINK)
    CREATE CURSOR TEMPO (RECODICE C(15),REDESCRI C(45),RETIPMOD C(2),; 
 UTCC N(4,0),UTCV N(4,0),UTDC D,UTDV D,REDTINVA D,REDTOBSO D,; 
 CPROWNUM N(3,0),CPROWORD N(4,0),RETIPSTE C(2),REDESOPE C(100),REPARAME C(1),RENOMCUR C(8),; 
 ATDAFILT C(25), ATMULTAZ C(1),ATNTABLE C(50),ATCRIPTE C(1),ATCOODBC C(25),ATCOSTRI C(250),ATCAMCON C(150))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_RECODICEP = " "
    this.w_RECODICE = " "
    this.w_ERRORCAR = .F.
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE) 
 GO TOP 
 SCAN FOR NOT EMPTY(NVL(RECODICE," "))
    this.w_RECODICE = nvl(TEMPO.RECODICE,"")
    this.w_CPROWNUM = nvl(TEMPO.CPROWNUM,"")
    this.w_CPROWORD = nvl(TEMPO.CPROWORD,"")
    this.w_RETIPSTE = nvl(TEMPO.RETIPSTE,"")
    this.w_REDESOPE = nvl(TEMPO.REDESOPE,"")
    this.w_REPARAME = nvl(TEMPO.REPARAME,"")
    this.w_RENOMCUR = nvl(TEMPO.RENOMCUR,"")
    this.w_ATDAFILT = nvl(TEMPO.ATDAFILT,"")
    this.w_ATMULTAZ = nvl(TEMPO.ATMULTAZ," ")
    this.w_ATCRIPTE = NVL(TEMPO.ATCRIPTE,"")
    this.w_ATNTABLE = NVL(TEMPO.ATNTABLE,"")
    this.w_ATCOODBC = NVL(TEMPO.ATCOODBC,"")
    this.w_ATCOSTRI = NVL(TEMPO.ATCOSTRI,"")
    this.w_ATCAMCON = NVL(TEMPO.ATCAMCON,"")
    if this.w_RECODICE = this.w_RECODICEP
      * --- Inserisco Dettaglio della regola corrente
      this.w_TIPDEST = "D"
      this.w_RECODICEP = this.w_RECODICE
      GSIN_BSB(this,this.w_TIPDEST)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ERRORCAR
        * --- Raise
        i_Error=ah_Msgformat("Import fallito")
        return
      endif
    else
      * --- Inserisco Testata
      this.w_TIPDEST = "M"
      this.w_REDESCRI = nvl(TEMPO.REDESCRI,"")
      this.w_RETIPMOD = nvl(TEMPO.RETIPMOD,"")
      this.w_UTCC = nvl(TEMPO.UTCC,"")
      this.w_UTCV = nvl(TEMPO.UTCV,"")
      this.w_UTDC = nvl(TEMPO.UTDC,"")
      this.w_UTDV = nvl(TEMPO.UTDV,"")
      this.w_REDTINVA = nvl(TEMPO.REDTINVA,"")
      this.w_REDTOBSO = nvl(TEMPO.REDTOBSO,"")
      * --- Inserisco Dettaglio della regola corrente
      this.w_RECODICEP = this.w_RECODICE
      GSIN_BSB(this,this.w_TIPDEST)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ERRORCAR
        * --- Raise
        i_Error=ah_Msgformat("Import fallito")
        return
      endif
    endif
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc


  procedure Page_13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento Gruppi di Regole (INFOLINK)
    CREATE CURSOR TEMPO (GRCODICE C(15),GRDESCRI C(35),GRDTINVA D,GRDTOBSO D,; 
 GRNUMSEQ N(4,0),GRCODREG C(15),CPROWNUM N(3,0),CPROWORD N(4,0))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_GRCODICEP = " "
    this.w_GRCODICE = " "
    this.w_ERRORGRU = .F.
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE) 
 GO TOP 
 SCAN FOR NOT EMPTY(NVL(GRCODICE," "))
    this.w_GRCODICE = nvl(TEMPO.GRCODICE,"")
    if this.w_GRCODICE = this.w_GRCODICEP
      * --- Inserisco Dettaglio della regola corrente
      this.w_TIPODEST = "D"
      this.w_CPROWNUM = nvl(TEMPO.CPROWNUM,"")
      this.w_CPROWORD = nvl(TEMPO.CPROWORD,"")
      this.w_GRNUMSEQ = nvl(TEMPO.GRNUMSEQ,"")
      this.w_GRCODREG = nvl(TEMPO.GRCODREG,"")
      this.w_GRCODICEP = this.w_GRCODICE
      GSIN_BGR(this,this.w_TIPODEST)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ERRORGRU
        * --- Raise
        i_Error=ah_Msgformat("Import fallito")
        return
      endif
    else
      * --- Inserisco Testata
      this.w_TIPODEST = "M"
      this.w_GRDESCRI = nvl(TEMPO.GRDESCRI,"")
      this.w_GRDTINVA = nvl(TEMPO.GRDTINVA,"")
      this.w_GRDTOBSO = nvl(TEMPO.GRDTOBSO,"")
      * --- Inserisco Dettaglio della regola corrente
      this.w_CPROWNUM = nvl(TEMPO.CPROWNUM,"")
      this.w_CPROWORD = nvl(TEMPO.CPROWORD,"")
      this.w_GRNUMSEQ = nvl(TEMPO.GRNUMSEQ,"")
      this.w_GRCODREG = nvl(TEMPO.GRCODREG,"")
      this.w_GRCODICEP = this.w_GRCODICE
      GSIN_BGR(this,this.w_TIPODEST)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ERRORGRU
        * --- Raise
        i_Error=ah_Msgformat("Import fallito")
        return
      endif
    endif
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc


  procedure Page_14
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento Gestioni Schedulatore
    this.w_ERRORGRU = .f.
    this.w_GSNOME = " "
    this.w_FLMD = "M"
    this.w_AGG=ah_YesNo("Vuoi aggiornare i dati gi� esistenti?")
    CREATE CURSOR TEMPO (GSNOME C(20),GSDESC C(254),GSNOMVAL C(20),; 
 GSNOMDES C(254),GSFLGACT C(1),GSVALVAR C(254), CPROWNUM N(6,0), GSTIPESC C(1), GSTIPPAR C(1))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE) 
 GO TOP 
 SCAN FOR NOT EMPTY(NVL(GSNOME," "))
    if nvl(TEMPO.GSNOME,"") = this.w_GSNOME
      this.w_FLMD = "D"
    else
      this.w_FLMD = "M"
    endif
    this.w_GSNOME = nvl(TEMPO.GSNOME,"")
    this.w_GSNOME = STRTRAN(this.w_GSNOME, "#", '"')
    this.w_GSDESC = nvl(TEMPO.GSDESC,"")
    this.w_CPROWNUM = nvl(TEMPO.CPROWNUM,"")
    this.w_GSNOMVAL = nvl(TEMPO.GSNOMVAL,"")
    this.w_GSNOMDES = nvl(TEMPO.GSNOMDES,"")
    this.w_GSFLGACT = nvl(TEMPO.GSFLGACT,"")
    this.w_GSVALVAR = nvl(TEMPO.GSVALVAR,"")
    this.w_GSVALVAR = STRTRAN(this.w_GSVALVAR, "#", '"')
    this.w_GSTIPESC = nvl(TEMPO.GSTIPESC,"S")
    this.w_GSTIPPAR = nvl(TEMPO.GSTIPPAR,"V")
    do GSJB_BGE with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_ERRORGRU
      * --- Raise
      i_Error=ah_Msgformat("Import fallito")
      return
    endif
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc


  procedure Page_15
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica configurazione stampa su file
    if ah_YesNo("Vuoi aggiornare le configurazioni stampa su file gi� esistenti?")
      CREATE CURSOR TEMPO (SFPCNAME C(20),SFFORMAT C(10), SFEXTFOR C(10), CPROWORD N(4), SFTYPECL C(20),; 
 SFFLGSEL C(1),SFMSKCFG C(20), CPROWNUM N(4,0))
      APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
      this.w_CURSORE = SELECT()
      * --- Inserisco nell'archivio
      SELECT (this.w_CURSORE) 
 GO TOP 
 SCAN FOR NOT EMPTY(NVL(SFPCNAME," "))
      if Alltrim(this.w_SFPCNAME) <> alltrim(TEMPO.SFPCNAME)
        * --- Cancello tutte le vecchie impostazioni per ciascuna workstation
        * --- Try
        local bErr_04DBBF40
        bErr_04DBBF40=bTrsErr
        this.Try_04DBBF40()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04DBBF40
        * --- End
      endif
      this.w_SFPCNAME = TEMPO.SFPCNAME
      this.w_SFFORMAT = NVL(TEMPO.SFFORMAT," ")
      this.w_SFEXTFOR = NVL(TEMPO.SFEXTFOR," ")
      this.w_CPROWNUM = TEMPO.CPROWNUM
      this.w_CPROWORD = NVL(TEMPO.CPROWORD, 0)
      this.w_SFTYPECL = NVL(TEMPO.SFTYPECL," ")
      this.w_SFFLGSEL = NVL(TEMPO.SFFLGSEL,"N")
      this.w_SFMSKCFG = NVL(TEMPO.SFMSKCFG, " ")
      * --- Inserisco...
      * --- Try
      local bErr_04DC6200
      bErr_04DC6200=bTrsErr
      this.Try_04DC6200()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_04DC85A0
        bErr_04DC85A0=bTrsErr
        this.Try_04DC85A0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          if not ah_YesNo("Errore di scrittura in %1%0%2%0Continuo?","", this.w_SFPCNAME+"-"+this.w_SFFORMAT, STR(this.w_CPROWNUM,4,0)+"-"+this.w_SFFORMAT )
            * --- Raise
            i_Error=ah_Msgformat("Import fallito")
            return
          else
            * --- accept error
            bTrsErr=.f.
          endif
        endif
        bTrsErr=bTrsErr or bErr_04DC85A0
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_04DC6200
      * --- End
      SELECT (this.w_CURSORE)
      ENDSCAN
    endif
  endproc
  proc Try_04DBBF40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from STMPFILE
    i_nConn=i_TableProp[this.STMPFILE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"SFPCNAME = "+cp_ToStrODBC(TEMPO.SFPCNAME);
             )
    else
      delete from (i_cTable) where;
            SFPCNAME = TEMPO.SFPCNAME;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_04DC6200()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STMPFILE
    i_nConn=i_TableProp[this.STMPFILE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STMPFILE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SFPCNAME"+",CPROWNUM"+",SFFORMAT"+",SFTYPECL"+",SFFLGSEL"+",SFMSKCFG"+",SFEXTFOR"+",CPROWORD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SFPCNAME),'STMPFILE','SFPCNAME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'STMPFILE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SFFORMAT),'STMPFILE','SFFORMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SFTYPECL),'STMPFILE','SFTYPECL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SFFLGSEL),'STMPFILE','SFFLGSEL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SFMSKCFG),'STMPFILE','SFMSKCFG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SFEXTFOR),'STMPFILE','SFEXTFOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'STMPFILE','CPROWORD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SFPCNAME',this.w_SFPCNAME,'CPROWNUM',this.w_CPROWNUM,'SFFORMAT',this.w_SFFORMAT,'SFTYPECL',this.w_SFTYPECL,'SFFLGSEL',this.w_SFFLGSEL,'SFMSKCFG',this.w_SFMSKCFG,'SFEXTFOR',this.w_SFEXTFOR,'CPROWORD',this.w_CPROWORD)
      insert into (i_cTable) (SFPCNAME,CPROWNUM,SFFORMAT,SFTYPECL,SFFLGSEL,SFMSKCFG,SFEXTFOR,CPROWORD &i_ccchkf. );
         values (;
           this.w_SFPCNAME;
           ,this.w_CPROWNUM;
           ,this.w_SFFORMAT;
           ,this.w_SFTYPECL;
           ,this.w_SFFLGSEL;
           ,this.w_SFMSKCFG;
           ,this.w_SFEXTFOR;
           ,this.w_CPROWORD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_MESS = this.w_SFPCNAME+"-"+STR(this.w_CPROWNUM,4,0)+"-"+this.w_SFFORMAT
    ah_msg("Inserisco: %1",.t.,,,this.w_MESS )
    return
  proc Try_04DC85A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into STMPFILE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.STMPFILE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.STMPFILE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SFFORMAT ="+cp_NullLink(cp_ToStrODBC(this.w_SFFORMAT),'STMPFILE','SFFORMAT');
      +",SFTYPECL ="+cp_NullLink(cp_ToStrODBC(this.w_SFTYPECL),'STMPFILE','SFTYPECL');
      +",SFFLGSEL ="+cp_NullLink(cp_ToStrODBC(this.w_SFFLGSEL),'STMPFILE','SFFLGSEL');
      +",SFMSKCFG ="+cp_NullLink(cp_ToStrODBC(this.w_SFMSKCFG),'STMPFILE','SFMSKCFG');
          +i_ccchkf ;
      +" where ";
          +"SFPCNAME = "+cp_ToStrODBC(this.w_SFPCNAME);
          +" and CPROWNUM = "+cp_ToStrODBC(this.W_CPROWNUM);
             )
    else
      update (i_cTable) set;
          SFFORMAT = this.w_SFFORMAT;
          ,SFTYPECL = this.w_SFTYPECL;
          ,SFFLGSEL = this.w_SFFLGSEL;
          ,SFMSKCFG = this.w_SFMSKCFG;
          &i_ccchkf. ;
       where;
          SFPCNAME = this.w_SFPCNAME;
          and CPROWNUM = this.W_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_MESS = this.w_SFPCNAME+"-"+STR(this.w_CPROWNUM,4,0)+"-"+this.w_SFFORMAT
    ah_msg("Scrivo: %1",.t.,,,this.w_MESS )
    return


  procedure Page_16
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento dati InfoPublisher
    this.w_ERRORGRU = .f.
    if this.oParentObject.w_TIPARC = "IR"
      * --- --
      * --- --
      * --- --
      * --- --
      * --- --
      * --- --
      this.w_AQCODICE = " "
      this.w_FLMD = "M"
      this.w_AGG=ah_YesNo("Vuoi aggiornare i dati gi� esistenti?")
      CREATE CURSOR TEMPO ( AQCODICE C(20), AQDES C(254), AQQRPATH C(254), AQDTFILE C(254), ; 
 AQIMPATH C(254), AQLOG C(254), AQADOCS C(254), AQFLGZCP C(1), AQFORTYP C(10), AQCONTYP C(15),; 
 AQCURLOC C(1), AQCURTYP C(20), AQENGTYP C(1), AQTIMOUT N(3), AQCMDTMO N(3), AQQUEFIL C(1), AQMAILR C(100),; 
 AQROWORD N(5,0), AQUTENTE N(4,0), AQFLUE C(1), GIROWNUM N(4,0), GIROWORD N(5,0), GIPROGRA C(254),; 
 GIPARAM C(15), GITABNAM C(15), GIDESC C(50), QAROWPRG N(5,0), QAFLUTGR C(1), ; 
 QAUTEGRP N(6,0), FIROWNUM N(4), FIROWORD N(5), FIFLNAME C(30), FITIPFIL C(8), ; 
 FIFILTRO C(254), FIPRGRN N(4), DERISERV C(1), DEFOLDER N(10), DEDESFIL C(100), DE__NOTE M, DEROWNUM N(6),; 
 DEROWORD N(5), DETIPDES C(1), DECODGRU N(6), DETIPCON C(1), DECODCON C(15), DECODAGE C(5), DECODROL N(6),; 
 DEVALDES C(25), DE__READ C(1), DE_WRITE C(1), DEDELETE C(1), DERIFCFO C(1), ; 
 AQCRIPTE C(1), FIFLGAZI C(1), FIFLGREM C(1), GIDESGES C(50), FIVISMSK C(254), FIFLNAME1 C(30), FITIPFIL1 C(8), ; 
 FIFILTRO1 C(254), FIFLGREM1 C(1), FIFLGAZI1 C(1), FIVISMSK1 C(254), FIROWNUM1 N(4), FIROWORD1 N(5),; 
 PFROWNUM N(5), PFTIPVAR C(1), PFVARNAM C(20), PFDESVAR C(50), PFLENVAR N(3), PFDECVAR N(3), PFROWORD N(5),; 
 GI__MENU C(10), MG__MENU C(254), AQFLJOIN C(1))
      APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
      this.w_CURSORE = SELECT()
      * --- Inserisco nell'archivio
      SELECT (this.w_CURSORE) 
 GO TOP 
 SCAN FOR NOT EMPTY(NVL(AQCODICE," "))
      if nvl(TEMPO.AQCODICE,"") = this.w_AQCODICE
        this.w_FLMD = "D"
      else
        this.w_FLMD = "M"
      endif
      this.w_AQCODICE = NVL(TEMPO.AQCODICE,"")
      this.w_AQDES = NVL(TEMPO.AQDES,"")
      this.w_AQQRPATH = NVL(TEMPO.AQQRPATH,"")
      this.w_AQDTFILE = NVL(TEMPO.AQDTFILE,"")
      this.w_AQIMPATH = NVL(TEMPO.AQIMPATH,"")
      this.w_AQLOG = NVL(TEMPO.AQLOG,"")
      this.w_AQADOCS = NVL(TEMPO.AQADOCS,"")
      this.w_AQFORTYP = NVL(TEMPO.AQFORTYP,"")
      this.w_AQCONTYP = IIF( EMPTY( NVL( TEMPO.AQCONTYP, " " ) ), CP_DBTYPE, TEMPO.AQCONTYP )
      this.w_AQCURLOC = EVL(NVL(TEMPO.AQCURLOC,""), "S")
      this.w_AQCURTYP = EVL(NVL(TEMPO.AQCURTYP,""), "ctOpenForwardOnly")
      this.w_AQENGTYP = NVL(TEMPO.AQENGTYP,"")
      this.w_AQTIMOUT = NVL(TEMPO.AQTIMOUT,"")
      this.w_AQCMDTMO = NVL(TEMPO.AQCMDTMO,"")
      this.w_AQQUEFIL = NVL(TEMPO.AQQUEFIL,"")
      this.w_AQFLGZCP = NVL(TEMPO.AQFLGZCP,"")
      this.w_AQCRIPTE = NVL(TEMPO.AQCRIPTE,"N")
      * --- --
      this.w_AQMAILR = NVL(TEMPO.AQMAILR,"")
      this.w_AQROWORD = NVL(TEMPO.AQROWORD,"")
      this.w_AQUTENTE = NVL(TEMPO.AQUTENTE,"")
      this.w_AQFLUE = NVL(TEMPO.AQFLUE,"")
      * --- --
      this.w_GIROWNUM = NVL(TEMPO.GIROWNUM,"")
      this.w_GIROWORD = NVL(TEMPO.GIROWORD,"")
      this.w_GIPROGRA = NVL(TEMPO.GIPROGRA,"")
      this.w_GIPARAM = NVL(TEMPO.GIPARAM,"")
      this.w_GITABNAM = NVL(TEMPO.GITABNAM,"")
      this.w_GIDESC = NVL(TEMPO.GIDESC,"")
      this.w_GIDESGES = NVL(TEMPO.GIDESGES,"")
      * --- --
      this.w_QAROWPRG = NVL(TEMPO.QAROWPRG,0)
      this.w_QAFLUTGR = NVL(TEMPO.QAFLUTGR,"")
      this.w_QAUTEGRP = NVL(TEMPO.QAUTEGRP,0)
      * --- --
      this.w_FIPRGRN = NVL(TEMPO.FIPRGRN,"")
      this.w_FIROWNUM = NVL(TEMPO.FIROWNUM,"")
      this.w_FIFLNAME = NVL(TEMPO.FIFLNAME,"")
      this.w_FIROWORD = NVL(TEMPO.FIROWORD,"")
      this.w_FITIPFIL = NVL(TEMPO.FITIPFIL,"")
      this.w_FIFILTRO = NVL(TEMPO.FIFILTRO,"")
      this.w_FIFLGAZI = NVL(FIFLGAZI, "")
      this.w_FIFLGREM = NVL(FIFLGREM, "")
      this.w_FIVISMSK = NVL(FIVISMSK,"")
      * --- --
      this.w_FIROWNUM1 = NVL(TEMPO.FIROWNUM1,"")
      this.w_FIFLNAME1 = NVL(TEMPO.FIFLNAME1,"")
      this.w_FIROWORD1 = NVL(TEMPO.FIROWORD1,"")
      this.w_FITIPFIL1 = NVL(TEMPO.FITIPFIL1,"")
      this.w_FIFILTRO1 = NVL(TEMPO.FIFILTRO1,"")
      this.w_FIFLGAZI1 = NVL(FIFLGAZI1, "")
      this.w_FIFLGREM1 = NVL(FIFLGREM1, "")
      this.w_FIVISMSK1 = NVL(FIVISMSK1,"")
      * --- --
      this.w_DERISERV = NVL(DERISERV,"")
      this.w_DEROWORD = NVL(DEROWORD,"")
      this.w_DEROWNUM = NVL(DEROWNUM,"")
      this.w_DETIPDES = NVL(DETIPDES,"")
      this.w_DECODGRU = NVL(DECODGRU,"")
      this.w_DETIPCON = NVL(DETIPCON,"")
      this.w_DECODCON = NVL(DECODCON,"")
      this.w_DECODAGE = NVL(DECODAGE,"")
      this.w_DERIFCFO = NVL(DERIFCFO,"")
      this.w_DEVALDES = NVL(DEVALDES,"")
      this.w_DECODROL = NVL(DECODROL,"")
      this.w_DE__READ = NVL(DE__READ,"")
      this.w_DE_WRITE = NVL(DE_WRITE,"")
      this.w_DEDELETE = NVL(DEDELETE,"")
      this.w_DEFOLDER = NVL(DEFOLDER,"")
      this.w_DEDESFIL = NVL(DEDESFIL,"")
      this.w_DE__NOTE = NVL(DE__NOTE,"")
      * --- --
      this.w_PFROWNUM = NVL(PFROWNUM,0)
      this.w_PFTIPVAR = NVL(PFTIPVAR," ")
      this.w_PFVARNAM = NVL(PFVARNAM, space(20))
      this.w_PFDESVAR = NVL(PFDESVAR, space(50))
      this.w_PFLENVAR = NVL(PFLENVAR, 0)
      this.w_PFDECVAR = NVL(PFDECVAR, 0)
      this.w_PFROWORD = NVL(PFROWORD, 0)
      * --- --
      this.w_GI__MENU = GI__MENU
      this.w_MG__MENU = NVL(MG__MENU, space(254))
      this.w_AQFLJOIN = EVL(NVL(TEMPO.AQFLJOIN,""), "S")
      * --- Carico i dati nelle tabelle
      GSIR_BSC(this,"A")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT (this.w_CURSORE)
      ENDSCAN
    else
      * --- Carica gruppi query
      this.w_GQCODICE = " "
      this.w_FLMD = "M"
      this.w_AGG=ah_YesNo("Vuoi aggiornare i dati gi� esistenti?")
      CREATE CURSOR TEMPO (GQCODICE C(15),GQDESCRI C(45),GQDTINVA D(8),; 
 GQDTOBSO D(8),CPROWNUM N(3,0), CPROWORD N(4,0), GQCODQUE C(20), GQNUMSEQ N(4,0))
      APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
      this.w_CURSORE = SELECT()
      * --- Inserisco nell'archivio
      SELECT (this.w_CURSORE) 
 GO TOP 
 SCAN FOR NOT EMPTY(NVL(GQCODICE," "))
      if nvl(TEMPO.GQCODICE,"") = this.w_GQCODICE
        this.w_FLMD = "D"
      else
        this.w_FLMD = "M"
      endif
      this.w_GQCODICE = NVL(TEMPO.GQCODICE,"")
      this.w_GQDESCRI = NVL(TEMPO.GQDESCRI,"")
      this.w_GQDTINVA = NVL(TEMPO.GQDTINVA,"" )
      this.w_GQDTOBSO = NVL(TEMPO.GQDTOBSO,"" )
      this.w_CPROWNUM = NVL(TEMPO.CPROWNUM,"")
      this.w_CPROWORD = NVL(TEMPO.CPROWORD,"")
      this.w_GQCODQUE = NVL(TEMPO.GQCODQUE,"")
      this.w_GQNUMSEQ = NVL(TEMPO.GQNUMSEQ,0)
      GSIR_BSC(this,"G")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ERRORGRU
        * --- Raise
        i_Error=ah_Msgformat("Import fallito")
        return
      endif
      SELECT (this.w_CURSORE)
      ENDSCAN
    endif
  endproc


  procedure Page_17
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica Regole analisi di bilancio
    CREATE CURSOR TEMPO (RECODICE C(15), REDESCRI C(45), REDESSUP M, RE__TIPO C(1),REDTINVA D,REDTOBSO D, ;
    CPROWNUM N(3,0),CPROWORD N(4,0),RETIPSTE C(2),REDESOPE C(100),RENOMCUR C(8))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_RECODICE = SPACE(15)
    this.w_RECODICEP = SPACE(15)
    this.w_REDESCRI = SPACE(45)
    this.w_ERRORE = .F.
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(RECODICE," "))
    this.w_RECODICE = nvl(TEMPO.RECODICE,"")
    if this.w_RECODICE=this.w_RECODICEP
      * --- Inserisco Dettaglio della regola corrente
      this.w_TIPINSERT = "D"
      this.w_CPROWNUM = NVL(TEMPO.CPROWNUM,0)
      this.w_CPROWORD = NVL(TEMPO.CPROWORD,0)
      this.w_RETIPSTE = NVL(TEMPO.RETIPSTE,"")
      this.w_REDESOPE = NVL(TEMPO.REDESOPE,"")
      this.w_RENOMCUR = NVL(TEMPO.RENOMCUR,"")
      GSBI_BRI(this,this.w_TIPINSERT)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ERRORE
        * --- Raise
        i_Error=ah_Msgformat("Import fallito")
        return
      endif
    else
      * --- Inserisco testata della nuova regola
      this.w_TIPINSERT = "M"
      * --- Campi di testata
      this.w_RECODICEP = nvl(TEMPO.RECODICE,"")
      this.w_REDESCRI = NVL(TEMPO.REDESCRI,"")
      this.w_REDESSUP = NVL(TEMPO.REDESSUP,"")
      this.w_RE__TIPO = NVL(TEMPO.RE__TIPO,"")
      this.w_REDTINVA = NVL(TEMPO.REDTINVA,cp_CharToDate(" - - "))
      this.w_REDTOBSO = NVL(TEMPO.REDTOBSO,cp_CharToDate(" - - "))
      * --- Campi del dettaglio
      this.w_CPROWNUM = NVL(TEMPO.CPROWNUM,0)
      this.w_CPROWORD = NVL(TEMPO.CPROWORD,0)
      this.w_RETIPSTE = NVL(TEMPO.RETIPSTE,"")
      this.w_REDESOPE = NVL(TEMPO.REDESOPE,"")
      this.w_RENOMCUR = NVL(TEMPO.RENOMCUR,"")
      GSBI_BRI(this,this.w_TIPINSERT)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ERRORE
        * --- Raise
        i_Error=ah_Msgformat("Import fallito")
        return
      endif
    endif
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc


  procedure Page_18
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importazione Struttura di Bilnacio UE
    CREATE CURSOR TEMPO (TRCODICE C(15),TRDESCRI C(40),TR__NOTE C(254),CPROWNUM N(5,0),CPROWORD N(5,0), ; 
 TRTIPDET C(1), TRTIPVOC C(1),TRDESDET C(50),TR__COL1 C(1),TR__COL2 C(1),TR__COL3 C(1),TC__COLO N(10,0), ; 
 TC__INDE N(10,0),TC__FTST C(1),TC__FONT C(1),TR__FLAG C(1),TRSEQCAL N(5,0),TR_RIFUE C(1),RTCODICE C(15), ; 
 RTNUMRIG N(5,0),RTRIGTOT N(5,0),RT_SEGNO C(1),VRCODVOC C(15),VRROWORD N(5,0),VRDESVOC C(50), ; 
 VR__NOTE C(254),DVCODVOC C(15),DVROWORD N(5,0),CPROWNUM2 N(5,0),DVFLMACO C(1),DVCODMAS C(15), ; 
 DVCODCON C(15),DV_SEGNO C(1),DV__DAVE C(1))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_TRCODICE = " " 
    this.w_TRCODICP = " "
    this.w_TRDESCRI = " "
    this.w_CANCSTRUC = .F.
    * --- Select from TIR_MAST
    i_nConn=i_TableProp[this.TIR_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIR_MAST_idx,2],.t.,this.TIR_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select COUNT(*) AS NUMREC  from "+i_cTable+" TIR_MAST ";
           ,"_Curs_TIR_MAST")
    else
      select COUNT(*) AS NUMREC from (i_cTable);
        into cursor _Curs_TIR_MAST
    endif
    if used('_Curs_TIR_MAST')
      select _Curs_TIR_MAST
      locate for 1=1
      do while not(eof())
      if _Curs_TIR_MAST.NUMREC > 0
        this.w_CANCSTRUC = .T.
      endif
        select _Curs_TIR_MAST
        continue
      enddo
      use
    endif
    this.w_BGOON = .t.
    if this.w_CANCSTRUC
      if ah_YesNo("Struttura gi� presente, si vogliono cancellare la struttura e le voci di raccordo Basilea2 collegate?")
        * --- Cancello tutti gli archivi della struttura
        * --- Delete from DETTRICL
        i_nConn=i_TableProp[this.DETTRICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DETTRICL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from VOCIRICL
        i_nConn=i_TableProp[this.VOCIRICL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIRICL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from RIGTOTAL
        i_nConn=i_TableProp[this.RIGTOTAL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIGTOTAL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from RACDBILA
        i_nConn=i_TableProp[this.RACDBILA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RACDBILA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from RACCBILA
        i_nConn=i_TableProp[this.RACCBILA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RACCBILA_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from TIR_DETT
        i_nConn=i_TableProp[this.TIR_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIR_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from TIR_MAST
        i_nConn=i_TableProp[this.TIR_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIR_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"1 = "+cp_ToStrODBC(1);
                 )
        else
          delete from (i_cTable) where;
                1 = 1;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      else
        ah_Msg("Aggiornamento struttura bilancio UE annullato",.T.)
        this.w_BGOON = .f.
        this.w_NOMSG = .T.
      endif
    endif
    if this.w_BGOON
      * --- Inserisco nell'archivio
      SELECT (this.w_CURSORE)
      GO TOP
      SCAN FOR NOT EMPTY(NVL(TRCODICE," "))
      this.w_TRCODICE = nvl(TEMPO.TRCODICE,"")
      this.w_CPROWNUM = NVL(TEMPO.CPROWNUM,0)
      this.w_CPROWORD = NVL(TEMPO.CPROWORD,0)
      if this.w_TRCODICE = this.w_TRCODICP 
        if this.w_CPROWORD<>this.w_CPROWORDP AND this.w_CPROWNUM<>this.w_CPROWNUMP
          * --- Inserisco dati dettaglio
          this.w_CPROWNUMP = this.w_CPROWNUM
          this.w_CPROWORDP = this.w_CPROWORD
          this.w_TRTIPDET = NVL(TEMPO.TRTIPDET," ")
          this.w_TRTIPVOC = NVL(TEMPO.TRTIPVOC," ")
          this.w_TRDESDET = NVL(TEMPO.TRDESDET," ")
          this.w_TR__COL1 = NVL(TEMPO.TR__COL1," ")
          this.w_TR__COL2 = NVL(TEMPO.TR__COL2," ")
          this.w_TR__COL3 = NVL(TEMPO.TR__COL3," ")
          this.w_TC__COLO = NVL(TEMPO.TC__COLO,0)
          this.w_TC__INDE = NVL(TEMPO.TC__INDE,0)
          this.w_TC__FTST = NVL(TEMPO.TC__FTST," ")
          this.w_TC__FONT = NVL(TEMPO.TC__FONT," ")
          this.w_TR__FLAG = NVL(TEMPO.TR__FLAG," ")
          this.w_TRSEQCAL = NVL(TEMPO.TRSEQCAL,0)
          this.w_TR_RIFUE = NVL(TEMPO.TR_RIFUE," ")
          * --- Try
          local bErr_04EC2D30
          bErr_04EC2D30=bTrsErr
          this.Try_04EC2D30()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Try
            local bErr_04EC4260
            bErr_04EC4260=bTrsErr
            this.Try_04EC4260()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              if not ah_YesNo("Errore di scrittura riga n� %1 struttura %2%0Continuo?", ALLTRIM(str(this.w_CPROWORD)), this.w_TRDESCRI)
                * --- Raise
                i_Error=ah_Msgformat("Import fallito")
                return
              else
                * --- accept error
                bTrsErr=.f.
              endif
            endif
            bTrsErr=bTrsErr or bErr_04EC4260
            * --- End
          endif
          bTrsErr=bTrsErr or bErr_04EC2D30
          * --- End
        endif
      else
        * --- Inserisco dati testata 
        this.w_TRCODICP = this.w_TRCODICE
        this.w_TRDESCRI = NVL(TEMPO.TRDESCRI," ")
        this.w_TR__NOTE = NVL(TEMPO.TR__NOTE," ")
        * --- Try
        local bErr_04ED8AD8
        bErr_04ED8AD8=bTrsErr
        this.Try_04ED8AD8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Try
          local bErr_04F12ED8
          bErr_04F12ED8=bTrsErr
          this.Try_04F12ED8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Se non riesco ad inserire la testata blocco l'importazione della struttura di bilancio UE
            this.w_MESS = ah_Msgformat("Import fallito struttura di bilancio UE",this.w_TRDESCRI)
            * --- Raise
            i_Error=this.w_MESS
            return
          endif
          bTrsErr=bTrsErr or bErr_04F12ED8
          * --- End
        endif
        bTrsErr=bTrsErr or bErr_04ED8AD8
        * --- End
        * --- Inserisco la prima riga del dettaglio
        this.w_CPROWNUM = NVL(TEMPO.CPROWNUM,0)
        this.w_CPROWORD = NVL(TEMPO.CPROWORD,0)
        this.w_CPROWNUMP = this.w_CPROWNUM
        this.w_CPROWORDP = this.w_CPROWORD
        this.w_TRTIPDET = NVL(TEMPO.TRTIPDET," ")
        this.w_TRTIPVOC = NVL(TEMPO.TRTIPVOC," ")
        this.w_TRDESDET = NVL(TEMPO.TRDESDET," ")
        this.w_TR__COL1 = NVL(TEMPO.TR__COL1," ")
        this.w_TR__COL2 = NVL(TEMPO.TR__COL2," ")
        this.w_TR__COL3 = NVL(TEMPO.TR__COL3," ")
        this.w_TC__COLO = NVL(TEMPO.TC__COLO,0)
        this.w_TC__INDE = NVL(TEMPO.TC__INDE,0)
        this.w_TC__FTST = NVL(TEMPO.TC__FTST," ")
        this.w_TC__FONT = NVL(TEMPO.TC__FONT," ")
        this.w_TR__FLAG = NVL(TEMPO.TR__FLAG," ")
        this.w_TRSEQCAL = NVL(TEMPO.TRSEQCAL,0)
        this.w_TR_RIFUE = NVL(TEMPO.TR_RIFUE," ")
        * --- Try
        local bErr_04ED0768
        bErr_04ED0768=bTrsErr
        this.Try_04ED0768()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Try
          local bErr_04ED1E78
          bErr_04ED1E78=bTrsErr
          this.Try_04ED1E78()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            if not ah_YesNo("Errore di scrittura riga n� %1 struttura %2%0Continuo?", ALLTRIM(str(this.w_CPROWORD)), this.w_TRDESCRI)
              * --- Raise
              i_Error=ah_Msgformat("Import fallito")
              return
            else
              * --- accept error
              bTrsErr=.f.
            endif
          endif
          bTrsErr=bTrsErr or bErr_04ED1E78
          * --- End
        endif
        bTrsErr=bTrsErr or bErr_04ED0768
        * --- End
      endif
      * --- Inserisco i dati
      this.w_RTCODICE = NVL(TEMPO.RTCODICE," ")
      this.w_RTNUMRIG = NVL(TEMPO.RTNUMRIG,0)
      this.w_RTRIGTOT = NVL(TEMPO.RTRIGTOT,0)
      this.w_RT_SEGNO = NVL(TEMPO.RT_SEGNO," ")
      if NOT EMPTY(this.w_RTCODICE)
        * --- Try
        local bErr_04EAD308
        bErr_04EAD308=bTrsErr
        this.Try_04EAD308()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into RIGTOTAL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.RIGTOTAL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RIGTOTAL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.RIGTOTAL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"RT_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.w_RT_SEGNO),'RIGTOTAL','RT_SEGNO');
                +i_ccchkf ;
            +" where ";
                +"RTCODICE = "+cp_ToStrODBC(this.w_RTCODICE);
                +" and RTNUMRIG = "+cp_ToStrODBC(this.w_RTNUMRIG);
                +" and RTRIGTOT = "+cp_ToStrODBC(this.w_RTRIGTOT);
                   )
          else
            update (i_cTable) set;
                RT_SEGNO = this.w_RT_SEGNO;
                &i_ccchkf. ;
             where;
                RTCODICE = this.w_RTCODICE;
                and RTNUMRIG = this.w_RTNUMRIG;
                and RTRIGTOT = this.w_RTRIGTOT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04EAD308
        * --- End
      endif
      this.w_VRCODVOC = NVL(TEMPO.VRCODVOC," ")
      this.w_VRROWORD = NVL(TEMPO.VRROWORD,0)
      this.w_VRDESVOC = NVL(TEMPO.VRDESVOC," ")
      this.w_VR__NOTE = NVL(TEMPO.VR__NOTE," ")
      * --- Inserisco i dati
      if NOT EMPTY(this.w_VRCODVOC)
        * --- Try
        local bErr_04EA5F28
        bErr_04EA5F28=bTrsErr
        this.Try_04EA5F28()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into VOCIRICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.VOCIRICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIRICL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.VOCIRICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"VRDESVOC ="+cp_NullLink(cp_ToStrODBC(this.w_VRDESVOC),'VOCIRICL','VRDESVOC');
            +",VR__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_VR__NOTE),'VOCIRICL','VR__NOTE');
                +i_ccchkf ;
            +" where ";
                +"VRCODVOC = "+cp_ToStrODBC(this.w_VRCODVOC);
                +" and VRROWORD = "+cp_ToStrODBC(this.w_VRROWORD);
                   )
          else
            update (i_cTable) set;
                VRDESVOC = this.w_VRDESVOC;
                ,VR__NOTE = this.w_VR__NOTE;
                &i_ccchkf. ;
             where;
                VRCODVOC = this.w_VRCODVOC;
                and VRROWORD = this.w_VRROWORD;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04EA5F28
        * --- End
      endif
      this.w_DVCODVOC = NVL(TEMPO.DVCODVOC," ")
      this.w_DVROWORD = NVL(TEMPO.DVROWORD,0)
      this.w_CPROWNUM2 = NVL(TEMPO.CPROWNUM2,0)
      this.w_DVFLMACO = NVL(TEMPO.DVFLMACO," ")
      this.w_DVCODMAS = NVL(TEMPO.DVCODMAS," ")
      this.w_DVCODCON = NVL(TEMPO.DVCODCON," ")
      this.w_DV_SEGNO = NVL(TEMPO.DV_SEGNO," ")
      this.w_DV__DAVE = NVL(TEMPO.DV__DAVE," ")
      if NOT EMPTY(this.w_DVCODVOC)
        * --- Try
        local bErr_04E9CDE0
        bErr_04E9CDE0=bTrsErr
        this.Try_04E9CDE0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into DETTRICL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DETTRICL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DETTRICL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DETTRICL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DVFLMACO ="+cp_NullLink(cp_ToStrODBC(this.w_DVFLMACO),'DETTRICL','DVFLMACO');
            +",DVCODMAS ="+cp_NullLink(cp_ToStrODBC(this.w_DVCODMAS),'DETTRICL','DVCODMAS');
            +",DVCODCON ="+cp_NullLink(cp_ToStrODBC(this.w_DVCODCON),'DETTRICL','DVCODCON');
            +",DV_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.w_DV_SEGNO),'DETTRICL','DV_SEGNO');
            +",DV__DAVE ="+cp_NullLink(cp_ToStrODBC(this.w_DV__DAVE),'DETTRICL','DV__DAVE');
                +i_ccchkf ;
            +" where ";
                +"DVCODVOC = "+cp_ToStrODBC(this.w_DVCODVOC);
                +" and DVROWORD = "+cp_ToStrODBC(this.w_DVROWORD);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM2);
                   )
          else
            update (i_cTable) set;
                DVFLMACO = this.w_DVFLMACO;
                ,DVCODMAS = this.w_DVCODMAS;
                ,DVCODCON = this.w_DVCODCON;
                ,DV_SEGNO = this.w_DV_SEGNO;
                ,DV__DAVE = this.w_DV__DAVE;
                &i_ccchkf. ;
             where;
                DVCODVOC = this.w_DVCODVOC;
                and DVROWORD = this.w_DVROWORD;
                and CPROWNUM = this.w_CPROWNUM2;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04E9CDE0
        * --- End
      endif
      ENDSCAN
    endif
  endproc
  proc Try_04EC2D30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("Inserisco: riga n� %1 struttura %2",.T.,.F.,.F., ALLTRIM(str(this.w_CPROWORD)), this.w_TRDESCRI )
    * --- Insert into TIR_DETT
    i_nConn=i_TableProp[this.TIR_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIR_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIR_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",CPROWNUM"+",CPROWORD"+",TRTIPDET"+",TRTIPVOC"+",TRDESDET"+",TR__COL1"+",TR__COL2"+",TR__COL3"+",TC__COLO"+",TC__INDE"+",TC__FTST"+",TC__FONT"+",TR__FLAG"+",TRSEQCAL"+",TR_RIFUE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TRCODICE),'TIR_DETT','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'TIR_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'TIR_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TRTIPDET),'TIR_DETT','TRTIPDET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TRTIPVOC),'TIR_DETT','TRTIPVOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TRDESDET),'TIR_DETT','TRDESDET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TR__COL1),'TIR_DETT','TR__COL1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TR__COL2),'TIR_DETT','TR__COL2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TR__COL3),'TIR_DETT','TR__COL3');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TC__COLO),'TIR_DETT','TC__COLO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TC__INDE),'TIR_DETT','TC__INDE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TC__FTST),'TIR_DETT','TC__FTST');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TC__FONT),'TIR_DETT','TC__FONT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TR__FLAG),'TIR_DETT','TR__FLAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TRSEQCAL),'TIR_DETT','TRSEQCAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TR_RIFUE),'TIR_DETT','TR_RIFUE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',this.w_TRCODICE,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'TRTIPDET',this.w_TRTIPDET,'TRTIPVOC',this.w_TRTIPVOC,'TRDESDET',this.w_TRDESDET,'TR__COL1',this.w_TR__COL1,'TR__COL2',this.w_TR__COL2,'TR__COL3',this.w_TR__COL3,'TC__COLO',this.w_TC__COLO,'TC__INDE',this.w_TC__INDE,'TC__FTST',this.w_TC__FTST)
      insert into (i_cTable) (TRCODICE,CPROWNUM,CPROWORD,TRTIPDET,TRTIPVOC,TRDESDET,TR__COL1,TR__COL2,TR__COL3,TC__COLO,TC__INDE,TC__FTST,TC__FONT,TR__FLAG,TRSEQCAL,TR_RIFUE &i_ccchkf. );
         values (;
           this.w_TRCODICE;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_TRTIPDET;
           ,this.w_TRTIPVOC;
           ,this.w_TRDESDET;
           ,this.w_TR__COL1;
           ,this.w_TR__COL2;
           ,this.w_TR__COL3;
           ,this.w_TC__COLO;
           ,this.w_TC__INDE;
           ,this.w_TC__FTST;
           ,this.w_TC__FONT;
           ,this.w_TR__FLAG;
           ,this.w_TRSEQCAL;
           ,this.w_TR_RIFUE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04EC4260()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into TIR_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TIR_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIR_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TIR_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'TIR_DETT','CPROWORD');
      +",TC__COLO ="+cp_NullLink(cp_ToStrODBC(this.w_TC__COLO),'TIR_DETT','TC__COLO');
      +",TC__FONT ="+cp_NullLink(cp_ToStrODBC(this.w_TC__FONT),'TIR_DETT','TC__FONT');
      +",TC__FTST ="+cp_NullLink(cp_ToStrODBC(this.w_TC__FTST),'TIR_DETT','TC__FTST');
      +",TC__INDE ="+cp_NullLink(cp_ToStrODBC(this.w_TC__INDE),'TIR_DETT','TC__INDE');
      +",TR__COL1 ="+cp_NullLink(cp_ToStrODBC(this.w_TR__COL1),'TIR_DETT','TR__COL1');
      +",TR__COL2 ="+cp_NullLink(cp_ToStrODBC(this.w_TR__COL2),'TIR_DETT','TR__COL2');
      +",TR__COL3 ="+cp_NullLink(cp_ToStrODBC(this.w_TR__COL3),'TIR_DETT','TR__COL3');
      +",TR__FLAG ="+cp_NullLink(cp_ToStrODBC(this.w_TR__FLAG),'TIR_DETT','TR__FLAG');
      +",TR_RIFUE ="+cp_NullLink(cp_ToStrODBC(this.w_TR_RIFUE),'TIR_DETT','TR_RIFUE');
      +",TRDESDET ="+cp_NullLink(cp_ToStrODBC(this.w_TRDESDET),'TIR_DETT','TRDESDET');
      +",TRSEQCAL ="+cp_NullLink(cp_ToStrODBC(this.w_TRSEQCAL),'TIR_DETT','TRSEQCAL');
      +",TRTIPDET ="+cp_NullLink(cp_ToStrODBC(this.w_TRTIPDET),'TIR_DETT','TRTIPDET');
      +",TRTIPVOC ="+cp_NullLink(cp_ToStrODBC(this.w_TRTIPVOC),'TIR_DETT','TRTIPVOC');
          +i_ccchkf ;
      +" where ";
          +"TRCODICE = "+cp_ToStrODBC(this.w_TRCODICE);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          CPROWORD = this.w_CPROWORD;
          ,TC__COLO = this.w_TC__COLO;
          ,TC__FONT = this.w_TC__FONT;
          ,TC__FTST = this.w_TC__FTST;
          ,TC__INDE = this.w_TC__INDE;
          ,TR__COL1 = this.w_TR__COL1;
          ,TR__COL2 = this.w_TR__COL2;
          ,TR__COL3 = this.w_TR__COL3;
          ,TR__FLAG = this.w_TR__FLAG;
          ,TR_RIFUE = this.w_TR_RIFUE;
          ,TRDESDET = this.w_TRDESDET;
          ,TRSEQCAL = this.w_TRSEQCAL;
          ,TRTIPDET = this.w_TRTIPDET;
          ,TRTIPVOC = this.w_TRTIPVOC;
          &i_ccchkf. ;
       where;
          TRCODICE = this.w_TRCODICE;
          and CPROWNUM = this.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04ED8AD8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TIR_MAST
    i_nConn=i_TableProp[this.TIR_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIR_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIR_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",TRDESCRI"+",TR__NOTE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TRCODICE),'TIR_MAST','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TRDESCRI),'TIR_MAST','TRDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TR__NOTE),'TIR_MAST','TR__NOTE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',this.w_TRCODICE,'TRDESCRI',this.w_TRDESCRI,'TR__NOTE',this.w_TR__NOTE)
      insert into (i_cTable) (TRCODICE,TRDESCRI,TR__NOTE &i_ccchkf. );
         values (;
           this.w_TRCODICE;
           ,this.w_TRDESCRI;
           ,this.w_TR__NOTE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04F12ED8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into TIR_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TIR_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIR_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TIR_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TRDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_TRDESCRI),'TIR_MAST','TRDESCRI');
      +",TR__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_TR__NOTE),'TIR_MAST','TR__NOTE');
          +i_ccchkf ;
      +" where ";
          +"TRCODICE = "+cp_ToStrODBC(this.w_TRCODICE);
             )
    else
      update (i_cTable) set;
          TRDESCRI = this.w_TRDESCRI;
          ,TR__NOTE = this.w_TR__NOTE;
          &i_ccchkf. ;
       where;
          TRCODICE = this.w_TRCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04ED0768()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TIR_DETT
    i_nConn=i_TableProp[this.TIR_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIR_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIR_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TRCODICE"+",CPROWNUM"+",CPROWORD"+",TRTIPDET"+",TRTIPVOC"+",TRDESDET"+",TR__COL1"+",TR__COL2"+",TR__COL3"+",TC__COLO"+",TC__INDE"+",TC__FTST"+",TC__FONT"+",TR__FLAG"+",TRSEQCAL"+",TR_RIFUE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TRCODICE),'TIR_DETT','TRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'TIR_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'TIR_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TRTIPDET),'TIR_DETT','TRTIPDET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TRTIPVOC),'TIR_DETT','TRTIPVOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TRDESDET),'TIR_DETT','TRDESDET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TR__COL1),'TIR_DETT','TR__COL1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TR__COL2),'TIR_DETT','TR__COL2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TR__COL3),'TIR_DETT','TR__COL3');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TC__COLO),'TIR_DETT','TC__COLO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TC__INDE),'TIR_DETT','TC__INDE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TC__FTST),'TIR_DETT','TC__FTST');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TC__FONT),'TIR_DETT','TC__FONT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TR__FLAG),'TIR_DETT','TR__FLAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TRSEQCAL),'TIR_DETT','TRSEQCAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TR_RIFUE),'TIR_DETT','TR_RIFUE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TRCODICE',this.w_TRCODICE,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'TRTIPDET',this.w_TRTIPDET,'TRTIPVOC',this.w_TRTIPVOC,'TRDESDET',this.w_TRDESDET,'TR__COL1',this.w_TR__COL1,'TR__COL2',this.w_TR__COL2,'TR__COL3',this.w_TR__COL3,'TC__COLO',this.w_TC__COLO,'TC__INDE',this.w_TC__INDE,'TC__FTST',this.w_TC__FTST)
      insert into (i_cTable) (TRCODICE,CPROWNUM,CPROWORD,TRTIPDET,TRTIPVOC,TRDESDET,TR__COL1,TR__COL2,TR__COL3,TC__COLO,TC__INDE,TC__FTST,TC__FONT,TR__FLAG,TRSEQCAL,TR_RIFUE &i_ccchkf. );
         values (;
           this.w_TRCODICE;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_TRTIPDET;
           ,this.w_TRTIPVOC;
           ,this.w_TRDESDET;
           ,this.w_TR__COL1;
           ,this.w_TR__COL2;
           ,this.w_TR__COL3;
           ,this.w_TC__COLO;
           ,this.w_TC__INDE;
           ,this.w_TC__FTST;
           ,this.w_TC__FONT;
           ,this.w_TR__FLAG;
           ,this.w_TRSEQCAL;
           ,this.w_TR_RIFUE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04ED1E78()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into TIR_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TIR_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIR_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TIR_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'TIR_DETT','CPROWORD');
      +",TRTIPDET ="+cp_NullLink(cp_ToStrODBC(this.w_TRTIPDET),'TIR_DETT','TRTIPDET');
      +",TRTIPVOC ="+cp_NullLink(cp_ToStrODBC(this.w_TRTIPVOC),'TIR_DETT','TRTIPVOC');
      +",TRDESDET ="+cp_NullLink(cp_ToStrODBC(this.w_TRDESDET),'TIR_DETT','TRDESDET');
      +",TR__COL1 ="+cp_NullLink(cp_ToStrODBC(this.w_TR__COL1),'TIR_DETT','TR__COL1');
      +",TR__COL2 ="+cp_NullLink(cp_ToStrODBC(this.w_TR__COL2),'TIR_DETT','TR__COL2');
      +",TR__COL3 ="+cp_NullLink(cp_ToStrODBC(this.w_TR__COL3),'TIR_DETT','TR__COL3');
      +",TC__COLO ="+cp_NullLink(cp_ToStrODBC(this.w_TC__COLO),'TIR_DETT','TC__COLO');
      +",TC__INDE ="+cp_NullLink(cp_ToStrODBC(this.w_TC__INDE),'TIR_DETT','TC__INDE');
      +",TC__FTST ="+cp_NullLink(cp_ToStrODBC(this.w_TC__FTST),'TIR_DETT','TC__FTST');
      +",TC__FONT ="+cp_NullLink(cp_ToStrODBC(this.w_TC__FONT),'TIR_DETT','TC__FONT');
      +",TR__FLAG ="+cp_NullLink(cp_ToStrODBC(this.w_TR__FLAG),'TIR_DETT','TR__FLAG');
      +",TRSEQCAL ="+cp_NullLink(cp_ToStrODBC(this.w_TRSEQCAL),'TIR_DETT','TRSEQCAL');
      +",TR_RIFUE ="+cp_NullLink(cp_ToStrODBC(this.w_TR_RIFUE),'TIR_DETT','TR_RIFUE');
          +i_ccchkf ;
      +" where ";
          +"TRCODICE = "+cp_ToStrODBC(this.w_TRCODICE);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          CPROWORD = this.w_CPROWORD;
          ,TRTIPDET = this.w_TRTIPDET;
          ,TRTIPVOC = this.w_TRTIPVOC;
          ,TRDESDET = this.w_TRDESDET;
          ,TR__COL1 = this.w_TR__COL1;
          ,TR__COL2 = this.w_TR__COL2;
          ,TR__COL3 = this.w_TR__COL3;
          ,TC__COLO = this.w_TC__COLO;
          ,TC__INDE = this.w_TC__INDE;
          ,TC__FTST = this.w_TC__FTST;
          ,TC__FONT = this.w_TC__FONT;
          ,TR__FLAG = this.w_TR__FLAG;
          ,TRSEQCAL = this.w_TRSEQCAL;
          ,TR_RIFUE = this.w_TR_RIFUE;
          &i_ccchkf. ;
       where;
          TRCODICE = this.w_TRCODICE;
          and CPROWNUM = this.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04EAD308()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into RIGTOTAL
    i_nConn=i_TableProp[this.RIGTOTAL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RIGTOTAL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RIGTOTAL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RTCODICE"+",RTNUMRIG"+",RTRIGTOT"+",RT_SEGNO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_RTCODICE),'RIGTOTAL','RTCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RTNUMRIG),'RIGTOTAL','RTNUMRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RTRIGTOT),'RIGTOTAL','RTRIGTOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RT_SEGNO),'RIGTOTAL','RT_SEGNO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RTCODICE',this.w_RTCODICE,'RTNUMRIG',this.w_RTNUMRIG,'RTRIGTOT',this.w_RTRIGTOT,'RT_SEGNO',this.w_RT_SEGNO)
      insert into (i_cTable) (RTCODICE,RTNUMRIG,RTRIGTOT,RT_SEGNO &i_ccchkf. );
         values (;
           this.w_RTCODICE;
           ,this.w_RTNUMRIG;
           ,this.w_RTRIGTOT;
           ,this.w_RT_SEGNO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04EA5F28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into VOCIRICL
    i_nConn=i_TableProp[this.VOCIRICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOCIRICL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VOCIRICL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"VRCODVOC"+",VRROWORD"+",VRDESVOC"+",VR__NOTE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_VRCODVOC),'VOCIRICL','VRCODVOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VRROWORD),'VOCIRICL','VRROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VRDESVOC),'VOCIRICL','VRDESVOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VR__NOTE),'VOCIRICL','VR__NOTE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'VRCODVOC',this.w_VRCODVOC,'VRROWORD',this.w_VRROWORD,'VRDESVOC',this.w_VRDESVOC,'VR__NOTE',this.w_VR__NOTE)
      insert into (i_cTable) (VRCODVOC,VRROWORD,VRDESVOC,VR__NOTE &i_ccchkf. );
         values (;
           this.w_VRCODVOC;
           ,this.w_VRROWORD;
           ,this.w_VRDESVOC;
           ,this.w_VR__NOTE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04E9CDE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DETTRICL
    i_nConn=i_TableProp[this.DETTRICL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DETTRICL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DETTRICL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DVCODVOC"+",DVROWORD"+",CPROWNUM"+",DVFLMACO"+",DVCODMAS"+",DVCODCON"+",DV_SEGNO"+",DV__DAVE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_DVCODVOC),'DETTRICL','DVCODVOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DVROWORD),'DETTRICL','DVROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM2),'DETTRICL','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DVFLMACO),'DETTRICL','DVFLMACO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DVCODMAS),'DETTRICL','DVCODMAS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DVCODCON),'DETTRICL','DVCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DV_SEGNO),'DETTRICL','DV_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DV__DAVE),'DETTRICL','DV__DAVE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DVCODVOC',this.w_DVCODVOC,'DVROWORD',this.w_DVROWORD,'CPROWNUM',this.w_CPROWNUM2,'DVFLMACO',this.w_DVFLMACO,'DVCODMAS',this.w_DVCODMAS,'DVCODCON',this.w_DVCODCON,'DV_SEGNO',this.w_DV_SEGNO,'DV__DAVE',this.w_DV__DAVE)
      insert into (i_cTable) (DVCODVOC,DVROWORD,CPROWNUM,DVFLMACO,DVCODMAS,DVCODCON,DV_SEGNO,DV__DAVE &i_ccchkf. );
         values (;
           this.w_DVCODVOC;
           ,this.w_DVROWORD;
           ,this.w_CPROWNUM2;
           ,this.w_DVFLMACO;
           ,this.w_DVCODMAS;
           ,this.w_DVCODCON;
           ,this.w_DV_SEGNO;
           ,this.w_DV__DAVE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_19
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica Voci Bilancio Basilea2\Fisco Azienda\Bilancio&oltre
    if this.oParentObject.w_TIPARC="VF"
      this.w_CODAZI = i_CODAZI
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZPERAZI"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZPERAZI;
          from (i_cTable) where;
              AZCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERAZI = NVL(cp_ToDate(_read_.AZPERAZI),cp_NullValue(_read_.AZPERAZI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_PERAZI = ICASE(this.w_PERAZI="N","SC",this.w_PERAZI="P","SP","PF")
    endif
    this.w_OLDCHIAVE = REPL("@",65)
     
 CREATE CURSOR TEMPO ( VBGRUPPO C(15) , VBMASTRO C(15) , VB_CONTO C(15) , VB__VOCE C(20), ; 
 VBDESCRI C(120),VBNATURA C(1),VB___NOTE C(254), VBPARTEC C(1),CPROWNUM N(4),CPROWORD N(4),VB__TIPO C(1),; 
 VBCODCON C(15),VBCODMAS C(15),VBTIPVOC C(1),VBCODVOC C(15),VBROWORD N(5),VBFORMUL C(30),; 
 VB_SEGNO N(1),VBCONDIZ C(1),VBESPDET C(1))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
     
 Select TEMPO 
 Go Top
    do while Not Eof("TEMPO")
      if (TEMPO.VBMASTRO=this.w_PERAZI and this.oParentObject.w_TIPARC="VF") or this.oParentObject.w_TIPARC$ "VB-BO"
        * --- Tipo Quadro in Fisco azienda
        this.w_VBGRUPPO = TEMPO.VBGRUPPO
        * --- Tipo Azienda in Fisco azienda
        this.w_VBMASTRO = TEMPO.VBMASTRO
        * --- Riferimento Ascii in Fisco azienda
        this.w_VB_CONTO = TEMPO.VB_CONTO
        * --- Voce Logica in Fisco azienda
        this.w_VB__VOCE = TEMPO.VB__VOCE
        * --- identificativo record di Fisco azienda
        this.w_VBNATURA = IIF(this.oParentObject.w_TIPARC="VF","M",TEMPO.VBNATURA)
        this.w_VBTIPESP = ICASE(this.oParentObject.w_TIPARC="VF","M",this.oParentObject.w_TIPARC="VB","B","O")
        this.w_VBDESCRI = TEMPO.VBDESCRI
        * --- Se vuoto o nullo o diverso da 'S' / 'N' allora lo considero che partecipa
        this.w_VBPARTEC = Iif ( Not Nvl( TEMPO.VBPARTEC , "S") $"SN" , "S" , TEMPO.VBPARTEC )
        this.w_CHIAVE = Alltrim(this.w_VBGRUPPO) +"#" +Alltrim(this.w_VBMASTRO) +"#" + Alltrim(this.w_VB_CONTO) +"#" + Alltrim(this.w_VB__VOCE)+"#" + Alltrim(this.w_VBNATURA)
        this.w_CPROWNUM = NVL(TEMPO.CPROWNUM,0)
        this.w_CPROWORD = NVL(TEMPO.CPROWORD,0)
        this.w_VB__TIPO = NVL(TEMPO.VB__TIPO," ")
        this.w_VBCODCON = NVL(TEMPO.VBCODCON,Space(15))
        this.w_VBCODMAS = NVL(TEMPO.VBCODMAS,Space(15))
        this.w_VBTIPVOC = NVL(TEMPO.VBTIPVOC," ")
        this.w_VBCODVOC = NVL(TEMPO.VBCODVOC,Space(15))
        this.w_VBROWORD = NVL(TEMPO.VBROWORD,0)
        this.w_VBFORMUL = NVL(TEMPO.VBFORMUL,Space(30))
        this.w_VB_SEGNO = NVL(TEMPO.VB_SEGNO,0)
        this.w_VBCONDIZ = NVL(TEMPO.VBCONDIZ," ")
        this.w_VB___NOTE = NVL(TEMPO.VB___NOTE," ")
        this.w_VBESPDET = NVL(TEMPO.VBESPDET," ")
        if this.w_CHIAVE<>this.w_OLDCHIAVE
          * --- Try
          local bErr_04F460D0
          bErr_04F460D0=bTrsErr
          this.Try_04F460D0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into RACCBILA
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.RACCBILA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RACCBILA_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.RACCBILA_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"VBDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_VBDESCRI),'RACCBILA','VBDESCRI');
              +",VBPARTEC ="+cp_NullLink(cp_ToStrODBC(this.w_VBPARTEC),'RACCBILA','VBPARTEC');
              +",VB___NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_VB___NOTE),'RACCBILA','VB___NOTE');
              +",VBTIPESP ="+cp_NullLink(cp_ToStrODBC(this.w_VBTIPESP),'RACCBILA','VBTIPESP');
              +",VBESPDET ="+cp_NullLink(cp_ToStrODBC(this.w_VBESPDET),'RACCBILA','VBESPDET');
                  +i_ccchkf ;
              +" where ";
                  +"VBGRUPPO = "+cp_ToStrODBC(this.w_VBGRUPPO);
                  +" and VBMASTRO = "+cp_ToStrODBC(this.w_VBMASTRO);
                  +" and VB_CONTO = "+cp_ToStrODBC(this.w_VB_CONTO);
                  +" and VB__VOCE = "+cp_ToStrODBC(this.w_VB__VOCE);
                  +" and VBNATURA = "+cp_ToStrODBC(this.w_VBNATURA);
                     )
            else
              update (i_cTable) set;
                  VBDESCRI = this.w_VBDESCRI;
                  ,VBPARTEC = this.w_VBPARTEC;
                  ,VB___NOTE = this.w_VB___NOTE;
                  ,VBTIPESP = this.w_VBTIPESP;
                  ,VBESPDET = this.w_VBESPDET;
                  &i_ccchkf. ;
               where;
                  VBGRUPPO = this.w_VBGRUPPO;
                  and VBMASTRO = this.w_VBMASTRO;
                  and VB_CONTO = this.w_VB_CONTO;
                  and VB__VOCE = this.w_VB__VOCE;
                  and VBNATURA = this.w_VBNATURA;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_04F460D0
          * --- End
          this.w_OLDCHIAVE = this.w_CHIAVE
        endif
        if this.w_CPROWNUM>0
          * --- Try
          local bErr_04F53DE0
          bErr_04F53DE0=bTrsErr
          this.Try_04F53DE0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into RACDBILA
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.RACDBILA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RACDBILA_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.RACDBILA_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'RACDBILA','CPROWORD');
              +",VB__TIPO ="+cp_NullLink(cp_ToStrODBC(this.w_VB__TIPO),'RACDBILA','VB__TIPO');
              +",VB_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.w_VB_SEGNO),'RACDBILA','VB_SEGNO');
              +",VBCODCON ="+cp_NullLink(cp_ToStrODBC(this.w_VBCODCON),'RACDBILA','VBCODCON');
              +",VBCODMAS ="+cp_NullLink(cp_ToStrODBC(this.w_VBCODMAS),'RACDBILA','VBCODMAS');
              +",VBCODVOC ="+cp_NullLink(cp_ToStrODBC(this.w_VBCODVOC),'RACDBILA','VBCODVOC');
              +",VBCONDIZ ="+cp_NullLink(cp_ToStrODBC(this.w_VBCONDIZ),'RACDBILA','VBCONDIZ');
              +",VBFORMUL ="+cp_NullLink(cp_ToStrODBC(this.w_VBFORMUL),'RACDBILA','VBFORMUL');
              +",VBROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_VBROWORD),'RACDBILA','VBROWORD');
              +",VBTIPVOC ="+cp_NullLink(cp_ToStrODBC(this.w_VBTIPVOC),'RACDBILA','VBTIPVOC');
                  +i_ccchkf ;
              +" where ";
                  +"VBGRUPPO = "+cp_ToStrODBC(this.w_VBGRUPPO);
                  +" and VBMASTRO = "+cp_ToStrODBC(this.w_VBMASTRO);
                  +" and VB_CONTO = "+cp_ToStrODBC(this.w_VB_CONTO);
                  +" and VB__VOCE = "+cp_ToStrODBC(this.w_VB__VOCE);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                  +" and VBNATURA = "+cp_ToStrODBC(this.w_VBNATURA);
                     )
            else
              update (i_cTable) set;
                  CPROWORD = this.w_CPROWORD;
                  ,VB__TIPO = this.w_VB__TIPO;
                  ,VB_SEGNO = this.w_VB_SEGNO;
                  ,VBCODCON = this.w_VBCODCON;
                  ,VBCODMAS = this.w_VBCODMAS;
                  ,VBCODVOC = this.w_VBCODVOC;
                  ,VBCONDIZ = this.w_VBCONDIZ;
                  ,VBFORMUL = this.w_VBFORMUL;
                  ,VBROWORD = this.w_VBROWORD;
                  ,VBTIPVOC = this.w_VBTIPVOC;
                  &i_ccchkf. ;
               where;
                  VBGRUPPO = this.w_VBGRUPPO;
                  and VBMASTRO = this.w_VBMASTRO;
                  and VB_CONTO = this.w_VB_CONTO;
                  and VB__VOCE = this.w_VB__VOCE;
                  and CPROWNUM = this.w_CPROWNUM;
                  and VBNATURA = this.w_VBNATURA;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_04F53DE0
          * --- End
        endif
      endif
       
 Select TEMPO 
 Skip
    enddo
  endproc
  proc Try_04F460D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into RACCBILA
    i_nConn=i_TableProp[this.RACCBILA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RACCBILA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RACCBILA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"VBGRUPPO"+",VBMASTRO"+",VB_CONTO"+",VB__VOCE"+",VBNATURA"+",VBDESCRI"+",VBPARTEC"+",VB___NOTE"+",VBTIPESP"+",VBESPDET"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_VBGRUPPO),'RACCBILA','VBGRUPPO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VBMASTRO),'RACCBILA','VBMASTRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VB_CONTO),'RACCBILA','VB_CONTO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VB__VOCE),'RACCBILA','VB__VOCE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VBNATURA),'RACCBILA','VBNATURA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VBDESCRI),'RACCBILA','VBDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VBPARTEC),'RACCBILA','VBPARTEC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VB___NOTE),'RACCBILA','VB___NOTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VBTIPESP),'RACCBILA','VBTIPESP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VBESPDET),'RACCBILA','VBESPDET');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'VBGRUPPO',this.w_VBGRUPPO,'VBMASTRO',this.w_VBMASTRO,'VB_CONTO',this.w_VB_CONTO,'VB__VOCE',this.w_VB__VOCE,'VBNATURA',this.w_VBNATURA,'VBDESCRI',this.w_VBDESCRI,'VBPARTEC',this.w_VBPARTEC,'VB___NOTE',this.w_VB___NOTE,'VBTIPESP',this.w_VBTIPESP,'VBESPDET',this.w_VBESPDET)
      insert into (i_cTable) (VBGRUPPO,VBMASTRO,VB_CONTO,VB__VOCE,VBNATURA,VBDESCRI,VBPARTEC,VB___NOTE,VBTIPESP,VBESPDET &i_ccchkf. );
         values (;
           this.w_VBGRUPPO;
           ,this.w_VBMASTRO;
           ,this.w_VB_CONTO;
           ,this.w_VB__VOCE;
           ,this.w_VBNATURA;
           ,this.w_VBDESCRI;
           ,this.w_VBPARTEC;
           ,this.w_VB___NOTE;
           ,this.w_VBTIPESP;
           ,this.w_VBESPDET;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04F53DE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into RACDBILA
    i_nConn=i_TableProp[this.RACDBILA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RACDBILA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RACDBILA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"VBGRUPPO"+",VBMASTRO"+",VB_CONTO"+",VB__VOCE"+",VBNATURA"+",CPROWNUM"+",CPROWORD"+",VB__TIPO"+",VB_SEGNO"+",VBCODCON"+",VBCODMAS"+",VBCODVOC"+",VBCONDIZ"+",VBFORMUL"+",VBROWORD"+",VBTIPVOC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_VBGRUPPO),'RACDBILA','VBGRUPPO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VBMASTRO),'RACDBILA','VBMASTRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VB_CONTO),'RACDBILA','VB_CONTO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VB__VOCE),'RACDBILA','VB__VOCE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VBNATURA),'RACDBILA','VBNATURA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'RACDBILA','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'RACDBILA','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VB__TIPO),'RACDBILA','VB__TIPO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VB_SEGNO),'RACDBILA','VB_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VBCODCON),'RACDBILA','VBCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VBCODMAS),'RACDBILA','VBCODMAS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VBCODVOC),'RACDBILA','VBCODVOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VBCONDIZ),'RACDBILA','VBCONDIZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VBFORMUL),'RACDBILA','VBFORMUL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VBROWORD),'RACDBILA','VBROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VBTIPVOC),'RACDBILA','VBTIPVOC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'VBGRUPPO',this.w_VBGRUPPO,'VBMASTRO',this.w_VBMASTRO,'VB_CONTO',this.w_VB_CONTO,'VB__VOCE',this.w_VB__VOCE,'VBNATURA',this.w_VBNATURA,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'VB__TIPO',this.w_VB__TIPO,'VB_SEGNO',this.w_VB_SEGNO,'VBCODCON',this.w_VBCODCON,'VBCODMAS',this.w_VBCODMAS,'VBCODVOC',this.w_VBCODVOC)
      insert into (i_cTable) (VBGRUPPO,VBMASTRO,VB_CONTO,VB__VOCE,VBNATURA,CPROWNUM,CPROWORD,VB__TIPO,VB_SEGNO,VBCODCON,VBCODMAS,VBCODVOC,VBCONDIZ,VBFORMUL,VBROWORD,VBTIPVOC &i_ccchkf. );
         values (;
           this.w_VBGRUPPO;
           ,this.w_VBMASTRO;
           ,this.w_VB_CONTO;
           ,this.w_VB__VOCE;
           ,this.w_VBNATURA;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_VB__TIPO;
           ,this.w_VB_SEGNO;
           ,this.w_VBCODCON;
           ,this.w_VBCODMAS;
           ,this.w_VBCODVOC;
           ,this.w_VBCONDIZ;
           ,this.w_VBFORMUL;
           ,this.w_VBROWORD;
           ,this.w_VBTIPVOC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_20
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Codici Catastali
    CREATE CURSOR TEMPO (CCCODICE C(4),CCLOCALI C(40), CC___CAP C(9) , CC____PR C(2) , CCREGION C(3) , CCAREAGE C(2))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
     
 Select TEMPO 
 Go Top
    do while Not Eof("TEMPO")
      this.w_CCCODICE = TEMPO.CCCODICE
      this.w_CCLOCALI = TEMPO.CCLOCALI
      this.w_CC___CAP = TEMPO.CC___CAP
      this.w_CC____PR = TEMPO.CC____PR
      this.w_CCREGION = TEMPO.CCREGION
      this.w_CCAREAGE = TEMPO.CCAREAGE
      ah_Msg("Inserisco/aggiorno codice catastale %1",.T.,.F.,.F., this.w_CCCODICE)
      * --- Try
      local bErr_04F67E20
      bErr_04F67E20=bTrsErr
      this.Try_04F67E20()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into COD_CATA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.COD_CATA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COD_CATA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.COD_CATA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CCLOCALI ="+cp_NullLink(cp_ToStrODBC(this.w_CCLOCALI),'COD_CATA','CCLOCALI');
          +",CC___CAP ="+cp_NullLink(cp_ToStrODBC(this.w_CC___CAP),'COD_CATA','CC___CAP');
          +",CC____PR ="+cp_NullLink(cp_ToStrODBC(this.w_CC____PR),'COD_CATA','CC____PR');
          +",CCREGION ="+cp_NullLink(cp_ToStrODBC(this.w_CCREGION),'COD_CATA','CCREGION');
          +",CCAREAGE ="+cp_NullLink(cp_ToStrODBC(this.w_CCAREAGE),'COD_CATA','CCAREAGE');
              +i_ccchkf ;
          +" where ";
              +"CCCODICE = "+cp_ToStrODBC(this.w_CCCODICE);
                 )
        else
          update (i_cTable) set;
              CCLOCALI = this.w_CCLOCALI;
              ,CC___CAP = this.w_CC___CAP;
              ,CC____PR = this.w_CC____PR;
              ,CCREGION = this.w_CCREGION;
              ,CCAREAGE = this.w_CCAREAGE;
              &i_ccchkf. ;
           where;
              CCCODICE = this.w_CCCODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_04F67E20
      * --- End
       
 Select TEMPO 
 Skip
    enddo
  endproc
  proc Try_04F67E20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into COD_CATA
    i_nConn=i_TableProp[this.COD_CATA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COD_CATA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COD_CATA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CCCODICE"+",CCLOCALI"+",CC___CAP"+",CC____PR"+",CCREGION"+",CCAREAGE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CCCODICE),'COD_CATA','CCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCLOCALI),'COD_CATA','CCLOCALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CC___CAP),'COD_CATA','CC___CAP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CC____PR),'COD_CATA','CC____PR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCREGION),'COD_CATA','CCREGION');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCAREAGE),'COD_CATA','CCAREAGE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CCCODICE',this.w_CCCODICE,'CCLOCALI',this.w_CCLOCALI,'CC___CAP',this.w_CC___CAP,'CC____PR',this.w_CC____PR,'CCREGION',this.w_CCREGION,'CCAREAGE',this.w_CCAREAGE)
      insert into (i_cTable) (CCCODICE,CCLOCALI,CC___CAP,CC____PR,CCREGION,CCAREAGE &i_ccchkf. );
         values (;
           this.w_CCCODICE;
           ,this.w_CCLOCALI;
           ,this.w_CC___CAP;
           ,this.w_CC____PR;
           ,this.w_CCREGION;
           ,this.w_CCAREAGE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_21
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica messaggi controllo SSFA
    this.w_FILENAME = this.oParentObject.w_PATH
    do GSCP_BIM with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_22
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_OLDCHIAVE = REPL("@",5)
     
 CREATE CURSOR TEMPO ( TACODICE C(5) , TADESCRI C(40) , TACODCLA C(5) , TACLADES C(40), ; 
 TAFLUNIC C(1),TAFLDEFA C(1), TDPUBWEB C(1))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
     
 Select TEMPO 
 Go Top
    do while Not Eof("TEMPO")
      this.w_TACODICE = TEMPO.TACODICE
      this.w_TADESCRI = TEMPO.TADESCRI
      this.w_TACODCLA = TEMPO.TACODCLA
      this.w_TACLADES = TEMPO.TACLADES
      this.w_TAFLUNIC = iif(Nvl(TEMPO.TAFLUNIC," ")<>"S" and not empty(nvl(TEMPO.TAFLUNIC," "))," ",TEMPO.TAFLUNIC)
      this.w_TAFLDEFA = iif(Nvl(TEMPO.TAFLDEFA," ")<>"S" and not empty(nvl(TEMPO.TAFLDEFA," "))," ",TEMPO.TAFLDEFA)
      this.w_TDPUBWEB = Iif ( Not Nvl( TEMPO.TDPUBWEB , "S") $"SN" , "N" , TEMPO.TDPUBWEB )
      this.w_CHIAVE = this.w_TACODICE
      if this.w_CHIAVE<>this.w_OLDCHIAVE
        * --- Try
        local bErr_04F859D8
        bErr_04F859D8=bTrsErr
        this.Try_04F859D8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into TIP_ALLE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TIP_ALLE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_ALLE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_ALLE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TADESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_TADESCRI),'TIP_ALLE','TADESCRI');
                +i_ccchkf ;
            +" where ";
                +"TACODICE = "+cp_ToStrODBC(this.w_TACODICE);
                   )
          else
            update (i_cTable) set;
                TADESCRI = this.w_TADESCRI;
                &i_ccchkf. ;
             where;
                TACODICE = this.w_TACODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04F859D8
        * --- End
        this.w_OLDCHIAVE = this.w_CHIAVE
      endif
      if NOT EMPTY(NVL(this.w_TACODCLA,""))
        * --- Try
        local bErr_04F83968
        bErr_04F83968=bTrsErr
        this.Try_04F83968()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into CLA_ALLE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CLA_ALLE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CLA_ALLE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CLA_ALLE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TACLADES ="+cp_NullLink(cp_ToStrODBC(this.w_TACLADES),'CLA_ALLE','TACLADES');
            +",TAFLUNIC ="+cp_NullLink(cp_ToStrODBC(this.w_TAFLUNIC),'CLA_ALLE','TAFLUNIC');
            +",TAFLDEFA ="+cp_NullLink(cp_ToStrODBC(this.w_TAFLDEFA),'CLA_ALLE','TAFLDEFA');
            +",TDPUBWEB ="+cp_NullLink(cp_ToStrODBC(this.w_TDPUBWEB),'CLA_ALLE','TDPUBWEB');
                +i_ccchkf ;
            +" where ";
                +"TACODICE = "+cp_ToStrODBC(this.w_TACODICE);
                +" and TACODCLA = "+cp_ToStrODBC(this.w_TACODCLA);
                   )
          else
            update (i_cTable) set;
                TACLADES = this.w_TACLADES;
                ,TAFLUNIC = this.w_TAFLUNIC;
                ,TAFLDEFA = this.w_TAFLDEFA;
                ,TDPUBWEB = this.w_TDPUBWEB;
                &i_ccchkf. ;
             where;
                TACODICE = this.w_TACODICE;
                and TACODCLA = this.w_TACODCLA;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04F83968
        * --- End
      endif
       
 Select TEMPO 
 Skip
    enddo
  endproc
  proc Try_04F859D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TIP_ALLE
    i_nConn=i_TableProp[this.TIP_ALLE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_ALLE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_ALLE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TACODICE"+",TADESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TACODICE),'TIP_ALLE','TACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TADESCRI),'TIP_ALLE','TADESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TACODICE',this.w_TACODICE,'TADESCRI',this.w_TADESCRI)
      insert into (i_cTable) (TACODICE,TADESCRI &i_ccchkf. );
         values (;
           this.w_TACODICE;
           ,this.w_TADESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04F83968()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CLA_ALLE
    i_nConn=i_TableProp[this.CLA_ALLE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CLA_ALLE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CLA_ALLE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TACODICE"+",TACODCLA"+",TACLADES"+",TAFLUNIC"+",TAFLDEFA"+",TDPUBWEB"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TACODICE),'CLA_ALLE','TACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TACODCLA),'CLA_ALLE','TACODCLA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TACLADES),'CLA_ALLE','TACLADES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TAFLUNIC),'CLA_ALLE','TAFLUNIC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TAFLDEFA),'CLA_ALLE','TAFLDEFA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TDPUBWEB),'CLA_ALLE','TDPUBWEB');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TACODICE',this.w_TACODICE,'TACODCLA',this.w_TACODCLA,'TACLADES',this.w_TACLADES,'TAFLUNIC',this.w_TAFLUNIC,'TAFLDEFA',this.w_TAFLDEFA,'TDPUBWEB',this.w_TDPUBWEB)
      insert into (i_cTable) (TACODICE,TACODCLA,TACLADES,TAFLUNIC,TAFLDEFA,TDPUBWEB &i_ccchkf. );
         values (;
           this.w_TACODICE;
           ,this.w_TACODCLA;
           ,this.w_TACLADES;
           ,this.w_TAFLUNIC;
           ,this.w_TAFLDEFA;
           ,this.w_TDPUBWEB;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_23
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Estensioni allegati
    if Not ah_YesNo("Assicurarsi di avere gi� eseguito il caricamento delle tipologie allegati%0continuare?")
      i_retcode = 'stop'
      return
    endif
     
 CREATE CURSOR TEMPO ( EXCODICE C(10), EXDESCRI C(40) , EXTIPALL C(5) , EXCLAALL C(5),EXESTCBI C(4))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
     
 Select TEMPO 
 Go Top
    do while Not Eof("TEMPO")
      this.w_EXCODICE = TEMPO.EXCODICE
      this.w_EXDESCRI = TEMPO.EXDESCRI
      this.w_EXTIPALL = TEMPO.EXTIPALL
      this.w_EXCLAALL = TEMPO.EXCLAALL
      this.w_EXESTCBI = TEMPO.EXESTCBI
      if NOT EMPTY(NVL(this.w_EXCODICE,""))
        * --- Try
        local bErr_04F8B540
        bErr_04F8B540=bTrsErr
        this.Try_04F8B540()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into EXT_ENS
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.EXT_ENS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.EXT_ENS_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.EXT_ENS_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"EXDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_EXDESCRI),'EXT_ENS','EXDESCRI');
            +",EXTIPALL ="+cp_NullLink(cp_ToStrODBC(this.w_EXTIPALL),'EXT_ENS','EXTIPALL');
            +",EXCLAALL ="+cp_NullLink(cp_ToStrODBC(this.w_EXCLAALL),'EXT_ENS','EXCLAALL');
            +",EXESTCBI ="+cp_NullLink(cp_ToStrODBC(this.w_EXESTCBI),'EXT_ENS','EXESTCBI');
                +i_ccchkf ;
            +" where ";
                +"EXCODICE = "+cp_ToStrODBC(this.w_EXCODICE);
                   )
          else
            update (i_cTable) set;
                EXDESCRI = this.w_EXDESCRI;
                ,EXTIPALL = this.w_EXTIPALL;
                ,EXCLAALL = this.w_EXCLAALL;
                ,EXESTCBI = this.w_EXESTCBI;
                &i_ccchkf. ;
             where;
                EXCODICE = this.w_EXCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04F8B540
        * --- End
      endif
       
 Select TEMPO 
 Skip
    enddo
  endproc
  proc Try_04F8B540()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into EXT_ENS
    i_nConn=i_TableProp[this.EXT_ENS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.EXT_ENS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.EXT_ENS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"EXCODICE"+",EXDESCRI"+",EXTIPALL"+",EXCLAALL"+",EXESTCBI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_EXCODICE),'EXT_ENS','EXCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EXDESCRI),'EXT_ENS','EXDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EXTIPALL),'EXT_ENS','EXTIPALL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EXCLAALL),'EXT_ENS','EXCLAALL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EXESTCBI),'EXT_ENS','EXESTCBI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'EXCODICE',this.w_EXCODICE,'EXDESCRI',this.w_EXDESCRI,'EXTIPALL',this.w_EXTIPALL,'EXCLAALL',this.w_EXCLAALL,'EXESTCBI',this.w_EXESTCBI)
      insert into (i_cTable) (EXCODICE,EXDESCRI,EXTIPALL,EXCLAALL,EXESTCBI &i_ccchkf. );
         values (;
           this.w_EXCODICE;
           ,this.w_EXDESCRI;
           ,this.w_EXTIPALL;
           ,this.w_EXCLAALL;
           ,this.w_EXESTCBI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_24
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_ISONAZ="ESP"
      do GLES_BCP with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      ah_ErrorMsg("Modulo localizzazione spagnola non installato")
    endif
  endproc


  procedure Page_25
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_ISONAZ="ESP"
      do GLES_BIP with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      ah_ErrorMsg("Modulo localizzazione spagnola non installato")
    endif
  endproc


  procedure Page_26
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     
 CREATE CURSOR TEMPO ( TICODICE C(10) , TIPREC C(1) , CODICE C(15) , TIDESCRI C(40), ; 
 TITIPTOT C(1),TIFLDECI C(1),CPROWNUM N(4), TIFORMUL C(254),TI_FONTE C(10),TIDESMIS C(80),; 
 TICODFAZ C(15),TICODCOM C(15),CPROWORD N(4),DIVALCHR C(50),DIVALNUM N(20,5),; 
 DITIPDAT C(1),DITIPDIM C(2),DIARLINK C(15),DIVLCOMC C(10),DIVLCOMN N(10),DIVALLIN C(5),; 
 DIVALCAU C(5),DIVALATT C(5),DIVALCON C(15),DIFLZERO C(1),DIVALDAT D(8))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED WITH CHARACTER @
     
 Select TEMPO 
 Go Top
    do while Not Eof("TEMPO")
      if TEMPO.TIPREC="A"
        * --- Try
        local bErr_04FA6F18
        bErr_04FA6F18=bTrsErr
        this.Try_04FA6F18()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- aggiorno testate totalizzatori
          * --- accept error
          bTrsErr=.f.
          * --- Write into TOT_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TOT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TOT_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TOT_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TIDESCRI ="+cp_NullLink(cp_ToStrODBC(TEMPO.TIDESCRI),'TOT_MAST','TIDESCRI');
                +i_ccchkf ;
            +" where ";
                +"TICODICE = "+cp_ToStrODBC(TEMPO.TICODICE);
                   )
          else
            update (i_cTable) set;
                TIDESCRI = TEMPO.TIDESCRI;
                &i_ccchkf. ;
             where;
                TICODICE = TEMPO.TICODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04FA6F18
        * --- End
        * --- aggiorno righe totalizzatori
        * --- Try
        local bErr_04FA7218
        bErr_04FA7218=bTrsErr
        this.Try_04FA7218()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- aggiorno righe totalizzatori
          * --- accept error
          bTrsErr=.f.
          * --- Write into TOT_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TOT_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TOT_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TOT_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TIDESMIS ="+cp_NullLink(cp_ToStrODBC(TEMPO.TIDESMIS),'TOT_DETT','TIDESMIS');
            +",TIFORMUL ="+cp_NullLink(cp_ToStrODBC(TEMPO.TIFORMUL),'TOT_DETT','TIFORMUL');
            +",TICODFAZ ="+cp_NullLink(cp_ToStrODBC(TEMPO.TICODFAZ),'TOT_DETT','TICODFAZ');
            +",CPROWORD ="+cp_NullLink(cp_ToStrODBC(TEMPO.CPROWORD),'TOT_DETT','CPROWORD');
                +i_ccchkf ;
            +" where ";
                +"TICODICE = "+cp_ToStrODBC(TEMPO.TICODICE);
                +" and CPROWNUM = "+cp_ToStrODBC(TEMPO.CPROWNUM);
                   )
          else
            update (i_cTable) set;
                TIDESMIS = TEMPO.TIDESMIS;
                ,TIFORMUL = TEMPO.TIFORMUL;
                ,TICODFAZ = TEMPO.TICODFAZ;
                ,CPROWORD = TEMPO.CPROWORD;
                &i_ccchkf. ;
             where;
                TICODICE = TEMPO.TICODICE;
                and CPROWNUM = TEMPO.CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04FA7218
        * --- End
      else
        * --- aggiorno dimensioni
        * --- Try
        local bErr_04F9C958
        bErr_04F9C958=bTrsErr
        this.Try_04F9C958()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- aggiorno Dimensioni
          * --- accept error
          bTrsErr=.f.
          * --- Write into DET_DIME
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DET_DIME_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DET_DIME_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DET_DIME_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DIDESCRI ="+cp_NullLink(cp_ToStrODBC(TEMPO.TIDESMIS),'DET_DIME','DIDESCRI');
            +",DIFLZERO ="+cp_NullLink(cp_ToStrODBC(TEMPO.DIFLZERO),'DET_DIME','DIFLZERO');
            +",DIVALCHR ="+cp_NullLink(cp_ToStrODBC(TEMPO.DIVALCHR),'DET_DIME','DIVALCHR');
            +",DIVALNUM ="+cp_NullLink(cp_ToStrODBC(TEMPO.DIVALNUM),'DET_DIME','DIVALNUM');
            +",DIVALDAT ="+cp_NullLink(cp_ToStrODBC(TEMPO.DIVALDAT),'DET_DIME','DIVALDAT');
            +",DIVLCOMC ="+cp_NullLink(cp_ToStrODBC(TEMPO.DIVLCOMC),'DET_DIME','DIVLCOMC');
            +",DIVLCOMN ="+cp_NullLink(cp_ToStrODBC(TEMPO.DIVLCOMN),'DET_DIME','DIVLCOMN');
            +",DIVALLIN ="+cp_NullLink(cp_ToStrODBC(TEMPO.DIVALLIN),'DET_DIME','DIVALLIN');
            +",DIVALCAU ="+cp_NullLink(cp_ToStrODBC(TEMPO.DIVALCAU),'DET_DIME','DIVALCAU');
            +",DIVALATT ="+cp_NullLink(cp_ToStrODBC(TEMPO.DIVALATT),'DET_DIME','DIVALATT');
            +",DIVALCON ="+cp_NullLink(cp_ToStrODBC(TEMPO.DIVALCON),'DET_DIME','DIVALCON');
                +i_ccchkf ;
            +" where ";
                +"DICODTOT = "+cp_ToStrODBC(TEMPO.TICODICE);
                +" and DICODICE = "+cp_ToStrODBC(TEMPO.CODICE);
                   )
          else
            update (i_cTable) set;
                DIDESCRI = TEMPO.TIDESMIS;
                ,DIFLZERO = TEMPO.DIFLZERO;
                ,DIVALCHR = TEMPO.DIVALCHR;
                ,DIVALNUM = TEMPO.DIVALNUM;
                ,DIVALDAT = TEMPO.DIVALDAT;
                ,DIVLCOMC = TEMPO.DIVLCOMC;
                ,DIVLCOMN = TEMPO.DIVLCOMN;
                ,DIVALLIN = TEMPO.DIVALLIN;
                ,DIVALCAU = TEMPO.DIVALCAU;
                ,DIVALATT = TEMPO.DIVALATT;
                ,DIVALCON = TEMPO.DIVALCON;
                &i_ccchkf. ;
             where;
                DICODTOT = TEMPO.TICODICE;
                and DICODICE = TEMPO.CODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04F9C958
        * --- End
      endif
       
 Select TEMPO 
 Skip
    enddo
  endproc
  proc Try_04FA6F18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- carico testate totalizzatori
    * --- Insert into TOT_MAST
    i_nConn=i_TableProp[this.TOT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TOT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TOT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TICODICE"+",TI_FONTE"+",TIDESCRI"+",TITIPTOT"+",TIFLDECI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(TEMPO.TICODICE),'TOT_MAST','TICODICE');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.TI_FONTE),'TOT_MAST','TI_FONTE');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.TIDESCRI),'TOT_MAST','TIDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.TITIPTOT),'TOT_MAST','TITIPTOT');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.TIFLDECI),'TOT_MAST','TIFLDECI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TICODICE',TEMPO.TICODICE,'TI_FONTE',TEMPO.TI_FONTE,'TIDESCRI',TEMPO.TIDESCRI,'TITIPTOT',TEMPO.TITIPTOT,'TIFLDECI',TEMPO.TIFLDECI)
      insert into (i_cTable) (TICODICE,TI_FONTE,TIDESCRI,TITIPTOT,TIFLDECI &i_ccchkf. );
         values (;
           TEMPO.TICODICE;
           ,TEMPO.TI_FONTE;
           ,TEMPO.TIDESCRI;
           ,TEMPO.TITIPTOT;
           ,TEMPO.TIFLDECI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04FA7218()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TOT_DETT
    i_nConn=i_TableProp[this.TOT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TOT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TOT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TICODICE"+",CPROWNUM"+",TIFORMUL"+",TICODMIS"+",TIMFONTE"+",TIDESMIS"+",TICODFAZ"+",TICODCOM"+",CPROWORD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(TEMPO.TICODICE),'TOT_DETT','TICODICE');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.CPROWNUM),'TOT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.TIFORMUL),'TOT_DETT','TIFORMUL');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.CODICE),'TOT_DETT','TICODMIS');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.TI_FONTE),'TOT_DETT','TIMFONTE');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.TIDESMIS),'TOT_DETT','TIDESMIS');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.TICODFAZ),'TOT_DETT','TICODFAZ');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.TICODCOM),'TOT_DETT','TICODCOM');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.CPROWORD),'TOT_DETT','CPROWORD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TICODICE',TEMPO.TICODICE,'CPROWNUM',TEMPO.CPROWNUM,'TIFORMUL',TEMPO.TIFORMUL,'TICODMIS',TEMPO.CODICE,'TIMFONTE',TEMPO.TI_FONTE,'TIDESMIS',TEMPO.TIDESMIS,'TICODFAZ',TEMPO.TICODFAZ,'TICODCOM',TEMPO.TICODCOM,'CPROWORD',TEMPO.CPROWORD)
      insert into (i_cTable) (TICODICE,CPROWNUM,TIFORMUL,TICODMIS,TIMFONTE,TIDESMIS,TICODFAZ,TICODCOM,CPROWORD &i_ccchkf. );
         values (;
           TEMPO.TICODICE;
           ,TEMPO.CPROWNUM;
           ,TEMPO.TIFORMUL;
           ,TEMPO.CODICE;
           ,TEMPO.TI_FONTE;
           ,TEMPO.TIDESMIS;
           ,TEMPO.TICODFAZ;
           ,TEMPO.TICODCOM;
           ,TEMPO.CPROWORD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04F9C958()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into DET_DIME
    i_nConn=i_TableProp[this.DET_DIME_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DET_DIME_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DET_DIME_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DICODTOT"+",DI_FONTE"+",DICODICE"+",CPROWNUM"+",CPROWORD"+",DIDESCRI"+",DIVALCHR"+",DIVALNUM"+",DIVALDAT"+",DITIPDAT"+",DITIPDIM"+",DIARLINK"+",DIVLCOMC"+",DIVLCOMN"+",DIVALLIN"+",DIVALCAU"+",DIVALATT"+",DIVALCON"+",DIFLZERO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(TEMPO.TICODICE),'DET_DIME','DICODTOT');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.TI_FONTE),'DET_DIME','DI_FONTE');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.CODICE),'DET_DIME','DICODICE');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.CPROWNUM),'DET_DIME','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.CPROWORD),'DET_DIME','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.TIDESMIS),'DET_DIME','DIDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.DIVALCHR),'DET_DIME','DIVALCHR');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.DIVALNUM),'DET_DIME','DIVALNUM');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.DIVALDAT),'DET_DIME','DIVALDAT');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.DITIPDAT),'DET_DIME','DITIPDAT');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.DITIPDIM),'DET_DIME','DITIPDIM');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.DIARLINK),'DET_DIME','DIARLINK');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.DIVLCOMC),'DET_DIME','DIVLCOMC');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.DIVLCOMN),'DET_DIME','DIVLCOMN');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.DIVALLIN),'DET_DIME','DIVALLIN');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.DIVALCAU),'DET_DIME','DIVALCAU');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.DIVALATT),'DET_DIME','DIVALATT');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.DIVALCON),'DET_DIME','DIVALCON');
      +","+cp_NullLink(cp_ToStrODBC(TEMPO.DIFLZERO),'DET_DIME','DIFLZERO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DICODTOT',TEMPO.TICODICE,'DI_FONTE',TEMPO.TI_FONTE,'DICODICE',TEMPO.CODICE,'CPROWNUM',TEMPO.CPROWNUM,'CPROWORD',TEMPO.CPROWORD,'DIDESCRI',TEMPO.TIDESMIS,'DIVALCHR',TEMPO.DIVALCHR,'DIVALNUM',TEMPO.DIVALNUM,'DIVALDAT',TEMPO.DIVALDAT,'DITIPDAT',TEMPO.DITIPDAT,'DITIPDIM',TEMPO.DITIPDIM,'DIARLINK',TEMPO.DIARLINK)
      insert into (i_cTable) (DICODTOT,DI_FONTE,DICODICE,CPROWNUM,CPROWORD,DIDESCRI,DIVALCHR,DIVALNUM,DIVALDAT,DITIPDAT,DITIPDIM,DIARLINK,DIVLCOMC,DIVLCOMN,DIVALLIN,DIVALCAU,DIVALATT,DIVALCON,DIFLZERO &i_ccchkf. );
         values (;
           TEMPO.TICODICE;
           ,TEMPO.TI_FONTE;
           ,TEMPO.CODICE;
           ,TEMPO.CPROWNUM;
           ,TEMPO.CPROWORD;
           ,TEMPO.TIDESMIS;
           ,TEMPO.DIVALCHR;
           ,TEMPO.DIVALNUM;
           ,TEMPO.DIVALDAT;
           ,TEMPO.DITIPDAT;
           ,TEMPO.DITIPDIM;
           ,TEMPO.DIARLINK;
           ,TEMPO.DIVLCOMC;
           ,TEMPO.DIVLCOMN;
           ,TEMPO.DIVALLIN;
           ,TEMPO.DIVALCAU;
           ,TEMPO.DIVALATT;
           ,TEMPO.DIVALCON;
           ,TEMPO.DIFLZERO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_27
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento Classi Documentali (DOCM)
    * --- w_NOMSG � true se import da batch GSRV_BCH_S
    CREATE CURSOR TEMPO (CLCODICE C(15), CLDESCRI C(50), CDTIPARC C(1), CDMOTARC C(1), CDCLAAWE C(15),CDRIFDEF C(1), CDRIFTAB C(20), CDMODALL C(1), ;
    CDTIPRAG C(1),CDCAMRAG C(50),CDPATSTD C(75),CPROWNUM N(5,0),CPROWORD N(5,0),CDCODATT C(15),CDDESATT C(40),CDTIPATT C(1), ;
    CDCAMCUR C(254),CDCHKOBB C(1),CDTABKEY C(15),CDVLPRED C(50),CDNATAWE C(15), CDLUNATT N(3,0), CDDECATT N(3,0), CDKEYAWE C(1), CDTIPALL C(5), CDCLAALL C(5), ;
    CDCOMTIP C(1), CDCOMCLA C(1), CDFOLDER C(1), CDFIRDIG C(1), CDCONSOS C(1), CDCLASOS C(10), CDAVVSOS N(3,0), CDKEYUPD C(1), CDGETDES C(1), ;
    CDSERVER C(20), CD__PORT N(6,0), CD__USER C(20), CDPASSWD C(20), CDARCHIVE C(20), CDFLCTRL C(1), CDFLGSOS C(2), CDRIFTEM C(1), CDATTSOS C(10), ;
    CDCLAPRA C(1), CDATTPRI C(1), CDALIASF C(15), CDFLGDES C(1), CDATTINF C(15), CDTIPOBC C(1), CDPUBWEB C(1), CDNOMFIL C(200), CDERASEF C(1), CDREPOBC C(254))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED WITH "*"
    this.w_CURSORE = SELECT()
    this.w_CLCODICEP = " "
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CLCODICE," "))
    this.w_CLCODICE = nvl(TEMPO.CLCODICE,"")
    if this.w_CLCODICE<>this.w_CLCODICEP
      * --- Inserisco testata
      this.w_TIPINSERT = "M"
      this.w_ERRORECLA = .F.
      this.w_CLCODICEP = nvl(TEMPO.CLCODICE,"")
      this.w_CLDESCRI = NVL(TEMPO.CLDESCRI,"")
      this.w_CDTIPARC = NVL(TEMPO.CDTIPARC,"")
      this.w_CDMOTARC = NVL(TEMPO.CDMOTARC,"")
      this.w_CDCLAAWE = NVL(TEMPO.CDCLAAWE,"")
      this.w_CDRIFDEF = NVL(TEMPO.CDRIFDEF,"")
      this.w_CDRIFTAB = NVL(TEMPO.CDRIFTAB,"")
      this.w_CDMODALL = NVL(TEMPO.CDMODALL,"")
      this.w_CDTIPRAG = NVL(TEMPO.CDTIPRAG,"")
      this.w_CDCAMRAG = NVL(TEMPO.CDCAMRAG,"")
      this.w_CDPATSTD = NVL(TEMPO.CDPATSTD,"")
      this.w_CDTIPALL = NVL(TEMPO.CDTIPALL,"")
      this.w_CDCLAALL = NVL(TEMPO.CDCLAALL,"")
      this.w_CDCOMTIP = NVL(TEMPO.CDCOMTIP,"")
      this.w_CDCOMCLA = NVL(TEMPO.CDCOMCLA,"")
      this.w_CDFOLDER = NVL(TEMPO.CDFOLDER,"")
      this.w_CDFIRDIG = NVL(TEMPO.CDFIRDIG,"")
      this.w_CDCONSOS = NVL(TEMPO.CDCONSOS,"")
      this.w_CDCLASOS = IIF(EMPTY(NVL(TEMPO.CDCLASOS,"")), .null.,TEMPO.CDCLASOS)
      this.w_CDAVVSOS = NVL(TEMPO.CDAVVSOS,0)
      this.w_CDKEYUPD = NVL(TEMPO.CDKEYUPD,"")
      this.w_CDGETDES = NVL(TEMPO.CDGETDES,"")
      this.w_CDSERVER = NVL(TEMPO.CDSERVER,"")
      this.w_CD__PORT = NVL(TEMPO.CD__PORT,0)
      this.w_CD__USER = NVL(TEMPO.CD__USER,"")
      this.w_CDPASSWD = NVL(TEMPO.CDPASSWD,"")
      this.w_CDARCHIVE = NVL(TEMPO.CDARCHIVE,"")
      this.w_CDFLCTRL = NVL(TEMPO.CDFLCTRL,"")
      this.w_CDCLAPRA = NVL(TEMPO.CDCLAPRA,"")
      this.w_CDALIASF = NVL(TEMPO.CDALIASF,"")
      this.w_CDFLGDES = NVL(TEMPO.CDFLGDES,"")
      this.w_CDTIPOBC = NVL(TEMPO.CDTIPOBC,"E")
      this.w_CDPUBWEB = NVL(TEMPO.CDPUBWEB,"")
      this.w_CDNOMFIL = NVL(TEMPO.CDNOMFIL,SPACE(200))
      this.w_CDERASEF = NVL(TEMPO.CDERASEF,"")
      this.w_CDREPOBC = NVL(TEMPO.CDREPOBC,"")
      GSUT_BPC(this,this.w_TIPINSERT)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ERRORECLA
        if this.w_NOMSG
          this.w_MSGERROR = this.w_MSGERROR + alltrim(this.w_CLCODICE) +", "
        else
          * --- Raise
          i_Error="Import fallito"
          return
        endif
      endif
    endif
    if not this.w_ERRORECLA
      * --- Inserisco Dettaglio
      this.w_TIPINSERT = "D"
      this.w_CPROWNUM = NVL(TEMPO.CPROWNUM,0)
      this.w_CPROWORD = NVL(TEMPO.CPROWORD,0)
      this.w_CDCODATT = NVL(TEMPO.CDCODATT,"")
      this.w_CDDESATT = NVL(TEMPO.CDDESATT,"")
      this.w_CDTIPATT = NVL(TEMPO.CDTIPATT,"")
      this.w_CDCAMCUR = TEMPO.CDCAMCUR
      this.w_CDCHKOBB = NVL(TEMPO.CDCHKOBB,"")
      this.w_CDTABKEY = NVL(TEMPO.CDTABKEY,"")
      this.w_CDVLPRED = NVL(TEMPO.CDVLPRED,"")
      this.w_CDNATAWE = NVL(TEMPO.CDNATAWE,"")
      this.w_CDLUNATT = NVL(TEMPO.CDLUNATT,0)
      this.w_CDDECATT = NVL(TEMPO.CDDECATT,0)
      this.w_CDKEYAWE = NVL(TEMPO.CDKEYAWE,"")
      this.w_CDFLGSOS = NVL(TEMPO.CDFLGSOS,"")
      this.w_CDRIFTEM = NVL(TEMPO.CDRIFTEM,"")
      this.w_CDATTSOS = IIF(EMPTY(NVL(TEMPO.CDATTSOS,"")), .null.,TEMPO.CDATTSOS)
      this.w_CDATTPRI = NVL(TEMPO.CDATTPRI,"")
      this.w_CDATTINF = NVL(TEMPO.CDATTINF,space(15))
      GSUT_BPC(this,this.w_TIPINSERT)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ERRORECLA
        if this.w_NOMSG
          this.w_MSGERROR = this.w_MSGERROR + alltrim(this.w_CLCODICE) +", "
        else
          * --- Raise
          i_Error="Import fallito"
          return
        endif
      endif
    endif
    SELECT (this.w_CURSORE)
    ENDSCAN
    if not Empty(this.w_MSGERROR)
      if Upper( this.w_PADRE.Class )="TGSRV_BCH_S"
        this.w_MSGERROR = ah_msgformat("Nell'azienda %1 non sono state importate le seguenti classi documentali perch� gi� esistenti: %2", i_CODAZI, left(this.w_MSGERROR, len(this.w_MSGERROR)-2))
      else
        this.w_MSGERROR = "Non � stato possibile importare le seguenti classi documentali: " + left(this.w_MSGERROR, len(this.w_MSGERROR)-2)
      endif
    endif
  endproc


  procedure Page_28
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamneto Processi Documentali
    CREATE CURSOR TEMPO (PDCODPRO C(10), PDDESPRO C(40), PDCLADOC C(15), PDCHKOBB C(1), PDCHKLOG C(1),PDCHKSTA C(1), PDCHKMAI C(1), ;
    PDCHKFAX C(1),PDCHKPTL C(1),PDCHKWWP C(1),PDCHKARC C(1),PDCHKANT C(1),PDRILSTA C(1),PDRILMAI C(1),PDRILFAX C(1), ;
    PDRILPTL C(1),PDRILWWP C(1),PDRILARC C(1),PDNMAXAL N(4,0),PDREPORT C(100),PDDESREP C(50),PDKEYPRO C(100), ;
    PDCONCON C(60),PDOGGETT C(254),PD_TESTO C(254),PDFILTRO C(254),PDCHKZCP C(1),PDRILZCP C(1),PDISOLAN C(60) , PDTIPRIF C(2) , PDCODSED C(60) , PDCODORD C(60),;
    PDCHKPEC C(1),PDRILPEC C(1),PDPROCES C(3))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED WITH "*"
    this.w_CURSORE = SELECT()
    this.w_PDCODPROP = " "
    this.w_PDCODPRO = " "
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(PDCODPRO," "))
    this.w_PDCODPRO = nvl(TEMPO.PDCODPRO,"")
    if this.w_PDCODPRO<>this.w_PDCODPROP
      * --- Inserisco testata
      this.w_TIPINSERT = "M"
      this.w_ERROREPROC = .F.
      * --- Tengo traccia del processo attuale
      this.w_PDCODPROP = nvl(TEMPO.PDCODPRO,"")
      this.w_PDDESPRO = NVL(TEMPO.PDDESPRO,"")
      this.w_PDCLADOC = NVL(TEMPO.PDCLADOC,"")
      this.w_PDCHKOBB = NVL(TEMPO.PDCHKOBB,"")
      this.w_PDCHKLOG = NVL(TEMPO.PDCHKLOG,"")
      this.w_PDCHKSTA = NVL(TEMPO.PDCHKSTA,"")
      this.w_PDCHKMAI = NVL(TEMPO.PDCHKMAI,"")
      this.w_PDCHKFAX = NVL(TEMPO.PDCHKFAX,"")
      this.w_PDCHKPTL = NVL(TEMPO.PDCHKPTL,"")
      this.w_PDCHKWWP = NVL(TEMPO.PDCHKWWP,"")
      this.w_PDCHKARC = NVL(TEMPO.PDCHKARC,"")
      this.w_PDCHKANT = NVL(TEMPO.PDCHKANT,"")
      this.w_PDRILSTA = NVL(TEMPO.PDRILSTA,"")
      this.w_PDRILMAI = NVL(TEMPO.PDRILMAI,"")
      this.w_PDRILFAX = NVL(TEMPO.PDRILFAX,"")
      this.w_PDRILPTL = NVL(TEMPO.PDRILPTL,"")
      this.w_PDRILWWP = NVL(TEMPO.PDRILWWP,"")
      this.w_PDRILARC = NVL(TEMPO.PDRILARC,"")
      this.w_PDNMAXAL = NVL(TEMPO.PDNMAXAL,"")
      this.w_PDCHKZCP = NVL(TEMPO.PDCHKZCP,"")
      this.w_PDRILZCP = NVL(TEMPO.PDRILZCP,"")
      this.w_PDCHKPEC = NVL (TEMPO.PDCHKPEC ,"N")
      this.w_PDRILPEC = NVL (TEMPO.PDRILPEC ,"N")
      this.w_PDPROCES = NVL (TEMPO.PDPROCES,"")
      GSDM_BCP(this,this.w_TIPINSERT)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ERROREPROC
        if this.w_NOMSG
          this.w_MSGERROR = this.w_MSGERROR + alltrim(this.w_PDCODPRO) +", "
        else
          * --- Raise
          i_Error="Import fallito"
          return
        endif
      endif
    endif
    if not this.w_ERROREPROC
      * --- Inserisco Dettaglio della regola corrente
      this.w_TIPINSERT = "D"
      this.w_PDREPORT = NVL(TEMPO.PDREPORT,"")
      this.w_PDDESREP = NVL(TEMPO.PDDESREP,"")
      this.w_PDKEYPRO = NVL(TEMPO.PDKEYPRO,"")
      this.w_PDCONCON = NVL(TEMPO.PDCONCON,"")
      this.w_PDOGGETT = TEMPO.PDOGGETT
      this.w_PD_TESTO = TEMPO.PD_TESTO
      this.w_PDFILTRO = TEMPO.PDFILTRO
      this.w_PDISOLAN = TEMPO.PDISOLAN
      this.w_PDTIPRIF = TEMPO.PDTIPRIF
      this.w_PDCODSED = NVL (TEMPO.PDCODSED , "")
      this.w_PDCODORD = NVL (TEMPO.PDCODORD , "")
      GSDM_BCP(this,this.w_TIPINSERT)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ERROREPROC
        if this.w_NOMSG
          this.w_MSGERROR = this.w_MSGERROR + alltrim(this.w_PDCODPRO) +", "
        else
          * --- Raise
          i_Error="Import fallito"
          return
        endif
      endif
    endif
    SELECT (this.w_CURSORE)
    ENDSCAN
    if not Empty(this.w_MSGERROR)
      if Upper( this.w_PADRE.Class )="TGSRV_BCH_S"
        this.w_MSGERROR = ah_msgformat("Nell'azienda %1 non sono stati importati i seguenti processi documentali perch� gi� esistenti: %2", i_CODAZI, left(this.w_MSGERROR, len(this.w_MSGERROR)-2))
      else
        this.w_MSGERROR = "Non � stato possibile importare i seguenti processi documentali: " + left(this.w_MSGERROR, len(this.w_MSGERROR)-2)
      endif
    endif
  endproc


  procedure Page_29
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento cfg gestioni
    CREATE CURSOR TEMPO (CGNOMGES C(30), PRROWNUM N(6,0), CGDESCFG C(50), CGMODSAV C(1), CGDEFCFG C(1), ; 
 CGTIPCFG C(1), CGUTEGRP N(6,0), CGCODAZI C(5), CPROWNUM N(6,0), CPROWORD N(6,0), PRNOMVAR C(20), PRNOMDES C(254), PRVALVAR C(254))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_ERRORGRU = .F.
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE) 
 GO TOP 
 SCAN FOR NOT EMPTY(NVL(CGNOMGES," "))
    if nvl(TEMPO.CGNOMGES,"") = this.w_GSNOME AND nvl(TEMPO.PRROWNUM,0)=this.w_PRROWNUM
      this.w_FLMD = "D"
    else
      this.w_FLMD = "M"
    endif
    this.w_GSNOME = nvl(TEMPO.CGNOMGES,"")
    this.w_GSNOME = STRTRAN(this.w_GSNOME, "#", '"')
    this.w_PRROWNUM = nvl(TEMPO.PRROWNUM,0)
    this.w_GSDESC = nvl(TEMPO.CGDESCFG ,"")
    this.w_GSNOMVAL = nvl(TEMPO.PRNOMVAR,"")
    this.w_GSNOMDES = nvl(TEMPO.PRNOMDES,"")
    this.w_GSVALVAR = nvl(TEMPO.PRVALVAR,"")
    this.w_GSVALVAR = STRTRAN(this.w_GSVALVAR, "#", '"')
    this.w_CPROWNUM = nvl(TEMPO.CPROWNUM,0)
    this.w_CPROWORD = nvl(TEMPO.CPROWORD,0)
    this.w_CGDEFCFG = nvl(TEMPO.CGDEFCFG,"")
    this.w_CGMODSAV = nvl(TEMPO.CGMODSAV,"")
    this.w_CGTIPCFG = nvl(TEMPO.CGTIPCFG,"")
    this.w_CGCODAZI = nvl(TEMPO.CGCODAZI,"")
    this.w_CGUTEGRP = nvl(TEMPO.CGUTEGRP,0)
    if this.w_FLMD="M"
      * --- Testata
      * --- Try
      local bErr_05079B40
      bErr_05079B40=bTrsErr
      this.Try_05079B40()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_050CB5A0
        bErr_050CB5A0=bTrsErr
        this.Try_050CB5A0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_ERRORGRU = .T.
        endif
        bTrsErr=bTrsErr or bErr_050CB5A0
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_05079B40
      * --- End
      * --- Dettaglio
      * --- Try
      local bErr_0507A8F0
      bErr_0507A8F0=bTrsErr
      this.Try_0507A8F0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into PRCFGGES
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PRCFGGES_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRCFGGES_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PRCFGGES_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PRCFGGES','CPROWORD');
          +",PRNOMVAR ="+cp_NullLink(cp_ToStrODBC(this.w_GSNOMVAL),'PRCFGGES','PRNOMVAR');
          +",PRNOMDES ="+cp_NullLink(cp_ToStrODBC(this.w_GSNOMDES),'PRCFGGES','PRNOMDES');
          +",PRVALVAR ="+cp_NullLink(cp_ToStrODBC(this.w_GSVALVAR),'PRCFGGES','PRVALVAR');
              +i_ccchkf ;
          +" where ";
              +"PRNOMGES = "+cp_ToStrODBC(this.w_GSNOME);
              +" and PRROWNUM = "+cp_ToStrODBC(this.w_PRROWNUM);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 )
        else
          update (i_cTable) set;
              CPROWORD = this.w_CPROWORD;
              ,PRNOMVAR = this.w_GSNOMVAL;
              ,PRNOMDES = this.w_GSNOMDES;
              ,PRVALVAR = this.w_GSVALVAR;
              &i_ccchkf. ;
           where;
              PRNOMGES = this.w_GSNOME;
              and PRROWNUM = this.w_PRROWNUM;
              and CPROWNUM = this.w_CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Try
        local bErr_050C8AE0
        bErr_050C8AE0=bTrsErr
        this.Try_050C8AE0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_ERRORGRU = .T.
        endif
        bTrsErr=bTrsErr or bErr_050C8AE0
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_0507A8F0
      * --- End
    else
      * --- Dettaglio
      * --- Try
      local bErr_050CDFA0
      bErr_050CDFA0=bTrsErr
      this.Try_050CDFA0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into PRCFGGES
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PRCFGGES_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRCFGGES_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PRCFGGES_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PRCFGGES','CPROWORD');
          +",PRNOMVAR ="+cp_NullLink(cp_ToStrODBC(this.w_GSNOMVAL),'PRCFGGES','PRNOMVAR');
          +",PRNOMDES ="+cp_NullLink(cp_ToStrODBC(this.w_GSNOMDES),'PRCFGGES','PRNOMDES');
          +",PRVALVAR ="+cp_NullLink(cp_ToStrODBC(this.w_GSVALVAR),'PRCFGGES','PRVALVAR');
              +i_ccchkf ;
          +" where ";
              +"PRNOMGES = "+cp_ToStrODBC(this.w_GSNOME);
              +" and PRROWNUM = "+cp_ToStrODBC(this.w_PRROWNUM);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 )
        else
          update (i_cTable) set;
              CPROWORD = this.w_CPROWORD;
              ,PRNOMVAR = this.w_GSNOMVAL;
              ,PRNOMDES = this.w_GSNOMDES;
              ,PRVALVAR = this.w_GSVALVAR;
              &i_ccchkf. ;
           where;
              PRNOMGES = this.w_GSNOME;
              and PRROWNUM = this.w_PRROWNUM;
              and CPROWNUM = this.w_CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Try
        local bErr_050CF8F0
        bErr_050CF8F0=bTrsErr
        this.Try_050CF8F0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.w_ERRORGRU = .T.
        endif
        bTrsErr=bTrsErr or bErr_050CF8F0
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_050CDFA0
      * --- End
    endif
    if this.w_ERRORGRU
      * --- Raise
      i_Error=ah_Msgformat("Import fallito")
      return
    endif
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_05079B40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CFG_GEST
    i_nConn=i_TableProp[this.CFG_GEST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CFG_GEST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CFG_GEST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CGNOMGES"+",CPROWNUM"+",CGDESCFG"+",CGMODSAV"+",CGDEFCFG"+",CGTIPCFG"+",CGUTEGRP"+",CGCODAZI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GSNOME),'CFG_GEST','CGNOMGES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRROWNUM),'CFG_GEST','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSDESC),'CFG_GEST','CGDESCFG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CGMODSAV),'CFG_GEST','CGMODSAV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CGDEFCFG),'CFG_GEST','CGDEFCFG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CGTIPCFG),'CFG_GEST','CGTIPCFG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CGUTEGRP),'CFG_GEST','CGUTEGRP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CGCODAZI),'CFG_GEST','CGCODAZI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CGNOMGES',this.w_GSNOME,'CPROWNUM',this.w_PRROWNUM,'CGDESCFG',this.w_GSDESC,'CGMODSAV',this.w_CGMODSAV,'CGDEFCFG',this.w_CGDEFCFG,'CGTIPCFG',this.w_CGTIPCFG,'CGUTEGRP',this.w_CGUTEGRP,'CGCODAZI',this.w_CGCODAZI)
      insert into (i_cTable) (CGNOMGES,CPROWNUM,CGDESCFG,CGMODSAV,CGDEFCFG,CGTIPCFG,CGUTEGRP,CGCODAZI &i_ccchkf. );
         values (;
           this.w_GSNOME;
           ,this.w_PRROWNUM;
           ,this.w_GSDESC;
           ,this.w_CGMODSAV;
           ,this.w_CGDEFCFG;
           ,this.w_CGTIPCFG;
           ,this.w_CGUTEGRP;
           ,this.w_CGCODAZI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_050CB5A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CFG_GEST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CFG_GEST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CFG_GEST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CFG_GEST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CGDESCFG ="+cp_NullLink(cp_ToStrODBC(this.w_GSDESC),'CFG_GEST','CGDESCFG');
      +",CGMODSAV ="+cp_NullLink(cp_ToStrODBC(this.w_CGMODSAV),'CFG_GEST','CGMODSAV');
      +",CGDEFCFG ="+cp_NullLink(cp_ToStrODBC(this.w_CGDEFCFG),'CFG_GEST','CGDEFCFG');
      +",CGTIPCFG ="+cp_NullLink(cp_ToStrODBC(this.w_CGTIPCFG),'CFG_GEST','CGTIPCFG');
      +",CGUTEGRP ="+cp_NullLink(cp_ToStrODBC(this.w_CGUTEGRP),'CFG_GEST','CGUTEGRP');
      +",CGCODAZI ="+cp_NullLink(cp_ToStrODBC(this.w_CGCODAZI),'CFG_GEST','CGCODAZI');
          +i_ccchkf ;
      +" where ";
          +"CGNOMGES = "+cp_ToStrODBC(this.w_GSNOME);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_PRROWNUM);
             )
    else
      update (i_cTable) set;
          CGDESCFG = this.w_GSDESC;
          ,CGMODSAV = this.w_CGMODSAV;
          ,CGDEFCFG = this.w_CGDEFCFG;
          ,CGTIPCFG = this.w_CGTIPCFG;
          ,CGUTEGRP = this.w_CGUTEGRP;
          ,CGCODAZI = this.w_CGCODAZI;
          &i_ccchkf. ;
       where;
          CGNOMGES = this.w_GSNOME;
          and CPROWNUM = this.w_PRROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_0507A8F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRCFGGES
    i_nConn=i_TableProp[this.PRCFGGES_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRCFGGES_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRCFGGES_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRNOMGES"+",PRROWNUM"+",CPROWNUM"+",CPROWORD"+",PRNOMVAR"+",PRNOMDES"+",PRVALVAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GSNOME),'PRCFGGES','PRNOMGES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRROWNUM),'PRCFGGES','PRROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PRCFGGES','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PRCFGGES','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSNOMVAL),'PRCFGGES','PRNOMVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSNOMDES),'PRCFGGES','PRNOMDES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSVALVAR),'PRCFGGES','PRVALVAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRNOMGES',this.w_GSNOME,'PRROWNUM',this.w_PRROWNUM,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PRNOMVAR',this.w_GSNOMVAL,'PRNOMDES',this.w_GSNOMDES,'PRVALVAR',this.w_GSVALVAR)
      insert into (i_cTable) (PRNOMGES,PRROWNUM,CPROWNUM,CPROWORD,PRNOMVAR,PRNOMDES,PRVALVAR &i_ccchkf. );
         values (;
           this.w_GSNOME;
           ,this.w_PRROWNUM;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_GSNOMVAL;
           ,this.w_GSNOMDES;
           ,this.w_GSVALVAR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_050C8AE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    return
  proc Try_050CDFA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRCFGGES
    i_nConn=i_TableProp[this.PRCFGGES_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRCFGGES_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRCFGGES_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRNOMGES"+",PRROWNUM"+",CPROWNUM"+",CPROWORD"+",PRNOMVAR"+",PRNOMDES"+",PRVALVAR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GSNOME),'PRCFGGES','PRNOMGES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRROWNUM),'PRCFGGES','PRROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PRCFGGES','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PRCFGGES','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSNOMVAL),'PRCFGGES','PRNOMVAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSNOMDES),'PRCFGGES','PRNOMDES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GSVALVAR),'PRCFGGES','PRVALVAR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRNOMGES',this.w_GSNOME,'PRROWNUM',this.w_PRROWNUM,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PRNOMVAR',this.w_GSNOMVAL,'PRNOMDES',this.w_GSNOMDES,'PRVALVAR',this.w_GSVALVAR)
      insert into (i_cTable) (PRNOMGES,PRROWNUM,CPROWNUM,CPROWORD,PRNOMVAR,PRNOMDES,PRVALVAR &i_ccchkf. );
         values (;
           this.w_GSNOME;
           ,this.w_PRROWNUM;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_GSNOMVAL;
           ,this.w_GSNOMDES;
           ,this.w_GSVALVAR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_050CF8F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    return


  procedure Page_30
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modelli documenti
    if Not ah_YesNo("Assicurarsi di avere gi� eseguito il caricamento delle classi documentali%0Continuare?")
      i_retcode = 'stop'
      return
    endif
    this.w_OK = .T.
    this.w_AggiornaTipo_Mate = .T.
    this.w_Procedi_Tipo_Mate = .T.
    * --- Select from PROMODEL
    i_nConn=i_TableProp[this.PROMODEL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMODEL_idx,2],.t.,this.PROMODEL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MOCODICE  from "+i_cTable+" PROMODEL ";
           ,"_Curs_PROMODEL")
    else
      select MOCODICE from (i_cTable);
        into cursor _Curs_PROMODEL
    endif
    if used('_Curs_PROMODEL')
      select _Curs_PROMODEL
      locate for 1=1
      do while not(eof())
      this.w_OK = ah_YesNo("Sono gi� presenti alcuni modelli all'interno del database. Si desidera inserire i nuovi modelli anche a parit� di descrizione?%0Rispondendo No, i modelli del file di testo con descrizione gi� presente nel database non verranno considerati.")
      if !this.w_Ok
        * --- Si � scelto di non forzare il caricamento dei nuovi modelli
        this.w_AggiornaTipo_Mate = ah_YesNo("Si desidera eventualmente aggiornare i dati relativi a tipologia, materia e autorit� pratica nei modelli gi� presenti nel database?")
      endif
      exit
        select _Curs_PROMODEL
        continue
      enddo
      use
    endif
    CREATE CURSOR TEMPO (MOCODICE C(10), MODESCRI C(60), MOCLASSE C(15), MOPATMOD C(254), MOQUERY C(60), MO__NOTE M(10), MOPREFER C(1),; 
 MOTIPPRA C(10), MOMATPRA C(10), MO__ENTE C(10), MODTOBSO D, MODTINVA D)
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    * --- Nel .txt il campo MODTOBSO � definito come carattere
    SELECT TEMPO
    SCAN
    this.w_MODESCRI = MODESCRI
    this.w_MOTIPPRA = MOTIPPRA
    this.w_MOMATPRA = MOMATPRA
    this.w_MO__ENTE = MO__ENTE
    this.w_MODTOBSO = NVL(MODTOBSO,cp_CharToDate(" - - "))
    this.w_MODTINVA = NVL(MODTINVA,cp_CharToDate(" - - "))
    this.w_Procedi_Tipo_Mate = .F.
    * --- Read from PROMODEL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PROMODEL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMODEL_idx,2],.t.,this.PROMODEL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MOCODICE"+;
        " from "+i_cTable+" PROMODEL where ";
            +"MODESCRI = "+cp_ToStrODBC(this.w_MODESCRI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MOCODICE;
        from (i_cTable) where;
            MODESCRI = this.w_MODESCRI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MOCODICE = NVL(cp_ToDate(_read_.MOCODICE),cp_NullValue(_read_.MOCODICE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_rows=0 or this.w_OK
      this.w_Procedi_Tipo_Mate = .T.
      this.w_MOCODICE = Space(10)
      i_Conn=i_TableProp[this.PROMODEL_IDX, 3]
      cp_NextTableProg(this, i_Conn, "SEMDO", "i_codazi,w_MOCODICE")
      this.w_MOCLASSE = MOCLASSE
      this.w_MOPATMOD = MOPATMOD
      this.w_MOQUERY = MOQUERY
      this.w_MO__NOTE = MO__NOTE
      this.w_MOPREFER = MOPREFER
      ah_Msg("Inserimento modello %1",.T.,.F.,.F., this.w_MOCODICE)
      * --- Insert into PROMODEL
      i_nConn=i_TableProp[this.PROMODEL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PROMODEL_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PROMODEL_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"MOCODICE"+",MODESCRI"+",MOCLASSE"+",MOPATMOD"+",MOQUERY"+",MO__NOTE"+",MOPREFER"+",MODTOBSO"+",MODTINVA"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_MOCODICE),'PROMODEL','MOCODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MODESCRI),'PROMODEL','MODESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MOCLASSE),'PROMODEL','MOCLASSE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MOPATMOD),'PROMODEL','MOPATMOD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MOQUERY),'PROMODEL','MOQUERY');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MO__NOTE),'PROMODEL','MO__NOTE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MOPREFER),'PROMODEL','MOPREFER');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MODTOBSO),'PROMODEL','MODTOBSO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MODTINVA),'PROMODEL','MODTINVA');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'MOCODICE',this.w_MOCODICE,'MODESCRI',this.w_MODESCRI,'MOCLASSE',this.w_MOCLASSE,'MOPATMOD',this.w_MOPATMOD,'MOQUERY',this.w_MOQUERY,'MO__NOTE',this.w_MO__NOTE,'MOPREFER',this.w_MOPREFER,'MODTOBSO',this.w_MODTOBSO,'MODTINVA',this.w_MODTINVA)
        insert into (i_cTable) (MOCODICE,MODESCRI,MOCLASSE,MOPATMOD,MOQUERY,MO__NOTE,MOPREFER,MODTOBSO,MODTINVA &i_ccchkf. );
           values (;
             this.w_MOCODICE;
             ,this.w_MODESCRI;
             ,this.w_MOCLASSE;
             ,this.w_MOPATMOD;
             ,this.w_MOQUERY;
             ,this.w_MO__NOTE;
             ,this.w_MOPREFER;
             ,this.w_MODTOBSO;
             ,this.w_MODTINVA;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    * --- Modello appena inserito o aggiornamento di un modello gi� presente
    if !EMPTY(this.w_MOCODICE) AND (this.w_Procedi_Tipo_Mate OR this.w_AggiornaTipo_Mate)
      * --- Tentiamo di aggiornare tipo, materia ed ente
      if !EMPTY(this.w_MOTIPPRA)
        * --- Accettiamo l'errore: significa semplicemente che il tipo in esame non � presente nei tipi pratica
        * --- Try
        local bErr_050DA198
        bErr_050DA198=bTrsErr
        this.Try_050DA198()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_050DA198
        * --- End
      endif
      if !EMPTY(this.w_MOMATPRA)
        * --- Accettiamo l'errore: significa semplicemente che la materia in esame non � presente nelle autorit� pratica
        * --- Try
        local bErr_050E7F98
        bErr_050E7F98=bTrsErr
        this.Try_050E7F98()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_050E7F98
        * --- End
      endif
      if !EMPTY(this.w_MO__ENTE)
        * --- Accettiamo l'errore: significa semplicemente che l'ente in esame non � presente nelle autorit� pratica
        * --- Try
        local bErr_050E61F8
        bErr_050E61F8=bTrsErr
        this.Try_050E61F8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_050E61F8
        * --- End
      endif
    endif
    ENDSCAN
  endproc
  proc Try_050DA198()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PROMODEL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PROMODEL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMODEL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMODEL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MOTIPPRA ="+cp_NullLink(cp_ToStrODBC(this.w_MOTIPPRA),'PROMODEL','MOTIPPRA');
          +i_ccchkf ;
      +" where ";
          +"MOCODICE = "+cp_ToStrODBC(this.w_MOCODICE);
             )
    else
      update (i_cTable) set;
          MOTIPPRA = this.w_MOTIPPRA;
          &i_ccchkf. ;
       where;
          MOCODICE = this.w_MOCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_050E7F98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PROMODEL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PROMODEL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMODEL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMODEL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MOMATPRA ="+cp_NullLink(cp_ToStrODBC(this.w_MOMATPRA),'PROMODEL','MOMATPRA');
          +i_ccchkf ;
      +" where ";
          +"MOCODICE = "+cp_ToStrODBC(this.w_MOCODICE);
             )
    else
      update (i_cTable) set;
          MOMATPRA = this.w_MOMATPRA;
          &i_ccchkf. ;
       where;
          MOCODICE = this.w_MOCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_050E61F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PROMODEL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PROMODEL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMODEL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMODEL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MO__ENTE ="+cp_NullLink(cp_ToStrODBC(this.w_MO__ENTE),'PROMODEL','MO__ENTE');
          +i_ccchkf ;
      +" where ";
          +"MOCODICE = "+cp_ToStrODBC(this.w_MOCODICE);
             )
    else
      update (i_cTable) set;
          MO__ENTE = this.w_MO__ENTE;
          &i_ccchkf. ;
       where;
          MOCODICE = this.w_MOCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_31
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Uffici competenti relativi al comune
    this.w_OLD_DESCOM = SPACE(50)
    CREATE CURSOR TEMPO (CCDESCOM C(50), CCTIPUFF C(1), CCLOCUFF C(50), CCINDUFF C(50), CCTELUFF C(50), CCFAXUFF C(50), CCMAILUF C(50) )
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
     
 Select TEMPO 
 Go Top
    do while Not Eof("TEMPO")
      this.w_CONTA = this.w_CONTA + 1
      this.w_CCDESCOM = left(Nvl(TEMPO.CCDESCOM,Space(50)),50)
      this.w_CCTIPUFF = left(Nvl(TEMPO.CCTIPUFF,Space(1)),1)
      this.w_CCLOCUFF = left(Nvl(TEMPO.CCLOCUFF,Space(50)),50)
      this.w_CCINDUFF = left(Nvl(TEMPO.CCINDUFF,Space(50)),50)
      this.w_CCTELUFF = left(Nvl(TEMPO.CCTELUFF,Space(50)),50)
      this.w_CCFAXUFF = left(Nvl(TEMPO.CCFAXUFF,Space(50)),50)
      this.w_CCMAILUF = left(Nvl(TEMPO.CCMAILUF,Space(50)),50)
      * --- In presenza di un nuovo Comune sul txt
      if this.w_OLD_DESCOM <> this.w_CCDESCOM
        this.w_ROWNUM = 0
        this.w_SERIAL = space(20)
        * --- Read from UFF_COMP
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UFF_COMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UFF_COMP_idx,2],.t.,this.UFF_COMP_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UPSERIAL"+;
            " from "+i_cTable+" UFF_COMP where ";
                +"UPDESCOM = "+cp_ToStrODBC(this.w_CCDESCOM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UPSERIAL;
            from (i_cTable) where;
                UPDESCOM = this.w_CCDESCOM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SERIAL = NVL(cp_ToDate(_read_.UPSERIAL),cp_NullValue(_read_.UPSERIAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Se esiste il Comune nella tabella UFF_COMP
        if NOT EMPTY(this.w_SERIAL)
          * --- Cancella le righe relative al Comune
          * --- Delete from UFF_COMP
          i_nConn=i_TableProp[this.UFF_COMP_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.UFF_COMP_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"UPSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            delete from (i_cTable) where;
                  UPSERIAL = this.w_SERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        else
          i_Conn=i_TableProp[this.UFF_COMP_IDX, 3]
          * --- Legge il progressivo, lo incrementa di 1 (ponendo il valore ottenuto in w_SERIAL) ed aggiorna la tabella
          cp_NextTableProg(this, i_Conn, "UFFCO", "w_SERIAL")
        endif
      endif
      this.w_ROWNUM = this.w_ROWNUM + 1
      if MOD(this.w_CONTA,100)=0
        ah_Msg("Inserisco/aggiorno ufficio comune %1",.T.,.F.,.F., alltrim(this.w_CCLOCUFF))
      endif
      * --- Try
      local bErr_05107CD0
      bErr_05107CD0=bTrsErr
      this.Try_05107CD0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_05107CD0
      * --- End
      this.w_OLD_DESCOM = this.w_CCDESCOM
       
 Select TEMPO 
 Skip
    enddo
  endproc
  proc Try_05107CD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into UFF_COMP
    i_nConn=i_TableProp[this.UFF_COMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UFF_COMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.UFF_COMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"UPSERIAL"+",UPDESCOM"+",CPROWNUM"+",UPTIPUFF"+",UPLOCUFF"+",UPINDUFF"+",UPTELUFF"+",UPFAXUFF"+",UPMAILUF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'UFF_COMP','UPSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESCOM),'UFF_COMP','UPDESCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'UFF_COMP','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCTIPUFF),'UFF_COMP','UPTIPUFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCLOCUFF),'UFF_COMP','UPLOCUFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCINDUFF),'UFF_COMP','UPINDUFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCTELUFF),'UFF_COMP','UPTELUFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFAXUFF),'UFF_COMP','UPFAXUFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCMAILUF),'UFF_COMP','UPMAILUF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'UPSERIAL',this.w_SERIAL,'UPDESCOM',this.w_CCDESCOM,'CPROWNUM',this.w_ROWNUM,'UPTIPUFF',this.w_CCTIPUFF,'UPLOCUFF',this.w_CCLOCUFF,'UPINDUFF',this.w_CCINDUFF,'UPTELUFF',this.w_CCTELUFF,'UPFAXUFF',this.w_CCFAXUFF,'UPMAILUF',this.w_CCMAILUF)
      insert into (i_cTable) (UPSERIAL,UPDESCOM,CPROWNUM,UPTIPUFF,UPLOCUFF,UPINDUFF,UPTELUFF,UPFAXUFF,UPMAILUF &i_ccchkf. );
         values (;
           this.w_SERIAL;
           ,this.w_CCDESCOM;
           ,this.w_ROWNUM;
           ,this.w_CCTIPUFF;
           ,this.w_CCLOCUFF;
           ,this.w_CCINDUFF;
           ,this.w_CCTELUFF;
           ,this.w_CCFAXUFF;
           ,this.w_CCMAILUF;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_32
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    CREATE CURSOR TEMPO (RMSERIAL C(5), RMCODICE C(5), RMDESCRI C(60),RMDESAGG C(60), RMVALMIN N(18,4), RMVALMAX N(18,4))
    this.OldPoint = SET("POINT")
    SET POINT TO "."
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    SET POINT TO this.OldPoint
     
 Select TEMPO 
 Go Top
    do while Not Eof("TEMPO")
      this.w_RMSERIAL = TEMPO.RMSERIAL
      i_Conn=i_TableProp[this.UFF_COMP_IDX, 3]
      cp_NextTableProg(this, i_Conn, "SERIM", "w_RMSERIAL")
      this.w_RMCODICE = TEMPO.RMCODICE
      this.w_RMDESCRI = TEMPO.RMDESCRI
      this.w_RMDESAGG = TEMPO.RMDESAGG
      this.w_RMVALMIN = TEMPO.RMVALMIN
      this.w_RMVALMAX = TEMPO.RMVALMAX
      * --- In presenza di un nuovo Comune sul txt
      ah_Msg("Inserisco/aggiorno righe modello %1",.T.,.F.,.F., alltrim(this.w_RMSERIAL))
      * --- Try
      local bErr_05118D28
      bErr_05118D28=bTrsErr
      this.Try_05118D28()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into SET_RIGH
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SET_RIGH_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SET_RIGH_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SET_RIGH_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"RMCODICE ="+cp_NullLink(cp_ToStrODBC(this.w_RMCODICE),'SET_RIGH','RMCODICE');
          +",RMDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_RMDESCRI),'SET_RIGH','RMDESCRI');
          +",RMDESAGG ="+cp_NullLink(cp_ToStrODBC(this.w_RMDESAGG),'SET_RIGH','RMDESAGG');
          +",RMVALMIN ="+cp_NullLink(cp_ToStrODBC(this.w_RMVALMIN),'SET_RIGH','RMVALMIN');
          +",RMVALMAX ="+cp_NullLink(cp_ToStrODBC(this.w_RMVALMAX),'SET_RIGH','RMVALMAX');
              +i_ccchkf ;
          +" where ";
              +"RMSERIAL = "+cp_ToStrODBC(this.w_RMSERIAL);
                 )
        else
          update (i_cTable) set;
              RMCODICE = this.w_RMCODICE;
              ,RMDESCRI = this.w_RMDESCRI;
              ,RMDESAGG = this.w_RMDESAGG;
              ,RMVALMIN = this.w_RMVALMIN;
              ,RMVALMAX = this.w_RMVALMAX;
              &i_ccchkf. ;
           where;
              RMSERIAL = this.w_RMSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_05118D28
      * --- End
       
 Select TEMPO 
 Skip
    enddo
  endproc
  proc Try_05118D28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SET_RIGH
    i_nConn=i_TableProp[this.SET_RIGH_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SET_RIGH_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SET_RIGH_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RMSERIAL"+",RMCODICE"+",RMDESCRI"+",RMDESAGG"+",RMVALMIN"+",RMVALMAX"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_RMSERIAL),'SET_RIGH','RMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RMCODICE),'SET_RIGH','RMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RMDESCRI),'SET_RIGH','RMDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RMDESAGG),'SET_RIGH','RMDESAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RMVALMIN),'SET_RIGH','RMVALMIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RMVALMAX),'SET_RIGH','RMVALMAX');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RMSERIAL',this.w_RMSERIAL,'RMCODICE',this.w_RMCODICE,'RMDESCRI',this.w_RMDESCRI,'RMDESAGG',this.w_RMDESAGG,'RMVALMIN',this.w_RMVALMIN,'RMVALMAX',this.w_RMVALMAX)
      insert into (i_cTable) (RMSERIAL,RMCODICE,RMDESCRI,RMDESAGG,RMVALMIN,RMVALMAX &i_ccchkf. );
         values (;
           this.w_RMSERIAL;
           ,this.w_RMCODICE;
           ,this.w_RMDESCRI;
           ,this.w_RMDESAGG;
           ,this.w_RMVALMIN;
           ,this.w_RMVALMAX;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_33
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    CREATE CURSOR TEMPO (RGCODICE C(5), RGDESCRI C(60), RGCODRIG C(5), CPROWORD N(5))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
     
 Select TEMPO 
 Go Top
    do while Not Eof("TEMPO")
      this.w_RGCODICE = TEMPO.RGCODICE
      this.w_RGDESCRI = TEMPO.RGDESCRI
      this.w_RGCODRIG = TEMPO.RGCODRIG 
      this.w_CPROWORD = TEMPO.CPROWORD
      * --- In presenza di un nuovo Comune sul txt
      ah_Msg("Inserisco/aggiorno righe modello %1",.T.,.F.,.F., alltrim(this.w_RMSERIAL))
      * --- Try
      local bErr_05122A48
      bErr_05122A48=bTrsErr
      this.Try_05122A48()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into SET_RAGG
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SET_RAGG_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SET_RAGG_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SET_RAGG_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"RGDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_RGDESCRI),'SET_RAGG','RGDESCRI');
          +",CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'SET_RAGG','CPROWORD');
              +i_ccchkf ;
          +" where ";
              +"RGCODICE = "+cp_ToStrODBC(this.w_RGCODICE);
              +" and RGCODRIG = "+cp_ToStrODBC(this.w_RGCODRIG );
                 )
        else
          update (i_cTable) set;
              RGDESCRI = this.w_RGDESCRI;
              ,CPROWORD = this.w_CPROWORD;
              &i_ccchkf. ;
           where;
              RGCODICE = this.w_RGCODICE;
              and RGCODRIG = this.w_RGCODRIG ;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_05122A48
      * --- End
       
 Select TEMPO 
 Skip
    enddo
  endproc
  proc Try_05122A48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SET_RAGG
    i_nConn=i_TableProp[this.SET_RAGG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SET_RAGG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SET_RAGG_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RGCODICE"+",RGDESCRI"+",RGCODRIG"+",CPROWORD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_RGCODICE),'SET_RAGG','RGCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RGDESCRI),'SET_RAGG','RGDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RGCODRIG ),'SET_RAGG','RGCODRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'SET_RAGG','CPROWORD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RGCODICE',this.w_RGCODICE,'RGDESCRI',this.w_RGDESCRI,'RGCODRIG',this.w_RGCODRIG ,'CPROWORD',this.w_CPROWORD)
      insert into (i_cTable) (RGCODICE,RGDESCRI,RGCODRIG,CPROWORD &i_ccchkf. );
         values (;
           this.w_RGCODICE;
           ,this.w_RGDESCRI;
           ,this.w_RGCODRIG ;
           ,this.w_CPROWORD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_34
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Uffici competenti relativi al comune
    this.w_OLD_COD = "@@@@@@"
    CREATE CURSOR TEMPO (TCCODICE C(5), TCDESCRI C(60), CPROWNUM N(4), CPROWORD N(5), TCCODRIG C(5), TCDATINI T(10),TCDATFIN T(10) )
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
     
 Select TEMPO 
 Go Top
    do while Not Eof("TEMPO")
      this.w_TCCODICE = TEMPO.TCCODICE
      this.w_TCDESCRI = TEMPO.TCDESCRI
      this.w_CPROWNUM = TEMPO.CPROWNUM
      this.w_CPROWORD = TEMPO.CPROWORD
      this.w_TCCODRIG = TEMPO.TCCODRIG
      this.w_TCDATINI = CP_TODATE(TEMPO.TCDATINI)
      this.w_TCDATFIN = CP_TODATE(TEMPO.TCDATFIN)
      * --- In presenza di un nuovo Comune sul txt
      if this.w_OLD_COD <> this.w_TCCODICE
        ah_Msg("Inserisco/aggiorno tipologia clientela %1",.T.,.F.,.F., alltrim(this.w_TCCODICE))
        * --- Try
        local bErr_0512E788
        bErr_0512E788=bTrsErr
        this.Try_0512E788()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into TIP_CLIE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.TIP_CLIE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_CLIE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.TIP_CLIE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TCDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_TCDESCRI),'TIP_CLIE','TCDESCRI');
                +i_ccchkf ;
            +" where ";
                +"TCCODICE = "+cp_ToStrODBC(this.w_TCCODICE);
                   )
          else
            update (i_cTable) set;
                TCDESCRI = this.w_TCDESCRI;
                &i_ccchkf. ;
             where;
                TCCODICE = this.w_TCCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_0512E788
        * --- End
      endif
      * --- Try
      local bErr_0513ADE8
      bErr_0513ADE8=bTrsErr
      this.Try_0513ADE8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into SET_TIPC
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SET_TIPC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SET_TIPC_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SET_TIPC_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'SET_TIPC','CPROWORD');
          +",CPROWNUM ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'SET_TIPC','CPROWNUM');
          +",TCCODRIG ="+cp_NullLink(cp_ToStrODBC(this.w_TCCODRIG),'SET_TIPC','TCCODRIG');
          +",TCDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_TCDATINI),'SET_TIPC','TCDATINI');
          +",TCDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_TCDATFIN),'SET_TIPC','TCDATFIN');
              +i_ccchkf ;
          +" where ";
              +"TCCODTIP = "+cp_ToStrODBC(this.w_TCCODICE);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 )
        else
          update (i_cTable) set;
              CPROWORD = this.w_CPROWORD;
              ,CPROWNUM = this.w_CPROWNUM;
              ,TCCODRIG = this.w_TCCODRIG;
              ,TCDATINI = this.w_TCDATINI;
              ,TCDATFIN = this.w_TCDATFIN;
              &i_ccchkf. ;
           where;
              TCCODTIP = this.w_TCCODICE;
              and CPROWNUM = this.w_CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_0513ADE8
      * --- End
      this.w_OLD_COD = this.w_TCCODICE
       
 Select TEMPO 
 Skip
    enddo
  endproc
  proc Try_0512E788()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TIP_CLIE
    i_nConn=i_TableProp[this.TIP_CLIE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_CLIE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TIP_CLIE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TCCODICE"+",TCDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TCCODICE),'TIP_CLIE','TCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TCDESCRI),'TIP_CLIE','TCDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TCCODICE',this.w_TCCODICE,'TCDESCRI',this.w_TCDESCRI)
      insert into (i_cTable) (TCCODICE,TCDESCRI &i_ccchkf. );
         values (;
           this.w_TCCODICE;
           ,this.w_TCDESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0513ADE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SET_TIPC
    i_nConn=i_TableProp[this.SET_TIPC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SET_TIPC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SET_TIPC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TCCODTIP"+",CPROWNUM"+",CPROWORD"+",TCCODRIG"+",TCDATINI"+",TCDATFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TCCODICE),'SET_TIPC','TCCODTIP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'SET_TIPC','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'SET_TIPC','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TCCODRIG),'SET_TIPC','TCCODRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TCDATINI),'SET_TIPC','TCDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TCDATFIN),'SET_TIPC','TCDATFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TCCODTIP',this.w_TCCODICE,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'TCCODRIG',this.w_TCCODRIG,'TCDATINI',this.w_TCDATINI,'TCDATFIN',this.w_TCDATFIN)
      insert into (i_cTable) (TCCODTIP,CPROWNUM,CPROWORD,TCCODRIG,TCDATINI,TCDATFIN &i_ccchkf. );
         values (;
           this.w_TCCODICE;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_TCCODRIG;
           ,this.w_TCDATINI;
           ,this.w_TCDATFIN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_35
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- F23 - Contributo unificato
    USE IN SELECT("TEMPO")
    do case
      case this.oParentObject.w_TIPMDF="CU"
        CREATE CURSOR TEMPO (CUSERIAL C(4), CUDESCRI C(130), CUFLTIPO C(1), CUVALPRA N(18,4), CUIMPORT N(18,4), CUIMPANT N(18,4), CUTRIBUT C(1) )
        this.OldPoint = SET("POINT")
        SET POINT TO "."
        APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
        SET POINT TO this.OldPoint
         
 Select TEMPO 
 Go Top
        do while Not Eof("TEMPO")
          * --- Ci appoggiamo alle variabili gi� dichiarate
          this.w_CUSERIAL = TEMPO.CUSERIAL
          this.w_CUDESCRI = TEMPO.CUDESCRI
          this.w_CUFLTIPO = TEMPO.CUFLTIPO
          this.w_CUVALPRA = TEMPO.CUVALPRA
          this.w_CUIMPORT = TEMPO.CUIMPORT
          this.w_CUIMPANT = TEMPO.CUIMPANT
          this.w_CUTRIBUT = NVL(TEMPO.CUTRIBUT,"N")
          * --- In presenza di un nuovo Comune sul txt
          ah_Msg("Inserisco/aggiorno contributo unificato %1",.T.,.F.,.F., alltrim(this.w_CUSERIAL))
          * --- Try
          local bErr_0514BDC0
          bErr_0514BDC0=bTrsErr
          this.Try_0514BDC0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into CON_UNIF
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CON_UNIF_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CON_UNIF_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CON_UNIF_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CUDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_CUDESCRI),'CON_UNIF','CUDESCRI');
              +",CUFLTIPO ="+cp_NullLink(cp_ToStrODBC(this.w_CUFLTIPO),'CON_UNIF','CUFLTIPO');
              +",CUVALPRA ="+cp_NullLink(cp_ToStrODBC(this.w_CUVALPRA),'CON_UNIF','CUVALPRA');
              +",CUIMPORT ="+cp_NullLink(cp_ToStrODBC(this.w_CUIMPORT),'CON_UNIF','CUIMPORT');
              +",CUIMPANT ="+cp_NullLink(cp_ToStrODBC(this.w_CUIMPANT),'CON_UNIF','CUIMPANT');
              +",CUTRIBUT ="+cp_NullLink(cp_ToStrODBC(this.w_CUTRIBUT),'CON_UNIF','CUTRIBUT');
                  +i_ccchkf ;
              +" where ";
                  +"CUSERIAL = "+cp_ToStrODBC(this.w_CUSERIAL);
                     )
            else
              update (i_cTable) set;
                  CUDESCRI = this.w_CUDESCRI;
                  ,CUFLTIPO = this.w_CUFLTIPO;
                  ,CUVALPRA = this.w_CUVALPRA;
                  ,CUIMPORT = this.w_CUIMPORT;
                  ,CUIMPANT = this.w_CUIMPANT;
                  ,CUTRIBUT = this.w_CUTRIBUT;
                  &i_ccchkf. ;
               where;
                  CUSERIAL = this.w_CUSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_0514BDC0
          * --- End
           
 Select TEMPO 
 Skip
        enddo
      case this.oParentObject.w_TIPMDF="CT"
        CREATE CURSOR TEMPO (CTCODICE C(5), CTDESCRI C(80), CTFLCONT C(1))
        APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
         
 Select TEMPO 
 Go Top
        do while Not Eof("TEMPO")
          * --- Ci appoggiamo alle variabili gi� dichiarate
          this.w_CTCODICE = TEMPO.CTCODICE
          this.w_CTDESCRI = TEMPO.CTDESCRI
          this.w_CTFLCONT = TEMPO.CTFLCONT
          * --- In presenza di un nuovo Comune sul txt
          ah_Msg("Inserisco/aggiorno codici tributo F23 %1",.T.,.F.,.F., alltrim(this.w_CTCODICE))
          * --- Try
          local bErr_051629C0
          bErr_051629C0=bTrsErr
          this.Try_051629C0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into F23_TRIB
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.F23_TRIB_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.F23_TRIB_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.F23_TRIB_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CTDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_CTDESCRI),'F23_TRIB','CTDESCRI');
              +",CTFLCONT ="+cp_NullLink(cp_ToStrODBC(this.w_CTFLCONT),'F23_TRIB','CTFLCONT');
                  +i_ccchkf ;
              +" where ";
                  +"CTCODICE = "+cp_ToStrODBC(this.w_CTCODICE);
                     )
            else
              update (i_cTable) set;
                  CTDESCRI = this.w_CTDESCRI;
                  ,CTFLCONT = this.w_CTFLCONT;
                  &i_ccchkf. ;
               where;
                  CTCODICE = this.w_CTCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_051629C0
          * --- End
           
 Select TEMPO 
 Skip
        enddo
      case this.oParentObject.w_TIPMDF="CC"
        CREATE CURSOR TEMPO (COCODICE C(2), CODESCRI C(80))
        APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
         
 Select TEMPO 
 Go Top
        do while Not Eof("TEMPO")
          * --- Ci appoggiamo alle variabili gi� dichiarate
          this.w_COCODICE = TEMPO.COCODICE
          this.w_CODESCRI = TEMPO.CODESCRI
          * --- In presenza di un nuovo Comune sul txt
          ah_Msg("Inserisco/aggiorno codici contenzioso F23 %1",.T.,.F.,.F., alltrim(this.w_CTCODICE))
          * --- Try
          local bErr_0516AAC0
          bErr_0516AAC0=bTrsErr
          this.Try_0516AAC0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into F23_CONT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.F23_CONT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.F23_CONT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.F23_CONT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CODESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_CODESCRI),'F23_CONT','CODESCRI');
                  +i_ccchkf ;
              +" where ";
                  +"COCODICE = "+cp_ToStrODBC(this.w_COCODICE);
                     )
            else
              update (i_cTable) set;
                  CODESCRI = this.w_CODESCRI;
                  &i_ccchkf. ;
               where;
                  COCODICE = this.w_COCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_0516AAC0
          * --- End
           
 Select TEMPO 
 Skip
        enddo
      case this.oParentObject.w_TIPMDF="CF"
        CREATE CURSOR TEMPO (CACODICE C(3), CADESCRI C(80))
        APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
         
 Select TEMPO 
 Go Top
        do while Not Eof("TEMPO")
          * --- Ci appoggiamo alle variabili gi� dichiarate
          this.w_CACODICE = TEMPO.CACODICE
          this.w_CADESCRI = TEMPO.CADESCRI
          * --- In presenza di un nuovo Comune sul txt
          ah_Msg("Inserisco/aggiorno causali F23 %1",.T.,.F.,.F., alltrim(this.w_CACODICE))
          * --- Try
          local bErr_05182FC8
          bErr_05182FC8=bTrsErr
          this.Try_05182FC8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
            * --- Write into F23_CAUS
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.F23_CAUS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.F23_CAUS_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.F23_CAUS_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CADESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_CADESCRI),'F23_CAUS','CADESCRI');
                  +i_ccchkf ;
              +" where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_CACODICE);
                     )
            else
              update (i_cTable) set;
                  CADESCRI = this.w_CADESCRI;
                  &i_ccchkf. ;
               where;
                  CACODICE = this.w_CACODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_05182FC8
          * --- End
           
 Select TEMPO 
 Skip
        enddo
    endcase
    USE IN SELECT("TEMPO")
  endproc
  proc Try_0514BDC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CON_UNIF
    i_nConn=i_TableProp[this.CON_UNIF_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_UNIF_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_UNIF_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CUSERIAL"+",CUDESCRI"+",CUFLTIPO"+",CUVALPRA"+",CUIMPORT"+",CUIMPANT"+",CUTRIBUT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CUSERIAL),'CON_UNIF','CUSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CUDESCRI),'CON_UNIF','CUDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CUFLTIPO),'CON_UNIF','CUFLTIPO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CUVALPRA),'CON_UNIF','CUVALPRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CUIMPORT),'CON_UNIF','CUIMPORT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CUIMPANT),'CON_UNIF','CUIMPANT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CUTRIBUT),'CON_UNIF','CUTRIBUT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CUSERIAL',this.w_CUSERIAL,'CUDESCRI',this.w_CUDESCRI,'CUFLTIPO',this.w_CUFLTIPO,'CUVALPRA',this.w_CUVALPRA,'CUIMPORT',this.w_CUIMPORT,'CUIMPANT',this.w_CUIMPANT,'CUTRIBUT',this.w_CUTRIBUT)
      insert into (i_cTable) (CUSERIAL,CUDESCRI,CUFLTIPO,CUVALPRA,CUIMPORT,CUIMPANT,CUTRIBUT &i_ccchkf. );
         values (;
           this.w_CUSERIAL;
           ,this.w_CUDESCRI;
           ,this.w_CUFLTIPO;
           ,this.w_CUVALPRA;
           ,this.w_CUIMPORT;
           ,this.w_CUIMPANT;
           ,this.w_CUTRIBUT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_051629C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into F23_TRIB
    i_nConn=i_TableProp[this.F23_TRIB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.F23_TRIB_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.F23_TRIB_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CTCODICE"+",CTDESCRI"+",CTFLCONT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CTCODICE),'F23_TRIB','CTCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CTDESCRI),'F23_TRIB','CTDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CTFLCONT),'F23_TRIB','CTFLCONT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CTCODICE',this.w_CTCODICE,'CTDESCRI',this.w_CTDESCRI,'CTFLCONT',this.w_CTFLCONT)
      insert into (i_cTable) (CTCODICE,CTDESCRI,CTFLCONT &i_ccchkf. );
         values (;
           this.w_CTCODICE;
           ,this.w_CTDESCRI;
           ,this.w_CTFLCONT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0516AAC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into F23_CONT
    i_nConn=i_TableProp[this.F23_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.F23_CONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.F23_CONT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"COCODICE"+",CODESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_COCODICE),'F23_CONT','COCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODESCRI),'F23_CONT','CODESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'COCODICE',this.w_COCODICE,'CODESCRI',this.w_CODESCRI)
      insert into (i_cTable) (COCODICE,CODESCRI &i_ccchkf. );
         values (;
           this.w_COCODICE;
           ,this.w_CODESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_05182FC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into F23_CAUS
    i_nConn=i_TableProp[this.F23_CAUS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.F23_CAUS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.F23_CAUS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CACODICE"+",CADESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CACODICE),'F23_CAUS','CACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CADESCRI),'F23_CAUS','CADESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CACODICE',this.w_CACODICE,'CADESCRI',this.w_CADESCRI)
      insert into (i_cTable) (CACODICE,CADESCRI &i_ccchkf. );
         values (;
           this.w_CACODICE;
           ,this.w_CADESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_36
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Codici uffici giudiziari
    CREATE CURSOR TEMPO (UFCODICE C(15), UFLOCALI C (50), UF__NOME C(80), UFTIPUFF C(1), UFPROVIN C(2), UFPRORIF C(4), UFINDMAI C(250))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_UFCODICE = SPACE(15)
    this.w_UFLOCALI = SPACE(50)
    this.w_UF__NOME = SPACE(80)
    this.w_UFTIPUFF = SPACE(1)
    this.w_UFPROVIN = SPACE(2)
    this.w_UFPRORIF = SPACE(4)
    this.w_UFINDMAI = SPACE(250)
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(UFCODICE," "))
    * --- Inserisco...
    this.w_UFCODICE = TEMPO.UFCODICE
    this.w_UFLOCALI = NVL(TEMPO.UFLOCALI, SPACE(50))
    this.w_UF__NOME = NVL(TEMPO.UF__NOME, SPACE(80))
    this.w_UFTIPUFF = NVL(TEMPO.UFTIPUFF, " ")
    this.w_UFPROVIN = NVL(UFPROVIN, SPACE(2))
    this.w_UFPRORIF = NVL(UFPRORIF, SPACE(4))
    this.w_UFINDMAI = NVL(UFINDMAI, SPACE(250))
    * --- Try
    local bErr_05180088
    bErr_05180088=bTrsErr
    this.Try_05180088()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_0519E320
      bErr_0519E320=bTrsErr
      this.Try_0519E320()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura in codice ufficio giudiziario: %1%0Continuo?","", this.w_UFCODICE )
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_0519E320
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_05180088
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_05180088()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into UFF_GIUD
    i_nConn=i_TableProp[this.UFF_GIUD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UFF_GIUD_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.UFF_GIUD_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"UFCODICE"+",UFLOCALI"+",UF__NOME"+",UFTIPUFF"+",UFPROVIN"+",UFPRORIF"+",UFINDMAI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_UFCODICE),'UFF_GIUD','UFCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UFLOCALI),'UFF_GIUD','UFLOCALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UF__NOME),'UFF_GIUD','UF__NOME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UFTIPUFF),'UFF_GIUD','UFTIPUFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UFPROVIN),'UFF_GIUD','UFPROVIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UFPRORIF),'UFF_GIUD','UFPRORIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UFINDMAI),'UFF_GIUD','UFINDMAI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'UFCODICE',this.w_UFCODICE,'UFLOCALI',this.w_UFLOCALI,'UF__NOME',this.w_UF__NOME,'UFTIPUFF',this.w_UFTIPUFF,'UFPROVIN',this.w_UFPROVIN,'UFPRORIF',this.w_UFPRORIF,'UFINDMAI',this.w_UFINDMAI)
      insert into (i_cTable) (UFCODICE,UFLOCALI,UF__NOME,UFTIPUFF,UFPROVIN,UFPRORIF,UFINDMAI &i_ccchkf. );
         values (;
           this.w_UFCODICE;
           ,this.w_UFLOCALI;
           ,this.w_UF__NOME;
           ,this.w_UFTIPUFF;
           ,this.w_UFPROVIN;
           ,this.w_UFPRORIF;
           ,this.w_UFINDMAI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco codice ufficio giudiziario: %1",.t.,,,this.w_UFCODICE)
    return
  proc Try_0519E320()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into UFF_GIUD
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.UFF_GIUD_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UFF_GIUD_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.UFF_GIUD_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"UFLOCALI ="+cp_NullLink(cp_ToStrODBC(this.w_UFLOCALI),'UFF_GIUD','UFLOCALI');
      +",UF__NOME ="+cp_NullLink(cp_ToStrODBC(this.w_UF__NOME),'UFF_GIUD','UF__NOME');
      +",UFTIPUFF ="+cp_NullLink(cp_ToStrODBC(this.w_UFTIPUFF),'UFF_GIUD','UFTIPUFF');
      +",UFPROVIN ="+cp_NullLink(cp_ToStrODBC(this.w_UFPROVIN),'UFF_GIUD','UFPROVIN');
      +",UFPRORIF ="+cp_NullLink(cp_ToStrODBC(this.w_UFPRORIF),'UFF_GIUD','UFPRORIF');
          +i_ccchkf ;
      +" where ";
          +"UFCODICE = "+cp_ToStrODBC(this.w_UFCODICE);
             )
    else
      update (i_cTable) set;
          UFLOCALI = this.w_UFLOCALI;
          ,UF__NOME = this.w_UF__NOME;
          ,UFTIPUFF = this.w_UFTIPUFF;
          ,UFPROVIN = this.w_UFPROVIN;
          ,UFPRORIF = this.w_UFPRORIF;
          &i_ccchkf. ;
       where;
          UFCODICE = this.w_UFCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Scrivo codice ufficio giudiziario: %1",.t.,,,this.w_UFCODICE)
    return


  procedure Page_37
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Codici uffici giudiziari
    CREATE CURSOR TEMPO (UFCODICE C(15), UFDESCRI C (60), UF_TIPOL C(1))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(UFCODICE," "))
    * --- Inserisco...
    this.w_UFCODICE = TEMPO.UFCODICE
    this.w_UFDESCRI = NVL(TEMPO.UFDESCRI, SPACE(50))
    this.w_UF_TIPOL = NVL(TEMPO.UF_TIPOL, SPACE(80))
    * --- Try
    local bErr_05198410
    bErr_05198410=bTrsErr
    this.Try_05198410()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_0519AA80
      bErr_0519AA80=bTrsErr
      this.Try_0519AA80()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura in codice ufficio pratiche: %1%0Continuo?","", this.w_UFCODICE )
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_0519AA80
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_05198410
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_05198410()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRA_UFFI
    i_nConn=i_TableProp[this.PRA_UFFI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRA_UFFI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRA_UFFI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"UFCODICE"+",UFDESCRI"+",UF_TIPOL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_UFCODICE),'PRA_UFFI','UFCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UFDESCRI),'PRA_UFFI','UFDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UF_TIPOL),'PRA_UFFI','UF_TIPOL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'UFCODICE',this.w_UFCODICE,'UFDESCRI',this.w_UFDESCRI,'UF_TIPOL',this.w_UF_TIPOL)
      insert into (i_cTable) (UFCODICE,UFDESCRI,UF_TIPOL &i_ccchkf. );
         values (;
           this.w_UFCODICE;
           ,this.w_UFDESCRI;
           ,this.w_UF_TIPOL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco codice ufficio giudiziario: %1",.t.,,,this.w_UFCODICE)
    return
  proc Try_0519AA80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PRA_UFFI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRA_UFFI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRA_UFFI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRA_UFFI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"UFDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_UFDESCRI),'PRA_UFFI','UFDESCRI');
      +",UF_TIPOL ="+cp_NullLink(cp_ToStrODBC(this.w_UF_TIPOL),'PRA_UFFI','UF_TIPOL');
          +i_ccchkf ;
      +" where ";
          +"UFCODICE = "+cp_ToStrODBC(this.w_UFCODICE);
             )
    else
      update (i_cTable) set;
          UFDESCRI = this.w_UFDESCRI;
          ,UF_TIPOL = this.w_UF_TIPOL;
          &i_ccchkf. ;
       where;
          UFCODICE = this.w_UFCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Scrivo codice ufficio pratiche: %1",.t.,,,this.w_UFCODICE)
    return


  procedure Page_38
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Enti Pratiche
    CREATE CURSOR TEMPO (EPCODICE C(10), EPDESCRI C (60), EP__TIPO C(1),EPCODENT C(5),EPCALTAR C(10),EPTIPUFF C(1))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(EPCODICE," "))
    * --- Inserisco...
    this.w_EPCODICE = TEMPO.EPCODICE
    this.w_EPDESCRI = NVL(TEMPO.EPDESCRI, SPACE(60))
    this.w_EP__TIPO = NVL(TEMPO.EP__TIPO, SPACE(1))
    this.w_EPCALTAR = NVL(TEMPO.EPCALTAR, SPACE(10))
    this.w_EPCODENT = NVL(TEMPO.EPCODENT, SPACE(5))
    this.w_EPTIPUFF = NVL(TEMPO.EPTIPUFF, SPACE(1))
    * --- Try
    local bErr_051B0208
    bErr_051B0208=bTrsErr
    this.Try_051B0208()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_051B2878
      bErr_051B2878=bTrsErr
      this.Try_051B2878()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura enti pratiche: %1%0Continuo?","", this.w_EPCODICE )
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_051B2878
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_051B0208
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_051B0208()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRA_ENTI
    i_nConn=i_TableProp[this.PRA_ENTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRA_ENTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRA_ENTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"EPCODICE"+",EPDESCRI"+",EP__TIPO"+",EPCODENT"+",EPCALTAR"+",EPTIPUFF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_EPCODICE),'PRA_ENTI','EPCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EPDESCRI),'PRA_ENTI','EPDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EP__TIPO),'PRA_ENTI','EP__TIPO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EPCODENT),'PRA_ENTI','EPCODENT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EPCALTAR),'PRA_ENTI','EPCALTAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EPTIPUFF),'PRA_ENTI','EPTIPUFF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'EPCODICE',this.w_EPCODICE,'EPDESCRI',this.w_EPDESCRI,'EP__TIPO',this.w_EP__TIPO,'EPCODENT',this.w_EPCODENT,'EPCALTAR',this.w_EPCALTAR,'EPTIPUFF',this.w_EPTIPUFF)
      insert into (i_cTable) (EPCODICE,EPDESCRI,EP__TIPO,EPCODENT,EPCALTAR,EPTIPUFF &i_ccchkf. );
         values (;
           this.w_EPCODICE;
           ,this.w_EPDESCRI;
           ,this.w_EP__TIPO;
           ,this.w_EPCODENT;
           ,this.w_EPCALTAR;
           ,this.w_EPTIPUFF;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco enti pratiche: %1",.t.,,,this.w_EPCODICE)
    return
  proc Try_051B2878()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PRA_ENTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRA_ENTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRA_ENTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRA_ENTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"EPDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_EPDESCRI),'PRA_ENTI','EPDESCRI');
      +",EP__TIPO ="+cp_NullLink(cp_ToStrODBC(this.w_EP__TIPO),'PRA_ENTI','EP__TIPO');
      +",EPCODENT ="+cp_NullLink(cp_ToStrODBC(this.w_EPCODENT),'PRA_ENTI','EPCODENT');
      +",EPCALTAR ="+cp_NullLink(cp_ToStrODBC(this.w_EPCALTAR),'PRA_ENTI','EPCALTAR');
      +",EPTIPUFF ="+cp_NullLink(cp_ToStrODBC(this.w_EPTIPUFF),'PRA_ENTI','EPTIPUFF');
          +i_ccchkf ;
      +" where ";
          +"EPCODICE = "+cp_ToStrODBC(this.w_EPCODICE);
             )
    else
      update (i_cTable) set;
          EPDESCRI = this.w_EPDESCRI;
          ,EP__TIPO = this.w_EP__TIPO;
          ,EPCODENT = this.w_EPCODENT;
          ,EPCALTAR = this.w_EPCALTAR;
          ,EPTIPUFF = this.w_EPTIPUFF;
          &i_ccchkf. ;
       where;
          EPCODICE = this.w_EPCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Scrivo enti pratiche: %1",.t.,,,this.w_EPCODICE)
    return


  procedure Page_39
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- oggetti pratica
    CREATE CURSOR TEMPO (OPCODICE C(10), OPDESCRI C (250), OP_RUOLO C(100),OPMATERI C(100),OPGRUPPO C(60),OPCLRUOL C(1),OPCLMATE C(2),OPCLOGGE C(3), OPCODMAT C(10))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(OPCODICE," "))
    * --- Inserisco...
    this.w_OPCODICE = TEMPO.OPCODICE
    this.w_OPDESCRI = NVL(TEMPO.OPDESCRI, SPACE(60))
    this.w_OP_RUOLO = NVL(TEMPO.OP_RUOLO, SPACE(100))
    this.w_OPMATERI = NVL(TEMPO.OPMATERI, SPACE(100))
    this.w_OPGRUPPO = NVL(TEMPO.OPGRUPPO, SPACE(60))
    this.w_OPCLRUOL = NVL(TEMPO.OPCLRUOL, SPACE(1))
    this.w_OPCLMATE = NVL(TEMPO.OPCLMATE, SPACE(2))
    this.w_OPCLOGGE = NVL(TEMPO.OPCLOGGE, SPACE(3))
    this.w_OPCODMAT = NVL(TEMPO.OPCODMAT, SPACE(10))
    * --- Try
    local bErr_051BB000
    bErr_051BB000=bTrsErr
    this.Try_051BB000()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_051BFD40
      bErr_051BFD40=bTrsErr
      this.Try_051BFD40()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura oggetti pratiche: %1%0Continuo?","", this.w_OPCODICE )
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_051BFD40
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_051BB000
    * --- End
    if !EMPTY(this.w_OPCODMAT)
      * --- Proviamo ad inserire anche il codice materia. Nel caso in cui l'update non andasse a buon fine � dovuto al fatto che manca la materia, quindi ignoriamo l'errore
      * --- Try
      local bErr_051BC980
      bErr_051BC980=bTrsErr
      this.Try_051BC980()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_051BC980
      * --- End
    endif
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_051BB000()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRA_OGGE
    i_nConn=i_TableProp[this.PRA_OGGE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRA_OGGE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRA_OGGE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"OPCODICE"+",OPDESCRI"+",OP_RUOLO"+",OPMATERI"+",OPGRUPPO"+",OPCLRUOL"+",OPCLMATE"+",OPCLOGGE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_OPCODICE),'PRA_OGGE','OPCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OPDESCRI),'PRA_OGGE','OPDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OP_RUOLO),'PRA_OGGE','OP_RUOLO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OPMATERI),'PRA_OGGE','OPMATERI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OPGRUPPO),'PRA_OGGE','OPGRUPPO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OPCLRUOL),'PRA_OGGE','OPCLRUOL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OPCLMATE),'PRA_OGGE','OPCLMATE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OPCLOGGE),'PRA_OGGE','OPCLOGGE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'OPCODICE',this.w_OPCODICE,'OPDESCRI',this.w_OPDESCRI,'OP_RUOLO',this.w_OP_RUOLO,'OPMATERI',this.w_OPMATERI,'OPGRUPPO',this.w_OPGRUPPO,'OPCLRUOL',this.w_OPCLRUOL,'OPCLMATE',this.w_OPCLMATE,'OPCLOGGE',this.w_OPCLOGGE)
      insert into (i_cTable) (OPCODICE,OPDESCRI,OP_RUOLO,OPMATERI,OPGRUPPO,OPCLRUOL,OPCLMATE,OPCLOGGE &i_ccchkf. );
         values (;
           this.w_OPCODICE;
           ,this.w_OPDESCRI;
           ,this.w_OP_RUOLO;
           ,this.w_OPMATERI;
           ,this.w_OPGRUPPO;
           ,this.w_OPCLRUOL;
           ,this.w_OPCLMATE;
           ,this.w_OPCLOGGE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco oggetti pratiche: %1",.t.,,,this.w_OPCODICE)
    return
  proc Try_051BFD40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PRA_OGGE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRA_OGGE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRA_OGGE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRA_OGGE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OPDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_OPDESCRI),'PRA_OGGE','OPDESCRI');
      +",OP_RUOLO ="+cp_NullLink(cp_ToStrODBC(this.w_OP_RUOLO),'PRA_OGGE','OP_RUOLO');
      +",OPMATERI ="+cp_NullLink(cp_ToStrODBC(this.w_OPMATERI),'PRA_OGGE','OPMATERI');
      +",OPGRUPPO ="+cp_NullLink(cp_ToStrODBC(this.w_OPGRUPPO),'PRA_OGGE','OPGRUPPO');
      +",OPCLRUOL ="+cp_NullLink(cp_ToStrODBC(this.w_OPCLRUOL),'PRA_OGGE','OPCLRUOL');
      +",OPCLMATE ="+cp_NullLink(cp_ToStrODBC(this.w_OPCLMATE),'PRA_OGGE','OPCLMATE');
      +",OPCLOGGE ="+cp_NullLink(cp_ToStrODBC(this.w_OPCLOGGE),'PRA_OGGE','OPCLOGGE');
          +i_ccchkf ;
      +" where ";
          +"OPCODICE = "+cp_ToStrODBC(this.w_OPCODICE);
             )
    else
      update (i_cTable) set;
          OPDESCRI = this.w_OPDESCRI;
          ,OP_RUOLO = this.w_OP_RUOLO;
          ,OPMATERI = this.w_OPMATERI;
          ,OPGRUPPO = this.w_OPGRUPPO;
          ,OPCLRUOL = this.w_OPCLRUOL;
          ,OPCLMATE = this.w_OPCLMATE;
          ,OPCLOGGE = this.w_OPCLOGGE;
          &i_ccchkf. ;
       where;
          OPCODICE = this.w_OPCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Scrivo oggetti pratiche: %1",.t.,,,this.w_OPCODICE)
    return
  proc Try_051BC980()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PRA_OGGE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRA_OGGE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRA_OGGE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRA_OGGE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OPCODMAT ="+cp_NullLink(cp_ToStrODBC(this.w_OPCODMAT),'PRA_OGGE','OPCODMAT');
          +i_ccchkf ;
      +" where ";
          +"OPCODICE = "+cp_ToStrODBC(this.w_OPCODICE);
             )
    else
      update (i_cTable) set;
          OPCODMAT = this.w_OPCODMAT;
          &i_ccchkf. ;
       where;
          OPCODICE = this.w_OPCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_40
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Materie 
    this.w_OLD_COD = "@@@@@@"
    CREATE CURSOR TEMPO (MPCODICE C(10), MPDESCRI C(60), MP_TIPOL C(1),CPROWNUM N(4), CPROWORD N(5), SECODRIG C(5), SEDATINI T(10),SEDATFIN T(10) )
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
     
 Select TEMPO 
 Go Top
    do while Not Eof("TEMPO")
      this.w_MPCODICE = TEMPO.MPCODICE
      this.w_MPDESCRI = Nvl(TEMPO.MPDESCRI,Space(60))
      this.w_MP_TIPOL = Nvl(TEMPO.MP_TIPOL," ")
      this.w_CPROWNUM = TEMPO.CPROWNUM
      this.w_CPROWORD = TEMPO.CPROWORD
      this.w_SECODRIG = TEMPO.SECODRIG
      this.w_SEDATINI = CP_TODATE(TEMPO.SEDATINI)
      this.w_SEDATFIN = CP_TODATE(TEMPO.SEDATFIN)
      * --- In presenza di una nuova materia pratica sul txt
      if this.w_OLD_COD <> this.w_MPCODICE
        ah_Msg("Inserisco/aggiorno materia pratica %1",.T.,.F.,.F., alltrim(this.w_MPCODICE))
        * --- Try
        local bErr_051CF338
        bErr_051CF338=bTrsErr
        this.Try_051CF338()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into PRA_MATE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PRA_MATE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRA_MATE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PRA_MATE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MPCODICE ="+cp_NullLink(cp_ToStrODBC(this.w_MPCODICE),'PRA_MATE','MPCODICE');
            +",MPDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_MPDESCRI),'PRA_MATE','MPDESCRI');
            +",MP_TIPOL ="+cp_NullLink(cp_ToStrODBC(this.w_MP_TIPOL),'PRA_MATE','MP_TIPOL');
                +i_ccchkf ;
            +" where ";
                +"MPCODICE = "+cp_ToStrODBC(this.w_MPCODICE);
                   )
          else
            update (i_cTable) set;
                MPCODICE = this.w_MPCODICE;
                ,MPDESCRI = this.w_MPDESCRI;
                ,MP_TIPOL = this.w_MP_TIPOL;
                &i_ccchkf. ;
             where;
                MPCODICE = this.w_MPCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_051CF338
        * --- End
      endif
      if this.w_CPROWNUM<>0
        * --- Try
        local bErr_051CCE18
        bErr_051CCE18=bTrsErr
        this.Try_051CCE18()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into SET_MATE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SET_MATE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SET_MATE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SET_MATE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'SET_MATE','CPROWORD');
            +",CPROWNUM ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'SET_MATE','CPROWNUM');
            +",SECODRIG ="+cp_NullLink(cp_ToStrODBC(this.w_SECODRIG),'SET_MATE','SECODRIG');
            +",SEDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_SEDATINI),'SET_MATE','SEDATINI');
            +",SEDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_SEDATFIN),'SET_MATE','SEDATFIN');
                +i_ccchkf ;
            +" where ";
                +"SECODMAT = "+cp_ToStrODBC(this.w_MPCODICE);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                   )
          else
            update (i_cTable) set;
                CPROWORD = this.w_CPROWORD;
                ,CPROWNUM = this.w_CPROWNUM;
                ,SECODRIG = this.w_SECODRIG;
                ,SEDATINI = this.w_SEDATINI;
                ,SEDATFIN = this.w_SEDATFIN;
                &i_ccchkf. ;
             where;
                SECODMAT = this.w_MPCODICE;
                and CPROWNUM = this.w_CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_051CCE18
        * --- End
      endif
      this.w_OLD_COD = this.w_MPCODICE
       
 Select TEMPO 
 Skip
    enddo
  endproc
  proc Try_051CF338()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRA_MATE
    i_nConn=i_TableProp[this.PRA_MATE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRA_MATE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRA_MATE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MPCODICE"+",MPDESCRI"+",MP_TIPOL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MPCODICE),'PRA_MATE','MPCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MPDESCRI),'PRA_MATE','MPDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MP_TIPOL),'PRA_MATE','MP_TIPOL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MPCODICE',this.w_MPCODICE,'MPDESCRI',this.w_MPDESCRI,'MP_TIPOL',this.w_MP_TIPOL)
      insert into (i_cTable) (MPCODICE,MPDESCRI,MP_TIPOL &i_ccchkf. );
         values (;
           this.w_MPCODICE;
           ,this.w_MPDESCRI;
           ,this.w_MP_TIPOL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_051CCE18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SET_MATE
    i_nConn=i_TableProp[this.SET_MATE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SET_MATE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SET_MATE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SECODMAT"+",CPROWNUM"+",CPROWORD"+",SECODRIG"+",SEDATINI"+",SEDATFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MPCODICE),'SET_MATE','SECODMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'SET_MATE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'SET_MATE','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SECODRIG),'SET_MATE','SECODRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SEDATINI),'SET_MATE','SEDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SEDATFIN),'SET_MATE','SEDATFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SECODMAT',this.w_MPCODICE,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'SECODRIG',this.w_SECODRIG,'SEDATINI',this.w_SEDATINI,'SEDATFIN',this.w_SEDATFIN)
      insert into (i_cTable) (SECODMAT,CPROWNUM,CPROWORD,SECODRIG,SEDATINI,SEDATFIN &i_ccchkf. );
         values (;
           this.w_MPCODICE;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_SECODRIG;
           ,this.w_SEDATINI;
           ,this.w_SEDATFIN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_41
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tipi pratiche
    this.w_OLD_COD = "@@@@@@"
    CREATE CURSOR TEMPO (TPCODICE C(10), TPDESCRI C(60), TPFLGIUD C(1), CPROWNUM N(4), CPROWORD N(5), STCODRIG C(5), STDATINI T(10),STDATFIN T(10) )
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
     
 Select TEMPO 
 Go Top
    do while Not Eof("TEMPO")
      this.w_TPCODICE = TEMPO.TPCODICE
      this.w_TPDESCRI = Nvl(TEMPO.TPDESCRI,Space(60))
      this.w_TPFLGIUD = Nvl(TEMPO.TPFLGIUD," ")
      this.w_CPROWNUM = TEMPO.CPROWNUM
      this.w_CPROWORD = TEMPO.CPROWORD
      this.w_STCODRIG = TEMPO.STCODRIG
      this.w_STDATINI = CP_TODATE(TEMPO.STDATINI)
      this.w_STDATFIN = CP_TODATE(TEMPO.STDATFIN)
      * --- In presenza di un nuovo tipo pratica sul txt
      if this.w_OLD_COD <> this.w_TPCODICE
        ah_Msg("Inserisco/aggiorno tipo pratica %1",.T.,.F.,.F., alltrim(this.w_TPCODICE))
        * --- Try
        local bErr_051FA318
        bErr_051FA318=bTrsErr
        this.Try_051FA318()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into PRA_TIPI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PRA_TIPI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRA_TIPI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PRA_TIPI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"TPCODICE ="+cp_NullLink(cp_ToStrODBC(this.w_MPCODICE),'PRA_TIPI','TPCODICE');
            +",TPDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_MPDESCRI),'PRA_TIPI','TPDESCRI');
            +",TPFLGIUD ="+cp_NullLink(cp_ToStrODBC(this.w_TPFLGIUD),'PRA_TIPI','TPFLGIUD');
                +i_ccchkf ;
            +" where ";
                +"TPCODICE = "+cp_ToStrODBC(this.w_MPCODICE);
                   )
          else
            update (i_cTable) set;
                TPCODICE = this.w_MPCODICE;
                ,TPDESCRI = this.w_MPDESCRI;
                ,TPFLGIUD = this.w_TPFLGIUD;
                &i_ccchkf. ;
             where;
                TPCODICE = this.w_MPCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_051FA318
        * --- End
      endif
      if this.w_CPROWNUM<>0
        * --- Try
        local bErr_051E2830
        bErr_051E2830=bTrsErr
        this.Try_051E2830()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into SET_TIPI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SET_TIPI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SET_TIPI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SET_TIPI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CPROWNUM ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'SET_TIPI','CPROWNUM');
            +",CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'SET_TIPI','CPROWORD');
            +",STCODRIG ="+cp_NullLink(cp_ToStrODBC(this.w_STCODRIG),'SET_TIPI','STCODRIG');
            +",STDATINI ="+cp_NullLink(cp_ToStrODBC(this.w_STDATINI),'SET_TIPI','STDATINI');
            +",STDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_STDATFIN),'SET_TIPI','STDATFIN');
                +i_ccchkf ;
            +" where ";
                +"STCODTIP = "+cp_ToStrODBC(this.w_TPCODICE);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                   )
          else
            update (i_cTable) set;
                CPROWNUM = this.w_CPROWNUM;
                ,CPROWORD = this.w_CPROWORD;
                ,STCODRIG = this.w_STCODRIG;
                ,STDATINI = this.w_STDATINI;
                ,STDATFIN = this.w_STDATFIN;
                &i_ccchkf. ;
             where;
                STCODTIP = this.w_TPCODICE;
                and CPROWNUM = this.w_CPROWNUM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_051E2830
        * --- End
      endif
      this.w_OLD_COD = this.w_TPCODICE
       
 Select TEMPO 
 Skip
    enddo
  endproc
  proc Try_051FA318()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRA_TIPI
    i_nConn=i_TableProp[this.PRA_TIPI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRA_TIPI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRA_TIPI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TPCODICE"+",TPDESCRI"+",TPFLGIUD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TPCODICE),'PRA_TIPI','TPCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TPDESCRI),'PRA_TIPI','TPDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TPFLGIUD),'PRA_TIPI','TPFLGIUD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TPCODICE',this.w_TPCODICE,'TPDESCRI',this.w_TPDESCRI,'TPFLGIUD',this.w_TPFLGIUD)
      insert into (i_cTable) (TPCODICE,TPDESCRI,TPFLGIUD &i_ccchkf. );
         values (;
           this.w_TPCODICE;
           ,this.w_TPDESCRI;
           ,this.w_TPFLGIUD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_051E2830()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SET_TIPI
    i_nConn=i_TableProp[this.SET_TIPI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SET_TIPI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SET_TIPI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"STCODTIP"+",CPROWNUM"+",CPROWORD"+",STCODRIG"+",STDATINI"+",STDATFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TPCODICE),'SET_TIPI','STCODTIP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'SET_TIPI','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'SET_TIPI','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_STCODRIG),'SET_TIPI','STCODRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_STDATINI),'SET_TIPI','STDATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_STDATFIN),'SET_TIPI','STDATFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'STCODTIP',this.w_TPCODICE,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'STCODRIG',this.w_STCODRIG,'STDATINI',this.w_STDATINI,'STDATFIN',this.w_STDATFIN)
      insert into (i_cTable) (STCODTIP,CPROWNUM,CPROWORD,STCODRIG,STDATINI,STDATFIN &i_ccchkf. );
         values (;
           this.w_TPCODICE;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_STCODRIG;
           ,this.w_STDATINI;
           ,this.w_STDATFIN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_42
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Raggruppamenti di prestazioni
    CREATE CURSOR TEMPO (ITCODICE C(10), ITDESCRI C(60), CPROWNUM N(4), CPROWORD N(5), ITCODPRE C(20), ITCOD_PR C(41), ITDESPRE C(40) )
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
     
 Select TEMPO 
 Go Top
    do while Not Eof("TEMPO")
      this.w_ITCODICE = TEMPO.ITCODICE
      this.w_ITDESCRI = Nvl(TEMPO.ITDESCRI,Space(60))
      this.w_CPROWNUM = TEMPO.CPROWNUM
      this.w_CPROWORD = TEMPO.CPROWORD
      this.w_ITCODPRE = Nvl(TEMPO.ITCODPRE,Space(20))
      this.w_ITCOD_PR = Nvl(TEMPO.ITCODPRE,Space(41))
      this.w_ITDESPRE = Nvl(TEMPO.ITDESPRE,Space(40))
      * --- In presenza di un nuovo raggruppamento di prestazioni sul txt
      ah_Msg("Inserisco/aggiorno raggrupamento di prestazioni %1",.T.,.F.,.F., alltrim(this.w_ITCODICE))
      * --- Try
      local bErr_051F4168
      bErr_051F4168=bTrsErr
      this.Try_051F4168()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_051F60B8
        bErr_051F60B8=bTrsErr
        this.Try_051F60B8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_051F60B8
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_051F4168
      * --- End
       
 Select TEMPO 
 Skip
    enddo
  endproc
  proc Try_051F4168()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRE_ITER
    i_nConn=i_TableProp[this.PRE_ITER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRE_ITER_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRE_ITER_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ITCODICE"+",ITDESCRI"+",CPROWNUM"+",CPROWORD"+",ITCODPRE"+",ITCOD_PR"+",ITDESPRE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ITCODICE),'PRE_ITER','ITCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ITDESCRI),'PRE_ITER','ITDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PRE_ITER','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PRE_ITER','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ITCODPRE),'PRE_ITER','ITCODPRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ITCOD_PR),'PRE_ITER','ITCOD_PR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ITDESPRE),'PRE_ITER','ITDESPRE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ITCODICE',this.w_ITCODICE,'ITDESCRI',this.w_ITDESCRI,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'ITCODPRE',this.w_ITCODPRE,'ITCOD_PR',this.w_ITCOD_PR,'ITDESPRE',this.w_ITDESPRE)
      insert into (i_cTable) (ITCODICE,ITDESCRI,CPROWNUM,CPROWORD,ITCODPRE,ITCOD_PR,ITDESPRE &i_ccchkf. );
         values (;
           this.w_ITCODICE;
           ,this.w_ITDESCRI;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_ITCODPRE;
           ,this.w_ITCOD_PR;
           ,this.w_ITDESPRE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_051F60B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PRE_ITER
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRE_ITER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRE_ITER_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_ITER_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ITDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_ITDESCRI),'PRE_ITER','ITDESCRI');
      +",CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PRE_ITER','CPROWORD');
      +",ITCODPRE ="+cp_NullLink(cp_ToStrODBC(this.w_ITCODPRE),'PRE_ITER','ITCODPRE');
      +",ITCOD_PR ="+cp_NullLink(cp_ToStrODBC(this.w_ITCOD_PR),'PRE_ITER','ITCOD_PR');
      +",ITDESPRE ="+cp_NullLink(cp_ToStrODBC(this.w_ITDESPRE),'PRE_ITER','ITDESPRE');
          +i_ccchkf ;
      +" where ";
          +"ITCODICE = "+cp_ToStrODBC(this.w_ITCODICE);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          ITDESCRI = this.w_ITDESCRI;
          ,CPROWORD = this.w_CPROWORD;
          ,ITCODPRE = this.w_ITCODPRE;
          ,ITCOD_PR = this.w_ITCOD_PR;
          ,ITDESPRE = this.w_ITDESPRE;
          &i_ccchkf. ;
       where;
          ITCODICE = this.w_ITCODICE;
          and CPROWNUM = this.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_43
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tipi attivit�
    this.w_OLD_COD = "@@@@@@"
    CREATE CURSOR TEMPO (CACODICE C(20), CADESCRI C(254), CADURORE N(3), CADURMIN N(2), CADATINI T(14), CAGGPREA N(3), CAPROMEM T(14), CAPRIORI N(1), ; 
 CATIPATT C(5), CACHKOBB C(1), CASTAATT C(1), CAFLNSAP C(1), CAFLTRIS C(1), CAFLRINV C(1), CACAUDOC C(5), CARAGGST C(1), CADISPON N(1), CPROWNUM N(3), CPROWORD N(4), ; 
 CACODSER C(20), CAKEYART C(41), CADESSER C(40), CADESAGG C(254), CAFLDEFF C(1), CAFLRESP C(1), CATIPRIS C(1), CAFLNOTI C(1), CAFLATRI C(1), CACHKNOM C(1), CADOCCLI C(1), ; 
 CA__NOTE C(254), CAMINPRE N(6), CAORASYS C(1), CAFLPREA C(1), CAFLANAL C(1), CATIPRIG C(1), CAPUBWEB C(1), CAFLGLIS C(1), CACAUACQ C(5), CACHKINI C(1), CACHKFOR C(1), ; 
 CAFLSTAT C(1), CACOLORE N(8), CAFLPRAT C(1), CAFLGIUD C(1), CAFLPART C(1), CAFLSOGG C(1), CAFLATTI C(1), CAFLNOMI C(1), CAFLPRAN N(1), CAFLGIUN N(1), CAFLPARN N(1), ; 
 CAFLSOGN N(1), CAFLATTN N(1), CAFLNOMN N(1), CATIPPRA C(10), CAMATPRA C(10), CA__ENTE C(10), CATIPRI2 C(1))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
     
 Select TEMPO 
 Go Top
    do while Not Eof("TEMPO")
      this.w_CACODICE = TEMPO.CACODICE
      this.w_CADESCRI = Nvl(TEMPO.CADESCRI,Space(60))
      this.w_CADURORE = Nvl(TEMPO.CADURORE,0)
      this.w_CADURMIN = Nvl(TEMPO.CADURMIN,0)
      this.w_CADATINI = Nvl(TEMPO.CADATINI,cp_CharToDatetime("  /  /       :  :  "))
      this.w_CAGGPREA = Nvl(TEMPO.CAGGPREA,0)
      this.w_CAPROMEM = Nvl(TEMPO.CAPROMEM,cp_CharToDatetime("  /  /       :  :  "))
      this.w_CAPRIORI = Nvl(TEMPO.CAPRIORI,0)
      this.w_CATIPATT = Nvl(TEMPO.CATIPATT,Space(5))
      this.w_CACHKOBB = Nvl(TEMPO.CACHKOBB,Space(1))
      this.w_CASTAATT = Nvl(TEMPO.CASTAATT,Space(1))
      this.w_CAFLNSAP = Nvl(TEMPO.CAFLNSAP,Space(1))
      this.w_CAFLTRIS = Nvl(TEMPO.CAFLTRIS,Space(1))
      this.w_CAFLRINV = Nvl(TEMPO.CAFLRINV,Space(1))
      this.w_CACAUDOC = Nvl(TEMPO.CACAUDOC,Space(5))
      this.w_CARAGGST = Nvl(TEMPO.CARAGGST,Space(1))
      this.w_CADISPON = Nvl(TEMPO.CADISPON,0)
      this.w_CPROWNUM = TEMPO.CPROWNUM
      this.w_CPROWORD = TEMPO.CPROWORD
      this.w_CACODSER = Nvl(TEMPO.CACODSER,Space(20))
      this.w_CAKEYART = Nvl(TEMPO.CAKEYART,Space(41))
      this.w_CADESSER = Nvl(TEMPO.CADESSER,Space(40))
      this.w_CADESAGG = Nvl(TEMPO.CADESAGG,Space(254))
      this.w_CAFLDEFF = Nvl(TEMPO.CAFLDEFF,Space(1))
      this.w_CAFLRESP = Nvl(TEMPO.CAFLRESP,Space(1))
      this.w_CATIPRIS = Nvl(TEMPO.CATIPRIS,Space(1))
      this.w_CAFLNOTI = Nvl(TEMPO.CAFLNOTI,Space(1))
      this.w_CAFLATRI = Nvl(TEMPO.CAFLATRI,Space(1))
      this.w_CACHKNOM = Nvl(TEMPO.CACHKNOM,Space(1))
      this.w_CADOCCLI = Nvl(TEMPO.CADOCCLI,Space(1))
      this.w_CA__NOTE = ALLTRIM(Nvl(TEMPO.CA__NOTE,""))
      this.w_CAMINPRE = Nvl(TEMPO.CAMINPRE,0)
      this.w_CAORASYS = Nvl(TEMPO.CAORASYS,Space(1))
      this.w_CAFLPREA = Nvl(TEMPO.CAFLPREA,Space(1))
      this.w_CAFLANAL = Nvl(TEMPO.CAFLANAL,Space(1))
      this.w_CATIPRIG = Nvl(TEMPO.CATIPRIG,Space(1))
      this.w_CAPUBWEB = Nvl(TEMPO.CAPUBWEB,Space(1))
      this.w_CAFLGLIS = Nvl(TEMPO.CAFLGLIS,Space(1))
      this.w_CACAUACQ = Nvl(TEMPO.CACAUACQ,Space(1))
      this.w_CACHKINI = Nvl(TEMPO.CACHKINI,Space(1))
      this.w_CACHKFOR = Nvl(TEMPO.CACHKFOR,Space(1))
      this.w_CAFLSTAT = Nvl(TEMPO.CAFLSTAT,Space(1))
      this.w_CACOLORE = Nvl(TEMPO.CACOLORE,0)
      this.w_CAFLPRAT = Nvl(TEMPO.CAFLPRAT,Space(1))
      this.w_CAFLGIUD = Nvl(TEMPO.CAFLGIUD,Space(1))
      this.w_CAFLPART = Nvl(TEMPO.CAFLPART,Space(1))
      this.w_CAFLSOGG = Nvl(TEMPO.CAFLSOGG,Space(1))
      this.w_CAFLATTI = Nvl(TEMPO.CAFLATTI,Space(1))
      this.w_CAFLNOMI = Nvl(TEMPO.CAFLNOMI,Space(1))
      this.w_CAFLPRAN = Nvl(TEMPO.CAFLPRAN,0)
      this.w_CAFLGIUN = Nvl(TEMPO.CAFLGIUN,0)
      this.w_CAFLPARN = Nvl(TEMPO.CAFLPARN,0)
      this.w_CAFLSOGN = Nvl(TEMPO.CAFLSOGN,0)
      this.w_CAFLATTN = Nvl(TEMPO.CAFLATTN,0)
      this.w_CAFLNOMN = Nvl(TEMPO.CAFLNOMN,0)
      this.w_CATIPPRA = NVL(TEMPO.CATIPPRA,SPACE(10))
      this.w_CAMATPRA = NVL(TEMPO.CAMATPRA,SPACE(10))
      this.w_CA__ENTE = NVL(TEMPO.CA__ENTE,SPACE(10))
      this.w_CATIPRI2 = Nvl(TEMPO.CATIPRI2,Space(1))
      * --- In presenza di un nuovo tipo attivit� sul txt
      if this.w_OLD_COD <> this.w_CACODICE
        ah_Msg("Inserisco/aggiorno tipo attivit� %1",.T.,.F.,.F., alltrim(this.w_CACODICE))
        * --- Try
        local bErr_05215C18
        bErr_05215C18=bTrsErr
        this.Try_05215C18()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Write into CAUMATTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CAUMATTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CAUMATTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CADESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_CADESCRI),'CAUMATTI','CADESCRI');
            +",CADURORE ="+cp_NullLink(cp_ToStrODBC(this.w_CADURORE),'CAUMATTI','CADURORE');
            +",CADURMIN ="+cp_NullLink(cp_ToStrODBC(this.w_CADURMIN),'CAUMATTI','CADURMIN');
            +",CADATINI ="+cp_NullLink(cp_ToStrODBC(this.w_CADATINI),'CAUMATTI','CADATINI');
            +",CAGGPREA ="+cp_NullLink(cp_ToStrODBC(this.w_CAGGPREA),'CAUMATTI','CAGGPREA');
            +",CAPROMEM ="+cp_NullLink(cp_ToStrODBC(this.w_CAPROMEM),'CAUMATTI','CAPROMEM');
            +",CAPRIORI ="+cp_NullLink(cp_ToStrODBC(this.w_CAPRIORI),'CAUMATTI','CAPRIORI');
            +",CATIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPATT),'CAUMATTI','CATIPATT');
            +",CACHKOBB ="+cp_NullLink(cp_ToStrODBC(this.w_CACHKOBB),'CAUMATTI','CACHKOBB');
            +",CASTAATT ="+cp_NullLink(cp_ToStrODBC(this.w_CASTAATT),'CAUMATTI','CASTAATT');
            +",CAFLNSAP ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLNSAP),'CAUMATTI','CAFLNSAP');
            +",CAFLTRIS ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLTRIS),'CAUMATTI','CAFLTRIS');
            +",CAFLRINV ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLRINV),'CAUMATTI','CAFLRINV');
            +",CACAUDOC ="+cp_NullLink(cp_ToStrODBC(this.w_CACAUDOC),'CAUMATTI','CACAUDOC');
            +",CARAGGST ="+cp_NullLink(cp_ToStrODBC(this.w_CARAGGST),'CAUMATTI','CARAGGST');
            +",CADISPON ="+cp_NullLink(cp_ToStrODBC(this.w_CADISPON),'CAUMATTI','CADISPON');
            +",CAFLNOTI ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLNOTI),'CAUMATTI','CAFLNOTI');
            +",CAFLATRI ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLATRI),'CAUMATTI','CAFLATRI');
            +",CACHKNOM ="+cp_NullLink(cp_ToStrODBC(this.w_CACHKNOM),'CAUMATTI','CACHKNOM');
            +",CADOCCLI ="+cp_NullLink(cp_ToStrODBC(this.w_CADOCCLI),'CAUMATTI','CADOCCLI');
            +",CA__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_CA__NOTE),'CAUMATTI','CA__NOTE');
            +",CAMINPRE ="+cp_NullLink(cp_ToStrODBC(this.w_CAMINPRE),'CAUMATTI','CAMINPRE');
            +",CAORASYS ="+cp_NullLink(cp_ToStrODBC(this.w_CAORASYS),'CAUMATTI','CAORASYS');
            +",CAFLPREA ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLPREA),'CAUMATTI','CAFLPREA');
            +",CAFLANAL ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLANAL),'CAUMATTI','CAFLANAL');
            +",CAPUBWEB ="+cp_NullLink(cp_ToStrODBC(this.w_CAPUBWEB),'CAUMATTI','CAPUBWEB');
            +",CAFLGLIS ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLGLIS),'CAUMATTI','CAFLGLIS');
            +",CACAUACQ ="+cp_NullLink(cp_ToStrODBC(this.w_CACAUACQ),'CAUMATTI','CACAUACQ');
            +",CACHKINI ="+cp_NullLink(cp_ToStrODBC(this.w_CACHKINI),'CAUMATTI','CACHKINI');
            +",CACHKFOR ="+cp_NullLink(cp_ToStrODBC(this.w_CACHKFOR),'CAUMATTI','CACHKFOR');
            +",CAFLSTAT ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLSTAT),'CAUMATTI','CAFLSTAT');
            +",CACOLORE ="+cp_NullLink(cp_ToStrODBC(this.w_CACOLORE),'CAUMATTI','CACOLORE');
            +",CAFLPRAT ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLPRAT),'CAUMATTI','CAFLPRAT');
            +",CAFLGIUD ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLGIUD),'CAUMATTI','CAFLGIUD');
            +",CAFLPART ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLPART),'CAUMATTI','CAFLPART');
            +",CAFLSOGG ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLSOGG),'CAUMATTI','CAFLSOGG');
            +",CAFLATTI ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLATTI),'CAUMATTI','CAFLATTI');
            +",CAFLNOMI ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLNOMI),'CAUMATTI','CAFLNOMI');
            +",CAFLPRAN ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLPRAN),'CAUMATTI','CAFLPRAN');
            +",CAFLGIUN ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLGIUN),'CAUMATTI','CAFLGIUN');
            +",CAFLPARN ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLPARN),'CAUMATTI','CAFLPARN');
            +",CAFLSOGN ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLSOGN),'CAUMATTI','CAFLSOGN');
            +",CAFLATTN ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLATTN),'CAUMATTI','CAFLATTN');
            +",CAFLNOMN ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLNOMN),'CAUMATTI','CAFLNOMN');
                +i_ccchkf ;
            +" where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_CACODICE);
                   )
          else
            update (i_cTable) set;
                CADESCRI = this.w_CADESCRI;
                ,CADURORE = this.w_CADURORE;
                ,CADURMIN = this.w_CADURMIN;
                ,CADATINI = this.w_CADATINI;
                ,CAGGPREA = this.w_CAGGPREA;
                ,CAPROMEM = this.w_CAPROMEM;
                ,CAPRIORI = this.w_CAPRIORI;
                ,CATIPATT = this.w_CATIPATT;
                ,CACHKOBB = this.w_CACHKOBB;
                ,CASTAATT = this.w_CASTAATT;
                ,CAFLNSAP = this.w_CAFLNSAP;
                ,CAFLTRIS = this.w_CAFLTRIS;
                ,CAFLRINV = this.w_CAFLRINV;
                ,CACAUDOC = this.w_CACAUDOC;
                ,CARAGGST = this.w_CARAGGST;
                ,CADISPON = this.w_CADISPON;
                ,CAFLNOTI = this.w_CAFLNOTI;
                ,CAFLATRI = this.w_CAFLATRI;
                ,CACHKNOM = this.w_CACHKNOM;
                ,CADOCCLI = this.w_CADOCCLI;
                ,CA__NOTE = this.w_CA__NOTE;
                ,CAMINPRE = this.w_CAMINPRE;
                ,CAORASYS = this.w_CAORASYS;
                ,CAFLPREA = this.w_CAFLPREA;
                ,CAFLANAL = this.w_CAFLANAL;
                ,CAPUBWEB = this.w_CAPUBWEB;
                ,CAFLGLIS = this.w_CAFLGLIS;
                ,CACAUACQ = this.w_CACAUACQ;
                ,CACHKINI = this.w_CACHKINI;
                ,CACHKFOR = this.w_CACHKFOR;
                ,CAFLSTAT = this.w_CAFLSTAT;
                ,CACOLORE = this.w_CACOLORE;
                ,CAFLPRAT = this.w_CAFLPRAT;
                ,CAFLGIUD = this.w_CAFLGIUD;
                ,CAFLPART = this.w_CAFLPART;
                ,CAFLSOGG = this.w_CAFLSOGG;
                ,CAFLATTI = this.w_CAFLATTI;
                ,CAFLNOMI = this.w_CAFLNOMI;
                ,CAFLPRAN = this.w_CAFLPRAN;
                ,CAFLGIUN = this.w_CAFLGIUN;
                ,CAFLPARN = this.w_CAFLPARN;
                ,CAFLSOGN = this.w_CAFLSOGN;
                ,CAFLATTN = this.w_CAFLATTN;
                ,CAFLNOMN = this.w_CAFLNOMN;
                &i_ccchkf. ;
             where;
                CACODICE = this.w_CACODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_05215C18
        * --- End
        * --- Tentiamo di gestire tipo, materia e autorit�
        if !EMPTY(this.w_CATIPPRA)
          * --- Accettiamo l'errore: significa semplicemente che il tipo in esame non � presente nei tipi pratica
          * --- Try
          local bErr_0521C458
          bErr_0521C458=bTrsErr
          this.Try_0521C458()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0521C458
          * --- End
        endif
        if !EMPTY(this.w_CAMATPRA)
          * --- Accettiamo l'errore: significa semplicemente che la materia in esame non � presente nelle autorit� pratica
          * --- Try
          local bErr_0521A208
          bErr_0521A208=bTrsErr
          this.Try_0521A208()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0521A208
          * --- End
        endif
        if !EMPTY(this.w_CA__ENTE)
          * --- Accettiamo l'errore: significa semplicemente che l'ente in esame non � presente nelle autorit� pratica
          * --- Try
          local bErr_05217F58
          bErr_05217F58=bTrsErr
          this.Try_05217F58()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_05217F58
          * --- End
        endif
      endif
      * --- Se w_CPROWNUM=0 allora scrive solo la testata
      if this.w_CPROWNUM<>0
        * --- Try
        local bErr_05212888
        bErr_05212888=bTrsErr
        this.Try_05212888()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Try
          local bErr_05213C08
          bErr_05213C08=bTrsErr
          this.Try_05213C08()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_05213C08
          * --- End
        endif
        bTrsErr=bTrsErr or bErr_05212888
        * --- End
      endif
      this.w_OLD_COD = this.w_CACODICE
       
 Select TEMPO 
 Skip
    enddo
  endproc
  proc Try_05215C18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAUMATTI
    i_nConn=i_TableProp[this.CAUMATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAUMATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CACODICE"+",CADESCRI"+",CADURORE"+",CADURMIN"+",CADATINI"+",CAGGPREA"+",CAPROMEM"+",CAPRIORI"+",CATIPATT"+",CACHKOBB"+",CASTAATT"+",CAFLNSAP"+",CAFLTRIS"+",CAFLRINV"+",CACAUDOC"+",CARAGGST"+",CADISPON"+",CAFLNOTI"+",CAFLATRI"+",CACHKNOM"+",CADOCCLI"+",CA__NOTE"+",CAMINPRE"+",CAORASYS"+",CAFLPREA"+",CAFLANAL"+",CAPUBWEB"+",CAFLGLIS"+",CACAUACQ"+",CACHKINI"+",CACHKFOR"+",CAFLSTAT"+",CACOLORE"+",CAFLPRAT"+",CAFLGIUD"+",CAFLPART"+",CAFLSOGG"+",CAFLATTI"+",CAFLNOMI"+",CAFLPRAN"+",CAFLGIUN"+",CAFLPARN"+",CAFLSOGN"+",CAFLATTN"+",CAFLNOMN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CACODICE),'CAUMATTI','CACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CADESCRI),'CAUMATTI','CADESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CADURORE),'CAUMATTI','CADURORE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CADURMIN),'CAUMATTI','CADURMIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CADATINI),'CAUMATTI','CADATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAGGPREA),'CAUMATTI','CAGGPREA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAPROMEM),'CAUMATTI','CAPROMEM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAPRIORI),'CAUMATTI','CAPRIORI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CATIPATT),'CAUMATTI','CATIPATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CACHKOBB),'CAUMATTI','CACHKOBB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CASTAATT),'CAUMATTI','CASTAATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLNSAP),'CAUMATTI','CAFLNSAP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLTRIS),'CAUMATTI','CAFLTRIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLRINV),'CAUMATTI','CAFLRINV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CACAUDOC),'CAUMATTI','CACAUDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CARAGGST),'CAUMATTI','CARAGGST');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CADISPON),'CAUMATTI','CADISPON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLNOTI),'CAUMATTI','CAFLNOTI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLATRI),'CAUMATTI','CAFLATRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CACHKNOM),'CAUMATTI','CACHKNOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CADOCCLI),'CAUMATTI','CADOCCLI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CA__NOTE),'CAUMATTI','CA__NOTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAMINPRE),'CAUMATTI','CAMINPRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAORASYS),'CAUMATTI','CAORASYS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLPREA),'CAUMATTI','CAFLPREA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLANAL),'CAUMATTI','CAFLANAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAPUBWEB),'CAUMATTI','CAPUBWEB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLGLIS),'CAUMATTI','CAFLGLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CACAUACQ),'CAUMATTI','CACAUACQ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CACHKINI),'CAUMATTI','CACHKINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CACHKFOR),'CAUMATTI','CACHKFOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLSTAT),'CAUMATTI','CAFLSTAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CACOLORE),'CAUMATTI','CACOLORE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLPRAT),'CAUMATTI','CAFLPRAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLGIUD),'CAUMATTI','CAFLGIUD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLPART),'CAUMATTI','CAFLPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLSOGG),'CAUMATTI','CAFLSOGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLATTI),'CAUMATTI','CAFLATTI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLNOMI),'CAUMATTI','CAFLNOMI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLPRAN),'CAUMATTI','CAFLPRAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLGIUN),'CAUMATTI','CAFLGIUN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLPARN),'CAUMATTI','CAFLPARN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLSOGN),'CAUMATTI','CAFLSOGN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLATTN),'CAUMATTI','CAFLATTN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLNOMN),'CAUMATTI','CAFLNOMN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CACODICE',this.w_CACODICE,'CADESCRI',this.w_CADESCRI,'CADURORE',this.w_CADURORE,'CADURMIN',this.w_CADURMIN,'CADATINI',this.w_CADATINI,'CAGGPREA',this.w_CAGGPREA,'CAPROMEM',this.w_CAPROMEM,'CAPRIORI',this.w_CAPRIORI,'CATIPATT',this.w_CATIPATT,'CACHKOBB',this.w_CACHKOBB,'CASTAATT',this.w_CASTAATT,'CAFLNSAP',this.w_CAFLNSAP)
      insert into (i_cTable) (CACODICE,CADESCRI,CADURORE,CADURMIN,CADATINI,CAGGPREA,CAPROMEM,CAPRIORI,CATIPATT,CACHKOBB,CASTAATT,CAFLNSAP,CAFLTRIS,CAFLRINV,CACAUDOC,CARAGGST,CADISPON,CAFLNOTI,CAFLATRI,CACHKNOM,CADOCCLI,CA__NOTE,CAMINPRE,CAORASYS,CAFLPREA,CAFLANAL,CAPUBWEB,CAFLGLIS,CACAUACQ,CACHKINI,CACHKFOR,CAFLSTAT,CACOLORE,CAFLPRAT,CAFLGIUD,CAFLPART,CAFLSOGG,CAFLATTI,CAFLNOMI,CAFLPRAN,CAFLGIUN,CAFLPARN,CAFLSOGN,CAFLATTN,CAFLNOMN &i_ccchkf. );
         values (;
           this.w_CACODICE;
           ,this.w_CADESCRI;
           ,this.w_CADURORE;
           ,this.w_CADURMIN;
           ,this.w_CADATINI;
           ,this.w_CAGGPREA;
           ,this.w_CAPROMEM;
           ,this.w_CAPRIORI;
           ,this.w_CATIPATT;
           ,this.w_CACHKOBB;
           ,this.w_CASTAATT;
           ,this.w_CAFLNSAP;
           ,this.w_CAFLTRIS;
           ,this.w_CAFLRINV;
           ,this.w_CACAUDOC;
           ,this.w_CARAGGST;
           ,this.w_CADISPON;
           ,this.w_CAFLNOTI;
           ,this.w_CAFLATRI;
           ,this.w_CACHKNOM;
           ,this.w_CADOCCLI;
           ,this.w_CA__NOTE;
           ,this.w_CAMINPRE;
           ,this.w_CAORASYS;
           ,this.w_CAFLPREA;
           ,this.w_CAFLANAL;
           ,this.w_CAPUBWEB;
           ,this.w_CAFLGLIS;
           ,this.w_CACAUACQ;
           ,this.w_CACHKINI;
           ,this.w_CACHKFOR;
           ,this.w_CAFLSTAT;
           ,this.w_CACOLORE;
           ,this.w_CAFLPRAT;
           ,this.w_CAFLGIUD;
           ,this.w_CAFLPART;
           ,this.w_CAFLSOGG;
           ,this.w_CAFLATTI;
           ,this.w_CAFLNOMI;
           ,this.w_CAFLPRAN;
           ,this.w_CAFLGIUN;
           ,this.w_CAFLPARN;
           ,this.w_CAFLSOGN;
           ,this.w_CAFLATTN;
           ,this.w_CAFLNOMN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0521C458()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CAUMATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CAUMATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CAUMATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CATIPPRA ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPPRA),'CAUMATTI','CATIPPRA');
          +i_ccchkf ;
      +" where ";
          +"CACODICE = "+cp_ToStrODBC(this.w_CACODICE);
             )
    else
      update (i_cTable) set;
          CATIPPRA = this.w_CATIPPRA;
          &i_ccchkf. ;
       where;
          CACODICE = this.w_CACODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_0521A208()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CAUMATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CAUMATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CAUMATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CAMATPRA ="+cp_NullLink(cp_ToStrODBC(this.w_CAMATPRA),'CAUMATTI','CAMATPRA');
          +i_ccchkf ;
      +" where ";
          +"CACODICE = "+cp_ToStrODBC(this.w_CACODICE);
             )
    else
      update (i_cTable) set;
          CAMATPRA = this.w_CAMATPRA;
          &i_ccchkf. ;
       where;
          CACODICE = this.w_CACODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_05217F58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CAUMATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CAUMATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUMATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CAUMATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CA__ENTE ="+cp_NullLink(cp_ToStrODBC(this.w_CA__ENTE),'CAUMATTI','CA__ENTE');
          +i_ccchkf ;
      +" where ";
          +"CACODICE = "+cp_ToStrODBC(this.w_CACODICE);
             )
    else
      update (i_cTable) set;
          CA__ENTE = this.w_CA__ENTE;
          &i_ccchkf. ;
       where;
          CACODICE = this.w_CACODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_05212888()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAU_ATTI
    i_nConn=i_TableProp[this.CAU_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_ATTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAU_ATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CACODICE"+",CPROWNUM"+",CPROWORD"+",CACODSER"+",CAKEYART"+",CADESSER"+",CADESAGG"+",CAFLDEFF"+",CAFLRESP"+",CATIPRIS"+",CATIPRIG"+",CATIPRI2"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CACODICE),'CAU_ATTI','CACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'CAU_ATTI','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'CAU_ATTI','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CACODSER),'CAU_ATTI','CACODSER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAKEYART),'CAU_ATTI','CAKEYART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CADESSER),'CAU_ATTI','CADESSER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CADESAGG),'CAU_ATTI','CADESAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLDEFF),'CAU_ATTI','CAFLDEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLRESP),'CAU_ATTI','CAFLRESP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CATIPRIS),'CAU_ATTI','CATIPRIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CATIPRIG),'CAU_ATTI','CATIPRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CATIPRI2),'CAU_ATTI','CATIPRI2');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CACODICE',this.w_CACODICE,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'CACODSER',this.w_CACODSER,'CAKEYART',this.w_CAKEYART,'CADESSER',this.w_CADESSER,'CADESAGG',this.w_CADESAGG,'CAFLDEFF',this.w_CAFLDEFF,'CAFLRESP',this.w_CAFLRESP,'CATIPRIS',this.w_CATIPRIS,'CATIPRIG',this.w_CATIPRIG,'CATIPRI2',this.w_CATIPRI2)
      insert into (i_cTable) (CACODICE,CPROWNUM,CPROWORD,CACODSER,CAKEYART,CADESSER,CADESAGG,CAFLDEFF,CAFLRESP,CATIPRIS,CATIPRIG,CATIPRI2 &i_ccchkf. );
         values (;
           this.w_CACODICE;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_CACODSER;
           ,this.w_CAKEYART;
           ,this.w_CADESSER;
           ,this.w_CADESAGG;
           ,this.w_CAFLDEFF;
           ,this.w_CAFLRESP;
           ,this.w_CATIPRIS;
           ,this.w_CATIPRIG;
           ,this.w_CATIPRI2;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_05213C08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CAU_ATTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CAU_ATTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_ATTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CAU_ATTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'CAU_ATTI','CPROWORD');
      +",CACODSER ="+cp_NullLink(cp_ToStrODBC(this.w_CACODSER),'CAU_ATTI','CACODSER');
      +",CAKEYART ="+cp_NullLink(cp_ToStrODBC(this.w_CAKEYART),'CAU_ATTI','CAKEYART');
      +",CADESSER ="+cp_NullLink(cp_ToStrODBC(this.w_CADESSER),'CAU_ATTI','CADESSER');
      +",CADESAGG ="+cp_NullLink(cp_ToStrODBC(this.w_CADESAGG),'CAU_ATTI','CADESAGG');
      +",CAFLDEFF ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLDEFF),'CAU_ATTI','CAFLDEFF');
      +",CAFLRESP ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLRESP),'CAU_ATTI','CAFLRESP');
      +",CATIPRIS ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPRIS),'CAU_ATTI','CATIPRIS');
      +",CATIPRIG ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPRIG),'CAU_ATTI','CATIPRIG');
      +",CATIPRI2 ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPRI2),'CAU_ATTI','CATIPRI2');
          +i_ccchkf ;
      +" where ";
          +"CACODICE = "+cp_ToStrODBC(this.w_CACODICE);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          CPROWORD = this.w_CPROWORD;
          ,CACODSER = this.w_CACODSER;
          ,CAKEYART = this.w_CAKEYART;
          ,CADESSER = this.w_CADESSER;
          ,CADESAGG = this.w_CADESAGG;
          ,CAFLDEFF = this.w_CAFLDEFF;
          ,CAFLRESP = this.w_CAFLRESP;
          ,CATIPRIS = this.w_CATIPRIS;
          ,CATIPRIG = this.w_CATIPRIG;
          ,CATIPRI2 = this.w_CATIPRI2;
          &i_ccchkf. ;
       where;
          CACODICE = this.w_CACODICE;
          and CPROWNUM = this.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_44
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Attivit� collegate
    * --- Se si � scelto di eliminare i reggruppamenti di attivit� (a pag.1)
    if IsAlt() AND this.w_CANC_RAGGR
      * --- Try
      local bErr_052D4658
      bErr_052D4658=bTrsErr
      this.Try_052D4658()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_052D4658
      * --- End
    endif
    CREATE CURSOR TEMPO (IPCODICE C(10), IPDESCRI C(60), CPROWNUM N(4), CPROWORD N(5), IPCODCAU C(20), IPDESCAU C(254), IPNUMGIO N(5), IPRIFPRO N(5) )
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
     
 Select TEMPO 
 Go Top
    do while Not Eof("TEMPO")
      this.w_IPCODICE = TEMPO.IPCODICE
      this.w_IPDESCRI = Nvl(TEMPO.IPDESCRI,Space(60))
      this.w_CPROWNUM = TEMPO.CPROWNUM
      this.w_CPROWORD = TEMPO.CPROWORD
      this.w_IPCODCAU = Nvl(TEMPO.IPCODCAU,Space(20))
      this.w_IPDESCAU = Nvl(TEMPO.IPDESCAU,Space(254))
      this.w_IPNUMGIO = Nvl(TEMPO.IPNUMGIO,0)
      this.w_IPRIFPRO = Nvl(TEMPO.IPRIFPRO,0)
      * --- In presenza di una nuova attivit� collegata sul txt
      ah_Msg("Inserisco/aggiorno attivit� collegata %1",.T.,.F.,.F., alltrim(this.w_IPCODICE))
      * --- Try
      local bErr_052CE088
      bErr_052CE088=bTrsErr
      this.Try_052CE088()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_052CFC48
        bErr_052CFC48=bTrsErr
        this.Try_052CFC48()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_052CFC48
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_052CE088
      * --- End
       
 Select TEMPO 
 Skip
    enddo
  endproc
  proc Try_052D4658()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from PRO_ITER
    i_nConn=i_TableProp[this.PRO_ITER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_ITER_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_052CE088()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRO_ITER
    i_nConn=i_TableProp[this.PRO_ITER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_ITER_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRO_ITER_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"IPCODICE"+",IPDESCRI"+",CPROWNUM"+",CPROWORD"+",IPCODCAU"+",IPDESCAU"+",IPNUMGIO"+",IPRIFPRO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_IPCODICE),'PRO_ITER','IPCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IPDESCRI),'PRO_ITER','IPDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PRO_ITER','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PRO_ITER','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IPCODCAU),'PRO_ITER','IPCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IPDESCAU),'PRO_ITER','IPDESCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IPNUMGIO),'PRO_ITER','IPNUMGIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IPRIFPRO),'PRO_ITER','IPRIFPRO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'IPCODICE',this.w_IPCODICE,'IPDESCRI',this.w_IPDESCRI,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'IPCODCAU',this.w_IPCODCAU,'IPDESCAU',this.w_IPDESCAU,'IPNUMGIO',this.w_IPNUMGIO,'IPRIFPRO',this.w_IPRIFPRO)
      insert into (i_cTable) (IPCODICE,IPDESCRI,CPROWNUM,CPROWORD,IPCODCAU,IPDESCAU,IPNUMGIO,IPRIFPRO &i_ccchkf. );
         values (;
           this.w_IPCODICE;
           ,this.w_IPDESCRI;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_IPCODCAU;
           ,this.w_IPDESCAU;
           ,this.w_IPNUMGIO;
           ,this.w_IPRIFPRO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_052CFC48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PRO_ITER
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRO_ITER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_ITER_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_ITER_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"IPDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_IPDESCRI),'PRO_ITER','IPDESCRI');
      +",CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PRO_ITER','CPROWORD');
      +",IPCODCAU ="+cp_NullLink(cp_ToStrODBC(this.w_IPCODCAU),'PRO_ITER','IPCODCAU');
      +",IPDESCAU ="+cp_NullLink(cp_ToStrODBC(this.w_IPDESCAU),'PRO_ITER','IPDESCAU');
      +",IPNUMGIO ="+cp_NullLink(cp_ToStrODBC(this.w_IPNUMGIO),'PRO_ITER','IPNUMGIO');
      +",IPRIFPRO ="+cp_NullLink(cp_ToStrODBC(this.w_IPRIFPRO),'PRO_ITER','IPRIFPRO');
          +i_ccchkf ;
      +" where ";
          +"IPCODICE = "+cp_ToStrODBC(this.w_IPCODICE);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          IPDESCRI = this.w_IPDESCRI;
          ,CPROWORD = this.w_CPROWORD;
          ,IPCODCAU = this.w_IPCODCAU;
          ,IPDESCAU = this.w_IPDESCAU;
          ,IPNUMGIO = this.w_IPNUMGIO;
          ,IPRIFPRO = this.w_IPRIFPRO;
          &i_ccchkf. ;
       where;
          IPCODICE = this.w_IPCODICE;
          and CPROWNUM = this.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_45
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento della lista dei metodi WS SOStitutiva
    CREATE CURSOR TEMPO (NOMEMETO C(60),DESCMETO C(254))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    * --- Marco tutti i metodi presenti come obsoleti
    * --- Write into MET_SOS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MET_SOS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MET_SOS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MET_SOS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OBSOLETO ="+cp_NullLink(cp_ToStrODBC("S"),'MET_SOS','OBSOLETO');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          OBSOLETO = "S";
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_CURSORE = SELECT()
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE) 
 GO TOP 
 SCAN FOR NOT EMPTY(NVL(NOMEMETO," "))
    this.w_NOMEMETODO = TEMPO.NOMEMETO
    this.w_DESCMETODO = TEMPO.DESCMETO
    * --- Try
    local bErr_052CB118
    bErr_052CB118=bTrsErr
    this.Try_052CB118()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Write into MET_SOS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MET_SOS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MET_SOS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MET_SOS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DESCMETO ="+cp_NullLink(cp_ToStrODBC(this.w_DESCMETODO),'MET_SOS','DESCMETO');
            +i_ccchkf ;
        +" where ";
            +"NOMEMETO = "+cp_ToStrODBC(this.w_NOMEMETODO);
               )
      else
        update (i_cTable) set;
            DESCMETO = this.w_DESCMETODO;
            &i_ccchkf. ;
         where;
            NOMEMETO = this.w_NOMEMETODO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if i_Rows=0
        * --- accept error
        bTrsErr=.f.
        this.w_ERRORGRU = .T.
      endif
    endif
    bTrsErr=bTrsErr or bErr_052CB118
    * --- End
    ENDSCAN
  endproc
  proc Try_052CB118()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MET_SOS
    i_nConn=i_TableProp[this.MET_SOS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MET_SOS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MET_SOS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"NOMEMETO"+",DESCMETO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_NOMEMETODO),'MET_SOS','NOMEMETO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCMETODO),'MET_SOS','DESCMETO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'NOMEMETO',this.w_NOMEMETODO,'DESCMETO',this.w_DESCMETODO)
      insert into (i_cTable) (NOMEMETO,DESCMETO &i_ccchkf. );
         values (;
           this.w_NOMEMETODO;
           ,this.w_DESCMETODO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_46
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento degli stati dichiarati in SOStitutiva
    CREATE CURSOR TEMPO (CODSTATO C(18),DESCSTATO C(35))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE) 
 GO TOP 
 SCAN FOR NOT EMPTY(NVL(CODSTATO," "))
    this.w_CODICESTATO = TEMPO.CODSTATO
    this.w_DESCSTATO = TEMPO.DESCSTATO
    * --- Try
    local bErr_052E4380
    bErr_052E4380=bTrsErr
    this.Try_052E4380()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Write into STATISOS
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.STATISOS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STATISOS_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.STATISOS_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"STATODES ="+cp_NullLink(cp_ToStrODBC(this.w_DESCSTATO),'STATISOS','STATODES');
            +i_ccchkf ;
        +" where ";
            +"STATOVAL = "+cp_ToStrODBC(this.w_CODICESTATO);
               )
      else
        update (i_cTable) set;
            STATODES = this.w_DESCSTATO;
            &i_ccchkf. ;
         where;
            STATOVAL = this.w_CODICESTATO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if i_Rows=0
        * --- accept error
        bTrsErr=.f.
        this.w_ERRORGRU = .T.
      endif
    endif
    bTrsErr=bTrsErr or bErr_052E4380
    * --- End
    ENDSCAN
  endproc
  proc Try_052E4380()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STATISOS
    i_nConn=i_TableProp[this.STATISOS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STATISOS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STATISOS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"STATOVAL"+",STATODES"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODICESTATO),'STATISOS','STATOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCSTATO),'STATISOS','STATODES');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'STATOVAL',this.w_CODICESTATO,'STATODES',this.w_DESCSTATO)
      insert into (i_cTable) (STATOVAL,STATODES &i_ccchkf. );
         values (;
           this.w_CODICESTATO;
           ,this.w_DESCSTATO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_47
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- RSS Feeds
    this.w_SUBSCRIBE = "S"
    CREATE CURSOR TEMPO (RFRSSNAM C(15), RFDESCRI C(254), RFRSSURL C(254), RFMAXITM N(3,0), RFFLGDSK C(1))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
     
 Select TEMPO 
 Go Top
    do while Not Eof("TEMPO")
      this.w_RFRSSNAM = TEMPO.RFRSSNAM
      this.w_RFDESCRI = TEMPO.RFDESCRI
      this.w_RFRSSURL = TEMPO.RFRSSURL
      this.w_RFMAXITM = TEMPO.RFMAXITM
      this.w_RFFLGDSK = TEMPO.RFFLGDSK
      * --- In presenza di un nuovo Comune sul txt
      ah_Msg("Inserisco/aggiorno RSS Feed %1",.T.,.F.,.F., alltrim(this.w_RFDESCRI))
      * --- Try
      local bErr_052EDE00
      bErr_052EDE00=bTrsErr
      this.Try_052EDE00()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into RSS_FEED
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.RSS_FEED_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RSS_FEED_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.RSS_FEED_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"RFDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_RFDESCRI),'RSS_FEED','RFDESCRI');
          +",RFRSSURL ="+cp_NullLink(cp_ToStrODBC(this.w_RFRSSURL),'RSS_FEED','RFRSSURL');
          +",RFMAXITM ="+cp_NullLink(cp_ToStrODBC(this.w_RFMAXITM),'RSS_FEED','RFMAXITM');
          +",RFFLGDSK ="+cp_NullLink(cp_ToStrODBC(this.w_RFFLGDSK),'RSS_FEED','RFFLGDSK');
              +i_ccchkf ;
          +" where ";
              +"RFRSSNAM = "+cp_ToStrODBC(this.w_RFRSSNAM);
                 )
        else
          update (i_cTable) set;
              RFDESCRI = this.w_RFDESCRI;
              ,RFRSSURL = this.w_RFRSSURL;
              ,RFMAXITM = this.w_RFMAXITM;
              ,RFFLGDSK = this.w_RFFLGDSK;
              &i_ccchkf. ;
           where;
              RFRSSNAM = this.w_RFRSSNAM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_052EDE00
      * --- End
      * --- Sottoscrivo il feed
      if this.w_RFFLGDSK = "A"
        GSUT_BRS(this,"S","")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
       
 Select TEMPO 
 Skip
    enddo
  endproc
  proc Try_052EDE00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into RSS_FEED
    i_nConn=i_TableProp[this.RSS_FEED_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RSS_FEED_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RSS_FEED_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RFRSSNAM"+",RFDESCRI"+",RFRSSURL"+",RFMAXITM"+",RFFLGDSK"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_RFRSSNAM),'RSS_FEED','RFRSSNAM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RFDESCRI),'RSS_FEED','RFDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RFRSSURL),'RSS_FEED','RFRSSURL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RFMAXITM),'RSS_FEED','RFMAXITM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RFFLGDSK),'RSS_FEED','RFFLGDSK');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RFRSSNAM',this.w_RFRSSNAM,'RFDESCRI',this.w_RFDESCRI,'RFRSSURL',this.w_RFRSSURL,'RFMAXITM',this.w_RFMAXITM,'RFFLGDSK',this.w_RFFLGDSK)
      insert into (i_cTable) (RFRSSNAM,RFDESCRI,RFRSSURL,RFMAXITM,RFFLGDSK &i_ccchkf. );
         values (;
           this.w_RFRSSNAM;
           ,this.w_RFDESCRI;
           ,this.w_RFRSSURL;
           ,this.w_RFMAXITM;
           ,this.w_RFFLGDSK;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_48
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Campi Outlook
    if this.oParentObject.w_TIPARC="OU"
      * --- Dettaglio propriet� Outlook
      CREATE CURSOR TEMPO (MOTIPO_P C(1), MOPRPRTY C(50), MODESCRI C(50))
      APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
      this.w_CURSORE = SELECT()
      * --- Inserisco nell'archivio
      SELECT (this.w_CURSORE)
      GO TOP
      SCAN FOR NOT EMPTY(NVL(MOPRPRTY," "))
      * --- Inserisco... Non controllo la tipologia C-Contatto o A-Appuntamento
      this.w_MOPRPRTY = TEMPO.MOPRPRTY
      this.w_MOTIPO_P = NVL(TEMPO.MOTIPO_P, "C" )
      this.w_MODESCRI = NVL(TEMPO.MODESCRI, SPACE(50))
      * --- Try
      local bErr_052F6CA0
      bErr_052F6CA0=bTrsErr
      this.Try_052F6CA0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_052F96D0
        bErr_052F96D0=bTrsErr
        this.Try_052F96D0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          if not ah_YesNo("Errore di scrittura campi Outlook: %1%0Continuo?","", this.w_OPCODICE )
            * --- Raise
            i_Error=ah_Msgformat("Import fallito")
            return
          else
            * --- accept error
            bTrsErr=.f.
          endif
        endif
        bTrsErr=bTrsErr or bErr_052F96D0
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_052F6CA0
      * --- End
      SELECT (this.w_CURSORE)
      ENDSCAN
    else
      * --- w_TIPARC='OM'
      *     Mappatura campi per sincronizzazione con Outlook
      this.w_MOPRPRTY = ""
      * --- Select from MAP_OUTL
      i_nConn=i_TableProp[this.MAP_OUTL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MAP_OUTL_idx,2],.t.,this.MAP_OUTL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MAX(MOPRPRTY) AS PREC_INS  from "+i_cTable+" MAP_OUTL ";
             ,"_Curs_MAP_OUTL")
      else
        select MAX(MOPRPRTY) AS PREC_INS from (i_cTable);
          into cursor _Curs_MAP_OUTL
      endif
      if used('_Curs_MAP_OUTL')
        select _Curs_MAP_OUTL
        locate for 1=1
        do while not(eof())
        this.w_MOPRPRTY = NVL(PREC_INS,"")
          select _Curs_MAP_OUTL
          continue
        enddo
        use
      endif
      if EMPTY(this.w_MOPRPRTY)
        ah_ErrorMsg("Impossibile procedere! E' necessario importare prima i campi di Outlook")
        this.w_NOMSG = .T.
      else
        * --- Utiilzziamo ROWNUM definito a pag. 31
        this.w_ROWNUM = 0
        this.w_CODAZI = "XXXXX"
        this.w_ELIMINA = ah_YesNo("Elimino i dati precedentemente inseriti?")
        if this.w_ELIMINA=.T.
          * --- Delete from MAP_SINC
          i_nConn=i_TableProp[this.MAP_SINC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAP_SINC_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"MCCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                   )
          else
            delete from (i_cTable) where;
                  MCCODAZI = this.w_CODAZI;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          this.w_ROWNUM = 0
        else
          * --- Select from MAP_SINC
          i_nConn=i_TableProp[this.MAP_SINC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MAP_SINC_idx,2],.t.,this.MAP_SINC_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select MAX(CPROWORD) AS CPROWORD  from "+i_cTable+" MAP_SINC ";
                +" where MCCODAZI="+cp_ToStrODBC(this.w_CODAZI)+"";
                 ,"_Curs_MAP_SINC")
          else
            select MAX(CPROWORD) AS CPROWORD from (i_cTable);
             where MCCODAZI=this.w_CODAZI;
              into cursor _Curs_MAP_SINC
          endif
          if used('_Curs_MAP_SINC')
            select _Curs_MAP_SINC
            locate for 1=1
            do while not(eof())
            this.w_ROWNUM = NVL(CPROWORD,0)
              select _Curs_MAP_SINC
              continue
            enddo
            use
          endif
        endif
        CREATE CURSOR TEMPO (MCNOMTAB C(20), MCGESTIO C(10), MC_VERSO C(1), MCTIPO_P C(1), MCMSOUTL C(50), MCTIPEXP C(1), MCEXPRES C(250), MCFL_KEY C(1))
        APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
        this.w_CURSORE = SELECT()
        * --- Inserisco nell'archivio
        SELECT (this.w_CURSORE)
        GO TOP
        SCAN FOR NOT EMPTY(NVL(MCGESTIO," ")) AND NOT EMPTY(NVL(TEMPO.MCNOMTAB,""))
        * --- Inserisco... 
        this.w_MCNOMTAB = TEMPO.MCNOMTAB
        this.w_MCGESTIO = TEMPO.MCGESTIO
        this.w_MC_VERSO = NVL(TEMPO.MC_VERSO,"I")
        this.w_MCTIPO_P = NVL(TEMPO.MCTIPO_P,"C")
        this.w_MCMSOUTL = NVL(TEMPO.MCMSOUTL,"")
        this.w_MCTIPEXP = NVL(TEMPO.MCTIPEXP,"N")
        this.w_MCEXPRES = NVL(TEMPO.MCEXPRES,"")
        this.w_ROWNUM = this.w_ROWNUM + 10
        this.w_MCFL_KEY = NVL(TEMPO.MCFL_KEY,"N")
        * --- Procediamo incrementando ROWNUM
        * --- Try
        local bErr_0530B538
        bErr_0530B538=bTrsErr
        this.Try_0530B538()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Try
          local bErr_0530DF68
          bErr_0530DF68=bTrsErr
          this.Try_0530DF68()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            if not ah_YesNo("Errore di scrittura mappatura campi sincronizzazione con Outlook: %1%0Continuo?","", this.w_OPCODICE )
              * --- Raise
              i_Error=ah_Msgformat("Import fallito")
              return
            else
              * --- accept error
              bTrsErr=.f.
            endif
          endif
          bTrsErr=bTrsErr or bErr_0530DF68
          * --- End
        endif
        bTrsErr=bTrsErr or bErr_0530B538
        * --- End
        SELECT (this.w_CURSORE)
        ENDSCAN
      endif
    endif
  endproc
  proc Try_052F6CA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MAP_OUTL
    i_nConn=i_TableProp[this.MAP_OUTL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAP_OUTL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MAP_OUTL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MOPRPRTY"+",MOTIPO_P"+",MODESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MOPRPRTY),'MAP_OUTL','MOPRPRTY');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MOTIPO_P),'MAP_OUTL','MOTIPO_P');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MODESCRI),'MAP_OUTL','MODESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MOPRPRTY',this.w_MOPRPRTY,'MOTIPO_P',this.w_MOTIPO_P,'MODESCRI',this.w_MODESCRI)
      insert into (i_cTable) (MOPRPRTY,MOTIPO_P,MODESCRI &i_ccchkf. );
         values (;
           this.w_MOPRPRTY;
           ,this.w_MOTIPO_P;
           ,this.w_MODESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco campi Outlook: %1",.t.,,,this.w_MOPRPRTY)
    return
  proc Try_052F96D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into MAP_OUTL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MAP_OUTL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAP_OUTL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MAP_OUTL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MODESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_MODESCRI),'MAP_OUTL','MODESCRI');
          +i_ccchkf ;
      +" where ";
          +"MOPRPRTY = "+cp_ToStrODBC(this.w_MOPRPRTY);
          +" and MOTIPO_P = "+cp_ToStrODBC(this.w_MOTIPO_P);
             )
    else
      update (i_cTable) set;
          MODESCRI = this.w_MODESCRI;
          &i_ccchkf. ;
       where;
          MOPRPRTY = this.w_MOPRPRTY;
          and MOTIPO_P = this.w_MOTIPO_P;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Scrivo campi Outlook: %1",.t.,,,this.w_MOPRPRTY)
    return
  proc Try_0530B538()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MAP_SINC
    i_nConn=i_TableProp[this.MAP_SINC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAP_SINC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MAP_SINC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MCCODAZI"+",MCNOMTAB"+",MCGESTIO"+",MC_VERSO"+",MCTIPO_P"+",MCMSOUTL"+",MCTIPEXP"+",MCEXPRES"+",CPROWORD"+",MCFL_KEY"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CodAzi),'MAP_SINC','MCCODAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCNOMTAB),'MAP_SINC','MCNOMTAB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCGESTIO),'MAP_SINC','MCGESTIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MC_VERSO),'MAP_SINC','MC_VERSO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCTIPO_P),'MAP_SINC','MCTIPO_P');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCMSOUTL),'MAP_SINC','MCMSOUTL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCTIPEXP),'MAP_SINC','MCTIPEXP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCEXPRES),'MAP_SINC','MCEXPRES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'MAP_SINC','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCFL_KEY),'MAP_SINC','MCFL_KEY');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MCCODAZI',this.w_CodAzi,'MCNOMTAB',this.w_MCNOMTAB,'MCGESTIO',this.w_MCGESTIO,'MC_VERSO',this.w_MC_VERSO,'MCTIPO_P',this.w_MCTIPO_P,'MCMSOUTL',this.w_MCMSOUTL,'MCTIPEXP',this.w_MCTIPEXP,'MCEXPRES',this.w_MCEXPRES,'CPROWORD',this.w_ROWNUM,'MCFL_KEY',this.w_MCFL_KEY)
      insert into (i_cTable) (MCCODAZI,MCNOMTAB,MCGESTIO,MC_VERSO,MCTIPO_P,MCMSOUTL,MCTIPEXP,MCEXPRES,CPROWORD,MCFL_KEY &i_ccchkf. );
         values (;
           this.w_CodAzi;
           ,this.w_MCNOMTAB;
           ,this.w_MCGESTIO;
           ,this.w_MC_VERSO;
           ,this.w_MCTIPO_P;
           ,this.w_MCMSOUTL;
           ,this.w_MCTIPEXP;
           ,this.w_MCEXPRES;
           ,this.w_ROWNUM;
           ,this.w_MCFL_KEY;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco mappatura campi sincronizzazione con Outlook: %1",.t.,,,this.w_MOPRPRTY)
    return
  proc Try_0530DF68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into MAP_SINC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MAP_SINC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MAP_SINC_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MAP_SINC_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MCEXPRES ="+cp_NullLink(cp_ToStrODBC(this.w_MCEXPRES),'MAP_SINC','MCEXPRES');
      +",MCMSOUTL ="+cp_NullLink(cp_ToStrODBC(this.w_MCMSOUTL),'MAP_SINC','MCMSOUTL');
      +",MCTIPEXP ="+cp_NullLink(cp_ToStrODBC(this.w_MCTIPEXP),'MAP_SINC','MCTIPEXP');
      +",MCTIPO_P ="+cp_NullLink(cp_ToStrODBC(this.w_MCTIPO_P),'MAP_SINC','MCTIPO_P');
      +",CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'MAP_SINC','CPROWORD');
      +",MCFL_KEY ="+cp_NullLink(cp_ToStrODBC(this.w_MCFL_KEY),'MAP_SINC','MCFL_KEY');
          +i_ccchkf ;
      +" where ";
          +"MCCODAZI = "+cp_ToStrODBC(this.w_CodAzi);
          +" and MCNOMTAB = "+cp_ToStrODBC(this.w_MCNOMTAB);
          +" and MCGESTIO = "+cp_ToStrODBC(this.w_MCGESTIO);
          +" and MC_VERSO = "+cp_ToStrODBC(this.w_MC_VERSO);
             )
    else
      update (i_cTable) set;
          MCEXPRES = this.w_MCEXPRES;
          ,MCMSOUTL = this.w_MCMSOUTL;
          ,MCTIPEXP = this.w_MCTIPEXP;
          ,MCTIPO_P = this.w_MCTIPO_P;
          ,CPROWORD = this.w_ROWNUM;
          ,MCFL_KEY = this.w_MCFL_KEY;
          &i_ccchkf. ;
       where;
          MCCODAZI = this.w_CodAzi;
          and MCNOMTAB = this.w_MCNOMTAB;
          and MCGESTIO = this.w_MCGESTIO;
          and MC_VERSO = this.w_MC_VERSO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Scrivo mappatura campi sincronizzazione con Outlook: %1",.t.,,,this.w_MOPRPRTY)
    return


  procedure Page_49
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica regole controllo flussi
    CREATE CURSOR TEMPO (REBUSOBJ c(15), RETABMST c(20), RETABDTL c(20), REFLENAB c(1), RESAVEDB c(1), REUSEROP c(1), ; 
 REDESCRI c(50), REREGSUC c(1), RETFLTEX c(254), REPRIORI n(4,0), REGG_LOG n(4,0), RETDELEX c(254), REPSTBTN c(1), REKEYSEL c(60), ; 
 REKEYGES c(1), REMSGERC c(254), RE_TABLE c(20), REFLDNAM c(30), CPROWORD n(6,0), REKEYREC c(1), RERIFREC c(1), REMONINS c(1), ; 
 REMONMOD c(1), REMONDEL c(1), REBLKINS c(1), REOBBINS c(1), REFLTEXP c(254), SERIAL C(10), REFLDCOM c(254), REMSGERR c(254))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE) 
 GO TOP 
 SCAN FOR NOT EMPTY(REBUSOBJ)
    SELECT (this.w_CURSORE)
    if this.w_SERIAL<>TEMPO.SERIAL
      * --- Inserisco master
      this.w_SERIAL = TEMPO.SERIAL
      this.w_RESERIAL = cp_GetProg("REF_MAST", "SEREG", this.w_RESERIAL, i_codazi)
      this.w_REBUSOBJ = TEMPO.REBUSOBJ
      this.w_RETABMST = TEMPO.RETABMST
      this.w_RETABDTL = TEMPO.RETABDTL
      this.w_REDESCRI = TEMPO.REDESCRI 
      this.w_REPSTBTN = TEMPO.REPSTBTN
      this.w_REKEYSEL = TEMPO.REKEYSEL
      this.w_REFLENAB = TEMPO.REFLENAB
      this.w_RESAVEDB = TEMPO.RESAVEDB
      this.w_REUSEROP = TEMPO.REUSEROP
      this.w_REREGSUC = TEMPO.REREGSUC
      this.w_RETFLTEX = TEMPO.RETFLTEX
      this.w_RETDELEX = TEMPO.RETDELEX
      this.w_REPRIORI = TEMPO.REPRIORI
      this.w_REGG_LOG = TEMPO.REGG_LOG
      this.w_REKEYGES = TEMPO.REKEYGES
      this.w_REMSGERC = TEMPO.REMSGERC
      * --- Insert into REF_MAST
      i_nConn=i_TableProp[this.REF_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.REF_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.REF_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"RESERIAL"+",REBUSOBJ"+",RETABMST"+",RETABDTL"+",REFLENAB"+",RESAVEDB"+",REUSEROP"+",REDESCRI"+",REREGSUC"+",RETFLTEX"+",REPRIORI"+",REGG_LOG"+",REPSTBTN"+",REKEYSEL"+",REKEYGES"+",REMSGERC"+",RETDELEX"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_RESERIAL),'REF_MAST','RESERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_REBUSOBJ),'REF_MAST','REBUSOBJ');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RETABMST),'REF_MAST','RETABMST');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RETABDTL),'REF_MAST','RETABDTL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_REFLENAB),'REF_MAST','REFLENAB');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RESAVEDB),'REF_MAST','RESAVEDB');
        +","+cp_NullLink(cp_ToStrODBC(this.w_REUSEROP),'REF_MAST','REUSEROP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_REDESCRI),'REF_MAST','REDESCRI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_REREGSUC),'REF_MAST','REREGSUC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RETFLTEX),'REF_MAST','RETFLTEX');
        +","+cp_NullLink(cp_ToStrODBC(this.w_REPRIORI),'REF_MAST','REPRIORI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_REGG_LOG),'REF_MAST','REGG_LOG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_REPSTBTN),'REF_MAST','REPSTBTN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_REKEYSEL),'REF_MAST','REKEYSEL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_REKEYGES),'REF_MAST','REKEYGES');
        +","+cp_NullLink(cp_ToStrODBC(this.w_REMSGERC),'REF_MAST','REMSGERC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_RETDELEX),'REF_MAST','RETDELEX');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'RESERIAL',this.w_RESERIAL,'REBUSOBJ',this.w_REBUSOBJ,'RETABMST',this.w_RETABMST,'RETABDTL',this.w_RETABDTL,'REFLENAB',this.w_REFLENAB,'RESAVEDB',this.w_RESAVEDB,'REUSEROP',this.w_REUSEROP,'REDESCRI',this.w_REDESCRI,'REREGSUC',this.w_REREGSUC,'RETFLTEX',this.w_RETFLTEX,'REPRIORI',this.w_REPRIORI,'REGG_LOG',this.w_REGG_LOG)
        insert into (i_cTable) (RESERIAL,REBUSOBJ,RETABMST,RETABDTL,REFLENAB,RESAVEDB,REUSEROP,REDESCRI,REREGSUC,RETFLTEX,REPRIORI,REGG_LOG,REPSTBTN,REKEYSEL,REKEYGES,REMSGERC,RETDELEX &i_ccchkf. );
           values (;
             this.w_RESERIAL;
             ,this.w_REBUSOBJ;
             ,this.w_RETABMST;
             ,this.w_RETABDTL;
             ,this.w_REFLENAB;
             ,this.w_RESAVEDB;
             ,this.w_REUSEROP;
             ,this.w_REDESCRI;
             ,this.w_REREGSUC;
             ,this.w_RETFLTEX;
             ,this.w_REPRIORI;
             ,this.w_REGG_LOG;
             ,this.w_REPSTBTN;
             ,this.w_REKEYSEL;
             ,this.w_REKEYGES;
             ,this.w_REMSGERC;
             ,this.w_RETDELEX;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      this.w_CPROWNUM = 0
    endif
    * --- Inserisco detail
    this.w_CPROWNUM = this.w_CPROWNUM+1
    this.w_CPROWORD = TEMPO.CPROWORD
    this.w_RE_TABLE = TEMPO.RE_TABLE
    this.w_REFLDNAM = TEMPO.REFLDNAM
    this.w_REKEYREC = TEMPO.REKEYREC
    this.w_RERIFREC = TEMPO.RERIFREC
    this.w_REMONINS = TEMPO.REMONINS
    this.w_REMONMOD = TEMPO.REMONMOD
    this.w_REMONDEL = TEMPO.REMONDEL
    this.w_REBLKINS = TEMPO.REBLKINS
    this.w_REOBBINS = TEMPO.REOBBINS
    this.w_REFLTEXP = TEMPO.REFLTEXP
    this.w_REFLDCOM = TEMPO.REFLDCOM
    this.w_REMSGERR = TEMPO.REMSGERR
    * --- Insert into REF_DETT
    i_nConn=i_TableProp[this.REF_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.REF_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.REF_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RESERIAL"+",CPROWNUM"+",RE_TABLE"+",REFLDNAM"+",CPROWORD"+",REKEYREC"+",RERIFREC"+",REMONINS"+",REMONMOD"+",REMONDEL"+",REBLKINS"+",REOBBINS"+",REFLTEXP"+",REFLDCOM"+",REMSGERR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_RESERIAL),'REF_DETT','RESERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'REF_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RE_TABLE),'REF_DETT','RE_TABLE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_REFLDNAM),'REF_DETT','REFLDNAM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'REF_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_REKEYREC),'REF_DETT','REKEYREC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RERIFREC),'REF_DETT','RERIFREC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_REMONINS),'REF_DETT','REMONINS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_REMONMOD),'REF_DETT','REMONMOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_REMONDEL),'REF_DETT','REMONDEL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_REBLKINS),'REF_DETT','REBLKINS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_REOBBINS),'REF_DETT','REOBBINS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_REFLTEXP),'REF_DETT','REFLTEXP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_REFLDCOM),'REF_DETT','REFLDCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_REMSGERR),'REF_DETT','REMSGERR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RESERIAL',this.w_RESERIAL,'CPROWNUM',this.w_CPROWNUM,'RE_TABLE',this.w_RE_TABLE,'REFLDNAM',this.w_REFLDNAM,'CPROWORD',this.w_CPROWORD,'REKEYREC',this.w_REKEYREC,'RERIFREC',this.w_RERIFREC,'REMONINS',this.w_REMONINS,'REMONMOD',this.w_REMONMOD,'REMONDEL',this.w_REMONDEL,'REBLKINS',this.w_REBLKINS,'REOBBINS',this.w_REOBBINS)
      insert into (i_cTable) (RESERIAL,CPROWNUM,RE_TABLE,REFLDNAM,CPROWORD,REKEYREC,RERIFREC,REMONINS,REMONMOD,REMONDEL,REBLKINS,REOBBINS,REFLTEXP,REFLDCOM,REMSGERR &i_ccchkf. );
         values (;
           this.w_RESERIAL;
           ,this.w_CPROWNUM;
           ,this.w_RE_TABLE;
           ,this.w_REFLDNAM;
           ,this.w_CPROWORD;
           ,this.w_REKEYREC;
           ,this.w_RERIFREC;
           ,this.w_REMONINS;
           ,this.w_REMONMOD;
           ,this.w_REMONDEL;
           ,this.w_REBLKINS;
           ,this.w_REOBBINS;
           ,this.w_REFLTEXP;
           ,this.w_REFLDCOM;
           ,this.w_REMSGERR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ENDSCAN
  endproc


  procedure Page_50
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Raggruppamenti tipi file
    CREATE CURSOR TEMPO (RASERIAL C(10), RADESCRI C (40), RAESTENS C(254), RATIPBST C(1))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(RASERIAL," "))
    * --- Inserisco...
    this.w_RASERIAL = TEMPO.RASERIAL
    this.w_RADESCRI = NVL(TEMPO.RADESCRI, SPACE(40))
    this.w_RAESTENS = NVL(TEMPO.RAESTENS, SPACE(254))
    this.w_RATIPBST = NVL(TEMPO.RATIPBST, SPACE(1))
    * --- Try
    local bErr_0537F488
    bErr_0537F488=bTrsErr
    this.Try_0537F488()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_053828A8
      bErr_053828A8=bTrsErr
      this.Try_053828A8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura in codice raggruppamento tipi file: %1%0Continuo?","", this.w_RASERIAL )
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_053828A8
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_0537F488
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_0537F488()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    i_Conn=i_TableProp[this.RAG_TIPI_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SERAG", "i_codazi,w_RASERIAL")
    * --- Insert into RAG_TIPI
    i_nConn=i_TableProp[this.RAG_TIPI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAG_TIPI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RAG_TIPI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RASERIAL"+",RADESCRI"+",RAESTENS"+",RATIPBST"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_RASERIAL),'RAG_TIPI','RASERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RADESCRI),'RAG_TIPI','RADESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RAESTENS),'RAG_TIPI','RAESTENS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RATIPBST),'RAG_TIPI','RATIPBST');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RASERIAL',this.w_RASERIAL,'RADESCRI',this.w_RADESCRI,'RAESTENS',this.w_RAESTENS,'RATIPBST',this.w_RATIPBST)
      insert into (i_cTable) (RASERIAL,RADESCRI,RAESTENS,RATIPBST &i_ccchkf. );
         values (;
           this.w_RASERIAL;
           ,this.w_RADESCRI;
           ,this.w_RAESTENS;
           ,this.w_RATIPBST;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco codice raggruppamento tipi file: %1",.t.,,,this.w_RASERIAL)
    return
  proc Try_053828A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into RAG_TIPI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RAG_TIPI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RAG_TIPI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.RAG_TIPI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"RADESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_RADESCRI),'RAG_TIPI','RADESCRI');
      +",RAESTENS ="+cp_NullLink(cp_ToStrODBC(this.w_RAESTENS),'RAG_TIPI','RAESTENS');
      +",RATIPBST ="+cp_NullLink(cp_ToStrODBC(this.w_RATIPBST),'RAG_TIPI','RATIPBST');
          +i_ccchkf ;
      +" where ";
          +"RASERIAL = "+cp_ToStrODBC(this.w_RASERIAL);
             )
    else
      update (i_cTable) set;
          RADESCRI = this.w_RADESCRI;
          ,RAESTENS = this.w_RAESTENS;
          ,RATIPBST = this.w_RATIPBST;
          &i_ccchkf. ;
       where;
          RASERIAL = this.w_RASERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Scrivo codice raggruppamento tipi file: %1",.t.,,,this.w_RASERIAL)
    return


  procedure Page_51
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modelli report wizard
    * --- Raggruppamenti tipi file
    CREATE CURSOR TEMPO (SERIAL C(10),PATMOD C(254), DESCRI C (60), IMGMOD C(254))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(PATMOD," "))
    * --- Inserisco...
    this.w_MRSERIAL = TEMPO.SERIAL
    this.w_MRPATMOD = TEMPO.PATMOD
    this.w_MRDESCRI = NVL(TEMPO.DESCRI, SPACE(60))
    this.w_MRIMGMOD = NVL(TEMPO.IMGMOD, SPACE(254))
    * --- Try
    local bErr_0537CF38
    bErr_0537CF38=bTrsErr
    this.Try_0537CF38()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_05392650
      bErr_05392650=bTrsErr
      this.Try_05392650()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura in modello report: %1%0Continuo?","", this.w_MRDESCRI )
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_05392650
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_0537CF38
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_0537CF38()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into WZMODREP
    i_nConn=i_TableProp[this.WZMODREP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.WZMODREP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.WZMODREP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MRSERIAL"+",MRPATMOD"+",MRDESCRI"+",MRIMGMOD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MRSERIAL),'WZMODREP','MRSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MRPATMOD),'WZMODREP','MRPATMOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MRDESCRI),'WZMODREP','MRDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MRIMGMOD),'WZMODREP','MRIMGMOD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MRSERIAL',this.w_MRSERIAL,'MRPATMOD',this.w_MRPATMOD,'MRDESCRI',this.w_MRDESCRI,'MRIMGMOD',this.w_MRIMGMOD)
      insert into (i_cTable) (MRSERIAL,MRPATMOD,MRDESCRI,MRIMGMOD &i_ccchkf. );
         values (;
           this.w_MRSERIAL;
           ,this.w_MRPATMOD;
           ,this.w_MRDESCRI;
           ,this.w_MRIMGMOD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserito modello report: %1",.t.,,,this.w_MRDESCRI)
    return
  proc Try_05392650()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into WZMODREP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.WZMODREP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.WZMODREP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.WZMODREP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MRPATMOD ="+cp_NullLink(cp_ToStrODBC(this.w_MRPATMOD),'WZMODREP','MRPATMOD');
      +",MRDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_MRDESCRI),'WZMODREP','MRDESCRI');
      +",MRIMGMOD ="+cp_NullLink(cp_ToStrODBC(this.w_MRIMGMOD),'WZMODREP','MRIMGMOD');
          +i_ccchkf ;
      +" where ";
          +"MRSERIAL = "+cp_ToStrODBC(this.w_MRSERIAL);
             )
    else
      update (i_cTable) set;
          MRPATMOD = this.w_MRPATMOD;
          ,MRDESCRI = this.w_MRDESCRI;
          ,MRIMGMOD = this.w_MRIMGMOD;
          &i_ccchkf. ;
       where;
          MRSERIAL = this.w_MRSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Aggiornato modello report: %1",.t.,,,this.w_MRDESCRI)
    return


  procedure Page_52
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Festivit�
    this.w_OLDCHIAVE = REPL("@", 3)
    CREATE CURSOR TEMPO (FECODICE C(3), FEDESCRI C (35), FEPREDEF C(1), FEDATFES T(14), FEDESFES C(35))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(FECODICE," "))
    * --- Inserisco...
    this.w_CHIAVE = NVL(TEMPO.FECODICE, space(3))
    this.w_FECODICE = NVL(TEMPO.FECODICE, space(3))
    this.w_FEDESCRI = NVL(TEMPO.FEDESCRI, space(35))
    this.w_FEPREDEF = NVL(TEMPO.FEPREDEF, "N")
    this.w_FEPREDEF = IIF(EMPTY(this.w_FEPREDEF), "N", this.w_FEPREDEF)
    this.w_FEDATFES = NVL(TTOD(TEMPO.FEDATFES), cp_CharToDate("  /  /  "))
    this.w_FEDESFES = NVL(TEMPO.FEDESFES, space(35))
    if this.w_CHIAVE<>this.w_OLDCHIAVE
      * --- Try
      local bErr_053A2460
      bErr_053A2460=bTrsErr
      this.Try_053A2460()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into FES_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.FES_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.FES_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.FES_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FEDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_FEDESCRI),'FES_MAST','FEDESCRI');
          +",FEPREDEF ="+cp_NullLink(cp_ToStrODBC(this.w_FEPREDEF),'FES_MAST','FEPREDEF');
              +i_ccchkf ;
          +" where ";
              +"FECODICE = "+cp_ToStrODBC(this.w_FECODICE);
                 )
        else
          update (i_cTable) set;
              FEDESCRI = this.w_FEDESCRI;
              ,FEPREDEF = this.w_FEPREDEF;
              &i_ccchkf. ;
           where;
              FECODICE = this.w_FECODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        ah_msg("Scrivo codice festivit�: %1",.t.,,,this.w_FECODICE)
      endif
      bTrsErr=bTrsErr or bErr_053A2460
      * --- End
      this.w_OLDCHIAVE = this.w_CHIAVE
    endif
    if NOT EMPTY(this.w_FEDATFES)
      * --- Try
      local bErr_0538F440
      bErr_0538F440=bTrsErr
      this.Try_0538F440()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into FES_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.FES_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.FES_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.FES_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"FEDESFES ="+cp_NullLink(cp_ToStrODBC(this.w_FEDESFES),'FES_DETT','FEDESFES');
              +i_ccchkf ;
          +" where ";
              +"FECODICE = "+cp_ToStrODBC(this.w_FECODICE);
              +" and FEDATFES = "+cp_ToStrODBC(this.w_FEDATFES);
                 )
        else
          update (i_cTable) set;
              FEDESFES = this.w_FEDESFES;
              &i_ccchkf. ;
           where;
              FECODICE = this.w_FECODICE;
              and FEDATFES = this.w_FEDATFES;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_0538F440
      * --- End
    endif
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_053A2460()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into FES_MAST
    i_nConn=i_TableProp[this.FES_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FES_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FES_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FECODICE"+",FEDESCRI"+",FEPREDEF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_FECODICE),'FES_MAST','FECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FEDESCRI),'FES_MAST','FEDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FEPREDEF),'FES_MAST','FEPREDEF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FECODICE',this.w_FECODICE,'FEDESCRI',this.w_FEDESCRI,'FEPREDEF',this.w_FEPREDEF)
      insert into (i_cTable) (FECODICE,FEDESCRI,FEPREDEF &i_ccchkf. );
         values (;
           this.w_FECODICE;
           ,this.w_FEDESCRI;
           ,this.w_FEPREDEF;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco codice festivit�: %1",.t.,,,this.w_FECODICE)
    return
  proc Try_0538F440()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into FES_DETT
    i_nConn=i_TableProp[this.FES_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FES_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FES_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FECODICE"+",FEDATFES"+",FEDESFES"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_FECODICE),'FES_DETT','FECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FEDATFES),'FES_DETT','FEDATFES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FEDESFES),'FES_DETT','FEDESFES');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FECODICE',this.w_FECODICE,'FEDATFES',this.w_FEDATFES,'FEDESFES',this.w_FEDESFES)
      insert into (i_cTable) (FECODICE,FEDATFES,FEDESFES &i_ccchkf. );
         values (;
           this.w_FECODICE;
           ,this.w_FEDATFES;
           ,this.w_FEDESFES;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_53
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Punti di accesso
    CREATE CURSOR TEMPO (PRCODICE C(5), PRDESCRI C (80), PRINDIRI C(254))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(PRCODICE," "))
    * --- Inserisco...
    this.w_PRCODICE = NVL(TEMPO.PRCODICE, SPACE(5))
    this.w_PRDESCRI = NVL(TEMPO.PRDESCRI, SPACE(80))
    this.w_PRINDIRI = NVL(TEMPO.PRINDIRI, SPACE(254))
    * --- Try
    local bErr_0539E470
    bErr_0539E470=bTrsErr
    this.Try_0539E470()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_053A0780
      bErr_053A0780=bTrsErr
      this.Try_053A0780()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura punti di accesso: %1%0Continuo?","", this.w_OPCODICE )
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_053A0780
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_0539E470
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_0539E470()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PUN_ACCE
    i_nConn=i_TableProp[this.PUN_ACCE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PUN_ACCE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PUN_ACCE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PRCODICE"+",PRDESCRI"+",PRINDIRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PRCODICE),'PUN_ACCE','PRCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRDESCRI),'PUN_ACCE','PRDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PRINDIRI),'PUN_ACCE','PRINDIRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PRCODICE',this.w_PRCODICE,'PRDESCRI',this.w_PRDESCRI,'PRINDIRI',this.w_PRINDIRI)
      insert into (i_cTable) (PRCODICE,PRDESCRI,PRINDIRI &i_ccchkf. );
         values (;
           this.w_PRCODICE;
           ,this.w_PRDESCRI;
           ,this.w_PRINDIRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco punti di accesso: %1",.t.,,,this.w_OPCODICE)
    return
  proc Try_053A0780()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PUN_ACCE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PUN_ACCE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PUN_ACCE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PUN_ACCE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PRDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_PRDESCRI),'PUN_ACCE','PRDESCRI');
      +",PRINDIRI ="+cp_NullLink(cp_ToStrODBC(this.w_PRINDIRI),'PUN_ACCE','PRINDIRI');
          +i_ccchkf ;
      +" where ";
          +"PRCODICE = "+cp_ToStrODBC(this.w_PRCODICE);
             )
    else
      update (i_cTable) set;
          PRDESCRI = this.w_PRDESCRI;
          ,PRINDIRI = this.w_PRINDIRI;
          &i_ccchkf. ;
       where;
          PRCODICE = this.w_PRCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Scrivo punti di accesso: %1",.t.,,,this.w_OPCODICE)
    return


  procedure Page_54
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestori locali
    CREATE CURSOR TEMPO (GLCODICE C(5), GLDESCRI C (80))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(GLCODICE," "))
    * --- Inserisco...
    this.w_GLCODICE = NVL(TEMPO.GLCODICE, SPACE(5))
    this.w_GLDESCRI = NVL(TEMPO.GLDESCRI, SPACE(80))
    * --- Try
    local bErr_053B8DD8
    bErr_053B8DD8=bTrsErr
    this.Try_053B8DD8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_053BB4A8
      bErr_053BB4A8=bTrsErr
      this.Try_053BB4A8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura gestori locali %1%0Continuo?","", this.w_GLCODICE )
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_053BB4A8
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_053B8DD8
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_053B8DD8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PWB_GLOC
    i_nConn=i_TableProp[this.PWB_GLOC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PWB_GLOC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PWB_GLOC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GLCODICE"+",GLDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GLCODICE),'PWB_GLOC','GLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GLDESCRI),'PWB_GLOC','GLDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GLCODICE',this.w_GLCODICE,'GLDESCRI',this.w_GLDESCRI)
      insert into (i_cTable) (GLCODICE,GLDESCRI &i_ccchkf. );
         values (;
           this.w_GLCODICE;
           ,this.w_GLDESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco gestore locale: %1",.t.,,,this.w_GLCODICE)
    return
  proc Try_053BB4A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PWB_GLOC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PWB_GLOC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PWB_GLOC_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PWB_GLOC_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"GLDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_GLDESCRI),'PWB_GLOC','GLDESCRI');
          +i_ccchkf ;
      +" where ";
          +"GLCODICE = "+cp_ToStrODBC(this.w_GLCODICE);
             )
    else
      update (i_cTable) set;
          GLDESCRI = this.w_GLDESCRI;
          &i_ccchkf. ;
       where;
          GLCODICE = this.w_GLCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Scrivo gestore locale: %1",.t.,,,this.w_GLCODICE)
    return


  procedure Page_55
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Automatismi per causali contabili
    CREATE CURSOR TEMPO (CCCODICE C(5),CCDESCRI C(35),CCFLPART C(1),CCFLANAL C(1),CCFLSALI C(1),CCFLSALF C(1),CCFLCOMP C(1),CCFLRIFE C(1),CCTIPDOC C(2),; 
 CCTIPREG C(1),CCNUMREG N(2),CCNUMDOC C(1),CCFLIVDF C(1),CCFLPDIF C(1),CCFLSTDA C(1),CCCALDOC C(1),CCTESDOC C(1),CCFLRITE C(1),CCDTINVA T(14),CCDTOBSO T(14),; 
 CCSERDOC C(10),CCFLPDOC C(1),CCSERPRO C(10),CCFLPPRO C(1),CCCFDAVE C(1),CCCONIVA C(15),CCFLBESE C(1),CCFLMOVC C(1),CCCAUMOV C(1),CCFLAUTR C(1),; 
 CCFLINSO C(1),CCMOVCES C(1),CCCAUCES C(5),CCGESRIT C(1),CCCAUSBF C(5),CCFLASSE C(1),CCDESSUP C(254),CCDESRIG C(254),CCPAGINS C(5),CCREGMAR C(1),; 
 CCGESCAS C(1),CCFLCOSE C(1),RNUM_PRI N(3), APTIPCON C(1), APCODCON C(15), APFLDAVE C(1), APFLAUTO C(1), APFLPART C(1), APCAURIG C(5),APCONBAN C(15),; 
 APCAUMOV C(5), RNUM_IVA N(3),AICODIVA C(5),AITIPCON C(1),AICONDET C(15),AICONIND C(15),AITIPREG C(1),AINUMREG N(2),AICONTRO C(15),AITIPCOP C(1))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    * --- Appoggiamo in un cursore proprio ciascun archivio da gestire
    *     CAUCON - Causale contabile
    *     RIGCON - Righe contabili
    *     RIGIVA - Righe iva
    SELECT DISTINCT CCCODICE,CCDESCRI,CCFLPART,CCFLANAL,CCFLSALI,CCFLSALF,CCFLCOMP,CCFLRIFE,CCTIPDOC,CCTIPREG,CCNUMREG,CCNUMDOC,CCFLIVDF,CCFLPDIF,CCFLSTDA,; 
 CCCALDOC,CCTESDOC,CCFLRITE,CCDTINVA,CCDTOBSO,CCSERDOC,CCFLPDOC,CCSERPRO,CCFLPPRO,CCCFDAVE,CCCONIVA,CCFLBESE,CCFLMOVC,CCCAUMOV,CCFLAUTR,; 
 CCFLINSO,CCMOVCES,CCCAUCES,CCGESRIT,CCCAUSBF,CCFLASSE,CCDESSUP,CCDESRIG,CCPAGINS,CCREGMAR,CCGESCAS,CCFLCOSE FROM TEMPO INTO CURSOR CAUCON
    SELECT DISTINCT CCCODICE, RNUM_PRI, APTIPCON, APCODCON, APFLDAVE, APFLAUTO, APFLPART, APCAURIG, APCONBAN, APCAUMOV FROM TEMPO INTO CURSOR RIGCON
    SELECT DISTINCT CCCODICE, RNUM_IVA, AICODIVA, AITIPCON, AICONDET, AICONIND, AITIPREG, AINUMREG, AICONTRO, AITIPCOP FROM TEMPO INTO CURSOR RIGIVA
    if USED("CAUCON")
      SELECT CAUCON
      GO TOP
      SCAN FOR NOT EMPTY(NVL(CCCODICE," "))
      this.w_CCCODICE = NVL(CAUCON.CCCODICE, SPACE(5))
      this.w_CCDESCRI = NVL(CAUCON.CCDESCRI, SPACE(5))
      this.w_CCFLPART = NVL(CAUCON.CCFLPART, SPACE(1))
      this.w_CCFLANAL = NVL(CAUCON.CCFLANAL, SPACE(1))
      this.w_CCFLSALI = NVL(CAUCON.CCFLSALI, SPACE(1))
      this.w_CCFLSALF = NVL(CAUCON.CCFLSALF, SPACE(1))
      this.w_CCFLCOMP = NVL(CAUCON.CCFLCOMP, SPACE(1))
      this.w_CCFLRIFE = NVL(CAUCON.CCFLRIFE, SPACE(1))
      this.w_CCTIPDOC = NVL(CAUCON.CCTIPDOC, SPACE(2))
      this.w_CCTIPREG = NVL(CAUCON.CCTIPREG, SPACE(1))
      this.w_CCNUMREG = NVL(CAUCON.CCNUMREG, 0)
      this.w_CCNUMDOC = NVL(CAUCON.CCNUMDOC, SPACE(1))
      this.w_CCFLIVDF = NVL(CAUCON.CCFLIVDF, SPACE(1))
      this.w_CCFLPDIF = NVL(CAUCON.CCFLPDIF, SPACE(1))
      this.w_CCFLSTDA = NVL(CAUCON.CCFLSTDA, SPACE(1))
      this.w_CCCALDOC = NVL(CAUCON.CCCALDOC, SPACE(1))
      this.w_CCTESDOC = NVL(CAUCON.CCTESDOC, SPACE(1))
      this.w_CCFLRITE = NVL(CAUCON.CCFLRITE, SPACE(1))
      this.w_CCDTINVA = NVL(CAUCON.CCDTINVA, cp_CharToDate("  /  /  "))
      this.w_CCDTOBSO = NVL(CAUCON.CCDTOBSO, cp_CharToDate("  /  /  "))
      this.w_CCSERDOC = NVL(CAUCON.CCSERDOC, SPACE(10))
      this.w_CCFLPDOC = NVL(CAUCON.CCFLPDOC, SPACE(1))
      this.w_CCSERPRO = NVL(CAUCON.CCSERPRO, SPACE(10))
      this.w_CCFLPPRO = NVL(CAUCON.CCFLPPRO, SPACE(1))
      this.w_CCCFDAVE = NVL(CAUCON.CCCFDAVE, SPACE(1))
      this.w_CCCONIVA = NVL(CAUCON.CCCONIVA, SPACE(15))
      this.w_CCFLBESE = NVL(CAUCON.CCFLBESE, SPACE(1))
      this.w_CCFLMOVC = NVL(CAUCON.CCFLMOVC, SPACE(1))
      this.w_CCCAUMOV = NVL(CAUCON.CCCAUMOV, SPACE(5))
      this.w_CCFLAUTR = NVL(CAUCON.CCFLAUTR, SPACE(1))
      this.w_CCFLINSO = NVL(CAUCON.CCFLINSO, SPACE(1))
      this.w_CCMOVCES = NVL(CAUCON.CCMOVCES, SPACE(1))
      this.w_CCCAUCES = NVL(CAUCON.CCCAUCES, SPACE(5))
      this.w_CCGESRIT = NVL(CAUCON.CCGESRIT, SPACE(1))
      this.w_CCCAUSBF = NVL(CAUCON.CCCAUSBF, SPACE(5))
      this.w_CCFLASSE = NVL(CAUCON.CCFLASSE, SPACE(1))
      this.w_CCDESSUP = NVL(CAUCON.CCDESSUP, SPACE(254))
      this.w_CCDESRIG = NVL(CAUCON.CCDESRIG, SPACE(254))
      this.w_CCPAGINS = NVL(CAUCON.CCPAGINS, SPACE(5))
      this.w_CCREGMAR = NVL(CAUCON.CCREGMAR, SPACE(1))
      this.w_CCGESCAS = NVL(CAUCON.CCGESCAS, SPACE(1))
      this.w_CCFLCOSE = NVL(CAUCON.CCFLCOSE, SPACE(1))
      * --- Preventiva cancellazione di tutti i dettagli
      * --- Try
      local bErr_05421BC8
      bErr_05421BC8=bTrsErr
      this.Try_05421BC8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_05421BC8
      * --- End
      * --- Try
      local bErr_05412268
      bErr_05412268=bTrsErr
      this.Try_05412268()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_05412268
      * --- End
      * --- Inserimento causale contabile
      * --- Try
      local bErr_05413018
      bErr_05413018=bTrsErr
      this.Try_05413018()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_054153E8
        bErr_054153E8=bTrsErr
        this.Try_054153E8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Fallito aggiornamento - � probabile che non sia rispettata un'integrit� referenziale
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_054153E8
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_05413018
      * --- End
      SELECT CAUCON
      ENDSCAN
    endif
    USE IN SELECT ("CAUCON")
    if USED("RIGCON")
      * --- Righe contabili
      SELECT "RIGCON"
      GO TOP
      SCAN FOR NOT EMPTY(NVL(CCCODICE," ")) AND RNUM_PRI>0
      this.w_CCCODICE = NVL(RIGCON.CCCODICE, SPACE(5))
      this.w_CPROWNUM = NVL(RIGCON.RNUM_PRI,0)
      this.w_APTIPCON = NVL(RIGCON.APTIPCON, SPACE(1))
      this.w_APCODCON = NVL(RIGCON.APCODCON, SPACE(15))
      this.w_APFLDAVE = NVL(RIGCON.APFLDAVE, SPACE(1))
      this.w_APFLAUTO = NVL(RIGCON.APFLAUTO, SPACE(1))
      this.w_APFLPART = NVL(RIGCON.APFLPART, SPACE(1))
      this.w_APCAURIG = NVL(RIGCON.APCAURIG, SPACE(5))
      this.w_APCONBAN = NVL(RIGCON.APCONBAN, SPACE(15))
      this.w_APCAUMOV = NVL(RIGCON.APCAUMOV, SPACE(5))
      * --- Try
      local bErr_0541DA28
      bErr_0541DA28=bTrsErr
      this.Try_0541DA28()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0541DA28
      * --- End
      SELECT "RIGCON"
      ENDSCAN
    endif
    USE IN SELECT ("RIGCON")
    if USED("RIGIVA")
      * --- Righe iva
      SELECT "RIGIVA"
      GO TOP
      SCAN FOR NOT EMPTY(NVL(CCCODICE," ")) AND RNUM_IVA>0
      this.w_CCCODICE = NVL(RIGIVA.CCCODICE, SPACE(5))
      this.w_CPROWNUM = NVL(RIGIVA.RNUM_IVA,0)
      this.w_AICODIVA = NVL(RIGIVA.AICODIVA, SPACE(5))
      this.w_AITIPCON = NVL(RIGIVA.AITIPCON, SPACE(1))
      this.w_AICONDET = NVL(RIGIVA.AICONDET, SPACE(5))
      this.w_AICONIND = NVL(RIGIVA.AICONIND, SPACE(5))
      this.w_AITIPREG = NVL(RIGIVA.AITIPREG, SPACE(1))
      this.w_AINUMREG = NVL(RIGIVA.AINUMREG, 0)
      this.w_AICONTRO = NVL(RIGIVA.AICONTRO, SPACE(5))
      this.w_AITIPCOP = NVL(RIGIVA.AITIPCOP, SPACE(1))
      * --- Try
      local bErr_054037C0
      bErr_054037C0=bTrsErr
      this.Try_054037C0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_054037C0
      * --- End
      SELECT "RIGIVA"
      ENDSCAN
    endif
    USE IN SELECT ("RIGIVA")
  endproc
  proc Try_05421BC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from CAUPRI1
    i_nConn=i_TableProp[this.CAUPRI1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"APCODCAU = "+cp_ToStrODBC(this.w_CCCODICE);
             )
    else
      delete from (i_cTable) where;
            APCODCAU = this.w_CCCODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_05412268()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from CAUIVA1
    i_nConn=i_TableProp[this.CAUIVA1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"AICODCAU = "+cp_ToStrODBC(this.w_CCCODICE);
             )
    else
      delete from (i_cTable) where;
            AICODCAU = this.w_CCCODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_05413018()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAU_CONT
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAU_CONT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CCCODICE"+",CCDESCRI"+",CCFLPART"+",CCFLANAL"+",CCFLSALI"+",CCFLSALF"+",CCFLCOMP"+",CCFLRIFE"+",CCTIPDOC"+",CCTIPREG"+",CCNUMREG"+",CCNUMDOC"+",CCFLIVDF"+",CCFLPDIF"+",CCFLSTDA"+",CCCALDOC"+",CCTESDOC"+",CCFLRITE"+",CCDTINVA"+",CCDTOBSO"+",CCSERDOC"+",CCFLPDOC"+",CCSERPRO"+",CCFLPPRO"+",CCCFDAVE"+",CCCONIVA"+",CCFLBESE"+",CCFLMOVC"+",CCCAUMOV"+",CCFLAUTR"+",CCFLINSO"+",CCMOVCES"+",CCCAUCES"+",CCGESRIT"+",CCCAUSBF"+",CCFLASSE"+",CCDESSUP"+",CCDESRIG"+",CCPAGINS"+",CCREGMAR"+",CCGESCAS"+",CCFLCOSE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CCCODICE),'CAU_CONT','CCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESCRI),'CAU_CONT','CCDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLPART),'CAU_CONT','CCFLPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLANAL),'CAU_CONT','CCFLANAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLSALI),'CAU_CONT','CCFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLSALF),'CAU_CONT','CCFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCOMP),'CAU_CONT','CCFLCOMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLRIFE),'CAU_CONT','CCFLRIFE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCTIPDOC),'CAU_CONT','CCTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCTIPREG),'CAU_CONT','CCTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCNUMREG),'CAU_CONT','CCNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCNUMDOC),'CAU_CONT','CCNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLIVDF),'CAU_CONT','CCFLIVDF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLPDIF),'CAU_CONT','CCFLPDIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLSTDA),'CAU_CONT','CCFLSTDA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCCALDOC),'CAU_CONT','CCCALDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCTESDOC),'CAU_CONT','CCTESDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLRITE),'CAU_CONT','CCFLRITE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDTINVA),'CAU_CONT','CCDTINVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDTOBSO),'CAU_CONT','CCDTOBSO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCSERDOC),'CAU_CONT','CCSERDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLPDOC),'CAU_CONT','CCFLPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCSERPRO),'CAU_CONT','CCSERPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLPPRO),'CAU_CONT','CCFLPPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCCFDAVE),'CAU_CONT','CCCFDAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCCONIVA),'CAU_CONT','CCCONIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLBESE),'CAU_CONT','CCFLBESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLMOVC),'CAU_CONT','CCFLMOVC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCCAUMOV),'CAU_CONT','CCCAUMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLAUTR),'CAU_CONT','CCFLAUTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLINSO),'CAU_CONT','CCFLINSO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCMOVCES),'CAU_CONT','CCMOVCES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCCAUCES),'CAU_CONT','CCCAUCES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCGESRIT),'CAU_CONT','CCGESRIT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCCAUSBF),'CAU_CONT','CCCAUSBF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLASSE),'CAU_CONT','CCFLASSE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESSUP),'CAU_CONT','CCDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESRIG),'CAU_CONT','CCDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCPAGINS),'CAU_CONT','CCPAGINS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCREGMAR),'CAU_CONT','CCREGMAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCGESCAS),'CAU_CONT','CCGESCAS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCOSE),'CAU_CONT','CCFLCOSE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CCCODICE',this.w_CCCODICE,'CCDESCRI',this.w_CCDESCRI,'CCFLPART',this.w_CCFLPART,'CCFLANAL',this.w_CCFLANAL,'CCFLSALI',this.w_CCFLSALI,'CCFLSALF',this.w_CCFLSALF,'CCFLCOMP',this.w_CCFLCOMP,'CCFLRIFE',this.w_CCFLRIFE,'CCTIPDOC',this.w_CCTIPDOC,'CCTIPREG',this.w_CCTIPREG,'CCNUMREG',this.w_CCNUMREG,'CCNUMDOC',this.w_CCNUMDOC)
      insert into (i_cTable) (CCCODICE,CCDESCRI,CCFLPART,CCFLANAL,CCFLSALI,CCFLSALF,CCFLCOMP,CCFLRIFE,CCTIPDOC,CCTIPREG,CCNUMREG,CCNUMDOC,CCFLIVDF,CCFLPDIF,CCFLSTDA,CCCALDOC,CCTESDOC,CCFLRITE,CCDTINVA,CCDTOBSO,CCSERDOC,CCFLPDOC,CCSERPRO,CCFLPPRO,CCCFDAVE,CCCONIVA,CCFLBESE,CCFLMOVC,CCCAUMOV,CCFLAUTR,CCFLINSO,CCMOVCES,CCCAUCES,CCGESRIT,CCCAUSBF,CCFLASSE,CCDESSUP,CCDESRIG,CCPAGINS,CCREGMAR,CCGESCAS,CCFLCOSE &i_ccchkf. );
         values (;
           this.w_CCCODICE;
           ,this.w_CCDESCRI;
           ,this.w_CCFLPART;
           ,this.w_CCFLANAL;
           ,this.w_CCFLSALI;
           ,this.w_CCFLSALF;
           ,this.w_CCFLCOMP;
           ,this.w_CCFLRIFE;
           ,this.w_CCTIPDOC;
           ,this.w_CCTIPREG;
           ,this.w_CCNUMREG;
           ,this.w_CCNUMDOC;
           ,this.w_CCFLIVDF;
           ,this.w_CCFLPDIF;
           ,this.w_CCFLSTDA;
           ,this.w_CCCALDOC;
           ,this.w_CCTESDOC;
           ,this.w_CCFLRITE;
           ,this.w_CCDTINVA;
           ,this.w_CCDTOBSO;
           ,this.w_CCSERDOC;
           ,this.w_CCFLPDOC;
           ,this.w_CCSERPRO;
           ,this.w_CCFLPPRO;
           ,this.w_CCCFDAVE;
           ,this.w_CCCONIVA;
           ,this.w_CCFLBESE;
           ,this.w_CCFLMOVC;
           ,this.w_CCCAUMOV;
           ,this.w_CCFLAUTR;
           ,this.w_CCFLINSO;
           ,this.w_CCMOVCES;
           ,this.w_CCCAUCES;
           ,this.w_CCGESRIT;
           ,this.w_CCCAUSBF;
           ,this.w_CCFLASSE;
           ,this.w_CCDESSUP;
           ,this.w_CCDESRIG;
           ,this.w_CCPAGINS;
           ,this.w_CCREGMAR;
           ,this.w_CCGESCAS;
           ,this.w_CCFLCOSE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco causale: %1 ",.t.,,,this.w_CCCODICE)
    return
  proc Try_054153E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CAU_CONT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CAU_CONT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CCDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_CCDESCRI),'CAU_CONT','CCDESCRI');
      +",CCFLPART ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLPART),'CAU_CONT','CCFLPART');
      +",CCFLANAL ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLANAL),'CAU_CONT','CCFLANAL');
      +",CCFLSALI ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLSALI),'CAU_CONT','CCFLSALI');
      +",CCFLSALF ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLSALF),'CAU_CONT','CCFLSALF');
      +",CCFLCOMP ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLCOMP),'CAU_CONT','CCFLCOMP');
      +",CCFLRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLRIFE),'CAU_CONT','CCFLRIFE');
      +",CCTIPDOC ="+cp_NullLink(cp_ToStrODBC(this.w_CCTIPDOC),'CAU_CONT','CCTIPDOC');
      +",CCTIPREG ="+cp_NullLink(cp_ToStrODBC(this.w_CCTIPREG),'CAU_CONT','CCTIPREG');
      +",CCNUMREG ="+cp_NullLink(cp_ToStrODBC(this.w_CCNUMREG),'CAU_CONT','CCNUMREG');
      +",CCNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_CCNUMDOC),'CAU_CONT','CCNUMDOC');
      +",CCFLIVDF ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLIVDF),'CAU_CONT','CCFLIVDF');
      +",CCFLPDIF ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLPDIF),'CAU_CONT','CCFLPDIF');
      +",CCFLSTDA ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLSTDA),'CAU_CONT','CCFLSTDA');
      +",CCCALDOC ="+cp_NullLink(cp_ToStrODBC(this.w_CCCALDOC),'CAU_CONT','CCCALDOC');
      +",CCTESDOC ="+cp_NullLink(cp_ToStrODBC(this.w_CCTESDOC),'CAU_CONT','CCTESDOC');
      +",CCFLRITE ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLRITE),'CAU_CONT','CCFLRITE');
      +",CCDTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_CCDTINVA),'CAU_CONT','CCDTINVA');
      +",CCDTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_CCDTOBSO),'CAU_CONT','CCDTOBSO');
      +",CCSERDOC ="+cp_NullLink(cp_ToStrODBC(this.w_CCSERDOC),'CAU_CONT','CCSERDOC');
      +",CCFLPDOC ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLPDOC),'CAU_CONT','CCFLPDOC');
      +",CCSERPRO ="+cp_NullLink(cp_ToStrODBC(this.w_CCSERPRO),'CAU_CONT','CCSERPRO');
      +",CCFLPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLPPRO),'CAU_CONT','CCFLPPRO');
      +",CCCFDAVE ="+cp_NullLink(cp_ToStrODBC(this.w_CCCFDAVE),'CAU_CONT','CCCFDAVE');
      +",CCCONIVA ="+cp_NullLink(cp_ToStrODBC(this.w_CCCONIVA),'CAU_CONT','CCCONIVA');
      +",CCFLBESE ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLBESE),'CAU_CONT','CCFLBESE');
      +",CCFLMOVC ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLMOVC),'CAU_CONT','CCFLMOVC');
      +",CCCAUMOV ="+cp_NullLink(cp_ToStrODBC(this.w_CCCAUMOV),'CAU_CONT','CCCAUMOV');
      +",CCFLAUTR ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLAUTR),'CAU_CONT','CCFLAUTR');
      +",CCFLINSO ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLINSO),'CAU_CONT','CCFLINSO');
      +",CCMOVCES ="+cp_NullLink(cp_ToStrODBC(this.w_CCMOVCES),'CAU_CONT','CCMOVCES');
      +",CCCAUCES ="+cp_NullLink(cp_ToStrODBC(this.w_CCCAUCES),'CAU_CONT','CCCAUCES');
      +",CCGESRIT ="+cp_NullLink(cp_ToStrODBC(this.w_CCGESRIT),'CAU_CONT','CCGESRIT');
      +",CCCAUSBF ="+cp_NullLink(cp_ToStrODBC(this.w_CCCAUSBF),'CAU_CONT','CCCAUSBF');
      +",CCFLASSE ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLASSE),'CAU_CONT','CCFLASSE');
      +",CCDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_CCDESSUP),'CAU_CONT','CCDESSUP');
      +",CCDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_CCDESRIG),'CAU_CONT','CCDESRIG');
      +",CCPAGINS ="+cp_NullLink(cp_ToStrODBC(this.w_CCPAGINS),'CAU_CONT','CCPAGINS');
      +",CCREGMAR ="+cp_NullLink(cp_ToStrODBC(this.w_CCREGMAR),'CAU_CONT','CCREGMAR');
      +",CCGESCAS ="+cp_NullLink(cp_ToStrODBC(this.w_CCGESCAS),'CAU_CONT','CCGESCAS');
      +",CCFLCOSE ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLCOSE),'CAU_CONT','CCFLCOSE');
          +i_ccchkf ;
      +" where ";
          +"CCCODICE = "+cp_ToStrODBC(this.w_CCCODICE);
             )
    else
      update (i_cTable) set;
          CCDESCRI = this.w_CCDESCRI;
          ,CCFLPART = this.w_CCFLPART;
          ,CCFLANAL = this.w_CCFLANAL;
          ,CCFLSALI = this.w_CCFLSALI;
          ,CCFLSALF = this.w_CCFLSALF;
          ,CCFLCOMP = this.w_CCFLCOMP;
          ,CCFLRIFE = this.w_CCFLRIFE;
          ,CCTIPDOC = this.w_CCTIPDOC;
          ,CCTIPREG = this.w_CCTIPREG;
          ,CCNUMREG = this.w_CCNUMREG;
          ,CCNUMDOC = this.w_CCNUMDOC;
          ,CCFLIVDF = this.w_CCFLIVDF;
          ,CCFLPDIF = this.w_CCFLPDIF;
          ,CCFLSTDA = this.w_CCFLSTDA;
          ,CCCALDOC = this.w_CCCALDOC;
          ,CCTESDOC = this.w_CCTESDOC;
          ,CCFLRITE = this.w_CCFLRITE;
          ,CCDTINVA = this.w_CCDTINVA;
          ,CCDTOBSO = this.w_CCDTOBSO;
          ,CCSERDOC = this.w_CCSERDOC;
          ,CCFLPDOC = this.w_CCFLPDOC;
          ,CCSERPRO = this.w_CCSERPRO;
          ,CCFLPPRO = this.w_CCFLPPRO;
          ,CCCFDAVE = this.w_CCCFDAVE;
          ,CCCONIVA = this.w_CCCONIVA;
          ,CCFLBESE = this.w_CCFLBESE;
          ,CCFLMOVC = this.w_CCFLMOVC;
          ,CCCAUMOV = this.w_CCCAUMOV;
          ,CCFLAUTR = this.w_CCFLAUTR;
          ,CCFLINSO = this.w_CCFLINSO;
          ,CCMOVCES = this.w_CCMOVCES;
          ,CCCAUCES = this.w_CCCAUCES;
          ,CCGESRIT = this.w_CCGESRIT;
          ,CCCAUSBF = this.w_CCCAUSBF;
          ,CCFLASSE = this.w_CCFLASSE;
          ,CCDESSUP = this.w_CCDESSUP;
          ,CCDESRIG = this.w_CCDESRIG;
          ,CCPAGINS = this.w_CCPAGINS;
          ,CCREGMAR = this.w_CCREGMAR;
          ,CCGESCAS = this.w_CCGESCAS;
          ,CCFLCOSE = this.w_CCFLCOSE;
          &i_ccchkf. ;
       where;
          CCCODICE = this.w_CCCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Aggiorno causale: %1 ",.t.,,,this.w_CCCODICE)
    return
  proc Try_0541DA28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAUPRI1
    i_nConn=i_TableProp[this.CAUPRI1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUPRI1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAUPRI1_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"APCODCAU"+",CPROWNUM"+",APTIPCON"+",APCODCON"+",APFLDAVE"+",APFLAUTO"+",APFLPART"+",APCAURIG"+",APCONBAN"+",APCAUMOV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CCCODICE),'CAUPRI1','APCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'CAUPRI1','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_APTIPCON),'CAUPRI1','APTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_APCODCON),'CAUPRI1','APCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_APFLDAVE),'CAUPRI1','APFLDAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_APFLAUTO),'CAUPRI1','APFLAUTO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_APFLPART),'CAUPRI1','APFLPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_APCAURIG),'CAUPRI1','APCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_APCONBAN),'CAUPRI1','APCONBAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_APCAUMOV),'CAUPRI1','APCAUMOV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'APCODCAU',this.w_CCCODICE,'CPROWNUM',this.w_CPROWNUM,'APTIPCON',this.w_APTIPCON,'APCODCON',this.w_APCODCON,'APFLDAVE',this.w_APFLDAVE,'APFLAUTO',this.w_APFLAUTO,'APFLPART',this.w_APFLPART,'APCAURIG',this.w_APCAURIG,'APCONBAN',this.w_APCONBAN,'APCAUMOV',this.w_APCAUMOV)
      insert into (i_cTable) (APCODCAU,CPROWNUM,APTIPCON,APCODCON,APFLDAVE,APFLAUTO,APFLPART,APCAURIG,APCONBAN,APCAUMOV &i_ccchkf. );
         values (;
           this.w_CCCODICE;
           ,this.w_CPROWNUM;
           ,this.w_APTIPCON;
           ,this.w_APCODCON;
           ,this.w_APFLDAVE;
           ,this.w_APFLAUTO;
           ,this.w_APFLPART;
           ,this.w_APCAURIG;
           ,this.w_APCONBAN;
           ,this.w_APCAUMOV;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco riga contabile: %1 %2 %3",.t.,,,this.w_CCCODICE, this.w_APTIPCON, this.w_APCODCON)
    return
  proc Try_054037C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAUIVA1
    i_nConn=i_TableProp[this.CAUIVA1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAUIVA1_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AICODCAU"+",CPROWNUM"+",AICODIVA"+",AITIPCON"+",AICONDET"+",AICONIND"+",AITIPREG"+",AINUMREG"+",AICONTRO"+",AITIPCOP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CCCODICE),'CAUIVA1','AICODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'CAUIVA1','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AICODIVA),'CAUIVA1','AICODIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AITIPCON),'CAUIVA1','AITIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AICONDET),'CAUIVA1','AICONDET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AICONIND),'CAUIVA1','AICONIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AITIPREG),'CAUIVA1','AITIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AINUMREG),'CAUIVA1','AINUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AICONTRO),'CAUIVA1','AICONTRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AITIPCOP),'CAUIVA1','AITIPCOP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AICODCAU',this.w_CCCODICE,'CPROWNUM',this.w_CPROWNUM,'AICODIVA',this.w_AICODIVA,'AITIPCON',this.w_AITIPCON,'AICONDET',this.w_AICONDET,'AICONIND',this.w_AICONIND,'AITIPREG',this.w_AITIPREG,'AINUMREG',this.w_AINUMREG,'AICONTRO',this.w_AICONTRO,'AITIPCOP',this.w_AITIPCOP)
      insert into (i_cTable) (AICODCAU,CPROWNUM,AICODIVA,AITIPCON,AICONDET,AICONIND,AITIPREG,AINUMREG,AICONTRO,AITIPCOP &i_ccchkf. );
         values (;
           this.w_CCCODICE;
           ,this.w_CPROWNUM;
           ,this.w_AICODIVA;
           ,this.w_AITIPCON;
           ,this.w_AICONDET;
           ,this.w_AICONIND;
           ,this.w_AITIPREG;
           ,this.w_AINUMREG;
           ,this.w_AICONTRO;
           ,this.w_AITIPCOP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco riga iva: %1 %2 ",.t.,,,this.w_CCCODICE, this.w_AICODIVA)
    return


  procedure Page_56
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Causali Remote Banking
    * --- Read from CONTROPA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "COCAUINS"+;
        " from "+i_cTable+" CONTROPA where ";
            +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        COCAUINS;
        from (i_cTable) where;
            COCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAUINS = NVL(cp_ToDate(_read_.COCAUINS),cp_NullValue(_read_.COCAUINS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    CREATE CURSOR TEMPO (CODCAU C(5), DESCCAU C(60), TIPMOV C(3))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_CODCAUR = " "
    this.w_DESCCAU = " "
    this.w_CAUTESR = " "
    this.w_TIPMOVR = " "
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODCAU," "))
    * --- Inserimento . . .
    this.w_CODCAUR = nvl(TEMPO.CODCAU," ")
    this.w_DESCCAU = nvl(TEMPO.DESCCAU," ")
    this.w_TIPMOVR = nvl(TEMPO.TIPMOV,"   ")
    if this.w_CODCAUR = "42010"
      this.w_CAUTESR = NVL(this.w_CAUINS,space(5))
    else
      this.w_CAUTESR = space(5)
    endif
    ah_msg("Inserisco codice: %1",.t.,,,this.w_CODCAUR)
    do GSSO_BRI with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc


  procedure Page_57
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_VEFA="S"
      CREATE CURSOR "IMPCONF" (CPSERIAL C(10), CPDESCRI C(30), CPCODCAU C(5), CPFLPRED C(1), CPNOMCAM C(46), CPFLROTT C(1), CPFLORDI C(1) )
      APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
      GSVA_BCO(this,"IMPORT", "IMPCONF" )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_58
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Fasi tariffario
    CREATE CURSOR TEMPO (FTCODICE C(10), FTDESCRI C (60))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(FTCODICE," "))
    * --- Inserisco...
    this.w_FTCODICE = TEMPO.FTCODICE
    this.w_FTDESCRI = NVL(TEMPO.FTDESCRI, SPACE(60))
    * --- Try
    local bErr_054927E8
    bErr_054927E8=bTrsErr
    this.Try_054927E8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_0549E550
      bErr_0549E550=bTrsErr
      this.Try_0549E550()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura fase tariffario: %1%0Continuo?","", this.w_FTCODICE )
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_0549E550
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_054927E8
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_054927E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into FAS_TARI
    i_nConn=i_TableProp[this.FAS_TARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FAS_TARI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FAS_TARI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FTCODICE"+",FTDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_FTCODICE),'FAS_TARI','FTCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FTDESCRI),'FAS_TARI','FTDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FTCODICE',this.w_FTCODICE,'FTDESCRI',this.w_FTDESCRI)
      insert into (i_cTable) (FTCODICE,FTDESCRI &i_ccchkf. );
         values (;
           this.w_FTCODICE;
           ,this.w_FTDESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco fase tariffario: %1",.t.,,,this.w_FTCODICE)
    return
  proc Try_0549E550()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into FAS_TARI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.FAS_TARI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FAS_TARI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.FAS_TARI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FTDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_FTDESCRI),'FAS_TARI','FTDESCRI');
          +i_ccchkf ;
      +" where ";
          +"FTCODICE = "+cp_ToStrODBC(this.w_FTCODICE);
             )
    else
      update (i_cTable) set;
          FTDESCRI = this.w_FTDESCRI;
          &i_ccchkf. ;
       where;
          FTCODICE = this.w_FTCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Scrivo fase tariffario: %1",.t.,,,this.w_FTCODICE)
    return


  procedure Page_59
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica DESCRIZIONE PROGRESSIVI
    CREATE CURSOR TEMPO (CODICE C(20), DESCR C(40), NOMETAB C(20), NOMECAMPO C(20), DIPENDENZE C(50), COMMENTO C(250))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_CODICE = SPACE(20)
    this.w_DESCR = SPACE(30)
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODICE," "))
    * --- Inserisco...
    this.w_CODICE = "prog\"+ALLTRIM(TEMPO.CODICE)+"\%"
    this.w_CODPROGR = NVL(TEMPO.CODICE," ")
    this.w_DESCR = NVL(TEMPO.DESCR," ")
    this.w_NOMETAB = NVL(TEMPO.NOMETAB," ")
    this.w_NOMECAMPO = NVL(TEMPO.NOMECAMPO," ")
    this.w_DIPENDENZE = NVL(TEMPO.DIPENDENZE," ")
    this.w_COMMENTO = NVL(TEMPO.COMMENTO," ")
    * --- Esgue Aggiornamento
    ah_msg("Aggiorno progressivo %1",.t.,,,this.w_CODICE+chr(32)+this.w_DESCR )
    * --- Try
    local bErr_054ABB40
    bErr_054ABB40=bTrsErr
    this.Try_054ABB40()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_0549CC90
      bErr_0549CC90=bTrsErr
      this.Try_0549CC90()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_MESS = "Errore di scrittura in codice: %1%0Continuo?"
        if not AH_YESNO(this.w_MESS,,this.w_CODPROGR)
          * --- Raise
          i_Error="Import fallito"
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_0549CC90
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_054ABB40
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_054ABB40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TSDESPRO
    i_nConn=i_TableProp[this.TSDESPRO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TSDESPRO_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TSDESPRO_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DPCODICE"+",DPDESPRO"+",DPCOMTAB"+",DP_CAMPO"+",DPDEPEON"+",DP__NOTE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODPROGR),'TSDESPRO','DPCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCR),'TSDESPRO','DPDESPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOMETAB),'TSDESPRO','DPCOMTAB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOMECAMPO),'TSDESPRO','DP_CAMPO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DIPENDENZE),'TSDESPRO','DPDEPEON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMMENTO),'TSDESPRO','DP__NOTE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DPCODICE',this.w_CODPROGR,'DPDESPRO',this.w_DESCR,'DPCOMTAB',this.w_NOMETAB,'DP_CAMPO',this.w_NOMECAMPO,'DPDEPEON',this.w_DIPENDENZE,'DP__NOTE',this.w_COMMENTO)
      insert into (i_cTable) (DPCODICE,DPDESPRO,DPCOMTAB,DP_CAMPO,DPDEPEON,DP__NOTE &i_ccchkf. );
         values (;
           this.w_CODPROGR;
           ,this.w_DESCR;
           ,this.w_NOMETAB;
           ,this.w_NOMECAMPO;
           ,this.w_DIPENDENZE;
           ,this.w_COMMENTO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0549CC90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into TSDESPRO
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TSDESPRO_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TSDESPRO_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TSDESPRO_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DPDESPRO ="+cp_NullLink(cp_ToStrODBC(this.w_DESCR),'TSDESPRO','DPDESPRO');
      +",DPCOMTAB ="+cp_NullLink(cp_ToStrODBC(this.w_NOMETAB),'TSDESPRO','DPCOMTAB');
      +",DP_CAMPO ="+cp_NullLink(cp_ToStrODBC(this.w_NOMECAMPO),'TSDESPRO','DP_CAMPO');
      +",DPDEPEON ="+cp_NullLink(cp_ToStrODBC(this.w_DIPENDENZE),'TSDESPRO','DPDEPEON');
      +",DP__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_COMMENTO),'TSDESPRO','DP__NOTE');
          +i_ccchkf ;
      +" where ";
          +"DPCODICE = "+cp_ToStrODBC(this.w_CODPROGR);
             )
    else
      update (i_cTable) set;
          DPDESPRO = this.w_DESCR;
          ,DPCOMTAB = this.w_NOMETAB;
          ,DP_CAMPO = this.w_NOMECAMPO;
          ,DPDEPEON = this.w_DIPENDENZE;
          ,DP__NOTE = this.w_COMMENTO;
          &i_ccchkf. ;
       where;
          DPCODICE = this.w_CODPROGR;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_60
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Automatismi per causali cespiti
    * --- w_CCFLRIFE gi� definita a pag 55
    * --- w_CCDTINVA gi� definita a pag 55
    * --- w_CCDTOBSO gi� definita a pag 55
    * --- w_CPROWNUM e w_CPROWORD gi� definite in altre pagine
    * --- w_CPROWNUM e w_CPROWORD gi� definite in altre pagine
    CREATE CURSOR TEMPO (CCCODICE C(5),CCDESCRI C(40),CCFLRIFE C(1),CCFLRAGG C(1),CCFLPRIU C(1),CCFLGA01 C(1),CCFLGA02 C(1),CCFLGA03 C(1),CCFLONFI C(1),; 
 CCFLGA04 C(1),CCFLGA05 C(1),CCFLGA06 C(1),CCFLGA07 C(1),CCFLGA08 C(1),CCFLGA09 C(1),CCFLGA10 C(1),CCFLGA11 C(1),CCFLGA12 C(1),CCFLGA13 C(1),CCFLGA14 C(1),; 
 CCDATAGG C(1),CCFLCONT C(1),CCCAUCON C(5),CCDTINVA T(14),CCDTOBSO T(14),CCFLGA15 C(1),CCFLGA16 C(1),CCFLDADI C(1),CCFLGA17 C(1),CCFLGOPE C(1),CCRIVALU C(1),; 
 CCFLGA18 C(1),CCFLGA19 C(1),CCFLGA20 C(1),CCFLGA21 C(1),CCFLGA22 C(1),CCFLGA23 C(1),CCFLGA24 C(1),CCFLONFF C(1),CCFLPRIC C(1),; 
 ROWNUM_FO N(11), ROWORD_FO N(11), FODESCRI C(40), FORISULT C(3), FOFORMUL C(100), FOCONDIZ C(50),; 
 ROWNUM_MC N(11), ROWORD_MC N(11), MCCODICE C(3), MCCONDAR C(3), MCCONAVE C(3) )
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    * --- Appoggiamo in un cursore proprio ciascun archivio da gestire
    *     CAU_CESP - Causali cespiti
    *     FOR_CESP - Formule
    *     MCO_CESP - Modelli
    * --- ,CCFLGOPE
    SELECT DISTINCT CCCODICE, CCDESCRI,CCFLRIFE, CCFLRAGG, CCFLPRIU, CCFLGA01, CCFLGA02, CCFLGA03, CCFLONFI, ; 
 CCFLGA04, CCFLGA05, CCFLGA06, CCFLGA07, CCFLGA08, CCFLGA09, CCFLGA10, CCFLGA11, CCFLGA12, CCFLGA13, CCFLGA14, ; 
 CCDATAGG, CCFLCONT, CCCAUCON, CCDTINVA, CCDTOBSO, CCFLGA15, CCFLGA16, CCFLDADI, CCFLGA17, CCFLGOPE, CCRIVALU, ; 
 CCFLGA18, CCFLGA19, CCFLGA20, CCFLGA21, CCFLGA22, CCFLGA23, CCFLGA24, CCFLONFF, CCFLPRIC FROM TEMPO INTO CURSOR CAUCESP
    SELECT DISTINCT CCCODICE, ROWNUM_FO, ROWORD_FO, FODESCRI, FORISULT, FOFORMUL, FOCONDIZ FROM TEMPO INTO CURSOR FORCESP
    SELECT DISTINCT CCCODICE, ROWNUM_MC, ROWORD_MC, MCCODICE, MCCONDAR, MCCONAVE FROM TEMPO INTO CURSOR MCOCESP
    if USED("CAUCESP")
      * --- Causali cespiti
      SELECT "CAUCESP"
      GO TOP
      SCAN FOR NOT EMPTY(NVL(CCCODICE," "))
      this.w_CCCODICE = NVL(CAUCESP.CCCODICE, SPACE(5))
      this.w_CCDESCRI = NVL(CAUCESP.CCDESCRI, SPACE(40))
      this.w_CCFLRIFE = NVL(CAUCESP.CCFLRIFE, SPACE(1))
      this.w_CCFLRAGG = NVL(CAUCESP.CCFLRAGG, SPACE(1))
      this.w_CCFLPRIU = NVL(CAUCESP.CCFLPRIU, SPACE(1))
      this.w_CCFLGA01 = NVL(CAUCESP.CCFLGA01, SPACE(1))
      this.w_CCFLGA02 = NVL(CAUCESP.CCFLGA02, SPACE(1))
      this.w_CCFLGA03 = NVL(CAUCESP.CCFLGA03, SPACE(1))
      this.w_CCFLONFI = NVL(CAUCESP.CCFLONFI, SPACE(1))
      this.w_CCFLGA04 = NVL(CAUCESP.CCFLGA04, SPACE(1))
      this.w_CCFLGA05 = NVL(CAUCESP.CCFLGA05, SPACE(1))
      this.w_CCFLGA06 = NVL(CAUCESP.CCFLGA06, SPACE(1))
      this.w_CCFLGA07 = NVL(CAUCESP.CCFLGA07, SPACE(1))
      this.w_CCFLGA08 = NVL(CAUCESP.CCFLGA08, SPACE(1))
      this.w_CCFLGA09 = NVL(CAUCESP.CCFLGA09, SPACE(1))
      this.w_CCFLGA10 = NVL(CAUCESP.CCFLGA10, SPACE(1))
      this.w_CCFLGA11 = NVL(CAUCESP.CCFLGA11, SPACE(1))
      this.w_CCFLGA12 = NVL(CAUCESP.CCFLGA12, SPACE(1))
      this.w_CCFLGA13 = NVL(CAUCESP.CCFLGA13, SPACE(1))
      this.w_CCFLGA14 = NVL(CAUCESP.CCFLGA14, SPACE(1))
      this.w_CCDATAGG = NVL(CAUCESP.CCDATAGG, " ")
      this.w_CCFLCONT = NVL(CAUCESP.CCFLCONT, SPACE(1))
      this.w_CCCAUCON = NVL(CAUCESP.CCCAUCON, SPACE(5))
      this.w_CCDTINVA = NVL(CAUCESP.CCDTINVA, cp_CharToDate("  /  /  "))
      this.w_CCDTOBSO = NVL(CAUCESP.CCDTOBSO, cp_CharToDate("  /  /  "))
      this.w_CCFLGA15 = NVL(CAUCESP.CCFLGA15, SPACE(1))
      this.w_CCFLGA16 = NVL(CAUCESP.CCFLGA16, SPACE(1))
      this.w_CCFLGA17 = NVL(CAUCESP.CCFLGA17, SPACE(1))
      this.w_CCFLGOPE = NVL(CAUCESP.CCFLGOPE, SPACE(1))
      this.w_CCFLDADI = NVL(CAUCESP.CCFLDADI, SPACE(1))
      this.w_CCRIVALU = NVL(CAUCESP.CCRIVALU, SPACE(1))
      this.w_CCFLGA18 = NVL(CAUCESP.CCFLGA18, SPACE(1))
      this.w_CCFLGA19 = NVL(CAUCESP.CCFLGA19, SPACE(1))
      this.w_CCFLGA20 = NVL(CAUCESP.CCFLGA20, SPACE(1))
      this.w_CCFLGA21 = NVL(CAUCESP.CCFLGA21, SPACE(1))
      this.w_CCFLGA22 = NVL(CAUCESP.CCFLGA22, SPACE(1))
      this.w_CCFLGA23 = NVL(CAUCESP.CCFLGA23, SPACE(1))
      this.w_CCFLGA24 = NVL(CAUCESP.CCFLGA24, SPACE(1))
      this.w_CCFLONFF = NVL(CAUCESP.CCFLONFF, SPACE(1))
      this.w_CCFLPRIC = NVL(CAUCESP.CCFLPRIC, SPACE(1))
      * --- Preventiva cancellazione di tutti i dettagli
      * --- Try
      local bErr_054CCFF8
      bErr_054CCFF8=bTrsErr
      this.Try_054CCFF8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_054CCFF8
      * --- End
      * --- Try
      local bErr_054CD688
      bErr_054CD688=bTrsErr
      this.Try_054CD688()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_054CD688
      * --- End
      * --- Inserimento causale cespite
      * --- Try
      local bErr_054CDFB8
      bErr_054CDFB8=bTrsErr
      this.Try_054CDFB8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Se mancava la causale contabile l'inserimento � fallito ma il record potrebbe non essere presente: riprovo inserendo senza CAU_CONT
        * --- Try
        local bErr_054D0E68
        bErr_054D0E68=bTrsErr
        this.Try_054D0E68()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Try
          local bErr_054E2018
          bErr_054E2018=bTrsErr
          this.Try_054E2018()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Fallito aggiornamento - � probabile che non sia rispettata un'integrit� referenziale
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_054E2018
          * --- End
        endif
        bTrsErr=bTrsErr or bErr_054D0E68
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_054CDFB8
      * --- End
      SELECT "CAUCESP"
      ENDSCAN
    endif
    USE IN SELECT ("CAUCESP")
    if USED("FORCESP")
      * --- Formule
      SELECT "FORCESP"
      GO TOP
      SCAN FOR NOT EMPTY(NVL(CCCODICE," ")) AND ROWNUM_FO>0
      this.w_CCCODICE = NVL(FORCESP.CCCODICE, SPACE(5))
      this.w_CPROWNUM = NVL(FORCESP.ROWNUM_FO,0)
      this.w_CPROWORD = NVL(FORCESP.ROWORD_FO,0)
      this.w_FODESCRI = NVL(FORCESP.FODESCRI,SPACE(40))
      this.w_FORISULT = NVL(FORCESP.FORISULT,SPACE(3))
      this.w_FOFORMUL = NVL(FORCESP.FOFORMUL,SPACE(100))
      this.w_FOCONDIZ = NVL(FORCESP.FOCONDIZ,SPACE(50))
      * --- Try
      local bErr_054C84F8
      bErr_054C84F8=bTrsErr
      this.Try_054C84F8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_054C84F8
      * --- End
      SELECT "FORCESP"
      ENDSCAN
    endif
    USE IN SELECT ("FORCESP")
    if USED("MCOCESP")
      * --- Modelli
      SELECT "MCOCESP"
      GO TOP
      SCAN FOR NOT EMPTY(NVL(CCCODICE," ")) AND ROWNUM_MC>0
      this.w_CCCODICE = NVL(MCOCESP.CCCODICE, SPACE(5))
      this.w_CPROWNUM = NVL(MCOCESP.ROWNUM_MC,0)
      this.w_CPROWORD = NVL(MCOCESP.ROWORD_MC,0)
      this.w_MCCODICE = NVL(MCOCESP.MCCODICE, SPACE(3))
      this.w_MCCONDAR = NVL(MCOCESP.MCCONDAR, SPACE(3))
      this.w_MCCONAVE = NVL(MCOCESP.MCCONAVE, SPACE(3))
      * --- Try
      local bErr_054B7858
      bErr_054B7858=bTrsErr
      this.Try_054B7858()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_054B7858
      * --- End
      SELECT "MCOCESP"
      ENDSCAN
    endif
    USE IN SELECT ("MCOCESP")
  endproc
  proc Try_054CCFF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from FOR_CESP
    i_nConn=i_TableProp[this.FOR_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FOR_CESP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"FOCODCAU = "+cp_ToStrODBC(this.w_CCCODICE);
             )
    else
      delete from (i_cTable) where;
            FOCODCAU = this.w_CCCODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_054CD688()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from MCO_CESP
    i_nConn=i_TableProp[this.MCO_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MCO_CESP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MCCODCAU = "+cp_ToStrODBC(this.w_CCCODICE);
             )
    else
      delete from (i_cTable) where;
            MCCODCAU = this.w_CCCODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_054CDFB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAU_CESP
    i_nConn=i_TableProp[this.CAU_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CESP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAU_CESP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CCCODICE"+",CCDESCRI"+",CCFLRIFE"+",CCFLRAGG"+",CCFLPRIU"+",CCFLGA01"+",CCFLGA02"+",CCFLGA03"+",CCFLONFI"+",CCFLGA04"+",CCFLGA05"+",CCFLGA06"+",CCFLGA07"+",CCFLGA08"+",CCFLGA09"+",CCFLGA10"+",CCFLGA11"+",CCFLGA12"+",CCFLGA13"+",CCFLGA14"+",CCDATAGG"+",CCFLCONT"+",CCCAUCON"+",CCDTINVA"+",CCDTOBSO"+",CCFLGA15"+",CCFLGA16"+",CCFLGA17"+",CCFLDADI"+",CCFLGOPE"+",CCRIVALU"+",CCFLGA18"+",CCFLGA19"+",CCFLGA20"+",CCFLGA21"+",CCFLGA22"+",CCFLGA23"+",CCFLGA24"+",CCFLONFF"+",CCFLPRIC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CCCODICE),'CAU_CESP','CCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESCRI),'CAU_CESP','CCDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLRIFE),'CAU_CESP','CCFLRIFE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLRAGG),'CAU_CESP','CCFLRAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLPRIU),'CAU_CESP','CCFLPRIU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA01),'CAU_CESP','CCFLGA01');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA02),'CAU_CESP','CCFLGA02');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA03),'CAU_CESP','CCFLGA03');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLONFI),'CAU_CESP','CCFLONFI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA04),'CAU_CESP','CCFLGA04');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA05),'CAU_CESP','CCFLGA05');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA06),'CAU_CESP','CCFLGA06');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA07),'CAU_CESP','CCFLGA07');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA08),'CAU_CESP','CCFLGA08');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA09),'CAU_CESP','CCFLGA09');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA10),'CAU_CESP','CCFLGA10');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA11),'CAU_CESP','CCFLGA11');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA12),'CAU_CESP','CCFLGA12');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA13),'CAU_CESP','CCFLGA13');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA14),'CAU_CESP','CCFLGA14');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDATAGG),'CAU_CESP','CCDATAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCONT),'CAU_CESP','CCFLCONT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCCAUCON),'CAU_CESP','CCCAUCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDTINVA),'CAU_CESP','CCDTINVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDTOBSO),'CAU_CESP','CCDTOBSO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA15),'CAU_CESP','CCFLGA15');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA16),'CAU_CESP','CCFLGA16');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA17),'CAU_CESP','CCFLGA17');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLDADI),'CAU_CESP','CCFLDADI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGOPE),'CAU_CESP','CCFLGOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCRIVALU),'CAU_CESP','CCRIVALU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA18),'CAU_CESP','CCFLGA18');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA19),'CAU_CESP','CCFLGA19');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA20),'CAU_CESP','CCFLGA20');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA21),'CAU_CESP','CCFLGA21');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA22),'CAU_CESP','CCFLGA22');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA23),'CAU_CESP','CCFLGA23');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA24),'CAU_CESP','CCFLGA24');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLONFF),'CAU_CESP','CCFLONFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLPRIC),'CAU_CESP','CCFLPRIC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CCCODICE',this.w_CCCODICE,'CCDESCRI',this.w_CCDESCRI,'CCFLRIFE',this.w_CCFLRIFE,'CCFLRAGG',this.w_CCFLRAGG,'CCFLPRIU',this.w_CCFLPRIU,'CCFLGA01',this.w_CCFLGA01,'CCFLGA02',this.w_CCFLGA02,'CCFLGA03',this.w_CCFLGA03,'CCFLONFI',this.w_CCFLONFI,'CCFLGA04',this.w_CCFLGA04,'CCFLGA05',this.w_CCFLGA05,'CCFLGA06',this.w_CCFLGA06)
      insert into (i_cTable) (CCCODICE,CCDESCRI,CCFLRIFE,CCFLRAGG,CCFLPRIU,CCFLGA01,CCFLGA02,CCFLGA03,CCFLONFI,CCFLGA04,CCFLGA05,CCFLGA06,CCFLGA07,CCFLGA08,CCFLGA09,CCFLGA10,CCFLGA11,CCFLGA12,CCFLGA13,CCFLGA14,CCDATAGG,CCFLCONT,CCCAUCON,CCDTINVA,CCDTOBSO,CCFLGA15,CCFLGA16,CCFLGA17,CCFLDADI,CCFLGOPE,CCRIVALU,CCFLGA18,CCFLGA19,CCFLGA20,CCFLGA21,CCFLGA22,CCFLGA23,CCFLGA24,CCFLONFF,CCFLPRIC &i_ccchkf. );
         values (;
           this.w_CCCODICE;
           ,this.w_CCDESCRI;
           ,this.w_CCFLRIFE;
           ,this.w_CCFLRAGG;
           ,this.w_CCFLPRIU;
           ,this.w_CCFLGA01;
           ,this.w_CCFLGA02;
           ,this.w_CCFLGA03;
           ,this.w_CCFLONFI;
           ,this.w_CCFLGA04;
           ,this.w_CCFLGA05;
           ,this.w_CCFLGA06;
           ,this.w_CCFLGA07;
           ,this.w_CCFLGA08;
           ,this.w_CCFLGA09;
           ,this.w_CCFLGA10;
           ,this.w_CCFLGA11;
           ,this.w_CCFLGA12;
           ,this.w_CCFLGA13;
           ,this.w_CCFLGA14;
           ,this.w_CCDATAGG;
           ,this.w_CCFLCONT;
           ,this.w_CCCAUCON;
           ,this.w_CCDTINVA;
           ,this.w_CCDTOBSO;
           ,this.w_CCFLGA15;
           ,this.w_CCFLGA16;
           ,this.w_CCFLGA17;
           ,this.w_CCFLDADI;
           ,this.w_CCFLGOPE;
           ,this.w_CCRIVALU;
           ,this.w_CCFLGA18;
           ,this.w_CCFLGA19;
           ,this.w_CCFLGA20;
           ,this.w_CCFLGA21;
           ,this.w_CCFLGA22;
           ,this.w_CCFLGA23;
           ,this.w_CCFLGA24;
           ,this.w_CCFLONFF;
           ,this.w_CCFLPRIC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco causale: %1 ",.t.,,,this.w_CCCODICE)
    return
  proc Try_054D0E68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CAU_CESP
    i_nConn=i_TableProp[this.CAU_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CESP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAU_CESP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CCCODICE"+",CCDESCRI"+",CCFLRIFE"+",CCFLRAGG"+",CCFLPRIU"+",CCFLGA01"+",CCFLGA02"+",CCFLGA03"+",CCFLONFI"+",CCFLGA04"+",CCFLGA05"+",CCFLGA06"+",CCFLGA07"+",CCFLGA08"+",CCFLGA09"+",CCFLGA10"+",CCFLGA11"+",CCFLGA12"+",CCFLGA13"+",CCFLGA14"+",CCDATAGG"+",CCFLCONT"+",CCDTINVA"+",CCDTOBSO"+",CCFLGA15"+",CCFLGA16"+",CCFLGA17"+",CCFLDADI"+",CCFLGOPE"+",CCRIVALU"+",CCFLGA18"+",CCFLGA19"+",CCFLGA20"+",CCFLGA21"+",CCFLGA22"+",CCFLGA23"+",CCFLGA24"+",CCFLONFF"+",CCFLPRIC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CCCODICE),'CAU_CESP','CCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDESCRI),'CAU_CESP','CCDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLRIFE),'CAU_CESP','CCFLRIFE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLRAGG),'CAU_CESP','CCFLRAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLPRIU),'CAU_CESP','CCFLPRIU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA01),'CAU_CESP','CCFLGA01');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA02),'CAU_CESP','CCFLGA02');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA03),'CAU_CESP','CCFLGA03');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLONFI),'CAU_CESP','CCFLONFI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA04),'CAU_CESP','CCFLGA04');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA05),'CAU_CESP','CCFLGA05');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA06),'CAU_CESP','CCFLGA06');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA07),'CAU_CESP','CCFLGA07');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA08),'CAU_CESP','CCFLGA08');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA09),'CAU_CESP','CCFLGA09');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA10),'CAU_CESP','CCFLGA10');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA11),'CAU_CESP','CCFLGA11');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA12),'CAU_CESP','CCFLGA12');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA13),'CAU_CESP','CCFLGA13');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA14),'CAU_CESP','CCFLGA14');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDATAGG),'CAU_CESP','CCDATAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLCONT),'CAU_CESP','CCFLCONT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDTINVA),'CAU_CESP','CCDTINVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCDTOBSO),'CAU_CESP','CCDTOBSO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA15),'CAU_CESP','CCFLGA15');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA16),'CAU_CESP','CCFLGA16');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA17),'CAU_CESP','CCFLGA17');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLDADI),'CAU_CESP','CCFLDADI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGOPE),'CAU_CESP','CCFLGOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCRIVALU),'CAU_CESP','CCRIVALU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA18),'CAU_CESP','CCFLGA18');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA19),'CAU_CESP','CCFLGA19');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA20),'CAU_CESP','CCFLGA20');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA21),'CAU_CESP','CCFLGA21');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA22),'CAU_CESP','CCFLGA22');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA23),'CAU_CESP','CCFLGA23');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA24),'CAU_CESP','CCFLGA24');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLONFF),'CAU_CESP','CCFLONFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CCFLPRIC),'CAU_CESP','CCFLPRIC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CCCODICE',this.w_CCCODICE,'CCDESCRI',this.w_CCDESCRI,'CCFLRIFE',this.w_CCFLRIFE,'CCFLRAGG',this.w_CCFLRAGG,'CCFLPRIU',this.w_CCFLPRIU,'CCFLGA01',this.w_CCFLGA01,'CCFLGA02',this.w_CCFLGA02,'CCFLGA03',this.w_CCFLGA03,'CCFLONFI',this.w_CCFLONFI,'CCFLGA04',this.w_CCFLGA04,'CCFLGA05',this.w_CCFLGA05,'CCFLGA06',this.w_CCFLGA06)
      insert into (i_cTable) (CCCODICE,CCDESCRI,CCFLRIFE,CCFLRAGG,CCFLPRIU,CCFLGA01,CCFLGA02,CCFLGA03,CCFLONFI,CCFLGA04,CCFLGA05,CCFLGA06,CCFLGA07,CCFLGA08,CCFLGA09,CCFLGA10,CCFLGA11,CCFLGA12,CCFLGA13,CCFLGA14,CCDATAGG,CCFLCONT,CCDTINVA,CCDTOBSO,CCFLGA15,CCFLGA16,CCFLGA17,CCFLDADI,CCFLGOPE,CCRIVALU,CCFLGA18,CCFLGA19,CCFLGA20,CCFLGA21,CCFLGA22,CCFLGA23,CCFLGA24,CCFLONFF,CCFLPRIC &i_ccchkf. );
         values (;
           this.w_CCCODICE;
           ,this.w_CCDESCRI;
           ,this.w_CCFLRIFE;
           ,this.w_CCFLRAGG;
           ,this.w_CCFLPRIU;
           ,this.w_CCFLGA01;
           ,this.w_CCFLGA02;
           ,this.w_CCFLGA03;
           ,this.w_CCFLONFI;
           ,this.w_CCFLGA04;
           ,this.w_CCFLGA05;
           ,this.w_CCFLGA06;
           ,this.w_CCFLGA07;
           ,this.w_CCFLGA08;
           ,this.w_CCFLGA09;
           ,this.w_CCFLGA10;
           ,this.w_CCFLGA11;
           ,this.w_CCFLGA12;
           ,this.w_CCFLGA13;
           ,this.w_CCFLGA14;
           ,this.w_CCDATAGG;
           ,this.w_CCFLCONT;
           ,this.w_CCDTINVA;
           ,this.w_CCDTOBSO;
           ,this.w_CCFLGA15;
           ,this.w_CCFLGA16;
           ,this.w_CCFLGA17;
           ,this.w_CCFLDADI;
           ,this.w_CCFLGOPE;
           ,this.w_CCRIVALU;
           ,this.w_CCFLGA18;
           ,this.w_CCFLGA19;
           ,this.w_CCFLGA20;
           ,this.w_CCFLGA21;
           ,this.w_CCFLGA22;
           ,this.w_CCFLGA23;
           ,this.w_CCFLGA24;
           ,this.w_CCFLONFF;
           ,this.w_CCFLPRIC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco causale: %1 ",.t.,,,this.w_CCCODICE)
    return
  proc Try_054E2018()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CAU_CESP
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CAU_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CESP_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CAU_CESP_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CCDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_CCDESCRI),'CAU_CESP','CCDESCRI');
      +",CCFLRIFE ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLRIFE),'CAU_CESP','CCFLRIFE');
      +",CCFLRAGG ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLRAGG),'CAU_CESP','CCFLRAGG');
      +",CCFLPRIU ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLPRIU),'CAU_CESP','CCFLPRIU');
      +",CCFLGA01 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA01),'CAU_CESP','CCFLGA01');
      +",CCFLGA02 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA02),'CAU_CESP','CCFLGA02');
      +",CCFLGA03 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA03),'CAU_CESP','CCFLGA03');
      +",CCFLONFI ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLONFI),'CAU_CESP','CCFLONFI');
      +",CCFLGA04 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA04),'CAU_CESP','CCFLGA04');
      +",CCFLGA05 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA05),'CAU_CESP','CCFLGA05');
      +",CCFLGA06 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA06),'CAU_CESP','CCFLGA06');
      +",CCFLGA07 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA07),'CAU_CESP','CCFLGA07');
      +",CCFLGA08 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA08),'CAU_CESP','CCFLGA08');
      +",CCFLGA09 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA09),'CAU_CESP','CCFLGA09');
      +",CCFLGA10 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA10),'CAU_CESP','CCFLGA10');
      +",CCFLGA11 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA11),'CAU_CESP','CCFLGA11');
      +",CCFLGA12 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA12),'CAU_CESP','CCFLGA12');
      +",CCFLGA13 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA13),'CAU_CESP','CCFLGA13');
      +",CCFLGA14 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA14),'CAU_CESP','CCFLGA14');
      +",CCDATAGG ="+cp_NullLink(cp_ToStrODBC(this.w_CCDATAGG),'CAU_CESP','CCDATAGG');
      +",CCFLCONT ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLCONT),'CAU_CESP','CCFLCONT');
      +",CCCAUCON ="+cp_NullLink(cp_ToStrODBC(this.w_CCCAUCON),'CAU_CESP','CCCAUCON');
      +",CCDTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_CCDTINVA),'CAU_CESP','CCDTINVA');
      +",CCDTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_CCDTOBSO),'CAU_CESP','CCDTOBSO');
      +",CCFLGA15 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA15),'CAU_CESP','CCFLGA15');
      +",CCFLGA16 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA16),'CAU_CESP','CCFLGA16');
      +",CCFLGA17 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA17),'CAU_CESP','CCFLGA17');
      +",CCFLDADI ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLDADI),'CAU_CESP','CCFLDADI');
      +",CCFLGOPE ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGOPE),'CAU_CESP','CCFLGOPE');
      +",CCRIVALU ="+cp_NullLink(cp_ToStrODBC(this.w_CCRIVALU),'CAU_CESP','CCRIVALU');
      +",CCFLGA18 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA18),'CAU_CESP','CCFLGA18');
      +",CCFLGA19 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA19),'CAU_CESP','CCFLGA19');
      +",CCFLGA20 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA20),'CAU_CESP','CCFLGA20');
      +",CCFLGA21 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA21),'CAU_CESP','CCFLGA21');
      +",CCFLGA22 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA22),'CAU_CESP','CCFLGA22');
      +",CCFLGA23 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA23),'CAU_CESP','CCFLGA23');
      +",CCFLGA24 ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLGA24),'CAU_CESP','CCFLGA24');
      +",CCFLONFF ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLONFF),'CAU_CESP','CCFLONFF');
      +",CCFLPRIC ="+cp_NullLink(cp_ToStrODBC(this.w_CCFLPRIC),'CAU_CESP','CCFLPRIC');
          +i_ccchkf ;
      +" where ";
          +"CCCODICE = "+cp_ToStrODBC(this.w_CCCODICE);
             )
    else
      update (i_cTable) set;
          CCDESCRI = this.w_CCDESCRI;
          ,CCFLRIFE = this.w_CCFLRIFE;
          ,CCFLRAGG = this.w_CCFLRAGG;
          ,CCFLPRIU = this.w_CCFLPRIU;
          ,CCFLGA01 = this.w_CCFLGA01;
          ,CCFLGA02 = this.w_CCFLGA02;
          ,CCFLGA03 = this.w_CCFLGA03;
          ,CCFLONFI = this.w_CCFLONFI;
          ,CCFLGA04 = this.w_CCFLGA04;
          ,CCFLGA05 = this.w_CCFLGA05;
          ,CCFLGA06 = this.w_CCFLGA06;
          ,CCFLGA07 = this.w_CCFLGA07;
          ,CCFLGA08 = this.w_CCFLGA08;
          ,CCFLGA09 = this.w_CCFLGA09;
          ,CCFLGA10 = this.w_CCFLGA10;
          ,CCFLGA11 = this.w_CCFLGA11;
          ,CCFLGA12 = this.w_CCFLGA12;
          ,CCFLGA13 = this.w_CCFLGA13;
          ,CCFLGA14 = this.w_CCFLGA14;
          ,CCDATAGG = this.w_CCDATAGG;
          ,CCFLCONT = this.w_CCFLCONT;
          ,CCCAUCON = this.w_CCCAUCON;
          ,CCDTINVA = this.w_CCDTINVA;
          ,CCDTOBSO = this.w_CCDTOBSO;
          ,CCFLGA15 = this.w_CCFLGA15;
          ,CCFLGA16 = this.w_CCFLGA16;
          ,CCFLGA17 = this.w_CCFLGA17;
          ,CCFLDADI = this.w_CCFLDADI;
          ,CCFLGOPE = this.w_CCFLGOPE;
          ,CCRIVALU = this.w_CCRIVALU;
          ,CCFLGA18 = this.w_CCFLGA18;
          ,CCFLGA19 = this.w_CCFLGA19;
          ,CCFLGA20 = this.w_CCFLGA20;
          ,CCFLGA21 = this.w_CCFLGA21;
          ,CCFLGA22 = this.w_CCFLGA22;
          ,CCFLGA23 = this.w_CCFLGA23;
          ,CCFLGA24 = this.w_CCFLGA24;
          ,CCFLONFF = this.w_CCFLONFF;
          ,CCFLPRIC = this.w_CCFLPRIC;
          &i_ccchkf. ;
       where;
          CCCODICE = this.w_CCCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Aggiorno causale: %1 ",.t.,,,this.w_CCCODICE)
    return
  proc Try_054C84F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into FOR_CESP
    i_nConn=i_TableProp[this.FOR_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FOR_CESP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FOR_CESP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FOCODCAU"+",CPROWNUM"+",CPROWORD"+",FODESCRI"+",FORISULT"+",FOFORMUL"+",FOCONDIZ"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CCCODICE),'FOR_CESP','FOCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'FOR_CESP','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'FOR_CESP','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FODESCRI),'FOR_CESP','FODESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FORISULT),'FOR_CESP','FORISULT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FOFORMUL),'FOR_CESP','FOFORMUL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FOCONDIZ),'FOR_CESP','FOCONDIZ');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FOCODCAU',this.w_CCCODICE,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'FODESCRI',this.w_FODESCRI,'FORISULT',this.w_FORISULT,'FOFORMUL',this.w_FOFORMUL,'FOCONDIZ',this.w_FOCONDIZ)
      insert into (i_cTable) (FOCODCAU,CPROWNUM,CPROWORD,FODESCRI,FORISULT,FOFORMUL,FOCONDIZ &i_ccchkf. );
         values (;
           this.w_CCCODICE;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_FODESCRI;
           ,this.w_FORISULT;
           ,this.w_FOFORMUL;
           ,this.w_FOCONDIZ;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco riga contabile: %1 %2 %3",.t.,,,this.w_CCCODICE, this.w_APTIPCON, this.w_APCODCON)
    return
  proc Try_054B7858()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MCO_CESP
    i_nConn=i_TableProp[this.MCO_CESP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MCO_CESP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MCO_CESP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MCCODCAU"+",CPROWNUM"+",CPROWORD"+",MCCODICE"+",MCCONDAR"+",MCCONAVE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CCCODICE),'MCO_CESP','MCCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MCO_CESP','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'MCO_CESP','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCCODICE),'MCO_CESP','MCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCCONDAR),'MCO_CESP','MCCONDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MCCONAVE),'MCO_CESP','MCCONAVE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MCCODCAU',this.w_CCCODICE,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'MCCODICE',this.w_MCCODICE,'MCCONDAR',this.w_MCCONDAR,'MCCONAVE',this.w_MCCONAVE)
      insert into (i_cTable) (MCCODCAU,CPROWNUM,CPROWORD,MCCODICE,MCCONDAR,MCCONAVE &i_ccchkf. );
         values (;
           this.w_CCCODICE;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_MCCODICE;
           ,this.w_MCCONDAR;
           ,this.w_MCCONAVE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco riga iva: %1 %2 ",.t.,,,this.w_CCCODICE, this.w_AICODIVA)
    return


  procedure Page_61
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tipologie atto per busta telematica
    CREATE CURSOR TEMPO (BTCODICE C(5), BTDESCRI C (120), BT_SCHEMA C(90), BTTIPREC C(1))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_OK = ah_YesNo("Vuoi mantenere le descrizioni delle tipologie busta gi� esistenti?")
    * --- Gestione obsolescenza...
    if this.oParentObject.w_ABICABOBSO="S"
      * --- Imposto alla data di oggi la data osbolescenza dei Tipi atto che
      *     non hanno il campo valorizzato...
      * --- Write into BUS_TIPI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.BUS_TIPI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.BUS_TIPI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.BUS_TIPI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"BTDTOBSO ="+cp_NullLink(cp_ToStrODBC(i_DATSYS),'BUS_TIPI','BTDTOBSO');
            +i_ccchkf ;
        +" where ";
            +"BTDTOBSO is null ";
               )
      else
        update (i_cTable) set;
            BTDTOBSO = i_DATSYS;
            &i_ccchkf. ;
         where;
            BTDTOBSO is null;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(BTCODICE," "))
    * --- Inserisco...
    this.w_BTCODICE = TEMPO.BTCODICE
    this.w_BTDESCRI = NVL(TEMPO.BTDESCRI, SPACE(120))
    this.w_BT_SCHEMA = NVL(TEMPO.BT_SCHEMA, SPACE(90))
    this.w_BTTIPREC = NVL(TEMPO.BTTIPREC,SPACE(1))
    * --- Try
    local bErr_0551BDB8
    bErr_0551BDB8=bTrsErr
    this.Try_0551BDB8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_0551E2A8
      bErr_0551E2A8=bTrsErr
      this.Try_0551E2A8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore di scrittura tipologia atto busta telematica: %1%0Continuo?","", this.w_BTCODICE )
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_0551E2A8
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_0551BDB8
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_0551BDB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into BUS_TIPI
    i_nConn=i_TableProp[this.BUS_TIPI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BUS_TIPI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.BUS_TIPI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"BTCODICE"+",BTDESCRI"+",BT_SCHEMA"+",BTTIPREC"+",BTDTOBSO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_BTCODICE),'BUS_TIPI','BTCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BTDESCRI),'BUS_TIPI','BTDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BT_SCHEMA),'BUS_TIPI','BT_SCHEMA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BTTIPREC),'BUS_TIPI','BTTIPREC');
      +","+cp_NullLink(cp_ToStrODBC(Cp_CharTodate( "  -  -    " )),'BUS_TIPI','BTDTOBSO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'BTCODICE',this.w_BTCODICE,'BTDESCRI',this.w_BTDESCRI,'BT_SCHEMA',this.w_BT_SCHEMA,'BTTIPREC',this.w_BTTIPREC,'BTDTOBSO',Cp_CharTodate( "  -  -    " ))
      insert into (i_cTable) (BTCODICE,BTDESCRI,BT_SCHEMA,BTTIPREC,BTDTOBSO &i_ccchkf. );
         values (;
           this.w_BTCODICE;
           ,this.w_BTDESCRI;
           ,this.w_BT_SCHEMA;
           ,this.w_BTTIPREC;
           ,Cp_CharTodate( "  -  -    " );
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserisco tipologia atto busta telematica: %1",.t.,,,this.w_BTCODICE)
    return
  proc Try_0551E2A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if NOT this.w_OK
      * --- Non viene mantenuta la descrizione
      * --- Write into BUS_TIPI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.BUS_TIPI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.BUS_TIPI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.BUS_TIPI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"BTDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_BTDESCRI),'BUS_TIPI','BTDESCRI');
        +",BT_SCHEMA ="+cp_NullLink(cp_ToStrODBC(this.w_BT_SCHEMA),'BUS_TIPI','BT_SCHEMA');
        +",BTDTOBSO ="+cp_NullLink(cp_ToStrODBC(Cp_CharTodate( "  -  -    " )),'BUS_TIPI','BTDTOBSO');
            +i_ccchkf ;
        +" where ";
            +"BTCODICE = "+cp_ToStrODBC(this.w_BTCODICE);
            +" and BTTIPREC = "+cp_ToStrODBC(this.w_BTTIPREC);
               )
      else
        update (i_cTable) set;
            BTDESCRI = this.w_BTDESCRI;
            ,BT_SCHEMA = this.w_BT_SCHEMA;
            ,BTDTOBSO = Cp_CharTodate( "  -  -    " );
            &i_ccchkf. ;
         where;
            BTCODICE = this.w_BTCODICE;
            and BTTIPREC = this.w_BTTIPREC;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Viene mantenuta la descrizione
      * --- Write into BUS_TIPI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.BUS_TIPI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.BUS_TIPI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.BUS_TIPI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"BT_SCHEMA ="+cp_NullLink(cp_ToStrODBC(this.w_BT_SCHEMA),'BUS_TIPI','BT_SCHEMA');
        +",BTDTOBSO ="+cp_NullLink(cp_ToStrODBC(Cp_CharTodate( "  -  -    " )),'BUS_TIPI','BTDTOBSO');
            +i_ccchkf ;
        +" where ";
            +"BTCODICE = "+cp_ToStrODBC(this.w_BTCODICE);
            +" and BTTIPREC = "+cp_ToStrODBC(this.w_BTTIPREC);
               )
      else
        update (i_cTable) set;
            BT_SCHEMA = this.w_BT_SCHEMA;
            ,BTDTOBSO = Cp_CharTodate( "  -  -    " );
            &i_ccchkf. ;
         where;
            BTCODICE = this.w_BTCODICE;
            and BTTIPREC = this.w_BTTIPREC;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    ah_msg("Scrivo tipologia atto busta telematica: %1",.t.,,,this.w_BTCODICE)
    return


  procedure Page_62
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Modelli Gadget
    CREATE CURSOR TEMPO (SRCPRG C(50), DESCRI C(254), PREIMG C(254))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_SRCPRG = ""
    this.w_DESCRI = ""
    m.w_PREIMG = ""
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(SRCPRG," "))
    * --- Inserisco...
    this.w_SRCPRG = Alltrim(TEMPO.SRCPRG)
    this.w_DESCRI = Alltrim(NVL(TEMPO.DESCRI," "))
    this.w_DESCRI = Strtran(this.w_DESCRI, this.w_DATRASCO, '"')
    m.w_PREIMG = Alltrim(NVL(TEMPO.PREIMG," "))
    * --- Try
    local bErr_0552A300
    bErr_0552A300=bTrsErr
    this.Try_0552A300()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_0552CA90
      bErr_0552CA90=bTrsErr
      this.Try_0552CA90()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore durante l'aggiornamento del modello: '%1'%0Continuo?","",this.w_DESCRI)
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
          exit
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_0552CA90
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_0552A300
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_0552A300()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MOD_GADG
    i_nConn=i_TableProp[this.MOD_GADG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_GADG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOD_GADG_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MGSRCPRG"+",MGDESCRI"+",MGPREIMG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SRCPRG),'MOD_GADG','MGSRCPRG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'MOD_GADG','MGDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(m.w_PREIMG),'MOD_GADG','MGPREIMG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MGSRCPRG',this.w_SRCPRG,'MGDESCRI',this.w_DESCRI,'MGPREIMG',m.w_PREIMG)
      insert into (i_cTable) (MGSRCPRG,MGDESCRI,MGPREIMG &i_ccchkf. );
         values (;
           this.w_SRCPRG;
           ,this.w_DESCRI;
           ,m.w_PREIMG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserito modello '%1'",.t.,,,this.w_DESCRI)
    return
  proc Try_0552CA90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into MOD_GADG
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOD_GADG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOD_GADG_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOD_GADG_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MGDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'MOD_GADG','MGDESCRI');
      +",MGPREIMG ="+cp_NullLink(cp_ToStrODBC(m.w_PREIMG),'MOD_GADG','MGPREIMG');
          +i_ccchkf ;
      +" where ";
          +"MGSRCPRG = "+cp_ToStrODBC(this.w_SRCPRG);
             )
    else
      update (i_cTable) set;
          MGDESCRI = this.w_DESCRI;
          ,MGPREIMG = m.w_PREIMG;
          &i_ccchkf. ;
       where;
          MGSRCPRG = this.w_SRCPRG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Aggiornato modello '%1'",.t.,,,this.w_DESCRI)
    return


  procedure Page_63
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Impostazioni MyGadget
    CREATE CURSOR TEMPO (MGCODUTE N(4), MGCHKCOL C(1), MG_WIDTH N(4), MGPIANOV C(1), MGBCKCOL N(10), MGEFTRAN C(1), MGSALCNF C(1), ; 
 MG_START C(1), MG_TIMER C(1), MG_PINMG C(1), MGORDGRP C(100), MGDTHEME C(5), MGHIDREF C(1), MGCODGAD C(10), MGCODAZI C(5), MGCODGRP C(5))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_ROTTURA = -1
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR (NVL(MGCODUTE,0))>0
    * --- Inserisco...
    this.w_CODUTE = TEMPO.MGCODUTE
    if this.w_CODUTE<>this.w_ROTTURA
      this.w_ROTTURA = this.w_CODUTE
      this.w_CHKCOL = Nvl(TEMPO.MGCHKCOL,"S")
      this.w_WIDTH = Nvl(TEMPO.MG_WIDTH,300)
      this.w_PIANO = Alltrim(Nvl(TEMPO.MGPIANOV,"B"))
      this.w_COLOR = Nvl(TEMPO.MGBCKCOL,RGB(0,0,255))
      this.w_TRANSIZ = Alltrim(Nvl(TEMPO.MGEFTRAN,"X"))
      this.w_UTESAVE = Alltrim(Nvl(TEMPO.MGSALCNF,"S"))
      this.w_STARTUP = Alltrim(Nvl(TEMPO.MG_START,"S"))
      this.w_TIMER = Alltrim(Nvl(TEMPO.MG_TIMER," "))
      this.w_PIN = Alltrim(Nvl(TEMPO.MG_PINMG," "))
      this.w_ORDGRP = Alltrim(Nvl(TEMPO.MGORDGRP,""))
      this.w_DTHEME = Alltrim(Nvl(TEMPO.MGDTHEME,""))
      this.w_HIDREF = Alltrim(Nvl(TEMPO.MGHIDREF,"N"))
      * --- Inserimento testata
      * --- Try
      local bErr_0558F470
      bErr_0558F470=bTrsErr
      this.Try_0558F470()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_05591090
        bErr_05591090=bTrsErr
        this.Try_05591090()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          if not ah_YesNo("Errore durante l'aggiornamento delle impostazioni per l'utente: '%1'%0Continuo?","",this.w_CODUTE)
            * --- Raise
            i_Error=ah_Msgformat("Import fallito")
            return
            exit
          else
            * --- accept error
            bTrsErr=.f.
          endif
        endif
        bTrsErr=bTrsErr or bErr_05591090
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_0558F470
      * --- End
    endif
    this.w_GADGET = Alltrim(Nvl(TEMPO.MGCODGAD,""))
    this.w_CODAZI = Alltrim(Nvl(TEMPO.MGCODAZI,""))
    this.w_GRPCOD = Alltrim(Nvl(TEMPO.MGCODGRP,""))
    this.w_GRPCOD = Iif(Empty(this.w_GRPCOD), .Null., this.w_GRPCOD)
    if Not Empty(this.w_GADGET) And Not Empty(this.w_CODAZI)
      * --- Inserimento dettaglio
      * --- Try
      local bErr_055938E0
      bErr_055938E0=bTrsErr
      this.Try_055938E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_055959B0
        bErr_055959B0=bTrsErr
        this.Try_055959B0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          if not ah_YesNo("Errore durante l'aggiornamento della pagina di visualizzazione del Gadget: '%1' per l'utente %2%0Continuo?","",this.w_GADGET, this.w_CODUTE)
            * --- Raise
            i_Error=ah_Msgformat("Import fallito")
            return
            exit
          else
            * --- accept error
            bTrsErr=.f.
          endif
        endif
        bTrsErr=bTrsErr or bErr_055959B0
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_055938E0
      * --- End
    endif
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_0558F470()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MYG_MAST
    i_nConn=i_TableProp[this.MYG_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MYG_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MYG_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MGCODUTE"+",MGCHKCOL"+",MG_WIDTH"+",MGPIANOV"+",MGBCKCOL"+",MGEFTRAN"+",MGSALCNF"+",MG_START"+",MG_TIMER"+",MG_PINMG"+",MGORDGRP"+",MGDTHEME"+",MGHIDREF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODUTE),'MYG_MAST','MGCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CHKCOL),'MYG_MAST','MGCHKCOL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_WIDTH),'MYG_MAST','MG_WIDTH');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PIANO),'MYG_MAST','MGPIANOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COLOR),'MYG_MAST','MGBCKCOL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TRANSIZ),'MYG_MAST','MGEFTRAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTESAVE),'MYG_MAST','MGSALCNF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_STARTUP),'MYG_MAST','MG_START');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIMER),'MYG_MAST','MG_TIMER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PIN),'MYG_MAST','MG_PINMG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ORDGRP),'MYG_MAST','MGORDGRP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DTHEME),'MYG_MAST','MGDTHEME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_HIDREF),'MYG_MAST','MGHIDREF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MGCODUTE',this.w_CODUTE,'MGCHKCOL',this.w_CHKCOL,'MG_WIDTH',this.w_WIDTH,'MGPIANOV',this.w_PIANO,'MGBCKCOL',this.w_COLOR,'MGEFTRAN',this.w_TRANSIZ,'MGSALCNF',this.w_UTESAVE,'MG_START',this.w_STARTUP,'MG_TIMER',this.w_TIMER,'MG_PINMG',this.w_PIN,'MGORDGRP',this.w_ORDGRP,'MGDTHEME',this.w_DTHEME)
      insert into (i_cTable) (MGCODUTE,MGCHKCOL,MG_WIDTH,MGPIANOV,MGBCKCOL,MGEFTRAN,MGSALCNF,MG_START,MG_TIMER,MG_PINMG,MGORDGRP,MGDTHEME,MGHIDREF &i_ccchkf. );
         values (;
           this.w_CODUTE;
           ,this.w_CHKCOL;
           ,this.w_WIDTH;
           ,this.w_PIANO;
           ,this.w_COLOR;
           ,this.w_TRANSIZ;
           ,this.w_UTESAVE;
           ,this.w_STARTUP;
           ,this.w_TIMER;
           ,this.w_PIN;
           ,this.w_ORDGRP;
           ,this.w_DTHEME;
           ,this.w_HIDREF;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserite impostazioni MyGadget per utente '%1'",.t.,,,this.w_CODUTE)
    return
  proc Try_05591090()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into MYG_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MYG_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MYG_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MYG_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MGCHKCOL ="+cp_NullLink(cp_ToStrODBC(this.w_CHKCOL),'MYG_MAST','MGCHKCOL');
      +",MG_WIDTH ="+cp_NullLink(cp_ToStrODBC(this.w_WIDTH),'MYG_MAST','MG_WIDTH');
      +",MGPIANOV ="+cp_NullLink(cp_ToStrODBC(this.w_PIANO),'MYG_MAST','MGPIANOV');
      +",MGBCKCOL ="+cp_NullLink(cp_ToStrODBC(this.w_COLOR),'MYG_MAST','MGBCKCOL');
      +",MGEFTRAN ="+cp_NullLink(cp_ToStrODBC(this.w_TRANSIZ),'MYG_MAST','MGEFTRAN');
      +",MGSALCNF ="+cp_NullLink(cp_ToStrODBC(this.w_UTESAVE),'MYG_MAST','MGSALCNF');
      +",MG_START ="+cp_NullLink(cp_ToStrODBC(this.w_STARTUP),'MYG_MAST','MG_START');
      +",MG_TIMER ="+cp_NullLink(cp_ToStrODBC(this.w_TIMER),'MYG_MAST','MG_TIMER');
      +",MG_PINMG ="+cp_NullLink(cp_ToStrODBC(this.w_PIN),'MYG_MAST','MG_PINMG');
      +",MGORDGRP ="+cp_NullLink(cp_ToStrODBC(this.w_ORDGRP),'MYG_MAST','MGORDGRP');
      +",MGDTHEME ="+cp_NullLink(cp_ToStrODBC(this.w_DTHEME),'MYG_MAST','MGDTHEME');
      +",MGHIDREF ="+cp_NullLink(cp_ToStrODBC(this.w_HIDREF),'MYG_MAST','MGHIDREF');
          +i_ccchkf ;
      +" where ";
          +"MGCODUTE = "+cp_ToStrODBC(this.w_CODUTE);
             )
    else
      update (i_cTable) set;
          MGCHKCOL = this.w_CHKCOL;
          ,MG_WIDTH = this.w_WIDTH;
          ,MGPIANOV = this.w_PIANO;
          ,MGBCKCOL = this.w_COLOR;
          ,MGEFTRAN = this.w_TRANSIZ;
          ,MGSALCNF = this.w_UTESAVE;
          ,MG_START = this.w_STARTUP;
          ,MG_TIMER = this.w_TIMER;
          ,MG_PINMG = this.w_PIN;
          ,MGORDGRP = this.w_ORDGRP;
          ,MGDTHEME = this.w_DTHEME;
          ,MGHIDREF = this.w_HIDREF;
          &i_ccchkf. ;
       where;
          MGCODUTE = this.w_CODUTE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Aggiornate impostazioni MyGadget per utente '%1'",.t.,,,this.w_CODUTE)
    return
  proc Try_055938E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MYG_DETT
    i_nConn=i_TableProp[this.MYG_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MYG_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MYG_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MGCODUTE"+",MGCODGAD"+",MGCODAZI"+",MGCODGRP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODUTE),'MYG_DETT','MGCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GADGET),'MYG_DETT','MGCODGAD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODAZI),'MYG_DETT','MGCODAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GRPCOD),'MYG_DETT','MGCODGRP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MGCODUTE',this.w_CODUTE,'MGCODGAD',this.w_GADGET,'MGCODAZI',this.w_CODAZI,'MGCODGRP',this.w_GRPCOD)
      insert into (i_cTable) (MGCODUTE,MGCODGAD,MGCODAZI,MGCODGRP &i_ccchkf. );
         values (;
           this.w_CODUTE;
           ,this.w_GADGET;
           ,this.w_CODAZI;
           ,this.w_GRPCOD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserito Gadget per utente '%1'",.t.,,,this.w_CODUTE)
    return
  proc Try_055959B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into MYG_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MYG_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MYG_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MYG_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MGCODGRP ="+cp_NullLink(cp_ToStrODBC(this.w_GRPCOD),'MYG_DETT','MGCODGRP');
          +i_ccchkf ;
      +" where ";
          +"MGCODUTE = "+cp_ToStrODBC(this.w_CODUTE);
          +" and MGCODGAD = "+cp_ToStrODBC(this.w_GADGET);
          +" and MGCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      update (i_cTable) set;
          MGCODGRP = this.w_GRPCOD;
          &i_ccchkf. ;
       where;
          MGCODUTE = this.w_CODUTE;
          and MGCODGAD = this.w_GADGET;
          and MGCODAZI = this.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_64
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Gadget
    if this.oParentObject.w_GAUPDATE="N" And !ah_YesNo("I gadget da caricare verranno aggiunti a quelli attualmente esistenti"+Chr(13)+Chr(10)+"Continuare?")
      * --- Raise
      i_Error=ah_Msgformat("Import annullato")
      return
    endif
    this.w_Handle = FOpen(Alltrim(this.oParentObject.w_PATH))
    if this.w_Handle=-1
      * --- Raise
      i_Error=ah_Msgformat('Impossibile aprire il file "%1"%0Import annullato',this.oParentObject.w_PATH)
      return
    endif
    if Upper(FRead(this.w_Handle,10))=='{"VFPDATA"'
      cp_JsonParseCursor(FileToStr(this.oParentObject.w_PATH), "TEMPO")
    else
      CREATE CURSOR TEMPO (GACODICE C(10), GA__NOME C(50), GADESCRI C(254), GASRCPRG C(50), GAGRPCOD C(5),; 
 GACNDATT C(254), GACHKATT C(1), GAFORAZI C(1), GAUTEESC N(4), GA__PROP C(50), GAVALORE C(254), GACODUTE N(4), GACODAZI C(5), GA_RESET C(1))
      APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    endif
    FClose(this.w_Handle)
    this.w_CURSORE = SELECT()
    this.w_ROTTURA = "0000000000"
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR Not Empty(NVL(GACODICE,"")) AND Not Empty(NVL(GA__PROP,""))
    * --- Inserisco...
    this.w_CODICE = Alltrim(TEMPO.GACODICE)
    if this.w_CODICE<>this.w_ROTTURA
      this.w_ROTTURA = this.w_CODICE
      this.w_NOME = Alltrim(Nvl(TEMPO.GA__NOME,""))
      this.w_NOME = Strtran(this.w_NOME, this.w_DATRASCO, '"')
      this.w_DESCRI = Alltrim(Nvl(TEMPO.GADESCRI,""))
      this.w_DESCRI = Strtran(this.w_DESCRI, this.w_DATRASCO, '"')
      this.w_SRCPRG = Alltrim(Nvl(TEMPO.GASRCPRG,""))
      this.w_GRPCOD = Alltrim(Nvl(TEMPO.GAGRPCOD,""))
      this.w_GRPCOD = Iif(Empty(this.w_GRPCOD), .Null., this.w_GRPCOD)
      this.w_CNDATT = Alltrim(Nvl(TEMPO.GACNDATT,""))
      this.w_CNDATT = Strtran(this.w_CNDATT, this.w_DATRASCO, '"')
      this.w_CHKATT = Alltrim(Nvl(TEMPO.GACHKATT,"S"))
      this.w_FORAZI = Alltrim(Nvl(TEMPO.GAFORAZI,"N"))
      this.w_UTEESC = NVL(GAUTEESC,0)
      * --- Se attivo l'update cerco un gadget su cui far convergere questi dati
      this.w_GSERIAL = Space(10)
      if this.oParentObject.w_GAUPDATE="S"
        * --- Devo utilizzare una vqr per poter utilizzare una Nvl sul valore letto dal campo GAUTEESC
        * --- Select from QUERY\GSUT_GAE
        do vq_exec with 'QUERY\GSUT_GAE',this,'_Curs_QUERY_GSUT_GAE','',.f.,.t.
        if used('_Curs_QUERY_GSUT_GAE')
          select _Curs_QUERY_GSUT_GAE
          locate for 1=1
          do while not(eof())
          this.w_GSERIAL = _Curs_QUERY_GSUT_GAE.GACODICE
          * --- Mi � sufficiente leggere il primo codice
          exit
            select _Curs_QUERY_GSUT_GAE
            continue
          enddo
          use
        endif
      endif
      * --- Se il seriale � vuoto devo calcolarmene uno nuovo
      if Empty(this.w_GSERIAL)
        i_nConn=i_TableProp[this.GAD_MAST_IDX, 3] 
 cp_NextTableProg(this,i_nConn,"SEGADG","w_GSERIAL")
      endif
      * --- Inserimento testata
      * --- Try
      local bErr_055D6698
      bErr_055D6698=bTrsErr
      this.Try_055D6698()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if this.oParentObject.w_GAUPDATE="S"
          * --- accept error
          bTrsErr=.f.
          * --- Try
          local bErr_055CB448
          bErr_055CB448=bTrsErr
          this.Try_055CB448()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.w_ERRORE = .T.
          endif
          bTrsErr=bTrsErr or bErr_055CB448
          * --- End
        else
          this.w_ERRORE = .T.
        endif
        if this.w_ERRORE And not ah_YesNo("Errore durante l'inserimento del Gadget: %1%0Continuo?","",this.w_NOME)
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
          exit
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_055D6698
      * --- End
    endif
    * --- Inserimento dettaglio
    this.w_PROP = Alltrim(Nvl(TEMPO.GA__PROP,""))
    this.w_VALORE = Alltrim(Nvl(TEMPO.GAVALORE,""))
    this.w_VALORE = Strtran(this.w_VALORE, this.w_DATRASCO, '"')
    this.w_CODUTE = Nvl(TEMPO.GACODUTE,-1)
    this.w_CODAZI = Alltrim(Nvl(TEMPO.GACODAZI,"xxx"))
    this.w_RESET = Alltrim(Nvl(TEMPO.GA_RESET,"N"))
    * --- Try
    local bErr_055C05D0
    bErr_055C05D0=bTrsErr
    this.Try_055C05D0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if this.oParentObject.w_GAUPDATE="S"
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_055C4F50
        bErr_055C4F50=bTrsErr
        this.Try_055C4F50()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_ERRORE = .T.
        endif
        bTrsErr=bTrsErr or bErr_055C4F50
        * --- End
      else
        this.w_ERRORE = .T.
      endif
      if this.w_ERRORE And not ah_YesNo("Errore durante l'inserimento della propriet� %1 per il Gadget %2%0Continuo?","",this.w_PROP,this.w_NOME)
        * --- Raise
        i_Error=ah_Msgformat("Import fallito")
        return
        exit
      else
        * --- accept error
        bTrsErr=.f.
      endif
    endif
    bTrsErr=bTrsErr or bErr_055C05D0
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_055D6698()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_ERRORE = .F.
    * --- Insert into GAD_MAST
    i_nConn=i_TableProp[this.GAD_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GAD_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GACODICE"+",GA__NOME"+",GADESCRI"+",GASRCPRG"+",GACNDATT"+",GACHKATT"+",GAFORAZI"+",GAGRPCOD"+",GAUTEESC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GSERIAL),'GAD_MAST','GACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOME),'GAD_MAST','GA__NOME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'GAD_MAST','GADESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SRCPRG),'GAD_MAST','GASRCPRG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CNDATT),'GAD_MAST','GACNDATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CHKATT),'GAD_MAST','GACHKATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FORAZI),'GAD_MAST','GAFORAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GRPCOD),'GAD_MAST','GAGRPCOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTEESC),'GAD_MAST','GAUTEESC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GSERIAL,'GA__NOME',this.w_NOME,'GADESCRI',this.w_DESCRI,'GASRCPRG',this.w_SRCPRG,'GACNDATT',this.w_CNDATT,'GACHKATT',this.w_CHKATT,'GAFORAZI',this.w_FORAZI,'GAGRPCOD',this.w_GRPCOD,'GAUTEESC',this.w_UTEESC)
      insert into (i_cTable) (GACODICE,GA__NOME,GADESCRI,GASRCPRG,GACNDATT,GACHKATT,GAFORAZI,GAGRPCOD,GAUTEESC &i_ccchkf. );
         values (;
           this.w_GSERIAL;
           ,this.w_NOME;
           ,this.w_DESCRI;
           ,this.w_SRCPRG;
           ,this.w_CNDATT;
           ,this.w_CHKATT;
           ,this.w_FORAZI;
           ,this.w_GRPCOD;
           ,this.w_UTEESC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserimento Gadget '%1'",.t.,,,this.w_NOME)
    return
  proc Try_055CB448()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into GAD_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.GAD_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.GAD_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"GADESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'GAD_MAST','GADESCRI');
      +",GAGRPCOD ="+cp_NullLink(cp_ToStrODBC(this.w_GRPCOD),'GAD_MAST','GAGRPCOD');
      +",GACHKATT ="+cp_NullLink(cp_ToStrODBC(this.w_CHKATT),'GAD_MAST','GACHKATT');
      +",GACNDATT ="+cp_NullLink(cp_ToStrODBC(this.w_CNDATT),'GAD_MAST','GACNDATT');
          +i_ccchkf ;
      +" where ";
          +"GACODICE = "+cp_ToStrODBC(this.w_GSERIAL);
             )
    else
      update (i_cTable) set;
          GADESCRI = this.w_DESCRI;
          ,GAGRPCOD = this.w_GRPCOD;
          ,GACHKATT = this.w_CHKATT;
          ,GACNDATT = this.w_CNDATT;
          &i_ccchkf. ;
       where;
          GACODICE = this.w_GSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Aggiornamento Gadget '%1'",.t.,,,this.w_NOME)
    return
  proc Try_055C05D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_ERRORE = .F.
    * --- Insert into GAD_DETT
    i_nConn=i_TableProp[this.GAD_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GAD_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GACODICE"+",GA__PROP"+",GAVALORE"+",GACODUTE"+",GACODAZI"+",GA_RESET"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GSERIAL),'GAD_DETT','GACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PROP),'GAD_DETT','GA__PROP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALORE),'GAD_DETT','GAVALORE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODUTE),'GAD_DETT','GACODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODAZI),'GAD_DETT','GACODAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RESET),'GAD_DETT','GA_RESET');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GSERIAL,'GA__PROP',this.w_PROP,'GAVALORE',this.w_VALORE,'GACODUTE',this.w_CODUTE,'GACODAZI',this.w_CODAZI,'GA_RESET',this.w_RESET)
      insert into (i_cTable) (GACODICE,GA__PROP,GAVALORE,GACODUTE,GACODAZI,GA_RESET &i_ccchkf. );
         values (;
           this.w_GSERIAL;
           ,this.w_PROP;
           ,this.w_VALORE;
           ,this.w_CODUTE;
           ,this.w_CODAZI;
           ,this.w_RESET;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_055C4F50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into GAD_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.GAD_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.GAD_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"GAVALORE ="+cp_NullLink(cp_ToStrODBC(this.w_VALORE),'GAD_DETT','GAVALORE');
      +",GA_RESET ="+cp_NullLink(cp_ToStrODBC(this.w_RESET),'GAD_DETT','GA_RESET');
          +i_ccchkf ;
      +" where ";
          +"GACODICE = "+cp_ToStrODBC(this.w_GSERIAL);
          +" and GA__PROP = "+cp_ToStrODBC(this.w_PROP);
          +" and GACODUTE = "+cp_ToStrODBC(this.w_CODUTE);
          +" and GACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      update (i_cTable) set;
          GAVALORE = this.w_VALORE;
          ,GA_RESET = this.w_RESET;
          &i_ccchkf. ;
       where;
          GACODICE = this.w_GSERIAL;
          and GA__PROP = this.w_PROP;
          and GACODUTE = this.w_CODUTE;
          and GACODAZI = this.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_65
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Elaborazioni KPI
    CREATE CURSOR TEMPO (EKCODICE C(10), EKDESCRI C(100), EKTIPOUG C(1), EKUTEGRP N(5), EKCNDATT C(254), EKCHKATT C(1),; 
 EK_QUERY C(250), EKTIPRES C(1), EKTIPSIN C(3), EKBENCHM C(3), EKBENFIX N(18,5), EKBENCOD C(10), EKVISIBI C(1),; 
 EKWRTLOG C(1), EKFRQEXE N(10), EKNUMRES N(5), CPROWNUM N(5), CPROWORD N(5), EK_PARAM C(20), EKVALORE C(250))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_ROTTURA = "0000000000"
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR !(Empty(Nvl(EKCODICE,"")) Or Empty(Nvl(EK_QUERY,"")))
    * --- Inserisco...
    this.w_CODICE = Alltrim(TEMPO.EKCODICE)
    if NOT (this.w_CODICE==this.w_ROTTURA)
      this.w_ROTTURA = this.w_CODICE
      this.w_DESCRI = Alltrim(Nvl(TEMPO.EKDESCRI,""))
      this.w_DESCRI = Strtran(this.w_DESCRI, this.w_DATRASCO, '"')
      this.w_TIPOUG = Nvl(TEMPO.EKTIPOUG,"U")
      this.w_UTEGRP = Nvl(TEMPO.EKUTEGRP,0)
      this.w_CNDATT = Alltrim(Nvl(TEMPO.EKCNDATT,""))
      this.w_CNDATT = Strtran(this.w_CNDATT, this.w_DATRASCO, '"')
      this.w_CHKATT = Nvl(TEMPO.EKCHKATT,"S")
      this.w_QUERY = Alltrim(TEMPO.EK_QUERY)
      this.w_TIPRES = TEMPO.EKTIPRES
      this.w_TIPSIN = TEMPO.EKTIPSIN
      this.w_BENCHM = TEMPO.EKBENCHM
      this.w_BENFIX = Nvl(TEMPO.EKBENFIX,0)
      this.w_BENCOD = Nvl(TEMPO.EKBENCOD,"")
      this.w_VISIBI = TEMPO.EKVISIBI
      this.w_WRTLOG = Nvl(TEMPO.EKWRTLOG,"N")
      this.w_FRQEXE = Nvl(TEMPO.EKFRQEXE,60)
      this.w_NUMRES = Nvl(TEMPO.EKNUMRES,1)
      * --- Inserimento testata
      * --- Try
      local bErr_055EEE18
      bErr_055EEE18=bTrsErr
      this.Try_055EEE18()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Try
        local bErr_055F0BB8
        bErr_055F0BB8=bTrsErr
        this.Try_055F0BB8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          if not ah_YesNo("Errore durante l'aggiornamento della KPI: '%1'%0Continuo?","",this.w_CODICE)
            * --- Raise
            i_Error=ah_Msgformat("Import fallito")
            return
            exit
          else
            * --- accept error
            bTrsErr=.f.
          endif
        endif
        bTrsErr=bTrsErr or bErr_055F0BB8
        * --- End
      endif
      bTrsErr=bTrsErr or bErr_055EEE18
      * --- End
    endif
    * --- Inserimento dettaglio
    this.w_CPROWNUM = Nvl(TEMPO.CPROWNUM,0)
    if this.w_CPROWNUM<>0
      this.w_CPROWORD = Nvl(TEMPO.CPROWORD,0)
      if this.w_CPROWORD==0
        this.w_CPROWORD = this.w_CPROWNUM*10
      endif
      this.w_PARAM = Alltrim(TEMPO.EK_PARAM)
      this.w_VALORE = Alltrim(TEMPO.EKVALORE)
      this.w_VALORE = Strtran(this.w_VALORE, this.w_DATRASCO, '"')
      * --- Try
      local bErr_055DF120
      bErr_055DF120=bTrsErr
      this.Try_055DF120()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore durante l'inserimento del parametro '%1' della KPI '%2'%0Continuo?","",this.w_PARAM,this.w_CODICE)
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
          exit
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_055DF120
      * --- End
    endif
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_055EEE18()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ELABKPIM
    i_nConn=i_TableProp[this.ELABKPIM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELABKPIM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ELABKPIM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"EKCODICE"+",EKDESCRI"+",EKUTEGRP"+",EKTIPOUG"+",EKCHKATT"+",EKCNDATT"+",EK_QUERY"+",EKTIPRES"+",EKTIPSIN"+",EKBENCHM"+",EKBENFIX"+",EKBENCOD"+",EKVISIBI"+",EKWRTLOG"+",EKFRQEXE"+",EKNUMRES"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODICE),'ELABKPIM','EKCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'ELABKPIM','EKDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_UTEGRP),'ELABKPIM','EKUTEGRP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPOUG),'ELABKPIM','EKTIPOUG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CHKATT),'ELABKPIM','EKCHKATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CNDATT),'ELABKPIM','EKCNDATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QUERY),'ELABKPIM','EK_QUERY');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPRES),'ELABKPIM','EKTIPRES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPSIN),'ELABKPIM','EKTIPSIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BENCHM),'ELABKPIM','EKBENCHM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BENFIX),'ELABKPIM','EKBENFIX');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BENCOD),'ELABKPIM','EKBENCOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VISIBI),'ELABKPIM','EKVISIBI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_WRTLOG),'ELABKPIM','EKWRTLOG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FRQEXE),'ELABKPIM','EKFRQEXE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRES),'ELABKPIM','EKNUMRES');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'EKCODICE',this.w_CODICE,'EKDESCRI',this.w_DESCRI,'EKUTEGRP',this.w_UTEGRP,'EKTIPOUG',this.w_TIPOUG,'EKCHKATT',this.w_CHKATT,'EKCNDATT',this.w_CNDATT,'EK_QUERY',this.w_QUERY,'EKTIPRES',this.w_TIPRES,'EKTIPSIN',this.w_TIPSIN,'EKBENCHM',this.w_BENCHM,'EKBENFIX',this.w_BENFIX,'EKBENCOD',this.w_BENCOD)
      insert into (i_cTable) (EKCODICE,EKDESCRI,EKUTEGRP,EKTIPOUG,EKCHKATT,EKCNDATT,EK_QUERY,EKTIPRES,EKTIPSIN,EKBENCHM,EKBENFIX,EKBENCOD,EKVISIBI,EKWRTLOG,EKFRQEXE,EKNUMRES &i_ccchkf. );
         values (;
           this.w_CODICE;
           ,this.w_DESCRI;
           ,this.w_UTEGRP;
           ,this.w_TIPOUG;
           ,this.w_CHKATT;
           ,this.w_CNDATT;
           ,this.w_QUERY;
           ,this.w_TIPRES;
           ,this.w_TIPSIN;
           ,this.w_BENCHM;
           ,this.w_BENFIX;
           ,this.w_BENCOD;
           ,this.w_VISIBI;
           ,this.w_WRTLOG;
           ,this.w_FRQEXE;
           ,this.w_NUMRES;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserita KPI '%1'",.t.,,,this.w_CODICE)
    return
  proc Try_055F0BB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Svuoto il dettagglio attualmente presente per reimportare tutto pulito
    * --- Delete from ELABKPID
    i_nConn=i_TableProp[this.ELABKPID_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELABKPID_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"EKCODICE = "+cp_ToStrODBC(this.w_CODICE);
             )
    else
      delete from (i_cTable) where;
            EKCODICE = this.w_CODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Write into ELABKPIM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ELABKPIM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELABKPIM_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ELABKPIM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"EKDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'ELABKPIM','EKDESCRI');
      +",EKTIPOUG ="+cp_NullLink(cp_ToStrODBC(this.w_TIPOUG),'ELABKPIM','EKTIPOUG');
      +",EKUTEGRP ="+cp_NullLink(cp_ToStrODBC(this.w_UTEGRP),'ELABKPIM','EKUTEGRP');
      +",EKCHKATT ="+cp_NullLink(cp_ToStrODBC(this.w_CHKATT),'ELABKPIM','EKCHKATT');
      +",EKCNDATT ="+cp_NullLink(cp_ToStrODBC(this.w_CNDATT),'ELABKPIM','EKCNDATT');
      +",EK_QUERY ="+cp_NullLink(cp_ToStrODBC(this.w_QUERY),'ELABKPIM','EK_QUERY');
      +",EKTIPRES ="+cp_NullLink(cp_ToStrODBC(this.w_TIPRES),'ELABKPIM','EKTIPRES');
      +",EKTIPSIN ="+cp_NullLink(cp_ToStrODBC(this.w_TIPSIN),'ELABKPIM','EKTIPSIN');
      +",EKBENCHM ="+cp_NullLink(cp_ToStrODBC(this.w_BENCHM),'ELABKPIM','EKBENCHM');
      +",EKBENFIX ="+cp_NullLink(cp_ToStrODBC(this.w_BENFIX),'ELABKPIM','EKBENFIX');
      +",EKBENCOD ="+cp_NullLink(cp_ToStrODBC(this.w_BENCOD),'ELABKPIM','EKBENCOD');
      +",EKVISIBI ="+cp_NullLink(cp_ToStrODBC(this.w_VISIBI),'ELABKPIM','EKVISIBI');
      +",EKWRTLOG ="+cp_NullLink(cp_ToStrODBC(this.w_WRTLOG),'ELABKPIM','EKWRTLOG');
      +",EKFRQEXE ="+cp_NullLink(cp_ToStrODBC(this.w_FRQEXE),'ELABKPIM','EKFRQEXE');
      +",EKNUMRES ="+cp_NullLink(cp_ToStrODBC(this.w_NUMRES),'ELABKPIM','EKNUMRES');
          +i_ccchkf ;
      +" where ";
          +"EKCODICE = "+cp_ToStrODBC(this.w_CODICE);
             )
    else
      update (i_cTable) set;
          EKDESCRI = this.w_DESCRI;
          ,EKTIPOUG = this.w_TIPOUG;
          ,EKUTEGRP = this.w_UTEGRP;
          ,EKCHKATT = this.w_CHKATT;
          ,EKCNDATT = this.w_CNDATT;
          ,EK_QUERY = this.w_QUERY;
          ,EKTIPRES = this.w_TIPRES;
          ,EKTIPSIN = this.w_TIPSIN;
          ,EKBENCHM = this.w_BENCHM;
          ,EKBENFIX = this.w_BENFIX;
          ,EKBENCOD = this.w_BENCOD;
          ,EKVISIBI = this.w_VISIBI;
          ,EKWRTLOG = this.w_WRTLOG;
          ,EKFRQEXE = this.w_FRQEXE;
          ,EKNUMRES = this.w_NUMRES;
          &i_ccchkf. ;
       where;
          EKCODICE = this.w_CODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Aggiornata KPI '%1'",.t.,,,this.w_CODICE)
    return
  proc Try_055DF120()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ELABKPID
    i_nConn=i_TableProp[this.ELABKPID_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELABKPID_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ELABKPID_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"EKCODICE"+",CPROWNUM"+",CPROWORD"+",EK_PARAM"+",EKVALORE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODICE),'ELABKPID','EKCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'ELABKPID','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'ELABKPID','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PARAM),'ELABKPID','EK_PARAM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALORE),'ELABKPID','EKVALORE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'EKCODICE',this.w_CODICE,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'EK_PARAM',this.w_PARAM,'EKVALORE',this.w_VALORE)
      insert into (i_cTable) (EKCODICE,CPROWNUM,CPROWORD,EK_PARAM,EKVALORE &i_ccchkf. );
         values (;
           this.w_CODICE;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_PARAM;
           ,this.w_VALORE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserito parametro '%1' per KPI '%2'",.t.,,,this.w_PARAM,this.w_CODICE)
    return


  procedure Page_66
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Impostazioni Timer KPI
    CREATE CURSOR TEMPO (TKCODUTE N(4), TK_AVVIO C(1), TKMINUTI C(10))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_CODUTE = 0
    this.w_AVVIO = ""
    this.w_MINUTI = 0
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT NVL(TKCODUTE,0)=0
    * --- Inserisco...
    this.w_CODUTE = TEMPO.TKCODUTE
    this.w_AVVIO = NVL(TEMPO.TK_AVVIO,"N")
    this.w_MINUTI = NVL(TEMPO.TKMINUTI,60)
    * --- Try
    local bErr_0560CE68
    bErr_0560CE68=bTrsErr
    this.Try_0560CE68()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_05624A70
      bErr_05624A70=bTrsErr
      this.Try_05624A70()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore durante l'aggiornamento delle impostazione del Timer KPI per l'utente: '%1'%0Continuo?","",this.w_CODUTE)
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
          exit
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_05624A70
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_0560CE68
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_0560CE68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMRELKPI
    i_nConn=i_TableProp[this.TMRELKPI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMRELKPI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMRELKPI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"TKCODUTE"+",TK_AVVIO"+",TKMINUTI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODUTE),'TMRELKPI','TKCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AVVIO),'TMRELKPI','TK_AVVIO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MINUTI),'TMRELKPI','TKMINUTI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'TKCODUTE',this.w_CODUTE,'TK_AVVIO',this.w_AVVIO,'TKMINUTI',this.w_MINUTI)
      insert into (i_cTable) (TKCODUTE,TK_AVVIO,TKMINUTI &i_ccchkf. );
         values (;
           this.w_CODUTE;
           ,this.w_AVVIO;
           ,this.w_MINUTI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserita configurazione Timer KPI per utente '%1'",.t.,,,this.w_CODUTE)
    return
  proc Try_05624A70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into TMRELKPI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMRELKPI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMRELKPI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMRELKPI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"TK_AVVIO ="+cp_NullLink(cp_ToStrODBC(this.w_AVVIO),'TMRELKPI','TK_AVVIO');
      +",TKMINUTI ="+cp_NullLink(cp_ToStrODBC(this.w_MINUTI),'TMRELKPI','TKMINUTI');
          +i_ccchkf ;
      +" where ";
          +"TKCODUTE = "+cp_ToStrODBC(this.w_CODUTE);
             )
    else
      update (i_cTable) set;
          TK_AVVIO = this.w_AVVIO;
          ,TKMINUTI = this.w_MINUTI;
          &i_ccchkf. ;
       where;
          TKCODUTE = this.w_CODUTE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Aggiornate impostazioni Timer KPI per l'utente '%1'",.t.,,,this.w_CODUTE)
    return


  procedure Page_67
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Gruppi Gadget
    CREATE CURSOR TEMPO (GGCODICE C(5), GGDESCRI C(30))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_CODICE = ""
    this.w_DESCRI = ""
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(GGCODICE," "))
    * --- Inserisco...
    this.w_CODICE = Alltrim(TEMPO.GGCODICE)
    this.w_DESCRI = Alltrim(NVL(TEMPO.GGDESCRI," "))
    this.w_DESCRI = Left(Strtran(this.w_DESCRI, this.w_DATRASCO, '"'),30)
    * --- Try
    local bErr_0561E140
    bErr_0561E140=bTrsErr
    this.Try_0561E140()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_05620E70
      bErr_05620E70=bTrsErr
      this.Try_05620E70()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore durante l'aggiornamento del gruppo: '%1'%0Continuo?","",this.w_DESCRI)
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
          exit
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_05620E70
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_0561E140
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_0561E140()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into GRP_GADG
    i_nConn=i_TableProp[this.GRP_GADG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GRP_GADG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GRP_GADG_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GGCODICE"+",GGDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODICE),'GRP_GADG','GGCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'GRP_GADG','GGDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GGCODICE',this.w_CODICE,'GGDESCRI',this.w_DESCRI)
      insert into (i_cTable) (GGCODICE,GGDESCRI &i_ccchkf. );
         values (;
           this.w_CODICE;
           ,this.w_DESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserito gruppo '%1'",.t.,,,this.w_DESCRI)
    return
  proc Try_05620E70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into GRP_GADG
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.GRP_GADG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GRP_GADG_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.GRP_GADG_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"GGDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'GRP_GADG','GGDESCRI');
          +i_ccchkf ;
      +" where ";
          +"GGCODICE = "+cp_ToStrODBC(this.w_CODICE);
             )
    else
      update (i_cTable) set;
          GGDESCRI = this.w_DESCRI;
          &i_ccchkf. ;
       where;
          GGCODICE = this.w_CODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ah_msg("Aggiornato gruppo '%1'",.t.,,,this.w_DESCRI)
    return


  procedure Page_68
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importa Temi Gadget
    CREATE CURSOR TEMPO (GCCODICE C(5), GCDESCRI C(50), GC__PROP C(50), GCVALORE C(254), GC__NOME C(50))
    APPEND FROM (this.oParentObject.w_PATH) TYPE DELIMITED
    this.w_CURSORE = SELECT()
    this.w_CODICE = ""
    this.w_DESCRI = ""
    this.w_ROWNUM = 0
    this.w_PROP = ""
    this.w_VALORE = ""
    this.w_NOME = ""
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(GCCODICE," "))
    * --- Inserisco...
    this.w_CODICE = Alltrim(TEMPO.GCCODICE)
    this.w_DESCRI = Alltrim(NVL(TEMPO.GCDESCRI," "))
    this.w_DESCRI = Left(Strtran(this.w_DESCRI, this.w_DATRASCO, '"'),50)
    this.w_ROWNUM = this.w_ROWNUM+1
    this.w_PROP = Alltrim(TEMPO.GC__PROP)
    this.w_VALORE = Alltrim(TEMPO.GCVALORE)
    this.w_VALORE = Strtran(this.w_VALORE, this.w_DATRASCO, '"')
    this.w_NOME = Alltrim(TEMPO.GC__NOME)
    * --- Try
    local bErr_05645178
    bErr_05645178=bTrsErr
    this.Try_05645178()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Tema gi� presente: lo cancello e lo reimporto da file
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_05648298
      bErr_05648298=bTrsErr
      this.Try_05648298()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore durante l'aggiornamento della propriet�: '%1'%0Continuo?","",this.w_PROP)
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
          exit
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_05648298
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_05645178
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_05645178()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into GADCOLOR
    i_nConn=i_TableProp[this.GADCOLOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GADCOLOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GADCOLOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GCCODICE"+",GCDESCRI"+",CPROWNUM"+",GC__PROP"+",GCVALORE"+",GC__NOME"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODICE),'GADCOLOR','GCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'GADCOLOR','GCDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'GADCOLOR','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PROP),'GADCOLOR','GC__PROP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALORE),'GADCOLOR','GCVALORE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOME),'GADCOLOR','GC__NOME');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GCCODICE',this.w_CODICE,'GCDESCRI',this.w_DESCRI,'CPROWNUM',this.w_ROWNUM,'GC__PROP',this.w_PROP,'GCVALORE',this.w_VALORE,'GC__NOME',this.w_NOME)
      insert into (i_cTable) (GCCODICE,GCDESCRI,CPROWNUM,GC__PROP,GCVALORE,GC__NOME &i_ccchkf. );
         values (;
           this.w_CODICE;
           ,this.w_DESCRI;
           ,this.w_ROWNUM;
           ,this.w_PROP;
           ,this.w_VALORE;
           ,this.w_NOME;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserita propriet� '%1' per tema '%2'",.t.,,,this.w_PROP,this.w_DESCRI)
    return
  proc Try_05648298()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from GADCOLOR
    i_nConn=i_TableProp[this.GADCOLOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GADCOLOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"GCCODICE = "+cp_ToStrODBC(this.w_CODICE);
             )
    else
      delete from (i_cTable) where;
            GCCODICE = this.w_CODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Insert into GADCOLOR
    i_nConn=i_TableProp[this.GADCOLOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GADCOLOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GADCOLOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GCCODICE"+",GCDESCRI"+",CPROWNUM"+",GC__PROP"+",GCVALORE"+",GC__NOME"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODICE),'GADCOLOR','GCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'GADCOLOR','GCDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'GADCOLOR','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PROP),'GADCOLOR','GC__PROP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALORE),'GADCOLOR','GCVALORE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOME),'GADCOLOR','GC__NOME');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GCCODICE',this.w_CODICE,'GCDESCRI',this.w_DESCRI,'CPROWNUM',this.w_ROWNUM,'GC__PROP',this.w_PROP,'GCVALORE',this.w_VALORE,'GC__NOME',this.w_NOME)
      insert into (i_cTable) (GCCODICE,GCDESCRI,CPROWNUM,GC__PROP,GCVALORE,GC__NOME &i_ccchkf. );
         values (;
           this.w_CODICE;
           ,this.w_DESCRI;
           ,this.w_ROWNUM;
           ,this.w_PROP;
           ,this.w_VALORE;
           ,this.w_NOME;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Aggiornata propriet� '%1' per tema '%2'",.t.,,,this.w_PROP,this.w_DESCRI)
    return


  procedure Page_69
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elenco espressioni
    cp_JsonParseCursor(FileToStr(this.oParentObject.w_PATH), "TEMPO")
    this.w_CURSORE = SELECT()
    this.w_CODICE = ""
    this.w_DESCRI = ""
    this.w_VALORE = ""
    this.w_TIPODEST = ""
    this.w_ESCLUSIVO = ""
    * --- Inserisco nell'archivio
    SELECT (this.w_CURSORE)
    GO TOP
    SCAN FOR NOT EMPTY(NVL(EECODICE," "))
    * --- Inserisco...
    this.w_CODICE = Alltrim(TEMPO.EECODICE)
    this.w_DESCRI = Alltrim(TEMPO.EEDESCRI)
    this.w_VALORE = Alltrim(TEMPO.EE__EXPR)
    this.w_TIPODEST = Alltrim(TEMPO.EE__TIPO)
    this.w_ESCLUSIVO = Alltrim(TEMPO.EE_ESCLU)
    * --- Try
    local bErr_0567CD28
    bErr_0567CD28=bTrsErr
    this.Try_0567CD28()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Espressione gi� presente: la cancello e la reimporto da file
      * --- accept error
      bTrsErr=.f.
      * --- Try
      local bErr_0567FC08
      bErr_0567FC08=bTrsErr
      this.Try_0567FC08()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if not ah_YesNo("Errore durante l'aggiornamento dell'espressione: '%1'%0Continuo?","",this.w_CODICE)
          * --- Raise
          i_Error=ah_Msgformat("Import fallito")
          return
          exit
        else
          * --- accept error
          bTrsErr=.f.
        endif
      endif
      bTrsErr=bTrsErr or bErr_0567FC08
      * --- End
    endif
    bTrsErr=bTrsErr or bErr_0567CD28
    * --- End
    SELECT (this.w_CURSORE)
    ENDSCAN
  endproc
  proc Try_0567CD28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ELE_EXPR
    i_nConn=i_TableProp[this.ELE_EXPR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_EXPR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ELE_EXPR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"EECODICE"+",EEDESCRI"+",EE__EXPR"+",EE__TIPO"+",EE_ESCLU"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODICE),'ELE_EXPR','EECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'ELE_EXPR','EEDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALORE),'ELE_EXPR','EE__EXPR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPODEST),'ELE_EXPR','EE__TIPO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ESCLUSIVO),'ELE_EXPR','EE_ESCLU');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'EECODICE',this.w_CODICE,'EEDESCRI',this.w_DESCRI,'EE__EXPR',this.w_VALORE,'EE__TIPO',this.w_TIPODEST,'EE_ESCLU',this.w_ESCLUSIVO)
      insert into (i_cTable) (EECODICE,EEDESCRI,EE__EXPR,EE__TIPO,EE_ESCLU &i_ccchkf. );
         values (;
           this.w_CODICE;
           ,this.w_DESCRI;
           ,this.w_VALORE;
           ,this.w_TIPODEST;
           ,this.w_ESCLUSIVO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Inserita espressione %1",.t.,,,this.w_CODICE)
    return
  proc Try_0567FC08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from ELE_EXPR
    i_nConn=i_TableProp[this.ELE_EXPR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_EXPR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"EECODICE = "+cp_ToStrODBC(this.w_CODICE);
             )
    else
      delete from (i_cTable) where;
            EECODICE = this.w_CODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Insert into ELE_EXPR
    i_nConn=i_TableProp[this.ELE_EXPR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELE_EXPR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ELE_EXPR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"EECODICE"+",EEDESCRI"+",EE__EXPR"+",EE__TIPO"+",EE_ESCLU"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODICE),'ELE_EXPR','EECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'ELE_EXPR','EEDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALORE),'ELE_EXPR','EE__EXPR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPODEST),'ELE_EXPR','EE__TIPO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ESCLUSIVO),'ELE_EXPR','EE_ESCLU');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'EECODICE',this.w_CODICE,'EEDESCRI',this.w_DESCRI,'EE__EXPR',this.w_VALORE,'EE__TIPO',this.w_TIPODEST,'EE_ESCLU',this.w_ESCLUSIVO)
      insert into (i_cTable) (EECODICE,EEDESCRI,EE__EXPR,EE__TIPO,EE_ESCLU &i_ccchkf. );
         values (;
           this.w_CODICE;
           ,this.w_DESCRI;
           ,this.w_VALORE;
           ,this.w_TIPODEST;
           ,this.w_ESCLUSIVO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTIPO)
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,93)]
    this.cWorkTables[1]='ANAG_CAP'
    this.cWorkTables[2]='ANAG_PRO'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='CAU_AEN'
    this.cWorkTables[5]='CAU_INPS'
    this.cWorkTables[6]='COD_ABI'
    this.cWorkTables[7]='COD_CAB'
    this.cWorkTables[8]='COD_CATA'
    this.cWorkTables[9]='COD_PREV'
    this.cWorkTables[10]='COD_TRIB'
    this.cWorkTables[11]='DETTRICL'
    this.cWorkTables[12]='ENTI_COM'
    this.cWorkTables[13]='NOMENCLA'
    this.cWorkTables[14]='OUT_PUTS'
    this.cWorkTables[15]='RACCBILA'
    this.cWorkTables[16]='RACDBILA'
    this.cWorkTables[17]='REG_PROV'
    this.cWorkTables[18]='RIGTOTAL'
    this.cWorkTables[19]='SE_INAIL'
    this.cWorkTables[20]='SED_INPS'
    this.cWorkTables[21]='STMPFILE'
    this.cWorkTables[22]='TIR_DETT'
    this.cWorkTables[23]='TIR_MAST'
    this.cWorkTables[24]='UNIMIS'
    this.cWorkTables[25]='VOCIRICL'
    this.cWorkTables[26]='TIP_ALLE'
    this.cWorkTables[27]='CLA_ALLE'
    this.cWorkTables[28]='EXT_ENS'
    this.cWorkTables[29]='TOT_MAST'
    this.cWorkTables[30]='TOT_DETT'
    this.cWorkTables[31]='DET_DIME'
    this.cWorkTables[32]='CFG_GEST'
    this.cWorkTables[33]='PRCFGGES'
    this.cWorkTables[34]='MODMCONF'
    this.cWorkTables[35]='MODDCONF'
    this.cWorkTables[36]='PROMODEL'
    this.cWorkTables[37]='UFF_COMP'
    this.cWorkTables[38]='SET_RIGH'
    this.cWorkTables[39]='SET_RAGG'
    this.cWorkTables[40]='TIP_CLIE'
    this.cWorkTables[41]='SET_TIPC'
    this.cWorkTables[42]='CON_UNIF'
    this.cWorkTables[43]='F23_TRIB'
    this.cWorkTables[44]='F23_CONT'
    this.cWorkTables[45]='F23_CAUS'
    this.cWorkTables[46]='UFF_GIUD'
    this.cWorkTables[47]='PRA_UFFI'
    this.cWorkTables[48]='PRA_ENTI'
    this.cWorkTables[49]='PRA_OGGE'
    this.cWorkTables[50]='TIP_MATE'
    this.cWorkTables[51]='PRA_MATE'
    this.cWorkTables[52]='SET_MATE'
    this.cWorkTables[53]='PRA_TIPI'
    this.cWorkTables[54]='SET_TIPI'
    this.cWorkTables[55]='PRE_ITER'
    this.cWorkTables[56]='CAUMATTI'
    this.cWorkTables[57]='CAU_ATTI'
    this.cWorkTables[58]='PRO_ITER'
    this.cWorkTables[59]='MET_SOS'
    this.cWorkTables[60]='STATISOS'
    this.cWorkTables[61]='RSS_FEED'
    this.cWorkTables[62]='MAP_OUTL'
    this.cWorkTables[63]='MAP_SINC'
    this.cWorkTables[64]='REF_MAST'
    this.cWorkTables[65]='REF_DETT'
    this.cWorkTables[66]='RAG_TIPI'
    this.cWorkTables[67]='WZMODREP'
    this.cWorkTables[68]='FES_MAST'
    this.cWorkTables[69]='FES_DETT'
    this.cWorkTables[70]='PUN_ACCE'
    this.cWorkTables[71]='PWB_GLOC'
    this.cWorkTables[72]='CAUPRI1'
    this.cWorkTables[73]='CAUIVA1'
    this.cWorkTables[74]='CAU_CONT'
    this.cWorkTables[75]='CONTROPA'
    this.cWorkTables[76]='FAS_TARI'
    this.cWorkTables[77]='TSDESPRO'
    this.cWorkTables[78]='FOR_CESP'
    this.cWorkTables[79]='MCO_CESP'
    this.cWorkTables[80]='CAU_CESP'
    this.cWorkTables[81]='BUS_TIPI'
    this.cWorkTables[82]='MOD_GADG'
    this.cWorkTables[83]='MYG_MAST'
    this.cWorkTables[84]='MYG_DETT'
    this.cWorkTables[85]='GAD_MAST'
    this.cWorkTables[86]='GAD_DETT'
    this.cWorkTables[87]='ELABKPIM'
    this.cWorkTables[88]='ELABKPID'
    this.cWorkTables[89]='TMRELKPI'
    this.cWorkTables[90]='GAD_ACCE'
    this.cWorkTables[91]='GRP_GADG'
    this.cWorkTables[92]='GADCOLOR'
    this.cWorkTables[93]='ELE_EXPR'
    return(this.OpenAllTables(93))

  proc CloseCursors()
    if used('_Curs_query_STRU_BIL_NOTE')
      use in _Curs_query_STRU_BIL_NOTE
    endif
    if used('_Curs_TIR_MAST')
      use in _Curs_TIR_MAST
    endif
    if used('_Curs_PROMODEL')
      use in _Curs_PROMODEL
    endif
    if used('_Curs_MAP_OUTL')
      use in _Curs_MAP_OUTL
    endif
    if used('_Curs_MAP_SINC')
      use in _Curs_MAP_SINC
    endif
    if used('_Curs_QUERY_GSUT_GAE')
      use in _Curs_QUERY_GSUT_GAE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO"
endproc
