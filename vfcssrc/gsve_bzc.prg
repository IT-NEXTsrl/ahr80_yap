* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bzc                                                        *
*              Lancia anagrafiche articoli                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-18                                                      *
* Last revis.: 2013-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bzc",oParentObject)
return(i_retval)

define class tgsve_bzc as StdBatch
  * --- Local variables
  w_PROG = space(8)
  w_DXBTN = .f.
  w_CODICE = space(5)
  w_FLVEAC = space(1)
  w_CATDOC = space(2)
  * --- WorkFile variables
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia la causale documento corretta (vendite - acquisti).
    *     Utilizzata nelle causali documento per selezionare causali di esplosione
    this.w_DXBTN = .F.
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_DXBTN = .T.
      this.w_CODICE = Nvl(g_oMenu.oKey(1,3),"")
      if Not Empty(this.w_CODICE)
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDFLVEAC,TDCATDOC"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.w_CODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDFLVEAC,TDCATDOC;
            from (i_cTable) where;
                TDTIPDOC = this.w_CODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
          this.w_CATDOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        if Type("g_oMenu.oParentObject")="O" And Type("g_oMenu.oParentObject.w_TDCATDOC")="C"
          * --- Se ho fatto tasto destro sul campo vuoto cerco di capire dalla gestione quale gestione devo lanciare
          this.w_FLVEAC = g_oMenu.oParentObject.w_TDFLVEAC
          this.w_CATDOC = g_oMenu.oParentObject.w_TDCATDOC
          if this.w_CATDOC="OR"
            * --- Se sono sugli ordini ed eseguo un caricamento sul campo dei documenti di esplosione
            *     non devo aprire una causale ordine ma una causale vendita/acquisto
            this.w_CATDOC = "XX"
          endif
        endif
      endif
    else
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      * --- Tipo di Conto
      * --- Dal cursore dello zoom prendo codice causale, ciclo, categoria
      this.w_CODICE = &cCurs..TDTIPDOC
      this.w_FLVEAC = &cCurs..TDFLVEAC
      this.w_CATDOC = &cCurs..TDCATDOC
      if Empty(this.w_CODICE)
        * --- Se non ho codice apro le vendite
        this.w_FLVEAC = "V"
        * --- Per essere sicuri che non entri in w_CATDOC='OR'
        this.w_CATDOC = "XX"
      endif
    endif
    if this.w_CATDOC="OR"
      * --- Lancia ordini
      this.w_PROG = "GSOR_ATD"
    else
      do case
        case this.w_FLVEAC = "A"
          if g_ACQU="S"
            * --- Lancia acquisti
            this.w_PROG = "GSAC_ATD"
          else
            this.w_PROG = "GSVE_ATD"
          endif
        case this.w_FLVEAC = "V"
          * --- Lancia vendite
          this.w_PROG = "GSVE_ATD"
      endcase
    endif
    * --- Nel caso ho premuto tasto destro sul controll apro la anagrafica sul codice 
    *     presente nella variabile
    OpenGest(IIF(this.w_DXBTN,g_oMenu.cBatchType,"S"),this.w_PROG,"TDTIPDOC",this.w_CODICE)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='TIP_DOCU'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
