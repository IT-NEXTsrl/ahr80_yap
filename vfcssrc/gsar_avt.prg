* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_avt                                                        *
*              Vettori                                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_17]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2012-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_avt"))

* --- Class definition
define class tgsar_avt as StdForm
  Top    = 39
  Left   = 27

  * --- Standard Properties
  Width  = 562
  Height = 239+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-03-26"
  HelpContextID=59340649
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  VETTORI_IDX = 0
  CONTI_IDX = 0
  METCALSP_IDX = 0
  cFile = "VETTORI"
  cKeySelect = "VTCODVET"
  cKeyWhere  = "VTCODVET=this.w_VTCODVET"
  cKeyWhereODBC = '"VTCODVET="+cp_ToStrODBC(this.w_VTCODVET)';

  cKeyWhereODBCqualified = '"VETTORI.VTCODVET="+cp_ToStrODBC(this.w_VTCODVET)';

  cPrg = "gsar_avt"
  cComment = "Vettori"
  icon = "anag.ico"
  cAutoZoom = 'GSVE0AVT'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_VTCODVET = space(5)
  w_VTDESVET = space(35)
  w_VTINDVET = space(25)
  w_VTVETCAP = space(8)
  w_VTLOCVET = space(30)
  w_VTPROVET = space(2)
  w_VTNUMISC = space(40)
  w_VTTIPCON = space(1)
  w_VTCODCON = space(15)
  w_DESFOR = space(40)
  w_VTMCCODI = space(5)
  w_VTMCCODT = space(5)
  w_VTDTOBSO = ctod('  /  /  ')
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_VTDTINVA = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESMCIMB = space(40)
  w_DESMCTRA = space(40)
  w_VT__NOTE = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'VETTORI','gsar_avt')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_avtPag1","gsar_avt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Vettore")
      .Pages(1).HelpContextID = 169725098
      .Pages(2).addobject("oPag","tgsar_avtPag2","gsar_avt",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Note")
      .Pages(2).HelpContextID = 52216618
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oVTCODVET_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='METCALSP'
    this.cWorkTables[3]='VETTORI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VETTORI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VETTORI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_VTCODVET = NVL(VTCODVET,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_9_joined
    link_1_9_joined=.f.
    local link_1_11_joined
    link_1_11_joined=.f.
    local link_1_12_joined
    link_1_12_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from VETTORI where VTCODVET=KeySet.VTCODVET
    *
    i_nConn = i_TableProp[this.VETTORI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VETTORI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VETTORI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' VETTORI '
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'VTCODVET',this.w_VTCODVET  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESFOR = space(40)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_DESMCIMB = space(40)
        .w_DESMCTRA = space(40)
        .w_VTCODVET = NVL(VTCODVET,space(5))
        .w_VTDESVET = NVL(VTDESVET,space(35))
        .w_VTINDVET = NVL(VTINDVET,space(25))
        .w_VTVETCAP = NVL(VTVETCAP,space(8))
        .w_VTLOCVET = NVL(VTLOCVET,space(30))
        .w_VTPROVET = NVL(VTPROVET,space(2))
        .w_VTNUMISC = NVL(VTNUMISC,space(40))
        .w_VTTIPCON = NVL(VTTIPCON,space(1))
        .w_VTCODCON = NVL(VTCODCON,space(15))
          if link_1_9_joined
            this.w_VTCODCON = NVL(ANCODICE109,NVL(this.w_VTCODCON,space(15)))
            this.w_DESFOR = NVL(ANDESCRI109,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO109),ctod("  /  /  "))
          else
          .link_1_9('Load')
          endif
        .w_VTMCCODI = NVL(VTMCCODI,space(5))
          if link_1_11_joined
            this.w_VTMCCODI = NVL(MSCODICE111,NVL(this.w_VTMCCODI,space(5)))
            this.w_DESMCIMB = NVL(MSDESCRI111,space(40))
          else
          .link_1_11('Load')
          endif
        .w_VTMCCODT = NVL(VTMCCODT,space(5))
          if link_1_12_joined
            this.w_VTMCCODT = NVL(MSCODICE112,NVL(this.w_VTMCCODT,space(5)))
            this.w_DESMCTRA = NVL(MSDESCRI112,space(40))
          else
          .link_1_12('Load')
          endif
        .w_VTDTOBSO = NVL(cp_ToDate(VTDTOBSO),ctod("  /  /  "))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_VTDTINVA = NVL(cp_ToDate(VTDTINVA),ctod("  /  /  "))
        .w_VT__NOTE = NVL(VT__NOTE,space(0))
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        cp_LoadRecExtFlds(this,'VETTORI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_VTCODVET = space(5)
      .w_VTDESVET = space(35)
      .w_VTINDVET = space(25)
      .w_VTVETCAP = space(8)
      .w_VTLOCVET = space(30)
      .w_VTPROVET = space(2)
      .w_VTNUMISC = space(40)
      .w_VTTIPCON = space(1)
      .w_VTCODCON = space(15)
      .w_DESFOR = space(40)
      .w_VTMCCODI = space(5)
      .w_VTMCCODT = space(5)
      .w_VTDTOBSO = ctod("  /  /  ")
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_VTDTINVA = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_DESMCIMB = space(40)
      .w_DESMCTRA = space(40)
      .w_VT__NOTE = space(0)
      if .cFunction<>"Filter"
          .DoRTCalc(1,7,.f.)
        .w_VTTIPCON = 'F'
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_VTCODCON))
          .link_1_9('Full')
          endif
        .DoRTCalc(10,11,.f.)
          if not(empty(.w_VTMCCODI))
          .link_1_11('Full')
          endif
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_VTMCCODT))
          .link_1_12('Full')
          endif
          .DoRTCalc(13,18,.f.)
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'VETTORI')
    this.DoRTCalc(20,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oVTCODVET_1_1.enabled = i_bVal
      .Page1.oPag.oVTDESVET_1_2.enabled = i_bVal
      .Page1.oPag.oVTINDVET_1_3.enabled = i_bVal
      .Page1.oPag.oVTVETCAP_1_4.enabled = i_bVal
      .Page1.oPag.oVTLOCVET_1_5.enabled = i_bVal
      .Page1.oPag.oVTPROVET_1_6.enabled = i_bVal
      .Page1.oPag.oVTNUMISC_1_7.enabled = i_bVal
      .Page1.oPag.oVTCODCON_1_9.enabled = i_bVal
      .Page1.oPag.oVTMCCODI_1_11.enabled = i_bVal
      .Page1.oPag.oVTMCCODT_1_12.enabled = i_bVal
      .Page1.oPag.oVTDTOBSO_1_18.enabled = i_bVal
      .Page1.oPag.oVTDTINVA_1_26.enabled = i_bVal
      .Page2.oPag.oVT__NOTE_2_1.enabled = i_bVal
      .Page1.oPag.oObj_1_34.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oVTCODVET_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oVTCODVET_1_1.enabled = .t.
        .Page1.oPag.oVTDESVET_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'VETTORI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VETTORI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTCODVET,"VTCODVET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTDESVET,"VTDESVET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTINDVET,"VTINDVET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTVETCAP,"VTVETCAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTLOCVET,"VTLOCVET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTPROVET,"VTPROVET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTNUMISC,"VTNUMISC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTTIPCON,"VTTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTCODCON,"VTCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTMCCODI,"VTMCCODI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTMCCODT,"VTMCCODT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTDTOBSO,"VTDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VTDTINVA,"VTDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_VT__NOTE,"VT__NOTE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VETTORI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
    i_lTable = "VETTORI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.VETTORI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SVT with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VETTORI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.VETTORI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into VETTORI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VETTORI')
        i_extval=cp_InsertValODBCExtFlds(this,'VETTORI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(VTCODVET,VTDESVET,VTINDVET,VTVETCAP,VTLOCVET"+;
                  ",VTPROVET,VTNUMISC,VTTIPCON,VTCODCON,VTMCCODI"+;
                  ",VTMCCODT,VTDTOBSO,UTCC,UTCV,UTDC"+;
                  ",UTDV,VTDTINVA,VT__NOTE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_VTCODVET)+;
                  ","+cp_ToStrODBC(this.w_VTDESVET)+;
                  ","+cp_ToStrODBC(this.w_VTINDVET)+;
                  ","+cp_ToStrODBC(this.w_VTVETCAP)+;
                  ","+cp_ToStrODBC(this.w_VTLOCVET)+;
                  ","+cp_ToStrODBC(this.w_VTPROVET)+;
                  ","+cp_ToStrODBC(this.w_VTNUMISC)+;
                  ","+cp_ToStrODBC(this.w_VTTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_VTCODCON)+;
                  ","+cp_ToStrODBCNull(this.w_VTMCCODI)+;
                  ","+cp_ToStrODBCNull(this.w_VTMCCODT)+;
                  ","+cp_ToStrODBC(this.w_VTDTOBSO)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_VTDTINVA)+;
                  ","+cp_ToStrODBC(this.w_VT__NOTE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VETTORI')
        i_extval=cp_InsertValVFPExtFlds(this,'VETTORI')
        cp_CheckDeletedKey(i_cTable,0,'VTCODVET',this.w_VTCODVET)
        INSERT INTO (i_cTable);
              (VTCODVET,VTDESVET,VTINDVET,VTVETCAP,VTLOCVET,VTPROVET,VTNUMISC,VTTIPCON,VTCODCON,VTMCCODI,VTMCCODT,VTDTOBSO,UTCC,UTCV,UTDC,UTDV,VTDTINVA,VT__NOTE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_VTCODVET;
                  ,this.w_VTDESVET;
                  ,this.w_VTINDVET;
                  ,this.w_VTVETCAP;
                  ,this.w_VTLOCVET;
                  ,this.w_VTPROVET;
                  ,this.w_VTNUMISC;
                  ,this.w_VTTIPCON;
                  ,this.w_VTCODCON;
                  ,this.w_VTMCCODI;
                  ,this.w_VTMCCODT;
                  ,this.w_VTDTOBSO;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_VTDTINVA;
                  ,this.w_VT__NOTE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.VETTORI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.VETTORI_IDX,i_nConn)
      *
      * update VETTORI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'VETTORI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " VTDESVET="+cp_ToStrODBC(this.w_VTDESVET)+;
             ",VTINDVET="+cp_ToStrODBC(this.w_VTINDVET)+;
             ",VTVETCAP="+cp_ToStrODBC(this.w_VTVETCAP)+;
             ",VTLOCVET="+cp_ToStrODBC(this.w_VTLOCVET)+;
             ",VTPROVET="+cp_ToStrODBC(this.w_VTPROVET)+;
             ",VTNUMISC="+cp_ToStrODBC(this.w_VTNUMISC)+;
             ",VTTIPCON="+cp_ToStrODBC(this.w_VTTIPCON)+;
             ",VTCODCON="+cp_ToStrODBCNull(this.w_VTCODCON)+;
             ",VTMCCODI="+cp_ToStrODBCNull(this.w_VTMCCODI)+;
             ",VTMCCODT="+cp_ToStrODBCNull(this.w_VTMCCODT)+;
             ",VTDTOBSO="+cp_ToStrODBC(this.w_VTDTOBSO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",VTDTINVA="+cp_ToStrODBC(this.w_VTDTINVA)+;
             ",VT__NOTE="+cp_ToStrODBC(this.w_VT__NOTE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'VETTORI')
        i_cWhere = cp_PKFox(i_cTable  ,'VTCODVET',this.w_VTCODVET  )
        UPDATE (i_cTable) SET;
              VTDESVET=this.w_VTDESVET;
             ,VTINDVET=this.w_VTINDVET;
             ,VTVETCAP=this.w_VTVETCAP;
             ,VTLOCVET=this.w_VTLOCVET;
             ,VTPROVET=this.w_VTPROVET;
             ,VTNUMISC=this.w_VTNUMISC;
             ,VTTIPCON=this.w_VTTIPCON;
             ,VTCODCON=this.w_VTCODCON;
             ,VTMCCODI=this.w_VTMCCODI;
             ,VTMCCODT=this.w_VTMCCODT;
             ,VTDTOBSO=this.w_VTDTOBSO;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,VTDTINVA=this.w_VTDTINVA;
             ,VT__NOTE=this.w_VT__NOTE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.VETTORI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.VETTORI_IDX,i_nConn)
      *
      * delete VETTORI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'VTCODVET',this.w_VTCODVET  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VETTORI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,7,.t.)
            .w_VTTIPCON = 'F'
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=VTCODCON
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VTCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_VTCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_VTTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_VTTIPCON;
                     ,'ANCODICE',trim(this.w_VTCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VTCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_VTCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_VTTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_VTCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_VTTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_VTCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oVTCODCON_1_9'),i_cWhere,'GSAR_BZC',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_VTTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_VTTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VTCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_VTCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_VTTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_VTTIPCON;
                       ,'ANCODICE',this.w_VTCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VTCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_VTCODCON = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_VTCODCON = space(15)
        this.w_DESFOR = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VTCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.ANCODICE as ANCODICE109"+ ",link_1_9.ANDESCRI as ANDESCRI109"+ ",link_1_9.ANDTOBSO as ANDTOBSO109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on VETTORI.VTCODCON=link_1_9.ANCODICE"+" and VETTORI.VTTIPCON=link_1_9.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and VETTORI.VTCODCON=link_1_9.ANCODICE(+)"'+'+" and VETTORI.VTTIPCON=link_1_9.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VTMCCODI
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VTMCCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_VTMCCODI)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_VTMCCODI))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VTMCCODI)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VTMCCODI) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oVTMCCODI_1_11'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VTMCCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_VTMCCODI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_VTMCCODI)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VTMCCODI = NVL(_Link_.MSCODICE,space(5))
      this.w_DESMCIMB = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_VTMCCODI = space(5)
      endif
      this.w_DESMCIMB = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VTMCCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.MSCODICE as MSCODICE111"+ ",link_1_11.MSDESCRI as MSDESCRI111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on VETTORI.VTMCCODI=link_1_11.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and VETTORI.VTMCCODI=link_1_11.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VTMCCODT
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VTMCCODT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_VTMCCODT)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_VTMCCODT))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VTMCCODT)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VTMCCODT) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oVTMCCODT_1_12'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VTMCCODT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_VTMCCODT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_VTMCCODT)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VTMCCODT = NVL(_Link_.MSCODICE,space(5))
      this.w_DESMCTRA = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_VTMCCODT = space(5)
      endif
      this.w_DESMCTRA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VTMCCODT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.MSCODICE as MSCODICE112"+ ",link_1_12.MSDESCRI as MSDESCRI112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on VETTORI.VTMCCODT=link_1_12.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and VETTORI.VTMCCODT=link_1_12.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oVTCODVET_1_1.value==this.w_VTCODVET)
      this.oPgFrm.Page1.oPag.oVTCODVET_1_1.value=this.w_VTCODVET
    endif
    if not(this.oPgFrm.Page1.oPag.oVTDESVET_1_2.value==this.w_VTDESVET)
      this.oPgFrm.Page1.oPag.oVTDESVET_1_2.value=this.w_VTDESVET
    endif
    if not(this.oPgFrm.Page1.oPag.oVTINDVET_1_3.value==this.w_VTINDVET)
      this.oPgFrm.Page1.oPag.oVTINDVET_1_3.value=this.w_VTINDVET
    endif
    if not(this.oPgFrm.Page1.oPag.oVTVETCAP_1_4.value==this.w_VTVETCAP)
      this.oPgFrm.Page1.oPag.oVTVETCAP_1_4.value=this.w_VTVETCAP
    endif
    if not(this.oPgFrm.Page1.oPag.oVTLOCVET_1_5.value==this.w_VTLOCVET)
      this.oPgFrm.Page1.oPag.oVTLOCVET_1_5.value=this.w_VTLOCVET
    endif
    if not(this.oPgFrm.Page1.oPag.oVTPROVET_1_6.value==this.w_VTPROVET)
      this.oPgFrm.Page1.oPag.oVTPROVET_1_6.value=this.w_VTPROVET
    endif
    if not(this.oPgFrm.Page1.oPag.oVTNUMISC_1_7.value==this.w_VTNUMISC)
      this.oPgFrm.Page1.oPag.oVTNUMISC_1_7.value=this.w_VTNUMISC
    endif
    if not(this.oPgFrm.Page1.oPag.oVTCODCON_1_9.value==this.w_VTCODCON)
      this.oPgFrm.Page1.oPag.oVTCODCON_1_9.value=this.w_VTCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_10.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_10.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oVTMCCODI_1_11.value==this.w_VTMCCODI)
      this.oPgFrm.Page1.oPag.oVTMCCODI_1_11.value=this.w_VTMCCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oVTMCCODT_1_12.value==this.w_VTMCCODT)
      this.oPgFrm.Page1.oPag.oVTMCCODT_1_12.value=this.w_VTMCCODT
    endif
    if not(this.oPgFrm.Page1.oPag.oVTDTOBSO_1_18.value==this.w_VTDTOBSO)
      this.oPgFrm.Page1.oPag.oVTDTOBSO_1_18.value=this.w_VTDTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oVTDTINVA_1_26.value==this.w_VTDTINVA)
      this.oPgFrm.Page1.oPag.oVTDTINVA_1_26.value=this.w_VTDTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMCIMB_1_31.value==this.w_DESMCIMB)
      this.oPgFrm.Page1.oPag.oDESMCIMB_1_31.value=this.w_DESMCIMB
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMCTRA_1_32.value==this.w_DESMCTRA)
      this.oPgFrm.Page1.oPag.oDESMCTRA_1_32.value=this.w_DESMCTRA
    endif
    if not(this.oPgFrm.Page2.oPag.oVT__NOTE_2_1.value==this.w_VT__NOTE)
      this.oPgFrm.Page2.oPag.oVT__NOTE_2_1.value=this.w_VT__NOTE
    endif
    cp_SetControlsValueExtFlds(this,'VETTORI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_VTCODVET))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVTCODVET_1_1.SetFocus()
            i_bnoObbl = !empty(.w_VTCODVET)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_VTCODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVTCODCON_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_avtPag1 as StdContainer
  Width  = 558
  height = 239
  stdWidth  = 558
  stdheight = 239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVTCODVET_1_1 as StdField with uid="QBWGSWHISS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_VTCODVET", cQueryName = "VTCODVET",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del vettore",;
    HelpContextID = 118100650,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=112, Top=10, InputMask=replicate('X',5)

  add object oVTDESVET_1_2 as StdField with uid="GURIEGFNBZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_VTDESVET", cQueryName = "VTDESVET",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del vettore",;
    HelpContextID = 133178026,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=177, Top=10, InputMask=replicate('X',35)

  add object oVTINDVET_1_3 as StdField with uid="XMGIWYTTXL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_VTINDVET", cQueryName = "VTINDVET",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo in cui ha sede il vettore",;
    HelpContextID = 118059690,;
   bGlobalFont=.t.,;
    Height=21, Width=208, Left=112, Top=40, InputMask=replicate('X',25)

  add object oVTVETCAP_1_4 as StdField with uid="OCUNQPZVVW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_VTVETCAP", cQueryName = "VTVETCAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "CAP della sede del vettore",;
    HelpContextID = 83968678,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=112, Top=70, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',8), bHasZoom = .t. 

  proc oVTVETCAP_1_4.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_VTVETCAP",".w_VTLOCVET",".w_VTPROVET")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oVTLOCVET_1_5 as StdField with uid="EKTNTUZGXJ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_VTLOCVET", cQueryName = "VTLOCVET",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� in cui ha sede il vettore",;
    HelpContextID = 117088938,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=242, Top=70, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oVTLOCVET_1_5.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_VTVETCAP",".w_VTLOCVET",".w_VTPROVET")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oVTPROVET_1_6 as StdField with uid="AIXYWPFQWP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_VTPROVET", cQueryName = "VTPROVET",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla provincia del vettore",;
    HelpContextID = 129884842,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=515, Top=70, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  proc oVTPROVET_1_6.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_VTPROVET")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oVTNUMISC_1_7 as StdField with uid="AWLORHZGLB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_VTNUMISC", cQueryName = "VTNUMISC",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Numero iscrizione albo autotrasportatori",;
    HelpContextID = 178307737,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=112, Top=96, InputMask=replicate('X',40)

  add object oVTCODCON_1_9 as StdField with uid="EUSIIMXJEV",rtseq=9,rtrep=.f.,;
    cFormVar = "w_VTCODCON", cQueryName = "VTCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Codice dell'eventuale fornitore di riferimento",;
    HelpContextID = 200666460,;
   bGlobalFont=.t.,;
    Height=21, Width=127, Left=112, Top=122, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_VTTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_VTCODCON"

  func oVTCODCON_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oVTCODCON_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVTCODCON_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_VTTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_VTTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oVTCODCON_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oVTCODCON_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_VTTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_VTCODCON
     i_obj.ecpSave()
  endproc

  add object oDESFOR_1_10 as StdField with uid="LLZOWDYPWS",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 61997622,;
   bGlobalFont=.t.,;
    Height=21, Width=312, Left=244, Top=122, InputMask=replicate('X',40)

  add object oVTMCCODI_1_11 as StdField with uid="SFBCGWYHIH",rtseq=11,rtrep=.f.,;
    cFormVar = "w_VTMCCODI", cQueryName = "VTMCCODI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo spese imballo",;
    HelpContextID = 267301535,;
   bGlobalFont=.t.,;
    Height=21, Width=68, Left=170, Top=149, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_VTMCCODI"

  func oVTMCCODI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oVTMCCODI_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVTMCCODI_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oVTMCCODI_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oVTMCCODI_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_VTMCCODI
     i_obj.ecpSave()
  endproc

  add object oVTMCCODT_1_12 as StdField with uid="VBHFCIEJSS",rtseq=12,rtrep=.f.,;
    cFormVar = "w_VTMCCODT", cQueryName = "VTMCCODT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo spese di trasporto",;
    HelpContextID = 267301546,;
   bGlobalFont=.t.,;
    Height=21, Width=68, Left=170, Top=175, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_VTMCCODT"

  func oVTMCCODT_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oVTMCCODT_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVTMCCODT_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oVTMCCODT_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oVTMCCODT_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_VTMCCODT
     i_obj.ecpSave()
  endproc

  add object oVTDTOBSO_1_18 as StdField with uid="QXGJBGFZEO",rtseq=13,rtrep=.f.,;
    cFormVar = "w_VTDTOBSO", cQueryName = "VTDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di validit�",;
    HelpContextID = 62857893,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=477, Top=208, tabstop=.f.

  add object oVTDTINVA_1_26 as StdField with uid="ZJMOOSLKII",rtseq=18,rtrep=.f.,;
    cFormVar = "w_VTDTINVA", cQueryName = "VTDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit�",;
    HelpContextID = 257893015,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=241, Top=208, tabstop=.f.

  add object oDESMCIMB_1_31 as StdField with uid="TPRNVEINLT",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESMCIMB", cQueryName = "DESMCIMB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 101121416,;
   bGlobalFont=.t.,;
    Height=21, Width=312, Left=243, Top=149, InputMask=replicate('X',40)

  add object oDESMCTRA_1_32 as StdField with uid="DZTLRVIOWR",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESMCTRA", cQueryName = "DESMCTRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 83427959,;
   bGlobalFont=.t.,;
    Height=21, Width=312, Left=243, Top=175, InputMask=replicate('X',40)


  add object oObj_1_34 as cp_runprogram with uid="ROAAWEFSEL",left=7, top=265, width=159,height=19,;
    caption='GSAR_BVT',;
   bGlobalFont=.t.,;
    prg="GSAR_BVT",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 79491258

  add object oStr_1_13 as StdString with uid="DQWMDFSPBG",Visible=.t., Left=7, Top=10,;
    Alignment=1, Width=103, Height=15,;
    Caption="Codice vettore:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_14 as StdString with uid="LGOXQREFYY",Visible=.t., Left=7, Top=40,;
    Alignment=1, Width=103, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="OSTSIMJCNM",Visible=.t., Left=7, Top=70,;
    Alignment=1, Width=103, Height=15,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="WBZCQRDSYR",Visible=.t., Left=180, Top=70,;
    Alignment=1, Width=60, Height=15,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="AORPPZBKDS",Visible=.t., Left=483, Top=70,;
    Alignment=1, Width=33, Height=18,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="YVPNKXOBER",Visible=.t., Left=351, Top=208,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="ZOFAVNXLUH",Visible=.t., Left=7, Top=125,;
    Alignment=1, Width=103, Height=15,;
    Caption="Rif.fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="RKXTDEJWTA",Visible=.t., Left=140, Top=208,;
    Alignment=1, Width=98, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="AVLOTAFZOF",Visible=.t., Left=27, Top=149,;
    Alignment=1, Width=140, Height=18,;
    Caption="M. c. spese imballo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="XLLGNWHQWQ",Visible=.t., Left=28, Top=175,;
    Alignment=1, Width=139, Height=18,;
    Caption="M. c. spese trasporto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="ERRZAQGWNJ",Visible=.t., Left=13, Top=99,;
    Alignment=1, Width=97, Height=15,;
    Caption="Nr. iscrizione:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsar_avtPag2 as StdContainer
  Width  = 558
  height = 239
  stdWidth  = 558
  stdheight = 239
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oVT__NOTE_2_1 as StdMemo with uid="DPUJLTXVXI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_VT__NOTE", cQueryName = "VT__NOTE",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 12309147,;
   bGlobalFont=.t.,;
    Height=228, Width=548, Left=5, Top=8
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_avt','VETTORI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".VTCODVET=VETTORI.VTCODVET";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
