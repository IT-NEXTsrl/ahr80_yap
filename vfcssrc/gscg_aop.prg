* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_aop                                                        *
*              Modello pagamento F24/2007                                      *
*                                                                              *
*      Author: Zucchetti TAM Srl & Zucchetti                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-14                                                      *
* Last revis.: 2018-05-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gscg_aop
**Indica se il tipo di comunicazione � Entratel o CBI
PARAMETERS pEntCbi, pAnno
* --- Fine Area Manuale
return(createobject("tgscg_aop"))

* --- Class definition
define class tgscg_aop as StdForm
  Top    = 3
  Left   = 10

  * --- Standard Properties
  Width  = 873
  Height = 441+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-05-22"
  HelpContextID=91609751
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=230

  * --- Constant Properties
  MOD_PAG_IDX = 0
  TRI_BUTI_IDX = 0
  AZIENDA_IDX = 0
  TITOLARI_IDX = 0
  COD_TRIB_IDX = 0
  CODI_UFF_IDX = 0
  SED_INPS_IDX = 0
  CAU_INPS_IDX = 0
  REG_PROV_IDX = 0
  ENTI_LOC_IDX = 0
  SE_INAIL_IDX = 0
  COD_PREV_IDX = 0
  SED_AEN_IDX = 0
  CAU_AEN_IDX = 0
  MODVPAG_IDX = 0
  MODCPAG_IDX = 0
  MODEPAG_IDX = 0
  VALUTE_IDX = 0
  CPUSERS_IDX = 0
  MODIPAG_IDX = 0
  SEDIAZIE_IDX = 0
  ANAG_PRO_IDX = 0
  DAT_RAPP_IDX = 0
  COC_MAST_IDX = 0
  cFile = "MOD_PAG"
  cKeySelect = "MFSERIAL"
  cKeyWhere  = "MFSERIAL=this.w_MFSERIAL"
  cKeyWhereODBC = '"MFSERIAL="+cp_ToStrODBC(this.w_MFSERIAL)';

  cKeyWhereODBCqualified = '"MOD_PAG.MFSERIAL="+cp_ToStrODBC(this.w_MFSERIAL)';

  cPrg = "gscg_aop"
  cComment = "Modello pagamento F24/2007"
  icon = "anag.ico"
  cAutoZoom = 'GSCG_AOP'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MFSERIAL = space(10)
  o_MFSERIAL = space(10)
  w_MFMESRIF = space(2)
  w_MFANNRIF = space(4)
  w_MFVALUTA = space(3)
  w_APPCOIN = space(1)
  o_APPCOIN = space(1)
  w_MF_COINC = space(1)
  w_MFSERIAL = space(10)
  w_APPOIMP = 0
  w_MFCODUFF = space(3)
  w_MFCODATT = space(11)
  w_MESE = space(2)
  w_ANNO = space(4)
  w_GENERA = space(10)
  w_IMPORTA = .F.
  w_MFSERIAL = space(10)
  w_MFCDSED1 = space(5)
  o_MFCDSED1 = space(5)
  w_MFCCONT1 = space(5)
  o_MFCCONT1 = space(5)
  w_MFMINPS1 = space(17)
  w_APP1A = .F.
  o_APP1A = .F.
  w_APP1 = .F.
  o_APP1 = .F.
  w_MFDAMES1 = space(2)
  w_MFDAANN1 = space(4)
  w_MF_AMES1 = space(2)
  w_MFAN1 = space(4)
  w_MFIMPSD1 = 0
  w_MFIMPSC1 = 0
  w_MFCDSED2 = space(5)
  o_MFCDSED2 = space(5)
  w_MFCCONT2 = space(5)
  o_MFCCONT2 = space(5)
  w_MFMINPS2 = space(17)
  w_APP2A = .F.
  o_APP2A = .F.
  w_MFDAMES2 = space(2)
  w_MFDAANN2 = space(4)
  w_APP2 = .F.
  o_APP2 = .F.
  w_MF_AMES2 = space(2)
  w_MFAN2 = space(4)
  w_MFIMPSD2 = 0
  w_MFIMPSC2 = 0
  w_MFCDSED3 = space(5)
  o_MFCDSED3 = space(5)
  w_MFCCONT3 = space(5)
  o_MFCCONT3 = space(5)
  w_MFMINPS3 = space(17)
  w_APP3A = .F.
  o_APP3A = .F.
  w_MFDAMES3 = space(2)
  w_APP3 = .F.
  o_APP3 = .F.
  w_MFDAANN3 = space(4)
  w_MF_AMES3 = space(2)
  w_MFAN3 = space(4)
  w_MFIMPSD3 = 0
  w_MFIMPSC3 = 0
  w_MFCDSED4 = space(5)
  o_MFCDSED4 = space(5)
  w_MFCCONT4 = space(5)
  o_MFCCONT4 = space(5)
  w_MFMINPS4 = space(17)
  w_APP4A = .F.
  o_APP4A = .F.
  w_MFDAMES4 = space(2)
  w_APP4 = .F.
  o_APP4 = .F.
  w_MFDAANN4 = space(4)
  w_MF_AMES4 = space(2)
  w_MFAN4 = space(4)
  w_MFIMPSD4 = 0
  w_MFIMPSC4 = 0
  w_MFTOTDPS = 0
  w_MFTOTCPS = 0
  w_MFSALDPS = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_GENINPS = space(10)
  w_IMPINPS = .F.
  w_MFSERIAL = space(10)
  w_MFCODRE1 = space(2)
  o_MFCODRE1 = space(2)
  w_MFTRIRE1 = space(5)
  o_MFTRIRE1 = space(5)
  w_MFRATRE1 = space(4)
  w_MFMESER1 = space(2)
  w_MFANNRE1 = space(4)
  w_MFIMDRE1 = 0
  w_MFIMCRE1 = 0
  w_MFCODRE2 = space(2)
  o_MFCODRE2 = space(2)
  w_MFTRIRE2 = space(5)
  o_MFTRIRE2 = space(5)
  w_MFRATRE2 = space(4)
  w_MFMESER2 = space(2)
  w_MFANNRE2 = space(4)
  w_MFIMDRE2 = 0
  w_MFIMCRE2 = 0
  w_MFCODRE3 = space(2)
  o_MFCODRE3 = space(2)
  w_MFTRIRE3 = space(5)
  o_MFTRIRE3 = space(5)
  w_MFRATRE3 = space(4)
  w_MFMESER3 = space(2)
  w_MFANNRE3 = space(4)
  w_MFIMDRE3 = 0
  w_MFIMCRE3 = 0
  w_MFCODRE4 = space(2)
  o_MFCODRE4 = space(2)
  w_MFTRIRE4 = space(5)
  o_MFTRIRE4 = space(5)
  w_MFRATRE4 = space(4)
  w_MFMESER4 = space(2)
  w_MFANNRE4 = space(4)
  w_MFIMDRE4 = 0
  w_MFIMCRE4 = 0
  w_MFTOTDRE = 0
  w_MFTOTCRE = 0
  w_MFSALDRE = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_MFSERIAL = space(10)
  w_MFSINAI1 = space(5)
  o_MFSINAI1 = space(5)
  w_MF_NPOS1 = space(8)
  w_MF_PACC1 = space(2)
  w_MF_NRIF1 = space(6)
  w_MFCAUSA1 = space(2)
  w_MFIMDIL1 = 0
  w_MFIMCIL1 = 0
  w_MFSINAI2 = space(5)
  o_MFSINAI2 = space(5)
  w_MF_NPOS2 = space(8)
  w_MF_PACC2 = space(2)
  w_MF_NRIF2 = space(6)
  w_MFCAUSA2 = space(2)
  w_MFIMDIL2 = 0
  w_MFIMCIL2 = 0
  w_MFSINAI3 = space(5)
  o_MFSINAI3 = space(5)
  w_MF_NPOS3 = space(8)
  w_MF_PACC3 = space(2)
  w_MF_NRIF3 = space(6)
  w_MFCAUSA3 = space(2)
  w_MFIMDIL3 = 0
  w_MFIMCIL3 = 0
  w_MFTDINAI = 0
  w_MFTCINAI = 0
  w_MFSALINA = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_MFSERIAL = space(10)
  w_MFCDENTE = space(5)
  o_MFCDENTE = space(5)
  w_VRSDENT1 = space(5)
  o_VRSDENT1 = space(5)
  w_VPSDENT1 = space(5)
  o_VPSDENT1 = space(5)
  w_MFSDENT1 = space(5)
  o_MFSDENT1 = space(5)
  w_MFCCOAE1 = space(5)
  o_MFCCOAE1 = space(5)
  w_OLSDENT1 = space(5)
  w_MFCDPOS1 = space(9)
  w_MFMSINE1 = space(2)
  w_MFANINE1 = space(4)
  w_MFMSFIE1 = space(2)
  w_MFANF1 = space(4)
  w_MFIMDAE1 = 0
  w_MFIMCAE1 = 0
  w_VPSDENT2 = space(5)
  o_VPSDENT2 = space(5)
  w_VRSDENT2 = space(5)
  o_VRSDENT2 = space(5)
  w_MFSDENT2 = space(5)
  o_MFSDENT2 = space(5)
  w_MFCCOAE2 = space(5)
  o_MFCCOAE2 = space(5)
  w_OLSDENT2 = space(5)
  w_MFCDPOS2 = space(9)
  w_MFMSINE2 = space(2)
  w_MFANINE2 = space(4)
  w_MFMSFIE2 = space(2)
  w_MFANF2 = space(4)
  w_MFIMDAE2 = 0
  w_MFIMCAE2 = 0
  w_MFTDAENT = 0
  w_MFTCAENT = 0
  w_MFSALAEN = 0
  w_DESAENTE = space(30)
  w_MESE = space(2)
  w_ANNO = space(4)
  w_GENPREV = space(10)
  w_RESCHK = 0
  w_IMPPREV = .F.
  w_MFSERIAL = space(10)
  w_MFSALFIN = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_TIPOMOD = space(3)
  w_APPOIMP2 = 0
  w_ANNORIF = space(4)
  w_TIPTRI = space(10)
  w_MFDESSTA = space(5)
  o_MFDESSTA = space(5)
  w_CAP1 = space(9)
  o_CAP1 = space(9)
  w_LOCALI1 = space(30)
  o_LOCALI1 = space(30)
  w_INDIRI1 = space(35)
  o_INDIRI1 = space(35)
  w_PROVIN1 = space(2)
  o_PROVIN1 = space(2)
  w_CAP = space(9)
  w_LOCALI = space(30)
  w_PROVIN = space(2)
  w_INDIRI = space(35)
  w_DESSTA = space(1)
  w_CODAZIE = space(5)
  w_MFTIPMOD = space(10)
  w_CHKOBBME1 = space(1)
  o_CHKOBBME1 = space(1)
  w_CHKOBBME2 = space(1)
  o_CHKOBBME2 = space(1)
  w_CHKOBBME3 = space(1)
  o_CHKOBBME3 = space(1)
  w_CHKOBBME4 = space(1)
  o_CHKOBBME4 = space(1)
  w_RAPFIRM = space(5)
  o_RAPFIRM = space(5)
  w_READAZI = space(10)
  w_AZCONBAN = space(15)
  w_MFCODFIR2 = space(16)
  w_MFCARFIR2 = space(2)
  w_MFCOGFIR2 = space(24)
  w_MFNOMFIR2 = space(20)
  w_MFSESFIR2 = space(1)
  w_MFDATFIR2 = ctod('  /  /  ')
  w_MFCOMFIR2 = space(40)
  w_MFPROFIR2 = space(2)
  w_MFDOMFIR2 = space(40)
  w_MFPRDFIR2 = space(2)
  w_MFCAPFIR2 = space(5)
  w_MFINDFIR2 = space(35)
  w_MFCODFIR = space(16)
  w_MFCHKFIR = space(1)
  o_MFCHKFIR = space(1)
  w_MFCARFIR = space(2)
  o_MFCARFIR = space(2)
  w_MFCOGFIR = space(24)
  w_MFNOMFIR = space(20)
  w_MFSESFIR = space(1)
  w_MFDATFIR = ctod('  /  /  ')
  w_MFCOMFIR = space(40)
  w_MFPROFIR = space(2)
  w_MFDOMFIR = space(40)
  w_MFPRDFIR = space(2)
  w_MFCAPFIR = space(5)
  w_MFINDFIR = space(35)
  w_MFENTRAT = space(1)
  w_CFPERFIS = space(1)
  w_MFMAIVER = space(60)
  w_MFBANPAS = space(15)
  o_MFBANPAS = space(15)
  w_BADESCRI = space(35)
  w_MFCOIBAN = space(34)
  w_MFSERIAL = space(10)
  w_MFSALFIN = 0
  w_MESE = space(2)
  w_ANNO = space(4)
  w_BACONSBF = space(1)
  w_DATOBSO1 = ctod('  /  /  ')
  w_BATIPCON = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_MFCOIBAN = space(34)
  w_IBAN = space(35)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_MFSERIAL = this.W_MFSERIAL

  * --- Children pointers
  GSCG_AFR = .NULL.
  GSCG_AVF = .NULL.
  GSCG_AIG = .NULL.
  GSCG_ACG = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscg_aop
  w_ENTRATEL = ' '
  w_ANNOMOD=' '
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=9, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOD_PAG','gscg_aop')
    stdPageFrame::Init()
    *set procedure to GSCG_ACG additive
    with this
      .Pages(1).addobject("oPag","tgscg_aopPag1","gscg_aop",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Contribuente")
      .Pages(1).HelpContextID = 108913387
      .Pages(2).addobject("oPag","tgscg_aopPag2","gscg_aop",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Erario")
      .Pages(2).HelpContextID = 75603642
      .Pages(3).addobject("oPag","tgscg_aopPag3","gscg_aop",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("INPS")
      .Pages(3).HelpContextID = 97398150
      .Pages(4).addobject("oPag","tgscg_aopPag4","gscg_aop",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Regioni enti locali")
      .Pages(4).HelpContextID = 112366599
      .Pages(5).addobject("oPag","tgscg_aopPag5","gscg_aop",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("INAIL")
      .Pages(5).HelpContextID = 176373126
      .Pages(6).addobject("oPag","tgscg_aopPag6","gscg_aop",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Altri enti")
      .Pages(6).HelpContextID = 58721356
      .Pages(7).addobject("oPag","tgscg_aopPag7","gscg_aop",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Estr.versamento")
      .Pages(7).HelpContextID = 98841880
      .Pages(8).addobject("oPag","tgscg_aopPag8","gscg_aop",8)
      .Pages(8).oPag.Visible=.t.
      .Pages(8).Caption=cp_Translate("Versante/Firmatario")
      .Pages(8).HelpContextID = 229597396
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMFSERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCG_ACG
    * --- Area Manuale = Init Page Frame
    * --- gscg_aop
    WITH THIS.PARENT
       .w_ENTRATEL=pEntCbi
       .w_ANNOMOD=pAnno
       IF .w_ENTRATEL='E'
          .cComment= ah_msgformat("Modello pagamento F24/%1 Entratel",.w_ANNOMOD)
          IF .w_ANNOMOD='2013'
          .cAutoZoom = 'GSCG1AOP'
       ELSE
          .cAutoZoom = 'GSCG2AOP'
          ENDIF
       ELSE
          .cComment= ah_msgformat("Modello pagamento F24/%1 CBI",.w_ANNOMOD)
          IF .w_ANNOMOD='2013'
          .cAutoZoom = 'GSCG1AFN'
          ELSE
          .cAutoZoom = 'GSCG2AFN'
          ENDIF
       ENDIF
       .w_MFENTRAT=pEntCbi
    ENDWITH
    
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[24]
    this.cWorkTables[1]='TRI_BUTI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='TITOLARI'
    this.cWorkTables[4]='COD_TRIB'
    this.cWorkTables[5]='CODI_UFF'
    this.cWorkTables[6]='SED_INPS'
    this.cWorkTables[7]='CAU_INPS'
    this.cWorkTables[8]='REG_PROV'
    this.cWorkTables[9]='ENTI_LOC'
    this.cWorkTables[10]='SE_INAIL'
    this.cWorkTables[11]='COD_PREV'
    this.cWorkTables[12]='SED_AEN'
    this.cWorkTables[13]='CAU_AEN'
    this.cWorkTables[14]='MODVPAG'
    this.cWorkTables[15]='MODCPAG'
    this.cWorkTables[16]='MODEPAG'
    this.cWorkTables[17]='VALUTE'
    this.cWorkTables[18]='CPUSERS'
    this.cWorkTables[19]='MODIPAG'
    this.cWorkTables[20]='SEDIAZIE'
    this.cWorkTables[21]='ANAG_PRO'
    this.cWorkTables[22]='DAT_RAPP'
    this.cWorkTables[23]='COC_MAST'
    this.cWorkTables[24]='MOD_PAG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(24))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOD_PAG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOD_PAG_IDX,3]
  return

  function CreateChildren()
    this.GSCG_AFR = CREATEOBJECT('stdDynamicChild',this,'GSCG_AFR',this.oPgFrm.Page2.oPag.oLinkPC_2_2)
    this.GSCG_AVF = CREATEOBJECT('stdDynamicChild',this,'GSCG_AVF',this.oPgFrm.Page7.oPag.oLinkPC_7_3)
    this.GSCG_AIG = CREATEOBJECT('stdDynamicChild',this,'GSCG_AIG',this.oPgFrm.Page4.oPag.oLinkPC_4_45)
    this.GSCG_ACG = CREATEOBJECT('stdDynamicChild',this,'GSCG_ACG',this.oPgFrm.Page1.oPag.oLinkPC_1_18)
    this.GSCG_ACG.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCG_AFR)
      this.GSCG_AFR.DestroyChildrenChain()
      this.GSCG_AFR=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_2')
    if !ISNULL(this.GSCG_AVF)
      this.GSCG_AVF.DestroyChildrenChain()
      this.GSCG_AVF=.NULL.
    endif
    this.oPgFrm.Page7.oPag.RemoveObject('oLinkPC_7_3')
    if !ISNULL(this.GSCG_AIG)
      this.GSCG_AIG.DestroyChildrenChain()
      this.GSCG_AIG=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_45')
    if !ISNULL(this.GSCG_ACG)
      this.GSCG_ACG.DestroyChildrenChain()
      this.GSCG_ACG=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_18')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_AFR.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_AVF.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_AIG.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_ACG.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_AFR.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_AVF.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_AIG.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_ACG.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_AFR.NewDocument()
    this.GSCG_AVF.NewDocument()
    this.GSCG_AIG.NewDocument()
    this.GSCG_ACG.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCG_AFR.SetKey(;
            .w_MFSERIAL,"EFSERIAL";
            )
      this.GSCG_AVF.SetKey(;
            .w_MFSERIAL,"VFSERIAL";
            )
      this.GSCG_AIG.SetKey(;
            .w_MFSERIAL,"IFSERIAL";
            )
      this.GSCG_ACG.SetKey(;
            .w_MFSERIAL,"CFSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCG_AFR.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"EFSERIAL";
             )
      .GSCG_AVF.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"VFSERIAL";
             )
      .GSCG_AIG.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"IFSERIAL";
             )
      .GSCG_ACG.ChangeRow(this.cRowID+'      1',1;
             ,.w_MFSERIAL,"CFSERIAL";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCG_AFR)
        i_f=.GSCG_AFR.BuildFilter()
        if !(i_f==.GSCG_AFR.cQueryFilter)
          i_fnidx=.GSCG_AFR.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_AFR.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_AFR.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_AFR.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_AFR.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_AVF)
        i_f=.GSCG_AVF.BuildFilter()
        if !(i_f==.GSCG_AVF.cQueryFilter)
          i_fnidx=.GSCG_AVF.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_AVF.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_AVF.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_AVF.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_AVF.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_AIG)
        i_f=.GSCG_AIG.BuildFilter()
        if !(i_f==.GSCG_AIG.cQueryFilter)
          i_fnidx=.GSCG_AIG.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_AIG.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_AIG.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_AIG.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_AIG.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_ACG)
        i_f=.GSCG_ACG.BuildFilter()
        if !(i_f==.GSCG_ACG.cQueryFilter)
          i_fnidx=.GSCG_ACG.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_ACG.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_ACG.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_ACG.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_ACG.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MFSERIAL = NVL(MFSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_4_3_joined
    link_4_3_joined=.f.
    local link_4_13_joined
    link_4_13_joined=.f.
    local link_4_20_joined
    link_4_20_joined=.f.
    local link_4_27_joined
    link_4_27_joined=.f.
    local link_6_3_joined
    link_6_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MOD_PAG where MFSERIAL=KeySet.MFSERIAL
    *
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOD_PAG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOD_PAG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOD_PAG '
      link_4_3_joined=this.AddJoinedLink_4_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_13_joined=this.AddJoinedLink_4_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_20_joined=this.AddJoinedLink_4_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_27_joined=this.AddJoinedLink_4_27(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_6_3_joined=this.AddJoinedLink_6_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MFSERIAL',this.w_MFSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_APPCOIN = space(1)
        .w_APPOIMP = 0
        .w_GENERA = Sys(2015)
        .w_IMPORTA = .F.
        .w_GENINPS = Sys(2015)
        .w_IMPINPS = .F.
        .w_DESAENTE = space(30)
        .w_GENPREV = Sys(2015)
        .w_RESCHK = 0
        .w_IMPPREV = .F.
        .w_TIPOMOD = '007'
        .w_APPOIMP2 = 0
        .w_ANNORIF = '2003'
        .w_TIPTRI = space(10)
        .w_CAP1 = space(9)
        .w_LOCALI1 = space(30)
        .w_INDIRI1 = space(35)
        .w_PROVIN1 = space(2)
        .w_DESSTA = space(1)
        .w_CODAZIE = i_CODAZI
        .w_CHKOBBME1 = space(1)
        .w_CHKOBBME2 = space(1)
        .w_CHKOBBME3 = space(1)
        .w_CHKOBBME4 = space(1)
        .w_RAPFIRM = i_CODAZI
        .w_AZCONBAN = space(15)
        .w_MFCODFIR2 = space(16)
        .w_MFCARFIR2 = space(2)
        .w_MFCOGFIR2 = space(24)
        .w_MFNOMFIR2 = space(20)
        .w_MFSESFIR2 = space(1)
        .w_MFDATFIR2 = ctod("  /  /  ")
        .w_MFCOMFIR2 = space(40)
        .w_MFPROFIR2 = space(2)
        .w_MFDOMFIR2 = space(40)
        .w_MFPRDFIR2 = space(2)
        .w_MFCAPFIR2 = space(5)
        .w_MFINDFIR2 = space(35)
        .w_CFPERFIS = space(1)
        .w_MFBANPAS = .w_AZCONBAN
        .w_BADESCRI = space(35)
        .w_BACONSBF = space(1)
        .w_DATOBSO1 = ctod("  /  /  ")
        .w_BATIPCON = space(1)
        .w_OBTEST = i_DATSYS
        .w_IBAN = space(35)
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFMESRIF = NVL(MFMESRIF,space(2))
        .w_MFANNRIF = NVL(MFANNRIF,space(4))
        .w_MFVALUTA = NVL(MFVALUTA,space(3))
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .w_MF_COINC = NVL(MF_COINC,space(1))
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCODUFF = NVL(MFCODUFF,space(3))
          * evitabile
          *.link_2_4('Load')
        .w_MFCODATT = NVL(MFCODATT,space(11))
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .oPgFrm.Page2.oPag.oObj_2_13.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_15.Calculate()
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCDSED1 = NVL(MFCDSED1,space(5))
          * evitabile
          *.link_3_2('Load')
        .w_MFCCONT1 = NVL(MFCCONT1,space(5))
          * evitabile
          *.link_3_3('Load')
        .w_MFMINPS1 = NVL(MFMINPS1,space(17))
        .w_APP1A = !empty(.w_MFCCONT1) and alltrim(.w_MFCCONT1)$ 'ABR-ABS-AC-ACON-DOM2-EMI-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-DSOS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_APP1 = !empty(.w_MFCCONT1) and (alltrim(.w_MFCCONT1)$  ('AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBAT-EBTU-EMCO-EMDM-EMLA-FIPP-HTL1-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-IADP-ENBA-CCNL-ART1-ACIM-TCEB-SADP-SADD-PANE-EBA1'+'-EDFO-ENBC-ENBF-ENBI-FEDA-OPNC-CADD-EBNA-VITA-EBUC-CIFE-EST1-EBPA-EBCE-TUEB-LACC-FOIN-EBAR-ONBS'+'-ENFE-EBTS-MI53-EBIL-CUST-PULI-CLIS-EBAG-EBAN-EBMS-EBSA-EBTR-ENBL-EPAR-FSL1-EBMC-EART'+'-QUAS-EBIN-EBAS-EBIP-ASIM-EBIT-EBI1-EFEI-FASD-EBI7-EBG9-EBIM-1AST-EBAI-EBTI-EB04-RCAD-CALL-ENBT-EBCT-ANBA-EBLC-ENBP-ENP1-RCSO-EBNS-EBCA-BINT-EBCO-EBAP-EBFC-EBLE-EBFO-ASI1-EBIC-EBG1-ENB1-MET1-EBCD-ELAV-EBFW-FAST-FOSI-ENAB-EBIS-EBSP-EBIG-OBIL-EBIF'+'-ASS1-DM10-LAV1') or .w_APP1A)
        .w_MFDAMES1 = NVL(MFDAMES1,space(2))
        .w_MFDAANN1 = NVL(MFDAANN1,space(4))
        .w_MF_AMES1 = NVL(MF_AMES1,space(2))
        .w_MFAN1 = NVL(MFAN1,space(4))
        .w_MFIMPSD1 = NVL(MFIMPSD1,0)
        .w_MFIMPSC1 = NVL(MFIMPSC1,0)
        .w_MFCDSED2 = NVL(MFCDSED2,space(5))
          * evitabile
          *.link_3_13('Load')
        .w_MFCCONT2 = NVL(MFCCONT2,space(5))
          * evitabile
          *.link_3_14('Load')
        .w_MFMINPS2 = NVL(MFMINPS2,space(17))
        .w_APP2A = !empty(.w_MFCCONT2) and alltrim(.w_MFCCONT2)$'ABR-ABS-AC-ACON-DOM2-EMI-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-DSOS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES2 = NVL(MFDAMES2,space(2))
        .w_MFDAANN2 = NVL(MFDAANN2,space(4))
        .w_APP2 = !empty(.w_MFCCONT2) and (alltrim(.w_MFCCONT2)$  ('AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBAT-EBTU-EMCO-EMDM-EMLA-FIPP-HTL1-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-IADP-ENBA-CCNL-ART1-ACIM-TCEB-SADP-SADD-PANE-EBA1'+'-EDFO-ENBC-ENBF-ENBI-FEDA-OPNC-CADD-EBNA-VITA-EBUC-CIFE-EST1-EBPA-EBCE-TUEB-LACC-FOIN-EBAR-ONBS'+'-ENFE-EBTS-MI53-EBIL-CUST-PULI-CLIS-EBAG-EBAN-EBMS-EBSA-EBTR-ENBL-EPAR-FSL1-EBMC-EART'+'-QUAS-EBIN-EBAS-EBIP-ASIM-EBIT-EBI1-EFEI-FASD-EBI7-EBG9-EBIM-1AST-EBAI-EBTI-EB04-RCAD-CALL-ENBT-EBCT-ANBA-EBLC-ENBP-ENP1-RCSO-EBNS-EBCA-BINT-EBCO-EBAP-EBFC-EBLE-EBFO-ASI1-EBIC-EBG1-ENB1-MET1-EBCD-ELAV-EBFW-FAST-FOSI-ENAB-EBIS-EBSP-EBIG-OBIL-EBIF'+'-ASS1-DM10-LAV1') or .w_APP2A)
        .w_MF_AMES2 = NVL(MF_AMES2,space(2))
        .w_MFAN2 = NVL(MFAN2,space(4))
        .w_MFIMPSD2 = NVL(MFIMPSD2,0)
        .w_MFIMPSC2 = NVL(MFIMPSC2,0)
        .w_MFCDSED3 = NVL(MFCDSED3,space(5))
          * evitabile
          *.link_3_24('Load')
        .w_MFCCONT3 = NVL(MFCCONT3,space(5))
          * evitabile
          *.link_3_25('Load')
        .w_MFMINPS3 = NVL(MFMINPS3,space(17))
        .w_APP3A = !empty(.w_MFCCONT3) and alltrim(.w_MFCCONT3)$ 'ABR-ABS-AC-ACON-DOM2-EMI-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-DSOS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES3 = NVL(MFDAMES3,space(2))
        .w_APP3 = !empty(.w_MFCCONT3) and (alltrim(.w_MFCCONT3)$  ('AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBAT-EBTU-EMCO-EMDM-EMLA-FIPP-HTL1-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-IADP-ENBA-CCNL-ART1-ACIM-TCEB-SADP-SADD-PANE-EBA1'+'-EDFO-ENBC-ENBF-ENBI-FEDA-OPNC-CADD-EBNA-VITA-EBUC-CIFE-EST1-EBPA-EBCE-TUEB-LACC-FOIN-EBAR-ONBS'+'-ENFE-EBTS-MI53-EBIL-CUST-PULI-CLIS-EBAG-EBAN-EBMS-EBSA-EBTR-ENBL-EPAR-FSL1-EBMC-EART'+'-QUAS-EBIN-EBAS-EBIP-ASIM-EBIT-EBI1-EFEI-FASD-EBI7-EBG9-EBIM-1AST-EBAI-EBTI-EB04-RCAD-CALL-ENBT-EBCT-ANBA-EBLC-ENBP-ENP1-RCSO-EBNS-EBCA-BINT-EBCO-EBAP-EBFC-EBLE-EBFO-ASI1-EBIC-EBG1-ENB1-MET1-EBCD-ELAV-EBFW-FAST-FOSI-ENAB-EBIS-EBSP-EBIG-OBIL-EBIF'+'-ASS1-DM10-LAV1') or .w_APP3A)
        .w_MFDAANN3 = NVL(MFDAANN3,space(4))
        .w_MF_AMES3 = NVL(MF_AMES3,space(2))
        .w_MFAN3 = NVL(MFAN3,space(4))
        .w_MFIMPSD3 = NVL(MFIMPSD3,0)
        .w_MFIMPSC3 = NVL(MFIMPSC3,0)
        .w_MFCDSED4 = NVL(MFCDSED4,space(5))
          * evitabile
          *.link_3_35('Load')
        .w_MFCCONT4 = NVL(MFCCONT4,space(5))
          * evitabile
          *.link_3_36('Load')
        .w_MFMINPS4 = NVL(MFMINPS4,space(17))
        .w_APP4A = !empty(.w_MFCCONT4) and alltrim(.w_MFCCONT4)$ 'ABR-ABS-AC-ACON-DOM2-EMI-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-DSOS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES4 = NVL(MFDAMES4,space(2))
        .w_APP4 = !empty(.w_MFCCONT4) and (alltrim(.w_MFCCONT4)$  ('AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBAT-EBTU-EMCO-EMDM-EMLA-FIPP-HTL1-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-IADP-ENBA-CCNL-ART1-ACIM-TCEB-SADP-SADD-PANE-EBA1'+'-EDFO-ENBC-ENBF-ENBI-FEDA-OPNC-CADD-EBNA-VITA-EBUC-CIFE-EST1-EBPA-EBCE-TUEB-LACC-FOIN-EBAR-ONBS'+'-ENFE-EBTS-MI53-EBIL-CUST-PULI-CLIS-EBAG-EBAN-EBMS-EBSA-EBTR-ENBL-EPAR-FSL1-EBMC-EART'+'-QUAS-EBIN-EBAS-EBIP-ASIM-EBIT-EBI1-EFEI-FASD-EBI7-EBG9-EBIM-1AST-EBAI-EBTI-EB04-RCAD-CALL-ENBT-EBCT-ANBA-EBLC-ENBP-ENP1-RCSO-EBNS-EBCA-BINT-EBCO-EBAP-EBFC-EBLE-EBFO-ASI1-EBIC-EBG1-ENB1-MET1-EBCD-ELAV-EBFW-FAST-FOSI-ENAB-EBIS-EBSP-EBIG-OBIL-EBIF'+'-ASS1-DM10-LAV1') or .w_APP4A)
        .w_MFDAANN4 = NVL(MFDAANN4,space(4))
        .w_MF_AMES4 = NVL(MF_AMES4,space(2))
        .w_MFAN4 = NVL(MFAN4,space(4))
        .w_MFIMPSD4 = NVL(MFIMPSD4,0)
        .w_MFIMPSC4 = NVL(MFIMPSC4,0)
        .w_MFTOTDPS = NVL(MFTOTDPS,0)
        .w_MFTOTCPS = NVL(MFTOTCPS,0)
        .w_MFSALDPS = NVL(MFSALDPS,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCODRE1 = NVL(MFCODRE1,space(2))
          * evitabile
          *.link_4_2('Load')
        .w_MFTRIRE1 = NVL(MFTRIRE1,space(5))
          if link_4_3_joined
            this.w_MFTRIRE1 = NVL(TRCODICE403,NVL(this.w_MFTRIRE1,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI403,space(10))
            this.w_CHKOBBME1 = NVL(TRFLMERF403,space(1))
          else
          .link_4_3('Load')
          endif
        .w_MFRATRE1 = NVL(MFRATRE1,space(4))
        .w_MFMESER1 = NVL(MFMESER1,space(2))
        .w_MFANNRE1 = NVL(MFANNRE1,space(4))
        .w_MFIMDRE1 = NVL(MFIMDRE1,0)
        .w_MFIMCRE1 = NVL(MFIMCRE1,0)
        .w_MFCODRE2 = NVL(MFCODRE2,space(2))
          * evitabile
          *.link_4_12('Load')
        .w_MFTRIRE2 = NVL(MFTRIRE2,space(5))
          if link_4_13_joined
            this.w_MFTRIRE2 = NVL(TRCODICE413,NVL(this.w_MFTRIRE2,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI413,space(10))
            this.w_CHKOBBME2 = NVL(TRFLMERF413,space(1))
          else
          .link_4_13('Load')
          endif
        .w_MFRATRE2 = NVL(MFRATRE2,space(4))
        .w_MFMESER2 = NVL(MFMESER2,space(2))
        .w_MFANNRE2 = NVL(MFANNRE2,space(4))
        .w_MFIMDRE2 = NVL(MFIMDRE2,0)
        .w_MFIMCRE2 = NVL(MFIMCRE2,0)
        .w_MFCODRE3 = NVL(MFCODRE3,space(2))
          * evitabile
          *.link_4_19('Load')
        .w_MFTRIRE3 = NVL(MFTRIRE3,space(5))
          if link_4_20_joined
            this.w_MFTRIRE3 = NVL(TRCODICE420,NVL(this.w_MFTRIRE3,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI420,space(10))
            this.w_CHKOBBME3 = NVL(TRFLMERF420,space(1))
          else
          .link_4_20('Load')
          endif
        .w_MFRATRE3 = NVL(MFRATRE3,space(4))
        .w_MFMESER3 = NVL(MFMESER3,space(2))
        .w_MFANNRE3 = NVL(MFANNRE3,space(4))
        .w_MFIMDRE3 = NVL(MFIMDRE3,0)
        .w_MFIMCRE3 = NVL(MFIMCRE3,0)
        .w_MFCODRE4 = NVL(MFCODRE4,space(2))
          * evitabile
          *.link_4_26('Load')
        .w_MFTRIRE4 = NVL(MFTRIRE4,space(5))
          if link_4_27_joined
            this.w_MFTRIRE4 = NVL(TRCODICE427,NVL(this.w_MFTRIRE4,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI427,space(10))
            this.w_CHKOBBME4 = NVL(TRFLMERF427,space(1))
          else
          .link_4_27('Load')
          endif
        .w_MFRATRE4 = NVL(MFRATRE4,space(4))
        .w_MFMESER4 = NVL(MFMESER4,space(2))
        .w_MFANNRE4 = NVL(MFANNRE4,space(4))
        .w_MFIMDRE4 = NVL(MFIMDRE4,0)
        .w_MFIMCRE4 = NVL(MFIMCRE4,0)
        .w_MFTOTDRE = NVL(MFTOTDRE,0)
        .w_MFTOTCRE = NVL(MFTOTCRE,0)
        .w_MFSALDRE = NVL(MFSALDRE,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFSINAI1 = NVL(MFSINAI1,space(5))
          * evitabile
          *.link_5_2('Load')
        .w_MF_NPOS1 = NVL(MF_NPOS1,space(8))
        .w_MF_PACC1 = NVL(MF_PACC1,space(2))
        .w_MF_NRIF1 = NVL(MF_NRIF1,space(6))
        .w_MFCAUSA1 = NVL(MFCAUSA1,space(2))
        .w_MFIMDIL1 = NVL(MFIMDIL1,0)
        .w_MFIMCIL1 = NVL(MFIMCIL1,0)
        .w_MFSINAI2 = NVL(MFSINAI2,space(5))
          * evitabile
          *.link_5_9('Load')
        .w_MF_NPOS2 = NVL(MF_NPOS2,space(8))
        .w_MF_PACC2 = NVL(MF_PACC2,space(2))
        .w_MF_NRIF2 = NVL(MF_NRIF2,space(6))
        .w_MFCAUSA2 = NVL(MFCAUSA2,space(2))
        .w_MFIMDIL2 = NVL(MFIMDIL2,0)
        .w_MFIMCIL2 = NVL(MFIMCIL2,0)
        .w_MFSINAI3 = NVL(MFSINAI3,space(5))
          * evitabile
          *.link_5_16('Load')
        .w_MF_NPOS3 = NVL(MF_NPOS3,space(8))
        .w_MF_PACC3 = NVL(MF_PACC3,space(2))
        .w_MF_NRIF3 = NVL(MF_NRIF3,space(6))
        .w_MFCAUSA3 = NVL(MFCAUSA3,space(2))
        .w_MFIMDIL3 = NVL(MFIMDIL3,0)
        .w_MFIMCIL3 = NVL(MFIMCIL3,0)
        .w_MFTDINAI = NVL(MFTDINAI,0)
        .w_MFTCINAI = NVL(MFTCINAI,0)
        .w_MFSALINA = NVL(MFSALINA,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFCDENTE = NVL(MFCDENTE,space(5))
          if link_6_3_joined
            this.w_MFCDENTE = NVL(CPCODICE603,NVL(this.w_MFCDENTE,space(5)))
            this.w_DESAENTE = NVL(CPDESCRI603,space(30))
          else
          .link_6_3('Load')
          endif
        .w_VRSDENT1 = space(5)
          .link_6_4('Load')
        .w_VPSDENT1 = space(2)
          .link_6_5('Load')
        .w_MFSDENT1 = NVL(MFSDENT1,space(5))
        .w_MFCCOAE1 = NVL(MFCCOAE1,space(5))
          * evitabile
          *.link_6_7('Load')
        .w_OLSDENT1 = .w_MFSDENT1
        .w_MFCDPOS1 = NVL(MFCDPOS1,space(9))
        .w_MFMSINE1 = NVL(MFMSINE1,space(2))
        .w_MFANINE1 = NVL(MFANINE1,space(4))
        .w_MFMSFIE1 = NVL(MFMSFIE1,space(2))
        .w_MFANF1 = NVL(MFANF1,space(4))
        .w_MFIMDAE1 = NVL(MFIMDAE1,0)
        .w_MFIMCAE1 = NVL(MFIMCAE1,0)
        .w_VPSDENT2 = space(2)
          .link_6_16('Load')
        .w_VRSDENT2 = space(5)
          .link_6_17('Load')
        .w_MFSDENT2 = NVL(MFSDENT2,space(5))
        .w_MFCCOAE2 = NVL(MFCCOAE2,space(5))
          * evitabile
          *.link_6_19('Load')
        .w_OLSDENT2 = .w_MFSDENT2
        .w_MFCDPOS2 = NVL(MFCDPOS2,space(9))
        .w_MFMSINE2 = NVL(MFMSINE2,space(2))
        .w_MFANINE2 = NVL(MFANINE2,space(4))
        .w_MFMSFIE2 = NVL(MFMSFIE2,space(2))
        .w_MFANF2 = NVL(MFANF2,space(4))
        .w_MFIMDAE2 = NVL(MFIMDAE2,0)
        .w_MFIMCAE2 = NVL(MFIMCAE2,0)
        .w_MFTDAENT = NVL(MFTDAENT,0)
        .w_MFTCAENT = NVL(MFTCAENT,0)
        .w_MFSALAEN = NVL(MFSALAEN,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFSALFIN = NVL(MFSALFIN,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .oPgFrm.Page6.oPag.oObj_6_51.Calculate()
        .w_MFDESSTA = NVL(MFDESSTA,space(5))
          .link_7_10('Load')
        .w_CAP = iif(empty(NVL(.w_MFDESSTA,'')),g_capazi,.w_CAP1)
        .w_LOCALI = UPPER(iif(EMPTY(NVL(.w_MFDESSTA,'')),g_locazi,.w_LOCALI1))
        .w_PROVIN = UPPER(iif(empty(NVL(.w_MFDESSTA,'')),g_proazi,.w_PROVIN1))
        .w_INDIRI = UPPER(iif(empty(NVL(.w_MFDESSTA,'')),g_indazi,.w_INDIRI1))
        .w_MFTIPMOD = NVL(MFTIPMOD,space(10))
        .oPgFrm.Page4.oPag.oObj_4_52.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page4.oPag.oObj_4_53.Calculate(ah_MSGFORMAT('Mese di %0riferimento') )
        .oPgFrm.Page4.oPag.oObj_4_54.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page4.oPag.oObj_4_55.Calculate(ah_MSGFORMAT('Codice %0regione'))
        .oPgFrm.Page3.oPag.oObj_3_68.Calculate(ah_MSGFORMAT('Codice %0sede'))
        .oPgFrm.Page3.oPag.oObj_3_69.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page5.oPag.oObj_5_42.Calculate(ah_MSGFORMAT('Numero di %0riferimento'))
        .oPgFrm.Page6.oPag.oObj_6_52.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page6.oPag.oObj_6_53.Calculate(iif(.w_MFCDENTE<>'0003' AND .w_MFCDENTE<>'0005'  AND .w_MFCDENTE<>'0006' ,ah_MSGFORMAT('Codice %0sede'),ah_MSGFORMAT('Codice %0provincia')))
        .oPgFrm.Page6.oPag.oObj_6_54.Calculate(ah_MSGFORMAT('Codice %0posizione'))
          .link_8_1('Load')
        .w_READAZI = i_codazi
          .link_8_2('Load')
        .w_MFCODFIR = NVL(MFCODFIR,space(16))
        .w_MFCHKFIR = NVL(MFCHKFIR,space(1))
        .w_MFCARFIR = NVL(MFCARFIR,space(2))
        .w_MFCOGFIR = NVL(MFCOGFIR,space(24))
        .w_MFNOMFIR = NVL(MFNOMFIR,space(20))
        .w_MFSESFIR = NVL(MFSESFIR,space(1))
        .w_MFDATFIR = NVL(cp_ToDate(MFDATFIR),ctod("  /  /  "))
        .w_MFCOMFIR = NVL(MFCOMFIR,space(40))
        .w_MFPROFIR = NVL(MFPROFIR,space(2))
        .w_MFDOMFIR = NVL(MFDOMFIR,space(40))
        .w_MFPRDFIR = NVL(MFPRDFIR,space(2))
        .w_MFCAPFIR = NVL(MFCAPFIR,space(5))
        .w_MFINDFIR = NVL(MFINDFIR,space(35))
        .w_MFENTRAT = NVL(MFENTRAT,space(1))
        .w_MFMAIVER = NVL(MFMAIVER,space(60))
          .link_8_45('Load')
        .w_MFCOIBAN = NVL(MFCOIBAN,space(34))
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .w_MFSERIAL = NVL(MFSERIAL,space(10))
        .op_MFSERIAL = .w_MFSERIAL
        .w_MFSALFIN = NVL(MFSALFIN,0)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_MFCOIBAN = NVL(MFCOIBAN,space(34))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'MOD_PAG')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page7.oPag.oBtn_7_27.enabled = this.oPgFrm.Page7.oPag.oBtn_7_27.mCond()
      this.oPgFrm.Page8.oPag.oBtn_8_50.enabled = this.oPgFrm.Page8.oPag.oBtn_8_50.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gscg_aop
    this.w_VPSDENT1=this.w_OLSDENT1
    this.w_VRSDENT1=this.w_OLSDENT1
    this.w_VPSDENT2=this.w_OLSDENT2
    this.w_VRSDENT2=this.w_OLSDENT2
    this.SaveDependsOn()
    this.SetControlsValue()
    if this.GSCG_AVF.w_VFDTPRES <= cp_CharToDate('31-12-2005')
      this.BlankRec()
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MFSERIAL = space(10)
      .w_MFMESRIF = space(2)
      .w_MFANNRIF = space(4)
      .w_MFVALUTA = space(3)
      .w_APPCOIN = space(1)
      .w_MF_COINC = space(1)
      .w_MFSERIAL = space(10)
      .w_APPOIMP = 0
      .w_MFCODUFF = space(3)
      .w_MFCODATT = space(11)
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_GENERA = space(10)
      .w_IMPORTA = .f.
      .w_MFSERIAL = space(10)
      .w_MFCDSED1 = space(5)
      .w_MFCCONT1 = space(5)
      .w_MFMINPS1 = space(17)
      .w_APP1A = .f.
      .w_APP1 = .f.
      .w_MFDAMES1 = space(2)
      .w_MFDAANN1 = space(4)
      .w_MF_AMES1 = space(2)
      .w_MFAN1 = space(4)
      .w_MFIMPSD1 = 0
      .w_MFIMPSC1 = 0
      .w_MFCDSED2 = space(5)
      .w_MFCCONT2 = space(5)
      .w_MFMINPS2 = space(17)
      .w_APP2A = .f.
      .w_MFDAMES2 = space(2)
      .w_MFDAANN2 = space(4)
      .w_APP2 = .f.
      .w_MF_AMES2 = space(2)
      .w_MFAN2 = space(4)
      .w_MFIMPSD2 = 0
      .w_MFIMPSC2 = 0
      .w_MFCDSED3 = space(5)
      .w_MFCCONT3 = space(5)
      .w_MFMINPS3 = space(17)
      .w_APP3A = .f.
      .w_MFDAMES3 = space(2)
      .w_APP3 = .f.
      .w_MFDAANN3 = space(4)
      .w_MF_AMES3 = space(2)
      .w_MFAN3 = space(4)
      .w_MFIMPSD3 = 0
      .w_MFIMPSC3 = 0
      .w_MFCDSED4 = space(5)
      .w_MFCCONT4 = space(5)
      .w_MFMINPS4 = space(17)
      .w_APP4A = .f.
      .w_MFDAMES4 = space(2)
      .w_APP4 = .f.
      .w_MFDAANN4 = space(4)
      .w_MF_AMES4 = space(2)
      .w_MFAN4 = space(4)
      .w_MFIMPSD4 = 0
      .w_MFIMPSC4 = 0
      .w_MFTOTDPS = 0
      .w_MFTOTCPS = 0
      .w_MFSALDPS = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_GENINPS = space(10)
      .w_IMPINPS = .f.
      .w_MFSERIAL = space(10)
      .w_MFCODRE1 = space(2)
      .w_MFTRIRE1 = space(5)
      .w_MFRATRE1 = space(4)
      .w_MFMESER1 = space(2)
      .w_MFANNRE1 = space(4)
      .w_MFIMDRE1 = 0
      .w_MFIMCRE1 = 0
      .w_MFCODRE2 = space(2)
      .w_MFTRIRE2 = space(5)
      .w_MFRATRE2 = space(4)
      .w_MFMESER2 = space(2)
      .w_MFANNRE2 = space(4)
      .w_MFIMDRE2 = 0
      .w_MFIMCRE2 = 0
      .w_MFCODRE3 = space(2)
      .w_MFTRIRE3 = space(5)
      .w_MFRATRE3 = space(4)
      .w_MFMESER3 = space(2)
      .w_MFANNRE3 = space(4)
      .w_MFIMDRE3 = 0
      .w_MFIMCRE3 = 0
      .w_MFCODRE4 = space(2)
      .w_MFTRIRE4 = space(5)
      .w_MFRATRE4 = space(4)
      .w_MFMESER4 = space(2)
      .w_MFANNRE4 = space(4)
      .w_MFIMDRE4 = 0
      .w_MFIMCRE4 = 0
      .w_MFTOTDRE = 0
      .w_MFTOTCRE = 0
      .w_MFSALDRE = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_MFSERIAL = space(10)
      .w_MFSINAI1 = space(5)
      .w_MF_NPOS1 = space(8)
      .w_MF_PACC1 = space(2)
      .w_MF_NRIF1 = space(6)
      .w_MFCAUSA1 = space(2)
      .w_MFIMDIL1 = 0
      .w_MFIMCIL1 = 0
      .w_MFSINAI2 = space(5)
      .w_MF_NPOS2 = space(8)
      .w_MF_PACC2 = space(2)
      .w_MF_NRIF2 = space(6)
      .w_MFCAUSA2 = space(2)
      .w_MFIMDIL2 = 0
      .w_MFIMCIL2 = 0
      .w_MFSINAI3 = space(5)
      .w_MF_NPOS3 = space(8)
      .w_MF_PACC3 = space(2)
      .w_MF_NRIF3 = space(6)
      .w_MFCAUSA3 = space(2)
      .w_MFIMDIL3 = 0
      .w_MFIMCIL3 = 0
      .w_MFTDINAI = 0
      .w_MFTCINAI = 0
      .w_MFSALINA = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_MFSERIAL = space(10)
      .w_MFCDENTE = space(5)
      .w_VRSDENT1 = space(5)
      .w_VPSDENT1 = space(5)
      .w_MFSDENT1 = space(5)
      .w_MFCCOAE1 = space(5)
      .w_OLSDENT1 = space(5)
      .w_MFCDPOS1 = space(9)
      .w_MFMSINE1 = space(2)
      .w_MFANINE1 = space(4)
      .w_MFMSFIE1 = space(2)
      .w_MFANF1 = space(4)
      .w_MFIMDAE1 = 0
      .w_MFIMCAE1 = 0
      .w_VPSDENT2 = space(5)
      .w_VRSDENT2 = space(5)
      .w_MFSDENT2 = space(5)
      .w_MFCCOAE2 = space(5)
      .w_OLSDENT2 = space(5)
      .w_MFCDPOS2 = space(9)
      .w_MFMSINE2 = space(2)
      .w_MFANINE2 = space(4)
      .w_MFMSFIE2 = space(2)
      .w_MFANF2 = space(4)
      .w_MFIMDAE2 = 0
      .w_MFIMCAE2 = 0
      .w_MFTDAENT = 0
      .w_MFTCAENT = 0
      .w_MFSALAEN = 0
      .w_DESAENTE = space(30)
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_GENPREV = space(10)
      .w_RESCHK = 0
      .w_IMPPREV = .f.
      .w_MFSERIAL = space(10)
      .w_MFSALFIN = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_TIPOMOD = space(3)
      .w_APPOIMP2 = 0
      .w_ANNORIF = space(4)
      .w_TIPTRI = space(10)
      .w_MFDESSTA = space(5)
      .w_CAP1 = space(9)
      .w_LOCALI1 = space(30)
      .w_INDIRI1 = space(35)
      .w_PROVIN1 = space(2)
      .w_CAP = space(9)
      .w_LOCALI = space(30)
      .w_PROVIN = space(2)
      .w_INDIRI = space(35)
      .w_DESSTA = space(1)
      .w_CODAZIE = space(5)
      .w_MFTIPMOD = space(10)
      .w_CHKOBBME1 = space(1)
      .w_CHKOBBME2 = space(1)
      .w_CHKOBBME3 = space(1)
      .w_CHKOBBME4 = space(1)
      .w_RAPFIRM = space(5)
      .w_READAZI = space(10)
      .w_AZCONBAN = space(15)
      .w_MFCODFIR2 = space(16)
      .w_MFCARFIR2 = space(2)
      .w_MFCOGFIR2 = space(24)
      .w_MFNOMFIR2 = space(20)
      .w_MFSESFIR2 = space(1)
      .w_MFDATFIR2 = ctod("  /  /  ")
      .w_MFCOMFIR2 = space(40)
      .w_MFPROFIR2 = space(2)
      .w_MFDOMFIR2 = space(40)
      .w_MFPRDFIR2 = space(2)
      .w_MFCAPFIR2 = space(5)
      .w_MFINDFIR2 = space(35)
      .w_MFCODFIR = space(16)
      .w_MFCHKFIR = space(1)
      .w_MFCARFIR = space(2)
      .w_MFCOGFIR = space(24)
      .w_MFNOMFIR = space(20)
      .w_MFSESFIR = space(1)
      .w_MFDATFIR = ctod("  /  /  ")
      .w_MFCOMFIR = space(40)
      .w_MFPROFIR = space(2)
      .w_MFDOMFIR = space(40)
      .w_MFPRDFIR = space(2)
      .w_MFCAPFIR = space(5)
      .w_MFINDFIR = space(35)
      .w_MFENTRAT = space(1)
      .w_CFPERFIS = space(1)
      .w_MFMAIVER = space(60)
      .w_MFBANPAS = space(15)
      .w_BADESCRI = space(35)
      .w_MFCOIBAN = space(34)
      .w_MFSERIAL = space(10)
      .w_MFSALFIN = 0
      .w_MESE = space(2)
      .w_ANNO = space(4)
      .w_BACONSBF = space(1)
      .w_DATOBSO1 = ctod("  /  /  ")
      .w_BATIPCON = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_MFCOIBAN = space(34)
      .w_IBAN = space(35)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_MFMESRIF = right('00'+ALLTRIM(STR(MONTH(i_datsys))),2)
        .w_MFANNRIF = ALLTRIM(STR(YEAR(i_datsys)))
        .w_MFVALUTA = g_CODEUR
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
          .DoRTCalc(5,5,.f.)
        .w_MF_COINC = .w_APPCOIN
        .DoRTCalc(7,9,.f.)
          if not(empty(.w_MFCODUFF))
          .link_2_4('Full')
          endif
          .DoRTCalc(10,10,.f.)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_GENERA = Sys(2015)
        .oPgFrm.Page2.oPag.oObj_2_13.Calculate()
        .w_IMPORTA = .F.
        .oPgFrm.Page2.oPag.oObj_2_15.Calculate()
        .DoRTCalc(15,16,.f.)
          if not(empty(.w_MFCDSED1))
          .link_3_2('Full')
          endif
        .w_MFCCONT1 = iif(empty(.w_MFCDSED1),' ',.w_MFCCONT1)
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_MFCCONT1))
          .link_3_3('Full')
          endif
        .w_MFMINPS1 = iif(empty(.w_MFCDSED1),' ',.w_MFMINPS1)
        .w_APP1A = !empty(.w_MFCCONT1) and alltrim(.w_MFCCONT1)$ 'ABR-ABS-AC-ACON-DOM2-EMI-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-DSOS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_APP1 = !empty(.w_MFCCONT1) and (alltrim(.w_MFCCONT1)$  ('AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBAT-EBTU-EMCO-EMDM-EMLA-FIPP-HTL1-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-IADP-ENBA-CCNL-ART1-ACIM-TCEB-SADP-SADD-PANE-EBA1'+'-EDFO-ENBC-ENBF-ENBI-FEDA-OPNC-CADD-EBNA-VITA-EBUC-CIFE-EST1-EBPA-EBCE-TUEB-LACC-FOIN-EBAR-ONBS'+'-ENFE-EBTS-MI53-EBIL-CUST-PULI-CLIS-EBAG-EBAN-EBMS-EBSA-EBTR-ENBL-EPAR-FSL1-EBMC-EART'+'-QUAS-EBIN-EBAS-EBIP-ASIM-EBIT-EBI1-EFEI-FASD-EBI7-EBG9-EBIM-1AST-EBAI-EBTI-EB04-RCAD-CALL-ENBT-EBCT-ANBA-EBLC-ENBP-ENP1-RCSO-EBNS-EBCA-BINT-EBCO-EBAP-EBFC-EBLE-EBFO-ASI1-EBIC-EBG1-ENB1-MET1-EBCD-ELAV-EBFW-FAST-FOSI-ENAB-EBIS-EBSP-EBIG-OBIL-EBIF'+'-ASS1-DM10-LAV1') or .w_APP1A)
        .w_MFDAMES1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1A,' ',.w_MFDAMES1))
        .w_MFDAANN1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1A,' ',.w_MFDAANN1))
        .w_MF_AMES1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,' ',.w_MF_AMES1))
        .w_MFAN1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,' ',.w_MFAN1))
        .w_MFIMPSD1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSD1)
        .w_MFIMPSC1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSC1)
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_MFCDSED2))
          .link_3_13('Full')
          endif
        .w_MFCCONT2 = iif(empty(.w_MFCDSED2),' ',.w_MFCCONT2)
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_MFCCONT2))
          .link_3_14('Full')
          endif
        .w_MFMINPS2 = iif(empty(.w_MFCDSED2),' ',.w_MFMINPS2)
        .w_APP2A = !empty(.w_MFCCONT2) and alltrim(.w_MFCCONT2)$'ABR-ABS-AC-ACON-DOM2-EMI-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-DSOS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2A,' ',.w_MFDAMES2))
        .w_MFDAANN2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2A,' ',.w_MFDAANN2))
        .w_APP2 = !empty(.w_MFCCONT2) and (alltrim(.w_MFCCONT2)$  ('AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBAT-EBTU-EMCO-EMDM-EMLA-FIPP-HTL1-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-IADP-ENBA-CCNL-ART1-ACIM-TCEB-SADP-SADD-PANE-EBA1'+'-EDFO-ENBC-ENBF-ENBI-FEDA-OPNC-CADD-EBNA-VITA-EBUC-CIFE-EST1-EBPA-EBCE-TUEB-LACC-FOIN-EBAR-ONBS'+'-ENFE-EBTS-MI53-EBIL-CUST-PULI-CLIS-EBAG-EBAN-EBMS-EBSA-EBTR-ENBL-EPAR-FSL1-EBMC-EART'+'-QUAS-EBIN-EBAS-EBIP-ASIM-EBIT-EBI1-EFEI-FASD-EBI7-EBG9-EBIM-1AST-EBAI-EBTI-EB04-RCAD-CALL-ENBT-EBCT-ANBA-EBLC-ENBP-ENP1-RCSO-EBNS-EBCA-BINT-EBCO-EBAP-EBFC-EBLE-EBFO-ASI1-EBIC-EBG1-ENB1-MET1-EBCD-ELAV-EBFW-FAST-FOSI-ENAB-EBIS-EBSP-EBIG-OBIL-EBIF'+'-ASS1-DM10-LAV1') or .w_APP2A)
        .w_MF_AMES2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,' ',.w_MF_AMES2))
        .w_MFAN2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,' ',.w_MFAN2))
        .w_MFIMPSD2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSD2)
        .w_MFIMPSC2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSC2)
        .DoRTCalc(38,38,.f.)
          if not(empty(.w_MFCDSED3))
          .link_3_24('Full')
          endif
        .w_MFCCONT3 = iif(empty(.w_MFCDSED3),' ',.w_MFCCONT3)
        .DoRTCalc(39,39,.f.)
          if not(empty(.w_MFCCONT3))
          .link_3_25('Full')
          endif
        .w_MFMINPS3 = iif(empty(.w_MFCDSED3),' ',.w_MFMINPS3)
        .w_APP3A = !empty(.w_MFCCONT3) and alltrim(.w_MFCCONT3)$ 'ABR-ABS-AC-ACON-DOM2-EMI-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-DSOS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3A,' ',.w_MFDAMES3))
        .w_APP3 = !empty(.w_MFCCONT3) and (alltrim(.w_MFCCONT3)$  ('AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBAT-EBTU-EMCO-EMDM-EMLA-FIPP-HTL1-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-IADP-ENBA-CCNL-ART1-ACIM-TCEB-SADP-SADD-PANE-EBA1'+'-EDFO-ENBC-ENBF-ENBI-FEDA-OPNC-CADD-EBNA-VITA-EBUC-CIFE-EST1-EBPA-EBCE-TUEB-LACC-FOIN-EBAR-ONBS'+'-ENFE-EBTS-MI53-EBIL-CUST-PULI-CLIS-EBAG-EBAN-EBMS-EBSA-EBTR-ENBL-EPAR-FSL1-EBMC-EART'+'-QUAS-EBIN-EBAS-EBIP-ASIM-EBIT-EBI1-EFEI-FASD-EBI7-EBG9-EBIM-1AST-EBAI-EBTI-EB04-RCAD-CALL-ENBT-EBCT-ANBA-EBLC-ENBP-ENP1-RCSO-EBNS-EBCA-BINT-EBCO-EBAP-EBFC-EBLE-EBFO-ASI1-EBIC-EBG1-ENB1-MET1-EBCD-ELAV-EBFW-FAST-FOSI-ENAB-EBIS-EBSP-EBIG-OBIL-EBIF'+'-ASS1-DM10-LAV1') or .w_APP3A)
        .w_MFDAANN3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3A,' ',.w_MFDAANN3))
        .w_MF_AMES3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,' ',.w_MF_AMES3))
        .w_MFAN3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,' ',.w_MFAN3))
        .w_MFIMPSD3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSD3)
        .w_MFIMPSC3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSC3)
        .DoRTCalc(49,49,.f.)
          if not(empty(.w_MFCDSED4))
          .link_3_35('Full')
          endif
        .w_MFCCONT4 = iif(empty(.w_MFCDSED4),' ',.w_MFCCONT4)
        .DoRTCalc(50,50,.f.)
          if not(empty(.w_MFCCONT4))
          .link_3_36('Full')
          endif
        .w_MFMINPS4 = iif(empty(.w_MFCDSED4),' ',.w_MFMINPS4)
        .w_APP4A = !empty(.w_MFCCONT4) and alltrim(.w_MFCCONT4)$ 'ABR-ABS-AC-ACON-DOM2-EMI-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-DSOS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        .w_MFDAMES4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4A,' ',.w_MFDAMES4))
        .w_APP4 = !empty(.w_MFCCONT4) and (alltrim(.w_MFCCONT4)$  ('AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBAT-EBTU-EMCO-EMDM-EMLA-FIPP-HTL1-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-IADP-ENBA-CCNL-ART1-ACIM-TCEB-SADP-SADD-PANE-EBA1'+'-EDFO-ENBC-ENBF-ENBI-FEDA-OPNC-CADD-EBNA-VITA-EBUC-CIFE-EST1-EBPA-EBCE-TUEB-LACC-FOIN-EBAR-ONBS'+'-ENFE-EBTS-MI53-EBIL-CUST-PULI-CLIS-EBAG-EBAN-EBMS-EBSA-EBTR-ENBL-EPAR-FSL1-EBMC-EART'+'-QUAS-EBIN-EBAS-EBIP-ASIM-EBIT-EBI1-EFEI-FASD-EBI7-EBG9-EBIM-1AST-EBAI-EBTI-EB04-RCAD-CALL-ENBT-EBCT-ANBA-EBLC-ENBP-ENP1-RCSO-EBNS-EBCA-BINT-EBCO-EBAP-EBFC-EBLE-EBFO-ASI1-EBIC-EBG1-ENB1-MET1-EBCD-ELAV-EBFW-FAST-FOSI-ENAB-EBIS-EBSP-EBIG-OBIL-EBIF'+'-ASS1-DM10-LAV1') or .w_APP4A)
        .w_MFDAANN4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4A,' ',.w_MFDAANN4))
        .w_MF_AMES4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,' ',.w_MF_AMES4))
        .w_MFAN4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,' ',.w_MFAN4))
        .w_MFIMPSD4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSD4)
        .w_MFIMPSC4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSC4)
        .w_MFTOTDPS = .w_MFIMPSD1+.w_MFIMPSD2+.w_MFIMPSD3+.w_MFIMPSD4
        .w_MFTOTCPS = .w_MFIMPSC1+.w_MFIMPSC2+.w_MFIMPSC3+.w_MFIMPSC4
        .w_MFSALDPS = .w_MFTOTDPS-.w_MFTOTCPS
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_GENINPS = Sys(2015)
        .w_IMPINPS = .F.
        .DoRTCalc(67,68,.f.)
          if not(empty(.w_MFCODRE1))
          .link_4_2('Full')
          endif
        .w_MFTRIRE1 = iif(empty(.w_MFCODRE1),' ',.w_MFTRIRE1)
        .DoRTCalc(69,69,.f.)
          if not(empty(.w_MFTRIRE1))
          .link_4_3('Full')
          endif
        .w_MFRATRE1 = iif(empty(.w_MFCODRE1)  or .w_CHKOBBME1='S',' ',.w_MFRATRE1)
        .w_MFMESER1 = iif(empty(.w_MFCODRE1),' ',.w_MFMESER1)
        .w_MFANNRE1 = iif(empty(.w_MFCODRE1),' ',iif(.w_MFTRIRE1= '3805','0000',.w_MFANNRE1))
        .w_MFIMDRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMDRE1)
        .w_MFIMCRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMCRE1)
        .DoRTCalc(75,75,.f.)
          if not(empty(.w_MFCODRE2))
          .link_4_12('Full')
          endif
        .w_MFTRIRE2 = iif(empty(.w_MFCODRE2),' ',.w_MFTRIRE2)
        .DoRTCalc(76,76,.f.)
          if not(empty(.w_MFTRIRE2))
          .link_4_13('Full')
          endif
        .w_MFRATRE2 = iif(empty(.w_MFCODRE2) or .w_CHKOBBME2='S',' ',.w_MFRATRE2)
        .w_MFMESER2 = iif(empty(.w_MFCODRE2),' ',.w_MFMESER2)
        .w_MFANNRE2 = iif(empty(.w_MFCODRE2),' ',iif(.w_MFTRIRE2 ='3805','0000',.w_MFANNRE2))
        .w_MFIMDRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMDRE2)
        .w_MFIMCRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMCRE2)
        .DoRTCalc(82,82,.f.)
          if not(empty(.w_MFCODRE3))
          .link_4_19('Full')
          endif
        .w_MFTRIRE3 = iif(empty(.w_MFCODRE3),' ',.w_MFTRIRE3)
        .DoRTCalc(83,83,.f.)
          if not(empty(.w_MFTRIRE3))
          .link_4_20('Full')
          endif
        .w_MFRATRE3 = iif(empty(.w_MFCODRE3) or .w_CHKOBBME3='S',' ',.w_MFRATRE3)
        .w_MFMESER3 = iif(empty(.w_MFCODRE3),' ',.w_MFMESER3)
        .w_MFANNRE3 = iif(empty(.w_MFCODRE3),' ',iif(.w_MFTRIRE3 = '3805','0000',.w_MFANNRE3))
        .w_MFIMDRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMDRE3)
        .w_MFIMCRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMCRE3)
        .DoRTCalc(89,89,.f.)
          if not(empty(.w_MFCODRE4))
          .link_4_26('Full')
          endif
        .w_MFTRIRE4 = iif(empty(.w_MFCODRE4),' ',.w_MFTRIRE4)
        .DoRTCalc(90,90,.f.)
          if not(empty(.w_MFTRIRE4))
          .link_4_27('Full')
          endif
        .w_MFRATRE4 = iif(empty(.w_MFCODRE4) or .w_CHKOBBME4='S',' ',.w_MFRATRE4)
        .w_MFMESER4 = iif(empty(.w_MFCODRE4),' ',.w_MFMESER4)
        .w_MFANNRE4 = iif(empty(.w_MFCODRE4),' ',iif( .w_MFTRIRE4 = '3805','0000',.w_MFANNRE4))
        .w_MFIMDRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMDRE4)
        .w_MFIMCRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMCRE4)
        .w_MFTOTDRE = .w_MFIMDRE1+.w_MFIMDRE2+.w_MFIMDRE3+.w_MFIMDRE4
        .w_MFTOTCRE = .w_MFIMCRE1+.w_MFIMCRE2+.w_MFIMCRE3+.w_MFIMCRE4
        .w_MFSALDRE = .w_MFTOTDRE-.w_MFTOTCRE
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .DoRTCalc(101,102,.f.)
          if not(empty(.w_MFSINAI1))
          .link_5_2('Full')
          endif
        .w_MF_NPOS1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NPOS1)
        .w_MF_PACC1 = iif(empty(.w_MFSINAI1),' ',.w_MF_PACC1)
        .w_MF_NRIF1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NRIF1)
        .w_MFCAUSA1 = iif(empty(.w_MFSINAI1),' ',.w_MFCAUSA1)
        .w_MFIMDIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMDIL1)
        .w_MFIMCIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMCIL1)
        .DoRTCalc(109,109,.f.)
          if not(empty(.w_MFSINAI2))
          .link_5_9('Full')
          endif
        .w_MF_NPOS2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NPOS2)
        .w_MF_PACC2 = iif(empty(.w_MFSINAI2),' ',.w_MF_PACC2)
        .w_MF_NRIF2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NRIF2)
        .w_MFCAUSA2 = iif(empty(.w_MFSINAI2),' ',.w_MFCAUSA2)
        .w_MFIMDIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMDIL2)
        .w_MFIMCIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMCIL2)
        .DoRTCalc(116,116,.f.)
          if not(empty(.w_MFSINAI3))
          .link_5_16('Full')
          endif
        .w_MF_NPOS3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NPOS3)
        .w_MF_PACC3 = iif(empty(.w_MFSINAI3),' ',.w_MF_PACC3)
        .w_MF_NRIF3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NRIF3)
        .w_MFCAUSA3 = iif(empty(.w_MFSINAI3),' ',.w_MFCAUSA3)
        .w_MFIMDIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMDIL3)
        .w_MFIMCIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMCIL3)
        .w_MFTDINAI = .w_MFIMDIL1+.w_MFIMDIL2+.w_MFIMDIL3
        .w_MFTCINAI = .w_MFIMCIL1+.w_MFIMCIL2+.w_MFIMCIL3
        .w_MFSALINA = .w_MFTDINAI-.w_MFTCINAI
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .DoRTCalc(128,129,.f.)
          if not(empty(.w_MFCDENTE))
          .link_6_3('Full')
          endif
        .w_VRSDENT1 = space(5)
        .DoRTCalc(130,130,.f.)
          if not(empty(.w_VRSDENT1))
          .link_6_4('Full')
          endif
        .w_VPSDENT1 = space(2)
        .DoRTCalc(131,131,.f.)
          if not(empty(.w_VPSDENT1))
          .link_6_5('Full')
          endif
        .w_MFSDENT1 = iif(.w_MFCDENTE='0003' or .w_MFCDENTE='0005' or .w_MFCDENTE='0006',.w_VPSDENT1,.w_VRSDENT1)
        .w_MFCCOAE1 = iif(empty(.w_MFCDENTE) ,' ',.w_MFCCOAE1)
        .DoRTCalc(133,133,.f.)
          if not(empty(.w_MFCCOAE1))
          .link_6_7('Full')
          endif
        .w_OLSDENT1 = .w_MFSDENT1
        .w_MFCDPOS1 = iif(empty(.w_MFCCOAE1),' ',.w_MFCDPOS1)
        .w_MFMSINE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR',' ',.w_MFMSINE1))
        .w_MFANINE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR',' ',.w_MFANINE1))
        .w_MFMSFIE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFMSFIE1))
        .w_MFANF1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFANF1))
        .w_MFIMDAE1 = iif(empty(.w_MFCCOAE1),0,.w_MFIMDAE1)
        .w_MFIMCAE1 = iif(empty(.w_MFCCOAE1) or  .w_MFCDENTE='0003',0,.w_MFIMCAE1)
        .w_VPSDENT2 = space(2)
        .DoRTCalc(142,142,.f.)
          if not(empty(.w_VPSDENT2))
          .link_6_16('Full')
          endif
        .w_VRSDENT2 = space(5)
        .DoRTCalc(143,143,.f.)
          if not(empty(.w_VRSDENT2))
          .link_6_17('Full')
          endif
        .w_MFSDENT2 = iif(.w_MFCDENTE='0003' or .w_MFCDENTE='0005' or .w_MFCDENTE='0006' ,.w_VPSDENT2,.w_VRSDENT2)
        .w_MFCCOAE2 = iif(empty(.w_MFCDENTE),' ',.w_MFCCOAE2)
        .DoRTCalc(145,145,.f.)
          if not(empty(.w_MFCCOAE2))
          .link_6_19('Full')
          endif
        .w_OLSDENT2 = .w_MFSDENT2
        .w_MFCDPOS2 = iif(empty(.w_MFCCOAE2),' ',.w_MFCDPOS2)
        .w_MFMSINE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR',' ',.w_MFMSINE2))
        .w_MFANINE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR',' ',.w_MFANINE2))
        .w_MFMSFIE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFMSFIE2))
        .w_MFANF2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFANF2))
        .w_MFIMDAE2 = iif(empty(.w_MFCCOAE2),0,.w_MFIMDAE2)
        .w_MFIMCAE2 = iif(empty(.w_MFCCOAE2) or .w_MFCDENTE='0003',0,.w_MFIMCAE2)
        .w_MFTDAENT = .w_MFIMDAE1+.w_MFIMDAE2
        .w_MFTCAENT = .w_MFIMCAE1+.w_MFIMCAE2
        .w_MFSALAEN = .w_MFTDAENT-.w_MFTCAENT
          .DoRTCalc(157,157,.f.)
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_GENPREV = Sys(2015)
          .DoRTCalc(161,161,.f.)
        .w_IMPPREV = .F.
          .DoRTCalc(163,163,.f.)
        .w_MFSALFIN = .w_APPOIMP + .w_APPOIMP2 + .w_MFSALDPS + .w_MFSALDRE + .w_MFSALINA + .w_MFSALAEN
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
        .w_TIPOMOD = '007'
        .oPgFrm.Page6.oPag.oObj_6_51.Calculate()
          .DoRTCalc(168,168,.f.)
        .w_ANNORIF = '2003'
        .DoRTCalc(170,171,.f.)
          if not(empty(.w_MFDESSTA))
          .link_7_10('Full')
          endif
          .DoRTCalc(172,175,.f.)
        .w_CAP = iif(empty(NVL(.w_MFDESSTA,'')),g_capazi,.w_CAP1)
        .w_LOCALI = UPPER(iif(EMPTY(NVL(.w_MFDESSTA,'')),g_locazi,.w_LOCALI1))
        .w_PROVIN = UPPER(iif(empty(NVL(.w_MFDESSTA,'')),g_proazi,.w_PROVIN1))
        .w_INDIRI = UPPER(iif(empty(NVL(.w_MFDESSTA,'')),g_indazi,.w_INDIRI1))
          .DoRTCalc(180,180,.f.)
        .w_CODAZIE = i_CODAZI
        .w_MFTIPMOD = '2007'
        .oPgFrm.Page4.oPag.oObj_4_52.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page4.oPag.oObj_4_53.Calculate(ah_MSGFORMAT('Mese di %0riferimento') )
        .oPgFrm.Page4.oPag.oObj_4_54.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page4.oPag.oObj_4_55.Calculate(ah_MSGFORMAT('Codice %0regione'))
        .oPgFrm.Page3.oPag.oObj_3_68.Calculate(ah_MSGFORMAT('Codice %0sede'))
        .oPgFrm.Page3.oPag.oObj_3_69.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page5.oPag.oObj_5_42.Calculate(ah_MSGFORMAT('Numero di %0riferimento'))
        .oPgFrm.Page6.oPag.oObj_6_52.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page6.oPag.oObj_6_53.Calculate(iif(.w_MFCDENTE<>'0003' AND .w_MFCDENTE<>'0005'  AND .w_MFCDENTE<>'0006' ,ah_MSGFORMAT('Codice %0sede'),ah_MSGFORMAT('Codice %0provincia')))
        .oPgFrm.Page6.oPag.oObj_6_54.Calculate(ah_MSGFORMAT('Codice %0posizione'))
          .DoRTCalc(183,186,.f.)
        .w_RAPFIRM = i_CODAZI
        .DoRTCalc(187,187,.f.)
          if not(empty(.w_RAPFIRM))
          .link_8_1('Full')
          endif
        .w_READAZI = i_codazi
        .DoRTCalc(188,188,.f.)
          if not(empty(.w_READAZI))
          .link_8_2('Full')
          endif
          .DoRTCalc(189,201,.f.)
        .w_MFCODFIR = .w_MFCODFIR2
        .w_MFCHKFIR = IIF (.w_MFENTRAT='E' , IIF (this.gscg_acg.w_CFPERFIS='N', 'S' , ' ' ) , ' ')
        .w_MFCARFIR = .w_MFCARFIR2
        .w_MFCOGFIR = LEFT (.w_MFCOGFIR2,24)
        .w_MFNOMFIR = LEFT (.w_MFNOMFIR2,20) 
        .w_MFSESFIR = .w_MFSESFIR2
        .w_MFDATFIR = .w_MFDATFIR2
        .w_MFCOMFIR = .w_MFCOMFIR2
        .w_MFPROFIR = .w_MFPROFIR2
        .w_MFDOMFIR = .w_MFDOMFIR2
        .w_MFPRDFIR = .w_MFPRDFIR2
        .w_MFCAPFIR = left (.w_MFCAPFIR2,5)
        .w_MFINDFIR = .w_MFINDFIR2
        .w_MFENTRAT = .w_ENTRATEL
          .DoRTCalc(216,217,.f.)
        .w_MFBANPAS = .w_AZCONBAN
        .DoRTCalc(218,218,.f.)
          if not(empty(.w_MFBANPAS))
          .link_8_45('Full')
          endif
          .DoRTCalc(219,219,.f.)
        .w_MFCOIBAN = SUBSTR(.w_IBAN,1,34)
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
          .DoRTCalc(221,221,.f.)
        .w_MFSALFIN = .w_APPOIMP + .w_APPOIMP2 + .w_MFSALDPS + .w_MFSALDRE + .w_MFSALINA + .w_MFSALAEN
        .w_MESE = .w_MFMESRIF
        .w_ANNO = .w_MFANNRIF
          .DoRTCalc(225,227,.f.)
        .w_OBTEST = i_DATSYS
        .w_MFCOIBAN = SUBSTR(.w_IBAN,1,34)
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOD_PAG')
    this.DoRTCalc(230,230,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page7.oPag.oBtn_7_27.enabled = this.oPgFrm.Page7.oPag.oBtn_7_27.mCond()
    this.oPgFrm.Page8.oPag.oBtn_8_50.enabled = this.oPgFrm.Page8.oPag.oBtn_8_50.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_aop
    * instanzio il figlio della seconda,quarta e settima pagina immediatamente
       if Upper(this.GSCG_AVF.class)='STDDYNAMICCHILD'
         This.oPgFrm.Pages[7].opag.uienable(.T.)
         This.oPgFrm.ActivePage=1
       Endif
       if Upper(this.GSCG_AIG.class)='STDDYNAMICCHILD'
         This.oPgFrm.Pages[4].opag.uienable(.T.)
         This.oPgFrm.ActivePage=1
       Endif
       if Upper(this.GSCG_AFR.class)='STDDYNAMICCHILD'
         This.oPgFrm.Pages[2].opag.uienable(.T.)
         This.oPgFrm.ActivePage=1
       Endif
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"MODEL","i_CODAZI,w_MFSERIAL")
      .op_CODAZI = .w_CODAZI
      .op_MFSERIAL = .w_MFSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMFSERIAL_1_1.enabled = i_bVal
      .Page1.oPag.oMFMESRIF_1_2.enabled = i_bVal
      .Page1.oPag.oMFANNRIF_1_3.enabled = i_bVal
      .Page1.oPag.oMF_COINC_1_7.enabled = i_bVal
      .Page2.oPag.oMFCODUFF_2_4.enabled = i_bVal
      .Page2.oPag.oMFCODATT_2_5.enabled = i_bVal
      .Page3.oPag.oMFCDSED1_3_2.enabled = i_bVal
      .Page3.oPag.oMFCCONT1_3_3.enabled = i_bVal
      .Page3.oPag.oMFMINPS1_3_4.enabled = i_bVal
      .Page3.oPag.oMFDAMES1_3_7.enabled = i_bVal
      .Page3.oPag.oMFDAANN1_3_8.enabled = i_bVal
      .Page3.oPag.oMF_AMES1_3_9.enabled = i_bVal
      .Page3.oPag.oMFAN1_3_10.enabled = i_bVal
      .Page3.oPag.oMFIMPSD1_3_11.enabled = i_bVal
      .Page3.oPag.oMFIMPSC1_3_12.enabled = i_bVal
      .Page3.oPag.oMFCDSED2_3_13.enabled = i_bVal
      .Page3.oPag.oMFCCONT2_3_14.enabled = i_bVal
      .Page3.oPag.oMFMINPS2_3_15.enabled = i_bVal
      .Page3.oPag.oMFDAMES2_3_17.enabled = i_bVal
      .Page3.oPag.oMFDAANN2_3_18.enabled = i_bVal
      .Page3.oPag.oMF_AMES2_3_20.enabled = i_bVal
      .Page3.oPag.oMFAN2_3_21.enabled = i_bVal
      .Page3.oPag.oMFIMPSD2_3_22.enabled = i_bVal
      .Page3.oPag.oMFIMPSC2_3_23.enabled = i_bVal
      .Page3.oPag.oMFCDSED3_3_24.enabled = i_bVal
      .Page3.oPag.oMFCCONT3_3_25.enabled = i_bVal
      .Page3.oPag.oMFMINPS3_3_26.enabled = i_bVal
      .Page3.oPag.oMFDAMES3_3_28.enabled = i_bVal
      .Page3.oPag.oMFDAANN3_3_30.enabled = i_bVal
      .Page3.oPag.oMF_AMES3_3_31.enabled = i_bVal
      .Page3.oPag.oMFAN3_3_32.enabled = i_bVal
      .Page3.oPag.oMFIMPSD3_3_33.enabled = i_bVal
      .Page3.oPag.oMFIMPSC3_3_34.enabled = i_bVal
      .Page3.oPag.oMFCDSED4_3_35.enabled = i_bVal
      .Page3.oPag.oMFCCONT4_3_36.enabled = i_bVal
      .Page3.oPag.oMFMINPS4_3_37.enabled = i_bVal
      .Page3.oPag.oMFDAMES4_3_39.enabled = i_bVal
      .Page3.oPag.oMFDAANN4_3_41.enabled = i_bVal
      .Page3.oPag.oMF_AMES4_3_42.enabled = i_bVal
      .Page3.oPag.oMFAN4_3_43.enabled = i_bVal
      .Page3.oPag.oMFIMPSD4_3_44.enabled = i_bVal
      .Page3.oPag.oMFIMPSC4_3_45.enabled = i_bVal
      .Page4.oPag.oMFCODRE1_4_2.enabled = i_bVal
      .Page4.oPag.oMFTRIRE1_4_3.enabled = i_bVal
      .Page4.oPag.oMFRATRE1_4_4.enabled = i_bVal
      .Page4.oPag.oMFMESER1_4_5.enabled = i_bVal
      .Page4.oPag.oMFANNRE1_4_6.enabled = i_bVal
      .Page4.oPag.oMFIMDRE1_4_7.enabled = i_bVal
      .Page4.oPag.oMFIMCRE1_4_8.enabled = i_bVal
      .Page4.oPag.oMFCODRE2_4_12.enabled = i_bVal
      .Page4.oPag.oMFTRIRE2_4_13.enabled = i_bVal
      .Page4.oPag.oMFRATRE2_4_14.enabled = i_bVal
      .Page4.oPag.oMFMESER2_4_15.enabled = i_bVal
      .Page4.oPag.oMFANNRE2_4_16.enabled = i_bVal
      .Page4.oPag.oMFIMDRE2_4_17.enabled = i_bVal
      .Page4.oPag.oMFIMCRE2_4_18.enabled = i_bVal
      .Page4.oPag.oMFCODRE3_4_19.enabled = i_bVal
      .Page4.oPag.oMFTRIRE3_4_20.enabled = i_bVal
      .Page4.oPag.oMFRATRE3_4_21.enabled = i_bVal
      .Page4.oPag.oMFMESER3_4_22.enabled = i_bVal
      .Page4.oPag.oMFANNRE3_4_23.enabled = i_bVal
      .Page4.oPag.oMFIMDRE3_4_24.enabled = i_bVal
      .Page4.oPag.oMFIMCRE3_4_25.enabled = i_bVal
      .Page4.oPag.oMFCODRE4_4_26.enabled = i_bVal
      .Page4.oPag.oMFTRIRE4_4_27.enabled = i_bVal
      .Page4.oPag.oMFRATRE4_4_28.enabled = i_bVal
      .Page4.oPag.oMFMESER4_4_29.enabled = i_bVal
      .Page4.oPag.oMFANNRE4_4_30.enabled = i_bVal
      .Page4.oPag.oMFIMDRE4_4_31.enabled = i_bVal
      .Page4.oPag.oMFIMCRE4_4_32.enabled = i_bVal
      .Page5.oPag.oMFSINAI1_5_2.enabled = i_bVal
      .Page5.oPag.oMF_NPOS1_5_3.enabled = i_bVal
      .Page5.oPag.oMF_PACC1_5_4.enabled = i_bVal
      .Page5.oPag.oMF_NRIF1_5_5.enabled = i_bVal
      .Page5.oPag.oMFCAUSA1_5_6.enabled = i_bVal
      .Page5.oPag.oMFIMDIL1_5_7.enabled = i_bVal
      .Page5.oPag.oMFIMCIL1_5_8.enabled = i_bVal
      .Page5.oPag.oMFSINAI2_5_9.enabled = i_bVal
      .Page5.oPag.oMF_NPOS2_5_10.enabled = i_bVal
      .Page5.oPag.oMF_PACC2_5_11.enabled = i_bVal
      .Page5.oPag.oMF_NRIF2_5_12.enabled = i_bVal
      .Page5.oPag.oMFCAUSA2_5_13.enabled = i_bVal
      .Page5.oPag.oMFIMDIL2_5_14.enabled = i_bVal
      .Page5.oPag.oMFIMCIL2_5_15.enabled = i_bVal
      .Page5.oPag.oMFSINAI3_5_16.enabled = i_bVal
      .Page5.oPag.oMF_NPOS3_5_17.enabled = i_bVal
      .Page5.oPag.oMF_PACC3_5_18.enabled = i_bVal
      .Page5.oPag.oMF_NRIF3_5_19.enabled = i_bVal
      .Page5.oPag.oMFCAUSA3_5_20.enabled = i_bVal
      .Page5.oPag.oMFIMDIL3_5_21.enabled = i_bVal
      .Page5.oPag.oMFIMCIL3_5_22.enabled = i_bVal
      .Page6.oPag.oMFCDENTE_6_3.enabled = i_bVal
      .Page6.oPag.oVRSDENT1_6_4.enabled = i_bVal
      .Page6.oPag.oVPSDENT1_6_5.enabled = i_bVal
      .Page6.oPag.oMFCCOAE1_6_7.enabled = i_bVal
      .Page6.oPag.oMFCDPOS1_6_9.enabled = i_bVal
      .Page6.oPag.oMFMSINE1_6_10.enabled = i_bVal
      .Page6.oPag.oMFANINE1_6_11.enabled = i_bVal
      .Page6.oPag.oMFMSFIE1_6_12.enabled = i_bVal
      .Page6.oPag.oMFANF1_6_13.enabled = i_bVal
      .Page6.oPag.oMFIMDAE1_6_14.enabled = i_bVal
      .Page6.oPag.oMFIMCAE1_6_15.enabled = i_bVal
      .Page6.oPag.oVPSDENT2_6_16.enabled = i_bVal
      .Page6.oPag.oVRSDENT2_6_17.enabled = i_bVal
      .Page6.oPag.oMFCCOAE2_6_19.enabled = i_bVal
      .Page6.oPag.oMFCDPOS2_6_21.enabled = i_bVal
      .Page6.oPag.oMFMSINE2_6_22.enabled = i_bVal
      .Page6.oPag.oMFANINE2_6_23.enabled = i_bVal
      .Page6.oPag.oMFMSFIE2_6_24.enabled = i_bVal
      .Page6.oPag.oMFANF2_6_25.enabled = i_bVal
      .Page6.oPag.oMFIMDAE2_6_26.enabled = i_bVal
      .Page6.oPag.oMFIMCAE2_6_27.enabled = i_bVal
      .Page7.oPag.oMFDESSTA_7_10.enabled = i_bVal
      .Page8.oPag.oMFCODFIR_8_16.enabled = i_bVal
      .Page8.oPag.oMFCHKFIR_8_18.enabled = i_bVal
      .Page8.oPag.oMFCARFIR_8_19.enabled = i_bVal
      .Page8.oPag.oMFCOGFIR_8_20.enabled = i_bVal
      .Page8.oPag.oMFNOMFIR_8_21.enabled = i_bVal
      .Page8.oPag.oMFSESFIR_8_22.enabled = i_bVal
      .Page8.oPag.oMFDATFIR_8_23.enabled = i_bVal
      .Page8.oPag.oMFCOMFIR_8_24.enabled = i_bVal
      .Page8.oPag.oMFPROFIR_8_25.enabled = i_bVal
      .Page8.oPag.oMFDOMFIR_8_26.enabled = i_bVal
      .Page8.oPag.oMFPRDFIR_8_27.enabled = i_bVal
      .Page8.oPag.oMFCAPFIR_8_28.enabled = i_bVal
      .Page8.oPag.oMFINDFIR_8_32.enabled = i_bVal
      .Page8.oPag.oMFMAIVER_8_44.enabled = i_bVal
      .Page8.oPag.oMFBANPAS_8_45.enabled = i_bVal
      .Page8.oPag.oMFCOIBAN_8_47.enabled = i_bVal
      .Page8.oPag.oMFCOIBAN_8_66.enabled = i_bVal
      .Page6.oPag.oBtn_6_47.enabled = i_bVal
      .Page2.oPag.oBtn_2_16.enabled = i_bVal
      .Page3.oPag.oBtn_3_67.enabled = i_bVal
      .Page7.oPag.oBtn_7_26.enabled = i_bVal
      .Page7.oPag.oBtn_7_27.enabled = .Page7.oPag.oBtn_7_27.mCond()
      .Page8.oPag.oBtn_8_49.enabled = i_bVal
      .Page8.oPag.oBtn_8_50.enabled = .Page8.oPag.oBtn_8_50.mCond()
      .Page1.oPag.oObj_1_5.enabled = i_bVal
      .Page2.oPag.oObj_2_13.enabled = i_bVal
      .Page2.oPag.oObj_2_15.enabled = i_bVal
      .Page6.oPag.oObj_6_51.enabled = i_bVal
      .Page1.oPag.oObj_1_19.enabled = i_bVal
      .Page1.oPag.oObj_1_20.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMFSERIAL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMFSERIAL_1_1.enabled = .t.
        .Page1.oPag.oMFMESRIF_1_2.enabled = .t.
        .Page1.oPag.oMFANNRIF_1_3.enabled = .t.
      endif
    endwith
    this.GSCG_AFR.SetStatus(i_cOp)
    this.GSCG_AVF.SetStatus(i_cOp)
    this.GSCG_AIG.SetStatus(i_cOp)
    this.GSCG_ACG.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'MOD_PAG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_AFR.SetChildrenStatus(i_cOp)
  *  this.GSCG_AVF.SetChildrenStatus(i_cOp)
  *  this.GSCG_AIG.SetChildrenStatus(i_cOp)
  *  this.GSCG_ACG.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMESRIF,"MFMESRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRIF,"MFANNRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFVALUTA,"MFVALUTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_COINC,"MF_COINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODUFF,"MFCODUFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODATT,"MFCODATT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED1,"MFCDSED1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT1,"MFCCONT1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS1,"MFMINPS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES1,"MFDAMES1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN1,"MFDAANN1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES1,"MF_AMES1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN1,"MFAN1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD1,"MFIMPSD1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC1,"MFIMPSC1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED2,"MFCDSED2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT2,"MFCCONT2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS2,"MFMINPS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES2,"MFDAMES2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN2,"MFDAANN2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES2,"MF_AMES2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN2,"MFAN2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD2,"MFIMPSD2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC2,"MFIMPSC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED3,"MFCDSED3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT3,"MFCCONT3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS3,"MFMINPS3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES3,"MFDAMES3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN3,"MFDAANN3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES3,"MF_AMES3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN3,"MFAN3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD3,"MFIMPSD3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC3,"MFIMPSC3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDSED4,"MFCDSED4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCONT4,"MFCCONT4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMINPS4,"MFMINPS4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAMES4,"MFDAMES4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDAANN4,"MFDAANN4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_AMES4,"MF_AMES4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFAN4,"MFAN4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSD4,"MFIMPSD4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMPSC4,"MFIMPSC4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTDPS,"MFTOTDPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTCPS,"MFTOTCPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALDPS,"MFSALDPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE1,"MFCODRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE1,"MFTRIRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE1,"MFRATRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMESER1,"MFMESER1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE1,"MFANNRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE1,"MFIMDRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE1,"MFIMCRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE2,"MFCODRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE2,"MFTRIRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE2,"MFRATRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMESER2,"MFMESER2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE2,"MFANNRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE2,"MFIMDRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE2,"MFIMCRE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE3,"MFCODRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE3,"MFTRIRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE3,"MFRATRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMESER3,"MFMESER3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE3,"MFANNRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE3,"MFIMDRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE3,"MFIMCRE3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODRE4,"MFCODRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTRIRE4,"MFTRIRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFRATRE4,"MFRATRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMESER4,"MFMESER4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANNRE4,"MFANNRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDRE4,"MFIMDRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCRE4,"MFIMCRE4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTDRE,"MFTOTDRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTOTCRE,"MFTOTCRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALDRE,"MFSALDRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSINAI1,"MFSINAI1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NPOS1,"MF_NPOS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_PACC1,"MF_PACC1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NRIF1,"MF_NRIF1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCAUSA1,"MFCAUSA1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDIL1,"MFIMDIL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCIL1,"MFIMCIL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSINAI2,"MFSINAI2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NPOS2,"MF_NPOS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_PACC2,"MF_PACC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NRIF2,"MF_NRIF2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCAUSA2,"MFCAUSA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDIL2,"MFIMDIL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCIL2,"MFIMCIL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSINAI3,"MFSINAI3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NPOS3,"MF_NPOS3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_PACC3,"MF_PACC3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MF_NRIF3,"MF_NRIF3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCAUSA3,"MFCAUSA3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDIL3,"MFIMDIL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCIL3,"MFIMCIL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTDINAI,"MFTDINAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTCINAI,"MFTCINAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALINA,"MFSALINA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDENTE,"MFCDENTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSDENT1,"MFSDENT1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCOAE1,"MFCCOAE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDPOS1,"MFCDPOS1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMSINE1,"MFMSINE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANINE1,"MFANINE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMSFIE1,"MFMSFIE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANF1,"MFANF1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDAE1,"MFIMDAE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCAE1,"MFIMCAE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSDENT2,"MFSDENT2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCCOAE2,"MFCCOAE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCDPOS2,"MFCDPOS2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMSINE2,"MFMSINE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANINE2,"MFANINE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMSFIE2,"MFMSFIE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFANF2,"MFANF2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMDAE2,"MFIMDAE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFIMCAE2,"MFIMCAE2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTDAENT,"MFTDAENT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTCAENT,"MFTCAENT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALAEN,"MFSALAEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALFIN,"MFSALFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDESSTA,"MFDESSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFTIPMOD,"MFTIPMOD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCODFIR,"MFCODFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCHKFIR,"MFCHKFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCARFIR,"MFCARFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCOGFIR,"MFCOGFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFNOMFIR,"MFNOMFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSESFIR,"MFSESFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDATFIR,"MFDATFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCOMFIR,"MFCOMFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFPROFIR,"MFPROFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFDOMFIR,"MFDOMFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFPRDFIR,"MFPRDFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCAPFIR,"MFCAPFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFINDFIR,"MFINDFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFENTRAT,"MFENTRAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFMAIVER,"MFMAIVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCOIBAN,"MFCOIBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSERIAL,"MFSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFSALFIN,"MFSALFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MFCOIBAN,"MFCOIBAN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    i_lTable = "MOD_PAG"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOD_PAG_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        do GSCG_BMG with this
      endwith
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MOD_PAG_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"MODEL","i_CODAZI,w_MFSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOD_PAG
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOD_PAG')
        i_extval=cp_InsertValODBCExtFlds(this,'MOD_PAG')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MFSERIAL,MFMESRIF,MFANNRIF,MFVALUTA,MF_COINC"+;
                  ",MFCODUFF,MFCODATT,MFCDSED1,MFCCONT1,MFMINPS1"+;
                  ",MFDAMES1,MFDAANN1,MF_AMES1,MFAN1,MFIMPSD1"+;
                  ",MFIMPSC1,MFCDSED2,MFCCONT2,MFMINPS2,MFDAMES2"+;
                  ",MFDAANN2,MF_AMES2,MFAN2,MFIMPSD2,MFIMPSC2"+;
                  ",MFCDSED3,MFCCONT3,MFMINPS3,MFDAMES3,MFDAANN3"+;
                  ",MF_AMES3,MFAN3,MFIMPSD3,MFIMPSC3,MFCDSED4"+;
                  ",MFCCONT4,MFMINPS4,MFDAMES4,MFDAANN4,MF_AMES4"+;
                  ",MFAN4,MFIMPSD4,MFIMPSC4,MFTOTDPS,MFTOTCPS"+;
                  ",MFSALDPS,MFCODRE1,MFTRIRE1,MFRATRE1,MFMESER1"+;
                  ",MFANNRE1,MFIMDRE1,MFIMCRE1,MFCODRE2,MFTRIRE2"+;
                  ",MFRATRE2,MFMESER2,MFANNRE2,MFIMDRE2,MFIMCRE2"+;
                  ",MFCODRE3,MFTRIRE3,MFRATRE3,MFMESER3,MFANNRE3"+;
                  ",MFIMDRE3,MFIMCRE3,MFCODRE4,MFTRIRE4,MFRATRE4"+;
                  ",MFMESER4,MFANNRE4,MFIMDRE4,MFIMCRE4,MFTOTDRE"+;
                  ",MFTOTCRE,MFSALDRE,MFSINAI1,MF_NPOS1,MF_PACC1"+;
                  ",MF_NRIF1,MFCAUSA1,MFIMDIL1,MFIMCIL1,MFSINAI2"+;
                  ",MF_NPOS2,MF_PACC2,MF_NRIF2,MFCAUSA2,MFIMDIL2"+;
                  ",MFIMCIL2,MFSINAI3,MF_NPOS3,MF_PACC3,MF_NRIF3"+;
                  ",MFCAUSA3,MFIMDIL3,MFIMCIL3,MFTDINAI,MFTCINAI"+;
                  ",MFSALINA,MFCDENTE,MFSDENT1,MFCCOAE1,MFCDPOS1"+;
                  ",MFMSINE1,MFANINE1,MFMSFIE1,MFANF1,MFIMDAE1"+;
                  ",MFIMCAE1,MFSDENT2,MFCCOAE2,MFCDPOS2,MFMSINE2"+;
                  ",MFANINE2,MFMSFIE2,MFANF2,MFIMDAE2,MFIMCAE2"+;
                  ",MFTDAENT,MFTCAENT,MFSALAEN,MFSALFIN,MFDESSTA"+;
                  ",MFTIPMOD,MFCODFIR,MFCHKFIR,MFCARFIR,MFCOGFIR"+;
                  ",MFNOMFIR,MFSESFIR,MFDATFIR,MFCOMFIR,MFPROFIR"+;
                  ",MFDOMFIR,MFPRDFIR,MFCAPFIR,MFINDFIR,MFENTRAT"+;
                  ",MFMAIVER,MFCOIBAN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MFSERIAL)+;
                  ","+cp_ToStrODBC(this.w_MFMESRIF)+;
                  ","+cp_ToStrODBC(this.w_MFANNRIF)+;
                  ","+cp_ToStrODBC(this.w_MFVALUTA)+;
                  ","+cp_ToStrODBC(this.w_MF_COINC)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODUFF)+;
                  ","+cp_ToStrODBC(this.w_MFCODATT)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT1)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS1)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES1)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN1)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES1)+;
                  ","+cp_ToStrODBC(this.w_MFAN1)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD1)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT2)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS2)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES2)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN2)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES2)+;
                  ","+cp_ToStrODBC(this.w_MFAN2)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD2)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED3)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT3)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS3)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES3)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN3)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES3)+;
                  ","+cp_ToStrODBC(this.w_MFAN3)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD3)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC3)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDSED4)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCONT4)+;
                  ","+cp_ToStrODBC(this.w_MFMINPS4)+;
                  ","+cp_ToStrODBC(this.w_MFDAMES4)+;
                  ","+cp_ToStrODBC(this.w_MFDAANN4)+;
                  ","+cp_ToStrODBC(this.w_MF_AMES4)+;
                  ","+cp_ToStrODBC(this.w_MFAN4)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSD4)+;
                  ","+cp_ToStrODBC(this.w_MFIMPSC4)+;
                  ","+cp_ToStrODBC(this.w_MFTOTDPS)+;
                  ","+cp_ToStrODBC(this.w_MFTOTCPS)+;
                  ","+cp_ToStrODBC(this.w_MFSALDPS)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE1)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE1)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE1)+;
                  ","+cp_ToStrODBC(this.w_MFMESER1)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE1)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE1)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE2)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE2)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE2)+;
                  ","+cp_ToStrODBC(this.w_MFMESER2)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE2)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE2)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE3)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE3)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE3)+;
                  ","+cp_ToStrODBC(this.w_MFMESER3)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE3)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE3)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE3)+;
                  ","+cp_ToStrODBCNull(this.w_MFCODRE4)+;
                  ","+cp_ToStrODBCNull(this.w_MFTRIRE4)+;
                  ","+cp_ToStrODBC(this.w_MFRATRE4)+;
                  ","+cp_ToStrODBC(this.w_MFMESER4)+;
                  ","+cp_ToStrODBC(this.w_MFANNRE4)+;
                  ","+cp_ToStrODBC(this.w_MFIMDRE4)+;
                  ","+cp_ToStrODBC(this.w_MFIMCRE4)+;
                  ","+cp_ToStrODBC(this.w_MFTOTDRE)+;
                  ","+cp_ToStrODBC(this.w_MFTOTCRE)+;
                  ","+cp_ToStrODBC(this.w_MFSALDRE)+;
                  ","+cp_ToStrODBCNull(this.w_MFSINAI1)+;
                  ","+cp_ToStrODBC(this.w_MF_NPOS1)+;
                  ","+cp_ToStrODBC(this.w_MF_PACC1)+;
                  ","+cp_ToStrODBC(this.w_MF_NRIF1)+;
                  ","+cp_ToStrODBC(this.w_MFCAUSA1)+;
                  ","+cp_ToStrODBC(this.w_MFIMDIL1)+;
                  ","+cp_ToStrODBC(this.w_MFIMCIL1)+;
                  ","+cp_ToStrODBCNull(this.w_MFSINAI2)+;
                  ","+cp_ToStrODBC(this.w_MF_NPOS2)+;
                  ","+cp_ToStrODBC(this.w_MF_PACC2)+;
                  ","+cp_ToStrODBC(this.w_MF_NRIF2)+;
                  ","+cp_ToStrODBC(this.w_MFCAUSA2)+;
                  ","+cp_ToStrODBC(this.w_MFIMDIL2)+;
                  ","+cp_ToStrODBC(this.w_MFIMCIL2)+;
                  ","+cp_ToStrODBCNull(this.w_MFSINAI3)+;
                  ","+cp_ToStrODBC(this.w_MF_NPOS3)+;
                  ","+cp_ToStrODBC(this.w_MF_PACC3)+;
                  ","+cp_ToStrODBC(this.w_MF_NRIF3)+;
                  ","+cp_ToStrODBC(this.w_MFCAUSA3)+;
                  ","+cp_ToStrODBC(this.w_MFIMDIL3)+;
                  ","+cp_ToStrODBC(this.w_MFIMCIL3)+;
                  ","+cp_ToStrODBC(this.w_MFTDINAI)+;
                  ","+cp_ToStrODBC(this.w_MFTCINAI)+;
             ""
             i_nnn=i_nnn+;
                  ","+cp_ToStrODBC(this.w_MFSALINA)+;
                  ","+cp_ToStrODBCNull(this.w_MFCDENTE)+;
                  ","+cp_ToStrODBC(this.w_MFSDENT1)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCOAE1)+;
                  ","+cp_ToStrODBC(this.w_MFCDPOS1)+;
                  ","+cp_ToStrODBC(this.w_MFMSINE1)+;
                  ","+cp_ToStrODBC(this.w_MFANINE1)+;
                  ","+cp_ToStrODBC(this.w_MFMSFIE1)+;
                  ","+cp_ToStrODBC(this.w_MFANF1)+;
                  ","+cp_ToStrODBC(this.w_MFIMDAE1)+;
                  ","+cp_ToStrODBC(this.w_MFIMCAE1)+;
                  ","+cp_ToStrODBC(this.w_MFSDENT2)+;
                  ","+cp_ToStrODBCNull(this.w_MFCCOAE2)+;
                  ","+cp_ToStrODBC(this.w_MFCDPOS2)+;
                  ","+cp_ToStrODBC(this.w_MFMSINE2)+;
                  ","+cp_ToStrODBC(this.w_MFANINE2)+;
                  ","+cp_ToStrODBC(this.w_MFMSFIE2)+;
                  ","+cp_ToStrODBC(this.w_MFANF2)+;
                  ","+cp_ToStrODBC(this.w_MFIMDAE2)+;
                  ","+cp_ToStrODBC(this.w_MFIMCAE2)+;
                  ","+cp_ToStrODBC(this.w_MFTDAENT)+;
                  ","+cp_ToStrODBC(this.w_MFTCAENT)+;
                  ","+cp_ToStrODBC(this.w_MFSALAEN)+;
                  ","+cp_ToStrODBC(this.w_MFSALFIN)+;
                  ","+cp_ToStrODBCNull(this.w_MFDESSTA)+;
                  ","+cp_ToStrODBC(this.w_MFTIPMOD)+;
                  ","+cp_ToStrODBC(this.w_MFCODFIR)+;
                  ","+cp_ToStrODBC(this.w_MFCHKFIR)+;
                  ","+cp_ToStrODBC(this.w_MFCARFIR)+;
                  ","+cp_ToStrODBC(this.w_MFCOGFIR)+;
                  ","+cp_ToStrODBC(this.w_MFNOMFIR)+;
                  ","+cp_ToStrODBC(this.w_MFSESFIR)+;
                  ","+cp_ToStrODBC(this.w_MFDATFIR)+;
                  ","+cp_ToStrODBC(this.w_MFCOMFIR)+;
                  ","+cp_ToStrODBC(this.w_MFPROFIR)+;
                  ","+cp_ToStrODBC(this.w_MFDOMFIR)+;
                  ","+cp_ToStrODBC(this.w_MFPRDFIR)+;
                  ","+cp_ToStrODBC(this.w_MFCAPFIR)+;
                  ","+cp_ToStrODBC(this.w_MFINDFIR)+;
                  ","+cp_ToStrODBC(this.w_MFENTRAT)+;
                  ","+cp_ToStrODBC(this.w_MFMAIVER)+;
                  ","+cp_ToStrODBC(this.w_MFCOIBAN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOD_PAG')
        i_extval=cp_InsertValVFPExtFlds(this,'MOD_PAG')
        cp_CheckDeletedKey(i_cTable,0,'MFSERIAL',this.w_MFSERIAL)
        INSERT INTO (i_cTable);
              (MFSERIAL,MFMESRIF,MFANNRIF,MFVALUTA,MF_COINC,MFCODUFF,MFCODATT,MFCDSED1,MFCCONT1,MFMINPS1,MFDAMES1,MFDAANN1,MF_AMES1,MFAN1,MFIMPSD1,MFIMPSC1,MFCDSED2,MFCCONT2,MFMINPS2,MFDAMES2,MFDAANN2,MF_AMES2,MFAN2,MFIMPSD2,MFIMPSC2,MFCDSED3,MFCCONT3,MFMINPS3,MFDAMES3,MFDAANN3,MF_AMES3,MFAN3,MFIMPSD3,MFIMPSC3,MFCDSED4,MFCCONT4,MFMINPS4,MFDAMES4,MFDAANN4,MF_AMES4,MFAN4,MFIMPSD4,MFIMPSC4,MFTOTDPS,MFTOTCPS,MFSALDPS,MFCODRE1,MFTRIRE1,MFRATRE1,MFMESER1,MFANNRE1,MFIMDRE1,MFIMCRE1,MFCODRE2,MFTRIRE2,MFRATRE2,MFMESER2,MFANNRE2,MFIMDRE2,MFIMCRE2,MFCODRE3,MFTRIRE3,MFRATRE3,MFMESER3,MFANNRE3,MFIMDRE3,MFIMCRE3,MFCODRE4,MFTRIRE4,MFRATRE4,MFMESER4,MFANNRE4,MFIMDRE4,MFIMCRE4,MFTOTDRE,MFTOTCRE,MFSALDRE,MFSINAI1,MF_NPOS1,MF_PACC1,MF_NRIF1,MFCAUSA1,MFIMDIL1,MFIMCIL1,MFSINAI2,MF_NPOS2,MF_PACC2,MF_NRIF2,MFCAUSA2,MFIMDIL2,MFIMCIL2,MFSINAI3,MF_NPOS3,MF_PACC3,MF_NRIF3,MFCAUSA3,MFIMDIL3,MFIMCIL3,MFTDINAI,MFTCINAI,MFSALINA,MFCDENTE,MFSDENT1,MFCCOAE1,MFCDPOS1,MFMSINE1,MFANINE1,MFMSFIE1,MFANF1,MFIMDAE1,MFIMCAE1,MFSDENT2,MFCCOAE2,MFCDPOS2,MFMSINE2,MFANINE2,MFMSFIE2,MFANF2,MFIMDAE2,MFIMCAE2,MFTDAENT,MFTCAENT,MFSALAEN,MFSALFIN,MFDESSTA,MFTIPMOD,MFCODFIR,MFCHKFIR,MFCARFIR,MFCOGFIR,MFNOMFIR,MFSESFIR,MFDATFIR,MFCOMFIR,MFPROFIR,MFDOMFIR,MFPRDFIR,MFCAPFIR,MFINDFIR,MFENTRAT,MFMAIVER,MFCOIBAN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MFSERIAL;
                  ,this.w_MFMESRIF;
                  ,this.w_MFANNRIF;
                  ,this.w_MFVALUTA;
                  ,this.w_MF_COINC;
                  ,this.w_MFCODUFF;
                  ,this.w_MFCODATT;
                  ,this.w_MFCDSED1;
                  ,this.w_MFCCONT1;
                  ,this.w_MFMINPS1;
                  ,this.w_MFDAMES1;
                  ,this.w_MFDAANN1;
                  ,this.w_MF_AMES1;
                  ,this.w_MFAN1;
                  ,this.w_MFIMPSD1;
                  ,this.w_MFIMPSC1;
                  ,this.w_MFCDSED2;
                  ,this.w_MFCCONT2;
                  ,this.w_MFMINPS2;
                  ,this.w_MFDAMES2;
                  ,this.w_MFDAANN2;
                  ,this.w_MF_AMES2;
                  ,this.w_MFAN2;
                  ,this.w_MFIMPSD2;
                  ,this.w_MFIMPSC2;
                  ,this.w_MFCDSED3;
                  ,this.w_MFCCONT3;
                  ,this.w_MFMINPS3;
                  ,this.w_MFDAMES3;
                  ,this.w_MFDAANN3;
                  ,this.w_MF_AMES3;
                  ,this.w_MFAN3;
                  ,this.w_MFIMPSD3;
                  ,this.w_MFIMPSC3;
                  ,this.w_MFCDSED4;
                  ,this.w_MFCCONT4;
                  ,this.w_MFMINPS4;
                  ,this.w_MFDAMES4;
                  ,this.w_MFDAANN4;
                  ,this.w_MF_AMES4;
                  ,this.w_MFAN4;
                  ,this.w_MFIMPSD4;
                  ,this.w_MFIMPSC4;
                  ,this.w_MFTOTDPS;
                  ,this.w_MFTOTCPS;
                  ,this.w_MFSALDPS;
                  ,this.w_MFCODRE1;
                  ,this.w_MFTRIRE1;
                  ,this.w_MFRATRE1;
                  ,this.w_MFMESER1;
                  ,this.w_MFANNRE1;
                  ,this.w_MFIMDRE1;
                  ,this.w_MFIMCRE1;
                  ,this.w_MFCODRE2;
                  ,this.w_MFTRIRE2;
                  ,this.w_MFRATRE2;
                  ,this.w_MFMESER2;
                  ,this.w_MFANNRE2;
                  ,this.w_MFIMDRE2;
                  ,this.w_MFIMCRE2;
                  ,this.w_MFCODRE3;
                  ,this.w_MFTRIRE3;
                  ,this.w_MFRATRE3;
                  ,this.w_MFMESER3;
                  ,this.w_MFANNRE3;
                  ,this.w_MFIMDRE3;
                  ,this.w_MFIMCRE3;
                  ,this.w_MFCODRE4;
                  ,this.w_MFTRIRE4;
                  ,this.w_MFRATRE4;
                  ,this.w_MFMESER4;
                  ,this.w_MFANNRE4;
                  ,this.w_MFIMDRE4;
                  ,this.w_MFIMCRE4;
                  ,this.w_MFTOTDRE;
                  ,this.w_MFTOTCRE;
                  ,this.w_MFSALDRE;
                  ,this.w_MFSINAI1;
                  ,this.w_MF_NPOS1;
                  ,this.w_MF_PACC1;
                  ,this.w_MF_NRIF1;
                  ,this.w_MFCAUSA1;
                  ,this.w_MFIMDIL1;
                  ,this.w_MFIMCIL1;
                  ,this.w_MFSINAI2;
                  ,this.w_MF_NPOS2;
                  ,this.w_MF_PACC2;
                  ,this.w_MF_NRIF2;
                  ,this.w_MFCAUSA2;
                  ,this.w_MFIMDIL2;
                  ,this.w_MFIMCIL2;
                  ,this.w_MFSINAI3;
                  ,this.w_MF_NPOS3;
                  ,this.w_MF_PACC3;
                  ,this.w_MF_NRIF3;
                  ,this.w_MFCAUSA3;
                  ,this.w_MFIMDIL3;
                  ,this.w_MFIMCIL3;
                  ,this.w_MFTDINAI;
                  ,this.w_MFTCINAI;
                  ,this.w_MFSALINA;
                  ,this.w_MFCDENTE;
                  ,this.w_MFSDENT1;
                  ,this.w_MFCCOAE1;
                  ,this.w_MFCDPOS1;
                  ,this.w_MFMSINE1;
                  ,this.w_MFANINE1;
                  ,this.w_MFMSFIE1;
                  ,this.w_MFANF1;
                  ,this.w_MFIMDAE1;
                  ,this.w_MFIMCAE1;
                  ,this.w_MFSDENT2;
                  ,this.w_MFCCOAE2;
                  ,this.w_MFCDPOS2;
                  ,this.w_MFMSINE2;
                  ,this.w_MFANINE2;
                  ,this.w_MFMSFIE2;
                  ,this.w_MFANF2;
                  ,this.w_MFIMDAE2;
                  ,this.w_MFIMCAE2;
                  ,this.w_MFTDAENT;
                  ,this.w_MFTCAENT;
                  ,this.w_MFSALAEN;
                  ,this.w_MFSALFIN;
                  ,this.w_MFDESSTA;
                  ,this.w_MFTIPMOD;
                  ,this.w_MFCODFIR;
                  ,this.w_MFCHKFIR;
                  ,this.w_MFCARFIR;
                  ,this.w_MFCOGFIR;
                  ,this.w_MFNOMFIR;
                  ,this.w_MFSESFIR;
                  ,this.w_MFDATFIR;
                  ,this.w_MFCOMFIR;
                  ,this.w_MFPROFIR;
                  ,this.w_MFDOMFIR;
                  ,this.w_MFPRDFIR;
                  ,this.w_MFCAPFIR;
                  ,this.w_MFINDFIR;
                  ,this.w_MFENTRAT;
                  ,this.w_MFMAIVER;
                  ,this.w_MFCOIBAN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- gscg_aop
    * Forza aggiornamento del database
    this.bupdated=.t.
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MOD_PAG_IDX,i_nConn)
      *
      * update MOD_PAG
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MOD_PAG')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MFMESRIF="+cp_ToStrODBC(this.w_MFMESRIF)+;
             ",MFANNRIF="+cp_ToStrODBC(this.w_MFANNRIF)+;
             ",MFVALUTA="+cp_ToStrODBC(this.w_MFVALUTA)+;
             ",MF_COINC="+cp_ToStrODBC(this.w_MF_COINC)+;
             ",MFCODUFF="+cp_ToStrODBCNull(this.w_MFCODUFF)+;
             ",MFCODATT="+cp_ToStrODBC(this.w_MFCODATT)+;
             ",MFCDSED1="+cp_ToStrODBCNull(this.w_MFCDSED1)+;
             ",MFCCONT1="+cp_ToStrODBCNull(this.w_MFCCONT1)+;
             ",MFMINPS1="+cp_ToStrODBC(this.w_MFMINPS1)+;
             ",MFDAMES1="+cp_ToStrODBC(this.w_MFDAMES1)+;
             ",MFDAANN1="+cp_ToStrODBC(this.w_MFDAANN1)+;
             ",MF_AMES1="+cp_ToStrODBC(this.w_MF_AMES1)+;
             ",MFAN1="+cp_ToStrODBC(this.w_MFAN1)+;
             ",MFIMPSD1="+cp_ToStrODBC(this.w_MFIMPSD1)+;
             ",MFIMPSC1="+cp_ToStrODBC(this.w_MFIMPSC1)+;
             ",MFCDSED2="+cp_ToStrODBCNull(this.w_MFCDSED2)+;
             ",MFCCONT2="+cp_ToStrODBCNull(this.w_MFCCONT2)+;
             ",MFMINPS2="+cp_ToStrODBC(this.w_MFMINPS2)+;
             ",MFDAMES2="+cp_ToStrODBC(this.w_MFDAMES2)+;
             ",MFDAANN2="+cp_ToStrODBC(this.w_MFDAANN2)+;
             ",MF_AMES2="+cp_ToStrODBC(this.w_MF_AMES2)+;
             ",MFAN2="+cp_ToStrODBC(this.w_MFAN2)+;
             ",MFIMPSD2="+cp_ToStrODBC(this.w_MFIMPSD2)+;
             ",MFIMPSC2="+cp_ToStrODBC(this.w_MFIMPSC2)+;
             ",MFCDSED3="+cp_ToStrODBCNull(this.w_MFCDSED3)+;
             ",MFCCONT3="+cp_ToStrODBCNull(this.w_MFCCONT3)+;
             ",MFMINPS3="+cp_ToStrODBC(this.w_MFMINPS3)+;
             ",MFDAMES3="+cp_ToStrODBC(this.w_MFDAMES3)+;
             ",MFDAANN3="+cp_ToStrODBC(this.w_MFDAANN3)+;
             ",MF_AMES3="+cp_ToStrODBC(this.w_MF_AMES3)+;
             ",MFAN3="+cp_ToStrODBC(this.w_MFAN3)+;
             ",MFIMPSD3="+cp_ToStrODBC(this.w_MFIMPSD3)+;
             ",MFIMPSC3="+cp_ToStrODBC(this.w_MFIMPSC3)+;
             ",MFCDSED4="+cp_ToStrODBCNull(this.w_MFCDSED4)+;
             ",MFCCONT4="+cp_ToStrODBCNull(this.w_MFCCONT4)+;
             ",MFMINPS4="+cp_ToStrODBC(this.w_MFMINPS4)+;
             ",MFDAMES4="+cp_ToStrODBC(this.w_MFDAMES4)+;
             ",MFDAANN4="+cp_ToStrODBC(this.w_MFDAANN4)+;
             ",MF_AMES4="+cp_ToStrODBC(this.w_MF_AMES4)+;
             ",MFAN4="+cp_ToStrODBC(this.w_MFAN4)+;
             ",MFIMPSD4="+cp_ToStrODBC(this.w_MFIMPSD4)+;
             ",MFIMPSC4="+cp_ToStrODBC(this.w_MFIMPSC4)+;
             ",MFTOTDPS="+cp_ToStrODBC(this.w_MFTOTDPS)+;
             ",MFTOTCPS="+cp_ToStrODBC(this.w_MFTOTCPS)+;
             ",MFSALDPS="+cp_ToStrODBC(this.w_MFSALDPS)+;
             ",MFCODRE1="+cp_ToStrODBCNull(this.w_MFCODRE1)+;
             ",MFTRIRE1="+cp_ToStrODBCNull(this.w_MFTRIRE1)+;
             ",MFRATRE1="+cp_ToStrODBC(this.w_MFRATRE1)+;
             ",MFMESER1="+cp_ToStrODBC(this.w_MFMESER1)+;
             ",MFANNRE1="+cp_ToStrODBC(this.w_MFANNRE1)+;
             ",MFIMDRE1="+cp_ToStrODBC(this.w_MFIMDRE1)+;
             ",MFIMCRE1="+cp_ToStrODBC(this.w_MFIMCRE1)+;
             ",MFCODRE2="+cp_ToStrODBCNull(this.w_MFCODRE2)+;
             ",MFTRIRE2="+cp_ToStrODBCNull(this.w_MFTRIRE2)+;
             ",MFRATRE2="+cp_ToStrODBC(this.w_MFRATRE2)+;
             ",MFMESER2="+cp_ToStrODBC(this.w_MFMESER2)+;
             ",MFANNRE2="+cp_ToStrODBC(this.w_MFANNRE2)+;
             ",MFIMDRE2="+cp_ToStrODBC(this.w_MFIMDRE2)+;
             ",MFIMCRE2="+cp_ToStrODBC(this.w_MFIMCRE2)+;
             ",MFCODRE3="+cp_ToStrODBCNull(this.w_MFCODRE3)+;
             ",MFTRIRE3="+cp_ToStrODBCNull(this.w_MFTRIRE3)+;
             ",MFRATRE3="+cp_ToStrODBC(this.w_MFRATRE3)+;
             ",MFMESER3="+cp_ToStrODBC(this.w_MFMESER3)+;
             ",MFANNRE3="+cp_ToStrODBC(this.w_MFANNRE3)+;
             ",MFIMDRE3="+cp_ToStrODBC(this.w_MFIMDRE3)+;
             ",MFIMCRE3="+cp_ToStrODBC(this.w_MFIMCRE3)+;
             ",MFCODRE4="+cp_ToStrODBCNull(this.w_MFCODRE4)+;
             ",MFTRIRE4="+cp_ToStrODBCNull(this.w_MFTRIRE4)+;
             ",MFRATRE4="+cp_ToStrODBC(this.w_MFRATRE4)+;
             ",MFMESER4="+cp_ToStrODBC(this.w_MFMESER4)+;
             ",MFANNRE4="+cp_ToStrODBC(this.w_MFANNRE4)+;
             ",MFIMDRE4="+cp_ToStrODBC(this.w_MFIMDRE4)+;
             ",MFIMCRE4="+cp_ToStrODBC(this.w_MFIMCRE4)+;
             ",MFTOTDRE="+cp_ToStrODBC(this.w_MFTOTDRE)+;
             ",MFTOTCRE="+cp_ToStrODBC(this.w_MFTOTCRE)+;
             ",MFSALDRE="+cp_ToStrODBC(this.w_MFSALDRE)+;
             ",MFSINAI1="+cp_ToStrODBCNull(this.w_MFSINAI1)+;
             ",MF_NPOS1="+cp_ToStrODBC(this.w_MF_NPOS1)+;
             ",MF_PACC1="+cp_ToStrODBC(this.w_MF_PACC1)+;
             ",MF_NRIF1="+cp_ToStrODBC(this.w_MF_NRIF1)+;
             ",MFCAUSA1="+cp_ToStrODBC(this.w_MFCAUSA1)+;
             ",MFIMDIL1="+cp_ToStrODBC(this.w_MFIMDIL1)+;
             ",MFIMCIL1="+cp_ToStrODBC(this.w_MFIMCIL1)+;
             ",MFSINAI2="+cp_ToStrODBCNull(this.w_MFSINAI2)+;
             ",MF_NPOS2="+cp_ToStrODBC(this.w_MF_NPOS2)+;
             ",MF_PACC2="+cp_ToStrODBC(this.w_MF_PACC2)+;
             ",MF_NRIF2="+cp_ToStrODBC(this.w_MF_NRIF2)+;
             ",MFCAUSA2="+cp_ToStrODBC(this.w_MFCAUSA2)+;
             ",MFIMDIL2="+cp_ToStrODBC(this.w_MFIMDIL2)+;
             ",MFIMCIL2="+cp_ToStrODBC(this.w_MFIMCIL2)+;
             ",MFSINAI3="+cp_ToStrODBCNull(this.w_MFSINAI3)+;
             ",MF_NPOS3="+cp_ToStrODBC(this.w_MF_NPOS3)+;
             ",MF_PACC3="+cp_ToStrODBC(this.w_MF_PACC3)+;
             ",MF_NRIF3="+cp_ToStrODBC(this.w_MF_NRIF3)+;
             ",MFCAUSA3="+cp_ToStrODBC(this.w_MFCAUSA3)+;
             ",MFIMDIL3="+cp_ToStrODBC(this.w_MFIMDIL3)+;
             ",MFIMCIL3="+cp_ToStrODBC(this.w_MFIMCIL3)+;
             ",MFTDINAI="+cp_ToStrODBC(this.w_MFTDINAI)+;
             ",MFTCINAI="+cp_ToStrODBC(this.w_MFTCINAI)+;
             ",MFSALINA="+cp_ToStrODBC(this.w_MFSALINA)+;
             ""
             i_nnn=i_nnn+;
             ",MFCDENTE="+cp_ToStrODBCNull(this.w_MFCDENTE)+;
             ",MFSDENT1="+cp_ToStrODBC(this.w_MFSDENT1)+;
             ",MFCCOAE1="+cp_ToStrODBCNull(this.w_MFCCOAE1)+;
             ",MFCDPOS1="+cp_ToStrODBC(this.w_MFCDPOS1)+;
             ",MFMSINE1="+cp_ToStrODBC(this.w_MFMSINE1)+;
             ",MFANINE1="+cp_ToStrODBC(this.w_MFANINE1)+;
             ",MFMSFIE1="+cp_ToStrODBC(this.w_MFMSFIE1)+;
             ",MFANF1="+cp_ToStrODBC(this.w_MFANF1)+;
             ",MFIMDAE1="+cp_ToStrODBC(this.w_MFIMDAE1)+;
             ",MFIMCAE1="+cp_ToStrODBC(this.w_MFIMCAE1)+;
             ",MFSDENT2="+cp_ToStrODBC(this.w_MFSDENT2)+;
             ",MFCCOAE2="+cp_ToStrODBCNull(this.w_MFCCOAE2)+;
             ",MFCDPOS2="+cp_ToStrODBC(this.w_MFCDPOS2)+;
             ",MFMSINE2="+cp_ToStrODBC(this.w_MFMSINE2)+;
             ",MFANINE2="+cp_ToStrODBC(this.w_MFANINE2)+;
             ",MFMSFIE2="+cp_ToStrODBC(this.w_MFMSFIE2)+;
             ",MFANF2="+cp_ToStrODBC(this.w_MFANF2)+;
             ",MFIMDAE2="+cp_ToStrODBC(this.w_MFIMDAE2)+;
             ",MFIMCAE2="+cp_ToStrODBC(this.w_MFIMCAE2)+;
             ",MFTDAENT="+cp_ToStrODBC(this.w_MFTDAENT)+;
             ",MFTCAENT="+cp_ToStrODBC(this.w_MFTCAENT)+;
             ",MFSALAEN="+cp_ToStrODBC(this.w_MFSALAEN)+;
             ",MFSALFIN="+cp_ToStrODBC(this.w_MFSALFIN)+;
             ",MFDESSTA="+cp_ToStrODBCNull(this.w_MFDESSTA)+;
             ",MFTIPMOD="+cp_ToStrODBC(this.w_MFTIPMOD)+;
             ",MFCODFIR="+cp_ToStrODBC(this.w_MFCODFIR)+;
             ",MFCHKFIR="+cp_ToStrODBC(this.w_MFCHKFIR)+;
             ",MFCARFIR="+cp_ToStrODBC(this.w_MFCARFIR)+;
             ",MFCOGFIR="+cp_ToStrODBC(this.w_MFCOGFIR)+;
             ",MFNOMFIR="+cp_ToStrODBC(this.w_MFNOMFIR)+;
             ",MFSESFIR="+cp_ToStrODBC(this.w_MFSESFIR)+;
             ",MFDATFIR="+cp_ToStrODBC(this.w_MFDATFIR)+;
             ",MFCOMFIR="+cp_ToStrODBC(this.w_MFCOMFIR)+;
             ",MFPROFIR="+cp_ToStrODBC(this.w_MFPROFIR)+;
             ",MFDOMFIR="+cp_ToStrODBC(this.w_MFDOMFIR)+;
             ",MFPRDFIR="+cp_ToStrODBC(this.w_MFPRDFIR)+;
             ",MFCAPFIR="+cp_ToStrODBC(this.w_MFCAPFIR)+;
             ",MFINDFIR="+cp_ToStrODBC(this.w_MFINDFIR)+;
             ",MFENTRAT="+cp_ToStrODBC(this.w_MFENTRAT)+;
             ",MFMAIVER="+cp_ToStrODBC(this.w_MFMAIVER)+;
             ",MFCOIBAN="+cp_ToStrODBC(this.w_MFCOIBAN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MOD_PAG')
        i_cWhere = cp_PKFox(i_cTable  ,'MFSERIAL',this.w_MFSERIAL  )
        UPDATE (i_cTable) SET;
              MFMESRIF=this.w_MFMESRIF;
             ,MFANNRIF=this.w_MFANNRIF;
             ,MFVALUTA=this.w_MFVALUTA;
             ,MF_COINC=this.w_MF_COINC;
             ,MFCODUFF=this.w_MFCODUFF;
             ,MFCODATT=this.w_MFCODATT;
             ,MFCDSED1=this.w_MFCDSED1;
             ,MFCCONT1=this.w_MFCCONT1;
             ,MFMINPS1=this.w_MFMINPS1;
             ,MFDAMES1=this.w_MFDAMES1;
             ,MFDAANN1=this.w_MFDAANN1;
             ,MF_AMES1=this.w_MF_AMES1;
             ,MFAN1=this.w_MFAN1;
             ,MFIMPSD1=this.w_MFIMPSD1;
             ,MFIMPSC1=this.w_MFIMPSC1;
             ,MFCDSED2=this.w_MFCDSED2;
             ,MFCCONT2=this.w_MFCCONT2;
             ,MFMINPS2=this.w_MFMINPS2;
             ,MFDAMES2=this.w_MFDAMES2;
             ,MFDAANN2=this.w_MFDAANN2;
             ,MF_AMES2=this.w_MF_AMES2;
             ,MFAN2=this.w_MFAN2;
             ,MFIMPSD2=this.w_MFIMPSD2;
             ,MFIMPSC2=this.w_MFIMPSC2;
             ,MFCDSED3=this.w_MFCDSED3;
             ,MFCCONT3=this.w_MFCCONT3;
             ,MFMINPS3=this.w_MFMINPS3;
             ,MFDAMES3=this.w_MFDAMES3;
             ,MFDAANN3=this.w_MFDAANN3;
             ,MF_AMES3=this.w_MF_AMES3;
             ,MFAN3=this.w_MFAN3;
             ,MFIMPSD3=this.w_MFIMPSD3;
             ,MFIMPSC3=this.w_MFIMPSC3;
             ,MFCDSED4=this.w_MFCDSED4;
             ,MFCCONT4=this.w_MFCCONT4;
             ,MFMINPS4=this.w_MFMINPS4;
             ,MFDAMES4=this.w_MFDAMES4;
             ,MFDAANN4=this.w_MFDAANN4;
             ,MF_AMES4=this.w_MF_AMES4;
             ,MFAN4=this.w_MFAN4;
             ,MFIMPSD4=this.w_MFIMPSD4;
             ,MFIMPSC4=this.w_MFIMPSC4;
             ,MFTOTDPS=this.w_MFTOTDPS;
             ,MFTOTCPS=this.w_MFTOTCPS;
             ,MFSALDPS=this.w_MFSALDPS;
             ,MFCODRE1=this.w_MFCODRE1;
             ,MFTRIRE1=this.w_MFTRIRE1;
             ,MFRATRE1=this.w_MFRATRE1;
             ,MFMESER1=this.w_MFMESER1;
             ,MFANNRE1=this.w_MFANNRE1;
             ,MFIMDRE1=this.w_MFIMDRE1;
             ,MFIMCRE1=this.w_MFIMCRE1;
             ,MFCODRE2=this.w_MFCODRE2;
             ,MFTRIRE2=this.w_MFTRIRE2;
             ,MFRATRE2=this.w_MFRATRE2;
             ,MFMESER2=this.w_MFMESER2;
             ,MFANNRE2=this.w_MFANNRE2;
             ,MFIMDRE2=this.w_MFIMDRE2;
             ,MFIMCRE2=this.w_MFIMCRE2;
             ,MFCODRE3=this.w_MFCODRE3;
             ,MFTRIRE3=this.w_MFTRIRE3;
             ,MFRATRE3=this.w_MFRATRE3;
             ,MFMESER3=this.w_MFMESER3;
             ,MFANNRE3=this.w_MFANNRE3;
             ,MFIMDRE3=this.w_MFIMDRE3;
             ,MFIMCRE3=this.w_MFIMCRE3;
             ,MFCODRE4=this.w_MFCODRE4;
             ,MFTRIRE4=this.w_MFTRIRE4;
             ,MFRATRE4=this.w_MFRATRE4;
             ,MFMESER4=this.w_MFMESER4;
             ,MFANNRE4=this.w_MFANNRE4;
             ,MFIMDRE4=this.w_MFIMDRE4;
             ,MFIMCRE4=this.w_MFIMCRE4;
             ,MFTOTDRE=this.w_MFTOTDRE;
             ,MFTOTCRE=this.w_MFTOTCRE;
             ,MFSALDRE=this.w_MFSALDRE;
             ,MFSINAI1=this.w_MFSINAI1;
             ,MF_NPOS1=this.w_MF_NPOS1;
             ,MF_PACC1=this.w_MF_PACC1;
             ,MF_NRIF1=this.w_MF_NRIF1;
             ,MFCAUSA1=this.w_MFCAUSA1;
             ,MFIMDIL1=this.w_MFIMDIL1;
             ,MFIMCIL1=this.w_MFIMCIL1;
             ,MFSINAI2=this.w_MFSINAI2;
             ,MF_NPOS2=this.w_MF_NPOS2;
             ,MF_PACC2=this.w_MF_PACC2;
             ,MF_NRIF2=this.w_MF_NRIF2;
             ,MFCAUSA2=this.w_MFCAUSA2;
             ,MFIMDIL2=this.w_MFIMDIL2;
             ,MFIMCIL2=this.w_MFIMCIL2;
             ,MFSINAI3=this.w_MFSINAI3;
             ,MF_NPOS3=this.w_MF_NPOS3;
             ,MF_PACC3=this.w_MF_PACC3;
             ,MF_NRIF3=this.w_MF_NRIF3;
             ,MFCAUSA3=this.w_MFCAUSA3;
             ,MFIMDIL3=this.w_MFIMDIL3;
             ,MFIMCIL3=this.w_MFIMCIL3;
             ,MFTDINAI=this.w_MFTDINAI;
             ,MFTCINAI=this.w_MFTCINAI;
             ,MFSALINA=this.w_MFSALINA;
             ,MFCDENTE=this.w_MFCDENTE;
             ,MFSDENT1=this.w_MFSDENT1;
             ,MFCCOAE1=this.w_MFCCOAE1;
             ,MFCDPOS1=this.w_MFCDPOS1;
             ,MFMSINE1=this.w_MFMSINE1;
             ,MFANINE1=this.w_MFANINE1;
             ,MFMSFIE1=this.w_MFMSFIE1;
             ,MFANF1=this.w_MFANF1;
             ,MFIMDAE1=this.w_MFIMDAE1;
             ,MFIMCAE1=this.w_MFIMCAE1;
             ,MFSDENT2=this.w_MFSDENT2;
             ,MFCCOAE2=this.w_MFCCOAE2;
             ,MFCDPOS2=this.w_MFCDPOS2;
             ,MFMSINE2=this.w_MFMSINE2;
             ,MFANINE2=this.w_MFANINE2;
             ,MFMSFIE2=this.w_MFMSFIE2;
             ,MFANF2=this.w_MFANF2;
             ,MFIMDAE2=this.w_MFIMDAE2;
             ,MFIMCAE2=this.w_MFIMCAE2;
             ,MFTDAENT=this.w_MFTDAENT;
             ,MFTCAENT=this.w_MFTCAENT;
             ,MFSALAEN=this.w_MFSALAEN;
             ,MFSALFIN=this.w_MFSALFIN;
             ,MFDESSTA=this.w_MFDESSTA;
             ,MFTIPMOD=this.w_MFTIPMOD;
             ,MFCODFIR=this.w_MFCODFIR;
             ,MFCHKFIR=this.w_MFCHKFIR;
             ,MFCARFIR=this.w_MFCARFIR;
             ,MFCOGFIR=this.w_MFCOGFIR;
             ,MFNOMFIR=this.w_MFNOMFIR;
             ,MFSESFIR=this.w_MFSESFIR;
             ,MFDATFIR=this.w_MFDATFIR;
             ,MFCOMFIR=this.w_MFCOMFIR;
             ,MFPROFIR=this.w_MFPROFIR;
             ,MFDOMFIR=this.w_MFDOMFIR;
             ,MFPRDFIR=this.w_MFPRDFIR;
             ,MFCAPFIR=this.w_MFCAPFIR;
             ,MFINDFIR=this.w_MFINDFIR;
             ,MFENTRAT=this.w_MFENTRAT;
             ,MFMAIVER=this.w_MFMAIVER;
             ,MFCOIBAN=this.w_MFCOIBAN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCG_AFR : Saving
      this.GSCG_AFR.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"EFSERIAL";
             )
      this.GSCG_AFR.mReplace()
      * --- GSCG_AVF : Saving
      this.GSCG_AVF.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"VFSERIAL";
             )
      this.GSCG_AVF.mReplace()
      * --- GSCG_AIG : Saving
      this.GSCG_AIG.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"IFSERIAL";
             )
      this.GSCG_AIG.mReplace()
      * --- GSCG_ACG : Saving
      this.GSCG_ACG.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MFSERIAL,"CFSERIAL";
             )
      this.GSCG_ACG.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCG_AFR : Deleting
    this.GSCG_AFR.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"EFSERIAL";
           )
    this.GSCG_AFR.mDelete()
    * --- GSCG_AVF : Deleting
    this.GSCG_AVF.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"VFSERIAL";
           )
    this.GSCG_AVF.mDelete()
    * --- GSCG_AIG : Deleting
    this.GSCG_AIG.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"IFSERIAL";
           )
    this.GSCG_AIG.mDelete()
    * --- GSCG_ACG : Deleting
    this.GSCG_ACG.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MFSERIAL,"CFSERIAL";
           )
    this.GSCG_ACG.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MOD_PAG_IDX,i_nConn)
      *
      * delete MOD_PAG
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MFSERIAL',this.w_MFSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .DoRTCalc(1,5,.t.)
        if .o_APPCOIN<>.w_APPCOIN
            .w_MF_COINC = .w_APPCOIN
        endif
        .DoRTCalc(7,10,.t.)
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .oPgFrm.Page2.oPag.oObj_2_13.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_15.Calculate()
        .DoRTCalc(13,16,.t.)
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFCCONT1 = iif(empty(.w_MFCDSED1),' ',.w_MFCCONT1)
          .link_3_3('Full')
        endif
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFMINPS1 = iif(empty(.w_MFCDSED1),' ',.w_MFMINPS1)
        endif
        if .o_MFCCONT1<>.w_MFCCONT1
            .w_APP1A = !empty(.w_MFCCONT1) and alltrim(.w_MFCCONT1)$ 'ABR-ABS-AC-ACON-DOM2-EMI-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-DSOS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        endif
        if .o_MFCCONT1<>.w_MFCCONT1.or. .o_APP1A<>.w_APP1A
            .w_APP1 = !empty(.w_MFCCONT1) and (alltrim(.w_MFCCONT1)$  ('AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBAT-EBTU-EMCO-EMDM-EMLA-FIPP-HTL1-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-IADP-ENBA-CCNL-ART1-ACIM-TCEB-SADP-SADD-PANE-EBA1'+'-EDFO-ENBC-ENBF-ENBI-FEDA-OPNC-CADD-EBNA-VITA-EBUC-CIFE-EST1-EBPA-EBCE-TUEB-LACC-FOIN-EBAR-ONBS'+'-ENFE-EBTS-MI53-EBIL-CUST-PULI-CLIS-EBAG-EBAN-EBMS-EBSA-EBTR-ENBL-EPAR-FSL1-EBMC-EART'+'-QUAS-EBIN-EBAS-EBIP-ASIM-EBIT-EBI1-EFEI-FASD-EBI7-EBG9-EBIM-1AST-EBAI-EBTI-EB04-RCAD-CALL-ENBT-EBCT-ANBA-EBLC-ENBP-ENP1-RCSO-EBNS-EBCA-BINT-EBCO-EBAP-EBFC-EBLE-EBFO-ASI1-EBIC-EBG1-ENB1-MET1-EBCD-ELAV-EBFW-FAST-FOSI-ENAB-EBIS-EBSP-EBIG-OBIL-EBIF'+'-ASS1-DM10-LAV1') or .w_APP1A)
        endif
        if .o_MFCDSED1<>.w_MFCDSED1.or. .o_APP1A<>.w_APP1A
            .w_MFDAMES1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1A,' ',.w_MFDAMES1))
        endif
        if .o_APP1A<>.w_APP1A.or. .o_MFCDSED1<>.w_MFCDSED1
            .w_MFDAANN1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1A,' ',.w_MFDAANN1))
        endif
        if .o_MFCDSED1<>.w_MFCDSED1.or. .o_APP1<>.w_APP1
            .w_MF_AMES1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,' ',.w_MF_AMES1))
        endif
        if .o_MFCDSED1<>.w_MFCDSED1.or. .o_APP1<>.w_APP1
            .w_MFAN1 = iif(empty(.w_MFCDSED1),' ',iif(.w_APP1,' ',.w_MFAN1))
        endif
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFIMPSD1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSD1)
        endif
        if .o_MFCDSED1<>.w_MFCDSED1
            .w_MFIMPSC1 = iif(empty(.w_MFCDSED1),0,.w_MFIMPSC1)
        endif
        .DoRTCalc(27,27,.t.)
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFCCONT2 = iif(empty(.w_MFCDSED2),' ',.w_MFCCONT2)
          .link_3_14('Full')
        endif
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFMINPS2 = iif(empty(.w_MFCDSED2),' ',.w_MFMINPS2)
        endif
        if .o_MFCCONT2<>.w_MFCCONT2
            .w_APP2A = !empty(.w_MFCCONT2) and alltrim(.w_MFCCONT2)$'ABR-ABS-AC-ACON-DOM2-EMI-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-DSOS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        endif
        if .o_APP2A<>.w_APP2A.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MFDAMES2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2A,' ',.w_MFDAMES2))
        endif
        if .o_APP2A<>.w_APP2A.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MFDAANN2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2A,' ',.w_MFDAANN2))
        endif
        if .o_MFCCONT2<>.w_MFCCONT2.or. .o_APP2A<>.w_APP2A
            .w_APP2 = !empty(.w_MFCCONT2) and (alltrim(.w_MFCCONT2)$  ('AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBAT-EBTU-EMCO-EMDM-EMLA-FIPP-HTL1-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-IADP-ENBA-CCNL-ART1-ACIM-TCEB-SADP-SADD-PANE-EBA1'+'-EDFO-ENBC-ENBF-ENBI-FEDA-OPNC-CADD-EBNA-VITA-EBUC-CIFE-EST1-EBPA-EBCE-TUEB-LACC-FOIN-EBAR-ONBS'+'-ENFE-EBTS-MI53-EBIL-CUST-PULI-CLIS-EBAG-EBAN-EBMS-EBSA-EBTR-ENBL-EPAR-FSL1-EBMC-EART'+'-QUAS-EBIN-EBAS-EBIP-ASIM-EBIT-EBI1-EFEI-FASD-EBI7-EBG9-EBIM-1AST-EBAI-EBTI-EB04-RCAD-CALL-ENBT-EBCT-ANBA-EBLC-ENBP-ENP1-RCSO-EBNS-EBCA-BINT-EBCO-EBAP-EBFC-EBLE-EBFO-ASI1-EBIC-EBG1-ENB1-MET1-EBCD-ELAV-EBFW-FAST-FOSI-ENAB-EBIS-EBSP-EBIG-OBIL-EBIF'+'-ASS1-DM10-LAV1') or .w_APP2A)
        endif
        if .o_APP2<>.w_APP2.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MF_AMES2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,' ',.w_MF_AMES2))
        endif
        if .o_APP2<>.w_APP2.or. .o_MFCDSED2<>.w_MFCDSED2
            .w_MFAN2 = iif(empty(.w_MFCDSED2),' ',iif(.w_APP2,' ',.w_MFAN2))
        endif
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFIMPSD2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSD2)
        endif
        if .o_MFCDSED2<>.w_MFCDSED2
            .w_MFIMPSC2 = iif(empty(.w_MFCDSED2),0,.w_MFIMPSC2)
        endif
        .DoRTCalc(38,38,.t.)
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFCCONT3 = iif(empty(.w_MFCDSED3),' ',.w_MFCCONT3)
          .link_3_25('Full')
        endif
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFMINPS3 = iif(empty(.w_MFCDSED3),' ',.w_MFMINPS3)
        endif
        if .o_MFCCONT3<>.w_MFCCONT3
            .w_APP3A = !empty(.w_MFCCONT3) and alltrim(.w_MFCCONT3)$ 'ABR-ABS-AC-ACON-DOM2-EMI-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-DSOS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        endif
        if .o_APP3A<>.w_APP3A.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MFDAMES3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3A,' ',.w_MFDAMES3))
        endif
        if .o_MFCCONT3<>.w_MFCCONT3.or. .o_APP3A<>.w_APP3A
            .w_APP3 = !empty(.w_MFCCONT3) and (alltrim(.w_MFCCONT3)$  ('AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBAT-EBTU-EMCO-EMDM-EMLA-FIPP-HTL1-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-IADP-ENBA-CCNL-ART1-ACIM-TCEB-SADP-SADD-PANE-EBA1'+'-EDFO-ENBC-ENBF-ENBI-FEDA-OPNC-CADD-EBNA-VITA-EBUC-CIFE-EST1-EBPA-EBCE-TUEB-LACC-FOIN-EBAR-ONBS'+'-ENFE-EBTS-MI53-EBIL-CUST-PULI-CLIS-EBAG-EBAN-EBMS-EBSA-EBTR-ENBL-EPAR-FSL1-EBMC-EART'+'-QUAS-EBIN-EBAS-EBIP-ASIM-EBIT-EBI1-EFEI-FASD-EBI7-EBG9-EBIM-1AST-EBAI-EBTI-EB04-RCAD-CALL-ENBT-EBCT-ANBA-EBLC-ENBP-ENP1-RCSO-EBNS-EBCA-BINT-EBCO-EBAP-EBFC-EBLE-EBFO-ASI1-EBIC-EBG1-ENB1-MET1-EBCD-ELAV-EBFW-FAST-FOSI-ENAB-EBIS-EBSP-EBIG-OBIL-EBIF'+'-ASS1-DM10-LAV1') or .w_APP3A)
        endif
        if .o_APP3A<>.w_APP3A.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MFDAANN3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3A,' ',.w_MFDAANN3))
        endif
        if .o_APP3<>.w_APP3.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MF_AMES3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,' ',.w_MF_AMES3))
        endif
        if .o_APP3<>.w_APP3.or. .o_MFCDSED3<>.w_MFCDSED3
            .w_MFAN3 = iif(empty(.w_MFCDSED3),' ',iif(.w_APP3,' ',.w_MFAN3))
        endif
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFIMPSD3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSD3)
        endif
        if .o_MFCDSED3<>.w_MFCDSED3
            .w_MFIMPSC3 = iif(empty(.w_MFCDSED3),0,.w_MFIMPSC3)
        endif
        .DoRTCalc(49,49,.t.)
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFCCONT4 = iif(empty(.w_MFCDSED4),' ',.w_MFCCONT4)
          .link_3_36('Full')
        endif
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFMINPS4 = iif(empty(.w_MFCDSED4),' ',.w_MFMINPS4)
        endif
        if .o_MFCCONT4<>.w_MFCCONT4
            .w_APP4A = !empty(.w_MFCCONT4) and alltrim(.w_MFCCONT4)$ 'ABR-ABS-AC-ACON-DOM2-EMI-FRI-LAZ-BAS-BLAA-BLAS-BOL-BPCF-CAL-CAM-CBS-CC-COC-COS-COSI-DCON-DLAA-DLAS-DMC-LIG-LOM-MAR-MOL-PCAS-PIE-POC-POS-DSOS-POSI-PUG-RCFL-SAR-SIC-TOS-TRE-UMB-UNSI-VAL-VEN-VMCF-VMCS'
        endif
        if .o_APP4A<>.w_APP4A.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MFDAMES4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4A,' ',.w_MFDAMES4))
        endif
        if .o_MFCCONT4<>.w_MFCCONT4.or. .o_APP4A<>.w_APP4A
            .w_APP4 = !empty(.w_MFCCONT4) and (alltrim(.w_MFCCONT4)$  ('AGRU-ASS-C10-CXX-DM10-DMRA-DOM4-DPC-DMRP-EBCM-EBAT-EBTU-EMCO-EMDM-EMLA-FIPP-HTL1-LAA-LAAP-PCF-PCFP-PCFS-PESC-PORT-TAFS-IADP-ENBA-CCNL-ART1-ACIM-TCEB-SADP-SADD-PANE-EBA1'+'-EDFO-ENBC-ENBF-ENBI-FEDA-OPNC-CADD-EBNA-VITA-EBUC-CIFE-EST1-EBPA-EBCE-TUEB-LACC-FOIN-EBAR-ONBS'+'-ENFE-EBTS-MI53-EBIL-CUST-PULI-CLIS-EBAG-EBAN-EBMS-EBSA-EBTR-ENBL-EPAR-FSL1-EBMC-EART'+'-QUAS-EBIN-EBAS-EBIP-ASIM-EBIT-EBI1-EFEI-FASD-EBI7-EBG9-EBIM-1AST-EBAI-EBTI-EB04-RCAD-CALL-ENBT-EBCT-ANBA-EBLC-ENBP-ENP1-RCSO-EBNS-EBCA-BINT-EBCO-EBAP-EBFC-EBLE-EBFO-ASI1-EBIC-EBG1-ENB1-MET1-EBCD-ELAV-EBFW-FAST-FOSI-ENAB-EBIS-EBSP-EBIG-OBIL-EBIF'+'-ASS1-DM10-LAV1') or .w_APP4A)
        endif
        if .o_APP4A<>.w_APP4A.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MFDAANN4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4A,' ',.w_MFDAANN4))
        endif
        if .o_APP4<>.w_APP4.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MF_AMES4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,' ',.w_MF_AMES4))
        endif
        if .o_APP4<>.w_APP4.or. .o_MFCDSED4<>.w_MFCDSED4
            .w_MFAN4 = iif(empty(.w_MFCDSED4),' ',iif(.w_APP4,' ',.w_MFAN4))
        endif
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFIMPSD4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSD4)
        endif
        if .o_MFCDSED4<>.w_MFCDSED4
            .w_MFIMPSC4 = iif(empty(.w_MFCDSED4),0,.w_MFIMPSC4)
        endif
            .w_MFTOTDPS = .w_MFIMPSD1+.w_MFIMPSD2+.w_MFIMPSD3+.w_MFIMPSD4
            .w_MFTOTCPS = .w_MFIMPSC1+.w_MFIMPSC2+.w_MFIMPSC3+.w_MFIMPSC4
            .w_MFSALDPS = .w_MFTOTDPS-.w_MFTOTCPS
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(65,68,.t.)
        if .o_MFCODRE1<>.w_MFCODRE1.or. .o_MFTRIRE1<>.w_MFTRIRE1
            .w_MFTRIRE1 = iif(empty(.w_MFCODRE1),' ',.w_MFTRIRE1)
          .link_4_3('Full')
        endif
        if .o_MFCODRE1<>.w_MFCODRE1
            .w_MFRATRE1 = iif(empty(.w_MFCODRE1)  or .w_CHKOBBME1='S',' ',.w_MFRATRE1)
        endif
        if .o_MFCODRE1<>.w_MFCODRE1.or. .o_CHKOBBME1<>.w_CHKOBBME1
            .w_MFMESER1 = iif(empty(.w_MFCODRE1),' ',.w_MFMESER1)
        endif
        if .o_MFCODRE1<>.w_MFCODRE1.or. .o_MFTRIRE1<>.w_MFTRIRE1
            .w_MFANNRE1 = iif(empty(.w_MFCODRE1),' ',iif(.w_MFTRIRE1= '3805','0000',.w_MFANNRE1))
        endif
        if .o_MFCODRE1<>.w_MFCODRE1
            .w_MFIMDRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMDRE1)
        endif
        if .o_MFCODRE1<>.w_MFCODRE1
            .w_MFIMCRE1 = iif(empty(.w_MFCODRE1),0,.w_MFIMCRE1)
        endif
        .DoRTCalc(75,75,.t.)
        if .o_MFCODRE2<>.w_MFCODRE2.or. .o_MFTRIRE2<>.w_MFTRIRE2
            .w_MFTRIRE2 = iif(empty(.w_MFCODRE2),' ',.w_MFTRIRE2)
          .link_4_13('Full')
        endif
        if .o_MFCODRE2<>.w_MFCODRE2
            .w_MFRATRE2 = iif(empty(.w_MFCODRE2) or .w_CHKOBBME2='S',' ',.w_MFRATRE2)
        endif
        if .o_MFCODRE2<>.w_MFCODRE2.or. .o_CHKOBBME2<>.w_CHKOBBME2
            .w_MFMESER2 = iif(empty(.w_MFCODRE2),' ',.w_MFMESER2)
        endif
        if .o_MFCODRE2<>.w_MFCODRE2.or. .o_MFTRIRE2<>.w_MFTRIRE2
            .w_MFANNRE2 = iif(empty(.w_MFCODRE2),' ',iif(.w_MFTRIRE2 ='3805','0000',.w_MFANNRE2))
        endif
        if .o_MFCODRE2<>.w_MFCODRE2
            .w_MFIMDRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMDRE2)
        endif
        if .o_MFCODRE2<>.w_MFCODRE2
            .w_MFIMCRE2 = iif(empty(.w_MFCODRE2),0,.w_MFIMCRE2)
        endif
        .DoRTCalc(82,82,.t.)
        if .o_MFCODRE3<>.w_MFCODRE3.or. .o_MFTRIRE3<>.w_MFTRIRE3
            .w_MFTRIRE3 = iif(empty(.w_MFCODRE3),' ',.w_MFTRIRE3)
          .link_4_20('Full')
        endif
        if .o_MFCODRE3<>.w_MFCODRE3
            .w_MFRATRE3 = iif(empty(.w_MFCODRE3) or .w_CHKOBBME3='S',' ',.w_MFRATRE3)
        endif
        if .o_MFCODRE3<>.w_MFCODRE3.or. .o_CHKOBBME3<>.w_CHKOBBME3
            .w_MFMESER3 = iif(empty(.w_MFCODRE3),' ',.w_MFMESER3)
        endif
        if .o_MFCODRE3<>.w_MFCODRE3.or. .o_MFTRIRE3<>.w_MFTRIRE3
            .w_MFANNRE3 = iif(empty(.w_MFCODRE3),' ',iif(.w_MFTRIRE3 = '3805','0000',.w_MFANNRE3))
        endif
        if .o_MFCODRE3<>.w_MFCODRE3
            .w_MFIMDRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMDRE3)
        endif
        if .o_MFCODRE3<>.w_MFCODRE3
            .w_MFIMCRE3 = iif(empty(.w_MFCODRE3),0,.w_MFIMCRE3)
        endif
        .DoRTCalc(89,89,.t.)
        if .o_MFCODRE4<>.w_MFCODRE4.or. .o_MFTRIRE4<>.w_MFTRIRE4
            .w_MFTRIRE4 = iif(empty(.w_MFCODRE4),' ',.w_MFTRIRE4)
          .link_4_27('Full')
        endif
        if .o_MFCODRE4<>.w_MFCODRE4
            .w_MFRATRE4 = iif(empty(.w_MFCODRE4) or .w_CHKOBBME4='S',' ',.w_MFRATRE4)
        endif
        if .o_MFCODRE4<>.w_MFCODRE4.or. .o_CHKOBBME4<>.w_CHKOBBME4
            .w_MFMESER4 = iif(empty(.w_MFCODRE4),' ',.w_MFMESER4)
        endif
        if .o_MFCODRE4<>.w_MFCODRE4.or. .o_MFTRIRE4<>.w_MFTRIRE4
            .w_MFANNRE4 = iif(empty(.w_MFCODRE4),' ',iif( .w_MFTRIRE4 = '3805','0000',.w_MFANNRE4))
        endif
        if .o_MFCODRE4<>.w_MFCODRE4
            .w_MFIMDRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMDRE4)
        endif
        if .o_MFCODRE4<>.w_MFCODRE4
            .w_MFIMCRE4 = iif(empty(.w_MFCODRE4),0,.w_MFIMCRE4)
        endif
            .w_MFTOTDRE = .w_MFIMDRE1+.w_MFIMDRE2+.w_MFIMDRE3+.w_MFIMDRE4
            .w_MFTOTCRE = .w_MFIMCRE1+.w_MFIMCRE2+.w_MFIMCRE3+.w_MFIMCRE4
            .w_MFSALDRE = .w_MFTOTDRE-.w_MFTOTCRE
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(101,102,.t.)
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MF_NPOS1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NPOS1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MF_PACC1 = iif(empty(.w_MFSINAI1),' ',.w_MF_PACC1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MF_NRIF1 = iif(empty(.w_MFSINAI1),' ',.w_MF_NRIF1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MFCAUSA1 = iif(empty(.w_MFSINAI1),' ',.w_MFCAUSA1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MFIMDIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMDIL1)
        endif
        if .o_MFSINAI1<>.w_MFSINAI1
            .w_MFIMCIL1 = iif(empty(.w_MFSINAI1),0,.w_MFIMCIL1)
        endif
        .DoRTCalc(109,109,.t.)
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MF_NPOS2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NPOS2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MF_PACC2 = iif(empty(.w_MFSINAI2),' ',.w_MF_PACC2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MF_NRIF2 = iif(empty(.w_MFSINAI2),' ',.w_MF_NRIF2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MFCAUSA2 = iif(empty(.w_MFSINAI2),' ',.w_MFCAUSA2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MFIMDIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMDIL2)
        endif
        if .o_MFSINAI2<>.w_MFSINAI2
            .w_MFIMCIL2 = iif(empty(.w_MFSINAI2),0,.w_MFIMCIL2)
        endif
        .DoRTCalc(116,116,.t.)
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MF_NPOS3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NPOS3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MF_PACC3 = iif(empty(.w_MFSINAI3),' ',.w_MF_PACC3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MF_NRIF3 = iif(empty(.w_MFSINAI3),' ',.w_MF_NRIF3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MFCAUSA3 = iif(empty(.w_MFSINAI3),' ',.w_MFCAUSA3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MFIMDIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMDIL3)
        endif
        if .o_MFSINAI3<>.w_MFSINAI3
            .w_MFIMCIL3 = iif(empty(.w_MFSINAI3),0,.w_MFIMCIL3)
        endif
            .w_MFTDINAI = .w_MFIMDIL1+.w_MFIMDIL2+.w_MFIMDIL3
            .w_MFTCINAI = .w_MFIMCIL1+.w_MFIMCIL2+.w_MFIMCIL3
            .w_MFSALINA = .w_MFTDINAI-.w_MFTCINAI
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(128,129,.t.)
        if .o_MFCDENTE<>.w_MFCDENTE
            .w_VRSDENT1 = space(5)
          .link_6_4('Full')
        endif
        if .o_MFCDENTE<>.w_MFCDENTE
            .w_VPSDENT1 = space(2)
          .link_6_5('Full')
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_VRSDENT1<>.w_VRSDENT1.or. .o_VPSDENT1<>.w_VPSDENT1
            .w_MFSDENT1 = iif(.w_MFCDENTE='0003' or .w_MFCDENTE='0005' or .w_MFCDENTE='0006',.w_VPSDENT1,.w_VRSDENT1)
        endif
        if .o_MFCDENTE<>.w_MFCDENTE
            .w_MFCCOAE1 = iif(empty(.w_MFCDENTE) ,' ',.w_MFCCOAE1)
          .link_6_7('Full')
        endif
        if .o_MFSDENT1<>.w_MFSDENT1
            .w_OLSDENT1 = .w_MFSDENT1
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1
            .w_MFCDPOS1 = iif(empty(.w_MFCCOAE1),' ',.w_MFCDPOS1)
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1
            .w_MFMSINE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR',' ',.w_MFMSINE1))
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1
            .w_MFANINE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR',' ',.w_MFANINE1))
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1
            .w_MFMSFIE1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFMSFIE1))
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1
            .w_MFANF1 = iif(empty(.w_MFCCOAE1),' ',iif(alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFANF1))
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1
            .w_MFIMDAE1 = iif(empty(.w_MFCCOAE1),0,.w_MFIMDAE1)
        endif
        if .o_MFCCOAE1<>.w_MFCCOAE1.or. .o_MFCDENTE<>.w_MFCDENTE
            .w_MFIMCAE1 = iif(empty(.w_MFCCOAE1) or  .w_MFCDENTE='0003',0,.w_MFIMCAE1)
        endif
        if .o_MFCDENTE<>.w_MFCDENTE
            .w_VPSDENT2 = space(2)
          .link_6_16('Full')
        endif
        if .o_MFCDENTE<>.w_MFCDENTE
            .w_VRSDENT2 = space(5)
          .link_6_17('Full')
        endif
        if .o_MFCDENTE<>.w_MFCDENTE.or. .o_VRSDENT2<>.w_VRSDENT2.or. .o_VPSDENT2<>.w_VPSDENT2
            .w_MFSDENT2 = iif(.w_MFCDENTE='0003' or .w_MFCDENTE='0005' or .w_MFCDENTE='0006' ,.w_VPSDENT2,.w_VRSDENT2)
        endif
        if .o_MFCDENTE<>.w_MFCDENTE
            .w_MFCCOAE2 = iif(empty(.w_MFCDENTE),' ',.w_MFCCOAE2)
          .link_6_19('Full')
        endif
        if .o_MFSDENT2<>.w_MFSDENT2
            .w_OLSDENT2 = .w_MFSDENT2
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2
            .w_MFCDPOS2 = iif(empty(.w_MFCCOAE2),' ',.w_MFCDPOS2)
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2
            .w_MFMSINE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR',' ',.w_MFMSINE2))
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2
            .w_MFANINE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR',' ',.w_MFANINE2))
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2
            .w_MFMSFIE2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFMSFIE2))
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2
            .w_MFANF2 = iif(empty(.w_MFCCOAE2),' ',iif(alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR',' ',.w_MFANF2))
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2
            .w_MFIMDAE2 = iif(empty(.w_MFCCOAE2),0,.w_MFIMDAE2)
        endif
        if .o_MFCCOAE2<>.w_MFCCOAE2.or. .o_MFCDENTE<>.w_MFCDENTE
            .w_MFIMCAE2 = iif(empty(.w_MFCCOAE2) or .w_MFCDENTE='0003',0,.w_MFIMCAE2)
        endif
            .w_MFTDAENT = .w_MFIMDAE1+.w_MFIMDAE2
            .w_MFTCAENT = .w_MFIMCAE1+.w_MFIMCAE2
            .w_MFSALAEN = .w_MFTDAENT-.w_MFTCAENT
        .DoRTCalc(157,157,.t.)
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .DoRTCalc(160,163,.t.)
            .w_MFSALFIN = .w_APPOIMP + .w_APPOIMP2 + .w_MFSALDPS + .w_MFSALDRE + .w_MFSALINA + .w_MFSALAEN
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        .oPgFrm.Page6.oPag.oObj_6_51.Calculate()
        .DoRTCalc(167,175,.t.)
        if .o_MFDESSTA<>.w_MFDESSTA.or. .o_MFSERIAL<>.w_MFSERIAL.or. .o_CAP1<>.w_CAP1
            .w_CAP = iif(empty(NVL(.w_MFDESSTA,'')),g_capazi,.w_CAP1)
        endif
        if .o_MFDESSTA<>.w_MFDESSTA.or. .o_MFSERIAL<>.w_MFSERIAL.or. .o_LOCALI1<>.w_LOCALI1
            .w_LOCALI = UPPER(iif(EMPTY(NVL(.w_MFDESSTA,'')),g_locazi,.w_LOCALI1))
        endif
        if .o_MFDESSTA<>.w_MFDESSTA.or. .o_MFSERIAL<>.w_MFSERIAL.or. .o_PROVIN1<>.w_PROVIN1
            .w_PROVIN = UPPER(iif(empty(NVL(.w_MFDESSTA,'')),g_proazi,.w_PROVIN1))
        endif
        if .o_MFDESSTA<>.w_MFDESSTA.or. .o_MFSERIAL<>.w_MFSERIAL.or. .o_INDIRI1<>.w_INDIRI1
            .w_INDIRI = UPPER(iif(empty(NVL(.w_MFDESSTA,'')),g_indazi,.w_INDIRI1))
        endif
        .oPgFrm.Page4.oPag.oObj_4_52.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page4.oPag.oObj_4_53.Calculate(ah_MSGFORMAT('Mese di %0riferimento') )
        .oPgFrm.Page4.oPag.oObj_4_54.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page4.oPag.oObj_4_55.Calculate(ah_MSGFORMAT('Codice %0regione'))
        .oPgFrm.Page3.oPag.oObj_3_68.Calculate(ah_MSGFORMAT('Codice %0sede'))
        .oPgFrm.Page3.oPag.oObj_3_69.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page5.oPag.oObj_5_42.Calculate(ah_MSGFORMAT('Numero di %0riferimento'))
        .oPgFrm.Page6.oPag.oObj_6_52.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page6.oPag.oObj_6_53.Calculate(iif(.w_MFCDENTE<>'0003' AND .w_MFCDENTE<>'0005'  AND .w_MFCDENTE<>'0006' ,ah_MSGFORMAT('Codice %0sede'),ah_MSGFORMAT('Codice %0provincia')))
        .oPgFrm.Page6.oPag.oObj_6_54.Calculate(ah_MSGFORMAT('Codice %0posizione'))
        .DoRTCalc(180,186,.t.)
        if .o_RAPFIRM<>.w_RAPFIRM
          .link_8_1('Full')
        endif
            .w_READAZI = i_codazi
          .link_8_2('Full')
        .DoRTCalc(189,219,.t.)
        if .o_MFBANPAS<>.w_MFBANPAS
            .w_MFCOIBAN = SUBSTR(.w_IBAN,1,34)
        endif
        if .o_MFCHKFIR<>.w_MFCHKFIR.or. .o_MFCARFIR<>.w_MFCARFIR
          .Calculate_XIGBWIJOES()
        endif
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
        .DoRTCalc(221,221,.t.)
            .w_MFSALFIN = .w_APPOIMP + .w_APPOIMP2 + .w_MFSALDPS + .w_MFSALDRE + .w_MFSALINA + .w_MFSALAEN
            .w_MESE = .w_MFMESRIF
            .w_ANNO = .w_MFANNRIF
        if .o_MFCHKFIR<>.w_MFCHKFIR
          .Calculate_VNXBCMFFJG()
        endif
        .DoRTCalc(225,228,.t.)
        if .o_MFBANPAS<>.w_MFBANPAS
            .w_MFCOIBAN = SUBSTR(.w_IBAN,1,34)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"MODEL","i_CODAZI,w_MFSERIAL")
          .op_MFSERIAL = .w_MFSERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(230,230,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_13.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_15.Calculate()
        .oPgFrm.Page6.oPag.oObj_6_51.Calculate()
        .oPgFrm.Page4.oPag.oObj_4_52.Calculate(ah_MSGFORMAT('Anno di %0riferimento'))
        .oPgFrm.Page4.oPag.oObj_4_53.Calculate(ah_MSGFORMAT('Mese di %0riferimento') )
        .oPgFrm.Page4.oPag.oObj_4_54.Calculate(ah_MSGFORMAT('Cod. %0tributo'))
        .oPgFrm.Page4.oPag.oObj_4_55.Calculate(ah_MSGFORMAT('Codice %0regione'))
        .oPgFrm.Page3.oPag.oObj_3_68.Calculate(ah_MSGFORMAT('Codice %0sede'))
        .oPgFrm.Page3.oPag.oObj_3_69.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page5.oPag.oObj_5_42.Calculate(ah_MSGFORMAT('Numero di %0riferimento'))
        .oPgFrm.Page6.oPag.oObj_6_52.Calculate(ah_MSGFORMAT('Causale %0contributo'))
        .oPgFrm.Page6.oPag.oObj_6_53.Calculate(iif(.w_MFCDENTE<>'0003' AND .w_MFCDENTE<>'0005'  AND .w_MFCDENTE<>'0006' ,ah_MSGFORMAT('Codice %0sede'),ah_MSGFORMAT('Codice %0provincia')))
        .oPgFrm.Page6.oPag.oObj_6_54.Calculate(ah_MSGFORMAT('Codice %0posizione'))
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate()
    endwith
  return

  proc Calculate_XFNGMSLKPR()
    with this
          * --- Inizializza check firmatario
          .w_MFCHKFIR = IIF (.w_MFENTRAT='E' , IIF (this.gscg_acg.w_CFPERFIS='N', 'S' , ' ' ) , ' ')
    endwith
  endproc
  proc Calculate_XIGBWIJOES()
    with this
          * --- Aggiorna i dati del contribuente
          AggionaContibuente(this;
             )
    endwith
  endproc
  proc Calculate_VNXBCMFFJG()
    with this
          * --- Sbianca campi firmatario
          .w_MFCODFIR = IIF ( .w_MFCHKFIR='S'  ,  .w_MFCODFIR ,space (16))
          .w_MFCARFIR = IIF ( .w_MFCHKFIR='S'  ,  .w_MFCARFIR ,space (1))
          .w_MFCOGFIR = IIF ( .w_MFCHKFIR='S'  ,  .w_MFCOGFIR ,space (24))
          .w_MFNOMFIR = IIF ( .w_MFCHKFIR='S'  ,  .w_MFNOMFIR ,space (20))
          .w_MFSESFIR = IIF ( .w_MFCHKFIR='S'  ,  .w_MFSESFIR ,'M')
          .w_MFDATFIR = IIF ( .w_MFCHKFIR='S'  ,  .w_MFDATFIR ,ctod('  /  /  '))
          .w_MFCOMFIR = IIF ( .w_MFCHKFIR='S'  ,  .w_MFCOMFIR ,space (40))
          .w_MFPROFIR = IIF ( .w_MFCHKFIR='S'  ,  .w_MFPROFIR ,space (2))
          .w_MFDOMFIR = IIF ( .w_MFCHKFIR='S'  , .w_MFDOMFIR ,space (40))
          .w_MFPRDFIR = IIF ( .w_MFCHKFIR='S'  , .w_MFPRDFIR ,space (2))
          .w_MFCAPFIR = IIF ( .w_MFCHKFIR='S'  , .w_MFCAPFIR ,space (5))
          .w_MFINDFIR = IIF ( .w_MFCHKFIR='S'  , .w_MFINDFIR ,space (35))
          .w_MFMAIVER = IIF ( .w_MFCHKFIR='S'  , .w_MFMAIVER ,space (60))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.enabled = this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.enabled = this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES1_3_7.enabled = this.oPgFrm.Page3.oPag.oMFDAMES1_3_7.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN1_3_8.enabled = this.oPgFrm.Page3.oPag.oMFDAANN1_3_8.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES1_3_9.enabled = this.oPgFrm.Page3.oPag.oMF_AMES1_3_9.mCond()
    this.oPgFrm.Page3.oPag.oMFAN1_3_10.enabled = this.oPgFrm.Page3.oPag.oMFAN1_3_10.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD1_3_11.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD1_3_11.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC1_3_12.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC1_3_12.mCond()
    this.oPgFrm.Page3.oPag.oMFCCONT2_3_14.enabled = this.oPgFrm.Page3.oPag.oMFCCONT2_3_14.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS2_3_15.enabled = this.oPgFrm.Page3.oPag.oMFMINPS2_3_15.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES2_3_17.enabled = this.oPgFrm.Page3.oPag.oMFDAMES2_3_17.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN2_3_18.enabled = this.oPgFrm.Page3.oPag.oMFDAANN2_3_18.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES2_3_20.enabled = this.oPgFrm.Page3.oPag.oMF_AMES2_3_20.mCond()
    this.oPgFrm.Page3.oPag.oMFAN2_3_21.enabled = this.oPgFrm.Page3.oPag.oMFAN2_3_21.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD2_3_22.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD2_3_22.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC2_3_23.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC2_3_23.mCond()
    this.oPgFrm.Page3.oPag.oMFCCONT3_3_25.enabled = this.oPgFrm.Page3.oPag.oMFCCONT3_3_25.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS3_3_26.enabled = this.oPgFrm.Page3.oPag.oMFMINPS3_3_26.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES3_3_28.enabled = this.oPgFrm.Page3.oPag.oMFDAMES3_3_28.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN3_3_30.enabled = this.oPgFrm.Page3.oPag.oMFDAANN3_3_30.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES3_3_31.enabled = this.oPgFrm.Page3.oPag.oMF_AMES3_3_31.mCond()
    this.oPgFrm.Page3.oPag.oMFAN3_3_32.enabled = this.oPgFrm.Page3.oPag.oMFAN3_3_32.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD3_3_33.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD3_3_33.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC3_3_34.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC3_3_34.mCond()
    this.oPgFrm.Page3.oPag.oMFCCONT4_3_36.enabled = this.oPgFrm.Page3.oPag.oMFCCONT4_3_36.mCond()
    this.oPgFrm.Page3.oPag.oMFMINPS4_3_37.enabled = this.oPgFrm.Page3.oPag.oMFMINPS4_3_37.mCond()
    this.oPgFrm.Page3.oPag.oMFDAMES4_3_39.enabled = this.oPgFrm.Page3.oPag.oMFDAMES4_3_39.mCond()
    this.oPgFrm.Page3.oPag.oMFDAANN4_3_41.enabled = this.oPgFrm.Page3.oPag.oMFDAANN4_3_41.mCond()
    this.oPgFrm.Page3.oPag.oMF_AMES4_3_42.enabled = this.oPgFrm.Page3.oPag.oMF_AMES4_3_42.mCond()
    this.oPgFrm.Page3.oPag.oMFAN4_3_43.enabled = this.oPgFrm.Page3.oPag.oMFAN4_3_43.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSD4_3_44.enabled = this.oPgFrm.Page3.oPag.oMFIMPSD4_3_44.mCond()
    this.oPgFrm.Page3.oPag.oMFIMPSC4_3_45.enabled = this.oPgFrm.Page3.oPag.oMFIMPSC4_3_45.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE1_4_3.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE1_4_3.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE1_4_4.enabled = this.oPgFrm.Page4.oPag.oMFRATRE1_4_4.mCond()
    this.oPgFrm.Page4.oPag.oMFMESER1_4_5.enabled = this.oPgFrm.Page4.oPag.oMFMESER1_4_5.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE1_4_6.enabled = this.oPgFrm.Page4.oPag.oMFANNRE1_4_6.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE1_4_7.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE1_4_7.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE1_4_8.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE1_4_8.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE2_4_13.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE2_4_13.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE2_4_14.enabled = this.oPgFrm.Page4.oPag.oMFRATRE2_4_14.mCond()
    this.oPgFrm.Page4.oPag.oMFMESER2_4_15.enabled = this.oPgFrm.Page4.oPag.oMFMESER2_4_15.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE2_4_16.enabled = this.oPgFrm.Page4.oPag.oMFANNRE2_4_16.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE2_4_17.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE2_4_17.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE2_4_18.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE2_4_18.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE3_4_20.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE3_4_20.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE3_4_21.enabled = this.oPgFrm.Page4.oPag.oMFRATRE3_4_21.mCond()
    this.oPgFrm.Page4.oPag.oMFMESER3_4_22.enabled = this.oPgFrm.Page4.oPag.oMFMESER3_4_22.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE3_4_23.enabled = this.oPgFrm.Page4.oPag.oMFANNRE3_4_23.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE3_4_24.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE3_4_24.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE3_4_25.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE3_4_25.mCond()
    this.oPgFrm.Page4.oPag.oMFTRIRE4_4_27.enabled = this.oPgFrm.Page4.oPag.oMFTRIRE4_4_27.mCond()
    this.oPgFrm.Page4.oPag.oMFRATRE4_4_28.enabled = this.oPgFrm.Page4.oPag.oMFRATRE4_4_28.mCond()
    this.oPgFrm.Page4.oPag.oMFMESER4_4_29.enabled = this.oPgFrm.Page4.oPag.oMFMESER4_4_29.mCond()
    this.oPgFrm.Page4.oPag.oMFANNRE4_4_30.enabled = this.oPgFrm.Page4.oPag.oMFANNRE4_4_30.mCond()
    this.oPgFrm.Page4.oPag.oMFIMDRE4_4_31.enabled = this.oPgFrm.Page4.oPag.oMFIMDRE4_4_31.mCond()
    this.oPgFrm.Page4.oPag.oMFIMCRE4_4_32.enabled = this.oPgFrm.Page4.oPag.oMFIMCRE4_4_32.mCond()
    this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.enabled = this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.mCond()
    this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.enabled = this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.mCond()
    this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.enabled = this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.mCond()
    this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.enabled = this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.mCond()
    this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.enabled = this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.mCond()
    this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.enabled = this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.mCond()
    this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.enabled = this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.mCond()
    this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.enabled = this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.mCond()
    this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.enabled = this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.mCond()
    this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.enabled = this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.mCond()
    this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.enabled = this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.mCond()
    this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.enabled = this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.mCond()
    this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.enabled = this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.mCond()
    this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.enabled = this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.mCond()
    this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.enabled = this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.mCond()
    this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.enabled = this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.mCond()
    this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.enabled = this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.mCond()
    this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.enabled = this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.mCond()
    this.oPgFrm.Page6.oPag.oVRSDENT1_6_4.enabled = this.oPgFrm.Page6.oPag.oVRSDENT1_6_4.mCond()
    this.oPgFrm.Page6.oPag.oVPSDENT1_6_5.enabled = this.oPgFrm.Page6.oPag.oVPSDENT1_6_5.mCond()
    this.oPgFrm.Page6.oPag.oMFCCOAE1_6_7.enabled = this.oPgFrm.Page6.oPag.oMFCCOAE1_6_7.mCond()
    this.oPgFrm.Page6.oPag.oMFCDPOS1_6_9.enabled = this.oPgFrm.Page6.oPag.oMFCDPOS1_6_9.mCond()
    this.oPgFrm.Page6.oPag.oMFMSINE1_6_10.enabled = this.oPgFrm.Page6.oPag.oMFMSINE1_6_10.mCond()
    this.oPgFrm.Page6.oPag.oMFANINE1_6_11.enabled = this.oPgFrm.Page6.oPag.oMFANINE1_6_11.mCond()
    this.oPgFrm.Page6.oPag.oMFMSFIE1_6_12.enabled = this.oPgFrm.Page6.oPag.oMFMSFIE1_6_12.mCond()
    this.oPgFrm.Page6.oPag.oMFANF1_6_13.enabled = this.oPgFrm.Page6.oPag.oMFANF1_6_13.mCond()
    this.oPgFrm.Page6.oPag.oMFIMDAE1_6_14.enabled = this.oPgFrm.Page6.oPag.oMFIMDAE1_6_14.mCond()
    this.oPgFrm.Page6.oPag.oMFIMCAE1_6_15.enabled = this.oPgFrm.Page6.oPag.oMFIMCAE1_6_15.mCond()
    this.oPgFrm.Page6.oPag.oVPSDENT2_6_16.enabled = this.oPgFrm.Page6.oPag.oVPSDENT2_6_16.mCond()
    this.oPgFrm.Page6.oPag.oVRSDENT2_6_17.enabled = this.oPgFrm.Page6.oPag.oVRSDENT2_6_17.mCond()
    this.oPgFrm.Page6.oPag.oMFCCOAE2_6_19.enabled = this.oPgFrm.Page6.oPag.oMFCCOAE2_6_19.mCond()
    this.oPgFrm.Page6.oPag.oMFCDPOS2_6_21.enabled = this.oPgFrm.Page6.oPag.oMFCDPOS2_6_21.mCond()
    this.oPgFrm.Page6.oPag.oMFMSINE2_6_22.enabled = this.oPgFrm.Page6.oPag.oMFMSINE2_6_22.mCond()
    this.oPgFrm.Page6.oPag.oMFANINE2_6_23.enabled = this.oPgFrm.Page6.oPag.oMFANINE2_6_23.mCond()
    this.oPgFrm.Page6.oPag.oMFMSFIE2_6_24.enabled = this.oPgFrm.Page6.oPag.oMFMSFIE2_6_24.mCond()
    this.oPgFrm.Page6.oPag.oMFANF2_6_25.enabled = this.oPgFrm.Page6.oPag.oMFANF2_6_25.mCond()
    this.oPgFrm.Page6.oPag.oMFIMDAE2_6_26.enabled = this.oPgFrm.Page6.oPag.oMFIMDAE2_6_26.mCond()
    this.oPgFrm.Page6.oPag.oMFIMCAE2_6_27.enabled = this.oPgFrm.Page6.oPag.oMFIMCAE2_6_27.mCond()
    this.oPgFrm.Page8.oPag.oMFCODFIR_8_16.enabled = this.oPgFrm.Page8.oPag.oMFCODFIR_8_16.mCond()
    this.oPgFrm.Page8.oPag.oMFCARFIR_8_19.enabled = this.oPgFrm.Page8.oPag.oMFCARFIR_8_19.mCond()
    this.oPgFrm.Page8.oPag.oMFCOGFIR_8_20.enabled = this.oPgFrm.Page8.oPag.oMFCOGFIR_8_20.mCond()
    this.oPgFrm.Page8.oPag.oMFNOMFIR_8_21.enabled = this.oPgFrm.Page8.oPag.oMFNOMFIR_8_21.mCond()
    this.oPgFrm.Page8.oPag.oMFSESFIR_8_22.enabled = this.oPgFrm.Page8.oPag.oMFSESFIR_8_22.mCond()
    this.oPgFrm.Page8.oPag.oMFDATFIR_8_23.enabled = this.oPgFrm.Page8.oPag.oMFDATFIR_8_23.mCond()
    this.oPgFrm.Page8.oPag.oMFCOMFIR_8_24.enabled = this.oPgFrm.Page8.oPag.oMFCOMFIR_8_24.mCond()
    this.oPgFrm.Page8.oPag.oMFPROFIR_8_25.enabled = this.oPgFrm.Page8.oPag.oMFPROFIR_8_25.mCond()
    this.oPgFrm.Page8.oPag.oMFDOMFIR_8_26.enabled = this.oPgFrm.Page8.oPag.oMFDOMFIR_8_26.mCond()
    this.oPgFrm.Page8.oPag.oMFPRDFIR_8_27.enabled = this.oPgFrm.Page8.oPag.oMFPRDFIR_8_27.mCond()
    this.oPgFrm.Page8.oPag.oMFCAPFIR_8_28.enabled = this.oPgFrm.Page8.oPag.oMFCAPFIR_8_28.mCond()
    this.oPgFrm.Page8.oPag.oMFINDFIR_8_32.enabled = this.oPgFrm.Page8.oPag.oMFINDFIR_8_32.mCond()
    this.oPgFrm.Page8.oPag.oMFMAIVER_8_44.enabled = this.oPgFrm.Page8.oPag.oMFMAIVER_8_44.mCond()
    this.oPgFrm.Page6.oPag.oBtn_6_47.enabled = this.oPgFrm.Page6.oPag.oBtn_6_47.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show6
    i_show6=not(this.w_ENTRATEL='E')
    this.oPgFrm.Pages(7).enabled=i_show6
    this.oPgFrm.Pages(7).caption=iif(i_show6,cp_translate("Estr.versamento"),"")
    this.oPgFrm.Pages(7).oPag.visible=this.oPgFrm.Pages(7).enabled
    local i_show7
    i_show7=not(EMPTY (this.w_ENTRATEL))
    this.oPgFrm.Pages(8).enabled=i_show7
    this.oPgFrm.Pages(8).caption=iif(i_show7,cp_translate("Versante/Firmatario"),"")
    this.oPgFrm.Pages(8).oPag.visible=this.oPgFrm.Pages(8).enabled
    this.oPgFrm.Page6.oPag.oVRSDENT1_6_4.visible=!this.oPgFrm.Page6.oPag.oVRSDENT1_6_4.mHide()
    this.oPgFrm.Page6.oPag.oVPSDENT1_6_5.visible=!this.oPgFrm.Page6.oPag.oVPSDENT1_6_5.mHide()
    this.oPgFrm.Page6.oPag.oVPSDENT2_6_16.visible=!this.oPgFrm.Page6.oPag.oVPSDENT2_6_16.mHide()
    this.oPgFrm.Page6.oPag.oVRSDENT2_6_17.visible=!this.oPgFrm.Page6.oPag.oVRSDENT2_6_17.mHide()
    this.oPgFrm.Page6.oPag.oBtn_6_47.visible=!this.oPgFrm.Page6.oPag.oBtn_6_47.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_16.visible=!this.oPgFrm.Page2.oPag.oBtn_2_16.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_67.visible=!this.oPgFrm.Page3.oPag.oBtn_3_67.mHide()
    this.oPgFrm.Page8.oPag.oMFCODFIR_8_16.visible=!this.oPgFrm.Page8.oPag.oMFCODFIR_8_16.mHide()
    this.oPgFrm.Page8.oPag.oMFCOGFIR_8_20.visible=!this.oPgFrm.Page8.oPag.oMFCOGFIR_8_20.mHide()
    this.oPgFrm.Page8.oPag.oMFNOMFIR_8_21.visible=!this.oPgFrm.Page8.oPag.oMFNOMFIR_8_21.mHide()
    this.oPgFrm.Page8.oPag.oMFDATFIR_8_23.visible=!this.oPgFrm.Page8.oPag.oMFDATFIR_8_23.mHide()
    this.oPgFrm.Page8.oPag.oMFCOMFIR_8_24.visible=!this.oPgFrm.Page8.oPag.oMFCOMFIR_8_24.mHide()
    this.oPgFrm.Page8.oPag.oMFPROFIR_8_25.visible=!this.oPgFrm.Page8.oPag.oMFPROFIR_8_25.mHide()
    this.oPgFrm.Page8.oPag.oMFDOMFIR_8_26.visible=!this.oPgFrm.Page8.oPag.oMFDOMFIR_8_26.mHide()
    this.oPgFrm.Page8.oPag.oMFPRDFIR_8_27.visible=!this.oPgFrm.Page8.oPag.oMFPRDFIR_8_27.mHide()
    this.oPgFrm.Page8.oPag.oMFCAPFIR_8_28.visible=!this.oPgFrm.Page8.oPag.oMFCAPFIR_8_28.mHide()
    this.oPgFrm.Page8.oPag.oMFINDFIR_8_32.visible=!this.oPgFrm.Page8.oPag.oMFINDFIR_8_32.mHide()
    this.oPgFrm.Page8.oPag.oMFBANPAS_8_45.visible=!this.oPgFrm.Page8.oPag.oMFBANPAS_8_45.mHide()
    this.oPgFrm.Page8.oPag.oBADESCRI_8_46.visible=!this.oPgFrm.Page8.oPag.oBADESCRI_8_46.mHide()
    this.oPgFrm.Page8.oPag.oMFCOIBAN_8_47.visible=!this.oPgFrm.Page8.oPag.oMFCOIBAN_8_47.mHide()
    this.oPgFrm.Page8.oPag.oStr_8_60.visible=!this.oPgFrm.Page8.oPag.oStr_8_60.mHide()
    this.oPgFrm.Page8.oPag.oStr_8_61.visible=!this.oPgFrm.Page8.oPag.oStr_8_61.mHide()
    this.oPgFrm.Page8.oPag.oMFCOIBAN_8_66.visible=!this.oPgFrm.Page8.oPag.oMFCOIBAN_8_66.mHide()
    this.oPgFrm.Page8.oPag.oStr_8_67.visible=!this.oPgFrm.Page8.oPag.oStr_8_67.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_13.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_15.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_51.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_52.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_53.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_54.Event(cEvent)
      .oPgFrm.Page4.oPag.oObj_4_55.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_68.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_69.Event(cEvent)
      .oPgFrm.Page5.oPag.oObj_5_42.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_52.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_53.Event(cEvent)
      .oPgFrm.Page6.oPag.oObj_6_54.Event(cEvent)
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Init")
          .Calculate_XFNGMSLKPR()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
        if lower(cEvent)==lower("Blank") or lower(cEvent)==lower("Init")
          .Calculate_VNXBCMFFJG()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gscg_aop
    IF NOT ( this.w_MFCARFIR $ "'1','2','3','7'" )
    this.w_MFCARFIR ='1'
    ENDIF
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MFCODUFF
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CODI_UFF_IDX,3]
    i_lTable = "CODI_UFF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CODI_UFF_IDX,2], .t., this.CODI_UFF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CODI_UFF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODUFF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AUF',True,'CODI_UFF')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UFCODICE like "+cp_ToStrODBC(trim(this.w_MFCODUFF)+"%");

          i_ret=cp_SQL(i_nConn,"select UFCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UFCODICE',trim(this.w_MFCODUFF))
          select UFCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODUFF)==trim(_Link_.UFCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODUFF) and !this.bDontReportError
            deferred_cp_zoom('CODI_UFF','*','UFCODICE',cp_AbsName(oSource.parent,'oMFCODUFF_2_4'),i_cWhere,'GSCG_AUF',"Codici ufficio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',oSource.xKey(1))
            select UFCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODUFF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(this.w_MFCODUFF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',this.w_MFCODUFF)
            select UFCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODUFF = NVL(_Link_.UFCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODUFF = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CODI_UFF_IDX,2])+'\'+cp_ToStr(_Link_.UFCODICE,1)
      cp_ShowWarn(i_cKey,this.CODI_UFF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODUFF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED1
  func Link_3_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED1)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED1))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED1)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED1) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED1_3_2'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED1)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED1 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT1
  func Link_3_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT1)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT1))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT1)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT1) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT1_3_3'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT1)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT1 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED2
  func Link_3_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED2)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED2))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED2)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED2) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED2_3_13'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED2)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED2 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT2
  func Link_3_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT2)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT2))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT2)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT2) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT2_3_14'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT2)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT2 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED3
  func Link_3_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED3)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED3))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED3)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED3) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED3_3_24'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED3)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED3 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT3
  func Link_3_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT3)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT3))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT3)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT3) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT3_3_25'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT3)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT3 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDSED4
  func Link_3_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDSED4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_MFCDSED4)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_MFCDSED4))
          select PSCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDSED4)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDSED4) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oMFCDSED4_3_35'),i_cWhere,'GSCG_APS',"Codici sede INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDSED4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_MFCDSED4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_MFCDSED4)
            select PSCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDSED4 = NVL(_Link_.PSCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDSED4 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDSED4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCONT4
  func Link_3_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_INPS_IDX,3]
    i_lTable = "CAU_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2], .t., this.CAU_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCONT4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACS',True,'CAU_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CS_CAUSA like "+cp_ToStrODBC(trim(this.w_MFCCONT4)+"%");

          i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CS_CAUSA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CS_CAUSA',trim(this.w_MFCCONT4))
          select CS_CAUSA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CS_CAUSA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCONT4)==trim(_Link_.CS_CAUSA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCONT4) and !this.bDontReportError
            deferred_cp_zoom('CAU_INPS','*','CS_CAUSA',cp_AbsName(oSource.parent,'oMFCCONT4_3_36'),i_cWhere,'GSCG_ACS',"Causali contributo INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                     +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',oSource.xKey(1))
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCONT4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CS_CAUSA";
                   +" from "+i_cTable+" "+i_lTable+" where CS_CAUSA="+cp_ToStrODBC(this.w_MFCCONT4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CS_CAUSA',this.w_MFCCONT4)
            select CS_CAUSA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCONT4 = NVL(_Link_.CS_CAUSA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCONT4 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_INPS_IDX,2])+'\'+cp_ToStr(_Link_.CS_CAUSA,1)
      cp_ShowWarn(i_cKey,this.CAU_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCONT4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCODRE1
  func Link_4_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE1)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE1))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE1)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE1) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE1_4_2'),i_cWhere,'GSCG_ARP',"Codici regioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE1)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE1 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE1 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE1
  func Link_4_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE1)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE1))
          select TRCODICE,TRTIPTRI,TRFLMERF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE1)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE1) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE1_4_3'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE1)
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE1 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
      this.w_CHKOBBME1 = NVL(_Link_.TRFLMERF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE1 = space(5)
      endif
      this.w_TIPTRI = space(10)
      this.w_CHKOBBME1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI='Regione' or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
        endif
        this.w_MFTRIRE1 = space(5)
        this.w_TIPTRI = space(10)
        this.w_CHKOBBME1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_3.TRCODICE as TRCODICE403"+ ",link_4_3.TRTIPTRI as TRTIPTRI403"+ ",link_4_3.TRFLMERF as TRFLMERF403"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_3 on MOD_PAG.MFTRIRE1=link_4_3.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_3"
          i_cKey=i_cKey+'+" and MOD_PAG.MFTRIRE1=link_4_3.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MFCODRE2
  func Link_4_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE2)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE2))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE2)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE2) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE2_4_12'),i_cWhere,'GSCG_ARP',"Codici regioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE2)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE2 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE2 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE2
  func Link_4_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE2))
          select TRCODICE,TRTIPTRI,TRFLMERF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE2)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE2) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE2_4_13'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE2)
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE2 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
      this.w_CHKOBBME2 = NVL(_Link_.TRFLMERF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE2 = space(5)
      endif
      this.w_TIPTRI = space(10)
      this.w_CHKOBBME2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI='Regione'  or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
        endif
        this.w_MFTRIRE2 = space(5)
        this.w_TIPTRI = space(10)
        this.w_CHKOBBME2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_13.TRCODICE as TRCODICE413"+ ",link_4_13.TRTIPTRI as TRTIPTRI413"+ ",link_4_13.TRFLMERF as TRFLMERF413"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_13 on MOD_PAG.MFTRIRE2=link_4_13.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_13"
          i_cKey=i_cKey+'+" and MOD_PAG.MFTRIRE2=link_4_13.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MFCODRE3
  func Link_4_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE3)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE3))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE3)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE3) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE3_4_19'),i_cWhere,'GSCG_ARP',"Codici regioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE3)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE3 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE3 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE3
  func Link_4_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE3)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE3))
          select TRCODICE,TRTIPTRI,TRFLMERF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE3)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE3) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE3_4_20'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE3)
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE3 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
      this.w_CHKOBBME3 = NVL(_Link_.TRFLMERF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE3 = space(5)
      endif
      this.w_TIPTRI = space(10)
      this.w_CHKOBBME3 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI='Regione'   or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
        endif
        this.w_MFTRIRE3 = space(5)
        this.w_TIPTRI = space(10)
        this.w_CHKOBBME3 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_20.TRCODICE as TRCODICE420"+ ",link_4_20.TRTIPTRI as TRTIPTRI420"+ ",link_4_20.TRFLMERF as TRFLMERF420"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_20 on MOD_PAG.MFTRIRE3=link_4_20.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_20"
          i_cKey=i_cKey+'+" and MOD_PAG.MFTRIRE3=link_4_20.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MFCODRE4
  func Link_4_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REG_PROV_IDX,3]
    i_lTable = "REG_PROV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2], .t., this.REG_PROV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCODRE4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ARP',True,'REG_PROV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RPCODICE like "+cp_ToStrODBC(trim(this.w_MFCODRE4)+"%");

          i_ret=cp_SQL(i_nConn,"select RPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RPCODICE',trim(this.w_MFCODRE4))
          select RPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCODRE4)==trim(_Link_.RPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCODRE4) and !this.bDontReportError
            deferred_cp_zoom('REG_PROV','*','RPCODICE',cp_AbsName(oSource.parent,'oMFCODRE4_4_26'),i_cWhere,'GSCG_ARP',"Codici regioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',oSource.xKey(1))
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCODRE4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where RPCODICE="+cp_ToStrODBC(this.w_MFCODRE4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RPCODICE',this.w_MFCODRE4)
            select RPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCODRE4 = NVL(_Link_.RPCODICE,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MFCODRE4 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REG_PROV_IDX,2])+'\'+cp_ToStr(_Link_.RPCODICE,1)
      cp_ShowWarn(i_cKey,this.REG_PROV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCODRE4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFTRIRE4
  func Link_4_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFTRIRE4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_MFTRIRE4)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_MFTRIRE4))
          select TRCODICE,TRTIPTRI,TRFLMERF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFTRIRE4)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFTRIRE4) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oMFTRIRE4_4_27'),i_cWhere,'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFTRIRE4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI,TRFLMERF";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_MFTRIRE4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_MFTRIRE4)
            select TRCODICE,TRTIPTRI,TRFLMERF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFTRIRE4 = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
      this.w_CHKOBBME4 = NVL(_Link_.TRFLMERF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MFTRIRE4 = space(5)
      endif
      this.w_TIPTRI = space(10)
      this.w_CHKOBBME4 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI='Regione'  or empty(.w_TIPTRI)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
        endif
        this.w_MFTRIRE4 = space(5)
        this.w_TIPTRI = space(10)
        this.w_CHKOBBME4 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFTRIRE4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_27(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_27.TRCODICE as TRCODICE427"+ ",link_4_27.TRTIPTRI as TRTIPTRI427"+ ",link_4_27.TRFLMERF as TRFLMERF427"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_27 on MOD_PAG.MFTRIRE4=link_4_27.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_27"
          i_cKey=i_cKey+'+" and MOD_PAG.MFTRIRE4=link_4_27.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MFSINAI1
  func Link_5_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_lTable = "SE_INAIL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2], .t., this.SE_INAIL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFSINAI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ASI',True,'SE_INAIL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SICODICE like "+cp_ToStrODBC(trim(this.w_MFSINAI1)+"%");

          i_ret=cp_SQL(i_nConn,"select SICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SICODICE',trim(this.w_MFSINAI1))
          select SICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFSINAI1)==trim(_Link_.SICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFSINAI1) and !this.bDontReportError
            deferred_cp_zoom('SE_INAIL','*','SICODICE',cp_AbsName(oSource.parent,'oMFSINAI1_5_2'),i_cWhere,'GSCG_ASI',"Codici sede INAIL",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',oSource.xKey(1))
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFSINAI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(this.w_MFSINAI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',this.w_MFSINAI1)
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFSINAI1 = NVL(_Link_.SICODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFSINAI1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])+'\'+cp_ToStr(_Link_.SICODICE,1)
      cp_ShowWarn(i_cKey,this.SE_INAIL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFSINAI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFSINAI2
  func Link_5_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_lTable = "SE_INAIL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2], .t., this.SE_INAIL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFSINAI2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ASI',True,'SE_INAIL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SICODICE like "+cp_ToStrODBC(trim(this.w_MFSINAI2)+"%");

          i_ret=cp_SQL(i_nConn,"select SICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SICODICE',trim(this.w_MFSINAI2))
          select SICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFSINAI2)==trim(_Link_.SICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFSINAI2) and !this.bDontReportError
            deferred_cp_zoom('SE_INAIL','*','SICODICE',cp_AbsName(oSource.parent,'oMFSINAI2_5_9'),i_cWhere,'GSCG_ASI',"Codici sede INAIL",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',oSource.xKey(1))
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFSINAI2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(this.w_MFSINAI2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',this.w_MFSINAI2)
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFSINAI2 = NVL(_Link_.SICODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFSINAI2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])+'\'+cp_ToStr(_Link_.SICODICE,1)
      cp_ShowWarn(i_cKey,this.SE_INAIL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFSINAI2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFSINAI3
  func Link_5_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SE_INAIL_IDX,3]
    i_lTable = "SE_INAIL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2], .t., this.SE_INAIL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFSINAI3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ASI',True,'SE_INAIL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SICODICE like "+cp_ToStrODBC(trim(this.w_MFSINAI3)+"%");

          i_ret=cp_SQL(i_nConn,"select SICODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SICODICE',trim(this.w_MFSINAI3))
          select SICODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFSINAI3)==trim(_Link_.SICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFSINAI3) and !this.bDontReportError
            deferred_cp_zoom('SE_INAIL','*','SICODICE',cp_AbsName(oSource.parent,'oMFSINAI3_5_16'),i_cWhere,'GSCG_ASI',"Codici sede INAIL",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',oSource.xKey(1))
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFSINAI3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SICODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SICODICE="+cp_ToStrODBC(this.w_MFSINAI3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SICODICE',this.w_MFSINAI3)
            select SICODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFSINAI3 = NVL(_Link_.SICODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFSINAI3 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SE_INAIL_IDX,2])+'\'+cp_ToStr(_Link_.SICODICE,1)
      cp_ShowWarn(i_cKey,this.SE_INAIL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFSINAI3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCDENTE
  func Link_6_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_PREV_IDX,3]
    i_lTable = "COD_PREV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_PREV_IDX,2], .t., this.COD_PREV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_PREV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCDENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACP',True,'COD_PREV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CPCODICE like "+cp_ToStrODBC(trim(this.w_MFCDENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select CPCODICE,CPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CPCODICE',trim(this.w_MFCDENTE))
          select CPCODICE,CPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCDENTE)==trim(_Link_.CPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCDENTE) and !this.bDontReportError
            deferred_cp_zoom('COD_PREV','*','CPCODICE',cp_AbsName(oSource.parent,'oMFCDENTE_6_3'),i_cWhere,'GSCG_ACP',"Codici enti previdenziali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CPCODICE,CPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CPCODICE',oSource.xKey(1))
            select CPCODICE,CPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCDENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CPCODICE,CPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CPCODICE="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CPCODICE',this.w_MFCDENTE)
            select CPCODICE,CPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCDENTE = NVL(_Link_.CPCODICE,space(5))
      this.w_DESAENTE = NVL(_Link_.CPDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_MFCDENTE = space(5)
      endif
      this.w_DESAENTE = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_PREV_IDX,2])+'\'+cp_ToStr(_Link_.CPCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_PREV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCDENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_6_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_PREV_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_PREV_IDX,2])
      i_cNewSel = i_cSel+ ",link_6_3.CPCODICE as CPCODICE603"+ ",link_6_3.CPDESCRI as CPDESCRI603"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_6_3 on MOD_PAG.MFCDENTE=link_6_3.CPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_6_3"
          i_cKey=i_cKey+'+" and MOD_PAG.MFCDENTE=link_6_3.CPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=VRSDENT1
  func Link_6_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_AEN_IDX,3]
    i_lTable = "SED_AEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2], .t., this.SED_AEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VRSDENT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MSE',True,'SED_AEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SECODSED like "+cp_ToStrODBC(trim(this.w_VRSDENT1)+"%");
                   +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);

          i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SECODENT,SECODSED","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SECODENT',this.w_MFCDENTE;
                     ,'SECODSED',trim(this.w_VRSDENT1))
          select SECODENT,SECODSED;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SECODENT,SECODSED into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VRSDENT1)==trim(_Link_.SECODSED) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VRSDENT1) and !this.bDontReportError
            deferred_cp_zoom('SED_AEN','*','SECODENT,SECODSED',cp_AbsName(oSource.parent,'oVRSDENT1_6_4'),i_cWhere,'GSCG_MSE',"Codici sede",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MFCDENTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                     +" from "+i_cTable+" "+i_lTable+" where SECODSED="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODENT',oSource.xKey(1);
                       ,'SECODSED',oSource.xKey(2))
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VRSDENT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                   +" from "+i_cTable+" "+i_lTable+" where SECODSED="+cp_ToStrODBC(this.w_VRSDENT1);
                   +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODENT',this.w_MFCDENTE;
                       ,'SECODSED',this.w_VRSDENT1)
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VRSDENT1 = NVL(_Link_.SECODSED,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VRSDENT1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2])+'\'+cp_ToStr(_Link_.SECODENT,1)+'\'+cp_ToStr(_Link_.SECODSED,1)
      cp_ShowWarn(i_cKey,this.SED_AEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VRSDENT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPSDENT1
  func Link_6_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANAG_PRO_IDX,3]
    i_lTable = "ANAG_PRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2], .t., this.ANAG_PRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPSDENT1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APR',True,'ANAG_PRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PRCODPRO like "+cp_ToStrODBC(trim(this.w_VPSDENT1)+"%");

          i_ret=cp_SQL(i_nConn,"select PRCODPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PRCODPRO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PRCODPRO',trim(this.w_VPSDENT1))
          select PRCODPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PRCODPRO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VPSDENT1)==trim(_Link_.PRCODPRO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VPSDENT1) and !this.bDontReportError
            deferred_cp_zoom('ANAG_PRO','*','PRCODPRO',cp_AbsName(oSource.parent,'oVPSDENT1_6_5'),i_cWhere,'GSAR_APR',"Elenco province",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODPRO";
                     +" from "+i_cTable+" "+i_lTable+" where PRCODPRO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODPRO',oSource.xKey(1))
            select PRCODPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPSDENT1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODPRO";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODPRO="+cp_ToStrODBC(this.w_VPSDENT1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODPRO',this.w_VPSDENT1)
            select PRCODPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPSDENT1 = NVL(_Link_.PRCODPRO,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VPSDENT1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])+'\'+cp_ToStr(_Link_.PRCODPRO,1)
      cp_ShowWarn(i_cKey,this.ANAG_PRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPSDENT1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCOAE1
  func Link_6_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_AEN_IDX,3]
    i_lTable = "CAU_AEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2], .t., this.CAU_AEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCOAE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MAE',True,'CAU_AEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AECAUSEN like "+cp_ToStrODBC(trim(this.w_MFCCOAE1)+"%");
                   +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);

          i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AECODENT,AECAUSEN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AECODENT',this.w_MFCDENTE;
                     ,'AECAUSEN',trim(this.w_MFCCOAE1))
          select AECODENT,AECAUSEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AECODENT,AECAUSEN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCOAE1)==trim(_Link_.AECAUSEN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCOAE1) and !this.bDontReportError
            deferred_cp_zoom('CAU_AEN','*','AECODENT,AECAUSEN',cp_AbsName(oSource.parent,'oMFCCOAE1_6_7'),i_cWhere,'GSCG_MAE',"Causali contributo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MFCDENTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                     +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(oSource.xKey(2));
                     +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECODENT',oSource.xKey(1);
                       ,'AECAUSEN',oSource.xKey(2))
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCOAE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                   +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(this.w_MFCCOAE1);
                   +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECODENT',this.w_MFCDENTE;
                       ,'AECAUSEN',this.w_MFCCOAE1)
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCOAE1 = NVL(_Link_.AECAUSEN,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCOAE1 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])+'\'+cp_ToStr(_Link_.AECODENT,1)+'\'+cp_ToStr(_Link_.AECAUSEN,1)
      cp_ShowWarn(i_cKey,this.CAU_AEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCOAE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VPSDENT2
  func Link_6_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ANAG_PRO_IDX,3]
    i_lTable = "ANAG_PRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2], .t., this.ANAG_PRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VPSDENT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APR',True,'ANAG_PRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PRCODPRO like "+cp_ToStrODBC(trim(this.w_VPSDENT2)+"%");

          i_ret=cp_SQL(i_nConn,"select PRCODPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PRCODPRO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PRCODPRO',trim(this.w_VPSDENT2))
          select PRCODPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PRCODPRO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VPSDENT2)==trim(_Link_.PRCODPRO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VPSDENT2) and !this.bDontReportError
            deferred_cp_zoom('ANAG_PRO','*','PRCODPRO',cp_AbsName(oSource.parent,'oVPSDENT2_6_16'),i_cWhere,'GSAR_APR',"Elenco province",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODPRO";
                     +" from "+i_cTable+" "+i_lTable+" where PRCODPRO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODPRO',oSource.xKey(1))
            select PRCODPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VPSDENT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PRCODPRO";
                   +" from "+i_cTable+" "+i_lTable+" where PRCODPRO="+cp_ToStrODBC(this.w_VPSDENT2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PRCODPRO',this.w_VPSDENT2)
            select PRCODPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VPSDENT2 = NVL(_Link_.PRCODPRO,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VPSDENT2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ANAG_PRO_IDX,2])+'\'+cp_ToStr(_Link_.PRCODPRO,1)
      cp_ShowWarn(i_cKey,this.ANAG_PRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VPSDENT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VRSDENT2
  func Link_6_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_AEN_IDX,3]
    i_lTable = "SED_AEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2], .t., this.SED_AEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VRSDENT2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MSE',True,'SED_AEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SECODSED like "+cp_ToStrODBC(trim(this.w_VRSDENT2)+"%");
                   +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);

          i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SECODENT,SECODSED","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SECODENT',this.w_MFCDENTE;
                     ,'SECODSED',trim(this.w_VRSDENT2))
          select SECODENT,SECODSED;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SECODENT,SECODSED into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VRSDENT2)==trim(_Link_.SECODSED) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_VRSDENT2) and !this.bDontReportError
            deferred_cp_zoom('SED_AEN','*','SECODENT,SECODSED',cp_AbsName(oSource.parent,'oVRSDENT2_6_17'),i_cWhere,'GSCG_MSE',"Codici sede",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MFCDENTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                     +" from "+i_cTable+" "+i_lTable+" where SECODSED="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODENT',oSource.xKey(1);
                       ,'SECODSED',oSource.xKey(2))
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VRSDENT2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODENT,SECODSED";
                   +" from "+i_cTable+" "+i_lTable+" where SECODSED="+cp_ToStrODBC(this.w_VRSDENT2);
                   +" and SECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODENT',this.w_MFCDENTE;
                       ,'SECODSED',this.w_VRSDENT2)
            select SECODENT,SECODSED;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VRSDENT2 = NVL(_Link_.SECODSED,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VRSDENT2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_AEN_IDX,2])+'\'+cp_ToStr(_Link_.SECODENT,1)+'\'+cp_ToStr(_Link_.SECODSED,1)
      cp_ShowWarn(i_cKey,this.SED_AEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VRSDENT2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFCCOAE2
  func Link_6_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_AEN_IDX,3]
    i_lTable = "CAU_AEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2], .t., this.CAU_AEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFCCOAE2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_MAE',True,'CAU_AEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AECAUSEN like "+cp_ToStrODBC(trim(this.w_MFCCOAE2)+"%");
                   +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);

          i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AECODENT,AECAUSEN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AECODENT',this.w_MFCDENTE;
                     ,'AECAUSEN',trim(this.w_MFCCOAE2))
          select AECODENT,AECAUSEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AECODENT,AECAUSEN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFCCOAE2)==trim(_Link_.AECAUSEN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFCCOAE2) and !this.bDontReportError
            deferred_cp_zoom('CAU_AEN','*','AECODENT,AECAUSEN',cp_AbsName(oSource.parent,'oMFCCOAE2_6_19'),i_cWhere,'GSCG_MAE',"Causali contributo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MFCDENTE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                     +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(oSource.xKey(2));
                     +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECODENT',oSource.xKey(1);
                       ,'AECAUSEN',oSource.xKey(2))
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFCCOAE2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AECODENT,AECAUSEN";
                   +" from "+i_cTable+" "+i_lTable+" where AECAUSEN="+cp_ToStrODBC(this.w_MFCCOAE2);
                   +" and AECODENT="+cp_ToStrODBC(this.w_MFCDENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AECODENT',this.w_MFCDENTE;
                       ,'AECAUSEN',this.w_MFCCOAE2)
            select AECODENT,AECAUSEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFCCOAE2 = NVL(_Link_.AECAUSEN,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MFCCOAE2 = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_AEN_IDX,2])+'\'+cp_ToStr(_Link_.AECODENT,1)+'\'+cp_ToStr(_Link_.AECAUSEN,1)
      cp_ShowWarn(i_cKey,this.CAU_AEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFCCOAE2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFDESSTA
  func Link_7_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFDESSTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASE',True,'SEDIAZIE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SECODDES like "+cp_ToStrODBC(trim(this.w_MFDESSTA)+"%");
                   +" and SECODAZI="+cp_ToStrODBC(this.w_CODAZIE);

          i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SECODAZI,SECODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SECODAZI',this.w_CODAZIE;
                     ,'SECODDES',trim(this.w_MFDESSTA))
          select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SECODAZI,SECODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFDESSTA)==trim(_Link_.SECODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFDESSTA) and !this.bDontReportError
            deferred_cp_zoom('SEDIAZIE','*','SECODAZI,SECODDES',cp_AbsName(oSource.parent,'oMFDESSTA_7_10'),i_cWhere,'GSAR_ASE',"Sedi aziende",'GSAR_ASI.SEDIAZIE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZIE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA";
                     +" from "+i_cTable+" "+i_lTable+" where SECODDES="+cp_ToStrODBC(oSource.xKey(2));
                     +" and SECODAZI="+cp_ToStrODBC(this.w_CODAZIE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODAZI',oSource.xKey(1);
                       ,'SECODDES',oSource.xKey(2))
            select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFDESSTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA";
                   +" from "+i_cTable+" "+i_lTable+" where SECODDES="+cp_ToStrODBC(this.w_MFDESSTA);
                   +" and SECODAZI="+cp_ToStrODBC(this.w_CODAZIE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SECODAZI',this.w_CODAZIE;
                       ,'SECODDES',this.w_MFDESSTA)
            select SECODAZI,SECODDES,SE___CAP,SELOCALI,SEPROVIN,SEINDIRI,SEDESSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFDESSTA = NVL(_Link_.SECODDES,space(5))
      this.w_CAP1 = NVL(_Link_.SE___CAP,space(9))
      this.w_LOCALI1 = NVL(_Link_.SELOCALI,space(30))
      this.w_PROVIN1 = NVL(_Link_.SEPROVIN,space(2))
      this.w_INDIRI1 = NVL(_Link_.SEINDIRI,space(35))
      this.w_DESSTA = NVL(_Link_.SEDESSTA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MFDESSTA = space(5)
      endif
      this.w_CAP1 = space(9)
      this.w_LOCALI1 = space(30)
      this.w_PROVIN1 = space(2)
      this.w_INDIRI1 = space(35)
      this.w_DESSTA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SECODAZI,1)+'\'+cp_ToStr(_Link_.SECODDES,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFDESSTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RAPFIRM
  func Link_8_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DAT_RAPP_IDX,3]
    i_lTable = "DAT_RAPP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2], .t., this.DAT_RAPP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RAPFIRM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RAPFIRM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RFCODAZI,RFCODFIS,RFCODCAR,RFCOGNOM,RF__NOME,RF_SESSO,RFDATNAS,RFCOMNAS,RFPRONAS,RFCOMRES,RFPRORES,RFCAPRES,RFINDRES";
                   +" from "+i_cTable+" "+i_lTable+" where RFCODAZI="+cp_ToStrODBC(this.w_RAPFIRM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RFCODAZI',this.w_RAPFIRM)
            select RFCODAZI,RFCODFIS,RFCODCAR,RFCOGNOM,RF__NOME,RF_SESSO,RFDATNAS,RFCOMNAS,RFPRONAS,RFCOMRES,RFPRORES,RFCAPRES,RFINDRES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RAPFIRM = NVL(_Link_.RFCODAZI,space(5))
      this.w_MFCODFIR2 = NVL(_Link_.RFCODFIS,space(16))
      this.w_MFCARFIR2 = NVL(_Link_.RFCODCAR,space(2))
      this.w_MFCOGFIR2 = NVL(_Link_.RFCOGNOM,space(24))
      this.w_MFNOMFIR2 = NVL(_Link_.RF__NOME,space(20))
      this.w_MFSESFIR2 = NVL(_Link_.RF_SESSO,space(1))
      this.w_MFDATFIR2 = NVL(cp_ToDate(_Link_.RFDATNAS),ctod("  /  /  "))
      this.w_MFCOMFIR2 = NVL(_Link_.RFCOMNAS,space(40))
      this.w_MFPROFIR2 = NVL(_Link_.RFPRONAS,space(2))
      this.w_MFDOMFIR2 = NVL(_Link_.RFCOMRES,space(40))
      this.w_MFPRDFIR2 = NVL(_Link_.RFPRORES,space(2))
      this.w_MFCAPFIR2 = NVL(_Link_.RFCAPRES,space(5))
      this.w_MFINDFIR2 = NVL(_Link_.RFINDRES,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_RAPFIRM = space(5)
      endif
      this.w_MFCODFIR2 = space(16)
      this.w_MFCARFIR2 = space(2)
      this.w_MFCOGFIR2 = space(24)
      this.w_MFNOMFIR2 = space(20)
      this.w_MFSESFIR2 = space(1)
      this.w_MFDATFIR2 = ctod("  /  /  ")
      this.w_MFCOMFIR2 = space(40)
      this.w_MFPROFIR2 = space(2)
      this.w_MFDOMFIR2 = space(40)
      this.w_MFPRDFIR2 = space(2)
      this.w_MFCAPFIR2 = space(5)
      this.w_MFINDFIR2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DAT_RAPP_IDX,2])+'\'+cp_ToStr(_Link_.RFCODAZI,1)
      cp_ShowWarn(i_cKey,this.DAT_RAPP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RAPFIRM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READAZI
  func Link_8_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCONBAN";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_READAZI)
            select AZCODAZI,AZCONBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.AZCODAZI,space(10))
      this.w_AZCONBAN = NVL(_Link_.AZCONBAN,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(10)
      endif
      this.w_AZCONBAN = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MFBANPAS
  func Link_8_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MFBANPAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_MFBANPAS)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BATIPCON,BADESCRI,BADTOBSO,BACONSBF,BA__IBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_MFBANPAS))
          select BACODBAN,BATIPCON,BADESCRI,BADTOBSO,BACONSBF,BA__IBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MFBANPAS)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MFBANPAS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oMFBANPAS_8_45'),i_cWhere,'GSTE_ACB',"Conti banche",'GSCG_API.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BATIPCON,BADESCRI,BADTOBSO,BACONSBF,BA__IBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BATIPCON,BADESCRI,BADTOBSO,BACONSBF,BA__IBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MFBANPAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BATIPCON,BADESCRI,BADTOBSO,BACONSBF,BA__IBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_MFBANPAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_MFBANPAS)
            select BACODBAN,BATIPCON,BADESCRI,BADTOBSO,BACONSBF,BA__IBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MFBANPAS = NVL(_Link_.BACODBAN,space(15))
      this.w_BATIPCON = NVL(_Link_.BATIPCON,space(1))
      this.w_BADESCRI = NVL(_Link_.BADESCRI,space(35))
      this.w_DATOBSO1 = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_BACONSBF = NVL(_Link_.BACONSBF,space(1))
      this.w_IBAN = NVL(_Link_.BA__IBAN,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_MFBANPAS = space(15)
      endif
      this.w_BATIPCON = space(1)
      this.w_BADESCRI = space(35)
      this.w_DATOBSO1 = ctod("  /  /  ")
      this.w_BACONSBF = space(1)
      this.w_IBAN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_BATIPCON<>'S' AND (EMPTY(.w_DATOBSO1) OR .w_DATOBSO1>.w_OBTEST) AND .w_BACONSBF='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice banca inesistente, obsoleta, di compensazione o salvo buon fine")
        endif
        this.w_MFBANPAS = space(15)
        this.w_BATIPCON = space(1)
        this.w_BADESCRI = space(35)
        this.w_DATOBSO1 = ctod("  /  /  ")
        this.w_BACONSBF = space(1)
        this.w_IBAN = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MFBANPAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMFSERIAL_1_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page1.oPag.oMFSERIAL_1_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMFMESRIF_1_2.value==this.w_MFMESRIF)
      this.oPgFrm.Page1.oPag.oMFMESRIF_1_2.value=this.w_MFMESRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oMFANNRIF_1_3.value==this.w_MFANNRIF)
      this.oPgFrm.Page1.oPag.oMFANNRIF_1_3.value=this.w_MFANNRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oMF_COINC_1_7.RadioValue()==this.w_MF_COINC)
      this.oPgFrm.Page1.oPag.oMF_COINC_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMFSERIAL_2_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page2.oPag.oMFSERIAL_2_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page2.oPag.oMFCODUFF_2_4.value==this.w_MFCODUFF)
      this.oPgFrm.Page2.oPag.oMFCODUFF_2_4.value=this.w_MFCODUFF
    endif
    if not(this.oPgFrm.Page2.oPag.oMFCODATT_2_5.value==this.w_MFCODATT)
      this.oPgFrm.Page2.oPag.oMFCODATT_2_5.value=this.w_MFCODATT
    endif
    if not(this.oPgFrm.Page2.oPag.oMESE_2_8.value==this.w_MESE)
      this.oPgFrm.Page2.oPag.oMESE_2_8.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page2.oPag.oANNO_2_9.value==this.w_ANNO)
      this.oPgFrm.Page2.oPag.oANNO_2_9.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page3.oPag.oMFSERIAL_3_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page3.oPag.oMFSERIAL_3_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED1_3_2.value==this.w_MFCDSED1)
      this.oPgFrm.Page3.oPag.oMFCDSED1_3_2.value=this.w_MFCDSED1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.value==this.w_MFCCONT1)
      this.oPgFrm.Page3.oPag.oMFCCONT1_3_3.value=this.w_MFCCONT1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.value==this.w_MFMINPS1)
      this.oPgFrm.Page3.oPag.oMFMINPS1_3_4.value=this.w_MFMINPS1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES1_3_7.value==this.w_MFDAMES1)
      this.oPgFrm.Page3.oPag.oMFDAMES1_3_7.value=this.w_MFDAMES1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN1_3_8.value==this.w_MFDAANN1)
      this.oPgFrm.Page3.oPag.oMFDAANN1_3_8.value=this.w_MFDAANN1
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES1_3_9.value==this.w_MF_AMES1)
      this.oPgFrm.Page3.oPag.oMF_AMES1_3_9.value=this.w_MF_AMES1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN1_3_10.value==this.w_MFAN1)
      this.oPgFrm.Page3.oPag.oMFAN1_3_10.value=this.w_MFAN1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD1_3_11.value==this.w_MFIMPSD1)
      this.oPgFrm.Page3.oPag.oMFIMPSD1_3_11.value=this.w_MFIMPSD1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC1_3_12.value==this.w_MFIMPSC1)
      this.oPgFrm.Page3.oPag.oMFIMPSC1_3_12.value=this.w_MFIMPSC1
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED2_3_13.value==this.w_MFCDSED2)
      this.oPgFrm.Page3.oPag.oMFCDSED2_3_13.value=this.w_MFCDSED2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT2_3_14.value==this.w_MFCCONT2)
      this.oPgFrm.Page3.oPag.oMFCCONT2_3_14.value=this.w_MFCCONT2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS2_3_15.value==this.w_MFMINPS2)
      this.oPgFrm.Page3.oPag.oMFMINPS2_3_15.value=this.w_MFMINPS2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES2_3_17.value==this.w_MFDAMES2)
      this.oPgFrm.Page3.oPag.oMFDAMES2_3_17.value=this.w_MFDAMES2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN2_3_18.value==this.w_MFDAANN2)
      this.oPgFrm.Page3.oPag.oMFDAANN2_3_18.value=this.w_MFDAANN2
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES2_3_20.value==this.w_MF_AMES2)
      this.oPgFrm.Page3.oPag.oMF_AMES2_3_20.value=this.w_MF_AMES2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN2_3_21.value==this.w_MFAN2)
      this.oPgFrm.Page3.oPag.oMFAN2_3_21.value=this.w_MFAN2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD2_3_22.value==this.w_MFIMPSD2)
      this.oPgFrm.Page3.oPag.oMFIMPSD2_3_22.value=this.w_MFIMPSD2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC2_3_23.value==this.w_MFIMPSC2)
      this.oPgFrm.Page3.oPag.oMFIMPSC2_3_23.value=this.w_MFIMPSC2
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED3_3_24.value==this.w_MFCDSED3)
      this.oPgFrm.Page3.oPag.oMFCDSED3_3_24.value=this.w_MFCDSED3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT3_3_25.value==this.w_MFCCONT3)
      this.oPgFrm.Page3.oPag.oMFCCONT3_3_25.value=this.w_MFCCONT3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS3_3_26.value==this.w_MFMINPS3)
      this.oPgFrm.Page3.oPag.oMFMINPS3_3_26.value=this.w_MFMINPS3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES3_3_28.value==this.w_MFDAMES3)
      this.oPgFrm.Page3.oPag.oMFDAMES3_3_28.value=this.w_MFDAMES3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN3_3_30.value==this.w_MFDAANN3)
      this.oPgFrm.Page3.oPag.oMFDAANN3_3_30.value=this.w_MFDAANN3
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES3_3_31.value==this.w_MF_AMES3)
      this.oPgFrm.Page3.oPag.oMF_AMES3_3_31.value=this.w_MF_AMES3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN3_3_32.value==this.w_MFAN3)
      this.oPgFrm.Page3.oPag.oMFAN3_3_32.value=this.w_MFAN3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD3_3_33.value==this.w_MFIMPSD3)
      this.oPgFrm.Page3.oPag.oMFIMPSD3_3_33.value=this.w_MFIMPSD3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC3_3_34.value==this.w_MFIMPSC3)
      this.oPgFrm.Page3.oPag.oMFIMPSC3_3_34.value=this.w_MFIMPSC3
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCDSED4_3_35.value==this.w_MFCDSED4)
      this.oPgFrm.Page3.oPag.oMFCDSED4_3_35.value=this.w_MFCDSED4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFCCONT4_3_36.value==this.w_MFCCONT4)
      this.oPgFrm.Page3.oPag.oMFCCONT4_3_36.value=this.w_MFCCONT4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFMINPS4_3_37.value==this.w_MFMINPS4)
      this.oPgFrm.Page3.oPag.oMFMINPS4_3_37.value=this.w_MFMINPS4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAMES4_3_39.value==this.w_MFDAMES4)
      this.oPgFrm.Page3.oPag.oMFDAMES4_3_39.value=this.w_MFDAMES4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFDAANN4_3_41.value==this.w_MFDAANN4)
      this.oPgFrm.Page3.oPag.oMFDAANN4_3_41.value=this.w_MFDAANN4
    endif
    if not(this.oPgFrm.Page3.oPag.oMF_AMES4_3_42.value==this.w_MF_AMES4)
      this.oPgFrm.Page3.oPag.oMF_AMES4_3_42.value=this.w_MF_AMES4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFAN4_3_43.value==this.w_MFAN4)
      this.oPgFrm.Page3.oPag.oMFAN4_3_43.value=this.w_MFAN4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSD4_3_44.value==this.w_MFIMPSD4)
      this.oPgFrm.Page3.oPag.oMFIMPSD4_3_44.value=this.w_MFIMPSD4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFIMPSC4_3_45.value==this.w_MFIMPSC4)
      this.oPgFrm.Page3.oPag.oMFIMPSC4_3_45.value=this.w_MFIMPSC4
    endif
    if not(this.oPgFrm.Page3.oPag.oMFTOTDPS_3_55.value==this.w_MFTOTDPS)
      this.oPgFrm.Page3.oPag.oMFTOTDPS_3_55.value=this.w_MFTOTDPS
    endif
    if not(this.oPgFrm.Page3.oPag.oMFTOTCPS_3_56.value==this.w_MFTOTCPS)
      this.oPgFrm.Page3.oPag.oMFTOTCPS_3_56.value=this.w_MFTOTCPS
    endif
    if not(this.oPgFrm.Page3.oPag.oMFSALDPS_3_57.value==this.w_MFSALDPS)
      this.oPgFrm.Page3.oPag.oMFSALDPS_3_57.value=this.w_MFSALDPS
    endif
    if not(this.oPgFrm.Page3.oPag.oMESE_3_63.value==this.w_MESE)
      this.oPgFrm.Page3.oPag.oMESE_3_63.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page3.oPag.oANNO_3_64.value==this.w_ANNO)
      this.oPgFrm.Page3.oPag.oANNO_3_64.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page4.oPag.oMFSERIAL_4_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page4.oPag.oMFSERIAL_4_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE1_4_2.value==this.w_MFCODRE1)
      this.oPgFrm.Page4.oPag.oMFCODRE1_4_2.value=this.w_MFCODRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE1_4_3.value==this.w_MFTRIRE1)
      this.oPgFrm.Page4.oPag.oMFTRIRE1_4_3.value=this.w_MFTRIRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE1_4_4.value==this.w_MFRATRE1)
      this.oPgFrm.Page4.oPag.oMFRATRE1_4_4.value=this.w_MFRATRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFMESER1_4_5.value==this.w_MFMESER1)
      this.oPgFrm.Page4.oPag.oMFMESER1_4_5.value=this.w_MFMESER1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE1_4_6.value==this.w_MFANNRE1)
      this.oPgFrm.Page4.oPag.oMFANNRE1_4_6.value=this.w_MFANNRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE1_4_7.value==this.w_MFIMDRE1)
      this.oPgFrm.Page4.oPag.oMFIMDRE1_4_7.value=this.w_MFIMDRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE1_4_8.value==this.w_MFIMCRE1)
      this.oPgFrm.Page4.oPag.oMFIMCRE1_4_8.value=this.w_MFIMCRE1
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE2_4_12.value==this.w_MFCODRE2)
      this.oPgFrm.Page4.oPag.oMFCODRE2_4_12.value=this.w_MFCODRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE2_4_13.value==this.w_MFTRIRE2)
      this.oPgFrm.Page4.oPag.oMFTRIRE2_4_13.value=this.w_MFTRIRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE2_4_14.value==this.w_MFRATRE2)
      this.oPgFrm.Page4.oPag.oMFRATRE2_4_14.value=this.w_MFRATRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFMESER2_4_15.value==this.w_MFMESER2)
      this.oPgFrm.Page4.oPag.oMFMESER2_4_15.value=this.w_MFMESER2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE2_4_16.value==this.w_MFANNRE2)
      this.oPgFrm.Page4.oPag.oMFANNRE2_4_16.value=this.w_MFANNRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE2_4_17.value==this.w_MFIMDRE2)
      this.oPgFrm.Page4.oPag.oMFIMDRE2_4_17.value=this.w_MFIMDRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE2_4_18.value==this.w_MFIMCRE2)
      this.oPgFrm.Page4.oPag.oMFIMCRE2_4_18.value=this.w_MFIMCRE2
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE3_4_19.value==this.w_MFCODRE3)
      this.oPgFrm.Page4.oPag.oMFCODRE3_4_19.value=this.w_MFCODRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE3_4_20.value==this.w_MFTRIRE3)
      this.oPgFrm.Page4.oPag.oMFTRIRE3_4_20.value=this.w_MFTRIRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE3_4_21.value==this.w_MFRATRE3)
      this.oPgFrm.Page4.oPag.oMFRATRE3_4_21.value=this.w_MFRATRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFMESER3_4_22.value==this.w_MFMESER3)
      this.oPgFrm.Page4.oPag.oMFMESER3_4_22.value=this.w_MFMESER3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE3_4_23.value==this.w_MFANNRE3)
      this.oPgFrm.Page4.oPag.oMFANNRE3_4_23.value=this.w_MFANNRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE3_4_24.value==this.w_MFIMDRE3)
      this.oPgFrm.Page4.oPag.oMFIMDRE3_4_24.value=this.w_MFIMDRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE3_4_25.value==this.w_MFIMCRE3)
      this.oPgFrm.Page4.oPag.oMFIMCRE3_4_25.value=this.w_MFIMCRE3
    endif
    if not(this.oPgFrm.Page4.oPag.oMFCODRE4_4_26.value==this.w_MFCODRE4)
      this.oPgFrm.Page4.oPag.oMFCODRE4_4_26.value=this.w_MFCODRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTRIRE4_4_27.value==this.w_MFTRIRE4)
      this.oPgFrm.Page4.oPag.oMFTRIRE4_4_27.value=this.w_MFTRIRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFRATRE4_4_28.value==this.w_MFRATRE4)
      this.oPgFrm.Page4.oPag.oMFRATRE4_4_28.value=this.w_MFRATRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFMESER4_4_29.value==this.w_MFMESER4)
      this.oPgFrm.Page4.oPag.oMFMESER4_4_29.value=this.w_MFMESER4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFANNRE4_4_30.value==this.w_MFANNRE4)
      this.oPgFrm.Page4.oPag.oMFANNRE4_4_30.value=this.w_MFANNRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMDRE4_4_31.value==this.w_MFIMDRE4)
      this.oPgFrm.Page4.oPag.oMFIMDRE4_4_31.value=this.w_MFIMDRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFIMCRE4_4_32.value==this.w_MFIMCRE4)
      this.oPgFrm.Page4.oPag.oMFIMCRE4_4_32.value=this.w_MFIMCRE4
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTOTDRE_4_36.value==this.w_MFTOTDRE)
      this.oPgFrm.Page4.oPag.oMFTOTDRE_4_36.value=this.w_MFTOTDRE
    endif
    if not(this.oPgFrm.Page4.oPag.oMFTOTCRE_4_37.value==this.w_MFTOTCRE)
      this.oPgFrm.Page4.oPag.oMFTOTCRE_4_37.value=this.w_MFTOTCRE
    endif
    if not(this.oPgFrm.Page4.oPag.oMFSALDRE_4_38.value==this.w_MFSALDRE)
      this.oPgFrm.Page4.oPag.oMFSALDRE_4_38.value=this.w_MFSALDRE
    endif
    if not(this.oPgFrm.Page4.oPag.oMESE_4_43.value==this.w_MESE)
      this.oPgFrm.Page4.oPag.oMESE_4_43.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page4.oPag.oANNO_4_44.value==this.w_ANNO)
      this.oPgFrm.Page4.oPag.oANNO_4_44.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSERIAL_5_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page5.oPag.oMFSERIAL_5_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSINAI1_5_2.value==this.w_MFSINAI1)
      this.oPgFrm.Page5.oPag.oMFSINAI1_5_2.value=this.w_MFSINAI1
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.value==this.w_MF_NPOS1)
      this.oPgFrm.Page5.oPag.oMF_NPOS1_5_3.value=this.w_MF_NPOS1
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.value==this.w_MF_PACC1)
      this.oPgFrm.Page5.oPag.oMF_PACC1_5_4.value=this.w_MF_PACC1
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.value==this.w_MF_NRIF1)
      this.oPgFrm.Page5.oPag.oMF_NRIF1_5_5.value=this.w_MF_NRIF1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.value==this.w_MFCAUSA1)
      this.oPgFrm.Page5.oPag.oMFCAUSA1_5_6.value=this.w_MFCAUSA1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.value==this.w_MFIMDIL1)
      this.oPgFrm.Page5.oPag.oMFIMDIL1_5_7.value=this.w_MFIMDIL1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.value==this.w_MFIMCIL1)
      this.oPgFrm.Page5.oPag.oMFIMCIL1_5_8.value=this.w_MFIMCIL1
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSINAI2_5_9.value==this.w_MFSINAI2)
      this.oPgFrm.Page5.oPag.oMFSINAI2_5_9.value=this.w_MFSINAI2
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.value==this.w_MF_NPOS2)
      this.oPgFrm.Page5.oPag.oMF_NPOS2_5_10.value=this.w_MF_NPOS2
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.value==this.w_MF_PACC2)
      this.oPgFrm.Page5.oPag.oMF_PACC2_5_11.value=this.w_MF_PACC2
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.value==this.w_MF_NRIF2)
      this.oPgFrm.Page5.oPag.oMF_NRIF2_5_12.value=this.w_MF_NRIF2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.value==this.w_MFCAUSA2)
      this.oPgFrm.Page5.oPag.oMFCAUSA2_5_13.value=this.w_MFCAUSA2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.value==this.w_MFIMDIL2)
      this.oPgFrm.Page5.oPag.oMFIMDIL2_5_14.value=this.w_MFIMDIL2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.value==this.w_MFIMCIL2)
      this.oPgFrm.Page5.oPag.oMFIMCIL2_5_15.value=this.w_MFIMCIL2
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSINAI3_5_16.value==this.w_MFSINAI3)
      this.oPgFrm.Page5.oPag.oMFSINAI3_5_16.value=this.w_MFSINAI3
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.value==this.w_MF_NPOS3)
      this.oPgFrm.Page5.oPag.oMF_NPOS3_5_17.value=this.w_MF_NPOS3
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.value==this.w_MF_PACC3)
      this.oPgFrm.Page5.oPag.oMF_PACC3_5_18.value=this.w_MF_PACC3
    endif
    if not(this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.value==this.w_MF_NRIF3)
      this.oPgFrm.Page5.oPag.oMF_NRIF3_5_19.value=this.w_MF_NRIF3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.value==this.w_MFCAUSA3)
      this.oPgFrm.Page5.oPag.oMFCAUSA3_5_20.value=this.w_MFCAUSA3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.value==this.w_MFIMDIL3)
      this.oPgFrm.Page5.oPag.oMFIMDIL3_5_21.value=this.w_MFIMDIL3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.value==this.w_MFIMCIL3)
      this.oPgFrm.Page5.oPag.oMFIMCIL3_5_22.value=this.w_MFIMCIL3
    endif
    if not(this.oPgFrm.Page5.oPag.oMFTDINAI_5_30.value==this.w_MFTDINAI)
      this.oPgFrm.Page5.oPag.oMFTDINAI_5_30.value=this.w_MFTDINAI
    endif
    if not(this.oPgFrm.Page5.oPag.oMFTCINAI_5_31.value==this.w_MFTCINAI)
      this.oPgFrm.Page5.oPag.oMFTCINAI_5_31.value=this.w_MFTCINAI
    endif
    if not(this.oPgFrm.Page5.oPag.oMFSALINA_5_32.value==this.w_MFSALINA)
      this.oPgFrm.Page5.oPag.oMFSALINA_5_32.value=this.w_MFSALINA
    endif
    if not(this.oPgFrm.Page5.oPag.oMESE_5_40.value==this.w_MESE)
      this.oPgFrm.Page5.oPag.oMESE_5_40.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page5.oPag.oANNO_5_41.value==this.w_ANNO)
      this.oPgFrm.Page5.oPag.oANNO_5_41.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page6.oPag.oMFSERIAL_6_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page6.oPag.oMFSERIAL_6_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCDENTE_6_3.value==this.w_MFCDENTE)
      this.oPgFrm.Page6.oPag.oMFCDENTE_6_3.value=this.w_MFCDENTE
    endif
    if not(this.oPgFrm.Page6.oPag.oVRSDENT1_6_4.value==this.w_VRSDENT1)
      this.oPgFrm.Page6.oPag.oVRSDENT1_6_4.value=this.w_VRSDENT1
    endif
    if not(this.oPgFrm.Page6.oPag.oVPSDENT1_6_5.value==this.w_VPSDENT1)
      this.oPgFrm.Page6.oPag.oVPSDENT1_6_5.value=this.w_VPSDENT1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCCOAE1_6_7.value==this.w_MFCCOAE1)
      this.oPgFrm.Page6.oPag.oMFCCOAE1_6_7.value=this.w_MFCCOAE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCDPOS1_6_9.value==this.w_MFCDPOS1)
      this.oPgFrm.Page6.oPag.oMFCDPOS1_6_9.value=this.w_MFCDPOS1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFMSINE1_6_10.value==this.w_MFMSINE1)
      this.oPgFrm.Page6.oPag.oMFMSINE1_6_10.value=this.w_MFMSINE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFANINE1_6_11.value==this.w_MFANINE1)
      this.oPgFrm.Page6.oPag.oMFANINE1_6_11.value=this.w_MFANINE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFMSFIE1_6_12.value==this.w_MFMSFIE1)
      this.oPgFrm.Page6.oPag.oMFMSFIE1_6_12.value=this.w_MFMSFIE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFANF1_6_13.value==this.w_MFANF1)
      this.oPgFrm.Page6.oPag.oMFANF1_6_13.value=this.w_MFANF1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFIMDAE1_6_14.value==this.w_MFIMDAE1)
      this.oPgFrm.Page6.oPag.oMFIMDAE1_6_14.value=this.w_MFIMDAE1
    endif
    if not(this.oPgFrm.Page6.oPag.oMFIMCAE1_6_15.value==this.w_MFIMCAE1)
      this.oPgFrm.Page6.oPag.oMFIMCAE1_6_15.value=this.w_MFIMCAE1
    endif
    if not(this.oPgFrm.Page6.oPag.oVPSDENT2_6_16.value==this.w_VPSDENT2)
      this.oPgFrm.Page6.oPag.oVPSDENT2_6_16.value=this.w_VPSDENT2
    endif
    if not(this.oPgFrm.Page6.oPag.oVRSDENT2_6_17.value==this.w_VRSDENT2)
      this.oPgFrm.Page6.oPag.oVRSDENT2_6_17.value=this.w_VRSDENT2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCCOAE2_6_19.value==this.w_MFCCOAE2)
      this.oPgFrm.Page6.oPag.oMFCCOAE2_6_19.value=this.w_MFCCOAE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFCDPOS2_6_21.value==this.w_MFCDPOS2)
      this.oPgFrm.Page6.oPag.oMFCDPOS2_6_21.value=this.w_MFCDPOS2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFMSINE2_6_22.value==this.w_MFMSINE2)
      this.oPgFrm.Page6.oPag.oMFMSINE2_6_22.value=this.w_MFMSINE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFANINE2_6_23.value==this.w_MFANINE2)
      this.oPgFrm.Page6.oPag.oMFANINE2_6_23.value=this.w_MFANINE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFMSFIE2_6_24.value==this.w_MFMSFIE2)
      this.oPgFrm.Page6.oPag.oMFMSFIE2_6_24.value=this.w_MFMSFIE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFANF2_6_25.value==this.w_MFANF2)
      this.oPgFrm.Page6.oPag.oMFANF2_6_25.value=this.w_MFANF2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFIMDAE2_6_26.value==this.w_MFIMDAE2)
      this.oPgFrm.Page6.oPag.oMFIMDAE2_6_26.value=this.w_MFIMDAE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFIMCAE2_6_27.value==this.w_MFIMCAE2)
      this.oPgFrm.Page6.oPag.oMFIMCAE2_6_27.value=this.w_MFIMCAE2
    endif
    if not(this.oPgFrm.Page6.oPag.oMFTDAENT_6_35.value==this.w_MFTDAENT)
      this.oPgFrm.Page6.oPag.oMFTDAENT_6_35.value=this.w_MFTDAENT
    endif
    if not(this.oPgFrm.Page6.oPag.oMFTCAENT_6_36.value==this.w_MFTCAENT)
      this.oPgFrm.Page6.oPag.oMFTCAENT_6_36.value=this.w_MFTCAENT
    endif
    if not(this.oPgFrm.Page6.oPag.oMFSALAEN_6_37.value==this.w_MFSALAEN)
      this.oPgFrm.Page6.oPag.oMFSALAEN_6_37.value=this.w_MFSALAEN
    endif
    if not(this.oPgFrm.Page6.oPag.oDESAENTE_6_42.value==this.w_DESAENTE)
      this.oPgFrm.Page6.oPag.oDESAENTE_6_42.value=this.w_DESAENTE
    endif
    if not(this.oPgFrm.Page6.oPag.oMESE_6_45.value==this.w_MESE)
      this.oPgFrm.Page6.oPag.oMESE_6_45.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page6.oPag.oANNO_6_46.value==this.w_ANNO)
      this.oPgFrm.Page6.oPag.oANNO_6_46.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page7.oPag.oMFSERIAL_7_1.value==this.w_MFSERIAL)
      this.oPgFrm.Page7.oPag.oMFSERIAL_7_1.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page7.oPag.oMFSALFIN_7_2.value==this.w_MFSALFIN)
      this.oPgFrm.Page7.oPag.oMFSALFIN_7_2.value=this.w_MFSALFIN
    endif
    if not(this.oPgFrm.Page7.oPag.oMESE_7_7.value==this.w_MESE)
      this.oPgFrm.Page7.oPag.oMESE_7_7.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page7.oPag.oANNO_7_8.value==this.w_ANNO)
      this.oPgFrm.Page7.oPag.oANNO_7_8.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page7.oPag.oMFDESSTA_7_10.value==this.w_MFDESSTA)
      this.oPgFrm.Page7.oPag.oMFDESSTA_7_10.value=this.w_MFDESSTA
    endif
    if not(this.oPgFrm.Page7.oPag.oCAP_7_15.value==this.w_CAP)
      this.oPgFrm.Page7.oPag.oCAP_7_15.value=this.w_CAP
    endif
    if not(this.oPgFrm.Page7.oPag.oLOCALI_7_16.value==this.w_LOCALI)
      this.oPgFrm.Page7.oPag.oLOCALI_7_16.value=this.w_LOCALI
    endif
    if not(this.oPgFrm.Page7.oPag.oPROVIN_7_17.value==this.w_PROVIN)
      this.oPgFrm.Page7.oPag.oPROVIN_7_17.value=this.w_PROVIN
    endif
    if not(this.oPgFrm.Page7.oPag.oINDIRI_7_18.value==this.w_INDIRI)
      this.oPgFrm.Page7.oPag.oINDIRI_7_18.value=this.w_INDIRI
    endif
    if not(this.oPgFrm.Page8.oPag.oMFCODFIR_8_16.value==this.w_MFCODFIR)
      this.oPgFrm.Page8.oPag.oMFCODFIR_8_16.value=this.w_MFCODFIR
    endif
    if not(this.oPgFrm.Page8.oPag.oMFCHKFIR_8_18.RadioValue()==this.w_MFCHKFIR)
      this.oPgFrm.Page8.oPag.oMFCHKFIR_8_18.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oMFCARFIR_8_19.RadioValue()==this.w_MFCARFIR)
      this.oPgFrm.Page8.oPag.oMFCARFIR_8_19.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oMFCOGFIR_8_20.value==this.w_MFCOGFIR)
      this.oPgFrm.Page8.oPag.oMFCOGFIR_8_20.value=this.w_MFCOGFIR
    endif
    if not(this.oPgFrm.Page8.oPag.oMFNOMFIR_8_21.value==this.w_MFNOMFIR)
      this.oPgFrm.Page8.oPag.oMFNOMFIR_8_21.value=this.w_MFNOMFIR
    endif
    if not(this.oPgFrm.Page8.oPag.oMFSESFIR_8_22.RadioValue()==this.w_MFSESFIR)
      this.oPgFrm.Page8.oPag.oMFSESFIR_8_22.SetRadio()
    endif
    if not(this.oPgFrm.Page8.oPag.oMFDATFIR_8_23.value==this.w_MFDATFIR)
      this.oPgFrm.Page8.oPag.oMFDATFIR_8_23.value=this.w_MFDATFIR
    endif
    if not(this.oPgFrm.Page8.oPag.oMFCOMFIR_8_24.value==this.w_MFCOMFIR)
      this.oPgFrm.Page8.oPag.oMFCOMFIR_8_24.value=this.w_MFCOMFIR
    endif
    if not(this.oPgFrm.Page8.oPag.oMFPROFIR_8_25.value==this.w_MFPROFIR)
      this.oPgFrm.Page8.oPag.oMFPROFIR_8_25.value=this.w_MFPROFIR
    endif
    if not(this.oPgFrm.Page8.oPag.oMFDOMFIR_8_26.value==this.w_MFDOMFIR)
      this.oPgFrm.Page8.oPag.oMFDOMFIR_8_26.value=this.w_MFDOMFIR
    endif
    if not(this.oPgFrm.Page8.oPag.oMFPRDFIR_8_27.value==this.w_MFPRDFIR)
      this.oPgFrm.Page8.oPag.oMFPRDFIR_8_27.value=this.w_MFPRDFIR
    endif
    if not(this.oPgFrm.Page8.oPag.oMFCAPFIR_8_28.value==this.w_MFCAPFIR)
      this.oPgFrm.Page8.oPag.oMFCAPFIR_8_28.value=this.w_MFCAPFIR
    endif
    if not(this.oPgFrm.Page8.oPag.oMFINDFIR_8_32.value==this.w_MFINDFIR)
      this.oPgFrm.Page8.oPag.oMFINDFIR_8_32.value=this.w_MFINDFIR
    endif
    if not(this.oPgFrm.Page8.oPag.oMFMAIVER_8_44.value==this.w_MFMAIVER)
      this.oPgFrm.Page8.oPag.oMFMAIVER_8_44.value=this.w_MFMAIVER
    endif
    if not(this.oPgFrm.Page8.oPag.oMFBANPAS_8_45.value==this.w_MFBANPAS)
      this.oPgFrm.Page8.oPag.oMFBANPAS_8_45.value=this.w_MFBANPAS
    endif
    if not(this.oPgFrm.Page8.oPag.oBADESCRI_8_46.value==this.w_BADESCRI)
      this.oPgFrm.Page8.oPag.oBADESCRI_8_46.value=this.w_BADESCRI
    endif
    if not(this.oPgFrm.Page8.oPag.oMFCOIBAN_8_47.value==this.w_MFCOIBAN)
      this.oPgFrm.Page8.oPag.oMFCOIBAN_8_47.value=this.w_MFCOIBAN
    endif
    if not(this.oPgFrm.Page8.oPag.oMFSERIAL_8_52.value==this.w_MFSERIAL)
      this.oPgFrm.Page8.oPag.oMFSERIAL_8_52.value=this.w_MFSERIAL
    endif
    if not(this.oPgFrm.Page8.oPag.oMFSALFIN_8_53.value==this.w_MFSALFIN)
      this.oPgFrm.Page8.oPag.oMFSALFIN_8_53.value=this.w_MFSALFIN
    endif
    if not(this.oPgFrm.Page8.oPag.oMESE_8_57.value==this.w_MESE)
      this.oPgFrm.Page8.oPag.oMESE_8_57.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page8.oPag.oANNO_8_58.value==this.w_ANNO)
      this.oPgFrm.Page8.oPag.oANNO_8_58.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page8.oPag.oMFCOIBAN_8_66.value==this.w_MFCOIBAN)
      this.oPgFrm.Page8.oPag.oMFCOIBAN_8_66.value=this.w_MFCOIBAN
    endif
    cp_SetControlsValueExtFlds(this,'MOD_PAG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(not empty(.w_MFMESRIF) and val(.w_MFMESRIF)>= 1 and val(.w_MFMESRIF)<13)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMFMESRIF_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Mese di riferimento non valido")
          case   not(not empty(.w_MFANNRIF) and val(.w_MFANNRIF)>=1950 and val(.w_MFANNRIF)<=2050)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMFANNRIF_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un anno compreso fra il 1950 e 2050")
          case   (empty(.w_MFCCONT1))  and (not empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT1_3_3.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_APP1A  Or  (!empty(.w_MFDAMES1) and 0<val(.w_MFDAMES1) and val(.w_MFDAMES1)<13))  and (not empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES1_3_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP1A  Or  (!empty(.w_MFDAANN1) and (val(.w_MFDAANN1)>=1996  and val(.w_MFDAANN1)<=2050)))  and (!empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN1_3_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(.w_APP1 Or (!empty(.w_MF_AMES1)  and 0<val(.w_MF_AMES1) and val(.w_MF_AMES1)<13))  and (!empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES1_3_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP1 Or (val(.w_MFAN1)<=2050 and(val(.w_MFDAANN1)<val(.w_MFAN1)or .w_MFDAANN1=.w_MFAN1 and val(.w_MFDAMES1)<=val(.w_MF_AMES1))))  and (!empty(.w_MFCDSED1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN1_3_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFCCONT2))  and (not empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT2_3_14.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_APP2A  Or  (!empty(.w_MFDAMES2) and 0<val(.w_MFDAMES2) and val(.w_MFDAMES2)<13))  and (not empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES2_3_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP2A  Or  (!empty(.w_MFDAANN2) and (val(.w_MFDAANN2)>=1996  and val(.w_MFDAANN2)<=2050)))  and (!empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN2_3_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(.w_APP2 Or (!empty(.w_MF_AMES2)  and 0<val(.w_MF_AMES2) and val(.w_MF_AMES2)<13))  and (!empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES2_3_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP2 Or (val(.w_MFAN2)<=2050 and(val(.w_MFDAANN2)<val(.w_MFAN2)or .w_MFDAANN2=.w_MFAN2 and val(.w_MFDAMES2)<=val(.w_MF_AMES2))))  and (!empty(.w_MFCDSED2))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN2_3_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFCCONT3))  and (not empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT3_3_25.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT3)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_APP3A  Or  (!empty(.w_MFDAMES3) and 0<val(.w_MFDAMES3) and val(.w_MFDAMES3)<13))  and (not empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES3_3_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP3A  Or  (!empty(.w_MFDAANN3) and (val(.w_MFDAANN3)>=1996  and val(.w_MFDAANN3)<=2050)))  and (!empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN3_3_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(.w_APP3 Or (!empty(.w_MF_AMES3)  and 0<val(.w_MF_AMES3) and val(.w_MF_AMES3)<13))  and (!empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES3_3_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP3 Or (val(.w_MFAN3)<=2050 and(val(.w_MFDAANN3)<val(.w_MFAN3)or .w_MFDAANN3=.w_MFAN3 and val(.w_MFDAMES3)<=val(.w_MF_AMES3))))  and (!empty(.w_MFCDSED3))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN3_3_32.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFCCONT4))  and (not empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFCCONT4_3_36.SetFocus()
            i_bnoObbl = !empty(.w_MFCCONT4)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_APP4A  Or  (!empty(.w_MFDAMES4) and 0<val(.w_MFDAMES4) and val(.w_MFDAMES4)<13))  and (not empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAMES4_3_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP4A  Or  (!empty(.w_MFDAANN4) and (val(.w_MFDAANN4)>=1996  and val(.w_MFDAANN4)<=2050)))  and (!empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFDAANN4_3_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un anno compreso fra 1996 e 2050")
          case   not(.w_APP4 Or (!empty(.w_MF_AMES4)  and 0<val(.w_MF_AMES4) and val(.w_MF_AMES4)<13))  and (!empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMF_AMES4_3_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: selezionare un mese valido nella forma MM")
          case   not(.w_APP4 Or (val(.w_MFAN4)<=2050 and(val(.w_MFDAANN4)<val(.w_MFAN4)or .w_MFDAANN4=.w_MFAN4 and val(.w_MFDAMES4)<=val(.w_MF_AMES4))))  and (!empty(.w_MFCDSED4))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oMFAN4_3_43.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050")
          case   ((empty(.w_MFTRIRE1)) or not(.w_TIPTRI='Regione' or empty(.w_TIPTRI)))  and (not empty(.w_MFCODRE1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE1_4_3.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
          case   not(empty(.w_MFRATRE1) or left(.w_MFRATRE1,2)<=right(.w_MFRATRE1,2) and len(alltrim(.w_MFRATRE1))=4)  and (not empty(.w_MFCODRE1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE1_4_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(.w_MFMESER1>='01' and .w_MFMESER1<='12' or (empty(.w_MFMESER1)  and .w_CHKOBBME1<>'S'))  and (!empty(.w_MFCODRE1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFMESER1_4_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o dato obbligatorio")
          case   not(!empty(.w_MFANNRE1) and (val(.w_MFANNRE1)>=1996  and val(.w_MFANNRE1)<=2050  or .w_MFANNRE1='0000'))  and (!empty(.w_MFCODRE1))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE1_4_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   ((empty(.w_MFTRIRE2)) or not(.w_TIPTRI='Regione'  or empty(.w_TIPTRI)))  and (not empty(.w_MFCODRE2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE2_4_13.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
          case   not(empty(.w_MFRATRE2) or left(.w_MFRATRE2,2)<=right(.w_MFRATRE2,2) and len(alltrim(.w_MFRATRE2))=4)  and (not empty(.w_MFCODRE2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE2_4_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(.w_MFMESER2>='01' and .w_MFMESER2<='12' or (empty(.w_MFMESER2)  and .w_CHKOBBME2<>'S'))  and (!empty(.w_MFCODRE2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFMESER2_4_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o dato obbligatorio")
          case   not(!empty(.w_MFANNRE2) and (val(.w_MFANNRE2)>=1996  and val(.w_MFANNRE2)<=2050   or .w_MFANNRE2='0000'))  and (!empty(.w_MFCODRE2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE2_4_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   ((empty(.w_MFTRIRE3)) or not(.w_TIPTRI='Regione'   or empty(.w_TIPTRI)))  and (not empty(.w_MFCODRE3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE3_4_20.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE3)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
          case   not(empty(.w_MFRATRE3) or left(.w_MFRATRE3,2)<=right(.w_MFRATRE3,2) and len(alltrim(.w_MFRATRE3))=4)  and (not empty(.w_MFCODRE3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE3_4_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(.w_MFMESER3>='01' and .w_MFMESER3<='12' or (empty(.w_MFMESER3)  and .w_CHKOBBME3<>'S'))  and (!empty(.w_MFCODRE3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFMESER3_4_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o dato obbligatorio")
          case   not(!empty(.w_MFANNRE3) and (val(.w_MFANNRE3)>=1996  and val(.w_MFANNRE3)<=2050  or .w_MFANNRE3='0000'))  and (!empty(.w_MFCODRE3))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE3_4_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   ((empty(.w_MFTRIRE4)) or not(.w_TIPTRI='Regione'  or empty(.w_TIPTRI)))  and (not empty(.w_MFCODRE4))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFTRIRE4_4_27.SetFocus()
            i_bnoObbl = !empty(.w_MFTRIRE4)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un codice tributo di tipo 'regione'")
          case   not(empty(.w_MFRATRE4) or left(.w_MFRATRE4,2)<=right(.w_MFRATRE4,2) and len(alltrim(.w_MFRATRE4))=4)  and (not empty(.w_MFCODRE4))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFRATRE4_4_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: numero rata o totale rate errato")
          case   not(.w_MFMESER4>='01' and .w_MFMESER4<='12' or (empty(.w_MFMESER4)  and .w_CHKOBBME4<>'S'))  and (!empty(.w_MFCODRE4))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFMESER4_4_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o dato obbligatorio")
          case   not(!empty(.w_MFANNRE4) and (val(.w_MFANNRE4)>=1996  and val(.w_MFANNRE4)<=2050   or  .w_MFANNRE4='0000'))  and (!empty(.w_MFCODRE4))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oMFANNRE4_4_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>")
          case   not(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFMSINE1) and 0< val(.w_MFMSINE1) and val(.w_MFMSINE1) < 13))  and (!empty(.w_MFCCOAE1))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFMSINE1_6_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un mese valido nella forma MM")
          case   not(alltrim(.w_MFCCOAE1) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFANINE1) and (val(.w_MFANINE1)>=1996  and  val(.w_MFANINE1)<=2050)))  and (!empty(.w_MFCCOAE1))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFANINE1_6_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un anno compreso fra 1996 e 2050")
          case   not(alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFMSFIE1) and 0<val(.w_MFMSFIE1) and val(.w_MFMSFIE1)<13))  and (!empty(.w_MFCCOAE1))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFMSFIE1_6_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un mese valido nella forma MM")
          case   not(alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFANF1)and((val(.w_MFANINE1)<val(.w_MFANF1)or .w_MFANINE1=.w_MFANF1 and val(.w_MFMSINE1)<=val(.w_MFMSFIE1)))))  and (!empty(.w_MFCCOAE1))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFANF1_6_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: periodo minore del precedente oppure anno maggiore del 2050")
          case   not(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFMSINE2) and 0<  val(.w_MFMSINE2) and val(.w_MFMSINE2) < 13))  and (!empty(.w_MFCCOAE2))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFMSINE2_6_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un mese valido nella forma MM")
          case   not(alltrim(.w_MFCCOAE2) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFANINE2)  and (val(.w_MFANINE2)>=1996  and val(.w_MFANINE2)<=2050)))  and (!empty(.w_MFCCOAE2))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFANINE2_6_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un anno compreso fra 1996 e 2050")
          case   not(alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFMSFIE2)  and 0<val(.w_MFMSFIE2) and val(.w_MFMSFIE2)<13))  and (!empty(.w_MFCCOAE2))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFMSFIE2_6_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: selezionare un mese valido nella forma MM")
          case   not(alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFANF2)and((val(.w_MFANINE2)<val(.w_MFANF2)or .w_MFANINE2=.w_MFANF2 and val(.w_MFMSINE2)<=val(.w_MFMSFIE2)))))  and (!empty(.w_MFCCOAE2))
            .oPgFrm.ActivePage = 6
            .oPgFrm.Page6.oPag.oMFANF2_6_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Sezione altri enti: periodo minore del precedente oppure anno maggiore del 2050")
          case   (empty(.w_MFCODFIR))  and not(EMPTY (.w_ENTRATEL))  and (.w_MFCHKFIR='S')
            .oPgFrm.ActivePage = 8
            .oPgFrm.Page8.oPag.oMFCODFIR_8_16.SetFocus()
            i_bnoObbl = !empty(.w_MFCODFIR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MFCOGFIR))  and not(EMPTY (.w_ENTRATEL))  and (.w_MFCHKFIR='S' )
            .oPgFrm.ActivePage = 8
            .oPgFrm.Page8.oPag.oMFCOGFIR_8_20.SetFocus()
            i_bnoObbl = !empty(.w_MFCOGFIR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MFNOMFIR))  and not(EMPTY (.w_ENTRATEL))  and (.w_MFCHKFIR='S' )
            .oPgFrm.ActivePage = 8
            .oPgFrm.Page8.oPag.oMFNOMFIR_8_21.SetFocus()
            i_bnoObbl = !empty(.w_MFNOMFIR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MFDATFIR))  and not(EMPTY (.w_ENTRATEL))  and (.w_MFCHKFIR='S' )
            .oPgFrm.ActivePage = 8
            .oPgFrm.Page8.oPag.oMFDATFIR_8_23.SetFocus()
            i_bnoObbl = !empty(.w_MFDATFIR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MFCOMFIR))  and not(EMPTY (.w_ENTRATEL))  and (.w_MFCHKFIR='S' )
            .oPgFrm.ActivePage = 8
            .oPgFrm.Page8.oPag.oMFCOMFIR_8_24.SetFocus()
            i_bnoObbl = !empty(.w_MFCOMFIR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MFPROFIR))  and not(EMPTY (.w_ENTRATEL))  and (.w_MFCHKFIR='S' )
            .oPgFrm.ActivePage = 8
            .oPgFrm.Page8.oPag.oMFPROFIR_8_25.SetFocus()
            i_bnoObbl = !empty(.w_MFPROFIR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_BATIPCON<>'S' AND (EMPTY(.w_DATOBSO1) OR .w_DATOBSO1>.w_OBTEST) AND .w_BACONSBF='C')  and not(.cFunction<>'Load')  and not(empty(.w_MFBANPAS))
            .oPgFrm.ActivePage = 8
            .oPgFrm.Page8.oPag.oMFBANPAS_8_45.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice banca inesistente, obsoleta, di compensazione o salvo buon fine")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCG_AFR.CheckForm()
      if i_bres
        i_bres=  .GSCG_AFR.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_AVF.CheckForm()
      if i_bres
        i_bres=  .GSCG_AVF.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=7
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_AIG.CheckForm()
      if i_bres
        i_bres=  .GSCG_AIG.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_ACG.CheckForm()
      if i_bres
        i_bres=  .GSCG_ACG.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gscg_aop
      if i_bRes
        .w_RESCHK=0
        .NotifyEvent('ControllaDati')
        if .w_RESCHK<>0
          i_bRes=.f.
        endif
      endif
      
      IF .w_MFENTRAT='E' AND .w_MFCHKFIR='S'
       
         IF .gscg_acg.w_CFPERFIS ='S' AND  .w_MFCARFIR='1' 
           AH_ERRORMSG ("Se il contribuente � persona fisica, il codice carica del firmatario non pu� essere impostato a Rappresentante legale")
           i_bRes=.f.
       ENDIF
       
       
        IF .gscg_acg.w_CFPERFIS ='N'
         IF NOT .w_MFCARFIR $ " 1 3 "  
           AH_ERRORMSG ("Se il contribuente non � persona fisica, il codice carica del firmatario pu� essere impostato solo a Rappresentante legale oppure curatore fallimentare")
           i_bRes=.f.
         ENDIF 
       ENDIF
      
       
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MFSERIAL = this.w_MFSERIAL
    this.o_APPCOIN = this.w_APPCOIN
    this.o_MFCDSED1 = this.w_MFCDSED1
    this.o_MFCCONT1 = this.w_MFCCONT1
    this.o_APP1A = this.w_APP1A
    this.o_APP1 = this.w_APP1
    this.o_MFCDSED2 = this.w_MFCDSED2
    this.o_MFCCONT2 = this.w_MFCCONT2
    this.o_APP2A = this.w_APP2A
    this.o_APP2 = this.w_APP2
    this.o_MFCDSED3 = this.w_MFCDSED3
    this.o_MFCCONT3 = this.w_MFCCONT3
    this.o_APP3A = this.w_APP3A
    this.o_APP3 = this.w_APP3
    this.o_MFCDSED4 = this.w_MFCDSED4
    this.o_MFCCONT4 = this.w_MFCCONT4
    this.o_APP4A = this.w_APP4A
    this.o_APP4 = this.w_APP4
    this.o_MFCODRE1 = this.w_MFCODRE1
    this.o_MFTRIRE1 = this.w_MFTRIRE1
    this.o_MFCODRE2 = this.w_MFCODRE2
    this.o_MFTRIRE2 = this.w_MFTRIRE2
    this.o_MFCODRE3 = this.w_MFCODRE3
    this.o_MFTRIRE3 = this.w_MFTRIRE3
    this.o_MFCODRE4 = this.w_MFCODRE4
    this.o_MFTRIRE4 = this.w_MFTRIRE4
    this.o_MFSINAI1 = this.w_MFSINAI1
    this.o_MFSINAI2 = this.w_MFSINAI2
    this.o_MFSINAI3 = this.w_MFSINAI3
    this.o_MFCDENTE = this.w_MFCDENTE
    this.o_VRSDENT1 = this.w_VRSDENT1
    this.o_VPSDENT1 = this.w_VPSDENT1
    this.o_MFSDENT1 = this.w_MFSDENT1
    this.o_MFCCOAE1 = this.w_MFCCOAE1
    this.o_VPSDENT2 = this.w_VPSDENT2
    this.o_VRSDENT2 = this.w_VRSDENT2
    this.o_MFSDENT2 = this.w_MFSDENT2
    this.o_MFCCOAE2 = this.w_MFCCOAE2
    this.o_MFDESSTA = this.w_MFDESSTA
    this.o_CAP1 = this.w_CAP1
    this.o_LOCALI1 = this.w_LOCALI1
    this.o_INDIRI1 = this.w_INDIRI1
    this.o_PROVIN1 = this.w_PROVIN1
    this.o_CHKOBBME1 = this.w_CHKOBBME1
    this.o_CHKOBBME2 = this.w_CHKOBBME2
    this.o_CHKOBBME3 = this.w_CHKOBBME3
    this.o_CHKOBBME4 = this.w_CHKOBBME4
    this.o_RAPFIRM = this.w_RAPFIRM
    this.o_MFCHKFIR = this.w_MFCHKFIR
    this.o_MFCARFIR = this.w_MFCARFIR
    this.o_MFBANPAS = this.w_MFBANPAS
    * --- GSCG_AFR : Depends On
    this.GSCG_AFR.SaveDependsOn()
    * --- GSCG_AVF : Depends On
    this.GSCG_AVF.SaveDependsOn()
    * --- GSCG_AIG : Depends On
    this.GSCG_AIG.SaveDependsOn()
    * --- GSCG_ACG : Depends On
    this.GSCG_ACG.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_aopPag1 as StdContainer
  Width  = 869
  height = 441
  stdWidth  = 869
  stdheight = 441
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_1_1 as StdField with uid="INGQIDSUEM",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero modello",;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=90, Left=127, Top=19, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFMESRIF_1_2 as StdField with uid="HFOOEBHDWN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MFMESRIF", cQueryName = "MFMESRIF",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Mese di riferimento non valido",;
    ToolTipText = "Mese di riferimento",;
    HelpContextID = 217052684,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=383, Top=19, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMESRIF_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_MFMESRIF) and val(.w_MFMESRIF)>= 1 and val(.w_MFMESRIF)<13)
    endwith
    return bRes
  endfunc

  add object oMFANNRIF_1_3 as StdField with uid="SPMPJCSSZI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MFANNRIF", cQueryName = "MFANNRIF",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un anno compreso fra il 1950 e 2050",;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 212350476,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=408, Top=19, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  proc oMFANNRIF_1_3.mAfter
    with this.Parent.oContained
      .w_MFANNRIF=yy2yyyy(.w_MFANNRIF)
    endwith
  endproc

  func oMFANNRIF_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_MFANNRIF) and val(.w_MFANNRIF)>=1950 and val(.w_MFANNRIF)<=2050)
    endwith
    return bRes
  endfunc


  add object oObj_1_5 as cp_runprogram with uid="UGGQSTDUFI",left=-3, top=459, width=134,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BIM('N','ANN')",;
    cEvent = "New record",;
    nPag=1;
    , HelpContextID = 267263514

  add object oMF_COINC_1_7 as StdCheck with uid="RKATVGKMBB",rtseq=6,rtrep=.f.,left=654, top=52, caption="",;
    HelpContextID = 206629367,;
    cFormVar="w_MF_COINC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMF_COINC_1_7.RadioValue()
    return(iif(this.value =1,'N',;
    'S'))
  endfunc
  func oMF_COINC_1_7.GetRadio()
    this.Parent.oContained.w_MF_COINC = this.RadioValue()
    return .t.
  endfunc

  func oMF_COINC_1_7.SetRadio()
    this.Parent.oContained.w_MF_COINC=trim(this.Parent.oContained.w_MF_COINC)
    this.value = ;
      iif(this.Parent.oContained.w_MF_COINC=='N',1,;
      0)
  endfunc


  add object oLinkPC_1_18 as stdDynamicChildContainer with uid="VFEXZQDZFH",left=10, top=78, width=850, height=359, bOnScreen=.t.;



  add object oObj_1_19 as cp_runprogram with uid="EAXHMSXTSY",left=150, top=460, width=134,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BAK('N','ANN')",;
    cEvent = "New record",;
    nPag=1;
    , HelpContextID = 267263514


  add object oObj_1_20 as cp_runprogram with uid="GFQIHJVWOZ",left=304, top=460, width=251,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BMG",;
    cEvent = "Record Inserted,Record Updated",;
    nPag=1;
    , HelpContextID = 267263514

  add object oStr_1_8 as StdString with uid="RIMPGFGUUC",Visible=.t., Left=339, Top=54,;
    Alignment=1, Width=308, Height=15,;
    Caption="Anno di imposta non coincidente con anno solare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="EFRBFXDZBK",Visible=.t., Left=1, Top=19,;
    Alignment=1, Width=122, Height=15,;
    Caption="Numero modello:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="DNRJNKBPHF",Visible=.t., Left=224, Top=19,;
    Alignment=1, Width=154, Height=15,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="IWKPFCUQCR",Visible=.t., Left=485, Top=20,;
    Alignment=1, Width=124, Height=15,;
    Caption="Valuta di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="IZPIVVEYIL",Visible=.t., Left=614, Top=20,;
    Alignment=0, Width=34, Height=18,;
    Caption="Euro"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgscg_aopPag2 as StdContainer
  Width  = 869
  height = 441
  stdWidth  = 869
  stdheight = 441
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_2_1 as StdField with uid="ODPEQKOEGB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero modello",;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=90, Left=135, Top=18, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)


  add object oLinkPC_2_2 as stdDynamicChildContainer with uid="LPQACJIYQL",left=5, top=44, width=765, height=297, bOnScreen=.t.;


  add object oMFCODUFF_2_4 as StdField with uid="RYJFAXKYGF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MFCODUFF", cQueryName = "MFCODUFF",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice ufficio",;
    HelpContextID = 252270092,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=100, Top=353, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CODI_UFF", cZoomOnZoom="GSCG_AUF", oKey_1_1="UFCODICE", oKey_1_2="this.w_MFCODUFF"

  func oMFCODUFF_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODUFF_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODUFF_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CODI_UFF','*','UFCODICE',cp_AbsName(this.parent,'oMFCODUFF_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AUF',"Codici ufficio",'',this.parent.oContained
  endproc
  proc oMFCODUFF_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AUF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UFCODICE=this.parent.oContained.w_MFCODUFF
     i_obj.ecpSave()
  endproc

  add object oMFCODATT_2_5 as StdField with uid="TNODRPVFFT",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MFCODATT", cQueryName = "MFCODATT",;
    bObbl = .f. , nPag = 2, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice atto",;
    HelpContextID = 83274214,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=240, Top=353, cSayPict='"99999999999"', cGetPict='"99999999999"', InputMask=replicate('X',11)

  add object oMESE_2_8 as StdField with uid="KPWCJAHHDA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=357, Top=19, InputMask=replicate('X',2)

  add object oANNO_2_9 as StdField with uid="QEFDSTIJJN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=382, Top=19, InputMask=replicate('X',4)


  add object oObj_2_13 as cp_runprogram with uid="EVNNJKOTZA",left=-4, top=460, width=189,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="Gscg_Bvi('I')",;
    cEvent = "Insert end,Update end",;
    nPag=2;
    , HelpContextID = 267263514


  add object oObj_2_15 as cp_runprogram with uid="EWTIJKBMUU",left=-4, top=482, width=128,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="Gscg_Bvi('C')",;
    cEvent = "Delete end",;
    nPag=2;
    , HelpContextID = 267263514


  add object oBtn_2_16 as StdButton with uid="TWGRUZRJZH",left=729, top=346, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per visualizzare le distinte versamento di tipo I.R.PE.F.";
    , HelpContextID = 182357498;
    , tabstop=.f., caption='\<Distinte';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_16.Click()
      with this.Parent.oContained
        GSRI_BVI(this.Parent.oContained,"IR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_16.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_Rite<>'S')
     endwith
    endif
  endfunc

  add object oStr_2_6 as StdString with uid="DUVZRBXUKS",Visible=.t., Left=7, Top=19,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_7 as StdString with uid="EWLCKETTJZ",Visible=.t., Left=227, Top=21,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="NFEBJOXPXM",Visible=.t., Left=6, Top=356,;
    Alignment=1, Width=90, Height=15,;
    Caption="Codice ufficio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="LRITFRLPUR",Visible=.t., Left=156, Top=356,;
    Alignment=1, Width=80, Height=15,;
    Caption="Codice atto:"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_afr",lower(this.oContained.GSCG_AFR.class))=0
        this.oContained.GSCG_AFR.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgscg_aopPag3 as StdContainer
  Width  = 869
  height = 441
  stdWidth  = 869
  stdheight = 441
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_3_1 as StdField with uid="UMTAXXYHVZ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=90, Left=140, Top=26, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFCDSED1_3_2 as StdField with uid="CKABJGIONW",rtseq=16,rtrep=.f.,;
    cFormVar = "w_MFCDSED1", cQueryName = "MFCDSED1",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 1157641,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=113, Top=131, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED1"

  func oMFCDSED1_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED1_3_2.ecpDrop(oSource)
    this.Parent.oContained.link_3_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED1_3_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED1_3_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED1_3_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED1
     i_obj.ecpSave()
  endproc

  add object oMFCCONT1_3_3 as StdField with uid="SPGALMZAEZ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_MFCCONT1", cQueryName = "MFCCONT1",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 145577463,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=192, Top=131, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT1"

  func oMFCCONT1_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFCCONT1_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT1_3_3.ecpDrop(oSource)
    this.Parent.oContained.link_3_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT1_3_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT1_3_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT1_3_3.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT1
     i_obj.ecpSave()
  endproc

  add object oMFMINPS1_3_4 as StdField with uid="UZPQGAZZEX",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MFMINPS1", cQueryName = "MFMINPS1",;
    bObbl = .f. , nPag = 3, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 89917961,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=274, Top=131, InputMask=replicate('X',17)

  func oMFMINPS1_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  add object oMFDAMES1_3_7 as StdField with uid="LNKWVLBIEA",rtseq=21,rtrep=.f.,;
    cFormVar = "w_MFDAMES1", cQueryName = "MFDAMES1",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7641609,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=475, Top=131, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES1_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFDAMES1_3_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP1A  Or  (!empty(.w_MFDAMES1) and 0<val(.w_MFDAMES1) and val(.w_MFDAMES1)<13))
    endwith
    return bRes
  endfunc

  add object oMFDAANN1_3_8 as StdField with uid="AKJYRQPYXF",rtseq=22,rtrep=.f.,;
    cFormVar = "w_MFDAANN1", cQueryName = "MFDAANN1",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 137665033,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=502, Top=131, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN1_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFDAANN1_3_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP1A  Or  (!empty(.w_MFDAANN1) and (val(.w_MFDAANN1)>=1996  and val(.w_MFDAANN1)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMF_AMES1_3_9 as StdField with uid="UGNEEGCWGA",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MF_AMES1", cQueryName = "MF_AMES1",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7531017,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=557, Top=131, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES1_3_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMF_AMES1_3_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP1 Or (!empty(.w_MF_AMES1)  and 0<val(.w_MF_AMES1) and val(.w_MF_AMES1)<13))
    endwith
    return bRes
  endfunc

  add object oMFAN1_3_10 as StdField with uid="HMOIVJLGQJ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_MFAN1", cQueryName = "MFAN1",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 148387270,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=586, Top=131, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN1_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  func oMFAN1_3_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP1 Or (val(.w_MFAN1)<=2050 and(val(.w_MFDAANN1)<val(.w_MFAN1)or .w_MFDAANN1=.w_MFAN1 and val(.w_MFDAMES1)<=val(.w_MF_AMES1))))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD1_3_11 as StdField with uid="TCTJSLGIBA",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MFIMPSD1", cQueryName = "MFIMPSD1",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 231192055,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=113, Top=254, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSD1_3_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  add object oMFIMPSC1_3_12 as StdField with uid="FPDWLHKRZP",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MFIMPSC1", cQueryName = "MFIMPSC1",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243401,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=277, Top=254, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSC1_3_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED1))
    endwith
   endif
  endfunc

  add object oMFCDSED2_3_13 as StdField with uid="SXZYRVNBKS",rtseq=27,rtrep=.f.,;
    cFormVar = "w_MFCDSED2", cQueryName = "MFCDSED2",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 1157640,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=113, Top=152, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED2"

  func oMFCDSED2_3_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED2_3_13.ecpDrop(oSource)
    this.Parent.oContained.link_3_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED2_3_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED2_3_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED2_3_13.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED2
     i_obj.ecpSave()
  endproc

  add object oMFCCONT2_3_14 as StdField with uid="AVTFDROKIG",rtseq=28,rtrep=.f.,;
    cFormVar = "w_MFCCONT2", cQueryName = "MFCCONT2",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 145577464,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=192, Top=152, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT2"

  func oMFCCONT2_3_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFCCONT2_3_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT2_3_14.ecpDrop(oSource)
    this.Parent.oContained.link_3_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT2_3_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT2_3_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT2_3_14.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT2
     i_obj.ecpSave()
  endproc

  add object oMFMINPS2_3_15 as StdField with uid="TLRSSESNPI",rtseq=29,rtrep=.f.,;
    cFormVar = "w_MFMINPS2", cQueryName = "MFMINPS2",;
    bObbl = .f. , nPag = 3, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 89917960,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=274, Top=152, InputMask=replicate('X',17)

  func oMFMINPS2_3_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  add object oMFDAMES2_3_17 as StdField with uid="HZGQCKOMWJ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_MFDAMES2", cQueryName = "MFDAMES2",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7641608,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=475, Top=152, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES2_3_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFDAMES2_3_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP2A  Or  (!empty(.w_MFDAMES2) and 0<val(.w_MFDAMES2) and val(.w_MFDAMES2)<13))
    endwith
    return bRes
  endfunc

  add object oMFDAANN2_3_18 as StdField with uid="WBAUDZABUC",rtseq=32,rtrep=.f.,;
    cFormVar = "w_MFDAANN2", cQueryName = "MFDAANN2",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 137665032,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=502, Top=152, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN2_3_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFDAANN2_3_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP2A  Or  (!empty(.w_MFDAANN2) and (val(.w_MFDAANN2)>=1996  and val(.w_MFDAANN2)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMF_AMES2_3_20 as StdField with uid="XRSTGKETUN",rtseq=34,rtrep=.f.,;
    cFormVar = "w_MF_AMES2", cQueryName = "MF_AMES2",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7531016,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=557, Top=152, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES2_3_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMF_AMES2_3_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP2 Or (!empty(.w_MF_AMES2)  and 0<val(.w_MF_AMES2) and val(.w_MF_AMES2)<13))
    endwith
    return bRes
  endfunc

  add object oMFAN2_3_21 as StdField with uid="LQXSVVFIUC",rtseq=35,rtrep=.f.,;
    cFormVar = "w_MFAN2", cQueryName = "MFAN2",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 149435846,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=586, Top=152, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN2_3_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  func oMFAN2_3_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP2 Or (val(.w_MFAN2)<=2050 and(val(.w_MFDAANN2)<val(.w_MFAN2)or .w_MFDAANN2=.w_MFAN2 and val(.w_MFDAMES2)<=val(.w_MF_AMES2))))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD2_3_22 as StdField with uid="DPPNTFIXZF",rtseq=36,rtrep=.f.,;
    cFormVar = "w_MFIMPSD2", cQueryName = "MFIMPSD2",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 231192056,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=113, Top=274, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSD2_3_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  add object oMFIMPSC2_3_23 as StdField with uid="DKBHHCVJQC",rtseq=37,rtrep=.f.,;
    cFormVar = "w_MFIMPSC2", cQueryName = "MFIMPSC2",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243400,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=277, Top=274, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSC2_3_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED2))
    endwith
   endif
  endfunc

  add object oMFCDSED3_3_24 as StdField with uid="PMUMNAHIUI",rtseq=38,rtrep=.f.,;
    cFormVar = "w_MFCDSED3", cQueryName = "MFCDSED3",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 1157639,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=113, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED3"

  func oMFCDSED3_3_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED3_3_24.ecpDrop(oSource)
    this.Parent.oContained.link_3_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED3_3_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED3_3_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED3_3_24.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED3
     i_obj.ecpSave()
  endproc

  add object oMFCCONT3_3_25 as StdField with uid="MUKHFMMIGC",rtseq=39,rtrep=.f.,;
    cFormVar = "w_MFCCONT3", cQueryName = "MFCCONT3",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 145577465,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=192, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT3"

  func oMFCCONT3_3_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFCCONT3_3_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT3_3_25.ecpDrop(oSource)
    this.Parent.oContained.link_3_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT3_3_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT3_3_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT3_3_25.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT3
     i_obj.ecpSave()
  endproc

  add object oMFMINPS3_3_26 as StdField with uid="CPVWKREMYW",rtseq=40,rtrep=.f.,;
    cFormVar = "w_MFMINPS3", cQueryName = "MFMINPS3",;
    bObbl = .f. , nPag = 3, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 89917959,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=274, Top=173, InputMask=replicate('X',17)

  func oMFMINPS3_3_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  add object oMFDAMES3_3_28 as StdField with uid="UCGDBCLFJK",rtseq=42,rtrep=.f.,;
    cFormVar = "w_MFDAMES3", cQueryName = "MFDAMES3",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7641607,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=475, Top=173, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES3_3_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFDAMES3_3_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP3A  Or  (!empty(.w_MFDAMES3) and 0<val(.w_MFDAMES3) and val(.w_MFDAMES3)<13))
    endwith
    return bRes
  endfunc

  add object oMFDAANN3_3_30 as StdField with uid="PYVGITCREO",rtseq=44,rtrep=.f.,;
    cFormVar = "w_MFDAANN3", cQueryName = "MFDAANN3",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 137665031,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=502, Top=173, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN3_3_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFDAANN3_3_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP3A  Or  (!empty(.w_MFDAANN3) and (val(.w_MFDAANN3)>=1996  and val(.w_MFDAANN3)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMF_AMES3_3_31 as StdField with uid="QTOOIBGIKI",rtseq=45,rtrep=.f.,;
    cFormVar = "w_MF_AMES3", cQueryName = "MF_AMES3",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7531015,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=557, Top=173, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES3_3_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMF_AMES3_3_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP3 Or (!empty(.w_MF_AMES3)  and 0<val(.w_MF_AMES3) and val(.w_MF_AMES3)<13))
    endwith
    return bRes
  endfunc

  add object oMFAN3_3_32 as StdField with uid="FWZATATEDM",rtseq=46,rtrep=.f.,;
    cFormVar = "w_MFAN3", cQueryName = "MFAN3",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 150484422,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=586, Top=173, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN3_3_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  func oMFAN3_3_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP3 Or (val(.w_MFAN3)<=2050 and(val(.w_MFDAANN3)<val(.w_MFAN3)or .w_MFDAANN3=.w_MFAN3 and val(.w_MFDAMES3)<=val(.w_MF_AMES3))))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD3_3_33 as StdField with uid="NRTFEMYREN",rtseq=47,rtrep=.f.,;
    cFormVar = "w_MFIMPSD3", cQueryName = "MFIMPSD3",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 231192057,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=113, Top=294, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSD3_3_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  add object oMFIMPSC3_3_34 as StdField with uid="RMAERXBXYN",rtseq=48,rtrep=.f.,;
    cFormVar = "w_MFIMPSC3", cQueryName = "MFIMPSC3",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243399,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=277, Top=294, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSC3_3_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED3))
    endwith
   endif
  endfunc

  add object oMFCDSED4_3_35 as StdField with uid="RENIINYJRG",rtseq=49,rtrep=.f.,;
    cFormVar = "w_MFCDSED4", cQueryName = "MFCDSED4",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS",;
    HelpContextID = 1157638,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=113, Top=194, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_MFCDSED4"

  func oMFCDSED4_3_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCDSED4_3_35.ecpDrop(oSource)
    this.Parent.oContained.link_3_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDSED4_3_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oMFCDSED4_3_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Codici sede INPS",'',this.parent.oContained
  endproc
  proc oMFCDSED4_3_35.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_MFCDSED4
     i_obj.ecpSave()
  endproc

  add object oMFCCONT4_3_36 as StdField with uid="ZYWRGKWSCK",rtseq=50,rtrep=.f.,;
    cFormVar = "w_MFCCONT4", cQueryName = "MFCCONT4",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo INPS",;
    HelpContextID = 145577466,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=192, Top=194, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_INPS", cZoomOnZoom="GSCG_ACS", oKey_1_1="CS_CAUSA", oKey_1_2="this.w_MFCCONT4"

  func oMFCCONT4_3_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFCCONT4_3_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCONT4_3_36.ecpDrop(oSource)
    this.Parent.oContained.link_3_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCONT4_3_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_INPS','*','CS_CAUSA',cp_AbsName(this.parent,'oMFCCONT4_3_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACS',"Causali contributo INPS",'',this.parent.oContained
  endproc
  proc oMFCCONT4_3_36.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CS_CAUSA=this.parent.oContained.w_MFCCONT4
     i_obj.ecpSave()
  endproc

  add object oMFMINPS4_3_37 as StdField with uid="YUDKXKBRDK",rtseq=51,rtrep=.f.,;
    cFormVar = "w_MFMINPS4", cQueryName = "MFMINPS4",;
    bObbl = .f. , nPag = 3, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Matricola/codice INPS/filiale azienda",;
    HelpContextID = 89917958,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=274, Top=194, InputMask=replicate('X',17)

  func oMFMINPS4_3_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  add object oMFDAMES4_3_39 as StdField with uid="IUDRVUKOUP",rtseq=53,rtrep=.f.,;
    cFormVar = "w_MFDAMES4", cQueryName = "MFDAMES4",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7641606,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=475, Top=194, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFDAMES4_3_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFDAMES4_3_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP4A  Or  (!empty(.w_MFDAMES4) and 0<val(.w_MFDAMES4) and val(.w_MFDAMES4)<13))
    endwith
    return bRes
  endfunc

  add object oMFDAANN4_3_41 as StdField with uid="HJXJWHWGKV",rtseq=55,rtrep=.f.,;
    cFormVar = "w_MFDAANN4", cQueryName = "MFDAANN4",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 137665030,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=502, Top=194, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFDAANN4_3_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFDAANN4_3_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP4A  Or  (!empty(.w_MFDAANN4) and (val(.w_MFDAANN4)>=1996  and val(.w_MFDAANN4)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMF_AMES4_3_42 as StdField with uid="RGSVDGJKVT",rtseq=56,rtrep=.f.,;
    cFormVar = "w_MF_AMES4", cQueryName = "MF_AMES4",nZero=2,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 7531014,;
   bGlobalFont=.t.,;
    Height=21, Width=26, Left=557, Top=194, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_AMES4_3_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMF_AMES4_3_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP4 Or (!empty(.w_MF_AMES4)  and 0<val(.w_MF_AMES4) and val(.w_MF_AMES4)<13))
    endwith
    return bRes
  endfunc

  add object oMFAN4_3_43 as StdField with uid="CFHZNDFFLJ",rtseq=57,rtrep=.f.,;
    cFormVar = "w_MFAN4", cQueryName = "MFAN4",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione INPS: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 151532998,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=586, Top=194, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFAN4_3_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  func oMFAN4_3_43.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_APP4 Or (val(.w_MFAN4)<=2050 and(val(.w_MFDAANN4)<val(.w_MFAN4)or .w_MFDAANN4=.w_MFAN4 and val(.w_MFDAMES4)<=val(.w_MF_AMES4))))
    endwith
    return bRes
  endfunc

  add object oMFIMPSD4_3_44 as StdField with uid="MNPGVFYFDQ",rtseq=58,rtrep=.f.,;
    cFormVar = "w_MFIMPSD4", cQueryName = "MFIMPSD4",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 231192058,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=113, Top=314, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSD4_3_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  add object oMFIMPSC4_3_45 as StdField with uid="UYJTGTJJGS",rtseq=59,rtrep=.f.,;
    cFormVar = "w_MFIMPSC4", cQueryName = "MFIMPSC4",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37243398,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=277, Top=314, cSayPict="'@Z '+ v_GV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMPSC4_3_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDSED4))
    endwith
   endif
  endfunc

  add object oMFTOTDPS_3_55 as StdField with uid="XAUJQDXFLK",rtseq=60,rtrep=.f.,;
    cFormVar = "w_MFTOTDPS", cQueryName = "MFTOTDPS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 16095719,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=113, Top=340, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFTOTCPS_3_56 as StdField with uid="MGBYMLHXPA",rtseq=61,rtrep=.f.,;
    cFormVar = "w_MFTOTCPS", cQueryName = "MFTOTCPS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 32872935,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=277, Top=340, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFSALDPS_3_57 as StdField with uid="JXAPRSKVXL",rtseq=62,rtrep=.f.,;
    cFormVar = "w_MFSALDPS", cQueryName = "MFSALDPS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25405927,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=421, Top=340, cSayPict="v_PV(78)", cGetPict="v_GV(78)"

  add object oMESE_3_63 as StdField with uid="BKZRGNZFUB",rtseq=63,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=375, Top=26, InputMask=replicate('X',2)

  add object oANNO_3_64 as StdField with uid="MJMYMEQJVT",rtseq=64,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=400, Top=26, InputMask=replicate('X',4)


  add object oBtn_3_67 as StdButton with uid="LBKQCEWXSG",left=731, top=349, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per visualizzare le distinte versamento di tipo I.N.P.S.";
    , HelpContextID = 182357498;
    , tabstop=.f., caption='\<Distinte';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_67.Click()
      with this.Parent.oContained
        GSRI_BVI(this.Parent.oContained,"IN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_67.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_Rite<>'S')
     endwith
    endif
  endfunc


  add object oObj_3_68 as cp_calclbl with uid="XFLZEJFPVI",left=113, top=88, width=62,height=35,;
    caption='Codice sede',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=3;
    , HelpContextID = 249804567


  add object oObj_3_69 as cp_calclbl with uid="YEBTDQCIIE",left=192, top=88, width=60,height=35,;
    caption='Causale contributo',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=3;
    , HelpContextID = 123970517

  add object oStr_3_46 as StdString with uid="IYEELTZMSP",Visible=.t., Left=271, Top=89,;
    Alignment=2, Width=156, Height=15,;
    Caption="Matricola INPS/codice INPS/"  ;
  , bGlobalFont=.t.

  add object oStr_3_47 as StdString with uid="YEXOAXJKLC",Visible=.t., Left=299, Top=105,;
    Alignment=2, Width=104, Height=15,;
    Caption="Filiale azienda"  ;
  , bGlobalFont=.t.

  add object oStr_3_48 as StdString with uid="MFUFBDQZKN",Visible=.t., Left=488, Top=89,;
    Alignment=2, Width=123, Height=15,;
    Caption="Periodo di riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_3_49 as StdString with uid="BOQOXBLNLK",Visible=.t., Left=459, Top=107,;
    Alignment=0, Width=85, Height=15,;
    Caption="da mm/aaaa"  ;
  , bGlobalFont=.t.

  add object oStr_3_50 as StdString with uid="YZBSWXWFTX",Visible=.t., Left=550, Top=107,;
    Alignment=0, Width=67, Height=15,;
    Caption="a mm/aaaa"  ;
  , bGlobalFont=.t.

  add object oStr_3_51 as StdString with uid="SYTXHRSHZV",Visible=.t., Left=113, Top=231,;
    Alignment=0, Width=154, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_3_52 as StdString with uid="HOACILSNNJ",Visible=.t., Left=275, Top=231,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_3_53 as StdString with uid="LGLKGDJLHM",Visible=.t., Left=17, Top=340,;
    Alignment=1, Width=94, Height=15,;
    Caption="Totale C"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_54 as StdString with uid="TZCTEEYMBI",Visible=.t., Left=256, Top=341,;
    Alignment=0, Width=15, Height=14,;
    Caption="D"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_58 as StdString with uid="XBSXKBBESY",Visible=.t., Left=421, Top=316,;
    Alignment=2, Width=132, Height=15,;
    Caption="Saldo (C-D)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_59 as StdString with uid="ASFKUCRSCQ",Visible=.t., Left=12, Top=26,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_61 as StdString with uid="KPNHKKCGVH",Visible=.t., Left=13, Top=52,;
    Alignment=0, Width=82, Height=18,;
    Caption="Sezione INPS"  ;
  , bGlobalFont=.t.

  add object oStr_3_62 as StdString with uid="IZXDXVWWJG",Visible=.t., Left=244, Top=28,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oBox_3_60 as StdBox with uid="DBOPZLDODS",left=13, top=73, width=846,height=1
enddefine
define class tgscg_aopPag4 as StdContainer
  Width  = 869
  height = 441
  stdWidth  = 869
  stdheight = 441
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_4_1 as StdField with uid="WUJQAZLJAD",rtseq=67,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 4, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=90, Left=138, Top=8, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFCODRE1_4_2 as StdField with uid="AIZVRMHYSY",rtseq=68,rtrep=.f.,;
    cFormVar = "w_MFCODRE1", cQueryName = "MFCODRE1",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 201938423,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=41, Left=26, Top=99, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE1"

  func oMFCODRE1_4_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE1_4_2.ecpDrop(oSource)
    this.Parent.oContained.link_4_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE1_4_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE1_4_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regioni",'',this.parent.oContained
  endproc
  proc oMFCODRE1_4_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE1
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE1_4_3 as StdField with uid="EOPPRNCXED",rtseq=69,rtrep=.f.,;
    cFormVar = "w_MFTRIRE1", cQueryName = "MFTRIRE1",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'regione'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 207447543,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=87, Top=99, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE1"

  func oMFTRIRE1_4_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  func oMFTRIRE1_4_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE1_4_3.ecpDrop(oSource)
    this.Parent.oContained.link_4_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE1_4_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE1_4_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE1_4_3.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE1
     i_obj.ecpSave()
  endproc

  add object oMFRATRE1_4_4 as StdField with uid="WJSZNHKTPL",rtseq=70,rtrep=.f.,;
    cFormVar = "w_MFRATRE1", cQueryName = "MFRATRE1",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 217859575,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=161, Top=99, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE1_4_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  func oMFRATRE1_4_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE1) or left(.w_MFRATRE1,2)<=right(.w_MFRATRE1,2) and len(alltrim(.w_MFRATRE1))=4)
    endwith
    return bRes
  endfunc

  add object oMFMESER1_4_5 as StdField with uid="WCZVQNMEYJ",rtseq=71,rtrep=.f.,;
    cFormVar = "w_MFMESER1", cQueryName = "MFMESER1",nZero=2,;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o dato obbligatorio",;
    ToolTipText = "Mese di riferimento (mm)",;
    HelpContextID = 1051145,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=243, Top=99, InputMask=replicate('X',2)

  func oMFMESER1_4_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  func oMFMESER1_4_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MFMESER1>='01' and .w_MFMESER1<='12' or (empty(.w_MFMESER1)  and .w_CHKOBBME1<>'S'))
    endwith
    return bRes
  endfunc

  add object oMFANNRE1_4_6 as StdField with uid="MUARMFNAEO",rtseq=72,rtrep=.f.,;
    cFormVar = "w_MFANNRE1", cQueryName = "MFANNRE1",nZero=4,;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 212350455,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=316, Top=99, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE1_4_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  func oMFANNRE1_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE1) and (val(.w_MFANNRE1)>=1996  and val(.w_MFANNRE1)<=2050  or .w_MFANNRE1='0000'))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE1_4_7 as StdField with uid="DHWEMFLHFR",rtseq=73,rtrep=.f.,;
    cFormVar = "w_MFIMDRE1", cQueryName = "MFIMDRE1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 201831927,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=379, Top=99, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDRE1_4_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  add object oMFIMCRE1_4_8 as StdField with uid="UKNUQBICLX",rtseq=74,rtrep=.f.,;
    cFormVar = "w_MFIMCRE1", cQueryName = "MFIMCRE1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 200783351,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=541, Top=99, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCRE1_4_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE1))
    endwith
   endif
  endfunc

  add object oMFCODRE2_4_12 as StdField with uid="ZYXFOZVGTH",rtseq=75,rtrep=.f.,;
    cFormVar = "w_MFCODRE2", cQueryName = "MFCODRE2",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 201938424,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=26, Top=119, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE2"

  func oMFCODRE2_4_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE2_4_12.ecpDrop(oSource)
    this.Parent.oContained.link_4_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE2_4_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE2_4_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regioni",'',this.parent.oContained
  endproc
  proc oMFCODRE2_4_12.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE2
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE2_4_13 as StdField with uid="FKBLLTEVWV",rtseq=76,rtrep=.f.,;
    cFormVar = "w_MFTRIRE2", cQueryName = "MFTRIRE2",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'regione'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 207447544,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=87, Top=119, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE2"

  func oMFTRIRE2_4_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  func oMFTRIRE2_4_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE2_4_13.ecpDrop(oSource)
    this.Parent.oContained.link_4_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE2_4_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE2_4_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE2_4_13.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE2
     i_obj.ecpSave()
  endproc

  add object oMFRATRE2_4_14 as StdField with uid="UQVLTAPSHS",rtseq=77,rtrep=.f.,;
    cFormVar = "w_MFRATRE2", cQueryName = "MFRATRE2",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 217859576,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=161, Top=119, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE2_4_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  func oMFRATRE2_4_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE2) or left(.w_MFRATRE2,2)<=right(.w_MFRATRE2,2) and len(alltrim(.w_MFRATRE2))=4)
    endwith
    return bRes
  endfunc

  add object oMFMESER2_4_15 as StdField with uid="ZBFOIGJQED",rtseq=78,rtrep=.f.,;
    cFormVar = "w_MFMESER2", cQueryName = "MFMESER2",nZero=2,;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o dato obbligatorio",;
    ToolTipText = "Mese di riferimento (mm)",;
    HelpContextID = 1051144,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=243, Top=119, InputMask=replicate('X',2)

  func oMFMESER2_4_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  func oMFMESER2_4_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MFMESER2>='01' and .w_MFMESER2<='12' or (empty(.w_MFMESER2)  and .w_CHKOBBME2<>'S'))
    endwith
    return bRes
  endfunc

  add object oMFANNRE2_4_16 as StdField with uid="HEIMJNJWRR",rtseq=79,rtrep=.f.,;
    cFormVar = "w_MFANNRE2", cQueryName = "MFANNRE2",nZero=4,;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 212350456,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=316, Top=119, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE2_4_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  func oMFANNRE2_4_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE2) and (val(.w_MFANNRE2)>=1996  and val(.w_MFANNRE2)<=2050   or .w_MFANNRE2='0000'))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE2_4_17 as StdField with uid="XOVRSESBNM",rtseq=80,rtrep=.f.,;
    cFormVar = "w_MFIMDRE2", cQueryName = "MFIMDRE2",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 201831928,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=379, Top=119, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDRE2_4_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  add object oMFIMCRE2_4_18 as StdField with uid="RCLUETTAKO",rtseq=81,rtrep=.f.,;
    cFormVar = "w_MFIMCRE2", cQueryName = "MFIMCRE2",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 200783352,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=541, Top=119, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCRE2_4_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE2))
    endwith
   endif
  endfunc

  add object oMFCODRE3_4_19 as StdField with uid="ZVBRNRSCQE",rtseq=82,rtrep=.f.,;
    cFormVar = "w_MFCODRE3", cQueryName = "MFCODRE3",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 201938425,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=26, Top=139, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE3"

  func oMFCODRE3_4_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE3_4_19.ecpDrop(oSource)
    this.Parent.oContained.link_4_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE3_4_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE3_4_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regioni",'',this.parent.oContained
  endproc
  proc oMFCODRE3_4_19.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE3
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE3_4_20 as StdField with uid="YCEJPTYMRK",rtseq=83,rtrep=.f.,;
    cFormVar = "w_MFTRIRE3", cQueryName = "MFTRIRE3",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'regione'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 207447545,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=87, Top=139, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE3"

  func oMFTRIRE3_4_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  func oMFTRIRE3_4_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE3_4_20.ecpDrop(oSource)
    this.Parent.oContained.link_4_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE3_4_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE3_4_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE3_4_20.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE3
     i_obj.ecpSave()
  endproc

  add object oMFRATRE3_4_21 as StdField with uid="TWMNLHYLWV",rtseq=84,rtrep=.f.,;
    cFormVar = "w_MFRATRE3", cQueryName = "MFRATRE3",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 217859577,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=161, Top=139, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE3_4_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  func oMFRATRE3_4_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE3) or left(.w_MFRATRE3,2)<=right(.w_MFRATRE3,2) and len(alltrim(.w_MFRATRE3))=4)
    endwith
    return bRes
  endfunc

  add object oMFMESER3_4_22 as StdField with uid="MWMXAZPHPH",rtseq=85,rtrep=.f.,;
    cFormVar = "w_MFMESER3", cQueryName = "MFMESER3",nZero=2,;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o dato obbligatorio",;
    ToolTipText = "Mese di riferimento (mm)",;
    HelpContextID = 1051143,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=243, Top=139, InputMask=replicate('X',2)

  func oMFMESER3_4_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  func oMFMESER3_4_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MFMESER3>='01' and .w_MFMESER3<='12' or (empty(.w_MFMESER3)  and .w_CHKOBBME3<>'S'))
    endwith
    return bRes
  endfunc

  add object oMFANNRE3_4_23 as StdField with uid="SUHDLNHBSQ",rtseq=86,rtrep=.f.,;
    cFormVar = "w_MFANNRE3", cQueryName = "MFANNRE3",nZero=4,;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 212350457,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=316, Top=139, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE3_4_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  func oMFANNRE3_4_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE3) and (val(.w_MFANNRE3)>=1996  and val(.w_MFANNRE3)<=2050  or .w_MFANNRE3='0000'))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE3_4_24 as StdField with uid="CYOULTQZVW",rtseq=87,rtrep=.f.,;
    cFormVar = "w_MFIMDRE3", cQueryName = "MFIMDRE3",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 201831929,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=379, Top=139, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDRE3_4_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  add object oMFIMCRE3_4_25 as StdField with uid="QQOFTRRZLZ",rtseq=88,rtrep=.f.,;
    cFormVar = "w_MFIMCRE3", cQueryName = "MFIMCRE3",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 200783353,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=541, Top=139, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCRE3_4_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE3))
    endwith
   endif
  endfunc

  add object oMFCODRE4_4_26 as StdField with uid="MBZNNKESOD",rtseq=89,rtrep=.f.,;
    cFormVar = "w_MFCODRE4", cQueryName = "MFCODRE4",;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice della regione",;
    HelpContextID = 201938426,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=26, Top=159, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="REG_PROV", cZoomOnZoom="GSCG_ARP", oKey_1_1="RPCODICE", oKey_1_2="this.w_MFCODRE4"

  func oMFCODRE4_4_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCODRE4_4_26.ecpDrop(oSource)
    this.Parent.oContained.link_4_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCODRE4_4_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REG_PROV','*','RPCODICE',cp_AbsName(this.parent,'oMFCODRE4_4_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ARP',"Codici regioni",'',this.parent.oContained
  endproc
  proc oMFCODRE4_4_26.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ARP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RPCODICE=this.parent.oContained.w_MFCODRE4
     i_obj.ecpSave()
  endproc

  add object oMFTRIRE4_4_27 as StdField with uid="MJAPDUZSJV",rtseq=90,rtrep=.f.,;
    cFormVar = "w_MFTRIRE4", cQueryName = "MFTRIRE4",;
    bObbl = .t. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un codice tributo di tipo 'regione'",;
    ToolTipText = "Codice tributo",;
    HelpContextID = 207447546,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=87, Top=159, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_MFTRIRE4"

  func oMFTRIRE4_4_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  func oMFTRIRE4_4_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFTRIRE4_4_27.ecpDrop(oSource)
    this.Parent.oContained.link_4_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFTRIRE4_4_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oMFTRIRE4_4_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributo",'GSCG1ZTR.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oMFTRIRE4_4_27.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_MFTRIRE4
     i_obj.ecpSave()
  endproc

  add object oMFRATRE4_4_28 as StdField with uid="NDAJMIEPRZ",rtseq=91,rtrep=.f.,;
    cFormVar = "w_MFRATRE4", cQueryName = "MFRATRE4",;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: numero rata o totale rate errato",;
    ToolTipText = "Numero rata (nn) + totale rate (nn)",;
    HelpContextID = 217859578,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=161, Top=159, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFRATRE4_4_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  func oMFRATRE4_4_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_MFRATRE4) or left(.w_MFRATRE4,2)<=right(.w_MFRATRE4,2) and len(alltrim(.w_MFRATRE4))=4)
    endwith
    return bRes
  endfunc

  add object oMFMESER4_4_29 as StdField with uid="ZAVBIIVHMK",rtseq=92,rtrep=.f.,;
    cFormVar = "w_MFMESER4", cQueryName = "MFMESER4",nZero=2,;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o dato obbligatorio",;
    ToolTipText = "Mese di riferimento (mm)",;
    HelpContextID = 1051142,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=243, Top=159, InputMask=replicate('X',2)

  func oMFMESER4_4_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  func oMFMESER4_4_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MFMESER4>='01' and .w_MFMESER4<='12' or (empty(.w_MFMESER4)  and .w_CHKOBBME4<>'S'))
    endwith
    return bRes
  endfunc

  add object oMFANNRE4_4_30 as StdField with uid="BIEFIFIRQX",rtseq=93,rtrep=.f.,;
    cFormVar = "w_MFANNRE4", cQueryName = "MFANNRE4",nZero=4,;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione regioni: selezionare un anno compreso fra 1996 e 2050. Se il tributo non prevede l'indicazione dell'anno digitare <0000>",;
    ToolTipText = "Anno di riferimento (aaaa)",;
    HelpContextID = 212350458,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=316, Top=159, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANNRE4_4_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  func oMFANNRE4_4_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (!empty(.w_MFANNRE4) and (val(.w_MFANNRE4)>=1996  and val(.w_MFANNRE4)<=2050   or  .w_MFANNRE4='0000'))
    endwith
    return bRes
  endfunc

  add object oMFIMDRE4_4_31 as StdField with uid="BCINHDFTDU",rtseq=94,rtrep=.f.,;
    cFormVar = "w_MFIMDRE4", cQueryName = "MFIMDRE4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 201831930,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=379, Top=159, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDRE4_4_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  add object oMFIMCRE4_4_32 as StdField with uid="GJHJZPGKWU",rtseq=95,rtrep=.f.,;
    cFormVar = "w_MFIMCRE4", cQueryName = "MFIMCRE4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 200783354,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=541, Top=159, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCRE4_4_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCODRE4))
    endwith
   endif
  endfunc

  add object oMFTOTDRE_4_36 as StdField with uid="RWVYQDEOZY",rtseq=96,rtrep=.f.,;
    cFormVar = "w_MFTOTDRE", cQueryName = "MFTOTDRE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 16095733,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=379, Top=187, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFTOTCRE_4_37 as StdField with uid="NEFFOASBPH",rtseq=97,rtrep=.f.,;
    cFormVar = "w_MFTOTCRE", cQueryName = "MFTOTCRE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 32872949,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=541, Top=187, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFSALDRE_4_38 as StdField with uid="ZPSTXWEYRU",rtseq=98,rtrep=.f.,;
    cFormVar = "w_MFSALDRE", cQueryName = "MFSALDRE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 25405941,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=683, Top=187, cSayPict="v_PV(78)", cGetPict="v_GV(78)"

  add object oMESE_4_43 as StdField with uid="QDHEDGHHIW",rtseq=99,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=375, Top=9, InputMask=replicate('X',2)

  add object oANNO_4_44 as StdField with uid="EBJMOQWTXY",rtseq=100,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=400, Top=9, InputMask=replicate('X',4)


  add object oLinkPC_4_45 as stdDynamicChildContainer with uid="IJXOOZHGBN",left=3, top=213, width=859, height=200, bOnScreen=.t.;



  add object oObj_4_52 as cp_calclbl with uid="GKGLWDZFVF",left=307, top=61, width=64,height=35,;
    caption='Anno di riferimento',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=4;
    , HelpContextID = 58759029


  add object oObj_4_53 as cp_calclbl with uid="PKUHVTUSLK",left=234, top=61, width=64,height=35,;
    caption='Mese di riferimento',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=4;
    , HelpContextID = 59396021


  add object oObj_4_54 as cp_calclbl with uid="ZUOHGMEAFA",left=85, top=61, width=52,height=35,;
    caption='Cod. tributo',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=4;
    , HelpContextID = 203508143


  add object oObj_4_55 as cp_calclbl with uid="KSGOLPXVPB",left=26, top=61, width=47,height=35,;
    caption='Codice regione',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=4;
    , HelpContextID = 225151976

  add object oStr_4_9 as StdString with uid="YQGQUJLBET",Visible=.t., Left=147, Top=78,;
    Alignment=0, Width=89, Height=15,;
    Caption="Rateazione"  ;
  , bGlobalFont=.t.

  add object oStr_4_10 as StdString with uid="MVBKBDZKBM",Visible=.t., Left=378, Top=76,;
    Alignment=0, Width=154, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_4_11 as StdString with uid="NHHSLKGDGU",Visible=.t., Left=539, Top=76,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_4_33 as StdString with uid="KEBVMVBZNJ",Visible=.t., Left=280, Top=189,;
    Alignment=1, Width=93, Height=15,;
    Caption="Totale E"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_34 as StdString with uid="FIWKHDGIJY",Visible=.t., Left=683, Top=162,;
    Alignment=2, Width=132, Height=15,;
    Caption="Saldo (E-F)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_35 as StdString with uid="WDIGBVHLVI",Visible=.t., Left=520, Top=188,;
    Alignment=0, Width=18, Height=16,;
    Caption="F"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_40 as StdString with uid="WRQXUQEJZJ",Visible=.t., Left=10, Top=9,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_41 as StdString with uid="XBBOUQJYEG",Visible=.t., Left=244, Top=12,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_42 as StdString with uid="DORQAGMARM",Visible=.t., Left=6, Top=31,;
    Alignment=0, Width=109, Height=18,;
    Caption="Sezione regioni"  ;
  , bGlobalFont=.t.

  add object oBox_4_39 as StdBox with uid="KHJPYHBFQV",left=6, top=55, width=860,height=1
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_aig",lower(this.oContained.GSCG_AIG.class))=0
        this.oContained.GSCG_AIG.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgscg_aopPag5 as StdContainer
  Width  = 869
  height = 441
  stdWidth  = 869
  stdheight = 441
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_5_1 as StdField with uid="UHIVYKYYLY",rtseq=101,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 5, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=90, Left=133, Top=24, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFSINAI1_5_2 as StdField with uid="VJCJNRLRZM",rtseq=102,rtrep=.f.,;
    cFormVar = "w_MFSINAI1", cQueryName = "MFSINAI1",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INAIL",;
    HelpContextID = 195319287,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=9, Top=173, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SE_INAIL", cZoomOnZoom="GSCG_ASI", oKey_1_1="SICODICE", oKey_1_2="this.w_MFSINAI1"

  func oMFSINAI1_5_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFSINAI1_5_2.ecpDrop(oSource)
    this.Parent.oContained.link_5_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFSINAI1_5_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SE_INAIL','*','SICODICE',cp_AbsName(this.parent,'oMFSINAI1_5_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ASI',"Codici sede INAIL",'',this.parent.oContained
  endproc
  proc oMFSINAI1_5_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ASI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SICODICE=this.parent.oContained.w_MFSINAI1
     i_obj.ecpSave()
  endproc

  add object oMF_NPOS1_5_3 as StdField with uid="IOZFIFMCFH",rtseq=103,rtrep=.f.,;
    cFormVar = "w_MF_NPOS1", cQueryName = "MF_NPOS1",nZero=8,;
    bObbl = .f. , nPag = 5, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Numero posizione assicurativa",;
    HelpContextID = 104196617,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=86, Top=173, cSayPict='"99999999"', cGetPict='"99999999"', InputMask=replicate('X',8)

  func oMF_NPOS1_5_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMF_PACC1_5_4 as StdField with uid="ZRBNXSIDVP",rtseq=104,rtrep=.f.,;
    cFormVar = "w_MF_PACC1", cQueryName = "MF_PACC1",nZero=2,;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "C.C. posizione assicurativa",;
    HelpContextID = 52685321,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=158, Top=173, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_PACC1_5_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMF_NRIF1_5_5 as StdField with uid="MNCHMZYNTX",rtseq=105,rtrep=.f.,;
    cFormVar = "w_MF_NRIF1", cQueryName = "MF_NRIF1",nZero=6,;
    bObbl = .f. , nPag = 5, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero di riferimento",;
    HelpContextID = 65672695,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=204, Top=173, cSayPict='"999999"', cGetPict='"999999"', InputMask=replicate('X',6)

  func oMF_NRIF1_5_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFCAUSA1_5_6 as StdField with uid="UYLFQZKBVP",rtseq=106,rtrep=.f.,;
    cFormVar = "w_MFCAUSA1", cQueryName = "MFCAUSA1",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Causale",;
    HelpContextID = 32811529,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=293, Top=173, cSayPict='repl("!",1)', cGetPict='repl("!",1)', InputMask=replicate('X',2)

  func oMFCAUSA1_5_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFIMDIL1_5_7 as StdField with uid="KHMXTBZGND",rtseq=107,rtrep=.f.,;
    cFormVar = "w_MFIMDIL1", cQueryName = "MFIMDIL1",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 217598473,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=335, Top=173, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDIL1_5_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFIMCIL1_5_8 as StdField with uid="LCWCGXKFZS",rtseq=108,rtrep=.f.,;
    cFormVar = "w_MFIMCIL1", cQueryName = "MFIMCIL1",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 218647049,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=498, Top=173, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCIL1_5_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI1))
    endwith
   endif
  endfunc

  add object oMFSINAI2_5_9 as StdField with uid="CRJUFFVUOV",rtseq=109,rtrep=.f.,;
    cFormVar = "w_MFSINAI2", cQueryName = "MFSINAI2",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INAIL",;
    HelpContextID = 195319288,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=9, Top=193, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SE_INAIL", cZoomOnZoom="GSCG_ASI", oKey_1_1="SICODICE", oKey_1_2="this.w_MFSINAI2"

  func oMFSINAI2_5_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFSINAI2_5_9.ecpDrop(oSource)
    this.Parent.oContained.link_5_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFSINAI2_5_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SE_INAIL','*','SICODICE',cp_AbsName(this.parent,'oMFSINAI2_5_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ASI',"Codici sede INAIL",'',this.parent.oContained
  endproc
  proc oMFSINAI2_5_9.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ASI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SICODICE=this.parent.oContained.w_MFSINAI2
     i_obj.ecpSave()
  endproc

  add object oMF_NPOS2_5_10 as StdField with uid="FUEQFYRIPH",rtseq=110,rtrep=.f.,;
    cFormVar = "w_MF_NPOS2", cQueryName = "MF_NPOS2",nZero=8,;
    bObbl = .f. , nPag = 5, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Numero posizione assicurativa",;
    HelpContextID = 104196616,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=86, Top=193, cSayPict='"99999999"', cGetPict='"99999999"', InputMask=replicate('X',8)

  func oMF_NPOS2_5_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMF_PACC2_5_11 as StdField with uid="TUSBHCGAVC",rtseq=111,rtrep=.f.,;
    cFormVar = "w_MF_PACC2", cQueryName = "MF_PACC2",nZero=2,;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "C.C. posizione assicurativa",;
    HelpContextID = 52685320,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=158, Top=193, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_PACC2_5_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMF_NRIF2_5_12 as StdField with uid="DJFDGNQMDU",rtseq=112,rtrep=.f.,;
    cFormVar = "w_MF_NRIF2", cQueryName = "MF_NRIF2",nZero=6,;
    bObbl = .f. , nPag = 5, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero di riferimento",;
    HelpContextID = 65672696,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=204, Top=193, cSayPict='"999999"', cGetPict='"999999"', InputMask=replicate('X',6)

  func oMF_NRIF2_5_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFCAUSA2_5_13 as StdField with uid="HNWHAKLLNT",rtseq=113,rtrep=.f.,;
    cFormVar = "w_MFCAUSA2", cQueryName = "MFCAUSA2",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Causale",;
    HelpContextID = 32811528,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=293, Top=193, cSayPict='repl("!",1)', cGetPict='repl("!",1)', InputMask=replicate('X',2)

  func oMFCAUSA2_5_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFIMDIL2_5_14 as StdField with uid="KEXJJEVGQG",rtseq=114,rtrep=.f.,;
    cFormVar = "w_MFIMDIL2", cQueryName = "MFIMDIL2",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 217598472,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=335, Top=193, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDIL2_5_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFIMCIL2_5_15 as StdField with uid="HAECOJYPAK",rtseq=115,rtrep=.f.,;
    cFormVar = "w_MFIMCIL2", cQueryName = "MFIMCIL2",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 218647048,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=498, Top=193, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCIL2_5_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI2))
    endwith
   endif
  endfunc

  add object oMFSINAI3_5_16 as StdField with uid="XBUILCQVSF",rtseq=116,rtrep=.f.,;
    cFormVar = "w_MFSINAI3", cQueryName = "MFSINAI3",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INAIL",;
    HelpContextID = 195319289,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=9, Top=213, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SE_INAIL", cZoomOnZoom="GSCG_ASI", oKey_1_1="SICODICE", oKey_1_2="this.w_MFSINAI3"

  func oMFSINAI3_5_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFSINAI3_5_16.ecpDrop(oSource)
    this.Parent.oContained.link_5_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFSINAI3_5_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SE_INAIL','*','SICODICE',cp_AbsName(this.parent,'oMFSINAI3_5_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ASI',"Codici sede INAIL",'',this.parent.oContained
  endproc
  proc oMFSINAI3_5_16.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ASI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SICODICE=this.parent.oContained.w_MFSINAI3
     i_obj.ecpSave()
  endproc

  add object oMF_NPOS3_5_17 as StdField with uid="KYNIYEUWFN",rtseq=117,rtrep=.f.,;
    cFormVar = "w_MF_NPOS3", cQueryName = "MF_NPOS3",nZero=8,;
    bObbl = .f. , nPag = 5, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Numero posizione assicurativa",;
    HelpContextID = 104196615,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=86, Top=213, cSayPict='"99999999"', cGetPict='"99999999"', InputMask=replicate('X',8)

  func oMF_NPOS3_5_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMF_PACC3_5_18 as StdField with uid="RMJFRMESCX",rtseq=118,rtrep=.f.,;
    cFormVar = "w_MF_PACC3", cQueryName = "MF_PACC3",nZero=2,;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "C.C. posizione assicurativa",;
    HelpContextID = 52685319,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=158, Top=213, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMF_PACC3_5_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMF_NRIF3_5_19 as StdField with uid="PAMPHMQGFW",rtseq=119,rtrep=.f.,;
    cFormVar = "w_MF_NRIF3", cQueryName = "MF_NRIF3",nZero=6,;
    bObbl = .f. , nPag = 5, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero di riferimento",;
    HelpContextID = 65672697,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=204, Top=213, cSayPict='"999999"', cGetPict='"999999"', InputMask=replicate('X',6)

  func oMF_NRIF3_5_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFCAUSA3_5_20 as StdField with uid="BWTVBLXRGC",rtseq=120,rtrep=.f.,;
    cFormVar = "w_MFCAUSA3", cQueryName = "MFCAUSA3",;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Causale",;
    HelpContextID = 32811527,;
   bGlobalFont=.t.,;
    Height=21, Width=24, Left=293, Top=213, cSayPict='repl("!",1)', cGetPict='repl("!",1)', InputMask=replicate('X',2)

  func oMFCAUSA3_5_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFIMDIL3_5_21 as StdField with uid="ZSLZCWMSTL",rtseq=121,rtrep=.f.,;
    cFormVar = "w_MFIMDIL3", cQueryName = "MFIMDIL3",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 217598471,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=335, Top=213, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDIL3_5_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFIMCIL3_5_22 as StdField with uid="RWHHSNNRDK",rtseq=122,rtrep=.f.,;
    cFormVar = "w_MFIMCIL3", cQueryName = "MFIMCIL3",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 218647047,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=498, Top=213, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCIL3_5_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty (.w_MFSINAI3))
    endwith
   endif
  endfunc

  add object oMFTDINAI_5_30 as StdField with uid="PSBGPKLIAA",rtseq=123,rtrep=.f.,;
    cFormVar = "w_MFTDINAI", cQueryName = "MFTDINAI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 129014257,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=335, Top=242, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFTCINAI_5_31 as StdField with uid="XTGTLEVZXZ",rtseq=124,rtrep=.f.,;
    cFormVar = "w_MFTCINAI", cQueryName = "MFTCINAI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 129079793,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=498, Top=242, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFSALINA_5_32 as StdField with uid="DZWWWGTXFX",rtseq=125,rtrep=.f.,;
    cFormVar = "w_MFSALINA", cQueryName = "MFSALINA",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 209955321,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=637, Top=242, cSayPict="v_PV(78)", cGetPict="v_GV(78)"

  add object oMESE_5_40 as StdField with uid="MLVBGKPOHZ",rtseq=126,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=371, Top=25, InputMask=replicate('X',2)

  add object oANNO_5_41 as StdField with uid="JTXVHPAOPB",rtseq=127,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=396, Top=25, InputMask=replicate('X',4)


  add object oObj_5_42 as cp_calclbl with uid="EXHYHMEIKF",left=204, top=135, width=73,height=35,;
    caption='Numero di riferimento',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=5;
    , HelpContextID = 255703043

  add object oStr_5_23 as StdString with uid="TZTHCVPSZI",Visible=.t., Left=86, Top=151,;
    Alignment=0, Width=51, Height=15,;
    Caption="Numero"  ;
  , bGlobalFont=.t.

  add object oStr_5_24 as StdString with uid="FGFSRZKDQK",Visible=.t., Left=158, Top=153,;
    Alignment=0, Width=24, Height=15,;
    Caption="C.C."  ;
  , bGlobalFont=.t.

  add object oStr_5_25 as StdString with uid="XMJKQNQCQQ",Visible=.t., Left=335, Top=153,;
    Alignment=0, Width=154, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_5_26 as StdString with uid="QPZQHUEURM",Visible=.t., Left=498, Top=153,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_5_27 as StdString with uid="YCITJFFMQZ",Visible=.t., Left=7, Top=151,;
    Alignment=0, Width=63, Height=15,;
    Caption="Cod.sede"  ;
  , bGlobalFont=.t.

  add object oStr_5_28 as StdString with uid="DUQPKICCWN",Visible=.t., Left=86, Top=135,;
    Alignment=0, Width=109, Height=15,;
    Caption="Posiz.assicurativa"  ;
  , bGlobalFont=.t.

  add object oStr_5_29 as StdString with uid="GWKFEEEWGD",Visible=.t., Left=285, Top=153,;
    Alignment=2, Width=45, Height=15,;
    Caption="Caus."  ;
  , bGlobalFont=.t.

  add object oStr_5_33 as StdString with uid="CAXNCWDVNP",Visible=.t., Left=235, Top=244,;
    Alignment=1, Width=89, Height=15,;
    Caption="Totale I"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_34 as StdString with uid="YQYWHXKWQX",Visible=.t., Left=651, Top=220,;
    Alignment=0, Width=73, Height=15,;
    Caption="Saldo (I-L)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_35 as StdString with uid="HTBHHGCOXH",Visible=.t., Left=476, Top=244,;
    Alignment=0, Width=20, Height=15,;
    Caption="L"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_36 as StdString with uid="UICRRZTZSD",Visible=.t., Left=12, Top=63,;
    Alignment=0, Width=311, Height=18,;
    Caption="Altri enti previdenziali ed assicurativi - INAIL"  ;
  , bGlobalFont=.t.

  add object oStr_5_38 as StdString with uid="VXPRYYUHZU",Visible=.t., Left=5, Top=25,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_39 as StdString with uid="HJKKMLLUEV",Visible=.t., Left=239, Top=26,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oBox_5_37 as StdBox with uid="DLSNHYHKXJ",left=12, top=86, width=853,height=1
enddefine
define class tgscg_aopPag6 as StdContainer
  Width  = 869
  height = 441
  stdWidth  = 869
  stdheight = 441
  resizeXpos=724
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_6_1 as StdField with uid="BOVCFDHJYO",rtseq=128,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 6, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=90, Left=133, Top=26, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFCDENTE_6_3 as StdField with uid="ZPTITZNOZB",rtseq=129,rtrep=.f.,;
    cFormVar = "w_MFCDENTE", cQueryName = "MFCDENTE",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice ente previdenziale",;
    HelpContextID = 135157259,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=109, Top=114, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_PREV", cZoomOnZoom="GSCG_ACP", oKey_1_1="CPCODICE", oKey_1_2="this.w_MFCDENTE"

  func oMFCDENTE_6_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_3('Part',this)
      if .not. empty(.w_VRSDENT1)
        bRes2=.link_6_4('Full')
      endif
      if .not. empty(.w_MFCCOAE1)
        bRes2=.link_6_7('Full')
      endif
      if .not. empty(.w_VRSDENT2)
        bRes2=.link_6_17('Full')
      endif
      if .not. empty(.w_MFCCOAE2)
        bRes2=.link_6_19('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oMFCDENTE_6_3.ecpDrop(oSource)
    this.Parent.oContained.link_6_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCDENTE_6_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_PREV','*','CPCODICE',cp_AbsName(this.parent,'oMFCDENTE_6_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACP',"Codici enti previdenziali",'',this.parent.oContained
  endproc
  proc oMFCDENTE_6_3.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CPCODICE=this.parent.oContained.w_MFCDENTE
     i_obj.ecpSave()
  endproc

  add object oVRSDENT1_6_4 as StdField with uid="RWNSAXJAAF",rtseq=130,rtrep=.f.,;
    cFormVar = "w_VRSDENT1", cQueryName = "VRSDENT1",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede",;
    HelpContextID = 135225991,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=473, Top=144, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_AEN", cZoomOnZoom="GSCG_MSE", oKey_1_1="SECODENT", oKey_1_2="this.w_MFCDENTE", oKey_2_1="SECODSED", oKey_2_2="this.w_VRSDENT1"

  func oVRSDENT1_6_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE))
    endwith
   endif
  endfunc

  func oVRSDENT1_6_4.mHide()
    with this.Parent.oContained
      return (.w_MFCDENTE='0003' OR .w_MFCDENTE='0005'  OR .w_MFCDENTE='0006' )
    endwith
  endfunc

  func oVRSDENT1_6_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oVRSDENT1_6_4.ecpDrop(oSource)
    this.Parent.oContained.link_6_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVRSDENT1_6_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SED_AEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODENT="+cp_ToStrODBC(this.Parent.oContained.w_MFCDENTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODENT="+cp_ToStr(this.Parent.oContained.w_MFCDENTE)
    endif
    do cp_zoom with 'SED_AEN','*','SECODENT,SECODSED',cp_AbsName(this.parent,'oVRSDENT1_6_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MSE',"Codici sede",'',this.parent.oContained
  endproc
  proc oVRSDENT1_6_4.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MSE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SECODENT=w_MFCDENTE
     i_obj.w_SECODSED=this.parent.oContained.w_VRSDENT1
     i_obj.ecpSave()
  endproc

  add object oVPSDENT1_6_5 as StdField with uid="VVYWAPAEPT",rtseq=131,rtrep=.f.,;
    cFormVar = "w_VPSDENT1", cQueryName = "VPSDENT1",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice provincia",;
    HelpContextID = 135225479,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=473, Top=144, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ANAG_PRO", cZoomOnZoom="GSAR_APR", oKey_1_1="PRCODPRO", oKey_1_2="this.w_VPSDENT1"

  func oVPSDENT1_6_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE))
    endwith
   endif
  endfunc

  func oVPSDENT1_6_5.mHide()
    with this.Parent.oContained
      return (.w_MFCDENTE<>'0003' AND .w_MFCDENTE<>'0005'  AND .w_MFCDENTE<>'0006' )
    endwith
  endfunc

  func oVPSDENT1_6_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oVPSDENT1_6_5.ecpDrop(oSource)
    this.Parent.oContained.link_6_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVPSDENT1_6_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ANAG_PRO','*','PRCODPRO',cp_AbsName(this.parent,'oVPSDENT1_6_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APR',"Elenco province",'',this.parent.oContained
  endproc
  proc oVPSDENT1_6_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PRCODPRO=this.parent.oContained.w_VPSDENT1
     i_obj.ecpSave()
  endproc

  add object oMFCCOAE1_6_7 as StdField with uid="QKCNTKXASE",rtseq=133,rtrep=.f.,;
    cFormVar = "w_MFCCOAE1", cQueryName = "MFCCOAE1",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo",;
    HelpContextID = 195909111,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=544, Top=144, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_AEN", cZoomOnZoom="GSCG_MAE", oKey_1_1="AECODENT", oKey_1_2="this.w_MFCDENTE", oKey_2_1="AECAUSEN", oKey_2_2="this.w_MFCCOAE1"

  func oMFCCOAE1_6_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE))
    endwith
   endif
  endfunc

  func oMFCCOAE1_6_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCOAE1_6_7.ecpDrop(oSource)
    this.Parent.oContained.link_6_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCOAE1_6_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CAU_AEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AECODENT="+cp_ToStrODBC(this.Parent.oContained.w_MFCDENTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AECODENT="+cp_ToStr(this.Parent.oContained.w_MFCDENTE)
    endif
    do cp_zoom with 'CAU_AEN','*','AECODENT,AECAUSEN',cp_AbsName(this.parent,'oMFCCOAE1_6_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MAE',"Causali contributo",'',this.parent.oContained
  endproc
  proc oMFCCOAE1_6_7.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MAE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.AECODENT=w_MFCDENTE
     i_obj.w_AECAUSEN=this.parent.oContained.w_MFCCOAE1
     i_obj.ecpSave()
  endproc

  add object oMFCDPOS1_6_9 as StdField with uid="FVFMMSXZYE",rtseq=135,rtrep=.f.,;
    cFormVar = "w_MFCDPOS1", cQueryName = "MFCDPOS1",nZero=9,;
    bObbl = .f. , nPag = 6, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "Codice posizione",;
    HelpContextID = 104966665,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=55, Top=247, cSayPict='"999999999"', cGetPict='"999999999"', InputMask=replicate('X',9)

  func oMFCDPOS1_6_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCCOAE1))
    endwith
   endif
  endfunc

  add object oMFMSINE1_6_10 as StdField with uid="CQUCVMEFND",rtseq=136,rtrep=.f.,;
    cFormVar = "w_MFMSINE1", cQueryName = "MFMSINE1",nZero=2,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 140375543,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=146, Top=247, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMSINE1_6_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE1))
    endwith
   endif
  endfunc

  func oMFMSINE1_6_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE1) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFMSINE1) and 0< val(.w_MFMSINE1) and val(.w_MFMSINE1) < 13))
    endwith
    return bRes
  endfunc

  add object oMFANINE1_6_11 as StdField with uid="BSCZHMAMEK",rtseq=137,rtrep=.f.,;
    cFormVar = "w_MFANINE1", cQueryName = "MFANINE1",;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 139998711,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=174, Top=247, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANINE1_6_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE1))
    endwith
   endif
  endfunc

  func oMFANINE1_6_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE1) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFANINE1) and (val(.w_MFANINE1)>=1996  and  val(.w_MFANINE1)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMFMSFIE1_6_12 as StdField with uid="NJWCQBUUWE",rtseq=138,rtrep=.f.,;
    cFormVar = "w_MFMSFIE1", cQueryName = "MFMSFIE1",nZero=2,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 53343735,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=232, Top=247, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMSFIE1_6_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE1))
    endwith
   endif
  endfunc

  func oMFMSFIE1_6_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFMSFIE1) and 0<val(.w_MFMSFIE1) and val(.w_MFMSFIE1)<13))
    endwith
    return bRes
  endfunc

  add object oMFANF1_6_13 as StdField with uid="SCAIVPDBNL",rtseq=139,rtrep=.f.,;
    cFormVar = "w_MFANF1", cQueryName = "MFANF1",;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 81250874,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=260, Top=247, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANF1_6_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE1))
    endwith
   endif
  endfunc

  func oMFANF1_6_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE1) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE1) and !empty(.w_MFANF1)and((val(.w_MFANINE1)<val(.w_MFANF1)or .w_MFANINE1=.w_MFANF1 and val(.w_MFMSINE1)<=val(.w_MFMSFIE1)))))
    endwith
    return bRes
  endfunc

  add object oMFIMDAE1_6_14 as StdField with uid="SVMKLLZVBI",rtseq=140,rtrep=.f.,;
    cFormVar = "w_MFIMDAE1", cQueryName = "MFIMDAE1",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 185054711,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=309, Top=247, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDAE1_6_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE1))
    endwith
   endif
  endfunc

  add object oMFIMCAE1_6_15 as StdField with uid="WGFGYVXVVM",rtseq=141,rtrep=.f.,;
    cFormVar = "w_MFIMCAE1", cQueryName = "MFIMCAE1",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 184006135,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=467, Top=247, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCAE1_6_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE1) and .w_MFCDENTE<>'0003' AND .w_MFCDENTE<>'0004' AND .w_MFCDENTE<>'0005'  AND .w_MFCDENTE<>'0006' )
    endwith
   endif
  endfunc

  add object oVPSDENT2_6_16 as StdField with uid="XMQFIJZISL",rtseq=142,rtrep=.f.,;
    cFormVar = "w_VPSDENT2", cQueryName = "VPSDENT2",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice provincia",;
    HelpContextID = 135225480,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=473, Top=165, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ANAG_PRO", cZoomOnZoom="GSAR_APR", oKey_1_1="PRCODPRO", oKey_1_2="this.w_VPSDENT2"

  func oVPSDENT2_6_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE))
    endwith
   endif
  endfunc

  func oVPSDENT2_6_16.mHide()
    with this.Parent.oContained
      return (.w_MFCDENTE<>'0003' AND .w_MFCDENTE<>'0005'  AND .w_MFCDENTE<>'0006' )
    endwith
  endfunc

  func oVPSDENT2_6_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oVPSDENT2_6_16.ecpDrop(oSource)
    this.Parent.oContained.link_6_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVPSDENT2_6_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ANAG_PRO','*','PRCODPRO',cp_AbsName(this.parent,'oVPSDENT2_6_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APR',"Elenco province",'',this.parent.oContained
  endproc
  proc oVPSDENT2_6_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PRCODPRO=this.parent.oContained.w_VPSDENT2
     i_obj.ecpSave()
  endproc

  add object oVRSDENT2_6_17 as StdField with uid="LOAJBDQCPZ",rtseq=143,rtrep=.f.,;
    cFormVar = "w_VRSDENT2", cQueryName = "VRSDENT2",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede",;
    HelpContextID = 135225992,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=473, Top=165, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_AEN", cZoomOnZoom="GSCG_MSE", oKey_1_1="SECODENT", oKey_1_2="this.w_MFCDENTE", oKey_2_1="SECODSED", oKey_2_2="this.w_VRSDENT2"

  func oVRSDENT2_6_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE))
    endwith
   endif
  endfunc

  func oVRSDENT2_6_17.mHide()
    with this.Parent.oContained
      return (.w_MFCDENTE='0003' OR .w_MFCDENTE='0005'  OR .w_MFCDENTE='0006' )
    endwith
  endfunc

  func oVRSDENT2_6_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oVRSDENT2_6_17.ecpDrop(oSource)
    this.Parent.oContained.link_6_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVRSDENT2_6_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SED_AEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODENT="+cp_ToStrODBC(this.Parent.oContained.w_MFCDENTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODENT="+cp_ToStr(this.Parent.oContained.w_MFCDENTE)
    endif
    do cp_zoom with 'SED_AEN','*','SECODENT,SECODSED',cp_AbsName(this.parent,'oVRSDENT2_6_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MSE',"Codici sede",'',this.parent.oContained
  endproc
  proc oVRSDENT2_6_17.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MSE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SECODENT=w_MFCDENTE
     i_obj.w_SECODSED=this.parent.oContained.w_VRSDENT2
     i_obj.ecpSave()
  endproc

  add object oMFCCOAE2_6_19 as StdField with uid="NOTHDZQOTX",rtseq=145,rtrep=.f.,;
    cFormVar = "w_MFCCOAE2", cQueryName = "MFCCOAE2",;
    bObbl = .f. , nPag = 6, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contributo",;
    HelpContextID = 195909112,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=544, Top=164, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_AEN", cZoomOnZoom="GSCG_MAE", oKey_1_1="AECODENT", oKey_1_2="this.w_MFCDENTE", oKey_2_1="AECAUSEN", oKey_2_2="this.w_MFCCOAE2"

  func oMFCCOAE2_6_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCDENTE))
    endwith
   endif
  endfunc

  func oMFCCOAE2_6_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_6_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFCCOAE2_6_19.ecpDrop(oSource)
    this.Parent.oContained.link_6_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFCCOAE2_6_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CAU_AEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AECODENT="+cp_ToStrODBC(this.Parent.oContained.w_MFCDENTE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"AECODENT="+cp_ToStr(this.Parent.oContained.w_MFCDENTE)
    endif
    do cp_zoom with 'CAU_AEN','*','AECODENT,AECAUSEN',cp_AbsName(this.parent,'oMFCCOAE2_6_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_MAE',"Causali contributo",'',this.parent.oContained
  endproc
  proc oMFCCOAE2_6_19.mZoomOnZoom
    local i_obj
    i_obj=GSCG_MAE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.AECODENT=w_MFCDENTE
     i_obj.w_AECAUSEN=this.parent.oContained.w_MFCCOAE2
     i_obj.ecpSave()
  endproc

  add object oMFCDPOS2_6_21 as StdField with uid="YTUJHCWFYN",rtseq=147,rtrep=.f.,;
    cFormVar = "w_MFCDPOS2", cQueryName = "MFCDPOS2",nZero=9,;
    bObbl = .f. , nPag = 6, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "Codice posizione",;
    HelpContextID = 104966664,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=55, Top=267, cSayPict='"999999999"', cGetPict='"999999999"', InputMask=replicate('X',9)

  func oMFCDPOS2_6_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_MFCCOAE2))
    endwith
   endif
  endfunc

  add object oMFMSINE2_6_22 as StdField with uid="SHSFTEZLCU",rtseq=148,rtrep=.f.,;
    cFormVar = "w_MFMSINE2", cQueryName = "MFMSINE2",nZero=2,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 140375544,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=146, Top=267, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMSINE2_6_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE2))
    endwith
   endif
  endfunc

  func oMFMSINE2_6_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE2) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFMSINE2) and 0<  val(.w_MFMSINE2) and val(.w_MFMSINE2) < 13))
    endwith
    return bRes
  endfunc

  add object oMFANINE2_6_23 as StdField with uid="MCAXPQUTQA",rtseq=149,rtrep=.f.,;
    cFormVar = "w_MFANINE2", cQueryName = "MFANINE2",;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un anno compreso fra 1996 e 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 139998712,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=174, Top=267, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANINE2_6_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE2))
    endwith
   endif
  endfunc

  func oMFANINE2_6_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE2) $ 'RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFANINE2)  and (val(.w_MFANINE2)>=1996  and val(.w_MFANINE2)<=2050)))
    endwith
    return bRes
  endfunc

  add object oMFMSFIE2_6_24 as StdField with uid="YMMTXOEUQT",rtseq=150,rtrep=.f.,;
    cFormVar = "w_MFMSFIE2", cQueryName = "MFMSFIE2",nZero=2,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: selezionare un mese valido nella forma MM",;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 53343736,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=232, Top=267, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMFMSFIE2_6_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE2))
    endwith
   endif
  endfunc

  func oMFMSFIE2_6_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFMSFIE2)  and 0<val(.w_MFMSFIE2) and val(.w_MFMSFIE2)<13))
    endwith
    return bRes
  endfunc

  add object oMFANF2_6_25 as StdField with uid="CXTDVEYWGH",rtseq=151,rtrep=.f.,;
    cFormVar = "w_MFANF2", cQueryName = "MFANF2",;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Sezione altri enti: periodo minore del precedente oppure anno maggiore del 2050",;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 64473658,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=260, Top=267, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oMFANF2_6_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE2))
    endwith
   endif
  endfunc

  func oMFANF2_6_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (alltrim(.w_MFCCOAE2) $ 'CALS-CCSP-CCLS-RS-RC-PR' or (!empty(.w_MFCCOAE2) and !empty(.w_MFANF2)and((val(.w_MFANINE2)<val(.w_MFANF2)or .w_MFANINE2=.w_MFANF2 and val(.w_MFMSINE2)<=val(.w_MFMSFIE2)))))
    endwith
    return bRes
  endfunc

  add object oMFIMDAE2_6_26 as StdField with uid="JSQAGPZNHA",rtseq=152,rtrep=.f.,;
    cFormVar = "w_MFIMDAE2", cQueryName = "MFIMDAE2",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 185054712,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=309, Top=267, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMDAE2_6_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE2))
    endwith
   endif
  endfunc

  add object oMFIMCAE2_6_27 as StdField with uid="NZPTLZBXME",rtseq=153,rtrep=.f.,;
    cFormVar = "w_MFIMCAE2", cQueryName = "MFIMCAE2",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 184006136,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=467, Top=267, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  func oMFIMCAE2_6_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_MFCCOAE2) and .w_MFCDENTE<>'0003'  AND .w_MFCDENTE<>'0004' AND .w_MFCDENTE<>'0005'  AND .w_MFCDENTE<>'0006' )
    endwith
   endif
  endfunc

  add object oMFTDAENT_6_35 as StdField with uid="UBZHZRCDWP",rtseq=154,rtrep=.f.,;
    cFormVar = "w_MFTDAENT", cQueryName = "MFTDAENT",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 19962342,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=309, Top=318, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFTCAENT_6_36 as StdField with uid="VGJIBWUKLL",rtseq=155,rtrep=.f.,;
    cFormVar = "w_MFTCAENT", cQueryName = "MFTCAENT",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 20027878,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=467, Top=318, cSayPict="'@Z '+ v_PV(78)", cGetPict="'@Z '+ v_GV(78)"

  add object oMFSALAEN_6_37 as StdField with uid="NWKWVKNQGD",rtseq=156,rtrep=.f.,;
    cFormVar = "w_MFSALAEN", cQueryName = "MFSALAEN",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    HelpContextID = 192697876,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=607, Top=318, cSayPict="v_PV(78)", cGetPict="v_GV(78)"

  add object oDESAENTE_6_42 as StdField with uid="MHNJWSROFX",rtseq=157,rtrep=.f.,;
    cFormVar = "w_DESAENTE", cQueryName = "DESAENTE",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 135025787,;
   bGlobalFont=.t.,;
    Height=21, Width=185, Left=274, Top=114, InputMask=replicate('X',30)

  add object oMESE_6_45 as StdField with uid="BPSADYMFGH",rtseq=158,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=362, Top=27, InputMask=replicate('X',2)

  add object oANNO_6_46 as StdField with uid="XFUVSGDJBX",rtseq=159,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=387, Top=27, InputMask=replicate('X',4)


  add object oBtn_6_47 as StdButton with uid="FSUHCTGKMA",left=728, top=351, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=6;
    , ToolTipText = "Premere per visualizzare le distinte versamento di tipo previdenziali";
    , HelpContextID = 182357498;
    , tabstop=.f., caption='\<Distinte';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_6_47.Click()
      with this.Parent.oContained
        GSRI_BVI(this.Parent.oContained,"PR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_6_47.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MFCDENTE))
      endwith
    endif
  endfunc

  func oBtn_6_47.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_Rite<>'S')
     endwith
    endif
  endfunc


  add object oObj_6_51 as cp_runprogram with uid="IMVHWAQFRK",left=-4, top=457, width=143,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BBF('MP')",;
    cEvent = "ControllaDati",;
    nPag=6;
    , HelpContextID = 267263514


  add object oObj_6_52 as cp_calclbl with uid="HDNGOFJORS",left=544, top=108, width=62,height=35,;
    caption='Causale contributo',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=6;
    , HelpContextID = 123970517


  add object oObj_6_53 as cp_calclbl with uid="ZMENAVJMVQ",left=473, top=108, width=62,height=35,;
    caption='Codice sede\provincia',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=6;
    , HelpContextID = 16248303


  add object oObj_6_54 as cp_calclbl with uid="KQGEIDDWST",left=55, top=211, width=76,height=35,;
    caption='Codice posizione',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=6;
    , HelpContextID = 119970027

  add object oStr_6_2 as StdString with uid="XUFRHUWVCA",Visible=.t., Left=24, Top=116,;
    Alignment=1, Width=80, Height=18,;
    Caption="Codice ente:"  ;
  , bGlobalFont=.t.

  add object oStr_6_28 as StdString with uid="QDZVJIFSLW",Visible=.t., Left=161, Top=211,;
    Alignment=0, Width=123, Height=15,;
    Caption="Periodo di riferimento"  ;
  , bGlobalFont=.t.

  add object oStr_6_29 as StdString with uid="WFWPRZSWDC",Visible=.t., Left=137, Top=227,;
    Alignment=0, Width=85, Height=15,;
    Caption="da mm/aaaa"  ;
  , bGlobalFont=.t.

  add object oStr_6_30 as StdString with uid="UMJFKCLLIF",Visible=.t., Left=226, Top=227,;
    Alignment=0, Width=67, Height=15,;
    Caption="a mm/aaaa"  ;
  , bGlobalFont=.t.

  add object oStr_6_31 as StdString with uid="SAKCHWIMZS",Visible=.t., Left=307, Top=227,;
    Alignment=0, Width=154, Height=15,;
    Caption="Importi a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_6_32 as StdString with uid="ISMZUWQKMW",Visible=.t., Left=466, Top=227,;
    Alignment=0, Width=184, Height=15,;
    Caption="Importi a credito compensati"  ;
  , bGlobalFont=.t.

  add object oStr_6_33 as StdString with uid="TXOVKRQMIL",Visible=.t., Left=211, Top=320,;
    Alignment=1, Width=96, Height=15,;
    Caption="Totale M"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_34 as StdString with uid="ZHSIEZUFVM",Visible=.t., Left=441, Top=320,;
    Alignment=1, Width=18, Height=15,;
    Caption="N"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_38 as StdString with uid="LTURFGEZTI",Visible=.t., Left=607, Top=293,;
    Alignment=2, Width=132, Height=15,;
    Caption="Saldo (M-N)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_39 as StdString with uid="DDIXCLCYBG",Visible=.t., Left=6, Top=27,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_40 as StdString with uid="CTXZAWNSVI",Visible=.t., Left=13, Top=67,;
    Alignment=0, Width=263, Height=18,;
    Caption="Altri enti previdenziali ed assicurativi"  ;
  , bGlobalFont=.t.

  add object oStr_6_43 as StdString with uid="RVEZJNDSJX",Visible=.t., Left=185, Top=114,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_6_44 as StdString with uid="JWKSCNPXLY",Visible=.t., Left=231, Top=29,;
    Alignment=1, Width=126, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oBox_6_41 as StdBox with uid="OSEQIEOXBC",left=13, top=89, width=852,height=1
enddefine
define class tgscg_aopPag7 as StdContainer
  Width  = 869
  height = 441
  stdWidth  = 869
  stdheight = 441
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFSERIAL_7_1 as StdField with uid="FWNKVSYQIO",rtseq=163,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 7, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=90, Left=135, Top=26, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFSALFIN_7_2 as StdField with uid="XPRIIKCUAV",rtseq=164,rtrep=.f.,;
    cFormVar = "w_MFSALFIN", cQueryName = "MFSALFIN",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=0, bMultilanguage =  .f.,;
    HelpContextID = 8148500,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=126, Left=576, Top=28, cSayPict="v_PV(78)", cGetPict="v_GV(78)"


  add object oLinkPC_7_3 as stdDynamicChildContainer with uid="CDRKTLRCVN",left=5, top=73, width=798, height=249, bOnScreen=.t.;


  add object oMESE_7_7 as StdField with uid="VBABMPRSRE",rtseq=165,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=368, Top=27, InputMask=replicate('X',2)

  add object oANNO_7_8 as StdField with uid="DRPQPJTNKA",rtseq=166,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=393, Top=27, InputMask=replicate('X',4)

  add object oMFDESSTA_7_10 as StdField with uid="ZXJMGMFFSC",rtseq=171,rtrep=.f.,;
    cFormVar = "w_MFDESSTA", cQueryName = "MFDESSTA",;
    bObbl = .f. , nPag = 7, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della sede aziendale predefinita quale destinataria di stampa attestazione di pagamento modello F24",;
    HelpContextID = 233793031,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=198, Top=334, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SEDIAZIE", cZoomOnZoom="GSAR_ASE", oKey_1_1="SECODAZI", oKey_1_2="this.w_CODAZIE", oKey_2_1="SECODDES", oKey_2_2="this.w_MFDESSTA"

  func oMFDESSTA_7_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_7_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFDESSTA_7_10.ecpDrop(oSource)
    this.Parent.oContained.link_7_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFDESSTA_7_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.SEDIAZIE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZIE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"SECODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZIE)
    endif
    do cp_zoom with 'SEDIAZIE','*','SECODAZI,SECODDES',cp_AbsName(this.parent,'oMFDESSTA_7_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASE',"Sedi aziende",'GSAR_ASI.SEDIAZIE_VZM',this.parent.oContained
  endproc
  proc oMFDESSTA_7_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.SECODAZI=w_CODAZIE
     i_obj.w_SECODDES=this.parent.oContained.w_MFDESSTA
     i_obj.ecpSave()
  endproc

  add object oCAP_7_15 as StdField with uid="EHGCBYPICB",rtseq=176,rtrep=.f.,;
    cFormVar = "w_CAP", cQueryName = "CAP",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(9), bMultilanguage =  .f.,;
    ToolTipText = "Cap della sede aziendale predefinita quale destinataria di stampa attestazione di pagamento modello F24",;
    HelpContextID = 91955238,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=312, Top=334, InputMask=replicate('X',9)

  add object oLOCALI_7_16 as StdField with uid="YJQQJNWEDS",rtseq=177,rtrep=.f.,;
    cFormVar = "w_LOCALI", cQueryName = "LOCALI",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� della sede aziendale predefinita quale destinataria di stampa attestazione di pagamento modello F24",;
    HelpContextID = 210018634,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=478, Top=334, InputMask=replicate('X',30)

  add object oPROVIN_7_17 as StdField with uid="KKBGTKSRJE",rtseq=178,rtrep=.f.,;
    cFormVar = "w_PROVIN", cQueryName = "PROVIN",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia della sede aziendale predefinita quale destinataria di stampa attestazione di pagamento modello F24",;
    HelpContextID = 127852042,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=198, Top=363, InputMask=replicate('X',2)

  add object oINDIRI_7_18 as StdField with uid="LLDGSFXWVI",rtseq=179,rtrep=.f.,;
    cFormVar = "w_INDIRI", cQueryName = "INDIRI",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo della sede aziendale predefinita quale destinataria di stampa attestazione di pagamento modello F24",;
    HelpContextID = 203199098,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=312, Top=363, InputMask=replicate('X',35)


  add object oBtn_7_26 as StdButton with uid="CJUEHBMWXH",left=678, top=362, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=7;
    , ToolTipText = "Premere per salvare i risultati e stampare il modello";
    , HelpContextID = 91630310;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_26.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_7_27 as StdButton with uid="VSFOQMDZOF",left=730, top=362, width=48,height=45,;
    CpPicture="bmp\ESC.bmp", caption="", nPag=7;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91630310;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_7_27.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_7_4 as StdString with uid="VKEGRLLTGP",Visible=.t., Left=454, Top=31,;
    Alignment=1, Width=115, Height=15,;
    Caption="SALDO FINALE:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_5 as StdString with uid="BYCBSRREFL",Visible=.t., Left=6, Top=28,;
    Alignment=1, Width=125, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_7_6 as StdString with uid="SCVOUDRHJZ",Visible=.t., Left=229, Top=29,;
    Alignment=1, Width=135, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_7_19 as StdString with uid="YNXKDJTXGM",Visible=.t., Left=268, Top=338,;
    Alignment=1, Width=44, Height=18,;
    Caption="Cap:"  ;
  , bGlobalFont=.t.

  add object oStr_7_20 as StdString with uid="TSJBINZCHU",Visible=.t., Left=418, Top=338,;
    Alignment=1, Width=60, Height=18,;
    Caption="Localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_7_21 as StdString with uid="GGEVHAPSLD",Visible=.t., Left=125, Top=367,;
    Alignment=1, Width=70, Height=18,;
    Caption="Provincia:"  ;
  , bGlobalFont=.t.

  add object oStr_7_22 as StdString with uid="JLVIFDUYJQ",Visible=.t., Left=7, Top=338,;
    Alignment=1, Width=188, Height=18,;
    Caption="Sede destinatario di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_7_23 as StdString with uid="GNVKOPQKBS",Visible=.t., Left=241, Top=367,;
    Alignment=1, Width=71, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oBox_7_9 as StdBox with uid="LIMWEMRJGT",left=5, top=68, width=860,height=2
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_avf",lower(this.oContained.GSCG_AVF.class))=0
        this.oContained.GSCG_AVF.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgscg_aopPag8 as StdContainer
  Width  = 869
  height = 441
  stdWidth  = 869
  stdheight = 441
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMFCODFIR_8_16 as StdField with uid="YKVNIHUMSL",rtseq=202,rtrep=.f.,;
    cFormVar = "w_MFCODFIR", cQueryName = "MFCODFIR",;
    bObbl = .t. , nPag = 8, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del versante/firmatario",;
    HelpContextID = 611864,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=25, Top=114, cSayPict='repl("!",16)', cGetPict='repl("!",16)', InputMask=replicate('X',16)

  func oMFCODFIR_8_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MFCHKFIR='S')
    endwith
   endif
  endfunc

  func oMFCODFIR_8_16.mHide()
    with this.Parent.oContained
      return (EMPTY (.w_ENTRATEL))
    endwith
  endfunc

  add object oMFCHKFIR_8_18 as StdCheck with uid="EJOHJSXMXD",rtseq=203,rtrep=.f.,left=25, top=73, caption="Firmatario diverso dal contribuente",;
    ToolTipText = "Se attivo, il soggetto che effettua il versamento � diverso dal contribuente",;
    HelpContextID = 7493144,;
    cFormVar="w_MFCHKFIR", bObbl = .f. , nPag = 8;
   , bGlobalFont=.t.


  func oMFCHKFIR_8_18.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oMFCHKFIR_8_18.GetRadio()
    this.Parent.oContained.w_MFCHKFIR = this.RadioValue()
    return .t.
  endfunc

  func oMFCHKFIR_8_18.SetRadio()
    this.Parent.oContained.w_MFCHKFIR=trim(this.Parent.oContained.w_MFCHKFIR)
    this.value = ;
      iif(this.Parent.oContained.w_MFCHKFIR=='S',1,;
      0)
  endfunc


  add object oMFCARFIR_8_19 as StdCombo with uid="ZXQSKGMMLB",rtseq=204,rtrep=.f.,left=237,top=114,width=302,height=21;
    , ToolTipText = "Codice carica";
    , HelpContextID = 14374424;
    , cFormVar="w_MFCARFIR",RowSource=""+"1) Rappr. legale o negoziale,"+"2) Rappr. di minore - curatore eredit�,"+"3) Curatore fallimentare,"+"7) Erede", bObbl = .f. , nPag = 8;
  , bGlobalFont=.t.


  func oMFCARFIR_8_19.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'7',;
    space(2))))))
  endfunc
  func oMFCARFIR_8_19.GetRadio()
    this.Parent.oContained.w_MFCARFIR = this.RadioValue()
    return .t.
  endfunc

  func oMFCARFIR_8_19.SetRadio()
    this.Parent.oContained.w_MFCARFIR=trim(this.Parent.oContained.w_MFCARFIR)
    this.value = ;
      iif(this.Parent.oContained.w_MFCARFIR=='1',1,;
      iif(this.Parent.oContained.w_MFCARFIR=='2',2,;
      iif(this.Parent.oContained.w_MFCARFIR=='3',3,;
      iif(this.Parent.oContained.w_MFCARFIR=='7',4,;
      0))))
  endfunc

  func oMFCARFIR_8_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MFCHKFIR='S')
    endwith
   endif
  endfunc

  add object oMFCOGFIR_8_20 as StdField with uid="TZSAQVZZVW",rtseq=205,rtrep=.f.,;
    cFormVar = "w_MFCOGFIR", cQueryName = "MFCOGFIR",;
    bObbl = .t. , nPag = 8, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del versante/firmatario",;
    HelpContextID = 3757592,;
   bGlobalFont=.t.,;
    Height=21, Width=197, Left=25, Top=161, cSayPict='repl("!",24)', cGetPict='repl("!",24)', InputMask=replicate('X',24)

  func oMFCOGFIR_8_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MFCHKFIR='S' )
    endwith
   endif
  endfunc

  func oMFCOGFIR_8_20.mHide()
    with this.Parent.oContained
      return (EMPTY (.w_ENTRATEL))
    endwith
  endfunc

  add object oMFNOMFIR_8_21 as StdField with uid="QJTXUASRCB",rtseq=206,rtrep=.f.,;
    cFormVar = "w_MFNOMFIR", cQueryName = "MFNOMFIR",;
    bObbl = .t. , nPag = 8, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del versante/firmatario",;
    HelpContextID = 10094104,;
   bGlobalFont=.t.,;
    Height=21, Width=180, Left=237, Top=161, cSayPict='repl("!",20)', cGetPict='repl("!",20)', InputMask=replicate('X',20)

  func oMFNOMFIR_8_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MFCHKFIR='S' )
    endwith
   endif
  endfunc

  func oMFNOMFIR_8_21.mHide()
    with this.Parent.oContained
      return (EMPTY (.w_ENTRATEL))
    endwith
  endfunc


  add object oMFSESFIR_8_22 as StdCombo with uid="SINWHYZRUD",rtseq=207,rtrep=.f.,left=443,top=161,width=138,height=21;
    , ToolTipText = "Sesso del versante/firmatario";
    , HelpContextID = 15750680;
    , cFormVar="w_MFSESFIR",RowSource=""+"Maschio,"+"Femmina", bObbl = .f. , nPag = 8;
  , bGlobalFont=.t.


  func oMFSESFIR_8_22.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oMFSESFIR_8_22.GetRadio()
    this.Parent.oContained.w_MFSESFIR = this.RadioValue()
    return .t.
  endfunc

  func oMFSESFIR_8_22.SetRadio()
    this.Parent.oContained.w_MFSESFIR=trim(this.Parent.oContained.w_MFSESFIR)
    this.value = ;
      iif(this.Parent.oContained.w_MFSESFIR=='M',1,;
      iif(this.Parent.oContained.w_MFSESFIR=='F',2,;
      0))
  endfunc

  func oMFSESFIR_8_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MFCHKFIR='S' )
    endwith
   endif
  endfunc

  add object oMFDATFIR_8_23 as StdField with uid="XTBQCXIXJJ",rtseq=208,rtrep=.f.,;
    cFormVar = "w_MFDATFIR", cQueryName = "MFDATFIR",;
    bObbl = .t. , nPag = 8, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del versante/firmatario",;
    HelpContextID = 16475672,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=25, Top=207

  func oMFDATFIR_8_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MFCHKFIR='S' )
    endwith
   endif
  endfunc

  func oMFDATFIR_8_23.mHide()
    with this.Parent.oContained
      return (EMPTY (.w_ENTRATEL))
    endwith
  endfunc

  add object oMFCOMFIR_8_24 as StdField with uid="TKKSRUCNZN",rtseq=209,rtrep=.f.,;
    cFormVar = "w_MFCOMFIR", cQueryName = "MFCOMFIR",;
    bObbl = .t. , nPag = 8, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune (o stato estero) di nascita del versante/firmatario",;
    HelpContextID = 10049048,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=143, Top=207, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oMFCOMFIR_8_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MFCHKFIR='S' )
    endwith
   endif
  endfunc

  func oMFCOMFIR_8_24.mHide()
    with this.Parent.oContained
      return (EMPTY (.w_ENTRATEL))
    endwith
  endfunc

  add object oMFPROFIR_8_25 as StdField with uid="BAKMPIILVE",rtseq=210,rtrep=.f.,;
    cFormVar = "w_MFPROFIR", cQueryName = "MFPROFIR",;
    bObbl = .t. , nPag = 8, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di nascita del versante/firmatario",;
    HelpContextID = 12396056,;
   bGlobalFont=.t.,;
    Height=21, Width=39, Left=470, Top=207, cSayPict='repl("!",2)', cGetPict='repl("!",2)', InputMask=replicate('X',2), bHasZoom = .t. 

  func oMFPROFIR_8_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MFCHKFIR='S' )
    endwith
   endif
  endfunc

  func oMFPROFIR_8_25.mHide()
    with this.Parent.oContained
      return (EMPTY (.w_ENTRATEL))
    endwith
  endfunc

  proc oMFPROFIR_8_25.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_MFPROFIR")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oMFDOMFIR_8_26 as StdField with uid="ZIALRUHHOG",rtseq=211,rtrep=.f.,;
    cFormVar = "w_MFDOMFIR", cQueryName = "MFDOMFIR",;
    bObbl = .f. , nPag = 8, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune di residenza anagrafica o di domicilio fiscale del versante/firmatario",;
    HelpContextID = 10053144,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=25, Top=254, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40)

  func oMFDOMFIR_8_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MFCHKFIR='S')
    endwith
   endif
  endfunc

  func oMFDOMFIR_8_26.mHide()
    with this.Parent.oContained
      return (EMPTY (.w_ENTRATEL))
    endwith
  endfunc

  add object oMFPRDFIR_8_27 as StdField with uid="CSVBYIQHNF",rtseq=212,rtrep=.f.,;
    cFormVar = "w_MFPRDFIR", cQueryName = "MFPRDFIR",;
    bObbl = .f. , nPag = 8, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza anagrafica o di domicilio fiscale",;
    HelpContextID = 861720,;
   bGlobalFont=.t.,;
    Height=21, Width=39, Left=331, Top=254, cSayPict='repl("!",2)', cGetPict='repl("!",2)', InputMask=replicate('X',2), bHasZoom = .t. 

  func oMFPRDFIR_8_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MFCHKFIR='S')
    endwith
   endif
  endfunc

  func oMFPRDFIR_8_27.mHide()
    with this.Parent.oContained
      return (EMPTY (.w_ENTRATEL))
    endwith
  endfunc

  proc oMFPRDFIR_8_27.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P","","",".w_MFPRDFIR")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oMFCAPFIR_8_28 as StdField with uid="TFZRJGAHWV",rtseq=213,rtrep=.f.,;
    cFormVar = "w_MFCAPFIR", cQueryName = "MFCAPFIR",;
    bObbl = .f. , nPag = 8, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "C.A.P. della residenza anagrafica o del domicilio fiscale del versante/firmatario",;
    HelpContextID = 12277272,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=395, Top=254, cSayPict='repl("!",5)', cGetPict='repl("!",5)', InputMask=replicate('X',5)

  func oMFCAPFIR_8_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MFCHKFIR='S')
    endwith
   endif
  endfunc

  func oMFCAPFIR_8_28.mHide()
    with this.Parent.oContained
      return (EMPTY (.w_ENTRATEL))
    endwith
  endfunc

  add object oMFINDFIR_8_32 as StdField with uid="DEFTDGNTUI",rtseq=214,rtrep=.f.,;
    cFormVar = "w_MFINDFIR", cQueryName = "MFINDFIR",;
    bObbl = .f. , nPag = 8, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo ( frazione, via e numero civico) di residenza anagrafica o domicilio",;
    HelpContextID = 570904,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=466, Top=254, cSayPict='repl("!",35)', cGetPict='repl("!",35)', InputMask=replicate('X',35)

  func oMFINDFIR_8_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MFCHKFIR='S')
    endwith
   endif
  endfunc

  func oMFINDFIR_8_32.mHide()
    with this.Parent.oContained
      return (EMPTY (.w_ENTRATEL))
    endwith
  endfunc

  add object oMFMAIVER_8_44 as StdField with uid="PAHSGYGHXO",rtseq=217,rtrep=.f.,;
    cFormVar = "w_MFMAIVER", cQueryName = "MFMAIVER",;
    bObbl = .f. , nPag = 8, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di posta elettronica del versante/firmatario",;
    HelpContextID = 4978200,;
   bGlobalFont=.t.,;
    Height=21, Width=427, Left=25, Top=301, cSayPict='repl("!",60)', cGetPict='repl("!",60)', InputMask=replicate('X',60)

  func oMFMAIVER_8_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MFCHKFIR='S')
    endwith
   endif
  endfunc

  add object oMFBANPAS_8_45 as StdField with uid="CXHRQSHKLA",rtseq=218,rtrep=.f.,;
    cFormVar = "w_MFBANPAS", cQueryName = "MFBANPAS",;
    bObbl = .f. , nPag = 8, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice banca inesistente, obsoleta, di compensazione o salvo buon fine",;
    HelpContextID = 90487271,;
   bGlobalFont=.t.,;
    Height=21, Width=131, Left=25, Top=350, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_MFBANPAS"

  func oMFBANPAS_8_45.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc

  func oMFBANPAS_8_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_8_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oMFBANPAS_8_45.ecpDrop(oSource)
    this.Parent.oContained.link_8_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMFBANPAS_8_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oMFBANPAS_8_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banche",'GSCG_API.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oMFBANPAS_8_45.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_MFBANPAS
     i_obj.ecpSave()
  endproc

  add object oBADESCRI_8_46 as StdField with uid="UHOHRDJWEA",rtseq=219,rtrep=.f.,;
    cFormVar = "w_BADESCRI", cQueryName = "BADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 34643873,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=158, Top=350, InputMask=replicate('X',35)

  func oBADESCRI_8_46.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc

  add object oMFCOIBAN_8_47 as StdField with uid="JBIOEDTVUE",rtseq=220,rtrep=.f.,;
    cFormVar = "w_MFCOIBAN", cQueryName = "MFCOIBAN",;
    bObbl = .f. , nPag = 8, value=space(34), bMultilanguage =  .f.,;
    HelpContextID = 61254124,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=466, Top=350, InputMask=replicate('X',34)

  func oMFCOIBAN_8_47.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc


  add object oBtn_8_49 as StdButton with uid="WCQNGTFGGG",left=739, top=381, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=8;
    , ToolTipText = "Premere per salvare i risultati e stampare il modello";
    , HelpContextID = 91630310;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_8_49.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_8_50 as StdButton with uid="RNJONCDWFL",left=791, top=381, width=48,height=45,;
    CpPicture="bmp\ESC.bmp", caption="", nPag=8;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91630310;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_8_50.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMFSERIAL_8_52 as StdField with uid="UOJTRSZUTE",rtseq=221,rtrep=.f.,;
    cFormVar = "w_MFSERIAL", cQueryName = "MFSERIAL",enabled=.f.,nZero=10,;
    bObbl = .f. , nPag = 8, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 203401710,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=90, Left=121, Top=9, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oMFSALFIN_8_53 as StdField with uid="PFMLKVATFH",rtseq=222,rtrep=.f.,;
    cFormVar = "w_MFSALFIN", cQueryName = "MFSALFIN",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=0, bMultilanguage =  .f.,;
    HelpContextID = 8148500,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=126, Left=562, Top=11, cSayPict="v_PV(78)", cGetPict="v_GV(78)"

  add object oMESE_8_57 as StdField with uid="XBQDXDUIMO",rtseq=223,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 96490694,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=354, Top=10, InputMask=replicate('X',2)

  add object oANNO_8_58 as StdField with uid="MSLQJKYUEO",rtseq=224,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 97127686,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=379, Top=10, InputMask=replicate('X',4)

  add object oMFCOIBAN_8_66 as StdField with uid="NVIAFEHMCZ",rtseq=229,rtrep=.f.,;
    cFormVar = "w_MFCOIBAN", cQueryName = "MFCOIBAN",;
    bObbl = .f. , nPag = 8, value=space(34), bMultilanguage =  .f.,;
    HelpContextID = 61254124,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=25, Top=350, InputMask=replicate('X',34)

  func oMFCOIBAN_8_66.mHide()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  add object oStr_8_29 as StdString with uid="HLTFUEIMSS",Visible=.t., Left=466, Top=237,;
    Alignment=0, Width=203, Height=18,;
    Caption="Indirizzo, frazione, via e numero civico"  ;
  , bGlobalFont=.t.

  add object oStr_8_31 as StdString with uid="FHMRICXWEG",Visible=.t., Left=25, Top=52,;
    Alignment=0, Width=367, Height=18,;
    Caption="Dati relativi al versante/firmatario della dichiarazione"  ;
  , bGlobalFont=.t.

  add object oStr_8_33 as StdString with uid="OUFXKVQDCD",Visible=.t., Left=25, Top=96,;
    Alignment=0, Width=140, Height=18,;
    Caption="Codice fiscale"  ;
  , bGlobalFont=.t.

  add object oStr_8_34 as StdString with uid="CNEQWRATVP",Visible=.t., Left=237, Top=96,;
    Alignment=0, Width=92, Height=18,;
    Caption="Codice carica"  ;
  , bGlobalFont=.t.

  add object oStr_8_35 as StdString with uid="WDHWDQAXSI",Visible=.t., Left=25, Top=144,;
    Alignment=0, Width=117, Height=18,;
    Caption="Cognome"  ;
  , bGlobalFont=.t.

  add object oStr_8_36 as StdString with uid="REXTSCILXO",Visible=.t., Left=237, Top=144,;
    Alignment=0, Width=96, Height=18,;
    Caption="Nome"  ;
  , bGlobalFont=.t.

  add object oStr_8_37 as StdString with uid="XUHJXOYZOH",Visible=.t., Left=443, Top=144,;
    Alignment=0, Width=98, Height=18,;
    Caption="Sesso"  ;
  , bGlobalFont=.t.

  add object oStr_8_38 as StdString with uid="QAEPQCNYCW",Visible=.t., Left=25, Top=189,;
    Alignment=0, Width=108, Height=18,;
    Caption="Data di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_8_39 as StdString with uid="CNXPBYZDFR",Visible=.t., Left=143, Top=189,;
    Alignment=0, Width=190, Height=18,;
    Caption="Comune (o stato estero) di nascita"  ;
  , bGlobalFont=.t.

  add object oStr_8_40 as StdString with uid="WJIAKBWWDV",Visible=.t., Left=470, Top=189,;
    Alignment=0, Width=94, Height=18,;
    Caption="Provincia"  ;
  , bGlobalFont=.t.

  add object oStr_8_41 as StdString with uid="NJXCAYPDMX",Visible=.t., Left=25, Top=237,;
    Alignment=0, Width=204, Height=18,;
    Caption="Comune (o stato estero) di residenza"  ;
  , bGlobalFont=.t.

  add object oStr_8_42 as StdString with uid="PNQVTCNWWJ",Visible=.t., Left=331, Top=237,;
    Alignment=0, Width=63, Height=18,;
    Caption="Provincia"  ;
  , bGlobalFont=.t.

  add object oStr_8_43 as StdString with uid="KSQZRXPMIB",Visible=.t., Left=395, Top=237,;
    Alignment=0, Width=52, Height=18,;
    Caption="CAP"  ;
  , bGlobalFont=.t.

  add object oStr_8_51 as StdString with uid="OPLZGITBIW",Visible=.t., Left=25, Top=284,;
    Alignment=0, Width=160, Height=18,;
    Caption="Indirizzo di posta elettronica"  ;
  , bGlobalFont=.t.

  add object oStr_8_54 as StdString with uid="WJJEIFKNYI",Visible=.t., Left=440, Top=14,;
    Alignment=1, Width=115, Height=15,;
    Caption="SALDO FINALE:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_8_55 as StdString with uid="XRSRWLHITT",Visible=.t., Left=8, Top=11,;
    Alignment=1, Width=109, Height=15,;
    Caption="Modello numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_8_56 as StdString with uid="HTGTZLWRLD",Visible=.t., Left=215, Top=12,;
    Alignment=1, Width=135, Height=18,;
    Caption="Periodo di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_8_60 as StdString with uid="PTFTJCULZG",Visible=.t., Left=25, Top=334,;
    Alignment=0, Width=92, Height=18,;
    Caption="Banca passiva"  ;
  , bGlobalFont=.t.

  func oStr_8_60.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc

  add object oStr_8_61 as StdString with uid="GLNBMOSWHM",Visible=.t., Left=466, Top=334,;
    Alignment=0, Width=170, Height=18,;
    Caption="Coordinate IBAN di addebito"  ;
  , bGlobalFont=.t.

  func oStr_8_61.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc

  add object oStr_8_67 as StdString with uid="KGDHXXCAYV",Visible=.t., Left=25, Top=334,;
    Alignment=0, Width=170, Height=18,;
    Caption="Coordinate IBAN di addebito"  ;
  , bGlobalFont=.t.

  func oStr_8_67.mHide()
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
  endfunc

  add object oBox_8_30 as StdBox with uid="LEBGSAUSDO",left=25, top=72, width=839,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_aop','MOD_PAG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MFSERIAL=MOD_PAG.MFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_aop
Procedure AggionaContibuente (oparent)
local campo
IF  NOT (.w_MFENTRAT<>'E' OR  ( oparent.gscg_acg.w_CFPERFIS='S' AND .w_MFCHKFIR=' ' ) OR  ( oparent.gscg_acg.w_CFPERFIS='N' AND .w_MFCHKFIR='S' AND .w_MFCARFIR='1' ) )
       oparent.gscg_acg.w_CFCODIDE=' '
       campo=oparent.gscg_acg.getctrl ("w_CFCODIDE")
       campo.value=""
       oparent.gscg_acg.w_CFSECCOD=' '
       campo=oparent.gscg_acg.getctrl ("w_CFSECCOD")
       campo.value=""
     ENDIF
oparent.gscg_acg.mEnableControls()
**oparent.gscg_acg.mcalc(.t.)
ENDPROC
* --- Fine Area Manuale
