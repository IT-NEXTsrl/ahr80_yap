* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_bel                                                        *
*              Eliminazione PDA                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-01-18                                                      *
* Last revis.: 2016-01-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_bel",oParentObject,m.pPARAM)
return(i_retval)

define class tgsac_bel as StdBatch
  * --- Local variables
  pPARAM = space(1)
  PADRE = .NULL.
  w_NUMREC = 0
  w_PDSERIAL = space(10)
  w_PDROWNUM = 0
  w_PDSERRIF = space(10)
  w_ERRORE = .f.
  w_oERRORLOG = .NULL.
  w_MVCODART = space(20)
  w_COMMDEFA = space(15)
  w_COMMAPPO = space(15)
  w_SALCOM = space(1)
  w_MVKEYSAL = space(20)
  w_MVCODMAG = space(5)
  w_MVCODMAT = space(5)
  w_MVFLCASC = space(1)
  w_MVFLIMPE = space(1)
  w_MVFLORDI = space(1)
  w_MVFLRISE = space(1)
  w_MVF2CASC = space(1)
  w_MVF2IMPE = space(1)
  w_MVF2ORDI = space(1)
  w_MVF2RISE = space(1)
  w_FLGSAL = space(1)
  w_MVQTASAL = 0
  w_MVQTAUM1 = 0
  w_MVIMPCOM = 0
  w_MVCODATT = space(15)
  w_MVCODCOM = space(15)
  w_MVCODCOS = space(15)
  w_MVTIPATT = space(1)
  w_MVFLORCO = space(1)
  w_MVFLCOCO = space(1)
  * --- WorkFile variables
  SALDIART_idx=0
  MA_COSTI_idx=0
  PDA_DETT_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  ART_ICOL_idx=0
  SALDICOM_idx=0
  CON_PAGA_idx=0
  DOC_RATE_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione PDA  (GSAC_KEL)
    *     
    *     pPARAM: 'E' Eliminazione
    *     pPARAM: 'S' Seleziona deseleziona record zoom
    this.PADRE = this.oParentObject
    NC = this.PADRE.w_ZoomSel.cCursor
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    do case
      case this.pPARAM="E"
        * --- Eliminazione ordini
        * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
        this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
        if used(NC)
          Select (NC) 
 Locate for xchk=1
          if found()
            Select * from (NC) where xchk=1 into cursor elab 
 Select elab 
 GO TOP 
 SCAN
            this.w_NUMREC = RECNO()
            this.w_PDSERIAL = NVL(PDSERIAL,"")
            this.w_PDROWNUM = NVL(PDROWNUM,0)
            this.w_PDSERRIF = NVL(PDSERRIF,"")
            this.w_ERRORE = .f.
            * --- Controllo ordine associato
            if !empty(this.w_PDSERRIF)
              vq_exec("QUERY\GSAC1KEL.VQR",this,"DOCCOL")
              if USED("DOCCOL")
                Select DOCCOL
                if RECCOUNT() > 0
                  this.w_oERRORLOG.AddMsgLog("PDA %1 associato a documento evaso",alltrim(this.w_PDSERIAL))     
                  this.w_ERRORE = .t.
                else
                  * --- Cancellazione documento associato
                  this.Page_2()
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
              USE IN Select ("DOCCOL")
            endif
            Select Elab 
 GOTO this.w_NUMREC
            if !this.w_ERRORE
              * --- Try
              local bErr_0448D7B8
              bErr_0448D7B8=bTrsErr
              this.Try_0448D7B8()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- rollback
                bTrsErr=.t.
                cp_EndTrs(.t.)
                this.w_oERRORLOG.AddMsgLog("Ordine %1: impossibile cancellare",alltrim(this.w_PDSERIAL))     
              endif
              bTrsErr=bTrsErr or bErr_0448D7B8
              * --- End
            endif
            Select Elab 
 GOTO this.w_NUMREC
            ENDSCAN
          else
            ah_errormsg("Selezionare almeno un ordine!",48)
          endif
          if this.w_oERRORLOG.ISFULLLOG()
            this.w_oERRORLOG.PRINTLOG(THIS , ah_msgformat("Eliminazione proposte di acquisto" , .f.))     
          else
            ah_ErrorMsg("Elaborazione terminata",,"")
          endif
          this.PADRE.NotifyEvent("ActivatePage 2")     
          USE IN Select ("Elab")
        endif
      case this.pPARAM="S"
        if used(NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (NC) SET xChk=0
          endif
        endif
    endcase
  endproc
  proc Try_0448D7B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    ah_msg("Cancellazione ordine %1 in corso...",.t.,.f.,.f.,this.w_PDSERIAL)
    * --- Cancella lista materiali stimati
    * --- Delete from PDA_DETT
    i_nConn=i_TableProp[this.PDA_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PDA_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PDSERIAL = "+cp_ToStrODBC(this.w_PDSERIAL);
            +" and PDROWNUM = "+cp_ToStrODBC(this.w_PDROWNUM);
             )
    else
      delete from (i_cTable) where;
            PDSERIAL = this.w_PDSERIAL;
            and PDROWNUM = this.w_PDROWNUM;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancellazione documento associato
    * --- Try
    local bErr_0446D888
    bErr_0446D888=bTrsErr
    this.Try_0446D888()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      this.w_oERRORLOG.AddMsgLog("Impossibile effettuare la cancellazione del documento associato all'ordine %1",alltrim(this.w_PDSERIAL))     
      this.w_ERRORE = .t.
    endif
    bTrsErr=bTrsErr or bErr_0446D888
    * --- End
  endproc
  proc Try_0446D888()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Select from DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MVKEYSAL,MVCODART,MVCODMAG,MVCODMAT,MVFLCASC,MVFLIMPE,MVFLORDI,MVFLRISE,MVF2CASC,MVF2IMPE,MVF2ORDI,MVF2RISE,MVQTASAL,MVQTAUM1,MVIMPCOM,MVCODATT,MVCODCOM,MVCODCOS,MVTIPATT,MVFLORCO,MVFLCOCO  from "+i_cTable+" DOC_DETT ";
          +" where DOC_DETT.MVSERIAL="+cp_ToStrODBC(this.w_PDSERRIF)+"";
           ,"_Curs_DOC_DETT")
    else
      select MVKEYSAL,MVCODART,MVCODMAG,MVCODMAT,MVFLCASC,MVFLIMPE,MVFLORDI,MVFLRISE,MVF2CASC,MVF2IMPE,MVF2ORDI,MVF2RISE,MVQTASAL,MVQTAUM1,MVIMPCOM,MVCODATT,MVCODCOM,MVCODCOS,MVTIPATT,MVFLORCO,MVFLCOCO from (i_cTable);
       where DOC_DETT.MVSERIAL=this.w_PDSERRIF;
        into cursor _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      select _Curs_DOC_DETT
      locate for 1=1
      do while not(eof())
      this.w_MVKEYSAL = _Curs_DOC_DETT.MVKEYSAL
      this.w_MVCODART = _Curs_DOC_DETT.MVCODART
      this.w_MVCODMAG = _Curs_DOC_DETT.MVCODMAG
      this.w_MVCODMAT = _Curs_DOC_DETT.MVCODMAT
      this.w_MVQTASAL = _Curs_DOC_DETT.MVQTASAL
      this.w_MVQTAUM1 = _Curs_DOC_DETT.MVQTAUM1
      this.w_MVIMPCOM = _Curs_DOC_DETT.MVIMPCOM
      this.w_MVCODATT = _Curs_DOC_DETT.MVCODATT
      this.w_MVCODCOM = _Curs_DOC_DETT.MVCODCOM
      this.w_MVCODCOS = _Curs_DOC_DETT.MVCODCOS
      this.w_MVTIPATT = _Curs_DOC_DETT.MVTIPATT
      this.w_MVFLCASC = iif(_Curs_DOC_DETT.MVFLCASC="+","-",iif(_Curs_DOC_DETT.MVFLCASC="-","+"," "))
      this.w_MVFLIMPE = iif(_Curs_DOC_DETT.MVFLIMPE="+","-",iif(_Curs_DOC_DETT.MVFLIMPE="-","+"," "))
      this.w_MVFLORDI = iif(_Curs_DOC_DETT.MVFLORDI="+","-",iif(_Curs_DOC_DETT.MVFLORDI="-","+"," "))
      this.w_MVFLRISE = iif(_Curs_DOC_DETT.MVFLRISE="+","-",iif(_Curs_DOC_DETT.MVFLRISE="-","+"," "))
      this.w_MVFLCASC = iif(_Curs_DOC_DETT.MVFLCASC="+","-",iif(_Curs_DOC_DETT.MVFLCASC="-","+"," "))
      this.w_MVF2IMPE = iif(_Curs_DOC_DETT.MVF2IMPE="+","-",iif(_Curs_DOC_DETT.MVF2IMPE="-","+"," "))
      this.w_MVF2ORDI = iif(_Curs_DOC_DETT.MVF2ORDI="+","-",iif(_Curs_DOC_DETT.MVF2ORDI="-","+"," "))
      this.w_MVF2RISE = iif(_Curs_DOC_DETT.MVF2RISE="+","-",iif(_Curs_DOC_DETT.MVF2RISE="-","+"," "))
      this.w_MVFLORCO = iif(this.w_MVFLORCO="+","-",iif(this.w_MVFLORCO="-","+"," "))
      this.w_MVFLCOCO = iif(this.w_MVFLCOCO="+","-",iif(this.w_MVFLCOCO="-","+"," "))
      * --- Storno i saldi di magazzino e di commessa
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_MVFLCASC,'SLQTAPER','_Curs_DOC_DETT.MVQTAUM1',_Curs_DOC_DETT.MVQTAUM1,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MVFLRISE,'SLQTRPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_MVFLORDI,'SLQTOPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_MVFLIMPE,'SLQTIPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
        +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
        +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
               )
      else
        update (i_cTable) set;
            SLQTAPER = &i_cOp1.;
            ,SLQTRPER = &i_cOp2.;
            ,SLQTOPER = &i_cOp3.;
            ,SLQTIPER = &i_cOp4.;
            &i_ccchkf. ;
         where;
            SLCODICE = this.w_MVKEYSAL;
            and SLCODMAG = this.w_MVCODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_MVF2CASC,'SLQTAPER','_Curs_DOC_DETT.MVQTAUM1',_Curs_DOC_DETT.MVQTAUM1,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MVF2RISE,'SLQTRPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_MVF2ORDI,'SLQTOPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_MVF2IMPE,'SLQTIPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
        +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
        +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
               )
      else
        update (i_cTable) set;
            SLQTAPER = &i_cOp1.;
            ,SLQTRPER = &i_cOp2.;
            ,SLQTOPER = &i_cOp3.;
            ,SLQTIPER = &i_cOp4.;
            &i_ccchkf. ;
         where;
            SLCODICE = this.w_MVKEYSAL;
            and SLCODMAG = this.w_MVCODMAT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Storno i saldi commessa
      * --- Write into MA_COSTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.MA_COSTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_MVFLCOCO,'CSCONSUN','this.w_MVIMPCOM',this.w_MVIMPCOM,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MVFLORCO,'CSORDIN','this.w_MVIMPCOM',this.w_MVIMPCOM,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
        +",CSORDIN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSORDIN');
            +i_ccchkf ;
        +" where ";
            +"CSCODCOM = "+cp_ToStrODBC(this.w_MVCODCOM);
            +" and CSTIPSTR = "+cp_ToStrODBC(this.w_MVTIPATT);
            +" and CSCODMAT = "+cp_ToStrODBC(this.w_MVCODATT);
            +" and CSCODCOS = "+cp_ToStrODBC(this.w_MVCODCOS);
               )
      else
        update (i_cTable) set;
            CSCONSUN = &i_cOp1.;
            ,CSORDIN = &i_cOp2.;
            &i_ccchkf. ;
         where;
            CSCODCOM = this.w_MVCODCOM;
            and CSTIPSTR = this.w_MVTIPATT;
            and CSCODMAT = this.w_MVCODATT;
            and CSCODCOS = this.w_MVCODCOS;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Delete from CON_PAGA
      i_nConn=i_TableProp[this.CON_PAGA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_PAGA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CPSERIAL = "+cp_ToStrODBC(this.w_PDSERRIF);
               )
      else
        delete from (i_cTable) where;
              CPSERIAL = this.w_PDSERRIF;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from DOC_RATE
      i_nConn=i_TableProp[this.DOC_RATE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"RSSERIAL = "+cp_ToStrODBC(this.w_PDSERRIF);
               )
      else
        delete from (i_cTable) where;
              RSSERIAL = this.w_PDSERRIF;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARSALCOM"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARSALCOM;
          from (i_cTable) where;
              ARCODART = this.w_MVCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SALCOM="S"
        if empty(nvl(this.w_MVCODCOM,""))
          this.w_COMMAPPO = this.w_COMMDEFA
        else
          this.w_COMMAPPO = this.w_MVCODCOM
        endif
        * --- Write into SALDICOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_MVFLCASC,'SCQTAPER','_Curs_DOC_DETT.MVQTAUM1',_Curs_DOC_DETT.MVQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_MVFLRISE,'SCQTRPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_MVFLORDI,'SCQTOPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_MVFLIMPE,'SCQTIPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
          +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
          +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
          +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
              +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                 )
        else
          update (i_cTable) set;
              SCQTAPER = &i_cOp1.;
              ,SCQTRPER = &i_cOp2.;
              ,SCQTOPER = &i_cOp3.;
              ,SCQTIPER = &i_cOp4.;
              &i_ccchkf. ;
           where;
              SCCODICE = this.w_MVKEYSAL;
              and SCCODMAG = this.w_MVCODMAG;
              and SCCODCAN = this.w_COMMAPPO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Write into SALDICOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_MVF2CASC,'SCQTAPER','_Curs_DOC_DETT.MVQTAUM1',_Curs_DOC_DETT.MVQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_MVF2RISE,'SCQTRPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_MVF2ORDI,'SCQTOPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_MVF2IMPE,'SCQTIPER','_Curs_DOC_DETT.MVQTASAL',_Curs_DOC_DETT.MVQTASAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
          +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
          +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
          +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
              +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                 )
        else
          update (i_cTable) set;
              SCQTAPER = &i_cOp1.;
              ,SCQTRPER = &i_cOp2.;
              ,SCQTOPER = &i_cOp3.;
              ,SCQTIPER = &i_cOp4.;
              &i_ccchkf. ;
           where;
              SCCODICE = this.w_MVKEYSAL;
              and SCCODMAG = this.w_MVCODMAT;
              and SCCODCAN = this.w_COMMAPPO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Delete from DOC_DETT
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_PDSERRIF);
               )
      else
        delete from (i_cTable) where;
              MVSERIAL = this.w_PDSERRIF;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
        select _Curs_DOC_DETT
        continue
      enddo
      use
    endif
    * --- Delete from DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_PDSERRIF);
             )
    else
      delete from (i_cTable) where;
            MVSERIAL = this.w_PDSERRIF;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='SALDIART'
    this.cWorkTables[2]='MA_COSTI'
    this.cWorkTables[3]='PDA_DETT'
    this.cWorkTables[4]='DOC_DETT'
    this.cWorkTables[5]='DOC_MAST'
    this.cWorkTables[6]='ART_ICOL'
    this.cWorkTables[7]='SALDICOM'
    this.cWorkTables[8]='CON_PAGA'
    this.cWorkTables[9]='DOC_RATE'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
