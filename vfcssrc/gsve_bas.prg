* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bas                                                        *
*              Abbinamento spese - fatture                                     *
*                                                                              *
*      Author: Zucchetti SpA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_98]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-29                                                      *
* Last revis.: 2003-09-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bas",oParentObject,m.pTipo)
return(i_retval)

define class tgsve_bas as StdBatch
  * --- Local variables
  pTipo = space(10)
  w_MESS = space(10)
  w_IMPAGG = 0
  w_VALMAG = 0
  w_IMPCOM = 0
  w_VALCOM = space(3)
  w_DOCCAOVAL = space(3)
  w_DOCDECTOT = 0
  w_ROWNUM = 0
  w_NUMRIF = 0
  w_TOTAGG = 0
  w_SERDOC = space(10)
  w_TMPTIPOPE = space(10)
  w_FLCOCO = space(1)
  w_FLORCO = space(1)
  w_TIPATT = space(1)
  w_CODATT = space(1)
  w_CODCOM = space(1)
  w_CODCOS = space(5)
  w_COMDECTOT = 0
  w_SER_FAT_SPE = space(10)
  w_CODESE = space(4)
  w_DATREG = ctod("  /  /  ")
  w_CONACQ = ctod("  /  /  ")
  w_CONVEN = ctod("  /  /  ")
  w_FLVEAC = space(1)
  w_TOTMAG = 0
  w_LOOP = 0
  w_FLVEACZ = space(1)
  w_NUMDOCZ = 0
  w_ALFDOCZ = space(10)
  w_DATDOCZ = ctod("  /  /  ")
  w_TIPDOCZ = space(5)
  w_CODVALZ = space(3)
  w_TIPCONZ = space(1)
  w_CODCONZ = space(15)
  w_SERIALZ = space(10)
  w_DESINT2Z = space(40)
  w_DESCAU2Z = space(35)
  w_MVSERIAL = space(35)
  * --- WorkFile variables
  DOC_DETT_idx=0
  ADDE_SPE_idx=0
  VALUTE_idx=0
  DOC_MAST_idx=0
  TIP_DOCU_idx=0
  CONTI_idx=0
  CAN_TIER_idx=0
  MA_COSTI_idx=0
  ESERCIZI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dalla maschera GSVE_KRS
    do case
      case this.pTIpo="CONFERMA" And this.oParentObject.w_TIPOPE="N"
        * --- Scrive nel database le scritture dell'utente
        *     Controllo che il totale imputato corrisponda al valore totale della riga forfettaria
         
 Select ( this.oParentObject.w_ZOOM.cCursor ) 
 Sum Impagg To this.w_TOTAGG
        if ABS(this.w_TOTAGG)>ABS(this.oParentObject.w_MVVALMAG)
          this.w_MESS = "Il totale ripartito (%1) � maggiore del valore (%2) della spesa selezionata"
          ah_ErrorMsg(this.w_MESS,"!","", alltrim(TRAN(this.w_TOTAGG,"@Z"+V_PV[38+VVP])), alltrim(TRAN(this.oParentObject.w_MVVALMAG,"@Z"+V_PV[38+VVP])) )
          i_retcode = 'stop'
          return
        endif
        * --- Se non sono state effettuate modifiche manuali e non � stata calcolata la ripartizione automatica
        if this.w_TOTAGG=0
          this.w_MESS = "Attenzione! Nessun valore ripartito"
          ah_ErrorMsg(this.w_MESS,"!")
          i_retcode = 'stop'
          return
        endif
        * --- Inizio l'aggiornamento
        * --- Try
        local bErr_03D981C0
        bErr_03D981C0=bTrsErr
        this.Try_03D981C0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg(i_ErrMsg,"!","")
        endif
        bTrsErr=bTrsErr or bErr_03D981C0
        * --- End
      case this.pTIpo="ZOOM"
        this.oParentObject.w_ZOOM.Visible = this.oParentObject.w_TIPOPE="N"
        this.oParentObject.w_DISAB.Visible = this.oParentObject.w_TIPOPE="S"
        * --- La blankrec mi rimette la combo alla init mi mantengo il valore per reimpostarlo
        this.w_TMPTIPOPE = this.oParentObject.w_TIPOPE
        * --- Sbianco il Form quando cambio tipo di operazione
        This.oParentObject.BlankRec()
        this.oParentObject.w_TIPOPE = this.w_TMPTIPOPE
      case this.pTIpo="CONFERMA" And this.oParentObject.w_TIPOPE="S"
        * --- Try
        local bErr_03FA5FD0
        bErr_03FA5FD0=bTrsErr
        this.Try_03FA5FD0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("%1","!","", i_ErrMsg)
        endif
        bTrsErr=bTrsErr or bErr_03FA5FD0
        * --- End
      case this.pTIpo="TESTDOC" 
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVNUMDOC,MVALFDOC,MVDATDOC,MVTIPDOC,MVCODCON,MVCODVAL,MVVALNAZ"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_DOCSER);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVNUMDOC,MVALFDOC,MVDATDOC,MVTIPDOC,MVCODCON,MVCODVAL,MVVALNAZ;
            from (i_cTable) where;
                MVSERIAL = this.oParentObject.w_DOCSER;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
          this.oParentObject.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
          this.oParentObject.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
          this.oParentObject.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
          this.oParentObject.w_CODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
          this.oParentObject.w_CODVAL = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
          this.oParentObject.w_VALNAZ2 = NVL(cp_ToDate(_read_.MVVALNAZ),cp_NullValue(_read_.MVVALNAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_VALNAZ = this.oParentObject.w_VALNAZ2
        * --- Leggo la descrizione della causale
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDDESDOC"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_TIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDDESDOC;
            from (i_cTable) where;
                TDTIPDOC = this.oParentObject.w_TIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESCAU2 = NVL(cp_ToDate(_read_.TDDESDOC),cp_NullValue(_read_.TDDESDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Leggo la descrizione dell'intestatario
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANDESCRI"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANDESCRI;
            from (i_cTable) where;
                ANTIPCON = this.oParentObject.w_TIPCON;
                and ANCODICE = this.oParentObject.w_CODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESINT2 = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Leggo decimali
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECUNI"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_CODVAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECUNI;
            from (i_cTable) where;
                VACODVAL = this.oParentObject.w_CODVAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Non esegue la mCalc
        this.bUpdateParentObject = .F.
      case this.pTipo="RIPARTO"
         
 Select ( this.oParentObject.w_ZOOM.cCursor ) 
 Sum MVVALMAG To this.w_TOTMAG
        * --- Eseguo una Update sul cursore dello Zoom ripartendo
         
 Select ( this.oParentObject.w_ZOOM.cCursor ) 
 Go Top
        do while Not Eof()
           
 Replace IMPAGG With iif(this.w_TOTMAG=0,0,cp_Round(this.oParentObject.w_MVVALMAG * ( MVVALMAG / this.w_TOTMAG ), this.oParentObject.w_DECTOT ))
          Select ( this.oParentObject.w_ZOOM.cCursor )
          if Not Eof()
            Skip
          endif
        enddo
        * --- Aggiunto un eventuale resto sulla prima riga
         
 Select ( this.oParentObject.w_ZOOM.cCursor ) 
 Sum IMPAGG To this.w_TOTAGG
        if this.w_TOTAGG <> this.oParentObject.w_MVVALMAG
           
 Go top 
 Replace IMPAGG With IMPAGG + cp_Round(this.oParentObject.w_MVVALMAG - this.w_TOTAGG , this.oParentObject.w_DECTOT )
        endif
        * --- Aggiorno la Picture degli importi
        this.w_LOOP = 1
        do while this.w_LOOP <= this.oParentObject.w_ZOOM.grd.ColumnCount
          * --- This perch� non lo aggiunge nel codice
          if Upper ( this.oParentObject.w_ZOOM.grd.Columns( This.w_LOOP ).ControlSource ) = "IMPAGG"
            this.oParentObject.w_ZOOM.grd.Columns[ This.w_LOOP ].InputMask = cp_GetMask('"'+"@k "+v_PV(38+VVP)+'"') 
            this.w_LOOP = this.oParentObject.w_ZOOM.grd.ColumnCount+ 1
          endif
          this.w_LOOP = this.w_LOOP + 1
        enddo
         
 Select ( this.oParentObject.w_ZOOM.cCursor ) 
 Go Top
      case this.pTipo="DOCZOOM"
        vx_exec("Query\GSVE2KRS.vZM",this)
        this.oParentObject.w_NUMDOC = this.w_NUMDOCZ
        this.oParentObject.w_ALFDOC = this.w_ALFDOCZ
        this.oParentObject.w_DATDOC = this.w_DATDOCZ
        this.oParentObject.w_TIPDOC = this.w_TIPDOCZ
        this.oParentObject.w_CODVAL = this.w_CODVALZ
        this.oParentObject.w_TIPCON = this.w_TIPCONZ
        this.oParentObject.w_FLVEAC = this.w_FLVEACZ
        this.oParentObject.w_CODCON = this.w_CODCONZ
        this.oParentObject.w_SERIAL = this.w_SERIALZ
        this.oParentObject.w_DESINT2 = this.oParentObject.w_DESINT2
        this.oParentObject.w_DESCAU2 = this.w_DESCAU2Z
    endcase
  endproc
  proc Try_03D981C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Seriale Fattura spese.
    *     Per gestire lo zoom di selezione documenti su cui ripartire occorre w_MVSERIAL
    *     dichiarato come locale al batch, per cui non posso utilizzare w_MVSERIAL come caller
    this.w_SER_FAT_SPE = This.oParentObject.w_MVSERIAL
    * --- Leggo dall'archivio relazioni il totale ripartito per la riga fino ad ora
    * --- Select from ADDE_SPE
    i_nConn=i_TableProp[this.ADDE_SPE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ADDE_SPE_idx,2],.t.,this.ADDE_SPE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Sum(RLVALMAG) As VALMAG  from "+i_cTable+" ADDE_SPE ";
          +" where RLSERFAT= "+cp_ToStrODBC(this.w_SER_FAT_SPE)+" And RLNUMFAT= "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM)+" And RLROWFAT= "+cp_ToStrODBC(this.oParentObject.w_MVNUMRIF)+"";
           ,"_Curs_ADDE_SPE")
    else
      select Sum(RLVALMAG) As VALMAG from (i_cTable);
       where RLSERFAT= this.w_SER_FAT_SPE And RLNUMFAT= this.oParentObject.w_CPROWNUM And RLROWFAT= this.oParentObject.w_MVNUMRIF;
        into cursor _Curs_ADDE_SPE
    endif
    if used('_Curs_ADDE_SPE')
      select _Curs_ADDE_SPE
      locate for 1=1
      do while not(eof())
      * --- Al totale fino ad ora ripartito sommo il ripartito attuale
      this.w_TOTAGG = this.w_TOTAGG + Nvl( _Curs_ADDE_SPE.VALMAG ,0 )
        select _Curs_ADDE_SPE
        continue
      enddo
      use
    endif
    if ABS(this.w_TOTAGG)>ABS(this.oParentObject.w_GLVALMAG)
      * --- Raise
      i_Error="Fattura di spesa gia ripartita da altro utente, impossibile eseguire operazione"
      return
    endif
    * --- Marco la fattura come abbinata
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MV_FLAGG ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MV_FLAGG');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_SER_FAT_SPE);
          +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
          +" and MVNUMRIF = "+cp_ToStrODBC(this.oParentObject.w_MVNUMRIF);
             )
    else
      update (i_cTable) set;
          MV_FLAGG = "S";
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_SER_FAT_SPE;
          and CPROWNUM = this.oParentObject.w_CPROWNUM;
          and MVNUMRIF = this.oParentObject.w_MVNUMRIF;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Leggo tasso e decimali globali per valuta di conto documento aggiornato
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_VALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DOCCAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DOCDECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Scorro il cursore e per ogni riga creo una entrata in ADDE_SPE
     
 Select ( this.oParentObject.w_ZOOM.cCursor ) 
 Go Top
    do while Not Eof()
      this.w_ROWNUM = CPROWNUM
      this.w_NUMRIF = MVNUMRIF
      this.w_VALMAG = IMPAGG
      this.w_FLCOCO = Nvl ( FLCOCO ," " )
      this.w_FLORCO = Nvl ( FLORCO ," " )
      this.w_TIPATT = Nvl ( TIPATT ,"" )
      this.w_CODATT = Nvl ( CODATT ,"" )
      this.w_CODCOM = Nvl ( CODCOM ,"" )
      this.w_CODCOS = Nvl ( CODCOS ,"" )
      if this.w_VALMAG<>0
        * --- Inserisco l'importo traducendolo nella valuta di conto del documento che modifico
        if this.oParentObject.w_VALNAZ<>this.oParentObject.w_MVCODVAL
          * --- Se  MVCODVAL valuta non UE allora ambedue i documenti hanno la stessa valuta
          *     utilizzo il cambio sul documento di spesa, Come data documento passo la data odierna
          if g_APPLICATION = "ADHOC REVOLUTION"
            this.w_IMPAGG = VAL2MON(this.w_VALMAG,this.oParentObject.w_MVCAOVAL, this.w_DOCCAOVAL ,i_DATSYS,this.oParentObject.w_VALNAZ)
          else
            this.w_IMPAGG = VAL2MON(this.w_VALMAG,this.oParentObject.w_MVCAOVAL, this.w_DOCCAOVAL ,i_DATSYS,this.oParentObject.w_VALNAZ,this.w_DOCDECTOT)
          endif
        else
          this.w_IMPAGG = this.w_VALMAG
        endif
        * --- Calcoli per aggionramento produzione su commessa
        if g_COMM="S" And Not Empty( this.w_CODATT ) And Not Empty( this.w_CODCOS ) And Not Empty( this.w_FLORCO )
          * --- Raise
          i_Error="Il documento selezionato movimenta impegnato di commessa. Impossibile ripartire le spese"
          return
        endif
        if g_COMM="S" And Not Empty( this.w_CODATT ) And Not Empty( this.w_CODCOS ) And Not Empty( this.w_FLCOCO )
          * --- Read from CAN_TIER
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAN_TIER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNCODVAL"+;
              " from "+i_cTable+" CAN_TIER where ";
                  +"CNCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNCODVAL;
              from (i_cTable) where;
                  CNCODCAN = this.w_CODCOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_VALCOM = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VADECTOT"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_VALCOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VADECTOT;
              from (i_cTable) where;
                  VACODVAL = this.w_VALCOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_COMDECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_VALCOM<>this.oParentObject.w_VALNAZ
            this.w_IMPCOM = VAL2CAM(this.w_IMPAGG , this.oParentObject.w_VALNAZ , this.w_VALCOM , this.w_DOCCAOVAL , i_DATSYS , this.w_COMDECTOT)
          else
            this.w_IMPCOM = this.w_IMPAGG
          endif
          * --- Aggiorna i saldi di commessa
          this.w_IMPCOM = IIF( this.oParentObject.w_CATDOC="FA" , 1 , -1) * this.w_IMPCOM
          * --- Write into MA_COSTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MA_COSTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CSCONSUN =CSCONSUN+ "+cp_ToStrODBC(this.w_IMPCOM);
                +i_ccchkf ;
            +" where ";
                +"CSCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
                +" and CSTIPSTR = "+cp_ToStrODBC(this.w_TIPATT);
                +" and CSCODMAT = "+cp_ToStrODBC(this.w_CODATT);
                +" and CSCODCOS = "+cp_ToStrODBC(this.w_CODCOS);
                   )
          else
            update (i_cTable) set;
                CSCONSUN = CSCONSUN + this.w_IMPCOM;
                &i_ccchkf. ;
             where;
                CSCODCOM = this.w_CODCOM;
                and CSTIPSTR = this.w_TIPATT;
                and CSCODMAT = this.w_CODATT;
                and CSCODCOS = this.w_CODCOS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          this.w_IMPCOM = 0
        endif
        * --- Inserisco nuova entrata nel tabella di appoggio
        * --- Try
        local bErr_03D4F460
        bErr_03D4F460=bTrsErr
        this.Try_03D4F460()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Raise
          i_Error="Impossibile ripartire due volte la solita spesa sulla solita riga. Eseguire un'unica operazione"
          return
        endif
        bTrsErr=bTrsErr or bErr_03D4F460
        * --- End
        * --- aggiungo a MVIMPNAZ l'importo impostato
        this.w_IMPAGG = IIF( this.oParentObject.w_CATDOC="FA" , 1 , -1) * this.w_IMPAGG
        * --- In ad hoc REVOLUTION non esiste il campo MVIMPCOL (Importo Conto Lavoro)
        *     nel dettaglio documenti
        if g_APPLICATION = "ADHOC REVOLUTION"
          * --- Gestione ultimo prezzo / Ultimo Costo
          GSAR_BAC(this, this.oParentObject.w_SERIAL , this.w_ROWNUM , this.w_NUMRIF ,this.w_IMPAGG ,.T. )
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_cOp2=cp_SetTrsOp(this.w_FLCOCO,'MVIMPCOM','this.w_IMPCOM',this.w_IMPCOM,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVIMPNAZ =MVIMPNAZ+ "+cp_ToStrODBC(this.w_IMPAGG);
            +",MVIMPCOM ="+cp_NullLink(i_cOp2,'DOC_DETT','MVIMPCOM');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                   )
          else
            update (i_cTable) set;
                MVIMPNAZ = MVIMPNAZ + this.w_IMPAGG;
                ,MVIMPCOM = &i_cOp2.;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.oParentObject.w_SERIAL;
                and CPROWNUM = this.w_ROWNUM;
                and MVNUMRIF = this.w_NUMRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_cOp2=cp_SetTrsOp(this.w_FLCOCO,'MVIMPCOM','this.w_IMPCOM',this.w_IMPCOM,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVIMPNAZ =MVIMPNAZ+ "+cp_ToStrODBC(this.w_IMPAGG);
            +",MVIMPCOM ="+cp_NullLink(i_cOp2,'DOC_DETT','MVIMPCOM');
            +",MVIMPCOL =MVIMPCOL+ "+cp_ToStrODBC(this.w_IMPAGG);
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                   )
          else
            update (i_cTable) set;
                MVIMPNAZ = MVIMPNAZ + this.w_IMPAGG;
                ,MVIMPCOM = &i_cOp2.;
                ,MVIMPCOL = MVIMPCOL + this.w_IMPAGG;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.oParentObject.w_SERIAL;
                and CPROWNUM = this.w_ROWNUM;
                and MVNUMRIF = this.w_NUMRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
       
 Select ( this.oParentObject.w_ZOOM.cCursor )
      if Not Eof()
        Skip
      endif
    enddo
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Ripartizione terminata con successo","!")
    * --- Sbianco la maschera
    This.oParentObject.BlankRec()
    return
  proc Try_03FA5FD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACAOVAL,VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_VALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACAOVAL,VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_VALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DOCCAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.w_DOCDECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Scorro il cursore e per ogni riga creo una entrata in ADDE_SPE
     
 Select ( this.oParentObject.w_DISAB.cCursor ) 
 Go Top
    this.w_SER_FAT_SPE = This.oParentObject.w_MVSERIAL
    do while Not Eof()
      this.w_SERDOC = MVSERIAL
      * --- Verifico se la riga da stornare non appartiene a documenti con data
      *     antecedente alla data di consolidamento
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVCODESE,MVDATREG,MVFLVEAC"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVCODESE,MVDATREG,MVFLVEAC;
          from (i_cTable) where;
              MVSERIAL = this.w_SERDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODESE = NVL(cp_ToDate(_read_.MVCODESE),cp_NullValue(_read_.MVCODESE))
        this.w_DATREG = NVL(cp_ToDate(_read_.MVDATREG),cp_NullValue(_read_.MVDATREG))
        this.oParentObject.w_FLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESCONACQ,ESCONVEN"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(i_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.w_CODESE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESCONACQ,ESCONVEN;
          from (i_cTable) where;
              ESCODAZI = i_CODAZI;
              and ESCODESE = this.w_CODESE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CONACQ = NVL(cp_ToDate(_read_.ESCONACQ),cp_NullValue(_read_.ESCONACQ))
        this.w_CONVEN = NVL(cp_ToDate(_read_.ESCONVEN),cp_NullValue(_read_.ESCONVEN))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if ( this.oParentObject.w_FLVEAC="V" And this.w_CONVEN>=this.w_DATREG ) Or ( this.oParentObject.w_FLVEAC="A" And this.w_CONACQ>=this.w_DATREG )
         
 Select ( this.oParentObject.w_DISAB.cCursor )
        if this.oParentObject.w_FLVEAC="V"
          this.w_MESS = ah_Msgformat("Impossibile annullare ripartizione:%0Riga %1 appartiene ad un documento con data registrazione [%2] antecedente o uguale alla data di consolidamento ciclo vendite [%3]", ALLTRIM(STR(CPROWORD)), dtoc( this.w_DATREG), dtoc( this.w_CONVEN ) )
        else
          this.w_MESS = ah_Msgformat("Impossibile annullare ripartizione:%0Riga %1 appartiene ad un documento con data registrazione [%2] antecedente o uguale alla data di consolidamento ciclo acquisti [%3]", ALLTRIM(STR(CPROWORD)), dtoc( this.w_DATREG), dtoc( this.w_CONACQ ) )
        endif
        * --- Raise
        i_Error=this.w_MESS
        return
      endif
      this.w_ROWNUM = CPROWNUM
      this.w_NUMRIF = MVNUMRIF
      * --- Cambio di segno il flag aggiorna consuntivo di commessa
      this.w_FLCOCO = IIF (Nvl ( FLCOCO ,"" ) ="+" ,"-", IIF (Nvl ( FLCOCO ,"" ) ="-" ,"+", IIF (Nvl ( FLCOCO ,"" ) ="=" ,"=", " " ) ) )
      this.w_TIPATT = Nvl ( TIPATT ,"" )
      this.w_CODATT = Nvl ( CODATT ,"" )
      this.w_CODCOM = Nvl ( CODCOM ,"" )
      this.w_CODCOS = Nvl ( CODCOS ,"" )
      this.w_IMPAGG = IMPNAZ
      * --- Calcoli per aggiornamento produzione su commessa
      if g_COMM="S" And Not Empty( this.w_CODATT ) And Not Empty( this.w_CODCOS ) And Not Empty( this.w_FLCOCO )
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNCODVAL"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNCODVAL;
            from (i_cTable) where;
                CNCODCAN = this.w_CODCOM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_VALCOM = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_VALCOM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_VALCOM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COMDECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_VALCOM<>this.oParentObject.w_VALNAZ
          this.w_IMPCOM = VAL2CAM(this.w_IMPAGG , this.oParentObject.w_VALNAZ , this.w_VALCOM , this.w_DOCCAOVAL , i_DATSYS , this.w_COMDECTOT)
        else
          this.w_IMPCOM = this.w_IMPAGG
        endif
        * --- Aggiorna i saldi di commessa
        this.w_IMPCOM = IIF( this.oParentObject.w_CATDOC="FA" , 1 , -1) * this.w_IMPCOM
        * --- Write into MA_COSTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MA_COSTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CSCONSUN =CSCONSUN- "+cp_ToStrODBC(this.w_IMPCOM);
              +i_ccchkf ;
          +" where ";
              +"CSCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
              +" and CSTIPSTR = "+cp_ToStrODBC(this.w_TIPATT);
              +" and CSCODMAT = "+cp_ToStrODBC(this.w_CODATT);
              +" and CSCODCOS = "+cp_ToStrODBC(this.w_CODCOS);
                 )
        else
          update (i_cTable) set;
              CSCONSUN = CSCONSUN - this.w_IMPCOM;
              &i_ccchkf. ;
           where;
              CSCODCOM = this.w_CODCOM;
              and CSTIPSTR = this.w_TIPATT;
              and CSCODMAT = this.w_CODATT;
              and CSCODCOS = this.w_CODCOS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        this.w_IMPCOM = 0
      endif
      this.w_IMPAGG = IIF( this.oParentObject.w_CATDOC="FA" , 1 , -1) * this.w_IMPAGG
      * --- aggiungo a MVIMPNAZ l'importo impostato
      * --- In ad hoc REVOLUTION non esiste il campo MVIMPCOL (Importo Conto Lavoro)
      *     nel dettaglio documenti
      if g_APPLICATION = "ADHOC REVOLUTION"
        * --- Gestione ultimo prezzo / Ultimo Costo
        GSAR_BAC(this, this.w_SERDOC , this.w_ROWNUM , this.w_NUMRIF ,-this.w_IMPAGG ,.T. )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_cOp2=cp_SetTrsOp(this.w_FLCOCO,'MVIMPCOM','this.w_IMPCOM',this.w_IMPCOM,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVIMPNAZ =MVIMPNAZ- "+cp_ToStrODBC(this.w_IMPAGG);
          +",MVIMPCOM ="+cp_NullLink(i_cOp2,'DOC_DETT','MVIMPCOM');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOC);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                 )
        else
          update (i_cTable) set;
              MVIMPNAZ = MVIMPNAZ - this.w_IMPAGG;
              ,MVIMPCOM = &i_cOp2.;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERDOC;
              and CPROWNUM = this.w_ROWNUM;
              and MVNUMRIF = this.w_NUMRIF;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_cOp2=cp_SetTrsOp(this.w_FLCOCO,'MVIMPCOM','this.w_IMPCOM',this.w_IMPCOM,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVIMPNAZ =MVIMPNAZ- "+cp_ToStrODBC(this.w_IMPAGG);
          +",MVIMPCOM ="+cp_NullLink(i_cOp2,'DOC_DETT','MVIMPCOM');
          +",MVIMPCOL =MVIMPCOL- "+cp_ToStrODBC(this.w_IMPAGG);
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOC);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                 )
        else
          update (i_cTable) set;
              MVIMPNAZ = MVIMPNAZ - this.w_IMPAGG;
              ,MVIMPCOM = &i_cOp2.;
              ,MVIMPCOL = MVIMPCOL - this.w_IMPAGG;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERDOC;
              and CPROWNUM = this.w_ROWNUM;
              and MVNUMRIF = this.w_NUMRIF;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
       
 Select ( this.oParentObject.w_DISAB.cCursor )
      if Not Eof()
        Skip
      endif
    enddo
    * --- Elimino le entrate dall'archivio relazione
    * --- Delete from ADDE_SPE
    i_nConn=i_TableProp[this.ADDE_SPE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ADDE_SPE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"RLSERFAT = "+cp_ToStrODBC(this.w_SER_FAT_SPE);
            +" and RLNUMFAT = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
            +" and RLROWFAT = "+cp_ToStrODBC(this.oParentObject.w_MVNUMRIF);
             )
    else
      delete from (i_cTable) where;
            RLSERFAT = this.w_SER_FAT_SPE;
            and RLNUMFAT = this.oParentObject.w_CPROWNUM;
            and RLROWFAT = this.oParentObject.w_MVNUMRIF;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.w_TOTAGG = 0
    * --- Select from ADDE_SPE
    i_nConn=i_TableProp[this.ADDE_SPE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ADDE_SPE_idx,2],.t.,this.ADDE_SPE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Sum(RLVALMAG) As VALMAG  from "+i_cTable+" ADDE_SPE ";
          +" where RLSERFAT= "+cp_ToStrODBC(this.w_SER_FAT_SPE)+" And RLNUMFAT= "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM)+" And RLROWFAT= "+cp_ToStrODBC(this.oParentObject.w_MVNUMRIF)+"";
           ,"_Curs_ADDE_SPE")
    else
      select Sum(RLVALMAG) As VALMAG from (i_cTable);
       where RLSERFAT= this.w_SER_FAT_SPE And RLNUMFAT= this.oParentObject.w_CPROWNUM And RLROWFAT= this.oParentObject.w_MVNUMRIF;
        into cursor _Curs_ADDE_SPE
    endif
    if used('_Curs_ADDE_SPE')
      select _Curs_ADDE_SPE
      locate for 1=1
      do while not(eof())
      * --- Al totale fino ad ora ripartito sommo il ripartito attuale
      this.w_TOTAGG = Nvl( _Curs_ADDE_SPE.VALMAG ,0 )
        select _Curs_ADDE_SPE
        continue
      enddo
      use
    endif
    if this.w_TOTAGG=0
      * --- Rimuovo il flag abbinanta se spesa non ripartita su nessun documento
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MV_FLAGG ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MV_FLAGG');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SER_FAT_SPE);
            +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
            +" and MVNUMRIF = "+cp_ToStrODBC(this.oParentObject.w_MVNUMRIF);
               )
      else
        update (i_cTable) set;
            MV_FLAGG = " ";
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_SER_FAT_SPE;
            and CPROWNUM = this.oParentObject.w_CPROWNUM;
            and MVNUMRIF = this.oParentObject.w_MVNUMRIF;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Ripartizione annullata con successo","!")
    * --- Sbianco la maschera
    This.oParentObject.BlankRec()
    this.oParentObject.w_ZOOM.Visible = this.oParentObject.w_TIPOPE="N"
    this.oParentObject.w_DISAB.Visible = this.oParentObject.w_TIPOPE="S"
    return
  proc Try_03D4F460()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ADDE_SPE
    i_nConn=i_TableProp[this.ADDE_SPE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ADDE_SPE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ADDE_SPE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RLSERFAT"+",RLNUMFAT"+",RLROWFAT"+",RLSERDOC"+",RLNUMDOC"+",RLROWDOC"+",RLIMPNAZ"+",RLVALMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SER_FAT_SPE),'ADDE_SPE','RLSERFAT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'ADDE_SPE','RLNUMFAT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMRIF),'ADDE_SPE','RLROWFAT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERIAL),'ADDE_SPE','RLSERDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'ADDE_SPE','RLNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIF),'ADDE_SPE','RLROWDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IMPAGG),'ADDE_SPE','RLIMPNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALMAG),'ADDE_SPE','RLVALMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RLSERFAT',this.w_SER_FAT_SPE,'RLNUMFAT',this.oParentObject.w_CPROWNUM,'RLROWFAT',this.oParentObject.w_MVNUMRIF,'RLSERDOC',this.oParentObject.w_SERIAL,'RLNUMDOC',this.w_ROWNUM,'RLROWDOC',this.w_NUMRIF,'RLIMPNAZ',this.w_IMPAGG,'RLVALMAG',this.w_VALMAG)
      insert into (i_cTable) (RLSERFAT,RLNUMFAT,RLROWFAT,RLSERDOC,RLNUMDOC,RLROWDOC,RLIMPNAZ,RLVALMAG &i_ccchkf. );
         values (;
           this.w_SER_FAT_SPE;
           ,this.oParentObject.w_CPROWNUM;
           ,this.oParentObject.w_MVNUMRIF;
           ,this.oParentObject.w_SERIAL;
           ,this.w_ROWNUM;
           ,this.w_NUMRIF;
           ,this.w_IMPAGG;
           ,this.w_VALMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='ADDE_SPE'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='DOC_MAST'
    this.cWorkTables[5]='TIP_DOCU'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='CAN_TIER'
    this.cWorkTables[8]='MA_COSTI'
    this.cWorkTables[9]='ESERCIZI'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_ADDE_SPE')
      use in _Curs_ADDE_SPE
    endif
    if used('_Curs_ADDE_SPE')
      use in _Curs_ADDE_SPE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
