* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bgg                                                        *
*              Gestione Gadget                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-01-17                                                      *
* Last revis.: 2016-03-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper,pGadgetKey,pNoLoadGadget,pCodTheme,pAddType,pXCoord,pYCoord
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bgg",oParentObject,m.pOper,m.pGadgetKey,m.pNoLoadGadget,m.pCodTheme,m.pAddType,m.pXCoord,m.pYCoord)
return(i_retval)

define class tgsut_bgg as StdBatch
  * --- Local variables
  pOper = space(20)
  pGadgetKey = space(10)
  pNoLoadGadget = .f.
  pCodTheme = space(5)
  pAddType = 0
  pXCoord = 0
  pYCoord = 0
  w_PADRE = .NULL.
  w_CODICE = space(10)
  w_GACODICE = space(10)
  w_GA__NOME = space(50)
  w_GADESCRI = space(0)
  w_GASRCPRG = space(50)
  w_GACNDATT = space(254)
  w_GACHKATT = space(1)
  w_GAFORAZI = space(1)
  w_GAGRPCOD = space(5)
  w_GAUTEESC = 0
  w_TEST_F10 = space(1)
  w_TIPNEG = space(1)
  w_CODAZI = space(5)
  w_GADGET = .NULL.
  w_NGADG = 0
  w_CURSOR = space(10)
  w_CURSOR_LAST = space(20)
  w_YESNOMSG = space(50)
  w_UTECODE = 0
  w_UTENAME = space(20)
  w_GCCODICE = space(5)
  w_GCDESCRI = space(50)
  w_GGCODICE = space(5)
  w_GGDESCRI = space(30)
  w_ParamOK = .f.
  w_ErrorMsg = space(254)
  w_MGCODGAD = space(10)
  w_L_MGCHKCOL = space(1)
  w_MG_WIDTH = 0
  w_L_MGPIANOV = space(1)
  w_L_MGBCKCOL = 0
  w_L_MGDTHEME = space(5)
  w_L_MGHIDREF = space(1)
  w_L_MGEFTRAN = space(1)
  w_L_MGSALCNF = space(1)
  w_L_MG_TIMER = space(1)
  w_L_MG_PINMG = space(1)
  w_L_MG_START = space(1)
  w_L_MGORDGRP = space(100)
  w_L_MGDISRAP = space(1)
  w_ATTRIBUTE = space(50)
  w_EXPRATTR = space(50)
  w_FULLVALUE = space(254)
  w_VALUE = space(254)
  w_OLDVALUE = space(254)
  w_CharString = .f.
  w_InString = .f.
  w_SaveExpr = .f.
  w_CNDATT = space(254)
  w_FLGEXP = space(1)
  w_Attivazione = .f.
  w_AttErrMsg = space(254)
  w_ORDERGRP = space(100)
  w_Index = 0
  w_GADGET = .NULL.
  w_STANDARD = space(254)
  w_L_MGSALCNF = space(1)
  w_CODGRP = space(5)
  w_CURSORNAME = space(50)
  w_GA__CODE = space(10)
  w_GGACTGRP = space(5)
  w_IMAGE = space(252)
  w_APPLYTHEMEIMG = .f.
  w_FONTCLR = 0
  w_LABEL = space(252)
  w_TOOLTIP = space(252)
  w_DIMENSION = space(1)
  w_PROGRAM = space(252)
  w_SEC = space(50)
  w_PARAM = space(252)
  w_KEY = space(252)
  w_TITLE = space(254)
  w_HAVETITLE = .f.
  w_GADG = .NULL.
  w_GC__NOME = space(50)
  w_NUM = 0
  w_MAX = 0
  * --- WorkFile variables
  MYG_MAST_idx=0
  MYG_DETT_idx=0
  GAD_DETT_idx=0
  ELABKPIM_idx=0
  GAD_MAST_idx=0
  GAD_ACCE_idx=0
  TMP_GADG_idx=0
  GRP_GADG_idx=0
  MOD_GADG_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Parametro utilizzato da AddGadgetToCnt e per verificare se un area � attualmente in uso
    * --- pAddType = 0   inserimento singolo gadget
    *     pAddType = 1   inserimento multiplo di gadget tutti nella Home
    *     pAddType = 2   inserimento multiplo di gadget smistati nei relativi aree
    this.w_PADRE = This.oParentObject
    * --- Necessario per poter utilizzare &
    Local cProp
    do case
      case this.pOper=="New"
        this.w_PADRE.MarkPos()     
        this.w_PADRE.AddRow()     
        this.oParentObject.w_GA__PROP = "nCoordX1"
        this.oParentObject.w_GAVALORE = "10"
        this.oParentObject.w_GACODUTE = -1
        this.oParentObject.w_GA_RESET = "N"
        this.w_PADRE.SaveRow()     
        this.w_PADRE.AddRow()     
        this.oParentObject.w_GA__PROP = "nCoordY1"
        this.oParentObject.w_GAVALORE = "32"
        this.oParentObject.w_GACODUTE = -1
        this.oParentObject.w_GA_RESET = "N"
        this.w_PADRE.SaveRow()     
        this.w_PADRE.AddRow()     
        this.oParentObject.w_GA__PROP = "nCoordX2"
        this.oParentObject.w_GAVALORE = "258"
        this.oParentObject.w_GACODUTE = -1
        this.oParentObject.w_GA_RESET = "N"
        this.w_PADRE.SaveRow()     
        this.w_PADRE.AddRow()     
        this.oParentObject.w_GA__PROP = "nCoordY2"
        this.oParentObject.w_GAVALORE = "152"
        this.oParentObject.w_GACODUTE = -1
        this.oParentObject.w_GA_RESET = "N"
        this.w_PADRE.SaveRow()     
        this.w_PADRE.AddRow()     
        this.oParentObject.w_GA__PROP = "nBackColor"
        this.oParentObject.w_GAVALORE = Trans(i_ThemesManager.GetProp(109))
        this.oParentObject.w_GACODUTE = -1
        this.oParentObject.w_GA_RESET = "N"
        this.w_PADRE.SaveRow()     
        this.w_PADRE.AddRow()     
        this.oParentObject.w_GA__PROP = "nBorderColor"
        this.oParentObject.w_GAVALORE = Trans(i_ThemesManager.GetProp(109))
        this.oParentObject.w_GACODUTE = -1
        this.oParentObject.w_GA_RESET = "N"
        this.w_PADRE.SaveRow()     
        this.w_PADRE.AddRow()     
        this.oParentObject.w_GA__PROP = "nFontColor"
        this.oParentObject.w_GAVALORE = "-1"
        this.oParentObject.w_GACODUTE = -1
        this.oParentObject.w_GA_RESET = "N"
        this.w_PADRE.SaveRow()     
        this.w_PADRE.AddRow()     
        this.oParentObject.w_GA__PROP = "bSizable"
        this.oParentObject.w_GAVALORE = ".T."
        this.oParentObject.w_GACODUTE = -1
        this.oParentObject.w_GA_RESET = "N"
        this.w_PADRE.SaveRow()     
        this.w_PADRE.AddRow()     
        this.oParentObject.w_GA__PROP = "bMouseHover"
        this.oParentObject.w_GAVALORE = ".T."
        this.oParentObject.w_GACODUTE = -1
        this.oParentObject.w_GA_RESET = "N"
        this.w_PADRE.SaveRow()     
        this.w_PADRE.AddRow()     
        this.oParentObject.w_GA__PROP = "bUpdateOnTimer"
        this.oParentObject.w_GAVALORE = ".F."
        this.oParentObject.w_GACODUTE = -1
        this.oParentObject.w_GA_RESET = "N"
        this.w_PADRE.SaveRow()     
        this.w_PADRE.AddRow()     
        this.oParentObject.w_GA__PROP = "nUpdateInterval"
        this.oParentObject.w_GAVALORE = "0"
        this.oParentObject.w_GACODUTE = -1
        this.oParentObject.w_GA_RESET = "N"
        this.w_PADRE.SaveRow()     
        this.w_PADRE.AddRow()     
        this.oParentObject.w_GA__PROP = "bOptions"
        this.oParentObject.w_GAVALORE = ".F."
        this.oParentObject.w_GACODUTE = -1
        this.oParentObject.w_GA_RESET = "N"
        this.w_PADRE.SaveRow()     
        this.w_PADRE.AddRow()     
        this.oParentObject.w_GA__PROP = "cMskOptions"
        this.oParentObject.w_GAVALORE = "''"
        this.oParentObject.w_GACODUTE = -1
        this.oParentObject.w_GA_RESET = "N"
        this.w_PADRE.SaveRow()     
        this.w_PADRE.RePos()     
      case this.pOper=="Load"
        this.w_MGCODGAD = "XXXXXXXXXX"
        * --- Carica Attibuti
        * --- Read from MYG_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MYG_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MYG_MAST_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGCHKCOL,MG_WIDTH,MGPIANOV,MGBCKCOL,MGEFTRAN,MGSALCNF,MG_TIMER,MG_PINMG,MG_START,MGORDGRP,MGDTHEME,MGHIDREF,MGDISRAP"+;
            " from "+i_cTable+" MYG_MAST where ";
                +"MGCODUTE = "+cp_ToStrODBC(i_CODUTE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGCHKCOL,MG_WIDTH,MGPIANOV,MGBCKCOL,MGEFTRAN,MGSALCNF,MG_TIMER,MG_PINMG,MG_START,MGORDGRP,MGDTHEME,MGHIDREF,MGDISRAP;
            from (i_cTable) where;
                MGCODUTE = i_CODUTE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_L_MGCHKCOL = NVL(cp_ToDate(_read_.MGCHKCOL),cp_NullValue(_read_.MGCHKCOL))
          this.w_MG_WIDTH = NVL(cp_ToDate(_read_.MG_WIDTH),cp_NullValue(_read_.MG_WIDTH))
          this.w_L_MGPIANOV = NVL(cp_ToDate(_read_.MGPIANOV),cp_NullValue(_read_.MGPIANOV))
          this.w_L_MGBCKCOL = NVL(cp_ToDate(_read_.MGBCKCOL),cp_NullValue(_read_.MGBCKCOL))
          this.w_L_MGEFTRAN = NVL(cp_ToDate(_read_.MGEFTRAN),cp_NullValue(_read_.MGEFTRAN))
          this.w_L_MGSALCNF = NVL(cp_ToDate(_read_.MGSALCNF),cp_NullValue(_read_.MGSALCNF))
          this.w_L_MG_TIMER = NVL(cp_ToDate(_read_.MG_TIMER),cp_NullValue(_read_.MG_TIMER))
          this.w_L_MG_PINMG = NVL(cp_ToDate(_read_.MG_PINMG),cp_NullValue(_read_.MG_PINMG))
          this.w_L_MG_START = NVL(cp_ToDate(_read_.MG_START),cp_NullValue(_read_.MG_START))
          this.w_L_MGORDGRP = NVL(cp_ToDate(_read_.MGORDGRP),cp_NullValue(_read_.MGORDGRP))
          this.w_L_MGDTHEME = NVL(cp_ToDate(_read_.MGDTHEME),cp_NullValue(_read_.MGDTHEME))
          this.w_L_MGHIDREF = NVL(cp_ToDate(_read_.MGHIDREF),cp_NullValue(_read_.MGHIDREF))
          this.w_L_MGDISRAP = NVL(cp_ToDate(_read_.MGDISRAP),cp_NullValue(_read_.MGDISRAP))
          use
          if i_Rows=0
            * --- Restore setting charmbar
            this.w_L_MGCHKCOL = "N"
            this.w_L_MGPIANOV = " "
            this.w_L_MGBCKCOL = i_ThemesManager.GetProp(7)
            this.w_L_MGDTHEME = ""
            this.w_L_MGHIDREF = "N"
            this.w_L_MGEFTRAN = "B"
            this.w_L_MG_PINMG = " "
            this.w_L_MGSALCNF = " "
            this.w_L_MG_TIMER = "S"
            this.w_L_MG_PINMG = " "
            this.w_L_MG_START = "S"
            this.w_L_MGDISRAP = "N"
            * --- Se non ho una my gadget apro la library
            this.w_PADRE.bOpenGadgetLibrary = .T.
          endif
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PADRE.nWidthCollapse = this.w_MG_WIDTH
        this.w_PADRE.BackColor = this.w_L_MGBCKCOL
        this.w_PADRE.bArrangedFast = this.w_L_MGDISRAP="S"
        * --- Effetto animazione
        do case
          case this.w_L_MGEFTRAN = "L"
            this.w_PADRE.cEffect = "Linear"
          case this.w_L_MGEFTRAN = "B"
            this.w_PADRE.cEffect = "BounceEaseOut"
          case this.w_L_MGEFTRAN = "S"
            this.w_PADRE.cEffect = "SineEaseOut"
          case this.w_L_MGEFTRAN = "E"
            this.w_PADRE.cEffect = "ElasticEaseOut"
          case this.w_L_MGEFTRAN = "Q"
            this.w_PADRE.cEffect = "QuarticEaseInOut"
          case this.w_L_MGEFTRAN = "X"
            this.w_PADRE.cEffect = "ExponentialEaseInOut"
          otherwise
            this.w_PADRE.cEffect = ""
        endcase
        * --- Aggiungo CharmBar
        if !this.pNoLoadGadget
          this.w_PADRE.oCharmBar = GSUT_KMG(this.w_PADRE)
        endif
        this.w_PADRE.oCharmBar.w_MGPIANOV = this.w_L_MGPIANOV
        this.w_PADRE.oCharmBar.w_MGDTHEME = this.w_L_MGDTHEME
        this.w_PADRE.oCharmBar.w_MGHIDREF = this.w_L_MGHIDREF
        this.w_PADRE.oCharmBar.w_MGEFTRAN = this.w_L_MGEFTRAN
        this.w_PADRE.oCharmBar.w_MGSALCNF = this.w_L_MGSALCNF
        this.w_PADRE.oCharmBar.w_MGBCKCOL = this.w_L_MGBCKCOL
        this.w_PADRE.oCharmBar.w_MGCHKCOL = this.w_L_MGCHKCOL
        this.w_PADRE.oCharmBar.w_MG_TIMER = this.w_L_MG_TIMER
        this.w_PADRE.oCharmBar.w_MG_PINMG = this.w_L_MG_PINMG
        this.w_PADRE.oCharmBar.w_MG_START = this.w_L_MG_START
        this.w_PADRE.oCharmBar.w_MGDISRAP = this.w_L_MGDISRAP
        this.w_PADRE.oCharmBar.SetControlsValue()     
        this.w_PADRE.oCharmBar.GetColor(this.w_L_MGBCKCOL)     
        this.w_PADRE.oCharmBar.mCalc(.T.)     
        this.w_PADRE.oCharmBar.Move(_Screen.Width - this.w_PADRE.oCharmBar.Width + 5, 0)     
        this.w_PADRE.SetView(.F., this.w_L_MGCHKCOL<>"S", this.w_L_MGPIANOV="S", .T.)     
        this.w_PADRE.oCharmBar.oPgFrm.Page1.oPag.oBtn_1_6.CpPicture = IIF(this.w_PADRE.bOntop, "gd_back.bmp", "gd_front.bmp")
        this.w_PADRE.oCharmBar.oPgFrm.Page1.oPag.oBtn_1_6.ToolTiptext = IIF(this.w_PADRE.bOntop, cp_Translate("Porta in secondo piano"), cp_Translate("Porta in primo piano"))
        this.w_PADRE.oCharmBar.bPin = !(this.w_L_MG_PINMG = "S")
        this.w_PADRE.oCharmBar.Pin()     
        this.w_PADRE.oCharmBar.SetRegionOn()     
        * --- Forzo refresh
        INKEY(0.001, "H")
        this.w_GADGET = .NULL.
        this.w_CURSOR_LAST = "Last"+Sys(2015)
        if !this.pNoLoadGadget
          Messaggio=""
          this.w_AttErrMsg = ""
          * --- Select from QUERY\GSUT_BGG
          do vq_exec with 'QUERY\GSUT_BGG',this,'_Curs_QUERY_GSUT_BGG','',.f.,.t.
          if used('_Curs_QUERY_GSUT_BGG')
            select _Curs_QUERY_GSUT_BGG
            locate for 1=1
            do while not(eof())
            * --- MGCODGAD
            *     GASRCPRG
            *     GA__PROP
            *     GAVALORE
            if this.w_MGCODGAD # _Curs_QUERY_GSUT_BGG.MGCODGAD
              * --- Sposto il Gadget nella sua posizione
              if !ISNULL(this.w_GADGET)
                * --- Valorizzo le propriet� la cui valutazione � stata posticipata ( #LAST )
                if Empty(Messaggio) And Used(this.w_CURSOR_LAST)
                  l_OldArea = Select() 
 Select(this.w_CURSOR_LAST) 
 Go Top 
 Scan
                  this.w_ATTRIBUTE = Alltrim(ATTRIBUTE)
                  this.w_VALUE = Alltrim(VALUE)
                  this.w_FULLVALUE = Alltrim(FULLVALUE)
                  L_errsav=on("ERROR") 
 ON ERROR Messaggio=Messaggio+Chr(10)+Alltrim(ATTRIBUTE)+" = "+Alltrim(FULLVALUE)
                  * --- Valorizzo attributo
                  if !PEMSTATUS(this.w_GADGET, this.w_ATTRIBUTE, 5)
                    this.w_GADGET.AddProperty(this.w_ATTRIBUTE, Eval(this.w_VALUE))     
                  else
                    cProp = this.w_ATTRIBUTE 
 this.w_GADGET.&cProp = Eval(this.w_VALUE)
                  endif
                  on error &L_errsav 
 Select(this.w_CURSOR_LAST) 
 EndScan 
 
 Use in Select(this.w_CURSOR_LAST) 
 Select(m.l_OldArea)
                endif
                * --- Controllo la presenza di eventuali errori
                if !Empty(Messaggio)
                  Ah_ErrorMsg("Errore durante la valutazione attributi del gadget (%1):%2",.F.,.F., this.w_MGCODGAD, m.Messaggio)
                  this.w_PADRE.RemoveGadget(this.w_GADGET, .T.)     
                else
                  this.w_PADRE.SetRect(this.w_GADGET.Name, this.w_GADGET.nCoordX1, this.w_GADGET.nCoordY1, this.w_GADGET.nCoordX2, this.w_GADGET.nCoordY2)     
                  this.w_GADGET.mCalc(.T.)     
                  this.w_GADGET.SaveDependsOn()     
                  * --- valorizzate tutte le propriet� chiamo il metodo che le applica prima di passare al gadget successivo
                  this.w_GADGET.ApplyTheme()     
                  this.w_GADGET.NotifyEvent("GadgetOnInit")     
                  * --- Abilito la notifica dell'evento "GadgetArranged"
                  this.w_GADGET.bEventArranged = .T.
                  if this.w_GADGET.nPage = this.w_PADRE.nActivePage And !this.w_PADRE.bArrangedFast 
                    this.w_GADGET.Show()     
                  else
                    this.w_GADGET.Hide()     
                  endif
                  if Nvl(this.w_FLGEXP,"S")=="N"
                    this.w_GADGET.bInitCollapsed = .T.
                  endif
                  * --- Verifico se � consentito l'accesso alle opzioni e valorizzo la relativa propriet� bOptionsAccess
                  * --- Select from QUERY\GSUT3KGS
                  local _hAINQUEYDAA
                  _hAINQUEYDAA=createobject('prm_container')
                  addproperty(_hAINQUEYDAA,'w_TIPNEG',"M")
                  do vq_exec with 'QUERY\GSUT3KGS',_hAINQUEYDAA,'_Curs_QUERY_GSUT3KGS','',.f.,.t.
                  if used('_Curs_QUERY_GSUT3KGS')
                    select _Curs_QUERY_GSUT3KGS
                    locate for 1=1
                    do while not(eof())
                    if Alltrim(this.w_MGCODGAD)==Alltrim(_Curs_QUERY_GSUT3KGS.AGCODGAD)
                      this.w_GADGET.bOptionsAccess = .F.
                      exit
                    endif
                      select _Curs_QUERY_GSUT3KGS
                      continue
                    enddo
                    use
                  endif
                endif
                Messaggio=""
              endif
              * --- Aggiungo nuovo gadget
              this.w_MGCODGAD = _Curs_QUERY_GSUT_BGG.MGCODGAD
              this.w_CNDATT = Alltrim(Nvl(_Curs_QUERY_GSUT_BGG.GACNDATT,""))
              this.w_FLGEXP = Alltrim(Nvl(_Curs_QUERY_GSUT_BGG.MGFLGEXP,""))
              * --- Verifico che il Gadget rispetti la condizione di attivazione
              Local l_oldError,l_errMsg 
 m.l_oldError = On("Error") 
 l_ErrMsg="" 
 On Error l_ErrMsg=Message()
              this.w_ATTIVAZIONE = Empty(this.w_CNDATT) Or Eval(this.w_CNDATT)
              On Error &l_oldError
              if !Empty(m.l_ErrMsg)
                this.w_Attivazione = .F.
                this.w_ErrorMsg = ah_MsgFormat("Codice: %1%0Condizione: %2%0Errore: %3", this.w_MGCODGAD, this.w_CNDATT, m.l_ErrMsg)
                this.w_AttErrMsg = Iif(Empty(this.w_AttErrMsg), this.w_ErrorMsg, ah_MsgFormat("%1%0------------------------------%0%2", this.w_AttErrMsg, this.w_ErrorMsg))
              endif
              if this.w_ATTIVAZIONE
                this.w_GADGET = this.w_PADRE.AddGadget(_Curs_QUERY_GSUT_BGG.GASRCPRG, .T., _Curs_QUERY_GSUT_BGG.MGCODGRP, _Curs_QUERY_GSUT_BGG.GGDESCRI)
                * --- Chiave gadget necessaria in fase di salvataggio
                this.w_GADGET.cKeyGadget = this.w_MGCODGAD
                * --- ToolTip da mostrare sul titolo
                this.w_GADGET.cToolTipGadget = Alltrim(Nvl(_Curs_QUERY_GSUT_BGG.GADESCRI,""))
                * --- Inserisco spazi in fondo per essere sicuro che la visualizzazione
                *     delle Info mostri sempre la stessa maschera cp_showmessages
                this.w_GADGET.cToolTipGadget = Iif(Len(this.w_GADGET.cToolTipGadget)<1024, Padr(this.w_GADGET.cToolTipGadget,1025," "), this.w_GADGET.cToolTipGadget)
                * --- Codice area originale, utilizzato per lo sposta nel area
                this.w_GADGET.cSourceGroup = Alltrim(_Curs_QUERY_GSUT_BGG.GAGRPCOD)
                this.w_GADGET.cSourceGroupDes = Alltrim(_Curs_QUERY_GSUT_BGG.GGDESORI)
                this.w_GADGET.bCompany = _Curs_QUERY_GSUT_BGG.GAFORAZI="S"
              else
                this.w_GADGET = .NULL.
              endif
            endif
            * --- Procedo con la valorizzazione degli attributi solo se il gadget � stato effettivamente creato
            this.w_ATTRIBUTE = Alltrim(_Curs_QUERY_GSUT_BGG.GA__PROP)
            this.w_VALUE = Alltrim(_Curs_QUERY_GSUT_BGG.GAVALORE)
            this.w_FULLVALUE = Alltrim(_Curs_QUERY_GSUT_BGG.GAVALORE)
            if Alltrim(_Curs_QUERY_GSUT_BGG.GAVALORE)<>CHR(39)+CHR(39)
              * --- Problema apici
              this.w_VALUE = STRTRAN(this.w_VALUE, CHR(39)+CHR(39),"'+CHR(39)+'")
            endif
            if !ISNULL(this.w_GADGET)
              * --- Cancello eventuali commenti nel valore
              if At(" #HELP ",Upper(this.w_VALUE))>0
                this.w_VALUE = Alltrim(Left(this.w_VALUE,At(" #HELP ",Upper(this.w_VALUE))-1))
              endif
              * --- Controllo propriet� non personalizzabile
              if At("#CONST",Upper(this.w_VALUE))>0
                this.w_VALUE = Alltrim(Strtran(this.w_VALUE,"#CONST","",1,1,1))
              endif
              * --- Controllo se valore parametrico
              this.w_SaveExpr = .F.
              if At("w_",Lower(this.w_VALUE))>0
                this.w_OLDVALUE = this.w_VALUE
                this.w_VALUE = ""
                this.w_InString = .F.
                For l_idx=1 To Len(this.w_OLDVALUE)
                if this.w_InString
                  if this.w_CharString="'" And SubStr(this.w_OLDVALUE,m.l_idx,1)="'" Or this.w_CharString='"' And SubStr(this.w_OLDVALUE,m.l_idx,1)='"' Or this.w_CharString="[" And SubStr(this.w_OLDVALUE,m.l_idx,1)="]"
                    this.w_InString = .F.
                  endif
                  this.w_VALUE = this.w_VALUE + SubStr(this.w_OLDVALUE,m.l_idx,1)
                else
                  * --- Controllo se sto entrando in una stringa
                  if InList(SubStr(this.w_OLDVALUE,m.l_idx,1), '"', "'", "[")
                    this.w_CharString = SubStr(this.w_OLDVALUE,m.l_idx,1)
                    this.w_InString = .T.
                  endif
                  if Lower(SubStr(this.w_OLDVALUE,m.l_idx,1))=="w" And Lower(SubStr(this.w_OLDVALUE,m.l_idx,2))=="w_"
                    this.w_VALUE = this.w_VALUE + "This.w_GADGET."
                    this.w_SaveExpr = .T.
                  endif
                  this.w_VALUE = this.w_VALUE + SubStr(this.w_OLDVALUE,m.l_idx,1)
                endif
                EndFor
              endif
              * --- Controllo se attributo da valutare alla fine
              if At("#LAST",Upper(this.w_VALUE))>0
                if !Used(this.w_CURSOR_LAST)
                  Create Cursor (this.w_CURSOR_LAST) (ATTRIBUTE C(50), VALUE M, FULLVALUE C(254))
                endif
                Insert into (this.w_CURSOR_LAST) Values(this.w_ATTRIBUTE, Alltrim(Strtran(this.w_VALUE,"#LAST","",1,1,1)), this.w_FULLVALUE)
              else
                * --- Verifico errori di valutaizone degli attributi
                 
 L_errsav=on("ERROR") 
 ON ERROR Messaggio=Messaggio+Chr(10)+Alltrim(_Curs_QUERY_GSUT_BGG.GA__PROP)+" = "+Alltrim(_Curs_QUERY_GSUT_BGG.GAVALORE)
                * --- Valorizzo attributo
                if !PEMSTATUS(this.w_GADGET, this.w_ATTRIBUTE, 5)
                  this.w_GADGET.AddProperty(this.w_ATTRIBUTE, Eval(this.w_VALUE))     
                else
                  cProp = this.w_ATTRIBUTE 
 this.w_GADGET.&cProp = Eval(this.w_VALUE)
                endif
                * --- Ripristino la On Error
                on error &L_errsav
              endif
            endif
            * --- Se necessario memorizzo anche l'espressione di valutazione della propriet�
            if this.w_SaveExpr
              * --- Costruisco il nome della nuova propriet� (es: w_TITLE --> e_TITLE)
              this.w_EXPRATTR = "expr_"+Alltrim(this.w_ATTRIBUTE)
              this.w_VALUE = Strtran(this.w_VALUE,"#LAST","",1,1,1)
              this.w_VALUE = Strtran(this.w_VALUE, "This.w_GADGET.","This.")
              if !PEMSTATUS(this.w_GADGET, this.w_EXPRATTR, 5)
                this.w_GADGET.AddProperty(this.w_EXPRATTR, Alltrim(this.w_VALUE))     
              else
                cProp = this.w_EXPRATTR 
 this.w_GADGET.&cProp = Alltrim(this.w_VALUE)
              endif
            endif
              select _Curs_QUERY_GSUT_BGG
              continue
            enddo
            use
          endif
          * --- Sposto l'ultimo Gadget nella sua posizione
          if !ISNULL(this.w_GADGET)
            * --- Valorizzo le propriet� la cui valutazione � stata posticipata ( #LAST )
            if Empty(Messaggio) And Used(this.w_CURSOR_LAST)
              l_OldArea = Select() 
 Select(this.w_CURSOR_LAST) 
 Go Top 
 Scan
              this.w_ATTRIBUTE = Alltrim(ATTRIBUTE)
              this.w_VALUE = Alltrim(VALUE)
              L_errsav=on("ERROR") 
 ON ERROR Messaggio=Messaggio+Chr(10)+Alltrim(ATTRIBUTE)+" = "+Alltrim(FULLVALUE)
              * --- Valorizzo attributo
              if !PEMSTATUS(this.w_GADGET, this.w_ATTRIBUTE, 5)
                this.w_GADGET.AddProperty(this.w_ATTRIBUTE, Eval(this.w_VALUE))     
              else
                cProp = this.w_ATTRIBUTE 
 this.w_GADGET.&cProp = Eval(this.w_VALUE)
              endif
              on error &L_errsav 
 Select(this.w_CURSOR_LAST) 
 EndScan 
 
 Use in Select(this.w_CURSOR_LAST) 
 Select(m.l_OldArea)
            endif
            * --- Controllo la presenza di eventuali errori
            if !Empty(Messaggio)
              Ah_ErrorMsg("Errore durante la valutazione attributi del gadget (%1):%2",.F.,.F., this.w_GADGET.cKeyGadget, m.Messaggio)
              this.w_PADRE.RemoveGadget(this.w_GADGET, .T.)     
            else
              * --- Applico valori delle propriet� per tematizzazione
              this.w_GADGET.ApplyTheme()     
              this.w_PADRE.SetRect(this.w_GADGET.Name, this.w_GADGET.nCoordX1, this.w_GADGET.nCoordY1, this.w_GADGET.nCoordX2, this.w_GADGET.nCoordY2)     
              this.w_GADGET.mCalc(.T.)     
              this.w_GADGET.SaveDependsOn()     
              this.w_GADGET.NotifyEvent("GadgetOnInit")     
              * --- Abilito la notifica dell'evento "GadgetArranged"
              this.w_GADGET.bEventArranged = .T.
              if this.w_GADGET.nPage = this.w_PADRE.nActivePage And !this.w_PADRE.bArrangedFast
                this.w_GADGET.Show()     
              else
                this.w_GADGET.Hide()     
              endif
              if Nvl(this.w_FLGEXP,"S")=="N"
                this.w_GADGET.bInitCollapsed = .T.
              endif
              * --- Verifico se � consentito l'accesso alle opzioni e valorizzo la relativa propriet� bOptionsAccess
              * --- Select from QUERY\GSUT3KGS
              local _hQRSMJTCZXD
              _hQRSMJTCZXD=createobject('prm_container')
              addproperty(_hQRSMJTCZXD,'w_TIPNEG',"M")
              do vq_exec with 'QUERY\GSUT3KGS',_hQRSMJTCZXD,'_Curs_QUERY_GSUT3KGS','',.f.,.t.
              if used('_Curs_QUERY_GSUT3KGS')
                select _Curs_QUERY_GSUT3KGS
                locate for 1=1
                do while not(eof())
                if Alltrim(this.w_MGCODGAD)==Alltrim(_Curs_QUERY_GSUT3KGS.AGCODGAD)
                  this.w_GADGET.bOptionsAccess = .F.
                  exit
                endif
                  select _Curs_QUERY_GSUT3KGS
                  continue
                enddo
                use
              endif
            endif
            Messaggio=""
          endif
          * --- Riposizionamento di tutti i gadget
          this.w_PADRE.ArrangeAllGadgets()     
          * --- Notifico eventuali errori nella valutazione delel condizioni di attivazione dei vari gadget
          if !Empty(this.w_AttErrMsg)
            Ah_ErrorMsg("Errore durante la valutazione della condizione di attivazione di alcuni gadget:%0%0%1",.F.,.F., this.w_AttErrMsg)
          endif
        endif
        * --- *--- Aggiornamento automatico
        this.w_PADRE.bUpdateOnTimer = this.w_L_MG_TIMER="S"
        * --- *--- Aggiornamento automatico
        this.w_PADRE.bUpdateHiddenGadget = this.w_L_MGHIDREF="S"
        * --- *--- Ordinamento aree
        Alines(this.w_PADRE.aOrderPage, this.w_L_MGORDGRP, 5, ";")
        this.w_GADGET = .NULL.
      case this.pOper=="Save"
        * --- Costruisco la stringa con l'ordinamento delle pagine
        this.w_ORDERGRP = ""
        For Each cPage in this.w_PADRE.aOrderPage 
 this.w_ORDERGRP = Iif(Vartype(cPage)=="C", this.w_ORDERGRP+Alltrim(cPage)+";", this.w_ORDERGRP) 
 Endfor
        this.w_ORDERGRP = Left(this.w_ORDERGRP, Len(this.w_ORDERGRP)-1)
        this.w_L_MGCHKCOL = IIF(this.w_PADRE.bFullScreen, "N", "S")
        * --- Effettuo l'update, se fallisce faccio l'insert
        * --- Try
        local bErr_04070558
        bErr_04070558=bTrsErr
        this.Try_04070558()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04070558
        * --- End
        * --- Write into MYG_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MYG_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MYG_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MYG_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MG_WIDTH ="+cp_NullLink(cp_ToStrODBC(this.w_PADRE.nWidthCollapse),'MYG_MAST','MG_WIDTH');
          +",MGBCKCOL ="+cp_NullLink(cp_ToStrODBC(this.w_PADRE.BackColor),'MYG_MAST','MGBCKCOL');
          +",MGCHKCOL ="+cp_NullLink(cp_ToStrODBC(this.w_L_MGCHKCOL),'MYG_MAST','MGCHKCOL');
          +",MGORDGRP ="+cp_NullLink(cp_ToStrODBC(this.w_ORDERGRP),'MYG_MAST','MGORDGRP');
              +i_ccchkf ;
          +" where ";
              +"MGCODUTE = "+cp_ToStrODBC(i_CODUTE);
                 )
        else
          update (i_cTable) set;
              MG_WIDTH = this.w_PADRE.nWidthCollapse;
              ,MGBCKCOL = this.w_PADRE.BackColor;
              ,MGCHKCOL = this.w_L_MGCHKCOL;
              ,MGORDGRP = this.w_ORDERGRP;
              &i_ccchkf. ;
           where;
              MGCODUTE = i_CODUTE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_Index = 1
        * --- Carica Attibuti
        do while this.w_Index <= this.w_PADRE.aGadget.Count
          this.w_GADGET = this.w_PADRE.aGadget[this.w_Index]
          if this.w_GADGET.bCompany
            this.w_CODAZI = i_CODAZI
          else
            this.w_CODAZI = "xxx  "
          endif
          * --- Select from GAD_DETT
          i_nConn=i_TableProp[this.GAD_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select GA__PROP,GAVALORE,GA_RESET  from "+i_cTable+" GAD_DETT ";
                +" where GACODICE = "+cp_ToStrODBC(this.w_GADGET.cKeyGadget)+" AND GACODUTE=-1";
                 ,"_Curs_GAD_DETT")
          else
            select GA__PROP,GAVALORE,GA_RESET from (i_cTable);
             where GACODICE = this.w_GADGET.cKeyGadget AND GACODUTE=-1;
              into cursor _Curs_GAD_DETT
          endif
          if used('_Curs_GAD_DETT')
            select _Curs_GAD_DETT
            locate for 1=1
            do while not(eof())
            cProp = Alltrim(_Curs_GAD_DETT.GA__PROP)
            this.w_VALUE = this.w_GADGET.&cProp
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_STANDARD = Alltrim(_Curs_GAD_DETT.GAVALORE)
            * --- Cancello eventuali commenti nel valore
            if At(" #HELP ",Upper(Alltrim(this.w_STANDARD)))>0
              this.w_STANDARD = Alltrim(Left(this.w_STANDARD,At(" #HELP ",Upper(this.w_STANDARD))-1))
            endif
            * --- Cancello eventuali ashtag prima della valutazione
            if At("#LAST",Upper(Alltrim(this.w_STANDARD)))>0
              this.w_STANDARD = Alltrim(Strtran(this.w_STANDARD,"#LAST","",1,1,1))
            endif
            * --- Controllo se valore parametrico
            if At("w_",Lower(this.w_STANDARD))>0
              this.w_OLDVALUE = this.w_STANDARD
              this.w_STANDARD = ""
              this.w_InString = .F.
              For l_idx=1 To Len(this.w_OLDVALUE)
              if this.w_InString
                if this.w_CharString="'" And SubStr(this.w_OLDVALUE,m.l_idx,1)="'" Or this.w_CharString='"' And SubStr(this.w_OLDVALUE,m.l_idx,1)='"' Or this.w_CharString="[" And SubStr(this.w_OLDVALUE,m.l_idx,1)="]"
                  this.w_InString = .F.
                endif
                this.w_STANDARD = this.w_STANDARD + SubStr(this.w_OLDVALUE,m.l_idx,1)
              else
                * --- Controllo se sto entrando in una stringa
                if InList(SubStr(this.w_OLDVALUE,m.l_idx,1), '"', "'", "[")
                  this.w_CharString = SubStr(this.w_OLDVALUE,m.l_idx,1)
                  this.w_InString = .T.
                endif
                if Lower(SubStr(this.w_OLDVALUE,m.l_idx,1))=="w" And Lower(SubStr(this.w_OLDVALUE,m.l_idx,2))=="w_"
                  this.w_STANDARD = this.w_STANDARD + "This.w_GADGET."
                endif
                this.w_STANDARD = this.w_STANDARD + SubStr(this.w_OLDVALUE,m.l_idx,1)
              endif
              EndFor
            endif
            * --- rivaluto la propriet� standard per non personalizare variabili o funzioni con il loro valore
            if At("#CONST",Upper(Alltrim(this.w_STANDARD)))>0 Or Alltrim(this.w_STANDARD) == this.w_VALUE Or ((Vartype(Eval(Alltrim(this.w_STANDARD)))=="C" And; 
 Alltrim(Eval(Alltrim(this.w_STANDARD)))==Alltrim(Eval(this.w_VALUE))) Or Eval(Alltrim(this.w_STANDARD))==Eval(this.w_VALUE))
              * --- Cancello la personalizzazione dell'attributo per il mio utente, il valore di default � sufficente
              * --- Delete from GAD_DETT
              i_nConn=i_TableProp[this.GAD_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"GACODICE = "+cp_ToStrODBC(this.w_GADGET.cKeyGadget);
                      +" and GA__PROP = "+cp_ToStrODBC(_Curs_GAD_DETT.GA__PROP);
                      +" and GACODUTE = "+cp_ToStrODBC(i_CODUTE);
                      +" and GACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                       )
              else
                delete from (i_cTable) where;
                      GACODICE = this.w_GADGET.cKeyGadget;
                      and GA__PROP = _Curs_GAD_DETT.GA__PROP;
                      and GACODUTE = i_CODUTE;
                      and GACODAZI = this.w_CODAZI;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
            else
              * --- Insert into GAD_DETT
              i_nConn=i_TableProp[this.GAD_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
              i_commit = .f.
              local bErr_0404FFF0
              bErr_0404FFF0=bTrsErr
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GAD_DETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"GACODICE"+",GA__PROP"+",GAVALORE"+",GACODUTE"+",GACODAZI"+",GA_RESET"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_GADGET.cKeyGadget),'GAD_DETT','GACODICE');
                +","+cp_NullLink(cp_ToStrODBC(_Curs_GAD_DETT.GA__PROP),'GAD_DETT','GA__PROP');
                +","+cp_NullLink(cp_ToStrODBC(this.w_VALUE),'GAD_DETT','GAVALORE');
                +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'GAD_DETT','GACODUTE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_CODAZI),'GAD_DETT','GACODAZI');
                +","+cp_NullLink(cp_ToStrODBC(_Curs_GAD_DETT.GA_RESET),'GAD_DETT','GA_RESET');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GADGET.cKeyGadget,'GA__PROP',_Curs_GAD_DETT.GA__PROP,'GAVALORE',this.w_VALUE,'GACODUTE',i_CODUTE,'GACODAZI',this.w_CODAZI,'GA_RESET',_Curs_GAD_DETT.GA_RESET)
                insert into (i_cTable) (GACODICE,GA__PROP,GAVALORE,GACODUTE,GACODAZI,GA_RESET &i_ccchkf. );
                   values (;
                     this.w_GADGET.cKeyGadget;
                     ,_Curs_GAD_DETT.GA__PROP;
                     ,this.w_VALUE;
                     ,i_CODUTE;
                     ,this.w_CODAZI;
                     ,_Curs_GAD_DETT.GA_RESET;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                bTrsErr=bErr_0404FFF0
                * --- Write into GAD_DETT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.GAD_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.GAD_DETT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"GAVALORE ="+cp_NullLink(cp_ToStrODBC(this.w_VALUE),'GAD_DETT','GAVALORE');
                  +",GA_RESET ="+cp_NullLink(cp_ToStrODBC(_Curs_GAD_DETT.GA_RESET),'GAD_DETT','GA_RESET');
                      +i_ccchkf ;
                  +" where ";
                      +"GACODICE = "+cp_ToStrODBC(this.w_GADGET.cKeyGadget);
                      +" and GACODUTE = "+cp_ToStrODBC(i_CODUTE);
                      +" and GACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                      +" and GA__PROP = "+cp_ToStrODBC(_Curs_GAD_DETT.GA__PROP);
                         )
                else
                  update (i_cTable) set;
                      GAVALORE = this.w_VALUE;
                      ,GA_RESET = _Curs_GAD_DETT.GA_RESET;
                      &i_ccchkf. ;
                   where;
                      GACODICE = this.w_GADGET.cKeyGadget;
                      and GACODUTE = i_CODUTE;
                      and GACODAZI = this.w_CODAZI;
                      and GA__PROP = _Curs_GAD_DETT.GA__PROP;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
              select _Curs_GAD_DETT
              continue
            enddo
            use
          endif
          this.w_Index = this.w_Index + 1
        enddo
        this.w_GADGET = .NULL.
        * --- Aggiungo/Rimuovo gadget dalla mygadget utilizzando il cursore di appoggio
        * --- Remove gadget from table
        * --- leggo impostazioni utente
        * --- Read from MYG_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MYG_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MYG_MAST_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGSALCNF"+;
            " from "+i_cTable+" MYG_MAST where ";
                +"MGCODUTE = "+cp_ToStrODBC(i_CODUTE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGSALCNF;
            from (i_cTable) where;
                MGCODUTE = i_CODUTE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_L_MGSALCNF = NVL(cp_ToDate(_read_.MGSALCNF),cp_NullValue(_read_.MGSALCNF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if Used(this.w_PADRE.cCursorModify)
          Select (this.w_PADRE.cCursorModify)
          Locate for 1=1
          do while not(eof())
            this.w_CODAZI = cCompany
            this.w_CODGRP = cGroup
            do case
              case cAction = "I"
                * --- Insert into MYG_DETT
                i_nConn=i_TableProp[this.MYG_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MYG_DETT_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MYG_DETT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"MGCODUTE"+",MGCODGAD"+",MGCODAZI"+",MGCODGRP"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC(i_CODUTE),'MYG_DETT','MGCODUTE');
                  +","+cp_NullLink(cp_ToStrODBC(cKeyGadget),'MYG_DETT','MGCODGAD');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODAZI),'MYG_DETT','MGCODAZI');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODGRP),'MYG_DETT','MGCODGRP');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'MGCODUTE',i_CODUTE,'MGCODGAD',cKeyGadget,'MGCODAZI',this.w_CODAZI,'MGCODGRP',this.w_CODGRP)
                  insert into (i_cTable) (MGCODUTE,MGCODGAD,MGCODAZI,MGCODGRP &i_ccchkf. );
                     values (;
                       i_CODUTE;
                       ,cKeyGadget;
                       ,this.w_CODAZI;
                       ,this.w_CODGRP;
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
              case cAction = "R"
                if this.w_L_MGSALCNF <> "S"
                  * --- Cancello gli attributi dell'utente
                  * --- Delete from GAD_DETT
                  i_nConn=i_TableProp[this.GAD_DETT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                          +"GACODICE = "+cp_ToStrODBC(cKeyGadget);
                          +" and GACODUTE = "+cp_ToStrODBC(i_CODUTE);
                          +" and GACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                           )
                  else
                    delete from (i_cTable) where;
                          GACODICE = cKeyGadget;
                          and GACODUTE = i_CODUTE;
                          and GACODAZI = this.w_CODAZI;

                    i_Rows=_tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    * --- Error: delete not accepted
                    i_Error=MSG_DELETE_ERROR
                    return
                  endif
                endif
                * --- Cancello il codice del gadget dalla MyGadget dell'utente
                * --- Delete from MYG_DETT
                i_nConn=i_TableProp[this.MYG_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MYG_DETT_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"MGCODUTE = "+cp_ToStrODBC(i_CODUTE);
                        +" and MGCODGAD = "+cp_ToStrODBC(cKeyGadget);
                        +" and MGCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                         )
                else
                  delete from (i_cTable) where;
                        MGCODUTE = i_CODUTE;
                        and MGCODGAD = cKeyGadget;
                        and MGCODAZI = this.w_CODAZI;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error=MSG_DELETE_ERROR
                  return
                endif
            endcase
            Select (this.w_PADRE.cCursorModify)
            Continue
          enddo
          * --- Svuoto il cursore
          Select (this.w_PADRE.cCursorModify)
          DELETE ALL
        endif
      case this.pOper=="SaveSetting"
        * --- Salva settaggi MyGadget
        * --- Insert into MYG_MAST
        i_nConn=i_TableProp[this.MYG_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MYG_MAST_idx,2])
        i_commit = .f.
        local bErr_0404BF70
        bErr_0404BF70=bTrsErr
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MYG_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MGCODUTE"+",MGBCKCOL"+",MGCHKCOL"+",MGPIANOV"+",MGEFTRAN"+",MGSALCNF"+",MG_TIMER"+",MG_PINMG"+",MG_START"+",MGDTHEME"+",MGHIDREF"+",MGDISRAP"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(i_CODUTE),'MYG_MAST','MGCODUTE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGBCKCOL),'MYG_MAST','MGBCKCOL');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGCHKCOL),'MYG_MAST','MGCHKCOL');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGPIANOV),'MYG_MAST','MGPIANOV');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGEFTRAN),'MYG_MAST','MGEFTRAN');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGSALCNF),'MYG_MAST','MGSALCNF');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MG_TIMER),'MYG_MAST','MG_TIMER');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MG_PINMG),'MYG_MAST','MG_PINMG');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MG_START),'MYG_MAST','MG_START');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGDTHEME),'MYG_MAST','MGDTHEME');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGHIDREF),'MYG_MAST','MGHIDREF');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGDISRAP),'MYG_MAST','MGDISRAP');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MGCODUTE',i_CODUTE,'MGBCKCOL',this.oParentObject.w_MGBCKCOL,'MGCHKCOL',this.oParentObject.w_MGCHKCOL,'MGPIANOV',this.oParentObject.w_MGPIANOV,'MGEFTRAN',this.oParentObject.w_MGEFTRAN,'MGSALCNF',this.oParentObject.w_MGSALCNF,'MG_TIMER',this.oParentObject.w_MG_TIMER,'MG_PINMG',this.oParentObject.w_MG_PINMG,'MG_START',this.oParentObject.w_MG_START,'MGDTHEME',this.oParentObject.w_MGDTHEME,'MGHIDREF',this.oParentObject.w_MGHIDREF,'MGDISRAP',this.oParentObject.w_MGDISRAP)
          insert into (i_cTable) (MGCODUTE,MGBCKCOL,MGCHKCOL,MGPIANOV,MGEFTRAN,MGSALCNF,MG_TIMER,MG_PINMG,MG_START,MGDTHEME,MGHIDREF,MGDISRAP &i_ccchkf. );
             values (;
               i_CODUTE;
               ,this.oParentObject.w_MGBCKCOL;
               ,this.oParentObject.w_MGCHKCOL;
               ,this.oParentObject.w_MGPIANOV;
               ,this.oParentObject.w_MGEFTRAN;
               ,this.oParentObject.w_MGSALCNF;
               ,this.oParentObject.w_MG_TIMER;
               ,this.oParentObject.w_MG_PINMG;
               ,this.oParentObject.w_MG_START;
               ,this.oParentObject.w_MGDTHEME;
               ,this.oParentObject.w_MGHIDREF;
               ,this.oParentObject.w_MGDISRAP;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          bTrsErr=bErr_0404BF70
          * --- Write into MYG_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MYG_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MYG_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MYG_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MGBCKCOL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGBCKCOL),'MYG_MAST','MGBCKCOL');
            +",MGCHKCOL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGCHKCOL),'MYG_MAST','MGCHKCOL');
            +",MGPIANOV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGPIANOV),'MYG_MAST','MGPIANOV');
            +",MGEFTRAN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGEFTRAN),'MYG_MAST','MGEFTRAN');
            +",MGSALCNF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGSALCNF),'MYG_MAST','MGSALCNF');
            +",MG_TIMER ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MG_TIMER),'MYG_MAST','MG_TIMER');
            +",MG_PINMG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MG_PINMG),'MYG_MAST','MG_PINMG');
            +",MG_START ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MG_START),'MYG_MAST','MG_START');
            +",MGDTHEME ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGDTHEME),'MYG_MAST','MGDTHEME');
            +",MGHIDREF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGHIDREF),'MYG_MAST','MGHIDREF');
            +",MGDISRAP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MGDISRAP),'MYG_MAST','MGDISRAP');
                +i_ccchkf ;
            +" where ";
                +"MGCODUTE = "+cp_ToStrODBC(i_CODUTE);
                   )
          else
            update (i_cTable) set;
                MGBCKCOL = this.oParentObject.w_MGBCKCOL;
                ,MGCHKCOL = this.oParentObject.w_MGCHKCOL;
                ,MGPIANOV = this.oParentObject.w_MGPIANOV;
                ,MGEFTRAN = this.oParentObject.w_MGEFTRAN;
                ,MGSALCNF = this.oParentObject.w_MGSALCNF;
                ,MG_TIMER = this.oParentObject.w_MG_TIMER;
                ,MG_PINMG = this.oParentObject.w_MG_PINMG;
                ,MG_START = this.oParentObject.w_MG_START;
                ,MGDTHEME = this.oParentObject.w_MGDTHEME;
                ,MGHIDREF = this.oParentObject.w_MGHIDREF;
                ,MGDISRAP = this.oParentObject.w_MGDISRAP;
                &i_ccchkf. ;
             where;
                MGCODUTE = i_CODUTE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Aggiorno il manager richiamando il "Load"
        *     parent.oParentObject = ogadgetmanager
        GSUT_BGG(this.w_PADRE.oParentObject, "Load", "",.T.)
      case this.pOper=="RestoreSetting"
        * --- Restore setting charmbar
        this.oParentObject.w_MGCHKCOL = "S"
        this.oParentObject.w_MGPIANOV = " "
        this.oParentObject.w_MGBCKCOL = i_ThemesManager.GetProp(7)
        this.oParentObject.GetColor(this.oParentObject.w_MGBCKCOL)
        this.oParentObject.w_MGEFTRAN = "B"
        this.oParentObject.w_MG_PINMG = " "
        this.oParentObject.w_MGSALCNF = " "
        this.oParentObject.w_MG_TIMER = "S"
        this.oParentObject.w_MG_PINMG = " "
        this.oParentObject.w_MG_START = "S"
        this.oParentObject.w_MGDTHEME = ""
        this.oParentObject.w_MGHIDREF = "N"
        this.oParentObject.w_MGDISRAP = "N"
      case this.pOper=="AddFromLibrary"
        this.w_CURSORNAME = "NewGadget"+Sys(2015)
        this.w_CURSOR_LAST = "Last"+Sys(2015)
        this.w_GADGET = .NULL.
        this.w_GA__CODE = this.pGadgetKey
        Messaggio=""
        VQ_EXEC("QUERY\GSUTABGG.VQR", This, this.w_CURSORNAME)
        SELECT(this.w_CURSORNAME)
        LOCATE FOR 1=1
        do while !Eof()
          * --- GASRCPRG
          *     GA__PROP
          *     GAVALORE
          *     GAFORAZI
          this.w_ATTRIBUTE = Alltrim(GA__PROP)
          this.w_VALUE = Alltrim(GAVALORE)
          this.w_FULLVALUE = Alltrim(GAVALORE)
          if this.w_VALUE<>CHR(39)+CHR(39)
            * --- Problema apici
            this.w_VALUE = STRTRAN(this.w_VALUE, CHR(39)+CHR(39), "'+CHR(39)+'")
          endif
          this.w_GAFORAZI = Alltrim(GAFORAZI)
          if ISNULL(this.w_GADGET)
            * --- Aggiungo nuovo gadget
            this.w_GADGET = this.w_PADRE.AddGadget(GASRCPRG, .F., Iif(this.pAddType==1, .Null., Alltrim(GAGRPCOD)), Alltrim(GGDESCRI), (this.pAddType==0))
            * --- Chiave gadget necessaria in fase di salvataggio
            this.w_GADGET.cKeyGadget = this.pGadgetKey
            * --- Codice area originale, utilizzato per lo sposta nel area
            SELECT(this.w_CURSORNAME)
            * --- ToolTip da mostrare sul titolo
            this.w_GADGET.cToolTipGadget = Alltrim(Nvl(GADESCRI,""))
            * --- Inserisco spazi in fondo per essere sicuro che la visualizzazione
            *     delle Info mostri sempre la stessa maschera cp_showmessages
            this.w_GADGET.cToolTipGadget = Iif(Len(this.w_GADGET.cToolTipGadget)<1024, Padr(this.w_GADGET.cToolTipGadget,1025," "), this.w_GADGET.cToolTipGadget)
            this.w_GADGET.cSourceGroup = Alltrim(GAGRPCOD)
            this.w_GADGET.cSourceGroupDes = Alltrim(GGDESCRI)
            this.w_GADGET.bCompany = this.w_GAFORAZI="S"
          endif
          * --- Cancello eventuali commenti nel valore
          if At(" #HELP ",Upper(this.w_VALUE))>0
            this.w_VALUE = Alltrim(Left(this.w_VALUE,At(" #HELP ",Upper(this.w_VALUE))-1))
          endif
          * --- Controllo propriet� non personalizzabile
          if At("#CONST",Upper(this.w_VALUE))>0
            this.w_VALUE = Alltrim(Strtran(this.w_VALUE,"#CONST","",1,1,1))
          endif
          * --- Controllo se valore parametrico
          this.w_SaveExpr = .F.
          if At("w_",Lower(this.w_VALUE))>0
            this.w_OLDVALUE = this.w_VALUE
            this.w_VALUE = ""
            this.w_InString = .F.
            For l_idx=1 To Len(this.w_OLDVALUE)
            if this.w_InString
              if this.w_CharString="'" And SubStr(this.w_OLDVALUE,m.l_idx,1)="'" Or this.w_CharString='"' And SubStr(this.w_OLDVALUE,m.l_idx,1)='"' Or this.w_CharString="[" And SubStr(this.w_OLDVALUE,m.l_idx,1)="]"
                this.w_InString = .F.
              endif
              this.w_VALUE = this.w_VALUE + SubStr(this.w_OLDVALUE,m.l_idx,1)
            else
              * --- Controllo se sto entrando in una stringa
              if InList(SubStr(this.w_OLDVALUE,m.l_idx,1), '"', "'", "[")
                this.w_CharString = SubStr(this.w_OLDVALUE,m.l_idx,1)
                this.w_InString = .T.
              endif
              if Lower(SubStr(this.w_OLDVALUE,m.l_idx,1))=="w" And Lower(SubStr(this.w_OLDVALUE,m.l_idx,2))=="w_"
                this.w_VALUE = this.w_VALUE + "This.w_GADGET."
                this.w_SaveExpr = .T.
              endif
              this.w_VALUE = this.w_VALUE + SubStr(this.w_OLDVALUE,m.l_idx,1)
            endif
            EndFor
          endif
          * --- Controllo se attributo da valutare alla fine
          if At("#LAST",Upper(this.w_VALUE))>0
            if !Used(this.w_CURSOR_LAST)
              Create Cursor (this.w_CURSOR_LAST) (ATTRIBUTE C(50), VALUE M, FULLVALUE C(254))
            endif
            SELECT(this.w_CURSORNAME)
            Insert into (this.w_CURSOR_LAST) Values(this.w_ATTRIBUTE, Alltrim(Strtran(this.w_VALUE,"#LAST","",1,1,1)), this.w_FULLVALUE)
          else
            * --- Verifico errori di valutaizone degli attributi
             
 L_errsav=on("ERROR") 
 ON ERROR Messaggio=Messaggio+Chr(10)+Alltrim(GA__PROP)+" = "+Alltrim(GAVALORE)
            * --- Valorizzo attributo
            if !PEMSTATUS(this.w_GADGET, this.w_ATTRIBUTE, 5)
              this.w_GADGET.AddProperty(this.w_ATTRIBUTE, Eval(this.w_VALUE))     
            else
              cProp = this.w_ATTRIBUTE 
 this.w_GADGET.&cProp = Eval(this.w_VALUE)
            endif
            * --- Ripristino la On Error
            on error &L_errsav
          endif
          * --- Se necessario memorizzo anche l'espressione di valutazione della propriet�
          if this.w_SaveExpr
            * --- Costruisco il nome della nuova propriet� (es: w_TITLE --> e_TITLE)
            this.w_EXPRATTR = "expr_"+Alltrim(this.w_ATTRIBUTE)
            this.w_VALUE = Strtran(this.w_VALUE,"#LAST","",1,1,1)
            this.w_VALUE = Strtran(this.w_VALUE, "This.w_GADGET.","This.")
            if !PEMSTATUS(this.w_GADGET, this.w_EXPRATTR, 5)
              this.w_GADGET.AddProperty(this.w_EXPRATTR, Alltrim(this.w_VALUE))     
            else
              cProp = this.w_EXPRATTR 
 this.w_GADGET.&cProp = Alltrim(this.w_VALUE)
            endif
          endif
          SELECT(this.w_CURSORNAME)
          CONTINUE
        enddo
        USE IN SELECT(this.w_CURSORNAME)
        * --- Sposto il Gadget nella sua posizione
        if !ISNULL(this.w_GADGET)
          * --- Valorizzo le propriet� la cui valutazione � stata posticipata ( #LAST )
          if Empty(Messaggio) And Used(this.w_CURSOR_LAST)
            l_OldArea = Select() 
 Select(this.w_CURSOR_LAST) 
 Go Top 
 Scan
            this.w_ATTRIBUTE = Alltrim(ATTRIBUTE)
            this.w_VALUE = Alltrim(VALUE)
            this.w_FULLVALUE = Alltrim(FULLVALUE)
            L_errsav=on("ERROR") 
 ON ERROR Messaggio=Messaggio+Chr(10)+Alltrim(ATTRIBUTE)+" = "+Alltrim(FULLVALUE)
            * --- Valorizzo attributo
            if !PEMSTATUS(this.w_GADGET, this.w_ATTRIBUTE, 5)
              this.w_GADGET.AddProperty(this.w_ATTRIBUTE, Eval(this.w_VALUE))     
            else
              cProp = this.w_ATTRIBUTE 
 this.w_GADGET.&cProp = Eval(this.w_VALUE)
            endif
            on error &L_errsav 
 Select(this.w_CURSOR_LAST) 
 EndScan 
 
 Use in Select(this.w_CURSOR_LAST) 
 Select(m.l_OldArea)
          endif
          if !Empty(Messaggio)
            Ah_ErrorMsg("Errore durante la valutazione attributi del gadget (%1):%2",.F.,.F., this.w_GA__CODE, m.Messaggio)
            this.w_PADRE.RemoveGadget(this.w_GADGET)     
          else
            * --- Verifico se � stato passato un tema da applicare
            if Vartype(this.pCodTheme)=="C" And !Empty(this.pCodTheme)
              this.Pag3(this.w_GADGET.Name,this.pCodTheme)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- Applico valori delle propriet� lette
              this.w_GADGET.ApplyTheme()     
            endif
            * --- Verifico se � consentito l'accesso alle opzioni e valorizzo la relativa propriet� bOptionsAccess
            * --- Select from QUERY\GSUT3KGS
            local _hMXAZZJPLDG
            _hMXAZZJPLDG=createobject('prm_container')
            addproperty(_hMXAZZJPLDG,'w_TIPNEG',"M")
            do vq_exec with 'QUERY\GSUT3KGS',_hMXAZZJPLDG,'_Curs_QUERY_GSUT3KGS','',.f.,.t.
            if used('_Curs_QUERY_GSUT3KGS')
              select _Curs_QUERY_GSUT3KGS
              locate for 1=1
              do while not(eof())
              if Alltrim(this.pGadgetKey)==Alltrim(_Curs_QUERY_GSUT3KGS.AGCODGAD)
                this.w_GADGET.bOptionsAccess = .F.
                exit
              endif
                select _Curs_QUERY_GSUT3KGS
                continue
              enddo
              use
            endif
            this.w_PADRE.MoveGadget(this.w_GADGET.Name, this.w_GADGET.nCoordX1, this.w_GADGET.nCoordY1, this.w_GADGET.nCoordX2, this.w_GADGET.nCoordY2)     
            this.w_GADGET.mCalc(.T.)     
            this.w_GADGET.NotifyEvent("GadgetOnInit")     
            * --- Aggiungo il gadget alla mygadget
            this.w_PADRE.SetActionModify(this.w_GADGET, "I")     
            this.w_GADGET.ModifyMode(.T.)     
            this.w_GADGET.bEventArranged = .T.
            * --- Posiziono in base alla griglia
            this.w_PADRE.ArrangePositionGadget(this.w_GADGET.Name, this.pXCoord, this.pYCoord)     
          endif
        endif
        this.w_GADGET = .NULL.
      case this.pOper=="RemoveFromLibrary"
        this.w_CODICE = this.pGadgetKey
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper=="PageUse"
        * --- Select from MYG_DETT
        i_nConn=i_TableProp[this.MYG_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MYG_DETT_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select count(*) as count  from "+i_cTable+" MYG_DETT ";
              +" where Upper(Ltrim(Rtrim(MGCODGRP)))=Upper(Ltrim(Rtrim("+cp_ToStrODBC(this.pGadgetKey)+")))";
               ,"_Curs_MYG_DETT")
        else
          select count(*) as count from (i_cTable);
           where Upper(Ltrim(Rtrim(MGCODGRP)))=Upper(Ltrim(Rtrim(this.pGadgetKey)));
            into cursor _Curs_MYG_DETT
        endif
        if used('_Curs_MYG_DETT')
          select _Curs_MYG_DETT
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_NGADGRP = _Curs_MYG_DETT.count
            select _Curs_MYG_DETT
            continue
          enddo
          use
        endif
      case this.pOper=="ApplyTheme"
        this.Pag3(this.pGadgetKey,this.pCodTheme)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper=="Delete" Or this.pOper=="RestoreProperties" Or this.pOper=="Exclusive"Or this.pOper=="Generic" Or this.pOper=="SetGroup" Or this.pOper=="ResetGroup"
        this.w_NGADG = -1
        this.w_CURSOR = this.w_PADRE.w_ZoomGadg.cCursor
        Local l_Count 
 Select(this.w_CURSOR) 
 Count For XCHK=1 To m.l_Count 
 
        this.w_UTENAME = ""
        this.w_GGCODICE = ""
        this.w_ParamOK = .t.
        do case
          case this.pOper="Delete"
            this.w_YESNOMSG = "Cancellare"
          case this.pOper="RestoreProperties"
            vx_exec("QUERY/GSUTUKGG.VZM",this)
            this.w_YESNOMSG = "Ripristinare i valori di default per"
            this.w_ParamOK = !Empty(this.w_UTENAME)
            this.w_UTENAME = " per l'utente "+Alltrim(this.w_UTENAME)
          case this.pOper=="Exclusive"
            vx_exec("QUERY/GSUTUKGG.VZM",this)
            this.w_YESNOMSG = "Rendere  esclusivi "
            this.w_ParamOK = !Empty(this.w_UTENAME)
            this.w_UTENAME = " per l'utente "+Alltrim(this.w_UTENAME)
          case this.pOper=="Generic"
            this.w_YESNOMSG = "Rendere generici "
            this.W_UTECODE = 0
          case this.pOper=="SetGroup"
            vx_exec("QUERY/GSUTGKGG.VZM",this)
            this.w_YESNOMSG = "Abbinare "
            this.w_ParamOK = !Empty(this.w_GGCODICE)
            this.w_UTENAME = " all'area "+Alltrim(this.w_GGDESCRI)
          case this.pOper=="ResetGroup"
            this.w_YESNOMSG = "Disabbinare dalla propria area "
            this.w_GGCODICE = ""
        endcase
        if this.w_ParamOK
          if l_Count>0 And Ah_YesNo("%1 %2 gadget%3?","?", this.w_YESNOMSG, Trans(l_Count), this.w_UTENAME)
            this.w_NGADG = 0
            Select(this.w_CURSOR) 
 Go Top 
 Scan for XCHK==1
            this.w_CODICE = Alltrim(GACODICE)
            do case
              case this.pOper="Delete" And INUSE=="N"
                this.Pag4()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case this.pOper="RestoreProperties" And !Empty(this.w_UTENAME)
                this.Pag5()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case (this.pOper=="Exclusive" And INUSE=="N") Or this.pOper="Generic"
                this.Pag6(this.w_UTECODE)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              case (this.pOper=="SetGroup" Or this.pOper=="ResetGroup") And INUSE=="N"
                this.Pag7(this.w_GGCODICE)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
            endcase
            Select (this.w_CURSOR) 
 Endscan
          else
            * --- Se l_Count=0 si prende in considerazione il gadget corrente
            if l_Count=0
              do case
                case this.pOper="Exclusive"
                  this.w_YESNOMSG = "Rendere  esclusivo "
                case this.pOper=="Generic"
                  this.w_YESNOMSG = "Rendere generico "
              endcase
              this.w_CODICE = Alltrim(Nvl(this.pGadgetKey,""))
              if ! Empty(this.w_CODICE)
                if Ah_YesNo("%1 il gadget: %2%3?","?", this.w_YESNOMSG, this.w_CODICE, this.w_UTENAME)
                  this.w_NGADG = 0
                  do case
                    case this.pOper="Delete" And Nvl(this.oParentObject.w_INUSO,"N")=="N"
                      this.Pag4()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    case this.pOper="RestoreProperties"
                      this.Pag5()
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    case (this.pOper=="Exclusive" And Nvl(this.oParentObject.w_INUSO,"N")=="N") Or this.pOper="Generic"
                      this.Pag6(this.w_UTECODE)
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                    case (this.pOper=="SetGroup" Or this.pOper=="ResetGroup") And Nvl(this.oParentObject.w_INUSO,"N")=="N"
                      this.Pag7(this.w_GGCODICE)
                      if i_retcode='stop' or !empty(i_Error)
                        return
                      endif
                  endcase
                endif
              else
                Ah_ErrorMsg("Nessun gadget selezionato","!")
              endif
            endif
          endif
        endif
        this.w_YESNOMSG = iCase(this.pOper=="Delete", "cancellati", this.pOper=="RestoreProperties", "ripristinati", this.pOper=="Exclusive" And this.w_NGADG==1, "reso esclusivo", this.pOper=="Exclusive" And this.w_NGADG<>1, "resi esclusivi", this.pOper=="Generic" And this.w_NGADG==1, "reso generico", this.pOper=="Generic" And this.w_NGADG<>1, "resi generici")
        this.w_YESNOMSG = iCase(this.pOper=="SetGroup" And this.w_NGADG==1, "abbinato", this.pOper=="SetGroup" And this.w_NGADG<>1, "abbinati", this.pOper=="ResetGroup" And this.w_NGADG==1, "disabbinato dalla propria area", this.pOper=="ResetGroup" And this.w_NGADG<>1, "disabbinati dalla propria area", this.w_YESNOMSG)
        if this.w_NGADG>=0
          Ah_ErrorMsg("%1 gadget %2%3","i","",Trans(this.w_NGADG), this.w_YESNOMSG, this.w_UTENAME)
          if this.w_NGADG>0
            this.w_PADRE.NotifyEvent("AggiornaCruscotto")     
            if !Empty(Alltrim(Nvl(this.pGadgetKey,""))) And this.pOper<>"Delete"
              Select(this.w_CURSOR) 
 Locate For Alltrim(GACODICE)=Alltrim(this.pGadgetKey) 
 this.w_PADRE.w_ZoomGadg.Refresh()
            endif
            * --- Se ho cancellato tutti i gadget chiedo se si vuole svuotare anche l'archivio delle aree
            if this.pOper="Delete"
              * --- Select from GAD_MAST
              i_nConn=i_TableProp[this.GAD_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.GAD_MAST_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select Count(*) As nGadgCount  from "+i_cTable+" GAD_MAST ";
                     ,"_Curs_GAD_MAST")
              else
                select Count(*) As nGadgCount from (i_cTable);
                  into cursor _Curs_GAD_MAST
              endif
              if used('_Curs_GAD_MAST')
                select _Curs_GAD_MAST
                locate for 1=1
                do while not(eof())
                this.w_NGADG = Nvl(nGadgCount,0)
                  select _Curs_GAD_MAST
                  continue
                enddo
                use
              endif
              if this.w_NGADG=0 And Ah_YesNo("L'archivio gadget � vuoto: si desidera procedere anche alla cancellazione dell'archivio aree?", "?", this.w_YESNOMSG, Trans(l_Count), this.w_UTENAME)
                * --- Delete from GRP_GADG
                i_nConn=i_TableProp[this.GRP_GADG_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.GRP_GADG_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where 1=1")
                else
                  delete from (i_cTable) where 1=1
                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error=MSG_DELETE_ERROR
                  return
                endif
                Ah_ErrorMsg("Archivio aree cancellato","!")
              endif
            endif
          endif
        endif
      case this.pOper=="Enable" Or this.pOper=="Disable"
        this.w_NGADG = 0
        this.w_CURSOR = this.w_PADRE.w_ZoomGadg.cCursor
        Select(this.w_CURSOR) 
 Scan for XCHK=1
        this.w_CODICE = Alltrim(GACODICE)
        * --- Write into GAD_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.GAD_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GAD_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.GAD_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"GACHKATT ="+cp_NullLink(cp_ToStrODBC(iif(this.pOper=="Enable","S","N")),'GAD_MAST','GACHKATT');
              +i_ccchkf ;
          +" where ";
              +"GACODICE = "+cp_ToStrODBC(this.w_CODICE);
                 )
        else
          update (i_cTable) set;
              GACHKATT = iif(this.pOper=="Enable","S","N");
              &i_ccchkf. ;
           where;
              GACODICE = this.w_CODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_NGADG = this.w_NGADG+1
        Select (this.w_CURSOR) 
 Endscan
        if this.w_NGADG=0
          * --- Se non ci sono gadget selezionati controllo se ce n'� uno con l'highlight
          this.w_CODICE = Alltrim(Nvl(this.pGadgetKey,""))
          if ! Empty(Nvl(this.w_CODICE,""))
            * --- Write into GAD_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.GAD_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.GAD_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.GAD_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"GACHKATT ="+cp_NullLink(cp_ToStrODBC(iif(this.pOper=="Enable","S","N")),'GAD_MAST','GACHKATT');
                  +i_ccchkf ;
              +" where ";
                  +"GACODICE = "+cp_ToStrODBC(this.w_CODICE);
                     )
            else
              update (i_cTable) set;
                  GACHKATT = iif(this.pOper=="Enable","S","N");
                  &i_ccchkf. ;
               where;
                  GACODICE = this.w_CODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_NGADG = 1
          else
            Ah_ErrorMsg("Nessun gadget selezionato","!")
            i_retcode = 'stop'
            return
          endif
        endif
        this.w_YESNOMSG = iCase(this.pOper=="Enable" And this.w_NGADG==1, "abilitato", this.pOper=="Enable" And this.w_NGADG<>1, "abilitati", this.pOper=="Disable" And this.w_NGADG==1, "disabilitato", this.pOper=="Disable" And this.w_NGADG<>1, "disabilitati")
        Ah_ErrorMsg("%1 gadget %2","i","",Trans(this.w_NGADG),this.w_YESNOMSG)
        this.w_PADRE.NotifyEvent("AggiornaCruscotto")     
      case this.pOper=="SetDefaultTheme"
        this.w_NGADG = -1
        this.w_GCCODICE = ""
        this.w_GACODICE = ""
        vx_exec("QUERY/GSUTTKGG.VZM",this)
        this.w_ParamOK = !Empty(this.w_GCCODICE)
        * --- Create temporary table TMP_GADG
        i_nIdx=cp_AddTableDef('TMP_GADG') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.GAD_MAST_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.GAD_MAST_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"GACODICE as GACODICE "," from "+i_cTable;
              +" where 1=0";
              )
        this.TMP_GADG_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        if this.w_ParamOK
          this.w_YESNOMSG = 'Impostare il tema "'+this.w_GCCODICE+'" come default per'
          this.w_CURSOR = this.w_PADRE.w_ZoomGadg.cCursor
          Local l_Count 
 Select(this.w_CURSOR) 
 Count For XCHK=1 To m.l_Count 
 
          if l_Count>0 And Ah_YesNo("%1 %2 gadget?","?", this.w_YESNOMSG, Trans(l_Count))
            Select(this.w_CURSOR) 
 Go Top 
 Scan for XCHK==1
            this.w_GACODICE = Alltrim(GACODICE)
            * --- Insert into TMP_GADG
            i_nConn=i_TableProp[this.TMP_GADG_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMP_GADG_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_GADG_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"GACODICE"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_GACODICE),'TMP_GADG','GACODICE');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE)
              insert into (i_cTable) (GACODICE &i_ccchkf. );
                 values (;
                   this.w_GACODICE;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            Select (this.w_CURSOR) 
 Endscan
            this.w_NGADG = l_Count
          else
            * --- Se l_Count=0 si prende in considerazione il gadget corrente
            if l_Count=0
              this.w_CODICE = Alltrim(Nvl(this.pGadgetKey,""))
              if ! Empty(this.w_CODICE)
                if Ah_YesNo("%1 il gadget: %2?","?", this.w_YESNOMSG, this.w_CODICE)
                  this.w_NGADG = 1
                  this.w_GACODICE = this.w_CODICE
                  * --- Insert into TMP_GADG
                  i_nConn=i_TableProp[this.TMP_GADG_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.TMP_GADG_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_GADG_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"GACODICE"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_GACODICE),'TMP_GADG','GACODICE');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE)
                    insert into (i_cTable) (GACODICE &i_ccchkf. );
                       values (;
                         this.w_GACODICE;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error=MSG_INSERT_ERROR
                    return
                  endif
                endif
              else
                Ah_ErrorMsg("Nessun gadget selezionato","!")
              endif
            endif
          endif
          if !Empty(this.w_GACODICE)
            * --- Write into GAD_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.GAD_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="GACODICE,GA__PROP,GACODUTE"
              do vq_exec with 'gsuttbgg',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.GAD_DETT_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="GAD_DETT.GACODICE = _t2.GACODICE";
                      +" and "+"GAD_DETT.GA__PROP = _t2.GA__PROP";
                      +" and "+"GAD_DETT.GACODUTE = _t2.GACODUTE";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"GAVALORE = _t2.GCVALORE";
                  +i_ccchkf;
                  +" from "+i_cTable+" GAD_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="GAD_DETT.GACODICE = _t2.GACODICE";
                      +" and "+"GAD_DETT.GA__PROP = _t2.GA__PROP";
                      +" and "+"GAD_DETT.GACODUTE = _t2.GACODUTE";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" GAD_DETT, "+i_cQueryTable+" _t2 set ";
                  +"GAD_DETT.GAVALORE = _t2.GCVALORE";
                  +Iif(Empty(i_ccchkf),"",",GAD_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="GAD_DETT.GACODICE = t2.GACODICE";
                      +" and "+"GAD_DETT.GA__PROP = t2.GA__PROP";
                      +" and "+"GAD_DETT.GACODUTE = t2.GACODUTE";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" GAD_DETT set (";
                  +"GAVALORE";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.GCVALORE";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="GAD_DETT.GACODICE = _t2.GACODICE";
                      +" and "+"GAD_DETT.GA__PROP = _t2.GA__PROP";
                      +" and "+"GAD_DETT.GACODUTE = _t2.GACODUTE";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" GAD_DETT set ";
                  +"GAVALORE = _t2.GCVALORE";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".GACODICE = "+i_cQueryTable+".GACODICE";
                      +" and "+i_cTable+".GA__PROP = "+i_cQueryTable+".GA__PROP";
                      +" and "+i_cTable+".GACODUTE = "+i_cQueryTable+".GACODUTE";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"GAVALORE = (select GCVALORE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Drop temporary table TMP_GADG
          i_nIdx=cp_GetTableDefIdx('TMP_GADG')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMP_GADG')
          endif
        endif
        if this.w_NGADG>=0
          Ah_ErrorMsg("%1 gadget modificati","i","",Trans(this.w_NGADG))
          if this.w_NGADG>0
            this.w_PADRE.NotifyEvent("AggiornaCruscotto")     
            if !Empty(Alltrim(Nvl(this.pGadgetKey,"")))
              Select(this.w_CURSOR) 
 Locate For Alltrim(GACODICE)=Alltrim(this.pGadgetKey) 
 this.w_PADRE.w_ZoomGadg.Refresh()
            endif
          endif
        endif
      case this.pOper=="ShortcutWizard"
        this.w_TEST_F10 = "N"
        if oGadgetManager.nActivePage>1
          this.w_GGACTGRP = oGadgetManager.aPage.GetKey[oGadgetManager.nActivePage]
        endif
        this.w_GASRCPRG = "GSUT_GLK"
        this.w_SEC = Alltrim(this.w_PADRE.getSecuritycode())
        this.w_GA__NOME = this.w_PADRE.cComment
        this.w_LABEL = this.w_GA__NOME
        this.w_TOOLTIP = ""
        this.w_GCCODICE = ""
        this.w_DIMENSION = "M"
        this.w_IMAGE = "./bmp/gd_shortcut.png"
        this.w_APPLYTHEMEIMG = .T.
        this.w_PROGRAM = Alltrim(Substr(this.w_PADRE.Class,2,Len(this.w_PADRE.Class)-1))
        this.w_PARAM = Upper(Alltrim(Substr(this.w_SEC,Len(this.w_PROGRAM)+2)))
        if !Empty(Alltrim(Nvl(this.w_PARAM, " ")))
          this.w_PROGRAM = this.w_PROGRAM + "(*"+Alltrim(Nvl(this.w_PARAM, " "))+"*)"
        endif
        if this.w_PADRE.bLoaded
          this.w_KEY = ""
          ALines(aKeyFileds, this.w_PADRE.cKeySelect, ",") 
 For l_i=1 To Alen(this.w_PADRE.xKey)
          if Vartype(this.w_PADRE.xKey[l_i])="C"
            this.w_KEY = this.w_KEY + ', "' + Alltrim(aKeyFileds[l_i]) + '", "' + Alltrim(this.w_PADRE.xKey[l_i]) + '"'
          else
            this.w_KEY = this.w_KEY + ', "' + Alltrim(aKeyFileds[l_i]) + '", ' + Tran(this.w_PADRE.xKey[l_i])
          endif
          Endfor
          this.w_PROGRAM = 'OpenGest("A", "'+this.w_PROGRAM+'"'+this.w_KEY+")"
        else
          this.w_PROGRAM = 'OpenGest("S", "'+this.w_PROGRAM+'")'
        endif
        * --- Leggo da file il colore scelto per tematizzare l'immagine
        Create Cursor _TMP_LINK_WIZARD_ (ga__prop C(50), gavalore C(254), gacodute N(4), gacodazi C(5), ga_reset C(1), gafilter C(1))
        Append From LINKWIZARD.TXT For (gafilter=="D" Or gafilter==this.w_DIMENSION) Type CSV
        SELECT("_TMP_LINK_WIZARD_")
        LOCATE FOR Alltrim(ga__prop)=="nFontColor"
        this.w_FONTCLR = Iif(Eval(gavalore)<>-1, Eval(gavalore), Rgb(243,243,243))
        * --- Read from MOD_GADG
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MOD_GADG_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOD_GADG_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGSRCPRG"+;
            " from "+i_cTable+" MOD_GADG where ";
                +"MGSRCPRG = "+cp_ToStrODBC(this.w_GASRCPRG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGSRCPRG;
            from (i_cTable) where;
                MGSRCPRG = this.w_GASRCPRG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_GASRCPRG = NVL(cp_ToDate(_read_.MGSRCPRG),cp_NullValue(_read_.MGSRCPRG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if !Empty(this.w_GASRCPRG)
          do GSUT_KWS with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          Ah_ErrorMsg("Modello gadget %1 non trovato, impossibile proseguire.",,,"Shortcut")
        endif
        * --- w_TEST_F10 viene modificata dalla GSUT_KWS all'ecpSave
        if this.w_TEST_F10="S"
          this.w_GACODICE = Space(10)
          i_Conn=i_TableProp[this.GAD_MAST_IDX, 3]
          cp_NextTableProg(this, i_Conn, "SEGADG", "w_GACODICE")
          * --- Try
          local bErr_03FFAC98
          bErr_03FFAC98=bTrsErr
          this.Try_03FFAC98()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            ah_ErrorMsg("Errore nell'inserimento del nuovo gadget")
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
          bTrsErr=bTrsErr or bErr_03FFAC98
          * --- End
        endif
        Use In Select("_TMP_LINK_WIZARD_")
      case this.pOper=="Remove"
        this.w_GACODICE = ""
        this.w_CURSOR = this.w_PADRE.w_ZoomGadg.cCursor
        Local l_Count 
 Select(this.w_CURSOR) 
 Count For XCHK=1 To m.l_Count 
 
        * --- Creo tabella temporanea
        * --- Create temporary table TMP_GADG
        i_nIdx=cp_AddTableDef('TMP_GADG') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.GAD_MAST_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.GAD_MAST_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"GACODICE as GACODICE "," from "+i_cTable;
              +" where 1=0";
              )
        this.TMP_GADG_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        if l_Count>0
          Select(this.w_CURSOR) 
 Go Top 
 Scan for XCHK==1
          this.w_GACODICE = Alltrim(GACODICE)
          * --- Insert into TMP_GADG
          i_nConn=i_TableProp[this.TMP_GADG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP_GADG_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_GADG_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"GACODICE"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_GACODICE),'TMP_GADG','GACODICE');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE)
            insert into (i_cTable) (GACODICE &i_ccchkf. );
               values (;
                 this.w_GACODICE;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          Select (this.w_CURSOR) 
 Endscan
          this.w_NGADG = l_Count
        else
          * --- Se l_Count=0 si prende in considerazione il gadget corrente
          if l_Count=0
            this.w_CODICE = Alltrim(Nvl(this.pGadgetKey,""))
            if ! Empty(this.w_CODICE)
              this.w_GACODICE = this.w_CODICE
              * --- Insert into TMP_GADG
              i_nConn=i_TableProp[this.TMP_GADG_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMP_GADG_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_GADG_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"GACODICE"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_GACODICE),'TMP_GADG','GACODICE');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE)
                insert into (i_cTable) (GACODICE &i_ccchkf. );
                   values (;
                     this.w_GACODICE;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            else
              Ah_ErrorMsg("Nessun gadget selezionato","!")
            endif
          endif
        endif
        if !Empty(this.w_GACODICE)
          do GSUT_KRG with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_PADRE.NotifyEvent("AggiornaCruscotto")     
        endif
        * --- Drop temporary table TMP_GADG
        i_nIdx=cp_GetTableDefIdx('TMP_GADG')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMP_GADG')
        endif
      case this.pOper=="RemoveFromZoom"
        this.w_CURSOR = this.w_PADRE.w_ZoomRemove.cCursor
        Local l_Count 
 Select(this.w_CURSOR) 
 Count For XCHK=1 To m.l_Count 
 
        if l_Count>0 And Ah_YesNo(MSG_CONFIRM_DELETING_QP)
          Select(this.w_CURSOR) 
 Go Top 
 Scan for XCHK==1
          * --- Delete from MYG_DETT
          i_nConn=i_TableProp[this.MYG_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MYG_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"MGCODUTE = "+cp_ToStrODBC(MGCODUTE);
                  +" and MGCODGAD = "+cp_ToStrODBC(MGCODGAD);
                  +" and MGCODAZI = "+cp_ToStrODBC(MGCODAZI);
                   )
          else
            delete from (i_cTable) where;
                  MGCODUTE = MGCODUTE;
                  and MGCODGAD = MGCODGAD;
                  and MGCODAZI = MGCODAZI;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          Select (this.w_CURSOR) 
 Endscan
          this.w_PADRE.NotifyEvent("Removed")     
        else
          Ah_ErrorMsg("Nessun gadget selezionato","!")
        endif
      case this.pOper=="Export"
        this.w_GACODICE = ""
        this.w_CURSOR = this.w_PADRE.w_ZoomGadg.cCursor
        Local l_Count 
 Select(this.w_CURSOR) 
 Count For XCHK=1 To m.l_Count 
 
        if l_Count>0
          * --- Creo tabella temporanea
          * --- Create temporary table TMP_GADG
          i_nIdx=cp_AddTableDef('TMP_GADG') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          i_nConn=i_TableProp[this.GAD_MAST_idx,3] && recupera la connessione
          i_cTable=cp_SetAzi(i_TableProp[this.GAD_MAST_idx,2])
          cp_CreateTempTable(i_nConn,i_cTempTable,"GACODICE as GACODICE "," from "+i_cTable;
                +" where 1=0";
                )
          this.TMP_GADG_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          Select(this.w_CURSOR) 
 Go Top 
 Scan for XCHK==1
          this.w_GACODICE = Alltrim(GACODICE)
          * --- Insert into TMP_GADG
          i_nConn=i_TableProp[this.TMP_GADG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMP_GADG_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_GADG_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"GACODICE"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_GACODICE),'TMP_GADG','GACODICE');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE)
            insert into (i_cTable) (GACODICE &i_ccchkf. );
               values (;
                 this.w_GACODICE;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          Select (this.w_CURSOR) 
 Endscan 
 
 GSUT_BCO(this.w_PADRE,"SALVA")
          * --- Drop temporary table TMP_GADG
          i_nIdx=cp_GetTableDefIdx('TMP_GADG')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMP_GADG')
          endif
        else
          Ah_ErrorMsg("Nessun gadget selezionato","!")
        endif
      case this.pOper=="Duplicate"
        this.w_CODICE = this.pGadgetKey
        this.oParentObject.oGadgetLibrary.LockScreen = .F.
        * --- Preparo un nuovo codice
        this.w_GACODICE = Space(10)
        i_Conn=i_TableProp[this.GAD_MAST_IDX, 3]
        cp_AskTableProg(this, i_Conn, "SEGADG", "w_GACODICE")
        * --- Inizializzo i campi di testata
        * --- Read from GAD_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.GAD_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GAD_MAST_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "GA__NOME,GADESCRI,GASRCPRG,GACNDATT,GAGRPCOD"+;
            " from "+i_cTable+" GAD_MAST where ";
                +"GACODICE = "+cp_ToStrODBC(this.w_CODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            GA__NOME,GADESCRI,GASRCPRG,GACNDATT,GAGRPCOD;
            from (i_cTable) where;
                GACODICE = this.w_CODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_GA__NOME = NVL(cp_ToDate(_read_.GA__NOME),cp_NullValue(_read_.GA__NOME))
          this.w_GADESCRI = NVL(cp_ToDate(_read_.GADESCRI),cp_NullValue(_read_.GADESCRI))
          this.w_GASRCPRG = NVL(cp_ToDate(_read_.GASRCPRG),cp_NullValue(_read_.GASRCPRG))
          this.w_GACNDATT = NVL(cp_ToDate(_read_.GACNDATT),cp_NullValue(_read_.GACNDATT))
          this.w_GAGRPCOD = NVL(cp_ToDate(_read_.GAGRPCOD),cp_NullValue(_read_.GAGRPCOD))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_GA__NOME = ah_msgformat("%1 (%2)",Alltrim(this.w_GA__NOME),MSG_COPY)
        this.w_GAUTEESC = i_codute
        this.w_GACHKATT = "S"
        this.w_GAFORAZI = "N"
        this.w_TITLE = ""
        * --- Select from GAD_DETT
        i_nConn=i_TableProp[this.GAD_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select GAVALORE  from "+i_cTable+" GAD_DETT ";
              +" where GACODICE="+cp_ToStrODBC(this.w_CODICE)+" And GA__PROP='w_TITLE' And (GACODUTE=-1 Or GACODUTE="+cp_ToStrODBC(this.w_GAUTEESC)+")";
              +" order by GACODUTE Desc";
               ,"_Curs_GAD_DETT")
        else
          select GAVALORE from (i_cTable);
           where GACODICE=this.w_CODICE And GA__PROP="w_TITLE" And (GACODUTE=-1 Or GACODUTE=this.w_GAUTEESC);
           order by GACODUTE Desc;
            into cursor _Curs_GAD_DETT
        endif
        if used('_Curs_GAD_DETT')
          select _Curs_GAD_DETT
          locate for 1=1
          do while not(eof())
          this.w_TITLE = Alltrim(_Curs_GAD_DETT.GAVALORE)
          exit
            select _Curs_GAD_DETT
            continue
          enddo
          use
        endif
        this.w_HAVETITLE = !Empty(this.w_TITLE)
        do GSUT_KDG with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- w_TEST_F10 viene modificata dalla GSUT_KWS all'ecpSave
        if this.w_TEST_F10="S"
          * --- Try
          local bErr_03FDF6A8
          bErr_03FDF6A8=bTrsErr
          this.Try_03FDF6A8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            ah_ErrorMsg("Errore nell'inserimento del nuovo gadget")
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
          bTrsErr=bTrsErr or bErr_03FDF6A8
          * --- End
        endif
    endcase
  endproc
  proc Try_04070558()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into MYG_MAST
    i_nConn=i_TableProp[this.MYG_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MYG_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MYG_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MGCODUTE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(i_CODUTE),'MYG_MAST','MGCODUTE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MGCODUTE',i_CODUTE)
      insert into (i_cTable) (MGCODUTE &i_ccchkf. );
         values (;
           i_CODUTE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03FFAC98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Insert into GAD_MAST
    i_nConn=i_TableProp[this.GAD_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GAD_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GACODICE"+",GA__NOME"+",GADESCRI"+",GASRCPRG"+",GACHKATT"+",GAFORAZI"+",GAGRPCOD"+",GAUTEESC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GACODICE),'GAD_MAST','GACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GA__NOME),'GAD_MAST','GA__NOME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GADESCRI),'GAD_MAST','GADESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GASRCPRG),'GAD_MAST','GASRCPRG');
      +","+cp_NullLink(cp_ToStrODBC("S"),'GAD_MAST','GACHKATT');
      +","+cp_NullLink(cp_ToStrODBC("N"),'GAD_MAST','GAFORAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GAGRPCOD),'GAD_MAST','GAGRPCOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GAUTEESC),'GAD_MAST','GAUTEESC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE,'GA__NOME',this.w_GA__NOME,'GADESCRI',this.w_GADESCRI,'GASRCPRG',this.w_GASRCPRG,'GACHKATT',"S",'GAFORAZI',"N",'GAGRPCOD',this.w_GAGRPCOD,'GAUTEESC',this.w_GAUTEESC)
      insert into (i_cTable) (GACODICE,GA__NOME,GADESCRI,GASRCPRG,GACHKATT,GAFORAZI,GAGRPCOD,GAUTEESC &i_ccchkf. );
         values (;
           this.w_GACODICE;
           ,this.w_GA__NOME;
           ,this.w_GADESCRI;
           ,this.w_GASRCPRG;
           ,"S";
           ,"N";
           ,this.w_GAGRPCOD;
           ,this.w_GAUTEESC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Importo da file esterno tutte le altre propriet� standard
    SELECT("_TMP_LINK_WIZARD_")
    LOCATE FOR 1=1
    do while !EOF("_TMP_LINK_WIZARD_")
      * --- Insert into GAD_DETT
      i_nConn=i_TableProp[this.GAD_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GAD_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"GACODICE"+",GA__PROP"+",GAVALORE"+",GACODUTE"+",GACODAZI"+",GA_RESET"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_GACODICE),'GAD_DETT','GACODICE');
        +","+cp_NullLink(cp_ToStrODBC(Alltrim(ga__prop)),'GAD_DETT','GA__PROP');
        +","+cp_NullLink(cp_ToStrODBC(Alltrim(gavalore)),'GAD_DETT','GAVALORE');
        +","+cp_NullLink(cp_ToStrODBC(gacodute),'GAD_DETT','GACODUTE');
        +","+cp_NullLink(cp_ToStrODBC(Alltrim(gacodazi)),'GAD_DETT','GACODAZI');
        +","+cp_NullLink(cp_ToStrODBC(Alltrim(ga_reset)),'GAD_DETT','GA_RESET');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE,'GA__PROP',Alltrim(ga__prop),'GAVALORE',Alltrim(gavalore),'GACODUTE',gacodute,'GACODAZI',Alltrim(gacodazi),'GA_RESET',Alltrim(ga_reset))
        insert into (i_cTable) (GACODICE,GA__PROP,GAVALORE,GACODUTE,GACODAZI,GA_RESET &i_ccchkf. );
           values (;
             this.w_GACODICE;
             ,Alltrim(ga__prop);
             ,Alltrim(gavalore);
             ,gacodute;
             ,Alltrim(gacodazi);
             ,Alltrim(ga_reset);
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      SELECT("_TMP_LINK_WIZARD_")
      CONTINUE
    enddo
    * --- w_IMAGE
    * --- Insert into GAD_DETT
    i_nConn=i_TableProp[this.GAD_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GAD_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GACODICE"+",GA__PROP"+",GAVALORE"+",GACODUTE"+",GACODAZI"+",GA_RESET"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GACODICE),'GAD_DETT','GACODICE');
      +","+cp_NullLink(cp_ToStrODBC("w_IMAGE"),'GAD_DETT','GA__PROP');
      +","+cp_NullLink(cp_ToStrODBC(Cp_ToStrODBC(Alltrim(this.w_IMAGE))),'GAD_DETT','GAVALORE');
      +","+cp_NullLink(cp_ToStrODBC(-1),'GAD_DETT','GACODUTE');
      +","+cp_NullLink(cp_ToStrODBC("xxx  "),'GAD_DETT','GACODAZI');
      +","+cp_NullLink(cp_ToStrODBC("N"),'GAD_DETT','GA_RESET');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE,'GA__PROP',"w_IMAGE",'GAVALORE',Cp_ToStrODBC(Alltrim(this.w_IMAGE)),'GACODUTE',-1,'GACODAZI',"xxx  ",'GA_RESET',"N")
      insert into (i_cTable) (GACODICE,GA__PROP,GAVALORE,GACODUTE,GACODAZI,GA_RESET &i_ccchkf. );
         values (;
           this.w_GACODICE;
           ,"w_IMAGE";
           ,Cp_ToStrODBC(Alltrim(this.w_IMAGE));
           ,-1;
           ,"xxx  ";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- w_APPLYTHEMEIMG
    * --- Insert into GAD_DETT
    i_nConn=i_TableProp[this.GAD_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GAD_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GACODICE"+",GA__PROP"+",GAVALORE"+",GACODUTE"+",GACODAZI"+",GA_RESET"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GACODICE),'GAD_DETT','GACODICE');
      +","+cp_NullLink(cp_ToStrODBC("w_APPLYTHEMEIMG"),'GAD_DETT','GA__PROP');
      +","+cp_NullLink(cp_ToStrODBC(Tran(this.w_APPLYTHEMEIMG)),'GAD_DETT','GAVALORE');
      +","+cp_NullLink(cp_ToStrODBC(-1),'GAD_DETT','GACODUTE');
      +","+cp_NullLink(cp_ToStrODBC("xxx  "),'GAD_DETT','GACODAZI');
      +","+cp_NullLink(cp_ToStrODBC("N"),'GAD_DETT','GA_RESET');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE,'GA__PROP',"w_APPLYTHEMEIMG",'GAVALORE',Tran(this.w_APPLYTHEMEIMG),'GACODUTE',-1,'GACODAZI',"xxx  ",'GA_RESET',"N")
      insert into (i_cTable) (GACODICE,GA__PROP,GAVALORE,GACODUTE,GACODAZI,GA_RESET &i_ccchkf. );
         values (;
           this.w_GACODICE;
           ,"w_APPLYTHEMEIMG";
           ,Tran(this.w_APPLYTHEMEIMG);
           ,-1;
           ,"xxx  ";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- w_LABEL
    * --- Insert into GAD_DETT
    i_nConn=i_TableProp[this.GAD_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GAD_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GACODICE"+",GA__PROP"+",GAVALORE"+",GACODUTE"+",GACODAZI"+",GA_RESET"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GACODICE),'GAD_DETT','GACODICE');
      +","+cp_NullLink(cp_ToStrODBC("w_LABEL"),'GAD_DETT','GA__PROP');
      +","+cp_NullLink(cp_ToStrODBC(Cp_ToStrODBC(Alltrim(this.w_LABEL))),'GAD_DETT','GAVALORE');
      +","+cp_NullLink(cp_ToStrODBC(-1),'GAD_DETT','GACODUTE');
      +","+cp_NullLink(cp_ToStrODBC("xxx  "),'GAD_DETT','GACODAZI');
      +","+cp_NullLink(cp_ToStrODBC("N"),'GAD_DETT','GA_RESET');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE,'GA__PROP',"w_LABEL",'GAVALORE',Cp_ToStrODBC(Alltrim(this.w_LABEL)),'GACODUTE',-1,'GACODAZI',"xxx  ",'GA_RESET',"N")
      insert into (i_cTable) (GACODICE,GA__PROP,GAVALORE,GACODUTE,GACODAZI,GA_RESET &i_ccchkf. );
         values (;
           this.w_GACODICE;
           ,"w_LABEL";
           ,Cp_ToStrODBC(Alltrim(this.w_LABEL));
           ,-1;
           ,"xxx  ";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- w_TOOLTIP
    * --- Insert into GAD_DETT
    i_nConn=i_TableProp[this.GAD_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GAD_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GACODICE"+",GA__PROP"+",GAVALORE"+",GACODUTE"+",GACODAZI"+",GA_RESET"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GACODICE),'GAD_DETT','GACODICE');
      +","+cp_NullLink(cp_ToStrODBC("w_TOOLTIP"),'GAD_DETT','GA__PROP');
      +","+cp_NullLink(cp_ToStrODBC(Cp_ToStrODBC(Alltrim(this.w_TOOLTIP))),'GAD_DETT','GAVALORE');
      +","+cp_NullLink(cp_ToStrODBC(-1),'GAD_DETT','GACODUTE');
      +","+cp_NullLink(cp_ToStrODBC("xxx  "),'GAD_DETT','GACODAZI');
      +","+cp_NullLink(cp_ToStrODBC("N"),'GAD_DETT','GA_RESET');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE,'GA__PROP',"w_TOOLTIP",'GAVALORE',Cp_ToStrODBC(Alltrim(this.w_TOOLTIP)),'GACODUTE',-1,'GACODAZI',"xxx  ",'GA_RESET',"N")
      insert into (i_cTable) (GACODICE,GA__PROP,GAVALORE,GACODUTE,GACODAZI,GA_RESET &i_ccchkf. );
         values (;
           this.w_GACODICE;
           ,"w_TOOLTIP";
           ,Cp_ToStrODBC(Alltrim(this.w_TOOLTIP));
           ,-1;
           ,"xxx  ";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- w_PROGRAM
    * --- Insert into GAD_DETT
    i_nConn=i_TableProp[this.GAD_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GAD_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GACODICE"+",GA__PROP"+",GAVALORE"+",GACODUTE"+",GACODAZI"+",GA_RESET"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GACODICE),'GAD_DETT','GACODICE');
      +","+cp_NullLink(cp_ToStrODBC("w_PROGRAM"),'GAD_DETT','GA__PROP');
      +","+cp_NullLink(cp_ToStrODBC(Cp_ToStrODBC(Alltrim(this.w_PROGRAM))),'GAD_DETT','GAVALORE');
      +","+cp_NullLink(cp_ToStrODBC(-1),'GAD_DETT','GACODUTE');
      +","+cp_NullLink(cp_ToStrODBC("xxx  "),'GAD_DETT','GACODAZI');
      +","+cp_NullLink(cp_ToStrODBC("N"),'GAD_DETT','GA_RESET');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE,'GA__PROP',"w_PROGRAM",'GAVALORE',Cp_ToStrODBC(Alltrim(this.w_PROGRAM)),'GACODUTE',-1,'GACODAZI',"xxx  ",'GA_RESET',"N")
      insert into (i_cTable) (GACODICE,GA__PROP,GAVALORE,GACODUTE,GACODAZI,GA_RESET &i_ccchkf. );
         values (;
           this.w_GACODICE;
           ,"w_PROGRAM";
           ,Cp_ToStrODBC(Alltrim(this.w_PROGRAM));
           ,-1;
           ,"xxx  ";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    if !Empty(this.w_GCCODICE)
      * --- Create temporary table TMP_GADG
      i_nIdx=cp_AddTableDef('TMP_GADG') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.GAD_MAST_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.GAD_MAST_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"GACODICE as GACODICE "," from "+i_cTable;
            +" where 1=0";
            )
      this.TMP_GADG_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Insert into TMP_GADG
      i_nConn=i_TableProp[this.TMP_GADG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_GADG_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_GADG_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"GACODICE"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_GACODICE),'TMP_GADG','GACODICE');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE)
        insert into (i_cTable) (GACODICE &i_ccchkf. );
           values (;
             this.w_GACODICE;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into GAD_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.GAD_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="GACODICE,GA__PROP,GACODUTE"
        do vq_exec with 'gsuttbgg',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.GAD_DETT_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="GAD_DETT.GACODICE = _t2.GACODICE";
                +" and "+"GAD_DETT.GA__PROP = _t2.GA__PROP";
                +" and "+"GAD_DETT.GACODUTE = _t2.GACODUTE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"GAVALORE = _t2.GCVALORE";
            +i_ccchkf;
            +" from "+i_cTable+" GAD_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="GAD_DETT.GACODICE = _t2.GACODICE";
                +" and "+"GAD_DETT.GA__PROP = _t2.GA__PROP";
                +" and "+"GAD_DETT.GACODUTE = _t2.GACODUTE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" GAD_DETT, "+i_cQueryTable+" _t2 set ";
            +"GAD_DETT.GAVALORE = _t2.GCVALORE";
            +Iif(Empty(i_ccchkf),"",",GAD_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="GAD_DETT.GACODICE = t2.GACODICE";
                +" and "+"GAD_DETT.GA__PROP = t2.GA__PROP";
                +" and "+"GAD_DETT.GACODUTE = t2.GACODUTE";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" GAD_DETT set (";
            +"GAVALORE";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.GCVALORE";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="GAD_DETT.GACODICE = _t2.GACODICE";
                +" and "+"GAD_DETT.GA__PROP = _t2.GA__PROP";
                +" and "+"GAD_DETT.GACODUTE = _t2.GACODUTE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" GAD_DETT set ";
            +"GAVALORE = _t2.GCVALORE";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".GACODICE = "+i_cQueryTable+".GACODICE";
                +" and "+i_cTable+".GA__PROP = "+i_cQueryTable+".GA__PROP";
                +" and "+i_cTable+".GACODUTE = "+i_cQueryTable+".GACODUTE";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"GAVALORE = (select GCVALORE from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Drop temporary table TMP_GADG
      i_nIdx=cp_GetTableDefIdx('TMP_GADG')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP_GADG')
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03FDF6A8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_GACODICE = Space(10)
    cp_NextTableProg(this, i_Conn, "SEGADG", "w_GACODICE")
    * --- Insert into GAD_MAST
    i_nConn=i_TableProp[this.GAD_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GAD_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"GACODICE"+",GA__NOME"+",GADESCRI"+",GASRCPRG"+",GACHKATT"+",GAFORAZI"+",GAGRPCOD"+",GAUTEESC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_GACODICE),'GAD_MAST','GACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GA__NOME),'GAD_MAST','GA__NOME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GADESCRI),'GAD_MAST','GADESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GASRCPRG),'GAD_MAST','GASRCPRG');
      +","+cp_NullLink(cp_ToStrODBC("S"),'GAD_MAST','GACHKATT');
      +","+cp_NullLink(cp_ToStrODBC(""),'GAD_MAST','GAFORAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GAGRPCOD),'GAD_MAST','GAGRPCOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_GAUTEESC),'GAD_MAST','GAUTEESC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'GACODICE',this.w_GACODICE,'GA__NOME',this.w_GA__NOME,'GADESCRI',this.w_GADESCRI,'GASRCPRG',this.w_GASRCPRG,'GACHKATT',"S",'GAFORAZI',"",'GAGRPCOD',this.w_GAGRPCOD,'GAUTEESC',this.w_GAUTEESC)
      insert into (i_cTable) (GACODICE,GA__NOME,GADESCRI,GASRCPRG,GACHKATT,GAFORAZI,GAGRPCOD,GAUTEESC &i_ccchkf. );
         values (;
           this.w_GACODICE;
           ,this.w_GA__NOME;
           ,this.w_GADESCRI;
           ,this.w_GASRCPRG;
           ,"S";
           ,"";
           ,this.w_GAGRPCOD;
           ,this.w_GAUTEESC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Copio il dettaglio dal gadget di partenza
    * --- Insert into GAD_DETT
    i_nConn=i_TableProp[this.GAD_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query/gsut_kdg",this.GAD_DETT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- w_TITLE
    if this.w_HAVETITLE
      * --- La stringa vuota per una propriet� di un gadget equivale ad una stringa che contiene aperto e chiuso apice
      if Empty(this.w_TITLE)
        this.w_TITLE = "''"
      endif
      * --- Write into GAD_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.GAD_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.GAD_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"GAVALORE ="+cp_NullLink(cp_ToStrODBC(this.w_TITLE),'GAD_DETT','GAVALORE');
            +i_ccchkf ;
        +" where ";
            +"GACODICE = "+cp_ToStrODBC(this.w_GACODICE);
            +" and GA__PROP = "+cp_ToStrODBC("w_TITLE");
            +" and GACODUTE = "+cp_ToStrODBC(-1);
               )
      else
        update (i_cTable) set;
            GAVALORE = this.w_TITLE;
            &i_ccchkf. ;
         where;
            GACODICE = this.w_GACODICE;
            and GA__PROP = "w_TITLE";
            and GACODUTE = -1;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo tipo
    if ISNULL(this.w_VALUE)
      * --- Valore nullo
      this.w_VALUE=".NULL."
    else
      * --- Valore non nullo, determino il tipo
      do case
        case VarType(this.w_VALUE)="D"
          this.w_VALUE="cp_CharToDate('"+DTOC(this.w_VALUE)+"')"
        case VarType(this.w_VALUE)="T"
          this.w_VALUE="cp_CharToDateTime('"+TTOC(this.w_VALUE)+"')"
        case VarType(this.w_VALUE)="N" Or VarType(this.w_VALUE)="L"
          this.w_VALUE=Transform(this.w_VALUE)
        case VarType(this.w_VALUE)="C"
          this.w_VALUE=Cp_ToStrODBC(this.w_VALUE)
      endcase
    endif
  endproc


  procedure Pag3
    param w_GADKEY,w_CODTHEME
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_GCCODICE = Alltrim(m.w_CODTHEME)
    if Empty(m.w_GADKEY)
      * --- Se non passo nessuna chiave processo tutti gadget
      this.w_NUM = 1
      this.w_MAX = this.w_PADRE.aGadget.Count
    else
      * --- Solo il gadget corrente (w_GADKEY)
      this.w_NUM = this.w_PADRE.aGadget.GetKey(m.w_GADKEY)
      this.w_MAX = this.w_NUM
    endif
    do while this.w_NUM<=this.w_MAX
      this.w_GADG = this.w_PADRE.aGadget(this.w_NUM)
      * --- Solo se selezionato e nella pagina attiva oppure se ne sto facendo solo uno
      if (this.w_GADG.bSelected AND this.w_GADG.nPage = this.w_PADRE.nActivePage) OR !Empty(m.w_GADKEY)
        * --- Rieseguo la query ogni volta perche pu� cambiare il modello e quindi il valore degli attributi
        *     Si potrebbe raggruppare i gadget selezionati della pagina per modello ed eseguire la query solo per rottura modello
        this.w_GC__NOME = this.w_GADG.cPrg
        * --- Select from QUERY\GSUTBMGC
        do vq_exec with 'QUERY\GSUTBMGC',this,'_Curs_QUERY_GSUTBMGC','',.f.,.t.
        if used('_Curs_QUERY_GSUTBMGC')
          select _Curs_QUERY_GSUTBMGC
          locate for 1=1
          do while not(eof())
          * --- GC__PROP
          *     GCVALORE
          this.w_ATTRIBUTE = Alltrim(_Curs_QUERY_GSUTBMGC.GC__PROP)
          this.w_VALUE = Alltrim(_Curs_QUERY_GSUTBMGC.GCVALORE)
          if Alltrim(_Curs_QUERY_GSUTBMGC.GCVALORE)<>CHR(39)+CHR(39)
            * --- Problema apici
            this.w_VALUE = STRTRAN(this.w_VALUE, CHR(39)+CHR(39),"'+CHR(39)+'")
          endif
          if PEMSTATUS(this.w_GADG, this.w_ATTRIBUTE, 5)
            cProp = this.w_ATTRIBUTE 
 this.w_GADG.&cProp = Eval(this.w_VALUE)
          endif
            select _Curs_QUERY_GSUTBMGC
            continue
          enddo
          use
        endif
        this.w_GADG.ApplyTheme()     
      endif
      this.w_GADG = .Null.
      this.w_NUM = this.w_NUM + 1
    enddo
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancellazione gadget
    * --- Try
    local bErr_03FE3248
    bErr_03FE3248=bTrsErr
    this.Try_03FE3248()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      Ah_ErrorMsg("Errore tentando di cancellare il gadget %1:%2%3","!","",this.w_CODICE, CHR(10)+CHR(13), Message())
    endif
    bTrsErr=bTrsErr or bErr_03FE3248
    * --- End
  endproc
  proc Try_03FE3248()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Delete from MYG_DETT
    i_nConn=i_TableProp[this.MYG_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MYG_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"MGCODGAD = "+cp_ToStrODBC(this.w_CODICE);
             )
    else
      delete from (i_cTable) where;
            MGCODGAD = this.w_CODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from GAD_ACCE
    i_nConn=i_TableProp[this.GAD_ACCE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_ACCE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"AGCODGAD = "+cp_ToStrODBC(this.w_CODICE);
             )
    else
      delete from (i_cTable) where;
            AGCODGAD = this.w_CODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from GAD_DETT
    i_nConn=i_TableProp[this.GAD_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"GACODICE = "+cp_ToStrODBC(this.w_CODICE);
             )
    else
      delete from (i_cTable) where;
            GACODICE = this.w_CODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from GAD_MAST
    i_nConn=i_TableProp[this.GAD_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"GACODICE = "+cp_ToStrODBC(this.w_CODICE);
             )
    else
      delete from (i_cTable) where;
            GACODICE = this.w_CODICE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_NGADG = this.w_NGADG+1
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ripristino delle propriet� di default
    * --- Try
    local bErr_03FE1F58
    bErr_03FE1F58=bTrsErr
    this.Try_03FE1F58()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      Ah_ErrorMsg("Errore tentando di ripristinare il gadget %1:%2%3","!","",this.w_CODICE, CHR(10)+CHR(13), Message())
    endif
    bTrsErr=bTrsErr or bErr_03FE1F58
    * --- End
  endproc
  proc Try_03FE1F58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Delete from GAD_DETT
    i_nConn=i_TableProp[this.GAD_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"GACODICE = "+cp_ToStrODBC(this.w_CODICE);
            +" and GACODUTE = "+cp_ToStrODBC(this.W_UTECODE);
             )
    else
      delete from (i_cTable) where;
            GACODICE = this.w_CODICE;
            and GACODUTE = this.W_UTECODE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_NGADG = this.w_NGADG+1
    return


  procedure Pag6
    param pUteEsc
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna il campo "Esclusivo per utente"
    * --- Try
    local bErr_0270CF00
    bErr_0270CF00=bTrsErr
    this.Try_0270CF00()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      Ah_ErrorMsg("Errore aggiornando il gadget %1:%2%3","!","",this.w_CODICE, CHR(10)+CHR(13), Message())
    endif
    bTrsErr=bTrsErr or bErr_0270CF00
    * --- End
  endproc
  proc Try_0270CF00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into GAD_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.GAD_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.GAD_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"GAUTEESC ="+cp_NullLink(cp_ToStrODBC(m.pUteEsc),'GAD_MAST','GAUTEESC');
          +i_ccchkf ;
      +" where ";
          +"GACODICE = "+cp_ToStrODBC(this.w_CODICE);
             )
    else
      update (i_cTable) set;
          GAUTEESC = m.pUteEsc;
          &i_ccchkf. ;
       where;
          GACODICE = this.w_CODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_NGADG = this.w_NGADG+1
    return


  procedure Pag7
    param pGrpCod
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna il campo area sul gadget
    * --- Try
    local bErr_0270BD60
    bErr_0270BD60=bTrsErr
    this.Try_0270BD60()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      Ah_ErrorMsg("Errore aggiornando il gadget %1:%2%3","!","",this.w_CODICE, CHR(10)+CHR(13), Message())
    endif
    bTrsErr=bTrsErr or bErr_0270BD60
    * --- End
  endproc
  proc Try_0270BD60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into GAD_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.GAD_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GAD_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.GAD_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"GAGRPCOD ="+cp_NullLink(cp_ToStrODBC(m.pGrpCod),'GAD_MAST','GAGRPCOD');
          +i_ccchkf ;
      +" where ";
          +"GACODICE = "+cp_ToStrODBC(this.w_CODICE);
             )
    else
      update (i_cTable) set;
          GAGRPCOD = m.pGrpCod;
          &i_ccchkf. ;
       where;
          GACODICE = this.w_CODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_NGADG = this.w_NGADG+1
    return


  proc Init(oParentObject,pOper,pGadgetKey,pNoLoadGadget,pCodTheme,pAddType,pXCoord,pYCoord)
    this.pOper=pOper
    this.pGadgetKey=pGadgetKey
    this.pNoLoadGadget=pNoLoadGadget
    this.pCodTheme=pCodTheme
    this.pAddType=pAddType
    this.pXCoord=pXCoord
    this.pYCoord=pYCoord
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='MYG_MAST'
    this.cWorkTables[2]='MYG_DETT'
    this.cWorkTables[3]='GAD_DETT'
    this.cWorkTables[4]='ELABKPIM'
    this.cWorkTables[5]='GAD_MAST'
    this.cWorkTables[6]='GAD_ACCE'
    this.cWorkTables[7]='*TMP_GADG'
    this.cWorkTables[8]='GRP_GADG'
    this.cWorkTables[9]='MOD_GADG'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_QUERY_GSUT_BGG')
      use in _Curs_QUERY_GSUT_BGG
    endif
    if used('_Curs_QUERY_GSUT3KGS')
      use in _Curs_QUERY_GSUT3KGS
    endif
    if used('_Curs_QUERY_GSUT3KGS')
      use in _Curs_QUERY_GSUT3KGS
    endif
    if used('_Curs_GAD_DETT')
      use in _Curs_GAD_DETT
    endif
    if used('_Curs_QUERY_GSUT3KGS')
      use in _Curs_QUERY_GSUT3KGS
    endif
    if used('_Curs_MYG_DETT')
      use in _Curs_MYG_DETT
    endif
    if used('_Curs_GAD_MAST')
      use in _Curs_GAD_MAST
    endif
    if used('_Curs_GAD_DETT')
      use in _Curs_GAD_DETT
    endif
    if used('_Curs_QUERY_GSUTBMGC')
      use in _Curs_QUERY_GSUTBMGC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper,pGadgetKey,pNoLoadGadget,pCodTheme,pAddType,pXCoord,pYCoord"
endproc
