* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bcf                                                        *
*              Aggiorna flag saldi/competenze                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_10]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2008-06-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOpz
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bcf",oParentObject,m.pOpz)
return(i_retval)

define class tgscg_bcf as StdBatch
  * --- Local variables
  pOpz = space(1)
  w_OKPAR = .f.
  w_OKANA = .f.
  w_MESS = space(10)
  w_OK = .f.
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Flag Saldi/Competenza (da GSCG_MPN)
    * --- F = Cambio Flag Provvisorio/Confermato; C = Cambiata Competenza
    this.w_OKANA = .F.
    this.w_OKPAR = .F.
    this.w_OK = .F.
    this.w_oMess=createobject("Ah_Message")
    do case
      case this.pOpz="F"
        * --- Cambiato Flag provvisorio/Confermato, aggiorna Flag Saldi
        * --- Scorre sul Tmp Registrazioni Contabili
        SELECT (this.oParentObject.cTrsName)
        GO TOP
        SCAN
        * --- Legge i Dati del Temporaneo
        this.oParentObject.WorkFromTrs()
        * --- Aggiorna Saldi
        this.oParentObject.w_PNFLSALD = IIF(this.oParentObject.w_PNFLPROV="S", " ", "+")
        this.oParentObject.w_PNFLSALI = IIF(this.oParentObject.w_FLSALI="S" AND this.oParentObject.w_PNFLPROV<>"S" AND this.oParentObject.w_SEZB<>"T", "+", " ")
        this.oParentObject.w_PNFLSALF = IIF(this.oParentObject.w_FLSALF="S" AND this.oParentObject.w_PNFLPROV<>"S" AND this.oParentObject.w_SEZB<>"T", "+", " ")
        * --- Carica il Temporaneo dei Dati
        this.oParentObject.TrsFromWork()
        * --- Flag Notifica Riga Variata
        if i_SRV<>"A"
          replace i_SRV with "U"
        endif
        ENDSCAN
        this.w_OK = .T.
      case this.pOpz="C"
        * --- Aggiorna le Date di Inizio e Fine Competenza se Cambiata Competenza
        if  this.oParentObject.cFunction="Load"
          * --- Allineo Codice Eserizio dei Progressivi al Codice Esercizio di Competenza
          this.oParentObject.w_PNCODESE = this.oParentObject.w_PNCOMPET
        endif
        SELECT (this.oParentObject.cTrsName)
        if RECCOUNT()>1 OR this.oParentObject.cFunction<>"Load"
          this.w_OK = ah_YesNo("Aggiorno le date di inizio/fine competenza sulle righe di primanota?")
        endif
        if this.w_OK=.T.
          * --- Scorre sul Tmp Registrazioni Contabili
          GO TOP
          SCAN
          * --- Legge i Dati del Temporaneo
          this.oParentObject.WorkFromTrs()
          * --- Riga P.N. con Partite
          this.oParentObject.w_SEZB = CALCSEZ(Nvl(t_MASTRO," "))
          if g_PERPAR="S" AND NVL(t_PNFLPART,"N") $ "CSA"
            this.w_OKPAR = .T.
          endif
          * --- Riga P.N. con Centri di Costo
          if t_PNTIPCON="G" AND g_PERCCR="S" AND this.oParentObject.w_SEZB $ "CR" AND this.oParentObject.w_FLANAL="S"
            this.w_OKANA = .T.
          endif
          * --- Aggiorna Date di Inizio/Fine Competenza
          if this.oParentObject.w_SEZB $ "CR" 
            this.oParentObject.w_PNINICOM = this.oParentObject.w_INIESE
            this.oParentObject.w_PNFINCOM = this.oParentObject.w_FINESE
          endif
          * --- Carica il Temporaneo dei Dati
          this.oParentObject.TrsFromWork()
          * --- Flag Notifica Riga Variata
          if i_SRV<>"A"
            replace i_SRV with "U"
          endif
          ENDSCAN
          * --- Messaggio Date Variate in  Corrispondenza di una Riga P.N. con Partite
          if this.w_OKPAR=.T. OR this.w_OKANA=.T.
            do case
              case this.w_OKPAR=.T. AND this.w_OKANA=.T.
                this.w_oMess.AddMsgPart("Aggiornare le partite e i centri di costo")     
              case this.w_OKPAR=.T.
                this.w_oMess.AddMsgPart("Aggiornare le partite")     
              case this.w_OKANA=.T.
                this.w_oMess.AddMsgPart("Aggiornare i centri di costo")     
            endcase
            this.w_oPart = this.w_oMess.AddMsgPart("%1in funzione delle nuove date di competenza.")
            this.w_oPart.AddParam(space(1))     
            this.w_oMess.AddMsgPartNL("Tramite l'apposito bottone di manutenzione")     
            this.w_oMess.Ah_ErrorMsg()     
          endif
        endif
    endcase
    if this.w_OK=.T.
      * --- Questa Parte derivata dal Metodo LoadRec
      SELECT (this.oParentObject.cTrsName)
      GO TOP
      With this.oParentObject
      .WorkFromTrs()
      .SetControlsValue()
      .ChildrenChangeRow()
      .oPgFrm.Page1.oPag.oBody.Refresh()
      .oPgFrm.Page1.oPag.oBody.nAbsRow=1
      .oPgFrm.Page1.oPag.oBody.nRelRow=1
      EndWith
    endif
  endproc


  proc Init(oParentObject,pOpz)
    this.pOpz=pOpz
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOpz"
endproc
