* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kre                                                        *
*              Mancata quadratura                                              *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-01-02                                                      *
* Last revis.: 2007-06-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kre",oParentObject))

* --- Class definition
define class tgscg_kre as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 376
  Height = 294
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-06-12"
  HelpContextID=116008297
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_kre"
  cComment = "Mancata quadratura"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_QATT = 0
  w_QPAS = 0
  w_QCOS = 0
  w_QRIC = 0
  w_QSAL = 0
  w_QPRE = 0
  w_QTRA = 0
  w_QORD = 0
  w_QRIS = 0
  w_QRISP = 0
  * --- Area Manuale = Declare Variables
  * --- gscg_kre
  Autocenter=.T.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_krePag1","gscg_kre",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_QATT=0
      .w_QPAS=0
      .w_QCOS=0
      .w_QRIC=0
      .w_QSAL=0
      .w_QPRE=0
      .w_QTRA=0
      .w_QORD=0
      .w_QRIS=0
      .w_QRISP=0
      .w_QATT=oParentObject.w_QATT
      .w_QPAS=oParentObject.w_QPAS
      .w_QCOS=oParentObject.w_QCOS
      .w_QRIC=oParentObject.w_QRIC
      .w_QSAL=oParentObject.w_QSAL
      .w_QPRE=oParentObject.w_QPRE
      .w_QTRA=oParentObject.w_QTRA
      .w_QORD=oParentObject.w_QORD
      .w_QRIS=oParentObject.w_QRIS
      .w_QRISP=oParentObject.w_QRISP
    endwith
    this.DoRTCalc(1,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_QATT=.w_QATT
      .oParentObject.w_QPAS=.w_QPAS
      .oParentObject.w_QCOS=.w_QCOS
      .oParentObject.w_QRIC=.w_QRIC
      .oParentObject.w_QSAL=.w_QSAL
      .oParentObject.w_QPRE=.w_QPRE
      .oParentObject.w_QTRA=.w_QTRA
      .oParentObject.w_QORD=.w_QORD
      .oParentObject.w_QRIS=.w_QRIS
      .oParentObject.w_QRISP=.w_QRISP
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oQPRE_1_12.visible=!this.oPgFrm.Page1.oPag.oQPRE_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oQTRA_1_15.visible=!this.oPgFrm.Page1.oPag.oQTRA_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oQORD_1_17.visible=!this.oPgFrm.Page1.oPag.oQORD_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oQRIS_1_21.visible=!this.oPgFrm.Page1.oPag.oQRIS_1_21.mHide()
    this.oPgFrm.Page1.oPag.oQRISP_1_22.visible=!this.oPgFrm.Page1.oPag.oQRISP_1_22.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oQATT_1_6.value==this.w_QATT)
      this.oPgFrm.Page1.oPag.oQATT_1_6.value=this.w_QATT
    endif
    if not(this.oPgFrm.Page1.oPag.oQPAS_1_7.value==this.w_QPAS)
      this.oPgFrm.Page1.oPag.oQPAS_1_7.value=this.w_QPAS
    endif
    if not(this.oPgFrm.Page1.oPag.oQCOS_1_8.value==this.w_QCOS)
      this.oPgFrm.Page1.oPag.oQCOS_1_8.value=this.w_QCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oQRIC_1_9.value==this.w_QRIC)
      this.oPgFrm.Page1.oPag.oQRIC_1_9.value=this.w_QRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oQSAL_1_10.value==this.w_QSAL)
      this.oPgFrm.Page1.oPag.oQSAL_1_10.value=this.w_QSAL
    endif
    if not(this.oPgFrm.Page1.oPag.oQPRE_1_12.value==this.w_QPRE)
      this.oPgFrm.Page1.oPag.oQPRE_1_12.value=this.w_QPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oQTRA_1_15.value==this.w_QTRA)
      this.oPgFrm.Page1.oPag.oQTRA_1_15.value=this.w_QTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oQORD_1_17.value==this.w_QORD)
      this.oPgFrm.Page1.oPag.oQORD_1_17.value=this.w_QORD
    endif
    if not(this.oPgFrm.Page1.oPag.oQRIS_1_21.value==this.w_QRIS)
      this.oPgFrm.Page1.oPag.oQRIS_1_21.value=this.w_QRIS
    endif
    if not(this.oPgFrm.Page1.oPag.oQRISP_1_22.value==this.w_QRISP)
      this.oPgFrm.Page1.oPag.oQRISP_1_22.value=this.w_QRISP
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_krePag1 as StdContainer
  Width  = 372
  height = 294
  stdWidth  = 372
  stdheight = 294
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oQATT_1_6 as StdField with uid="MWAYNCWYIU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_QATT", cQueryName = "QATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 110141178,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=209, Top=6, cSayPict="v_PV[S]", cGetPict="v_PV[S]"

  add object oQPAS_1_7 as StdField with uid="OFSTWZHWRY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_QPAS", cQueryName = "QPAS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 110280698,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=209, Top=29, cSayPict="v_PV[S]", cGetPict="v_PV[S]"

  add object oQCOS_1_8 as StdField with uid="KCDUFMSJUD",rtseq=3,rtrep=.f.,;
    cFormVar = "w_QCOS", cQueryName = "QCOS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 110226682,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=209, Top=98, cSayPict="v_PV[S]", cGetPict="v_PV[S]"

  add object oQRIC_1_9 as StdField with uid="JVOZIHXMZW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_QRIC", cQueryName = "QRIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 111295994,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=209, Top=121, cSayPict="v_PV[S]", cGetPict="v_PV[S]"

  add object oQSAL_1_10 as StdField with uid="CBTOEGSBAE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_QSAL", cQueryName = "QSAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 110738682,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=209, Top=147, cSayPict="v_PV[S]", cGetPict="v_PV[S]"

  add object oQPRE_1_12 as StdField with uid="BJONGXANAG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_QPRE", cQueryName = "QPRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 111128570,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=209, Top=170, cSayPict="v_PV[S]", cGetPict="v_PV[S]"

  func oQPRE_1_12.mHide()
    with this.Parent.oContained
      return (NOT(.w_QPRE<>0 AND .oParentObject.oParentObject .w_VALAPP=.oParentObject.oParentObject .w_PRVALNAZ))
    endwith
  endfunc

  add object oQTRA_1_15 as StdField with uid="WOFLXYCPUR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_QTRA", cQueryName = "QTRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 111389690,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=209, Top=216, cSayPict="v_PV[S]", cGetPict="v_PV[S]"

  func oQTRA_1_15.mHide()
    with this.Parent.oContained
      return (.w_QTRA=0)
    endwith
  endfunc

  add object oQORD_1_17 as StdField with uid="ZSKAHRASMY",rtseq=8,rtrep=.f.,;
    cFormVar = "w_QORD", cQueryName = "QORD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 111194362,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=209, Top=193, cSayPict="v_PV[S]", cGetPict="v_PV[S]"

  func oQORD_1_17.mHide()
    with this.Parent.oContained
      return (.w_QORD=0)
    endwith
  endfunc


  add object oBtn_1_18 as StdButton with uid="ZMIZZNTUDJ",left=300, top=242, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 207582230;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oQRIS_1_21 as StdField with uid="PNLLFVRKNJ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_QRIS", cQueryName = "QRIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 110247418,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=209, Top=52, cSayPict="v_PV[S]", cGetPict="v_PV[S]"

  func oQRIS_1_21.mHide()
    with this.Parent.oContained
      return (.w_QRIS=0)
    endwith
  endfunc

  add object oQRISP_1_22 as StdField with uid="ZWQQHWZXZD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_QRISP", cQueryName = "QRISP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 26361338,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=209, Top=75, cSayPict="v_PV[S]", cGetPict="v_PV[S]"

  func oQRISP_1_22.mHide()
    with this.Parent.oContained
      return (NOT(.w_QRISP<>0 AND .oParentObject.oParentObject .w_VALAPP=.oParentObject.oParentObject .w_PRVALNAZ))
    endwith
  endfunc

  add object oStr_1_1 as StdString with uid="NHNFRWNLRY",Visible=.t., Left=149, Top=6,;
    Alignment=1, Width=53, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="STQCIOYSTM",Visible=.t., Left=144, Top=29,;
    Alignment=1, Width=58, Height=18,;
    Caption="Passivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="OYKVIENCSG",Visible=.t., Left=149, Top=98,;
    Alignment=1, Width=53, Height=18,;
    Caption="Costi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="UQYLMMJFND",Visible=.t., Left=149, Top=121,;
    Alignment=1, Width=53, Height=18,;
    Caption="Ricavi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="KKPGCDIVZI",Visible=.t., Left=92, Top=147,;
    Alignment=1, Width=110, Height=18,;
    Caption="Differenza esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="KGWXROUVRK",Visible=.t., Left=5, Top=170,;
    Alignment=1, Width=197, Height=18,;
    Caption="Differenza esercizio precedente:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (NOT(.w_QPRE<>0 AND .oParentObject.oParentObject .w_VALAPP=.oParentObject.oParentObject .w_PRVALNAZ))
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="WUMUERNEBE",Visible=.t., Left=107, Top=216,;
    Alignment=1, Width=95, Height=18,;
    Caption="Transitori:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_QTRA=0)
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="LBFETNDFVL",Visible=.t., Left=134, Top=193,;
    Alignment=1, Width=68, Height=18,;
    Caption="Ordine:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_QORD=0)
    endwith
  endfunc

  add object oStr_1_19 as StdString with uid="URUVHSJAKH",Visible=.t., Left=85, Top=52,;
    Alignment=1, Width=117, Height=18,;
    Caption="Risultato esercizio:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (.w_QRIS=0)
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="ECDDWVHCQM",Visible=.t., Left=8, Top=75,;
    Alignment=1, Width=194, Height=18,;
    Caption="Risultato esercizio precedente:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (NOT(.w_QRISP<>0 AND .oParentObject.oParentObject .w_VALAPP=.oParentObject.oParentObject .w_PRVALNAZ))
    endwith
  endfunc

  add object oBox_1_5 as StdBox with uid="GAXYHCAUPO",left=200, top=144, width=158,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kre','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
