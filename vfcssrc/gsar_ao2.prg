* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ao2                                                        *
*              Parametri acquisti                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_100]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2013-04-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_ao2")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_ao2")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_ao2")
  return

* --- Class definition
define class tgsar_ao2 as StdPCForm
  Width  = 605
  Height = 247
  Top    = 63
  Left   = 29
  cComment = "Parametri acquisti"
  cPrg = "gsar_ao2"
  HelpContextID=176781161
  add object cnt as tcgsar_ao2
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_ao2 as PCContext
  w_COCODAZI = space(5)
  w_RAGAZI = space(40)
  w_COTIPCON = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = space(8)
  w_UTDV = space(8)
  w_TIPART = space(1)
  w_OBTEST = space(8)
  w_DATOBSO = space(8)
  w_COACQINC = space(15)
  w_COACQIMB = space(15)
  w_COACQTRA = space(15)
  w_COCACPAG = space(5)
  w_DCACPAG = space(35)
  w_COCOCPAG = space(15)
  w_DESCPAG = space(40)
  w_COCOABOL = space(5)
  w_COCOAARR = space(5)
  w_PERIVB = 0
  w_TR22 = space(1)
  w_FP1 = space(1)
  w_DESBOL = space(35)
  w_DESARR = space(35)
  w_COCAUACO = space(5)
  w_CADES = space(35)
  w_TIPREGA = space(1)
  w_DESINCA = space(40)
  w_DESIMBA = space(40)
  w_DESTRAA = space(40)
  w_PART = space(1)
  proc Save(oFrom)
    this.w_COCODAZI = oFrom.w_COCODAZI
    this.w_RAGAZI = oFrom.w_RAGAZI
    this.w_COTIPCON = oFrom.w_COTIPCON
    this.w_UTCC = oFrom.w_UTCC
    this.w_UTCV = oFrom.w_UTCV
    this.w_UTDC = oFrom.w_UTDC
    this.w_UTDV = oFrom.w_UTDV
    this.w_TIPART = oFrom.w_TIPART
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_DATOBSO = oFrom.w_DATOBSO
    this.w_COACQINC = oFrom.w_COACQINC
    this.w_COACQIMB = oFrom.w_COACQIMB
    this.w_COACQTRA = oFrom.w_COACQTRA
    this.w_COCACPAG = oFrom.w_COCACPAG
    this.w_DCACPAG = oFrom.w_DCACPAG
    this.w_COCOCPAG = oFrom.w_COCOCPAG
    this.w_DESCPAG = oFrom.w_DESCPAG
    this.w_COCOABOL = oFrom.w_COCOABOL
    this.w_COCOAARR = oFrom.w_COCOAARR
    this.w_PERIVB = oFrom.w_PERIVB
    this.w_TR22 = oFrom.w_TR22
    this.w_FP1 = oFrom.w_FP1
    this.w_DESBOL = oFrom.w_DESBOL
    this.w_DESARR = oFrom.w_DESARR
    this.w_COCAUACO = oFrom.w_COCAUACO
    this.w_CADES = oFrom.w_CADES
    this.w_TIPREGA = oFrom.w_TIPREGA
    this.w_DESINCA = oFrom.w_DESINCA
    this.w_DESIMBA = oFrom.w_DESIMBA
    this.w_DESTRAA = oFrom.w_DESTRAA
    this.w_PART = oFrom.w_PART
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_COCODAZI = this.w_COCODAZI
    oTo.w_RAGAZI = this.w_RAGAZI
    oTo.w_COTIPCON = this.w_COTIPCON
    oTo.w_UTCC = this.w_UTCC
    oTo.w_UTCV = this.w_UTCV
    oTo.w_UTDC = this.w_UTDC
    oTo.w_UTDV = this.w_UTDV
    oTo.w_TIPART = this.w_TIPART
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_DATOBSO = this.w_DATOBSO
    oTo.w_COACQINC = this.w_COACQINC
    oTo.w_COACQIMB = this.w_COACQIMB
    oTo.w_COACQTRA = this.w_COACQTRA
    oTo.w_COCACPAG = this.w_COCACPAG
    oTo.w_DCACPAG = this.w_DCACPAG
    oTo.w_COCOCPAG = this.w_COCOCPAG
    oTo.w_DESCPAG = this.w_DESCPAG
    oTo.w_COCOABOL = this.w_COCOABOL
    oTo.w_COCOAARR = this.w_COCOAARR
    oTo.w_PERIVB = this.w_PERIVB
    oTo.w_TR22 = this.w_TR22
    oTo.w_FP1 = this.w_FP1
    oTo.w_DESBOL = this.w_DESBOL
    oTo.w_DESARR = this.w_DESARR
    oTo.w_COCAUACO = this.w_COCAUACO
    oTo.w_CADES = this.w_CADES
    oTo.w_TIPREGA = this.w_TIPREGA
    oTo.w_DESINCA = this.w_DESINCA
    oTo.w_DESIMBA = this.w_DESIMBA
    oTo.w_DESTRAA = this.w_DESTRAA
    oTo.w_PART = this.w_PART
    PCContext::Load(oTo)
enddefine

define class tcgsar_ao2 as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 605
  Height = 247
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-04-16"
  HelpContextID=176781161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Constant Properties
  CONTROPA_IDX = 0
  CONTI_IDX = 0
  CAU_CONT_IDX = 0
  VOCIIVA_IDX = 0
  PAG_AMEN_IDX = 0
  AZIENDA_IDX = 0
  KEY_ARTI_IDX = 0
  cFile = "CONTROPA"
  cKeySelect = "COCODAZI"
  cKeyWhere  = "COCODAZI=this.w_COCODAZI"
  cKeyWhereODBC = '"COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cKeyWhereODBCqualified = '"CONTROPA.COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cPrg = "gsar_ao2"
  cComment = "Parametri acquisti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_COCODAZI = space(5)
  w_RAGAZI = space(40)
  w_COTIPCON = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_TIPART = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_COACQINC = space(15)
  w_COACQIMB = space(15)
  w_COACQTRA = space(15)
  w_COCACPAG = space(5)
  w_DCACPAG = space(35)
  w_COCOCPAG = space(15)
  w_DESCPAG = space(40)
  w_COCOABOL = space(5)
  w_COCOAARR = space(5)
  w_PERIVB = 0
  w_TR22 = space(1)
  w_FP1 = space(1)
  w_DESBOL = space(35)
  w_DESARR = space(35)
  w_COCAUACO = space(5)
  w_CADES = space(35)
  w_TIPREGA = space(1)
  w_DESINCA = space(40)
  w_DESIMBA = space(40)
  w_DESTRAA = space(40)
  w_PART = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_ao2Pag1","gsar_ao2",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 118333706
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCOACQINC_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsar_ao2
    * --- nMaxFieldsJoin di default vale 200
    this.parent.nMaxFieldsJoin = iif(lower(i_ServerConn[1,6])="oracle", 150, this.parent.nMaxFieldsJoin)
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='VOCIIVA'
    this.cWorkTables[4]='PAG_AMEN'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='KEY_ARTI'
    this.cWorkTables[7]='CONTROPA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONTROPA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONTROPA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_ao2'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_11_joined
    link_1_11_joined=.f.
    local link_1_12_joined
    link_1_12_joined=.f.
    local link_1_13_joined
    link_1_13_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_29_joined
    link_1_29_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CONTROPA where COCODAZI=KeySet.COCODAZI
    *
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONTROPA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONTROPA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONTROPA '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_29_joined=this.AddJoinedLink_1_29(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RAGAZI = space(40)
        .w_TIPART = space(1)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_DCACPAG = space(35)
        .w_DESCPAG = space(40)
        .w_PERIVB = 0
        .w_TR22 = space(1)
        .w_FP1 = space(1)
        .w_DESBOL = space(35)
        .w_DESARR = space(35)
        .w_CADES = space(35)
        .w_TIPREGA = space(1)
        .w_DESINCA = space(40)
        .w_DESIMBA = space(40)
        .w_DESTRAA = space(40)
        .w_PART = space(1)
        .w_COCODAZI = NVL(COCODAZI,space(5))
          if link_1_1_joined
            this.w_COCODAZI = NVL(AZCODAZI101,NVL(this.w_COCODAZI,space(5)))
            this.w_RAGAZI = NVL(AZRAGAZI101,space(40))
          else
          .link_1_1('Load')
          endif
        .w_COTIPCON = NVL(COTIPCON,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_COACQINC = NVL(COACQINC,space(15))
          if link_1_11_joined
            this.w_COACQINC = NVL(ANCODICE111,NVL(this.w_COACQINC,space(15)))
            this.w_DESINCA = NVL(ANDESCRI111,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO111),ctod("  /  /  "))
            this.w_PART = NVL(ANPARTSN111,space(1))
          else
          .link_1_11('Load')
          endif
        .w_COACQIMB = NVL(COACQIMB,space(15))
          if link_1_12_joined
            this.w_COACQIMB = NVL(ANCODICE112,NVL(this.w_COACQIMB,space(15)))
            this.w_DESIMBA = NVL(ANDESCRI112,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO112),ctod("  /  /  "))
            this.w_PART = NVL(ANPARTSN112,space(1))
          else
          .link_1_12('Load')
          endif
        .w_COACQTRA = NVL(COACQTRA,space(15))
          if link_1_13_joined
            this.w_COACQTRA = NVL(ANCODICE113,NVL(this.w_COACQTRA,space(15)))
            this.w_DESTRAA = NVL(ANDESCRI113,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO113),ctod("  /  /  "))
            this.w_PART = NVL(ANPARTSN113,space(1))
          else
          .link_1_13('Load')
          endif
        .w_COCACPAG = NVL(COCACPAG,space(5))
          if link_1_14_joined
            this.w_COCACPAG = NVL(CCCODICE114,NVL(this.w_COCACPAG,space(5)))
            this.w_DCACPAG = NVL(CCDESCRI114,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO114),ctod("  /  /  "))
            this.w_TR22 = NVL(CCTIPREG114,space(1))
            this.w_FP1 = NVL(CCFLPART114,space(1))
          else
          .link_1_14('Load')
          endif
        .w_COCOCPAG = NVL(COCOCPAG,space(15))
          if link_1_16_joined
            this.w_COCOCPAG = NVL(ANCODICE116,NVL(this.w_COCOCPAG,space(15)))
            this.w_DESCPAG = NVL(ANDESCRI116,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO116),ctod("  /  /  "))
          else
          .link_1_16('Load')
          endif
        .w_COCOABOL = NVL(COCOABOL,space(5))
          if link_1_18_joined
            this.w_COCOABOL = NVL(IVCODIVA118,NVL(this.w_COCOABOL,space(5)))
            this.w_PERIVB = NVL(IVPERIVA118,0)
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO118),ctod("  /  /  "))
            this.w_DESBOL = NVL(IVDESIVA118,space(35))
          else
          .link_1_18('Load')
          endif
        .w_COCOAARR = NVL(COCOAARR,space(5))
          if link_1_19_joined
            this.w_COCOAARR = NVL(IVCODIVA119,NVL(this.w_COCOAARR,space(5)))
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO119),ctod("  /  /  "))
            this.w_DESARR = NVL(IVDESIVA119,space(35))
          else
          .link_1_19('Load')
          endif
        .w_COCAUACO = NVL(COCAUACO,space(5))
          if link_1_29_joined
            this.w_COCAUACO = NVL(CCCODICE129,NVL(this.w_COCAUACO,space(5)))
            this.w_CADES = NVL(CCDESCRI129,space(35))
            this.w_TIPREGA = NVL(CCTIPREG129,space(1))
          else
          .link_1_29('Load')
          endif
        cp_LoadRecExtFlds(this,'CONTROPA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_COCODAZI = space(5)
      .w_RAGAZI = space(40)
      .w_COTIPCON = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_TIPART = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_COACQINC = space(15)
      .w_COACQIMB = space(15)
      .w_COACQTRA = space(15)
      .w_COCACPAG = space(5)
      .w_DCACPAG = space(35)
      .w_COCOCPAG = space(15)
      .w_DESCPAG = space(40)
      .w_COCOABOL = space(5)
      .w_COCOAARR = space(5)
      .w_PERIVB = 0
      .w_TR22 = space(1)
      .w_FP1 = space(1)
      .w_DESBOL = space(35)
      .w_DESARR = space(35)
      .w_COCAUACO = space(5)
      .w_CADES = space(35)
      .w_TIPREGA = space(1)
      .w_DESINCA = space(40)
      .w_DESIMBA = space(40)
      .w_DESTRAA = space(40)
      .w_PART = space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_COCODAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_COTIPCON = 'G'
          .DoRTCalc(4,8,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(10,11,.f.)
          if not(empty(.w_COACQINC))
          .link_1_11('Full')
          endif
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_COACQIMB))
          .link_1_12('Full')
          endif
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_COACQTRA))
          .link_1_13('Full')
          endif
        .DoRTCalc(14,14,.f.)
          if not(empty(.w_COCACPAG))
          .link_1_14('Full')
          endif
        .DoRTCalc(15,16,.f.)
          if not(empty(.w_COCOCPAG))
          .link_1_16('Full')
          endif
        .DoRTCalc(17,18,.f.)
          if not(empty(.w_COCOABOL))
          .link_1_18('Full')
          endif
        .DoRTCalc(19,19,.f.)
          if not(empty(.w_COCOAARR))
          .link_1_19('Full')
          endif
        .DoRTCalc(20,25,.f.)
          if not(empty(.w_COCAUACO))
          .link_1_29('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONTROPA')
    this.DoRTCalc(26,31,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCOACQINC_1_11.enabled = i_bVal
      .Page1.oPag.oCOACQIMB_1_12.enabled = i_bVal
      .Page1.oPag.oCOACQTRA_1_13.enabled = i_bVal
      .Page1.oPag.oCOCACPAG_1_14.enabled = i_bVal
      .Page1.oPag.oCOCOCPAG_1_16.enabled = i_bVal
      .Page1.oPag.oCOCOABOL_1_18.enabled = i_bVal
      .Page1.oPag.oCOCOAARR_1_19.enabled = i_bVal
      .Page1.oPag.oCOCAUACO_1_29.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CONTROPA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODAZI,"COCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPCON,"COTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COACQINC,"COACQINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COACQIMB,"COACQIMB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COACQTRA,"COACQTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCACPAG,"COCACPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCOCPAG,"COCOCPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCOABOL,"COCOABOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCOAARR,"COCOAARR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCAUACO,"COCAUACO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CONTROPA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CONTROPA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValODBCExtFlds(this,'CONTROPA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(COCODAZI,COTIPCON,UTCC,UTCV,UTDC"+;
                  ",UTDV,COACQINC,COACQIMB,COACQTRA,COCACPAG"+;
                  ",COCOCPAG,COCOABOL,COCOAARR,COCAUACO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_COCODAZI)+;
                  ","+cp_ToStrODBC(this.w_COTIPCON)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBCNull(this.w_COACQINC)+;
                  ","+cp_ToStrODBCNull(this.w_COACQIMB)+;
                  ","+cp_ToStrODBCNull(this.w_COACQTRA)+;
                  ","+cp_ToStrODBCNull(this.w_COCACPAG)+;
                  ","+cp_ToStrODBCNull(this.w_COCOCPAG)+;
                  ","+cp_ToStrODBCNull(this.w_COCOABOL)+;
                  ","+cp_ToStrODBCNull(this.w_COCOAARR)+;
                  ","+cp_ToStrODBCNull(this.w_COCAUACO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValVFPExtFlds(this,'CONTROPA')
        cp_CheckDeletedKey(i_cTable,0,'COCODAZI',this.w_COCODAZI)
        INSERT INTO (i_cTable);
              (COCODAZI,COTIPCON,UTCC,UTCV,UTDC,UTDV,COACQINC,COACQIMB,COACQTRA,COCACPAG,COCOCPAG,COCOABOL,COCOAARR,COCAUACO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_COCODAZI;
                  ,this.w_COTIPCON;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_COACQINC;
                  ,this.w_COACQIMB;
                  ,this.w_COACQTRA;
                  ,this.w_COCACPAG;
                  ,this.w_COCOCPAG;
                  ,this.w_COCOABOL;
                  ,this.w_COCOAARR;
                  ,this.w_COCAUACO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CONTROPA_IDX,i_nConn)
      *
      * update CONTROPA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CONTROPA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " COTIPCON="+cp_ToStrODBC(this.w_COTIPCON)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",COACQINC="+cp_ToStrODBCNull(this.w_COACQINC)+;
             ",COACQIMB="+cp_ToStrODBCNull(this.w_COACQIMB)+;
             ",COACQTRA="+cp_ToStrODBCNull(this.w_COACQTRA)+;
             ",COCACPAG="+cp_ToStrODBCNull(this.w_COCACPAG)+;
             ",COCOCPAG="+cp_ToStrODBCNull(this.w_COCOCPAG)+;
             ",COCOABOL="+cp_ToStrODBCNull(this.w_COCOABOL)+;
             ",COCOAARR="+cp_ToStrODBCNull(this.w_COCOAARR)+;
             ",COCAUACO="+cp_ToStrODBCNull(this.w_COCAUACO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CONTROPA')
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        UPDATE (i_cTable) SET;
              COTIPCON=this.w_COTIPCON;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,COACQINC=this.w_COACQINC;
             ,COACQIMB=this.w_COACQIMB;
             ,COACQTRA=this.w_COACQTRA;
             ,COCACPAG=this.w_COCACPAG;
             ,COCOCPAG=this.w_COCOCPAG;
             ,COCOABOL=this.w_COCOABOL;
             ,COCOAARR=this.w_COCOAARR;
             ,COCAUACO=this.w_COCAUACO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CONTROPA_IDX,i_nConn)
      *
      * delete CONTROPA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
            .w_COTIPCON = 'G'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,31,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COCODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRAGAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_COCODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_COCODAZI)
            select AZCODAZI,AZRAGAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_RAGAZI = NVL(_Link_.AZRAGAZI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCODAZI = space(5)
      endif
      this.w_RAGAZI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AZIENDA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.AZCODAZI as AZCODAZI101"+ ",link_1_1.AZRAGAZI as AZRAGAZI101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on CONTROPA.COCODAZI=link_1_1.AZCODAZI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and CONTROPA.COCODAZI=link_1_1.AZCODAZI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COACQINC
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COACQINC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COACQINC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COACQINC))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COACQINC)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COACQINC)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COACQINC)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COACQINC) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOACQINC_1_11'),i_cWhere,'GSAR_API',"Conti spese incasso",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COACQINC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COACQINC);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COACQINC)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COACQINC = NVL(_Link_.ANCODICE,space(15))
      this.w_DESINCA = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PART = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COACQINC = space(15)
      endif
      this.w_DESINCA = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_PART<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_COACQINC = space(15)
        this.w_DESINCA = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COACQINC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.ANCODICE as ANCODICE111"+ ",link_1_11.ANDESCRI as ANDESCRI111"+ ",link_1_11.ANDTOBSO as ANDTOBSO111"+ ",link_1_11.ANPARTSN as ANPARTSN111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on CONTROPA.COACQINC=link_1_11.ANCODICE"+" and CONTROPA.COTIPCON=link_1_11.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and CONTROPA.COACQINC=link_1_11.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_11.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COACQIMB
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COACQIMB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COACQIMB)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COACQIMB))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COACQIMB)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COACQIMB)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COACQIMB)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COACQIMB) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOACQIMB_1_12'),i_cWhere,'GSAR_API',"Conti spese imballo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COACQIMB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COACQIMB);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COACQIMB)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COACQIMB = NVL(_Link_.ANCODICE,space(15))
      this.w_DESIMBA = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PART = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COACQIMB = space(15)
      endif
      this.w_DESIMBA = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_PART<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_COACQIMB = space(15)
        this.w_DESIMBA = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COACQIMB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.ANCODICE as ANCODICE112"+ ",link_1_12.ANDESCRI as ANDESCRI112"+ ",link_1_12.ANDTOBSO as ANDTOBSO112"+ ",link_1_12.ANPARTSN as ANPARTSN112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on CONTROPA.COACQIMB=link_1_12.ANCODICE"+" and CONTROPA.COTIPCON=link_1_12.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and CONTROPA.COACQIMB=link_1_12.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_12.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COACQTRA
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COACQTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COACQTRA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COACQTRA))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COACQTRA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COACQTRA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COACQTRA)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COACQTRA) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOACQTRA_1_13'),i_cWhere,'GSAR_API',"Conti spese trasporto",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COACQTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COACQTRA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COACQTRA)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COACQTRA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESTRAA = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PART = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COACQTRA = space(15)
      endif
      this.w_DESTRAA = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_PART<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_COACQTRA = space(15)
        this.w_DESTRAA = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COACQTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.ANCODICE as ANCODICE113"+ ",link_1_13.ANDESCRI as ANDESCRI113"+ ",link_1_13.ANDTOBSO as ANDTOBSO113"+ ",link_1_13.ANPARTSN as ANPARTSN113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on CONTROPA.COACQTRA=link_1_13.ANCODICE"+" and CONTROPA.COTIPCON=link_1_13.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and CONTROPA.COACQTRA=link_1_13.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_13.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCACPAG
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCACPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCACPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCACPAG))
          select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCACPAG)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCACPAG) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCACPAG_1_14'),i_cWhere,'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCACPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCACPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCACPAG)
            select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCACPAG = NVL(_Link_.CCCODICE,space(5))
      this.w_DCACPAG = NVL(_Link_.CCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_TR22 = NVL(_Link_.CCTIPREG,space(1))
      this.w_FP1 = NVL(_Link_.CCFLPART,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCACPAG = space(5)
      endif
      this.w_DCACPAG = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TR22 = space(1)
      this.w_FP1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TR22='N' AND (.w_FP1<>'N' OR g_PERPAR<>'S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente o no partite o obsoleta o di tipo IVA")
        endif
        this.w_COCACPAG = space(5)
        this.w_DCACPAG = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TR22 = space(1)
        this.w_FP1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCACPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.CCCODICE as CCCODICE114"+ ",link_1_14.CCDESCRI as CCDESCRI114"+ ",link_1_14.CCDTOBSO as CCDTOBSO114"+ ",link_1_14.CCTIPREG as CCTIPREG114"+ ",link_1_14.CCFLPART as CCFLPART114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on CONTROPA.COCACPAG=link_1_14.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and CONTROPA.COCACPAG=link_1_14.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCOCPAG
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCOCPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCOCPAG)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCOCPAG))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCOCPAG)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_COCOCPAG)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_COCOCPAG)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_COTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCOCPAG) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCOCPAG_1_16'),i_cWhere,'GSAR_BZC',"Conti contropartita",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCOCPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCOCPAG);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCOCPAG)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCOCPAG = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCPAG = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCOCPAG = space(15)
      endif
      this.w_DESCPAG = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_COCOCPAG = space(15)
        this.w_DESCPAG = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCOCPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.ANCODICE as ANCODICE116"+ ",link_1_16.ANDESCRI as ANDESCRI116"+ ",link_1_16.ANDTOBSO as ANDTOBSO116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on CONTROPA.COCOCPAG=link_1_16.ANCODICE"+" and CONTROPA.COTIPCON=link_1_16.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and CONTROPA.COCOCPAG=link_1_16.ANCODICE(+)"'+'+" and CONTROPA.COTIPCON=link_1_16.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCOABOL
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCOABOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_COCOABOL)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVDTOBSO,IVDESIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_COCOABOL))
          select IVCODIVA,IVPERIVA,IVDTOBSO,IVDESIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCOABOL)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCOABOL) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCOCOABOL_1_18'),i_cWhere,'GSAR_AIV',"Voci IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVDTOBSO,IVDESIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVPERIVA,IVDTOBSO,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCOABOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVPERIVA,IVDTOBSO,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_COCOABOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_COCOABOL)
            select IVCODIVA,IVPERIVA,IVDTOBSO,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCOABOL = NVL(_Link_.IVCODIVA,space(5))
      this.w_PERIVB = NVL(_Link_.IVPERIVA,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
      this.w_DESBOL = NVL(_Link_.IVDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_COCOABOL = space(5)
      endif
      this.w_PERIVB = 0
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DESBOL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PERIVB=0 and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
        endif
        this.w_COCOABOL = space(5)
        this.w_PERIVB = 0
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DESBOL = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCOABOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.IVCODIVA as IVCODIVA118"+ ",link_1_18.IVPERIVA as IVPERIVA118"+ ",link_1_18.IVDTOBSO as IVDTOBSO118"+ ",link_1_18.IVDESIVA as IVDESIVA118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on CONTROPA.COCOABOL=link_1_18.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and CONTROPA.COCOABOL=link_1_18.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCOAARR
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCOAARR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_COCOAARR)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO,IVDESIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_COCOAARR))
          select IVCODIVA,IVDTOBSO,IVDESIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCOAARR)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCOAARR) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oCOCOAARR_1_19'),i_cWhere,'GSAR_AIV',"Voci IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO,IVDESIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDTOBSO,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCOAARR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_COCOAARR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_COCOAARR)
            select IVCODIVA,IVDTOBSO,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCOAARR = NVL(_Link_.IVCODIVA,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
      this.w_DESARR = NVL(_Link_.IVDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_COCOAARR = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_DESARR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
        endif
        this.w_COCOAARR = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_DESARR = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCOAARR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.IVCODIVA as IVCODIVA119"+ ",link_1_19.IVDTOBSO as IVDTOBSO119"+ ",link_1_19.IVDESIVA as IVDESIVA119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on CONTROPA.COCOAARR=link_1_19.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and CONTROPA.COCOAARR=link_1_19.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCAUACO
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCAUACO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_COCAUACO)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_COCAUACO))
          select CCCODICE,CCDESCRI,CCTIPREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCAUACO)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_COCAUACO)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_COCAUACO)+"%");

            select CCCODICE,CCDESCRI,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCAUACO) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCOCAUACO_1_29'),i_cWhere,'GSCG_ACC',"",'GSAR_AO1.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCAUACO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_COCAUACO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_COCAUACO)
            select CCCODICE,CCDESCRI,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCAUACO = NVL(_Link_.CCCODICE,space(5))
      this.w_CADES = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPREGA = NVL(_Link_.CCTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCAUACO = space(5)
      endif
      this.w_CADES = space(35)
      this.w_TIPREGA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPREGA='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente o con IVA")
        endif
        this.w_COCAUACO = space(5)
        this.w_CADES = space(35)
        this.w_TIPREGA = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCAUACO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_29(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_29.CCCODICE as CCCODICE129"+ ",link_1_29.CCDESCRI as CCDESCRI129"+ ",link_1_29.CCTIPREG as CCTIPREG129"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_29 on CONTROPA.COCAUACO=link_1_29.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_29"
          i_cKey=i_cKey+'+" and CONTROPA.COCAUACO=link_1_29.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOACQINC_1_11.value==this.w_COACQINC)
      this.oPgFrm.Page1.oPag.oCOACQINC_1_11.value=this.w_COACQINC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOACQIMB_1_12.value==this.w_COACQIMB)
      this.oPgFrm.Page1.oPag.oCOACQIMB_1_12.value=this.w_COACQIMB
    endif
    if not(this.oPgFrm.Page1.oPag.oCOACQTRA_1_13.value==this.w_COACQTRA)
      this.oPgFrm.Page1.oPag.oCOACQTRA_1_13.value=this.w_COACQTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCACPAG_1_14.value==this.w_COCACPAG)
      this.oPgFrm.Page1.oPag.oCOCACPAG_1_14.value=this.w_COCACPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDCACPAG_1_15.value==this.w_DCACPAG)
      this.oPgFrm.Page1.oPag.oDCACPAG_1_15.value=this.w_DCACPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCOCPAG_1_16.value==this.w_COCOCPAG)
      this.oPgFrm.Page1.oPag.oCOCOCPAG_1_16.value=this.w_COCOCPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCPAG_1_17.value==this.w_DESCPAG)
      this.oPgFrm.Page1.oPag.oDESCPAG_1_17.value=this.w_DESCPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCOABOL_1_18.value==this.w_COCOABOL)
      this.oPgFrm.Page1.oPag.oCOCOABOL_1_18.value=this.w_COCOABOL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCOAARR_1_19.value==this.w_COCOAARR)
      this.oPgFrm.Page1.oPag.oCOCOAARR_1_19.value=this.w_COCOAARR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBOL_1_27.value==this.w_DESBOL)
      this.oPgFrm.Page1.oPag.oDESBOL_1_27.value=this.w_DESBOL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESARR_1_28.value==this.w_DESARR)
      this.oPgFrm.Page1.oPag.oDESARR_1_28.value=this.w_DESARR
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCAUACO_1_29.value==this.w_COCAUACO)
      this.oPgFrm.Page1.oPag.oCOCAUACO_1_29.value=this.w_COCAUACO
    endif
    if not(this.oPgFrm.Page1.oPag.oCADES_1_30.value==this.w_CADES)
      this.oPgFrm.Page1.oPag.oCADES_1_30.value=this.w_CADES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINCA_1_33.value==this.w_DESINCA)
      this.oPgFrm.Page1.oPag.oDESINCA_1_33.value=this.w_DESINCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIMBA_1_34.value==this.w_DESIMBA)
      this.oPgFrm.Page1.oPag.oDESIMBA_1_34.value=this.w_DESIMBA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTRAA_1_35.value==this.w_DESTRAA)
      this.oPgFrm.Page1.oPag.oDESTRAA_1_35.value=this.w_DESTRAA
    endif
    cp_SetControlsValueExtFlds(this,'CONTROPA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_PART<>'S')  and not(empty(.w_COACQINC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOACQINC_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_PART<>'S')  and not(empty(.w_COACQIMB))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOACQIMB_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_PART<>'S')  and not(empty(.w_COACQTRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOACQTRA_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
          case   ((empty(.w_COCACPAG)) or not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TR22='N' AND (.w_FP1<>'N' OR g_PERPAR<>'S')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCACPAG_1_14.SetFocus()
            i_bnoObbl = !empty(.w_COCACPAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente o no partite o obsoleta o di tipo IVA")
          case   ((empty(.w_COCOCPAG)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCOCPAG_1_16.SetFocus()
            i_bnoObbl = !empty(.w_COCOCPAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(.w_PERIVB=0 and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_COCOABOL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCOABOL_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_COCOAARR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCOAARR_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
          case   not(.w_TIPREGA='N')  and not(empty(.w_COCAUACO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCAUACO_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente o con IVA")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_ao2Pag1 as StdContainer
  Width  = 601
  height = 247
  stdWidth  = 601
  stdheight = 247
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOACQINC_1_11 as StdField with uid="HPNPAIOBCJ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_COACQINC", cQueryName = "COACQINC",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Se presente: conto di contabilizzazione delle spese di incasso per acquisti",;
    HelpContextID = 204608407,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=167, Top=12, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COACQINC"

  func oCOACQINC_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOACQINC_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOACQINC_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOACQINC_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti spese incasso",'',this.parent.oContained
  endproc
  proc oCOACQINC_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COACQINC
     i_obj.ecpSave()
  endproc

  add object oCOACQIMB_1_12 as StdField with uid="AJPCRJZTOH",rtseq=12,rtrep=.f.,;
    cFormVar = "w_COACQIMB", cQueryName = "COACQIMB",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Se presente: conto di contabilizzazione delle spese di imballo per acquisti",;
    HelpContextID = 204608408,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=167, Top=41, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COACQIMB"

  func oCOACQIMB_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOACQIMB_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOACQIMB_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOACQIMB_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti spese imballo",'',this.parent.oContained
  endproc
  proc oCOACQIMB_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COACQIMB
     i_obj.ecpSave()
  endproc

  add object oCOACQTRA_1_13 as StdField with uid="UCUDGELESN",rtseq=13,rtrep=.f.,;
    cFormVar = "w_COACQTRA", cQueryName = "COACQTRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Se presente: conto di contabilizzazione delle spese di trasporto per acquisti",;
    HelpContextID = 248376423,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=167, Top=70, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COACQTRA"

  func oCOACQTRA_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOACQTRA_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOACQTRA_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOACQTRA_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti spese trasporto",'',this.parent.oContained
  endproc
  proc oCOACQTRA_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COACQTRA
     i_obj.ecpSave()
  endproc

  add object oCOCACPAG_1_14 as StdField with uid="RUDPWZNULO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_COCACPAG", cQueryName = "COCACPAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente o no partite o obsoleta o di tipo IVA",;
    ToolTipText = "Causale contabile per la contabilizzazione degli acconti pagati",;
    HelpContextID = 101970835,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=167, Top=99, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCACPAG"

  func oCOCACPAG_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCACPAG_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCACPAG_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCACPAG_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'CAUCOEFA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCACPAG_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCACPAG
     i_obj.ecpSave()
  endproc

  add object oDCACPAG_1_15 as StdField with uid="CRWGHVNNVT",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DCACPAG", cQueryName = "DCACPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 196993078,;
   bGlobalFont=.t.,;
    Height=21, Width=287, Left=307, Top=99, InputMask=replicate('X',35)

  add object oCOCOCPAG_1_16 as StdField with uid="OTVJHSFXEY",rtseq=16,rtrep=.f.,;
    cFormVar = "w_COCOCPAG", cQueryName = "COCOCPAG",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Conto cassa o banca per contabilizzazione acconti pagati",;
    HelpContextID = 101053331,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=167, Top=128, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCOCPAG"

  func oCOCOCPAG_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCOCPAG_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCOCPAG_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCOCPAG_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contropartita",'',this.parent.oContained
  endproc
  proc oCOCOCPAG_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCOCPAG
     i_obj.ecpSave()
  endproc

  add object oDESCPAG_1_17 as StdField with uid="KWXHJLNOEA",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESCPAG", cQueryName = "DESCPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 197067318,;
   bGlobalFont=.t.,;
    Height=21, Width=287, Left=307, Top=128, InputMask=replicate('X',40)

  add object oCOCOABOL_1_18 as StdField with uid="NWBILEVFQZ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_COCOABOL", cQueryName = "COCOABOL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA obsoleto oppure inesistente",;
    ToolTipText = "Codice IVA da applicare sull'importo dei bolli",;
    HelpContextID = 69596046,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=167, Top=157, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_COCOABOL"

  func oCOCOABOL_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCOABOL_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCOABOL_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCOCOABOL_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'',this.parent.oContained
  endproc
  proc oCOCOABOL_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_COCOABOL
     i_obj.ecpSave()
  endproc

  add object oCOCOAARR_1_19 as StdField with uid="UUIRPEYKOU",rtseq=19,rtrep=.f.,;
    cFormVar = "w_COCOAARR", cQueryName = "COCOAARR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA obsoleto oppure inesistente",;
    ToolTipText = "Eventuale codice IVA da applicare sugli eventuali arrotondamenti",;
    HelpContextID = 86373256,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=167, Top=186, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_COCOAARR"

  func oCOCOAARR_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCOAARR_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCOAARR_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oCOCOAARR_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Voci IVA",'',this.parent.oContained
  endproc
  proc oCOCOAARR_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_COCOAARR
     i_obj.ecpSave()
  endproc

  add object oDESBOL_1_27 as StdField with uid="FWJOGIMAAA",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESBOL", cQueryName = "DESBOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 156368330,;
   bGlobalFont=.t.,;
    Height=21, Width=287, Left=307, Top=157, InputMask=replicate('X',35)

  add object oDESARR_1_28 as StdField with uid="NGNDQLVDKM",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESARR", cQueryName = "DESARR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 52624842,;
   bGlobalFont=.t.,;
    Height=21, Width=287, Left=307, Top=186, InputMask=replicate('X',35)

  add object oCOCAUACO_1_29 as StdField with uid="TWGTILBHUB",rtseq=25,rtrep=.f.,;
    cFormVar = "w_COCAUACO", cQueryName = "COCAUACO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente o con IVA",;
    ToolTipText = "Causale contabile per contabilizzazione omaggi (conto e contropartita)",;
    HelpContextID = 202116213,;
   bGlobalFont=.t.,;
    Height=21, Width=71, Left=167, Top=215, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_COCAUACO"

  func oCOCAUACO_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCAUACO_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCAUACO_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCOCAUACO_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"",'GSAR_AO1.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCOCAUACO_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_COCAUACO
     i_obj.ecpSave()
  endproc

  add object oCADES_1_30 as StdField with uid="DKENHGRUJY",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CADES", cQueryName = "CADES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 84931034,;
   bGlobalFont=.t.,;
    Height=21, Width=287, Left=307, Top=215, InputMask=replicate('X',35)

  add object oDESINCA_1_33 as StdField with uid="KFTVNJZPJA",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESINCA", cQueryName = "DESINCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 39517642,;
   bGlobalFont=.t.,;
    Height=21, Width=287, Left=307, Top=12, InputMask=replicate('X',40)

  add object oDESIMBA_1_34 as StdField with uid="GVFNAYFZBA",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESIMBA", cQueryName = "DESIMBA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 57343434,;
   bGlobalFont=.t.,;
    Height=21, Width=287, Left=307, Top=41, InputMask=replicate('X',40)

  add object oDESTRAA_1_35 as StdField with uid="VIWAGUUZQI",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESTRAA", cQueryName = "DESTRAA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 68156874,;
   bGlobalFont=.t.,;
    Height=21, Width=287, Left=307, Top=70, InputMask=replicate('X',40)

  add object oStr_1_21 as StdString with uid="CVULPCOAQP",Visible=.t., Left=6, Top=128,;
    Alignment=1, Width=159, Height=15,;
    Caption="Conto acconti pagati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="FXGOSLFTSZ",Visible=.t., Left=6, Top=99,;
    Alignment=1, Width=159, Height=15,;
    Caption="Causale acconti pagati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="VYSNUTIEFH",Visible=.t., Left=6, Top=157,;
    Alignment=1, Width=159, Height=18,;
    Caption="Cod.IVA bolli:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="LXPQGDCBRP",Visible=.t., Left=6, Top=189,;
    Alignment=1, Width=159, Height=18,;
    Caption="Cod.IVA arrotondamenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="QEODHCXJGP",Visible=.t., Left=6, Top=221,;
    Alignment=1, Width=159, Height=18,;
    Caption="Causale omaggi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="PSHDSWGLCU",Visible=.t., Left=6, Top=12,;
    Alignment=1, Width=159, Height=15,;
    Caption="Spese incasso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="JOEUMZDTDH",Visible=.t., Left=6, Top=41,;
    Alignment=1, Width=159, Height=15,;
    Caption="Spese imballo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="IGMPPEBGEJ",Visible=.t., Left=6, Top=70,;
    Alignment=1, Width=159, Height=15,;
    Caption="Spese trasporto:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ao2','CONTROPA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".COCODAZI=CONTROPA.COCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
