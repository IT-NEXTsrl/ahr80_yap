* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bbn                                                        *
*              Verifica univocit�                                              *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-12-01                                                      *
* Last revis.: 2007-01-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bbn",oParentObject)
return(i_retval)

define class tgsut_bbn as StdBatch
  * --- Local variables
  w_CLAALLE = space(5)
  w_CHIAVE = space(15)
  w_ARCHIVIO = space(100)
  w_NOMEFILE = space(100)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico se richiesta l'univocit� dell'allegato
    this.w_CLAALLE = this.oParentObject.w_IDCLAALL
    this.w_ARCHIVIO = this.oParentObject.w_IDTABKEY
    this.w_CHIAVE = this.oParentObject.w_IDVALATT
    this.w_NOMEFILE = this.oParentObject.w_IDNOMFIL
    this.oParentObject.w_UNIVOC = .T.
    * --- Select from GSUT1BCU
    do vq_exec with 'GSUT1BCU',this,'_Curs_GSUT1BCU','',.f.,.t.
    if used('_Curs_GSUT1BCU')
      select _Curs_GSUT1BCU
      locate for 1=1
      do while not(eof())
      AH_ERRORMSG("E' gi� stato allegato un file, l'univocit� della tipologia non consente ulteriori allegati!",48,"","")
      this.oParentObject.w_UNIVOC = .F.
      exit
        select _Curs_GSUT1BCU
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_GSUT1BCU')
      use in _Curs_GSUT1BCU
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
