* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_kpa                                                        *
*              Parametri PDA                                                   *
*                                                                              *
*      Author: Zucchetti TAM Srl                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_49]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-18                                                      *
* Last revis.: 2017-04-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsac_kpa",oParentObject))

* --- Class definition
define class tgsac_kpa as StdForm
  Top    = 1
  Left   = 9

  * --- Standard Properties
  Width  = 555
  Height = 626
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-04-04"
  HelpContextID=149579625
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=40

  * --- Constant Properties
  _IDX = 0
  PAR_PROD_IDX = 0
  MAGAZZIN_IDX = 0
  TAB_CALE_IDX = 0
  CENCOST_IDX = 0
  CAN_TIER_IDX = 0
  cPrg = "gsac_kpa"
  cComment = "Parametri PDA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_PPNMAXPE = 0
  w_PPNUMGIO = 0
  w_PPNUMSET = 0
  w_PPNUMMES = 0
  w_PPNUMTRI = 0
  w_PPCOLMPS = 0
  w_PPFONMPS = 0
  w_PPMAGPRO = space(5)
  w_DESMAG = space(30)
  w_DISMAG = space(1)
  w_PP_CICLI = space(1)
  o_PP_CICLI = space(1)
  w_PP___DTF = 0
  w_PPCALSTA = space(5)
  w_PPFLCCOS = space(1)
  o_PPFLCCOS = space(1)
  w_PPCENCOS = space(15)
  w_COLSUG = 0
  w_SFOSUG = 0
  w_COLCON = 0
  w_SFOCON = 0
  w_COLPIA = 0
  w_SFOPIA = 0
  w_PDACOLMIS = 0
  w_PDASFOMIS = 0
  w_BOX = space(0)
  w_BOX = space(0)
  w_BOX = space(0)
  w_BOX = space(0)
  w_SFOMCO = 0
  w_PPFLAROB = space(1)
  w_PPSALCOM = space(1)
  w_PPCODCOM = space(15)
  w_PPBLOMRP = space(1)
  w_PPNOSCSM = space(1)
  w_DESCAL = space(40)
  w_CRIFOR = space(1)
  w_CRIELA = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_DESCEN = space(40)
  w_DESCOM = space(40)
  w_SUGGE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsac_kpaPag1","gsac_kpa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generali")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPPNUMGIO_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_SUGGE = this.oPgFrm.Pages(1).oPag.SUGGE
    DoDefault()
    proc Destroy()
      this.w_SUGGE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='PAR_PROD'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='TAB_CALE'
    this.cWorkTables[4]='CENCOST'
    this.cWorkTables[5]='CAN_TIER'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_PPNMAXPE=0
      .w_PPNUMGIO=0
      .w_PPNUMSET=0
      .w_PPNUMMES=0
      .w_PPNUMTRI=0
      .w_PPCOLMPS=0
      .w_PPFONMPS=0
      .w_PPMAGPRO=space(5)
      .w_DESMAG=space(30)
      .w_DISMAG=space(1)
      .w_PP_CICLI=space(1)
      .w_PP___DTF=0
      .w_PPCALSTA=space(5)
      .w_PPFLCCOS=space(1)
      .w_PPCENCOS=space(15)
      .w_COLSUG=0
      .w_SFOSUG=0
      .w_COLCON=0
      .w_SFOCON=0
      .w_COLPIA=0
      .w_SFOPIA=0
      .w_PDACOLMIS=0
      .w_PDASFOMIS=0
      .w_BOX=space(0)
      .w_BOX=space(0)
      .w_BOX=space(0)
      .w_BOX=space(0)
      .w_SFOMCO=0
      .w_PPFLAROB=space(1)
      .w_PPSALCOM=space(1)
      .w_PPCODCOM=space(15)
      .w_PPBLOMRP=space(1)
      .w_PPNOSCSM=space(1)
      .w_DESCAL=space(40)
      .w_CRIFOR=space(1)
      .w_CRIELA=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESCEN=space(40)
      .w_DESCOM=space(40)
      .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .w_OBTEST = i_DATSYS
          .DoRTCalc(2,6,.f.)
        .w_PPCOLMPS = 65
        .w_PPFONMPS = 9
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_PPMAGPRO))
          .link_1_20('Full')
        endif
          .DoRTCalc(10,11,.f.)
        .w_PP_CICLI = ' '
        .w_PP___DTF = 0
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_PPCALSTA))
          .link_1_25('Full')
        endif
        .w_PPFLCCOS = 'N'
        .w_PPCENCOS = IIF(.w_PPFLCCOS $ 'E-P', .w_PPCENCOS, SPACE(15))
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_PPCENCOS))
          .link_1_27('Full')
        endif
          .DoRTCalc(17,20,.f.)
        .w_COLPIA = .w_COLCON
        .w_SFOPIA = .w_SFOCON
      .oPgFrm.Page1.oPag.SUGGE.Calculate('      1234567890     ',.w_COLSUG,.w_SFOSUG)
      .oPgFrm.Page1.oPag.oObj_1_50.Calculate('      1234567890     ',.w_COLCON,.w_SFOCON)
      .oPgFrm.Page1.oPag.oObj_1_55.Calculate('      1234567890     ',.w_PDACOLMIS,.w_PDASFOMIS)
      .oPgFrm.Page1.oPag.oObj_1_62.Calculate('      1234567890     ',.w_SFOMCO,.w_SFOMCO)
          .DoRTCalc(23,30,.f.)
        .w_PPSALCOM = 'P'
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_PPCODCOM))
          .link_1_66('Full')
        endif
          .DoRTCalc(33,33,.f.)
        .w_PPNOSCSM = IIF(.w_PP_CICLI='S',.w_PPNOSCSM,' ')
    endwith
    this.DoRTCalc(35,40,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_44.enabled = this.oPgFrm.Page1.oPag.oBtn_1_44.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_45.enabled = this.oPgFrm.Page1.oPag.oBtn_1_45.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_51.enabled = this.oPgFrm.Page1.oPag.oBtn_1_51.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_52.enabled = this.oPgFrm.Page1.oPag.oBtn_1_52.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_56.enabled = this.oPgFrm.Page1.oPag.oBtn_1_56.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_63.enabled = this.oPgFrm.Page1.oPag.oBtn_1_63.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_74.enabled = this.oPgFrm.Page1.oPag.oBtn_1_74.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_75.enabled = this.oPgFrm.Page1.oPag.oBtn_1_75.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .DoRTCalc(1,12,.t.)
        if .o_PP_CICLI<>.w_PP_CICLI
            .w_PP___DTF = 0
        endif
        .DoRTCalc(14,15,.t.)
        if .o_PPFLCCOS<>.w_PPFLCCOS
            .w_PPCENCOS = IIF(.w_PPFLCCOS $ 'E-P', .w_PPCENCOS, SPACE(15))
          .link_1_27('Full')
        endif
        .DoRTCalc(17,20,.t.)
            .w_COLPIA = .w_COLCON
            .w_SFOPIA = .w_SFOCON
        .oPgFrm.Page1.oPag.SUGGE.Calculate('      1234567890     ',.w_COLSUG,.w_SFOSUG)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate('      1234567890     ',.w_COLCON,.w_SFOCON)
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate('      1234567890     ',.w_PDACOLMIS,.w_PDASFOMIS)
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate('      1234567890     ',.w_SFOMCO,.w_SFOMCO)
        .DoRTCalc(23,33,.t.)
        if .o_PP_CICLI<>.w_PP_CICLI
            .w_PPNOSCSM = IIF(.w_PP_CICLI='S',.w_PPNOSCSM,' ')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(35,40,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .oPgFrm.Page1.oPag.SUGGE.Calculate('      1234567890     ',.w_COLSUG,.w_SFOSUG)
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate('      1234567890     ',.w_COLCON,.w_SFOCON)
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate('      1234567890     ',.w_PDACOLMIS,.w_PDASFOMIS)
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate('      1234567890     ',.w_SFOMCO,.w_SFOMCO)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPP___DTF_1_24.enabled = this.oPgFrm.Page1.oPag.oPP___DTF_1_24.mCond()
    this.oPgFrm.Page1.oPag.oPPCENCOS_1_27.enabled = this.oPgFrm.Page1.oPag.oPPCENCOS_1_27.mCond()
    this.oPgFrm.Page1.oPag.oPPNOSCSM_1_73.enabled = this.oPgFrm.Page1.oPag.oPPNOSCSM_1_73.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPPCODCOM_1_66.visible=!this.oPgFrm.Page1.oPag.oPPCODCOM_1_66.mHide()
    this.oPgFrm.Page1.oPag.oPPNOSCSM_1_73.visible=!this.oPgFrm.Page1.oPag.oPPNOSCSM_1_73.mHide()
    this.oPgFrm.Page1.oPag.oDESCEN_1_88.visible=!this.oPgFrm.Page1.oPag.oDESCEN_1_88.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_89.visible=!this.oPgFrm.Page1.oPag.oStr_1_89.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_92.visible=!this.oPgFrm.Page1.oPag.oStr_1_92.mHide()
    this.oPgFrm.Page1.oPag.oDESCOM_1_93.visible=!this.oPgFrm.Page1.oPag.oDESCOM_1_93.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_1.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_2.Event(cEvent)
      .oPgFrm.Page1.oPag.SUGGE.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_62.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PPMAGPRO
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPMAGPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_PPMAGPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_PPMAGPRO))
          select MGCODMAG,MGDESMAG,MGDISMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPMAGPRO)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPMAGPRO) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oPPMAGPRO_1_20'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDISMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPMAGPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDISMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_PPMAGPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_PPMAGPRO)
            select MGCODMAG,MGDESMAG,MGDISMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPMAGPRO = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DISMAG = NVL(_Link_.MGDISMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PPMAGPRO = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DISMAG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_PPMAGPRO) OR .w_DISMAG='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente o non nettificabile")
        endif
        this.w_PPMAGPRO = space(5)
        this.w_DESMAG = space(30)
        this.w_DISMAG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPMAGPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCALSTA
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_CALE_IDX,3]
    i_lTable = "TAB_CALE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2], .t., this.TAB_CALE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCALSTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATL',True,'TAB_CALE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_PPCALSTA)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_PPCALSTA))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCALSTA)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCALSTA) and !this.bDontReportError
            deferred_cp_zoom('TAB_CALE','*','TCCODICE',cp_AbsName(oSource.parent,'oPPCALSTA_1_25'),i_cWhere,'GSAR_ATL',"Calendari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCALSTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_PPCALSTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_PPCALSTA)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCALSTA = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCAL = NVL(_Link_.TCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PPCALSTA = space(5)
      endif
      this.w_DESCAL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_CALE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_CALE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCALSTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCENCOS
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCENCOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_PPCENCOS)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_PPCENCOS))
          select CC_CONTO,CCDESPIA,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCENCOS)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCENCOS) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oPPCENCOS_1_27'),i_cWhere,'',"Centri di costo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCENCOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_PPCENCOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_PPCENCOS)
            select CC_CONTO,CCDESPIA,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCENCOS = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCEN = NVL(_Link_.CCDESPIA,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PPCENCOS = space(15)
      endif
      this.w_DESCEN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Centro di Costo obsoleto!",.F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PPCENCOS = space(15)
        this.w_DESCEN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCENCOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PPCODCOM
  func Link_1_66(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PPCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_PPCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_PPCODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PPCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PPCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oPPCODCOM_1_66'),i_cWhere,'',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PPCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_PPCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_PPCODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PPCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PPCODCOM = space(15)
      endif
      this.w_DESCOM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PPCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPPNUMGIO_1_5.value==this.w_PPNUMGIO)
      this.oPgFrm.Page1.oPag.oPPNUMGIO_1_5.value=this.w_PPNUMGIO
    endif
    if not(this.oPgFrm.Page1.oPag.oPPNUMSET_1_6.value==this.w_PPNUMSET)
      this.oPgFrm.Page1.oPag.oPPNUMSET_1_6.value=this.w_PPNUMSET
    endif
    if not(this.oPgFrm.Page1.oPag.oPPNUMMES_1_7.value==this.w_PPNUMMES)
      this.oPgFrm.Page1.oPag.oPPNUMMES_1_7.value=this.w_PPNUMMES
    endif
    if not(this.oPgFrm.Page1.oPag.oPPNUMTRI_1_8.value==this.w_PPNUMTRI)
      this.oPgFrm.Page1.oPag.oPPNUMTRI_1_8.value=this.w_PPNUMTRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPPCOLMPS_1_17.value==this.w_PPCOLMPS)
      this.oPgFrm.Page1.oPag.oPPCOLMPS_1_17.value=this.w_PPCOLMPS
    endif
    if not(this.oPgFrm.Page1.oPag.oPPFONMPS_1_18.value==this.w_PPFONMPS)
      this.oPgFrm.Page1.oPag.oPPFONMPS_1_18.value=this.w_PPFONMPS
    endif
    if not(this.oPgFrm.Page1.oPag.oPPMAGPRO_1_20.value==this.w_PPMAGPRO)
      this.oPgFrm.Page1.oPag.oPPMAGPRO_1_20.value=this.w_PPMAGPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_21.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_21.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oPP_CICLI_1_23.RadioValue()==this.w_PP_CICLI)
      this.oPgFrm.Page1.oPag.oPP_CICLI_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPP___DTF_1_24.value==this.w_PP___DTF)
      this.oPgFrm.Page1.oPag.oPP___DTF_1_24.value=this.w_PP___DTF
    endif
    if not(this.oPgFrm.Page1.oPag.oPPCALSTA_1_25.value==this.w_PPCALSTA)
      this.oPgFrm.Page1.oPag.oPPCALSTA_1_25.value=this.w_PPCALSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oPPFLCCOS_1_26.RadioValue()==this.w_PPFLCCOS)
      this.oPgFrm.Page1.oPag.oPPFLCCOS_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPPCENCOS_1_27.value==this.w_PPCENCOS)
      this.oPgFrm.Page1.oPag.oPPCENCOS_1_27.value=this.w_PPCENCOS
    endif
    if not(this.oPgFrm.Page1.oPag.oBOX_1_37.value==this.w_BOX)
      this.oPgFrm.Page1.oPag.oBOX_1_37.value=this.w_BOX
    endif
    if not(this.oPgFrm.Page1.oPag.oBOX_1_38.value==this.w_BOX)
      this.oPgFrm.Page1.oPag.oBOX_1_38.value=this.w_BOX
    endif
    if not(this.oPgFrm.Page1.oPag.oBOX_1_39.value==this.w_BOX)
      this.oPgFrm.Page1.oPag.oBOX_1_39.value=this.w_BOX
    endif
    if not(this.oPgFrm.Page1.oPag.oBOX_1_40.value==this.w_BOX)
      this.oPgFrm.Page1.oPag.oBOX_1_40.value=this.w_BOX
    endif
    if not(this.oPgFrm.Page1.oPag.oPPFLAROB_1_64.RadioValue()==this.w_PPFLAROB)
      this.oPgFrm.Page1.oPag.oPPFLAROB_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPPSALCOM_1_65.RadioValue()==this.w_PPSALCOM)
      this.oPgFrm.Page1.oPag.oPPSALCOM_1_65.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPPCODCOM_1_66.value==this.w_PPCODCOM)
      this.oPgFrm.Page1.oPag.oPPCODCOM_1_66.value=this.w_PPCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oPPBLOMRP_1_67.RadioValue()==this.w_PPBLOMRP)
      this.oPgFrm.Page1.oPag.oPPBLOMRP_1_67.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPPNOSCSM_1_73.RadioValue()==this.w_PPNOSCSM)
      this.oPgFrm.Page1.oPag.oPPNOSCSM_1_73.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAL_1_76.value==this.w_DESCAL)
      this.oPgFrm.Page1.oPag.oDESCAL_1_76.value=this.w_DESCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCRIFOR_1_78.RadioValue()==this.w_CRIFOR)
      this.oPgFrm.Page1.oPag.oCRIFOR_1_78.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCRIELA_1_82.RadioValue()==this.w_CRIELA)
      this.oPgFrm.Page1.oPag.oCRIELA_1_82.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCEN_1_88.value==this.w_DESCEN)
      this.oPgFrm.Page1.oPag.oDESCEN_1_88.value=this.w_DESCEN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_93.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_93.value=this.w_DESCOM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_PPNUMGIO)) or not(.w_PPNUMGIO>0))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPNUMGIO_1_5.SetFocus()
            i_bnoObbl = !empty(.w_PPNUMGIO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un valore maggiore di zero")
          case   ((empty(.w_PPNUMSET)) or not(.w_PPNUMSET>0))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPNUMSET_1_6.SetFocus()
            i_bnoObbl = !empty(.w_PPNUMSET)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un valore maggiore di zero")
          case   ((empty(.w_PPNUMMES)) or not(.w_PPNUMMES>0))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPNUMMES_1_7.SetFocus()
            i_bnoObbl = !empty(.w_PPNUMMES)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un valore maggiore di zero")
          case   ((empty(.w_PPNUMTRI)) or not(.w_PPNUMTRI>0))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPNUMTRI_1_8.SetFocus()
            i_bnoObbl = !empty(.w_PPNUMTRI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un valore maggiore di zero")
          case   (empty(.w_PPCOLMPS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPCOLMPS_1_17.SetFocus()
            i_bnoObbl = !empty(.w_PPCOLMPS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PPFONMPS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPFONMPS_1_18.SetFocus()
            i_bnoObbl = !empty(.w_PPFONMPS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_PPMAGPRO) OR .w_DISMAG='S')  and not(empty(.w_PPMAGPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPMAGPRO_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente o non nettificabile")
          case   not(.w_PP_CICLI<>'S' OR .w_PP___DTF>0)  and (.w_PP_CICLI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPP___DTF_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Indicare il numero di giorni di scaduto")
          case   not(CHKDTOBS(.w_DATOBSO,.w_OBTEST,"Centro di Costo obsoleto!",.F.))  and (g_PERCCR='S' and .w_PPFLCCOS $ 'E-P')  and not(empty(.w_PPCENCOS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPPCENCOS_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PP_CICLI = this.w_PP_CICLI
    this.o_PPFLCCOS = this.w_PPFLCCOS
    return

enddefine

* --- Define pages as container
define class tgsac_kpaPag1 as StdContainer
  Width  = 551
  height = 626
  stdWidth  = 551
  stdheight = 626
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_1 as cp_runprogram with uid="TXHSZXCKUT",left=602, top=3, width=239,height=22,;
    caption='GSAC_BPA(LOAD)',;
   bGlobalFont=.t.,;
    prg='GSAC_BPA("LOAD")',;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 53264601


  add object oObj_1_2 as cp_runprogram with uid="WJCSVMRUVM",left=602, top=24, width=241,height=22,;
    caption='GSAC_BPA(SALVA)',;
   bGlobalFont=.t.,;
    prg='GSAC_BPA("SALVA")',;
    cEvent = "Update start",;
    nPag=1;
    , HelpContextID = 167942617

  add object oPPNUMGIO_1_5 as StdField with uid="LNOCRFNDCW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PPNUMGIO", cQueryName = "PPNUMGIO",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un valore maggiore di zero",;
    ToolTipText = "Numero minimo di periodi giornalieri",;
    HelpContextID = 54513221,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=231, Top=35, cSayPict='"999"', cGetPict='"999"'

  func oPPNUMGIO_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PPNUMGIO>0)
    endwith
    return bRes
  endfunc

  add object oPPNUMSET_1_6 as StdField with uid="NWIAEBOZDN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PPNUMSET", cQueryName = "PPNUMSET",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un valore maggiore di zero",;
    ToolTipText = "Numero minimo di periodi settimanali",;
    HelpContextID = 255839818,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=231, Top=55, cSayPict='"999"', cGetPict='"999"'

  func oPPNUMSET_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PPNUMSET>0)
    endwith
    return bRes
  endfunc

  add object oPPNUMMES_1_7 as StdField with uid="XVWFKYKYWF",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PPNUMMES", cQueryName = "PPNUMMES",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un valore maggiore di zero",;
    ToolTipText = "Numero minimo di periodi mensili",;
    HelpContextID = 155176521,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=231, Top=75, cSayPict='"999"', cGetPict='"999"'

  func oPPNUMMES_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PPNUMMES>0)
    endwith
    return bRes
  endfunc

  add object oPPNUMTRI_1_8 as StdField with uid="EMJRAPSTKG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PPNUMTRI", cQueryName = "PPNUMTRI",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un valore maggiore di zero",;
    ToolTipText = "Numero di periodi trimestrali",;
    HelpContextID = 4181567,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=231, Top=95, cSayPict='"999"', cGetPict='"999"'

  func oPPNUMTRI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PPNUMTRI>0)
    endwith
    return bRes
  endfunc

  add object oPPCOLMPS_1_17 as StdField with uid="OZYMHICSEC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PPCOLMPS", cQueryName = "PPCOLMPS",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione colonna per tabella PDA",;
    HelpContextID = 114745783,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=356, Top=35, cSayPict='"@Z 999"', cGetPict='"999"'

  add object oPPFONMPS_1_18 as StdField with uid="RIUNXVRMXE",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PPFONMPS", cQueryName = "PPFONMPS",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione font",;
    HelpContextID = 112636343,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=479, Top=35, cSayPict='"@Z 99"', cGetPict='"99"'

  add object oPPMAGPRO_1_20 as StdField with uid="RDCLKRSABG",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PPMAGPRO", cQueryName = "PPMAGPRO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente o non nettificabile",;
    ToolTipText = "Magazzino utilizzato per il sottoscorta",;
    HelpContextID = 70533563,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=238, Top=160, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_PPMAGPRO"

  func oPPMAGPRO_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPMAGPRO_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPMAGPRO_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oPPMAGPRO_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oPPMAGPRO_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_PPMAGPRO
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_21 as StdField with uid="SOPMZKHBPF",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 227012042,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=303, Top=160, InputMask=replicate('X',30)

  add object oPP_CICLI_1_23 as StdCheck with uid="TLLGFTARXJ",rtseq=12,rtrep=.f.,left=238, top=186, caption="No scaduto",;
    ToolTipText = "Se attivo: gli impegni scaduti di articoli con forn.abituale verranno posticipati per non generare ordini con data scaduta",;
    HelpContextID = 17899969,;
    cFormVar="w_PP_CICLI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPP_CICLI_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPP_CICLI_1_23.GetRadio()
    this.Parent.oContained.w_PP_CICLI = this.RadioValue()
    return .t.
  endfunc

  func oPP_CICLI_1_23.SetRadio()
    this.Parent.oContained.w_PP_CICLI=trim(this.Parent.oContained.w_PP_CICLI)
    this.value = ;
      iif(this.Parent.oContained.w_PP_CICLI=='S',1,;
      0)
  endfunc

  add object oPP___DTF_1_24 as StdField with uid="XIBTGSCVWD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PP___DTF", cQueryName = "PP___DTF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Indicare il numero di giorni di scaduto",;
    ToolTipText = "Numero massimo di giorni di scaduto nei documenti da considerare per posticipo ordini",;
    HelpContextID = 23780924,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=492, Top=186

  func oPP___DTF_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PP_CICLI='S')
    endwith
   endif
  endfunc

  func oPP___DTF_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PP_CICLI<>'S' OR .w_PP___DTF>0)
    endwith
    return bRes
  endfunc

  add object oPPCALSTA_1_25 as StdField with uid="NHMLPATPJR",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PPCALSTA", cQueryName = "PPCALSTA",nZero=5,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Calendario di stabilimento",;
    HelpContextID = 253435447,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=238, Top=234, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TAB_CALE", cZoomOnZoom="GSAR_ATL", oKey_1_1="TCCODICE", oKey_1_2="this.w_PPCALSTA"

  func oPPCALSTA_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCALSTA_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCALSTA_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_CALE','*','TCCODICE',cp_AbsName(this.parent,'oPPCALSTA_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATL',"Calendari",'',this.parent.oContained
  endproc
  proc oPPCALSTA_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_PPCALSTA
     i_obj.ecpSave()
  endproc


  add object oPPFLCCOS_1_26 as StdCombo with uid="XACPIJOCCH",rtseq=15,rtrep=.f.,left=182,top=260,width=153,height=22;
    , ToolTipText = "Imposta il criterio di scelta del centro di costo";
    , HelpContextID = 23703991;
    , cFormVar="w_PPFLCCOS",RowSource=""+"Da anagrafica articolo,"+"Da parametri PDA,"+"Articolo/parametri PDA,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPPFLCCOS_1_26.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'P',;
    iif(this.value =3,'E',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oPPFLCCOS_1_26.GetRadio()
    this.Parent.oContained.w_PPFLCCOS = this.RadioValue()
    return .t.
  endfunc

  func oPPFLCCOS_1_26.SetRadio()
    this.Parent.oContained.w_PPFLCCOS=trim(this.Parent.oContained.w_PPFLCCOS)
    this.value = ;
      iif(this.Parent.oContained.w_PPFLCCOS=='A',1,;
      iif(this.Parent.oContained.w_PPFLCCOS=='P',2,;
      iif(this.Parent.oContained.w_PPFLCCOS=='E',3,;
      iif(this.Parent.oContained.w_PPFLCCOS=='N',4,;
      0))))
  endfunc

  add object oPPCENCOS_1_27 as StdField with uid="AEEZGZBQCB",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PPCENCOS", cQueryName = "PPCENCOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice centro di costo proposto nelle proposte di acquisto",;
    HelpContextID = 12640695,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=182, Top=287, cSayPict="p_CEN", cGetPict="p_CEN", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", oKey_1_1="CC_CONTO", oKey_1_2="this.w_PPCENCOS"

  func oPPCENCOS_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERCCR='S' and .w_PPFLCCOS $ 'E-P')
    endwith
   endif
  endfunc

  func oPPCENCOS_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCENCOS_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCENCOS_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oPPCENCOS_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Centri di costo",'',this.parent.oContained
  endproc

  add object oBOX_1_37 as StdMemo with uid="IPGXEMBSYC",rtseq=25,rtrep=.f.,;
    cFormVar = "w_BOX", cQueryName = "BOX",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 149197802,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=417, Top=349, scrollbars=0

  add object oBOX_1_38 as StdMemo with uid="JMPXJDKXUV",rtseq=26,rtrep=.f.,;
    cFormVar = "w_BOX", cQueryName = "BOX",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 149197802,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=417, Top=455, scrollbars=0

  add object oBOX_1_39 as StdMemo with uid="ABJLNIKAJD",rtseq=27,rtrep=.f.,;
    cFormVar = "w_BOX", cQueryName = "BOX",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 149197802,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=417, Top=399, scrollbars=0

  add object oBOX_1_40 as StdMemo with uid="XXTNPHVIQP",rtseq=28,rtrep=.f.,;
    cFormVar = "w_BOX", cQueryName = "BOX",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 149197802,;
   bGlobalFont=.t.,;
    Height=21, Width=109, Left=417, Top=374, scrollbars=0


  add object SUGGE as cp_calclbl with uid="NZSYKJYMIJ",left=417, top=349, width=109,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=' ',;
    nPag=1;
    , HelpContextID = 28418022


  add object oBtn_1_44 as StdButton with uid="IBQYHSMVQN",left=242, top=351, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore del testo per PDA suggerito";
    , HelpContextID = 149378602;
  , bGlobalFont=.t.

    proc oBtn_1_44.Click()
      with this.Parent.oContained
        GSAC_BPA(this.Parent.oContained,"COLSUG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_45 as StdButton with uid="UVFPOGMJRN",left=289, top=351, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore dello sfondo per PDA suggerito";
    , HelpContextID = 149378602;
  , bGlobalFont=.t.

    proc oBtn_1_45.Click()
      with this.Parent.oContained
        GSAC_BPA(this.Parent.oContained,"SFOSUG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_50 as cp_calclbl with uid="XKUSUEFLCD",left=417, top=374, width=109,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=' ',;
    nPag=1;
    , HelpContextID = 28418022


  add object oBtn_1_51 as StdButton with uid="DHMXWKGUUP",left=242, top=376, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore del testo per PDA confermato";
    , HelpContextID = 149378602;
  , bGlobalFont=.t.

    proc oBtn_1_51.Click()
      with this.Parent.oContained
        GSAC_BPA(this.Parent.oContained,"COLCON")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_52 as StdButton with uid="YEBBQWEXAO",left=289, top=376, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore dello sfondo per PDA confermato";
    , HelpContextID = 149378602;
  , bGlobalFont=.t.

    proc oBtn_1_52.Click()
      with this.Parent.oContained
        GSAC_BPA(this.Parent.oContained,"SFOCON")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_55 as cp_calclbl with uid="VPHSIVEYAL",left=417, top=399, width=109,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=' ',;
    nPag=1;
    , HelpContextID = 28418022


  add object oBtn_1_56 as StdButton with uid="GPFXYLMZHW",left=242, top=401, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore del testo per PDA composizione mista";
    , HelpContextID = 149378602;
  , bGlobalFont=.t.

    proc oBtn_1_56.Click()
      with this.Parent.oContained
        GSAC_BPA(this.Parent.oContained,"PDACOLMIS")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_57 as StdButton with uid="ENYHKBKEKW",left=289, top=401, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore dello sfondo per PDA composizione mista";
    , HelpContextID = 149378602;
  , bGlobalFont=.t.

    proc oBtn_1_57.Click()
      with this.Parent.oContained
        GSAC_BPA(this.Parent.oContained,"PDASFOMIS")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_62 as cp_calclbl with uid="VBOCAAMEIP",left=417, top=455, width=109,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=' ',;
    nPag=1;
    , HelpContextID = 28418022


  add object oBtn_1_63 as StdButton with uid="VKEYZOZPYC",left=289, top=455, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il colore dello sfondo per il sovraccarico della capacit";
    , HelpContextID = 149378602;
  , bGlobalFont=.t.

    proc oBtn_1_63.Click()
      with this.Parent.oContained
        GSAC_BPA(this.Parent.oContained,"SFOMCO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPPFLAROB_1_64 as StdCheck with uid="TITLJSWEZG",rtseq=30,rtrep=.f.,left=356, top=493, caption="Pianifica articoli obsoleti",;
    ToolTipText = "Pianifica proposte di acquisto per articoli obsoleti",;
    HelpContextID = 42578376,;
    cFormVar="w_PPFLAROB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPPFLAROB_1_64.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPPFLAROB_1_64.GetRadio()
    this.Parent.oContained.w_PPFLAROB = this.RadioValue()
    return .t.
  endfunc

  func oPPFLAROB_1_64.SetRadio()
    this.Parent.oContained.w_PPFLAROB=trim(this.Parent.oContained.w_PPFLAROB)
    this.value = ;
      iif(this.Parent.oContained.w_PPFLAROB=='S',1,;
      0)
  endfunc


  add object oPPSALCOM_1_65 as StdCombo with uid="KZNOUGWCMS",rtseq=31,rtrep=.f.,left=174,top=518,width=167,height=21;
    , ToolTipText = "Consente di scegliere la modalitą di pianificazione per gli articoli gestiti a commessa";
    , HelpContextID = 14934461;
    , cFormVar="w_PPSALCOM",RowSource=""+"Pegging secondo livello,"+"Saldi commessa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPPSALCOM_1_65.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oPPSALCOM_1_65.GetRadio()
    this.Parent.oContained.w_PPSALCOM = this.RadioValue()
    return .t.
  endfunc

  func oPPSALCOM_1_65.SetRadio()
    this.Parent.oContained.w_PPSALCOM=trim(this.Parent.oContained.w_PPSALCOM)
    this.value = ;
      iif(this.Parent.oContained.w_PPSALCOM=='P',1,;
      iif(this.Parent.oContained.w_PPSALCOM=='S',2,;
      0))
  endfunc

  add object oPPCODCOM_1_66 as StdField with uid="IFRCPPJABI",rtseq=32,rtrep=.f.,;
    cFormVar = "w_PPCODCOM", cQueryName = "PPCODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa di default per saldi liberi",;
    HelpContextID = 22471101,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=174, Top=546, cSayPict="p_CEN", cGetPict="p_CEN", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", oKey_1_1="CNCODCAN", oKey_1_2="this.w_PPCODCOM"

  func oPPCODCOM_1_66.mHide()
    with this.Parent.oContained
      return (g_PROD='S')
    endwith
  endfunc

  func oPPCODCOM_1_66.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_66('Part',this)
    endwith
    return bRes
  endfunc

  proc oPPCODCOM_1_66.ecpDrop(oSource)
    this.Parent.oContained.link_1_66('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPPCODCOM_1_66.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oPPCODCOM_1_66'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Commesse",'',this.parent.oContained
  endproc

  add object oPPBLOMRP_1_67 as StdCheck with uid="FCRQSDKZEM",rtseq=33,rtrep=.f.,left=160, top=493, caption="Blocca PDA",;
    ToolTipText = " Se attivo: blocca temporaneamente la generazione PDA",;
    HelpContextID = 111800762,;
    cFormVar="w_PPBLOMRP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPPBLOMRP_1_67.RadioValue()
    return(iif(this.value =1,"S",;
    "N"))
  endfunc
  func oPPBLOMRP_1_67.GetRadio()
    this.Parent.oContained.w_PPBLOMRP = this.RadioValue()
    return .t.
  endfunc

  func oPPBLOMRP_1_67.SetRadio()
    this.Parent.oContained.w_PPBLOMRP=trim(this.Parent.oContained.w_PPBLOMRP)
    this.value = ;
      iif(this.Parent.oContained.w_PPBLOMRP=="S",1,;
      0)
  endfunc

  add object oPPNOSCSM_1_73 as StdCheck with uid="NKRRQMSBEI",rtseq=34,rtrep=.f.,left=238, top=208, caption="No scaduto scorta minima",;
    ToolTipText = "Se attivo: il fabbisogno dovuto a scorta minima viene posticipato per non generare ordini con data scaduta",;
    HelpContextID = 6697405,;
    cFormVar="w_PPNOSCSM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPPNOSCSM_1_73.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPPNOSCSM_1_73.GetRadio()
    this.Parent.oContained.w_PPNOSCSM = this.RadioValue()
    return .t.
  endfunc

  func oPPNOSCSM_1_73.SetRadio()
    this.Parent.oContained.w_PPNOSCSM=trim(this.Parent.oContained.w_PPNOSCSM)
    this.value = ;
      iif(this.Parent.oContained.w_PPNOSCSM=='S',1,;
      0)
  endfunc

  func oPPNOSCSM_1_73.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PP_CICLI='S')
    endwith
   endif
  endfunc

  func oPPNOSCSM_1_73.mHide()
    with this.Parent.oContained
      return (.w_PP_CICLI<>'S')
    endwith
  endfunc


  add object oBtn_1_74 as StdButton with uid="QYACFJOWLI",left=433, top=574, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte impostate";
    , HelpContextID = 149550874;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_74.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_75 as StdButton with uid="OVEULLOQUH",left=483, top=574, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare le scelte";
    , HelpContextID = 142262202;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_75.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCAL_1_76 as StdField with uid="VQWOQOUELG",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESCAL", cQueryName = "DESCAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 143781322,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=303, Top=234, InputMask=replicate('X',40)


  add object oCRIFOR_1_78 as StdCombo with uid="BKBXNLQDDK",rtseq=36,rtrep=.f.,left=356,top=83,width=162,height=21;
    , ToolTipText = "Criterio di scelta di default del miglior fornitore in base ai contratti in esse";
    , HelpContextID = 28279002;
    , cFormVar="w_CRIFOR",RowSource=""+"Tempo,"+"Prezzo,"+"Affidabilitą,"+"Prioritą", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCRIFOR_1_78.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'Z',;
    iif(this.value =3,'A',;
    iif(this.value =4,'I',;
    space(1))))))
  endfunc
  func oCRIFOR_1_78.GetRadio()
    this.Parent.oContained.w_CRIFOR = this.RadioValue()
    return .t.
  endfunc

  func oCRIFOR_1_78.SetRadio()
    this.Parent.oContained.w_CRIFOR=trim(this.Parent.oContained.w_CRIFOR)
    this.value = ;
      iif(this.Parent.oContained.w_CRIFOR=='T',1,;
      iif(this.Parent.oContained.w_CRIFOR=='Z',2,;
      iif(this.Parent.oContained.w_CRIFOR=='A',3,;
      iif(this.Parent.oContained.w_CRIFOR=='I',4,;
      0))))
  endfunc


  add object oCRIELA_1_82 as StdCombo with uid="KQYUEYOITH",rtseq=37,rtrep=.f.,left=356,top=132,width=162,height=21;
    , ToolTipText = "Criterio di scelta di default del miglior fornitore in base ai contratti in esse";
    , HelpContextID = 48267482;
    , cFormVar="w_CRIELA",RowSource=""+"Aggregata,"+"Per Magazzino,"+"Per gruppi di magazzini", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCRIELA_1_82.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'M',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oCRIELA_1_82.GetRadio()
    this.Parent.oContained.w_CRIELA = this.RadioValue()
    return .t.
  endfunc

  func oCRIELA_1_82.SetRadio()
    this.Parent.oContained.w_CRIELA=trim(this.Parent.oContained.w_CRIELA)
    this.value = ;
      iif(this.Parent.oContained.w_CRIELA=='A',1,;
      iif(this.Parent.oContained.w_CRIELA=='M',2,;
      iif(this.Parent.oContained.w_CRIELA=='G',3,;
      0)))
  endfunc

  add object oDESCEN_1_88 as StdField with uid="LIVHFQLEYG",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESCEN", cQueryName = "DESCEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 106032586,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=303, Top=287, InputMask=replicate('X',40)

  func oDESCEN_1_88.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oDESCOM_1_93 as StdField with uid="XISLSXZGHT",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 112324042,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=295, Top=546, InputMask=replicate('X',40)

  func oDESCOM_1_93.mHide()
    with this.Parent.oContained
      return (g_PROD='S')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="CWLWOJCEIV",Visible=.t., Left=22, Top=36,;
    Alignment=1, Width=202, Height=15,;
    Caption="Numero minimo periodi giornalieri:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="VKIEIJTAWI",Visible=.t., Left=11, Top=56,;
    Alignment=1, Width=213, Height=15,;
    Caption="Numero minimo periodi settimanali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="RHROCCKOIY",Visible=.t., Left=22, Top=76,;
    Alignment=1, Width=202, Height=15,;
    Caption="Numero minimo periodi mensili:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="DZQIELETCS",Visible=.t., Left=22, Top=96,;
    Alignment=1, Width=202, Height=15,;
    Caption="Numero periodi trimestrali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="WZANXJWXGR",Visible=.t., Left=30, Top=6,;
    Alignment=0, Width=255, Height=18,;
    Caption="Definizione time buckets"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="XVGDWKRNYU",Visible=.t., Left=272, Top=36,;
    Alignment=1, Width=80, Height=15,;
    Caption="Dim. colonna:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="WZMIRIBYUI",Visible=.t., Left=405, Top=36,;
    Alignment=1, Width=70, Height=15,;
    Caption="Dim. font:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="APJYDEPHGL",Visible=.t., Left=286, Top=6,;
    Alignment=0, Width=255, Height=18,;
    Caption="Dimensioni e colori piano PDA"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="LGJXHJHMRF",Visible=.t., Left=343, Top=349,;
    Alignment=1, Width=68, Height=15,;
    Caption="Esempio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="NZXEUNMETO",Visible=.t., Left=237, Top=333,;
    Alignment=0, Width=32, Height=15,;
    Caption="Testo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_47 as StdString with uid="KKZZCKYXCJ",Visible=.t., Left=279, Top=333,;
    Alignment=0, Width=71, Height=15,;
    Caption="Sfondo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_48 as StdString with uid="WDVWWVDEAN",Visible=.t., Left=29, Top=352,;
    Alignment=1, Width=202, Height=15,;
    Caption="PDA suggerita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="TGFWRBXNYS",Visible=.t., Left=343, Top=374,;
    Alignment=1, Width=68, Height=15,;
    Caption="Esempio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="KGSADKHGAN",Visible=.t., Left=29, Top=377,;
    Alignment=1, Width=202, Height=15,;
    Caption="PDA confermata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="OWYLODBZFI",Visible=.t., Left=343, Top=399,;
    Alignment=1, Width=68, Height=15,;
    Caption="Esempio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="GNYFGDAKMG",Visible=.t., Left=29, Top=401,;
    Alignment=1, Width=202, Height=15,;
    Caption="PDA composizione mista:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="EGDZWEMMTK",Visible=.t., Left=31, Top=314,;
    Alignment=0, Width=186, Height=15,;
    Caption="Definizione colori griglia PDA"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="SKVJZWGXEO",Visible=.t., Left=343, Top=455,;
    Alignment=1, Width=68, Height=15,;
    Caption="Esempio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="YWMGQNOSTP",Visible=.t., Left=29, Top=455,;
    Alignment=1, Width=202, Height=15,;
    Caption="Sfondo per ritardo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="OCMDCRAHQP",Visible=.t., Left=31, Top=428,;
    Alignment=0, Width=218, Height=15,;
    Caption="Parametri verifica temporale"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="LSAZUYSROF",Visible=.t., Left=29, Top=162,;
    Alignment=1, Width=202, Height=15,;
    Caption="Magazzino principale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="EGIFLLGLYH",Visible=.t., Left=388, Top=188,;
    Alignment=1, Width=104, Height=18,;
    Caption="Fino a n.giorni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="YMTGNEIBBP",Visible=.t., Left=135, Top=236,;
    Alignment=1, Width=96, Height=18,;
    Caption="Calendario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_79 as StdString with uid="PMBXTXAVVN",Visible=.t., Left=297, Top=85,;
    Alignment=1, Width=55, Height=15,;
    Caption="Default:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="NBEAWDCSUP",Visible=.t., Left=284, Top=60,;
    Alignment=0, Width=246, Height=15,;
    Caption="Criterio di scelta del fornitore"  ;
  , bGlobalFont=.t.

  add object oStr_1_83 as StdString with uid="VFGVCCTVJM",Visible=.t., Left=284, Top=111,;
    Alignment=0, Width=255, Height=15,;
    Caption="Criterio di elaborazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_85 as StdString with uid="RSCRYMENRG",Visible=.t., Left=297, Top=134,;
    Alignment=1, Width=55, Height=15,;
    Caption="Default:"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="PGGYYWPBGZ",Visible=.t., Left=39, Top=495,;
    Alignment=1, Width=117, Height=18,;
    Caption="Check blocco PDA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="ZDUMPECZNB",Visible=.t., Left=8, Top=289,;
    Alignment=1, Width=170, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_1_89.mHide()
    with this.Parent.oContained
      return (g_PERCCR<>'S')
    endwith
  endfunc

  add object oStr_1_90 as StdString with uid="MEABANIDSE",Visible=.t., Left=49, Top=264,;
    Alignment=1, Width=129, Height=18,;
    Caption="Proponi centro di costo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_91 as StdString with uid="RISQQCETLV",Visible=.t., Left=32, Top=520,;
    Alignment=1, Width=137, Height=18,;
    Caption="Gestisci commessa con:"  ;
  , bGlobalFont=.t.

  add object oStr_1_92 as StdString with uid="MGCOXOFDWE",Visible=.t., Left=-1, Top=548,;
    Alignment=1, Width=170, Height=18,;
    Caption="Commessa saldi liberi:"  ;
  , bGlobalFont=.t.

  func oStr_1_92.mHide()
    with this.Parent.oContained
      return (g_PROD='S')
    endwith
  endfunc

  add object oBox_1_14 as StdBox with uid="OMCTQHDULU",left=27, top=25, width=253,height=1

  add object oBox_1_28 as StdBox with uid="OTZTVLEAGP",left=283, top=25, width=244,height=2

  add object oBox_1_60 as StdBox with uid="BTSHSJEOXI",left=28, top=329, width=500,height=2

  add object oBox_1_70 as StdBox with uid="ZGRXAAQAJO",left=28, top=445, width=500,height=2

  add object oBox_1_81 as StdBox with uid="HPZZEVMUYY",left=281, top=76, width=245,height=1

  add object oBox_1_84 as StdBox with uid="WDDDFKJJFK",left=281, top=125, width=245,height=1

  add object oBox_1_94 as StdBox with uid="VAECYKEEUQ",left=28, top=486, width=500,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsac_kpa','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
