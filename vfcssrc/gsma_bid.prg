* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bid                                                        *
*              Imposta valore costo standard                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_23]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-18                                                      *
* Last revis.: 2000-04-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bid",oParentObject)
return(i_retval)

define class tgsma_bid as StdBatch
  * --- Local variables
  * --- WorkFile variables
  PAR_RIOR_idx=0
  PAR_RIMA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch relativo all'anagrafica GSMA_AID
    * --- Variabili della maschera (GSMA_AID)
    * --- Lettura costo standard
    * --- Read from PAR_RIMA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_RIMA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIMA_idx,2],.t.,this.PAR_RIMA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PRCOSSTA"+;
        " from "+i_cTable+" PAR_RIMA where ";
            +"PRCODART = "+cp_ToStrODBC(this.oParentObject.w_DICODICE);
            +" and PRCODMAG = "+cp_ToStrODBC(this.oParentObject.w_DICODMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PRCOSSTA;
        from (i_cTable) where;
            PRCODART = this.oParentObject.w_DICODICE;
            and PRCODMAG = this.oParentObject.w_DICODMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_DICOSSTA = NVL(cp_ToDate(_read_.PRCOSSTA),cp_NullValue(_read_.PRCOSSTA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.oParentObject.w_DICOSSTA=0
      * --- Read from PAR_RIOR
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PRCOSSTA"+;
          " from "+i_cTable+" PAR_RIOR where ";
              +"PRCODART = "+cp_ToStrODBC(this.oParentObject.w_DICODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PRCOSSTA;
          from (i_cTable) where;
              PRCODART = this.oParentObject.w_DICODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_DICOSSTA = NVL(cp_ToDate(_read_.PRCOSSTA),cp_NullValue(_read_.PRCOSSTA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_RIOR'
    this.cWorkTables[2]='PAR_RIMA'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
