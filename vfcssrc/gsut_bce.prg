* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bce                                                        *
*              Controllo classe predefinita                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101][VRS_12]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-16                                                      *
* Last revis.: 2005-05-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bce",oParentObject,m.pTipo)
return(i_retval)

define class tgsut_bce as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_PADRE = .NULL.
  w_CONTA = 0
  * --- WorkFile variables
  EXT_ENS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Check Predefinito al salvataggio delle Tipologie Allegati
    do case
      case this.pTipo = "S"
        this.w_CONTA = 0
        this.w_PADRE = This.oParentObject
        this.oParentObject.w_TrsErr = .F.
        this.w_PADRE.MarkPos()     
        this.w_PADRE.FirstRow()     
        do while Not this.w_PADRE.Eof_Trs()
          this.w_PADRE.SetRow()     
          if this.w_PADRE.FullRow() And this.oParentObject.w_TAFLDEFA="S"
            this.w_CONTA = this.w_CONTA + 1
          endif
          this.w_PADRE.NextRow()     
        enddo
        this.w_PADRE.RePos()     
        if this.w_CONTA >1
          this.oParentObject.w_TrsErr = .T.
        endif
      case this.pTipo = "U"
        * --- Ho attivato il check univoca. Devo controllare che non ci siano pi� di un allegato 
        *     con la classe relativa per lo stesso record
        if this.oParentObject.w_TAFLUNIC = "S"
          * --- Select from query\GSUT_BCE
          do vq_exec with 'query\GSUT_BCE',this,'_Curs_query_GSUT_BCE','',.f.,.t.
          if used('_Curs_query_GSUT_BCE')
            select _Curs_query_GSUT_BCE
            locate for 1=1
            do while not(eof())
            Ah_ErrorMsg("Sono gi� presenti pi� allegati allo stesso record per la tipologia %1 e classe %2","stop","", Alltrim(this.oParentObject.w_TACODICE), Alltrim(this.oParentObject.w_TACODCLA))
            this.oParentObject.w_TAFLUNIC = " "
            Exit
              select _Curs_query_GSUT_BCE
              continue
            enddo
            use
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='EXT_ENS'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_query_GSUT_BCE')
      use in _Curs_query_GSUT_BCE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
