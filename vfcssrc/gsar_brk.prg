* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_brk                                                        *
*              RECUPERO PROGR CLI/FOR                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-01-03                                                      *
* Last revis.: 2011-01-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pANTIPCON,pANCODICE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_brk",oParentObject,m.pANTIPCON,m.pANCODICE)
return(i_retval)

define class tgsar_brk as StdBatch
  * --- Local variables
  pANTIPCON = space(1)
  pANCODICE = space(15)
  w_SER = space(5)
  w_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  w_CHIAVE = space(100)
  w_OK_WARN = 0
  w_PROGN = space(15)
  * --- WorkFile variables
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per recupero Buchi numerazione Prograssivo nei documenti da GSVE_BMK
    *     Solo nel caso cancello il documento che contiene l'ultimo progressivo Utile
    if g_CFNUME="S"
      do case
        case this.pANTIPCON="C"
          this.w_SER = "PRNUCL"
        case this.pANTIPCON="F"
          this.w_SER = "PRNUFO"
      endcase
      if not empty(this.w_SER)
        this.w_ANTIPCON = this.pANTIPCON
        this.w_ANCODICE = this.pANCODICE
        i_Conn=i_TableProp[this.CONTI_IDX, 3]
        cp_AskTableProg(this, i_Conn, this.w_SER, "i_codazi,w_ANCODICE")
        * --- Dopo aver ricalcolato il PROGRESSIVO, se il quello che sto cancellando �
        *     l'ultimo progressivo calcolato, riporto indietro anche il progressivo
        if VAL(this.pANCODICE)>0 AND VAL(this.w_ANCODICE)>0 AND VAL(this.pANCODICE)=VAL(this.w_ANCODICE)-1
          * --- cerco il valore massimo (numerico)
          this.w_PROGN = 0
          * --- Select from CONTI
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ANCODICE  from "+i_cTable+" CONTI ";
                +" where ANTIPCON="+cp_ToStrODBC(this.pANTIPCON)+" AND   ANCODICE>='0' AND   ANCODICE<='999999999999999'";
                +" order by ANCODICE DESC";
                 ,"_Curs_CONTI")
          else
            select ANCODICE from (i_cTable);
             where ANTIPCON=this.pANTIPCON AND   ANCODICE>="0" AND   ANCODICE<="999999999999999";
             order by ANCODICE DESC;
              into cursor _Curs_CONTI
          endif
          if used('_Curs_CONTI')
            select _Curs_CONTI
            locate for 1=1
            do while not(eof())
            this.w_PROGN = NVL(VAL(_Curs_CONTI.ANCODICE), 0)
            if this.w_PROGN>0
              * --- ESCO
              GO BOTTOM
            endif
              select _Curs_CONTI
              continue
            enddo
            use
          endif
          * --- Aggiorno il progressivo all'ultimo documento inserito
          this.w_CHIAVE = "prog\"+ this.w_SER+"\"+ cp_ToStrODBC(alltrim(i_CODAZI))
          this.w_OK_WARN = cp_sqlexec(i_Conn,"UPDATE CPWARN SET AUTONUM="+cp_ToStrODBC(this.w_PROGN)+" WHERE TABLECODE="+cp_ToStrODBC(this.w_CHIAVE))
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pANTIPCON,pANCODICE)
    this.pANTIPCON=pANTIPCON
    this.pANCODICE=pANCODICE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pANTIPCON,pANCODICE"
endproc
