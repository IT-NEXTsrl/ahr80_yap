* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kci                                                        *
*              Giroconto IVA ventilata                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_19]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-21                                                      *
* Last revis.: 2014-05-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kci",oParentObject))

* --- Class definition
define class tgscg_kci as StdForm
  Top    = 14
  Left   = 24

  * --- Standard Properties
  Width  = 572
  Height = 247+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-05-26"
  HelpContextID=99231081
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=52

  * --- Constant Properties
  _IDX = 0
  CAU_CONT_IDX = 0
  CONTI_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gscg_kci"
  cComment = "Giroconto IVA ventilata"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_TIPCON = space(1)
  w_GIRIVA = space(2)
  w_TIPDEN = space(1)
  w_NEWIVA = space(1)
  o_NEWIVA = space(1)
  w_TIPREG = space(1)
  w_FLANAL = space(1)
  w_TOTCOV = 0
  o_TOTCOV = 0
  w_ACODCAU = space(5)
  w_PNDATREG = ctod('  /  /  ')
  w_GENVENT = space(1)
  w_ACODVEN = space(15)
  w_CODCAU = space(5)
  w_ATIPVEN = space(1)
  w_PARVEN = space(1)
  w_TIPVEN = space(1)
  w_CODVEN = space(15)
  w_ACODIVA = space(15)
  w_CODIVA = space(15)
  w_PNDESSUP = space(45)
  w_DESCAU = space(35)
  w_FLSALF = space(1)
  w_FLSALI = space(1)
  w_DESPIA = space(40)
  w_DESPIV = space(40)
  w_TIPIVA = space(1)
  w_CONFE2 = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_FLPART = space(1)
  w_PARIVA = space(1)
  w_AGGDOC = space(1)
  w_CAUGIR = space(5)
  w_CONIVA = space(15)
  w_CREDRIP = space(15)
  w_CREDSPE = space(15)
  w_ACONIND = space(15)
  w_INTDOV = space(15)
  w_CONIND = space(15)
  w_DESRIP = space(35)
  w_DESPE = space(35)
  w_DESDOV = space(35)
  w_GENAUTO = space(1)
  w_DESIVA = space(35)
  w_ACREDSPE = space(15)
  w_AINTDOV = space(15)
  w_ACREDRIP = space(15)
  w_ACONIVA = space(15)
  w_ACAUGIR = space(5)
  w_DESCAU1 = space(35)
  w_DESIND = space(35)
  w_STALIG = ctod('  /  /  ')
  w_DATBLO = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kciPag1","gscg_kci",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Giroconto IVA ventilata")
      .Pages(2).addobject("oPag","tgscg_kciPag2","gscg_kci",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Giroconto conti IVA")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPNDATREG_1_10
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='AZIENDA'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_kci
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_TIPCON=space(1)
      .w_GIRIVA=space(2)
      .w_TIPDEN=space(1)
      .w_NEWIVA=space(1)
      .w_TIPREG=space(1)
      .w_FLANAL=space(1)
      .w_TOTCOV=0
      .w_ACODCAU=space(5)
      .w_PNDATREG=ctod("  /  /  ")
      .w_GENVENT=space(1)
      .w_ACODVEN=space(15)
      .w_CODCAU=space(5)
      .w_ATIPVEN=space(1)
      .w_PARVEN=space(1)
      .w_TIPVEN=space(1)
      .w_CODVEN=space(15)
      .w_ACODIVA=space(15)
      .w_CODIVA=space(15)
      .w_PNDESSUP=space(45)
      .w_DESCAU=space(35)
      .w_FLSALF=space(1)
      .w_FLSALI=space(1)
      .w_DESPIA=space(40)
      .w_DESPIV=space(40)
      .w_TIPIVA=space(1)
      .w_CONFE2=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_FLPART=space(1)
      .w_PARIVA=space(1)
      .w_AGGDOC=space(1)
      .w_CAUGIR=space(5)
      .w_CONIVA=space(15)
      .w_CREDRIP=space(15)
      .w_CREDSPE=space(15)
      .w_ACONIND=space(15)
      .w_INTDOV=space(15)
      .w_CONIND=space(15)
      .w_DESRIP=space(35)
      .w_DESPE=space(35)
      .w_DESDOV=space(35)
      .w_GENAUTO=space(1)
      .w_DESIVA=space(35)
      .w_ACREDSPE=space(15)
      .w_AINTDOV=space(15)
      .w_ACREDRIP=space(15)
      .w_ACONIVA=space(15)
      .w_ACAUGIR=space(5)
      .w_DESCAU1=space(35)
      .w_DESIND=space(35)
      .w_STALIG=ctod("  /  /  ")
      .w_DATBLO=ctod("  /  /  ")
      .w_TIPREG=oParentObject.w_TIPREG
      .w_TOTCOV=oParentObject.w_TOTCOV
      .w_PNDATREG=oParentObject.w_PNDATREG
      .w_GENVENT=oParentObject.w_GENVENT
      .w_CODCAU=oParentObject.w_CODCAU
      .w_TIPVEN=oParentObject.w_TIPVEN
      .w_CODVEN=oParentObject.w_CODVEN
      .w_CODIVA=oParentObject.w_CODIVA
      .w_PNDESSUP=oParentObject.w_PNDESSUP
      .w_FLSALF=oParentObject.w_FLSALF
      .w_FLSALI=oParentObject.w_FLSALI
      .w_TIPIVA=oParentObject.w_TIPIVA
      .w_CONFE2=oParentObject.w_CONFE2
      .w_AGGDOC=oParentObject.w_AGGDOC
      .w_CAUGIR=oParentObject.w_CAUGIR
      .w_CONIVA=oParentObject.w_CONIVA
      .w_CREDRIP=oParentObject.w_CREDRIP
      .w_CREDSPE=oParentObject.w_CREDSPE
      .w_INTDOV=oParentObject.w_INTDOV
      .w_CONIND=oParentObject.w_CONIND
      .w_GENAUTO=oParentObject.w_GENAUTO
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_TIPCON = 'G'
          .DoRTCalc(3,9,.f.)
        .w_PNDATREG = i_datsys
          .DoRTCalc(11,12,.f.)
        .w_CODCAU = .w_ACODCAU
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODCAU))
          .link_1_13('Full')
        endif
          .DoRTCalc(14,16,.f.)
        .w_CODVEN = .w_ACODVEN
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CODVEN))
          .link_1_17('Full')
        endif
          .DoRTCalc(18,18,.f.)
        .w_CODIVA = .w_ACODIVA
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CODIVA))
          .link_1_21('Full')
        endif
          .DoRTCalc(20,26,.f.)
        .w_CONFE2 = 'S'
        .w_OBTEST = i_INIDAT
          .DoRTCalc(29,30,.f.)
        .w_AGGDOC = ' '
        .w_CAUGIR = .w_ACAUGIR
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_CAUGIR))
          .link_2_1('Full')
        endif
        .w_CONIVA = .w_ACONIVA
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_CONIVA))
          .link_2_2('Full')
        endif
        .w_CREDRIP = .w_ACREDRIP
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_CREDRIP))
          .link_2_4('Full')
        endif
        .w_CREDSPE = .w_ACREDSPE
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_CREDSPE))
          .link_2_6('Full')
        endif
          .DoRTCalc(36,36,.f.)
        .w_INTDOV = IIF(.w_TIPDEN='T',.w_AINTDOV,Space(15))
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_INTDOV))
          .link_2_9('Full')
        endif
        .w_CONIND = .w_ACONIND
        .DoRTCalc(38,38,.f.)
        if not(empty(.w_CONIND))
          .link_2_10('Full')
        endif
    endwith
    this.DoRTCalc(39,52,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_kci
    IF this.w_NEWIVA<>'S'
       this.oPgFrm.Pages(2).enabled=.f.
    ENDIF
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TIPREG=.w_TIPREG
      .oParentObject.w_TOTCOV=.w_TOTCOV
      .oParentObject.w_PNDATREG=.w_PNDATREG
      .oParentObject.w_GENVENT=.w_GENVENT
      .oParentObject.w_CODCAU=.w_CODCAU
      .oParentObject.w_TIPVEN=.w_TIPVEN
      .oParentObject.w_CODVEN=.w_CODVEN
      .oParentObject.w_CODIVA=.w_CODIVA
      .oParentObject.w_PNDESSUP=.w_PNDESSUP
      .oParentObject.w_FLSALF=.w_FLSALF
      .oParentObject.w_FLSALI=.w_FLSALI
      .oParentObject.w_TIPIVA=.w_TIPIVA
      .oParentObject.w_CONFE2=.w_CONFE2
      .oParentObject.w_AGGDOC=.w_AGGDOC
      .oParentObject.w_CAUGIR=.w_CAUGIR
      .oParentObject.w_CONIVA=.w_CONIVA
      .oParentObject.w_CREDRIP=.w_CREDRIP
      .oParentObject.w_CREDSPE=.w_CREDSPE
      .oParentObject.w_INTDOV=.w_INTDOV
      .oParentObject.w_CONIND=.w_CONIND
      .oParentObject.w_GENAUTO=.w_GENAUTO
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,10,.t.)
        if .o_TOTCOV<>.w_TOTCOV
            .w_GENVENT = IIF(.w_TOTCOV=0, ' ', 'S')
        endif
        .DoRTCalc(12,41,.t.)
        if .o_NEWIVA<>.w_NEWIVA
            .w_GENAUTO = IIF(.w_NEWIVA='S', 'S', ' ')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(43,52,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODCAU_1_13.enabled = this.oPgFrm.Page1.oPag.oCODCAU_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCODVEN_1_17.enabled = this.oPgFrm.Page1.oPag.oCODVEN_1_17.mCond()
    this.oPgFrm.Page1.oPag.oCODIVA_1_21.enabled = this.oPgFrm.Page1.oPag.oCODIVA_1_21.mCond()
    this.oPgFrm.Page2.oPag.oCAUGIR_2_1.enabled = this.oPgFrm.Page2.oPag.oCAUGIR_2_1.mCond()
    this.oPgFrm.Page2.oPag.oCONIVA_2_2.enabled = this.oPgFrm.Page2.oPag.oCONIVA_2_2.mCond()
    this.oPgFrm.Page2.oPag.oCREDRIP_2_4.enabled = this.oPgFrm.Page2.oPag.oCREDRIP_2_4.mCond()
    this.oPgFrm.Page2.oPag.oCREDSPE_2_6.enabled = this.oPgFrm.Page2.oPag.oCREDSPE_2_6.mCond()
    this.oPgFrm.Page2.oPag.oINTDOV_2_9.enabled = this.oPgFrm.Page2.oPag.oINTDOV_2_9.mCond()
    this.oPgFrm.Page2.oPag.oCONIND_2_10.enabled = this.oPgFrm.Page2.oPag.oCONIND_2_10.mCond()
    this.oPgFrm.Page1.oPag.oGENAUTO_1_41.enabled = this.oPgFrm.Page1.oPag.oGENAUTO_1_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODVEN_1_17.visible=!this.oPgFrm.Page1.oPag.oCODVEN_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oDESPIA_1_31.visible=!this.oPgFrm.Page1.oPag.oDESPIA_1_31.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCAUIVA,AZCONIVA,AZGIRIVA,AZNEWIVA,AZCAUGIR,AZIVCONT,AZCRERIP,AZCRESPE,AZINTDOV,AZCODVEN,AZTIPDEN,AZCONIND,AZSTALIG,AZDATBLO";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZCAUIVA,AZCONIVA,AZGIRIVA,AZNEWIVA,AZCAUGIR,AZIVCONT,AZCRERIP,AZCRESPE,AZINTDOV,AZCODVEN,AZTIPDEN,AZCONIND,AZSTALIG,AZDATBLO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_ACODCAU = NVL(_Link_.AZCAUIVA,space(5))
      this.w_ACODIVA = NVL(_Link_.AZCONIVA,space(15))
      this.w_GIRIVA = NVL(_Link_.AZGIRIVA,space(2))
      this.w_NEWIVA = NVL(_Link_.AZNEWIVA,space(1))
      this.w_ACAUGIR = NVL(_Link_.AZCAUGIR,space(5))
      this.w_ACONIVA = NVL(_Link_.AZIVCONT,space(15))
      this.w_ACREDRIP = NVL(_Link_.AZCRERIP,space(15))
      this.w_ACREDSPE = NVL(_Link_.AZCRESPE,space(15))
      this.w_AINTDOV = NVL(_Link_.AZINTDOV,space(15))
      this.w_ACODVEN = NVL(_Link_.AZCODVEN,space(15))
      this.w_TIPDEN = NVL(_Link_.AZTIPDEN,space(1))
      this.w_ACONIND = NVL(_Link_.AZCONIND,space(15))
      this.w_STALIG = NVL(cp_ToDate(_Link_.AZSTALIG),ctod("  /  /  "))
      this.w_DATBLO = NVL(cp_ToDate(_Link_.AZDATBLO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_ACODCAU = space(5)
      this.w_ACODIVA = space(15)
      this.w_GIRIVA = space(2)
      this.w_NEWIVA = space(1)
      this.w_ACAUGIR = space(5)
      this.w_ACONIVA = space(15)
      this.w_ACREDRIP = space(15)
      this.w_ACREDSPE = space(15)
      this.w_AINTDOV = space(15)
      this.w_ACODVEN = space(15)
      this.w_TIPDEN = space(1)
      this.w_ACONIND = space(15)
      this.w_STALIG = ctod("  /  /  ")
      this.w_DATBLO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAU
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLSALI,CCFLSALF,CCTIPREG,CCFLPART,CCFLANAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CODCAU))
          select CCCODICE,CCDESCRI,CCFLSALI,CCFLSALF,CCTIPREG,CCFLPART,CCFLANAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCODCAU_1_13'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCG0KCA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLSALI,CCFLSALF,CCTIPREG,CCFLPART,CCFLANAL";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLSALI,CCFLSALF,CCTIPREG,CCFLPART,CCFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLSALI,CCFLSALF,CCTIPREG,CCFLPART,CCFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CODCAU)
            select CCCODICE,CCDESCRI,CCFLSALI,CCFLSALF,CCTIPREG,CCFLPART,CCFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLSALF = NVL(_Link_.CCFLSALI,space(1))
      this.w_FLSALI = NVL(_Link_.CCFLSALF,space(1))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_FLPART = NVL(_Link_.CCFLPART,space(1))
      this.w_FLANAL = NVL(_Link_.CCFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAU = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_FLSALF = space(1)
      this.w_FLSALI = space(1)
      this.w_TIPREG = space(1)
      this.w_FLPART = space(1)
      this.w_FLANAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=( .w_TIPREG='N' OR .w_TIPREG=' ' ) AND .w_FLANAL<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, la causale contabile movimenta i registri o gestisce l'analitica")
        endif
        this.w_CODCAU = space(5)
        this.w_DESCAU = space(35)
        this.w_FLSALF = space(1)
        this.w_FLSALI = space(1)
        this.w_TIPREG = space(1)
        this.w_FLPART = space(1)
        this.w_FLANAL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVEN
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODVEN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODVEN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVEN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODVEN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODVEN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODVEN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODVEN_1_17'),i_cWhere,'GSAR_BZC',"Conti",'GSCG0KCO.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, conto non di tipo contropartite vendite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODVEN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODVEN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVEN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPIA = NVL(_Link_.ANDESCRI,space(40))
      this.w_ATIPVEN = NVL(_Link_.ANTIPSOT,space(1))
      this.w_PARVEN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODVEN = space(15)
      endif
      this.w_DESPIA = space(40)
      this.w_ATIPVEN = space(1)
      this.w_PARVEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ATIPVEN='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, conto non di tipo contropartite vendite")
        endif
        this.w_CODVEN = space(15)
        this.w_DESPIA = space(40)
        this.w_ATIPVEN = space(1)
        this.w_PARVEN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODIVA
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODIVA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODIVA))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODIVA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODIVA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODIVA)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODIVA) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODIVA_1_21'),i_cWhere,'GSAR_BZC',"Conti",'GSCG0KC1.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODIVA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODIVA)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODIVA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPIV = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPIVA = NVL(_Link_.ANTIPSOT,space(1))
      this.w_PARIVA = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODIVA = space(15)
      endif
      this.w_DESPIV = space(40)
      this.w_TIPIVA = space(1)
      this.w_PARIVA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPIVA='I'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODIVA = space(15)
        this.w_DESPIV = space(40)
        this.w_TIPIVA = space(1)
        this.w_PARIVA = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUGIR
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUGIR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CAUGIR)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLSALI,CCFLSALF,CCTIPREG,CCFLPART,CCFLANAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CAUGIR))
          select CCCODICE,CCDESCRI,CCFLSALI,CCFLSALF,CCTIPREG,CCFLPART,CCFLANAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUGIR)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUGIR) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCAUGIR_2_1'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCG0KCA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLSALI,CCFLSALF,CCTIPREG,CCFLPART,CCFLANAL";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLSALI,CCFLSALF,CCTIPREG,CCFLPART,CCFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUGIR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLSALI,CCFLSALF,CCTIPREG,CCFLPART,CCFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUGIR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUGIR)
            select CCCODICE,CCDESCRI,CCFLSALI,CCFLSALF,CCTIPREG,CCFLPART,CCFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUGIR = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU1 = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLSALF = NVL(_Link_.CCFLSALI,space(1))
      this.w_FLSALI = NVL(_Link_.CCFLSALF,space(1))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_FLPART = NVL(_Link_.CCFLPART,space(1))
      this.w_FLANAL = NVL(_Link_.CCFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUGIR = space(5)
      endif
      this.w_DESCAU1 = space(35)
      this.w_FLSALF = space(1)
      this.w_FLSALI = space(1)
      this.w_TIPREG = space(1)
      this.w_FLPART = space(1)
      this.w_FLANAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=( .w_TIPREG='N' OR .w_TIPREG=' ' ) AND .w_FLANAL<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CAUGIR = space(5)
        this.w_DESCAU1 = space(35)
        this.w_FLSALF = space(1)
        this.w_FLSALI = space(1)
        this.w_TIPREG = space(1)
        this.w_FLPART = space(1)
        this.w_FLANAL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUGIR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONIVA
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CONIVA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CONIVA))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONIVA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CONIVA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CONIVA)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONIVA) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCONIVA_2_2'),i_cWhere,'GSAR_BZC',"Conti",'GSCG0KC1.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CONIVA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CONIVA)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONIVA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESIVA = NVL(_Link_.ANDESCRI,space(35))
      this.w_TIPIVA = NVL(_Link_.ANTIPSOT,space(1))
      this.w_PARIVA = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CONIVA = space(15)
      endif
      this.w_DESIVA = space(35)
      this.w_TIPIVA = space(1)
      this.w_PARIVA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPIVA='I'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CONIVA = space(15)
        this.w_DESIVA = space(35)
        this.w_TIPIVA = space(1)
        this.w_PARIVA = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CREDRIP
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CREDRIP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CREDRIP)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CREDRIP))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CREDRIP)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CREDRIP)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CREDRIP)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CREDRIP) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCREDRIP_2_4'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CREDRIP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CREDRIP);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CREDRIP)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CREDRIP = NVL(_Link_.ANCODICE,space(15))
      this.w_DESRIP = NVL(_Link_.ANDESCRI,space(35))
      this.w_TIPVEN = NVL(_Link_.ANTIPSOT,space(1))
      this.w_PARVEN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CREDRIP = space(15)
      endif
      this.w_DESRIP = space(35)
      this.w_TIPVEN = space(1)
      this.w_PARVEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CREDRIP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CREDSPE
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CREDSPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CREDSPE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CREDSPE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CREDSPE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CREDSPE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CREDSPE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CREDSPE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCREDSPE_2_6'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CREDSPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CREDSPE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CREDSPE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CREDSPE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPE = NVL(_Link_.ANDESCRI,space(35))
      this.w_TIPVEN = NVL(_Link_.ANTIPSOT,space(1))
      this.w_PARVEN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CREDSPE = space(15)
      endif
      this.w_DESPE = space(35)
      this.w_TIPVEN = space(1)
      this.w_PARVEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CREDSPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=INTDOV
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_INTDOV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_INTDOV)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_INTDOV))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_INTDOV)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_INTDOV)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_INTDOV)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_INTDOV) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oINTDOV_2_9'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_INTDOV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_INTDOV);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_INTDOV)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_INTDOV = NVL(_Link_.ANCODICE,space(15))
      this.w_DESDOV = NVL(_Link_.ANDESCRI,space(35))
      this.w_TIPVEN = NVL(_Link_.ANTIPSOT,space(1))
      this.w_PARVEN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_INTDOV = space(15)
      endif
      this.w_DESDOV = space(35)
      this.w_TIPVEN = space(1)
      this.w_PARVEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_INTDOV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONIND
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONIND) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CONIND)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CONIND))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONIND)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CONIND)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CONIND)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONIND) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCONIND_2_10'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONIND)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CONIND);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CONIND)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONIND = NVL(_Link_.ANCODICE,space(15))
      this.w_DESIND = NVL(_Link_.ANDESCRI,space(35))
      this.w_TIPVEN = NVL(_Link_.ANTIPSOT,space(1))
      this.w_PARVEN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CONIND = space(15)
      endif
      this.w_DESIND = space(35)
      this.w_TIPVEN = space(1)
      this.w_PARVEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONIND Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTOTCOV_1_8.value==this.w_TOTCOV)
      this.oPgFrm.Page1.oPag.oTOTCOV_1_8.value=this.w_TOTCOV
    endif
    if not(this.oPgFrm.Page1.oPag.oPNDATREG_1_10.value==this.w_PNDATREG)
      this.oPgFrm.Page1.oPag.oPNDATREG_1_10.value=this.w_PNDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oGENVENT_1_11.RadioValue()==this.w_GENVENT)
      this.oPgFrm.Page1.oPag.oGENVENT_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAU_1_13.value==this.w_CODCAU)
      this.oPgFrm.Page1.oPag.oCODCAU_1_13.value=this.w_CODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVEN_1_17.value==this.w_CODVEN)
      this.oPgFrm.Page1.oPag.oCODVEN_1_17.value=this.w_CODVEN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODIVA_1_21.value==this.w_CODIVA)
      this.oPgFrm.Page1.oPag.oCODIVA_1_21.value=this.w_CODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oPNDESSUP_1_22.value==this.w_PNDESSUP)
      this.oPgFrm.Page1.oPag.oPNDESSUP_1_22.value=this.w_PNDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_28.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_28.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPIA_1_31.value==this.w_DESPIA)
      this.oPgFrm.Page1.oPag.oDESPIA_1_31.value=this.w_DESPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPIV_1_32.value==this.w_DESPIV)
      this.oPgFrm.Page1.oPag.oDESPIV_1_32.value=this.w_DESPIV
    endif
    if not(this.oPgFrm.Page1.oPag.oAGGDOC_1_40.RadioValue()==this.w_AGGDOC)
      this.oPgFrm.Page1.oPag.oAGGDOC_1_40.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCAUGIR_2_1.value==this.w_CAUGIR)
      this.oPgFrm.Page2.oPag.oCAUGIR_2_1.value=this.w_CAUGIR
    endif
    if not(this.oPgFrm.Page2.oPag.oCONIVA_2_2.value==this.w_CONIVA)
      this.oPgFrm.Page2.oPag.oCONIVA_2_2.value=this.w_CONIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oCREDRIP_2_4.value==this.w_CREDRIP)
      this.oPgFrm.Page2.oPag.oCREDRIP_2_4.value=this.w_CREDRIP
    endif
    if not(this.oPgFrm.Page2.oPag.oCREDSPE_2_6.value==this.w_CREDSPE)
      this.oPgFrm.Page2.oPag.oCREDSPE_2_6.value=this.w_CREDSPE
    endif
    if not(this.oPgFrm.Page2.oPag.oINTDOV_2_9.value==this.w_INTDOV)
      this.oPgFrm.Page2.oPag.oINTDOV_2_9.value=this.w_INTDOV
    endif
    if not(this.oPgFrm.Page2.oPag.oCONIND_2_10.value==this.w_CONIND)
      this.oPgFrm.Page2.oPag.oCONIND_2_10.value=this.w_CONIND
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRIP_2_14.value==this.w_DESRIP)
      this.oPgFrm.Page2.oPag.oDESRIP_2_14.value=this.w_DESRIP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPE_2_15.value==this.w_DESPE)
      this.oPgFrm.Page2.oPag.oDESPE_2_15.value=this.w_DESPE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDOV_2_16.value==this.w_DESDOV)
      this.oPgFrm.Page2.oPag.oDESDOV_2_16.value=this.w_DESDOV
    endif
    if not(this.oPgFrm.Page1.oPag.oGENAUTO_1_41.RadioValue()==this.w_GENAUTO)
      this.oPgFrm.Page1.oPag.oGENAUTO_1_41.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESIVA_2_18.value==this.w_DESIVA)
      this.oPgFrm.Page2.oPag.oDESIVA_2_18.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU1_2_24.value==this.w_DESCAU1)
      this.oPgFrm.Page2.oPag.oDESCAU1_2_24.value=this.w_DESCAU1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESIND_2_26.value==this.w_DESIND)
      this.oPgFrm.Page2.oPag.oDESIND_2_26.value=this.w_DESIND
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PNDATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPNDATREG_1_10.SetFocus()
            i_bnoObbl = !empty(.w_PNDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CODCAU)) or not(( .w_TIPREG='N' OR .w_TIPREG=' ' ) AND .w_FLANAL<>'S'))  and (.w_GENVENT='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCAU_1_13.SetFocus()
            i_bnoObbl = !empty(.w_CODCAU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, la causale contabile movimenta i registri o gestisce l'analitica")
          case   ((empty(.w_CODVEN)) or not(.w_ATIPVEN='V'))  and not(.w_GIRIVA='SI')  and (.w_GENVENT='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODVEN_1_17.SetFocus()
            i_bnoObbl = !empty(.w_CODVEN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, conto non di tipo contropartite vendite")
          case   ((empty(.w_CODIVA)) or not(.w_TIPIVA='I'))  and (.w_GENVENT='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODIVA_1_21.SetFocus()
            i_bnoObbl = !empty(.w_CODIVA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CAUGIR)) or not(( .w_TIPREG='N' OR .w_TIPREG=' ' ) AND .w_FLANAL<>'S'))  and (.w_GENAUTO='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCAUGIR_2_1.SetFocus()
            i_bnoObbl = !empty(.w_CAUGIR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CONIVA)) or not(.w_TIPIVA='I'))  and (.w_GENAUTO='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCONIVA_2_2.SetFocus()
            i_bnoObbl = !empty(.w_CONIVA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CREDRIP))  and (.w_GENAUTO='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCREDRIP_2_4.SetFocus()
            i_bnoObbl = !empty(.w_CREDRIP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CREDSPE))  and (.w_GENAUTO='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCREDSPE_2_6.SetFocus()
            i_bnoObbl = !empty(.w_CREDSPE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_INTDOV))  and (.w_GENAUTO='S' AND .w_TIPDEN='T')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oINTDOV_2_9.SetFocus()
            i_bnoObbl = !empty(.w_INTDOV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CONIND))  and (.w_GENAUTO='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCONIND_2_10.SetFocus()
            i_bnoObbl = !empty(.w_CONIND)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_kci
      if .w_FLPART<>'N' and i_bRes=.t.
         do case
            case .w_PARIVA='S'
              i_cErrorMsg = ah_MsgFormat("Conto IVA su corrispettivi gestito a partite. %0Selezionare un conto o una causale che non gestisca le partite")
              i_bnoChk = .f.
              i_bRes = .f.
            case .w_PARVEN='S'
              i_cErrorMsg = ah_MsgFormat("Conto vendite corrispettivi gestito a partite. %0Selezionare un conto o una causale che non gestisca le partite")
              i_bnoChk = .f.
              i_bRes = .f.
            case Not Empty(CHKCONS('P',.oparentobject.w_PNDATREG,'B','P'))
             i_cErrorMsg=CHKCONS('P',.oparentobject.w_PNDATREG,'B','N')
             i_bnoChk=.F.
             i_bRes=.F.
            endcase
      endif
      if i_bRes=.t.
        if cp_todate(.w_PNDATREG)<=.w_STALIG
             i_cErrorMsg = ah_MsgFormat("Data registrazione inferiore o uguale a ultima stampa libro giornale")
             i_bnoChk=.F.
             i_bRes=.F.
        endif
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NEWIVA = this.w_NEWIVA
    this.o_TOTCOV = this.w_TOTCOV
    return

enddefine

* --- Define pages as container
define class tgscg_kciPag1 as StdContainer
  Width  = 568
  height = 247
  stdWidth  = 568
  stdheight = 247
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTOTCOV_1_8 as StdField with uid="JMIFUXCAAI",rtseq=8,rtrep=.f.,;
    cFormVar = "w_TOTCOV", cQueryName = "TOTCOV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 89026358,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=21, Top=214, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oPNDATREG_1_10 as StdField with uid="EPOUZVQRDM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PNDATREG", cQueryName = "PNDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 241471939,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=153, Top=14

  add object oGENVENT_1_11 as StdCheck with uid="WSFZOORORB",rtseq=11,rtrep=.f.,left=283, top=14, caption="Genera giroconto IVA ventilata",;
    HelpContextID = 54459290,;
    cFormVar="w_GENVENT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGENVENT_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oGENVENT_1_11.GetRadio()
    this.Parent.oContained.w_GENVENT = this.RadioValue()
    return .t.
  endfunc

  func oGENVENT_1_11.SetRadio()
    this.Parent.oContained.w_GENVENT=trim(this.Parent.oContained.w_GENVENT)
    this.value = ;
      iif(this.Parent.oContained.w_GENVENT=='S',1,;
      0)
  endfunc

  add object oCODCAU_1_13 as StdField with uid="IHRARNSLZJ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODCAU", cQueryName = "CODCAU",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, la causale contabile movimenta i registri o gestisce l'analitica",;
    ToolTipText = "Causale giroconto IVA ventilata",;
    HelpContextID = 57503270,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=153, Top=41, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CODCAU"

  func oCODCAU_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GENVENT='S')
    endwith
   endif
  endfunc

  func oCODCAU_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAU_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAU_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCODCAU_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCG0KCA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCODCAU_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CODCAU
     i_obj.ecpSave()
  endproc

  add object oCODVEN_1_17 as StdField with uid="ILFTPFLION",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODVEN", cQueryName = "CODVEN",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, conto non di tipo contropartite vendite",;
    ToolTipText = "Conto vendite corrispettivi",;
    HelpContextID = 213937702,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=153, Top=69, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODVEN"

  func oCODVEN_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GENVENT='S')
    endwith
   endif
  endfunc

  func oCODVEN_1_17.mHide()
    with this.Parent.oContained
      return (.w_GIRIVA='SI')
    endwith
  endfunc

  func oCODVEN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVEN_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVEN_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODVEN_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'GSCG0KCO.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODVEN_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODVEN
     i_obj.ecpSave()
  endproc

  add object oCODIVA_1_21 as StdField with uid="PETZUGQNDT",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODIVA", cQueryName = "CODIVA",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto IVA su corrispettivi",;
    HelpContextID = 12807718,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=153, Top=97, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODIVA"

  func oCODIVA_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GENVENT='S')
    endwith
   endif
  endfunc

  func oCODIVA_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODIVA_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODIVA_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODIVA_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'GSCG0KC1.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODIVA_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODIVA
     i_obj.ecpSave()
  endproc

  add object oPNDESSUP_1_22 as StdField with uid="ZZCQIFXSJH",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PNDESSUP", cQueryName = "PNDESSUP",;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione aggiuntiva",;
    HelpContextID = 225481146,;
   bGlobalFont=.t.,;
    Height=21, Width=324, Left=153, Top=125, InputMask=replicate('X',45)

  add object oDESCAU_1_28 as StdField with uid="YYMPMMXSML",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 57562166,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=219, Top=41, InputMask=replicate('X',35)

  add object oDESPIA_1_31 as StdField with uid="BLMWSSNOHU",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESPIA", cQueryName = "DESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "descrizione",;
    HelpContextID = 268129334,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=292, Top=69, InputMask=replicate('X',40)

  func oDESPIA_1_31.mHide()
    with this.Parent.oContained
      return (.w_GIRIVA='SI')
    endwith
  endfunc

  add object oDESPIV_1_32 as StdField with uid="PZFQTFAXVZ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESPIV", cQueryName = "DESPIV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "descrizione",;
    HelpContextID = 83579958,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=292, Top=97, InputMask=replicate('X',40)


  add object oBtn_1_34 as StdButton with uid="XMPVUWJGWY",left=501, top=201, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 91913658;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_36 as StdButton with uid="EZEMKUTXGU",left=451, top=201, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 99202330;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((Not Empty(.w_CODCAU) And Not Empty(.w_CODIVA) And (Not Empty(.w_CODVEN) OR .w_GIRIVA='SI') AND .w_GENVENT='S' ) OR ((Not Empty(.w_INTDOV) OR .w_TIPDEN='M') And Not Empty(.w_CREDSPE) And Not Empty(.w_CREDRIP) AND Not Empty(.w_CONIND) AND .w_GENAUTO='S' ))
      endwith
    endif
  endfunc

  add object oAGGDOC_1_40 as StdCheck with uid="OLSYEIAFII",rtseq=31,rtrep=.f.,left=218, top=222, caption="Aggiorna documenti",;
    ToolTipText = "Aggiorna valore fiscale corrispettivi ventilati",;
    HelpContextID = 38704646,;
    cFormVar="w_AGGDOC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGGDOC_1_40.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAGGDOC_1_40.GetRadio()
    this.Parent.oContained.w_AGGDOC = this.RadioValue()
    return .t.
  endfunc

  func oAGGDOC_1_40.SetRadio()
    this.Parent.oContained.w_AGGDOC=trim(this.Parent.oContained.w_AGGDOC)
    this.value = ;
      iif(this.Parent.oContained.w_AGGDOC=='S',1,;
      0)
  endfunc

  add object oGENAUTO_1_41 as StdCheck with uid="IZFDJUPRCJ",rtseq=42,rtrep=.f.,left=153, top=153, caption="Genera giroconto IVA automatico",;
    ToolTipText = "Se attivo genera giroconto IVA automatico",;
    HelpContextID = 61604966,;
    cFormVar="w_GENAUTO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGENAUTO_1_41.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oGENAUTO_1_41.GetRadio()
    this.Parent.oContained.w_GENAUTO = this.RadioValue()
    return .t.
  endfunc

  func oGENAUTO_1_41.SetRadio()
    this.Parent.oContained.w_GENAUTO=trim(this.Parent.oContained.w_GENAUTO)
    this.value = ;
      iif(this.Parent.oContained.w_GENAUTO=='S',1,;
      0)
  endfunc

  func oGENAUTO_1_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NEWIVA='S')
    endwith
   endif
  endfunc

  add object oStr_1_18 as StdString with uid="XWEYXPCAHQ",Visible=.t., Left=34, Top=41,;
    Alignment=1, Width=116, Height=15,;
    Caption="Causale contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="MXDHGJSXTW",Visible=.t., Left=3, Top=125,;
    Alignment=1, Width=147, Height=15,;
    Caption="Descr. aggiuntiva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="XLEDOBGKXY",Visible=.t., Left=15, Top=69,;
    Alignment=1, Width=135, Height=15,;
    Caption="Conto vendite corr.:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_GIRIVA='SI')
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="KSEPVDPPRO",Visible=.t., Left=18, Top=97,;
    Alignment=1, Width=132, Height=15,;
    Caption="Conto IVA su corr.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="AERPJIYPPY",Visible=.t., Left=30, Top=14,;
    Alignment=1, Width=120, Height=15,;
    Caption="Data registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="VOOAQGKIKJ",Visible=.t., Left=9, Top=185,;
    Alignment=0, Width=169, Height=15,;
    Caption="Importo IVA ventilata"  ;
  , bGlobalFont=.t.

  add object oBox_1_26 as StdBox with uid="FNXJTHPPVP",left=9, top=204, width=169,height=39
enddefine
define class tgscg_kciPag2 as StdContainer
  Width  = 568
  height = 247
  stdWidth  = 568
  stdheight = 247
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCAUGIR_2_1 as StdField with uid="LNNKXVHBBF",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CAUGIR", cQueryName = "CAUGIR",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale giroconto IVA ventilata",;
    HelpContextID = 15888422,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=175, Top=10, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CAUGIR"

  func oCAUGIR_2_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GENAUTO='S')
    endwith
   endif
  endfunc

  func oCAUGIR_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUGIR_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUGIR_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCAUGIR_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCG0KCA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCAUGIR_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CAUGIR
     i_obj.ecpSave()
  endproc

  add object oCONIVA_2_2 as StdField with uid="VDLFBJPMRI",rtseq=33,rtrep=.f.,;
    cFormVar = "w_CONIVA", cQueryName = "CONIVA",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto IVA su corrispettivi",;
    HelpContextID = 12848678,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=175, Top=39, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CONIVA"

  func oCONIVA_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GENAUTO='S')
    endwith
   endif
  endfunc

  func oCONIVA_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONIVA_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONIVA_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCONIVA_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'GSCG0KC1.CONTI_VZM',this.parent.oContained
  endproc
  proc oCONIVA_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CONIVA
     i_obj.ecpSave()
  endproc

  add object oCREDRIP_2_4 as StdField with uid="CRPFJAOJLJ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CREDRIP", cQueryName = "CREDRIP",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto vendite corrispettivi",;
    HelpContextID = 142508326,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=174, Top=111, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CREDRIP"

  func oCREDRIP_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GENAUTO='S')
    endwith
   endif
  endfunc

  func oCREDRIP_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCREDRIP_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCREDRIP_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCREDRIP_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCREDRIP_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CREDRIP
     i_obj.ecpSave()
  endproc

  add object oCREDSPE_2_6 as StdField with uid="SWFCHKIOEB",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CREDSPE", cQueryName = "CREDSPE",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto vendite corrispettivi",;
    HelpContextID = 7438042,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=175, Top=140, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CREDSPE"

  func oCREDSPE_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GENAUTO='S')
    endwith
   endif
  endfunc

  func oCREDSPE_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCREDSPE_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCREDSPE_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCREDSPE_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCREDSPE_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CREDSPE
     i_obj.ecpSave()
  endproc

  add object oINTDOV_2_9 as StdField with uid="RNTBKZNHRT",rtseq=37,rtrep=.f.,;
    cFormVar = "w_INTDOV", cQueryName = "INTDOV",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto vendite corrispettivi",;
    HelpContextID = 89091462,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=175, Top=169, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_INTDOV"

  func oINTDOV_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GENAUTO='S' AND .w_TIPDEN='T')
    endwith
   endif
  endfunc

  func oINTDOV_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oINTDOV_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oINTDOV_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oINTDOV_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oINTDOV_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_INTDOV
     i_obj.ecpSave()
  endproc

  add object oCONIND_2_10 as StdField with uid="WLZVISDTUU",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CONIND", cQueryName = "CONIND",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto IVA indetraibile",;
    HelpContextID = 54791718,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=175, Top=198, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CONIND"

  func oCONIND_2_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GENAUTO='S')
    endwith
   endif
  endfunc

  func oCONIND_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONIND_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONIND_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCONIND_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCONIND_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CONIND
     i_obj.ecpSave()
  endproc

  add object oDESRIP_2_14 as StdField with uid="SQIXKFZWFD",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESRIP", cQueryName = "DESRIP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 251483190,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=311, Top=111, InputMask=replicate('X',35)

  add object oDESPE_2_15 as StdField with uid="NPOJHQYXSB",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESPE", cQueryName = "DESPE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 21277642,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=311, Top=140, InputMask=replicate('X',35)

  add object oDESDOV_2_16 as StdField with uid="NLTVVJNUVW",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESDOV", cQueryName = "DESDOV",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 89084982,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=311, Top=169, InputMask=replicate('X',35)

  add object oDESIVA_2_18 as StdField with uid="EVIOWNABDO",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 12866614,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=311, Top=39, InputMask=replicate('X',35)

  add object oDESCAU1_2_24 as StdField with uid="PMUQMJJASU",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESCAU1", cQueryName = "DESCAU1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 57562166,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=311, Top=10, InputMask=replicate('X',35)

  add object oDESIND_2_26 as StdField with uid="JLFHLOLEWU",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESIND", cQueryName = "DESIND",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 54809654,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=311, Top=198, InputMask=replicate('X',35)

  add object oStr_2_3 as StdString with uid="PXWQELIWQZ",Visible=.t., Left=20, Top=11,;
    Alignment=1, Width=152, Height=18,;
    Caption="Causale giroconto IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="CVBSOGBEVK",Visible=.t., Left=20, Top=111,;
    Alignment=1, Width=152, Height=18,;
    Caption="Debito/credito riportato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="ELMGAADIEH",Visible=.t., Left=20, Top=140,;
    Alignment=1, Width=152, Height=18,;
    Caption="Crediti speciali:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="XQVGKSHWXO",Visible=.t., Left=20, Top=169,;
    Alignment=1, Width=152, Height=18,;
    Caption="Interessi dovuti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="JIGGUYPURE",Visible=.t., Left=6, Top=86,;
    Alignment=0, Width=230, Height=18,;
    Caption="Contropartite giroconto IVA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_17 as StdString with uid="HRQVFYNAYN",Visible=.t., Left=20, Top=39,;
    Alignment=1, Width=152, Height=18,;
    Caption="Conto IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="RUQLASDPPX",Visible=.t., Left=5, Top=199,;
    Alignment=1, Width=167, Height=18,;
    Caption="Conto IVA indetraibile:"  ;
  , bGlobalFont=.t.

  add object oBox_2_13 as StdBox with uid="LQPXLCBLCI",left=2, top=103, width=563,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kci','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
