* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bke                                                        *
*              Evasione righe descrittive doc.chiusi                           *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-03                                                      *
* Last revis.: 2008-04-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bke",oParentObject,m.pTipo)
return(i_retval)

define class tgsve_bke as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_PUNPAD = .NULL.
  w_SERIAL = space(10)
  * --- WorkFile variables
  DOC_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riempimento Zoom Evasione Righe Descrittive.. GSVE_KED;  Update DOC_DETT
    this.w_PUNPAD = this.oParentObject
    MM = this.w_PUNPAD.w_ZoomDet.cCursor
    do case
      case this.pTipo="R"
        this.w_PUNPAD.NotifyEvent("Esegui")     
      case this.pTipo="E"
        Select * from (MM) where XCHK=1 into Cursor APPMM
        if USED("AppMM")
          if Not (_TALLY>0)
            ah_ErrorMsg("Nessun documento selezionato",48," ")
            SELECT AppMM
            USE
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Try
        local bErr_03993F60
        bErr_03993F60=bTrsErr
        this.Try_03993F60()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Aggiornamento non eseguito: %1", 16, "", message())
        endif
        bTrsErr=bTrsErr or bErr_03993F60
        * --- End
      case this.pTipo="S"
        if this.oParentObject.w_SELEZI = "S"
          * --- Seleziona Tutto 
          Update (MM) Set XCHK=1
        else
          * --- Deseleziona Tutto
          UPDATE (MM) SET XCHK=0 
        endif
        * --- Si riposiziona all'inizio
        Select (MM) 
 Go top
      case this.pTipo="C"
        * --- Imposta il titolo della maschera
        if this.oParentObject.w_CICLO="V"
          this.w_PUNPAD.Caption = ah_msgformat("Evasione righe descrittive di documenti chiusi (vendite)")
        else
          this.w_PUNPAD.Caption = ah_msgformat("Evasione righe descrittive di documenti chiusi (acquisti)")
        endif
    endcase
  endproc
  proc Try_03993F60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    do while not eof ("AppMM")
      this.w_SERIAL = MVSERIAL
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
            +" and MVTIPRIG = "+cp_ToStrODBC("D");
               )
      else
        update (i_cTable) set;
            MVFLEVAS = "S";
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_SERIAL;
            and MVTIPRIG = "D";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      Select AppMM
      if Not Eof("AppMM")
        Skip
      endif
    enddo
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Aggiornamento terminato")
    this.w_PUNPAD.NotifyEvent("Esegui")     
    if USED("AppMM")
      SELECT AppMM
      USE
    endif
    return


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_DETT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
