* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bbp                                                        *
*              Creaazione partite parametrica                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_48]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-16                                                      *
* Last revis.: 2014-09-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,pSERRIF,pORDRIF,pNUMRIF,pSERIAL,pROWORD,pROWNUM,pTOTIMP,pTOTABB,pTOTACC,pFLINDI,pFLIMPE,pNUMDIS,pCAOVAL,pCAOAPE,pDATAPE,pDESRIG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bbp",oParentObject,m.pOPER,m.pSERRIF,m.pORDRIF,m.pNUMRIF,m.pSERIAL,m.pROWORD,m.pROWNUM,m.pTOTIMP,m.pTOTABB,m.pTOTACC,m.pFLINDI,m.pFLIMPE,m.pNUMDIS,m.pCAOVAL,m.pCAOAPE,m.pDATAPE,m.pDESRIG)
return(i_retval)

define class tgscg_bbp as StdBatch
  * --- Local variables
  pOPER = space(1)
  pSERRIF = space(10)
  pORDRIF = 0
  pNUMRIF = 0
  pSERIAL = space(10)
  pROWORD = 0
  pROWNUM = 0
  pTOTIMP = 0
  pTOTABB = 0
  pTOTACC = 0
  pFLINDI = space(1)
  pFLIMPE = space(2)
  pNUMDIS = space(10)
  pCAOVAL = 0
  pCAOAPE = 0
  pDATAPE = ctod("  /  /  ")
  pDESRIG = space(50)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_PTROWNUM = 0
  w_PTNUMPAR = space(31)
  w_PT_SEGNO = space(1)
  w_PTCODVAL = space(3)
  w_PTCAOVAL = 0
  w_PTMODPAG = space(10)
  w_PTFLCRSA = space(1)
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PTNUMDOC = 0
  w_PTALFDOC = space(10)
  w_PTDATDOC = ctod("  /  /  ")
  w_PTDATSCA = ctod("  /  /  ")
  w_PTIMPDOC = 0
  w_PTFLSOSP = space(1)
  w_PTBANNOS = space(15)
  w_PTFLRAGG = space(1)
  w_PTBANAPP = space(10)
  w_PTNUMDIS = space(10)
  w_PTDESRIG = space(50)
  w_PTCODAGE = space(5)
  w_PTFLVABD = space(1)
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_PTTOTIMP = 0
  w_PTTOTABB = 0
  w_PTFLIMPE = space(2)
  w_PTFLINDI = space(1)
  w_PTCAOAPE = 0
  w_PTDATAPE = ctod("  /  /  ")
  w_MODPAG = space(10)
  w_TIPPAG = space(2)
  w_CODESE = space(4)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_DATSCA = ctod("  /  /  ")
  w_IMPORTO = 0
  w_VALACC = 0
  w_PTDATREG = ctod("  /  /  ")
  w_PTNUMCOR = space(25)
  w_CODAGE = space(5)
  w_ORIGINE = .NULL.
  w_DESCRIZIONE = space(50)
  w_RAGSOCINT = space(60)
  w_NUMRER = 0
  w_CLIBANAPP = space(10)
  w_CLIBANNOS = space(15)
  * --- WorkFile variables
  PAR_TITE_idx=0
  CONTI_idx=0
  PAG_2AME_idx=0
  CONTROPA_idx=0
  PNT_MAST_idx=0
  BAN_CONTI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiavi Partita di creazione
    *     Lanciato da 
    *     GSCG_BSS  chiusura Saldaconto
    *     GSCG_BCI  contabilizzazione Indiretta effetti
    * --- Tipo di operazione da eseguire
    *     A  crea partita di abbuono
    *     S  Salda Partite
    *     I  Indiretta Effetti
    *        in questo caso le banche devo leggerle dalla partita di creazione
    * --- Chiavi Primanota
    * --- Campi ricalcolati della partita di saldo
    this.w_PTSERIAL = this.pSERIAL
    this.w_PTROWORD = this.pROWORD
    this.w_PTROWNUM = this.pROWNUM
    this.w_PTSERRIF = this.pSERRIF
    this.w_PTORDRIF = this.pORDRIF
    this.w_PTNUMRIF = this.pNUMRIF
    this.w_PTTOTIMP = ABS(this.pTOTIMP-this.pTOTACC)
    this.w_PTTOTABB = this.pTOTABB
    this.w_VALACC = this.pTOTACC
    this.w_PTFLIMPE = this.pFLIMPE
    this.w_PTFLINDI = this.pFLINDI
    this.w_PTCAOAPE = this.pCAOAPE
    this.w_PTCAOVAL = this.pCAOVAL
    this.w_PTDATAPE = this.pDATAPE
    this.w_PTNUMDIS = this.pNUMDIS
    this.w_PTDESRIG = this.pDESRIG
    * --- Leggo informazioni dalla partita di Creazione
    if NOT EMPTY(this.pSERRIF)
      * --- Read from PAR_TITE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTCODVAL,PTNUMDOC,PTALFDOC,PTDATDOC,PTMODPAG,PTFLSOSP,PTFLRAGG,PTCODAGE,PTFLVABD,PTIMPDOC,PTDATREG,PTBANNOS,PTBANAPP,PTNUMCOR"+;
          " from "+i_cTable+" PAR_TITE where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERRIF);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_PTORDRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_PTNUMRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTCODVAL,PTNUMDOC,PTALFDOC,PTDATDOC,PTMODPAG,PTFLSOSP,PTFLRAGG,PTCODAGE,PTFLVABD,PTIMPDOC,PTDATREG,PTBANNOS,PTBANAPP,PTNUMCOR;
          from (i_cTable) where;
              PTSERIAL = this.w_PTSERRIF;
              and PTROWORD = this.w_PTORDRIF;
              and CPROWNUM = this.w_PTNUMRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PTNUMPAR = NVL(cp_ToDate(_read_.PTNUMPAR),cp_NullValue(_read_.PTNUMPAR))
        this.w_PTDATSCA = NVL(cp_ToDate(_read_.PTDATSCA),cp_NullValue(_read_.PTDATSCA))
        this.w_PTTIPCON = NVL(cp_ToDate(_read_.PTTIPCON),cp_NullValue(_read_.PTTIPCON))
        this.w_PTCODCON = NVL(cp_ToDate(_read_.PTCODCON),cp_NullValue(_read_.PTCODCON))
        this.w_PT_SEGNO = NVL(cp_ToDate(_read_.PT_SEGNO),cp_NullValue(_read_.PT_SEGNO))
        this.w_PTCODVAL = NVL(cp_ToDate(_read_.PTCODVAL),cp_NullValue(_read_.PTCODVAL))
        this.w_PTNUMDOC = NVL(cp_ToDate(_read_.PTNUMDOC),cp_NullValue(_read_.PTNUMDOC))
        this.w_PTALFDOC = NVL(cp_ToDate(_read_.PTALFDOC),cp_NullValue(_read_.PTALFDOC))
        this.w_PTDATDOC = NVL(cp_ToDate(_read_.PTDATDOC),cp_NullValue(_read_.PTDATDOC))
        this.w_PTMODPAG = NVL(cp_ToDate(_read_.PTMODPAG),cp_NullValue(_read_.PTMODPAG))
        this.w_PTFLSOSP = NVL(cp_ToDate(_read_.PTFLSOSP),cp_NullValue(_read_.PTFLSOSP))
        this.w_PTFLRAGG = NVL(cp_ToDate(_read_.PTFLRAGG),cp_NullValue(_read_.PTFLRAGG))
        this.w_PTCODAGE = NVL(cp_ToDate(_read_.PTCODAGE),cp_NullValue(_read_.PTCODAGE))
        this.w_PTFLVABD = NVL(cp_ToDate(_read_.PTFLVABD),cp_NullValue(_read_.PTFLVABD))
        this.w_PTIMPDOC = NVL(cp_ToDate(_read_.PTIMPDOC),cp_NullValue(_read_.PTIMPDOC))
        this.w_PT_SEGNO = NVL(cp_ToDate(_read_.PT_SEGNO),cp_NullValue(_read_.PT_SEGNO))
        this.w_PTDATREG = NVL(cp_ToDate(_read_.PTDATREG),cp_NullValue(_read_.PTDATREG))
        this.w_PTBANNOS = NVL(cp_ToDate(_read_.PTBANNOS),cp_NullValue(_read_.PTBANNOS))
        this.w_PTBANAPP = NVL(cp_ToDate(_read_.PTBANAPP),cp_NullValue(_read_.PTBANAPP))
        this.w_PTNUMCOR = NVL(cp_ToDate(_read_.PTNUMCOR),cp_NullValue(_read_.PTNUMCOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Read from PNT_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PNDATREG,PNNUMDOC,PNALFDOC,PNDATDOC,PNCODESE,PNNUMRER"+;
        " from "+i_cTable+" PNT_MAST where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PNDATREG,PNNUMDOC,PNALFDOC,PNDATDOC,PNCODESE,PNNUMRER;
        from (i_cTable) where;
            PNSERIAL = this.w_PTSERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATSCA = NVL(cp_ToDate(_read_.PNDATREG),cp_NullValue(_read_.PNDATREG))
      this.w_NUMDOC = NVL(cp_ToDate(_read_.PNNUMDOC),cp_NullValue(_read_.PNNUMDOC))
      this.w_ALFDOC = NVL(cp_ToDate(_read_.PNALFDOC),cp_NullValue(_read_.PNALFDOC))
      this.w_DATDOC = NVL(cp_ToDate(_read_.PNDATDOC),cp_NullValue(_read_.PNDATDOC))
      this.w_CODESE = NVL(cp_ToDate(_read_.PNCODESE),cp_NullValue(_read_.PNCODESE))
      this.w_NUMRER = NVL(cp_ToDate(_read_.PNNUMRER),cp_NullValue(_read_.PNNUMRER))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_PTDATREG = this.w_DATSCA
    if "Acconto" $ this.w_PTNUMPAR
      this.w_PTFLCRSA = "C"
      * --- Un acconto selezionato dall'utente assume come numero partita
      *     il numero registrazione seguito immediatamente da una 'A'.
      *     Questo perch� nel caso di un acconto saldato da chiudi partite in prima nota
      *     non ho riferimenti a fatture (sia la registrazione di acconto che di incasso
      *     non hanno necesariamente numero documento).
      *     Una 'A' alla fine per evitare di creare un numero partita potenzialmente
      *     utilizzabile da una partita normale.
      *     Acconto tolto dal numero partita per evitare che la stessa sia riconsiderata
      *     nelle varie elaborazione relative agli acconti (Like '%acconto%' � il filtro
      *     utilizzato)
      this.w_PTNUMPAR = CANUMPAR("A", this.w_CODESE, this.w_NUMRER)
      * --- Rinomino Partita di Acconto e inserisco  informazioni della partita di creazione
      *     da riportare
      * --- Write into PAR_TITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PTNUMPAR ="+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
        +",PTCODAGE ="+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
        +",PTSERRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PTSERIAL),'PAR_TITE','PTSERRIF');
        +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'PAR_TITE','PTORDRIF');
        +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PTROWNUM),'PAR_TITE','PTNUMRIF');
        +",PTFLVABD ="+cp_NullLink(cp_ToStrODBC(this.w_PTFLVABD),'PAR_TITE','PTFLVABD');
            +i_ccchkf ;
        +" where ";
            +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERRIF);
            +" and PTROWORD = "+cp_ToStrODBC(this.w_PTORDRIF);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_PTNUMRIF);
               )
      else
        update (i_cTable) set;
            PTNUMPAR = this.w_PTNUMPAR;
            ,PTCODAGE = this.w_PTCODAGE;
            ,PTSERRIF = this.w_PTSERIAL;
            ,PTORDRIF = this.w_PTROWORD;
            ,PTNUMRIF = this.w_PTROWNUM;
            ,PTFLVABD = this.w_PTFLVABD;
            &i_ccchkf. ;
         where;
            PTSERIAL = this.w_PTSERRIF;
            and PTROWORD = this.w_PTORDRIF;
            and CPROWNUM = this.w_PTNUMRIF;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Se sto saldando un acconto non devo inserire i riferimenti perch� sto
      *     inserendo la partita di creazione
      this.w_PTSERRIF = Space(10)
      this.w_PTORDRIF = 0
      this.w_PTNUMRIF = 0
    else
      this.w_PTFLCRSA = "S"
    endif
    if this.pOPER<>"I" AND this.w_PTTIPCON<>"G"
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODBAN,ANCODBA2,ANCODAG1,ANDESCRI"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PTTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PTCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODBAN,ANCODBA2,ANCODAG1,ANDESCRI;
          from (i_cTable) where;
              ANTIPCON = this.w_PTTIPCON;
              and ANCODICE = this.w_PTCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CLIBANAPP = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
        this.w_CLIBANNOS = NVL(cp_ToDate(_read_.ANCODBA2),cp_NullValue(_read_.ANCODBA2))
        this.w_CODAGE = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
        this.w_RAGSOCINT = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if Empty(this.w_PTNUMCOR)
      * --- Read from BAN_CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.BAN_CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.BAN_CONTI_idx,2],.t.,this.BAN_CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCCONCOR"+;
          " from "+i_cTable+" BAN_CONTI where ";
              +"CCTIPCON = "+cp_ToStrODBC(this.w_PTTIPCON);
              +" and CCCODCON = "+cp_ToStrODBC(this.w_PTCODCON);
              +" and CCCODBAN = "+cp_ToStrODBC(this.w_PTBANAPP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCCONCOR;
          from (i_cTable) where;
              CCTIPCON = this.w_PTTIPCON;
              and CCCODCON = this.w_PTCODCON;
              and CCCODBAN = this.w_PTBANAPP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PTNUMCOR = NVL(cp_ToDate(_read_.CCCONCOR),cp_NullValue(_read_.CCCONCOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_PTBANAPP = IIF(EMPTY(NVL(this.w_PTBANAPP," ")), this.w_CLIBANAPP,this.w_PTBANAPP)
    this.w_PTBANNOS = IIF(EMPTY(NVL(this.w_PTBANNOS," ")), this.w_CLIBANNOS,this.w_PTBANNOS)
    this.w_PT_SEGNO = IIF(this.w_PT_SEGNO="D","A","D")
    if this.pOPER="A"
      * --- Se creo partita di abbuono devo settare segno in funzione dell'importo
      this.w_PT_SEGNO = IIF(this.pTOTIMP<0,"A","D")
    endif
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_VALACC>0 AND this.pOPER="S"
      * --- Creo Partita d'Acconto 
      *     Leggo informazioni dalla testata di Primanota appena inserita
      * --- Read from CONTROPA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTROPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "COSAPAGA"+;
          " from "+i_cTable+" CONTROPA where ";
              +"COCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          COSAPAGA;
          from (i_cTable) where;
              COCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPPAG = NVL(cp_ToDate(_read_.COSAPAGA),cp_NullValue(_read_.COSAPAGA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Seleziono dai pagamenti la prima rata del tipo pagamento associato agli abbuoni e acconti nella tabella contropartite.
      * --- Select from PAG_2AME
      i_nConn=i_TableProp[this.PAG_2AME_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAG_2AME_idx,2],.t.,this.PAG_2AME_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select P2MODPAG  from "+i_cTable+" PAG_2AME ";
            +" where P2CODICE="+cp_ToStrODBC(this.w_TIPPAG)+"";
            +" order by P2NUMRAT";
             ,"_Curs_PAG_2AME")
      else
        select P2MODPAG from (i_cTable);
         where P2CODICE=this.w_TIPPAG;
         order by P2NUMRAT;
          into cursor _Curs_PAG_2AME
      endif
      if used('_Curs_PAG_2AME')
        select _Curs_PAG_2AME
        locate for 1=1
        do while not(eof())
        this.w_MODPAG = _Curs_PAG_2AME.P2MODPAG
          select _Curs_PAG_2AME
          continue
        enddo
        use
      endif
      this.w_PTCODAGE = this.w_CODAGE
      this.w_PTROWNUM = this.w_PTROWNUM+1
      this.w_PTNUMPAR = CANUMPAR("S", this.w_CODESE)
      this.w_PT_SEGNO = IIF(this.w_PTTIPCON="C", "A", "D")
      this.w_PTMODPAG = this.w_MODPAG
      this.w_PTTOTIMP = this.w_VALACC
      this.w_PTFLCRSA = "A"
      this.w_PTCAOAPE = this.pCAOVAL
      this.w_PTNUMDOC = this.w_NUMDOC
      this.w_PTALFDOC = this.w_ALFDOC
      this.w_PTDATDOC = this.w_DATDOC
      this.w_PTDATSCA = this.w_DATSCA
      this.w_PTDATDOC = IIF(EMPTY(this.w_PTDATDOC), this.w_PTDATSCA, this.w_PTDATDOC)
      this.w_PTTOTABB = 0
      this.w_PTIMPDOC = 0
      this.w_PTSERRIF = SPACE(10)
      this.w_PTORDRIF = 0
      this.w_PTNUMRIF = 0
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione Delle Partite
    * --- Insert into PAR_TITE
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTDATSCA"+",PTNUMPAR"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTMODPAG"+",PTFLCRSA"+",PTTIPCON"+",PTCODCON"+",PTCAOAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTFLSOSP"+",PTDATAPE"+",PTBANNOS"+",PTFLRAGG"+",PTFLINDI"+",PTBANAPP"+",PTFLIMPE"+",PTNUMDIS"+",PTDESRIG"+",PTCODAGE"+",PTFLVABD"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTTOTABB"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PTSERIAL),'PAR_TITE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'PAR_TITE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWNUM),'PAR_TITE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PT_SEGNO),'PAR_TITE','PT_SEGNO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_TITE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLCRSA),'PAR_TITE','PTFLCRSA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTIPCON),'PAR_TITE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODCON),'PAR_TITE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOAPE),'PAR_TITE','PTCAOAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDOC),'PAR_TITE','PTNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTALFDOC),'PAR_TITE','PTALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATDOC),'PAR_TITE','PTDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTIMPDOC),'PAR_TITE','PTIMPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLRAGG),'PAR_TITE','PTFLRAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLINDI),'PAR_TITE','PTFLINDI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_TITE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLIMPE),'PAR_TITE','PTFLIMPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDIS),'PAR_TITE','PTNUMDIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDESRIG),'PAR_TITE','PTDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODAGE),'PAR_TITE','PTCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLVABD),'PAR_TITE','PTFLVABD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTSERRIF),'PAR_TITE','PTSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTORDRIF),'PAR_TITE','PTORDRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMRIF),'PAR_TITE','PTNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTABB),'PAR_TITE','PTTOTABB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATREG),'PAR_TITE','PTDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_TITE','PTNUMCOR');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PTSERIAL,'PTROWORD',this.w_PTROWORD,'CPROWNUM',this.w_PTROWNUM,'PTDATSCA',this.w_PTDATSCA,'PTNUMPAR',this.w_PTNUMPAR,'PT_SEGNO',this.w_PT_SEGNO,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PTCODVAL,'PTCAOVAL',this.w_PTCAOVAL,'PTMODPAG',this.w_PTMODPAG,'PTFLCRSA',this.w_PTFLCRSA,'PTTIPCON',this.w_PTTIPCON)
      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTDATSCA,PTNUMPAR,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTMODPAG,PTFLCRSA,PTTIPCON,PTCODCON,PTCAOAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTFLSOSP,PTDATAPE,PTBANNOS,PTFLRAGG,PTFLINDI,PTBANAPP,PTFLIMPE,PTNUMDIS,PTDESRIG,PTCODAGE,PTFLVABD,PTSERRIF,PTORDRIF,PTNUMRIF,PTTOTABB,PTDATREG,PTNUMCOR &i_ccchkf. );
         values (;
           this.w_PTSERIAL;
           ,this.w_PTROWORD;
           ,this.w_PTROWNUM;
           ,this.w_PTDATSCA;
           ,this.w_PTNUMPAR;
           ,this.w_PT_SEGNO;
           ,this.w_PTTOTIMP;
           ,this.w_PTCODVAL;
           ,this.w_PTCAOVAL;
           ,this.w_PTMODPAG;
           ,this.w_PTFLCRSA;
           ,this.w_PTTIPCON;
           ,this.w_PTCODCON;
           ,this.w_PTCAOAPE;
           ,this.w_PTNUMDOC;
           ,this.w_PTALFDOC;
           ,this.w_PTDATDOC;
           ,this.w_PTIMPDOC;
           ,this.w_PTFLSOSP;
           ,this.w_PTDATAPE;
           ,this.w_PTBANNOS;
           ,this.w_PTFLRAGG;
           ,this.w_PTFLINDI;
           ,this.w_PTBANAPP;
           ,this.w_PTFLIMPE;
           ,this.w_PTNUMDIS;
           ,this.w_PTDESRIG;
           ,this.w_PTCODAGE;
           ,this.w_PTFLVABD;
           ,this.w_PTSERRIF;
           ,this.w_PTORDRIF;
           ,this.w_PTNUMRIF;
           ,this.w_PTTOTABB;
           ,this.w_PTDATREG;
           ,this.w_PTNUMCOR;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOPER,pSERRIF,pORDRIF,pNUMRIF,pSERIAL,pROWORD,pROWNUM,pTOTIMP,pTOTABB,pTOTACC,pFLINDI,pFLIMPE,pNUMDIS,pCAOVAL,pCAOAPE,pDATAPE,pDESRIG)
    this.pOPER=pOPER
    this.pSERRIF=pSERRIF
    this.pORDRIF=pORDRIF
    this.pNUMRIF=pNUMRIF
    this.pSERIAL=pSERIAL
    this.pROWORD=pROWORD
    this.pROWNUM=pROWNUM
    this.pTOTIMP=pTOTIMP
    this.pTOTABB=pTOTABB
    this.pTOTACC=pTOTACC
    this.pFLINDI=pFLINDI
    this.pFLIMPE=pFLIMPE
    this.pNUMDIS=pNUMDIS
    this.pCAOVAL=pCAOVAL
    this.pCAOAPE=pCAOAPE
    this.pDATAPE=pDATAPE
    this.pDESRIG=pDESRIG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='PAG_2AME'
    this.cWorkTables[4]='CONTROPA'
    this.cWorkTables[5]='PNT_MAST'
    this.cWorkTables[6]='BAN_CONTI'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_PAG_2AME')
      use in _Curs_PAG_2AME
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,pSERRIF,pORDRIF,pNUMRIF,pSERIAL,pROWORD,pROWNUM,pTOTIMP,pTOTABB,pTOTACC,pFLINDI,pFLIMPE,pNUMDIS,pCAOVAL,pCAOAPE,pDATAPE,pDESRIG"
endproc
