* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bi3                                                        *
*              Registra contab.indiretta                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_103]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-08                                                      *
* Last revis.: 2013-12-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_SERIAL,w_BOTTONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bi3",oParentObject,m.w_SERIAL,m.w_BOTTONE)
return(i_retval)

define class tgscg_bi3 as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_BOTTONE = .NULL.
  w_DATSCA = ctod("  /  /  ")
  w_NUMPAR = space(31)
  w_SEGNO = space(1)
  w_TOTIMP = 0
  w_SIMVAL = space(3)
  w_MODPAG = space(5)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_FLCRSA = space(1)
  w_BANAPP = space(10)
  w_BANNOS = space(15)
  w_TIPPAG = space(2)
  w_DATAPE = ctod("  /  /  ")
  w_DATDOC = ctod("  /  /  ")
  w_CAOVAL = 0
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_IMPDOC = 0
  w_CODPAG = space(5)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_FLRAGG = space(1)
  w_DESRIG = space(50)
  w_CODVAL = space(3)
  w_CAOAPE = 0
  w_FLINDI = 0
  w_NUMPRO = 0
  w_DATREG = ctod("  /  /  ")
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_BANFIL = space(15)
  w_CODAGE = space(5)
  w_FLVABD = space(1)
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_PTFLRAGG = 0
  w_TIPDOC = space(2)
  w_CONTA = 0
  w_PTRIFIND = space(10)
  * --- WorkFile variables
  TMP_PART_IND_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Conferma registrazione Contabilizzazione Indiretta (da GSCG_ACI)
    * --- Questo batch viene richiamato dal bottone di conferma presente sull'anagrafica
    * --- Memorizza l'Anagrafica che l'operatore sta' caricando e rientra in stato di variazione.
    * --- Parametri
    * --- Serial della Registrazione
    * --- Nome del bottone che ha lanciato questo batch
    * --- Variabili
    do case
      case EMPTY(this.oParentObject.w_CICODCAU)
        ah_ErrorMsg("Inserire la causale contabile",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_CICONEFA)
        ah_ErrorMsg("Inserire il conto effetti attivi",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_CICONDCA) OR EMPTY(this.oParentObject.w_CICONDCP)
        ah_ErrorMsg("Inserire i conti differenze cambio",,"")
        i_retcode = 'stop'
        return
      case Not Empty(CHKCONS("P",this.oParentObject.w_CIDATREG,"B","N"))
        * --- Esegue controllo Data Consolidamento
        ah_ErrorMsg("%1",,"",CHKCONS("P",this.oParentObject.w_CIDATREG,"B","N"))
        i_retcode = 'stop'
        return
    endcase
    if Not This.oParentObject.CheckForm()
      i_retcode = 'stop'
      return
    endif
    if This.oParentObject.cFunction="Load"
      * --- Memorizzazione Contabilizzazione indiretta
      *     Alla pressione del tasto ricerca, salvo la contabilizzazione (anagrafica)
      *     per avere il seriale (quindi eseguo la EcpSave)
      this.oParentobject.mCalc(.T.)
      this.oParentobject.ecpSave()
      this.oParentobject.ecpQuery()
      * --- Creazione cursore delle chiavi (w_OLDCISER contiene la chiave calcolata
      *     a seguito dell'F10, Area manuale Insert End)
      this.oParentobject.QueryKeySet( "CISERIAL="+cp_ToStrODBC( this.oParentObject.w_OLDCISER ) , "" )
      * --- Caricamento record ed accesso in modifica
      * --- Valorizzo w_FASE a 1 per non eseguire la query associata allo zoom
      *     Lo zoom sar� riempito dopo l'individuazione delle partite aperte
      this.oParentObject.w_FASE = 1
      this.oParentobject.LoadRecWarn()
      this.oParentObject.w_FASE = 0
      this.oParentobject.ecpEdit()
    endif
    * --- Creo temporaneo per contenere elenco partite aperte per le selezioni
    GSTE_BPA (this, this.oParentObject.w_CISCAINI , this.oParentObject.w_CISCAFIN , this.oParentObject.w_CITIPCLF , this.oParentObject.w_CICODCLF , this.oParentObject.w_CICODCLF,this.oParentObject.w_CICODVAL , "", this.oParentObject.w_CIPAG_RB , this.oParentObject.w_CIPAG_BO , this.oParentObject.w_CIPAG_RD , this.oParentObject.w_CIPAG_RI , this.oParentObject.w_CIPAG_MA , this.oParentObject.w_CIPAG_CA , "","N", cp_CharToDate("  -  -  ") )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    vq_exec("QUERY\GSCG1KCI.VQR",this,"TMP_PART")
    * --- Rimuovo temporaneo
    * --- Drop temporary table TMP_PART_APE
    i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_PART_APE')
    endif
    GSTE_BCP(this,"A", "TMP_PART" ," " )
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Filtro per pagamento ed elimino gli acconti
    *     Non l'usuale Delete per problemi VIsual Fox pro (in alcuni casi non cancellava partite all'interno della selezione)
    Select * From TMP_PART Where ; 
 TIPPAG $ this.oParentObject.w_CIPAG_RB+"-"+this.oParentObject.w_CIPAG_BO+"-"+this.oParentObject.w_CIPAG_RD+"-"+this.oParentObject.w_CIPAG_RI+"-"+this.oParentObject.w_CIPAG_MA+"-"+this.oParentObject.w_CIPAG_CA AND NVL(FLCRSA," ")<>"A" ; 
 Into Cursor Partite NoFilter
    =WrCursor("PARTITE")
     
 Update "PARTITE" Set SEGNO=IIF(TOTIMP<0, "A", "D") ,TOTIMP=IIF(TOTIMP<0, TOTIMP*(-1), TOTIMP) ; 
 Where SEGNO ="X" 
     
 Select PARTITE 
 Go Top
    * --- Per popolare lo zoom da dati interrogabili creo un temporano lato server
    *     riempiendolo con il cursore Fox partite
    *     
    *     1) creo il temporaneo struttura da query vuota...
    * --- Create temporary table TMP_PART_IND
    i_nIdx=cp_AddTableDef('TMP_PART_IND') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSCG3KCI',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMP_PART_IND_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Passo il temporaneo lato client in un temporaneo lato server...
    do while Not Eof( "PARTITE" )
       
 Select Partite
      this.w_DATSCA = CP_TODATE(DATSCA)
      this.w_NUMPAR = NVL(NUMPAR,SPACE(31))
      this.w_SEGNO = NVL(SEGNO," ")
      this.w_TOTIMP = NVL(TOTIMP,0)
      this.w_SIMVAL = NVL(SIMVAL,"   ")
      this.w_MODPAG = NVL(MODPAG,space(5))
      this.w_TIPCON = NVL(TIPCON," ")
      this.w_CODCON = NVL(CODCON,SPACE(15))
      this.w_FLCRSA = NVL(FLCRSA," ")
      this.w_BANAPP = NVL(BANAPP,SPACE(10))
      this.w_BANNOS = NVL(BANNOS,SPACE(15))
      this.w_TIPPAG = NVL(TIPPAG,"  ")
      this.w_DATAPE = CP_TODATE(DATAPE)
      this.w_DATDOC = CP_TODATE(DATDOC)
      this.w_CAOVAL = NVL(CAOVAL,0)
      this.w_NUMDOC = NVL(NUMDOC,0)
      this.w_ALFDOC = NVL(ALFDOC,"  ")
      this.w_IMPDOC = NVL(IMPDOC,0)
      this.w_CODPAG = NVL(CODPAG,SPACE(5))
      this.w_PTSERIAL = Nvl( PTSERIAL , Space(10))
      this.w_PTROWORD = Nvl( PTROWORD , 0 )
      this.w_CPROWNUM = Nvl(CPROWNUM,0)
      this.w_FLRAGG = NVL(FLRAGG," ")
      this.w_DESRIG = NVL(DESRIG,SPACE(50))
      this.w_CODVAL = NVL(CODVAL,SPACE(3))
      this.w_CAOAPE = NVL(CAOAPE,0)
      this.w_FLINDI = NVL(FLINDI,0)
      this.w_NUMPRO = NVL(NUMPRO,0)
      this.w_DATREG = CP_TODATE(DATREG)
      this.w_IMPDAR = NVL(IMPDAR,0)
      this.w_IMPAVE = NVL(IMPAVE,0)
      this.w_BANFIL = NVL(BANFIL,SPACE(15))
      this.w_CODAGE = NVL(CODAGE,SPACE(5))
      this.w_FLVABD = NVL(FLVABD," ")
      this.w_PTSERRIF = NVL(PTSERRIF,SPACE(10))
      this.w_PTORDRIF = NVL(PTORDRIF,0)
      this.w_PTNUMRIF = NVL(PTNUMRIF,0)
      this.w_PTFLRAGG = NVL(PTFLRAGG,0)
      this.w_TIPDOC = NVL(TIPDOC,"  ")
      this.w_CONTA = NVL(CONTA,0)
      this.w_PTRIFIND = NVL(PTRIFIND,SPACE(10))
      * --- Insert into TMP_PART_IND
      i_nConn=i_TableProp[this.TMP_PART_IND_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP_PART_IND_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_PART_IND_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DATSCA"+",NUMPAR"+",SEGNO"+",TOTIMP"+",SIMVAL"+",MODPAG"+",TIPCON"+",CODCON"+",FLCRSA"+",BANAPP"+",BANNOS"+",TIPPAG"+",DATAPE"+",DATDOC"+",CAOVAL"+",NUMDOC"+",ALFDOC"+",IMPDOC"+",CODPAG"+",PTSERIAL"+",PTROWORD"+",CPROWNUM"+",FLRAGG"+",DESRIG"+",CODVAL"+",CAOAPE"+",FLINDI"+",NUMPRO"+",DATREG"+",IMPDAR"+",IMPAVE"+",BANFIL"+",CODAGE"+",FLVABD"+",PTSERRIF"+",PTORDRIF"+",PTNUMRIF"+",PTFLRAGG"+",TIPDOC"+",CONTA"+",PTRIFIND"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_DATSCA),'TMP_PART_IND','DATSCA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMPAR),'TMP_PART_IND','NUMPAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SEGNO),'TMP_PART_IND','SEGNO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TOTIMP),'TMP_PART_IND','TOTIMP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SIMVAL),'TMP_PART_IND','SIMVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_MODPAG),'TMP_PART_IND','MODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'TMP_PART_IND','TIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'TMP_PART_IND','CODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLCRSA),'TMP_PART_IND','FLCRSA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_BANAPP),'TMP_PART_IND','BANAPP');
        +","+cp_NullLink(cp_ToStrODBC(this.w_BANNOS),'TMP_PART_IND','BANNOS');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TIPPAG),'TMP_PART_IND','TIPPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DATAPE),'TMP_PART_IND','DATAPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DATDOC),'TMP_PART_IND','DATDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAOVAL),'TMP_PART_IND','CAOVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDOC),'TMP_PART_IND','NUMDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ALFDOC),'TMP_PART_IND','ALFDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDOC),'TMP_PART_IND','IMPDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODPAG),'TMP_PART_IND','CODPAG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTSERIAL),'TMP_PART_IND','PTSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTROWORD),'TMP_PART_IND','PTROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'TMP_PART_IND','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLRAGG),'TMP_PART_IND','FLRAGG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DESRIG),'TMP_PART_IND','DESRIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'TMP_PART_IND','CODVAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CAOAPE),'TMP_PART_IND','CAOAPE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLINDI),'TMP_PART_IND','FLINDI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMPRO),'TMP_PART_IND','NUMPRO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'TMP_PART_IND','DATREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPDAR),'TMP_PART_IND','IMPDAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPAVE),'TMP_PART_IND','IMPAVE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_BANFIL),'TMP_PART_IND','BANFIL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'TMP_PART_IND','CODAGE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FLVABD),'TMP_PART_IND','FLVABD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTSERRIF),'TMP_PART_IND','PTSERRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTORDRIF),'TMP_PART_IND','PTORDRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMRIF),'TMP_PART_IND','PTNUMRIF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLRAGG),'TMP_PART_IND','PTFLRAGG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_TIPDOC),'TMP_PART_IND','TIPDOC');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CONTA),'TMP_PART_IND','CONTA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PTRIFIND),'TMP_PART_IND','PTRIFIND');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DATSCA',this.w_DATSCA,'NUMPAR',this.w_NUMPAR,'SEGNO',this.w_SEGNO,'TOTIMP',this.w_TOTIMP,'SIMVAL',this.w_SIMVAL,'MODPAG',this.w_MODPAG,'TIPCON',this.w_TIPCON,'CODCON',this.w_CODCON,'FLCRSA',this.w_FLCRSA,'BANAPP',this.w_BANAPP,'BANNOS',this.w_BANNOS,'TIPPAG',this.w_TIPPAG)
        insert into (i_cTable) (DATSCA,NUMPAR,SEGNO,TOTIMP,SIMVAL,MODPAG,TIPCON,CODCON,FLCRSA,BANAPP,BANNOS,TIPPAG,DATAPE,DATDOC,CAOVAL,NUMDOC,ALFDOC,IMPDOC,CODPAG,PTSERIAL,PTROWORD,CPROWNUM,FLRAGG,DESRIG,CODVAL,CAOAPE,FLINDI,NUMPRO,DATREG,IMPDAR,IMPAVE,BANFIL,CODAGE,FLVABD,PTSERRIF,PTORDRIF,PTNUMRIF,PTFLRAGG,TIPDOC,CONTA,PTRIFIND &i_ccchkf. );
           values (;
             this.w_DATSCA;
             ,this.w_NUMPAR;
             ,this.w_SEGNO;
             ,this.w_TOTIMP;
             ,this.w_SIMVAL;
             ,this.w_MODPAG;
             ,this.w_TIPCON;
             ,this.w_CODCON;
             ,this.w_FLCRSA;
             ,this.w_BANAPP;
             ,this.w_BANNOS;
             ,this.w_TIPPAG;
             ,this.w_DATAPE;
             ,this.w_DATDOC;
             ,this.w_CAOVAL;
             ,this.w_NUMDOC;
             ,this.w_ALFDOC;
             ,this.w_IMPDOC;
             ,this.w_CODPAG;
             ,this.w_PTSERIAL;
             ,this.w_PTROWORD;
             ,this.w_CPROWNUM;
             ,this.w_FLRAGG;
             ,this.w_DESRIG;
             ,this.w_CODVAL;
             ,this.w_CAOAPE;
             ,this.w_FLINDI;
             ,this.w_NUMPRO;
             ,this.w_DATREG;
             ,this.w_IMPDAR;
             ,this.w_IMPAVE;
             ,this.w_BANFIL;
             ,this.w_CODAGE;
             ,this.w_FLVABD;
             ,this.w_PTSERRIF;
             ,this.w_PTORDRIF;
             ,this.w_PTNUMRIF;
             ,this.w_PTFLRAGG;
             ,this.w_TIPDOC;
             ,this.w_CONTA;
             ,this.w_PTRIFIND;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      if Not Eof( "PARTITE" )
         
 Select Partite 
 Skip
      endif
    enddo
    * --- Nell'area manuale Notify Event � gestito il cambio di query legata allo zoom...
    this.oParentObject.w_NAME_ZOOM = "Query\GSCG4KCI"
    this.oParentobject.NotifyEvent("Legge")
    this.oParentObject.w_NAME_ZOOM = "Query\GSCG_KCI"
    if used ("PARTITE")
       
 Select PARTITE 
 Use
    endif
    if used ("TMP_PART")
       
 Select TMP_PART 
 Use
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_SERIAL,w_BOTTONE)
    this.w_SERIAL=w_SERIAL
    this.w_BOTTONE=w_BOTTONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*TMP_PART_IND'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_SERIAL,w_BOTTONE"
endproc
