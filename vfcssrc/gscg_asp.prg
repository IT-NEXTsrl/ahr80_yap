* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_asp                                                        *
*              Saldi conti                                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [116] [VRS_24]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2011-05-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_asp"))

* --- Class definition
define class tgscg_asp as StdForm
  Top    = 86
  Left   = 35

  * --- Standard Properties
  Width  = 528
  Height = 315+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-09"
  HelpContextID=158718615
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=25

  * --- Constant Properties
  SALDICON_IDX = 0
  CONTI_IDX = 0
  ESERCIZI_IDX = 0
  cFile = "SALDICON"
  cKeySelect = "SLTIPCON,SLCODICE,SLCODESE"
  cKeyWhere  = "SLTIPCON=this.w_SLTIPCON and SLCODICE=this.w_SLCODICE and SLCODESE=this.w_SLCODESE"
  cKeyWhereODBC = '"SLTIPCON="+cp_ToStrODBC(this.w_SLTIPCON)';
      +'+" and SLCODICE="+cp_ToStrODBC(this.w_SLCODICE)';
      +'+" and SLCODESE="+cp_ToStrODBC(this.w_SLCODESE)';

  cKeyWhereODBCqualified = '"SALDICON.SLTIPCON="+cp_ToStrODBC(this.w_SLTIPCON)';
      +'+" and SALDICON.SLCODICE="+cp_ToStrODBC(this.w_SLCODICE)';
      +'+" and SALDICON.SLCODESE="+cp_ToStrODBC(this.w_SLCODESE)';

  cPrg = "gscg_asp"
  cComment = "Saldi conti"
  icon = "anag.ico"
  cAutoZoom = 'GSCG0ASP'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  w_TIPOPE = space(4)
  w_SLTIPCON = space(1)
  w_SLCODICE = space(15)
  w_SLCODESE = space(4)
  w_DESCON = space(40)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_SLDARINI = 0
  w_SLAVEINI = 0
  w_SLDARINF = 0
  w_SLAVEINF = 0
  w_SLDARFIN = 0
  w_SLAVEFIN = 0
  w_SLDARFIF = 0
  w_SLAVEFIF = 0
  w_SLDARPRO = 0
  w_SLAVEPRO = 0
  w_SLDARPER = 0
  w_SLAVEPER = 0
  w_SALDO = 0
  w_VSALDO = 0
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CONTROL = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SALDICON','gscg_asp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_aspPag1","gscg_asp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Saldi conti")
      .Pages(1).HelpContextID = 260567947
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSLCODICE_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='SALDICON'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SALDICON_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SALDICON_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SLTIPCON = NVL(SLTIPCON,space(1))
      .w_SLCODICE = NVL(SLCODICE,space(15))
      .w_SLCODESE = NVL(SLCODESE,space(4))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SALDICON where SLTIPCON=KeySet.SLTIPCON
    *                            and SLCODICE=KeySet.SLCODICE
    *                            and SLCODESE=KeySet.SLCODESE
    *
    i_nConn = i_TableProp[this.SALDICON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDICON_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SALDICON')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SALDICON.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SALDICON '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SLTIPCON',this.w_SLTIPCON  ,'SLCODICE',this.w_SLCODICE  ,'SLCODESE',this.w_SLCODESE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCON = space(40)
        .w_INIESE = ctod("  /  /  ")
        .w_FINESE = ctod("  /  /  ")
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_CONTROL = 'N'
        .w_CODAZI = i_CODAZI
        .w_TIPOPE = this.cFunction
        .w_SLTIPCON = NVL(SLTIPCON,space(1))
        .w_SLCODICE = NVL(SLCODICE,space(15))
          if link_1_4_joined
            this.w_SLCODICE = NVL(ANCODICE104,NVL(this.w_SLCODICE,space(15)))
            this.w_DESCON = NVL(ANDESCRI104,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO104),ctod("  /  /  "))
          else
          .link_1_4('Load')
          endif
        .w_SLCODESE = NVL(SLCODESE,space(4))
          .link_1_5('Load')
        .w_SLDARINI = NVL(SLDARINI,0)
        .w_SLAVEINI = NVL(SLAVEINI,0)
        .w_SLDARINF = NVL(SLDARINF,0)
        .w_SLAVEINF = NVL(SLAVEINF,0)
        .w_SLDARFIN = NVL(SLDARFIN,0)
        .w_SLAVEFIN = NVL(SLAVEFIN,0)
        .w_SLDARFIF = NVL(SLDARFIF,0)
        .w_SLAVEFIF = NVL(SLAVEFIF,0)
        .w_SLDARPRO = NVL(SLDARPRO,0)
        .w_SLAVEPRO = NVL(SLAVEPRO,0)
        .w_SLDARPER = NVL(SLDARPER,0)
        .w_SLAVEPER = NVL(SLAVEPER,0)
        .w_SALDO = (.w_SLDARPRO+.w_SLDARPER+.w_SLDARFIN)-(.w_SLAVEPRO+.w_SLAVEPER+.w_SLAVEFIN)
        .w_VSALDO = ABS(.w_SALDO)
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(IIF((.w_CONTROL='S' OR .w_SLDARFIN<>0 OR .w_SLAVEFIN<>0),IIF(.w_SALDO>=0, cp_translate("Saldo DARE prima della chiusura:"),cp_translate("Saldo AVERE prima della chiusura:")),IIF(.w_SALDO >=0, cp_translate("Saldo DARE Attuale:"),cp_translate("Saldo AVERE Attuale:"))),'','')
        cp_LoadRecExtFlds(this,'SALDICON')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gscg_asp
    if this.w_SLTIPCON <> 'G'
      this.BlankRec()
    endif
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(5)
      .w_TIPOPE = space(4)
      .w_SLTIPCON = space(1)
      .w_SLCODICE = space(15)
      .w_SLCODESE = space(4)
      .w_DESCON = space(40)
      .w_INIESE = ctod("  /  /  ")
      .w_FINESE = ctod("  /  /  ")
      .w_SLDARINI = 0
      .w_SLAVEINI = 0
      .w_SLDARINF = 0
      .w_SLAVEINF = 0
      .w_SLDARFIN = 0
      .w_SLAVEFIN = 0
      .w_SLDARFIF = 0
      .w_SLAVEFIF = 0
      .w_SLDARPRO = 0
      .w_SLAVEPRO = 0
      .w_SLDARPER = 0
      .w_SLAVEPER = 0
      .w_SALDO = 0
      .w_VSALDO = 0
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_CONTROL = space(1)
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .w_TIPOPE = this.cFunction
        .w_SLTIPCON = 'G'
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_SLCODICE))
          .link_1_4('Full')
          endif
        .w_SLCODESE = g_CODESE
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_SLCODESE))
          .link_1_5('Full')
          endif
          .DoRTCalc(6,20,.f.)
        .w_SALDO = (.w_SLDARPRO+.w_SLDARPER+.w_SLDARFIN)-(.w_SLAVEPRO+.w_SLAVEPER+.w_SLAVEFIN)
        .w_VSALDO = ABS(.w_SALDO)
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
          .DoRTCalc(24,24,.f.)
        .w_CONTROL = 'N'
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(IIF((.w_CONTROL='S' OR .w_SLDARFIN<>0 OR .w_SLAVEFIN<>0),IIF(.w_SALDO>=0, cp_translate("Saldo DARE prima della chiusura:"),cp_translate("Saldo AVERE prima della chiusura:")),IIF(.w_SALDO >=0, cp_translate("Saldo DARE Attuale:"),cp_translate("Saldo AVERE Attuale:"))),'','')
      endif
    endwith
    cp_BlankRecExtFlds(this,'SALDICON')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSLCODICE_1_4.enabled = i_bVal
      .Page1.oPag.oSLCODESE_1_5.enabled = i_bVal
      .Page1.oPag.oSLDARINI_1_17.enabled = i_bVal
      .Page1.oPag.oSLAVEINI_1_18.enabled = i_bVal
      .Page1.oPag.oSLDARINF_1_19.enabled = i_bVal
      .Page1.oPag.oSLAVEINF_1_20.enabled = i_bVal
      .Page1.oPag.oSLDARFIN_1_21.enabled = i_bVal
      .Page1.oPag.oSLAVEFIN_1_22.enabled = i_bVal
      .Page1.oPag.oSLDARFIF_1_23.enabled = i_bVal
      .Page1.oPag.oSLAVEFIF_1_24.enabled = i_bVal
      .Page1.oPag.oSLDARPRO_1_25.enabled = i_bVal
      .Page1.oPag.oSLAVEPRO_1_26.enabled = i_bVal
      .Page1.oPag.oSLDARPER_1_27.enabled = i_bVal
      .Page1.oPag.oSLAVEPER_1_28.enabled = i_bVal
      .Page1.oPag.oObj_1_40.enabled = i_bVal
      .Page1.oPag.oObj_1_41.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSLCODICE_1_4.enabled = .f.
        .Page1.oPag.oSLCODESE_1_5.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSLCODICE_1_4.enabled = .t.
        .Page1.oPag.oSLCODESE_1_5.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SALDICON',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SALDICON_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLTIPCON,"SLTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLCODICE,"SLCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLCODESE,"SLCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLDARINI,"SLDARINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLAVEINI,"SLAVEINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLDARINF,"SLDARINF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLAVEINF,"SLAVEINF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLDARFIN,"SLDARFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLAVEFIN,"SLAVEFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLDARFIF,"SLDARFIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLAVEFIF,"SLAVEFIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLDARPRO,"SLDARPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLAVEPRO,"SLAVEPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLDARPER,"SLDARPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SLAVEPER,"SLAVEPER",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SALDICON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDICON_IDX,2])
    i_lTable = "SALDICON"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SALDICON_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCG_SSO with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SALDICON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALDICON_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SALDICON
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SALDICON')
        i_extval=cp_InsertValODBCExtFlds(this,'SALDICON')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SLTIPCON,SLCODICE,SLCODESE,SLDARINI,SLAVEINI"+;
                  ",SLDARINF,SLAVEINF,SLDARFIN,SLAVEFIN,SLDARFIF"+;
                  ",SLAVEFIF,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_SLTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_SLCODICE)+;
                  ","+cp_ToStrODBCNull(this.w_SLCODESE)+;
                  ","+cp_ToStrODBC(this.w_SLDARINI)+;
                  ","+cp_ToStrODBC(this.w_SLAVEINI)+;
                  ","+cp_ToStrODBC(this.w_SLDARINF)+;
                  ","+cp_ToStrODBC(this.w_SLAVEINF)+;
                  ","+cp_ToStrODBC(this.w_SLDARFIN)+;
                  ","+cp_ToStrODBC(this.w_SLAVEFIN)+;
                  ","+cp_ToStrODBC(this.w_SLDARFIF)+;
                  ","+cp_ToStrODBC(this.w_SLAVEFIF)+;
                  ","+cp_ToStrODBC(this.w_SLDARPRO)+;
                  ","+cp_ToStrODBC(this.w_SLAVEPRO)+;
                  ","+cp_ToStrODBC(this.w_SLDARPER)+;
                  ","+cp_ToStrODBC(this.w_SLAVEPER)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SALDICON')
        i_extval=cp_InsertValVFPExtFlds(this,'SALDICON')
        cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_SLTIPCON,'SLCODICE',this.w_SLCODICE,'SLCODESE',this.w_SLCODESE)
        INSERT INTO (i_cTable);
              (SLTIPCON,SLCODICE,SLCODESE,SLDARINI,SLAVEINI,SLDARINF,SLAVEINF,SLDARFIN,SLAVEFIN,SLDARFIF,SLAVEFIF,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SLTIPCON;
                  ,this.w_SLCODICE;
                  ,this.w_SLCODESE;
                  ,this.w_SLDARINI;
                  ,this.w_SLAVEINI;
                  ,this.w_SLDARINF;
                  ,this.w_SLAVEINF;
                  ,this.w_SLDARFIN;
                  ,this.w_SLAVEFIN;
                  ,this.w_SLDARFIF;
                  ,this.w_SLAVEFIF;
                  ,this.w_SLDARPRO;
                  ,this.w_SLAVEPRO;
                  ,this.w_SLDARPER;
                  ,this.w_SLAVEPER;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SALDICON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALDICON_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SALDICON_IDX,i_nConn)
      *
      * update SALDICON
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SALDICON')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SLDARINI="+cp_ToStrODBC(this.w_SLDARINI)+;
             ",SLAVEINI="+cp_ToStrODBC(this.w_SLAVEINI)+;
             ",SLDARINF="+cp_ToStrODBC(this.w_SLDARINF)+;
             ",SLAVEINF="+cp_ToStrODBC(this.w_SLAVEINF)+;
             ",SLDARFIN="+cp_ToStrODBC(this.w_SLDARFIN)+;
             ",SLAVEFIN="+cp_ToStrODBC(this.w_SLAVEFIN)+;
             ",SLDARFIF="+cp_ToStrODBC(this.w_SLDARFIF)+;
             ",SLAVEFIF="+cp_ToStrODBC(this.w_SLAVEFIF)+;
             ",SLDARPRO="+cp_ToStrODBC(this.w_SLDARPRO)+;
             ",SLAVEPRO="+cp_ToStrODBC(this.w_SLAVEPRO)+;
             ",SLDARPER="+cp_ToStrODBC(this.w_SLDARPER)+;
             ",SLAVEPER="+cp_ToStrODBC(this.w_SLAVEPER)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SALDICON')
        i_cWhere = cp_PKFox(i_cTable  ,'SLTIPCON',this.w_SLTIPCON  ,'SLCODICE',this.w_SLCODICE  ,'SLCODESE',this.w_SLCODESE  )
        UPDATE (i_cTable) SET;
              SLDARINI=this.w_SLDARINI;
             ,SLAVEINI=this.w_SLAVEINI;
             ,SLDARINF=this.w_SLDARINF;
             ,SLAVEINF=this.w_SLAVEINF;
             ,SLDARFIN=this.w_SLDARFIN;
             ,SLAVEFIN=this.w_SLAVEFIN;
             ,SLDARFIF=this.w_SLDARFIF;
             ,SLAVEFIF=this.w_SLAVEFIF;
             ,SLDARPRO=this.w_SLDARPRO;
             ,SLAVEPRO=this.w_SLAVEPRO;
             ,SLDARPER=this.w_SLDARPER;
             ,SLAVEPER=this.w_SLAVEPER;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SALDICON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALDICON_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SALDICON_IDX,i_nConn)
      *
      * delete SALDICON
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SLTIPCON',this.w_SLTIPCON  ,'SLCODICE',this.w_SLCODICE  ,'SLCODESE',this.w_SLCODESE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SALDICON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDICON_IDX,2])
    if i_bUpd
      with this
            .w_CODAZI = i_CODAZI
            .w_TIPOPE = this.cFunction
        .DoRTCalc(3,20,.t.)
            .w_SALDO = (.w_SLDARPRO+.w_SLDARPER+.w_SLDARFIN)-(.w_SLAVEPRO+.w_SLAVEPER+.w_SLAVEFIN)
            .w_VSALDO = ABS(.w_SALDO)
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(IIF((.w_CONTROL='S' OR .w_SLDARFIN<>0 OR .w_SLAVEFIN<>0),IIF(.w_SALDO>=0, cp_translate("Saldo DARE prima della chiusura:"),cp_translate("Saldo AVERE prima della chiusura:")),IIF(.w_SALDO >=0, cp_translate("Saldo DARE Attuale:"),cp_translate("Saldo AVERE Attuale:"))),'','')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(23,25,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(IIF((.w_CONTROL='S' OR .w_SLDARFIN<>0 OR .w_SLAVEFIN<>0),IIF(.w_SALDO>=0, cp_translate("Saldo DARE prima della chiusura:"),cp_translate("Saldo AVERE prima della chiusura:")),IIF(.w_SALDO >=0, cp_translate("Saldo DARE Attuale:"),cp_translate("Saldo AVERE Attuale:"))),'','')
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SLCODICE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SLCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_SLCODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SLTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_SLTIPCON;
                     ,'ANCODICE',trim(this.w_SLCODICE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SLCODICE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_SLCODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SLTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_SLCODICE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_SLTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SLCODICE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oSLCODICE_1_4'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_SLTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_SLTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SLCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_SLCODICE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_SLTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_SLTIPCON;
                       ,'ANCODICE',this.w_SLCODICE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SLCODICE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SLCODICE = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_SLCODICE = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SLCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.ANCODICE as ANCODICE104"+ ",link_1_4.ANDESCRI as ANDESCRI104"+ ",link_1_4.ANDTOBSO as ANDTOBSO104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on SALDICON.SLCODICE=link_1_4.ANCODICE"+" and SALDICON.SLTIPCON=link_1_4.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and SALDICON.SLCODICE=link_1_4.ANCODICE(+)"'+'+" and SALDICON.SLTIPCON=link_1_4.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SLCODESE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SLCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_SLCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_SLCODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SLCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SLCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oSLCODESE_1_5'),i_cWhere,'GSAR_KES',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Saldo gi� inserito o conto/esercizio inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SLCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_SLCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_SLCODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SLCODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SLCODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF(UPPER(.w_TIPOPE)='LOAD', CHKSALC(.w_SLTIPCON,.w_SLCODICE, .w_SLCODESE), .T.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Saldo gi� inserito o conto/esercizio inesistente")
        endif
        this.w_SLCODESE = space(4)
        this.w_INIESE = ctod("  /  /  ")
        this.w_FINESE = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SLCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSLCODICE_1_4.value==this.w_SLCODICE)
      this.oPgFrm.Page1.oPag.oSLCODICE_1_4.value=this.w_SLCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oSLCODESE_1_5.value==this.w_SLCODESE)
      this.oPgFrm.Page1.oPag.oSLCODESE_1_5.value=this.w_SLCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_6.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_6.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oINIESE_1_7.value==this.w_INIESE)
      this.oPgFrm.Page1.oPag.oINIESE_1_7.value=this.w_INIESE
    endif
    if not(this.oPgFrm.Page1.oPag.oFINESE_1_8.value==this.w_FINESE)
      this.oPgFrm.Page1.oPag.oFINESE_1_8.value=this.w_FINESE
    endif
    if not(this.oPgFrm.Page1.oPag.oSLDARINI_1_17.value==this.w_SLDARINI)
      this.oPgFrm.Page1.oPag.oSLDARINI_1_17.value=this.w_SLDARINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSLAVEINI_1_18.value==this.w_SLAVEINI)
      this.oPgFrm.Page1.oPag.oSLAVEINI_1_18.value=this.w_SLAVEINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSLDARINF_1_19.value==this.w_SLDARINF)
      this.oPgFrm.Page1.oPag.oSLDARINF_1_19.value=this.w_SLDARINF
    endif
    if not(this.oPgFrm.Page1.oPag.oSLAVEINF_1_20.value==this.w_SLAVEINF)
      this.oPgFrm.Page1.oPag.oSLAVEINF_1_20.value=this.w_SLAVEINF
    endif
    if not(this.oPgFrm.Page1.oPag.oSLDARFIN_1_21.value==this.w_SLDARFIN)
      this.oPgFrm.Page1.oPag.oSLDARFIN_1_21.value=this.w_SLDARFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSLAVEFIN_1_22.value==this.w_SLAVEFIN)
      this.oPgFrm.Page1.oPag.oSLAVEFIN_1_22.value=this.w_SLAVEFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSLDARFIF_1_23.value==this.w_SLDARFIF)
      this.oPgFrm.Page1.oPag.oSLDARFIF_1_23.value=this.w_SLDARFIF
    endif
    if not(this.oPgFrm.Page1.oPag.oSLAVEFIF_1_24.value==this.w_SLAVEFIF)
      this.oPgFrm.Page1.oPag.oSLAVEFIF_1_24.value=this.w_SLAVEFIF
    endif
    if not(this.oPgFrm.Page1.oPag.oSLDARPRO_1_25.value==this.w_SLDARPRO)
      this.oPgFrm.Page1.oPag.oSLDARPRO_1_25.value=this.w_SLDARPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oSLAVEPRO_1_26.value==this.w_SLAVEPRO)
      this.oPgFrm.Page1.oPag.oSLAVEPRO_1_26.value=this.w_SLAVEPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oSLDARPER_1_27.value==this.w_SLDARPER)
      this.oPgFrm.Page1.oPag.oSLDARPER_1_27.value=this.w_SLDARPER
    endif
    if not(this.oPgFrm.Page1.oPag.oSLAVEPER_1_28.value==this.w_SLAVEPER)
      this.oPgFrm.Page1.oPag.oSLAVEPER_1_28.value=this.w_SLAVEPER
    endif
    if not(this.oPgFrm.Page1.oPag.oVSALDO_1_30.value==this.w_VSALDO)
      this.oPgFrm.Page1.oPag.oVSALDO_1_30.value=this.w_VSALDO
    endif
    cp_SetControlsValueExtFlds(this,'SALDICON')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_SLCODICE)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSLCODICE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_SLCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   ((empty(.w_SLCODESE)) or not(IIF(UPPER(.w_TIPOPE)='LOAD', CHKSALC(.w_SLTIPCON,.w_SLCODICE, .w_SLCODESE), .T.)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSLCODESE_1_5.SetFocus()
            i_bnoObbl = !empty(.w_SLCODESE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Saldo gi� inserito o conto/esercizio inesistente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_aspPag1 as StdContainer
  Width  = 524
  height = 315
  stdWidth  = 524
  stdheight = 315
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSLCODICE_1_4 as StdField with uid="LZVESDWVIN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SLCODICE", cQueryName = "SLTIPCON,SLCODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Codice conto",;
    HelpContextID = 150381461,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=89, Top=8, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_SLTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_SLCODICE"

  func oSLCODICE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSLCODICE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSLCODICE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_SLTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_SLTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oSLCODICE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oSLCODICE_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_SLTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_SLCODICE
     i_obj.ecpSave()
  endproc

  add object oSLCODESE_1_5 as StdField with uid="AUXXZONIYL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SLCODESE", cQueryName = "SLTIPCON,SLCODICE,SLCODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Saldo gi� inserito o conto/esercizio inesistente",;
    ToolTipText = "Codice esercizio",;
    HelpContextID = 217490325,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=341, Top=8, InputMask=replicate('X',4), cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_SLCODESE"

  func oSLCODESE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSLCODESE_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oDESCON_1_6 as StdField with uid="FXYQBEMANX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 55684042,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=89, Top=33, InputMask=replicate('X',40)

  add object oINIESE_1_7 as StdField with uid="MQMEIKKYPN",rtseq=7,rtrep=.f.,;
    cFormVar = "w_INIESE", cQueryName = "INIESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 202392186,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=440, Top=8

  add object oFINESE_1_8 as StdField with uid="HFOEEFPPVT",rtseq=8,rtrep=.f.,;
    cFormVar = "w_FINESE", cQueryName = "FINESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 202373034,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=440, Top=33

  add object oSLDARINI_1_17 as StdField with uid="JWEGGECTWG",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SLDARINI", cQueryName = "SLDARINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo dare iniziale all'apertura dei conti patrimoniali",;
    HelpContextID = 136614801,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=141, Top=118, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oSLAVEINI_1_18 as StdField with uid="TKHYEOVXBI",rtseq=10,rtrep=.f.,;
    cFormVar = "w_SLAVEINI", cQueryName = "SLAVEINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo avere iniziale all'apertura dei conti patrimoniali",;
    HelpContextID = 148882321,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=363, Top=118, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oSLDARINF_1_19 as StdField with uid="OHCXECCZSF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_SLDARINF", cQueryName = "SLDARINF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo dare iniziale all'apertura dei conti patrimoniali fuori linea",;
    HelpContextID = 136614804,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=141, Top=142, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oSLAVEINF_1_20 as StdField with uid="TTCVFVICRW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SLAVEINF", cQueryName = "SLAVEINF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo dare iniziale all'apertura dei conti patrimoniali fuori linea",;
    HelpContextID = 148882324,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=363, Top=142, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oSLDARFIN_1_21 as StdField with uid="GLKFNDUOLG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SLDARFIN", cQueryName = "SLDARFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo dare finale alla chiusura esercizio",;
    HelpContextID = 81489012,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=141, Top=166, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oSLAVEFIN_1_22 as StdField with uid="DWNQUOAQQZ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_SLAVEFIN", cQueryName = "SLAVEFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo avere finale alla chiusura esercizio",;
    HelpContextID = 69221492,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=363, Top=166, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oSLDARFIF_1_23 as StdField with uid="EMSAIJTTED",rtseq=15,rtrep=.f.,;
    cFormVar = "w_SLDARFIF", cQueryName = "SLDARFIF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo dare finale alla chiusura esercizio fuori linea",;
    HelpContextID = 81489004,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=141, Top=190, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oSLAVEFIF_1_24 as StdField with uid="IJHOFONXHM",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SLAVEFIF", cQueryName = "SLAVEFIF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo dare iniziale all'apertura dei conti patrimoniali fuori linea",;
    HelpContextID = 69221484,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=363, Top=190, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oSLDARPRO_1_25 as StdField with uid="WIYYIBXWUO",rtseq=17,rtrep=.f.,;
    cFormVar = "w_SLDARPRO", cQueryName = "SLDARPRO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo dare dei movimenti passati a storico",;
    HelpContextID = 19174283,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=141, Top=229, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oSLAVEPRO_1_26 as StdField with uid="YDVGXFNXBY",rtseq=18,rtrep=.f.,;
    cFormVar = "w_SLAVEPRO", cQueryName = "SLAVEPRO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo avere dei movimenti passati a storico",;
    HelpContextID = 31441803,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=363, Top=229, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oSLDARPER_1_27 as StdField with uid="KDPQFGVWEZ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_SLDARPER", cQueryName = "SLDARPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo dare dei movimenti in linea",;
    HelpContextID = 249261176,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=141, Top=253, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oSLAVEPER_1_28 as StdField with uid="MNYPSRDDDD",rtseq=20,rtrep=.f.,;
    cFormVar = "w_SLAVEPER", cQueryName = "SLAVEPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo avere dei movimenti in linea",;
    HelpContextID = 236993656,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=363, Top=253, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oVSALDO_1_30 as StdField with uid="XNZUQBFJKT",rtseq=22,rtrep=.f.,;
    cFormVar = "w_VSALDO", cQueryName = "VSALDO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 49921194,;
    FontName = "Tahoma", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=156, Left=363, Top=292, cSayPict="v_PV(20)", cGetPict="v_GV(20)"


  add object oObj_1_40 as cp_runprogram with uid="DYVXYHCWRQ",left=-5, top=355, width=157,height=19,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSCG_BCO',;
    cEvent = "Delete start",;
    nPag=1;
    , ToolTipText = "Avviso se cancello un conto con saldo";
    , HelpContextID = 200154650


  add object oObj_1_41 as cp_runprogram with uid="CNLWTIIEBI",left=-5, top=373, width=157,height=19,;
    caption='GSCG_BFC',;
   bGlobalFont=.t.,;
    prg="GSCG_BFC",;
    cEvent = "Load",;
    nPag=1;
    , ToolTipText = "Controlla se l'esercizio � chiuso";
    , HelpContextID = 28402345


  add object oObj_1_43 as cp_calclbl with uid="FLKMLDYDMC",left=158, top=291, width=193,height=16,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 200154650

  add object oStr_1_9 as StdString with uid="GIKOHHSAIM",Visible=.t., Left=139, Top=88,;
    Alignment=2, Width=180, Height=15,;
    Caption="DARE"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="TZFVPLLIET",Visible=.t., Left=332, Top=88,;
    Alignment=2, Width=178, Height=15,;
    Caption="AVERE"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="UEOTWNQDAC",Visible=.t., Left=4, Top=253,;
    Alignment=1, Width=134, Height=15,;
    Caption="Movimenti in linea:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="UDCXXQVJNM",Visible=.t., Left=4, Top=229,;
    Alignment=1, Width=134, Height=15,;
    Caption="Movimenti fuori linea:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="GOVTQMVRYC",Visible=.t., Left=52, Top=168,;
    Alignment=1, Width=85, Height=15,;
    Caption="Saldo finale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="BRGRUBIZWO",Visible=.t., Left=55, Top=118,;
    Alignment=1, Width=83, Height=15,;
    Caption="Saldo iniziale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="JKOSCLYMFQ",Visible=.t., Left=3, Top=8,;
    Alignment=1, Width=83, Height=15,;
    Caption="Cod.conto:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_31 as StdString with uid="DYAJEYQVVS",Visible=.t., Left=228, Top=8,;
    Alignment=1, Width=111, Height=15,;
    Caption="Esercizio:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="UGQTFEROZN",Visible=.t., Left=388, Top=8,;
    Alignment=1, Width=49, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="UPSSJOYARI",Visible=.t., Left=388, Top=33,;
    Alignment=1, Width=49, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="WHVLKBNLLF",Visible=.t., Left=5, Top=88,;
    Alignment=2, Width=124, Height=15,;
    Caption="SALDI"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="YLIFYMXWWI",Visible=.t., Left=13, Top=142,;
    Alignment=1, Width=125, Height=18,;
    Caption="di cui fuori linea:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="ISOXXTICLS",Visible=.t., Left=12, Top=190,;
    Alignment=1, Width=125, Height=18,;
    Caption="di cui fuori linea:"  ;
  , bGlobalFont=.t.

  add object oBox_1_15 as StdBox with uid="MNBLYCSIWI",left=140, top=85, width=1,height=22

  add object oBox_1_34 as StdBox with uid="DNUATBJJUA",left=1, top=84, width=518,height=23

  add object oBox_1_35 as StdBox with uid="JVUHDZRGJS",left=331, top=85, width=1,height=197

  add object oBox_1_37 as StdBox with uid="ADUOIXXDBF",left=2, top=282, width=517,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_asp','SALDICON','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SLTIPCON=SALDICON.SLTIPCON";
  +" and "+i_cAliasName2+".SLCODICE=SALDICON.SLCODICE";
  +" and "+i_cAliasName2+".SLCODESE=SALDICON.SLCODESE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
