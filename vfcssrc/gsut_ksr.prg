* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_ksr                                                        *
*              Ricerca in report                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-01-24                                                      *
* Last revis.: 2012-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_ksr",oParentObject))

* --- Class definition
define class tgsut_ksr as StdForm
  Top    = 24
  Left   = 29

  * --- Standard Properties
  Width  = 661
  Height = 269
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-07-04"
  HelpContextID=99173225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_ksr"
  cComment = "Ricerca in report"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_REPTYPE = 0
  w_REPLACE = space(254)
  w_CASE = space(1)
  w_SEARCH = space(254)
  w_PATHSJ = space(200)
  w_SUBDIR = space(1)
  w_LOGFILE = space(254)
  w_VQUERY = space(254)
  w_CASEINS = space(1)
  w_NUMREP = 0
  w_NUMOCC = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_ksrPag1","gsut_ksr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oREPTYPE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_REPTYPE=0
      .w_REPLACE=space(254)
      .w_CASE=space(1)
      .w_SEARCH=space(254)
      .w_PATHSJ=space(200)
      .w_SUBDIR=space(1)
      .w_LOGFILE=space(254)
      .w_VQUERY=space(254)
      .w_CASEINS=space(1)
      .w_NUMREP=0
      .w_NUMOCC=0
        .w_REPTYPE = 0
        .w_REPLACE = ''
        .w_CASE = 'N'
        .w_SEARCH = ''
        .w_PATHSJ = LEFT(sys(5)+curdir(),LEN(sys(5)+curdir())-4)
          .DoRTCalc(6,6,.f.)
        .w_LOGFILE = ADDBS(SYS(2023))+Sys(2015)+'.dbf'
          .DoRTCalc(8,8,.f.)
        .w_CASEINS = 'N'
    endwith
    this.DoRTCalc(10,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oREPLACE_1_3.visible=!this.oPgFrm.Page1.oPag.oREPLACE_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCASE_1_4.visible=!this.oPgFrm.Page1.oPag.oCASE_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oREPTYPE_1_2.RadioValue()==this.w_REPTYPE)
      this.oPgFrm.Page1.oPag.oREPTYPE_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREPLACE_1_3.value==this.w_REPLACE)
      this.oPgFrm.Page1.oPag.oREPLACE_1_3.value=this.w_REPLACE
    endif
    if not(this.oPgFrm.Page1.oPag.oCASE_1_4.RadioValue()==this.w_CASE)
      this.oPgFrm.Page1.oPag.oCASE_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSEARCH_1_5.value==this.w_SEARCH)
      this.oPgFrm.Page1.oPag.oSEARCH_1_5.value=this.w_SEARCH
    endif
    if not(this.oPgFrm.Page1.oPag.oPATHSJ_1_6.value==this.w_PATHSJ)
      this.oPgFrm.Page1.oPag.oPATHSJ_1_6.value=this.w_PATHSJ
    endif
    if not(this.oPgFrm.Page1.oPag.oSUBDIR_1_7.RadioValue()==this.w_SUBDIR)
      this.oPgFrm.Page1.oPag.oSUBDIR_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLOGFILE_1_8.value==this.w_LOGFILE)
      this.oPgFrm.Page1.oPag.oLOGFILE_1_8.value=this.w_LOGFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oVQUERY_1_10.value==this.w_VQUERY)
      this.oPgFrm.Page1.oPag.oVQUERY_1_10.value=this.w_VQUERY
    endif
    if not(this.oPgFrm.Page1.oPag.oCASEINS_1_17.RadioValue()==this.w_CASEINS)
      this.oPgFrm.Page1.oPag.oCASEINS_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMREP_1_18.value==this.w_NUMREP)
      this.oPgFrm.Page1.oPag.oNUMREP_1_18.value=this.w_NUMREP
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMOCC_1_19.value==this.w_NUMOCC)
      this.oPgFrm.Page1.oPag.oNUMOCC_1_19.value=this.w_NUMOCC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_ksrPag1 as StdContainer
  Width  = 657
  height = 269
  stdWidth  = 657
  stdheight = 269
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oREPTYPE_1_2 as StdCombo with uid="RSQMTPMDPC",value=1,rtseq=1,rtrep=.f.,left=72,top=47,width=110,height=21;
    , HelpContextID = 1814;
    , cFormVar="w_REPTYPE",RowSource=""+"No,"+"Sostituisci,"+"Solo formato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oREPTYPE_1_2.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    0))))
  endfunc
  func oREPTYPE_1_2.GetRadio()
    this.Parent.oContained.w_REPTYPE = this.RadioValue()
    return .t.
  endfunc

  func oREPTYPE_1_2.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_REPTYPE==0,1,;
      iif(this.Parent.oContained.w_REPTYPE==1,2,;
      iif(this.Parent.oContained.w_REPTYPE==2,3,;
      0)))
  endfunc

  add object oREPLACE_1_3 as StdField with uid="LAZCKQXARA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_REPLACE", cQueryName = "REPLACE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 24643350,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=72, Top=71, InputMask=replicate('X',254)

  func oREPLACE_1_3.mHide()
    with this.Parent.oContained
      return (.w_REPTYPE<>1)
    endwith
  endfunc


  add object oCASE_1_4 as StdCombo with uid="FNQILRNNRE",rtseq=3,rtrep=.f.,left=72,top=98,width=143,height=21;
    , ToolTipText = "Se attivo mette l'espressione in cui si trova la stringa o tutta minuscola o tutta minuscola";
    , HelpContextID = 94293466;
    , cFormVar="w_CASE",RowSource=""+"Attuale,"+"Minuscolo,"+"Maiuscolo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCASE_1_4.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'L',;
    iif(this.value =3,'U',;
    space(1)))))
  endfunc
  func oCASE_1_4.GetRadio()
    this.Parent.oContained.w_CASE = this.RadioValue()
    return .t.
  endfunc

  func oCASE_1_4.SetRadio()
    this.Parent.oContained.w_CASE=trim(this.Parent.oContained.w_CASE)
    this.value = ;
      iif(this.Parent.oContained.w_CASE=='N',1,;
      iif(this.Parent.oContained.w_CASE=='L',2,;
      iif(this.Parent.oContained.w_CASE=='U',3,;
      0)))
  endfunc

  func oCASE_1_4.mHide()
    with this.Parent.oContained
      return (.w_REPTYPE<>2)
    endwith
  endfunc

  add object oSEARCH_1_5 as StdField with uid="MBIXFWDTKC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SEARCH", cQueryName = "SEARCH",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 157477082,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=72, Top=22, InputMask=replicate('X',254)

  add object oPATHSJ_1_6 as StdField with uid="FWBPGWHNRK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PATHSJ", cQueryName = "PATHSJ",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Path stampe schedulatore",;
    HelpContextID = 107724042,;
   bGlobalFont=.t.,;
    Height=20, Width=475, Left=72, Top=139, InputMask=replicate('X',200), bHasZoom = .t. 

  proc oPATHSJ_1_6.mZoom
    this.parent.ocontained.w_PATHSJ=left(cp_getdir(IIF(EMPTY(this.parent.ocontained.w_PATHSJ),sys(5)+sys(2003),this.parent.ocontained.w_PATHSJ),"Percorso di destinazione")+space(200),200)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oSUBDIR_1_7 as StdCheck with uid="TNLDYINCPG",rtseq=6,rtrep=.f.,left=557, top=139, caption="Sottocartelle",;
    ToolTipText = "Ricerca anche nelle sotto cartelle",;
    HelpContextID = 252758234,;
    cFormVar="w_SUBDIR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSUBDIR_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSUBDIR_1_7.GetRadio()
    this.Parent.oContained.w_SUBDIR = this.RadioValue()
    return .t.
  endfunc

  func oSUBDIR_1_7.SetRadio()
    this.Parent.oContained.w_SUBDIR=trim(this.Parent.oContained.w_SUBDIR)
    this.value = ;
      iif(this.Parent.oContained.w_SUBDIR=='S',1,;
      0)
  endfunc

  add object oLOGFILE_1_8 as StdField with uid="RUJMTTEGXY",rtseq=7,rtrep=.f.,;
    cFormVar = "w_LOGFILE", cQueryName = "LOGFILE",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "File di log",;
    HelpContextID = 183599286,;
   bGlobalFont=.t.,;
    Height=21, Width=575, Left=72, Top=163, InputMask=replicate('X',254), bHasZoom = .t. 

  proc oLOGFILE_1_8.mZoom
    this.parent.ocontained.w_LOGFILE=left(getfile(IIF(EMPTY(this.parent.ocontained.w_LOGFILE),sys(5)+sys(2003),this.parent.ocontained.w_LOGFILE),cp_translate("Log elaborazione"))+space(200),200)
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oVQUERY_1_10 as StdField with uid="DKIACZKQFU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_VQUERY", cQueryName = "VQUERY",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 125738154,;
   bGlobalFont=.t.,;
    Height=21, Width=575, Left=72, Top=191, InputMask=replicate('X',254)


  add object oBtn_1_12 as StdButton with uid="ZMIZZNTUDJ",left=547, top=217, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare le impostazioni";
    , HelpContextID = 44018154;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        SearchRep(this.Parent.oContained, .w_PATHSJ , .w_SUBDIR="S" , .w_SEARCH, .w_REPTYPE, .w_REPLACE, .w_CASE ,  .w_CASEINS, .w_LOGFILE  )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Len( .w_SEARCH )>0)
      endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="JWIBHYOYEX",left=597, top=217, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 91855802;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCASEINS_1_17 as StdCheck with uid="BIAUZCEVDF",rtseq=9,rtrep=.f.,left=433, top=22, caption="Case sensitive",;
    HelpContextID = 217133606,;
    cFormVar="w_CASEINS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCASEINS_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCASEINS_1_17.GetRadio()
    this.Parent.oContained.w_CASEINS = this.RadioValue()
    return .t.
  endfunc

  func oCASEINS_1_17.SetRadio()
    this.Parent.oContained.w_CASEINS=trim(this.Parent.oContained.w_CASEINS)
    this.value = ;
      iif(this.Parent.oContained.w_CASEINS=='S',1,;
      0)
  endfunc

  add object oNUMREP_1_18 as StdField with uid="RPLYFBXHGQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_NUMREP", cQueryName = "NUMREP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero report",;
    HelpContextID = 21109034,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=304, Top=221

  add object oNUMOCC_1_19 as StdField with uid="NQTXSLZQUA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_NUMOCC", cQueryName = "NUMOCC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero occorrenze",;
    HelpContextID = 241506602,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=126, Top=221

  add object oStr_1_1 as StdString with uid="TSSTXBYRXK",Visible=.t., Left=7, Top=24,;
    Alignment=1, Width=62, Height=18,;
    Caption="Ricerca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="NYTFAGEZHB",Visible=.t., Left=26, Top=140,;
    Alignment=1, Width=43, Height=18,;
    Caption="Path :"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="KIWYJZIBGC",Visible=.t., Left=26, Top=164,;
    Alignment=1, Width=43, Height=18,;
    Caption="Log :"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="NLUIUPSMEL",Visible=.t., Left=9, Top=50,;
    Alignment=1, Width=60, Height=18,;
    Caption="Sostituisci:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_REPLACE='N')
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="IMPNUBSASM",Visible=.t., Left=9, Top=100,;
    Alignment=1, Width=60, Height=18,;
    Caption="Carattere:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_REPTYPE<>2)
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="SIVGURLODI",Visible=.t., Left=9, Top=73,;
    Alignment=1, Width=60, Height=18,;
    Caption="con:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_REPTYPE<>1)
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="ZHPLHCDMGB",Visible=.t., Left=37, Top=221,;
    Alignment=1, Width=86, Height=18,;
    Caption="Trovate"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="UVRSUMWJWB",Visible=.t., Left=217, Top=221,;
    Alignment=0, Width=84, Height=18,;
    Caption="occorrenze su"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="AEBAUHLCXC",Visible=.t., Left=393, Top=221,;
    Alignment=0, Width=39, Height=18,;
    Caption="report"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_ksr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_ksr
* --- Area Manuale = Functions & Procedures
* --- gsut_ksr
* Parametri
* Obj 		=oggetto nel quale visualizzare l'avanzamento...
* sDir		=directory di partenza
* bSubFolder	=ricerca sotto directory (.t. / .f.)
* sSearch 		=parola da ricercare
* nReplType 	=se sostituire l'attuale contenuto con una nuova stringa (1), nessuna sostituzione (0), modificare solo il case (2)
* sReplace  	=stringa da sostituire
* sCase 		=('A' Attuale, 'L' lower case, 'U' uppercase
* sCaseSens	= Se 'S' ricerca case sensitive
* sLog		=Dbf con il log
Procedure SearchRep(obj, sdir, bSubFolder , sSearch, nReplType, sReplace,sCase,sCaseSens,sLog )

	* Stringa di ricerca

	sSearch=Rtrim(sSearch)
	if sCaseSens<>'S'
	 sSearch=Lower(sSearch)
	endif
	sdir=Alltrim(sdir)
	sReplace=Alltrim(sReplace)
	sLog=Alltrim(sLog)
	obj.w_NUMOCC=0
	obj.w_NUMREP=0
	If used(sLog)
	 Select (sLog)
	 Use
	Endif
	
	* Temporaneo in cui memorizzo il risultato..
	CREATE Table &sLog ;
	   (Rep_name Char(254), UniqueId C(10), Name M(10), Tipo Char(10), M1 M(10))
	
	* lo rendo scrivibile
	*=Wrcursor(sLog)
	
	* Avvio la scansione
	Scandir(obj, sdir, bSubFolder , sSearch, nReplType, sReplace,sCase,sCaseSens,sLog)		
  * Mostro il risultato a video..
  Select (JustFName(sLog))
 Browse
Endproc


PROCEDURE Scandir(obj, sdir, bSubFolder , sSearch, nReplType, sReplace,sCase,sCaseSens,sLog)
	LOCAL fso,folder,i,L_NumFile,nNumOcc, bFound
	* --- Esamino i report all'interno della cartella..
	If Not Empty(sDir)
		L_NumFile=Adir(ARep,sDir+'\*.frx')
		
		For i=1 to L_NumFile
			Obj.w_VQUERY='Searching in '+Alltrim( sDir+'\'+Arep[i,1] )+'...'
			obj.SetControlsValue()
			obj.Draw()
			nNumOcc=0
			bFound=.f.
			One_Rep(sDir+'\'+Arep[i,1], sSearch, nReplType, sReplace,sCase,sCaseSens,sLog,@nNumOcc, @bFound)
			obj.w_NUMOCC=obj.w_NUMOCC+nNumOcc
			if bFound
			obj.w_NUMREP=obj.w_NUMREP+1
			endif
		Endfor
	ENDIF
	* --- Se ricerca in sotto directory...
	If bSubFolder
		fso = CreateObject("Scripting.FileSystemObject")
		folder = fso.GetFolder(sDir)
		For Each myFile in folder.SubFolders
			IF MYFILE.ATTRIBUTES>=16		
				Scandir(obj,Myfile.path ,bSubFolder, sSearch, nReplType, sReplace,sCase,sCaseSens,sLog)
			ENDIF
		EndFor
		fso = null
	Endif
Endproc


* Dato un report accoda al risultato le espressioni che matchano
Proc One_Rep(L_Report, sSearch, nReplType, sReplace,sCase,sCaseSens,sLog, nNumOcc, bFound)
	* Eseguo la ricerca all'interno del report...
	Dimension afld[7,2]
	aFld[1,1]='Expr'
	aFld[2,1]='Name'
	aFld[3,1]='SupExpr'
	aFld[4,1]='User'
	aFld[5,1]='tag'
	aFld[6,1]='tag2'	
	aFld[7,1]='comment'
	aFld[1,2]='expr   '
	aFld[2,2]='name   '
	aFld[3,2]='Pr.When'
	aFld[4,2]='User   '
	aFld[5,2]='tag   '
	aFld[6,2]='tag2  '	
	aFld[7,2]='comment' 	
	* Costruisco la frase..
	Local cmdSql, n_i
	CmdSql=''
	For n_i=1 to Alen(aFld,1)
		CmdSql=CmdSql+iif(not empty( CmdSql  ) , ' Union All ','')
		CmdSql=CmdSql+' Select "'+L_Report+'" As  Rep_name,uniqueid,name,"'+aFld[n_i,2]+'" As Tipo, '+aFld[n_i,1]+' As M1 From '+L_Report+' Where '+IIF(sCaseSens<>'S','lower(','')+aFld[n_i,1]+IIF(sCaseSens<>'S',')','')+' Like "%'+sSearch+'%"'
	endfor
	CmdSql=CmdSql+' Into Cursor _Temp_ NoFilter'
	*  Lancio la query..
	&CmdSql
	
	* --- Attivit� di replace..
	IF nReplType<>0
		*se sostituire l'attuale contenuto con una nuova stringa (1) o modificare solo il case (2)
		* sReplace  	=stringa da sostituire
		* sCase 		=('A' Attuale, 'L' lower case, 'U' uppercase
		Dimension afldw[4]
		aFldw[1]='Expr'
		aFldw[2]='SupExpr'
		aFldw[3]='tag'
		aFldw[4]='tag2'	
		For n_i=1 to Alen(aFldw,1)
			CmdSql='Update '+L_report+' Set '+aFldw[n_i]+'= '
			CmdSql=CmdSql+iif(nReplType=2,iif(sCase='L', 'lower ( '+ aFldw[n_i] +' )' ,' Upper ( '+ aFldw[n_i] +' )') , 'STRTRAN('+aFldw[n_i] +',"'+sSearch+'" ,"'+sReplace+'",-1,-1,'+IIF(sCaseSens='S','0','1')+' )' )
			CmdSql=CmdSql+' Where '+IIF(sCaseSens<>'S','lower(','')+aFldw[n_i]+IIF(sCaseSens<>'S',')','')+' Like "%'+sSearch+'%"'
			* eseguo il comando
			&CmdSql
		endfor	
	endif
	
	* Rem inserisco il contenuto all'interno del cursore..
	nNumOcc=0
	Select _Temp_
	Go top
	Do While Not Eof('_Temp_')
	 SCATTER MEMVAR MEMO
	 INSERT INTO &sLog FROM MEMVAR
	 nNumOcc=nNumOcc+1
	 Select _Temp_
	 Skip
	Enddo
	bFound=nNumOcc>0
	Select _Temp_
	Use
	* --- Chiudo il report...
	L_Alias=Right(left(l_report,rat('.',l_report)-1),len(left(l_report,rat('.',l_report)-1))-rat('\',left(l_report,rat('.',l_report)-1)))
	Select &L_Alias
	Use

EndProc


* --- Fine Area Manuale
