* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mpa                                                        *
*              Situazione partite                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_338]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2014-09-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mpa")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mpa")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mpa")
  return

* --- Class definition
define class tgscg_mpa as StdPCForm
  Width  = 866
  Height = 210
  Top    = 234
  Left   = 5
  cComment = "Situazione partite"
  cPrg = "gscg_mpa"
  HelpContextID=147465577
  add object cnt as tcgscg_mpa
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mpa as PCContext
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CODVAL = space(3)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_CAOVAL = 0
  w_DATAPE = space(8)
  w_FLPART = space(3)
  w_CODPAG = space(5)
  w_FLABAC = space(3)
  w_OBTEST = space(8)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_TOTDOC = 0
  w_RIGDOC = 0
  w_DESPAG = space(30)
  w_PTFLCRSA = space(1)
  w_PTDATSCA = space(8)
  w_SEGNO = space(1)
  w_PTCAOAPE = 0
  w_PTCODVAL = space(3)
  w_PTCAOVAL = 0
  w_PTNUMPAR = space(31)
  w_PT_SEGNO = space(1)
  w_PTTOTIMP = 0
  w_PTFLSOSP = space(1)
  w_PTMODPAG = space(10)
  w_PTBANAPP = space(10)
  w_PTNUMDOC = 0
  w_PTALFDOC = space(10)
  w_PTDATDOC = space(8)
  w_PTIMPDOC = 0
  w_PTBANNOS = space(15)
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PTTOTABB = 0
  w_PTDATAPE = space(8)
  w_PTFLIMPE = space(2)
  w_PTNUMDIS = space(10)
  w_PTFLINDI = space(1)
  w_FLNDOC = space(1)
  w_PTCODAGE = space(5)
  w_PTNUMCOR = space(25)
  w_DESBA = space(35)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_PTDESRIG = space(50)
  w_PTNUMPRO = 0
  w_OBTEST = space(8)
  w_DATOBSO = space(8)
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_PTFLVABD = space(1)
  w_PTFLRAGG = space(1)
  w_PTRIFIND = space(10)
  w_TIPDOC = space(10)
  w_PTDATREG = space(8)
  w_DATDOC = space(8)
  w_DATSCA = space(8)
  w_TIPPAG = space(2)
  w_NUMCOR = space(25)
  w_BANNOS = space(15)
  w_BANAPP = space(10)
  w_CODAGE = space(5)
  w_NUMPAR = space(10)
  w_FLINSO = space(1)
  w_RIFCES = space(1)
  proc Save(i_oFrom)
    this.w_PTSERIAL = i_oFrom.w_PTSERIAL
    this.w_PTROWORD = i_oFrom.w_PTROWORD
    this.w_CODVAL = i_oFrom.w_CODVAL
    this.w_TIPCON = i_oFrom.w_TIPCON
    this.w_CODCON = i_oFrom.w_CODCON
    this.w_CAOVAL = i_oFrom.w_CAOVAL
    this.w_DATAPE = i_oFrom.w_DATAPE
    this.w_FLPART = i_oFrom.w_FLPART
    this.w_CODPAG = i_oFrom.w_CODPAG
    this.w_FLABAC = i_oFrom.w_FLABAC
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_NUMDOC = i_oFrom.w_NUMDOC
    this.w_ALFDOC = i_oFrom.w_ALFDOC
    this.w_TOTDOC = i_oFrom.w_TOTDOC
    this.w_RIGDOC = i_oFrom.w_RIGDOC
    this.w_DESPAG = i_oFrom.w_DESPAG
    this.w_PTFLCRSA = i_oFrom.w_PTFLCRSA
    this.w_PTDATSCA = i_oFrom.w_PTDATSCA
    this.w_SEGNO = i_oFrom.w_SEGNO
    this.w_PTCAOAPE = i_oFrom.w_PTCAOAPE
    this.w_PTCODVAL = i_oFrom.w_PTCODVAL
    this.w_PTCAOVAL = i_oFrom.w_PTCAOVAL
    this.w_PTNUMPAR = i_oFrom.w_PTNUMPAR
    this.w_PT_SEGNO = i_oFrom.w_PT_SEGNO
    this.w_PTTOTIMP = i_oFrom.w_PTTOTIMP
    this.w_PTFLSOSP = i_oFrom.w_PTFLSOSP
    this.w_PTMODPAG = i_oFrom.w_PTMODPAG
    this.w_PTBANAPP = i_oFrom.w_PTBANAPP
    this.w_PTNUMDOC = i_oFrom.w_PTNUMDOC
    this.w_PTALFDOC = i_oFrom.w_PTALFDOC
    this.w_PTDATDOC = i_oFrom.w_PTDATDOC
    this.w_PTIMPDOC = i_oFrom.w_PTIMPDOC
    this.w_PTBANNOS = i_oFrom.w_PTBANNOS
    this.w_PTTIPCON = i_oFrom.w_PTTIPCON
    this.w_PTCODCON = i_oFrom.w_PTCODCON
    this.w_PTTOTABB = i_oFrom.w_PTTOTABB
    this.w_PTDATAPE = i_oFrom.w_PTDATAPE
    this.w_PTFLIMPE = i_oFrom.w_PTFLIMPE
    this.w_PTNUMDIS = i_oFrom.w_PTNUMDIS
    this.w_PTFLINDI = i_oFrom.w_PTFLINDI
    this.w_FLNDOC = i_oFrom.w_FLNDOC
    this.w_PTCODAGE = i_oFrom.w_PTCODAGE
    this.w_PTNUMCOR = i_oFrom.w_PTNUMCOR
    this.w_DESBA = i_oFrom.w_DESBA
    this.w_DECTOT = i_oFrom.w_DECTOT
    this.w_CALCPICT = i_oFrom.w_CALCPICT
    this.w_PTDESRIG = i_oFrom.w_PTDESRIG
    this.w_PTNUMPRO = i_oFrom.w_PTNUMPRO
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_PTSERRIF = i_oFrom.w_PTSERRIF
    this.w_PTORDRIF = i_oFrom.w_PTORDRIF
    this.w_PTNUMRIF = i_oFrom.w_PTNUMRIF
    this.w_PTFLVABD = i_oFrom.w_PTFLVABD
    this.w_PTFLRAGG = i_oFrom.w_PTFLRAGG
    this.w_PTRIFIND = i_oFrom.w_PTRIFIND
    this.w_TIPDOC = i_oFrom.w_TIPDOC
    this.w_PTDATREG = i_oFrom.w_PTDATREG
    this.w_DATDOC = i_oFrom.w_DATDOC
    this.w_DATSCA = i_oFrom.w_DATSCA
    this.w_TIPPAG = i_oFrom.w_TIPPAG
    this.w_NUMCOR = i_oFrom.w_NUMCOR
    this.w_BANNOS = i_oFrom.w_BANNOS
    this.w_BANAPP = i_oFrom.w_BANAPP
    this.w_CODAGE = i_oFrom.w_CODAGE
    this.w_NUMPAR = i_oFrom.w_NUMPAR
    this.w_FLINSO = i_oFrom.w_FLINSO
    this.w_RIFCES = i_oFrom.w_RIFCES
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_PTSERIAL = this.w_PTSERIAL
    i_oTo.w_PTROWORD = this.w_PTROWORD
    i_oTo.w_CODVAL = this.w_CODVAL
    i_oTo.w_TIPCON = this.w_TIPCON
    i_oTo.w_CODCON = this.w_CODCON
    i_oTo.w_CAOVAL = this.w_CAOVAL
    i_oTo.w_DATAPE = this.w_DATAPE
    i_oTo.w_FLPART = this.w_FLPART
    i_oTo.w_CODPAG = this.w_CODPAG
    i_oTo.w_FLABAC = this.w_FLABAC
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_NUMDOC = this.w_NUMDOC
    i_oTo.w_ALFDOC = this.w_ALFDOC
    i_oTo.w_TOTDOC = this.w_TOTDOC
    i_oTo.w_RIGDOC = this.w_RIGDOC
    i_oTo.w_DESPAG = this.w_DESPAG
    i_oTo.w_PTFLCRSA = this.w_PTFLCRSA
    i_oTo.w_PTDATSCA = this.w_PTDATSCA
    i_oTo.w_SEGNO = this.w_SEGNO
    i_oTo.w_PTCAOAPE = this.w_PTCAOAPE
    i_oTo.w_PTCODVAL = this.w_PTCODVAL
    i_oTo.w_PTCAOVAL = this.w_PTCAOVAL
    i_oTo.w_PTNUMPAR = this.w_PTNUMPAR
    i_oTo.w_PT_SEGNO = this.w_PT_SEGNO
    i_oTo.w_PTTOTIMP = this.w_PTTOTIMP
    i_oTo.w_PTFLSOSP = this.w_PTFLSOSP
    i_oTo.w_PTMODPAG = this.w_PTMODPAG
    i_oTo.w_PTBANAPP = this.w_PTBANAPP
    i_oTo.w_PTNUMDOC = this.w_PTNUMDOC
    i_oTo.w_PTALFDOC = this.w_PTALFDOC
    i_oTo.w_PTDATDOC = this.w_PTDATDOC
    i_oTo.w_PTIMPDOC = this.w_PTIMPDOC
    i_oTo.w_PTBANNOS = this.w_PTBANNOS
    i_oTo.w_PTTIPCON = this.w_PTTIPCON
    i_oTo.w_PTCODCON = this.w_PTCODCON
    i_oTo.w_PTTOTABB = this.w_PTTOTABB
    i_oTo.w_PTDATAPE = this.w_PTDATAPE
    i_oTo.w_PTFLIMPE = this.w_PTFLIMPE
    i_oTo.w_PTNUMDIS = this.w_PTNUMDIS
    i_oTo.w_PTFLINDI = this.w_PTFLINDI
    i_oTo.w_FLNDOC = this.w_FLNDOC
    i_oTo.w_PTCODAGE = this.w_PTCODAGE
    i_oTo.w_PTNUMCOR = this.w_PTNUMCOR
    i_oTo.w_DESBA = this.w_DESBA
    i_oTo.w_DECTOT = this.w_DECTOT
    i_oTo.w_CALCPICT = this.w_CALCPICT
    i_oTo.w_PTDESRIG = this.w_PTDESRIG
    i_oTo.w_PTNUMPRO = this.w_PTNUMPRO
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_PTSERRIF = this.w_PTSERRIF
    i_oTo.w_PTORDRIF = this.w_PTORDRIF
    i_oTo.w_PTNUMRIF = this.w_PTNUMRIF
    i_oTo.w_PTFLVABD = this.w_PTFLVABD
    i_oTo.w_PTFLRAGG = this.w_PTFLRAGG
    i_oTo.w_PTRIFIND = this.w_PTRIFIND
    i_oTo.w_TIPDOC = this.w_TIPDOC
    i_oTo.w_PTDATREG = this.w_PTDATREG
    i_oTo.w_DATDOC = this.w_DATDOC
    i_oTo.w_DATSCA = this.w_DATSCA
    i_oTo.w_TIPPAG = this.w_TIPPAG
    i_oTo.w_NUMCOR = this.w_NUMCOR
    i_oTo.w_BANNOS = this.w_BANNOS
    i_oTo.w_BANAPP = this.w_BANAPP
    i_oTo.w_CODAGE = this.w_CODAGE
    i_oTo.w_NUMPAR = this.w_NUMPAR
    i_oTo.w_FLINSO = this.w_FLINSO
    i_oTo.w_RIFCES = this.w_RIFCES
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mpa as StdPcDetailPartite
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 866
  Height = 210
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-19"
  HelpContextID=147465577
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=68

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PAR_TITE_IDX = 0
  COC_MAST_IDX = 0
  BAN_CHE_IDX = 0
  CONTI_IDX = 0
  MOD_PAGA_IDX = 0
  PAG_AMEN_IDX = 0
  AGENTI_IDX = 0
  BAN_CONTI_IDX = 0
  cFile = "PAR_TITE"
  cKeySelect = "PTSERIAL,PTROWORD"
  cKeyWhere  = "PTSERIAL=this.w_PTSERIAL and PTROWORD=this.w_PTROWORD"
  cKeyDetail  = "PTSERIAL=this.w_PTSERIAL and PTROWORD=this.w_PTROWORD"
  cKeyWhereODBC = '"PTSERIAL="+cp_ToStrODBC(this.w_PTSERIAL)';
      +'+" and PTROWORD="+cp_ToStrODBC(this.w_PTROWORD)';

  cKeyDetailWhereODBC = '"PTSERIAL="+cp_ToStrODBC(this.w_PTSERIAL)';
      +'+" and PTROWORD="+cp_ToStrODBC(this.w_PTROWORD)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"PAR_TITE.PTSERIAL="+cp_ToStrODBC(this.w_PTSERIAL)';
      +'+" and PAR_TITE.PTROWORD="+cp_ToStrODBC(this.w_PTROWORD)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PAR_TITE.CPROWNUM '
  cPrg = "gscg_mpa"
  cComment = "Situazione partite"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PTSERIAL = space(10)
  o_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CODVAL = space(3)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_CAOVAL = 0
  w_DATAPE = ctod('  /  /  ')
  w_FLPART = space(3)
  w_CODPAG = space(5)
  o_CODPAG = space(5)
  w_FLABAC = space(3)
  w_OBTEST = ctod('  /  /  ')
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_TOTDOC = 0
  w_RIGDOC = 0
  w_DESPAG = space(30)
  w_PTFLCRSA = space(1)
  w_PTDATSCA = ctod('  /  /  ')
  o_PTDATSCA = ctod('  /  /  ')
  w_SEGNO = space(1)
  o_SEGNO = space(1)
  w_PTCAOAPE = 0
  w_PTCODVAL = space(3)
  w_PTCAOVAL = 0
  w_PTNUMPAR = space(31)
  o_PTNUMPAR = space(31)
  w_PT_SEGNO = space(1)
  w_PTTOTIMP = 0
  w_PTFLSOSP = space(1)
  w_PTMODPAG = space(10)
  w_PTBANAPP = space(10)
  o_PTBANAPP = space(10)
  w_PTNUMDOC = 0
  w_PTALFDOC = space(10)
  w_PTDATDOC = ctod('  /  /  ')
  w_PTIMPDOC = 0
  w_PTBANNOS = space(15)
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PTTOTABB = 0
  w_PTDATAPE = ctod('  /  /  ')
  w_PTFLIMPE = space(2)
  w_PTNUMDIS = space(10)
  w_PTFLINDI = space(1)
  w_FLNDOC = space(1)
  w_PTCODAGE = space(5)
  w_PTNUMCOR = space(25)
  w_DESBA = space(35)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_PTDESRIG = space(50)
  w_PTNUMPRO = 0
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_PTFLVABD = space(1)
  w_PTFLRAGG = space(1)
  w_PTRIFIND = space(10)
  w_TIPDOC = space(10)
  w_PTDATREG = ctod('  /  /  ')
  w_DATDOC = ctod('  /  /  ')
  w_DATSCA = ctod('  /  /  ')
  w_TIPPAG = space(2)
  w_NUMCOR = space(25)
  w_BANNOS = space(15)
  w_BANAPP = space(10)
  w_CODAGE = space(5)
  w_NUMPAR = space(10)
  w_FLINSO = space(1)
  w_RIFCES = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gscg_mpa
  w_OGG=.NULL.
  Procedure InitSon
  * Valorizzo le variabili che arrivano dal padre
  	With This
  	  .w_FLPART=THIS.oParentObject.w_PNFLPART
      .w_FLINSO=Nvl(THIS.oParentObject.w_FLINSO,'N')
      .w_RIFCES=Nvl(THIS.oParentObject.w_PNRIFCES,'N')
     if .w_FLPART <>'N'
        .w_TIPDOC=THIS.oParentObject.w_PNTIPDOC
        .w_CODVAL=THIS.oParentObject.w_PNCODVAL
        .w_TIPCON=THIS.oParentObject.w_PNTIPCON
        .w_CODCON=THIS.oParentObject.w_PNCODCON
        .w_CAOVAL=THIS.oParentObject.w_PNCAOVAL
        .w_DATAPE=IIF(EMPTY(this.oParentObject.w_PNDATDOC), cp_todate(this.oParentObject.w_PNDATREG), cp_todate(this.oParentObject.w_PNDATDOC))
        .w_FLABAC=THIS.oParentObject.w_PNFLABAN
        .w_FLNDOC=THIS.oParentObject.w_FLNDOC
        .w_NUMDOC=this.oParentObject.w_PNNUMDOC
        .w_DATDOC=cp_todate(this.oParentObject.w_PNDATDOC)
        .w_ALFDOC=this.oParentObject.w_PNALFDOC
        .w_TOTDOC=THIS.oParentObject.w_PNTOTDOC
        .w_RIGDOC=ABS( THIS.oParentObject.w_PNIMPDAR - THIS.oParentObject.w_PNIMPAVE )
        .w_CODPAG=THIS.oParentObject.w_PNCODPAG
        .w_DESPAG=THIS.oParentObject.w_DESCRI
        .w_DECTOT=THIS.oParentObject.w_DECTOT
        .w_OBTEST=THIS.oParentObject.w_PNDATREG
        .w_NUMCOR=THIS.oParentObject.w_NUMCOR
        .w_BANNOS=THIS.oParentObject.w_BANNOS
        .w_BANAPP=THIS.oParentObject.w_BANAPP
        .w_CODAGE=THIS.oParentObject.w_PNCODAGE
        if .cFunction='Load'
          .w_PT_SEGNO=IIF(Not Empty(.w_PT_SEGNO),.w_PT_SEGNO,IIF(.w_SEGNO='D', 'A', IIF(.w_SEGNO='A','D',IIF(this.oParentObject.w_PNIMPAVE<>0,'A','D'))))
          .w_PTBANAPP=iif(empty(.w_PTBANAPP),.w_BANAPP,.w_PTBANAPP)
          .w_PTDATDOC=iif(empty(.w_PTDATDOC),.w_DATDOC,.w_PTDATDOC)
          .w_PTBANNOS=iif(empty(.w_PTBANNOS),.w_BANNOS,.w_PTBANNOS)
          .w_PTCODAGE=iif(empty(.w_PTCODAGE),.w_CODAGE,.w_PTCODAGE)
          .w_PTDESRIG=iif(empty(.w_PTDESRIG),THIS.oParentObject.w_PNDESRIG,.w_PTDESRIG)
          .w_PTCODVAL=iif(empty(.w_PTCODVAL),.w_CODVAL,.w_PTCODVAL)
          .w_PTFLCRSA=iif(empty(.w_PTFLCRSA),.w_FLPART,.w_PTFLCRSA)
          .w_PTTIPCON=iif(empty(.w_PTTIPCON),.w_TIPCON,.w_PTTIPCON)
          .w_PTCODCON=iif(empty(.w_PTCODCON),.w_CODCON,.w_PTCODCON)
          .w_PTCAOVAL=iif(empty(.w_PTCAOVAL),.w_CAOVAL,.w_PTCAOVAL)
          .w_PTIMPDOC=IIF(.w_TOTDOC=0,.w_RIGDOC, .w_TOTDOC)
          .w_PTDATAPE=iif(empty(.w_PTDATAPE),.w_DATAPE,.w_PTDATAPE)
          .w_PTNUMCOR=iif(empty(.w_PTNUMPAR),.w_NUMCOR,.w_PTNUMCOR)
        endif
      .mCalc(.T.)
  		.SetControlsValue()
  		* Mostro / Nascondo i Controls
  		.mHideControls()
      endif
  
    EndWith
  EndProc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mpaPag1","gscg_mpa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODPAG_1_12
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='BAN_CHE'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='MOD_PAGA'
    this.cWorkTables[5]='PAG_AMEN'
    this.cWorkTables[6]='AGENTI'
    this.cWorkTables[7]='BAN_CONTI'
    this.cWorkTables[8]='PAR_TITE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(8))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_TITE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_TITE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_mpa'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_11_joined
    link_2_11_joined=.f.
    local link_2_17_joined
    link_2_17_joined=.f.
    local link_2_28_joined
    link_2_28_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- gscg_mpa
    This.InitSon()
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PAR_TITE where PTSERIAL=KeySet.PTSERIAL
    *                            and PTROWORD=KeySet.PTROWORD
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gscg_mpa
      * --- Setta Ordine per Data Scadenza
      i_cOrder = 'order by PTDATSCA '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2],this.bLoadRecFilter,this.PAR_TITE_IDX,"gscg_mpa")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_TITE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_TITE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_TITE '
      link_2_11_joined=this.AddJoinedLink_2_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_17_joined=this.AddJoinedLink_2_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_28_joined=this.AddJoinedLink_2_28(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PTSERIAL',this.w_PTSERIAL  ,'PTROWORD',this.w_PTROWORD  )
      select * from (i_cTable) PAR_TITE where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODPAG = .w_CODPAG
        .w_DESPAG = space(30)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_PTSERIAL = NVL(PTSERIAL,space(10))
        .w_PTROWORD = NVL(PTROWORD,0)
        .w_CODVAL = .w_CODVAL
        .w_TIPCON = .w_TIPCON
        .w_CODCON = .w_CODCON
        .w_CAOVAL = .w_CAOVAL
        .w_DATAPE = .w_DATAPE
        .w_FLPART = .w_FLPART
          .link_1_12('Load')
        .w_FLABAC = .w_FLABAC
        .w_OBTEST = .w_OBTEST
        .w_NUMDOC = .w_NUMDOC
        .w_ALFDOC = .w_ALFDOC
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .w_TOTDOC = .w_TOTDOC
        .w_RIGDOC = .w_RIGDOC
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .w_FLNDOC = .w_FLNDOC
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .w_DECTOT = .w_DECTOT
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_OBTEST = i_DATSYS
        .w_TIPDOC = .w_TIPDOC
        .w_DATDOC = .w_DATDOC
        .w_NUMCOR = .w_NUMCOR
        .w_BANNOS = .w_BANNOS
        .w_BANAPP = .w_BANAPP
        .w_CODAGE = .w_CODAGE
        .w_NUMPAR = 'variabile usata per forzare la creazione di o_PTNUMPAR'
        cp_LoadRecExtFlds(this,'PAR_TITE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_SEGNO = space(1)
          .w_DESBA = space(35)
          .w_DATOBSO = ctod("  /  /  ")
          .w_TIPPAG = space(2)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_PTFLCRSA = NVL(PTFLCRSA,space(1))
          .w_PTDATSCA = NVL(cp_ToDate(PTDATSCA),ctod("  /  /  "))
          .w_PTCAOAPE = NVL(PTCAOAPE,0)
          .w_PTCODVAL = NVL(PTCODVAL,space(3))
          .w_PTCAOVAL = NVL(PTCAOVAL,0)
          .w_PTNUMPAR = NVL(PTNUMPAR,space(31))
          .w_PT_SEGNO = NVL(PT_SEGNO,space(1))
          .w_PTTOTIMP = NVL(PTTOTIMP,0)
          .w_PTFLSOSP = NVL(PTFLSOSP,space(1))
          .w_PTMODPAG = NVL(PTMODPAG,space(10))
          if link_2_11_joined
            this.w_PTMODPAG = NVL(MPCODICE211,NVL(this.w_PTMODPAG,space(10)))
            this.w_TIPPAG = NVL(MPTIPPAG211,space(2))
          else
          .link_2_11('Load')
          endif
          .w_PTBANAPP = NVL(PTBANAPP,space(10))
          * evitabile
          *.link_2_12('Load')
          .w_PTNUMDOC = NVL(PTNUMDOC,0)
          .w_PTALFDOC = NVL(PTALFDOC,space(10))
          .w_PTDATDOC = NVL(cp_ToDate(PTDATDOC),ctod("  /  /  "))
          .w_PTIMPDOC = NVL(PTIMPDOC,0)
          .w_PTBANNOS = NVL(PTBANNOS,space(15))
          if link_2_17_joined
            this.w_PTBANNOS = NVL(BACODBAN217,NVL(this.w_PTBANNOS,space(15)))
            this.w_DESBA = NVL(BADESCRI217,space(35))
          else
          .link_2_17('Load')
          endif
          .w_PTTIPCON = NVL(PTTIPCON,space(1))
          .w_PTCODCON = NVL(PTCODCON,space(15))
          .w_PTTOTABB = NVL(PTTOTABB,0)
          .w_PTDATAPE = NVL(cp_ToDate(PTDATAPE),ctod("  /  /  "))
          .w_PTFLIMPE = NVL(PTFLIMPE,space(2))
          .w_PTNUMDIS = NVL(PTNUMDIS,space(10))
          .w_PTFLINDI = NVL(PTFLINDI,space(1))
          .w_PTCODAGE = NVL(PTCODAGE,space(5))
          if link_2_28_joined
            this.w_PTCODAGE = NVL(AGCODAGE228,NVL(this.w_PTCODAGE,space(5)))
            this.w_DATOBSO = NVL(cp_ToDate(AGDTOBSO228),ctod("  /  /  "))
          else
          .link_2_28('Load')
          endif
          .w_PTNUMCOR = NVL(PTNUMCOR,space(25))
          * evitabile
          *.link_2_29('Load')
          .w_PTDESRIG = NVL(PTDESRIG,space(50))
          .w_PTNUMPRO = NVL(PTNUMPRO,0)
          .w_PTSERRIF = NVL(PTSERRIF,space(10))
          .w_PTORDRIF = NVL(PTORDRIF,0)
          .w_PTNUMRIF = NVL(PTNUMRIF,0)
          .w_PTFLVABD = NVL(PTFLVABD,space(1))
          .w_PTFLRAGG = NVL(PTFLRAGG,space(1))
          .w_PTRIFIND = NVL(PTRIFIND,space(10))
          .w_PTDATREG = NVL(cp_ToDate(PTDATREG),ctod("  /  /  "))
        .w_DATSCA = .w_PTDATSCA
        .w_FLINSO = .w_FLINSO
        .w_RIFCES = .w_RIFCES
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CODVAL = .w_CODVAL
        .w_TIPCON = .w_TIPCON
        .w_CODCON = .w_CODCON
        .w_CAOVAL = .w_CAOVAL
        .w_DATAPE = .w_DATAPE
        .w_FLPART = .w_FLPART
        .w_FLABAC = .w_FLABAC
        .w_OBTEST = .w_OBTEST
        .w_NUMDOC = .w_NUMDOC
        .w_ALFDOC = .w_ALFDOC
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .w_TOTDOC = .w_TOTDOC
        .w_RIGDOC = .w_RIGDOC
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .w_FLNDOC = .w_FLNDOC
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .w_DECTOT = .w_DECTOT
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_OBTEST = i_DATSYS
        .w_TIPDOC = .w_TIPDOC
        .w_DATDOC = .w_DATDOC
        .w_NUMCOR = .w_NUMCOR
        .w_BANNOS = .w_BANNOS
        .w_BANAPP = .w_BANAPP
        .w_CODAGE = .w_CODAGE
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PTSERIAL=space(10)
      .w_PTROWORD=0
      .w_CODVAL=space(3)
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .w_CAOVAL=0
      .w_DATAPE=ctod("  /  /  ")
      .w_FLPART=space(3)
      .w_CODPAG=space(5)
      .w_FLABAC=space(3)
      .w_OBTEST=ctod("  /  /  ")
      .w_NUMDOC=0
      .w_ALFDOC=space(10)
      .w_TOTDOC=0
      .w_RIGDOC=0
      .w_DESPAG=space(30)
      .w_PTFLCRSA=space(1)
      .w_PTDATSCA=ctod("  /  /  ")
      .w_SEGNO=space(1)
      .w_PTCAOAPE=0
      .w_PTCODVAL=space(3)
      .w_PTCAOVAL=0
      .w_PTNUMPAR=space(31)
      .w_PT_SEGNO=space(1)
      .w_PTTOTIMP=0
      .w_PTFLSOSP=space(1)
      .w_PTMODPAG=space(10)
      .w_PTBANAPP=space(10)
      .w_PTNUMDOC=0
      .w_PTALFDOC=space(10)
      .w_PTDATDOC=ctod("  /  /  ")
      .w_PTIMPDOC=0
      .w_PTBANNOS=space(15)
      .w_PTTIPCON=space(1)
      .w_PTCODCON=space(15)
      .w_PTTOTABB=0
      .w_PTDATAPE=ctod("  /  /  ")
      .w_PTFLIMPE=space(2)
      .w_PTNUMDIS=space(10)
      .w_PTFLINDI=space(1)
      .w_FLNDOC=space(1)
      .w_PTCODAGE=space(5)
      .w_PTNUMCOR=space(25)
      .w_DESBA=space(35)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_PTDESRIG=space(50)
      .w_PTNUMPRO=0
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_PTSERRIF=space(10)
      .w_PTORDRIF=0
      .w_PTNUMRIF=0
      .w_PTFLVABD=space(1)
      .w_PTFLRAGG=space(1)
      .w_PTRIFIND=space(10)
      .w_TIPDOC=space(10)
      .w_PTDATREG=ctod("  /  /  ")
      .w_DATDOC=ctod("  /  /  ")
      .w_DATSCA=ctod("  /  /  ")
      .w_TIPPAG=space(2)
      .w_NUMCOR=space(25)
      .w_BANNOS=space(15)
      .w_BANAPP=space(10)
      .w_CODAGE=space(5)
      .w_NUMPAR=space(10)
      .w_FLINSO=space(1)
      .w_RIFCES=space(1)
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,2,.f.)
        .w_CODVAL = .w_CODVAL
        .w_TIPCON = .w_TIPCON
        .w_CODCON = .w_CODCON
        .w_CAOVAL = .w_CAOVAL
        .w_DATAPE = .w_DATAPE
        .w_FLPART = .w_FLPART
        .w_CODPAG = .w_CODPAG
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODPAG))
         .link_1_12('Full')
        endif
        .w_FLABAC = .w_FLABAC
        .w_OBTEST = .w_OBTEST
        .w_NUMDOC = .w_NUMDOC
        .w_ALFDOC = .w_ALFDOC
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .w_TOTDOC = .w_TOTDOC
        .w_RIGDOC = .w_RIGDOC
        .DoRTCalc(16,16,.f.)
        .w_PTFLCRSA = .w_FLPART
        .DoRTCalc(18,19,.f.)
        .w_PTCAOAPE = .w_CAOVAL
        .w_PTCODVAL = .w_CODVAL
        .w_PTCAOVAL = .w_CAOVAL
        .DoRTCalc(23,23,.f.)
        .w_PT_SEGNO = .w_PT_SEGNO
        .DoRTCalc(25,25,.f.)
        .w_PTFLSOSP = ' '
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_PTMODPAG))
         .link_2_11('Full')
        endif
        .w_PTBANAPP = .w_BANAPP
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_PTBANAPP))
         .link_2_12('Full')
        endif
        .w_PTNUMDOC = .w_NUMDOC
        .w_PTALFDOC = .w_ALFDOC
        .w_PTDATDOC = .w_DATDOC
        .w_PTIMPDOC = IIF(.w_TOTDOC=0, .w_RIGDOC, .w_TOTDOC)
        .w_PTBANNOS = .w_BANNOS
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_PTBANNOS))
         .link_2_17('Full')
        endif
        .w_PTTIPCON = .w_TIPCON
        .w_PTCODCON = .w_CODCON
        .w_PTTOTABB = 0
        .w_PTDATAPE = .w_DATAPE
        .DoRTCalc(38,39,.f.)
        .w_PTFLINDI = ' '
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .w_FLNDOC = .w_FLNDOC
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .w_PTCODAGE = .w_CODAGE
        .DoRTCalc(42,42,.f.)
        if not(empty(.w_PTCODAGE))
         .link_2_28('Full')
        endif
        .w_PTNUMCOR = .w_NUMCOR
        .DoRTCalc(43,43,.f.)
        if not(empty(.w_PTNUMCOR))
         .link_2_29('Full')
        endif
        .DoRTCalc(44,44,.f.)
        .w_DECTOT = .w_DECTOT
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_PTDESRIG = .w_PTDESRIG
        .DoRTCalc(48,48,.f.)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(50,56,.f.)
        .w_TIPDOC = .w_TIPDOC
        .DoRTCalc(58,58,.f.)
        .w_DATDOC = .w_DATDOC
        .w_DATSCA = .w_PTDATSCA
        .DoRTCalc(61,61,.f.)
        .w_NUMCOR = .w_NUMCOR
        .w_BANNOS = .w_BANNOS
        .w_BANAPP = .w_BANAPP
        .w_CODAGE = .w_CODAGE
        .w_NUMPAR = 'variabile usata per forzare la creazione di o_PTNUMPAR'
        .w_FLINSO = .w_FLINSO
        .w_RIFCES = .w_RIFCES
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_TITE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_mpa
    this.InitSon()
    this.bUpdated=.F.
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- gscg_mpa
    this.InitSon()
    
    
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCODPAG_1_12.enabled = i_bVal
      .Page1.oPag.oPTCODAGE_2_28.enabled = i_bVal
      .Page1.oPag.oPTNUMCOR_2_29.enabled = i_bVal
      .Page1.oPag.oPTDESRIG_2_31.enabled = i_bVal
      .Page1.oPag.oBtn_1_4.enabled = i_bVal
      .Page1.oPag.oBtn_1_5.enabled = i_bVal
      .Page1.oPag.oBtn_2_23.enabled = i_bVal
      .Page1.oPag.oObj_1_17.enabled = i_bVal
      .Page1.oPag.oObj_1_18.enabled = i_bVal
      .Page1.oPag.oObj_1_23.enabled = i_bVal
      .Page1.oPag.oObj_1_24.enabled = i_bVal
      .Page1.oPag.oObj_1_26.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PAR_TITE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PTSERIAL,"PTSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PTROWORD,"PTROWORD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PTDATSCA D(8);
      ,t_PTCODVAL C(3);
      ,t_PTNUMPAR C(31);
      ,t_PT_SEGNO C(1);
      ,t_PTTOTIMP N(18,4);
      ,t_PTFLSOSP N(3);
      ,t_PTMODPAG C(10);
      ,t_PTBANAPP C(10);
      ,t_PTBANNOS C(15);
      ,t_PTCODAGE C(5);
      ,t_PTNUMCOR C(25);
      ,t_PTDESRIG C(50);
      ,CPROWNUM N(10);
      ,t_PTFLCRSA C(1);
      ,t_SEGNO C(1);
      ,t_PTCAOAPE N(12,7);
      ,t_PTCAOVAL N(12,7);
      ,t_PTNUMDOC N(15);
      ,t_PTALFDOC C(10);
      ,t_PTDATDOC D(8);
      ,t_PTIMPDOC N(18,4);
      ,t_PTTIPCON C(1);
      ,t_PTCODCON C(15);
      ,t_PTTOTABB N(18,4);
      ,t_PTDATAPE D(8);
      ,t_PTFLIMPE C(2);
      ,t_PTNUMDIS C(10);
      ,t_PTFLINDI C(1);
      ,t_DESBA C(35);
      ,t_PTNUMPRO N(4);
      ,t_DATOBSO D(8);
      ,t_PTSERRIF C(10);
      ,t_PTORDRIF N(4);
      ,t_PTNUMRIF N(3);
      ,t_PTFLVABD C(1);
      ,t_PTFLRAGG C(1);
      ,t_PTRIFIND C(10);
      ,t_PTDATREG D(8);
      ,t_DATSCA D(8);
      ,t_TIPPAG C(2);
      ,t_FLINSO C(1);
      ,t_RIFCES C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mpabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTDATSCA_2_2.controlsource=this.cTrsName+'.t_PTDATSCA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTCODVAL_2_5.controlsource=this.cTrsName+'.t_PTCODVAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTNUMPAR_2_7.controlsource=this.cTrsName+'.t_PTNUMPAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPT_SEGNO_2_8.controlsource=this.cTrsName+'.t_PT_SEGNO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTTOTIMP_2_9.controlsource=this.cTrsName+'.t_PTTOTIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLSOSP_2_10.controlsource=this.cTrsName+'.t_PTFLSOSP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTMODPAG_2_11.controlsource=this.cTrsName+'.t_PTMODPAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANAPP_2_12.controlsource=this.cTrsName+'.t_PTBANAPP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANNOS_2_17.controlsource=this.cTrsName+'.t_PTBANNOS'
    this.oPgFRm.Page1.oPag.oPTCODAGE_2_28.controlsource=this.cTrsName+'.t_PTCODAGE'
    this.oPgFRm.Page1.oPag.oPTNUMCOR_2_29.controlsource=this.cTrsName+'.t_PTNUMCOR'
    this.oPgFRm.Page1.oPag.oPTDESRIG_2_31.controlsource=this.cTrsName+'.t_PTDESRIG'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(83)
    this.AddVLine(301)
    this.AddVLine(327)
    this.AddVLine(458)
    this.AddVLine(498)
    this.AddVLine(594)
    this.AddVLine(695)
    this.AddVLine(819)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTDATSCA_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
      *
      * insert into PAR_TITE
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_TITE')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_TITE')
        i_cFldBody=" "+;
                  "(PTSERIAL,PTROWORD,PTFLCRSA,PTDATSCA,PTCAOAPE"+;
                  ",PTCODVAL,PTCAOVAL,PTNUMPAR,PT_SEGNO,PTTOTIMP"+;
                  ",PTFLSOSP,PTMODPAG,PTBANAPP,PTNUMDOC,PTALFDOC"+;
                  ",PTDATDOC,PTIMPDOC,PTBANNOS,PTTIPCON,PTCODCON"+;
                  ",PTTOTABB,PTDATAPE,PTFLIMPE,PTNUMDIS,PTFLINDI"+;
                  ",PTCODAGE,PTNUMCOR,PTDESRIG,PTNUMPRO,PTSERRIF"+;
                  ",PTORDRIF,PTNUMRIF,PTFLVABD,PTFLRAGG,PTRIFIND"+;
                  ",PTDATREG,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PTSERIAL)+","+cp_ToStrODBC(this.w_PTROWORD)+","+cp_ToStrODBC(this.w_PTFLCRSA)+","+cp_ToStrODBC(this.w_PTDATSCA)+","+cp_ToStrODBC(this.w_PTCAOAPE)+;
             ","+cp_ToStrODBC(this.w_PTCODVAL)+","+cp_ToStrODBC(this.w_PTCAOVAL)+","+cp_ToStrODBC(this.w_PTNUMPAR)+","+cp_ToStrODBC(this.w_PT_SEGNO)+","+cp_ToStrODBC(this.w_PTTOTIMP)+;
             ","+cp_ToStrODBC(this.w_PTFLSOSP)+","+cp_ToStrODBCNull(this.w_PTMODPAG)+","+cp_ToStrODBCNull(this.w_PTBANAPP)+","+cp_ToStrODBC(this.w_PTNUMDOC)+","+cp_ToStrODBC(this.w_PTALFDOC)+;
             ","+cp_ToStrODBC(this.w_PTDATDOC)+","+cp_ToStrODBC(this.w_PTIMPDOC)+","+cp_ToStrODBCNull(this.w_PTBANNOS)+","+cp_ToStrODBC(this.w_PTTIPCON)+","+cp_ToStrODBC(this.w_PTCODCON)+;
             ","+cp_ToStrODBC(this.w_PTTOTABB)+","+cp_ToStrODBC(this.w_PTDATAPE)+","+cp_ToStrODBC(this.w_PTFLIMPE)+","+cp_ToStrODBC(this.w_PTNUMDIS)+","+cp_ToStrODBC(this.w_PTFLINDI)+;
             ","+cp_ToStrODBCNull(this.w_PTCODAGE)+","+cp_ToStrODBCNull(this.w_PTNUMCOR)+","+cp_ToStrODBC(this.w_PTDESRIG)+","+cp_ToStrODBC(this.w_PTNUMPRO)+","+cp_ToStrODBC(this.w_PTSERRIF)+;
             ","+cp_ToStrODBC(this.w_PTORDRIF)+","+cp_ToStrODBC(this.w_PTNUMRIF)+","+cp_ToStrODBC(this.w_PTFLVABD)+","+cp_ToStrODBC(this.w_PTFLRAGG)+","+cp_ToStrODBC(this.w_PTRIFIND)+;
             ","+cp_ToStrODBC(this.w_PTDATREG)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_TITE')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_TITE')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PTSERIAL',this.w_PTSERIAL,'PTROWORD',this.w_PTROWORD)
        INSERT INTO (i_cTable) (;
                   PTSERIAL;
                  ,PTROWORD;
                  ,PTFLCRSA;
                  ,PTDATSCA;
                  ,PTCAOAPE;
                  ,PTCODVAL;
                  ,PTCAOVAL;
                  ,PTNUMPAR;
                  ,PT_SEGNO;
                  ,PTTOTIMP;
                  ,PTFLSOSP;
                  ,PTMODPAG;
                  ,PTBANAPP;
                  ,PTNUMDOC;
                  ,PTALFDOC;
                  ,PTDATDOC;
                  ,PTIMPDOC;
                  ,PTBANNOS;
                  ,PTTIPCON;
                  ,PTCODCON;
                  ,PTTOTABB;
                  ,PTDATAPE;
                  ,PTFLIMPE;
                  ,PTNUMDIS;
                  ,PTFLINDI;
                  ,PTCODAGE;
                  ,PTNUMCOR;
                  ,PTDESRIG;
                  ,PTNUMPRO;
                  ,PTSERRIF;
                  ,PTORDRIF;
                  ,PTNUMRIF;
                  ,PTFLVABD;
                  ,PTFLRAGG;
                  ,PTRIFIND;
                  ,PTDATREG;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PTSERIAL;
                  ,this.w_PTROWORD;
                  ,this.w_PTFLCRSA;
                  ,this.w_PTDATSCA;
                  ,this.w_PTCAOAPE;
                  ,this.w_PTCODVAL;
                  ,this.w_PTCAOVAL;
                  ,this.w_PTNUMPAR;
                  ,this.w_PT_SEGNO;
                  ,this.w_PTTOTIMP;
                  ,this.w_PTFLSOSP;
                  ,this.w_PTMODPAG;
                  ,this.w_PTBANAPP;
                  ,this.w_PTNUMDOC;
                  ,this.w_PTALFDOC;
                  ,this.w_PTDATDOC;
                  ,this.w_PTIMPDOC;
                  ,this.w_PTBANNOS;
                  ,this.w_PTTIPCON;
                  ,this.w_PTCODCON;
                  ,this.w_PTTOTABB;
                  ,this.w_PTDATAPE;
                  ,this.w_PTFLIMPE;
                  ,this.w_PTNUMDIS;
                  ,this.w_PTFLINDI;
                  ,this.w_PTCODAGE;
                  ,this.w_PTNUMCOR;
                  ,this.w_PTDESRIG;
                  ,this.w_PTNUMPRO;
                  ,this.w_PTSERRIF;
                  ,this.w_PTORDRIF;
                  ,this.w_PTNUMRIF;
                  ,this.w_PTFLVABD;
                  ,this.w_PTFLRAGG;
                  ,this.w_PTRIFIND;
                  ,this.w_PTDATREG;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_PTDATSCA) AND (t_PTTOTIMP<>0 OR t_PTTOTABB<>0 )AND NOT EMPTY(t_PTNUMPAR)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_TITE')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_TITE')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_PTDATSCA) AND (t_PTTOTIMP<>0 OR t_PTTOTABB<>0 )AND NOT EMPTY(t_PTNUMPAR)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PAR_TITE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_TITE')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PTFLCRSA="+cp_ToStrODBC(this.w_PTFLCRSA)+;
                     ",PTDATSCA="+cp_ToStrODBC(this.w_PTDATSCA)+;
                     ",PTCAOAPE="+cp_ToStrODBC(this.w_PTCAOAPE)+;
                     ",PTCODVAL="+cp_ToStrODBC(this.w_PTCODVAL)+;
                     ",PTCAOVAL="+cp_ToStrODBC(this.w_PTCAOVAL)+;
                     ",PTNUMPAR="+cp_ToStrODBC(this.w_PTNUMPAR)+;
                     ",PT_SEGNO="+cp_ToStrODBC(this.w_PT_SEGNO)+;
                     ",PTTOTIMP="+cp_ToStrODBC(this.w_PTTOTIMP)+;
                     ",PTFLSOSP="+cp_ToStrODBC(this.w_PTFLSOSP)+;
                     ",PTMODPAG="+cp_ToStrODBCNull(this.w_PTMODPAG)+;
                     ",PTBANAPP="+cp_ToStrODBCNull(this.w_PTBANAPP)+;
                     ",PTNUMDOC="+cp_ToStrODBC(this.w_PTNUMDOC)+;
                     ",PTALFDOC="+cp_ToStrODBC(this.w_PTALFDOC)+;
                     ",PTDATDOC="+cp_ToStrODBC(this.w_PTDATDOC)+;
                     ",PTIMPDOC="+cp_ToStrODBC(this.w_PTIMPDOC)+;
                     ",PTBANNOS="+cp_ToStrODBCNull(this.w_PTBANNOS)+;
                     ",PTTIPCON="+cp_ToStrODBC(this.w_PTTIPCON)+;
                     ",PTCODCON="+cp_ToStrODBC(this.w_PTCODCON)+;
                     ",PTTOTABB="+cp_ToStrODBC(this.w_PTTOTABB)+;
                     ",PTDATAPE="+cp_ToStrODBC(this.w_PTDATAPE)+;
                     ",PTFLIMPE="+cp_ToStrODBC(this.w_PTFLIMPE)+;
                     ",PTNUMDIS="+cp_ToStrODBC(this.w_PTNUMDIS)+;
                     ",PTFLINDI="+cp_ToStrODBC(this.w_PTFLINDI)+;
                     ",PTCODAGE="+cp_ToStrODBCNull(this.w_PTCODAGE)+;
                     ",PTNUMCOR="+cp_ToStrODBCNull(this.w_PTNUMCOR)+;
                     ",PTDESRIG="+cp_ToStrODBC(this.w_PTDESRIG)+;
                     ",PTNUMPRO="+cp_ToStrODBC(this.w_PTNUMPRO)+;
                     ",PTSERRIF="+cp_ToStrODBC(this.w_PTSERRIF)+;
                     ",PTORDRIF="+cp_ToStrODBC(this.w_PTORDRIF)+;
                     ",PTNUMRIF="+cp_ToStrODBC(this.w_PTNUMRIF)+;
                     ",PTFLVABD="+cp_ToStrODBC(this.w_PTFLVABD)+;
                     ",PTFLRAGG="+cp_ToStrODBC(this.w_PTFLRAGG)+;
                     ",PTRIFIND="+cp_ToStrODBC(this.w_PTRIFIND)+;
                     ",PTDATREG="+cp_ToStrODBC(this.w_PTDATREG)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_TITE')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PTFLCRSA=this.w_PTFLCRSA;
                     ,PTDATSCA=this.w_PTDATSCA;
                     ,PTCAOAPE=this.w_PTCAOAPE;
                     ,PTCODVAL=this.w_PTCODVAL;
                     ,PTCAOVAL=this.w_PTCAOVAL;
                     ,PTNUMPAR=this.w_PTNUMPAR;
                     ,PT_SEGNO=this.w_PT_SEGNO;
                     ,PTTOTIMP=this.w_PTTOTIMP;
                     ,PTFLSOSP=this.w_PTFLSOSP;
                     ,PTMODPAG=this.w_PTMODPAG;
                     ,PTBANAPP=this.w_PTBANAPP;
                     ,PTNUMDOC=this.w_PTNUMDOC;
                     ,PTALFDOC=this.w_PTALFDOC;
                     ,PTDATDOC=this.w_PTDATDOC;
                     ,PTIMPDOC=this.w_PTIMPDOC;
                     ,PTBANNOS=this.w_PTBANNOS;
                     ,PTTIPCON=this.w_PTTIPCON;
                     ,PTCODCON=this.w_PTCODCON;
                     ,PTTOTABB=this.w_PTTOTABB;
                     ,PTDATAPE=this.w_PTDATAPE;
                     ,PTFLIMPE=this.w_PTFLIMPE;
                     ,PTNUMDIS=this.w_PTNUMDIS;
                     ,PTFLINDI=this.w_PTFLINDI;
                     ,PTCODAGE=this.w_PTCODAGE;
                     ,PTNUMCOR=this.w_PTNUMCOR;
                     ,PTDESRIG=this.w_PTDESRIG;
                     ,PTNUMPRO=this.w_PTNUMPRO;
                     ,PTSERRIF=this.w_PTSERRIF;
                     ,PTORDRIF=this.w_PTORDRIF;
                     ,PTNUMRIF=this.w_PTNUMRIF;
                     ,PTFLVABD=this.w_PTFLVABD;
                     ,PTFLRAGG=this.w_PTFLRAGG;
                     ,PTRIFIND=this.w_PTRIFIND;
                     ,PTDATREG=this.w_PTDATREG;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_PTDATSCA) AND (t_PTTOTIMP<>0 OR t_PTTOTABB<>0 )AND NOT EMPTY(t_PTNUMPAR)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PAR_TITE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_PTDATSCA) AND (t_PTTOTIMP<>0 OR t_PTTOTABB<>0 )AND NOT EMPTY(t_PTNUMPAR)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,2,.t.)
          .w_CODVAL = .w_CODVAL
          .w_TIPCON = .w_TIPCON
          .w_CODCON = .w_CODCON
          .w_CAOVAL = .w_CAOVAL
          .w_DATAPE = .w_DATAPE
          .w_FLPART = .w_FLPART
        .DoRTCalc(9,9,.t.)
          .w_FLABAC = .w_FLABAC
          .w_OBTEST = .w_OBTEST
          .w_NUMDOC = .w_NUMDOC
          .w_ALFDOC = .w_ALFDOC
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
          .w_TOTDOC = .w_TOTDOC
          .w_RIGDOC = .w_RIGDOC
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .DoRTCalc(16,40,.t.)
          .w_FLNDOC = .w_FLNDOC
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        .DoRTCalc(42,42,.t.)
        if .o_PTBANAPP<>.w_PTBANAPP
          .link_2_29('Full')
        endif
        .DoRTCalc(44,44,.t.)
          .w_DECTOT = .w_DECTOT
        if .o_CODPAG<>.w_CODPAG
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(47,48,.t.)
          .w_OBTEST = i_DATSYS
        .DoRTCalc(50,56,.t.)
          .w_TIPDOC = .w_TIPDOC
        .DoRTCalc(58,58,.t.)
          .w_DATDOC = .w_DATDOC
        if .o_PTSERIAL<>.w_PTSERIAL
          .w_DATSCA = .w_PTDATSCA
        endif
        .DoRTCalc(61,61,.t.)
          .w_NUMCOR = .w_NUMCOR
          .w_BANNOS = .w_BANNOS
          .w_BANAPP = .w_BANAPP
          .w_CODAGE = .w_CODAGE
        if .o_PTNUMPAR<>.w_PTNUMPAR
          .w_NUMPAR = 'variabile usata per forzare la creazione di o_PTNUMPAR'
        endif
          .w_FLINSO = .w_FLINSO
          .w_RIFCES = .w_RIFCES
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_PTFLCRSA with this.w_PTFLCRSA
      replace t_SEGNO with this.w_SEGNO
      replace t_PTCAOAPE with this.w_PTCAOAPE
      replace t_PTCAOVAL with this.w_PTCAOVAL
      replace t_PTNUMDOC with this.w_PTNUMDOC
      replace t_PTALFDOC with this.w_PTALFDOC
      replace t_PTDATDOC with this.w_PTDATDOC
      replace t_PTIMPDOC with this.w_PTIMPDOC
      replace t_PTTIPCON with this.w_PTTIPCON
      replace t_PTCODCON with this.w_PTCODCON
      replace t_PTTOTABB with this.w_PTTOTABB
      replace t_PTDATAPE with this.w_PTDATAPE
      replace t_PTFLIMPE with this.w_PTFLIMPE
      replace t_PTNUMDIS with this.w_PTNUMDIS
      replace t_PTFLINDI with this.w_PTFLINDI
      replace t_DESBA with this.w_DESBA
      replace t_PTNUMPRO with this.w_PTNUMPRO
      replace t_DATOBSO with this.w_DATOBSO
      replace t_PTSERRIF with this.w_PTSERRIF
      replace t_PTORDRIF with this.w_PTORDRIF
      replace t_PTNUMRIF with this.w_PTNUMRIF
      replace t_PTFLVABD with this.w_PTFLVABD
      replace t_PTFLRAGG with this.w_PTFLRAGG
      replace t_PTRIFIND with this.w_PTRIFIND
      replace t_PTDATREG with this.w_PTDATREG
      replace t_DATSCA with this.w_DATSCA
      replace t_TIPPAG with this.w_TIPPAG
      replace t_FLINSO with this.w_FLINSO
      replace t_RIFCES with this.w_RIFCES
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODPAG_1_12.enabled = this.oPgFrm.Page1.oPag.oCODPAG_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTNUMPAR_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTNUMPAR_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPT_SEGNO_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPT_SEGNO_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTTOTIMP_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTTOTIMP_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTBANAPP_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTBANAPP_2_12.mCond()
    this.oPgFrm.Page1.oPag.oPTCODAGE_2_28.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPTCODAGE_2_28.mCond()
    this.oPgFrm.Page1.oPag.oPTDESRIG_2_31.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPTDESRIG_2_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_23.enabled =this.oPgFrm.Page1.oPag.oBtn_2_23.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODPAG_1_12.visible=!this.oPgFrm.Page1.oPag.oCODPAG_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_21.visible=!this.oPgFrm.Page1.oPag.oStr_1_21.mHide()
    this.oPgFrm.Page1.oPag.oDESPAG_1_22.visible=!this.oPgFrm.Page1.oPag.oDESPAG_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oStr_2_18.visible=!this.oPgFrm.Page1.oPag.oStr_2_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_2_26.visible=!this.oPgFrm.Page1.oPag.oStr_2_26.mHide()
    this.oPgFrm.Page1.oPag.oPTCODAGE_2_28.visible=!this.oPgFrm.Page1.oPag.oPTCODAGE_2_28.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gscg_mpa
    
    **Per le partite di saldo non si pu� modificare la scadenza e il numero partita altrimenti la procedura non riesce piu a legare la partita di saldo alla partita di crea
    IF cevent ='w_PTDATSCA Changed' AND this.w_PTFLCRSA='S'
         ah_errormsg ('Non � possibile modificare la data scadenza per le partite di saldo')
         this.w_PTDATSCA=this.o_PTDATSCA
    ENDIF
    
    IF cevent ='w_PTFLSOSP Changed'and this.oparentobject.w_PNFLGSTO='S'and this.w_PTFLCRSA='C' AND this.oparentobject.cFunction='Load'
       If this.w_PTFLSOSP='S'
         ah_errormsg ('Attenzione: verr� sospesa anche la partita di saldo')
       else
         ah_errormsg ('Attenzione: la sospensione della partita verr� meno anche per la partita di saldo')
       endif
    ENDIF
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODPAG
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_CODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_CODPAG))
          select PACODICE,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oCODPAG_1_12'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_CODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_CODPAG)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PTMODPAG
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
    i_lTable = "MOD_PAGA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2], .t., this.MOD_PAGA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTMODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMP',True,'MOD_PAGA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MPCODICE like "+cp_ToStrODBC(trim(this.w_PTMODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MPCODICE,MPTIPPAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MPCODICE',trim(this.w_PTMODPAG))
          select MPCODICE,MPTIPPAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTMODPAG)==trim(_Link_.MPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PTMODPAG) and !this.bDontReportError
            deferred_cp_zoom('MOD_PAGA','*','MPCODICE',cp_AbsName(oSource.parent,'oPTMODPAG_2_11'),i_cWhere,'GSAR_AMP',"Tipi di pagamento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MPTIPPAG";
                     +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',oSource.xKey(1))
            select MPCODICE,MPTIPPAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTMODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MPTIPPAG";
                   +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(this.w_PTMODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',this.w_PTMODPAG)
            select MPCODICE,MPTIPPAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTMODPAG = NVL(_Link_.MPCODICE,space(10))
      this.w_TIPPAG = NVL(_Link_.MPTIPPAG,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PTMODPAG = space(10)
      endif
      this.w_TIPPAG = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])+'\'+cp_ToStr(_Link_.MPCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_PAGA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTMODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MOD_PAGA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_11.MPCODICE as MPCODICE211"+ ",link_2_11.MPTIPPAG as MPTIPPAG211"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_11 on PAR_TITE.PTMODPAG=link_2_11.MPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_11"
          i_cKey=i_cKey+'+" and PAR_TITE.PTMODPAG=link_2_11.MPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PTBANAPP
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTBANAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_PTBANAPP)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_PTBANAPP))
          select BACODBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTBANAPP)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PTBANAPP) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oPTBANAPP_2_12'),i_cWhere,'GSAR_ABA',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTBANAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_PTBANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_PTBANAPP)
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTBANAPP = NVL(_Link_.BACODBAN,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_PTBANAPP = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTBANAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PTBANNOS
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTBANNOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_PTBANNOS)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_PTBANNOS))
          select BACODBAN,BADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTBANNOS)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_PTBANNOS)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_PTBANNOS)+"%");

            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PTBANNOS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oPTBANNOS_2_17'),i_cWhere,'GSTE_ACB',"Elenco conti banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTBANNOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_PTBANNOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_PTBANNOS)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTBANNOS = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBA = NVL(_Link_.BADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PTBANNOS = space(15)
      endif
      this.w_DESBA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTBANNOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_17.BACODBAN as BACODBAN217"+ ",link_2_17.BADESCRI as BADESCRI217"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_17 on PAR_TITE.PTBANNOS=link_2_17.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_17"
          i_cKey=i_cKey+'+" and PAR_TITE.PTBANNOS=link_2_17.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PTCODAGE
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_PTCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_PTCODAGE))
          select AGCODAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PTCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oPTCODAGE_2_28'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_PTCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_PTCODAGE)
            select AGCODAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PTCODAGE = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice agente inesistente oppure obsoleto")
        endif
        this.w_PTCODAGE = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_28(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_28.AGCODAGE as AGCODAGE228"+ ",link_2_28.AGDTOBSO as AGDTOBSO228"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_28 on PAR_TITE.PTCODAGE=link_2_28.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_28"
          i_cKey=i_cKey+'+" and PAR_TITE.PTCODAGE=link_2_28.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PTNUMCOR
  func Link_2_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CONTI_IDX,3]
    i_lTable = "BAN_CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2], .t., this.BAN_CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTNUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BAN_CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCONCOR like "+cp_ToStrODBC(trim(this.w_PTNUMCOR)+"%");
                   +" and CCTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   +" and CCCODCON="+cp_ToStrODBC(this.w_CODCON);
                   +" and CCCODBAN="+cp_ToStrODBC(this.w_PTBANAPP);

          i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCTIPCON',this.w_TIPCON;
                     ,'CCCODCON',this.w_CODCON;
                     ,'CCCODBAN',this.w_PTBANAPP;
                     ,'CCCONCOR',trim(this.w_PTNUMCOR))
          select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTNUMCOR)==trim(_Link_.CCCONCOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PTNUMCOR) and !this.bDontReportError
            deferred_cp_zoom('BAN_CONTI','*','CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR',cp_AbsName(oSource.parent,'oPTNUMCOR_2_29'),i_cWhere,'',"Elenco conti correnti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);
           .or. this.w_CODCON<>oSource.xKey(2);
           .or. this.w_PTBANAPP<>oSource.xKey(3);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto corrente incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                     +" from "+i_cTable+" "+i_lTable+" where CCCONCOR="+cp_ToStrODBC(oSource.xKey(4));
                     +" and CCTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     +" and CCCODCON="+cp_ToStrODBC(this.w_CODCON);
                     +" and CCCODBAN="+cp_ToStrODBC(this.w_PTBANAPP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPCON',oSource.xKey(1);
                       ,'CCCODCON',oSource.xKey(2);
                       ,'CCCODBAN',oSource.xKey(3);
                       ,'CCCONCOR',oSource.xKey(4))
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTNUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                   +" from "+i_cTable+" "+i_lTable+" where CCCONCOR="+cp_ToStrODBC(this.w_PTNUMCOR);
                   +" and CCTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   +" and CCCODCON="+cp_ToStrODBC(this.w_CODCON);
                   +" and CCCODBAN="+cp_ToStrODBC(this.w_PTBANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPCON',this.w_TIPCON;
                       ,'CCCODCON',this.w_CODCON;
                       ,'CCCODBAN',this.w_PTBANAPP;
                       ,'CCCONCOR',this.w_PTNUMCOR)
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTNUMCOR = NVL(_Link_.CCCONCOR,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_PTNUMCOR = space(25)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPCON,1)+'\'+cp_ToStr(_Link_.CCCODCON,1)+'\'+cp_ToStr(_Link_.CCCODBAN,1)+'\'+cp_ToStr(_Link_.CCCONCOR,1)
      cp_ShowWarn(i_cKey,this.BAN_CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTNUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCODPAG_1_12.value==this.w_CODPAG)
      this.oPgFrm.Page1.oPag.oCODPAG_1_12.value=this.w_CODPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAG_1_22.value==this.w_DESPAG)
      this.oPgFrm.Page1.oPag.oDESPAG_1_22.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oPTCODAGE_2_28.value==this.w_PTCODAGE)
      this.oPgFrm.Page1.oPag.oPTCODAGE_2_28.value=this.w_PTCODAGE
      replace t_PTCODAGE with this.oPgFrm.Page1.oPag.oPTCODAGE_2_28.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPTNUMCOR_2_29.value==this.w_PTNUMCOR)
      this.oPgFrm.Page1.oPag.oPTNUMCOR_2_29.value=this.w_PTNUMCOR
      replace t_PTNUMCOR with this.oPgFrm.Page1.oPag.oPTNUMCOR_2_29.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPTDESRIG_2_31.value==this.w_PTDESRIG)
      this.oPgFrm.Page1.oPag.oPTDESRIG_2_31.value=this.w_PTDESRIG
      replace t_PTDESRIG with this.oPgFrm.Page1.oPag.oPTDESRIG_2_31.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTDATSCA_2_2.value==this.w_PTDATSCA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTDATSCA_2_2.value=this.w_PTDATSCA
      replace t_PTDATSCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTDATSCA_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTCODVAL_2_5.value==this.w_PTCODVAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTCODVAL_2_5.value=this.w_PTCODVAL
      replace t_PTCODVAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTCODVAL_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTNUMPAR_2_7.value==this.w_PTNUMPAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTNUMPAR_2_7.value=this.w_PTNUMPAR
      replace t_PTNUMPAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTNUMPAR_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPT_SEGNO_2_8.value==this.w_PT_SEGNO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPT_SEGNO_2_8.value=this.w_PT_SEGNO
      replace t_PT_SEGNO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPT_SEGNO_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTTOTIMP_2_9.value==this.w_PTTOTIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTTOTIMP_2_9.value=this.w_PTTOTIMP
      replace t_PTTOTIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTTOTIMP_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLSOSP_2_10.RadioValue()==this.w_PTFLSOSP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLSOSP_2_10.SetRadio()
      replace t_PTFLSOSP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLSOSP_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTMODPAG_2_11.value==this.w_PTMODPAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTMODPAG_2_11.value=this.w_PTMODPAG
      replace t_PTMODPAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTMODPAG_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANAPP_2_12.value==this.w_PTBANAPP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANAPP_2_12.value=this.w_PTBANAPP
      replace t_PTBANAPP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANAPP_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANNOS_2_17.value==this.w_PTBANNOS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANNOS_2_17.value=this.w_PTBANNOS
      replace t_PTBANNOS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANNOS_2_17.value
    endif
    cp_SetControlsValueExtFlds(this,'PAR_TITE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODPAG))  and not(.w_FLPART='S')  and (.w_FLPART<>'S' AND NOT EMPTY(.w_CODCON))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCODPAG_1_12.SetFocus()
            i_bnoObbl = !empty(.w_CODPAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento incongruente o inesistente")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case not((.w_TIPPAG<>'BO' OR (.w_TIPPAG='BO' AND NOT EMPTY(.w_PTNUMCOR)) OR (.w_TIPPAG='BO' AND .w_PT_SEGNO='D')) OR .w_TIPCON<>'F')
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = cp_Translate("Attenzione: Inserire un Conto Corrente!")
        case   empty(.w_PTNUMPAR) and (.w_PTFLCRSA<>'S') and (NOT EMPTY(.w_PTDATSCA) AND (.w_PTTOTIMP<>0 OR .w_PTTOTABB<>0 )AND NOT EMPTY(.w_PTNUMPAR))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTNUMPAR_2_7
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(.w_PT_SEGNO $ "DA") and (.w_PTFLCRSA<>'S') and (NOT EMPTY(.w_PTDATSCA) AND (.w_PTTOTIMP<>0 OR .w_PTTOTABB<>0 )AND NOT EMPTY(.w_PTNUMPAR))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPT_SEGNO_2_8
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(.w_PTTOTIMP>0) and (.w_PTFLCRSA<>'S') and (NOT EMPTY(.w_PTDATSCA) AND (.w_PTTOTIMP<>0 OR .w_PTTOTABB<>0 )AND NOT EMPTY(.w_PTNUMPAR))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTTOTIMP_2_9
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Attenzione, impossibile inserire importi negativi")
        case   empty(.w_PTMODPAG) and (NOT EMPTY(.w_PTDATSCA) AND (.w_PTTOTIMP<>0 OR .w_PTTOTABB<>0 )AND NOT EMPTY(.w_PTNUMPAR))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTMODPAG_2_11
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire il codice tipo pagamento")
        case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)) and ((.w_FLPART<>'S' AND .w_TIPCON='C')) and not(empty(.w_PTCODAGE)) and (NOT EMPTY(.w_PTDATSCA) AND (.w_PTTOTIMP<>0 OR .w_PTTOTABB<>0 )AND NOT EMPTY(.w_PTNUMPAR))
          .oNewFocus=.oPgFrm.Page1.oPag.oPTCODAGE_2_28
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice agente inesistente oppure obsoleto")
      endcase
      if NOT EMPTY(.w_PTDATSCA) AND (.w_PTTOTIMP<>0 OR .w_PTTOTABB<>0 )AND NOT EMPTY(.w_PTNUMPAR)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PTSERIAL = this.w_PTSERIAL
    this.o_CODPAG = this.w_CODPAG
    this.o_PTDATSCA = this.w_PTDATSCA
    this.o_SEGNO = this.w_SEGNO
    this.o_PTNUMPAR = this.w_PTNUMPAR
    this.o_PTBANAPP = this.w_PTBANAPP
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_PTDATSCA) AND (t_PTTOTIMP<>0 OR t_PTTOTABB<>0 )AND NOT EMPTY(t_PTNUMPAR))
    select(i_nArea)
    return(i_bRes)
  Endfunc
  Func CanDeleteRow()
    local i_bRes
    i_bRes=t_FLINSO<>'S' or t_RIFCES<>'C'
    if !i_bRes
      cp_ErrorMsg("Impossibile eliminare riga da registrazione di contabilizzazione insoluti.","stop","")
    endif
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PTFLCRSA=space(1)
      .w_PTDATSCA=ctod("  /  /  ")
      .w_SEGNO=space(1)
      .w_PTCAOAPE=0
      .w_PTCODVAL=space(3)
      .w_PTCAOVAL=0
      .w_PTNUMPAR=space(31)
      .w_PT_SEGNO=space(1)
      .w_PTTOTIMP=0
      .w_PTFLSOSP=space(1)
      .w_PTMODPAG=space(10)
      .w_PTBANAPP=space(10)
      .w_PTNUMDOC=0
      .w_PTALFDOC=space(10)
      .w_PTDATDOC=ctod("  /  /  ")
      .w_PTIMPDOC=0
      .w_PTBANNOS=space(15)
      .w_PTTIPCON=space(1)
      .w_PTCODCON=space(15)
      .w_PTTOTABB=0
      .w_PTDATAPE=ctod("  /  /  ")
      .w_PTFLIMPE=space(2)
      .w_PTNUMDIS=space(10)
      .w_PTFLINDI=space(1)
      .w_PTCODAGE=space(5)
      .w_PTNUMCOR=space(25)
      .w_DESBA=space(35)
      .w_PTDESRIG=space(50)
      .w_PTNUMPRO=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_PTSERRIF=space(10)
      .w_PTORDRIF=0
      .w_PTNUMRIF=0
      .w_PTFLVABD=space(1)
      .w_PTFLRAGG=space(1)
      .w_PTRIFIND=space(10)
      .w_PTDATREG=ctod("  /  /  ")
      .w_DATSCA=ctod("  /  /  ")
      .w_TIPPAG=space(2)
      .w_FLINSO=space(1)
      .w_RIFCES=space(1)
      .DoRTCalc(1,16,.f.)
        .w_PTFLCRSA = .w_FLPART
      .DoRTCalc(18,19,.f.)
        .w_PTCAOAPE = .w_CAOVAL
        .w_PTCODVAL = .w_CODVAL
        .w_PTCAOVAL = .w_CAOVAL
      .DoRTCalc(23,23,.f.)
        .w_PT_SEGNO = .w_PT_SEGNO
      .DoRTCalc(25,25,.f.)
        .w_PTFLSOSP = ' '
      .DoRTCalc(27,27,.f.)
      if not(empty(.w_PTMODPAG))
        .link_2_11('Full')
      endif
        .w_PTBANAPP = .w_BANAPP
      .DoRTCalc(28,28,.f.)
      if not(empty(.w_PTBANAPP))
        .link_2_12('Full')
      endif
        .w_PTNUMDOC = .w_NUMDOC
        .w_PTALFDOC = .w_ALFDOC
        .w_PTDATDOC = .w_DATDOC
        .w_PTIMPDOC = IIF(.w_TOTDOC=0, .w_RIGDOC, .w_TOTDOC)
        .w_PTBANNOS = .w_BANNOS
      .DoRTCalc(33,33,.f.)
      if not(empty(.w_PTBANNOS))
        .link_2_17('Full')
      endif
        .w_PTTIPCON = .w_TIPCON
        .w_PTCODCON = .w_CODCON
        .w_PTTOTABB = 0
        .w_PTDATAPE = .w_DATAPE
      .DoRTCalc(38,39,.f.)
        .w_PTFLINDI = ' '
      .DoRTCalc(41,41,.f.)
        .w_PTCODAGE = .w_CODAGE
      .DoRTCalc(42,42,.f.)
      if not(empty(.w_PTCODAGE))
        .link_2_28('Full')
      endif
        .w_PTNUMCOR = .w_NUMCOR
      .DoRTCalc(43,43,.f.)
      if not(empty(.w_PTNUMCOR))
        .link_2_29('Full')
      endif
      .DoRTCalc(44,46,.f.)
        .w_PTDESRIG = .w_PTDESRIG
      .DoRTCalc(48,59,.f.)
        .w_DATSCA = .w_PTDATSCA
      .DoRTCalc(61,66,.f.)
        .w_FLINSO = .w_FLINSO
        .w_RIFCES = .w_RIFCES
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PTFLCRSA = t_PTFLCRSA
    this.w_PTDATSCA = t_PTDATSCA
    this.w_SEGNO = t_SEGNO
    this.w_PTCAOAPE = t_PTCAOAPE
    this.w_PTCODVAL = t_PTCODVAL
    this.w_PTCAOVAL = t_PTCAOVAL
    this.w_PTNUMPAR = t_PTNUMPAR
    this.w_PT_SEGNO = t_PT_SEGNO
    this.w_PTTOTIMP = t_PTTOTIMP
    this.w_PTFLSOSP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLSOSP_2_10.RadioValue(.t.)
    this.w_PTMODPAG = t_PTMODPAG
    this.w_PTBANAPP = t_PTBANAPP
    this.w_PTNUMDOC = t_PTNUMDOC
    this.w_PTALFDOC = t_PTALFDOC
    this.w_PTDATDOC = t_PTDATDOC
    this.w_PTIMPDOC = t_PTIMPDOC
    this.w_PTBANNOS = t_PTBANNOS
    this.w_PTTIPCON = t_PTTIPCON
    this.w_PTCODCON = t_PTCODCON
    this.w_PTTOTABB = t_PTTOTABB
    this.w_PTDATAPE = t_PTDATAPE
    this.w_PTFLIMPE = t_PTFLIMPE
    this.w_PTNUMDIS = t_PTNUMDIS
    this.w_PTFLINDI = t_PTFLINDI
    this.w_PTCODAGE = t_PTCODAGE
    this.w_PTNUMCOR = t_PTNUMCOR
    this.w_DESBA = t_DESBA
    this.w_PTDESRIG = t_PTDESRIG
    this.w_PTNUMPRO = t_PTNUMPRO
    this.w_DATOBSO = t_DATOBSO
    this.w_PTSERRIF = t_PTSERRIF
    this.w_PTORDRIF = t_PTORDRIF
    this.w_PTNUMRIF = t_PTNUMRIF
    this.w_PTFLVABD = t_PTFLVABD
    this.w_PTFLRAGG = t_PTFLRAGG
    this.w_PTRIFIND = t_PTRIFIND
    this.w_PTDATREG = t_PTDATREG
    this.w_DATSCA = t_DATSCA
    this.w_TIPPAG = t_TIPPAG
    this.w_FLINSO = t_FLINSO
    this.w_RIFCES = t_RIFCES
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PTFLCRSA with this.w_PTFLCRSA
    replace t_PTDATSCA with this.w_PTDATSCA
    replace t_SEGNO with this.w_SEGNO
    replace t_PTCAOAPE with this.w_PTCAOAPE
    replace t_PTCODVAL with this.w_PTCODVAL
    replace t_PTCAOVAL with this.w_PTCAOVAL
    replace t_PTNUMPAR with this.w_PTNUMPAR
    replace t_PT_SEGNO with this.w_PT_SEGNO
    replace t_PTTOTIMP with this.w_PTTOTIMP
    replace t_PTFLSOSP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLSOSP_2_10.ToRadio()
    replace t_PTMODPAG with this.w_PTMODPAG
    replace t_PTBANAPP with this.w_PTBANAPP
    replace t_PTNUMDOC with this.w_PTNUMDOC
    replace t_PTALFDOC with this.w_PTALFDOC
    replace t_PTDATDOC with this.w_PTDATDOC
    replace t_PTIMPDOC with this.w_PTIMPDOC
    replace t_PTBANNOS with this.w_PTBANNOS
    replace t_PTTIPCON with this.w_PTTIPCON
    replace t_PTCODCON with this.w_PTCODCON
    replace t_PTTOTABB with this.w_PTTOTABB
    replace t_PTDATAPE with this.w_PTDATAPE
    replace t_PTFLIMPE with this.w_PTFLIMPE
    replace t_PTNUMDIS with this.w_PTNUMDIS
    replace t_PTFLINDI with this.w_PTFLINDI
    replace t_PTCODAGE with this.w_PTCODAGE
    replace t_PTNUMCOR with this.w_PTNUMCOR
    replace t_DESBA with this.w_DESBA
    replace t_PTDESRIG with this.w_PTDESRIG
    replace t_PTNUMPRO with this.w_PTNUMPRO
    replace t_DATOBSO with this.w_DATOBSO
    replace t_PTSERRIF with this.w_PTSERRIF
    replace t_PTORDRIF with this.w_PTORDRIF
    replace t_PTNUMRIF with this.w_PTNUMRIF
    replace t_PTFLVABD with this.w_PTFLVABD
    replace t_PTFLRAGG with this.w_PTFLRAGG
    replace t_PTRIFIND with this.w_PTRIFIND
    replace t_PTDATREG with this.w_PTDATREG
    replace t_DATSCA with this.w_DATSCA
    replace t_TIPPAG with this.w_TIPPAG
    replace t_FLINSO with this.w_FLINSO
    replace t_RIFCES with this.w_RIFCES
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mpaPag1 as StdContainer
  Width  = 862
  height = 210
  stdWidth  = 862
  stdheight = 210
  resizeXpos=421
  resizeYpos=86
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="HBNWIGPNUX",left=2, top=3, width=858,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    nNumberColumn=9,Field1="PTDATSCA",Label1="Scadenza",Field2="PTNUMPAR",Label2="Numero partita",Field3="PT_SEGNO",Label3="D/A",Field4="PTTOTIMP",Label4="Importo",Field5="SIMBVA",Label5="Valuta",Field6="PTMODPAG",Label6="Pagamento",Field7="PTBANAPP",Label7="Banca appoggio",Field8="PTBANNOS",Label8="Ns.banca",Field9="PTFLSOSP",Label9="Sosp.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 30532070


  add object oBtn_1_4 as StdButton with uid="XVFSKZRQBE",left=757, top=160, width=48,height=45,;
    CpPicture="BMP\CALCOLA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per calcolo automatico partite";
    , HelpContextID = 177209382;
    , tabstop=.f., Caption='\<Calcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        do GSCG_BPP with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    with this.Parent.oContained
      return (.w_FLPART<>'S' and (.w_FLINSO <>'S' or .w_RIFCES<>'C'))
    endwith
  endfunc


  add object oBtn_1_5 as StdButton with uid="RJRXLUPBSZ",left=808, top=160, width=48,height=45,;
    CpPicture="BMP\ScadTL.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per chiudere le partite";
    , HelpContextID = 38731482;
    , tabstop=.f., Caption='\<Salda';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        GSCG_BSP(this.Parent.oContained,"P")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    with this.Parent.oContained
      return (.w_FLPART='S' AND LEFT(.w_FLABAC, 2)<>'AB')
    endwith
  endfunc

  add object oCODPAG_1_12 as StdField with uid="ZKIPGVQVIJ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODPAG", cQueryName = "CODPAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento incongruente o inesistente",;
    ToolTipText = "Codice pagamento utilizzato per movimenti con IVA o calcoli scadenze",;
    HelpContextID = 224760282,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=155, Top=137, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_CODPAG"

  func oCODPAG_1_12.mCond()
    with this.Parent.oContained
      return (.w_FLPART<>'S' AND NOT EMPTY(.w_CODCON))
    endwith
  endfunc

  func oCODPAG_1_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLPART='S')
    endwith
    endif
  endfunc

  func oCODPAG_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPAG_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPAG_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oCODPAG_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oCODPAG_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_CODPAG
    i_obj.ecpSave()
  endproc


  add object oObj_1_17 as cp_runprogram with uid="DCOOBJWJVA",left=252, top=-48, width=152,height=25,;
    caption='GSCG_BSP(P)',;
   bGlobalFont=.t.,;
    prg="GSCG_BSP('P')",;
    cEvent = "Blank,Acconti",;
    nPag=1;
    , HelpContextID = 9157322


  add object oObj_1_18 as cp_runprogram with uid="TBACEPEYNI",left=8, top=254, width=219,height=26,;
    caption='GSCG_BCP',;
   bGlobalFont=.t.,;
    prg="GSCG_BCP",;
    cEvent = "ChiudePartite",;
    nPag=1;
    , HelpContextID = 9346378

  add object oDESPAG_1_22 as StdField with uid="HLPSRBBGWP",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 224701386,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=219, Top=137, InputMask=replicate('X',30)

  func oDESPAG_1_22.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLPART='S')
    endwith
    endif
  endfunc


  add object oObj_1_23 as cp_runprogram with uid="WHYEAIWPMF",left=7, top=-48, width=243,height=25,;
    caption='GSCG_BPD',;
   bGlobalFont=.t.,;
    prg="GSCG_BPD",;
    cEvent = "w_PTDATSCA Changed",;
    nPag=1;
    , HelpContextID = 9346390


  add object oObj_1_24 as cp_runprogram with uid="XSJOQKJDAB",left=8, top=282, width=218,height=26,;
    caption='GSCG_BKP(P,QUERY)',;
   bGlobalFont=.t.,;
    prg="GSCG_BKP('P','Query')",;
    cEvent = "Delete row start",;
    nPag=1;
    , HelpContextID = 99256289


  add object oObj_1_26 as cp_runprogram with uid="UGYRJSJBRF",left=8, top=311, width=219,height=26,;
    caption='gscg_bkp(P,Edit)',;
   bGlobalFont=.t.,;
    prg="GSCG_BKP('P','Edit')",;
    cEvent = "Update row start",;
    nPag=1;
    , HelpContextID = 253466495

  add object oStr_1_21 as StdString with uid="PURPWJQPPP",Visible=.t., Left=8, Top=138,;
    Alignment=1, Width=144, Height=18,;
    Caption="Pagamento primanota:"  ;
  , bGlobalFont=.t.

  func oStr_1_21.mHide()
    with this.Parent.oContained
      return (.w_FLPART='S')
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="GVGIGOEIQF",Visible=.t., Left=8, Top=186,;
    Alignment=1, Width=144, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="ODNXLEQWAI",Visible=.t., Left=492, Top=138,;
    Alignment=1, Width=126, Height=18,;
    Caption="Codice agente:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (.w_PTTIPCON='F' OR .w_PTTIPCON='G')
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="HJKNITARLJ",Visible=.t., Left=1, Top=-123,;
    Alignment=0, Width=824, Height=19,;
    Caption="Attenzione, per tutti i cambiamenti dimensionali della maschera deve essere adeguata l'area manuale function e procedure"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_34 as StdString with uid="RJMJYBAFOA",Visible=.t., Left=56, Top=160,;
    Alignment=1, Width=96, Height=18,;
    Caption="Conto corrente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="HODHTSGGFC",Visible=.t., Left=1, Top=-101,;
    Alignment=0, Width=587, Height=19,;
    Caption="Attenzione, aggiungere i campi anche nella corrispondente gestione dello storico"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=19,;
    width=849+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=20,width=848+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='MOD_PAGA|BAN_CHE|COC_MAST|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oPTCODAGE_2_28.Refresh()
      this.Parent.oPTNUMCOR_2_29.Refresh()
      this.Parent.oPTDESRIG_2_31.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='MOD_PAGA'
        oDropInto=this.oBodyCol.oRow.oPTMODPAG_2_11
      case cFile='BAN_CHE'
        oDropInto=this.oBodyCol.oRow.oPTBANAPP_2_12
      case cFile='COC_MAST'
        oDropInto=this.oBodyCol.oRow.oPTBANNOS_2_17
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_23 as StdButton with uid="YFOQHFFGJL",width=48,height=45,;
   left=706, top=160,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alla situazione della partita selezionata";
    , HelpContextID = 141597642;
    , tabstop=.f., Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_23.Click()
      do GSCG_KPA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_23.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_PTNUMPAR))
    endwith
  endfunc

  add object oPTCODAGE_2_28 as StdTrsField with uid="YGIGUOPYLD",rtseq=42,rtrep=.t.,;
    cFormVar="w_PTCODAGE",value=space(5),;
    HelpContextID = 214524987,;
    cTotal="", bFixedPos=.t., cQueryName = "PTCODAGE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice agente inesistente oppure obsoleto",;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=622, Top=137, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_PTCODAGE"

  func oPTCODAGE_2_28.mCond()
    with this.Parent.oContained
      return ((.w_FLPART<>'S' AND .w_TIPCON='C'))
    endwith
  endfunc

  func oPTCODAGE_2_28.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PTTIPCON='F' OR .w_PTTIPCON='G')
    endwith
    endif
  endfunc

  func oPTCODAGE_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oPTCODAGE_2_28.ecpDrop(oSource)
    this.Parent.oContained.link_2_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPTCODAGE_2_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oPTCODAGE_2_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oPTCODAGE_2_28.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_PTCODAGE
    i_obj.ecpSave()
  endproc

  add object oPTNUMCOR_2_29 as StdTrsField with uid="TSPNFCBXSM",rtseq=43,rtrep=.t.,;
    cFormVar="w_PTNUMCOR",value=space(25),;
    ToolTipText = "Conto corrente",;
    HelpContextID = 10480568,;
    cTotal="", bFixedPos=.t., cQueryName = "PTNUMCOR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Conto corrente incongruente",;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=155, Top=160, InputMask=replicate('X',25), bHasZoom = .t. , cLinkFile="BAN_CONTI", oKey_1_1="CCTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="CCCODCON", oKey_2_2="this.w_CODCON", oKey_3_1="CCCODBAN", oKey_3_2="this.w_PTBANAPP", oKey_4_1="CCCONCOR", oKey_4_2="this.w_PTNUMCOR"

  func oPTNUMCOR_2_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oPTNUMCOR_2_29.ecpDrop(oSource)
    this.Parent.oContained.link_2_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPTNUMCOR_2_29.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BAN_CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODCON="+cp_ToStrODBC(this.Parent.oContained.w_CODCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODBAN="+cp_ToStrODBC(this.Parent.oContained.w_PTBANAPP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODCON="+cp_ToStr(this.Parent.oContained.w_CODCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODBAN="+cp_ToStr(this.Parent.oContained.w_PTBANAPP)
    endif
    do cp_zoom with 'BAN_CONTI','*','CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR',cp_AbsName(this.parent,'oPTNUMCOR_2_29'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco conti correnti",'',this.parent.oContained
  endproc

  add object oPTDESRIG_2_31 as StdTrsField with uid="RPKIVRTWNK",rtseq=47,rtrep=.t.,;
    cFormVar="w_PTDESRIG",value=space(50),;
    ToolTipText = "Eventuali note aggiuntive di riga",;
    HelpContextID = 246379581,;
    cTotal="", bFixedPos=.t., cQueryName = "PTDESRIG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=155, Top=184, InputMask=replicate('X',50)

  func oPTDESRIG_2_31.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_PTDATSCA) AND NOT EMPTY(.w_PTNUMPAR))
    endwith
  endfunc

  add object oStr_2_18 as StdString with uid="FINJOGPVUU",Visible=.t., Left=703, Top=137,;
    Alignment=0, Width=65, Height=15,;
    Caption="(Abbuono)"  ;
  , bGlobalFont=.t.

  func oStr_2_18.mHide()
    with this.Parent.oContained
      return (LEFT(.w_FLABAC, 2)<>'AB')
    endwith
  endfunc

  add object oStr_2_26 as StdString with uid="YQXRRRFFHY",Visible=.t., Left=773, Top=137,;
    Alignment=0, Width=78, Height=15,;
    Caption="(Distinta)"  ;
  , bGlobalFont=.t.

  func oStr_2_26.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_PTNUMDIS))
    endwith
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_mpaBodyRow as CPBodyRowCnt
  Width=839
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPTDATSCA_2_2 as StdTrsField with uid="EWPIGXSGAB",rtseq=18,rtrep=.t.,;
    cFormVar="w_PTDATSCA",value=ctod("  /  /  "),;
    ToolTipText = "Data della scadenza",;
    HelpContextID = 263943223,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=-2, Top=0

  add object oPTCODVAL_2_5 as StdTrsField with uid="SMUQEZWXXA",rtseq=21,rtrep=.t.,;
    cFormVar="w_PTCODVAL",value=space(3),enabled=.f.,;
    ToolTipText = "Codice valuta della partita",;
    HelpContextID = 238459838,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=452, Top=0, InputMask=replicate('X',3)

  add object oPTNUMPAR_2_7 as StdTrsField with uid="SIQEUCVZNC",rtseq=23,rtrep=.t.,;
    cFormVar="w_PTNUMPAR",value=space(31),;
    ToolTipText = "Numero della partita",;
    HelpContextID = 60812216,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=216, Left=76, Top=0, InputMask=replicate('X',31)

  func oPTNUMPAR_2_7.mCond()
    with this.Parent.oContained
      return (.w_PTFLCRSA<>'S')
    endwith
  endfunc

  add object oPT_SEGNO_2_8 as StdTrsField with uid="ZXKFMPBPHG",rtseq=24,rtrep=.t.,;
    cFormVar="w_PT_SEGNO",value=space(1),;
    ToolTipText = "Sezione importo partita: D= dare; A =avere",;
    HelpContextID = 220257211,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=23, Left=295, Top=0, cSayPict=["!"], cGetPict=["!"], InputMask=replicate('X',1)

  func oPT_SEGNO_2_8.mCond()
    with this.Parent.oContained
      return (.w_PTFLCRSA<>'S')
    endwith
  endfunc

  func oPT_SEGNO_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PT_SEGNO $ "DA")
    endwith
    return bRes
  endfunc

  add object oPTTOTIMP_2_9 as StdTrsField with uid="DTEXZJIRUO",rtseq=25,rtrep=.t.,;
    cFormVar="w_PTTOTIMP",value=0,;
    ToolTipText = "Importo della scadenza",;
    HelpContextID = 171281338,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, impossibile inserire importi negativi",;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=321, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oPTTOTIMP_2_9.mCond()
    with this.Parent.oContained
      return (.w_PTFLCRSA<>'S')
    endwith
  endfunc

  func oPTTOTIMP_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PTTOTIMP>0)
    endwith
    return bRes
  endfunc

  add object oPTFLSOSP_2_10 as StdTrsCheck with uid="TUEZHLHOAL",rtrep=.t.,;
    cFormVar="w_PTFLSOSP",  caption="",;
    ToolTipText = "Se attivo: partita sospesa",;
    HelpContextID = 196514886,;
    Left=812, Top=0, Width=22,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oPTFLSOSP_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PTFLSOSP,&i_cF..t_PTFLSOSP),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oPTFLSOSP_2_10.GetRadio()
    this.Parent.oContained.w_PTFLSOSP = this.RadioValue()
    return .t.
  endfunc

  func oPTFLSOSP_2_10.ToRadio()
    this.Parent.oContained.w_PTFLSOSP=trim(this.Parent.oContained.w_PTFLSOSP)
    return(;
      iif(this.Parent.oContained.w_PTFLSOSP=='S',1,;
      0))
  endfunc

  func oPTFLSOSP_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPTMODPAG_2_11 as StdTrsField with uid="WJGSXUNOIY",rtseq=27,rtrep=.t.,;
    cFormVar="w_PTMODPAG",value=space(10),;
    ToolTipText = "Codice tipo di pagamento",;
    HelpContextID = 70646723,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire il codice tipo pagamento",;
   bGlobalFont=.t.,;
    Height=17, Width=93, Left=492, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_PAGA", cZoomOnZoom="GSAR_AMP", oKey_1_1="MPCODICE", oKey_1_2="this.w_PTMODPAG"

  func oPTMODPAG_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oPTMODPAG_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPTMODPAG_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_PAGA','*','MPCODICE',cp_AbsName(this.parent,'oPTMODPAG_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMP',"Tipi di pagamento",'',this.parent.oContained
  endproc
  proc oPTMODPAG_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MPCODICE=this.parent.oContained.w_PTMODPAG
    i_obj.ecpSave()
  endproc

  add object oPTBANAPP_2_12 as StdTrsField with uid="MTMLWWKVTI",rtseq=28,rtrep=.t.,;
    cFormVar="w_PTBANAPP",value=space(10),;
    ToolTipText = "banca di appoggio del cliente/fornitore",;
    HelpContextID = 44346298,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=98, Left=587, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_PTBANAPP"

  func oPTBANAPP_2_12.mCond()
    with this.Parent.oContained
      return (.w_PTTIPCON $ 'CF')
    endwith
  endfunc

  func oPTBANAPP_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
      if .not. empty(.w_PTNUMCOR)
        bRes2=.link_2_29('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPTBANAPP_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPTBANAPP_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oPTBANAPP_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"Elenco banche",'',this.parent.oContained
  endproc
  proc oPTBANAPP_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_PTBANAPP
    i_obj.ecpSave()
  endproc

  add object oPTBANNOS_2_17 as StdTrsField with uid="DFFYWBRPJS",rtseq=33,rtrep=.t.,;
    cFormVar="w_PTBANNOS",value=space(15),;
    ToolTipText = "Nostro C/C. di appoggio per bonifici e RB",;
    HelpContextID = 94677943,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=120, Left=690, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_PTBANNOS"

  func oPTBANNOS_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oPTBANNOS_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPTBANNOS_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oPTBANNOS_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Elenco conti banche",'',this.parent.oContained
  endproc
  proc oPTBANNOS_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_PTBANNOS
    i_obj.ecpSave()
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oPTDATSCA_2_2.When()
    return(.t.)
  proc oPTDATSCA_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPTDATSCA_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mpa','PAR_TITE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PTSERIAL=PAR_TITE.PTSERIAL";
  +" and "+i_cAliasName2+".PTROWORD=PAR_TITE.PTROWORD";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_mpa
define class StdPcDetailPartite as StdPCTrsContainer
  proc LoadAllTrs(i_cRowID,i_cRow,i_cCurs,i_LoadContext,i_cNomTab,i_k1v,i_k1f,i_k2v,i_k2f,i_k3v,i_k3f,i_k4v,i_k4f,i_k5v,i_k5f,i_k6v,i_k6f,i_k7v,i_k7f,i_k8v,i_k8f,i_k9v,i_k9f,i_k10v,i_k10f)
    * --- Metodo per interrogare il Transitorio delle movimentazioni figlie integrate in un detail
    * --- partendo da un qualsiasi punto del detail
    * --- Parametri:
    * --- i_cRowID - Valore di .cRowID del Padre
    * --- i_cRow - recno() del cursore padre
    * --- i_cCurs - Nome cursore contenente il valore del Transitorio
    * --- i_LoadContext - Bool, se .T. carica e legge anche i figli non istanziati ; .f. legge solo i figli non istanziati.
    * ---- I parametri successivi se i_LoadContext=.f. non sono necessari
    * --- i_cNomTab - Nome della tabella da istanziare
    * --- i_k1v,  ... - Valori delle Chiavi da ricercare
    * --- i_k1f,  ... - Nomi dei campi Chiave da utilizzare per la ricerca
    local i_cSel,i_nNextRow,i,j,n,v,t,l,p,i_fn
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn
    local i_a, i_TrsChar
    i_TrsChar=.f.
    i_cSel=select()
    i_nNextRow=this.GetKeyPos(i_cRowID+str(i_cRow,7,0))
    if i_nNextRow<>0 and type("this.oSaveStatus[i_nNextRow]")<>'O' and i_LoadContext=.t.
      * --- Crea il nuovo  context
      i_TrsChar=.t.
      if alen(this.oSaveStatus)<i_nNextRow
        dimension this.oSaveStatus[i_nNextRow]
      endif
      this.oSaveStatus[i_nNextRow]=this.NewContext()
      * --- Setta la Chiave di ricerca
      this.SetKey(i_k1v,i_k1f,i_k2v,i_k2f,i_k3v,i_k3f,i_k4v,i_k4f,i_k5v,i_k5f,i_k6v,i_k6f,i_k7v,i_k7f,i_k8v,i_k8f,i_k9v,i_k9f,i_k10v,i_k10f)
      * --- Carica il Transitorio (Metodo derivato dalla LoadRec)
      i_cTF = this.cCursor
      n='this.'+i_cNomTab+'_IDX'
      i_nConn = i_TableProp[&n,3]
      i_cTable = cp_SetAzi(i_TableProp[&n,2])
      i_cKey = this.cKeyWhereODBC
      i_nRes = cp_SQL(i_nConn,"select * from "+i_cTable+" where "+&i_cKey,this.cCursor)
      if i_nRes<>-1 and not(eof())
        * --- Leggo il Cursore e creo il transitorio
        select (this.cTrsName)
        zap
        dimension i_a[1]
		select (this.cCursor)
		go top
        n=afields(i_a)
        scan
          for i=1 to n
            v=i_a[i,1]
            t=i_a[i,2]
            l=i_a[i,3]
            p='this.w_'+v
            if type(p)<>'U'
              do case
                case t = 'C'
                  &p=NVL(&v, SPACE(l))
                case t $ 'DT'
                  &p=CP_TODATE(&v)
                case t $ 'NFIBY'
                  &p=NVL(&v, 0)
                case t $ 'MG'
                  &p=NVL(&v, '')
                case t = 'L'
                  &p=NVL(&v, .f.)
              endcase
            endif
          endfor
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          this.TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          select (this.cCursor)
        endscan
	  endif
    endif
    * --- Crea e Apre cursore di Lavoro della stessa struttura di cCursor
    if type("this.oSaveStatus[i_nNextRow]")='O' and used(this.cTrsName)
      if not used(i_cCurs)
        * -- Se oggetto gia' istanziato copia dall'array
        select (this.cTrsName)
        * -- creo l'oggetto nella cartella temporanea (%TEMP%)
        copy to (tempadhoc()+'\'+i_cCurs) while .f.
        use (tempadhoc()+'\'+i_cCurs) in 0
      endif
      select (i_cCurs)
      if i_TrsChar=.f.
        * --- carica un cursore da un array, anche con i campi MEMO!
	      this.oSaveStatus[i_nNextRow].LoadCursor(3, i_cCurs )
      else
        select (this.cTrsName)
        go top
        scan
          scatter memvar
          select (i_cCurs)
 		  append blank
 		  gather memvar
          select (this.cTrsName)
        endscan
      endif
    endif
    select (i_cSel)
    return
  endproc
enddefine

* --- Class definition
define class tgscg_mpa as StdPCForm
  Width  = 888
  Height = 214
  Top    = 234
  Left   = 5
  cComment = "Situazione partite"
  HelpContextID=147465577
  add object cnt as tcgscg_mpa
  proc ecpSave()
  DoDefault()
  This.cnt.oParentObject.w_PNCODPAG=This.cnt.w_CODPAG
  THIS.cnt.oParentObject.w_DESCRI = this.cnt.w_DESPAG
  SELECT (THIS.cnt.oParentObject.cTrsName)
  if i_SRV<>'A'
    replace i_SRV with 'U'
  endif
  REPLACE t_PNCODPAG with This.cnt.w_CODPAG
  REPLACE t_DESCRI with This.cnt.w_DESPAG

  EndProc
  proc ecpQuit()
    DoDefault()
    if this.cnt.bUpdated
     This.cnt.oParentObject.w_PNCODPAG=This.cnt.w_CODPAG
     This.cnt.oParentObject.w_DESCRI = this.cnt.w_DESPAG
     SELECT (THIS.cnt.oParentObject.cTrsName)
      if i_SRV<>'A'
       replace i_SRV with 'U'
      endif
      REPLACE t_PNCODPAG with This.cnt.w_CODPAG
      REPLACE t_DESCRI with This.cnt.w_DESPAG
    ENDIF
   ENDPROC
enddefine

* --- Fine Area Manuale
