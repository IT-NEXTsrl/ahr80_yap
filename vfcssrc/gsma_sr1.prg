* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_sr1                                                        *
*              Aggiornamento progressivi registro beni                         *
*                                                                              *
*      Author: Zucchetti  AT                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-12-20                                                      *
* Last revis.: 2008-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_sr1",oParentObject))

* --- Class definition
define class tgsma_sr1 as StdForm
  Top    = 65
  Left   = 103

  * --- Standard Properties
  Width  = 396
  Height = 218
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-05-31"
  HelpContextID=107641705
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsma_sr1"
  cComment = "Aggiornamento progressivi registro beni"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DATARB = ctod('  /  /  ')
  w_CONFERMA = space(1)
  w_ULTPAG = 0
  w_CONTPAG = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_sr1Pag1","gsma_sr1",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCONFERMA_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATARB=ctod("  /  /  ")
      .w_CONFERMA=space(1)
      .w_ULTPAG=0
      .w_CONTPAG=space(1)
      .w_DATARB=oParentObject.w_DATARB
      .w_CONFERMA=oParentObject.w_CONFERMA
      .w_ULTPAG=oParentObject.w_ULTPAG
      .w_CONTPAG=oParentObject.w_CONTPAG
          .DoRTCalc(1,1,.f.)
        .w_CONFERMA = 'N'
    endwith
    this.DoRTCalc(3,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_DATARB=.w_DATARB
      .oParentObject.w_CONFERMA=.w_CONFERMA
      .oParentObject.w_ULTPAG=.w_ULTPAG
      .oParentObject.w_CONTPAG=.w_CONTPAG
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oULTPAG_1_11.visible=!this.oPgFrm.Page1.oPag.oULTPAG_1_11.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATARB_1_1.value==this.w_DATARB)
      this.oPgFrm.Page1.oPag.oDATARB_1_1.value=this.w_DATARB
    endif
    if not(this.oPgFrm.Page1.oPag.oCONFERMA_1_6.value==this.w_CONFERMA)
      this.oPgFrm.Page1.oPag.oCONFERMA_1_6.value=this.w_CONFERMA
    endif
    if not(this.oPgFrm.Page1.oPag.oULTPAG_1_11.value==this.w_ULTPAG)
      this.oPgFrm.Page1.oPag.oULTPAG_1_11.value=this.w_ULTPAG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CONFERMA)) or not(.w_CONFERMA $ 'SN'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONFERMA_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CONFERMA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare S o N per confermare o meno l'elaborazione")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsma_sr1Pag1 as StdContainer
  Width  = 392
  height = 218
  stdWidth  = 392
  stdheight = 218
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATARB_1_1 as StdField with uid="MFESDUMVVY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATARB", cQueryName = "DATARB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 251917770,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=226, Top=116

  add object oCONFERMA_1_6 as StdField with uid="QFQDLDHBZI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CONFERMA", cQueryName = "CONFERMA",;
    bObbl = .t. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare S o N per confermare o meno l'elaborazione",;
    ToolTipText = "S per confermare N per annullare",;
    HelpContextID = 265242521,;
   bGlobalFont=.t.,;
    Height=21, Width=16, Left=226, Top=189, cSayPict='"!"', cGetPict='"!"', InputMask=replicate('X',1)

  func oCONFERMA_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CONFERMA $ 'SN')
    endwith
    return bRes
  endfunc


  add object oBtn_1_8 as StdButton with uid="ONDYQSNNYY",left=339, top=169, width=48,height=45,;
    CpPicture="bmp\ok.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 107612954;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oULTPAG_1_11 as StdField with uid="ILVWWITNSA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ULTPAG", cQueryName = "ULTPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 184871354,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=226, Top=141

  func oULTPAG_1_11.mHide()
    with this.Parent.oContained
      return (.w_CONTPAG<>'S')
    endwith
  endfunc

  add object oStr_1_2 as StdString with uid="ZMKMCKTMAK",Visible=.t., Left=39, Top=65,;
    Alignment=0, Width=325, Height=15,;
    Caption="L'aggiornamento dei progressivi deve essere eseguito"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="WHQQIZHHNG",Visible=.t., Left=39, Top=82,;
    Alignment=0, Width=260, Height=18,;
    Caption="dopo la stampa definitiva."  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="XGVSUTQUXD",Visible=.t., Left=39, Top=41,;
    Alignment=0, Width=75, Height=15,;
    Caption="Attenzione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="TVTUQAASPQ",Visible=.t., Left=61, Top=116,;
    Alignment=1, Width=162, Height=15,;
    Caption="Ultima data stampata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="HPPPAKLZVR",Visible=.t., Left=39, Top=189,;
    Alignment=1, Width=179, Height=15,;
    Caption="Confermi aggiornamento (S/N)?:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="XWKLVDCZPP",Visible=.t., Left=71, Top=7,;
    Alignment=2, Width=242, Height=18,;
    Caption="REGISTRO BENI IN LAVORAZIONE"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="WSCFIDMNAK",Visible=.t., Left=77, Top=143,;
    Alignment=1, Width=146, Height=18,;
    Caption="Ultima pagina stampata:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_CONTPAG<>'S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_sr1','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
