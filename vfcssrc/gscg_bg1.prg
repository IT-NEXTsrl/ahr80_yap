* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bg1                                                        *
*              Funzioni piano mov. esig. diff.                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_96]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-06-26                                                      *
* Last revis.: 2016-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo,pSerial
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bg1",oParentObject,m.pTipo,m.pSerial)
return(i_retval)

define class tgscg_bg1 as StdBatch
  * --- Local variables
  pTipo = space(1)
  pSerial = space(10)
  w_SERIAL = space(10)
  w_SERINC = space(10)
  w_MESS = space(50)
  w_TEST = .f.
  w_PROG = .NULL.
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_CODCON = space(5)
  w_TIPCON = space(1)
  w_CODESE = space(4)
  w_OK = .f.
  w_DPTIPREG = space(1)
  w_NUM_REC = 0
  w_CODAZI = space(5)
  w_STALIG = ctod("  /  /  ")
  * --- WorkFile variables
  ESI_DETT_idx=0
  PNT_IVA_idx=0
  PNT_DETT_idx=0
  PNT_MAST_idx=0
  PAR_TITE_idx=0
  ESI_DIF_idx=0
  SALDICON_idx=0
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_NUM_REC = 0
    this.w_OK = .T.
    this.w_TEST = .T.
    this.w_SERIAL = "XXXYYYZZZZ"
    this.w_SERINC = "XXXYYYZZZZ"
    ND = this.oParentObject.w_DETT.cCursor
    if this.pTipo $ "E-C" 
      this.w_CODAZI = i_CODAZI
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZSTALIG"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZSTALIG;
          from (i_cTable) where;
              AZCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    do case
      case this.pTipo="E"
        * --- Cancellazzione dell'intero piano. Verranno cancellate anche le registrazioni di storno in primanota e le partite generate da queste.
        vq_exec("..\EXE\QUERY\GSCG0BG1.VQR",this,"REGSTA")
        if Reccount("REGSTA") <>0
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.w_OK
          if ah_YesNo("Tutte le registrazione generate da questo piano verranno eliminate.%0Si vuole procedere?")
            * --- Select from ESI_DETT
            i_nConn=i_TableProp[this.ESI_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ESI_DETT_idx,2],.t.,this.ESI_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select DPSERMOV,DPSERINC,DPTIPREG  from "+i_cTable+" ESI_DETT ";
                  +" where DPSERIAL="+cp_ToStrODBC(this.oParentObject.w_EDSERIAL)+"";
                   ,"_Curs_ESI_DETT")
            else
              select DPSERMOV,DPSERINC,DPTIPREG from (i_cTable);
               where DPSERIAL=this.oParentObject.w_EDSERIAL;
                into cursor _Curs_ESI_DETT
            endif
            if used('_Curs_ESI_DETT')
              select _Curs_ESI_DETT
              locate for 1=1
              do while not(eof())
              this.w_SERIAL = _Curs_ESI_DETT.DPSERMOV
              this.w_SERINC = _Curs_ESI_DETT.DPSERINC
              this.w_DPTIPREG = NVL(_Curs_ESI_DETT.DPTIPREG,"N")
              * --- Aggiornamento Saldi Contabili
              * --- Select from PNT_DETT
              i_nConn=i_TableProp[this.PNT_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2],.t.,this.PNT_DETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" PNT_DETT ";
                    +" where PNSERIAL="+cp_ToStrODBC(this.w_SERIAL)+"";
                     ,"_Curs_PNT_DETT")
              else
                select * from (i_cTable);
                 where PNSERIAL=this.w_SERIAL;
                  into cursor _Curs_PNT_DETT
              endif
              if used('_Curs_PNT_DETT')
                select _Curs_PNT_DETT
                locate for 1=1
                do while not(eof())
                this.w_IMPDAR = _Curs_PNT_DETT.PNIMPDAR
                this.w_IMPAVE = _Curs_PNT_DETT.PNIMPAVE
                this.w_CODCON = _Curs_PNT_DETT.PNCODCON
                this.w_TIPCON = _Curs_PNT_DETT.PNTIPCON
                * --- Read from PNT_MAST
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.PNT_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PNCODESE"+;
                    " from "+i_cTable+" PNT_MAST where ";
                        +"PNSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PNCODESE;
                    from (i_cTable) where;
                        PNSERIAL = this.w_SERIAL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CODESE = NVL(cp_ToDate(_read_.PNCODESE),cp_NullValue(_read_.PNCODESE))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Write into SALDICON
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDICON_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SLDARPER =SLDARPER- "+cp_ToStrODBC(this.w_IMPDAR);
                  +",SLAVEPER =SLAVEPER- "+cp_ToStrODBC(this.w_IMPAVE);
                      +i_ccchkf ;
                  +" where ";
                      +"SLTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                      +" and SLCODICE = "+cp_ToStrODBC(this.w_CODCON);
                      +" and SLCODESE = "+cp_ToStrODBC(this.w_CODESE);
                         )
                else
                  update (i_cTable) set;
                      SLDARPER = SLDARPER - this.w_IMPDAR;
                      ,SLAVEPER = SLAVEPER - this.w_IMPAVE;
                      &i_ccchkf. ;
                   where;
                      SLTIPCON = this.w_TIPCON;
                      and SLCODICE = this.w_CODCON;
                      and SLCODESE = this.w_CODESE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                  select _Curs_PNT_DETT
                  continue
                enddo
                use
              endif
              * --- Cancellazione registrazioni di storno
              * --- Delete from PNT_IVA
              i_nConn=i_TableProp[this.PNT_IVA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"IVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                       )
              else
                delete from (i_cTable) where;
                      IVSERIAL = this.w_SERIAL;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              * --- Delete from PNT_DETT
              i_nConn=i_TableProp[this.PNT_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"PNSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                       )
              else
                delete from (i_cTable) where;
                      PNSERIAL = this.w_SERIAL;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              * --- Delete from PNT_MAST
              i_nConn=i_TableProp[this.PNT_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"PNSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                       )
              else
                delete from (i_cTable) where;
                      PNSERIAL = this.w_SERIAL;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              * --- Cancellazione partite
              * --- Delete from PAR_TITE
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"PTSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                      +" and PTROWORD > "+cp_ToStrODBC(0);
                       )
              else
                delete from (i_cTable) where;
                      PTSERIAL = this.w_SERIAL;
                      and PTROWORD > 0;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              * --- Nel caso di cancellazione di una registrazione derivante da maturazione
              *     temporale, devo verificare se esistono registrazioni di incasso associate
              *     ad emissione fatture con IVA esigibilit� differita,
              *     se esistono il flag Pnflgdif deve essere valorizzato a 'X'
              * --- Select from Gscg4bg1
              do vq_exec with 'Gscg4bg1',this,'_Curs_Gscg4bg1','',.f.,.t.
              if used('_Curs_Gscg4bg1')
                select _Curs_Gscg4bg1
                locate for 1=1
                do while not(eof())
                this.w_NUM_REC = Nvl(_Curs_Gscg4bg1.Num_Rec,0)
                  select _Curs_Gscg4bg1
                  continue
                enddo
                use
              endif
              if this.w_DPTIPREG="E"
                * --- Write into PNT_IVA
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PNT_IVA_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
                if i_nConn<>0
                  local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
                  declare i_aIndex[1]
                  i_cQueryTable=cp_getTempTableName(i_nConn)
                  i_aIndex(1)="IVSERIAL,CPROWNUM"
                  do vq_exec with 'GSCG5BG1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_IVA_idx,i_nConn)	
                  i_cDB=cp_GetDatabaseType(i_nConn)
                  do case
                  case i_cDB="SQLServer"
                    i_cWhere="PNT_IVA.IVSERIAL = _t2.IVSERIAL";
                          +" and "+"PNT_IVA.CPROWNUM = _t2.CPROWNUM";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"IVFLSTOD ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_IVA','IVFLSTOD');
                      +i_ccchkf;
                      +" from "+i_cTable+" PNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
                  case i_cDB="MySQL"
                    i_cWhere="PNT_IVA.IVSERIAL = _t2.IVSERIAL";
                          +" and "+"PNT_IVA.CPROWNUM = _t2.CPROWNUM";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PNT_IVA, "+i_cQueryTable+" _t2 set ";
                  +"PNT_IVA.IVFLSTOD ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_IVA','IVFLSTOD');
                      +Iif(Empty(i_ccchkf),"",",PNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                      +" where "+i_cWhere)
                  case i_cDB="Oracle"
                    i_cWhere="PNT_IVA.IVSERIAL = t2.IVSERIAL";
                          +" and "+"PNT_IVA.CPROWNUM = t2.CPROWNUM";
                    
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PNT_IVA set (";
                      +"IVFLSTOD";
                      +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                      +cp_NullLink(cp_ToStrODBC("N"),'PNT_IVA','IVFLSTOD')+"";
                      +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                      +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
                  case i_cDB="PostgreSQL"
                    i_cWhere="PNT_IVA.IVSERIAL = _t2.IVSERIAL";
                          +" and "+"PNT_IVA.CPROWNUM = _t2.CPROWNUM";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PNT_IVA set ";
                  +"IVFLSTOD ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_IVA','IVFLSTOD');
                      +i_ccchkf;
                      +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
                  otherwise
                    i_cWhere=i_cTable+".IVSERIAL = "+i_cQueryTable+".IVSERIAL";
                          +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"IVFLSTOD ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_IVA','IVFLSTOD');
                      +i_ccchkf;
                      +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
                  endcase
                  cp_DropTempTable(i_nConn,i_cQueryTable)
                else
                  error "not yet implemented!"
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              else
                if this.w_DPTIPREG="S" And this.w_NUM_REC<>0
                  * --- Write into PNT_MAST
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PNT_MAST_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PNFLGDIF ="+cp_NullLink(cp_ToStrODBC("X"),'PNT_MAST','PNFLGDIF');
                        +i_ccchkf ;
                    +" where ";
                        +"PNSERIAL = "+cp_ToStrODBC(this.w_SERINC);
                           )
                  else
                    update (i_cTable) set;
                        PNFLGDIF = "X";
                        &i_ccchkf. ;
                     where;
                        PNSERIAL = this.w_SERINC;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                else
                  * --- Aggiornamento campo PNFLGDIF = ' ' nelle registrazioni di incasso, altrimenti non potrebbero essere pi� generate.
                  * --- Write into PNT_MAST
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PNT_MAST_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PNFLGDIF ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLGDIF');
                        +i_ccchkf ;
                    +" where ";
                        +"PNSERIAL = "+cp_ToStrODBC(this.w_SERINC);
                           )
                  else
                    update (i_cTable) set;
                        PNFLGDIF = " ";
                        &i_ccchkf. ;
                     where;
                        PNSERIAL = this.w_SERINC;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              endif
                select _Curs_ESI_DETT
                continue
              enddo
              use
            endif
            * --- Delete from ESI_DETT
            i_nConn=i_TableProp[this.ESI_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ESI_DETT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"DPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_EDSERIAL);
                     )
            else
              delete from (i_cTable) where;
                    DPSERIAL = this.oParentObject.w_EDSERIAL;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            DELETE FROM (ND)
          else
            this.w_OK = .F.
            this.w_MESS = "Cancellazione abbandonata"
          endif
        endif
        if Not this.w_OK
          this.w_MESS = ah_MsgFormat(this.w_Mess)
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pTipo="C"
        * --- Cancellazione Manuale Delle righe di dettaglio
        vq_exec("..\EXE\QUERY\GSCG2BG1.VQR",this,"REGSTA")
        SELECT * FROM (ND) WHERE XCHK = 1 INTO CURSOR APPO
        Select * From REGSTA Where SERINC In (Select DPSERINC From APPO) Into Cursor REGSTA
        if Reccount("REGSTA") <>0
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.w_OK
          if ah_YesNo("Le registrazioni di storno relative agli incassi/maturazioni temporali selezionati verranno eliminate.%0Si vuole procedere?")
            SELECT APPO
            if RECCOUNT("APPO") >0
              GO TOP
              SCAN
              this.w_SERINC = DPSERINC
              this.w_NUM_REC = 0
              * --- Select from ESI_DETT
              i_nConn=i_TableProp[this.ESI_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ESI_DETT_idx,2],.t.,this.ESI_DETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select DPSERMOV,DPSERINC,DPTIPREG  from "+i_cTable+" ESI_DETT ";
                    +" where DPSERINC="+cp_ToStrODBC(this.w_SERINC)+" AND DPSERIAL="+cp_ToStrODBC(this.oParentObject.w_EDSERIAL)+"";
                     ,"_Curs_ESI_DETT")
              else
                select DPSERMOV,DPSERINC,DPTIPREG from (i_cTable);
                 where DPSERINC=this.w_SERINC AND DPSERIAL=this.oParentObject.w_EDSERIAL;
                  into cursor _Curs_ESI_DETT
              endif
              if used('_Curs_ESI_DETT')
                select _Curs_ESI_DETT
                locate for 1=1
                do while not(eof())
                this.w_SERIAL = _Curs_ESI_DETT.DPSERMOV
                this.w_DPTIPREG = NVL(_Curs_ESI_DETT.DPTIPREG,"N")
                * --- Aggiornamento Saldi Contabili
                * --- Select from PNT_DETT
                i_nConn=i_TableProp[this.PNT_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2],.t.,this.PNT_DETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select * from "+i_cTable+" PNT_DETT ";
                      +" where PNSERIAL="+cp_ToStrODBC(this.w_SERIAL)+"";
                       ,"_Curs_PNT_DETT")
                else
                  select * from (i_cTable);
                   where PNSERIAL=this.w_SERIAL;
                    into cursor _Curs_PNT_DETT
                endif
                if used('_Curs_PNT_DETT')
                  select _Curs_PNT_DETT
                  locate for 1=1
                  do while not(eof())
                  this.w_IMPDAR = _Curs_PNT_DETT.PNIMPDAR
                  this.w_IMPAVE = _Curs_PNT_DETT.PNIMPAVE
                  this.w_CODCON = _Curs_PNT_DETT.PNCODCON
                  this.w_TIPCON = _Curs_PNT_DETT.PNTIPCON
                  * --- Read from PNT_MAST
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.PNT_MAST_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "PNCODESE"+;
                      " from "+i_cTable+" PNT_MAST where ";
                          +"PNSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      PNCODESE;
                      from (i_cTable) where;
                          PNSERIAL = this.w_SERIAL;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_CODESE = NVL(cp_ToDate(_read_.PNCODESE),cp_NullValue(_read_.PNCODESE))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  * --- Write into SALDICON
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALDICON_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SLDARPER =SLDARPER- "+cp_ToStrODBC(this.w_IMPDAR);
                    +",SLAVEPER =SLAVEPER- "+cp_ToStrODBC(this.w_IMPAVE);
                        +i_ccchkf ;
                    +" where ";
                        +"SLTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                        +" and SLCODICE = "+cp_ToStrODBC(this.w_CODCON);
                        +" and SLCODESE = "+cp_ToStrODBC(this.w_CODESE);
                           )
                  else
                    update (i_cTable) set;
                        SLDARPER = SLDARPER - this.w_IMPDAR;
                        ,SLAVEPER = SLAVEPER - this.w_IMPAVE;
                        &i_ccchkf. ;
                     where;
                        SLTIPCON = this.w_TIPCON;
                        and SLCODICE = this.w_CODCON;
                        and SLCODESE = this.w_CODESE;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                    select _Curs_PNT_DETT
                    continue
                  enddo
                  use
                endif
                * --- Cancellazione registrazioni di storno
                * --- Delete from PNT_IVA
                i_nConn=i_TableProp[this.PNT_IVA_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"IVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                         )
                else
                  delete from (i_cTable) where;
                        IVSERIAL = this.w_SERIAL;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error=MSG_DELETE_ERROR
                  return
                endif
                * --- Delete from PNT_DETT
                i_nConn=i_TableProp[this.PNT_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"PNSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                         )
                else
                  delete from (i_cTable) where;
                        PNSERIAL = this.w_SERIAL;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error=MSG_DELETE_ERROR
                  return
                endif
                * --- Delete from PNT_MAST
                i_nConn=i_TableProp[this.PNT_MAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"PNSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                         )
                else
                  delete from (i_cTable) where;
                        PNSERIAL = this.w_SERIAL;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error=MSG_DELETE_ERROR
                  return
                endif
                * --- Cancellazione partite
                * --- Delete from PAR_TITE
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"PTSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                         )
                else
                  delete from (i_cTable) where;
                        PTSERIAL = this.w_SERIAL;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error=MSG_DELETE_ERROR
                  return
                endif
                * --- Nel caso di cancellazione di una registrazione derivante da maturazione
                *     temporale, devo verificare se esistono registrazioni di incasso associate
                *     ad emissione fatture con IVA esigibilit� differita,
                *     se esistono il flag Pnflgdif deve essere valorizzato a 'X'
                * --- Select from Gscg4bg1
                do vq_exec with 'Gscg4bg1',this,'_Curs_Gscg4bg1','',.f.,.t.
                if used('_Curs_Gscg4bg1')
                  select _Curs_Gscg4bg1
                  locate for 1=1
                  do while not(eof())
                  this.w_NUM_REC = Nvl(_Curs_Gscg4bg1.Num_Rec,0)
                    select _Curs_Gscg4bg1
                    continue
                  enddo
                  use
                endif
                if this.w_DPTIPREG="S" And this.w_NUM_REC<>0
                  * --- Write into PNT_MAST
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PNT_MAST_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PNFLGDIF ="+cp_NullLink(cp_ToStrODBC("X"),'PNT_MAST','PNFLGDIF');
                        +i_ccchkf ;
                    +" where ";
                        +"PNSERIAL = "+cp_ToStrODBC(this.w_SERINC);
                           )
                  else
                    update (i_cTable) set;
                        PNFLGDIF = "X";
                        &i_ccchkf. ;
                     where;
                        PNSERIAL = this.w_SERINC;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                else
                  * --- Aggiornamento campo PNFLGDIF = ' ' nelle registrazioni di incasso, altrimenti non potrebbero essere pi� generate.
                  * --- Write into PNT_MAST
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PNT_MAST_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PNFLGDIF ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLGDIF');
                        +i_ccchkf ;
                    +" where ";
                        +"PNSERIAL = "+cp_ToStrODBC(this.w_SERINC);
                           )
                  else
                    update (i_cTable) set;
                        PNFLGDIF = " ";
                        &i_ccchkf. ;
                     where;
                        PNSERIAL = this.w_SERINC;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
                * --- Write into PNT_IVA
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PNT_IVA_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
                if i_nConn<>0
                  local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
                  declare i_aIndex[1]
                  i_cQueryTable=cp_getTempTableName(i_nConn)
                  i_aIndex(1)="IVSERIAL,CPROWNUM"
                  do vq_exec with 'GSCG5BG1',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_IVA_idx,i_nConn)	
                  i_cDB=cp_GetDatabaseType(i_nConn)
                  do case
                  case i_cDB="SQLServer"
                    i_cWhere="PNT_IVA.IVSERIAL = _t2.IVSERIAL";
                          +" and "+"PNT_IVA.CPROWNUM = _t2.CPROWNUM";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"IVFLSTOD ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_IVA','IVFLSTOD');
                      +i_ccchkf;
                      +" from "+i_cTable+" PNT_IVA, "+i_cQueryTable+" _t2 where "+i_cWhere)
                  case i_cDB="MySQL"
                    i_cWhere="PNT_IVA.IVSERIAL = _t2.IVSERIAL";
                          +" and "+"PNT_IVA.CPROWNUM = _t2.CPROWNUM";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PNT_IVA, "+i_cQueryTable+" _t2 set ";
                  +"PNT_IVA.IVFLSTOD ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_IVA','IVFLSTOD');
                      +Iif(Empty(i_ccchkf),"",",PNT_IVA.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                      +" where "+i_cWhere)
                  case i_cDB="Oracle"
                    i_cWhere="PNT_IVA.IVSERIAL = t2.IVSERIAL";
                          +" and "+"PNT_IVA.CPROWNUM = t2.CPROWNUM";
                    
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PNT_IVA set (";
                      +"IVFLSTOD";
                      +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                      +cp_NullLink(cp_ToStrODBC("N"),'PNT_IVA','IVFLSTOD')+"";
                      +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                      +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
                  case i_cDB="PostgreSQL"
                    i_cWhere="PNT_IVA.IVSERIAL = _t2.IVSERIAL";
                          +" and "+"PNT_IVA.CPROWNUM = _t2.CPROWNUM";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PNT_IVA set ";
                  +"IVFLSTOD ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_IVA','IVFLSTOD');
                      +i_ccchkf;
                      +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
                  otherwise
                    i_cWhere=i_cTable+".IVSERIAL = "+i_cQueryTable+".IVSERIAL";
                          +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"IVFLSTOD ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_IVA','IVFLSTOD');
                      +i_ccchkf;
                      +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
                  endcase
                  cp_DropTempTable(i_nConn,i_cQueryTable)
                else
                  error "not yet implemented!"
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                  select _Curs_ESI_DETT
                  continue
                enddo
                use
              endif
              * --- Delete from ESI_DETT
              i_nConn=i_TableProp[this.ESI_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ESI_DETT_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"DPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_EDSERIAL);
                      +" and DPSERINC = "+cp_ToStrODBC(this.w_SERINC);
                       )
              else
                delete from (i_cTable) where;
                      DPSERIAL = this.oParentObject.w_EDSERIAL;
                      and DPSERINC = this.w_SERINC;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              ENDSCAN
            else
              ah_ErrorMsg("Selezionare la registrazione da eliminare",,"")
              this.w_TEST = .F.
            endif
            if USED("APPO")
              SELECT APPO
              USE
            endif
            this.oParentObject.NotifyEvent("Carica")
            SELECT * FROM (ND) INTO CURSOR APPO
            * --- Se la riga cancellata � unica nel piano, viene cancellato anche il piano.
            if RECCOUNT("APPO") = 0 AND this.w_TEST
              ah_ErrorMsg("Il piano non ha pi� righe di dettaglio e verr� eliminato",,"")
              * --- Delete from ESI_DIF
              i_nConn=i_TableProp[this.ESI_DIF_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ESI_DIF_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"EDSERIAL = "+cp_ToStrODBC(this.oParentObject.w_EDSERIAL);
                       )
              else
                delete from (i_cTable) where;
                      EDSERIAL = this.oParentObject.w_EDSERIAL;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              this.oParentObject.ecpQuit()
            endif
          endif
        endif
        this.oParentObject.NotifyEvent("Carica")
        if Not this.w_OK
          ah_ErrorMsg(this.w_MESS,,"")
        endif
      case this.pTipo="P" 
        * --- PrimaNota
        GSAR_BZP(this,this.pSERIAL)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        return
      case this.pTipo="L" 
        * --- Riassegna le varibili delle causali sul piano
        if THIS.OPARENTOBJECT.CFUNCTION="Edit" or THIS.OPARENTOBJECT.CFUNCTION="Query"
          vq_exec("..\EXE\QUERY\GSCG0KED.VQR",this,"CARICA")
          Select CARICA 
 go top 
 scan
          do case
            case CARICA.CCTIPREG="A" AND CARICA.DPTIPREG="N"
              * --- Registro acquisti e no mat.temporale
              = setvaluelinked ( "M" , this.oparentobject , "w_CAUPAG" , CARICA.CAUINC)
            case CARICA.CCTIPREG="A" AND CARICA.DPTIPREG="S"
              * --- Registro acquisti e mat.temporale
              = setvaluelinked ( "M" , this.oparentobject , "w_CAUMTP" , CARICA.CAUINC)
            case CARICA.CCTIPREG="V" AND CARICA.DPTIPREG="N"
              * --- Registro vendite e no mat.temporale
              = setvaluelinked ( "M" , this.oparentobject , "w_CAUINC" , CARICA.CAUINC)
            case CARICA.CCTIPREG="V" AND CARICA.DPTIPREG="S"
              * --- Registro vendite e mat.temporale
              this.oParentObject.w_CAUMTA = CARICA.CAUINC
              = setvaluelinked ( "M" , this.oparentobject , "w_CAUMTA" , CARICA.CAUINC)
          endcase
          endscan
        endif
    endcase
    With this.oParentObject
    .SaveDependsOn()
    .SetControlsValue()
    EndWith
    if USED("CARICA")
      SELECT CARICA
      USE
    endif
    if USED("REGSTA")
      SELECT REGSTA
      USE
    endif
    if USED("APPO")
      SELECT APPO
      USE
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
     
 Select REGSTA 
 Go Top
    do case
      case REGSTA.TOTLBG>0
        this.w_MESS = "Esistono registrazioni con data inferiore o uguale a ultima stampa libro giornale, impossibile cancellare."
        this.w_OK = .F.
      case REGSTA.TOTSTA>0
        this.w_MESS = "Esistono registrazioni con data inferiore o uguale alla data di ultima stampa registri IVA, impossibile cancellare."
        this.w_OK = .F.
      case REGSTA.TOTBLO>0
        this.w_MESS = "Stampa registri IVA in corso con selezione comprendente alcune registrazioni del piano , impossibile cancellare."
        this.w_OK = .F.
    endcase
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipo,pSerial)
    this.pTipo=pTipo
    this.pSerial=pSerial
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='ESI_DETT'
    this.cWorkTables[2]='PNT_IVA'
    this.cWorkTables[3]='PNT_DETT'
    this.cWorkTables[4]='PNT_MAST'
    this.cWorkTables[5]='PAR_TITE'
    this.cWorkTables[6]='ESI_DIF'
    this.cWorkTables[7]='SALDICON'
    this.cWorkTables[8]='AZIENDA'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_ESI_DETT')
      use in _Curs_ESI_DETT
    endif
    if used('_Curs_PNT_DETT')
      use in _Curs_PNT_DETT
    endif
    if used('_Curs_Gscg4bg1')
      use in _Curs_Gscg4bg1
    endif
    if used('_Curs_ESI_DETT')
      use in _Curs_ESI_DETT
    endif
    if used('_Curs_PNT_DETT')
      use in _Curs_PNT_DETT
    endif
    if used('_Curs_Gscg4bg1')
      use in _Curs_Gscg4bg1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo,pSerial"
endproc
