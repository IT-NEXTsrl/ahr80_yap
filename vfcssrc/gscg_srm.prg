* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_srm                                                        *
*              Calcolo regime del margine                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_64]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-23                                                      *
* Last revis.: 2009-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_srm",oParentObject))

* --- Class definition
define class tgscg_srm as StdForm
  Top    = 28
  Left   = 64

  * --- Standard Properties
  Width  = 433
  Height = 217
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-11-17"
  HelpContextID=160815767
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  AZIENDA_IDX = 0
  ATTIMAST_IDX = 0
  cPrg = "gscg_srm"
  cComment = "Calcolo regime del margine"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_CODESE = space(4)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_CODATT = space(5)
  o_CODATT = space(5)
  w_FLDEFI = space(1)
  o_FLDEFI = space(1)
  w_FLDEFI = space(1)
  w_ANNO = space(4)
  w_NUMPER = 0
  o_NUMPER = 0
  w_DESCRI = space(30)
  w_FINPER = ctod('  /  /  ')
  w_ULTDAT = ctod('  /  /  ')
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_DATFI2 = ctod('  /  /  ')
  w_INTLIG = space(1)
  w_ATTDESC = space(35)
  w_PEREFF = space(1)
  w_GESTMAR = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_srmPag1","gscg_srm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODATT_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='ATTIMAST'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_srm
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_CODESE=space(4)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_CODATT=space(5)
      .w_FLDEFI=space(1)
      .w_FLDEFI=space(1)
      .w_ANNO=space(4)
      .w_NUMPER=0
      .w_DESCRI=space(30)
      .w_FINPER=ctod("  /  /  ")
      .w_ULTDAT=ctod("  /  /  ")
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_DATFI2=ctod("  /  /  ")
      .w_INTLIG=space(1)
      .w_ATTDESC=space(35)
      .w_PEREFF=space(1)
      .w_GESTMAR=space(1)
        .w_CODAZI = i_CODAZI
        .w_CODESE = g_CODESE
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODESE))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,5,.f.)
        if not(empty(.w_CODATT))
          .link_1_5('Full')
        endif
        .w_FLDEFI = 'N'
        .w_FLDEFI = 'N'
        .w_ANNO = CHECKPER(this,.w_CODATT,.w_FLDEFI,' ',0,'A','.w_DATINI','.w_DATFIN')
        .w_NUMPER = CHECKPER(this,.w_CODATT,.w_FLDEFI,' ',0,'P','.w_DATINI','.w_DATFIN')
        .w_DESCRI = IIF(.w_NUMPER>0, CALCPER(.w_NUMPER, g_TIPDEN), SPACE(30))
          .DoRTCalc(11,14,.f.)
        .w_DATFI2 = .w_DATFIN+15
          .DoRTCalc(16,17,.f.)
        .w_PEREFF = 'N'
      .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
    endwith
    this.DoRTCalc(19,19,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,7,.t.)
        if .o_CODATT<>.w_CODATT.or. .o_FLDEFI<>.w_FLDEFI
            .w_ANNO = CHECKPER(this,.w_CODATT,.w_FLDEFI,' ',0,'A','.w_DATINI','.w_DATFIN')
        endif
        if .o_CODATT<>.w_CODATT.or. .o_FLDEFI<>.w_FLDEFI
            .w_NUMPER = CHECKPER(this,.w_CODATT,.w_FLDEFI,' ',0,'P','.w_DATINI','.w_DATFIN')
        endif
        if .o_NUMPER<>.w_NUMPER
            .w_DESCRI = IIF(.w_NUMPER>0, CALCPER(.w_NUMPER, g_TIPDEN), SPACE(30))
        endif
        .DoRTCalc(11,14,.t.)
            .w_DATFI2 = .w_DATFIN+15
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oANNO_1_8.enabled = this.oPgFrm.Page1.oPag.oANNO_1_8.mCond()
    this.oPgFrm.Page1.oPag.oNUMPER_1_9.enabled = this.oPgFrm.Page1.oPag.oNUMPER_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLDEFI_1_6.visible=!this.oPgFrm.Page1.oPag.oFLDEFI_1_6.mHide()
    this.oPgFrm.Page1.oPag.oFLDEFI_1_7.visible=!this.oPgFrm.Page1.oPag.oFLDEFI_1_7.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODESE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODATT
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_lTable = "ATTIMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2], .t., this.ATTIMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODATT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_mat',True,'ATTIMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_CODATT)+"%");

          i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT,ATREGMAR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODATT',trim(this.w_CODATT))
          select ATCODATT,ATDESATT,ATREGMAR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODATT)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODATT) and !this.bDontReportError
            deferred_cp_zoom('ATTIMAST','*','ATCODATT',cp_AbsName(oSource.parent,'oCODATT_1_5'),i_cWhere,'gsar_mat',"Codici attivit�",'GSCG_SRM.ATTIMAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT,ATREGMAR";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',oSource.xKey(1))
            select ATCODATT,ATDESATT,ATREGMAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODATT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT,ATREGMAR";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_CODATT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_CODATT)
            select ATCODATT,ATDESATT,ATREGMAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODATT = NVL(_Link_.ATCODATT,space(5))
      this.w_ATTDESC = NVL(_Link_.ATDESATT,space(35))
      this.w_GESTMAR = NVL(_Link_.ATREGMAR,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODATT = space(5)
      endif
      this.w_ATTDESC = space(35)
      this.w_GESTMAR = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_GESTMAR = 'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attivit� che non gestisce il margine")
        endif
        this.w_CODATT = space(5)
        this.w_ATTDESC = space(35)
        this.w_GESTMAR = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODATT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODATT_1_5.value==this.w_CODATT)
      this.oPgFrm.Page1.oPag.oCODATT_1_5.value=this.w_CODATT
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDEFI_1_6.RadioValue()==this.w_FLDEFI)
      this.oPgFrm.Page1.oPag.oFLDEFI_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDEFI_1_7.RadioValue()==this.w_FLDEFI)
      this.oPgFrm.Page1.oPag.oFLDEFI_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_8.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_8.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMPER_1_9.value==this.w_NUMPER)
      this.oPgFrm.Page1.oPag.oNUMPER_1_9.value=this.w_NUMPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_10.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_10.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oULTDAT_1_12.value==this.w_ULTDAT)
      this.oPgFrm.Page1.oPag.oULTDAT_1_12.value=this.w_ULTDAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_13.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_13.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_14.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_14.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oATTDESC_1_26.value==this.w_ATTDESC)
      this.oPgFrm.Page1.oPag.oATTDESC_1_26.value=this.w_ATTDESC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_GESTMAR = 'S')  and not(empty(.w_CODATT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODATT_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attivit� che non gestisce il margine")
          case   ((empty(.w_ANNO)) or not(VAL(.w_ANNO)>1900 and CHECKPER(.null.,.w_CODATT,.w_FLDEFI,.w_ANNO,0,.F.,.w_DATINI)))  and (.w_FLDEFI $ 'R-N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_8.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NUMPER)) or not((.w_NUMPER<5 OR (.w_NUMPER<13 AND g_TIPDEN="M")) and CHECKPER(.null.,.w_CODATT,.w_FLDEFI,.w_ANNO,.w_NUMPER,.F.,.w_DATINI)))  and (.w_FLDEFI $ 'R-N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMPER_1_9.SetFocus()
            i_bnoObbl = !empty(.w_NUMPER)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero periodo non congruente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODATT = this.w_CODATT
    this.o_FLDEFI = this.w_FLDEFI
    this.o_NUMPER = this.w_NUMPER
    return

enddefine

* --- Define pages as container
define class tgscg_srmPag1 as StdContainer
  Width  = 429
  height = 217
  stdWidth  = 429
  stdheight = 217
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODATT_1_5 as StdField with uid="ANDDVHHYFJ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODATT", cQueryName = "CODATT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Attivit� che non gestisce il margine",;
    ToolTipText = "Codice attivit� su cui calcolare il regime del margine",;
    HelpContextID = 216306138,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=105, Top=7, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ATTIMAST", cZoomOnZoom="gsar_mat", oKey_1_1="ATCODATT", oKey_1_2="this.w_CODATT"

  func oCODATT_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODATT_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODATT_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ATTIMAST','*','ATCODATT',cp_AbsName(this.parent,'oCODATT_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_mat',"Codici attivit�",'GSCG_SRM.ATTIMAST_VZM',this.parent.oContained
  endproc
  proc oCODATT_1_5.mZoomOnZoom
    local i_obj
    i_obj=gsar_mat()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ATCODATT=this.parent.oContained.w_CODATT
     i_obj.ecpSave()
  endproc


  add object oFLDEFI_1_6 as StdCombo with uid="DJBBJZQPJZ",rtseq=6,rtrep=.f.,left=105,top=38,width=122,height=21;
    , ToolTipText = "Tipo di elaborazione: se definitiva, aggiorna l'ultima data di stampa dei registri";
    , HelpContextID = 146838698;
    , cFormVar="w_FLDEFI",RowSource=""+"Simulata,"+"Definitiva,"+"Ristampa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLDEFI_1_6.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oFLDEFI_1_6.GetRadio()
    this.Parent.oContained.w_FLDEFI = this.RadioValue()
    return .t.
  endfunc

  func oFLDEFI_1_6.SetRadio()
    this.Parent.oContained.w_FLDEFI=trim(this.Parent.oContained.w_FLDEFI)
    this.value = ;
      iif(this.Parent.oContained.w_FLDEFI=='N',1,;
      iif(this.Parent.oContained.w_FLDEFI=='S',2,;
      iif(this.Parent.oContained.w_FLDEFI=='R',3,;
      0)))
  endfunc

  func oFLDEFI_1_6.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc


  add object oFLDEFI_1_7 as StdCombo with uid="ZCSHNILGYU",rtseq=7,rtrep=.f.,left=105,top=38,width=122,height=21;
    , ToolTipText = "Tipo di elaborazione: se definitiva, aggiorna l'ultima data di stampa dei registri";
    , HelpContextID = 146838698;
    , cFormVar="w_FLDEFI",RowSource=""+"In prova,"+"Definitiva,"+"Ristampa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLDEFI_1_7.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oFLDEFI_1_7.GetRadio()
    this.Parent.oContained.w_FLDEFI = this.RadioValue()
    return .t.
  endfunc

  func oFLDEFI_1_7.SetRadio()
    this.Parent.oContained.w_FLDEFI=trim(this.Parent.oContained.w_FLDEFI)
    this.value = ;
      iif(this.Parent.oContained.w_FLDEFI=='N',1,;
      iif(this.Parent.oContained.w_FLDEFI=='S',2,;
      iif(this.Parent.oContained.w_FLDEFI=='R',3,;
      0)))
  endfunc

  func oFLDEFI_1_7.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oANNO_1_8 as StdField with uid="DITKXJLBMK",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",nZero=4,;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento dei saldi IVA da calcolare",;
    HelpContextID = 166333702,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=105, Top=64, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oANNO_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLDEFI $ 'R-N')
    endwith
   endif
  endfunc

  func oANNO_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ANNO)>1900 and CHECKPER(.null.,.w_CODATT,.w_FLDEFI,.w_ANNO,0,.F.,.w_DATINI))
    endwith
    return bRes
  endfunc

  add object oNUMPER_1_9 as StdField with uid="GNUQORRVDZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_NUMPER", cQueryName = "NUMPER",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero periodo non congruente",;
    ToolTipText = "Periodo: 1..4 per trimestrale; 1..12 per mensile",;
    HelpContextID = 264567594,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=105, Top=91, cSayPict='"99"', cGetPict='"99"'

  func oNUMPER_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLDEFI $ 'R-N')
    endwith
   endif
  endfunc

  func oNUMPER_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_NUMPER<5 OR (.w_NUMPER<13 AND g_TIPDEN="M")) and CHECKPER(.null.,.w_CODATT,.w_FLDEFI,.w_ANNO,.w_NUMPER,.F.,.w_DATINI))
    endwith
    return bRes
  endfunc

  add object oDESCRI_1_10 as StdField with uid="KINVSYYWJX",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 134327242,;
   bGlobalFont=.t.,;
    Height=21, Width=274, Left=147, Top=91, InputMask=replicate('X',30)

  add object oULTDAT_1_12 as StdField with uid="EWQTFOMHJM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ULTDAT", cQueryName = "ULTDAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di stampa del registro IVA selezionato",;
    HelpContextID = 235967418,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=105, Top=172

  add object oDATINI_1_13 as StdField with uid="NJHANDKUHN",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Usata nella query per selezionare le date di registrazione",;
    HelpContextID = 138125258,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=105, Top=118

  add object oDATFIN_1_14 as StdField with uid="CWRCHIOAWF",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Usata nel batch per filtrare le date di registrazione",;
    HelpContextID = 59678666,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=105, Top=145


  add object oBtn_1_16 as StdButton with uid="SENKBIZSYY",left=322, top=166, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per iniziare l'elaborazione";
    , HelpContextID = 160844518;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        do GSCG_BRM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="BYDIHEHRWY",left=373, top=166, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 168133190;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oATTDESC_1_26 as StdField with uid="XTCIHPNYTU",rtseq=17,rtrep=.f.,;
    cFormVar = "w_ATTDESC", cQueryName = "ATTDESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 248548602,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=170, Top=7, InputMask=replicate('X',35)


  add object oObj_1_28 as cp_runprogram with uid="NEXCUIDCOO",left=3, top=221, width=191,height=19,;
    caption='Calcolo date periodo',;
   bGlobalFont=.t.,;
    prg="GSCG_BBS('C')",;
    cEvent = "w_NUMPER Changed, w_ANNO Changed",;
    nPag=1;
    , HelpContextID = 265444744


  add object oObj_1_29 as cp_runprogram with uid="AEMLZZQWZK",left=3, top=243, width=191,height=19,;
    caption='Lettura ultima data di stampa',;
   bGlobalFont=.t.,;
    prg="GSCG_BBS('L',w_CODATT)",;
    cEvent = "w_CODATT Changed",;
    nPag=1;
    , HelpContextID = 242222259

  add object oStr_1_15 as StdString with uid="MEREFFIYQA",Visible=.t., Left=5, Top=173,;
    Alignment=1, Width=98, Height=15,;
    Caption="Ultima stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="PUHMGWINIU",Visible=.t., Left=14, Top=64,;
    Alignment=1, Width=89, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="RDYUWVGEWE",Visible=.t., Left=14, Top=91,;
    Alignment=1, Width=89, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="RXKNOHLYVI",Visible=.t., Left=14, Top=118,;
    Alignment=1, Width=89, Height=15,;
    Caption="Dalla data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="YFTYYCRCQJ",Visible=.t., Left=14, Top=145,;
    Alignment=1, Width=89, Height=15,;
    Caption="Alla data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="ZRWOXBUQOU",Visible=.t., Left=14, Top=38,;
    Alignment=1, Width=89, Height=15,;
    Caption="Elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="JKCGCPZJDW",Visible=.t., Left=14, Top=8,;
    Alignment=1, Width=89, Height=18,;
    Caption="Cod. attivit�:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_srm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
