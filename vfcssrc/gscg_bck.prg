* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bck                                                        *
*              Primanota controlli finali 1                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_718]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2015-12-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bck",oParentObject)
return(i_retval)

define class tgscg_bck as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_FOUND = .f.
  w_TESMES = .f.
  w_TIPCLF = space(1)
  w_TIPRNU = space(1)
  w_oMess1 = .NULL.
  w_AZFLUNIV = space(1)
  w_TMPS = space(200)
  w_MVACCOLD = 0
  w_RIFDIS = space(10)
  w_RIFIND = space(10)
  w_SERIALE = space(10)
  w_PNUMPAR = space(31)
  w_PTDATSCA = ctod("  /  /  ")
  w_PTTIPCON = space(1)
  w_PCODCON = space(15)
  w_PSEGNO = space(1)
  w_PTOTIMP = 0
  w_PCODVAL = space(3)
  w_PSERRIF = space(10)
  w_PORDRIF = 0
  w_PNUMRIF = 0
  w_DISTINTA = space(10)
  w_NUMDIS = space(4)
  w_STATO = space(1)
  w_NUMERO = 0
  w_PTSERRIF = space(10)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_PRESENTE = .f.
  w_OK1 = .f.
  w_ACMOVCES = space(10)
  w_MCNUMREG = 0
  w_MCCODESE = space(4)
  w_MCDATREG = ctod("  /  /  ")
  w_PRIMA = .f.
  w_TOTIMP = 0
  w_RIGA = 0
  w_GSCG_MPA = .NULL.
  w_CONTROPA = .f.
  w_CESPITE = .f.
  w_CONTROACQ = .f.
  w_RIGIVACHK = 0
  w_OKREG = .f.
  w_ESI_DET_IVA = .f.
  w_IMPIVA = 0
  w_IMPONI = 0
  w_TMP_IMPO = 0
  w_TOTMDR1 = 0
  w_TRFLGSTO = space(1)
  w_TOTRITE = 0
  w_TOTRITD = 0
  w_TOTRITA = 0
  w_TOTDAVER = 0
  w_OK = .f.
  w_OK2 = .f.
  w_NUMPRO = 0
  w_ALFPRO = space(10)
  w_DATTEST = ctod("  /  /  ")
  w_PTDATSCA = ctod("  /  /  ")
  w_PTNUMPAR = space(31)
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PTCODVAL = space(3)
  w_PTTOTIMP = 0
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_STALIG = ctod("  /  /  ")
  w_CONTA = 0
  w_DATBLO = ctod("  /  /  ")
  w_MESS = space(100)
  w_TOTDOC = 0
  w_TR = space(1)
  w_PNDOC = space(1)
  w_DIFDOC = 0
  w_NR = 0
  w_CFUNC = space(10)
  w_ElimRif = .f.
  w_ConfRif = .f.
  w_DR = ctod("  /  /  ")
  w_DC = ctod("  /  /  ")
  w_APPO = space(10)
  w_APPO1 = 0
  w_TOTRIG = 0
  w_TROVCF = .f.
  w_TOTD = 0
  w_SEZDAR = 0
  w_TOTA = 0
  w_SEZAVE = 0
  w_RIGIVA = 0
  w_TOTIVA = 0
  w_IVADET = 0
  w_DARE = 0
  w_DATRIF = ctod("  /  /  ")
  w_AVER = 0
  w_DIFIVA = 0
  w_IVA = 0
  w_FINEANNO = ctod("  /  /  ")
  w_IMP = 0
  w_INIANNO = ctod("  /  /  ")
  w_OKPART = .f.
  w_OKPAR2 = .f.
  w_SERDIF = space(10)
  w_MVACCPRE = 0
  w_PT_SEGNO = space(1)
  w_OLDNUMRE = 0
  w_UNIUTE = space(1)
  w_RIFDIS = space(10)
  w_PSEGNO = space(1)
  w_PCODCON = space(15)
  w_PDATSCA = ctod("  /  /  ")
  w_PTIPCON = space(1)
  w_PCODVAL = space(3)
  w_PTOTIMP = 0
  w_CHKPROT = space(100)
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_CASTIVA = .NULL.
  w_TIPREG = space(1)
  w_NUMREG = 0
  w_FLGREMA = space(1)
  w_MARREG = space(1)
  w_CONTROPAR = space(15)
  w_DATINITRA = ctod("  /  /  ")
  * --- WorkFile variables
  ATTIDETT_idx=0
  AZIENDA_idx=0
  CON_INDI_idx=0
  DIS_TINT_idx=0
  DOC_MAST_idx=0
  INC_CORR_idx=0
  PAR_TITE_idx=0
  PNT_MAST_idx=0
  PRI_MAST_idx=0
  VALUTE_idx=0
  ESI_DETT_idx=0
  CONTI_idx=0
  SALMDACO_idx=0
  SALDDACO_idx=0
  VOCIIVA_idx=0
  DATIRITE_idx=0
  INCDCORR_idx=0
  PRE_STAZ_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali di Primanota (da GSCG_MPN) (in A.M. Check Form e Evento: Delete Init)
    * --- Fallisce check o Inibisce la Transazione (Delete) se:
    * --- 1) - Gia' stampata sul Libro Giornale o Stampa L.G. in Corso
    * --- 2) - Test Univocita' Documento = 'S' ed esiste un Documento associato al Cliente
    * --- 3) - Segnala in caso di acquisto esiste un documento con data inferiore e num prot. maggiore
    * --- 4) - Segnala in caso di vendita se ci sono movimenti ritenute associati
    * --- Variabili Definite alla Pagina 3
    this.w_oMess=createobject("Ah_Message")
    Dimension CONTROPARTITE(1)
    this.w_PADRE = This.oParentObject
    this.w_OK = .T.
    this.w_OK2 = .T.
    this.w_OKPART = .F.
    this.w_CFUNC = this.oParentObject.cFunction
    this.w_ElimRif = .F.
    this.w_ConfRif = .F.
    this.oParentObject.w_RESCHK = 0
    * --- Se il movimento di primanota � statop generato da docfinance allora cancella il legame con il movimento di DocFinance
    if g_ISDF="S" And this.w_OK AND this.w_CFUNC="Query"
      GSDF_BDM (this, "DEL" , this.oParentObject.w_PNSERIAL)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Controllo se ci sono dei Movimenti ritenute associate al documento
    if g_RITE="S" And this.w_OK AND this.w_CFUNC="Query"
      VQ_EXEC("..\RITE\EXE\QUERY\GSRI_KMR", this, "RITENUTE")
      SELECT RITENUTE
      if RECCOUNT() > 0
        this.w_oMess.AddMsgPartNL("Esistono movimenti ritenute associate al documento")     
        this.w_oMess.AddMsgPartNL("Impossibile cancellare")     
        this.w_OK = .F.
      endif
      if used("RITENUTE")
         
 SELECT RITENUTE 
 USE
      endif
    endif
    * --- Documento Obbligatorio
    if (this.w_OK AND this.oParentObject.w_FLNDOC="O" AND (this.oParentObject.w_PNNUMDOC=0 OR EMPTY(this.oParentObject.w_PNDATDOC)) Or ( this.oParentObject.w_PNTIPREG="A" AND this.oParentObject.w_PNNUMPRO=0 and this.oParentObject.w_FLPPRO <> "N" )) AND this.w_CFUNC<>"Query"
      do case
        case EMPTY(this.oParentObject.w_PNDATDOC)
          this.w_oMess.AddMsgPartNL("Inserire la data documento")     
        case this.oParentObject.w_PNNUMDOC=0
          this.w_oMess.AddMsgPartNL("Inserire il numero documento")     
        case this.oParentObject.w_PNTIPREG="A" AND this.oParentObject.w_PNNUMPRO=0 and this.oParentObject.w_FLPPRO <> "N"
          this.w_oMess.AddMsgPartNL("Inserire il numero protocollo")     
        otherwise
          this.w_oMess.AddMsgPartNL("Inserire gli estremi del documento (numero e data)")     
      endcase
      this.w_OK = .F.
    endif
    * --- Se Modifico qualcosa e stampato sui registri IVA
    if this.w_OK AND (this.oParentObject.w_STAMPATO="S" OR this.oParentObject.w_STAMPRCV="S") AND this.w_CFUNC="Query"
      this.w_oMess.AddMsgPartNL("Documento gi� stampato sui registri IVA:%0Non eliminabile")     
      this.w_OK = .F.
    endif
    if this.w_OK AND this.oParentObject.w_PNNUMDOC>999999999999999
      this.w_oMess.AddMsgPartNL("Il n� doc. non pu� essere maggiore di 999999999999999")     
      this.w_OK = .F.
    endif
    if this.w_OK AND this.oParentObject.w_PNNUMPRO>999999999999999
      this.w_oMess.AddMsgPartNL("Il n� prot. non pu� essere maggiore di 999999999999999")     
      this.w_OK = .F.
    endif
    if this.w_OK
      * --- Controlli su Libro Giornale
      this.w_DATTEST = IIF(this.w_CFUNC $ "Load-Query", this.oParentObject.w_PNDATREG, MIN(this.oParentObject.w_PNDATREG, this.oParentObject.w_ODATREG))
      this.w_STALIG = cp_CharToDate("  -  -  ")
      this.w_DATBLO = cp_CharToDate("  -  -  ")
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZSTALIG,AZDATBLO,AZDATTRA"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZSTALIG,AZDATBLO,AZDATTRA;
          from (i_cTable) where;
              AZCODAZI = i_codazi;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
        this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
        this.w_DATINITRA = NVL(cp_ToDate(_read_.AZDATTRA),cp_NullValue(_read_.AZDATTRA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      do case
        case this.w_DATTEST <= this.w_STALIG
          * --- data Registrazione Inferiore all' Ultima Data Reg. stampata su L.G.
          if this.oParentObject.w_PNDATREG<= this.w_STALIG
            this.w_oMess.AddMsgPartNL("Data registrazione inferiore o uguale a ultima stampa libro giornale")     
          else
            this.w_oMess.AddMsgPartNL("Data registrazione maggiore a ultima stampa libro giornale")     
          endif
          this.w_OK = .F.
        case Not Empty(CHKCONS(IIF(this.oParentObject.w_FLANAL="S","PC","P"),this.oParentObject.w_PNDATREG,"B","N"))
          this.w_oPart = this.w_oMess.AddMsgPartNL("%1")
          this.w_oPart.AddParam(CHKCONS(IIF(this.oParentObject.w_FLANAL="S","PC","P"),this.oParentObject.w_PNDATREG,"B","N"))     
          this.w_OK = .F.
        case this.w_DATTEST <= this.w_DATBLO AND NOT EMPTY(this.w_DATBLO)
          * --- mentre stampo il giornale non devo inserire registrazioni <= alla data di blocco
          this.w_oMess.AddMsgPartNL("Stampa libro giornale in corso con selezione comprendente la registrazione")     
          this.w_OK = .F.
      endcase
    endif
    if this.w_OK AND this.w_CFUNC <> "Query" AND this.oParentObject.w_PNTIPREG $ "AV" AND NOT this.oParentObject.w_PNTIPDOC $ "CO" AND this.oParentObject.w_PNCOMIVA>this.oParentObject.w_PNDATREG
      this.w_oMess.AddMsgPartNL("Data competenza IVA superiore alla data registrazione")     
      this.w_OK = .F.
    endif
    * --- Verifico Univocit� Num. registrazione.
    if (this.w_CFUNC="Edit" OR this.w_CFUNC="Load") And this.w_OK
      this.w_FOUND = .F.
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZUNIUTE"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZUNIUTE;
          from (i_cTable) where;
              AZCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_UNIUTE = NVL(cp_ToDate(_read_.AZUNIUTE),cp_NullValue(_read_.AZUNIUTE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Select from PNT_MAST
      i_nConn=i_TableProp[this.PNT_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select PNDATREG  from "+i_cTable+" PNT_MAST ";
            +" where PNSERIAL<>"+cp_ToStrODBC(this.oParentObject.w_PNSERIAL)+" AND   PNCODESE="+cp_ToStrODBC(this.oParentObject.w_PNCODESE)+" AND   PNCODUTE="+cp_ToStrODBC(this.oParentObject.w_PNCODUTE)+" AND   PNNUMRER="+cp_ToStrODBC(this.oParentObject.w_PNNUMRER)+" AND   PNPRG="+cp_ToStrODBC(this.oParentObject.w_PNPRG)+"";
             ,"_Curs_PNT_MAST")
      else
        select PNDATREG from (i_cTable);
         where PNSERIAL<>this.oParentObject.w_PNSERIAL AND   PNCODESE=this.oParentObject.w_PNCODESE AND   PNCODUTE=this.oParentObject.w_PNCODUTE AND   PNNUMRER=this.oParentObject.w_PNNUMRER AND   PNPRG=this.oParentObject.w_PNPRG;
          into cursor _Curs_PNT_MAST
      endif
      if used('_Curs_PNT_MAST')
        select _Curs_PNT_MAST
        locate for 1=1
        do while not(eof())
        if (this.w_UNIUTE $ "GE" AND _Curs_PNT_MAST.PNDATREG=this.oParentObject.w_PNDATREG) OR this.w_UNIUTE $ "SU" 
          this.w_FOUND = .T.
        endif
          select _Curs_PNT_MAST
          continue
        enddo
        use
      endif
      if this.w_FOUND
        this.w_OK = ah_YesNo("Numero registrazione %1 gi� presente %2%0confermi ugualmente?",,ALLTRIM(STR(this.oParentObject.w_PNNUMRER)),iif(this.w_UNIUTE $ "GE","per la data specificata.","."))
        if ! this.w_OK
          this.w_oMess.AddMsgPartNL("Operazione abbandonata")     
        endif
      endif
    endif
    if this.w_OK And this.oParentObject.op_PNNUMRER=this.oParentObject.w_PNNUMRER AND this.w_CFUNC="Load"
      * --- Alla conferma ricalcolo sempre il primo progressivo utile cos� se � gi� stato inserito da altro utente non da problemi
      i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
      cp_AskTableProg(this.oParentObject, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
      if this.oParentObject.op_PNNUMRER<>this.oParentObject.w_PNNUMRER
        * --- Nel caso un'altro utente avesse gi� utilizzato il progressivo proposto, mando il messaggio e 
        *     faccio in modo che non rivenga calcolato con la CP_NEXTTABLEPROG di fine batch 
        ah_ErrorMsg("Riassegnato progressivo primanota n.%1",,"",ALLTRIM(STR(this.oParentObject.w_PNNUMRER)))
        this.oParentObject.op_PNNUMRER = this.oParentObject.w_PNNUMRER
      endif
    endif
    * --- Aggiornamento Tabella Progressivi - SOLO IN MODIFICA
    if this.w_OK AND this.w_CFUNC="Edit" AND this.oParentObject.w_MODNRE=.F. AND (this.oParentObject.w_PNCODESE<>this.oParentObject.w_OLCESE OR this.oParentObject.w_PNCODUTE<>this.oParentObject.w_OLCUTE OR this.oParentObject.w_PNPRG<>this.oParentObject.w_OLPRG)
      i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
      cp_NextTableProg(this.oParentObject, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
    endif
    * --- Controlli su Univocita' Numero Documento (non eseguito in Cancellazione)
    *     Se corrispettivi Nessun Controllo Univocit�
    if this.w_OK AND this.w_CFUNC <> "Query" And this.oParentObject.w_PNTIPREG $ "AVCN" AND NOT this.oParentObject.w_PNTIPDOC $ "CO" 
      * --- Controllo anche nei documenti (solo le fatture)
      ah_Msg("Controllo univocit� documento...",.T.)
      do case
        case (this.oParentObject.w_PNTIPREG="V" or this.oParentObject.w_PNTIPDOC="FC") and this.oParentObject.w_FLPDOC<>"N" and this.oParentObject.w_FLPDIF<>"S"
          * --- Controllo progressivo gi� assegnato
          if this.oParentObject.op_PNNUMDOC=this.oParentObject.w_PNNUMDOC AND this.oParentObject.op_PNALFDOC=this.oParentObject.w_PNALFDOC AND this.w_CFUNC="Load"
            * --- Alla conferma ricalcolo sempre il primo progressivo utile cos� se � gi� stato inserito da altro utente non da problemi
            i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
            cp_AskTableProg(this.oParentObject, i_Conn, "PRDOC", "i_codazi,w_PNANNDOC,w_PNPRD,w_PNALFDOC,w_PNNUMDOC")
            if this.oParentObject.op_PNNUMDOC<>this.oParentObject.w_PNNUMDOC OR this.oParentObject.op_PNALFDOC<>this.oParentObject.w_PNALFDOC
              * --- Nel caso un'altro utente avesse gi� utilizzato il progressivo proposto, mando il messaggio e 
              *     faccio in modo che non rivenga calcolato con la CP_NEXTTABLEPROG di fine batch 
              ah_ErrorMsg("Riassegnato progressivo documento n.%1",,"",ALLTRIM(STR(this.oParentObject.w_PNNUMDOC,15)))
              this.oParentObject.op_PNNUMDOC = this.oParentObject.w_PNNUMDOC
              this.oParentObject.w_ONUMDOC = this.oParentObject.w_PNNUMDOC
              this.oParentObject.op_PNALFDOC = this.oParentObject.w_PNALFDOC
              this.oParentObject.w_OALFDOC = this.oParentObject.w_PNALFDOC
            endif
          endif
          * --- Registro vendite, cerca Numero, Alfa Documento
          this.w_OK = UNIVOC(iif(this.w_CFUNC="Load",Space(10),this.oParentObject.w_PNSERIAL),SPACE(10),"V",this.oParentObject.w_PNNUMDOC,this.oParentObject.w_PNALFDOC,this.oParentObject.w_PNANNDOC,cp_CharToDate("  /  /    "),this.oParentObject.w_PNPRD,0,SPACE(2),SPACE(4), SPACE(15), " ", "R")
          this.w_OK2 = this.w_OK
        case this.oParentObject.w_PNTIPREG="A" and this.oParentObject.w_FLPPRO <> "N"
          if this.oParentObject.op_PNNUMPRO=this.oParentObject.w_PNNUMPRO AND this.oParentObject.op_PNALFPRO=this.oParentObject.w_PNALFPRO AND this.oParentObject.w_PNANNPRO<>"    " AND this.w_CFUNC="Load"
            * --- Alla conferma ricalcolo sempre il primo progressivo utile cos� se � gi� stato inserito da altro utente non da problemi
            i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
            cp_AskTableProg(this.oParentObject, i_Conn, "PRPRO", "i_codazi,w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO")
            if this.oParentObject.op_PNNUMPRO<>this.oParentObject.w_PNNUMPRO OR this.oParentObject.op_PNALFPRO<>this.oParentObject.w_PNALFPRO
              * --- Nel caso un'altro utente avesse gi� utilizzato il progressivo proposto, mando il messaggio e 
              *     faccio in modo che non rivenga calcolato con la CP_NEXTTABLEPROG di fine batch e della mInsert()
              ah_ErrorMsg("Riassegnato progressivo protocollo n.%1",,"",ALLTRIM(STR(this.oParentObject.w_PNNUMPRO)))
              this.oParentObject.op_PNNUMPRO = this.oParentObject.w_PNNUMPRO
              this.oParentObject.w_ONUMPRO = this.oParentObject.w_PNNUMPRO
              this.oParentObject.op_PNALFPRO = this.oParentObject.w_PNALFPRO
              this.oParentObject.w_OALFPRO = this.oParentObject.w_PNALFPRO
            endif
          endif
          this.w_OK = UNIVOC(iif(this.w_CFUNC="Load",Space(10),this.oParentObject.w_PNSERIAL),SPACE(10),this.oParentObject.w_PNTIPREG,0,SPACE(2),SPACE(4),cp_CharToDate("  /  /    "),this.oParentObject.w_PNPRP,this.oParentObject.w_PNNUMPRO,this.oParentObject.w_PNALFPRO,this.oParentObject.w_PNANNPRO,Space(15),Space(1),"P")
          this.w_OK2 = this.w_OK
          * --- Controllo su stesso numero stessa data e fornitore
          if this.w_OK and this.oParentObject.w_FLPDOC<>"N" and this.oParentObject.w_FLPDIF<>"S"
            * --- In caricamento non tento di escludere me stesso dalla ricerca perch� la
            *     registrazione corrente non � ancora sul database (batch lanciato dalla checkform)
            this.w_OK = UNIVOC(IIF( this.w_CFUNC="Load",Space(10),this.oParentObject.w_PNSERIAL),SPACE(10),this.oParentObject.w_PNTIPREG,this.oParentObject.w_PNNUMDOC,this.oParentObject.w_PNALFDOC,Space(4),this.oParentObject.w_PNDATDOC,Space(2),0,SPACE(2),SPACE(4), this.oParentObject.w_PNCODCLF, this.oParentObject.w_PNTIPCLF, "D")
            this.w_OK2 = this.w_OK
          endif
        case this.oParentObject.w_PNTIPREG="N" AND this.oParentObject.w_TESDOC="S"
          * --- No iva, cerca Numero, Alfa, Data, Fornitore Documento; Numero, Alfa Protocollo
          this.w_oMess1=createobject("Ah_Message")
          this.w_TESMES = .T.
          this.w_TIPCLF = IIF(Not Empty(this.oParentObject.w_PNTIPCLF),this.oParentObject.w_PNTIPCLF," ")
          * --- Select from GSCG3QUA
          do vq_exec with 'GSCG3QUA',this,'_Curs_GSCG3QUA','',.f.,.t.
          if used('_Curs_GSCG3QUA')
            select _Curs_GSCG3QUA
            locate for 1=1
            do while not(eof())
            this.w_NR = NVL(_Curs_GSCG3QUA.PNNUMRER, 0)
            this.w_DR = CP_TODATE(_Curs_GSCG3QUA.PNDATREG)
            this.w_TIPRNU = _Curs_GSCG3QUA.TIPO
            do case
              case this.oParentObject.w_FLPPRO<>"N" AND this.oParentObject.w_FLPDOC<>"N"
                this.w_oMess1.AddMsgPartNL("Numero documento e\o numero protocollo gi� esistente")     
                this.w_oPart = this.w_oMess1.AddMsgPartNL("(Primanota reg.n.%1 del. %2)")
                this.w_oPart.AddParam(STR(this.w_NR,6,0))     
                this.w_oPart.AddParam(DTOC(this.w_DR))     
                this.w_TESMES = .F.
              case this.oParentObject.w_FLPPRO<>"N"
                this.w_oMess1.AddMsgPartNL("Numero protocollo gi� esistente")     
                this.w_oPart = this.w_oMess1.AddMsgPartNL("(Primanota reg.n. %1 del. %2)")
                this.w_oPart.AddParam(STR(this.w_NR,6,0))     
                this.w_oPart.AddParam(DTOC(this.w_DR))     
                this.w_TESMES = .F.
              case this.oParentObject.w_FLPDOC<>"N"
                this.w_oMess1.AddMsgPartNL("Numero documento gi� esistente")     
                this.w_oPart = this.w_oMess1.AddMsgPartNL("(Primanota reg.n. %1 del. %2)")
                this.w_oPart.AddParam(STR(this.w_NR,6,0))     
                this.w_oPart.AddParam(DTOC(this.w_DR))     
                this.w_TESMES = .F.
            endcase
            if (this.oParentObject.w_FLPPRO<>"N" OR this.oParentObject.w_FLPDOC<>"N") AND NOT this.w_TESMES
              * --- Se � attivo nei dati azienda il blocco sul controllo univocit� non fa confermare
              * --- Read from AZIENDA
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.AZIENDA_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "AZFLUNIV"+;
                  " from "+i_cTable+" AZIENDA where ";
                      +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  AZFLUNIV;
                  from (i_cTable) where;
                      AZCODAZI = i_CODAZI;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_AZFLUNIV = NVL(cp_ToDate(_read_.AZFLUNIV),cp_NullValue(_read_.AZFLUNIV))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if this.w_AZFLUNIV="S" AND this.w_TIPRNU<>"L"
                this.w_oMess1.AddMsgPartNL("Impossibile confermare")     
                this.w_oMess1.Ah_ErrorMsg()     
                this.w_OK = .F.
              else
                this.w_oMess1.AddMsgPartNL("Confermi ugualmente?")     
                this.w_OK = this.w_oMess1.ah_YesNo()
              endif
            endif
              select _Curs_GSCG3QUA
              continue
            enddo
            use
          endif
          if ! this.w_OK
            this.w_oMess.AddMsgPartNL("Operazione abbandonata")     
          endif
          this.w_OK2 = this.w_OK
      endcase
      WAIT CLEAR
    endif
    if this.w_OK AND this.oParentObject.w_PNTIPREG="E" AND this.oParentObject.w_PNTIPDOC="CO"
      * --- Registrazione Corrispettivi Ventilazione (non ci sono righe IVA e quindi testa Immediatamente)
      * --- Viceversa il Test viene eseguito in GSCG_BIK
      * --- Testa la Data piu' elevata tra Data Reg. e Data Competenza IVA
      this.w_DATTEST = MIN(this.oParentObject.w_PNDATREG, this.oParentObject.w_PNCOMIVA)
      this.w_DATBLO = cp_CharToDate("  -  -  ")
      this.w_STALIG = cp_CharToDate("  -  -  ")
      this.w_CONTA = 0
      * --- Select from ATTIDETT
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ATDATSTA,ATDATBLO  from "+i_cTable+" ATTIDETT ";
            +" where ATNUMREG="+cp_ToStrODBC(this.oParentObject.w_PNNUMREG)+" AND ATTIPREG="+cp_ToStrODBC(this.oParentObject.w_PNTIPREG)+"";
             ,"_Curs_ATTIDETT")
      else
        select ATDATSTA,ATDATBLO from (i_cTable);
         where ATNUMREG=this.oParentObject.w_PNNUMREG AND ATTIPREG=this.oParentObject.w_PNTIPREG;
          into cursor _Curs_ATTIDETT
      endif
      if used('_Curs_ATTIDETT')
        select _Curs_ATTIDETT
        locate for 1=1
        do while not(eof())
        this.w_STALIG = CP_TODATE(_Curs_ATTIDETT.ATDATSTA)
        this.w_DATBLO = CP_TODATE(_Curs_ATTIDETT.ATDATBLO)
        this.w_CONTA = this.w_CONTA+1
        if this.w_CONTA>1
          this.w_oMess.AddMsgPartNL("Esistono pi� attivit� associate allo stesso registro; verificare attivit�")     
          this.w_OK = .F.
        endif
          select _Curs_ATTIDETT
          continue
        enddo
        use
      endif
      do case
        case this.w_DATTEST <= this.w_STALIG AND NOT EMPTY(this.w_STALIG)
          this.w_oMess.AddMsgPartNL("Data registrazione inferiore o uguale a ultima stampa registri IVA")     
          this.w_OK = .F.
        case this.w_DATTEST <= this.w_DATBLO AND NOT EMPTY(this.w_DATBLO)
          this.w_oMess.AddMsgPartNL("Stampa registri IVA in corso con selezione comprendente la registrazione")     
          this.w_OK = .F.
      endcase
    endif
    * --- 4) Segnala in caso di acquisto se esiste un documento con data inferiore e num prot. maggiore
    *     o se l'utente modifica il protocollo proposto
    if this.w_OK And this.w_CFUNC<>"Query" 
      do case
        case this.oParentObject.w_PNTIPREG="A" and this.oParentObject.w_FLPPRO <> "N"
          * --- Controlla che NON vi siano registrazioni (P. Nota) con numero minore e data maggiore
          * --- Oppure che non vi siano registrazioni con numero maggiore e data minore
          if this.oParentObject.w_FLCHKPRO AND this.oParentObject.w_PNPRP<>"NN"
            this.w_MESS = CHKPROT(this.oParentObject.w_PNNUMPRO,this.oParentObject.w_PNALFPRO,this.oParentObject.w_PNDATREG,this.oParentObject.w_PNANNPRO,this.oParentObject.w_PNPRP)
            if NOT EMPTY(this.w_MESS)
              this.w_OK = ah_YesNo("%1%0Confermi ugualmente?",,this.w_MESS)
              if NOT this.w_OK
                this.w_oMess.AddMsgPartNL("Operazione abbandonata")     
              endif
            endif
          endif
          if this.w_OK
            if this.oParentObject.w_PNNUMPRO<>this.oParentObject.op_PNNUMPRO OR this.oParentObject.w_PNALFPRO<>this.oParentObject.op_PNALFPRO
              this.w_OK = ah_YesNo("Numero e alfa protocollo modificati%0confermi ugualmente?")
              if NOT this.w_OK
                this.w_oMess.AddMsgPartNL("Operazione abbandonata")     
              endif
            endif
          endif
          this.w_OK2 = this.w_OK
        case this.oParentObject.w_PNTIPREG="V"
          * --- Controlla che NON vi siano registrazioni (P. Nota) con numero minore e data maggiore
          *     Oppure che non vi siano registrazioni con numero maggiore e data minore
          *     I controlli vanno fatti in base all'anno solare e non all'esercizio
          * --- Nel caso sia in caricamento non filtro sul seriale (la registrazione non � ancora sul
          *     database)
          if this.oParentObject.w_FLCHKNUMD
            this.w_TMPS = CHKNUMD(this.oParentObject.w_PNDATDOC,this.oParentObject.w_PNNUMDOC,this.oParentObject.w_PNALFDOC,this.oParentObject.w_PNANNDOC,this.oParentObject.w_PNPRD,IIF(this.w_CFUNC="Edit",this.oParentObject.w_PNSERIAL,Space(10)),"P")
            if Not Empty( this.w_TMPS )
              this.w_OK = ah_YesNo("%1%0Confermi ugualmente?",,this.w_TMPS)
              if NOT this.w_OK
                this.w_oMess.AddMsgPartNL("Operazione abbandonata")     
              endif
            endif
          endif
          if this.w_OK And ( this.oParentObject.w_PNNUMDOC<>this.oParentObject.op_PNNUMDOC OR this.oParentObject.w_PNALFDOC<>this.oParentObject.op_PNALFDOC )
            this.w_OK = ah_YesNo("Numero e alfa documento modificati%0confermi ugualmente?")
            if not this.w_OK
              this.w_oMess.AddMsgPartNL("Operazione abbandonata")     
            endif
          endif
          this.w_OK2 = this.w_OK
      endcase
    endif
    * --- Se il modulo ritenute � attivato controllo se ho dei riferimenti nei movimenti ritenute.
    if g_RITE="S" AND this.w_OK AND this.w_CFUNC<>"Load"
      * --- Select from GSRI_BCK
      do vq_exec with 'GSRI_BCK',this,'_Curs_GSRI_BCK','',.f.,.t.
      if used('_Curs_GSRI_BCK')
        select _Curs_GSRI_BCK
        locate for 1=1
        do while not(eof())
        this.w_OK = ah_YesNo("Registrazione collegata a movimenti ritenute%0confermi ugualmente?")
        if NOT this.w_OK
          this.w_oMess.AddMsgPartNL("Registrazione collegata a movimenti ritenute")     
        endif
        this.w_ElimRif = .T.
        Exit
          select _Curs_GSRI_BCK
          continue
        enddo
        use
      endif
    endif
    * --- Se Cancellazione, Elimina il Riferimento all' Eventuale Distinta o Documento
    if this.w_OK AND this.w_CFUNC="Query"
      * --- Try
      local bErr_039BA7B8
      bErr_039BA7B8=bTrsErr
      this.Try_039BA7B8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_039BA7B8
      * --- End
    endif
    * --- Controllo cancellazione/modifica registrazione di Storno Iva ad esigibilit� differita
    * --- Controlla se il Documento e' stato Generato da una Contabilizzazione
    if this.w_OK AND this.w_CFUNC<>"Query"
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_OK AND "CESP" $ UPPER(i_cModules) 
      * --- Controlllo congruenza con movimenti cespiti associati
      * --- Verifico che il transitorio sia stato valorizzato nel caricamento contestuale e non sia invece 
      *     'ereditato' dalla registrazione precedente
      * --- Il ramo del primo if vale solo se si � entrati nella maschera gscg_mac
      if type("THIS.OPARENTOBJECT.GSCG_MAC.CNT")="O" and THIS.OPARENTOBJECT.GSCG_MAC.CNT.BUPDATED
        SELECT * FROM (THIS.OPARENTOBJECT.GSCG_MAC.CNT.CTRSNAME) INTO CURSOR CESP1 WHERE NOT EMPTY(NVL(t_ACMOVCES,SPACE(10))) AND t_SER__PNT=THIS.OPARENTOBJECT.w_PNSERIAL
        * --- Se il transitorio non � vuoto mi baso su quello
        if RECCOUNT("CESP1")>0
          if this.w_CFUNC<>"Query"
            SELECT CESP1
            GO TOP
            SCAN
            this.w_ACMOVCES = NVL(t_ACMOVCES,SPACE(10))
            vq_exec("..\EXE\GSCG5BCK.VQR",this,"CESP")
            if RECCOUNT("CESP")>0
              SELECT CESP
              this.w_MCNUMREG = NVL(MCNUMREG,0)
              this.w_MCCODESE = NVL(MCCODESE,SPACE(4))
              this.w_MCDATREG = NVL(MCDATREG,cp_CharToDate("  -  -    "))
              this.w_MESS = CHKCESP(this.w_ACMOVCES, this.oParentObject.w_PNNUMDOC,this.oParentObject.w_PNDATDOC, this.oParentObject.w_PNALFDOC,this.oParentObject.w_PNCODCLF,this.oParentObject.w_PNTIPCLF, 0)
              if NOT EMPTY(this.w_MESS) AND this.oParentObject.w_FLRIFE<>"N"
                if ! ah_YesNo("%1 del movimento cespite associato n� %2/%3 del %4 non congruente%0confermi l'elaborazione?",,this.w_MESS,ALLTRIM(STR(this.w_MCNUMREG)),ALLTRIM(this.w_MCCODESE),DTOC(this.w_MCDATREG))
                  this.w_oMess.AddMsgPartNL("Elaborazione interrotta")     
                  this.w_OK = .F.
                  * --- Esco dallo scan...endscan per non inserire ripetizioni del messaggio precedente
                  exit
                endif
                this.w_PRIMA = .T.
              endif
              SELECT CESP1
            endif
            ENDSCAN
          else
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.w_PRESENTE = .T.
        else
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        * --- Se sono in caricamento e la registrazione non gestisce i cespiti, non devo far nulla
        if this.w_CFUNC<>"Load"
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      if this.w_CFUNC<>"Load" 
        if NVL(this.oParentObject.w_PNRIFCES, "N") = "S" 
          if this.w_PRIMA=.F.
            this.w_OK = ah_YesNo("Registrazione generata dai movimenti cespiti.%0Confermi ugualmente?")
            if ! this.w_OK
              this.w_oMess.AddMsgPartNL("Operazione abbandonata")     
            endif
          endif
          if this.w_OK=.T. AND this.w_CFUNC<>"Query" AND this.oParentObject.w_PNFLPROV="N"
            * --- Se Reg.Confermata, verifico l'esistenza di reg.Provvisorie
            vq_exec("..\CESP\EXE\QUERY\GSCG2BCK.VQR",this,"ASSEGN")
            if USED("ASSEGN")
              SELECT ASSEGN
              if RECCOUNT("ASSEGN")>0
                this.w_ConfRif = ah_YesNo("Esistono movimenti cespite provvisori; Si desidera confermarli?")
              endif
              USE
            endif
          endif
        else
          if this.w_PRIMA=.F. 
            * --- Se il modulo cespiti � attivato controllo se ho dei movimenti cespiti abbinati in PNT_CESP
            vq_exec("..\CESP\EXE\QUERY\GSCE1BCK.VQR",this,"ASSEGN")
            if USED("ASSEGN")
              SELECT ASSEGN
              if RECCOUNT("ASSEGN")>0
                this.w_OK = ah_YesNo("Registrazione collegata a movimento cespite.%0Confermi ugualmente?")
                this.w_ElimRif = .T.
                if ! this.w_OK
                  this.w_oMess.AddMsgPartNL("Operazione abbandonata")     
                endif
              endif
              USE
            endif
          endif
        endif
      endif
      * --- Se il flag movimenti cespiti � attivo e ci sono conti di tipo cespite obbliga a inserire almeno un record nella tabella movimenti cespiti
      *     Se il flag movimenti cespiti � attivo e  c'� almeno un record nella tabella movimenti cespiti obbliga a inserire almeno un  conto di tipo cespite 
      if this.oParentObject.w_MOVCES="S" and this.w_CFUNC<>"Query" and this.w_OK
        if type("THIS.OPARENTOBJECT.GSCG_MAC.CNT")="O"
          if This.oParentObject.GSCG_MAC.CNT.nDeferredFillRec=2
            SELECT * FROM (This.oParentObject.GSCG_MAC.CNT.ctrsname) INTO CURSOR CESP3 WHERE NOT EMPTY(NVL(t_ACMOVCES,SPACE(10)))
            if reccount("CESP3")>0
              this.w_PRESENTE = .T.
            endif
          endif
        endif
        if this.w_CESPITE
          * --- se la tabella pnt_cesp � vuota
          if this.w_PRESENTE=.F.
            this.w_oMess.AddMsgPartNL("Non esistono movimenti cespiti abbinati al documento.")     
            this.w_oMess.AddMsgPartNL("Impossibile confermare")     
            this.w_OK = .F.
          endif
        else
          * --- se la tabella pnt_cesp � piena
          if this.w_PRESENTE
            this.w_oMess.AddMsgPartNL("Non � presente nessun conto di tipo cespite.")     
            this.w_oMess.AddMsgPartNL("Impossibile confermare")     
            this.w_OK = .F.
          endif
        endif
      endif
      if used("CESP")
        SELECT CESP
        USE
      endif
      if used("CESP1")
        SELECT CESP1
        USE
      endif
      if used("CESP2")
        SELECT CESP2
        USE
      endif
      if used("CESP3")
        SELECT CESP3
        USE
      endif
    endif
    * --- Controllo se � attivo il modulo ritenute se non � vuoto il codice fornitore, se � 
    *     stata selezionata una causale contabile con attivo il flag gestiione ritenute e
    *     se il fornitore � soggetto a ritenute.
    if this.w_OK and g_RITE="S" and not empty(this.oParentObject.w_PNCODCLF) and nvl(this.oParentObject.w_GESRIT," ")="S" and ((this.oParentObject.w_RITENU$"CS" and this.oParentObject.w_PNTIPCLF="F") or (this.oParentObject.w_FLGRIT="S" and this.oParentObject.w_PNTIPCLF="C")) and this.oParentObject.w_TOTMDR<>-0.000001
      * --- Verifico se il movimento di primanota ha attivo il flag "Storno immediato". Se non
      *     � valorizzato non effettuo nessun controllo.
      if this.oParentObject.w_PNFLGSTO="S"
        * --- Se sono in caricamento ma non sono ancora entrato nei dati ritenute non
        *     devo effettuare nessun controllo
        if Not(this.w_CFUNC<>"Edit" and Not (this.oParentObject.w_PCCLICK))
          * --- Devo verificare che il totale da versare impostato sui dati ritenute
          *     sia identico al totale delle partite legate al fornitore.
          *     Utilizzzo la variabile PCCLICK per verificare se sono entrato all'interno della
          *     gestione dei Dati Ritenute
          if this.oParentObject.w_PCCLICK=.T.
            this.w_TOTDAVER = This.oParentobject.Gscg_Mdr.cnt.w_Drdavers
          else
            * --- Effettuo una select del cursore Datirite per avere il totale delle ritenute
            this.w_TOTRITE = 0
            * --- Select from DATIRITE
            i_nConn=i_TableProp[this.DATIRITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DATIRITE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" DATIRITE ";
                  +" where DRSERIAL="+cp_ToStrODBC(this.oParentObject.w_PNSERIAL)+"";
                   ,"_Curs_DATIRITE")
            else
              select * from (i_cTable);
               where DRSERIAL=this.oParentObject.w_PNSERIAL;
                into cursor _Curs_DATIRITE
            endif
            if used('_Curs_DATIRITE')
              select _Curs_DATIRITE
              locate for 1=1
              do while not(eof())
              this.w_TOTRITE = this.w_TOTRITE+_Curs_DATIRITE.DRRITENU
                select _Curs_DATIRITE
                continue
              enddo
              use
            endif
            * --- Calcolo il totale da versare
            this.w_TOTRITE = ABS(this.w_TOTRITE)
            this.w_TOTMDR1 = ABS(this.oParentObject.w_TOTMDR)
            this.w_TOTDAVER = this.w_TOTMDR1-this.w_TOTRITE-abs(this.oParentObject.w_PNTOTENA)
          endif
          this.w_TOTDAVER = ABS(this.w_TOTDAVER)
          this.w_PADRE.MarkPos()     
           
 SELECT (this.w_PADRE.cTrsName) 
 GO TOP 
 SCAN FOR NVL(t_PNTIPCON," ") $ "F-C" AND NOT EMPTY(NVL(t_PNCODCON," ")) AND t_PNFLPART$"CS"
          this.w_TOTRITD = this.w_TOTRITD + NVL(t_PNIMPDAR, 0)
          this.w_TOTRITA = this.w_TOTRITA + NVL(t_PNIMPAVE, 0)
          ENDSCAN
          if ABS(this.w_TOTRITA-this.w_TOTRITD)<>this.w_TOTDAVER
            * --- IIF(w_PNTIPCLF='F','Totale da Pagare specificato nei dati ritenute [','Totale da Incassare specificato nei dati ritenute [')+Alltrim(Tran( w_TOTDAVER , v_PV[40+VVP]))+' ]'+CHR(13)
            if this.oParentObject.w_PNTIPCLF="F"
              this.w_oPart = this.w_oMess.AddMsgPartNL("Totale da pagare specificato nei dati ritenute [%1]%0diverso dal totale addebitato sul fornitore [%2]%0Confermi ugualmente?")
            else
              this.w_oPart = this.w_oMess.AddMsgPartNL("Totale da incassare specificato nei dati ritenute [%1]%0diverso dal totale accredtitato sul cliente [%2]%0Confermi ugualmente?")
            endif
            this.w_oPart.AddParam(Alltrim(Tran(this.w_TOTDAVER,v_PV[40+VVP])))     
            this.w_oPart.AddParam(Alltrim(Tran(ABS(this.w_TOTRITA-this.w_TOTRITD),v_PV[40+VVP])))     
            if !ah_YesNo(this.w_oMess.ComposeMessage())
              this.w_OK = .F.
            endif
          endif
          this.w_PADRE.RePos(.T.)     
        endif
      else
        if this.oParentObject.w_PNTOTENA<>0 and this.w_CFUNC<>"Query"
          * --- cerco partita enasarco
          this.w_PADRE.MarkPos()     
           
 SELECT (this.w_PADRE.cTrsName) 
 GO TOP 
 SCAN FOR NVL(t_PNTIPCON," ") $ "F-C" AND NOT EMPTY(NVL(t_PNCODCON," ")) AND t_PNFLPART="S"
          this.w_TOTIMP = NVL(t_PNIMPDAR, 0)-NVL(t_PNIMPAVE, 0)
          EXIT
          ENDSCAN
          this.w_PADRE.RePos()     
          do case
            case this.w_TOTIMP=0
              this.w_oPart = this.w_oMess.AddMsgPartNL("Attenzione, partita di saldo relativa all' ENASARCO non inserita%0Confermi ugualmente?")
              if !ah_YesNo(this.w_oMess.ComposeMessage())
                this.w_OK = .F.
              endif
            case this.oParentObject.w_PNTOTENA <>this.w_TOTIMP AND this.w_TOTIMP<>0
              this.w_oPart = this.w_oMess.AddMsgPartNL("Totale ENASARCO specificato nei dati ritenute [%1] %0diverso dal totale addebitato sul fornitore [%2]%0Confermi ugualmente?")
              this.w_oPart.AddParam(Alltrim(Tran(this.oParentObject.w_PNTOTENA,v_PV[40+VVP])))     
              this.w_oPart.AddParam(Alltrim(Tran(ABS(this.w_TOTIMP),v_PV[40+VVP])))     
              if !ah_YesNo(this.w_oMess.ComposeMessage())
                this.w_OK = .F.
              endif
          endcase
        endif
      endif
    endif
    if this.w_OK AND this.w_CFUNC="Edit"
      if (this.oParentObject.w_PNNUMDOC<>this.oParentObject.w_ONUMDOC or this.oParentObject.w_PNALFDOC<>this.oParentObject.w_OALFDOC) AND this.oParentObject.w_PNNUMDOC>=this.oParentObject.op_PNNUMDOC AND NOT EMPTY(this.oParentObject.w_PNANNDOC)
        * --- Variato Documento
        * --- Aggiornamento Progressivi Documento - SOLO IN MODIFICA -
        if ! this.oParentObject.w_MODNDO AND (this.oParentObject.w_PNANNDOC<>this.oParentObject.w_OLANDO OR this.oParentObject.w_PNALFDOC<>this.oParentObject.w_OLALDO OR this.oParentObject.w_PNPRD<>this.oParentObject.w_OLPRD)
          i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
          cp_NextTableProg(this.oParentObject, i_Conn, "PRDOC", "i_codazi,w_PNANNDOC,w_PNPRD,w_PNALFDOC,w_PNNUMDOC")
        endif
      endif
      if this.oParentObject.w_AGGPRO
        this.oParentObject.op_PNNUMPRO = this.oParentObject.w_PNNUMPRO
        this.oParentObject.op_PNALFPRO = this.oParentObject.w_PNALFPRO
      endif
      if (this.oParentObject.w_PNNUMPRO<>this.oParentObject.w_ONUMPRO or this.oParentObject.w_PNALFPRO<>this.oParentObject.w_OALFPRO) AND this.oParentObject.w_PNNUMPRO>=this.oParentObject.op_PNNUMPRO AND NOT EMPTY(this.oParentObject.w_PNANNPRO)
        * --- Variato Protocollo
        i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
        if ah_YesNo("Aggiorno la tabella progressivi numero protocollo?")
          cp_NextTableProg(this.oParentObject, i_Conn, "PRPRO", "i_codazi,w_PNANNPRO,w_PNPRP,w_PNALFPRO,w_PNNUMPRO")
          this.oParentObject.w_ONUMPRO = this.oParentObject.w_PNNUMPRO
          this.oParentObject.w_OALFPRO = this.oParentObject.w_PNALFPRO
        endif
      endif
      if this.w_OK=.T. AND this.w_ConfRif=.T.
        do GSCE_BDC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if Not this.w_OK
      * --- Abbandona la Transazione se in cancellazione altrimenti notifica alla
      *     checkform l'impossibilit� di confermare la registrazione
      this.w_MESS = this.w_oMess.ComposeMessage()
      if empty(this.w_MESS)
        this.w_MESS = ah_Msgformat("Operazione abbandonata")
      endif
      if this.w_CFUNC="Query"
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
      else
        ah_ErrorMsg("%1",,"", this.w_MESS)
        this.oParentObject.w_RESCHK = -1
      endif
    else
      * --- Verifica se ci sono Acconti da Abbinare
      if this.oParentObject.w_SEARCHACC<>"S" AND this.w_OKPART AND NOT EMPTY(this.oParentObject.w_PNTIPCLF) AND NOT EMPTY(this.oParentObject.w_PNCODCLF) AND this.w_CFUNC<>"Query" And Not this.oParentObject.w_PNTIPDOC $ "NC-NE-NU" And Not this.oParentObject.w_NOACC
        this.w_APPO = 0
        this.w_APPO1 = 0
        * --- Select from GSCG_QRC
        do vq_exec with 'GSCG_QRC',this,'_Curs_GSCG_QRC','',.f.,.t.
        if used('_Curs_GSCG_QRC')
          select _Curs_GSCG_QRC
          locate for 1=1
          do while not(eof())
          this.w_APPO = this.w_APPO + 1
          this.w_PTDATSCA = CP_TODATE(_Curs_GSCG_QRC.PTDATSCA)
          this.w_PTTOTIMP = NVL(_Curs_GSCG_QRC.PTTOTIMP,0)
          this.w_APPO1 = this.w_APPO1 + (this.w_PTTOTIMP * IIF(NVL(_Curs_GSCG_QRC.PT_SEGNO, " ")="A", -1, 1))
            select _Curs_GSCG_QRC
            continue
          enddo
          use
        endif
        if this.w_APPO>0
          if this.w_APPO>1
            if this.oParentObject.w_PNTIPCLF="C"
              this.w_oPart = this.w_oMess.AddMsgPartNL("Esistono degli acconti, associati al cliente %1")
            else
              this.w_oPart = this.w_oMess.AddMsgPartNL("Esistono degli acconti, associati al fornitore %1")
            endif
          else
            if this.oParentObject.w_PNTIPCLF="C"
              this.w_oPart = this.w_oMess.AddMsgPartNL("Esiste un acconto, associato al cliente %1")
            else
              this.w_oPart = this.w_oMess.AddMsgPartNL("Esiste un acconto, associato al fornitore %1")
            endif
          endif
          this.w_oPart.AddParam(this.oParentObject.w_PNCODCLF)     
          this.w_oMess.AddMsgPartNL("Si vogliono visualizzare tali acconti caricati nell'archivio delle partite?")     
          this.w_OK = this.w_oMess.ah_YesNo()
          this.oParentObject.w_FLAGAC = IIF(this.w_OK=.T., "S", " ")
          this.oParentObject.w_SEARCHACC = IIF(this.w_OK=.T., "S", " ")
        endif
        if this.oParentObject.w_FLAGAC="S"
          * --- Apro Maschera Saldaconto per Visualizzare Acconti Disponibili
          if Type("this.oParentObject.gscg_mpa")="O"
            this.w_PADRE.FirstRow()     
            this.w_RIGA = this.w_PADRE.search("NVL(t_PNCODCON, SPACE(15)) = "+cp_tostr( this.oParentObject.w_PNCODCLF ) +" AND NVL(t_PNTIPCON, ' ') = "+cp_ToStr( this.oParentObject.w_PNTIPCLF ) +" And Not Deleted() ")
            if this.w_RIGA<>-1
              this.w_PADRE.SetRow(this.w_RIGA)     
              this.w_Padre.WorkFromTrs()     
              i_NREC=recno()
              this.oParentObject.GSCG_MPA.ChangeRow(this.w_PADRE.cRowID+ str(i_NREC,7,0), 0, .w_PNSERIAL, "PTSERIAL", this.w_RIGA,"PTROWORD")     
              this.oParentObject.GSCG_MPA.LinkPCClick()     
              this.w_GSCG_MPA = this.oParentObject.GSCG_MPA.Cnt
              if Empty(this.oParentObject.w_PARSAL)
                * --- Nel caso in cui non � stata eseguita la BLANCK nel gscg_mpa
                *     devo rilanciare l'apertura della maschera sladaconto eseguendo un evento
                *     specifico    Acconti
                this.w_GSCG_MPA.Notifyevent("Acconti")     
              endif
              this.oParentObject.w_RESCHK = -2
            endif
          endif
        endif
      endif
    endif
    * --- Controlli su castelletto IVA per il regime del margine 
    *      --->Il controllo avviene solo se la causale ha attivo il check 'Regime del margine' attivo<---
    if this.oParentObject.w_RESCHK = 0 and (this.oParentObject.w_REGMARG="S" OR g_TRAEXP $ "C-A-G")
      * --- Causale con check regime del margine attivo
      * --- Verifico congruit� richiesta dal regime del margine sui registri presenti nel castelletto IVA
      this.w_CASTIVA = this.w_PADRE.GSCG_MIV
      this.w_CASTIVA.MarkPos()     
      this.w_CASTIVA.FirstRow()     
      do while not this.w_CASTIVA.Eof_Trs()
        if this.w_CASTIVA.FullRow() and this.w_CASTIVA.Get("t_IVIMPONI") # 0
          this.w_CONTROPAR = this.w_CASTIVA.Get("t_IVCONTRO")
          if this.oParentObject.w_REGMARG="S"
            this.w_FLGREMA = this.w_CASTIVA.Get("t_REGMAR")
            this.w_TIPREG = IIF(this.w_CASTIVA.Get("t_IVTIPREG")=1,"V",IIF(this.w_CASTIVA.Get("t_IVTIPREG")=2,"A","C"))
            this.w_NUMREG = this.w_CASTIVA.Get("t_IVNUMREG")
            * --- Read from ATTIDETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ATTIDETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ATMARREG"+;
                " from "+i_cTable+" ATTIDETT where ";
                    +"ATTIPREG = "+cp_ToStrODBC(this.w_TIPREG);
                    +" and ATNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ATMARREG;
                from (i_cTable) where;
                    ATTIPREG = this.w_TIPREG;
                    and ATNUMREG = this.w_NUMREG;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MARREG = NVL(cp_ToDate(_read_.ATMARREG),cp_NullValue(_read_.ATMARREG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            do case
              case this.w_FLGREMA # " "
                * --- Caso 1:
                *                 codice IVA con combo 'beni usati' valorizzata
                if this.w_MARREG # "S"
                  * --- Blocco il salvataggio poich� esiste almeno un registro IVA che non ha attivo  il check 'regime del margine' nell'attivit�.
                  if ! Ah_YesNo("Registro IVA %1 di tipo %2 non ha attivo il check regime del margine.%0Confermi ugualmente?",48,this.w_NUMREG,iif(this.w_TIPREG="A","Acquisti",iif(this.w_TIPREG="V","Vendite","Corrispettivi")))
                    this.oParentObject.w_RESCHK = -3
                    exit
                  endif
                endif
              case this.w_FLGREMA = " "
                * --- Caso 2:
                *                 codice IVA con combo 'beni usati' non valorizzata
                if this.w_TIPREG $ "V-C" and this.w_MARREG = "S"
                  * --- Blocco il salvataggio poich� esiste almeno un registro IVA che ha attivo  il check 'regime del margine' nell'attivit�.
                  if ! Ah_YesNo("Il codice IVA %1 non gestisce il margine.%0Confermi ugualmente?",48,alltrim(this.w_CASTIVA.Get("t_IVCODIVA")))
                    this.oParentObject.w_RESCHK = -4
                    exit
                  endif
                endif
            endcase
          endif
          * --- Controllo che la contropartita sia presente nel dettaglio contabile
          if (this.w_CONTROPA OR this.w_CONTROACQ) AND !empty(this.w_DATINITRA) AND g_TRAEXP $ "C-A-G" AND this.oParentObject.w_PNDATREG>=this.w_DATINITRA AND ASCAN(CONTROPARTITE,this.w_CONTROPAR) = 0 
            if this.oParentObject.w_PNTIPDOC<>"FE" and this.oParentObject.w_PNTIPDOC<>"NE" and this.oParentObject.w_PNTIPDOC<>"AU" and this.oParentObject.w_PNTIPDOC<>"NU"
              ah_ErrorMsg("Contropartita IVA %1 incongruente con contropartita contabile o non presente",,"",ALLTRIM(this.w_CONTROPAR))
              this.oParentObject.w_RESCHK = -6
              exit
            else
              if !AH_YESNO ("Contropartita IVA %1 incongruente con contropartita contabile o non presente%0Confermi ugualmente ?","",ALLTRIM(this.w_CONTROPAR))
                this.oParentObject.w_RESCHK = -6
                exit
              endif
            endif
          endif
        endif
        this.w_CASTIVA.NextRow()     
      enddo
      this.w_CASTIVA.RePos()     
    endif
  endproc
  proc Try_039BA7B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case NOT EMPTY(NVL(this.oParentObject.w_PNRIFINC, " "))
        * --- Write into INC_CORR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.INC_CORR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INC_CORR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.INC_CORR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"INRIFCON ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'INC_CORR','INRIFCON');
          +",INFLCONT ="+cp_NullLink(cp_ToStrODBC(" "),'INC_CORR','INFLCONT');
              +i_ccchkf ;
          +" where ";
              +"INSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNRIFINC);
                 )
        else
          update (i_cTable) set;
              INRIFCON = SPACE(10);
              ,INFLCONT = " ";
              &i_ccchkf. ;
           where;
              INSERIAL = this.oParentObject.w_PNRIFINC;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Write into INCDCORR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.INCDCORR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INCDCORR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.INCDCORR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"IN_RIFCO ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'INCDCORR','IN_RIFCO');
          +",IN_FLCON ="+cp_NullLink(cp_ToStrODBC(" "),'INCDCORR','IN_FLCON');
              +i_ccchkf ;
          +" where ";
              +"IN_RIFCO = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
                 )
        else
          update (i_cTable) set;
              IN_RIFCO = SPACE(10);
              ,IN_FLCON = " ";
              &i_ccchkf. ;
           where;
              IN_RIFCO = this.oParentObject.w_PNSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case NOT EMPTY(NVL(this.oParentObject.w_PNRIFDOC, " "))
        ah_Msg("Annullo contabilizzazione documenti collegati...",.T.)
        do case
          case NVL(this.oParentObject.w_PNRIFACC," ")="S"
            * --- Write into DOC_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVRIFACC ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_MAST','MVRIFACC');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNRIFDOC);
                     )
            else
              update (i_cTable) set;
                  MVRIFACC = SPACE(10);
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.oParentObject.w_PNRIFDOC;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          case this.oParentObject.w_PNRIFDOC="CORRISP"
            * --- Write into DOC_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVRIFCON ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_MAST','MVRIFCON');
              +",MVFLCONT ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCONT');
                  +i_ccchkf ;
              +" where ";
                  +"MVRIFCON = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
                  +" and MVFLCONT = "+cp_ToStrODBC("S");
                     )
            else
              update (i_cTable) set;
                  MVRIFCON = SPACE(10);
                  ,MVFLCONT = " ";
                  &i_ccchkf. ;
               where;
                  MVRIFCON = this.oParentObject.w_PNSERIAL;
                  and MVFLCONT = "S";

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          otherwise
            * --- Contabilizzazione, verifica eventuali Acconti
            *     Leggo dal documento acconto precedente e vecchio acconto (quest'ultimo
            *     valorizzato al momento della contabilizzazione se accorpati acconti).
            *     Se MVACCPRE>MVACCOLD allora debbo ricostruire le rate sul documento
            *     Nel caso di Flag Accorpa Acconti poich� solo in tale caso pu� essere modificato il rapporto 
            *     nel documento fra rate e acconti
            if NOT EMPTY(this.oParentObject.w_PNCODCLF) AND this.oParentObject.w_PNTIPCLF $ "CF" 
              this.w_MVACCPRE = 0
              this.w_MVACCOLD = 0
              * --- Read from DOC_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MVACCPRE,MVACCOLD"+;
                  " from "+i_cTable+" DOC_MAST where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNRIFDOC);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MVACCPRE,MVACCOLD;
                  from (i_cTable) where;
                      MVSERIAL = this.oParentObject.w_PNRIFDOC;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_MVACCPRE = NVL(cp_ToDate(_read_.MVACCPRE),cp_NullValue(_read_.MVACCPRE))
                this.w_MVACCOLD = NVL(cp_ToDate(_read_.MVACCOLD),cp_NullValue(_read_.MVACCOLD))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Ricostruisco partite e storno acconti precedenti...
              if this.w_MVACCPRE>this.w_MVACCOLD AND (this.oParentObject.w_FLAACC="S" or this.w_MVACCOLD<>0)
                do GSVE_BAC with this
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
            * --- Elimino riferimenti contabilizzazione sul documento
            * --- Write into DOC_MAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVRIFCON ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_MAST','MVRIFCON');
              +",MVFLCONT ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCONT');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNRIFDOC);
                     )
            else
              update (i_cTable) set;
                  MVRIFCON = SPACE(10);
                  ,MVFLCONT = " ";
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.oParentObject.w_PNRIFDOC;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
        endcase
        if Isalt()
          * --- Write into PRE_STAZ
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PRRIFCON ="+cp_NullLink(cp_ToStrODBC(Space(10)),'PRE_STAZ','PRRIFCON');
                +i_ccchkf ;
            +" where ";
                +"PRRIFCON = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
                   )
          else
            update (i_cTable) set;
                PRRIFCON = Space(10);
                &i_ccchkf. ;
             where;
                PRRIFCON = this.oParentObject.w_PNSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        WAIT CLEAR
      case NOT EMPTY(NVL(this.oParentObject.w_PNRIFDIS, " "))
        if VAL(this.oParentObject.w_PNRIFDIS)<0
          * --- Elimino la reg. di Contabilizzazione Indiretta
          this.w_RIFDIS = "0"+RIGHT(this.oParentObject.w_PNRIFDIS,9)
          * --- Recupero tutte le partite legate alla contabilizzazione indiretta effetti
          *     che hanno PTRIFIND=w_PNSERIAL  e PTRIFIND=w_RIFDIS 
          *     (questo filtro lascaito per velocizzare)
          * --- Select from gscg_bhe
          do vq_exec with 'gscg_bhe',this,'_Curs_gscg_bhe','',.f.,.t.
          if used('_Curs_gscg_bhe')
            select _Curs_gscg_bhe
            locate for 1=1
            do while not(eof())
            * --- Aseegno i valori letti sulle scadenze
            this.w_PNUMPAR = _Curs_gscg_bhe.PTNUMPAR
            this.w_PTDATSCA = _Curs_gscg_bhe.PTDATSCA
            this.w_PTTIPCON = _Curs_gscg_bhe.PTTIPCON
            this.w_PCODCON = _Curs_gscg_bhe.PTCODCON
            this.w_PTOTIMP = _Curs_gscg_bhe.PTTOTIMP
            this.w_PCODVAL = _Curs_gscg_bhe.PTCODVAL
            this.w_PSEGNO = IIF(_Curs_gscg_bhe.PT_SEGNO="D","A","D")
            this.w_PSERRIF = _Curs_gscg_bhe.PTSERRIF
            this.w_PORDRIF = _Curs_gscg_bhe.PTORDRIF
            this.w_PNUMRIF = _Curs_gscg_bhe.PTNUMRIF
            * --- Cerco una partita in distinta con stesso numero partita, data scadenza, 
            *     tipo e codice conto, valuta e di segno opposto  e PTFLIMPE=DI.
            *     Non pu� essere diverso perch� GSCG_BHE blocca la modifica/cancellazione
            *     Inoltre deve avere PTFLINDI='S', questo valore � impostato in GSCG_BCI
            *     al momento della contabilizzazione indiretta effetti
            if Not empty(this.w_PSERRIF)
              * --- Read from PAR_TITE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PTSERIAL,CPROWNUM"+;
                  " from "+i_cTable+" PAR_TITE where ";
                      +"PTSERRIF = "+cp_ToStrODBC(this.w_PSERRIF);
                      +" and PTORDRIF = "+cp_ToStrODBC(this.w_PORDRIF);
                      +" and PTNUMRIF = "+cp_ToStrODBC(this.w_PNUMRIF);
                      +" and PTROWORD = "+cp_ToStrODBC(-3);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PTSERIAL,CPROWNUM;
                  from (i_cTable) where;
                      PTSERRIF = this.w_PSERRIF;
                      and PTORDRIF = this.w_PORDRIF;
                      and PTNUMRIF = this.w_PNUMRIF;
                      and PTROWORD = -3;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_DISTINTA = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
                this.w_NUMDIS = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Write into PAR_TITE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"PTFLINDI ="+cp_NullLink(cp_ToStrODBC(Space(1)),'PAR_TITE','PTFLINDI');
                +",PTSERRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PSERRIF),'PAR_TITE','PTSERRIF');
                +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PORDRIF),'PAR_TITE','PTORDRIF');
                +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PNUMRIF),'PAR_TITE','PTNUMRIF');
                    +i_ccchkf ;
                +" where ";
                    +"PTSERRIF = "+cp_ToStrODBC(this.w_DISTINTA);
                    +" and PTORDRIF = "+cp_ToStrODBC(-3);
                    +" and PTNUMRIF = "+cp_ToStrODBC(this.w_NUMDIS);
                    +" and PTROWORD = "+cp_ToStrODBC(-2);
                       )
              else
                update (i_cTable) set;
                    PTFLINDI = Space(1);
                    ,PTSERRIF = this.w_PSERRIF;
                    ,PTORDRIF = this.w_PORDRIF;
                    ,PTNUMRIF = this.w_PNUMRIF;
                    &i_ccchkf. ;
                 where;
                    PTSERRIF = this.w_DISTINTA;
                    and PTORDRIF = -3;
                    and PTNUMRIF = this.w_NUMDIS;
                    and PTROWORD = -2;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              * --- Svuoto PTFLINDI ed aggiorno i riferimenti della partita legata alla distinta
              *     che non punter� pi� alla partita legata alla contabilizzazione indiretta (-3)
              *     ma alla partita di creazione. Quindi erediter� i riferimenti presenti sulla
              *     partita di contabilizzazione
              * --- Select from PAR_TITE
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select PTFLIMPE,PTSERIAL,CPROWNUM  from "+i_cTable+" PAR_TITE ";
                    +" where PTTIPCON="+cp_ToStrODBC(this.w_PTTIPCON)+" AND PTCODCON="+cp_ToStrODBC(this.w_PCODCON)+" AND PTNUMPAR="+cp_ToStrODBC(this.w_PNUMPAR)+" AND PTDATSCA="+cp_ToStrODBC(this.w_PTDATSCA)+" AND PTCODVAL= "+cp_ToStrODBC(this.w_PCODVAL)+" AND PTTOTIMP="+cp_ToStrODBC(this.w_PTOTIMP)+" AND PT_SEGNO="+cp_ToStrODBC(this.w_PSEGNO)+" AND PTROWORD= -2 And PTFLIMPE='DI' And PTFLINDI='S'";
                     ,"_Curs_PAR_TITE")
              else
                select PTFLIMPE,PTSERIAL,CPROWNUM from (i_cTable);
                 where PTTIPCON=this.w_PTTIPCON AND PTCODCON=this.w_PCODCON AND PTNUMPAR=this.w_PNUMPAR AND PTDATSCA=this.w_PTDATSCA AND PTCODVAL= this.w_PCODVAL AND PTTOTIMP=this.w_PTOTIMP AND PT_SEGNO=this.w_PSEGNO AND PTROWORD= -2 And PTFLIMPE="DI" And PTFLINDI="S";
                  into cursor _Curs_PAR_TITE
              endif
              if used('_Curs_PAR_TITE')
                select _Curs_PAR_TITE
                locate for 1=1
                do while not(eof())
                this.w_DISTINTA = _Curs_PAR_TITE.PTSERIAL
                this.w_NUMDIS = _Curs_PAR_TITE.CPROWNUM
                * --- Write into PAR_TITE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PTFLINDI ="+cp_NullLink(cp_ToStrODBC(Space(1)),'PAR_TITE','PTFLINDI');
                  +",PTSERRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PSERRIF),'PAR_TITE','PTSERRIF');
                  +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PORDRIF),'PAR_TITE','PTORDRIF');
                  +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(this.w_PNUMRIF),'PAR_TITE','PTNUMRIF');
                      +i_ccchkf ;
                  +" where ";
                      +"PTSERIAL = "+cp_ToStrODBC(this.w_DISTINTA);
                      +" and PTROWORD = "+cp_ToStrODBC(-2);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_NUMDIS);
                         )
                else
                  update (i_cTable) set;
                      PTFLINDI = Space(1);
                      ,PTSERRIF = this.w_PSERRIF;
                      ,PTORDRIF = this.w_PORDRIF;
                      ,PTNUMRIF = this.w_PNUMRIF;
                      &i_ccchkf. ;
                   where;
                      PTSERIAL = this.w_DISTINTA;
                      and PTROWORD = -2;
                      and CPROWNUM = this.w_NUMDIS;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                  select _Curs_PAR_TITE
                  continue
                enddo
                use
              endif
            endif
              select _Curs_gscg_bhe
              continue
            enddo
            use
          endif
          * --- Elimino le partite legate alla contabilizzazione che hanno PTFIRIND 
          *     valorizzato con il seriale della registrazione (il filtro su PTSERIAL � 
          *     ridondante lasciato per ottimizzare la ricerca tramite chiave primaria)
          * --- Delete from PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"PTSERIAL = "+cp_ToStrODBC(this.w_RIFDIS);
                  +" and PTROWORD = "+cp_ToStrODBC(-3);
                  +" and PTRIFIND = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
                   )
          else
            delete from (i_cTable) where;
                  PTSERIAL = this.w_RIFDIS;
                  and PTROWORD = -3;
                  and PTRIFIND = this.oParentObject.w_PNSERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- Controllo se la cancellazione deriva direttamente da Primanota.
          if TYPE("THIS.oparentobject.w_RIFGSCGACI.caption")<>"C"
            * --- Read from PAR_TITE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PTSERIAL"+;
                " from "+i_cTable+" PAR_TITE where ";
                    +"PTROWORD = "+cp_ToStrODBC(-3);
                    +" and PTSERIAL = "+cp_ToStrODBC(this.w_RIFDIS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PTSERIAL;
                from (i_cTable) where;
                    PTROWORD = -3;
                    and PTSERIAL = this.w_RIFDIS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SERIALE = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Se non ci sono pi� partite nella registrazione di Contabilizzazione Indiretta Effetti
            *     cancello anche la testata.
            if I_ROWS=0
              ah_Msg("Eliminata corrispondente registrazione deila indiretta effetti...",.T.)
              * --- Delete from CON_INDI
              i_nConn=i_TableProp[this.CON_INDI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CON_INDI_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"CISERIAL = "+cp_ToStrODBC(this.w_RIFDIS);
                       )
              else
                delete from (i_cTable) where;
                      CISERIAL = this.w_RIFDIS;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
            endif
          endif
        else
          * --- Rimette la Distinta da Contabilizzare e in Stato Provvisorio
          this.w_NUMERO = 0
          this.w_STATO = "N"
          * --- Read from PAR_TITE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PTSERIAL"+;
              " from "+i_cTable+" PAR_TITE where ";
                  +"PTSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNRIFDIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PTSERIAL;
              from (i_cTable) where;
                  PTSERIAL = this.oParentObject.w_PNRIFDIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NUMDIS = NVL(cp_ToDate(_read_.PTSERIAL),cp_NullValue(_read_.PTSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Select from GSCG_QDK
          do vq_exec with 'GSCG_QDK',this,'_Curs_GSCG_QDK','',.f.,.t.
          if used('_Curs_GSCG_QDK')
            select _Curs_GSCG_QDK
            locate for 1=1
            do while not(eof())
            this.w_NUMERO = this.w_NUMERO+1
              select _Curs_GSCG_QDK
              continue
            enddo
            use
          endif
          this.w_STATO = IIF(this.w_NUMERO>1,"S","N")
          * --- Write into DIS_TINT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DIS_TINT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DIS_TINT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DIRIFCON ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DIS_TINT','DIRIFCON');
            +",DIFLDEFI ="+cp_NullLink(cp_ToStrODBC(this.w_STATO),'DIS_TINT','DIFLDEFI');
                +i_ccchkf ;
            +" where ";
                +"DINUMDIS = "+cp_ToStrODBC(this.oParentObject.w_PNRIFDIS);
                   )
          else
            update (i_cTable) set;
                DIRIFCON = SPACE(10);
                ,DIFLDEFI = this.w_STATO;
                &i_ccchkf. ;
             where;
                DINUMDIS = this.oParentObject.w_PNRIFDIS;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Select from PAR_TITE
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" PAR_TITE ";
                +" where PTRIFIND="+cp_ToStrODBC(this.oParentObject.w_PNSERIAL)+" AND PTROWORD=-2 AND PTFLIMPE='CG' AND PTFLINDI='S'";
                 ,"_Curs_PAR_TITE")
          else
            select * from (i_cTable);
             where PTRIFIND=this.oParentObject.w_PNSERIAL AND PTROWORD=-2 AND PTFLIMPE="CG" AND PTFLINDI="S";
              into cursor _Curs_PAR_TITE
          endif
          if used('_Curs_PAR_TITE')
            select _Curs_PAR_TITE
            locate for 1=1
            do while not(eof())
            this.w_PTDATSCA = CP_TODATE(_Curs_PAR_TITE.PTDATSCA)
            this.w_PTNUMPAR = NVL(_Curs_PAR_TITE.PTNUMPAR, " ")
            this.w_PTTIPCON = NVL(_Curs_PAR_TITE.PTTIPCON, " ")
            this.w_PTCODCON = NVL(_Curs_PAR_TITE.PTCODCON, " ")
            this.w_PTCODVAL = NVL(_Curs_PAR_TITE.PTCODVAL, " ")
            this.w_PTSERRIF = NVL(_Curs_PAR_TITE.PTSERRIF, " ")
            this.w_PTORDRIF = NVL(_Curs_PAR_TITE.PTORDRIF,0)
            this.w_PTNUMRIF = NVL(_Curs_PAR_TITE.PTNUMRIF,0)
            if NOT EMPTY(this.w_PTCODCON)
              * --- Riapre eventuali partite da Contabilizzazione Indiretta associate alla Distinta
              if NOT EMPTY(this.w_PTSERRIF)
                * --- Write into PAR_TITE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PTFLIMPE ="+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                      +i_ccchkf ;
                  +" where ";
                      +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERRIF);
                      +" and PTROWORD = "+cp_ToStrODBC(this.w_PTORDRIF);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_PTNUMRIF);
                         )
                else
                  update (i_cTable) set;
                      PTFLIMPE = "  ";
                      &i_ccchkf. ;
                   where;
                      PTSERIAL = this.w_PTSERRIF;
                      and PTROWORD = this.w_PTORDRIF;
                      and CPROWNUM = this.w_PTNUMRIF;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              else
                * --- Write into PAR_TITE
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PAR_TITE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PTFLIMPE ="+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
                      +i_ccchkf ;
                  +" where ";
                      +"PTDATSCA = "+cp_ToStrODBC(this.w_PTDATSCA);
                      +" and PTNUMPAR = "+cp_ToStrODBC(this.w_PTNUMPAR);
                      +" and PTTIPCON = "+cp_ToStrODBC(this.w_PTTIPCON);
                      +" and PTCODCON = "+cp_ToStrODBC(this.w_PTCODCON);
                      +" and PTCODVAL = "+cp_ToStrODBC(this.w_PTCODVAL);
                      +" and PTROWORD = "+cp_ToStrODBC(-3);
                         )
                else
                  update (i_cTable) set;
                      PTFLIMPE = "  ";
                      &i_ccchkf. ;
                   where;
                      PTDATSCA = this.w_PTDATSCA;
                      and PTNUMPAR = this.w_PTNUMPAR;
                      and PTTIPCON = this.w_PTTIPCON;
                      and PTCODCON = this.w_PTCODCON;
                      and PTCODVAL = this.w_PTCODVAL;
                      and PTROWORD = -3;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
              select _Curs_PAR_TITE
              continue
            enddo
            use
          endif
          * --- Disimpegna le Partite
          * --- Write into PAR_TITE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PAR_TITE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PTFLIMPE ="+cp_NullLink(cp_ToStrODBC(SPACE(2)),'PAR_TITE','PTFLIMPE');
            +",PTRIFIND ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTRIFIND');
                +i_ccchkf ;
            +" where ";
                +"PTRIFIND = "+cp_ToStrODBC(this.oParentObject.w_PNSERIAL);
                +" and PTROWORD = "+cp_ToStrODBC(-2);
                   )
          else
            update (i_cTable) set;
                PTFLIMPE = SPACE(2);
                ,PTRIFIND = SPACE(10);
                &i_ccchkf. ;
             where;
                PTRIFIND = this.oParentObject.w_PNSERIAL;
                and PTROWORD = -2;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.oParentObject.w_PNRIFCES="C" AND "SOLL" $ UPPER(i_cModules)
        * --- Elimina il Riferimento alla Registrazione di Contenzioso
        do GSSO_BDC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case NVL(this.oParentObject.w_PNRIFCES, "N")="S" AND "CESP" $ UPPER(i_cModules)
        * --- Elimina il Riferimento al Cespite da PNT_CESP toglie la data contabilizzazione dal movimento cespite
        this.w_ConfRif = .F.
        do GSCE_BDC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case NOT EMPTY(this.oParentObject.w_PNRIFSAL)
        ah_Msg("Eliminata corrispondente registrazione da saldaconto...",.T.)
        * --- Delete from SALDDACO
        i_nConn=i_TableProp[this.SALDDACO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDDACO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"SCSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNRIFSAL);
                 )
        else
          delete from (i_cTable) where;
                SCSERIAL = this.oParentObject.w_PNRIFSAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from SALMDACO
        i_nConn=i_TableProp[this.SALMDACO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALMDACO_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"SCSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PNRIFSAL);
                 )
        else
          delete from (i_cTable) where;
                SCSERIAL = this.oParentObject.w_PNRIFSAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      case this.w_ElimRif AND "RITE" $ UPPER(i_cModules)
        * --- Elimina Riferimento al movimento ritenute Mrgengir
        GSRI_BER(this,this.oParentObject.w_PNSERIAL)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifiche di Quadratura DARE/AVERE e IVA
    this.w_TOTRIG = 0
    this.w_TROVCF = .F.
    this.w_TOTD = 0
    this.w_SEZDAR = 0
    this.w_TOTA = 0
    this.w_SEZAVE = 0
    this.w_RIGIVA = 0
    this.w_RIGIVACHK = 0
    this.w_OKPAR2 = .F.
    i = 1
    this.w_PADRE.MarkPos()     
    SELECT (this.w_PADRE.cTrsName)
    GO TOP
    SCAN FOR NOT DELETED()
    CONTROPARTITE(i) = t_PNCODCON
    i = i + 1
    Dimension CONTROPARTITE(i)
    * --- Controllo Esistenza Codice Conto
    if EMPTY(t_PNTIPCON) AND this.w_CFUNC<>"Load" AND i_SRV="U"
      this.w_oPart = this.w_oMess.AddMsgPartNL("Verificare riga: %1 tipo conto non definito")
      this.w_oPart.AddParam(ALLTRIM(STR(NVL(t_CPROWORD,0))))     
      this.w_OK = .F.
      EXIT
    endif
    if t_PNIMPDAR=0 AND t_PNIMPAVE=0 AND t_PNFLZERO<>"S" AND this.w_CFUNC<>"Load" AND i_SRV="U"
      this.w_oPart = this.w_oMess.AddMsgPartNL("Verificare riga: %1 importi di riga non definiti")
      this.w_oPart.AddParam(ALLTRIM(STR(NVL(t_CPROWORD,0))))     
      this.w_OK = .F.
      EXIT
    endif
    if NOT EMPTY(t_PNTIPCON) AND (t_PNIMPDAR<>0 OR t_PNIMPAVE<>0 OR t_PNFLZERO="S")
      * --- Altre Righe Valide
      if EMPTY(NVL(t_PNCODCON," ")) 
        this.w_oPart = this.w_oMess.AddMsgPartNL("Verificare riga: %1 codice conto non definito")
        this.w_oPart.AddParam(ALLTRIM(STR(NVL(t_CPROWORD,0))))     
        this.w_OK = .F.
        EXIT
      endif
      * --- Controllo Esistenza Causale Di Riga
      if EMPTY(NVL(t_PNCAURIG," "))
        this.w_oPart = this.w_oMess.AddMsgPartNL("Verificare riga: %1 causale di riga inesistente")
        this.w_oPart.AddParam(ALLTRIM(STR(NVL(t_CPROWORD,0))))     
        this.w_OK = .F.
        EXIT
      endif
      * --- Controllo Congruita' Flag Aggiornamento Saldi
      if (this.oParentObject.w_PNFLPROV<>"S" AND t_PNFLSALD=" ") OR (this.oParentObject.w_PNFLPROV="S" AND t_PNFLSALD="+") 
        this.w_oPart = this.w_oMess.AddMsgPartNL("Verificare riga: %1 flag aggiornamento saldi incongruente")
        this.w_oPart.AddParam(ALLTRIM(STR(NVL(t_CPROWORD,0))))     
        this.w_OK = .F.
        EXIT
      endif
      * --- Controllo Congruita' Flag Aggiornamento Saldi Chiusura / Apertura
      if (this.oParentObject.w_PNFLPROV<>"S" AND this.oParentObject.w_FLSALI="S" AND t_SEZB<>"T" AND t_PNFLSALI=" ") OR ((this.oParentObject.w_PNFLPROV="S" OR this.oParentObject.w_FLSALI<>"S" OR t_SEZB="T") AND t_PNFLSALI="+") 
        this.w_oPart = this.w_oMess.AddMsgPartNL("Verificare riga: %1 flag aggiornamento saldo iniziale incongruente")
        this.w_oPart.AddParam(ALLTRIM(STR(NVL(t_CPROWORD,0))))     
        this.w_OK = .F.
        EXIT
      endif
      if (this.oParentObject.w_PNFLPROV<>"S" AND this.oParentObject.w_FLSALF="S" AND t_SEZB<>"T" AND t_PNFLSALF=" ") OR ((this.oParentObject.w_PNFLPROV="S" OR this.oParentObject.w_FLSALF<>"S" OR t_SEZB="T") AND t_PNFLSALF="+") 
        this.w_oPart = this.w_oMess.AddMsgPartNL("Verificare riga: %1 flag aggiornamento saldo finale incongruente")
        this.w_oPart.AddParam(ALLTRIM(STR(NVL(t_CPROWORD,0))))     
        this.w_OK = .F.
        EXIT
      endif
      * --- Per Eventuali Controlli sulle Partite di Acconto
      this.w_OKPART = IIF(g_PERPAR="S" AND this.w_CFUNC="Load" AND this.oParentObject.w_FLPART="C" AND t_PNFLPART="C", .T., this.w_OKPART)
      this.w_TOTRIG = this.w_TOTRIG + 1
      * --- Totali DARE e AVERE
      this.w_DARE = cp_ROUND(NVL(t_PNIMPDAR, 0), this.oParentObject.w_DECTOP)
      this.w_AVER = cp_ROUND(NVL(t_PNIMPAVE, 0), this.oParentObject.w_DECTOP)
      this.w_TOTD = this.w_TOTD + this.w_DARE
      this.w_TOTA = this.w_TOTA + this.w_AVER
      if t_PNTIPCON=this.oParentObject.w_PNTIPCLF AND t_PNCODCON=this.oParentObject.w_PNCODCLF AND t_PNCAURIG=this.oParentObject.w_PNCODCAU 
        * --- Riga Cli/For di Riferimento, Totali DARE e AVERE relativi al Cli/For
        this.w_SEZDAR = this.w_SEZDAR + this.w_DARE
        this.w_SEZAVE = this.w_SEZAVE + this.w_AVER
        this.w_TROVCF = .T.
        * --- Cliente con partite
        this.w_OKPAR2 = IIF(g_PERPAR="S" AND this.oParentObject.w_FLPART $ "CSA" AND t_PNFLPART $ "CSA", .T., this.w_OKPAR2)
      endif
      do case
        case t_TIPSOT="I" AND ((this.oParentObject.w_CFDAVE="D" AND this.w_DARE=0) OR (this.oParentObject.w_CFDAVE="A" AND this.w_AVER=0))
          * --- Riga Conto IVA
          this.w_RIGIVA = this.w_RIGIVA + (this.w_DARE-this.w_AVER)
          this.w_RIGIVACHK = this.w_RIGIVACHK + IIF(this.w_DARE <>0,this.w_DARE,this.w_AVER)
        case t_TIPSOT="V" OR t_TIPSOT="A"
          * --- Per controllo esistenza almeno una riga contropartita
          this.w_CONTROPA = .T.
        case t_TIPSOT="M"
          * --- Riga Conto Cespite: serve per i controlli successivi
          this.w_CESPITE = .T.
        case t_TIPSOT="A"
          * --- Per controllo contropartita di riga
          this.w_CONTROACQ = .T.
      endcase
      if i_SRV="U" AND NVL(t_PNIMPDAR=0,0) AND NVL(t_PNIMPAVE=0,0) AND NVL(t_PNFLZERO," ")<>"S"
        if NOT this.w_CFUNC $ "Load-Query"
          this.w_oMess.AddMsgPartNL("Impossibile azzerare importi di una riga in variazione. Occorre prima eliminarla")     
          this.w_OK = .F.
          EXIT
        endif
      endif
    endif
    ENDSCAN
    this.w_PADRE.RePos(.T.)     
    this.w_RIGIVA = ABS(this.w_RIGIVA)
    if this.w_OK AND this.w_TOTRIG=0
      this.w_oMess.AddMsgPartNL("Registrazione senza righe di dettaglio")     
      this.w_OK = .F.
    endif
    if Not this.w_CONTROPA and this.oParentObject.w_PNTIPREG = "E" AND this.oParentObject.w_PNTIPDOC = "CO" and this.oParentObject.w_FLPART <>"S" AND this.w_OK
      this.w_oMess.AddMsgPartNL("Attenzione, deve esistere almeno una riga di contropartita vendita")     
      this.w_OK = .F.
    endif
    * --- Controlli Differenza DARE/AVERE
    if this.w_OK AND this.w_TOTD<>this.w_TOTA
      * --- Salta se in Cancellazione
      if g_QUADRA="S"
        this.w_oPart = this.w_oMess.AddMsgPartNL("Manca quadratura dare/avere (%1)")
        this.w_oPart.AddParam(ALLTRIM(TRAN(this.w_TOTD-this.w_TOTA,v_PV[38+VVP])))     
        this.w_OK = .F.
      else
        this.w_OK = ah_YesNo("Manca quadratura dare/avere (%1). Confermi ugualmente?",,ALLTRIM(TRAN(this.w_TOTD-this.w_TOTA,v_PV[38+VVP])))
        if ! this.w_OK
          this.w_oMess.AddMsgPartNL("Operazione abbandonata")     
        endif
      endif
    endif
    * --- Controlli Differenza IVA
    if this.w_OK AND this.oParentObject.w_PNTIPDOC<>"FC" AND Not(this.oParentObject.w_PNTIPREG $ "E-N") AND this.oParentObject.w_FLPDIF<>"S" AND NOT EMPTY(this.oParentObject.w_PNTIPCLF) AND this.oParentObject.w_PNTIPREG<>"N"
      * --- Cicla sulle Righe IVA
      this.w_TOTIVA = 0
      this.w_IVADET = 0
      this.w_TMP_IMPO = 0
      * --- Variabile utilizzata per verificare la presenza di almeno una riga con registro IVA
      *     coerente alla cuasale contabile
      this.w_OKREG = .F.
      this.w_ESI_DET_IVA = .F.
      this.oParentObject.GSCG_MIV.MarkPos()
      SELECT (this.oParentObject.GSCG_MIV.cTrsName)
      GO TOP
      SCAN FOR NOT EMPTY(NVL(t_IVCODIVA," ")) 
      * --- Se Reg.IVA Castelletto 0 Reg.IVA Registrazione
      this.w_ESI_DET_IVA = .T.
      if (t_IVIMPONI<>0 OR t_IVIMPIVA<>0)
        if t_IVTIPREG=3
          this.w_TMP_IMPO = (nvl(t_IVIMPONI,0)+nvl(t_IVIMPIVA,0))/(1+(t_PERIVA/100))
          if this.oParentObject.w_PNCODVAL=g_CODLIR OR this.oParentObject.w_PNTIPDOC $ "FE-NE" OR (this.oParentObject.w_PNTIPDOC $ "AU-NU" AND Nvl(t_REVCHA," "))
            * --- Se Lire Arrotonda l' IVA all'importo Superiore
            this.w_TMP_IMPO = IVAROUND(this.w_TMP_IMPO, this.oParentObject.w_DECTOT, IIF(this.w_TMP_IMPO<0, 0, 1),this.oParentObject.w_PNVALNAZ)
          else
            this.w_TMP_IMPO = cp_ROUND(this.w_TMP_IMPO, this.oParentObject.w_DECTOT)
          endif
          * --- Determina l'Iva per differenza
          this.w_IMPIVA = (nvl(t_IVIMPONI,0)+nvl(t_IVIMPIVA,0)) - this.w_TMP_IMPO
          this.w_IMPONI = this.w_TMP_IMPO
        else
          this.w_TMP_IMPO = (t_IVIMPONI*t_PERIVA)/100
          if this.oParentObject.w_PNCODVAL=g_CODLIR OR this.oParentObject.w_PNTIPDOC $ "FE-NE" OR (this.oParentObject.w_PNTIPDOC $ "AU-NU" AND Nvl(t_REVCHA," ")="S")
            * --- Se Lire Arrotonda l' IVA all'importo Superiore
            this.w_IMPIVA = IVAROUND(this.w_TMP_IMPO, this.oParentObject.w_DECTOT, IIF(this.w_TMP_IMPO<0, 0, 1),this.oParentObject.w_PNVALNAZ)
          else
            this.w_IMPIVA = cp_ROUND(this.w_TMP_IMPO,this.oParentObject.w_DECTOT)
          endif
        endif
        do case
          case Nvl(t_IVPERIND,0)<>0 and t_IVTIPREG=1
            this.w_oPart = this.w_oMess.AddMsgPartNL("Riga castelletto: %1 codice IVA [%2] di tipo indetraibile")
            this.w_oPart.AddParam(Alltrim(STR(CPROWNUM,6,0)))     
            this.w_oPart.AddParam(t_IVCODIVA)     
            this.w_OK = .F.
            Exit
          case Nvl(t_IVIMPONI,0)=0 and Nvl(t_IVIMPIVA,0)<>0
            this.w_oPart = this.w_oMess.AddMsgPartNL("Riga castelletto: %1 imponibile a zero con imposta diversa da zero")
            this.w_oPart.AddParam(Alltrim(STR(CPROWNUM,6,0)))     
            this.w_OK = .F.
            Exit
        endcase
        if Nvl(t_IVIMPIVA,0)<>this.w_IMPIVA AND this.w_OK AND Nvl(t_IVFLOMAG,0) <>4 and this.oParentObject.w_PNVALNAZ=this.oParentObject.w_PNCODVAL
          if t_IVTIPREG=3
            this.w_oPart = this.w_oMess.AddMsgPartNL("Riga castelletto: %1 imposta incoerente con imponibile scorporato legato al codice IVA utilizzato")
            this.w_oPart.AddParam(Alltrim(STR(CPROWNUM,6,0)))     
          else
            this.w_oPart = this.w_oMess.AddMsgPartNL("Riga castelletto: %1 imposta [%2] incoerente con importo [%3] legato al codice IVA utilizzato")
            this.w_oPart.AddParam(Alltrim(STR(CPROWNUM,6,0)))     
            this.w_oPart.AddParam(Alltrim(str(t_IVIMPIVA,20,4)))     
            this.w_oPart.AddParam(Alltrim(str(this.w_IMPIVA,20,4)))     
          endif
          this.w_oMess.AddMsgPartNL("Confermi ugualmente?")     
          this.w_OK = this.w_oMess.ah_YesNo()
          if Not this.w_OK
            Exit
          endif
        endif
        this.w_IVA = cp_ROUND(NVL(t_IVIMPIVA, 0), this.oParentObject.w_DECTOP)
        this.w_IMP = cp_ROUND(NVL(t_IVIMPONI, 0), this.oParentObject.w_DECTOP)
        if t_CTIPREG=this.oParentObject.w_PNTIPREG
          this.w_TOTIVA = this.w_TOTIVA + IIF(this.oParentObject.w_CALDOC $ "IZ" OR t_IVCFLOMA $ "IE", 0, this.w_IMP)
          * --- Se Codice Iva di Tipo Reverse Charge non considero l'iva nel controllo
          this.w_TOTIVA = this.w_TOTIVA + IIF(this.oParentObject.w_CALDOC $ "MZ" OR t_IVCFLOMA="E" or (this.oParentObject.w_PNTIPDOC $ "AU-NU" and Nvl(t_REVCHA," ")="S"), 0, this.w_IVA)
        endif
        * --- Eseguo controllo IVA indetraibile
        * --- Se no Riga Omaggio
        this.w_APPO = cp_ROUND((this.w_IVA*t_IVPERIND)/100, this.oParentObject.w_DECTOP)
        if t_CTIPREG=this.oParentObject.w_PNTIPREG
          * --- Solo righe coerenti tipo regostro iva della causale
          this.w_IVADET = this.w_IVADET + (this.w_IVA - this.w_APPO)
        endif
      endif
      if ICASE(t_IVTIPREG=1,"V",t_IVTIPREG=2,"A",t_IVTIPREG=3,"C")=this.oParentObject.w_PNTIPREG
        this.w_OKREG = .T.
      endif
      ENDSCAN
      this.oParentObject.GSCG_MIV.RePos(.t.)
      if this.w_OK and Not this.w_OKREG and Not(this.oParentObject.w_PNTIPREG $ "N-E") AND Nvl(t_IVFLOMAG,0) <>4 AND Not (this.oParentObject.w_PNTIPREG="C" AND Not (this.w_ESI_DET_IVA))
        this.w_oMess.AddMsgPartNL("Attenzione: deve esistere almeno una riga con il tipo registro IVA coerente alla causale contabile")     
        this.w_OK = .F.
      endif
      if Not this.w_TROVCF and this.w_OK and NOT EMPTY(this.oParentObject.w_PNCODCLF) and not this.oParentObject.w_PNTIPREG $ "CE"
        this.w_oMess.AddMsgPartNL("Cliente/fornitore diverso dalla sezione IVA")     
        this.w_OK = .F.
      endif
      * --- Differenza IVA
      this.w_DIFIVA = ABS(IIF(this.oParentObject.w_CFDAVE="D", this.w_SEZDAR, this.w_SEZAVE)) - ABS(this.w_TOTIVA)
      this.w_DIFDOC = 0
      if this.oParentObject.w_PNTIPCLF $ "CF" and NOT EMPTY(this.oParentObject.w_PNCODCLF)
        * --- Esegue il Controllo Cli/For con il Totale Documento in Valuta
        this.w_DATRIF = IIF(EMPTY(this.oParentObject.w_PNDATDOC), this.oParentObject.w_PNDATREG, this.oParentObject.w_PNDATDOC)
        if this.oParentObject.w_PNVALNAZ<>this.oParentObject.w_PNCODVAL
          this.w_TOTDOC = cp_ROUND(VAL2MON(this.oParentObject.w_PNTOTDOC, this.oParentObject.w_PNCAOVAL, this.oParentObject.w_CAONAZ, this.w_DATRIF, this.oParentObject.w_PNVALNAZ), this.oParentObject.w_DECTOP)
          this.w_DIFDOC = ABS(IIF(this.oParentObject.w_CFDAVE="D", this.w_SEZDAR, this.w_SEZAVE)) - ABS(this.w_TOTDOC)
        else
          * --- Eseguo controllo Solo se il totale documento � imputato
          if this.oParentObject.w_PNTOTDOC<>0
            this.w_TOTDOC = cp_ROUND(this.oParentObject.w_PNTOTDOC, this.oParentObject.w_DECTOP)
            this.w_DIFDOC = ABS(IIF(this.oParentObject.w_CFDAVE="D", this.w_SEZDAR, this.w_SEZAVE)) - ABS(this.w_TOTDOC)
          endif
        endif
      endif
      do case
        case this.w_OK AND this.w_DIFIVA<>0 and NOT EMPTY(this.oParentObject.w_PNCODCLF) and not this.oParentObject.w_PNTIPREG $ "CE"
          * --- Quadratura IVA
          this.w_oPart = this.w_oMess.AddMsgPartNL("Importo su cliente/fornitore incongruente con sezione IVA. (Differenza IVA = %1)")
          this.w_oPart.AddParam(ALLTRIM(TRAN(this.w_DIFIVA,v_PV[38+VVP])))     
          this.w_OK = .F.
        case this.w_OK AND this.oParentObject.w_CALDOC<>"N" AND (this.w_SEZAVE<>0 OR this.w_SEZDAR<>0) AND ((this.oParentObject.w_CFDAVE="D" AND this.w_SEZDAR=0) OR (this.oParentObject.w_CFDAVE="A" AND this.w_SEZAVE=0)) and NOT EMPTY(this.oParentObject.w_PNCODCLF)
          * --- Documento sulla Sezione DARE/AVERE sbagliata
          this.w_oMess.AddMsgPartNL("Importo documento su sezione dare-avere errata")     
          this.w_OK = .F.
        case this.w_OK and this.w_IVADET<>this.w_RIGIVACHK
          * --- Importo IVA Detraibile non congruente con il Totale righe Conti IVA
          this.w_oMess.AddMsgPartNL("Importi righe IVA incongruenti con il totale IVA deducibile:")     
          this.w_oPart = this.w_oMess.AddMsgPartNL("(Tot. IVA deducibile = %1)")
          this.w_oPart.AddParam(ALLTRIM(TRAN(this.w_IVADET,v_PV[38+VVP])))     
          this.w_oPart = this.w_oMess.AddMsgPartNL("(Tot. righe conti IVA= %1)")
          this.w_oPart.AddParam(ALLTRIM(TRAN(this.w_RIGIVACHK,v_PV[38+VVP])))     
          this.w_OK = .F.
        case this.w_OK AND this.w_DIFDOC<>0 AND (this.oParentObject.w_CALDOC<>"I" OR this.w_OKPAR2) and NOT EMPTY(this.oParentObject.w_PNCODCLF)
          if this.oParentObject.w_PNVALNAZ<>this.oParentObject.w_PNCODVAL
            * --- Congruita' Primanota/Documenti (in Valuta)
            this.w_oMess.AddMsgPartNL("Importo su cliente/fornitore incongruente con totale documento in valuta")     
          else
            * --- Congruita' Primanota/Documenti 
            this.w_oMess.AddMsgPartNL("Importo su cliente/fornitore incongruente con totale documento")     
          endif
          this.w_oPart = this.w_oMess.AddMsgPartNL("(Differenza = %1)")
          this.w_oPart.AddParam(ALLTRIM(TRAN(this.w_DIFDOC,v_PV[38+VVP])))     
          this.w_OK = .F.
      endcase
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizione Variabili
    * --- Per controllo partite in distinta
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se il transitorio � vuoto mi baso sul database
    if this.w_CFUNC<>"Query"
      * --- Select from GSCG6BCK
      do vq_exec with 'GSCG6BCK',this,'_Curs_GSCG6BCK','',.f.,.t.
      if used('_Curs_GSCG6BCK')
        select _Curs_GSCG6BCK
        locate for 1=1
        do while not(eof())
        this.w_ACMOVCES = NVL(_Curs_GSCG6BCK.ACMOVCES,SPACE(10))
        this.w_MCNUMREG = NVL(_Curs_GSCG6BCK.MCNUMREG,0)
        this.w_MCCODESE = NVL(_Curs_GSCG6BCK.MCCODESE,SPACE(4))
        this.w_MCDATREG = NVL(_Curs_GSCG6BCK.MCDATREG,cp_CharToDate("  -  -    "))
        this.w_MESS = CHKCESP(this.w_ACMOVCES, this.oParentObject.w_PNNUMDOC,this.oParentObject.w_PNDATDOC, this.oParentObject.w_PNALFDOC,this.oParentObject.w_PNCODCLF,this.oParentObject.w_PNTIPCLF, 0)
        if NOT EMPTY(this.w_MESS) AND this.oParentObject.w_FLRIFE<>"N"
          if ah_YesNo("%1 del movimento cespite associato n� %2/%3 del %4 non congruente%0confermi l'elaborazione?",,this.w_MESS,ALLTRIM(STR(this.w_MCNUMREG)),ALLTRIM(this.w_MCCODESE),DTOC(this.w_MCDATREG))
          else
            this.w_oMess.AddMsgPartNL("Elaborazione interrotta")     
            this.w_OK = .F.
          endif
          this.w_PRIMA = .T.
        endif
        this.w_PRESENTE = .T.
          select _Curs_GSCG6BCK
          continue
        enddo
        use
      endif
    else
      * --- Utilizzo il transitorio perch� i record non sono pi� presenti i record sul database
      if type("THIS.OPARENTOBJECT.GSCG_MAC.CNT")="O" 
        SELECT * FROM (THIS.OPARENTOBJECT.GSCG_MAC.CNT.CTRSNAME) INTO CURSOR CESP1 WHERE NOT EMPTY(NVL(t_ACMOVCES,SPACE(10))) AND NVL(t_ACTIPASS," ")="M"
        if RECCOUNT("CESP1")>0
          this.w_oMess.AddMsgPart( "Esistono movimenti cespiti abbinati al documento")     
          this.w_oMess.AddMsgPartNL("Impossibile cancellare")     
          this.w_OK = .F.
          this.w_PRESENTE = .T.
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,18)]
    this.cWorkTables[1]='ATTIDETT'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='CON_INDI'
    this.cWorkTables[4]='DIS_TINT'
    this.cWorkTables[5]='DOC_MAST'
    this.cWorkTables[6]='INC_CORR'
    this.cWorkTables[7]='PAR_TITE'
    this.cWorkTables[8]='PNT_MAST'
    this.cWorkTables[9]='PRI_MAST'
    this.cWorkTables[10]='VALUTE'
    this.cWorkTables[11]='ESI_DETT'
    this.cWorkTables[12]='CONTI'
    this.cWorkTables[13]='SALMDACO'
    this.cWorkTables[14]='SALDDACO'
    this.cWorkTables[15]='VOCIIVA'
    this.cWorkTables[16]='DATIRITE'
    this.cWorkTables[17]='INCDCORR'
    this.cWorkTables[18]='PRE_STAZ'
    return(this.OpenAllTables(18))

  proc CloseCursors()
    if used('_Curs_PNT_MAST')
      use in _Curs_PNT_MAST
    endif
    if used('_Curs_GSCG3QUA')
      use in _Curs_GSCG3QUA
    endif
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_GSRI_BCK')
      use in _Curs_GSRI_BCK
    endif
    if used('_Curs_gscg_bhe')
      use in _Curs_gscg_bhe
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_GSCG_QDK')
      use in _Curs_GSCG_QDK
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    if used('_Curs_DATIRITE')
      use in _Curs_DATIRITE
    endif
    if used('_Curs_GSCG_QRC')
      use in _Curs_GSCG_QRC
    endif
    if used('_Curs_GSCG6BCK')
      use in _Curs_GSCG6BCK
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
