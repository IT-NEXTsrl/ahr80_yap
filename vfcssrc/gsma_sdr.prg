* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_sdr                                                        *
*              Confronto dati                                                  *
*                                                                              *
*      Author: Zucchetti Tam S.p.A                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_140]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-04-04                                                      *
* Last revis.: 2016-05-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_sdr",oParentObject))

* --- Class definition
define class tgsma_sdr as StdForm
  Top    = -1
  Left   = 24

  * --- Standard Properties
  Width  = 782
  Height = 430+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-03"
  HelpContextID=74087273
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=48

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  MAGAZZIN_IDX = 0
  RILEVAZI_IDX = 0
  CODIRILE_IDX = 0
  KEY_ARTI_IDX = 0
  AZIENDA_IDX = 0
  ESERCIZI_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MARCHI_IDX = 0
  CMT_MAST_IDX = 0
  CONTI_IDX = 0
  ADDERILE_IDX = 0
  CAN_TIER_IDX = 0
  cPrg = "gsma_sdr"
  cComment = "Confronto dati"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_CODMAG = space(5)
  w_CODRIL = space(10)
  w_CODART = space(20)
  w_CODART1 = space(20)
  w_DESART = space(40)
  w_DESMAG = space(30)
  w_SELEZI = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_UBICAZ = space(1)
  w_DATARIL = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_CODESE = space(5)
  w_DESCRI = space(40)
  w_TIPO1 = space(2)
  w_DTOBS2 = ctod('  /  /  ')
  w_TIPO = space(2)
  w_KEYSAL = space(40)
  w_KEYULO = space(40)
  w_CAUCAR = space(5)
  w_CAUSCA = space(10)
  w_CODMAT = space(40)
  w_ESVALNAZ = space(3)
  w_DTOBS1 = ctod('  /  /  ')
  w_DESART1 = space(40)
  w_CODFAM = space(5)
  w_DESFAM = space(35)
  w_CODGRU = space(5)
  w_DESGRU = space(35)
  w_CODCAT = space(5)
  w_DESCAT = space(35)
  w_CODMAR = space(5)
  w_DESMAR = space(35)
  w_CLAMAT = space(5)
  w_CODFOR = space(15)
  w_DESFOR = space(40)
  w_DESCLA = space(35)
  w_OBTESTFOR = ctod('  /  /  ')
  w_DATOBSOFOR = ctod('  /  /  ')
  w_TIPMAT = space(1)
  w_ORIG = space(1)
  w_ANTIPCON = space(10)
  w_MOVINI = ctod('  /  /  ')
  w_FLNONRIL = space(1)
  o_FLNONRIL = space(1)
  w_CODUTE = space(5)
  w_DESUTE = space(40)
  w_CODCOM = space(15)
  w_DESCOM = space(40)
  w_ZoomConf = .NULL.
  w_NORILBCK = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_sdrPag1","gsma_sdr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principale")
      .Pages(2).addobject("oPag","tgsma_sdrPag2","gsma_sdr",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODMAG_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomConf = this.oPgFrm.Pages(1).oPag.ZoomConf
    this.w_NORILBCK = this.oPgFrm.Pages(1).oPag.NORILBCK
    DoDefault()
    proc Destroy()
      this.w_ZoomConf = .NULL.
      this.w_NORILBCK = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[15]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='RILEVAZI'
    this.cWorkTables[4]='CODIRILE'
    this.cWorkTables[5]='KEY_ARTI'
    this.cWorkTables[6]='AZIENDA'
    this.cWorkTables[7]='ESERCIZI'
    this.cWorkTables[8]='FAM_ARTI'
    this.cWorkTables[9]='GRUMERC'
    this.cWorkTables[10]='CATEGOMO'
    this.cWorkTables[11]='MARCHI'
    this.cWorkTables[12]='CMT_MAST'
    this.cWorkTables[13]='CONTI'
    this.cWorkTables[14]='ADDERILE'
    this.cWorkTables[15]='CAN_TIER'
    return(this.OpenAllTables(15))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_CODMAG=space(5)
      .w_CODRIL=space(10)
      .w_CODART=space(20)
      .w_CODART1=space(20)
      .w_DESART=space(40)
      .w_DESMAG=space(30)
      .w_SELEZI=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_UBICAZ=space(1)
      .w_DATARIL=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_CODESE=space(5)
      .w_DESCRI=space(40)
      .w_TIPO1=space(2)
      .w_DTOBS2=ctod("  /  /  ")
      .w_TIPO=space(2)
      .w_KEYSAL=space(40)
      .w_KEYULO=space(40)
      .w_CAUCAR=space(5)
      .w_CAUSCA=space(10)
      .w_CODMAT=space(40)
      .w_ESVALNAZ=space(3)
      .w_DTOBS1=ctod("  /  /  ")
      .w_DESART1=space(40)
      .w_CODFAM=space(5)
      .w_DESFAM=space(35)
      .w_CODGRU=space(5)
      .w_DESGRU=space(35)
      .w_CODCAT=space(5)
      .w_DESCAT=space(35)
      .w_CODMAR=space(5)
      .w_DESMAR=space(35)
      .w_CLAMAT=space(5)
      .w_CODFOR=space(15)
      .w_DESFOR=space(40)
      .w_DESCLA=space(35)
      .w_OBTESTFOR=ctod("  /  /  ")
      .w_DATOBSOFOR=ctod("  /  /  ")
      .w_TIPMAT=space(1)
      .w_ORIG=space(1)
      .w_ANTIPCON=space(10)
      .w_MOVINI=ctod("  /  /  ")
      .w_FLNONRIL=space(1)
      .w_CODUTE=space(5)
      .w_DESUTE=space(40)
      .w_CODCOM=space(15)
      .w_DESCOM=space(40)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODMAG))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODRIL))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CODART))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODART1))
          .link_1_5('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomConf.Calculate()
          .DoRTCalc(6,7,.f.)
        .w_SELEZI = 'D'
          .DoRTCalc(9,11,.f.)
        .w_OBTEST = .w_DATARIL
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CODESE))
          .link_1_19('Full')
        endif
        .w_DESCRI = .w_ZoomConf.GETVAR('DESART')
          .DoRTCalc(15,17,.f.)
        .w_KEYSAL = .w_ZOOMConf.GETVAR('DRKEYSAL')
        .w_KEYULO = .w_ZOOMConf.GETVAR('DRKEYULO')
      .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
          .DoRTCalc(20,21,.f.)
        .w_CODMAT = .w_ZOOMConf.GETVAR('DRCODMAT')
      .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .DoRTCalc(23,26,.f.)
        if not(empty(.w_CODFAM))
          .link_2_1('Full')
        endif
        .DoRTCalc(27,28,.f.)
        if not(empty(.w_CODGRU))
          .link_2_3('Full')
        endif
        .DoRTCalc(29,30,.f.)
        if not(empty(.w_CODCAT))
          .link_2_5('Full')
        endif
        .DoRTCalc(31,32,.f.)
        if not(empty(.w_CODMAR))
          .link_2_7('Full')
        endif
        .DoRTCalc(33,34,.f.)
        if not(empty(.w_CLAMAT))
          .link_2_13('Full')
        endif
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_CODFOR))
          .link_2_14('Full')
        endif
          .DoRTCalc(36,37,.f.)
        .w_OBTESTFOR = i_DATSYS
          .DoRTCalc(39,40,.f.)
        .w_ORIG = 'A'
        .w_ANTIPCON = 'F'
          .DoRTCalc(43,43,.f.)
        .w_FLNONRIL = 'N'
        .w_CODUTE = IIF(.w_FLNONRIL<>'S', SPACE(5), .w_CODUTE)
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_CODUTE))
          .link_1_48('Full')
        endif
      .oPgFrm.Page1.oPag.NORILBCK.Calculate('Articoli non rilevati', 0 , rgb(230,230,230))
        .DoRTCalc(46,47,.f.)
        if not(empty(.w_CODCOM))
          .link_2_25('Full')
        endif
    endwith
    this.DoRTCalc(48,48,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomConf.Calculate()
        .DoRTCalc(1,11,.t.)
            .w_OBTEST = .w_DATARIL
          .link_1_19('Full')
            .w_DESCRI = .w_ZoomConf.GETVAR('DESART')
        .DoRTCalc(15,17,.t.)
            .w_KEYSAL = .w_ZOOMConf.GETVAR('DRKEYSAL')
            .w_KEYULO = .w_ZOOMConf.GETVAR('DRKEYULO')
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .DoRTCalc(20,21,.t.)
            .w_CODMAT = .w_ZOOMConf.GETVAR('DRCODMAT')
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .DoRTCalc(23,37,.t.)
            .w_OBTESTFOR = i_DATSYS
        .DoRTCalc(39,44,.t.)
        if .o_FLNONRIL<>.w_FLNONRIL
            .w_CODUTE = IIF(.w_FLNONRIL<>'S', SPACE(5), .w_CODUTE)
          .link_1_48('Full')
        endif
        .oPgFrm.Page1.oPag.NORILBCK.Calculate('Articoli non rilevati', 0 , rgb(230,230,230))
        if .o_FLNONRIL<>.w_FLNONRIL
          .Calculate_SYYGHVHFXO()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(46,48,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomConf.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_38.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_39.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate()
        .oPgFrm.Page1.oPag.NORILBCK.Calculate('Articoli non rilevati', 0 , rgb(230,230,230))
    endwith
  return

  proc Calculate_SYYGHVHFXO()
    with this
          * --- Visualizza legenda di sfondo
          .w_NORILBCK.Visible = .w_FLNONRIL='S'
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODUTE_1_48.enabled = this.oPgFrm.Page1.oPag.oCODUTE_1_48.mCond()
    this.oPgFrm.Page2.oPag.oCODCOM_2_25.enabled = this.oPgFrm.Page2.oPag.oCODCOM_2_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oCLAMAT_2_13.visible=!this.oPgFrm.Page2.oPag.oCLAMAT_2_13.mHide()
    this.oPgFrm.Page2.oPag.oDESCLA_2_17.visible=!this.oPgFrm.Page2.oPag.oDESCLA_2_17.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_18.visible=!this.oPgFrm.Page2.oPag.oStr_2_18.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomConf.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_38.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
      .oPgFrm.Page1.oPag.NORILBCK.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_SYYGHVHFXO()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODMAG
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_2'),i_cWhere,'',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGFLUBIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
      this.w_UBICAZ = NVL(_Link_.MGFLUBIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_UBICAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODMAG = space(5)
        this.w_DESMAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_UBICAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODRIL
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CODIRILE_IDX,3]
    i_lTable = "CODIRILE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2], .t., this.CODIRILE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODRIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CODIRILE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RICODICE like "+cp_ToStrODBC(trim(this.w_CODRIL)+"%");

          i_ret=cp_SQL(i_nConn,"select RICODICE,RIDATRIL,RICODESE,RICAUCAR,RICAUSCA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RICODICE',trim(this.w_CODRIL))
          select RICODICE,RIDATRIL,RICODESE,RICAUCAR,RICAUSCA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODRIL)==trim(_Link_.RICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODRIL) and !this.bDontReportError
            deferred_cp_zoom('CODIRILE','*','RICODICE',cp_AbsName(oSource.parent,'oCODRIL_1_3'),i_cWhere,'',"Codici rilevazione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDATRIL,RICODESE,RICAUCAR,RICAUSCA";
                     +" from "+i_cTable+" "+i_lTable+" where RICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODICE',oSource.xKey(1))
            select RICODICE,RIDATRIL,RICODESE,RICAUCAR,RICAUSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODRIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDATRIL,RICODESE,RICAUCAR,RICAUSCA";
                   +" from "+i_cTable+" "+i_lTable+" where RICODICE="+cp_ToStrODBC(this.w_CODRIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODICE',this.w_CODRIL)
            select RICODICE,RIDATRIL,RICODESE,RICAUCAR,RICAUSCA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODRIL = NVL(_Link_.RICODICE,space(10))
      this.w_DATARIL = NVL(cp_ToDate(_Link_.RIDATRIL),ctod("  /  /  "))
      this.w_CODESE = NVL(_Link_.RICODESE,space(5))
      this.w_CAUCAR = NVL(_Link_.RICAUCAR,space(5))
      this.w_CAUSCA = NVL(_Link_.RICAUSCA,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CODRIL = space(10)
      endif
      this.w_DATARIL = ctod("  /  /  ")
      this.w_CODESE = space(5)
      this.w_CAUCAR = space(5)
      this.w_CAUSCA = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2])+'\'+cp_ToStr(_Link_.RICODICE,1)
      cp_ShowWarn(i_cKey,this.CODIRILE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODRIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_4'),i_cWhere,'',"Articoli",'GSMA0ADI.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_DTOBS1 = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_TIPO = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_DTOBS1 = ctod("  /  /  ")
      this.w_TIPO = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPO$'PF-SE-MP-PH-MC-MA-IM-FS' And (.w_CODART<= .w_CODART1 Or Empty(.w_CODART1))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non di tipo articolo, inesistente oppure incongruente")
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
        this.w_DTOBS1 = ctod("  /  /  ")
        this.w_TIPO = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART1
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART1)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART1))
          select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART1)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART1)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART1)+"%");

            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART1) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART1_1_5'),i_cWhere,'',"Articoli",'GSMA0ADI.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART1)
            select ARCODART,ARDESART,ARDTOBSO,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART1 = NVL(_Link_.ARCODART,space(20))
      this.w_DESART1 = NVL(_Link_.ARDESART,space(40))
      this.w_DTOBS2 = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_TIPO1 = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODART1 = space(20)
      endif
      this.w_DESART1 = space(40)
      this.w_DTOBS2 = ctod("  /  /  ")
      this.w_TIPO1 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPO1$'PF-SE-MP-PH-MC-MA-IM-FS' And (.w_CODART<= .w_CODART1 Or Empty(.w_CODART))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non di tipo articolo, inesistente oppure incongruente")
        endif
        this.w_CODART1 = space(20)
        this.w_DESART1 = space(40)
        this.w_DTOBS2 = ctod("  /  /  ")
        this.w_TIPO1 = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(5))
      this.w_ESVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(5)
      endif
      this.w_ESVALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAM_2_1'),i_cWhere,'GSAR_AFA',"Famiglie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(5)
      endif
      this.w_DESFAM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_CODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_CODGRU))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODGRU)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oCODGRU_2_3'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_CODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_CODGRU)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CODCAT))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCODCAT_2_5'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CODCAT)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCAT = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAR
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_CODMAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_CODMAR))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAR) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oCODMAR_2_7'),i_cWhere,'GSAR_AMH',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_CODMAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_CODMAR)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAR = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAR = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAR = space(5)
      endif
      this.w_DESMAR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLAMAT
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CMT_MAST_IDX,3]
    i_lTable = "CMT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2], .t., this.CMT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLAMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMD_BZC',True,'CMT_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_CLAMAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMTIPCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_CLAMAT))
          select CMCODICE,CMDESCRI,CMTIPCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLAMAT)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLAMAT) and !this.bDontReportError
            deferred_cp_zoom('CMT_MAST','*','CMCODICE',cp_AbsName(oSource.parent,'oCLAMAT_2_13'),i_cWhere,'GSMD_BZC',"Classi matricole",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMTIPCLA";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMTIPCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLAMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMTIPCLA";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_CLAMAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_CLAMAT)
            select CMCODICE,CMDESCRI,CMTIPCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLAMAT = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCLA = NVL(_Link_.CMDESCRI,space(35))
      this.w_TIPMAT = NVL(_Link_.CMTIPCLA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CLAMAT = space(5)
      endif
      this.w_DESCLA = space(35)
      this.w_TIPMAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPMAT='M'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CLAMAT = space(5)
        this.w_DESCLA = space(35)
        this.w_TIPMAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CMT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CMT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLAMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFOR
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_ANTIPCON;
                     ,'ANCODICE',trim(this.w_CODFOR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_ANTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFOR_2_14'),i_cWhere,'GSAR_BZC',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ANTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ANTIPCON;
                       ,'ANCODICE',this.w_CODFOR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSOFOR = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODFOR = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DATOBSOFOR = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSOFOR) OR .w_DATOBSOFOR>.w_OBTESTFOR
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_CODFOR = space(15)
        this.w_DESFOR = space(40)
        this.w_DATOBSOFOR = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTE
  func Link_1_48(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ADDERILE_IDX,3]
    i_lTable = "ADDERILE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ADDERILE_IDX,2], .t., this.ADDERILE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ADDERILE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ADD',True,'ADDERILE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODICE like "+cp_ToStrODBC(trim(this.w_CODUTE)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODICE',trim(this.w_CODUTE))
          select ARCODICE,ARDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODUTE)==trim(_Link_.ARCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODUTE) and !this.bDontReportError
            deferred_cp_zoom('ADDERILE','*','ARCODICE',cp_AbsName(oSource.parent,'oCODUTE_1_48'),i_cWhere,'GSMA_ADD',"Addetti rilevazione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',oSource.xKey(1))
            select ARCODICE,ARDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(this.w_CODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',this.w_CODUTE)
            select ARCODICE,ARDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE = NVL(_Link_.ARCODICE,space(5))
      this.w_DESUTE = NVL(_Link_.ARDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE = space(5)
      endif
      this.w_DESUTE = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ADDERILE_IDX,2])+'\'+cp_ToStr(_Link_.ARCODICE,1)
      cp_ShowWarn(i_cKey,this.ADDERILE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCOM
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODCOM_2_25'),i_cWhere,'GSAR_ACN',"COMMESSE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCOM = space(15)
      endif
      this.w_DESCOM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_2.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_2.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODRIL_1_3.value==this.w_CODRIL)
      this.oPgFrm.Page1.oPag.oCODRIL_1_3.value=this.w_CODRIL
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_4.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_4.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART1_1_5.value==this.w_CODART1)
      this.oPgFrm.Page1.oPag.oCODART1_1_5.value=this.w_CODART1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_7.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_7.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_11.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_11.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_12.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATARIL_1_16.value==this.w_DATARIL)
      this.oPgFrm.Page1.oPag.oDATARIL_1_16.value=this.w_DATARIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_22.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_22.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART1_1_44.value==this.w_DESART1)
      this.oPgFrm.Page1.oPag.oDESART1_1_44.value=this.w_DESART1
    endif
    if not(this.oPgFrm.Page2.oPag.oCODFAM_2_1.value==this.w_CODFAM)
      this.oPgFrm.Page2.oPag.oCODFAM_2_1.value=this.w_CODFAM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAM_2_2.value==this.w_DESFAM)
      this.oPgFrm.Page2.oPag.oDESFAM_2_2.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODGRU_2_3.value==this.w_CODGRU)
      this.oPgFrm.Page2.oPag.oCODGRU_2_3.value=this.w_CODGRU
    endif
    if not(this.oPgFrm.Page2.oPag.oDESGRU_2_4.value==this.w_DESGRU)
      this.oPgFrm.Page2.oPag.oDESGRU_2_4.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCAT_2_5.value==this.w_CODCAT)
      this.oPgFrm.Page2.oPag.oCODCAT_2_5.value=this.w_CODCAT
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAT_2_6.value==this.w_DESCAT)
      this.oPgFrm.Page2.oPag.oDESCAT_2_6.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page2.oPag.oCODMAR_2_7.value==this.w_CODMAR)
      this.oPgFrm.Page2.oPag.oCODMAR_2_7.value=this.w_CODMAR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAR_2_8.value==this.w_DESMAR)
      this.oPgFrm.Page2.oPag.oDESMAR_2_8.value=this.w_DESMAR
    endif
    if not(this.oPgFrm.Page2.oPag.oCLAMAT_2_13.value==this.w_CLAMAT)
      this.oPgFrm.Page2.oPag.oCLAMAT_2_13.value=this.w_CLAMAT
    endif
    if not(this.oPgFrm.Page2.oPag.oCODFOR_2_14.value==this.w_CODFOR)
      this.oPgFrm.Page2.oPag.oCODFOR_2_14.value=this.w_CODFOR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFOR_2_15.value==this.w_DESFOR)
      this.oPgFrm.Page2.oPag.oDESFOR_2_15.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLA_2_17.value==this.w_DESCLA)
      this.oPgFrm.Page2.oPag.oDESCLA_2_17.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page2.oPag.oMOVINI_2_24.value==this.w_MOVINI)
      this.oPgFrm.Page2.oPag.oMOVINI_2_24.value=this.w_MOVINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNONRIL_1_47.RadioValue()==this.w_FLNONRIL)
      this.oPgFrm.Page1.oPag.oFLNONRIL_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_48.value==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_48.value=this.w_CODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_50.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_50.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCOM_2_25.value==this.w_CODCOM)
      this.oPgFrm.Page2.oPag.oCODCOM_2_25.value=this.w_CODCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOM_2_27.value==this.w_DESCOM)
      this.oPgFrm.Page2.oPag.oDESCOM_2_27.value=this.w_DESCOM
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CODMAG)) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_2.SetFocus()
            i_bnoObbl = !empty(.w_CODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODRIL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODRIL_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CODRIL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPO$'PF-SE-MP-PH-MC-MA-IM-FS' And (.w_CODART<= .w_CODART1 Or Empty(.w_CODART1)))  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non di tipo articolo, inesistente oppure incongruente")
          case   not(.w_TIPO1$'PF-SE-MP-PH-MC-MA-IM-FS' And (.w_CODART<= .w_CODART1 Or Empty(.w_CODART)))  and not(empty(.w_CODART1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART1_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non di tipo articolo, inesistente oppure incongruente")
          case   not(.w_TIPMAT='M')  and not(g_MATR<>'S')  and not(empty(.w_CLAMAT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCLAMAT_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSOFOR) OR .w_DATOBSOFOR>.w_OBTESTFOR)  and not(empty(.w_CODFOR))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODFOR_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   (empty(.w_CODUTE))  and (.w_FLNONRIL='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODUTE_1_48.SetFocus()
            i_bnoObbl = !empty(.w_CODUTE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLNONRIL = this.w_FLNONRIL
    return

enddefine

* --- Define pages as container
define class tgsma_sdrPag1 as StdContainer
  Width  = 778
  height = 430
  stdWidth  = 778
  stdheight = 430
  resizeXpos=348
  resizeYpos=259
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODMAG_1_2 as StdField with uid="XFQJGETPXM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del magazzino",;
    HelpContextID = 151578586,;
   bGlobalFont=.t.,;
    Height=21, Width=59, Left=87, Top=12, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Magazzini",'',this.parent.oContained
  endproc

  add object oCODRIL_1_3 as StdField with uid="EZFFRZJWTV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODRIL", cQueryName = "CODRIL",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice rilevazione",;
    HelpContextID = 58976218,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=471, Top=10, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CODIRILE", oKey_1_1="RICODICE", oKey_1_2="this.w_CODRIL"

  func oCODRIL_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODRIL_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODRIL_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CODIRILE','*','RICODICE',cp_AbsName(this.parent,'oCODRIL_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici rilevazione",'',this.parent.oContained
  endproc

  add object oCODART_1_4 as StdField with uid="MFUENXZDXR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non di tipo articolo, inesistente oppure incongruente",;
    ToolTipText = "Codice dell'articolo da considerare",;
    HelpContextID = 184870874,;
   bGlobalFont=.t.,;
    Height=21, Width=171, Left=87, Top=43, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSMA0ADI.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oCODART1_1_5 as StdField with uid="ABOPZGQOQM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODART1", cQueryName = "CODART1",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non di tipo articolo, inesistente oppure incongruente",;
    ToolTipText = "Codice dell'articolo da considerare",;
    HelpContextID = 184870874,;
   bGlobalFont=.t.,;
    Height=21, Width=171, Left=87, Top=73, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART1"

  func oCODART1_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART1_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART1_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART1_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Articoli",'GSMA0ADI.ART_ICOL_VZM',this.parent.oContained
  endproc


  add object oBtn_1_6 as StdButton with uid="BRVGHGZAYB",left=723, top=54, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 61649850;
    , Caption='\<Esegui';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        GSMA_BDR(this.Parent.oContained,"RIC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CODMAG) AND NOT EMPTY(.w_CODRIL))
      endwith
    endif
  endfunc

  add object oDESART_1_7 as StdField with uid="SBVRMWDVTV",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 184811978,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=260, Top=43, InputMask=replicate('X',40)


  add object ZoomConf as cp_szoombox with uid="CZSVCBIDGT",left=0, top=171, width=780,height=210,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="ART_ICOL",cZoomFile="GSMA_SDR",bOptions=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,bAdvOptions=.f.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="GSMA_AAR",;
    nPag=1;
    , HelpContextID = 103910374

  add object oDESMAG_1_11 as StdField with uid="WPSJYHROON",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 151519690,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=148, Top=12, InputMask=replicate('X',30)

  add object oSELEZI_1_12 as StdRadio with uid="IJDSJFZVFN",rtseq=8,rtrep=.f.,left=473, top=385, width=136,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_12.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 92303578
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 92303578
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe")
      StdRadio::init()
    endproc

  func oSELEZI_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_12.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_12.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  add object oDATARIL_1_16 as StdField with uid="DMKXFPEGVR",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATARIL", cQueryName = "DATARIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 100922826,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=609, Top=10


  add object oBtn_1_21 as StdButton with uid="MSSQUKZPYA",left=7, top=384, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare le singole rilevazioni associate";
    , HelpContextID = 115265889;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      do GSMA_KDR with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not Empty( .w_KEYSAL ))
      endwith
    endif
  endfunc

  add object oDESCRI_1_22 as StdField with uid="GZRIYMYRMG",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 100794826,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=166, Top=385, InputMask=replicate('X',40)


  add object oBtn_1_23 as StdButton with uid="MTXNONHMHL",left=723, top=384, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 66769850;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_24 as StdButton with uid="JKGQHTAABU",left=672, top=384, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare la rettifica";
    , HelpContextID = 74058522;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSMA_BDR(this.Parent.oContained,"MOV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_CODMAG) And Not Empty(.w_CODRIL))
      endwith
    endif
  endfunc


  add object oObj_1_36 as cp_runprogram with uid="SCEEQYUDPZ",left=594, top=442, width=256,height=19,;
    caption='GSMA_BRL(C)',;
   bGlobalFont=.t.,;
    prg="GSMA_BRL('C')",;
    cEvent = "ZOOMCONF row checked",;
    nPag=1;
    , HelpContextID = 204570062


  add object oObj_1_37 as cp_runprogram with uid="RBSXKKZRNZ",left=594, top=465, width=256,height=19,;
    caption='GSMA_BRL(E)',;
   bGlobalFont=.t.,;
    prg="GSMA_BRL('E')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 204569550


  add object oObj_1_38 as cp_runprogram with uid="AVMJXIUQBI",left=594, top=511, width=256,height=19,;
    caption='GSMA_BDR(RIC)',;
   bGlobalFont=.t.,;
    prg="GSMA_BDR('RIC')",;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 111382840


  add object oObj_1_39 as cp_runprogram with uid="EQCUVARJAJ",left=594, top=488, width=256,height=19,;
    caption='GSMA_BRL(I)',;
   bGlobalFont=.t.,;
    prg="GSMA_BRL('I')",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 204568526


  add object oObj_1_43 as cp_runprogram with uid="AKBEHROCCI",left=594, top=534, width=256,height=19,;
    caption='GSMA_BRL(S)',;
   bGlobalFont=.t.,;
    prg="GSMA_BRL('S')",;
    cEvent = "w_CODART Changed",;
    nPag=1;
    , HelpContextID = 204565966

  add object oDESART1_1_44 as StdField with uid="SPFZUQYSII",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESART1", cQueryName = "DESART1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 184811978,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=260, Top=73, InputMask=replicate('X',40)

  add object oFLNONRIL_1_47 as StdCheck with uid="UMIKBBOFSI",rtseq=44,rtrep=.f.,left=22, top=102, caption="Visualizza articoli non rilevati",;
    ToolTipText = "Se attivo visualizza anche gli articoli senza rilevazione che verranno considerati con giacenza nulla",;
    HelpContextID = 46773666,;
    cFormVar="w_FLNONRIL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLNONRIL_1_47.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLNONRIL_1_47.GetRadio()
    this.Parent.oContained.w_FLNONRIL = this.RadioValue()
    return .t.
  endfunc

  func oFLNONRIL_1_47.SetRadio()
    this.Parent.oContained.w_FLNONRIL=trim(this.Parent.oContained.w_FLNONRIL)
    this.value = ;
      iif(this.Parent.oContained.w_FLNONRIL=='S',1,;
      0)
  endfunc

  add object oCODUTE_1_48 as StdField with uid="CUKYPQNLEO",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CODUTE", cQueryName = "CODUTE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Addetto alla rilevazione",;
    HelpContextID = 164685786,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=410, Top=104, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ADDERILE", cZoomOnZoom="GSMA_ADD", oKey_1_1="ARCODICE", oKey_1_2="this.w_CODUTE"

  func oCODUTE_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLNONRIL='S')
    endwith
   endif
  endfunc

  func oCODUTE_1_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_48('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTE_1_48.ecpDrop(oSource)
    this.Parent.oContained.link_1_48('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTE_1_48.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ADDERILE','*','ARCODICE',cp_AbsName(this.parent,'oCODUTE_1_48'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ADD',"Addetti rilevazione",'',this.parent.oContained
  endproc
  proc oCODUTE_1_48.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ADD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODICE=this.parent.oContained.w_CODUTE
     i_obj.ecpSave()
  endproc

  add object oDESUTE_1_50 as StdField with uid="WQLLCKOBBX",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 164626890,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=478, Top=104, InputMask=replicate('X',40)


  add object NORILBCK as cp_calclbl with uid="GSOUHSEEQA",left=467, top=148, width=133,height=19,;
    caption='NORILBCK',;
   bGlobalFont=.t.,;
    caption="AAA",;
    nPag=1;
    , HelpContextID = 44300577

  add object oStr_1_8 as StdString with uid="KTRDUOEDAS",Visible=.t., Left=22, Top=43,;
    Alignment=1, Width=62, Height=18,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="CTDDYNKFKJ",Visible=.t., Left=16, Top=11,;
    Alignment=1, Width=69, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="FKVZBWRPGJ",Visible=.t., Left=398, Top=14,;
    Alignment=1, Width=66, Height=18,;
    Caption="Rilevazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="PZQADHNMZX",Visible=.t., Left=573, Top=13,;
    Alignment=1, Width=32, Height=18,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="OSTEXODWMB",Visible=.t., Left=92, Top=385,;
    Alignment=1, Width=71, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="JDBSZIHTZV",Visible=.t., Left=444, Top=131,;
    Alignment=1, Width=204, Height=18,;
    Caption="Movimenti di magazzino da generare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="EJEEPYGEGU",Visible=.t., Left=657, Top=131,;
    Alignment=0, Width=53, Height=18,;
    Caption="Carico"    , forecolor=RGB(255,0,0);
  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="MOYUSCKJFO",Visible=.t., Left=719, Top=131,;
    Alignment=0, Width=58, Height=18,;
    Caption="Scarico"    , forecolor=RGB(0,0,255);
  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="OEJUDUQNDM",Visible=.t., Left=702, Top=131,;
    Alignment=0, Width=18, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="THWYPUNBKO",Visible=.t., Left=22, Top=73,;
    Alignment=1, Width=62, Height=18,;
    Caption="a articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="MVPMQZCNKC",Visible=.t., Left=309, Top=106,;
    Alignment=1, Width=99, Height=18,;
    Caption="Addetto ril.:"  ;
  , bGlobalFont=.t.
enddefine
define class tgsma_sdrPag2 as StdContainer
  Width  = 778
  height = 430
  stdWidth  = 778
  stdheight = 430
  resizeXpos=441
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODFAM_2_1 as StdField with uid="MCPAWQEAGQ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_CODFAM", cQueryName = "CODFAM",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della famiglia",;
    HelpContextID = 51374042,;
   bGlobalFont=.t.,;
    Height=21, Width=70, Left=195, Top=21, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAM"

  func oCODFAM_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAM_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAM_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAM_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglie",'',this.parent.oContained
  endproc
  proc oCODFAM_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_CODFAM
     i_obj.ecpSave()
  endproc

  add object oDESFAM_2_2 as StdField with uid="XSVOEFBGJX",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 51315146,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=269, Top=21, InputMask=replicate('X',35)

  add object oCODGRU_2_3 as StdField with uid="HWKDNZTFNI",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CODGRU", cQueryName = "CODGRU",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo merceologico selezionato",;
    HelpContextID = 167700442,;
   bGlobalFont=.t.,;
    Height=21, Width=70, Left=195, Top=50, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_CODGRU"

  func oCODGRU_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRU_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oCODGRU_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc oCODGRU_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_CODGRU
     i_obj.ecpSave()
  endproc

  add object oDESGRU_2_4 as StdField with uid="VZRGQKGCXR",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 167641546,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=269, Top=50, InputMask=replicate('X',35)

  add object oCODCAT_2_5 as StdField with uid="AYKULNVLKR",rtseq=30,rtrep=.f.,;
    cFormVar = "w_CODCAT", cQueryName = "CODCAT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria omogenea",;
    HelpContextID = 202565594,;
   bGlobalFont=.t.,;
    Height=21, Width=70, Left=195, Top=78, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CODCAT"

  func oCODCAT_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAT_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCODCAT_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc oCODCAT_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CODCAT
     i_obj.ecpSave()
  endproc

  add object oDESCAT_2_6 as StdField with uid="VFOWNTZJPV",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 202506698,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=269, Top=78, InputMask=replicate('X',35)

  add object oCODMAR_2_7 as StdField with uid="MGBDZJJEUS",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CODMAR", cQueryName = "CODMAR",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della marca",;
    HelpContextID = 235464666,;
   bGlobalFont=.t.,;
    Height=21, Width=70, Left=195, Top=106, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_CODMAR"

  func oCODMAR_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAR_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAR_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oCODMAR_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchi",'',this.parent.oContained
  endproc
  proc oCODMAR_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_CODMAR
     i_obj.ecpSave()
  endproc

  add object oDESMAR_2_8 as StdField with uid="ZGNRCKTQCK",rtseq=33,rtrep=.f.,;
    cFormVar = "w_DESMAR", cQueryName = "DESMAR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 235405770,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=269, Top=106, InputMask=replicate('X',35)

  add object oCLAMAT_2_13 as StdField with uid="NTLNEJMXBP",rtseq=34,rtrep=.f.,;
    cFormVar = "w_CLAMAT", cQueryName = "CLAMAT",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classe matricola associata all'articolo",;
    HelpContextID = 201923290,;
   bGlobalFont=.t.,;
    Height=21, Width=70, Left=195, Top=134, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CMT_MAST", cZoomOnZoom="GSMD_BZC", oKey_1_1="CMCODICE", oKey_1_2="this.w_CLAMAT"

  func oCLAMAT_2_13.mHide()
    with this.Parent.oContained
      return (g_MATR<>'S')
    endwith
  endfunc

  func oCLAMAT_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLAMAT_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLAMAT_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CMT_MAST','*','CMCODICE',cp_AbsName(this.parent,'oCLAMAT_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMD_BZC',"Classi matricole",'',this.parent.oContained
  endproc
  proc oCLAMAT_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSMD_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_CLAMAT
     i_obj.ecpSave()
  endproc

  add object oCODFOR_2_14 as StdField with uid="YBKIRNHRLX",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CODFOR", cQueryName = "CODFOR",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice del fornitore abituale",;
    HelpContextID = 221243354,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=195, Top=162, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_ANTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFOR"

  func oCODFOR_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFOR_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFOR_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_ANTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_ANTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFOR_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oCODFOR_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_ANTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODFOR
     i_obj.ecpSave()
  endproc

  add object oDESFOR_2_15 as StdField with uid="WGKENYWBRY",rtseq=36,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 221184458,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=327, Top=163, InputMask=replicate('X',40)

  add object oDESCLA_2_17 as StdField with uid="HNGBSBYXGL",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 241304010,;
   bGlobalFont=.t.,;
    Height=21, Width=272, Left=269, Top=134, InputMask=replicate('X',35)

  func oDESCLA_2_17.mHide()
    with this.Parent.oContained
      return (g_MATR<>'S')
    endwith
  endfunc

  add object oMOVINI_2_24 as StdField with uid="DWWWFLOBKA",rtseq=43,rtrep=.f.,;
    cFormVar = "w_MOVINI", cQueryName = "MOVINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Selezione degli articoli movimentati solo dopo questa data. Il filtro viene effettuato sulle date di ultimo acquisto e vendita dei saldi per il magazzino selezionato",;
    HelpContextID = 104580922,;
   bGlobalFont=.t.,;
    Height=21, Width=94, Left=195, Top=190

  add object oCODCOM_2_25 as StdField with uid="QDGARZAOEL",rtseq=47,rtrep=.f.,;
    cFormVar = "w_CODCOM", cQueryName = "CODCOM",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Commessa selezionata",;
    HelpContextID = 36890586,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=195, Top=218, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODCOM"

  func oCODCOM_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S')
    endwith
   endif
  endfunc

  func oCODCOM_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCOM_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCOM_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODCOM_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"COMMESSE",'',this.parent.oContained
  endproc
  proc oCODCOM_2_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODCOM
     i_obj.ecpSave()
  endproc

  add object oDESCOM_2_27 as StdField with uid="EFNBUAQAWP",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 36831690,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=327, Top=218, InputMask=replicate('X',40)

  add object oStr_2_9 as StdString with uid="HQEBHXDTBU",Visible=.t., Left=63, Top=21,;
    Alignment=1, Width=130, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="HYHCLRHICJ",Visible=.t., Left=63, Top=50,;
    Alignment=1, Width=130, Height=15,;
    Caption="Gr. merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="HZYENRORBC",Visible=.t., Left=63, Top=78,;
    Alignment=1, Width=130, Height=15,;
    Caption="Cat. omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="SAVXOGFTRF",Visible=.t., Left=63, Top=106,;
    Alignment=1, Width=130, Height=15,;
    Caption="Marca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="UDUHDSQZQL",Visible=.t., Left=63, Top=162,;
    Alignment=1, Width=130, Height=18,;
    Caption="Fornitore abituale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="LHIBCIEKXR",Visible=.t., Left=63, Top=131,;
    Alignment=1, Width=130, Height=18,;
    Caption="Classe matricola:"  ;
  , bGlobalFont=.t.

  func oStr_2_18.mHide()
    with this.Parent.oContained
      return (g_MATR<>'S')
    endwith
  endfunc

  add object oStr_2_23 as StdString with uid="GQFVKNUAXL",Visible=.t., Left=0, Top=193,;
    Alignment=1, Width=192, Height=18,;
    Caption="Articoli non movimentati dal:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="HWXTFIMJIL",Visible=.t., Left=110, Top=221,;
    Alignment=1, Width=82, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_sdr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
