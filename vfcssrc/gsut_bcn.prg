* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bcn                                                        *
*              Colorazione pagina note                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-09-30                                                      *
* Last revis.: 2010-12-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione,pSecondoPar
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bcn",oParentObject,m.pAzione,m.pSecondoPar)
return(i_retval)

define class tgsut_bcn as StdBatch
  * --- Local variables
  pAzione = space(10)
  pSecondoPar = space(10)
  w_Obj = .NULL.
  w_Conta = 0
  w_TotPagine = 0
  w_ColorePagina = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- In tutte le gestioni � presente la variabile w_PaginaNote, che contiene il numero della pagina appunto di "Note"
    do case
      case IsAlt() AND UPPER(this.pAzione)=="PAGINA" AND VARTYPE(this.pSecondoPar)="C"
        * --- Data la gestione, rintraccia il numero della pagina che ha per caption pSecondoPar
        this.w_Obj = this.oParentObject
        this.w_Conta = 1
        this.w_TotPagine = this.w_Obj.oPgFrm.Pagecount
        do while this.oParentObject.w_PaginaNote= 0 AND this.w_Conta <= this.w_TotPagine
          ContaStr=ALLTRIM(STR(this.w_Conta))+".Caption"
          if ALLTRIM(this.w_Obj.oPgFrm.Page&ContaStr)==this.pSecondoPar
            this.oParentObject.w_PaginaNote = this.w_Conta
            Private ContaStr 
 ContaStr=""
            if i_VisualTheme=-1 OR i_cMenuTab="S"
              ContaStr=ALLTRIM(STR(this.oParentObject.w_PaginaNote))+".ForeColor"
              this.oParentObject.w_BckpForClr = this.w_Obj.oPGFRM.Page&ContaStr
            else
              ContaStr=PADL(ALLTRIM(STR(this.oParentObject.w_PaginaNote)),3,"0")+".ItemCaption.ForeColor"
              this.oParentObject.w_BckpForClr = this.w_Obj.oTabMenu.oTabmenu.MenuItem&ContaStr
            endif
          else
            this.w_Conta = this.w_Conta + 1
          endif
        enddo
      case this.oParentObject.w_PaginaNote>0 AND UPPER(this.pAzione)=="COLORA"
        this.w_Obj = this.oParentObject
        Private ContaStr 
 ContaStr=""
        if i_VisualTheme=-1 OR i_cMenuTab="S"
          ContaStr=ALLTRIM(STR(this.oParentObject.w_PaginaNote))+".ForeColor"
        else
          ContaStr=PADL(ALLTRIM(STR(this.oParentObject.w_PaginaNote)),3,"0")+".ItemCaption.ForeColor"
        endif
        if VARTYPE(this.pSecondoPar)="C" AND !EMPTY(this.pSecondoPar)
          this.w_ColorePagina = 255
        else
          this.w_ColorePagina = this.oParentObject.w_BckpForClr
        endif
        if i_VisualTheme=-1 OR i_cMenuTab="S"
          this.w_Obj.oPGFRM.Page&ContaStr = this.w_ColorePagina
        else
          this.w_Obj.oTabMenu.oTabmenu.MenuItem&ContaStr = this.w_ColorePagina
        endif
    endcase
  endproc


  proc Init(oParentObject,pAzione,pSecondoPar)
    this.pAzione=pAzione
    this.pSecondoPar=pSecondoPar
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione,pSecondoPar"
endproc
