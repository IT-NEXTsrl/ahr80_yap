* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bl4                                                        *
*              Controlli inserimento listino                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-19                                                      *
* Last revis.: 2010-01-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bl4",oParentObject)
return(i_retval)

define class tgsma_bl4 as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_LISCONT1 = 0
  w_LISCONT2 = 0
  w_LISCONT3 = 0
  w_LISCONT4 = 0
  w_LIQUANTI = 0
  w_LIPREZZO = 0
  w_CPROWNUM = 0
  w_LIROWNUM = 0
  w_ANNULLA = .f.
  w_RESOCON1 = space(0)
  w_MESBLOK = space(0)
  w_DAIM = .f.
  w_UNO = .f.
  w_NOSCAG = .f.
  * --- WorkFile variables
  LIS_TINI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli inserimento listino
    this.w_PADRE = This.oParentObject
    this.w_CPROWNUM = 0
    this.oParentObject.w_OK = .T.
    this.w_RESOCON1 = ah_MsgFormat("Per i seguenti articoli sono state riscontrate alcune incongruenze:%0")
    this.w_PADRE.FirstRow()     
    do while Not this.w_PADRE.Eof_Trs()
      if this.w_PADRE.FullRow()
        this.w_PADRE.SetRow()     
        this.w_PADRE.ChildrenChangeRow()     
        this.w_PADRE.GSMA_MS2.SetRow()     
        * --- Read from LIS_TINI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LIS_TINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CPROWNUM"+;
            " from "+i_cTable+" LIS_TINI where ";
                +"LICODART = "+cp_ToStrODBC(this.oParentObject.w_LICODART);
                +" and LICODLIS = "+cp_ToStrODBC(this.oParentObject.w_LICODLIS);
                +" and LIDATATT = "+cp_ToStrODBC(this.oParentObject.w_LIDATATT);
                +" and LIDATDIS = "+cp_ToStrODBC(this.oParentObject.w_LIDATDIS);
                +" and LIUNIMIS = "+cp_ToStrODBC(this.oParentObject.w_LIUNIMIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CPROWNUM;
            from (i_cTable) where;
                LICODART = this.oParentObject.w_LICODART;
                and LICODLIS = this.oParentObject.w_LICODLIS;
                and LIDATATT = this.oParentObject.w_LIDATATT;
                and LIDATDIS = this.oParentObject.w_LIDATDIS;
                and LIUNIMIS = this.oParentObject.w_LIUNIMIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CPROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0 AND this.w_CPROWNUM<>0
          this.w_RESOCON1 = this.w_RESOCON1 + Ah_MsgFormat("%1 Gi� presente un listino con stesse date di validit�%0",Alltrim(this.oParentObject.w_LICODART))
          this.oParentObject.w_OK = .F.
        endif
        this.w_UNO = .F.
        this.w_NOSCAG = .F.
        this.w_PADRE.GSMA_MS2.FirstRow()     
        do while Not this.w_PADRE.GSMA_MS2.Eof_Trs()
          this.w_PADRE.GSMA_MS2.SetRow()     
          if this.w_PADRE.GSMA_MS2.w_LIPREZZO = 0 And this.w_PADRE.GSMA_MS2.w_LIQUANTI= 0
            this.w_NOSCAG = .T.
          else
            * --- marco se c'� almeno uno scaglione inserito per evitare il messaggio sopra
            this.w_UNO = .T.
          endif
          this.w_PADRE.GSMA_MS2.NextRow()     
        enddo
        if !this.w_UNO AND this.w_NOSCAG=.T.
          this.w_RESOCON1 = this.w_RESOCON1 + Ah_MsgFormat("%1 Scaglioni non inseriti%0",Alltrim(this.oParentObject.w_LICODART))
          this.oParentObject.w_OK = .F.
        endif
      endif
      this.w_PADRE.NextRow()     
    enddo
    * --- Se esistono gi� i prezzi con le stesse date per alcuni articoli emetto messaggio di log
    if Not this.oParentObject.w_OK
      do GSVE_KLG with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.oParentObject.w_OK = this.w_ANNULLA
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LIS_TINI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
