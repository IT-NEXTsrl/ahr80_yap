* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bci                                                        *
*              Gestione configurazione interfaccia                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_35]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-02-16                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bci",oParentObject,m.pOper)
return(i_retval)

define class tgsut_bci as StdBatch
  * --- Local variables
  pOper = space(30)
  cStyle = space(25)
  cDefault = space(25)
  cFileXML = space(100)
  cFileConfig = space(100)
  cFileTheme = space(100)
  oXML = .NULL.
  oCtrl = .NULL.
  oRootNode = .NULL.
  cRootTagName = space(50)
  cParentName = space(50)
  cName = space(50)
  oNodeList = .NULL.
  oNode = .NULL.
  bHasChild = .f.
  oFatherNodeList = .NULL.
  oFatherNode = .NULL.
  nFatherLen = 0
  nFatherPass = 0
  oChildNodeList = .NULL.
  nChildLen = 0
  nPass = 0
  oChildNode = .NULL.
  cTextData = space(100)
  nTextDataLen = 0
  nNumNodes = 0
  nType = 0
  w_COLOR = 0
  * --- WorkFile variables
  CONF_INT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione maschera interfaccia configurazione
    do case
      case Left(this.pOper,5)="Style"
        * --- Aggiorno la maschera con i valori di default, alternando file di configurazione e tema
        this.cDefault = iif(this.oParentObject.oParentObject.w_CODUTE >= -1, "Predefinita", "Mobile_Predefinita")
        this.cStyle = this.cDefault
        this.cFileConfig = cPathVfcsim+"\Themes\Default_"+IIF(IsAhr(),"ahr",IIF(IsAhe(),"AHE","AETOP"))+".xml"
        this.oCtrl = This.oParentObject.GetCtrl("w_CIVTHEME")
        this.cFileTheme = this.oCtrl.GetPath()
        this.cFileXML = this.cFileConfig
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.cFileXML = this.cFileTheme
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Recupero lo stile dal parametro "Style-Valore"
        this.cStyle = StrTran(this.pOper,"Style-","")
        this.cStyle = StrTran(this.cStyle," ","_")
        * --- Continuo l'aggiornamento se � stato definita una configurazione diversa da Default
        if Upper(this.cStyle) <> Upper(this.cDefault)
          this.cFileXML = this.cFileConfig
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.cFileXML = this.cFileTheme
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        This.oParentObject.NotifyEvent("After BCI")
      case this.pOper="SetByTheme"
        this.cStyle = iif(this.oParentObject.oParentObject.w_CODUTE >= -1, "Predefinita", "Mobile_Predefinita")
        this.oCtrl = This.oParentObject.GetCtrl("w_CIVTHEME")
        this.cFileXML = this.oCtrl.GetPath()
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="Elimina"
        this.oParentObject.GSUT_ACU.bUpdated=.f.
        * --- Delete from CONF_INT
        i_nConn=i_TableProp[this.CONF_INT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONF_INT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"CICODUTE = "+cp_ToStrODBC(this.oParentObject.w_CODUTE);
                 )
        else
          delete from (i_cTable) where;
                CICODUTE = this.oParentObject.w_CODUTE;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        if this.oParentObject.w_CODUTE<>i_CODUTE
          this.oParentObject.w_CODUTE = i_CODUTE
          * --- Read from CONF_INT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONF_INT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONF_INT_idx,2],.t.,this.CONF_INT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" CONF_INT where ";
                  +"CICODUTE = "+cp_ToStrODBC(this.oParentObject.w_CODUTE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  CICODUTE = this.oParentObject.w_CODUTE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows=0
            this.oParentObject.w_CODUTE = -1
          endif
        else
          this.oParentObject.w_CODUTE = -1
        endif
        do CalcComboCODUTE with this.oParentObject in GSUT_KCI
        this.oParentObject.mCalc(.t.)
        this.oParentObject.NotifyEvent("Aggiorna")
      otherwise
        * --- Setto i colori, apro la dialog di selezione colore, se utente
        *     seleziona un colore lo salvo all'interno della relativa variabile
        *     Il parametro contiene la var. caller da valorizzare...
         
 Local L_ExecCMD 
 L_ExecCMD= "this.oParentObject.w_"+this.pOper
        this.w_COLOR = GetColor(&L_ExecCMD)
        if this.w_COLOR<>-1
           
 L_ExecCMD=L_ExecCMD+"=this.w_COLOR" 
 &L_ExecCMD
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura file XML
    this.oXML = CREATEOBJECT("MSXML2.DomDocument")
    this.oXML.ASYNC = .F.
    this.oXML.Load(this.cFileXML)     
    * --- Verifico che il file sia ben formato
    if Type("this.oXML.documentElement")="O" AND Not IsNull(this.oXML.documentElement)
      this.oRootNode = this.oXML.documentElement
      * --- Prendo solo la lista dei nodi relativi al tag richiesto
      this.oNodeList = this.oRootNode.getElementsByTagName(this.cStyle)
      this.nNumNodes = this.oNodeList.LENGTH
       For nPos = 0 To (this.nNumNodes-1) Step 1
      this.oNode = this.oNodeList.ITEM(nPos)
      this.cParentName = this.oNode.nodeName
      this.bHasChild = this.oNode.hasChildNodes()
      this.nType = this.oNode.nodeType
      if this.nType=1
        if this.bHasChild
          this.oFatherNodeList = this.oNode.childNodes
          this.nFatherLen = this.oFatherNodeList.LENGTH
          FOR this.nFatherPass = 0 TO (this.nFatherLen-1) STEP 1
          this.oFatherNode = this.oFatherNodeList.ITEM(this.nFatherPass)
          this.oChildNodeList = this.oFatherNode.childNodes
          this.nChildLen = this.oChildNodeList.LENGTH
          FOR this.nPass = 0 TO (this.nChildLen-1) STEP 1
          * --- Aggiorno il campo nella maschera
          * --- Accesso al nome del campo da aggiornare (nella propriet� cTagName)
          cTagName = this.oFatherNode.tagName
          this.oChildNode = this.oChildNodeList.ITEM(this.nPass)
          this.nType = this.oChildNode.nodeType
          this.bHasChild = this.oChildNode.hasChildNodes()
          this.cTextData = this.oChildNode.DATA
          this.nTextDataLen = this.oChildNode.LENGTH
          this.cTextData = StrTran(this.cTextData,"w_","This.oParentObject.w_")
          this.cTextData = StrTran(this.cTextData,"&lt;","<")
          if IsDigit(this.cTextData)
            * --- Numero
            InsData = Int(Val(this.cTextData))
          else
            * --- Colori, condizioni, spazi, booleani e espressioni forzate a stringa
            if Upper(Left(this.cTextData,3)) == "RGB" or Upper(Left(this.cTextData,3)) == "IIF" or Upper(Left(this.cTextData,5)) == "SPACE" or Upper(Left(this.cTextData,3)) == ".T." or Upper(Left(this.cTextData,3)) == ".F." or Left(this.cTextData,1) == "'"
              InsData = Evaluate(this.cTextData)
            else
              * --- Carattere
              InsData = this.cTextData
            endif
          endif
          This.oParentObject.w_&cTagName = InsData
          Next
          Next
        endif
      endif
      Next
    endif
    this.oXML = .Null.
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONF_INT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
