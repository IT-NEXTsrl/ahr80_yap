* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bpc                                                        *
*              INSERIMENTO CLASSI DOCUMENTALI                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF]                                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-28                                                      *
* Last revis.: 2016-04-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_TIPINSERT
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bpc",oParentObject,m.w_TIPINSERT)
return(i_retval)

define class tgsut_bpc as StdBatch
  * --- Local variables
  w_TIPINSERT = space(1)
  w_MESS = space(50)
  w_OLDNOMFIL = space(200)
  * --- WorkFile variables
  PRODCLAS_idx=0
  PROMCLAS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per l'inserimento delle Classi documentali, con Carica/Salva dati esterni, invocato da 'GSUT_BCO('Carica')'
    * --- Try
    local bErr_0468D530
    bErr_0468D530=bTrsErr
    this.Try_0468D530()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.oParentObject.w_ERRORECLA = .T.
      if this.oParentObject.w_NOMSG
        * --- accept error
        bTrsErr=.f.
        if not Empty(this.oParentObject.w_CDNOMFIL)
          * --- Aggiorno il nome file WEB (da wizard import classi di I.Revolution)
          * --- Read from PROMCLAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PROMCLAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CDNOMFIL"+;
              " from "+i_cTable+" PROMCLAS where ";
                  +"CDCODCLA = "+cp_ToStrODBC(this.oParentObject.w_CLCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CDNOMFIL;
              from (i_cTable) where;
                  CDCODCLA = this.oParentObject.w_CLCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_OLDNOMFIL = NVL(cp_ToDate(_read_.CDNOMFIL),cp_NullValue(_read_.CDNOMFIL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if Empty(this.w_OLDNOMFIL)
            * --- Write into PROMCLAS
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PROMCLAS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMCLAS_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CDNOMFIL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDNOMFIL),'PROMCLAS','CDNOMFIL');
                  +i_ccchkf ;
              +" where ";
                  +"CDCODCLA = "+cp_ToStrODBC(this.oParentObject.w_CLCODICE);
                     )
            else
              update (i_cTable) set;
                  CDNOMFIL = this.oParentObject.w_CDNOMFIL;
                  &i_ccchkf. ;
               where;
                  CDCODCLA = this.oParentObject.w_CLCODICE;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
    endif
    bTrsErr=bTrsErr or bErr_0468D530
    * --- End
  endproc
  proc Try_0468D530()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.w_TIPINSERT="D"
        * --- Try
        local bErr_0469A940
        bErr_0469A940=bTrsErr
        this.Try_0469A940()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Try
          local bErr_04698A80
          bErr_04698A80=bTrsErr
          this.Try_04698A80()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.w_MESS = "Errore di scrittura in codice: %1%2Continuo?"
            if this.oParentObject.w_NOMSG or not AH_YESNO(this.w_MESS,"",this.oParentObject.w_CLCODICE,CHR(13))
              * --- Raise
              i_Error="Import fallito"
              return
            else
              * --- accept error
              bTrsErr=.f.
            endif
          endif
          bTrsErr=bTrsErr or bErr_04698A80
          * --- End
        endif
        bTrsErr=bTrsErr or bErr_0469A940
        * --- End
      case this.w_TIPINSERT="M"
        * --- Try
        local bErr_046977C0
        bErr_046977C0=bTrsErr
        this.Try_046977C0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_MESS = "Errore di inserimento in codice: %1%2Continuo?"
          if this.oParentObject.w_NOMSG or not AH_YESNO(this.w_MESS,"",this.oParentObject.w_CLCODICE,CHR(13))
            * --- Raise
            i_Error="Import fallito"
            return
          else
            * --- accept error
            bTrsErr=.f.
          endif
        endif
        bTrsErr=bTrsErr or bErr_046977C0
        * --- End
    endcase
    return
  proc Try_0469A940()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRODCLAS
    i_nConn=i_TableProp[this.PRODCLAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODCLAS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CDCODCLA"+",CPROWNUM"+",CPROWORD"+",CDCODATT"+",CDDESATT"+",CDTIPATT"+",CDCAMCUR"+",CDCHKOBB"+",CDTABKEY"+",CDVLPRED"+",CDNATAWE"+",CDLUNATT"+",CDDECATT"+",CDKEYAWE"+",CDFLGSOS"+",CDRIFTEM"+",CDATTSOS"+",CDATTPRI"+",CDATTINF"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CLCODICE),'PRODCLAS','CDCODCLA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'PRODCLAS','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'PRODCLAS','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDCODATT),'PRODCLAS','CDCODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDDESATT),'PRODCLAS','CDDESATT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDTIPATT),'PRODCLAS','CDTIPATT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDCAMCUR),'PRODCLAS','CDCAMCUR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDCHKOBB),'PRODCLAS','CDCHKOBB');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDTABKEY),'PRODCLAS','CDTABKEY');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDVLPRED),'PRODCLAS','CDVLPRED');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDNATAWE),'PRODCLAS','CDNATAWE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDLUNATT),'PRODCLAS','CDLUNATT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDDECATT),'PRODCLAS','CDDECATT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDKEYAWE),'PRODCLAS','CDKEYAWE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDFLGSOS),'PRODCLAS','CDFLGSOS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDRIFTEM),'PRODCLAS','CDRIFTEM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDATTSOS),'PRODCLAS','CDATTSOS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDATTPRI),'PRODCLAS','CDATTPRI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDATTINF),'PRODCLAS','CDATTINF');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CDCODCLA',this.oParentObject.w_CLCODICE,'CPROWNUM',this.oParentObject.w_CPROWNUM,'CPROWORD',this.oParentObject.w_CPROWORD,'CDCODATT',this.oParentObject.w_CDCODATT,'CDDESATT',this.oParentObject.w_CDDESATT,'CDTIPATT',this.oParentObject.w_CDTIPATT,'CDCAMCUR',this.oParentObject.w_CDCAMCUR,'CDCHKOBB',this.oParentObject.w_CDCHKOBB,'CDTABKEY',this.oParentObject.w_CDTABKEY,'CDVLPRED',this.oParentObject.w_CDVLPRED,'CDNATAWE',this.oParentObject.w_CDNATAWE,'CDLUNATT',this.oParentObject.w_CDLUNATT)
      insert into (i_cTable) (CDCODCLA,CPROWNUM,CPROWORD,CDCODATT,CDDESATT,CDTIPATT,CDCAMCUR,CDCHKOBB,CDTABKEY,CDVLPRED,CDNATAWE,CDLUNATT,CDDECATT,CDKEYAWE,CDFLGSOS,CDRIFTEM,CDATTSOS,CDATTPRI,CDATTINF &i_ccchkf. );
         values (;
           this.oParentObject.w_CLCODICE;
           ,this.oParentObject.w_CPROWNUM;
           ,this.oParentObject.w_CPROWORD;
           ,this.oParentObject.w_CDCODATT;
           ,this.oParentObject.w_CDDESATT;
           ,this.oParentObject.w_CDTIPATT;
           ,this.oParentObject.w_CDCAMCUR;
           ,this.oParentObject.w_CDCHKOBB;
           ,this.oParentObject.w_CDTABKEY;
           ,this.oParentObject.w_CDVLPRED;
           ,this.oParentObject.w_CDNATAWE;
           ,this.oParentObject.w_CDLUNATT;
           ,this.oParentObject.w_CDDECATT;
           ,this.oParentObject.w_CDKEYAWE;
           ,this.oParentObject.w_CDFLGSOS;
           ,this.oParentObject.w_CDRIFTEM;
           ,this.oParentObject.w_CDATTSOS;
           ,this.oParentObject.w_CDATTPRI;
           ,this.oParentObject.w_CDATTINF;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04698A80()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into PRODCLAS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRODCLAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODCLAS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWORD),'PRODCLAS','CPROWORD');
      +",CDCODATT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDCODATT),'PRODCLAS','CDCODATT');
      +",CDDESATT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDDESATT),'PRODCLAS','CDDESATT');
      +",CDCHKOBB ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDCHKOBB),'PRODCLAS','CDCHKOBB');
      +",CDTIPATT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDTIPATT),'PRODCLAS','CDTIPATT');
      +",CDVLPRED ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDVLPRED),'PRODCLAS','CDVLPRED');
      +",CDCAMCUR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDCAMCUR),'PRODCLAS','CDCAMCUR');
      +",CDTABKEY ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDTABKEY),'PRODCLAS','CDTABKEY');
      +",CDNATAWE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDNATAWE),'PRODCLAS','CDNATAWE');
      +",CDLUNATT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDLUNATT),'PRODCLAS','CDLUNATT');
      +",CDDECATT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDDECATT),'PRODCLAS','CDDECATT');
      +",CDKEYAWE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDKEYAWE),'PRODCLAS','CDKEYAWE');
      +",CDFLGSOS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDFLGSOS),'PRODCLAS','CDFLGSOS');
      +",CDRIFTEM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDRIFTEM),'PRODCLAS','CDRIFTEM');
      +",CDATTSOS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDATTSOS),'PRODCLAS','CDATTSOS');
      +",CDATTPRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDATTPRI),'PRODCLAS','CDATTPRI');
      +",CDATTINF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDATTINF),'PRODCLAS','CDATTINF');
          +i_ccchkf ;
      +" where ";
          +"CDCODCLA = "+cp_ToStrODBC(this.oParentObject.w_CLCODICE);
          +" and CPROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
             )
    else
      update (i_cTable) set;
          CPROWORD = this.oParentObject.w_CPROWORD;
          ,CDCODATT = this.oParentObject.w_CDCODATT;
          ,CDDESATT = this.oParentObject.w_CDDESATT;
          ,CDCHKOBB = this.oParentObject.w_CDCHKOBB;
          ,CDTIPATT = this.oParentObject.w_CDTIPATT;
          ,CDVLPRED = this.oParentObject.w_CDVLPRED;
          ,CDCAMCUR = this.oParentObject.w_CDCAMCUR;
          ,CDTABKEY = this.oParentObject.w_CDTABKEY;
          ,CDNATAWE = this.oParentObject.w_CDNATAWE;
          ,CDLUNATT = this.oParentObject.w_CDLUNATT;
          ,CDDECATT = this.oParentObject.w_CDDECATT;
          ,CDKEYAWE = this.oParentObject.w_CDKEYAWE;
          ,CDFLGSOS = this.oParentObject.w_CDFLGSOS;
          ,CDRIFTEM = this.oParentObject.w_CDRIFTEM;
          ,CDATTSOS = this.oParentObject.w_CDATTSOS;
          ,CDATTPRI = this.oParentObject.w_CDATTPRI;
          ,CDATTINF = this.oParentObject.w_CDATTINF;
          &i_ccchkf. ;
       where;
          CDCODCLA = this.oParentObject.w_CLCODICE;
          and CPROWNUM = this.oParentObject.w_CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_046977C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_msg("Inserisco la classe: %1",.t.,.f.,.f.,this.oParentObject.w_CLCODICE)
    * --- Inserisco la testata
    * --- Insert into PROMCLAS
    i_nConn=i_TableProp[this.PROMCLAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PROMCLAS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CDCODCLA"+",CDDESCLA"+",CDTIPARC"+",CDMOTARC"+",CDCLAAWE"+",CDPATSTD"+",CDTIPRAG"+",CDCAMRAG"+",CDRIFDEF"+",CDRIFTAB"+",CDMODALL"+",CDTIPALL"+",CDCLAALL"+",CDCOMTIP"+",CDCOMCLA"+",CDFOLDER"+",CDFIRDIG"+",CDCONSOS"+",CDCLASOS"+",CDAVVSOS"+",CDKEYUPD"+",CDGETDES"+",CDSERVER"+",CD__PORT"+",CD__USER"+",CDPASSWD"+",CDARCHIVE"+",CDFLCTRL"+",CDCLAPRA"+",CDALIASF"+",CDFLGDES"+",CDNOMFIL"+",CDTIPOBC"+",CDPUBWEB"+",CDERASEF"+",CDREPOBC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CLCODICE),'PROMCLAS','CDCODCLA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CLDESCRI),'PROMCLAS','CDDESCLA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDTIPARC),'PROMCLAS','CDTIPARC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDMOTARC),'PROMCLAS','CDMOTARC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDCLAAWE),'PROMCLAS','CDCLAAWE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDPATSTD),'PROMCLAS','CDPATSTD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDTIPRAG),'PROMCLAS','CDTIPRAG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDCAMRAG),'PROMCLAS','CDCAMRAG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDRIFDEF),'PROMCLAS','CDRIFDEF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDRIFTAB),'PROMCLAS','CDRIFTAB');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDMODALL),'PROMCLAS','CDMODALL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDTIPALL),'PROMCLAS','CDTIPALL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDCLAALL),'PROMCLAS','CDCLAALL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDCOMTIP),'PROMCLAS','CDCOMTIP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDCOMCLA),'PROMCLAS','CDCOMCLA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDFOLDER),'PROMCLAS','CDFOLDER');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDFIRDIG),'PROMCLAS','CDFIRDIG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDCONSOS),'PROMCLAS','CDCONSOS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDCLASOS),'PROMCLAS','CDCLASOS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDAVVSOS),'PROMCLAS','CDAVVSOS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDKEYUPD),'PROMCLAS','CDKEYUPD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDGETDES),'PROMCLAS','CDGETDES');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDSERVER),'PROMCLAS','CDSERVER');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CD__PORT),'PROMCLAS','CD__PORT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CD__USER),'PROMCLAS','CD__USER');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDPASSWD),'PROMCLAS','CDPASSWD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDARCHIVE),'PROMCLAS','CDARCHIVE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDFLCTRL),'PROMCLAS','CDFLCTRL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDCLAPRA),'PROMCLAS','CDCLAPRA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDALIASF),'PROMCLAS','CDALIASF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDFLGDES),'PROMCLAS','CDFLGDES');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDNOMFIL),'PROMCLAS','CDNOMFIL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDTIPOBC),'PROMCLAS','CDTIPOBC');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDPUBWEB),'PROMCLAS','CDPUBWEB');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDERASEF),'PROMCLAS','CDERASEF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CDREPOBC),'PROMCLAS','CDREPOBC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CDCODCLA',this.oParentObject.w_CLCODICE,'CDDESCLA',this.oParentObject.w_CLDESCRI,'CDTIPARC',this.oParentObject.w_CDTIPARC,'CDMOTARC',this.oParentObject.w_CDMOTARC,'CDCLAAWE',this.oParentObject.w_CDCLAAWE,'CDPATSTD',this.oParentObject.w_CDPATSTD,'CDTIPRAG',this.oParentObject.w_CDTIPRAG,'CDCAMRAG',this.oParentObject.w_CDCAMRAG,'CDRIFDEF',this.oParentObject.w_CDRIFDEF,'CDRIFTAB',this.oParentObject.w_CDRIFTAB,'CDMODALL',this.oParentObject.w_CDMODALL,'CDTIPALL',this.oParentObject.w_CDTIPALL)
      insert into (i_cTable) (CDCODCLA,CDDESCLA,CDTIPARC,CDMOTARC,CDCLAAWE,CDPATSTD,CDTIPRAG,CDCAMRAG,CDRIFDEF,CDRIFTAB,CDMODALL,CDTIPALL,CDCLAALL,CDCOMTIP,CDCOMCLA,CDFOLDER,CDFIRDIG,CDCONSOS,CDCLASOS,CDAVVSOS,CDKEYUPD,CDGETDES,CDSERVER,CD__PORT,CD__USER,CDPASSWD,CDARCHIVE,CDFLCTRL,CDCLAPRA,CDALIASF,CDFLGDES,CDNOMFIL,CDTIPOBC,CDPUBWEB,CDERASEF,CDREPOBC &i_ccchkf. );
         values (;
           this.oParentObject.w_CLCODICE;
           ,this.oParentObject.w_CLDESCRI;
           ,this.oParentObject.w_CDTIPARC;
           ,this.oParentObject.w_CDMOTARC;
           ,this.oParentObject.w_CDCLAAWE;
           ,this.oParentObject.w_CDPATSTD;
           ,this.oParentObject.w_CDTIPRAG;
           ,this.oParentObject.w_CDCAMRAG;
           ,this.oParentObject.w_CDRIFDEF;
           ,this.oParentObject.w_CDRIFTAB;
           ,this.oParentObject.w_CDMODALL;
           ,this.oParentObject.w_CDTIPALL;
           ,this.oParentObject.w_CDCLAALL;
           ,this.oParentObject.w_CDCOMTIP;
           ,this.oParentObject.w_CDCOMCLA;
           ,this.oParentObject.w_CDFOLDER;
           ,this.oParentObject.w_CDFIRDIG;
           ,this.oParentObject.w_CDCONSOS;
           ,this.oParentObject.w_CDCLASOS;
           ,this.oParentObject.w_CDAVVSOS;
           ,this.oParentObject.w_CDKEYUPD;
           ,this.oParentObject.w_CDGETDES;
           ,this.oParentObject.w_CDSERVER;
           ,this.oParentObject.w_CD__PORT;
           ,this.oParentObject.w_CD__USER;
           ,this.oParentObject.w_CDPASSWD;
           ,this.oParentObject.w_CDARCHIVE;
           ,this.oParentObject.w_CDFLCTRL;
           ,this.oParentObject.w_CDCLAPRA;
           ,this.oParentObject.w_CDALIASF;
           ,this.oParentObject.w_CDFLGDES;
           ,this.oParentObject.w_CDNOMFIL;
           ,this.oParentObject.w_CDTIPOBC;
           ,this.oParentObject.w_CDPUBWEB;
           ,this.oParentObject.w_CDERASEF;
           ,this.oParentObject.w_CDREPOBC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  proc Init(oParentObject,w_TIPINSERT)
    this.w_TIPINSERT=w_TIPINSERT
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PRODCLAS'
    this.cWorkTables[2]='PROMCLAS'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_TIPINSERT"
endproc
