* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bkp                                                        *
*              Primanota controlli finali 1                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_210]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2016-12-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOri,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bkp",oParentObject,m.pOri,m.pOper)
return(i_retval)

define class tgscg_bkp as StdBatch
  * --- Local variables
  pOri = space(1)
  pOper = space(10)
  w_PNCODESE = space(4)
  w_PNSERIAL = space(10)
  w_PTDATSCA = ctod("  /  /  ")
  w_PTDATAPE = ctod("  /  /  ")
  w_PTNUMPAR = space(31)
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PTCODVAL = space(3)
  w_PT_SEGNO = space(1)
  w_PTTOTIMP = 0
  w_PTFLCRSA = space(1)
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_OK = .f.
  w_APPO = 0
  w_CFUNC = space(10)
  w_MESS = space(100)
  w_PTIPCLF = space(1)
  w_PCODCLF = space(15)
  w_PROWORD = 0
  w_SERIAL = space(10)
  w_FLINSO = space(1)
  w_RIFCES = space(1)
  w_OKINC = .f.
  w_ROWNUM = 0
  w_TEST = space(1)
  w_FLCRSA = space(1)
  w_CLADOC = space(2)
  w_SERACC = space(10)
  * --- WorkFile variables
  PAR_TITE_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali in Eliminazione/Modifica Partite (da GSCG_MPA e GSTE_MPA) 
    this.w_OK = .T.
    this.w_CFUNC = this.pOper
    this.w_MESS = "Operazione abbandonata"
    if Vartype(g_SCHEDULER)="C" and g_SCHEDULER="S"
      * --- Azzero g_MSG per eliminare parte del log non riferita ad un errore
       
 g_MSG=""
    endif
    if this.pORI="P"
      this.w_PNCODESE = this.oParentObject.oParentObject.w_PNCODESE
      this.w_PNSERIAL = this.oParentObject.oParentObject.w_PNSERIAL
      this.w_PTIPCLF = this.oParentObject.oParentObject.w_PNTIPCLF
      this.w_PCODCLF = this.oParentObject.oParentObject.w_PNCODCLF
      this.w_FLINSO = this.oParentObject.oParentObject.w_FLINSO
      this.w_RIFCES = this.oParentObject.oParentObject.w_PNRIFCES
    else
      this.w_PNCODESE = g_codese
      this.w_PNSERIAL = this.oParentObject.oParentObject.w_SCCODICE
      this.w_PTIPCLF = this.oParentObject.oParentObject.w_SCTIPCLF
      this.w_PCODCLF = this.oParentObject.oParentObject.w_SCCODCLF
      this.w_FLINSO = "N"
      this.w_RIFCES = "N"
    endif
    this.w_PROWORD = this.oParentObject.w_PTROWORD
    * --- Verifica eventual esistenza di partite a Saldo
    SELECT (this.oParentObject.cTrsName)
    this.w_PTDATSCA = CP_TODATE(t_DATSCA)
    this.w_PTDATAPE = CP_TODATE(t_PTDATAPE)
    this.w_PTNUMPAR = NVL(t_PTNUMPAR,"")
    this.w_PTTIPCON = NVL(t_PTTIPCON,"")
    this.w_PTCODCON = NVL(t_PTCODCON,"")
    this.w_PTCODVAL = NVL(t_PTCODVAL,"")
    if this.pORI="P"
      this.w_PTFLCRSA = NVL(t_PTFLCRSA," ")
    else
      this.w_PTFLCRSA = iif(NVL(t_PTFLCRSA,0)=1,"C",iif(NVL(t_PTFLCRSA,0)=2,"S","A"))
    endif
    this.w_PT_SEGNO = NVL(t_PT_SEGNO," ")
    this.w_PTTOTIMP = IIF(this.w_CFUNC="Query", 0, ABS(NVL(t_PTTOTIMP, 0)))
    this.w_ROWNUM = NVL( CPROWNUM , 0)
    this.w_OKINC = .T.
    if this.w_CFUNC="Edit" AND this.w_FLINSO="S" AND this.w_RIFCES="C"
      this.w_OK = .F.
      this.w_MESS = Ah_MsgFormat("Impossibile modificare da primanota registrazioni di insoluti.%0Accedere alla gestione partite/scadenze.")
    endif
    if g_SOLL="S" and this.w_PTFLCRSA="S" and this.w_OK
      * --- Select from gsso_bik
      do vq_exec with 'gsso_bik',this,'_Curs_gsso_bik','',.f.,.t.
      if used('_Curs_gsso_bik')
        select _Curs_gsso_bik
        locate for 1=1
        do while not(eof())
        if this.w_CFUNC="Edit"
          this.w_OK = .F.
          this.w_MESS = Ah_MsgFormat("Esistono incassi di insoluti associati alla partita del: %1.%0Modificazione impossibile",DTOC(this.w_PTDATSCA))
        else
          if this.w_OKINC
            this.w_OK = ah_YesNo("Esistono incassi avvenuti di contenziosi associati alla partita del: %1.%0Confermi ugualmente?",,DTOC(this.w_PTDATSCA))
            if Not this.w_OK
              this.w_MESS = Ah_MsgFormat("Transazione abbandonata")
            endif
          endif
          this.w_OKINC = .F.
        endif
        exit
          select _Curs_gsso_bik
          continue
        enddo
        use
      endif
    endif
    if ((this.w_PTFLCRSA $ "C-A" AND NOT EMPTY(this.w_PTNUMPAR) AND NOT EMPTY(this.w_PTTIPCON) AND NOT EMPTY(this.w_PTCODCON) AND NOT EMPTY(this.w_PTCODVAL)) OR RIGHT(ALLTRIM(this.w_PTNUMPAR),1)="A") and this.w_OK
      ah_Msg("Verifica eventuali partite saldate...",.T.)
      if this.w_PTFLCRSA = "C" OR (this.w_PTFLCRSA = "A" AND Empty(Nvl(t_PTSERRIF," ")) AND this.pORI="S") OR RIGHT(ALLTRIM(this.w_PTNUMPAR),1)="A"
        * --- Dalle scadenze diverse � possibile avere acconti chiusi da Partite di Saldo
        this.w_TEST = ""
        * --- Ricerco eventuale partita che punta la partita in procinto
        *     di essere cancellata.
        *     Contemplato il caso di registrazione di prima nota con due righe intestatario
        *     una crea e l'altra salda.
        * --- Select from PAR_TITE
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select PTSERIAL,PTROWORD,CPROWNUM,PTFLCRSA  from "+i_cTable+" PAR_TITE ";
              +" where "+cp_ToStrODBC(this.w_PNSERIAL)+"=PTSERRIF AND "+cp_ToStrODBC(this.w_PROWORD)+"=PTORDRIF AND "+cp_ToStrODBC(this.w_ROWNUM)+"= PTNUMRIF AND PTFLCRSA IN ('S','A') ";
               ,"_Curs_PAR_TITE")
        else
          select PTSERIAL,PTROWORD,CPROWNUM,PTFLCRSA from (i_cTable);
           where this.w_PNSERIAL=PTSERRIF AND this.w_PROWORD=PTORDRIF AND this.w_ROWNUM= PTNUMRIF AND PTFLCRSA IN ("S","A") ;
            into cursor _Curs_PAR_TITE
        endif
        if used('_Curs_PAR_TITE')
          select _Curs_PAR_TITE
          locate for 1=1
          do while not(eof())
          this.w_TEST = Nvl(_Curs_PAR_TITE.PTFLCRSA," ")
          this.w_PTSERIAL = _Curs_PAR_TITE.PTSERIAL
          this.w_PTROWORD = _Curs_PAR_TITE.PTROWORD
          this.w_CPROWNUM = _Curs_PAR_TITE.CPROWNUM
            select _Curs_PAR_TITE
            continue
          enddo
          use
        endif
        do case
          case this.w_TEST="S" And ( this.w_PTSERIAL<>this.w_PNSERIAL Or this.w_PTROWORD<0 )
            * --- La partita di creazione � puntata da una di saldo, non posso cancellare
            this.w_OK = .F.
          case RIGHT(ALLTRIM(this.w_PTNUMPAR),1)="A"
            * --- Cancellazione di una partita di saldo di un acconto generico
            SELECT (this.oParentObject.cTrsName)
            this.w_PTSERIAL = NVL(t_PTSERRIF,Space(10))
            this.w_PTROWORD = NVL(t_PTORDRIF,0)
            this.w_CPROWNUM = NVL(t_PTNUMRIF,0)
            * --- Read from PAR_TITE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PTFLCRSA"+;
                " from "+i_cTable+" PAR_TITE where ";
                    +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
                    +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PTFLCRSA;
                from (i_cTable) where;
                    PTSERIAL = this.w_PTSERIAL;
                    and PTROWORD = this.w_PTROWORD;
                    and CPROWNUM = this.w_CPROWNUM;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_FLCRSA = NVL(cp_ToDate(_read_.PTFLCRSA),cp_NullValue(_read_.PTFLCRSA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Eseguo il ripristino del numero partita della partita di creazione 
            *     saldata dalla partita in fase di eliminazione solo se la partita puntata
            *     � effettivamente un acconto
            if this.w_FLCRSA="A"
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          case this.w_TEST="A" 
            * --- La partita di creazione � stata creata tramite la chiusura di un acconto
            *     da prima nota, devo pulire i riferimenti sulla partita di acconto alla creazione
            *     prima di cancellare la creazione..
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          otherwise
            * --- Caso esteso comprende vecchie e nuove partite...
            if this.pORI="P"
              vq_exec("QUERY\GSCG_QRP.VQR",this,"ASSEGN")
            else
              vq_exec("QUERY\GSTE_QRP.VQR",this,"ASSEGN")
            endif
            if USED("ASSEGN")
              SELECT ASSEGN
              if RECCOUNT("ASSEGN")>0
                this.w_APPO = 0
                SUM NVL(PTTOTIMP, 0) TO this.w_APPO
                if this.w_PTTOTIMP<ABS(this.w_APPO)
                  GO TOP
                  LOCATE FOR NVL(PTFLCRSA," ")="S" AND (this.w_PTDATAPE<CP_TODATE(PTDATAPE) OR (this.w_PTDATAPE=CP_TODATE(PTDATAPE) AND this.w_PNSERIAL< PTSERIAL))
                  if FOUND()
                    this.w_OK = .F.
                  else
                    if this.w_CFUNC="Query"
                      GO TOP
                      SCAN FOR NVL(PTFLCRSA," ")="A"
                      this.w_PTSERIAL = NVL(PTSERIAL, " ")
                      this.w_PTROWORD = NVL(PTROWORD, 0)
                      this.w_CPROWNUM = NVL(CPROWNUM, 0)
                      if NOT EMPTY(this.w_PTSERIAL) AND this.w_PTROWORD<>0 AND this.w_CPROWNUM<>0
                        this.w_CLADOC = ""
                        * --- Verifico che  l'acconto non sia specifico del documento e se il documento
                        *     che ha generato l'acconto � contabilizzabile o meno (classe documento
                        *     fattura o nota di credito)
                        * --- Read from DOC_MAST
                        i_nOldArea=select()
                        if used('_read_')
                          select _read_
                          use
                        endif
                        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
                        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
                        if i_nConn<>0
                          cp_sqlexec(i_nConn,"select "+;
                            "MVSERIAL,MVCLADOC"+;
                            " from "+i_cTable+" DOC_MAST where ";
                                +"MVRIFACC = "+cp_ToStrODBC(this.w_PTSERIAL);
                                 ,"_read_")
                          i_Rows=iif(used('_read_'),reccount(),0)
                        else
                          select;
                            MVSERIAL,MVCLADOC;
                            from (i_cTable) where;
                                MVRIFACC = this.w_PTSERIAL;
                             into cursor _read_
                          i_Rows=_tally
                        endif
                        if used('_read_')
                          locate for 1=1
                          this.w_SERIAL = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
                          this.w_CLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
                          use
                        else
                          * --- Error: sql sentence error.
                          i_Error = MSG_READ_ERROR
                          return
                        endif
                        select (i_nOldArea)
                        * --- Elimino l'abbinamento della Partita di Acconto al Documento che si vuole Cancellare
                        *     se il documento non � contabilizzabile (Fattura e Note Credito)
                        if not this.w_CLADOC $ "FA-NC"
                          this.Page_2()
                          if i_retcode='stop' or !empty(i_Error)
                            return
                          endif
                        endif
                      endif
                      EndScan
                    endif
                  endif
                endif
              endif
              USE
            endif
        endcase
      else
        * --- Se � un acconto chiuso da prima nota allora devo verificare se
        *     la partita stessa punta alla sua partita di saldo (che per costruzione
        *     � inserita di creazione)
        SELECT (this.oParentObject.cTrsName)
        this.w_SERACC = NVL(t_PTSERRIF," ")
        if Not Empty( this.w_SERACC )
          this.w_OK = .F.
        endif
      endif
      if Not this.w_OK 
        if this.w_CFUNC="Query"
          this.w_MESS = Ah_MsgFormat("Esistono partite di chiusura associate al documento ; (Data: %1).%0Cancellazione impossibile.",DTOC(this.w_PTDATSCA))
        else
          this.w_MESS = Ah_MsgFormat("Esistono partite di chiusura associate al documento ; (Data: %1).%0Variazione impossibile.",DTOC(this.w_PTDATSCA))
        endif
      endif
    endif
    SELECT (this.oParentObject.cTrsName)
    this.bUpdateParentObject = .F.
    if Not this.w_OK 
      * --- Abbandona la Transazione
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_CFUNC="Query"
      this.w_PTNUMPAR = this.w_PNCODESE+"/Acconto"
      * --- Write into PAR_TITE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PAR_TITE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PTNUMPAR ="+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
        +",PTCODAGE ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'PAR_TITE','PTCODAGE');
        +",PTSERRIF ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTSERRIF');
        +",PTORDRIF ="+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTORDRIF');
        +",PTNUMRIF ="+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTNUMRIF');
            +i_ccchkf ;
        +" where ";
            +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
            +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            PTNUMPAR = this.w_PTNUMPAR;
            ,PTCODAGE = SPACE(5);
            ,PTSERRIF = SPACE(10);
            ,PTORDRIF = 0;
            ,PTNUMRIF = 0;
            &i_ccchkf. ;
         where;
            PTSERIAL = this.w_PTSERIAL;
            and PTROWORD = this.w_PTROWORD;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOri,pOper)
    this.pOri=pOri
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PAR_TITE'
    this.cWorkTables[2]='DOC_MAST'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_gsso_bik')
      use in _Curs_gsso_bik
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOri,pOper"
endproc
