* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_biv                                                        *
*              Controllo in cancellazione                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-11-03                                                      *
* Last revis.: 2014-11-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_biv",oParentObject)
return(i_retval)

define class tgsar_biv as StdBatch
  * --- Local variables
  w_ERR = .f.
  * --- WorkFile variables
  DOC_MAST_idx=0
  DOC_DETT_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Da GSAR_AIV: inibisce la cancellazione se il codice iva  � presente nel campo MVCODIVE in testata del documento e MVCODIVA nei documenti di riga.
    * --- Select from DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_MAST ";
          +" where MVCODIVE="+cp_ToStrODBC(this.oParentObject.w_IVCODIVA)+"";
           ,"_Curs_DOC_MAST")
    else
      select * from (i_cTable);
       where MVCODIVE=this.oParentObject.w_IVCODIVA;
        into cursor _Curs_DOC_MAST
    endif
    if used('_Curs_DOC_MAST')
      select _Curs_DOC_MAST
      locate for 1=1
      do while not(eof())
      this.w_ERR = .T.
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_MsgFormat("Il codice che si intende cancellare � presente nella testata dei documenti%0Impossibile cancellare")
      i_retcode = 'stop'
      return
        select _Curs_DOC_MAST
        continue
      enddo
      use
    endif
    if !this.w_ERR
      * --- Select from DOC_DETT
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_DETT ";
            +" where MVCODIVA="+cp_ToStrODBC(this.oParentObject.w_IVCODIVA)+"";
             ,"_Curs_DOC_DETT")
      else
        select * from (i_cTable);
         where MVCODIVA=this.oParentObject.w_IVCODIVA;
          into cursor _Curs_DOC_DETT
      endif
      if used('_Curs_DOC_DETT')
        select _Curs_DOC_DETT
        locate for 1=1
        do while not(eof())
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=ah_MsgFormat("Il codice che si intende cancellare � presente nelle righe dei documenti%0Impossibile cancellare")
        i_retcode = 'stop'
        return
          select _Curs_DOC_DETT
          continue
        enddo
        use
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='DOC_DETT'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_DOC_MAST')
      use in _Curs_DOC_MAST
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
