* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bll                                                        *
*              Gestione log servizio SMTP                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_8]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-06-18                                                      *
* Last revis.: 2003-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bll",oParentObject)
return(i_retval)

define class tgsut_bll as StdBatch
  * --- Local variables
  w_PATH = space(250)
  w_FILELT = space(250)
  w_FILEHANDLE = 0
  w_SIZE = 0
  w_CONTENT = space(0)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PATH = GETENV("TEMP")
    this.w_FILELT = ALLTRIM(this.w_PATH+"\logtot.smtp")
    this.w_FILEHANDLE = FOPEN(this.w_FILELT, 0)
    this.w_SIZE = FSEEK(this.w_FILEHANDLE,0,2)
    FSEEK(this.w_FILEHANDLE,0,0)
    this.oParentObject.w_MLOG = FREAD(this.w_FILEHANDLE, this.w_SIZE)
    FCLOSE(this.w_FILEHANDLE)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
