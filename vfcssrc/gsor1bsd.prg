* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor1bsd                                                        *
*              Elab. cursore ordini                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-28                                                      *
* Last revis.: 2012-03-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CURSOR,w_MultiReportVarEnv,w_OTES,w_TIPDOC,w_CARMEM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsor1bsd",oParentObject,m.w_CURSOR,m.w_MultiReportVarEnv,m.w_OTES,m.w_TIPDOC,m.w_CARMEM)
return(i_retval)

define class tgsor1bsd as StdBatch
  * --- Local variables
  w_CURSOR = space(254)
  w_MultiReportVarEnv = .NULL.
  w_OTES = .f.
  w_TIPDOC = space(5)
  w_CARMEM = 0
  w_TIPCON = space(1)
  w_CODICE1 = space(20)
  w_CODCON = space(15)
  w_CODART = space(20)
  w_OLDSER = space(10)
  w_APPO = 0
  w_APPO1 = 0
  w_AZZERA = space(1)
  w_KEYOBS = ctod("  /  /  ")
  w_DESART1 = space(40)
  w_DESSUP1 = space(0)
  w_MVCODICE = space(20)
  w_LINGUA = space(3)
  w_LENSCF = 0
  w_MVDESART = space(40)
  w_KEYDESART = space(40)
  w_IMPSCO1 = 0
  w_IMPSCO2 = 0
  w_TOTSCO = 0
  * --- WorkFile variables
  TRADARTI_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione cursore di stampa documenti - Ordini
    * --- Se ristampa ordini esiste w_TIPOVAO
    if TYPE("This.oParentObject.w_TIPOVAO")<>"U" 
      * --- Aggiungo colonna COPIA
      SELECT *, " " AS COPIA FROM (this.w_CURSOR) INTO CURSOR (this.w_CURSOR) READWRITE
      if USED(this.w_CURSOR)
        this.w_AZZERA = " "
        this.w_OLDSER = "ZXZXZXZXZX"
        this.w_TOTSCO = 0
        SELECT(this.w_CURSOR)
        GO TOP
        SCAN
        if MVSERIAL<>this.w_OLDSER
          if this.w_AZZERA="S"
            this.w_APPO = RECNO()
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            GOTO this.w_APPO
          endif
          this.w_AZZERA = " "
          this.w_TOTSCO = 0
          this.w_OLDSER = MVSERIAL
        endif
        do case
          case NOT EMPTY(this.oParentObject.w_SERIE1) AND NVL(MVALFDOC,"  ") < this.oParentObject.w_SERIE1
            DELETE
          case NOT EMPTY(this.oParentObject.w_SERIE2) AND NVL(MVALFDOC,"  ") > this.oParentObject.w_SERIE2
            DELETE
          case this.oParentObject.w_FLEVAS=" " AND (MVFLEVAS="S" OR (MVQTAMOV<=MVQTAEVA AND Not MVTIPRIG $ "DF" ) OR (MVPREZZO=MVIMPEVA And MVPREZZO <>0 AND MVTIPRIG="F"))
            * --- Elimina Righe Totalmente Evase
            DELETE
            this.w_AZZERA = "S"
          otherwise
            if NVL(ANFLCODI, " ")="S" AND MVTIPRIG<>"D"
              * --- Controllo se il cliente/fornitore imputato in testata del documento
              * --- ha il flag di codifica settato.Il controllo viene effettuato su un campo
              * --- che si trova nell'anagrafica Clienti/Fornitori (ANFLCODI)
              this.w_CODART = NVL(MVCODART,"")
              this.w_MVCODICE = NVL(MVCODICE," ")
              this.w_CODCON = MVCODCON
              this.w_MVDESART = NVL(MVDESART, SPACE(40) ) 
              * --- Read from KEY_ARTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CADESART"+;
                  " from "+i_cTable+" KEY_ARTI where ";
                      +"CACODICE = "+cp_ToStrODBC(this.w_MVCODICE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CADESART;
                  from (i_cTable) where;
                      CACODICE = this.w_MVCODICE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_KEYDESART = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_TIPCON = MVTIPCON
              this.w_KEYOBS = CP_TODATE(MVDATREG)
              this.w_CODICE1 = SPACE(20)
              this.w_LENSCF = 0
              * --- Select from KEY_ARTI
              i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select CACODICE,CADTOBSO,CALENSCF,CADESART  from "+i_cTable+" KEY_ARTI ";
                    +" where CACODART= "+cp_ToStrODBC(this.w_CODART)+" AND CACODCON= "+cp_ToStrODBC(this.w_CODCON)+" AND CATIPCON= "+cp_ToStrODBC(this.w_TIPCON)+" AND CACODICE<> "+cp_ToStrODBC(this.w_CODART)+" ";
                    +" order by CATIPBAR DESC,CALENSCF, CACODICE";
                     ,"_Curs_KEY_ARTI")
              else
                select CACODICE,CADTOBSO,CALENSCF,CADESART from (i_cTable);
                 where CACODART= this.w_CODART AND CACODCON= this.w_CODCON AND CATIPCON= this.w_TIPCON AND CACODICE<> this.w_CODART ;
                 order by CATIPBAR DESC,CALENSCF, CACODICE;
                  into cursor _Curs_KEY_ARTI
              endif
              if used('_Curs_KEY_ARTI')
                select _Curs_KEY_ARTI
                locate for 1=1
                do while not(eof())
                if (CP_TODATE(_Curs_KEY_ARTI.CADTOBSO) > this.w_KEYOBS) OR EMPTY(NVL(CP_TODATE(_Curs_KEY_ARTI.CADTOBSO)," "))
                  this.w_CODICE1 = _Curs_KEY_ARTI.CACODICE 
                  this.w_LENSCF = Nvl(_Curs_KEY_ARTI.CALENSCF,0)
                  if this.w_MVDESART=this.w_KEYDESART
                    this.w_DESART1 = NVL(_Curs_KEY_ARTI.CADESART,space(40))
                  else
                    * --- privilegia la descrizione modificata manualmente 
                    this.w_DESART1 = this.w_MVDESART
                  endif
                  if this.w_CODICE1= this.w_MVCODICE
                    * --- Nel caso sia stato trovato come codice di ricerca valido lo stesso usato sul documento
                    *     esce dal ciclo e mantiene questo.
                    EXIT
                  endif
                endif
                  select _Curs_KEY_ARTI
                  continue
                enddo
                use
              endif
              Select(this.w_CURSOR)
              this.w_LINGUA = NVL(ANCODLIN,"   ")
              if NOT EMPTY(NVL(this.w_CODICE1,""))
                 
 DECLARE ARRRIS (2) 
 Tradlin(this.w_CODICE1,this.w_LINGUA,"TRADARTI",@ARRRIS,"B") 
 this.w_DESART1=IIF(NOT EMPTY(ARRRIS(1)), ARRRIS(1), this.w_DESART1) 
 this.w_DESSUP1=ARRRIS(2)
                SELECT (this.w_CURSOR)
                if NOT EMPTY(NVL(this.w_DESART1," "))
                  REPLACE MVDESART WITH this.w_DESART1
                endif
                if NOT EMPTY(NVL(this.w_DESSUP1," "))
                  REPLACE MVDESSUP WITH this.w_DESSUP1
                endif
                * --- Se Attivo Flag Codifica Cliente/Fornitore Esclusiva in Azienda
                *     Prendo solo la prima parte del codice di ricerca (elimino il suffisso da destra)
                if g_FLCESC = "S" And this.w_LENSCF <> 0
                  this.w_CODICE1 = Left(Alltrim(this.w_CODICE1),Len(Alltrim(this.w_CODICE1))-this.w_LENSCF)
                endif
                REPLACE mvcodice WITH this.w_CODICE1
              endif
            endif
            if this.oParentObject.w_FLEVAS=" "
              do case
                case MVTIPRIG="F" 
                  if NVL(MVPREZZO, 0)<>0 AND NVL(MVIMPEVA, 0)<>0
                    this.w_APPO = NVL(MVPREZZO, 0)
                    this.w_APPO1 = this.w_APPO - NVL(MVIMPEVA, 0)
                    this.w_IMPSCO1 = NVL(MVIMPSCO,0)/this.w_APPO * this.w_APPO1
                    REPLACE MVPREZZO WITH this.w_APPO1
                    this.w_APPO1 = cp_ROUND((NVL(MVVALRIG,0) * this.w_APPO1) / this.w_APPO, NVL(VADECUNI, 0))
                    REPLACE MVVALRIG WITH this.w_APPO1
                    * --- riproporziono gli sconti in base all'evasione della riga
                    REPLACE MVIMPSCO WITH this.w_IMPSCO1
                    this.w_AZZERA = "S"
                  endif
                case MVTIPRIG<>"D"
                  if NVL(MVQTAMOV, 0)<>0 AND NVL(MVQTAEVA, 0)<>0
                    this.w_APPO = NVL(MVQTAMOV, 0)
                    this.w_APPO1 = this.w_APPO - NVL(MVQTAEVA, 0)
                    this.w_IMPSCO2 = NVL(MVIMPSCO,0)/this.w_APPO * this.w_APPO1
                    REPLACE MVQTAMOV WITH this.w_APPO1
                    this.w_APPO1 = cp_ROUND((NVL(MVVALRIG,0) * this.w_APPO1) / this.w_APPO, NVL(VADECUNI, 0))
                    REPLACE MVVALRIG WITH this.w_APPO1
                    REPLACE MVIMPSCO WITH this.w_IMPSCO2
                    this.w_AZZERA = "S"
                  endif
              endcase
            endif
            this.w_TOTSCO = this.w_TOTSCO + NVL(MVIMPSCO,0)
        endcase
        SELECT(this.w_CURSOR)
        ENDSCAN
      endif
      if this.w_AZZERA="S"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Segna l'ultima riga di ogni documento
      this.w_OLDSER = "ZXZXZXZXZX"
      SELECT(this.w_CURSOR)
      GO BOTTOM
      do while NOT BOF()
        if NOT DELETED()
          if MVSERIAL<>this.w_OLDSER
            REPLACE ULTRIG WITH "S"
            this.w_OLDSER = MVSERIAL
          endif
        endif
        SKIP -1
      enddo
      SELECT(this.w_CURSOR)
      GO TOP
      * --- Passo queste variabili al report
      this.w_MultiReportVarEnv.AddVariable("L_ARTESE", "")     
      this.w_MultiReportVarEnv.AddVariable("L_DESOPE", "")     
      this.w_MultiReportVarEnv.AddVariable("L_DATINI", cp_CharToDate("  -  -  "))     
      this.w_MultiReportVarEnv.AddVariable("L_DATFIN", cp_CharToDate("  -  -  "))     
      this.w_MultiReportVarEnv.AddVariable("L_NDIC", 0)     
      this.w_MultiReportVarEnv.AddVariable("L_ADIC", "")     
      this.w_MultiReportVarEnv.AddVariable("L_DDIC", cp_CharToDate("  -  -  "))     
      this.w_MultiReportVarEnv.AddVariable("L_TIPOPER", "")     
      this.w_MultiReportVarEnv.AddVariable("L_IMPONI", 0)     
      this.w_MultiReportVarEnv.AddVariable("L_FLINCA", "")     
      this.w_MultiReportVarEnv.AddVariable("L_LOGO", GETBMP(I_CODAZI))     
      if this.w_OTES
        * --- Lancio la funzione per divisione su pi� righe del campo memo della
        *     descrizione supplementare articolo solo se � stato definito il numero
        *     di caratteri per riga nell'output utente
        if this.w_CARMEM<>0
          * --- Lancio la funzione per la divisione del campo memo su pi� righe
          DIVIMEMO(this.w_CARMEM, this.w_CURSOR)
        endif
      endif
    else
      * --- Non � ristampa ordini richiamo il batch standard
      GSVE1BSD(This.oParentObject, this.w_CURSOR, this.w_MultiReportVarEnv, this.w_OTES, this.w_TIPDOC, this.w_CARMEM)
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    SELECT(this.w_CURSOR)
    GO TOP
    UPDATE (this.w_CURSOR) SET ;
    MVAIMPS1=0, MVAIMPS2=0, MVAIMPS3=0, MVAIMPS4=0, MVAIMPS5=0, MVAIMPS6=0, ;
    MVAIMPN1=0, MVAIMPN2=0, MVAIMPN3=0, MVAIMPN4=0, MVAIMPN5=0, MVAIMPN6=0, COPIA="S", MVSCONTI=this.w_TOTSCO;
    WHERE MVSERIAL=this.w_OLDSER
  endproc


  proc Init(oParentObject,w_CURSOR,w_MultiReportVarEnv,w_OTES,w_TIPDOC,w_CARMEM)
    this.w_CURSOR=w_CURSOR
    this.w_MultiReportVarEnv=w_MultiReportVarEnv
    this.w_OTES=w_OTES
    this.w_TIPDOC=w_TIPDOC
    this.w_CARMEM=w_CARMEM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='TRADARTI'
    this.cWorkTables[2]='KEY_ARTI'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_KEY_ARTI')
      use in _Curs_KEY_ARTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CURSOR,w_MultiReportVarEnv,w_OTES,w_TIPDOC,w_CARMEM"
endproc
