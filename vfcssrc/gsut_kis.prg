* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kis                                                        *
*              Sincronizzazione indici documenti pratiche                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-27                                                      *
* Last revis.: 2011-03-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kis",oParentObject))

* --- Class definition
define class tgsut_kis as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 898
  Height = 460+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-11"
  HelpContextID=266945385
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  cPrg = "gsut_kis"
  cComment = "Sincronizzazione indici documenti pratiche"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPOPATH = space(1)
  w_PRAAPE = space(15)
  w_TIPODESC = space(1)
  w_DESINDICE = space(220)
  w_FLAPE = space(1)
  w_DESCAN = space(100)
  w_SHOWLOG = .F.
  w_FILEEXIST = space(1)
  w_CONFARCH = space(1)
  w_MSG = space(0)
  w_ZOOMPATH = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kisPag1","gsut_kis",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Sincronizzazione")
      .Pages(2).addobject("oPag","tgsut_kisPag2","gsut_kis",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Log sincronizzazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOPATH_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMPATH = this.oPgFrm.Pages(1).oPag.ZOOMPATH
    DoDefault()
    proc Destroy()
      this.w_ZOOMPATH = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CAN_TIER'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSUT_BIS(this,"S")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOPATH=space(1)
      .w_PRAAPE=space(15)
      .w_TIPODESC=space(1)
      .w_DESINDICE=space(220)
      .w_FLAPE=space(1)
      .w_DESCAN=space(100)
      .w_SHOWLOG=.f.
      .w_FILEEXIST=space(1)
      .w_CONFARCH=space(1)
      .w_MSG=space(0)
      .oPgFrm.Page1.oPag.ZOOMPATH.Calculate()
        .w_TIPOPATH = 'P'
          .DoRTCalc(2,2,.f.)
        .w_TIPODESC = 'N'
          .DoRTCalc(4,4,.f.)
        .w_FLAPE = 'A'
          .DoRTCalc(6,6,.f.)
        .w_SHOWLOG = .F.
        .w_FILEEXIST = 'N'
        .w_CONFARCH = 'N'
    endwith
    this.DoRTCalc(10,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMPATH.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMPATH.Calculate()
    endwith
  return

  proc Calculate_AJQHQMRHBQ()
    with this
          * --- Ricerca
          GSUT_BIS(this;
              ,'R';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPRAAPE_1_3.visible=!this.oPgFrm.Page1.oPag.oPRAAPE_1_3.mHide()
    this.oPgFrm.Page1.oPag.oDESINDICE_1_5.visible=!this.oPgFrm.Page1.oPag.oDESINDICE_1_5.mHide()
    this.oPgFrm.Page1.oPag.oFLAPE_1_6.visible=!this.oPgFrm.Page1.oPag.oFLAPE_1_6.mHide()
    this.oPgFrm.Page1.oPag.oDESCAN_1_7.visible=!this.oPgFrm.Page1.oPag.oDESCAN_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_17.visible=!this.oPgFrm.Page1.oPag.oStr_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMPATH.Event(cEvent)
        if lower(cEvent)==lower("Init") or lower(cEvent)==lower("Ricerca")
          .Calculate_AJQHQMRHBQ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOPATH_1_2.RadioValue()==this.w_TIPOPATH)
      this.oPgFrm.Page1.oPag.oTIPOPATH_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRAAPE_1_3.value==this.w_PRAAPE)
      this.oPgFrm.Page1.oPag.oPRAAPE_1_3.value=this.w_PRAAPE
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPODESC_1_4.RadioValue()==this.w_TIPODESC)
      this.oPgFrm.Page1.oPag.oTIPODESC_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINDICE_1_5.value==this.w_DESINDICE)
      this.oPgFrm.Page1.oPag.oDESINDICE_1_5.value=this.w_DESINDICE
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAPE_1_6.RadioValue()==this.w_FLAPE)
      this.oPgFrm.Page1.oPag.oFLAPE_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_7.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_7.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oFILEEXIST_1_10.RadioValue()==this.w_FILEEXIST)
      this.oPgFrm.Page1.oPag.oFILEEXIST_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONFARCH_1_11.RadioValue()==this.w_CONFARCH)
      this.oPgFrm.Page1.oPag.oCONFARCH_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oMSG_2_1.value==this.w_MSG)
      this.oPgFrm.Page2.oPag.oMSG_2_1.value=this.w_MSG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DESINDICE))  and not(.w_TIPODESC<>'M')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDESINDICE_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DESINDICE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kisPag1 as StdContainer
  Width  = 894
  height = 460
  stdWidth  = 894
  stdheight = 460
  resizeXpos=483
  resizeYpos=219
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMPATH as cp_szoombox with uid="UGLSXXRRBC",left=13, top=84, width=871,height=315,;
    caption='ZOOMPATH',;
   bGlobalFont=.t.,;
    cTable="PASINCIN",cZoomFile="GSUT_KIS",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.f.,;
    cEvent = "RefreshZoom",;
    nPag=1;
    , HelpContextID = 107545054


  add object oTIPOPATH_1_2 as StdCombo with uid="MTDXPDYKGJ",rtseq=1,rtrep=.f.,left=78,top=12,width=179,height=21;
    , ToolTipText = "Selezionare il percorso di ricerca";
    , HelpContextID = 107678590;
    , cFormVar="w_TIPOPATH",RowSource=""+"Da tabella parametri,"+"Inseriti nelle pratiche", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOPATH_1_2.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oTIPOPATH_1_2.GetRadio()
    this.Parent.oContained.w_TIPOPATH = this.RadioValue()
    return .t.
  endfunc

  func oTIPOPATH_1_2.SetRadio()
    this.Parent.oContained.w_TIPOPATH=trim(this.Parent.oContained.w_TIPOPATH)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOPATH=='P',1,;
      iif(this.Parent.oContained.w_TIPOPATH=='C',2,;
      0))
  endfunc

  add object oPRAAPE_1_3 as AH_SEARCHFLD with uid="LEFUSAGYNV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PRAAPE", cQueryName = "PRAAPE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice o numero della pratica o parte di esso (ricerca contestuale)",;
    HelpContextID = 94624778,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=338, Top=12, InputMask=replicate('X',15)

  func oPRAAPE_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPOPATH='P')
    endwith
  endfunc


  add object oTIPODESC_1_4 as StdCombo with uid="BKDAMYSEQG",rtseq=3,rtrep=.f.,left=78,top=40,width=179,height=21;
    , ToolTipText = "Tipo descrizione indice documento";
    , HelpContextID = 162204537;
    , cFormVar="w_TIPODESC",RowSource=""+"Nome documento,"+"Titolo/Nome documento,"+"Manuale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPODESC_1_4.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'T',;
    iif(this.value =3,'M',;
    space(1)))))
  endfunc
  func oTIPODESC_1_4.GetRadio()
    this.Parent.oContained.w_TIPODESC = this.RadioValue()
    return .t.
  endfunc

  func oTIPODESC_1_4.SetRadio()
    this.Parent.oContained.w_TIPODESC=trim(this.Parent.oContained.w_TIPODESC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPODESC=='N',1,;
      iif(this.Parent.oContained.w_TIPODESC=='T',2,;
      iif(this.Parent.oContained.w_TIPODESC=='M',3,;
      0)))
  endfunc

  add object oDESINDICE_1_5 as StdField with uid="LLCLXAAOGA",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESINDICE", cQueryName = "DESINDICE",;
    bObbl = .t. , nPag = 1, value=space(220), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione indice creato dalla sincronizzazione",;
    HelpContextID = 155531977,;
   bGlobalFont=.t.,;
    Height=21, Width=421, Left=338, Top=40, InputMask=replicate('X',220)

  func oDESINDICE_1_5.mHide()
    with this.Parent.oContained
      return (.w_TIPODESC<>'M')
    endwith
  endfunc

  add object oFLAPE_1_6 as StdRadio with uid="MLIGHJJTAM",rtseq=5,rtrep=.f.,left=766, top=14, width=77,height=51;
    , ToolTipText = "Selezione su apertura/chiusura";
    , cFormVar="w_FLAPE", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oFLAPE_1_6.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Aperte"
      this.Buttons(1).HelpContextID = 189063850
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Chiuse"
      this.Buttons(2).HelpContextID = 189063850
      this.Buttons(2).Top=16
      this.Buttons(3).Caption="Tutte"
      this.Buttons(3).HelpContextID = 189063850
      this.Buttons(3).Top=32
      this.SetAll("Width",75)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Selezione su apertura/chiusura")
      StdRadio::init()
    endproc

  func oFLAPE_1_6.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oFLAPE_1_6.GetRadio()
    this.Parent.oContained.w_FLAPE = this.RadioValue()
    return .t.
  endfunc

  func oFLAPE_1_6.SetRadio()
    this.Parent.oContained.w_FLAPE=trim(this.Parent.oContained.w_FLAPE)
    this.value = ;
      iif(this.Parent.oContained.w_FLAPE=='A',1,;
      iif(this.Parent.oContained.w_FLAPE=='C',2,;
      iif(this.Parent.oContained.w_FLAPE=='T',3,;
      0)))
  endfunc

  func oFLAPE_1_6.mHide()
    with this.Parent.oContained
      return (.w_TIPOPATH='P')
    endwith
  endfunc

  add object oDESCAN_1_7 as AH_SEARCHFLD with uid="CBKKYKLBHC",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Nome pratica o parte di esso (ricerca contestuale)",;
    HelpContextID = 227592650,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=523, Top=12, InputMask=replicate('X',100)

  func oDESCAN_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPOPATH='P')
    endwith
  endfunc


  add object oBtn_1_9 as StdButton with uid="VAJGJMORRC",left=839, top=14, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la sincronizzazione";
    , HelpContextID = 125813994;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      this.parent.oContained.NotifyEvent("Ricerca")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFILEEXIST_1_10 as StdCheck with uid="JCEMGNRUSO",rtseq=8,rtrep=.f.,left=16, top=409, caption="Controlla esistenza file archiviati",;
    ToolTipText = "Se attivo controlla l'esistenza dei documenti archiviati precedentemente",;
    HelpContextID = 55521303,;
    cFormVar="w_FILEEXIST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFILEEXIST_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFILEEXIST_1_10.GetRadio()
    this.Parent.oContained.w_FILEEXIST = this.RadioValue()
    return .t.
  endfunc

  func oFILEEXIST_1_10.SetRadio()
    this.Parent.oContained.w_FILEEXIST=trim(this.Parent.oContained.w_FILEEXIST)
    this.value = ;
      iif(this.Parent.oContained.w_FILEEXIST=='S',1,;
      0)
  endfunc

  add object oCONFARCH_1_11 as StdCheck with uid="AGXWVQBNZB",rtseq=9,rtrep=.f.,left=276, top=409, caption="Conferma archiviazione",;
    ToolTipText = "Se attivo conferma subito l'archiviazione senza generare indici provvisori",;
    HelpContextID = 108130414,;
    cFormVar="w_CONFARCH", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCONFARCH_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCONFARCH_1_11.GetRadio()
    this.Parent.oContained.w_CONFARCH = this.RadioValue()
    return .t.
  endfunc

  func oCONFARCH_1_11.SetRadio()
    this.Parent.oContained.w_CONFARCH=trim(this.Parent.oContained.w_CONFARCH)
    this.value = ;
      iif(this.Parent.oContained.w_CONFARCH=='S',1,;
      0)
  endfunc


  add object oBtn_1_12 as StdButton with uid="ZMIZZNTUDJ",left=787, top=409, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la sincronizzazione";
    , HelpContextID = 211790314;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        GSUT_BIS(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="JWIBHYOYEX",left=839, top=409, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 259627962;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_14 as StdString with uid="YUTRCBGVMI",Visible=.t., Left=10, Top=12,;
    Alignment=1, Width=66, Height=17,;
    Caption="Percorsi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="QZFSIHZOVQ",Visible=.t., Left=20, Top=69,;
    Alignment=0, Width=225, Height=18,;
    Caption="Selezionare i percorsi di sincronizzazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="AUXASRUDLA",Visible=.t., Left=461, Top=12,;
    Alignment=1, Width=59, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_17.mHide()
    with this.Parent.oContained
      return (.w_TIPOPATH='P')
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="CAMFMKBFOD",Visible=.t., Left=2, Top=40,;
    Alignment=1, Width=74, Height=18,;
    Caption="Desc. indice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="ZJSDLLIWAE",Visible=.t., Left=267, Top=40,;
    Alignment=1, Width=68, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (.w_TIPODESC<>'M')
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="PXYZUVRXPL",Visible=.t., Left=282, Top=12,;
    Alignment=1, Width=53, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (.w_TIPOPATH='P')
    endwith
  endfunc
enddefine
define class tgsut_kisPag2 as StdContainer
  Width  = 894
  height = 460
  stdWidth  = 894
  stdheight = 460
  resizeXpos=687
  resizeYpos=160
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMSG_2_1 as StdMemo with uid="ZACPGHKXGL",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MSG", cQueryName = "MSG",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 266631994,;
   bGlobalFont=.t.,;
    Height=388, Width=884, Left=3, Top=8, Readonly=.t.


  add object oBtn_2_2 as StdButton with uid="XVTFJMLIFZ",left=839, top=409, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Esci";
    , HelpContextID = 259627962;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_3 as StdButton with uid="KWCNSAIPHV",left=8, top=409, width=48,height=45,;
    CpPicture="BMP\LOG.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per visualizzare la manutenzione della sincronizzazione";
    , HelpContextID = 266493770;
    , Caption='\<Manut.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      do GSUT1KIS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_SHOWLOG)
      endwith
    endif
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kis','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
