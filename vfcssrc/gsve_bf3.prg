* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bf3                                                        *
*              Elimina fatture differite                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_149]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-17                                                      *
* Last revis.: 2017-12-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bf3",oParentObject,m.pOper)
return(i_retval)

define class tgsve_bf3 as StdBatch
  * --- Local variables
  pOper = space(1)
  w_AZIONE = space(10)
  w_MVFLARIF = space(1)
  w_MVFLERIF = space(1)
  w_MESS = space(10)
  w_MVPREZZO = 0
  w_ALLDOC = .f.
  w_MVTIPRIG = space(1)
  w_FLCONT = space(1)
  w_FLPROV = space(1)
  w_TIPFAT = space(1)
  w_CODVAL = space(3)
  w_CAOVAL = 0
  w_OK = .f.
  w_OLDSER = space(10)
  w_NOCANC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_MVSERRIF = space(10)
  w_MVROWRIF = 0
  w_MVNUMRIF = 0
  w_SERDOC = space(10)
  w_NUMDOC = 0
  w_CODCLF = space(15)
  w_DESCRI = space(40)
  w_APPO = space(3)
  w_APPO1 = 0
  w_APPO2 = 0
  w_MVCODVAL = space(3)
  w_MVCAOVAL = 0
  w_CONTAB = .f.
  w_GENPRO = .f.
  w_RIGRAG = .f.
  w_QTAIMP = 0
  w_QTAIM1 = 0
  w_PREZZO = 0
  w_TIPRIG = space(1)
  w_FLRIFDCO = .f.
  w_FLSPERIP = space(1)
  w_RIFDCO = space(10)
  w_SPERIP = .f.
  w_PADRE = .NULL.
  w_FLPIASPE = .f.
  w_FLDOCEVA = .f.
  w_DPDOCEVA = space(10)
  w_TIPDOC = space(5)
  w_FLARCO = space(1)
  w_CAUPFI = space(5)
  w_RIFESP = space(10)
  w_COMPEV = 0
  w_COMMDEFA = space(15)
  w_SALCOM = space(1)
  w_COMMAPPO = space(15)
  w_MVCODUBI = space(20)
  w_MVCODUB2 = space(20)
  w_MVCODLOT = space(20)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_GEST_RIG = .f.
  w_FLEFFEVA = space(1)
  w_OKIND = .f.
  w_MVRIFDIC = space(15)
  w_MVAFLOM1 = space(1)
  w_MVAFLOM2 = space(1)
  w_MVAFLOM4 = space(1)
  w_MVAFLOM6 = space(1)
  w_MVAFLOM3 = space(1)
  w_MVAFLOM5 = space(1)
  w_MVAIMPN1 = 0
  w_MVAIMPN5 = 0
  w_MVAIMPN6 = 0
  w_MVAIMPN2 = 0
  w_MVAIMPN3 = 0
  w_MVAIMPN4 = 0
  w_MVSPEBOL = 0
  w_MVCODIVE = space(5)
  w_MVACIVA1 = space(5)
  w_MVACIVA2 = space(5)
  w_MVACIVA3 = space(5)
  w_MVACIVA4 = space(5)
  w_MVACIVA5 = space(5)
  w_MVACIVA6 = space(5)
  w_MVDATDOC = ctod("  /  /  ")
  w_TDFLSILI = space(1)
  w_IDSERIAL = space(20)
  w_VALATT = space(100)
  w_TABKEY = space(30)
  w_DELIND = .f.
  w_INDICE = .f.
  w_GESLOTUBI = .f.
  w_ORTIPDOC = space(5)
  w_DCFLEVAS = space(1)
  w_MVQTAIMP = 0
  w_MVQTAIM1 = 0
  w_PRD = space(2)
  w_FLVEAC = space(1)
  w_ANNDOC = space(4)
  w_ANNPRO = space(4)
  w_PRP = space(2)
  w_NUMEST = 0
  w_ALFEST = space(10)
  w_CLADOC = space(2)
  w_DITIPOPE = space(1)
  w_DICODIVA = space(5)
  w_NUMRIGA = 0
  w_TOTSTORN = 0
  w_RIFDIC = space(10)
  w_DIDICCOL_FILTRO = space(10)
  w_MVSERIAL = space(10)
  w_STORNO_PROC_CONV = .f.
  w_MVKEYSAL = space(20)
  w_MVCODMAG = space(5)
  w_MVCODMAT = space(5)
  w_MVFLIMPE = space(1)
  w_MVF2IMPE = space(1)
  w_MVFLORDI = space(1)
  w_MVF2ORDI = space(1)
  w_MVFLRISE = space(1)
  w_MVF2RISE = space(1)
  w_MVFLCASC = space(1)
  w_MVF2CASC = space(1)
  w_FLGSAL = space(1)
  w_MVQTASAL = 0
  w_MVIMPCOM = 0
  w_MVCODART = space(20)
  w_MVCODATT = space(15)
  w_MVCODCOM = space(15)
  w_MVCODCOS = space(15)
  w_MVTIPATT = space(1)
  w_MVFLORCO = space(1)
  w_MVFLCOCO = space(1)
  w_QTASAL = 0
  w_MVTIPDOC = space(5)
  w_TDORDAPE = space(1)
  w_MAXSER = space(10)
  w_MVDATEVA = ctod("  /  /  ")
  w_MVDATGEN = ctod("  /  /  ")
  w_OPDATDOC = ctod("  /  /  ")
  w_NUMRIGA = 0
  w_OPNUMDOC = 0
  w_OPALFDOC = space(10)
  * --- WorkFile variables
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  VALUTE_idx=0
  RAG_FATT_idx=0
  CON_PAGA_idx=0
  SALDIART_idx=0
  TIP_DOCU_idx=0
  TMP__DOC_idx=0
  MA_COSTI_idx=0
  DIC_INTE_idx=0
  DOC_COLL_idx=0
  ART_ICOL_idx=0
  SALDICOM_idx=0
  PRODINDI_idx=0
  PROMINDI_idx=0
  DICDINTE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina documenti associati al dettaglio fatture differite (GSVE_MFD)
    *     Elimina documenti associati al dettaglio piano di spedizione (GSVA_MPS)
    * --- R = Elimina la Singola Riga; T = Elimina Tutti i Documenti (Fatture differite)
    *     S = Elimina la Singola Riga; A = Elimina Tutti i Documenti (Piano di spedizione)
    do case
      case this.pOper=="S"
        this.pOper = "R"
        this.w_FLPIASPE = .T.
      case this.pOper=="A"
        this.pOper = "T"
        this.w_FLPIASPE = .T.
    endcase
    this.w_PADRE = This.oParentObject
    * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
    this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
    * --- Imposta la Deleted OFF per riattivare i record Cancellati con F6
    *     set deleted ripristinata al valore originario al termine del Batch
     
 OldsET = SET("DELETED") 
 Set Deleted Off
    this.w_ALLDOC = .F.
    this.w_OK = .T.
    this.w_NOCANC = 0
    this.w_CONTAB = .F.
    this.w_GENPRO = .F.
    this.w_FLRIFDCO = .F.
    * --- Esegue una scansione per capire se esistono delle FD da eliminare ....
    Select ( this.w_PADRE.cTrsName)
    Go Top
    SCAN FOR t_CPROWORD>0 AND IIF(this.w_FLPIASPE, !EMPTY(NVL(t_DPCHKDEL, " ")), !EMPTY(NVL(t_DPSERDOC," ")) ) AND I_SRV<>"A" And ( this.pOper="T" OR DELETED() )
    this.w_SERDOC = t_DPSERDOC
    if this.w_FLPIASPE
      this.w_NUMDOC = nvl(t_DPNUMDOC,0)
      this.w_ALFDOC = nvl(T_DPALFDOC, Space(10))
      this.w_DATDOC = nvl(T_DPDATDOC, cp_CharToDate("  -  -  "))
      this.w_DPDOCEVA = NVL(t_DPSEREVA, " ")
      this.w_FLDOCEVA = this.w_FLDOCEVA OR !EMPTY(this.w_DPDOCEVA)
    else
      this.w_NUMDOC = nvl(t_NUMDOC,0)
      this.w_ALFDOC = nvl(T_ALFDOC, Space(10))
      this.w_DATDOC = nvl(T_DATDOC, cp_CharToDate("  -  -  "))
      this.w_FLDOCEVA = .F.
      this.w_DPDOCEVA = SPACE(10)
    endif
    this.w_CODCLF = nvl(T_DPCODCLF, space(15))
    this.w_DESCRI = nvl(T_DESCRI , space(40))
    this.w_FLCONT = NVL(t_FLCONT, " ")
    this.w_FLPROV = NVL(t_GENPRO, " ")
    this.w_FLSPERIP = NVL(t_FLSPERIP, " ")
    this.w_TIPFAT = NVL(t_TIPFAT, " ")
    this.w_CODVAL = NVL(t_CODVAL, g_PERVAL)
    this.w_CAOVAL = NVL(t_CAOVAL, g_CAOVAL)
    this.w_CONTAB = IIF(this.w_FLCONT="S" , .T., this.w_CONTAB)
    this.w_GENPRO = IIF(this.w_FLPROV="S" AND g_PERAGE="S" , .T., this.w_GENPRO)
    this.w_SPERIP = IIF(this.w_FLSPERIP="S" , .T., this.w_SPERIP)
    this.w_RIFDCO = NVL(t_RIFDCO, " ")
    this.w_FLRIFDCO = this.w_FLRIFDCO OR (!EMPTY(NVL(t_RIFDCO, " ")) AND g_COGE<>"S")
    if !USED("ListaCan")
      * --- Crea Cursore interno
       
 CREATE CURSOR ListaCan ; 
 (NUMDOC N(15,0), ALFDOC C(10), DATDOC D(8), ; 
 CODCLF C(15), DESCRI C(40), SERDOC C(10), CODVAL C(3), CAOVAL N(12,6), FLCONT C(1), TIPFAT C(1), GENPRO C(1), FLSPERIP C(1), RIFDCO C(10), RIFEVA C(10))
    endif
    * --- Aggiunge documento a cursore ...
     
 INSERT INTO ListaCan ; 
 (NUMDOC, ALFDOC, DATDOC, CODCLF, DESCRI, SERDOC, CODVAL, CAOVAL, FLCONT, TIPFAT, GENPRO,FLSPERIP, RIFDCO, RIFEVA) VALUES ; 
 (this.w_NUMDOC, this.w_ALFDOC, this.w_DATDOC, this.w_CODCLF, this.w_DESCRI, this.w_SERDOC, this.w_CODVAL, this.w_CAOVAL, this.w_FLCONT, this.w_TIPFAT, this.w_FLPROV, this.w_FLSPERIP, this.w_RIFDCO, this.w_DPDOCEVA)
    SELECT (this.oParentObject.cTrsName)
    ENDSCAN
    * --- Mostra maschera con cursore risultato
    if used("ListaCan")
      this.w_OK = .T.
      if this.w_CONTAB OR this.w_GENPRO OR this.w_SPERIP OR this.w_FLRIFDCO OR this.w_FLDOCEVA
        * --- istanzio oggetto per mess. incrementali
        this.w_oMESS=createobject("ah_message")
        * --- Se Trovati documenti da eliminare da avviso prima di procedere...
        if this.w_CONTAB
          this.w_oMESS.AddMsgPartNL("Alcuni documenti presenti nel piano sono stati contabilizzati")     
        endif
        if this.w_GENPRO
          this.w_oMESS.AddMsgPartNL("Per alcuni documenti presenti nel piano sono state generate le provvigioni")     
        endif
        if this.w_SPERIP
          this.w_oMESS.AddMsgPartNL("Alcuni documenti presenti nel piano evadono documenti soggetti a ripartizione spese")     
        endif
        if this.w_FLRIFDCO
          this.w_oMESS.AddMsgPartNL("Alcuni documenti presenti nel piano sono stati confermati in gestione effetti")     
        endif
        if this.w_FLDOCEVA
          this.w_oMESS.AddMsgPartNL("Alcuni documenti presenti nel piano sono stati evasi")     
        endif
        if this.pOper="R"
          this.w_oMESS.AddMsgPartNL("Questi documenti non potranno essere eliminati dal piano%0Procedo comunque alla eliminazione dei restanti documenti?")     
        else
          this.w_oMESS.AddMsgPartNL("Questi documenti non saranno eliminati a seguito della cancellazione del piano%0Procedo comunque alla eliminazione del piano e dei restanti documenti?")     
        endif
        this.w_OK = this.w_oMESS.ah_YesNo()
      endif
      Select ListaCan
      go TOP
      this.w_Azione = "SOSPENDI"
      if this.w_OK
        do GSVE_KF3 with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Test
      if this.w_Azione = "SOSPENDI"
        * --- Blocca operazione
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=ah_Msgformat("Operazione sospesa")
      else
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      USE IN SELECT("ListaCan")
    endif
    * --- Chiude cursore
    * --- set deleted ripristinata al valore originario al tern�mine del Batch
    SET DELETED &OLDSET
    * --- Si riposiziona su cursore movimentazione
    if this.pOper = "R"
      this.w_PADRE.FirstRow()     
      this.w_PADRE.SetRow()     
    endif
    if this.w_NOCANC<>0
      ah_ErrorMsg("N. %1 documenti non sono stati eliminati%0in quanto evasi da altri documenti, gi� contabilizzati, con provvigioni generate,confermati in gestione effetti o collegati a indici allegati",,"", ALLTRIM(STR(this.w_NOCANC)) )
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue una scansione per cancellare le FD selezionate ...
    * --- Verifico se associato al piano di fatturazione esistono delle righe descrittive
    *     evase a causa della fatturazione per ordine, in questo caso eseguo lo strono 
    *     solo alla cancellazione del piano utilizzando la tabella...
    * --- E' attiva la nuova gestione righe descrittive ?
    this.w_GEST_RIG = !this.w_FLPIASPE AND GSVE_BGE( This , "NEW" , this.oParentObject.w_DPSERIAL )
    this.w_FLEFFEVA = IIF( this.w_FLPIASPE, "=", " ")
    this.w_DELIND = 0
    * --- Riordino il Cursore dei documenti cancellati per Alfa e Numero documento 
    *     per il 'recupero progressivi' (batch GSAR_BRP). Devo considerare sempre il 
    *     documento con numero pi� alto.
    select * From ListaCan into Cursor ListaCan Order By ALFDOC, NUMDOC Desc
    select ListaCan
    GO TOP
    SCAN
    this.w_SERDOC = ListaCan.SERDOC
    this.w_OKIND = .t.
    * --- Verifica se la fattura e' gia' stata Contabilizzata o Generate le Provvigioni
    this.w_FLCONT = ListaCan.FLCONT
    this.w_FLPROV = ListaCan.GENPRO
    this.w_FLSPERIP = ListaCan.FLSPERIP
    this.w_TIPFAT = ListaCan.TIPFAT
    this.w_MVCODVAL = ListaCan.CODVAL
    this.w_MVCAOVAL = ListaCan.CAOVAL
    this.w_RIFDCO = ListaCan.RIFDCO
    this.w_DPDOCEVA = ListaCan.RIFEVA
    * --- Lettura dei campi necessari al calcolo dei progressivi e documenti di esplosione
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            MVSERIAL = this.w_SERDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ANNDOC = NVL(cp_ToDate(_read_.MVANNDOC),cp_NullValue(_read_.MVANNDOC))
      this.w_PRD = NVL(cp_ToDate(_read_.MVPRD),cp_NullValue(_read_.MVPRD))
      this.w_PRP = NVL(cp_ToDate(_read_.MVPRP),cp_NullValue(_read_.MVPRP))
      this.w_FLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
      this.w_CLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
      this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
      this.w_NUMEST = NVL(cp_ToDate(_read_.MVNUMEST),cp_NullValue(_read_.MVNUMEST))
      this.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
      this.w_ALFEST = NVL(cp_ToDate(_read_.MVALFEST),cp_NullValue(_read_.MVALFEST))
      this.w_ANNPRO = NVL(cp_ToDate(_read_.MVANNPRO),cp_NullValue(_read_.MVANNPRO))
      this.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
      this.w_RIFESP = NVL(cp_ToDate(_read_.MVRIFESP),cp_NullValue(_read_.MVRIFESP))
      this.w_MVRIFDIC = NVL(cp_ToDate(_read_.MVRIFDIC),cp_NullValue(_read_.MVRIFDIC))
      this.w_MVAFLOM1 = NVL(cp_ToDate(_read_.MVAFLOM1),cp_NullValue(_read_.MVAFLOM1))
      this.w_MVAFLOM2 = NVL(cp_ToDate(_read_.MVAFLOM2),cp_NullValue(_read_.MVAFLOM2))
      this.w_MVAFLOM4 = NVL(cp_ToDate(_read_.MVAFLOM4),cp_NullValue(_read_.MVAFLOM4))
      this.w_MVAFLOM6 = NVL(cp_ToDate(_read_.MVAFLOM6),cp_NullValue(_read_.MVAFLOM6))
      this.w_MVAFLOM3 = NVL(cp_ToDate(_read_.MVAFLOM3),cp_NullValue(_read_.MVAFLOM3))
      this.w_MVAFLOM5 = NVL(cp_ToDate(_read_.MVAFLOM5),cp_NullValue(_read_.MVAFLOM5))
      this.w_MVAIMPN1 = NVL(cp_ToDate(_read_.MVAIMPN1),cp_NullValue(_read_.MVAIMPN1))
      this.w_MVAIMPN5 = NVL(cp_ToDate(_read_.MVAIMPN5),cp_NullValue(_read_.MVAIMPN5))
      this.w_MVAIMPN6 = NVL(cp_ToDate(_read_.MVAIMPN6),cp_NullValue(_read_.MVAIMPN6))
      this.w_MVAIMPN2 = NVL(cp_ToDate(_read_.MVAIMPN2),cp_NullValue(_read_.MVAIMPN2))
      this.w_MVAIMPN3 = NVL(cp_ToDate(_read_.MVAIMPN3),cp_NullValue(_read_.MVAIMPN3))
      this.w_MVAIMPN4 = NVL(cp_ToDate(_read_.MVAIMPN4),cp_NullValue(_read_.MVAIMPN4))
      this.w_MVSPEBOL = NVL(cp_ToDate(_read_.MVSPEBOL),cp_NullValue(_read_.MVSPEBOL))
      this.w_MVCODIVE = NVL(cp_ToDate(_read_.MVCODIVE),cp_NullValue(_read_.MVCODIVE))
      this.w_MVACIVA1 = NVL(cp_ToDate(_read_.MVACIVA1),cp_NullValue(_read_.MVACIVA1))
      this.w_MVACIVA2 = NVL(cp_ToDate(_read_.MVACIVA2),cp_NullValue(_read_.MVACIVA2))
      this.w_MVACIVA3 = NVL(cp_ToDate(_read_.MVACIVA3),cp_NullValue(_read_.MVACIVA3))
      this.w_MVACIVA4 = NVL(cp_ToDate(_read_.MVACIVA4),cp_NullValue(_read_.MVACIVA4))
      this.w_MVACIVA5 = NVL(cp_ToDate(_read_.MVACIVA5),cp_NullValue(_read_.MVACIVA5))
      this.w_MVACIVA6 = NVL(cp_ToDate(_read_.MVACIVA6),cp_NullValue(_read_.MVACIVA6))
      this.w_MVDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDFLARCO,TDCAUPFI,TDFLSILI"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDFLARCO,TDCAUPFI,TDFLSILI;
        from (i_cTable) where;
            TDTIPDOC = this.w_TIPDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLARCO = NVL(cp_ToDate(_read_.TDFLARCO),cp_NullValue(_read_.TDFLARCO))
      this.w_CAUPFI = NVL(cp_ToDate(_read_.TDCAUPFI),cp_NullValue(_read_.TDCAUPFI))
      this.w_TDFLSILI = NVL(cp_ToDate(_read_.TDFLSILI),cp_NullValue(_read_.TDFLSILI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_VALATT = this.w_SERDOC
    this.w_TABKEY = "DOC_MAST"
    * --- Read from PRODINDI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PRODINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IDSERIAL"+;
        " from "+i_cTable+" PRODINDI where ";
            +"IDTABKEY = "+cp_ToStrODBC(this.w_TABKEY);
            +" and IDVALATT = "+cp_ToStrODBC(this.w_VALATT);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IDSERIAL;
        from (i_cTable) where;
            IDTABKEY = this.w_TABKEY;
            and IDVALATT = this.w_VALATT;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_IDSERIAL = NVL(cp_ToDate(_read_.IDSERIAL),cp_NullValue(_read_.IDSERIAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_ROWS<>0 and Not Empty(this.w_IDSERIAL)
      if this.w_DELIND=0
        this.w_DELIND = IIF(Ah_yesno("Esiste almeno un indice allegato collegato, si vuole procedere con la cancellazione?"),1,2)
        if this.w_DELIND=1
          this.w_INDICE = Ah_yesno("Si vogliono eliminare anche gli indici allegati ?")
        endif
      endif
      if this.w_DELIND=2
        this.w_OKIND = .f.
      endif
      if this.w_INDICE
        * --- Delete from PRODINDI
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                 )
        else
          delete from (i_cTable) where;
                IDSERIAL = this.w_IDSERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from PROMINDI
        i_nConn=i_TableProp[this.PROMINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                 )
        else
          delete from (i_cTable) where;
                IDSERIAL = this.w_IDSERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
    endif
    if this.w_FLCONT="S" OR (this.w_FLPROV="S" AND g_PERAGE="S") OR this.w_FLSPERIP = "S" OR !EMPTY(this.w_RIFDCO) OR !EMPTY(this.w_DPDOCEVA) OR !this.w_OKIND
      * --- Documento non Eliminabile
      this.w_NOCANC = this.w_NOCANC + 1
      if this.pOper="T" AND EMPTY(this.w_DPDOCEVA)
        * --- Se Elimina Tutta la Distinta Fatturazione Toglie comunque il Riferimento
        * --- Try
        local bErr_05326788
        bErr_05326788=bTrsErr
        this.Try_05326788()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_05326788
        * --- End
      endif
    else
      * --- Elimina Completamente il Documento
      * --- Cicla sul Dettaglio
      this.w_OLDSER = "ZXCZXCZX"
      this.w_RIGRAG = .F.
      this.w_COMPEV = 0
      * --- Select from DOC_DETT
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MVSERRIF,MVROWRIF,MVNUMRIF,MVFLARIF,MVPREZZO,MVTIPRIG,MVQTAIMP,MVQTAIM1,MVRIFESC,CPROWNUM,MVFLCASC,MVF2CASC,MVFLORDI,MVF2ORDI,MVFLIMPE,MVF2IMPE,MVFLRISE,MVF2RISE,MVKEYSAL,MVCODMAG, MVCODLOT, MVCODUBI, MVCODUB2,MVCODART,MVCODCOM  from "+i_cTable+" DOC_DETT ";
            +" where MVSERIAL="+cp_ToStrODBC(this.w_SERDOC)+"";
            +" order by MVSERRIF";
             ,"_Curs_DOC_DETT")
      else
        select MVSERRIF,MVROWRIF,MVNUMRIF,MVFLARIF,MVPREZZO,MVTIPRIG,MVQTAIMP,MVQTAIM1,MVRIFESC,CPROWNUM,MVFLCASC,MVF2CASC,MVFLORDI,MVF2ORDI,MVFLIMPE,MVF2IMPE,MVFLRISE,MVF2RISE,MVKEYSAL,MVCODMAG, MVCODLOT, MVCODUBI, MVCODUB2,MVCODART,MVCODCOM from (i_cTable);
         where MVSERIAL=this.w_SERDOC;
         order by MVSERRIF;
          into cursor _Curs_DOC_DETT
      endif
      if used('_Curs_DOC_DETT')
        select _Curs_DOC_DETT
        locate for 1=1
        do while not(eof())
        * --- Storna Evasione Documento di Origine
        this.w_COMPEV = this.w_COMPEV + IIF( !Empty(Nvl(_Curs_DOC_DETT.MVRIFESC," ")) And !CHKEVCOM( _Curs_DOC_DETT.MVRIFESC ) ,1, 0 )
        this.w_MVSERRIF = NVL(_Curs_DOC_DETT.MVSERRIF," ")
        this.w_MVROWRIF = NVL(_Curs_DOC_DETT.MVROWRIF, 0)
        this.w_MVNUMRIF = NVL(_Curs_DOC_DETT.MVNUMRIF, -20)
        this.w_MVFLARIF = NVL(_Curs_DOC_DETT.MVFLARIF," ")
        this.w_MVPREZZO = NVL(_Curs_DOC_DETT.MVPREZZO, 0)
        this.w_MVTIPRIG = NVL(_Curs_DOC_DETT.MVTIPRIG, " ")
        this.w_QTAIMP = NVL(_Curs_DOC_DETT.MVQTAIMP, 0)
        this.w_QTAIM1 = NVL(_Curs_DOC_DETT.MVQTAIM1, 0)
        this.w_PREZZO = IIF(this.w_MVTIPRIG="F",NVL(_Curs_DOC_DETT.MVPREZZO, 0),0)
        if !this.w_GESLOTUBI
          * --- testa se c'� almeno un articolo a lotti o ubicazioni
          this.w_GESLOTUBI = (NOT EMPTY(NVL(_Curs_DOC_DETT.MVCODLOT," ")) OR NOT EMPTY(NVL(_Curs_DOC_DETT.MVCODUBI," ")))
        endif
        * --- Testa Presenza Riga Raggruppata
        this.w_RIGRAG = this.w_RIGRAG OR this.w_MVNUMRIF=-90
        if NOT EMPTY(this.w_MVSERRIF) AND this.w_MVROWRIF<>0 AND this.w_MVNUMRIF=-20
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVTIPDOC"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVTIPDOC;
              from (i_cTable) where;
                  MVSERIAL = this.w_MVSERRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ORTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from DOC_COLL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_COLL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_COLL_idx,2],.t.,this.DOC_COLL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DCFLEVAS"+;
              " from "+i_cTable+" DOC_COLL where ";
                  +"DCCODICE = "+cp_ToStrODBC(this.w_TIPDOC);
                  +" and DCCOLLEG = "+cp_ToStrODBC(this.w_ORTIPDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DCFLEVAS;
              from (i_cTable) where;
                  DCCODICE = this.w_TIPDOC;
                  and DCCOLLEG = this.w_ORTIPDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DCFLEVAS = NVL(cp_ToDate(_read_.DCFLEVAS),cp_NullValue(_read_.DCFLEVAS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- No Righe Raggruppate
          do case
            case this.w_MVFLARIF="-" AND this.w_MVTIPRIG="F"
              * --- Fattura di Acconto
              this.w_APPO = SPACE(3)
              * --- Read from DOC_MAST
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DOC_MAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MVCODVAL"+;
                  " from "+i_cTable+" DOC_MAST where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MVCODVAL;
                  from (i_cTable) where;
                      MVSERIAL = this.w_MVSERRIF;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_APPO = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              * --- Se FAttura di Acconto in altra Valuta converte l'importo evaso alla valuta di del Doc. di Origine
              if this.w_MVCODVAL<>this.w_APPO AND NOT EMPTY(this.w_APPO)
                this.w_APPO1 = 0
                this.w_APPO2 = 0
                this.w_APPO1 = GETCAM(this.w_APPO, this.w_MVDATDOC)
                this.w_APPO2 = GETVALUT(this.w_APPO, "VADECUNI")
                this.w_MVPREZZO = VAL2MON(this.w_MVPREZZO, this.w_APPO1,1, this.w_MVDATDOC, this.w_APPO2)
                * --- Converte in Euro
              endif
              * --- Write into DOC_DETT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DOC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
                +",MVIMPEVA =MVIMPEVA+ "+cp_ToStrODBC(this.w_MVPREZZO);
                +",MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVEFFEVA');
                +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
                +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
                    +i_ccchkf ;
                +" where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWRIF);
                    +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                       )
              else
                update (i_cTable) set;
                    MVFLEVAS = " ";
                    ,MVIMPEVA = MVIMPEVA + this.w_MVPREZZO;
                    ,MVEFFEVA = cp_CharToDate("  -  -  ");
                    ,MVQTAEVA = 0;
                    ,MVQTAEV1 = 0;
                    &i_ccchkf. ;
                 where;
                    MVSERIAL = this.w_MVSERRIF;
                    and CPROWNUM = this.w_MVROWRIF;
                    and MVNUMRIF = this.w_MVNUMRIF;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            case this.w_MVFLARIF="+" OR (EMPTY(this.w_MVFLARIF) AND this.w_DCFLEVAS="S")
              * --- Storno i saldi
              this.w_MVQTASAL = this.w_QTAIM1
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if this.w_MVFLARIF="+"
                * --- Write into DOC_DETT
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
                  i_cOp6=cp_SetTrsOp(this.w_FLGSAL,'MVQTASAL','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                  i_cOp7=cp_SetTrsOp(this.w_FLEFFEVA,'MVEFFEVA','cp_CharToDate("  -  -  ")',cp_CharToDate("  -  -  "),'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVDATGEN');
                  +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
                  +",MVQTAEVA =MVQTAEVA- "+cp_ToStrODBC(this.w_QTAIMP);
                  +",MVQTAEV1 =MVQTAEV1- "+cp_ToStrODBC(this.w_QTAIM1);
                  +",MVIMPEVA =MVIMPEVA- "+cp_ToStrODBC(this.w_PREZZO);
                  +",MVQTASAL ="+cp_NullLink(i_cOp6,'DOC_DETT','MVQTASAL');
                  +",MVEFFEVA ="+cp_NullLink(i_cOp7,'DOC_DETT','MVEFFEVA');
                      +i_ccchkf ;
                  +" where ";
                      +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWRIF);
                      +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                         )
                else
                  update (i_cTable) set;
                      MVDATGEN = cp_CharToDate("  -  -  ");
                      ,MVFLEVAS = " ";
                      ,MVQTAEVA = MVQTAEVA - this.w_QTAIMP;
                      ,MVQTAEV1 = MVQTAEV1 - this.w_QTAIM1;
                      ,MVIMPEVA = MVIMPEVA - this.w_PREZZO;
                      ,MVQTASAL = &i_cOp6.;
                      ,MVEFFEVA = &i_cOp7.;
                      &i_ccchkf. ;
                   where;
                      MVSERIAL = this.w_MVSERRIF;
                      and CPROWNUM = this.w_MVROWRIF;
                      and MVNUMRIF = this.w_MVNUMRIF;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
              this.w_MVFLIMPE = NVL(_Curs_DOC_DETT.MVFLIMPE, " ")
              this.w_MVF2IMPE = NVL(_Curs_DOC_DETT.MVF2IMPE, " ")
              this.w_MVFLORDI = NVL(_Curs_DOC_DETT.MVFLORDI, " ")
              this.w_MVF2ORDI = NVL(_Curs_DOC_DETT.MVF2ORDI, " ")
              this.w_MVFLRISE = NVL(_Curs_DOC_DETT.MVFLRISE, " ")
              this.w_MVF2RISE = NVL(_Curs_DOC_DETT.MVF2RISE, " ")
              this.w_MVFLCASC = NVL(_Curs_DOC_DETT.MVFLCASC, " ")
              this.w_MVF2CASC = NVL(_Curs_DOC_DETT.MVF2CASC, " ")
              this.w_MVKEYSAL = _Curs_DOC_DETT.MVKEYSAL
              this.w_MVCODMAG = _Curs_DOC_DETT.MVCODMAG
              this.w_MVCODUBI = _Curs_DOC_DETT.MVCODUBI
              this.w_MVCODLOT = _Curs_DOC_DETT.MVCODLOT
              this.w_MVCODUB2 = _Curs_DOC_DETT.MVCODUB2
              if NOT EMPTY(this.w_MVFLORDI+this.w_MVFLIMPE+this.w_MVF2ORDI+this.w_MVF2IMPE+this.w_MVFLCASC+this.w_MVF2CASC)
                this.w_MVFLIMPE = ICASE(this.w_MVFLIMPE="+", "-", this.w_MVFLIMPE="-", "+", this.w_MVFLIMPE )
                this.w_MVF2IMPE = ICASE(this.w_MVF2IMPE="+", "-",this.w_MVF2IMPE="-", "+", this.w_MVF2IMPE )
                this.w_MVFLORDI = ICASE(this.w_MVFLORDI="+", "-", this.w_MVFLORDI="-", "+", this.w_MVFLORDI )
                this.w_MVF2ORDI = ICASE(this.w_MVF2ORDI="+", "-", this.w_MVF2ORDI="-", "+", this.w_MVF2ORDI )
                this.w_MVFLRISE = ICASE(this.w_MVFLRISE="+", "-", this.w_MVFLRISE="-", "+", this.w_MVFLRISE )
                this.w_MVF2RISE = ICASE(this.w_MVF2RISE="+", "-", this.w_MVF2RISE="-", "+", this.w_MVF2RISE )
                this.w_MVFLCASC = ICASE(this.w_MVFLCASC="+", "-", this.w_MVFLCASC="-", "+", this.w_MVFLCASC )
                this.w_MVF2CASC = ICASE(this.w_MVF2CASC="+", "-", this.w_MVF2CASC="-", "+", this.w_MVF2CASC )
                * --- Aggiorno saldi MVCODMAG
                * --- Write into SALDIART
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_MVFLORDI,'SLQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_MVFLIMPE,'SLQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                  i_cOp3=cp_SetTrsOp(this.w_MVFLCASC,'SLQTAPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                  +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                  +",SLQTAPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTAPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                      +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                         )
                else
                  update (i_cTable) set;
                      SLQTOPER = &i_cOp1.;
                      ,SLQTIPER = &i_cOp2.;
                      ,SLQTAPER = &i_cOp3.;
                      &i_ccchkf. ;
                   where;
                      SLCODICE = this.w_MVKEYSAL;
                      and SLCODMAG = this.w_MVCODMAG;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Read from ART_ICOL
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.ART_ICOL_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ARSALCOM"+;
                    " from "+i_cTable+" ART_ICOL where ";
                        +"ARCODART = "+cp_ToStrODBC(_Curs_DOC_DETT.MVCODART);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ARSALCOM;
                    from (i_cTable) where;
                        ARCODART = _Curs_DOC_DETT.MVCODART;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if this.w_SALCOM="S"
                  if empty(nvl(_Curs_DOC_DETT.MVCODCOM,""))
                    this.w_COMMAPPO = this.w_COMMDEFA
                  else
                    this.w_COMMAPPO = _Curs_DOC_DETT.MVCODCOM
                  endif
                  * --- Write into SALDICOM
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALDICOM_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                    i_cOp1=cp_SetTrsOp(this.w_MVFLORDI,'SCQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                    i_cOp2=cp_SetTrsOp(this.w_MVFLIMPE,'SCQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                    i_cOp3=cp_SetTrsOp(this.w_MVFLCASC,'SCQTAPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                    +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                    +",SCQTAPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTAPER');
                        +i_ccchkf ;
                    +" where ";
                        +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                        +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                        +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                           )
                  else
                    update (i_cTable) set;
                        SCQTOPER = &i_cOp1.;
                        ,SCQTIPER = &i_cOp2.;
                        ,SCQTAPER = &i_cOp3.;
                        &i_ccchkf. ;
                     where;
                        SCCODICE = this.w_MVKEYSAL;
                        and SCCODMAG = this.w_MVCODMAG;
                        and SCCODCAN = this.w_COMMAPPO;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error='Errore Aggiornamento Saldi Commessa'
                    return
                  endif
                endif
                * --- Aggiorno saldi MVCODMAT
                * --- Write into SALDIART
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.SALDIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_MVF2ORDI,'SLQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                  i_cOp2=cp_SetTrsOp(this.w_MVF2IMPE,'SLQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                  i_cOp3=cp_SetTrsOp(this.w_MVF2CASC,'SLQTAPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
                  +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
                  +",SLQTAPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTAPER');
                      +i_ccchkf ;
                  +" where ";
                      +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                      +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
                         )
                else
                  update (i_cTable) set;
                      SLQTOPER = &i_cOp1.;
                      ,SLQTIPER = &i_cOp2.;
                      ,SLQTAPER = &i_cOp3.;
                      &i_ccchkf. ;
                   where;
                      SLCODICE = this.w_MVKEYSAL;
                      and SLCODMAG = this.w_MVCODMAT;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                if this.w_SALCOM="S"
                  * --- Write into SALDICOM
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.SALDICOM_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
                    i_cOp1=cp_SetTrsOp(this.w_MVF2ORDI,'SCQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                    i_cOp2=cp_SetTrsOp(this.w_MVF2IMPE,'SCQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                    i_cOp3=cp_SetTrsOp(this.w_MVF2CASC,'SCQTAPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
                    +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
                    +",SCQTAPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTAPER');
                        +i_ccchkf ;
                    +" where ";
                        +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                        +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
                        +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                           )
                  else
                    update (i_cTable) set;
                        SCQTOPER = &i_cOp1.;
                        ,SCQTIPER = &i_cOp2.;
                        ,SCQTAPER = &i_cOp3.;
                        &i_ccchkf. ;
                     where;
                        SCCODICE = this.w_MVKEYSAL;
                        and SCCODMAG = this.w_MVCODMAT;
                        and SCCODCAN = this.w_COMMAPPO;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error='Errore Aggiornamento Saldi Commessa'
                    return
                  endif
                endif
              endif
            otherwise
              * --- Ordine aperto
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
          endcase
          if this.w_FLARCO="S" AND ((!EMPTY(this.w_CAUPFI) AND !EMPTY(this.w_RIFESP)) OR this.w_COMPEV>0)
            this.w_MESS = GSVE_BEC( this, _Curs_DOC_DETT.MVRIFESC, this.w_SERDOC, _Curs_DOC_DETT.CPROWNUM, .T.)
          endif
        endif
        if NOT EMPTY(this.w_MVSERRIF) AND this.w_MVSERRIF<>this.w_OLDSER AND this.w_TIPFAT $ "CE" And Not this.w_GEST_RIG
          * --- Toglie riferimento evasione alle Righe descrittive solo se tipo fatturazione per ordine o ordine+Destinazione
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
            do vq_exec with 'GSVE_QEV',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                    +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVDATGEN');
            +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
            +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
            +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
                +i_ccchkf;
                +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                    +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
            +"DOC_DETT.MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVDATGEN');
            +",DOC_DETT.MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
            +",DOC_DETT.MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
            +",DOC_DETT.MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
                +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                    +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
                +"MVDATGEN,";
                +"MVFLEVAS,";
                +"MVQTAEVA,";
                +"MVQTAEV1";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVDATGEN')+",";
                +cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS')+",";
                +cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA')+",";
                +cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1')+"";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                    +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
            +"MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVDATGEN');
            +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
            +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
            +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                    +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                    +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVDATGEN');
            +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
            +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
            +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_OLDSER = this.w_MVSERRIF
        endif
          select _Curs_DOC_DETT
          continue
        enddo
        use
      endif
      if this.w_RIGRAG
        * --- Elimina Riferimenti associati a righe Raggruppate
        * --- Select from RAG_FATT
        i_nConn=i_TableProp[this.RAG_FATT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RAG_FATT_idx,2],.t.,this.RAG_FATT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" RAG_FATT ";
              +" where FRSERFAT="+cp_ToStrODBC(this.w_SERDOC)+"";
               ,"_Curs_RAG_FATT")
        else
          select * from (i_cTable);
           where FRSERFAT=this.w_SERDOC;
            into cursor _Curs_RAG_FATT
        endif
        if used('_Curs_RAG_FATT')
          select _Curs_RAG_FATT
          locate for 1=1
          do while not(eof())
          this.w_MVSERRIF = NVL(_Curs_RAG_FATT.FRSERDDT, SPACE(10))
          this.w_MVROWRIF = NVL(_Curs_RAG_FATT.FRROWDDT, 0)
          this.w_MVNUMRIF = NVL(_Curs_RAG_FATT.FRNUMDDT, 0)
          this.w_MVQTAIMP = NVL(_Curs_RAG_FATT.FRQTAIMP, 0)
          this.w_MVQTAIM1 = NVL(_Curs_RAG_FATT.FRQTAIM1, 0)
          if NOT EMPTY(this.w_MVSERRIF) AND this.w_MVROWRIF<>0 AND this.w_MVNUMRIF=-20
            * --- Read from DOC_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVFLERIF"+;
                " from "+i_cTable+" DOC_DETT where ";
                    +"MVSERIAL = "+cp_ToStrODBC(_Curs_RAG_FATT.FRSERFAT);
                    +" and CPROWNUM = "+cp_ToStrODBC(_Curs_RAG_FATT.FRROWFAT);
                    +" and MVNUMRIF = "+cp_ToStrODBC(_Curs_RAG_FATT.FRNUMFAT);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVFLERIF;
                from (i_cTable) where;
                    MVSERIAL = _Curs_RAG_FATT.FRSERFAT;
                    and CPROWNUM = _Curs_RAG_FATT.FRROWFAT;
                    and MVNUMRIF = _Curs_RAG_FATT.FRNUMFAT;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MVFLERIF = NVL(cp_ToDate(_read_.MVFLERIF),cp_NullValue(_read_.MVFLERIF))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_MVFLERIF="S"
              * --- Storno i saldi
              this.w_MVQTASAL = this.w_MVQTAIM1
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              * --- Righe Raggruppate (possono essere solo di tipo R o M)
              * --- Write into DOC_DETT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DOC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
                i_cOp6=cp_SetTrsOp(this.w_FLGSAL,'MVQTASAL','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVDATGEN');
                +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
                +",MVQTAEVA =MVQTAEVA- "+cp_ToStrODBC(this.w_MVQTAIMP);
                +",MVQTAEV1 =MVQTAEV1- "+cp_ToStrODBC(this.w_MVQTAIM1);
                +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
                +",MVQTASAL ="+cp_NullLink(i_cOp6,'DOC_DETT','MVQTASAL');
                    +i_ccchkf ;
                +" where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWRIF);
                    +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                       )
              else
                update (i_cTable) set;
                    MVDATGEN = cp_CharToDate("  -  -  ");
                    ,MVFLEVAS = " ";
                    ,MVQTAEVA = MVQTAEVA - this.w_MVQTAIMP;
                    ,MVQTAEV1 = MVQTAEV1 - this.w_MVQTAIM1;
                    ,MVIMPEVA = 0;
                    ,MVQTASAL = &i_cOp6.;
                    &i_ccchkf. ;
                 where;
                    MVSERIAL = this.w_MVSERRIF;
                    and CPROWNUM = this.w_MVROWRIF;
                    and MVNUMRIF = this.w_MVNUMRIF;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              * --- Ordine aperto
              this.Page_4()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
            select _Curs_RAG_FATT
            continue
          enddo
          use
        endif
      endif
      * --- Elimina Documento
      * --- Delete from CON_PAGA
      i_nConn=i_TableProp[this.CON_PAGA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_PAGA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CPSERIAL = "+cp_ToStrODBC(this.w_SERDOC);
               )
      else
        delete from (i_cTable) where;
              CPSERIAL = this.w_SERDOC;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from DOC_RATE
      i_nConn=i_TableProp[this.DOC_RATE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"RSSERIAL = "+cp_ToStrODBC(this.w_SERDOC);
               )
      else
        delete from (i_cTable) where;
              RSSERIAL = this.w_SERDOC;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      if this.w_FLARCO="S" AND ((!EMPTY(this.w_CAUPFI) AND !EMPTY(this.w_RIFESP)) OR this.w_COMPEV>0)
        this.w_MESS = GSVE_BEC( this, this.w_RIFESP, this.w_SERDOC, 0, .F.)
      endif
      if g_MADV="S" AND g_VEFA="S" and this.w_GESLOTUBI
        * --- Aggiorna lotti e ubicazioni da piano spedizione
        GSMD_BRL (this, this.w_SERDOC , "V" , "-" , ,.T.)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Delete from DOC_DETT
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOC);
               )
      else
        delete from (i_cTable) where;
              MVSERIAL = this.w_SERDOC;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      if !EMPTY(NVL(this.w_MVRIFDIC, " ")) AND NVL(this.w_TDFLSILI, " ")=="S"
        this.w_NUMRIGA = 0
        * --- Read from DIC_INTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIC_INTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DITIPOPE,DICODIVA"+;
            " from "+i_cTable+" DIC_INTE where ";
                +"DISERIAL = "+cp_ToStrODBC(this.w_MVRIFDIC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DITIPOPE,DICODIVA;
            from (i_cTable) where;
                DISERIAL = this.w_MVRIFDIC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DITIPOPE = NVL(cp_ToDate(_read_.DITIPOPE),cp_NullValue(_read_.DITIPOPE))
          this.w_DICODIVA = NVL(cp_ToDate(_read_.DICODIVA),cp_NullValue(_read_.DICODIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NVL(this.w_DITIPOPE, " ")<>"D"
          this.w_TOTSTORN = (IIF(this.w_MVAFLOM1="X" AND this.w_DICODIVA=this.w_MVACIVA1,this.w_MVAIMPN1,0)+IIF(this.w_MVAFLOM2="X" AND this.w_DICODIVA=this.w_MVACIVA2, this.w_MVAIMPN2,0)+IIF(this.w_MVAFLOM3="X" AND this.w_DICODIVA=this.w_MVACIVA3, this.w_MVAIMPN3,0)+IIF(this.w_MVAFLOM4="X" AND this.w_DICODIVA=this.w_MVACIVA4, this.w_MVAIMPN4,0)+IIF(this.w_MVAFLOM5="X" AND this.w_DICODIVA=this.w_MVACIVA5,this.w_MVAIMPN5,0)+IIF(this.w_MVAFLOM6="X" AND this.w_DICODIVA=this.w_MVACIVA6,this.w_MVAIMPN6,0))
          if g_PERVAL<>this.w_MVCODVAL
            this.w_TOTSTORN = VAL2MON(this.w_TOTSTORN, this.w_MVCAOVAL, g_CAOVAL, this.w_MVDATDOC, g_PERVAL,g_PERPVL)
          endif
          * --- Write into DIC_INTE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DIC_INTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
            i_cOp1=cp_SetTrsOp("-",'DIIMPUTI','this.w_TOTSTORN',this.w_TOTSTORN,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DIIMPUTI ="+cp_NullLink(i_cOp1,'DIC_INTE','DIIMPUTI');
                +i_ccchkf ;
            +" where ";
                +"DISERIAL = "+cp_ToStrODBC(this.w_MVRIFDIC);
                   )
          else
            update (i_cTable) set;
                DIIMPUTI = &i_cOp1.;
                &i_ccchkf. ;
             where;
                DISERIAL = this.w_MVRIFDIC;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Nel caso di cancellazione devo inserire un nuovo record sulla tabella DICDINTE
          this.w_RIFDIC = this.w_MVRIFDIC
          this.w_DIDICCOL_FILTRO = Space(10)
          this.w_MVSERIAL = this.w_SERDOC
          * --- Inserisco un nuovo record nella tabella "Dettaglio dichiarazioni di intento"
          *     per tenere traccia dell'operazione. Devo risalire all'ultima riga inserita nella tabella
          * --- Select from DICDINTE9
          do vq_exec with 'DICDINTE9',this,'_Curs_DICDINTE9','',.f.,.t.
          if used('_Curs_DICDINTE9')
            select _Curs_DICDINTE9
            locate for 1=1
            do while not(eof())
            this.w_NUMRIGA = IIF(_Curs_DICDINTE9.ORDINA="A",_Curs_DICDINTE9.CPROWNUM,this.w_NUMRIGA)
              select _Curs_DICDINTE9
              continue
            enddo
            use
          endif
          this.w_NUMRIGA = this.w_NUMRIGA+1
          * --- Insert into DICDINTE
          i_nConn=i_TableProp[this.DICDINTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_MVRIFDIC),'DICDINTE','DDSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'DICDINTE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA*10),'DICDINTE','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_SERDOC),'DICDINTE','DDDOCSER');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TOTSTORN*-1),'DICDINTE','DDIMPDIC');
            +","+cp_NullLink(cp_ToStrODBC("D"),'DICDINTE','DDTIPINS');
            +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
            +","+cp_NullLink(cp_ToStrODBC("C"),'DICDINTE','DDTIPAGG');
            +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
            +","+cp_NullLink(cp_ToStrODBC("N"),'DICDINTE','DDVISMAN');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.w_MVRIFDIC,'CPROWNUM',this.w_NUMRIGA,'CPROWORD',this.w_NUMRIGA*10,'DDDOCSER',this.w_SERDOC,'DDIMPDIC',this.w_TOTSTORN*-1,'DDTIPINS',"D",'DDDATOPE',i_datsys,'DDTIPAGG',"C",'DDCODUTE',i_codute,'DDVISMAN',"N")
            insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN &i_ccchkf. );
               values (;
                 this.w_MVRIFDIC;
                 ,this.w_NUMRIGA;
                 ,this.w_NUMRIGA*10;
                 ,this.w_SERDOC;
                 ,this.w_TOTSTORN*-1;
                 ,"D";
                 ,i_datsys;
                 ,"C";
                 ,i_codute;
                 ,"N";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      endif
      * --- Delete from DOC_MAST
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOC);
               )
      else
        delete from (i_cTable) where;
              MVSERIAL = this.w_SERDOC;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      if !this.w_FLPIASPE
        * --- Delete from RAG_FATT
        i_nConn=i_TableProp[this.RAG_FATT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RAG_FATT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"FRSERFAT = "+cp_ToStrODBC(this.w_SERDOC);
                 )
        else
          delete from (i_cTable) where;
                FRSERFAT = this.w_SERDOC;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
      * --- Provo a ripristinare i progressivi 
      *     (solo se sono in cancellazione dell'ultimo)
      GSAR_BRP(this,this.w_FLVEAC, this.w_ANNDOC, this.w_PRD, this.w_NUMDOC, this.w_ALFDOC, this.w_PRP, this.w_NUMEST, this.w_ALFEST, this.w_ANNPRO, this.w_CLADOC)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    SELECT ListaCan
    ENDSCAN
    * --- Al termine dell'elaborazione, se elimino l'intero piano di fatturazione allora
    *     riapro le righe descrittive eventualmente chiuse (Fatt. per Ordine)
    if this.w_GEST_RIG
      * --- Ho eliminato tutte le fatture del piano ?
      if GSVE_BGE( This, "CHECK" , this.oParentObject.w_DPSERIAL )
        GSVE_BGE(this, "RIAPRE" , this.oParentObject.w_DPSERIAL )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc
  proc Try_05326788()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVRIFFAD ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_MAST','MVRIFFAD');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOC);
             )
    else
      update (i_cTable) set;
          MVRIFFAD = SPACE(10);
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_SERDOC;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Storno saldi
    * --- Read from DOC_DETT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVCODMAG,MVFLRISE,MVFLORDI,MVFLIMPE,MVCODMAT,MVF2RISE,MVF2ORDI,MVF2IMPE,MVQTASAL,MVKEYSAL,MVFLCOCO,MVFLORCO,MVCODCOS,MVTIPATT,MVCODCOM,MVCODATT,MVIMPCOM,MVFLCASC,MVF2CASC,MVCODART,MVCODLOT,MVCODUBI,MVCODUB2"+;
        " from "+i_cTable+" DOC_DETT where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWRIF);
            +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVCODMAG,MVFLRISE,MVFLORDI,MVFLIMPE,MVCODMAT,MVF2RISE,MVF2ORDI,MVF2IMPE,MVQTASAL,MVKEYSAL,MVFLCOCO,MVFLORCO,MVCODCOS,MVTIPATT,MVCODCOM,MVCODATT,MVIMPCOM,MVFLCASC,MVF2CASC,MVCODART,MVCODLOT,MVCODUBI,MVCODUB2;
        from (i_cTable) where;
            MVSERIAL = this.w_MVSERRIF;
            and CPROWNUM = this.w_MVROWRIF;
            and MVNUMRIF = this.w_MVNUMRIF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVCODMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
      this.w_MVFLRISE = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
      this.w_MVFLORDI = NVL(cp_ToDate(_read_.MVFLORDI),cp_NullValue(_read_.MVFLORDI))
      this.w_MVFLIMPE = NVL(cp_ToDate(_read_.MVFLIMPE),cp_NullValue(_read_.MVFLIMPE))
      this.w_MVCODMAT = NVL(cp_ToDate(_read_.MVCODMAT),cp_NullValue(_read_.MVCODMAT))
      this.w_MVF2RISE = NVL(cp_ToDate(_read_.MVF2RISE),cp_NullValue(_read_.MVF2RISE))
      this.w_MVF2ORDI = NVL(cp_ToDate(_read_.MVF2ORDI),cp_NullValue(_read_.MVF2ORDI))
      this.w_MVF2IMPE = NVL(cp_ToDate(_read_.MVF2IMPE),cp_NullValue(_read_.MVF2IMPE))
      this.w_QTASAL = NVL(cp_ToDate(_read_.MVQTASAL),cp_NullValue(_read_.MVQTASAL))
      this.w_MVKEYSAL = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
      this.w_MVFLCOCO = NVL(cp_ToDate(_read_.MVFLCOCO),cp_NullValue(_read_.MVFLCOCO))
      this.w_MVFLORCO = NVL(cp_ToDate(_read_.MVFLORCO),cp_NullValue(_read_.MVFLORCO))
      this.w_MVCODCOS = NVL(cp_ToDate(_read_.MVCODCOS),cp_NullValue(_read_.MVCODCOS))
      this.w_MVTIPATT = NVL(cp_ToDate(_read_.MVTIPATT),cp_NullValue(_read_.MVTIPATT))
      this.w_MVCODCOM = NVL(cp_ToDate(_read_.MVCODCOM),cp_NullValue(_read_.MVCODCOM))
      this.w_MVCODATT = NVL(cp_ToDate(_read_.MVCODATT),cp_NullValue(_read_.MVCODATT))
      this.w_MVIMPCOM = NVL(cp_ToDate(_read_.MVIMPCOM),cp_NullValue(_read_.MVIMPCOM))
      this.w_MVFLCASC = NVL(cp_ToDate(_read_.MVFLCASC),cp_NullValue(_read_.MVFLCASC))
      this.w_MVF2CASC = NVL(cp_ToDate(_read_.MVF2CASC),cp_NullValue(_read_.MVF2CASC))
      this.w_MVCODART = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
      this.w_MVCODLOT = NVL(cp_ToDate(_read_.MVCODLOT),cp_NullValue(_read_.MVCODLOT))
      this.w_MVCODUBI = NVL(cp_ToDate(_read_.MVCODUBI),cp_NullValue(_read_.MVCODUBI))
      this.w_MVCODUB2 = NVL(cp_ToDate(_read_.MVCODUB2),cp_NullValue(_read_.MVCODUB2))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_FLGSAL = " "
    * --- Solo se evado un documento che evade ordinato o impegnato, quindi se fatturo Ordini
    if NOT EMPTY(this.w_MVFLORDI+this.w_MVFLIMPE+this.w_MVF2ORDI+this.w_MVF2IMPE+this.w_MVFLCASC+this.w_MVF2CASC)
      * --- Aggiorno saldi MVCODMAG
      this.w_MVFLIMPE = IIF(EMPTY(this.w_MVFLARIF) AND this.w_DCFLEVAS="S", " ", this.w_MVFLIMPE)
      this.w_MVFLORDI = IIF(EMPTY(this.w_MVFLARIF) AND this.w_DCFLEVAS="S", " ", this.w_MVFLORDI)
      this.w_MVF2IMPE = IIF(EMPTY(this.w_MVFLARIF) AND this.w_DCFLEVAS="S", " ", this.w_MVF2IMPE)
      this.w_MVF2ORDI = IIF(EMPTY(this.w_MVFLARIF) AND this.w_DCFLEVAS="S", " ", this.w_MVF2ORDI)
      this.w_MVFLCASC = IIF( (EMPTY(this.w_MVFLARIF) AND this.w_DCFLEVAS="S") OR this.pOper$"TR", " ", this.w_MVFLCASC)
      this.w_MVF2CASC = IIF( (EMPTY(this.w_MVFLARIF) AND this.w_DCFLEVAS="S") OR this.pOper$"TR", " ", this.w_MVF2CASC)
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_MVFLORDI,'SLQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MVFLIMPE,'SLQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_MVFLCASC,'SLQTAPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
        +",SLQTAPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTAPER');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
               )
      else
        update (i_cTable) set;
            SLQTOPER = &i_cOp1.;
            ,SLQTIPER = &i_cOp2.;
            ,SLQTAPER = &i_cOp3.;
            &i_ccchkf. ;
         where;
            SLCODICE = this.w_MVKEYSAL;
            and SLCODMAG = this.w_MVCODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARSALCOM"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARSALCOM;
          from (i_cTable) where;
              ARCODART = this.w_MVCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SALCOM="S"
        if empty(nvl(this.w_MVCODCOM,""))
          this.w_COMMAPPO = this.w_COMMDEFA
        else
          this.w_COMMAPPO = this.w_MVCODCOM
        endif
        * --- Write into SALDICOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_MVFLORDI,'SCQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_MVFLIMPE,'SCQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_MVFLCASC,'SCQTAPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
          +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
          +",SCQTAPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTAPER');
              +i_ccchkf ;
          +" where ";
              +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
              +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                 )
        else
          update (i_cTable) set;
              SCQTOPER = &i_cOp1.;
              ,SCQTIPER = &i_cOp2.;
              ,SCQTAPER = &i_cOp3.;
              &i_ccchkf. ;
           where;
              SCCODICE = this.w_MVKEYSAL;
              and SCCODMAG = this.w_MVCODMAG;
              and SCCODCAN = this.w_COMMAPPO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore Aggiornamento Saldi Commessa'
          return
        endif
      endif
      * --- Aggiorno saldi MVCODMAT
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_MVF2ORDI,'SLQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_MVF2IMPE,'SLQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_MVF2CASC,'SLQTAPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
        +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
        +",SLQTAPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTAPER');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
               )
      else
        update (i_cTable) set;
            SLQTOPER = &i_cOp1.;
            ,SLQTIPER = &i_cOp2.;
            ,SLQTAPER = &i_cOp3.;
            &i_ccchkf. ;
         where;
            SLCODICE = this.w_MVKEYSAL;
            and SLCODMAG = this.w_MVCODMAT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if this.w_SALCOM="S"
        * --- Write into SALDICOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_MVF2ORDI,'SCQTOPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_MVF2IMPE,'SCQTIPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_MVF2CASC,'SCQTAPER','this.w_MVQTASAL',this.w_MVQTASAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
          +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
          +",SCQTAPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTAPER');
              +i_ccchkf ;
          +" where ";
              +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
              +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                 )
        else
          update (i_cTable) set;
              SCQTOPER = &i_cOp1.;
              ,SCQTIPER = &i_cOp2.;
              ,SCQTAPER = &i_cOp3.;
              &i_ccchkf. ;
           where;
              SCCODICE = this.w_MVKEYSAL;
              and SCCODMAG = this.w_MVCODMAT;
              and SCCODCAN = this.w_COMMAPPO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore Aggiornamento Saldi Commessa'
          return
        endif
      endif
      if this.w_FLPIASPE
        this.w_FLGSAL = EVL(this.w_MVFLIMPE,this.w_MVFLORDI)
      else
        this.w_FLGSAL = "="
      endif
      * --- Se gestita e aggiornata commessa la storno
      if g_COMM="S" And Not Empty(Alltrim(this.w_MVFLORCO)+Alltrim(this.w_MVFLCOCO)) And Not Empty(this.w_MVCODATT) And Not Empty(this.w_MVTIPATT) And Not Empty(this.w_MVCODCOS) And Not Empty(this.w_MVCODCOM)
        * --- Inverto i segni perch� sono in cancellazione
        this.w_MVFLORCO = IIF(this.w_MVFLORCO="+","-",IIF(this.w_MVFLORCO="-","+",""))
        this.w_MVFLCOCO = IIF(this.w_MVFLCOCO="+","-",IIF(this.w_MVFLCOCO="-","+",""))
        * --- Write into MA_COSTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.MA_COSTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_MVFLCOCO,'CSCONSUN','this.w_MVIMPCOM',this.w_MVIMPCOM,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_MVFLORCO,'CSORDIN','this.w_MVIMPCOM',this.w_MVIMPCOM,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
          +",CSORDIN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSORDIN');
              +i_ccchkf ;
          +" where ";
              +"CSCODCOM = "+cp_ToStrODBC(this.w_MVCODCOM);
              +" and CSTIPSTR = "+cp_ToStrODBC(this.w_MVTIPATT);
              +" and CSCODMAT = "+cp_ToStrODBC(this.w_MVCODATT);
              +" and CSCODCOS = "+cp_ToStrODBC(this.w_MVCODCOS);
                 )
        else
          update (i_cTable) set;
              CSCONSUN = &i_cOp1.;
              ,CSORDIN = &i_cOp2.;
              &i_ccchkf. ;
           where;
              CSCODCOM = this.w_MVCODCOM;
              and CSTIPSTR = this.w_MVTIPATT;
              and CSCODMAT = this.w_MVCODATT;
              and CSCODCOS = this.w_MVCODCOS;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se necessario riporta indietro la data fatturazione dell'ordine aperto
    * --- Ordine aperto
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVTIPDOC"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVTIPDOC;
        from (i_cTable) where;
            MVSERIAL = this.w_MVSERRIF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MVTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDORDAPE"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDORDAPE;
        from (i_cTable) where;
            TDTIPDOC = this.w_MVTIPDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TDORDAPE = NVL(cp_ToDate(_read_.TDORDAPE),cp_NullValue(_read_.TDORDAPE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_TDORDAPE="S"
      * --- Utilizzo una tabella temporanea perch� sono gi� dentro una select di DOC_DETT
      * --- Create temporary table TMP__DOC
      i_nIdx=cp_AddTableDef('TMP__DOC') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.DOC_DETT_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"MAX(MVSERIAL) AS MVSERIAL "," from "+i_cTable;
            +" where MVSERRIF="+cp_ToStrODBC(this.w_MVSERRIF)+"";
            +" group by MVSERRIF";
            +" order by MVSERIAL";
            )
      this.TMP__DOC_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Select from TMP__DOC
      i_nConn=i_TableProp[this.TMP__DOC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMP__DOC_idx,2],.t.,this.TMP__DOC_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" TMP__DOC ";
             ,"_Curs_TMP__DOC")
      else
        select * from (i_cTable);
          into cursor _Curs_TMP__DOC
      endif
      if used('_Curs_TMP__DOC')
        select _Curs_TMP__DOC
        locate for 1=1
        do while not(eof())
        this.w_MAXSER = _Curs_TMP__DOC.MVSERIAL
        exit
          select _Curs_TMP__DOC
          continue
        enddo
        use
      endif
      * --- Droppo la tabella di appoggio
      * --- Drop temporary table TMP__DOC
      i_nIdx=cp_GetTableDefIdx('TMP__DOC')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMP__DOC')
      endif
      if this.w_MAXSER=this.w_SERDOC
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVDATEVA,MVDATGEN,CPROWORD"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWRIF);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVDATEVA,MVDATGEN,CPROWORD;
            from (i_cTable) where;
                MVSERIAL = this.w_MVSERRIF;
                and CPROWNUM = this.w_MVROWRIF;
                and MVNUMRIF = this.w_MVNUMRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVDATEVA = NVL(cp_ToDate(_read_.MVDATEVA),cp_NullValue(_read_.MVDATEVA))
          this.w_MVDATGEN = NVL(cp_ToDate(_read_.MVDATGEN),cp_NullValue(_read_.MVDATGEN))
          this.w_NUMRIGA = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVNUMDOC,MVALFDOC,MVDATDOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVNUMDOC,MVALFDOC,MVDATDOC;
            from (i_cTable) where;
                MVSERIAL = this.w_MVSERRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OPNUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
          this.w_OPALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
          this.w_OPDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(this.w_MVDATGEN)
          * --- Se � vuota dat gen devo dare un messaggio, Undo pu� essere fatto solo una volta.
          *     Non riesco a riportare indietro la date di prossima fatturazione dell'ordine
          ah_ErrorMsg("Impossibile aggiornare la data di prossima fatturazione dell' ordine aperto%0Sar� necessario aggiornarla manualmente%0Documento numero %1%2 del %3 riga %4", , ,ALLTRIM(STR(this.w_OPNUMDOC)), IIF(Empty(this.w_OPALFDOC), "", "/"+ALLTRIM(this.w_OPALFDOC)), DTOC(this.w_OPDATDOC), ALLTRIM(STR(this.w_NUMRIGA)))
        endif
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_cOp2=cp_SetTrsOp(IIF(Empty(this.w_MVDATEVA), "=", " "),'MVFLEVAS','" "'," ",'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVDATGEN');
          +",MVFLEVAS ="+cp_NullLink(i_cOp2,'DOC_DETT','MVFLEVAS');
          +",MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATGEN),'DOC_DETT','MVDATEVA');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWRIF);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                 )
        else
          update (i_cTable) set;
              MVDATGEN = cp_CharToDate("  -  -  ");
              ,MVFLEVAS = &i_cOp2.;
              ,MVDATEVA = this.w_MVDATGEN;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_MVSERRIF;
              and CPROWNUM = this.w_MVROWRIF;
              and MVNUMRIF = this.w_MVNUMRIF;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,17)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='DOC_RATE'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='RAG_FATT'
    this.cWorkTables[6]='CON_PAGA'
    this.cWorkTables[7]='SALDIART'
    this.cWorkTables[8]='TIP_DOCU'
    this.cWorkTables[9]='*TMP__DOC'
    this.cWorkTables[10]='MA_COSTI'
    this.cWorkTables[11]='DIC_INTE'
    this.cWorkTables[12]='DOC_COLL'
    this.cWorkTables[13]='ART_ICOL'
    this.cWorkTables[14]='SALDICOM'
    this.cWorkTables[15]='PRODINDI'
    this.cWorkTables[16]='PROMINDI'
    this.cWorkTables[17]='DICDINTE'
    return(this.OpenAllTables(17))

  proc CloseCursors()
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_RAG_FATT')
      use in _Curs_RAG_FATT
    endif
    if used('_Curs_DICDINTE9')
      use in _Curs_DICDINTE9
    endif
    if used('_Curs_TMP__DOC')
      use in _Curs_TMP__DOC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
