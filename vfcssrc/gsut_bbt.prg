* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bbt                                                        *
*              Backup automatico con gestione storico                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-02-15                                                      *
* Last revis.: 2013-09-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,nDay,nBackUp,w_RESULT,dDate
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bbt",oParentObject,m.nDay,m.nBackUp,m.w_RESULT,m.dDate)
return(i_retval)

define class tgsut_bbt as StdBatch
  * --- Local variables
  nDay = 0
  nBackUp = 0
  w_RESULT = space(254)
  dDate = ctot("")
  w_PATBCK = space(200)
  w_OriDat = space(40)
  w_user = space(20)
  w_passwd = space(20)
  w_service = space(20)
  w_ZipDB = space(1)
  w_CheckDB = space(1)
  w_ReIndexDb = space(1)
  w_connessi = space(20)
  w_conn1 = space(20)
  w_SERVERNAME = space(20)
  w_RiorgInd = space(1)
  w_BckDB = space(1)
  w_BNOSCHED = .f.
  w_BNOJBSH = .f.
  w_STRDAT = space(254)
  w_OGGI = ctod("  /  /  ")
  w_RES = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Back up automatico con gestione cancellazione back up.
    *     La routine esegue un back up attribuendo un nome al file di back up composto
    *     da
    *     Anno
    *     Mese
    *     Giono
    *     Ora
    *     Minuto
    *     Secondo (se passato dDate dati ricavati da questo altrimenti i_datsys
    *     Nome database
    *     .bak
    *     Prima dell'esecuzione del back up rimuove i back up pi� vecchi di una certa data
    *     oppure mantiene solo un certo numero di back up in linea..
    *     I back up saranno creati all'interno della cartella definita in azienda per i
    *     backup
    * --- nDay = Numero di giorni da manternere in linea
    *     nBackup = Numero di back up da mantenere in linea
    *     (se parametri passati a 0 nessuna operazione sui back up in essere)
    *     Restituisce '' se tutto ok altrimenti il messaggio di errore nel parametro
    *     w_RESULT passato per riferimento
    * --- Variabili per gestione riorganizzazione indici
    this.w_RiorgInd = "N"
    this.w_BckDB = "S"
    * --- Per gestione messaggistica di ritorno.
    *     Non posso visualizzare messaggi di avviso
    if Type("g_SCHEDULER")<>"C" Or cp_GetGlobalVar("g_SCHEDULER")="N"
      * --- Lo schedulatore � attivo
      cp_SetGlobalVar("g_SCHEDULER","S")
      * --- Ripulisco la variabile contenente il log
      cp_SetGlobalVar("g_MSG","")
      this.w_BNOSCHED = .T.
      if cp_GetGlobalVar("g_JBSH")="N"
        cp_SetGlobalVar("g_JBSH","S")
        this.w_BNOJBSH = .T.
      endif
    endif
    * --- Valorizzo i parametri generali...
    GSUT_BIN(this,"T")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Determino il nome del back up
    * --- La data di riferimento potrebbe essere un parametro...
    if Vartype( this.dDate )="D"
      this.w_OGGI = this.dDate
    else
      this.w_OGGI = DTOT(i_datsys)+VAL(SYS(2))
    endif
    this.w_PATBCK = ADDBS(JUSTPATH(this.w_PATBCK))+NOME_BCK( this.w_OGGI , this.w_ORIDAT ) 
    * --- Se qualcosa va storto mi ritorna .F.
    if GSUT_BBK( this )
      * --- Analizzo i file all'interno della cartella di back up, se richiesta la puliza
      *     dei back up storici...
      this.w_RES = GSBACKUP( ADDBS(JUSTPATH(this.w_PATBCK)) , this.nDay, this.nBackUp, TToD( this.w_OGGI ) )
      if Not Empty( this.w_RES )
        this.w_RESULT = this.w_RES
      endif
    else
      this.w_RESULT = cp_GetGlobalVar("g_MSG")
    endif
    if this.w_BNOSCHED
      * --- Lo schedulatore era disattivo
      cp_SetGlobalVar("g_SCHEDULER","N")
    endif
    if this.w_BNOJBSH
      cp_SetGlobalVar("g_JBSH","N")
    endif
    i_retcode = 'stop'
    i_retval = this.w_RESULT
    return
  endproc


  proc Init(oParentObject,nDay,nBackUp,w_RESULT,dDate)
    this.nDay=nDay
    this.nBackUp=nBackUp
    this.w_RESULT=w_RESULT
    this.dDate=dDate
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="nDay,nBackUp,w_RESULT,dDate"
endproc
