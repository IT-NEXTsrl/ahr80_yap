* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bi1                                                        *
*              Seleziona/deseleziona partite                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_4]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-08                                                      *
* Last revis.: 2011-07-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bi1",oParentObject,m.pOper)
return(i_retval)

define class tgscg_bi1 as StdBatch
  * --- Local variables
  pOper = 0
  w_COUNT = 0
  w_PUN = 0
  oggetto = .NULL.
  w_ZOOM = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Contabilizzazione Indiretta (Lanciato da GSCG_ACI)
    * --- --
    * --- --
    * --- --
    this.w_ZOOM = this.oParentObject.w_CalcZoom
    do case
      case this.pOper=1
        * --- Seleziona/Deseleziona Tutto
        this.oParentObject.w_FLSELE = IIF ( this.oParentObject.w_SELEZI="S" ,1, 0 )
        UPDATE ( this.w_ZOOM.cCursor ) SET XCHK = this.oParentObject.w_FLSELE
        * --- Calcola totale scadenza selezionate
         
 Select ( this.w_ZOOM.cCursor ) 
 GO TOP 
 SUM TOTIMP*iif(SEGNO = "D",1,-1) TO this.oParentObject.w_IMPORTO FOR XCHK=1
        if this.oParentObject.w_SELEZI="S"
          COUNT FOR XCHK=1 AND SEGNO="A" TO this.w_COUNT
          if this.w_COUNT>0
            AH_ERRORMSG("Attenzione sono state selezionate partite di segno opposto",64)
          endif
        endif
      case this.pOper=2 or this.pOper=3
        if (this.pOper=2 And this.oParentObject.w_CIFLDEFI<>"S" ) or (this.pOper=3 And this.oParentObject.w_CIFLDEFI="S")
          this.w_PUN = Recno(this.w_ZOOM.cCursor)
          * --- Calcola totale scadenza selezionate
           
 Select ( this.w_ZOOM.cCursor ) 
 GO TOP
          if this.pOper=2
            SUM TOTIMP*iif(SEGNO = "D",1,-1) TO this.oParentObject.w_IMPORTO FOR XCHK=1
          else
            SUM TOTIMP*iif(SEGNO = "D",1,-1) TO this.oParentObject.w_IMPORTO
          endif
          if this.w_PUN<=Reccount()
            Go this.w_PUN
          endif
        endif
      case this.pOper=4
        if this.oParentObject.w_CIFLDEFI<>"S" 
          COUNT FOR XCHK=1 AND SEGNO="A" TO this.w_COUNT
          if this.w_COUNT>0
            AH_ERRORMSG("Attenzione sono state selezionate partite di segno opposto",64)
          endif
        endif
      case this.pOper=5
        if this.oParentObject.w_CIFLDEFI<>"S" 
          if this.oParentObject.w_MESSEGN="A"
            AH_ERRORMSG("Attenzione � stata selezionata una partita di segno opposto",64)
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
