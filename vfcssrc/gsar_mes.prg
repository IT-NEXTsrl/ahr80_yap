* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mes                                                        *
*              Esercizi                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_84]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2014-10-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mes")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mes")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mes")
  return

* --- Class definition
define class tgsar_mes as StdPCForm
  Width  = 816
  Height = 350
  Top    = -3
  Left   = 10
  cComment = "Esercizi"
  cPrg = "gsar_mes"
  HelpContextID=63534953
  add object cnt as tcgsar_mes
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mes as PCContext
  w_ESCODAZI = space(5)
  w_ESCODESE = space(4)
  w_ESINIESE = space(8)
  w_ESFINESE = space(8)
  w_ESVALNAZ = space(3)
  w_DESAPP = space(35)
  w_ESPROLIG = 0
  w_ESDARLIG = 0
  w_ESAVELIG = 0
  w_ESFLSTAM = space(1)
  w_ESSTAINT = space(8)
  w_OCODESE = space(4)
  w_OINIESE = space(8)
  w_OFINESE = space(8)
  w_OVALNAZ = space(3)
  w_DATOBSO = space(8)
  w_TCAOVAL = 0
  w_OBTEST = space(8)
  w_AZICAS = space(5)
  w_CONCAS = space(1)
  w_TOTREGDEF = 0
  w_STALIG = space(8)
  proc Save(i_oFrom)
    this.w_ESCODAZI = i_oFrom.w_ESCODAZI
    this.w_ESCODESE = i_oFrom.w_ESCODESE
    this.w_ESINIESE = i_oFrom.w_ESINIESE
    this.w_ESFINESE = i_oFrom.w_ESFINESE
    this.w_ESVALNAZ = i_oFrom.w_ESVALNAZ
    this.w_DESAPP = i_oFrom.w_DESAPP
    this.w_ESPROLIG = i_oFrom.w_ESPROLIG
    this.w_ESDARLIG = i_oFrom.w_ESDARLIG
    this.w_ESAVELIG = i_oFrom.w_ESAVELIG
    this.w_ESFLSTAM = i_oFrom.w_ESFLSTAM
    this.w_ESSTAINT = i_oFrom.w_ESSTAINT
    this.w_OCODESE = i_oFrom.w_OCODESE
    this.w_OINIESE = i_oFrom.w_OINIESE
    this.w_OFINESE = i_oFrom.w_OFINESE
    this.w_OVALNAZ = i_oFrom.w_OVALNAZ
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_TCAOVAL = i_oFrom.w_TCAOVAL
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_AZICAS = i_oFrom.w_AZICAS
    this.w_CONCAS = i_oFrom.w_CONCAS
    this.w_TOTREGDEF = i_oFrom.w_TOTREGDEF
    this.w_STALIG = i_oFrom.w_STALIG
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_ESCODAZI = this.w_ESCODAZI
    i_oTo.w_ESCODESE = this.w_ESCODESE
    i_oTo.w_ESINIESE = this.w_ESINIESE
    i_oTo.w_ESFINESE = this.w_ESFINESE
    i_oTo.w_ESVALNAZ = this.w_ESVALNAZ
    i_oTo.w_DESAPP = this.w_DESAPP
    i_oTo.w_ESPROLIG = this.w_ESPROLIG
    i_oTo.w_ESDARLIG = this.w_ESDARLIG
    i_oTo.w_ESAVELIG = this.w_ESAVELIG
    i_oTo.w_ESFLSTAM = this.w_ESFLSTAM
    i_oTo.w_ESSTAINT = this.w_ESSTAINT
    i_oTo.w_OCODESE = this.w_OCODESE
    i_oTo.w_OINIESE = this.w_OINIESE
    i_oTo.w_OFINESE = this.w_OFINESE
    i_oTo.w_OVALNAZ = this.w_OVALNAZ
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_TCAOVAL = this.w_TCAOVAL
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_AZICAS = this.w_AZICAS
    i_oTo.w_CONCAS = this.w_CONCAS
    i_oTo.w_TOTREGDEF = this.w_TOTREGDEF
    i_oTo.w_STALIG = this.w_STALIG
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mes as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 816
  Height = 350
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-27"
  HelpContextID=63534953
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=22

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  PRO_NUME_IDX = 0
  AZIENDA_IDX = 0
  GIORMAGA_IDX = 0
  cFile = "ESERCIZI"
  cKeySelect = "ESCODAZI"
  cKeyWhere  = "ESCODAZI=this.w_ESCODAZI"
  cKeyDetail  = "ESCODAZI=this.w_ESCODAZI and ESCODESE=this.w_ESCODESE"
  cKeyWhereODBC = '"ESCODAZI="+cp_ToStrODBC(this.w_ESCODAZI)';

  cKeyDetailWhereODBC = '"ESCODAZI="+cp_ToStrODBC(this.w_ESCODAZI)';
      +'+" and ESCODESE="+cp_ToStrODBC(this.w_ESCODESE)';

  cKeyWhereODBCqualified = '"ESERCIZI.ESCODAZI="+cp_ToStrODBC(this.w_ESCODAZI)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsar_mes"
  cComment = "Esercizi"
  i_nRowNum = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  cAutoZoom = 'GSAR0MES'
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ESCODAZI = space(5)
  o_ESCODAZI = space(5)
  w_ESCODESE = space(4)
  w_ESINIESE = ctod('  /  /  ')
  w_ESFINESE = ctod('  /  /  ')
  w_ESVALNAZ = space(3)
  o_ESVALNAZ = space(3)
  w_DESAPP = space(35)
  w_ESPROLIG = 0
  w_ESDARLIG = 0
  w_ESAVELIG = 0
  w_ESFLSTAM = space(1)
  w_ESSTAINT = ctod('  /  /  ')
  w_OCODESE = space(4)
  w_OINIESE = ctod('  /  /  ')
  w_OFINESE = ctod('  /  /  ')
  w_OVALNAZ = space(3)
  w_DATOBSO = ctod('  /  /  ')
  w_TCAOVAL = 0
  w_OBTEST = ctod('  /  /  ')
  w_AZICAS = space(5)
  w_CONCAS = space(1)
  w_TOTREGDEF = 0
  w_STALIG = ctod('  /  /  ')

  * --- Children pointers
  GSAR_MPR = .NULL.
  GSAR_MGM = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSAR_MPR additive
    *set procedure to GSAR_MGM additive
    with this
      .Pages(1).addobject("oPag","tgsar_mesPag1","gsar_mes",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSAR_MPR
    *release procedure GSAR_MGM
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='PRO_NUME'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='GIORMAGA'
    this.cWorkTables[5]='ESERCIZI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ESERCIZI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ESERCIZI_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MPR = CREATEOBJECT('stdDynamicChild',this,'GSAR_MPR',this.oPgFrm.Page1.oPag.oLinkPC_2_11)
    this.GSAR_MPR.createrealchild()
    this.GSAR_MGM = CREATEOBJECT('stdDynamicChild',this,'GSAR_MGM',this.oPgFrm.Page1.oPag.oLinkPC_2_12)
    this.GSAR_MGM.createrealchild()
    return

  procedure NewContext()
    return(createobject('tsgsar_mes'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSAR_MPR)
      this.GSAR_MPR.DestroyChildrenChain()
    endif
    if !ISNULL(this.GSAR_MGM)
      this.GSAR_MGM.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSAR_MPR.HideChildrenChain()
    this.GSAR_MGM.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSAR_MPR.ShowChildrenChain()
    this.GSAR_MGM.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MPR)
      this.GSAR_MPR.DestroyChildrenChain()
      this.GSAR_MPR=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_11')
    if !ISNULL(this.GSAR_MGM)
      this.GSAR_MGM.DestroyChildrenChain()
      this.GSAR_MGM=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_12')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MPR.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MGM.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MPR.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MGM.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MPR.NewDocument()
    this.GSAR_MGM.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSAR_MPR.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_ESCODAZI,"PRCODAZI";
             ,.w_ESCODESE,"PRCODESE";
             )
      .GSAR_MGM.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_ESCODAZI,"GMCODAZI";
             ,.w_ESCODESE,"GMCODESE";
             )
    endwith
    select (i_cOldSel)
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_4_joined
    link_2_4_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ESERCIZI where ESCODAZI=KeySet.ESCODAZI
    *                            and ESCODESE=KeySet.ESCODESE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsar_mes
      * --- Setta Ordine per Esercizio dal + recente
      i_cOrder = 'order by ESINIESE Desc '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2],this.bLoadRecFilter,this.ESERCIZI_IDX,"gsar_mes")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ESERCIZI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ESERCIZI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ESERCIZI '
      link_2_4_joined=this.AddJoinedLink_2_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ESCODAZI',this.w_ESCODAZI  )
      select * from (i_cTable) ESERCIZI where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AZICAS = .w_ESCODAZI
        .w_CONCAS = space(1)
        .w_STALIG = ctod("  /  /  ")
        .w_ESCODAZI = NVL(ESCODAZI,space(5))
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
          .link_1_6('Load')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ESERCIZI')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESAPP = space(35)
          .w_DATOBSO = ctod("  /  /  ")
          .w_TOTREGDEF = 0
          .w_ESCODESE = NVL(ESCODESE,space(4))
          .w_ESINIESE = NVL(cp_ToDate(ESINIESE),ctod("  /  /  "))
          .w_ESFINESE = NVL(cp_ToDate(ESFINESE),ctod("  /  /  "))
          .w_ESVALNAZ = NVL(ESVALNAZ,space(3))
          if link_2_4_joined
            this.w_ESVALNAZ = NVL(VACODVAL204,NVL(this.w_ESVALNAZ,space(3)))
            this.w_DESAPP = NVL(VADESVAL204,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(VADTOBSO204),ctod("  /  /  "))
          else
          .link_2_4('Load')
          endif
          .w_ESPROLIG = NVL(ESPROLIG,0)
          .w_ESDARLIG = NVL(ESDARLIG,0)
          .w_ESAVELIG = NVL(ESAVELIG,0)
          .w_ESFLSTAM = NVL(ESFLSTAM,space(1))
          .w_ESSTAINT = NVL(cp_ToDate(ESSTAINT),ctod("  /  /  "))
        .w_OCODESE = .w_ESCODESE
        .w_OINIESE = .w_ESINIESE
        .w_OFINESE = .w_ESFINESE
        .w_OVALNAZ = .w_ESVALNAZ
        .w_TCAOVAL = GETCAM(.w_ESVALNAZ,.w_ESFINESE)
        .w_OBTEST = IIF(EMPTY(.w_ESFINESE), i_DATSYS, .w_ESFINESE)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace ESCODESE with .w_ESCODESE
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_ESCODAZI=space(5)
      .w_ESCODESE=space(4)
      .w_ESINIESE=ctod("  /  /  ")
      .w_ESFINESE=ctod("  /  /  ")
      .w_ESVALNAZ=space(3)
      .w_DESAPP=space(35)
      .w_ESPROLIG=0
      .w_ESDARLIG=0
      .w_ESAVELIG=0
      .w_ESFLSTAM=space(1)
      .w_ESSTAINT=ctod("  /  /  ")
      .w_OCODESE=space(4)
      .w_OINIESE=ctod("  /  /  ")
      .w_OFINESE=ctod("  /  /  ")
      .w_OVALNAZ=space(3)
      .w_DATOBSO=ctod("  /  /  ")
      .w_TCAOVAL=0
      .w_OBTEST=ctod("  /  /  ")
      .w_AZICAS=space(5)
      .w_CONCAS=space(1)
      .w_TOTREGDEF=0
      .w_STALIG=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .DoRTCalc(1,5,.f.)
        if not(empty(.w_ESVALNAZ))
         .link_2_4('Full')
        endif
        .DoRTCalc(6,11,.f.)
        .w_OCODESE = .w_ESCODESE
        .w_OINIESE = .w_ESINIESE
        .w_OFINESE = .w_ESFINESE
        .w_OVALNAZ = .w_ESVALNAZ
        .DoRTCalc(16,16,.f.)
        .w_TCAOVAL = GETCAM(.w_ESVALNAZ,.w_ESFINESE)
        .w_OBTEST = IIF(EMPTY(.w_ESFINESE), i_DATSYS, .w_ESFINESE)
        .w_AZICAS = .w_ESCODAZI
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_AZICAS))
         .link_1_6('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ESERCIZI')
    this.DoRTCalc(20,22,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oObj_1_2.enabled = i_bVal
      .Page1.oPag.oObj_1_5.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSAR_MPR.SetStatus(i_cOp)
    this.GSAR_MGM.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'ESERCIZI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MPR.SetChildrenStatus(i_cOp)
  *  this.GSAR_MGM.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ESCODAZI,"ESCODAZI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_ESCODESE C(4);
      ,t_ESINIESE D(8);
      ,t_ESFINESE D(8);
      ,t_ESVALNAZ C(3);
      ,t_ESPROLIG N(9);
      ,t_ESDARLIG N(18,4);
      ,t_ESAVELIG N(18,4);
      ,t_ESFLSTAM N(3);
      ,t_ESSTAINT D(8);
      ,t_TOTREGDEF N(6);
      ,ESCODESE C(4);
      ,t_DESAPP C(35);
      ,t_OCODESE C(4);
      ,t_OINIESE D(8);
      ,t_OFINESE D(8);
      ,t_OVALNAZ C(3);
      ,t_DATOBSO D(8);
      ,t_TCAOVAL N(12,6);
      ,t_OBTEST D(8);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mesbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oESCODESE_2_1.controlsource=this.cTrsName+'.t_ESCODESE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oESINIESE_2_2.controlsource=this.cTrsName+'.t_ESINIESE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oESFINESE_2_3.controlsource=this.cTrsName+'.t_ESFINESE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oESVALNAZ_2_4.controlsource=this.cTrsName+'.t_ESVALNAZ'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oESPROLIG_2_6.controlsource=this.cTrsName+'.t_ESPROLIG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oESDARLIG_2_7.controlsource=this.cTrsName+'.t_ESDARLIG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oESAVELIG_2_8.controlsource=this.cTrsName+'.t_ESAVELIG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oESFLSTAM_2_9.controlsource=this.cTrsName+'.t_ESFLSTAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oESSTAINT_2_10.controlsource=this.cTrsName+'.t_ESSTAINT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTOTREGDEF_2_20.controlsource=this.cTrsName+'.t_TOTREGDEF'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(49)
    this.AddVLine(130)
    this.AddVLine(212)
    this.AddVLine(263)
    this.AddVLine(347)
    this.AddVLine(468)
    this.AddVLine(595)
    this.AddVLine(626)
    this.AddVLine(700)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESCODESE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
      *
      * insert into ESERCIZI
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ESERCIZI')
        i_extval=cp_InsertValODBCExtFlds(this,'ESERCIZI')
        i_cFldBody=" "+;
                  "(ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ"+;
                  ",ESPROLIG,ESDARLIG,ESAVELIG,ESFLSTAM,ESSTAINT,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_ESCODAZI)+","+cp_ToStrODBC(this.w_ESCODESE)+","+cp_ToStrODBC(this.w_ESINIESE)+","+cp_ToStrODBC(this.w_ESFINESE)+","+cp_ToStrODBCNull(this.w_ESVALNAZ)+;
             ","+cp_ToStrODBC(this.w_ESPROLIG)+","+cp_ToStrODBC(this.w_ESDARLIG)+","+cp_ToStrODBC(this.w_ESAVELIG)+","+cp_ToStrODBC(this.w_ESFLSTAM)+","+cp_ToStrODBC(this.w_ESSTAINT)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ESERCIZI')
        i_extval=cp_InsertValVFPExtFlds(this,'ESERCIZI')
        cp_CheckDeletedKey(i_cTable,0,'ESCODAZI',this.w_ESCODAZI,'ESCODESE',this.w_ESCODESE)
        INSERT INTO (i_cTable) (;
                   ESCODAZI;
                  ,ESCODESE;
                  ,ESINIESE;
                  ,ESFINESE;
                  ,ESVALNAZ;
                  ,ESPROLIG;
                  ,ESDARLIG;
                  ,ESAVELIG;
                  ,ESFLSTAM;
                  ,ESSTAINT;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_ESCODAZI;
                  ,this.w_ESCODESE;
                  ,this.w_ESINIESE;
                  ,this.w_ESFINESE;
                  ,this.w_ESVALNAZ;
                  ,this.w_ESPROLIG;
                  ,this.w_ESDARLIG;
                  ,this.w_ESAVELIG;
                  ,this.w_ESFLSTAM;
                  ,this.w_ESSTAINT;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_ESCODESE<>space(4)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ESERCIZI')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and ESCODESE="+cp_ToStrODBC(&i_TN.->ESCODESE)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ESERCIZI')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and ESCODESE=&i_TN.->ESCODESE;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_ESCODESE<>space(4)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete row children
              this.GSAR_MPR.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_ESCODAZI,"PRCODAZI";
                     ,this.w_ESCODESE,"PRCODESE";
                     )
              this.GSAR_MGM.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_ESCODAZI,"GMCODAZI";
                     ,this.w_ESCODESE,"GMCODESE";
                     )
              this.GSAR_MPR.mDelete()
              this.GSAR_MGM.mDelete()
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and ESCODESE="+cp_ToStrODBC(&i_TN.->ESCODESE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and ESCODESE=&i_TN.->ESCODESE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace ESCODESE with this.w_ESCODESE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ESERCIZI
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ESERCIZI')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " ESINIESE="+cp_ToStrODBC(this.w_ESINIESE)+;
                     ",ESFINESE="+cp_ToStrODBC(this.w_ESFINESE)+;
                     ",ESVALNAZ="+cp_ToStrODBCNull(this.w_ESVALNAZ)+;
                     ",ESPROLIG="+cp_ToStrODBC(this.w_ESPROLIG)+;
                     ",ESDARLIG="+cp_ToStrODBC(this.w_ESDARLIG)+;
                     ",ESAVELIG="+cp_ToStrODBC(this.w_ESAVELIG)+;
                     ",ESFLSTAM="+cp_ToStrODBC(this.w_ESFLSTAM)+;
                     ",ESSTAINT="+cp_ToStrODBC(this.w_ESSTAINT)+;
                     ",ESCODESE="+cp_ToStrODBC(this.w_ESCODESE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and ESCODESE="+cp_ToStrODBC(ESCODESE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ESERCIZI')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      ESINIESE=this.w_ESINIESE;
                     ,ESFINESE=this.w_ESFINESE;
                     ,ESVALNAZ=this.w_ESVALNAZ;
                     ,ESPROLIG=this.w_ESPROLIG;
                     ,ESDARLIG=this.w_ESDARLIG;
                     ,ESAVELIG=this.w_ESAVELIG;
                     ,ESFLSTAM=this.w_ESFLSTAM;
                     ,ESSTAINT=this.w_ESSTAINT;
                     ,ESCODESE=this.w_ESCODESE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and ESCODESE=&i_TN.->ESCODESE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask to row children to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (t_ESCODESE<>space(4))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        if not(deleted())
          this.GSAR_MPR.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_ESCODAZI,"PRCODAZI";
               ,this.w_ESCODESE,"PRCODESE";
               )
          this.GSAR_MPR.mReplace()
          this.GSAR_MGM.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_ESCODAZI,"GMCODAZI";
               ,this.w_ESCODESE,"GMCODESE";
               )
          this.GSAR_MGM.mReplace()
          this.GSAR_MPR.bSaveContext=.f.
          this.GSAR_MGM.bSaveContext=.f.
        endif
      endscan
     this.GSAR_MPR.bSaveContext=.t.
     this.GSAR_MGM.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_ESCODESE<>space(4)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSAR_MPR : Deleting
        this.GSAR_MPR.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_ESCODAZI,"PRCODAZI";
               ,this.w_ESCODESE,"PRCODESE";
               )
        this.GSAR_MPR.mDelete()
        * --- GSAR_MGM : Deleting
        this.GSAR_MGM.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_ESCODAZI,"GMCODAZI";
               ,this.w_ESCODESE,"GMCODESE";
               )
        this.GSAR_MGM.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ESERCIZI
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and ESCODESE="+cp_ToStrODBC(&i_TN.->ESCODESE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and ESCODESE=&i_TN.->ESCODESE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_ESCODESE<>space(4)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .DoRTCalc(1,11,.t.)
        if .o_ESCODAZI<>.w_ESCODAZI
          .w_OCODESE = .w_ESCODESE
        endif
        if .o_ESCODAZI<>.w_ESCODAZI
          .w_OINIESE = .w_ESINIESE
        endif
        if .o_ESCODAZI<>.w_ESCODAZI
          .w_OFINESE = .w_ESFINESE
        endif
        if .o_ESCODAZI<>.w_ESCODAZI
          .w_OVALNAZ = .w_ESVALNAZ
        endif
        .DoRTCalc(16,16,.t.)
        if .o_ESVALNAZ<>.w_ESVALNAZ
          .w_TCAOVAL = GETCAM(.w_ESVALNAZ,.w_ESFINESE)
        endif
          .w_OBTEST = IIF(EMPTY(.w_ESFINESE), i_DATSYS, .w_ESFINESE)
          .link_1_6('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(20,22,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DESAPP with this.w_DESAPP
      replace t_OCODESE with this.w_OCODESE
      replace t_OINIESE with this.w_OINIESE
      replace t_OFINESE with this.w_OFINESE
      replace t_OVALNAZ with this.w_OVALNAZ
      replace t_DATOBSO with this.w_DATOBSO
      replace t_TCAOVAL with this.w_TCAOVAL
      replace t_OBTEST with this.w_OBTEST
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_IFYGBDVIRD()
    with this
          * --- Calcolo totale registrazioni stampate in definitiva nel libro giornale
          Gsar_Bce(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oESINIESE_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oESINIESE_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oESFINESE_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oESFINESE_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oESVALNAZ_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oESVALNAZ_2_4.mCond()
    this.GSAR_MPR.enabled = this.oPgFrm.Page1.oPag.oLinkPC_2_11.mCond()
    this.GSAR_MGM.enabled = this.oPgFrm.Page1.oPag.oLinkPC_2_12.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_4.visible=!this.oPgFrm.Page1.oPag.oStr_1_4.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oLinkPC_2_12.visible=!this.oPgFrm.Page1.oPag.oLinkPC_2_12.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_2.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
        if lower(cEvent)==lower("Load") or lower(cEvent)==lower("Totale_Reg")
          .Calculate_IFYGBDVIRD()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ESVALNAZ
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESVALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_ESVALNAZ)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_ESVALNAZ))
          select VACODVAL,VADESVAL,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESVALNAZ)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_ESVALNAZ)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_ESVALNAZ)+"%");

            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ESVALNAZ) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oESVALNAZ_2_4'),i_cWhere,'GSAR_AVL',"Valute",'VALUEZOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESVALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_ESVALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_ESVALNAZ)
            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESVALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_DESAPP = NVL(_Link_.VADESVAL,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESVALNAZ = space(3)
      endif
      this.w_DESAPP = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TCAOVAL>0 AND CHKDTOBS(.w_DATOBSO,.w_ESFINESE,"Valuta obsoleta alla data Attuale!", .F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta incongruente o obsoleta")
        endif
        this.w_ESVALNAZ = space(3)
        this.w_DESAPP = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESVALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_4.VACODVAL as VACODVAL204"+ ",link_2_4.VADESVAL as VADESVAL204"+ ",link_2_4.VADTOBSO as VADTOBSO204"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_4 on ESERCIZI.ESVALNAZ=link_2_4.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_4"
          i_cKey=i_cKey+'+" and ESERCIZI.ESVALNAZ=link_2_4.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AZICAS
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZICAS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZICAS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCONCAS,AZSTALIG";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZICAS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZICAS)
            select AZCODAZI,AZCONCAS,AZSTALIG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZICAS = NVL(_Link_.AZCODAZI,space(5))
      this.w_CONCAS = NVL(_Link_.AZCONCAS,space(1))
      this.w_STALIG = NVL(cp_ToDate(_Link_.AZSTALIG),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AZICAS = space(5)
      endif
      this.w_CONCAS = space(1)
      this.w_STALIG = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZICAS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESCODESE_2_1.value==this.w_ESCODESE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESCODESE_2_1.value=this.w_ESCODESE
      replace t_ESCODESE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESCODESE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESINIESE_2_2.value==this.w_ESINIESE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESINIESE_2_2.value=this.w_ESINIESE
      replace t_ESINIESE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESINIESE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESFINESE_2_3.value==this.w_ESFINESE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESFINESE_2_3.value=this.w_ESFINESE
      replace t_ESFINESE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESFINESE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESVALNAZ_2_4.value==this.w_ESVALNAZ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESVALNAZ_2_4.value=this.w_ESVALNAZ
      replace t_ESVALNAZ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESVALNAZ_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESPROLIG_2_6.value==this.w_ESPROLIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESPROLIG_2_6.value=this.w_ESPROLIG
      replace t_ESPROLIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESPROLIG_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESDARLIG_2_7.value==this.w_ESDARLIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESDARLIG_2_7.value=this.w_ESDARLIG
      replace t_ESDARLIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESDARLIG_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESAVELIG_2_8.value==this.w_ESAVELIG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESAVELIG_2_8.value=this.w_ESAVELIG
      replace t_ESAVELIG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESAVELIG_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESFLSTAM_2_9.RadioValue()==this.w_ESFLSTAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESFLSTAM_2_9.SetRadio()
      replace t_ESFLSTAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESFLSTAM_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESSTAINT_2_10.value==this.w_ESSTAINT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESSTAINT_2_10.value=this.w_ESSTAINT
      replace t_ESSTAINT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESSTAINT_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTOTREGDEF_2_20.value==this.w_TOTREGDEF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTOTREGDEF_2_20.value=this.w_TOTREGDEF
      replace t_TOTREGDEF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTOTREGDEF_2_20.value
    endif
    cp_SetControlsValueExtFlds(this,'ESERCIZI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   (empty(.w_ESINIESE) or not(.w_ESINIESE<=.w_ESFINESE OR EMPTY(.w_ESFINESE))) and (NOT EMPTY(.w_ESCODESE)) and (.w_ESCODESE<>space(4))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESINIESE_2_2
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   (empty(.w_ESFINESE) or not(.w_ESFINESE>=.w_ESINIESE)) and (NOT EMPTY(.w_ESCODESE)) and (.w_ESCODESE<>space(4))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESFINESE_2_3
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   (empty(.w_ESVALNAZ) or not(.w_TCAOVAL>0 AND CHKDTOBS(.w_DATOBSO,.w_ESFINESE,"Valuta obsoleta alla data Attuale!", .F.))) and (NOT EMPTY(.w_ESCODESE)) and (.w_ESCODESE<>space(4))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESVALNAZ_2_4
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Valuta incongruente o obsoleta")
      endcase
      i_bRes = i_bRes .and. .GSAR_MPR.CheckForm()
      i_bRes = i_bRes .and. .GSAR_MGM.CheckForm()
      if .w_ESCODESE<>space(4)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ESCODAZI = this.w_ESCODAZI
    this.o_ESVALNAZ = this.w_ESVALNAZ
    * --- GSAR_MPR : Depends On
    this.GSAR_MPR.SaveDependsOn()
    * --- GSAR_MGM : Depends On
    this.GSAR_MGM.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_ESCODESE<>space(4))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_ESCODESE=space(4)
      .w_ESINIESE=ctod("  /  /  ")
      .w_ESFINESE=ctod("  /  /  ")
      .w_ESVALNAZ=space(3)
      .w_DESAPP=space(35)
      .w_ESPROLIG=0
      .w_ESDARLIG=0
      .w_ESAVELIG=0
      .w_ESFLSTAM=space(1)
      .w_ESSTAINT=ctod("  /  /  ")
      .w_OCODESE=space(4)
      .w_OINIESE=ctod("  /  /  ")
      .w_OFINESE=ctod("  /  /  ")
      .w_OVALNAZ=space(3)
      .w_DATOBSO=ctod("  /  /  ")
      .w_TCAOVAL=0
      .w_OBTEST=ctod("  /  /  ")
      .w_TOTREGDEF=0
      .DoRTCalc(1,5,.f.)
      if not(empty(.w_ESVALNAZ))
        .link_2_4('Full')
      endif
      .DoRTCalc(6,11,.f.)
        .w_OCODESE = .w_ESCODESE
        .w_OINIESE = .w_ESINIESE
        .w_OFINESE = .w_ESFINESE
        .w_OVALNAZ = .w_ESVALNAZ
      .DoRTCalc(16,16,.f.)
        .w_TCAOVAL = GETCAM(.w_ESVALNAZ,.w_ESFINESE)
        .w_OBTEST = IIF(EMPTY(.w_ESFINESE), i_DATSYS, .w_ESFINESE)
    endwith
    this.DoRTCalc(19,22,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_ESCODESE = t_ESCODESE
    this.w_ESINIESE = t_ESINIESE
    this.w_ESFINESE = t_ESFINESE
    this.w_ESVALNAZ = t_ESVALNAZ
    this.w_DESAPP = t_DESAPP
    this.w_ESPROLIG = t_ESPROLIG
    this.w_ESDARLIG = t_ESDARLIG
    this.w_ESAVELIG = t_ESAVELIG
    this.w_ESFLSTAM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESFLSTAM_2_9.RadioValue(.t.)
    this.w_ESSTAINT = t_ESSTAINT
    this.w_OCODESE = t_OCODESE
    this.w_OINIESE = t_OINIESE
    this.w_OFINESE = t_OFINESE
    this.w_OVALNAZ = t_OVALNAZ
    this.w_DATOBSO = t_DATOBSO
    this.w_TCAOVAL = t_TCAOVAL
    this.w_OBTEST = t_OBTEST
    this.w_TOTREGDEF = t_TOTREGDEF
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_ESCODESE with this.w_ESCODESE
    replace t_ESINIESE with this.w_ESINIESE
    replace t_ESFINESE with this.w_ESFINESE
    replace t_ESVALNAZ with this.w_ESVALNAZ
    replace t_DESAPP with this.w_DESAPP
    replace t_ESPROLIG with this.w_ESPROLIG
    replace t_ESDARLIG with this.w_ESDARLIG
    replace t_ESAVELIG with this.w_ESAVELIG
    replace t_ESFLSTAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oESFLSTAM_2_9.ToRadio()
    replace t_ESSTAINT with this.w_ESSTAINT
    replace t_OCODESE with this.w_OCODESE
    replace t_OINIESE with this.w_OINIESE
    replace t_OFINESE with this.w_OFINESE
    replace t_OVALNAZ with this.w_OVALNAZ
    replace t_DATOBSO with this.w_DATOBSO
    replace t_TCAOVAL with this.w_TCAOVAL
    replace t_OBTEST with this.w_OBTEST
    replace t_TOTREGDEF with this.w_TOTREGDEF
    if i_srv='A'
      replace ESCODESE with this.w_ESCODESE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mesPag1 as StdContainer
  Width  = 812
  height = 350
  stdWidth  = 812
  stdheight = 350
  resizeXpos=237
  resizeYpos=169
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_2 as cp_runprogram with uid="UXDYPJVMXM",left=254, top=355, width=156,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSAR_BAM("modifica")',;
    cEvent = "Record Updated",;
    nPag=1;
    , HelpContextID = 114462694


  add object oObj_1_5 as cp_runprogram with uid="KWGKDNRXPJ",left=418, top=355, width=156,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BEK",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 114462694


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=2, top=4, width=804,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=10,Field1="ESCODESE",Label1="Eserc.",Field2="ESINIESE",Label2="Inizio",Field3="ESFINESE",Label3="Fine",Field4="ESVALNAZ",Label4="Valuta",Field5="ESPROLIG",Label5="IIF(w_CONCAS='S',AH_MSGFORMAT('N.riga.R.C.'),AH_MSGFORMAT('N.riga.L.G.'))",Field6="ESDARLIG",Label6="Totale dare",Field7="ESAVELIG",Label7="Totale avere",Field8="ESFLSTAM",Label8="St.totali",Field9="TOTREGDEF",Label9="Tot. reg. def.  ",Field10="ESSTAINT",Label10="Reg. intento",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 252555898

  add object oStr_1_3 as StdString with uid="JMNDQTUJQH",Visible=.t., Left=4, Top=193,;
    Alignment=0, Width=293, Height=15,;
    Caption="Progressivi numerazioni effetti"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="XPFVKVFFAO",Visible=.t., Left=478, Top=193,;
    Alignment=0, Width=324, Height=15,;
    Caption="Progressivi libro giornale magazzino"  ;
  , bGlobalFont=.t.

  func oStr_1_4.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsar_mpr",lower(this.oContained.GSAR_MPR.class))=0
        this.oContained.GSAR_MPR.createrealchild()
      endif
      if type('this.oContained')='O' and at("gsar_mgm",lower(this.oContained.GSAR_MGM.class))=0
        this.oContained.GSAR_MGM.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-7,top=23,;
    width=798+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-6,top=24,width=797+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='VALUTE|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- gsar_mes
      this.Parent.ocONTAINED.NotifyEvent('Totale_Reg')
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='VALUTE'
        oDropInto=this.oBodyCol.oRow.oESVALNAZ_2_4
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_11 as stdDynamicChildContainer with uid="DUGOFACHCM",bOnScreen=.t.,width=306,height=137,;
   left=1, top=211;


  func oLinkPC_2_11.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_ESCODESE))
    endwith
  endfunc

  add object oLinkPC_2_12 as stdDynamicChildContainer with uid="FMJMKHHDVQ",bOnScreen=.t.,width=341,height=135,;
   left=464, top=212;


  func oLinkPC_2_12.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_ESCODESE))
    endwith
  endfunc

  func oLinkPC_2_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mesBodyRow as CPBodyRowCnt
  Width=788
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oESCODESE_2_1 as StdTrsField with uid="NQNXZWMQFE",rtseq=2,rtrep=.t.,;
    cFormVar="w_ESCODESE",value=space(4),isprimarykey=.t.,;
    ToolTipText = "Codice esercizio",;
    HelpContextID = 97128587,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=44, Left=-2, Top=0, InputMask=replicate('X',4)

  add object oESINIESE_2_2 as StdTrsField with uid="DTDTXQONLQ",rtseq=3,rtrep=.t.,;
    cFormVar="w_ESINIESE",value=ctod("  /  /  "),;
    ToolTipText = "Data di inizio esercizio",;
    HelpContextID = 102330507,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=46, Top=0

  func oESINIESE_2_2.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_ESCODESE))
    endwith
  endfunc

  func oESINIESE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ESINIESE<=.w_ESFINESE OR EMPTY(.w_ESFINESE))
    endwith
    return bRes
  endfunc

  add object oESFINESE_2_3 as StdTrsField with uid="HWZIYQHBSX",rtseq=4,rtrep=.t.,;
    cFormVar="w_ESFINESE",value=ctod("  /  /  "),;
    ToolTipText = "Data di fine esercizio",;
    HelpContextID = 107233419,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=129, Top=0

  func oESFINESE_2_3.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_ESCODESE))
    endwith
  endfunc

  func oESFINESE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ESFINESE>=.w_ESINIESE)
    endwith
    return bRes
  endfunc

  add object oESVALNAZ_2_4 as StdTrsField with uid="DQVDJRVXQE",rtseq=5,rtrep=.t.,;
    cFormVar="w_ESVALNAZ",value=space(3),;
    ToolTipText = "Codice della divisa in corso per l'esercizio",;
    HelpContextID = 12762976,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Valuta incongruente o obsoleta",;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=209, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_ESVALNAZ"

  func oESVALNAZ_2_4.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_ESCODESE))
    endwith
  endfunc

  func oESVALNAZ_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oESVALNAZ_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oESVALNAZ_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oESVALNAZ_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUEZOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oESVALNAZ_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_ESVALNAZ
    i_obj.ecpSave()
  endproc

  add object oESPROLIG_2_6 as StdTrsField with uid="JQZCXJIDSN",rtseq=7,rtrep=.t.,;
    cFormVar="w_ESPROLIG",value=0,;
    ToolTipText = "Numero ultima riga libro giornale stampato su bollato",;
    HelpContextID = 226353293,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=82, Left=259, Top=0, cSayPict=["999999999"], cGetPict=["999999999"]

  add object oESDARLIG_2_7 as StdTrsField with uid="DXLRNOISOD",rtseq=8,rtrep=.t.,;
    cFormVar="w_ESDARLIG",value=0,;
    ToolTipText = "Importo dare aggiornato dell'ultima stampa giornale in definitiva",;
    HelpContextID = 228335757,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=120, Left=343, Top=0, cSayPict=[v_PV(18)], cGetPict=[v_GV(18)]

  add object oESAVELIG_2_8 as StdTrsField with uid="GXOQCLHGRO",rtseq=9,rtrep=.t.,;
    cFormVar="w_ESAVELIG",value=0,;
    ToolTipText = "Importo avere aggiornato dell'ultima stampa giornale in definitiva",;
    HelpContextID = 216068237,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=120, Left=464, Top=0, cSayPict=[v_PV(18)], cGetPict=[v_GV(18)]

  add object oESFLSTAM_2_9 as StdTrsCheck with uid="AJGUMZLLDH",rtrep=.t.,;
    cFormVar="w_ESFLSTAM",  caption="",;
    ToolTipText = "Se attivo: stampa i totali giornalieri sul libro giornale",;
    HelpContextID = 95895699,;
    Left=596, Top=0, Width=22,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , AutoSize=.F.;
   , bGlobalFont=.t.


  func oESFLSTAM_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ESFLSTAM,&i_cF..t_ESFLSTAM),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oESFLSTAM_2_9.GetRadio()
    this.Parent.oContained.w_ESFLSTAM = this.RadioValue()
    return .t.
  endfunc

  func oESFLSTAM_2_9.ToRadio()
    this.Parent.oContained.w_ESFLSTAM=trim(this.Parent.oContained.w_ESFLSTAM)
    return(;
      iif(this.Parent.oContained.w_ESFLSTAM=='S',1,;
      0))
  endfunc

  func oESFLSTAM_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oESSTAINT_2_10 as StdTrsField with uid="IEBKHRWIRH",rtseq=11,rtrep=.t.,;
    cFormVar="w_ESSTAINT",value=ctod("  /  /  "),;
    ToolTipText = "Data di stampa registro lettere di intento",;
    HelpContextID = 106950502,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=85, Left=698, Top=0

  add object oTOTREGDEF_2_20 as StdTrsField with uid="QKEZKQCXXG",rtseq=21,rtrep=.t.,;
    cFormVar="w_TOTREGDEF",value=0,enabled=.f.,;
    ToolTipText = "Visualizza il totale delle registrazione stampate in definitiva nel libro giornale",;
    HelpContextID = 131998171,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=624, Top=0, cSayPict=["999999"], cGetPict=["999999"]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oESCODESE_2_1.When()
    return(.t.)
  proc oESCODESE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oESCODESE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mes','ESERCIZI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ESCODAZI=ESERCIZI.ESCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
