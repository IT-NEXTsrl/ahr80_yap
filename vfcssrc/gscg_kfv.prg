* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kfv                                                        *
*              Generazione flusso F24                                          *
*                                                                              *
*      Author: TAM Software                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-06-02                                                      *
* Last revis.: 2011-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kfv",oParentObject))

* --- Class definition
define class tgscg_kfv as StdForm
  Top    = 13
  Left   = 73

  * --- Standard Properties
  Width  = 617
  Height = 527
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-10-21"
  HelpContextID=48899433
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  CONTROPA_IDX = 0
  MOD_PAG_IDX = 0
  AZIENDA_IDX = 0
  MODVPAG_IDX = 0
  cPrg = "gscg_kfv"
  cComment = "Generazione flusso F24"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_PATHTE = space(200)
  w_CODAZI1 = space(5)
  w_COD_FISC = space(16)
  w_NUMERO = space(10)
  o_NUMERO = space(10)
  w_NUMERO = space(10)
  w_TIPOFLU = space(1)
  o_TIPOFLU = space(1)
  w_MESE1 = space(2)
  w_ANNO1 = space(4)
  w_MESE = space(2)
  o_MESE = space(2)
  w_ANNO = space(4)
  w_DATACREA = ctod('  /  /  ')
  w_CFTITCC = space(16)
  w_TITCCPAG = space(1)
  w_INATTPAG = space(1)
  w_FLGFIRM = space(1)
  w_MFTIPMOD = space(10)
  w_SERIALE = space(10)
  w_NOMEFILE = space(40)
  o_NOMEFILE = space(40)
  w_SEPREC = space(1)
  w_SERIALE = space(10)
  w_DATAPRES = ctod('  /  /  ')
  o_DATAPRES = ctod('  /  /  ')
  w_LPATH = .F.
  w_ZOOMF24 = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kfvPag1","gscg_kfv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNUMERO_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMF24 = this.oPgFrm.Pages(1).oPag.ZOOMF24
    DoDefault()
    proc Destroy()
      this.w_ZOOMF24 = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='MOD_PAG'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='MODVPAG'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_PATHTE=space(200)
      .w_CODAZI1=space(5)
      .w_COD_FISC=space(16)
      .w_NUMERO=space(10)
      .w_NUMERO=space(10)
      .w_TIPOFLU=space(1)
      .w_MESE1=space(2)
      .w_ANNO1=space(4)
      .w_MESE=space(2)
      .w_ANNO=space(4)
      .w_DATACREA=ctod("  /  /  ")
      .w_CFTITCC=space(16)
      .w_TITCCPAG=space(1)
      .w_INATTPAG=space(1)
      .w_FLGFIRM=space(1)
      .w_MFTIPMOD=space(10)
      .w_SERIALE=space(10)
      .w_NOMEFILE=space(40)
      .w_SEPREC=space(1)
      .w_SERIALE=space(10)
      .w_DATAPRES=ctod("  /  /  ")
      .w_LPATH=.f.
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_CODAZI1 = i_CODAZI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODAZI1))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_NUMERO))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_NUMERO))
          .link_1_6('Full')
        endif
        .w_TIPOFLU = 'N'
          .DoRTCalc(8,9,.f.)
        .w_MESE = iif(EMPTY(NVL(.w_NUMERO,'')), right('00'+ALLTRIM(STR(MONTH(i_datsys))),2), .w_MESE1)
        .w_ANNO = iif(EMPTY(NVL(.w_NUMERO,'')), ALLTRIM(STR(YEAR(i_datsys))), .w_ANNO1)
        .w_DATACREA = i_DATSYS
        .w_CFTITCC = .w_COD_FISC
        .w_TITCCPAG = '3'
        .w_INATTPAG = '1'
        .w_FLGFIRM = '0'
        .w_MFTIPMOD = .w_ZOOMF24.getVar("MFTIPMOD")
        .w_SERIALE = .w_zoomf24.getvar('MFSERIAL')
        .w_NOMEFILE = .w_PATHTE
        .w_SEPREC = 'S'
      .oPgFrm.Page1.oPag.ZOOMF24.Calculate()
        .w_SERIALE = .w_NUMERO
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_SERIALE))
          .link_1_37('Full')
        endif
    endwith
    this.DoRTCalc(22,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,4,.t.)
        if .o_MESE<>.w_MESE.or. .o_DATAPRES<>.w_DATAPRES
          .link_1_5('Full')
        endif
        if .o_MESE<>.w_MESE.or. .o_DATAPRES<>.w_DATAPRES
          .link_1_6('Full')
        endif
        .DoRTCalc(7,9,.t.)
        if .o_NUMERO<>.w_NUMERO
            .w_MESE = iif(EMPTY(NVL(.w_NUMERO,'')), right('00'+ALLTRIM(STR(MONTH(i_datsys))),2), .w_MESE1)
        endif
        if .o_NUMERO<>.w_NUMERO
            .w_ANNO = iif(EMPTY(NVL(.w_NUMERO,'')), ALLTRIM(STR(YEAR(i_datsys))), .w_ANNO1)
        endif
        .DoRTCalc(12,16,.t.)
            .w_MFTIPMOD = .w_ZOOMF24.getVar("MFTIPMOD")
            .w_SERIALE = .w_zoomf24.getvar('MFSERIAL')
        .oPgFrm.Page1.oPag.ZOOMF24.Calculate()
        .DoRTCalc(19,20,.t.)
        if .o_NUMERO<>.w_NUMERO
            .w_SERIALE = .w_NUMERO
          .link_1_37('Full')
        endif
        if .o_TIPOFLU<>.w_TIPOFLU
          .Calculate_LZRMZDONIL()
        endif
        if .o_NOMEFILE<>.w_NOMEFILE
          .Calculate_LMZUISUYVI()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(22,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMF24.Calculate()
    endwith
  return

  proc Calculate_LZRMZDONIL()
    with this
          * --- Sbianca numero modello
          .w_NUMERO = ''
          AggiornaZoom(this;
             )
    endwith
  endproc
  proc Calculate_LMZUISUYVI()
    with this
          * --- Controllo path
          .w_NOMEFILE = IIF(right(alltrim(.w_NOMEFILE),1)='\' or empty(.w_NOMEFILE),.w_NOMEFILE,alltrim(.w_NOMEFILE)+iif(len(alltrim(.w_NOMEFILE))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_NOMEFILE,'F')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_36.enabled = this.oPgFrm.Page1.oPag.oBtn_1_36.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNUMERO_1_5.visible=!this.oPgFrm.Page1.oPag.oNUMERO_1_5.mHide()
    this.oPgFrm.Page1.oPag.oNUMERO_1_6.visible=!this.oPgFrm.Page1.oPag.oNUMERO_1_6.mHide()
    this.oPgFrm.Page1.oPag.oCFTITCC_1_13.visible=!this.oPgFrm.Page1.oPag.oCFTITCC_1_13.mHide()
    this.oPgFrm.Page1.oPag.oTITCCPAG_1_14.visible=!this.oPgFrm.Page1.oPag.oTITCCPAG_1_14.mHide()
    this.oPgFrm.Page1.oPag.oINATTPAG_1_15.visible=!this.oPgFrm.Page1.oPag.oINATTPAG_1_15.mHide()
    this.oPgFrm.Page1.oPag.oFLGFIRM_1_16.visible=!this.oPgFrm.Page1.oPag.oFLGFIRM_1_16.mHide()
    this.oPgFrm.Page1.oPag.oSEPREC_1_21.visible=!this.oPgFrm.Page1.oPag.oSEPREC_1_21.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_36.visible=!this.oPgFrm.Page1.oPag.oBtn_1_36.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMF24.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COPATHF4";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,COPATHF4;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_PATHTE = NVL(_Link_.COPATHF4,space(200))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_PATHTE = space(200)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI1
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZCOFAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI1)
            select AZCODAZI,AZCOFAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI1 = NVL(_Link_.AZCODAZI,space(5))
      this.w_COD_FISC = NVL(_Link_.AZCOFAZI,space(16))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI1 = space(5)
      endif
      this.w_COD_FISC = space(16)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMERO
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_lTable = "MOD_PAG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2], .t., this.MOD_PAG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMERO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AOO',True,'MOD_PAG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MFSERIAL like "+cp_ToStrODBC(trim(this.w_NUMERO)+"%");

          i_ret=cp_SQL(i_nConn,"select MFSERIAL,MFMESRIF,MFANNRIF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MFSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MFSERIAL',trim(this.w_NUMERO))
          select MFSERIAL,MFMESRIF,MFANNRIF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MFSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMERO)==trim(_Link_.MFSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMERO) and !this.bDontReportError
            deferred_cp_zoom('MOD_PAG','*','MFSERIAL',cp_AbsName(oSource.parent,'oNUMERO_1_5'),i_cWhere,'GSCG_AOO',"MODELLI F24",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MFSERIAL,MFMESRIF,MFANNRIF";
                     +" from "+i_cTable+" "+i_lTable+" where MFSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MFSERIAL',oSource.xKey(1))
            select MFSERIAL,MFMESRIF,MFANNRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMERO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MFSERIAL,MFMESRIF,MFANNRIF";
                   +" from "+i_cTable+" "+i_lTable+" where MFSERIAL="+cp_ToStrODBC(this.w_NUMERO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MFSERIAL',this.w_NUMERO)
            select MFSERIAL,MFMESRIF,MFANNRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMERO = NVL(_Link_.MFSERIAL,space(10))
      this.w_MESE1 = NVL(_Link_.MFMESRIF,space(2))
      this.w_ANNO1 = NVL(_Link_.MFANNRIF,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_NUMERO = space(10)
      endif
      this.w_MESE1 = space(2)
      this.w_ANNO1 = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=val(.w_ANNO1)>=2007 and val(.w_ANNO1)<2050
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un anno compreso fra il 2007 e 2050")
        endif
        this.w_NUMERO = space(10)
        this.w_MESE1 = space(2)
        this.w_ANNO1 = space(4)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])+'\'+cp_ToStr(_Link_.MFSERIAL,1)
      cp_ShowWarn(i_cKey,this.MOD_PAG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMERO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NUMERO
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_PAG_IDX,3]
    i_lTable = "MOD_PAG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2], .t., this.MOD_PAG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMERO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MOD_PAG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MFSERIAL like "+cp_ToStrODBC(trim(this.w_NUMERO)+"%");

          i_ret=cp_SQL(i_nConn,"select MFSERIAL,MFMESRIF,MFANNRIF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MFSERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MFSERIAL',trim(this.w_NUMERO))
          select MFSERIAL,MFMESRIF,MFANNRIF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MFSERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMERO)==trim(_Link_.MFSERIAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMERO) and !this.bDontReportError
            deferred_cp_zoom('MOD_PAG','*','MFSERIAL',cp_AbsName(oSource.parent,'oNUMERO_1_6'),i_cWhere,'',"Modelli F24",'GSCGEKFV.MOD_PAG_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MFSERIAL,MFMESRIF,MFANNRIF";
                     +" from "+i_cTable+" "+i_lTable+" where MFSERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MFSERIAL',oSource.xKey(1))
            select MFSERIAL,MFMESRIF,MFANNRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMERO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MFSERIAL,MFMESRIF,MFANNRIF";
                   +" from "+i_cTable+" "+i_lTable+" where MFSERIAL="+cp_ToStrODBC(this.w_NUMERO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MFSERIAL',this.w_NUMERO)
            select MFSERIAL,MFMESRIF,MFANNRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMERO = NVL(_Link_.MFSERIAL,space(10))
      this.w_MESE1 = NVL(_Link_.MFMESRIF,space(2))
      this.w_ANNO1 = NVL(_Link_.MFANNRIF,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_NUMERO = space(10)
      endif
      this.w_MESE1 = space(2)
      this.w_ANNO1 = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=val(.w_ANNO1)>=2007 and val(.w_ANNO1)<2050
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un anno compreso fra il 2007 e 2050")
        endif
        this.w_NUMERO = space(10)
        this.w_MESE1 = space(2)
        this.w_ANNO1 = space(4)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_PAG_IDX,2])+'\'+cp_ToStr(_Link_.MFSERIAL,1)
      cp_ShowWarn(i_cKey,this.MOD_PAG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMERO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SERIALE
  func Link_1_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODVPAG_IDX,3]
    i_lTable = "MODVPAG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODVPAG_IDX,2], .t., this.MODVPAG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODVPAG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SERIALE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SERIALE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VFSERIAL,VFDTPRES";
                   +" from "+i_cTable+" "+i_lTable+" where VFSERIAL="+cp_ToStrODBC(this.w_SERIALE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VFSERIAL',this.w_SERIALE)
            select VFSERIAL,VFDTPRES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SERIALE = NVL(_Link_.VFSERIAL,space(10))
      this.w_DATAPRES = NVL(cp_ToDate(_Link_.VFDTPRES),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SERIALE = space(10)
      endif
      this.w_DATAPRES = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODVPAG_IDX,2])+'\'+cp_ToStr(_Link_.VFSERIAL,1)
      cp_ShowWarn(i_cKey,this.MODVPAG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SERIALE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_5.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_5.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_6.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_6.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOFLU_1_7.RadioValue()==this.w_TIPOFLU)
      this.oPgFrm.Page1.oPag.oTIPOFLU_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMESE_1_10.value==this.w_MESE)
      this.oPgFrm.Page1.oPag.oMESE_1_10.value=this.w_MESE
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_11.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_11.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATACREA_1_12.value==this.w_DATACREA)
      this.oPgFrm.Page1.oPag.oDATACREA_1_12.value=this.w_DATACREA
    endif
    if not(this.oPgFrm.Page1.oPag.oCFTITCC_1_13.value==this.w_CFTITCC)
      this.oPgFrm.Page1.oPag.oCFTITCC_1_13.value=this.w_CFTITCC
    endif
    if not(this.oPgFrm.Page1.oPag.oTITCCPAG_1_14.RadioValue()==this.w_TITCCPAG)
      this.oPgFrm.Page1.oPag.oTITCCPAG_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINATTPAG_1_15.RadioValue()==this.w_INATTPAG)
      this.oPgFrm.Page1.oPag.oINATTPAG_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGFIRM_1_16.RadioValue()==this.w_FLGFIRM)
      this.oPgFrm.Page1.oPag.oFLGFIRM_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSERIALE_1_18.value==this.w_SERIALE)
      this.oPgFrm.Page1.oPag.oSERIALE_1_18.value=this.w_SERIALE
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMEFILE_1_19.value==this.w_NOMEFILE)
      this.oPgFrm.Page1.oPag.oNOMEFILE_1_19.value=this.w_NOMEFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oSEPREC_1_21.RadioValue()==this.w_SEPREC)
      this.oPgFrm.Page1.oPag.oSEPREC_1_21.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(val(.w_ANNO1)>=2007 and val(.w_ANNO1)<2050)  and not(.w_DATAPRES >= CTOD('29-10-2007') and ! empty(.w_DATAPRES))  and not(empty(.w_NUMERO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERO_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un anno compreso fra il 2007 e 2050")
          case   not(val(.w_ANNO1)>=2007 and val(.w_ANNO1)<2050)  and not(.w_DATAPRES < CTOD('29-10-2007') and ! empty(.w_DATAPRES))  and not(empty(.w_NUMERO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERO_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un anno compreso fra il 2007 e 2050")
          case   (empty(.w_MESE1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMESE1_1_8.SetFocus()
            i_bnoObbl = !empty(.w_MESE1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ANNO1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO1_1_9.SetFocus()
            i_bnoObbl = !empty(.w_ANNO1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_MESE)) or not(not empty(.w_MESE) and val(.w_MESE)>= 1 and val(.w_MESE)<13))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMESE_1_10.SetFocus()
            i_bnoObbl = !empty(.w_MESE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Mese di riferimento non valido")
          case   ((empty(.w_ANNO)) or not(not empty(.w_ANNO) and val(.w_ANNO)>=2007 and val(.w_ANNO)<=2050))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_11.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Selezionare un anno compreso fra il 2007 e 2050")
          case   (empty(.w_DATACREA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATACREA_1_12.SetFocus()
            i_bnoObbl = !empty(.w_DATACREA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CFTITCC)) or not(CHKCFP(.w_CFTITCC,"CF")))  and not(.w_TIPOFLU='E')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCFTITCC_1_13.SetFocus()
            i_bnoObbl = !empty(.w_CFTITCC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_TITCCPAG))  and not(.w_TIPOFLU='E')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTITCCPAG_1_14.SetFocus()
            i_bnoObbl = !empty(.w_TITCCPAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NOMEFILE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOMEFILE_1_19.SetFocus()
            i_bnoObbl = !empty(.w_NOMEFILE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NUMERO = this.w_NUMERO
    this.o_TIPOFLU = this.w_TIPOFLU
    this.o_MESE = this.w_MESE
    this.o_NOMEFILE = this.w_NOMEFILE
    this.o_DATAPRES = this.w_DATAPRES
    return

enddefine

* --- Define pages as container
define class tgscg_kfvPag1 as StdContainer
  Width  = 613
  height = 527
  stdWidth  = 613
  stdheight = 527
  resizeXpos=445
  resizeYpos=268
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNUMERO_1_5 as StdField with uid="NVILHEENZI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un anno compreso fra il 2007 e 2050",;
    ToolTipText = "Numero modello F24",;
    HelpContextID = 25167062,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=161, Top=11, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_PAG", cZoomOnZoom="GSCG_AOO", oKey_1_1="MFSERIAL", oKey_1_2="this.w_NUMERO"

  func oNUMERO_1_5.mHide()
    with this.Parent.oContained
      return (.w_DATAPRES >= CTOD('29-10-2007') and ! empty(.w_DATAPRES))
    endwith
  endfunc

  func oNUMERO_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMERO_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMERO_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_PAG','*','MFSERIAL',cp_AbsName(this.parent,'oNUMERO_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AOO',"MODELLI F24",'',this.parent.oContained
  endproc
  proc oNUMERO_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AOO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MFSERIAL=this.parent.oContained.w_NUMERO
     i_obj.ecpSave()
  endproc

  add object oNUMERO_1_6 as StdField with uid="ILGFIKONXZ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un anno compreso fra il 2007 e 2050",;
    ToolTipText = "Numero modello F24",;
    HelpContextID = 25167062,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=161, Top=11, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_PAG", oKey_1_1="MFSERIAL", oKey_1_2="this.w_NUMERO"

  func oNUMERO_1_6.mHide()
    with this.Parent.oContained
      return (.w_DATAPRES < CTOD('29-10-2007') and ! empty(.w_DATAPRES))
    endwith
  endfunc

  func oNUMERO_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMERO_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMERO_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_PAG','*','MFSERIAL',cp_AbsName(this.parent,'oNUMERO_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Modelli F24",'GSCGEKFV.MOD_PAG_VZM',this.parent.oContained
  endproc


  add object oTIPOFLU_1_7 as StdCombo with uid="KEINKRUMSJ",rtseq=7,rtrep=.f.,left=411,top=9,width=194,height=21;
    , ToolTipText = "Genera il file per CBI o Entratel";
    , HelpContextID = 231352630;
    , cFormVar="w_TIPOFLU",RowSource=""+"Flusso CBI,"+"Flusso Entratel", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOFLU_1_7.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oTIPOFLU_1_7.GetRadio()
    this.Parent.oContained.w_TIPOFLU = this.RadioValue()
    return .t.
  endfunc

  func oTIPOFLU_1_7.SetRadio()
    this.Parent.oContained.w_TIPOFLU=trim(this.Parent.oContained.w_TIPOFLU)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOFLU=='N',1,;
      iif(this.Parent.oContained.w_TIPOFLU=='E',2,;
      0))
  endfunc

  add object oMESE_1_10 as StdField with uid="EZJFCOMRCC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_MESE", cQueryName = "MESE",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Mese di riferimento non valido",;
    ToolTipText = "Mese del periodo di riferimento",;
    HelpContextID = 44018490,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=161, Top=36, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oMESE_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_MESE) and val(.w_MESE)>= 1 and val(.w_MESE)<13)
    endwith
    return bRes
  endfunc

  add object oANNO_1_11 as StdField with uid="FPEXKICLXJ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un anno compreso fra il 2007 e 2050",;
    ToolTipText = "Anno del periodo di riferimento",;
    HelpContextID = 43381498,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=411, Top=36, InputMask=replicate('X',4)

  func oANNO_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (not empty(.w_ANNO) and val(.w_ANNO)>=2007 and val(.w_ANNO)<=2050)
    endwith
    return bRes
  endfunc

  add object oDATACREA_1_12 as StdField with uid="CJETNXFPQM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATACREA", cQueryName = "DATACREA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data creazione flusso",;
    HelpContextID = 59531383,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=162, Top=63

  add object oCFTITCC_1_13 as StdField with uid="SSMBSWZCMA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CFTITCC", cQueryName = "CFTITCC",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del titolare del C/C di addebito",;
    HelpContextID = 94659878,;
   bGlobalFont=.t.,;
    Height=21, Width=194, Left=411, Top=63, cSayPict='REPL("!",16)', cGetPict='REPL("!",16)', InputMask=replicate('X',16)

  func oCFTITCC_1_13.mHide()
    with this.Parent.oContained
      return (.w_TIPOFLU='E')
    endwith
  endfunc

  func oCFTITCC_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_CFTITCC,"CF"))
    endwith
    return bRes
  endfunc


  add object oTITCCPAG_1_14 as StdCombo with uid="SXXLKLPIXC",rtseq=14,rtrep=.f.,left=162,top=93,width=317,height=21;
    , ToolTipText = "Titolare del C/C di pagamento";
    , HelpContextID = 26110333;
    , cFormVar="w_TITCCPAG",RowSource=""+"1 - Delega F24 con saldo finale uguale a zero,"+"2 - Titolare del C/C di pagam. corrisp. al contribuente,"+"3 - Titolare del C/C del pagam. corrisp. all'azienda mittente", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oTITCCPAG_1_14.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    space(1)))))
  endfunc
  func oTITCCPAG_1_14.GetRadio()
    this.Parent.oContained.w_TITCCPAG = this.RadioValue()
    return .t.
  endfunc

  func oTITCCPAG_1_14.SetRadio()
    this.Parent.oContained.w_TITCCPAG=trim(this.Parent.oContained.w_TITCCPAG)
    this.value = ;
      iif(this.Parent.oContained.w_TITCCPAG=='1',1,;
      iif(this.Parent.oContained.w_TITCCPAG=='2',2,;
      iif(this.Parent.oContained.w_TITCCPAG=='3',3,;
      0)))
  endfunc

  func oTITCCPAG_1_14.mHide()
    with this.Parent.oContained
      return (.w_TIPOFLU='E')
    endwith
  endfunc


  add object oINATTPAG_1_15 as StdCombo with uid="TZMJNEZEUT",rtseq=15,rtrep=.f.,left=162,top=123,width=139,height=21;
    , ToolTipText = "Seleziona il soggetto cui la banca invier� attestazione di pagamento";
    , HelpContextID = 44973517;
    , cFormVar="w_INATTPAG",RowSource=""+"Contribuente,"+"Destinatario stampa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oINATTPAG_1_15.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    space(1))))
  endfunc
  func oINATTPAG_1_15.GetRadio()
    this.Parent.oContained.w_INATTPAG = this.RadioValue()
    return .t.
  endfunc

  func oINATTPAG_1_15.SetRadio()
    this.Parent.oContained.w_INATTPAG=trim(this.Parent.oContained.w_INATTPAG)
    this.value = ;
      iif(this.Parent.oContained.w_INATTPAG=='1',1,;
      iif(this.Parent.oContained.w_INATTPAG=='2',2,;
      0))
  endfunc

  func oINATTPAG_1_15.mHide()
    with this.Parent.oContained
      return (.w_TIPOFLU='E')
    endwith
  endfunc

  add object oFLGFIRM_1_16 as StdCheck with uid="YJZXCDPIKY",rtseq=16,rtrep=.f.,left=162, top=148, caption="Flag firmatario",;
    ToolTipText = "Flag firmatario",;
    HelpContextID = 202335402,;
    cFormVar="w_FLGFIRM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGFIRM_1_16.RadioValue()
    return(iif(this.value =1,'1',;
    '0'))
  endfunc
  func oFLGFIRM_1_16.GetRadio()
    this.Parent.oContained.w_FLGFIRM = this.RadioValue()
    return .t.
  endfunc

  func oFLGFIRM_1_16.SetRadio()
    this.Parent.oContained.w_FLGFIRM=trim(this.Parent.oContained.w_FLGFIRM)
    this.value = ;
      iif(this.Parent.oContained.w_FLGFIRM=='1',1,;
      0)
  endfunc

  func oFLGFIRM_1_16.mHide()
    with this.Parent.oContained
      return (.w_TIPOFLU='E')
    endwith
  endfunc

  add object oSERIALE_1_18 as StdField with uid="OVJYGPEKTY",rtseq=18,rtrep=.f.,;
    cFormVar = "w_SERIALE", cQueryName = "SERIALE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero del modello selezionato",;
    HelpContextID = 225723686,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=144, Top=452, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oNOMEFILE_1_19 as StdField with uid="SGUJFOPRQO",rtseq=19,rtrep=.f.,;
    cFormVar = "w_NOMEFILE", cQueryName = "NOMEFILE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Nome del file ASCII da generare",;
    HelpContextID = 88080613,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=144, Top=476, InputMask=replicate('X',40)


  add object oBtn_1_20 as StdButton with uid="RUDQNHINUV",left=431, top=476, width=22,height=21,;
    caption="...", nPag=1;
    , HelpContextID = 48698410;
  , bGlobalFont=.t.

    proc oBtn_1_20.Click()
      with this.Parent.oContained
        .w_NOMEFILE=left(cp_GetDir(IIF(EMPTY(.w_NOMEFILE),sys(5)+sys(2003),.w_NOMEFILE),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oSEPREC_1_21 as StdCombo with uid="QGGYCSNGIF",rtseq=20,rtrep=.f.,left=144,top=500,width=130,height=21;
    , ToolTipText = "Caratteri utilizzati come separatori dei records";
    , HelpContextID = 79504678;
    , cFormVar="w_SEPREC",RowSource=""+"Return e linefeed,"+"Nessun separatore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSEPREC_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oSEPREC_1_21.GetRadio()
    this.Parent.oContained.w_SEPREC = this.RadioValue()
    return .t.
  endfunc

  func oSEPREC_1_21.SetRadio()
    this.Parent.oContained.w_SEPREC=trim(this.Parent.oContained.w_SEPREC)
    this.value = ;
      iif(this.Parent.oContained.w_SEPREC=='S',1,;
      iif(this.Parent.oContained.w_SEPREC=='N',2,;
      0))
  endfunc

  func oSEPREC_1_21.mHide()
    with this.Parent.oContained
      return (.w_TIPOFLU='E')
    endwith
  endfunc


  add object oBtn_1_22 as StdButton with uid="QKYBLFAXXW",left=502, top=473, width=48,height=45,;
    CpPicture="BMP\ok.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare";
    , HelpContextID = 42046486;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        do gsut_BFV with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((.not. empty(.w_NomeFile) and directory(.w_NomeFile)) .and. (.not. empty(.w_SERIALE)) .and. ( (.not. empty(.w_CFTITCC))   OR .w_TIPOFLU='E')  AND isinoltel()  AND NOT g_DEMO)
      endwith
    endif
  endfunc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT (NVL(.w_MFTIPMOD,'')="ASSISE06") OR g_APPLICATION = "ADHOC REVOLUTION")
     endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="UTJBWUDSTH",left=554, top=473, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 42046486;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOMF24 as cp_zoombox with uid="IOURBYVRMS",left=11, top=173, width=592,height=270,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="MOD_PAG",cZoomFile="gscg_KFV",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bRetriveAllRows=.f.,bAdvOptions=.t.,cZoomOnZoom="",cMenuFile="",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 129098214


  add object oBtn_1_36 as StdButton with uid="NMERFYEQVX",left=502, top=473, width=48,height=45,;
    CpPicture="BMP\ok.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare";
    , HelpContextID = 42046486;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      with this.Parent.oContained
        do gsCG_BFT with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_36.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((.not. empty(.w_NomeFile) and directory(.w_NomeFile)) .and. (.not. empty(.w_SERIALE)) .and. ( (.not. empty(.w_CFTITCC))  OR .w_TIPOFLU='E')  AND isinoltel()  AND NOT g_DEMO)
      endwith
    endif
  endfunc

  func oBtn_1_36.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return ((NVL(.w_MFTIPMOD,'')= "ASSISE06"))
     endwith
    endif
  endfunc


  add object oBtn_1_41 as StdButton with uid="RAUGJDKCYZ",left=554, top=126, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare";
    , HelpContextID = 164864582;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      with this.Parent.oContained
        .NotifyEvent('Esegui')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_25 as StdString with uid="ETCXQTQLTF",Visible=.t., Left=5, Top=504,;
    Alignment=1, Width=130, Height=15,;
    Caption="Separatore records:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (.w_TIPOFLU='E')
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="JVMNZBOWCU",Visible=.t., Left=32, Top=39,;
    Alignment=1, Width=128, Height=18,;
    Caption="Mese periodo di rif.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="SPPNJXIDRN",Visible=.t., Left=62, Top=14,;
    Alignment=1, Width=98, Height=18,;
    Caption="Numero modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="OEDQLYDRXO",Visible=.t., Left=13, Top=454,;
    Alignment=1, Width=122, Height=18,;
    Caption="Modello selezionato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="XDKOTWMSJT",Visible=.t., Left=49, Top=479,;
    Alignment=1, Width=86, Height=18,;
    Caption="File path:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="SVVDCBFYNX",Visible=.t., Left=52, Top=65,;
    Alignment=1, Width=108, Height=18,;
    Caption="Data creazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="DWZUSOODYW",Visible=.t., Left=221, Top=39,;
    Alignment=1, Width=188, Height=18,;
    Caption="Anno periodo di rif.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="JDTRINSMYX",Visible=.t., Left=5, Top=95,;
    Alignment=1, Width=155, Height=18,;
    Caption="Titolare del C/C di pag.:"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (.w_TIPOFLU='E')
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="ECGEGIKKDP",Visible=.t., Left=240, Top=66,;
    Alignment=1, Width=169, Height=18,;
    Caption="C.F.titolare C/C addebito:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (.w_TIPOFLU='E')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="XKGDXIUWZL",Visible=.t., Left=21, Top=125,;
    Alignment=1, Width=139, Height=18,;
    Caption="Attestazione pagamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (.w_TIPOFLU='E')
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="TKFSBYLHVB",Visible=.t., Left=289, Top=12,;
    Alignment=1, Width=120, Height=18,;
    Caption="Tipo di Flusso: "  ;
  , bGlobalFont=.t.

  add object oBox_1_29 as StdBox with uid="JPPGARCMLE",left=11, top=447, width=578,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kfv','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_kfv
Procedure AggiornaZoom (oparent)
Select (oparent.w_ZOOMF24.ccursor)
ZAP
ENDPROC
* --- Fine Area Manuale
