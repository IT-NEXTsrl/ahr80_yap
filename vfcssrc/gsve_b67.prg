* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_b67                                                        *
*              Converte in valuta di conto                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-09-07                                                      *
* Last revis.: 2009-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_CAMBIOL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_b67",oParentObject,m.w_CAMBIOL)
return(i_retval)

define class tgsve_b67 as StdBatch
  * --- Local variables
  w_CAMBIOL = 0
  w_SERIALE = space(10)
  w_TOTVEN = 0
  w_COSTO = 0
  w_CAMBIO = 0
  w_MVDATDOC = ctod("  /  /  ")
  w_TOTVENCV = 0
  w_COSTOCV = 0
  w_CPROWNUM = 0
  * --- WorkFile variables
  GSVE_B67_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Popola tabella temporanea GSVE_B67 utilizzata in query gsve67pv (prospetto venduto)
    * --- Create temporary table GSVE_B67
    i_nIdx=cp_AddTableDef('GSVE_B67') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('gsveb67pv',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.GSVE_B67_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Select from GSVE_B67
    i_nConn=i_TableProp[this.GSVE_B67_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.GSVE_B67_idx,2],.t.,this.GSVE_B67_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" GSVE_B67 ";
           ,"_Curs_GSVE_B67")
    else
      select * from (i_cTable);
        into cursor _Curs_GSVE_B67
    endif
    if used('_Curs_GSVE_B67')
      select _Curs_GSVE_B67
      locate for 1=1
      do while not(eof())
      this.w_SERIALE = NVL(_Curs_GSVE_B67.MVSERIAL,SPACE(10))
      this.w_CPROWNUM = NVL(_Curs_GSVE_B67.CPROWNUM,0)
      this.w_TOTVEN = NVL(_Curs_GSVE_B67.TOTVEN,0)
      this.w_COSTO = NVL(_Curs_GSVE_B67.COSTO,0)
      this.w_CAMBIO = NVL(_Curs_GSVE_B67.MVCAOVAL,0)
      this.w_MVDATDOC = NVL(_Curs_GSVE_B67.MVDATDOC, cp_chartodate("  -  -  "))
      if this.w_TOTVEN<>0 AND this.w_CAMBIO<>1
        this.w_TOTVENCV = VAL2VAL(this.w_TOTVEN, this.w_CAMBIO, this.w_MVDATDOC , i_DATSYS, getcam(g_perval,i_DATSYS), 6)
      else
        this.w_TOTVENCV = this.w_TOTVEN
      endif
      if this.w_COSTO<>0 AND this.w_CAMBIOL >0
        this.w_COSTOCV = VAL2VAL(this.w_COSTO, this.w_CAMBIOL, i_DATSYS , i_DATSYS, 1, 6)
      else
        this.w_COSTOCV = this.w_COSTO
      endif
      * --- Write into GSVE_B67
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.GSVE_B67_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.GSVE_B67_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.GSVE_B67_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"TOTVEN ="+cp_NullLink(cp_ToStrODBC(this.w_TOTVENCV),'GSVE_B67','TOTVEN');
        +",COSTO ="+cp_NullLink(cp_ToStrODBC(this.w_COSTOCV),'GSVE_B67','COSTO');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            TOTVEN = this.w_TOTVENCV;
            ,COSTO = this.w_COSTOCV;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_SERIALE;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
        select _Curs_GSVE_B67
        continue
      enddo
      use
    endif
  endproc


  proc Init(oParentObject,w_CAMBIOL)
    this.w_CAMBIOL=w_CAMBIOL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*GSVE_B67'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSVE_B67')
      use in _Curs_GSVE_B67
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_CAMBIOL"
endproc
