* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mto                                                        *
*              Tipologie colli                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_36]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-05                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_mto"))

* --- Class definition
define class tgsar_mto as StdTrsForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 497
  Height = 333+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=188123287
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  TIP_COLL_IDX = 0
  CON_COLL_IDX = 0
  TIPICONF_IDX = 0
  UNIMIS_IDX = 0
  cFile = "TIP_COLL"
  cFileDetail = "CON_COLL"
  cKeySelect = "TCCODICE"
  cKeyWhere  = "TCCODICE=this.w_TCCODICE"
  cKeyDetail  = "TCCODICE=this.w_TCCODICE and TCCODCON=this.w_TCCODCON"
  cKeyWhereODBC = '"TCCODICE="+cp_ToStrODBC(this.w_TCCODICE)';

  cKeyDetailWhereODBC = '"TCCODICE="+cp_ToStrODBC(this.w_TCCODICE)';
      +'+" and TCCODCON="+cp_ToStrODBC(this.w_TCCODCON)';

  cKeyWhereODBCqualified = '"CON_COLL.TCCODICE="+cp_ToStrODBC(this.w_TCCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gsar_mto"
  cComment = "Tipologie colli"
  i_nRowNum = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TCCODICE = space(5)
  w_TCDESCRI = space(35)
  w_TC__TARA = 0
  w_TCVOLUME = 0
  w_TCUMVOLU = space(3)
  w_TCCODCON = space(3)
  w_TCQTACON = 0
  w_TCPERTOL = 0
  w_TCFL_MIX = space(1)
  w_TCDESCON = space(35)
  w_CHECK = 0
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TIP_COLL','gsar_mto')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mtoPag1","gsar_mto",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Tipologie collo")
      .Pages(1).HelpContextID = 190689809
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTCCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='TIPICONF'
    this.cWorkTables[2]='UNIMIS'
    this.cWorkTables[3]='TIP_COLL'
    this.cWorkTables[4]='CON_COLL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TIP_COLL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TIP_COLL_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_TCCODICE = NVL(TCCODICE,space(5))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from TIP_COLL where TCCODICE=KeySet.TCCODICE
    *
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2],this.bLoadRecFilter,this.TIP_COLL_IDX,"gsar_mto")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TIP_COLL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TIP_COLL.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CON_COLL.","TIP_COLL.")
      i_cTable = i_cTable+' TIP_COLL '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TCCODICE',this.w_TCCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CHECK = 0
        .w_TCCODICE = NVL(TCCODICE,space(5))
        .w_TCDESCRI = NVL(TCDESCRI,space(35))
        .w_TC__TARA = NVL(TC__TARA,0)
        .w_TCVOLUME = NVL(TCVOLUME,0)
        .w_TCUMVOLU = NVL(TCUMVOLU,space(3))
          * evitabile
          *.link_1_7('Load')
        .w_TCFL_MIX = NVL(TCFL_MIX,space(1))
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'TIP_COLL')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CON_COLL where TCCODICE=KeySet.TCCODICE
      *                            and TCCODCON=KeySet.TCCODCON
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CON_COLL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CON_COLL')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CON_COLL.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CON_COLL"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'TCCODICE',this.w_TCCODICE  )
        select * from (i_cTable) CON_COLL where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_TCCODCON = NVL(TCCODCON,space(3))
          * evitabile
          *.link_2_1('Load')
          .w_TCQTACON = NVL(TCQTACON,0)
          .w_TCPERTOL = NVL(TCPERTOL,0)
          .w_TCDESCON = NVL(TCDESCON,space(35))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace TCCODCON with .w_TCCODCON
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_TCCODICE=space(5)
      .w_TCDESCRI=space(35)
      .w_TC__TARA=0
      .w_TCVOLUME=0
      .w_TCUMVOLU=space(3)
      .w_TCCODCON=space(3)
      .w_TCQTACON=0
      .w_TCPERTOL=0
      .w_TCFL_MIX=space(1)
      .w_TCDESCON=space(35)
      .w_CHECK=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,5,.f.)
        if not(empty(.w_TCUMVOLU))
         .link_1_7('Full')
        endif
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_TCCODCON))
         .link_2_1('Full')
        endif
        .DoRTCalc(7,8,.f.)
        .w_TCFL_MIX = 'N'
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .DoRTCalc(10,10,.f.)
        .w_CHECK = 0
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TIP_COLL')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTCCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oTCDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oTC__TARA_1_4.enabled = i_bVal
      .Page1.oPag.oTCVOLUME_1_5.enabled = i_bVal
      .Page1.oPag.oTCUMVOLU_1_7.enabled = i_bVal
      .Page1.oPag.oTCFL_MIX_1_11.enabled = i_bVal
      .Page1.oPag.oObj_1_12.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTCCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTCCODICE_1_1.enabled = .t.
        .Page1.oPag.oTCDESCRI_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TIP_COLL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCCODICE,"TCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCDESCRI,"TCDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TC__TARA,"TC__TARA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCVOLUME,"TCVOLUME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCUMVOLU,"TCUMVOLU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCFL_MIX,"TCFL_MIX",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    i_lTable = "TIP_COLL"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TIP_COLL_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SCO with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_TCCODCON C(3);
      ,t_TCQTACON N(6);
      ,t_TCPERTOL N(6,2);
      ,t_TCDESCON C(35);
      ,TCCODCON C(3);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mtobodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTCCODCON_2_1.controlsource=this.cTrsName+'.t_TCCODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTCQTACON_2_2.controlsource=this.cTrsName+'.t_TCQTACON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTCPERTOL_2_3.controlsource=this.cTrsName+'.t_TCPERTOL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTCDESCON_2_4.controlsource=this.cTrsName+'.t_TCDESCON'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(66)
    this.AddVLine(335)
    this.AddVLine(415)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTCCODCON_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TIP_COLL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TIP_COLL')
        i_extval=cp_InsertValODBCExtFlds(this,'TIP_COLL')
        local i_cFld
        i_cFld=" "+;
                  "(TCCODICE,TCDESCRI,TC__TARA,TCVOLUME,TCUMVOLU"+;
                  ",TCFL_MIX"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_TCCODICE)+;
                    ","+cp_ToStrODBC(this.w_TCDESCRI)+;
                    ","+cp_ToStrODBC(this.w_TC__TARA)+;
                    ","+cp_ToStrODBC(this.w_TCVOLUME)+;
                    ","+cp_ToStrODBCNull(this.w_TCUMVOLU)+;
                    ","+cp_ToStrODBC(this.w_TCFL_MIX)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TIP_COLL')
        i_extval=cp_InsertValVFPExtFlds(this,'TIP_COLL')
        cp_CheckDeletedKey(i_cTable,0,'TCCODICE',this.w_TCCODICE)
        INSERT INTO (i_cTable);
              (TCCODICE,TCDESCRI,TC__TARA,TCVOLUME,TCUMVOLU,TCFL_MIX &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_TCCODICE;
                  ,this.w_TCDESCRI;
                  ,this.w_TC__TARA;
                  ,this.w_TCVOLUME;
                  ,this.w_TCUMVOLU;
                  ,this.w_TCFL_MIX;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CON_COLL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2])
      *
      * insert into CON_COLL
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(TCCODICE,TCCODCON,TCQTACON,TCPERTOL,TCDESCON,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_TCCODICE)+","+cp_ToStrODBCNull(this.w_TCCODCON)+","+cp_ToStrODBC(this.w_TCQTACON)+","+cp_ToStrODBC(this.w_TCPERTOL)+","+cp_ToStrODBC(this.w_TCDESCON)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,0,'TCCODICE',this.w_TCCODICE,'TCCODCON',this.w_TCCODCON)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_TCCODICE,this.w_TCCODCON,this.w_TCQTACON,this.w_TCPERTOL,this.w_TCDESCON,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update TIP_COLL
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'TIP_COLL')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " TCDESCRI="+cp_ToStrODBC(this.w_TCDESCRI)+;
             ",TC__TARA="+cp_ToStrODBC(this.w_TC__TARA)+;
             ",TCVOLUME="+cp_ToStrODBC(this.w_TCVOLUME)+;
             ",TCUMVOLU="+cp_ToStrODBCNull(this.w_TCUMVOLU)+;
             ",TCFL_MIX="+cp_ToStrODBC(this.w_TCFL_MIX)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'TIP_COLL')
          i_cWhere = cp_PKFox(i_cTable  ,'TCCODICE',this.w_TCCODICE  )
          UPDATE (i_cTable) SET;
              TCDESCRI=this.w_TCDESCRI;
             ,TC__TARA=this.w_TC__TARA;
             ,TCVOLUME=this.w_TCVOLUME;
             ,TCUMVOLU=this.w_TCUMVOLU;
             ,TCFL_MIX=this.w_TCFL_MIX;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_TCCODCON))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CON_COLL_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from CON_COLL
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and TCCODCON="+cp_ToStrODBC(&i_TN.->TCCODCON)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and TCCODCON=&i_TN.->TCCODCON;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = 0
              replace TCCODCON with this.w_TCCODCON
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CON_COLL
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " TCQTACON="+cp_ToStrODBC(this.w_TCQTACON)+;
                     ",TCPERTOL="+cp_ToStrODBC(this.w_TCPERTOL)+;
                     ",TCDESCON="+cp_ToStrODBC(this.w_TCDESCON)+;
                     " ,TCCODCON="+cp_ToStrODBC(this.w_TCCODCON)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+;
                             " and TCCODCON="+cp_ToStrODBC(TCCODCON)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      TCQTACON=this.w_TCQTACON;
                     ,TCPERTOL=this.w_TCPERTOL;
                     ,TCDESCON=this.w_TCDESCON;
                     ,TCCODCON=this.w_TCCODCON;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere;
                                      and TCCODCON=&i_TN.->TCCODCON;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_TCCODCON))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CON_COLL_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2])
        *
        * delete CON_COLL
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and TCCODCON="+cp_ToStrODBC(&i_TN.->TCCODCON)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and TCCODCON=&i_TN.->TCCODCON;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
        *
        * delete TIP_COLL
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_TCCODCON))) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTCPERTOL_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oTCPERTOL_2_3.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TCUMVOLU
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TCUMVOLU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_TCUMVOLU)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_TCUMVOLU))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TCUMVOLU)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TCUMVOLU) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oTCUMVOLU_1_7'),i_cWhere,'GSAR_AUM',"Unit� di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TCUMVOLU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_TCUMVOLU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_TCUMVOLU)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TCUMVOLU = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_TCUMVOLU = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TCUMVOLU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TCCODCON
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPICONF_IDX,3]
    i_lTable = "TIPICONF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2], .t., this.TIPICONF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TCCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATC',True,'TIPICONF')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_TCCODCON)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI"+cp_TransInsFldName("TCDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_TCCODCON))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TCCODCON)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TCCODCON) and !this.bDontReportError
            deferred_cp_zoom('TIPICONF','*','TCCODICE',cp_AbsName(oSource.parent,'oTCCODCON_2_1'),i_cWhere,'GSAR_ATC',"Confezioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI"+cp_TransInsFldName("TCDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TCCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI"+cp_TransInsFldName("TCDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_TCCODCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_TCCODCON)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TCCODCON = NVL(_Link_.TCCODICE,space(3))
      this.w_TCDESCON = NVL(cp_TransLoadField('_Link_.TCDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_TCCODCON = space(3)
      endif
      this.w_TCDESCON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPICONF_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIPICONF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TCCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTCCODICE_1_1.value==this.w_TCCODICE)
      this.oPgFrm.Page1.oPag.oTCCODICE_1_1.value=this.w_TCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTCDESCRI_1_3.value==this.w_TCDESCRI)
      this.oPgFrm.Page1.oPag.oTCDESCRI_1_3.value=this.w_TCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTC__TARA_1_4.value==this.w_TC__TARA)
      this.oPgFrm.Page1.oPag.oTC__TARA_1_4.value=this.w_TC__TARA
    endif
    if not(this.oPgFrm.Page1.oPag.oTCVOLUME_1_5.value==this.w_TCVOLUME)
      this.oPgFrm.Page1.oPag.oTCVOLUME_1_5.value=this.w_TCVOLUME
    endif
    if not(this.oPgFrm.Page1.oPag.oTCUMVOLU_1_7.value==this.w_TCUMVOLU)
      this.oPgFrm.Page1.oPag.oTCUMVOLU_1_7.value=this.w_TCUMVOLU
    endif
    if not(this.oPgFrm.Page1.oPag.oTCFL_MIX_1_11.RadioValue()==this.w_TCFL_MIX)
      this.oPgFrm.Page1.oPag.oTCFL_MIX_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTCCODCON_2_1.value==this.w_TCCODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTCCODCON_2_1.value=this.w_TCCODCON
      replace t_TCCODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTCCODCON_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTCQTACON_2_2.value==this.w_TCQTACON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTCQTACON_2_2.value=this.w_TCQTACON
      replace t_TCQTACON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTCQTACON_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTCPERTOL_2_3.value==this.w_TCPERTOL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTCPERTOL_2_3.value=this.w_TCPERTOL
      replace t_TCPERTOL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTCPERTOL_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTCDESCON_2_4.value==this.w_TCDESCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTCDESCON_2_4.value=this.w_TCDESCON
      replace t_TCDESCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTCDESCON_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'TIP_COLL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TCCODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oTCCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_TCCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_TCQTACON) and (not(Empty(.w_TCCODCON)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTCQTACON_2_2
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_TCCODCON))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_TCCODCON)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_TCCODCON=space(3)
      .w_TCQTACON=0
      .w_TCPERTOL=0
      .w_TCDESCON=space(35)
      .DoRTCalc(1,6,.f.)
      if not(empty(.w_TCCODCON))
        .link_2_1('Full')
      endif
    endwith
    this.DoRTCalc(7,11,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_TCCODCON = t_TCCODCON
    this.w_TCQTACON = t_TCQTACON
    this.w_TCPERTOL = t_TCPERTOL
    this.w_TCDESCON = t_TCDESCON
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_TCCODCON with this.w_TCCODCON
    replace t_TCQTACON with this.w_TCQTACON
    replace t_TCPERTOL with this.w_TCPERTOL
    replace t_TCDESCON with this.w_TCDESCON
    if i_srv='A'
      replace TCCODCON with this.w_TCCODCON
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mtoPag1 as StdContainer
  Width  = 493
  height = 333
  stdWidth  = 493
  stdheight = 333
  resizeXpos=288
  resizeYpos=240
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTCCODICE_1_1 as StdField with uid="XJKDVLDAWO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TCCODICE", cQueryName = "TCCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice collo",;
    HelpContextID = 120979077,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=92, Top=13, InputMask=replicate('X',5)

  add object oTCDESCRI_1_3 as StdField with uid="CEVOVGJZFK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TCDESCRI", cQueryName = "TCDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 206564993,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=157, Top=13, InputMask=replicate('X',35)

  add object oTC__TARA_1_4 as StdField with uid="RGDFTJOAKG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TC__TARA", cQueryName = "TC__TARA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Tara del collo",;
    HelpContextID = 237256329,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=92, Top=42, cSayPict='"999999.999"', cGetPict='"999999.999"'

  add object oTCVOLUME_1_5 as StdField with uid="PZLQBECBAS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TCVOLUME", cQueryName = "TCVOLUME",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Volume del collo",;
    HelpContextID = 179621509,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=92, Top=71, cSayPict='"9999999.99"', cGetPict='"9999999.99"'

  add object oTCUMVOLU_1_7 as StdField with uid="JXBTXYMAWU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TCUMVOLU", cQueryName = "TCUMVOLU",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura del volume",;
    HelpContextID = 266936715,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=207, Top=71, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_TCUMVOLU"

  func oTCUMVOLU_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oTCUMVOLU_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTCUMVOLU_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oTCUMVOLU_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'',this.parent.oContained
  endproc
  proc oTCUMVOLU_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_TCUMVOLU
    i_obj.ecpSave()
  endproc

  add object oTCFL_MIX_1_11 as StdCheck with uid="JUHKOMUZKN",rtseq=9,rtrep=.f.,left=371, top=71, caption="Confezioni miste",;
    ToolTipText = "Il collo pu� contenere contemporaneamente tutti i tipi di confezioni specificati.",;
    HelpContextID = 242692494,;
    cFormVar="w_TCFL_MIX", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTCFL_MIX_1_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TCFL_MIX,&i_cF..t_TCFL_MIX),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oTCFL_MIX_1_11.GetRadio()
    this.Parent.oContained.w_TCFL_MIX = this.RadioValue()
    return .t.
  endfunc

  func oTCFL_MIX_1_11.ToRadio()
    this.Parent.oContained.w_TCFL_MIX=trim(this.Parent.oContained.w_TCFL_MIX)
    return(;
      iif(this.Parent.oContained.w_TCFL_MIX=='S',1,;
      0))
  endfunc

  func oTCFL_MIX_1_11.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oObj_1_12 as cp_runprogram with uid="SZVVMXOJRL",left=8, top=348, width=171,height=19,;
    caption='GSAR_BCC',;
   bGlobalFont=.t.,;
    prg="GSAR_BCC",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 209915735


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=110, width=485,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="TCCODCON",Label1="Confez.",Field2="TCDESCON",Label2="Descrizione confezione",Field3="TCQTACON",Label3="Numero max",Field4="TCPERTOL",Label4="% Toller.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 32656774

  add object oStr_1_2 as StdString with uid="RNXHJOOACT",Visible=.t., Left=8, Top=15,;
    Alignment=1, Width=83, Height=18,;
    Caption="Codice collo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="MBCNPAOETU",Visible=.t., Left=37, Top=71,;
    Alignment=1, Width=54, Height=18,;
    Caption="Volume:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="XCZDGWGYFF",Visible=.t., Left=178, Top=71,;
    Alignment=1, Width=26, Height=18,;
    Caption="UM:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="FNSPCEUERA",Visible=.t., Left=22, Top=42,;
    Alignment=1, Width=69, Height=18,;
    Caption="Peso tara:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="RZJWCIZSRA",Visible=.t., Left=184, Top=42,;
    Alignment=0, Width=39, Height=18,;
    Caption="in kg."  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=129,;
    width=481+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=130,width=480+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TIPICONF|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TIPICONF'
        oDropInto=this.oBodyCol.oRow.oTCCODCON_2_1
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mtoBodyRow as CPBodyRowCnt
  Width=471
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oTCCODCON_2_1 as StdTrsField with uid="RKXEBIOUEM",rtseq=6,rtrep=.t.,;
    cFormVar="w_TCCODCON",value=space(3),isprimarykey=.t.,;
    ToolTipText = "Codici confezioni adatti alla tipologia collo",;
    HelpContextID = 221642364,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=-2, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="TIPICONF", cZoomOnZoom="GSAR_ATC", oKey_1_1="TCCODICE", oKey_1_2="this.w_TCCODCON"

  func oTCCODCON_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oTCCODCON_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oTCCODCON_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oTCCODCON_2_1.readonly and this.parent.oTCCODCON_2_1.isprimarykey)
    do cp_zoom with 'TIPICONF','*','TCCODICE',cp_AbsName(this.parent,'oTCCODCON_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATC',"Confezioni",'',this.parent.oContained
   endif
  endproc
  proc oTCCODCON_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_TCCODCON
    i_obj.ecpSave()
  endproc

  add object oTCQTACON_2_2 as StdTrsField with uid="PYOJGYJYIY",rtseq=7,rtrep=.t.,;
    cFormVar="w_TCQTACON",value=0,;
    ToolTipText = "Numero massimo delle confezioni per collo",;
    HelpContextID = 224403068,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=75, Left=331, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oTCPERTOL_2_3 as StdTrsField with uid="WHIHFCASQQ",rtseq=8,rtrep=.t.,;
    cFormVar="w_TCPERTOL",value=0,;
    ToolTipText = "Percentuale di tolleranza. Si considera una percentuale di maggiorazione nella occupazione del collo",;
    HelpContextID = 190787198,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=411, Top=0, cSayPict=["9999.99"], cGetPict=["9999.99"]

  func oTCPERTOL_2_3.mCond()
    with this.Parent.oContained
      return (.w_TCFL_MIX='S')
    endwith
  endfunc

  add object oTCDESCON_2_4 as StdTrsField with uid="MTCTIIXOEV",rtseq=10,rtrep=.t.,;
    cFormVar="w_TCDESCON",value=space(35),enabled=.f.,;
    HelpContextID = 206564988,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=258, Left=67, Top=0, InputMask=replicate('X',35)
  add object oLast as LastKeyMover
  * ---
  func oTCCODCON_2_1.When()
    return(.t.)
  proc oTCCODCON_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oTCCODCON_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mto','TIP_COLL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TCCODICE=TIP_COLL.TCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
