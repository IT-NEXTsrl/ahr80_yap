* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_bps                                                        *
*              Parametri acquisizione clienti CPZ                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][22]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-07-08                                                      *
* Last revis.: 2015-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscp_bps",oParentObject,m.pAzione)
return(i_retval)

define class tgscp_bps as StdBatch
  * --- Local variables
  pAzione = space(1)
  w_NEWTOT = 0
  w_OLCATCOM = 0
  w_OLCODVAL = 0
  w_OLCODLIN = 0
  w_OLCODNAZ = 0
  w_OLCODPAG = 0
  w_CODICE = space(5)
  * --- WorkFile variables
  PES_RACC_idx=0
  PARIMPCL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.pAzione="S"
      if this.oParentObject.w_PSCATCOM + this.oParentObject.w_PSCODLIN + this.oParentObject.w_PSCODNAZ + this.oParentObject.w_PSCODPAG + this.oParentObject.w_PSCODVAL = icase(g_REVI="S", 100, g_CPIN="S", 90, 255)
        * --- Write into PES_RACC
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PES_RACC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PES_RACC_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PES_RACC_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PRCATCOM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSCATCOM),'PES_RACC','PRCATCOM');
          +",PRCODVAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSCODVAL),'PES_RACC','PRCODVAL');
          +",PRCODLIN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSCODLIN),'PES_RACC','PRCODLIN');
          +",PRNAZION ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSCODNAZ),'PES_RACC','PRNAZION');
          +",PRCODPAG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSCODPAG),'PES_RACC','PRCODPAG');
              +i_ccchkf ;
          +" where ";
              +"PRCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              PRCATCOM = this.oParentObject.w_PSCATCOM;
              ,PRCODVAL = this.oParentObject.w_PSCODVAL;
              ,PRCODLIN = this.oParentObject.w_PSCODLIN;
              ,PRNAZION = this.oParentObject.w_PSCODNAZ;
              ,PRCODPAG = this.oParentObject.w_PSCODPAG;
              &i_ccchkf. ;
           where;
              PRCODAZI = this.oParentObject.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if i_Rows=0
          * --- Non � stato aggiornato nulla, inserisco il record per l'azienda corrente
          * --- Insert into PES_RACC
          i_nConn=i_TableProp[this.PES_RACC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PES_RACC_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PES_RACC_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PRCODAZI"+",PRCATCOM"+",PRCODVAL"+",PRCODLIN"+",PRNAZION"+",PRCODPAG"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODAZI),'PES_RACC','PRCODAZI');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSCATCOM),'PES_RACC','PRCATCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSCODVAL),'PES_RACC','PRCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSCODLIN),'PES_RACC','PRCODLIN');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSCODNAZ),'PES_RACC','PRNAZION');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSCODPAG),'PES_RACC','PRCODPAG');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PRCODAZI',this.oParentObject.w_CODAZI,'PRCATCOM',this.oParentObject.w_PSCATCOM,'PRCODVAL',this.oParentObject.w_PSCODVAL,'PRCODLIN',this.oParentObject.w_PSCODLIN,'PRNAZION',this.oParentObject.w_PSCODNAZ,'PRCODPAG',this.oParentObject.w_PSCODPAG)
            insert into (i_cTable) (PRCODAZI,PRCATCOM,PRCODVAL,PRCODLIN,PRNAZION,PRCODPAG &i_ccchkf. );
               values (;
                 this.oParentObject.w_CODAZI;
                 ,this.oParentObject.w_PSCATCOM;
                 ,this.oParentObject.w_PSCODVAL;
                 ,this.oParentObject.w_PSCODLIN;
                 ,this.oParentObject.w_PSCODNAZ;
                 ,this.oParentObject.w_PSCODPAG;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        * --- Se esistono regole gi� salvate aggiorno il relativo peso totale della regola
        * --- Select from PARIMPCL
        i_nConn=i_TableProp[this.PARIMPCL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PARIMPCL_idx,2],.t.,this.PARIMPCL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select PACODREG,PACATCOM,PACODVAL,PACODLIN,PACODNAZ,PACODPAG  from "+i_cTable+" PARIMPCL ";
               ,"_Curs_PARIMPCL")
        else
          select PACODREG,PACATCOM,PACODVAL,PACODLIN,PACODNAZ,PACODPAG from (i_cTable);
            into cursor _Curs_PARIMPCL
        endif
        if used('_Curs_PARIMPCL')
          select _Curs_PARIMPCL
          locate for 1=1
          do while not(eof())
          this.w_NEWTOT = 0
          this.w_CODICE = _Curs_PARIMPCL.PACODREG
          this.w_NEWTOT = IIF(EMPTY(_Curs_PARIMPCL.PACATCOM),0, this.oParentObject.w_PSCATCOM)
          this.w_NEWTOT = this.w_NEWTOT+IIF(EMPTY(NVL(_Curs_PARIMPCL.PACODVAL," ")),0, this.oParentObject.w_PSCODVAL)
          this.w_NEWTOT = this.w_NEWTOT+IIF(EMPTY(NVL(_Curs_PARIMPCL.PACODLIN," ")),0, this.oParentObject.w_PSCODLIN)
          this.w_NEWTOT = this.w_NEWTOT+IIF(EMPTY(NVL(_Curs_PARIMPCL.PACODNAZ," ")),0, this.oParentObject.w_PSCODNAZ)
          this.w_NEWTOT = this.w_NEWTOT+IIF(EMPTY(NVL(_Curs_PARIMPCL.PACODPAG," ")),0, this.oParentObject.w_PSCODPAG)
          * --- Write into PARIMPCL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PARIMPCL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PARIMPCL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PARIMPCL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PAPESREG ="+cp_NullLink(cp_ToStrODBC(this.w_NEWTOT),'PARIMPCL','PAPESREG');
                +i_ccchkf ;
            +" where ";
                +"PACODREG = "+cp_ToStrODBC(this.w_CODICE);
                   )
          else
            update (i_cTable) set;
                PAPESREG = this.w_NEWTOT;
                &i_ccchkf. ;
             where;
                PACODREG = this.w_CODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
            select _Curs_PARIMPCL
            continue
          enddo
          use
        endif
        i_retcode = 'stop'
        i_retval = .t.
        return
      else
        this.oParentObject.w_PSCATCOM = 0
        this.oParentObject.w_PSCODLIN = 0
        ah_ErrorMsg("Esiste almeno un valore di priorit� duplicato. Impossibile salvare")
        i_retcode = 'stop'
        i_retval = .f.
        return
      endif
    else
      * --- Read from PES_RACC
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PES_RACC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PES_RACC_idx,2],.t.,this.PES_RACC_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PRCATCOM,PRCODVAL,PRCODLIN,PRNAZION,PRCODPAG"+;
          " from "+i_cTable+" PES_RACC where ";
              +"PRCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PRCATCOM,PRCODVAL,PRCODLIN,PRNAZION,PRCODPAG;
          from (i_cTable) where;
              PRCODAZI = this.oParentObject.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_OLCATCOM = NVL(cp_ToDate(_read_.PRCATCOM),cp_NullValue(_read_.PRCATCOM))
        this.w_OLCODVAL = NVL(cp_ToDate(_read_.PRCODVAL),cp_NullValue(_read_.PRCODVAL))
        this.w_OLCODLIN = NVL(cp_ToDate(_read_.PRCODLIN),cp_NullValue(_read_.PRCODLIN))
        this.w_OLCODNAZ = NVL(cp_ToDate(_read_.PRNAZION),cp_NullValue(_read_.PRNAZION))
        this.w_OLCODPAG = NVL(cp_ToDate(_read_.PRCODPAG),cp_NullValue(_read_.PRCODPAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows=0
        if g_CPIN="S" or g_REVI="S"
          this.oParentObject.w_PSCATCOM = 0
          this.oParentObject.w_PSCODVAL = 25
          this.oParentObject.w_PSCODLIN = 0
          this.oParentObject.w_PSCODNAZ = 30
          this.oParentObject.w_PSCODPAG = IIF(g_REVI="S", 45, 35)
        else
          this.oParentObject.w_PSCATCOM = 100
          this.oParentObject.w_PSCODVAL = 25
          this.oParentObject.w_PSCODLIN = 10
          this.oParentObject.w_PSCODNAZ = 50
          this.oParentObject.w_PSCODPAG = 70
        endif
      else
        this.oParentObject.w_PSCATCOM = this.w_OLCATCOM
        this.oParentObject.w_PSCODLIN = this.w_OLCODLIN
        this.oParentObject.w_PSCODNAZ = this.w_OLCODNAZ
        this.oParentObject.w_PSCODPAG = this.w_OLCODPAG
        this.oParentObject.w_PSCODVAL = this.w_OLCODVAL
        if g_CPIN="S" or g_REVI="S"
          this.oParentObject.w_PSCATCOM = 0
          this.oParentObject.w_PSCODLIN = 0
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PES_RACC'
    this.cWorkTables[2]='PARIMPCL'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PARIMPCL')
      use in _Curs_PARIMPCL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
