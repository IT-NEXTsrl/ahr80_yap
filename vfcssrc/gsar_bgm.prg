* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bgm                                                        *
*              Inserimento matricole da generatore documentale                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-17                                                      *
* Last revis.: 2008-09-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pROWMAT
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bgm",oParentObject,m.pROWMAT)
return(i_retval)

define class tgsar_bgm as StdBatch
  * --- Local variables
  pROWMAT = 0
  w_PADRE = .NULL.
  w_RETVAL = space(254)
  w_bUnderTran = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PADRE = this.oParentObject
    this.w_bUnderTran = vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    if this.w_bUnderTran
      * --- begin transaction
      cp_BeginTrs()
    endif
    if this.w_PADRE.w_MOVIMATR.reccount()<>0 and g_MATR="S" And w_MVDATREG>=nvl(g_DATMAT,cp_CharToDate("  /  /    ")) And this.w_PADRE.w_DOC_DETT.MVQTAMOV<>0 And (this.w_PADRE.w_DOC_DETT.MVFLCASC $ "+-" Or this.w_PADRE.w_DOC_DETT.MVF2CASC $ "+-" Or this.w_PADRE.w_DOC_DETT.MVFLRISE $ "+-" Or this.w_PADRE.w_DOC_DETT.MVF2RISE $ "+-" ) 
      this.w_PADRE.w_MOVIMATR.GoTop()     
      do while !this.w_PADRE.w_MOVIMATR.Eof()
        this.w_PADRE.w_MOVIMATR.ReadCurrentRecord()     
        if this.w_PADRE.w_MOVIMATR.MTROWNUM= iif(this.pROWMAT<>0,this.pROWMAT,this.w_PADRE.w_DOC_DETT.CPROWNUM)
          * --- Seleziono matricole agganciate alla riga documento
          DIMENSION ArrInsertm( 1 ,2)
          * --- Popolo l'array con i dati dei dati ritenute
          this.w_PADRE.w_MOVIMATR.MTSERIAL = this.w_PADRE.w_MVSERIAL
          this.w_PADRE.w_MOVIMATR.MTROWNUM = this.w_PADRE.w_DOC_DETT.CPROWNUM
          this.w_PADRE.w_MOVIMATR.MTNUMRIF = -20
          this.w_PADRE.w_MOVIMATR.cpccchk = cp_NewCCChk()
          this.w_PADRE.w_MOVIMATR.MTKEYSAL = this.w_PADRE.w_DOC_DETT.MVCODART
          this.w_PADRE.w_MOVIMATR.MTCODLOT = IIF(EMPTY(this.w_PADRE.w_MOVIMATR.MTCODLOT),this.w_PADRE.w_DOC_DETT.MVCODLOT,this.w_PADRE.w_MOVIMATR.MTCODLOT)
          if this.w_PADRE.w_DOC_DETT.MVFLCASC="-" Or this.w_PADRE.w_DOC_DETT.MVFLRISE="+" Or this.w_PADRE.w_DOC_DETT.MVF2CASC="-" Or this.w_PADRE.w_DOC_DETT.MVF2RISE="+"
            * --- Determino Magazzino di Scarico
            this.w_PADRE.w_MOVIMATR.MTMAGSCA = IIF(Empty(this.w_PADRE.w_MOVIMATR.MTMAGSCA),IIF(this.w_PADRE.w_DOC_DETT.MVFLCASC="-" Or this.w_PADRE.w_DOC_DETT.MVFLRISE="+", this.w_PADRE.w_DOC_DETT.MVCODMAG, this.w_PADRE.w_DOC_DETT.MVCODMAT),w_MTMAGSCA)
            this.w_PADRE.w_MOVIMATR.MTFLSCAR = IIF(this.w_PADRE.w_DOC_DETT.MVFLCASC="-" Or this.w_PADRE.w_DOC_DETT.MVF2CASC="-","E","R")
            this.w_PADRE.w_MOVIMATR.MTCODUBI = IIF(EMPTY(this.w_PADRE.w_MOVIMATR.MTCODUBI),IIF(this.w_PADRE.w_DOC_DETT.MVFLCASC="-" Or this.w_PADRE.w_DOC_DETT.MVFLRISE="+",this.w_PADRE.w_DOC_DETT.MVCODUBI,this.w_PADRE.w_DOC_DETT.MVCODUB2),this.w_PADRE.w_MOVIMATR.MTCODUBI)
          endif
          if this.w_PADRE.w_DOC_DETT.MVFLCASC="+" Or this.w_PADRE.w_DOC_DETT.MVFLRISE="-" Or this.w_PADRE.w_DOC_DETT.MVF2CASC="+" Or this.w_PADRE.w_DOC_DETT.MVF2RISE="-"
            * --- Determino Magazzino di Carico
            this.w_PADRE.w_MOVIMATR.MTMAGCAR = IIF(Empty(this.w_PADRE.w_MOVIMATR.MTMAGCAR),IIF(this.w_PADRE.w_DOC_DETT.MVFLCASC="+" Or this.w_PADRE.w_DOC_DETT.MVFLRISE="-",this.w_PADRE.w_DOC_DETT.MVCODMAG,this.w_PADRE.w_DOC_DETT.MVCODMAT),this.w_PADRE.w_MOVIMATR.MTMAGCAR)
            this.w_PADRE.w_MOVIMATR.MTFLCARI = IIF(this.w_PADRE.w_DOC_DETT.MVFLCASC="+" Or this.w_PADRE.w_DOC_DETT.MVF2CASC="+","E","R")
            this.w_PADRE.w_MOVIMATR.MTCODUBI = IIF(EMPTY(this.w_PADRE.w_MOVIMATR.MTCODUBI),IIF(this.w_PADRE.w_DOC_DETT.MVFLCASC="+" Or this.w_PADRE.w_DOC_DETT.MVFLRISE="-",this.w_PADRE.w_DOC_DETT.MVCODUBI,this.w_PADRE.w_DOC_DETT.MVCODUB2),this.w_PADRE.w_MOVIMATR.MTCODUBI)
          endif
          this.w_PADRE.w_MOVIMATR.SaveCurrentRecord()     
          this.w_PADRE.w_MOVIMATR.CurrentRecordToArray(@ArrInsertm)     
          this.w_RETVAL = InsertTable("MOVIMATR", @ArrInsertm )
          if !EMPTY(this.w_RETVAL)
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=ah_Msgformat("Errore inserimento riga matricole.%0%1", this.w_RETVAL)
          endif
        endif
        Release ArrInsertm
        this.w_PADRE.w_MOVIMATR.Next()     
      enddo
    endif
    if this.w_bUnderTran
      if EMPTY(this.w_RETVAL)
        * --- commit
        cp_EndTrs(.t.)
      else
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
    endif
    i_retcode = 'stop'
    i_retval = this.w_RETVAL
    return
  endproc


  proc Init(oParentObject,pROWMAT)
    this.pROWMAT=pROWMAT
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pROWMAT"
endproc
