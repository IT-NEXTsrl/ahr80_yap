* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bek                                                        *
*              Controlli integrazioni                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-12                                                      *
* Last revis.: 2006-04-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bek",oParentObject)
return(i_retval)

define class tgscg_bek as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PADRE = this.oParentObject
    if this.oParentObject.w_VBESPDET="S"
      if ah_yesno("Attenzione l'esportazione dettagliata richiede l'azzeramento dei vincoli ,delle formule e del segno , si desidera continuare?")
        * --- Azzero Vincolo e Formula al cambiare del flag esportazione
        this.w_PADRE.Markpos()     
        this.w_PADRE.FirstRow()     
        do while Not this.w_PADRE.Eof_Trs()
          this.w_PADRE.SetRow()     
          if this.w_PADRE.FullRow() 
            this.oParentObject.w_VBCONDIZ = "N"
            this.oParentObject.w_VBFORMUL = SPACE(50)
            this.oParentObject.w_VB_SEGNO = 1
            this.w_PADRE.SaveRow()     
          endif
          this.w_PADRE.NextRow()     
        enddo
        this.w_PADRE.Repos()     
      else
        this.oParentObject.w_VBESPDET = "N"
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
