* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_brs                                                        *
*              Ripartizione spese e sconti su documento                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_72]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-03-18                                                      *
* Last revis.: 2014-06-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPadre,pCursor,pFlveac,pFlscor,pSpeacc,pSconti,pTotMerce,pCodive,pCaoval,pDatcom,pCaonaz,pValnaz,pCodval,pSpeaccIva,pParent,pContributi
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_brs",oParentObject,m.pPadre,m.pCursor,m.pFlveac,m.pFlscor,m.pSpeacc,m.pSconti,m.pTotMerce,m.pCodive,m.pCaoval,m.pDatcom,m.pCaonaz,m.pValnaz,m.pCodval,m.pSpeaccIva,m.pParent,m.pContributi)
return(i_retval)

define class tgsar_brs as StdBatch
  * --- Local variables
  w_IMPACC_KIT = 0
  pPadre = space(2)
  pCursor = space(10)
  pFlveac = space(1)
  pFlscor = space(1)
  pSpeacc = 0
  pSconti = 0
  pTotMerce = 0
  pCodive = space(5)
  pCaoval = 0
  pDatcom = ctod("  /  /  ")
  pCaonaz = 0
  pValnaz = space(3)
  pCodval = space(3)
  pSpeaccIva = 0
  pParent = .NULL.
  pContributi = space(1)
  w_PADRE = space(1)
  w_Cursor = space(10)
  w_ACCSPE = 0
  w_SPEACC = 0
  w_PREC = 0
  w_TIMPACC = 0
  w_PRES = 0
  w_TIMPSCO = 0
  w_CODIVA = space(5)
  w_INDIVA = 0
  w_FLSERA = space(1)
  w_DATCOM = ctod("  /  /  ")
  w_COCODVAL = space(3)
  w_DECCOM = 0
  w_PERIVE = 0
  w_INDIVE = 0
  w_IMPACC = 0
  w_IMPSCO = 0
  w_VALMAG = 0
  w_IMPNAZ = 0
  w_CODCOM = space(15)
  w_IMPCOM = 0
  w_FLRINC = space(1)
  w_FLRIMB = space(1)
  w_FLRTRA = space(1)
  w_FLVEAC = space(1)
  w_FLSCOR = space(1)
  w_SPEINC = 0
  w_SPEIMB = 0
  w_SPETRA = 0
  w_SPEBOL = 0
  w_IMPARR = 0
  w_SCONTI = 0
  w_TOTMERCE = 0
  w_TOTMERCE_GIARIPA = 0
  w_CODIVE = space(5)
  w_DECTOT = 0
  w_CAOVAL = 0
  w_DATREG = ctod("  /  /  ")
  w_CAONAZ = 0
  w_VALNAZ = space(3)
  w_CODVAL = space(3)
  w_DECTOP = 0
  w_DECTOU = 0
  w_SPEACC_IVA = 0
  w_IMPACC_IVA = 0
  w_MSG_ERR = space(200)
  w_TIPRIG = space(1)
  w_TDRIPCON = space(1)
  w_ASSEGNA_CONTRIB_A_KIT = .f.
  w_ERRORESURIPARTIZIONEKIT = .f.
  w_MYSELF = .NULL.
  w_GSxx_MDV = .NULL.
  w_CURSTOT1 = space(10)
  w_CURSTOT3 = space(10)
  w_CURSTOT1_SCONTI = space(10)
  w_CURSTOT_GIARIPA = space(10)
  w_FILTER_MVFLOMAG = space(10)
  w_FILTER_MVESCRIP = space(10)
  w_FILTER2_MVESCRIP = space(10)
  w_CURSTOT2 = space(10)
  w_TOTMERCE_SCONTI = 0
  w_SPEACC_GIARIPA = 0
  w_AZZERA_MVESCRIP = .f.
  w_CURSTOT2 = space(10)
  w_TOTMERCE_SCONTI = 0
  w_ESCRIP = space(1)
  w_RIFKIT = 0
  w_FLOMAG = space(1)
  w_FLSERA = space(1)
  w_VALRIG = 0
  w_SCOVEN = 0
  w_PERIVA = 0
  w_TCODCOM = space(15)
  w_TQTAUM1 = 0
  w_ROWNUM = 0
  w_RIFCAC = 0
  w_SBLOCCO = .f.
  w_ESCRIP = space(1)
  w_RIFKIT = 0
  w_FLOMAG = space(1)
  w_FLSERA = space(1)
  w_VALRIG = 0
  w_SCOVEN = 0
  w_PERIVA = 0
  w_TCODCOM = space(15)
  w_TQTAUM1 = 0
  w_ROWNUM = 0
  w_RIFCAC = 0
  w_RIMPACC = 0
  w_RIMPSCO = 0
  w_CURSORE_KIT_1 = space(10)
  w_CURSORE_KIT_2 = space(10)
  w_FIELD_MVESCRIP = space(10)
  w_FIELD2_MVESCRIP = space(10)
  w_CURSORE_KIT = space(10)
  w_CURSORE_KIT_3 = space(10)
  w_CURSORE_KIT_4 = space(10)
  w_IMPORTO_RAEE_CONTRIBUTI_COMPONENTI = 0
  w_CURSORE_COAC = space(10)
  w_IMPSCO_KIT = 0
  w_RIGARESTO = 0
  w_RIGARESTOS = 0
  w_ARRIPCON = space(1)
  w_IMP_RAEE = 0
  w_PRESTA = space(1)
  w_FLSCOM = space(1)
  w_OLDCODIVA = space(5)
  w_TFLORCO = space(1)
  w_TFLEVAS = space(1)
  w_VALULT = 0
  * --- WorkFile variables
  CAN_TIER_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch per la ripartizione delle spese accessorie e sconti sulle righe documento
    *     utilizzato dai documenti stessi e dai vari batch di generazione
    *     
    *     Restituisce eventuale msg se incontra problemi altrimenti stringa vuota..
    *     
    *     pPadre : Lanciato da :   'B' -> Batchs di generazione documenti
    *                   oppure dalla gestione documenti
    *                                           'E' ->Evento RipaEnt da Documenti
    *     
    *     pCursor : Nome del cursore passato dal batch padre
    *     pFlveac : Test Vendite Acquisti ('V','A')
    *     pFlscor : Flag Scorporo Piede Fattura 
    *     pSpeacc : Totale spese Accessorie (Incasso, Imballo, Trasporto e se acquisti Spese Bolli) solo senza scorporo e senza codici iva spese
    *     pSconti : Valore degli sconti di Piede (MVSCONTI sui documenti)
    *     pTotMerce : variabile w_TOTMERCE dei Documenti. Totale Scontato della merce
    *     pCodive : Codice Iva Agevolato
    *     pCaoval : Cabio del Documento
    *     pDatCom: Data Documento se vuota Data Registrazione
    *     pCaonaz : Cambio della Valuta di conto
    *     pValnaz : Valuta di Conto
    *     pCodval : Valuta del documento
    *     pSpeaccIva : Totale spese Accessorie (Incasso, Imballo, Trasporto) scorporate nel caso di scorpoto piede fattura e codice Iva delle spese presente
    *     pParent : Se lanciato da gestione documentale riferimento alla gestione..
    *     pContributi : Se 'S' i contributi non a peso aggiornano il valore fiscale del relativo articolo - dipende dalla causale del documento (TDRIPCON)
    * --- CAMPI di pCURSOR NECESSARI AL FUNZIONAMENTO DI GSAR_BRS
    *     CPROWNUM
    *     t_ARRIPCON
    *     t_FLSERA
    *     t_IMP_RAEE
    *     t_MVCODCOM
    *     t_MVCODICE
    *     t_MVCODIVA
    *     t_MVFLOMAG
    *     t_MVIMPACC
    *     t_MVIMPCOM
    *     t_MVIMPNAZ
    *     t_MVIMPSCO
    *     t_MVQTAUM1
    *     t_MVRIFCAC
    *     t_MVRIFKIT
    *     t_MVTIPRIG
    *     t_MVVALMAG
    *     t_MVVALRIG
    *     t_PERIVA
    this.w_PADRE = this.pPadre
    this.w_FLVEAC = this.pFlveac
    this.w_FLSCOR = this.pFlscor
    this.w_SPEACC = this.pSpeacc
    this.w_SCONTI = this.pSconti
    this.w_TOTMERCE = this.pTotMerce
    this.w_CODIVE = this.pCodive
    this.w_CAOVAL = this.pCaoval
    this.w_DATCOM = this.pDatcom
    this.w_CAONAZ = this.pCaonaz
    this.w_VALNAZ = this.pValnaz
    this.w_CODVAL = this.pCodval
    this.w_SPEACC_IVA = this.pSpeaccIva
    this.w_TDRIPCON = IIF( VARTYPE( this.pContributi ) = "C" , this.pContributi , " " )
    * --- UTILIZZATA PER ALLINEARE I KIT AL VALORE DEL CONTRIBUTO AD ESSO ASSOCIATO
    this.w_ASSEGNA_CONTRIB_A_KIT = .F.
    * --- Utilizzate se la ripartizione sui kit non pu� essere eseguita
    this.w_ERRORESURIPARTIZIONEKIT = .f.
    if Not Empty(this.w_CODIVE)
      * --- Leggo la percentuale Iva e Indetraibilit� del Codice Iva Agevolato Se Presente
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA,IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA,IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_CODIVE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIVE = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        this.w_INDIVE = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Lettura decimali totali della valuta del documento
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_CODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura decimali totali della valuta di conto
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT,VADECUNI"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_VALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT,VADECUNI;
        from (i_cTable) where;
            VACODVAL = this.w_VALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOP = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      this.w_DECTOU = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CODCOM = Repl("@",16)
    this.w_Cursor = Alltrim(this.pCursor)
    * --- Totale Spese da Ripartire
    * --- Totalizzatori
    this.w_TIMPACC = 0
    this.w_TIMPSCO = 0
    this.w_PREC = 0
    this.w_PRES = 0
    this.w_MYSELF = This
    if this.pPadre = "B"
      this.w_GSxx_MDV = THIS
    else
      this.w_GSxx_MDV = this.pParent
    endif
    * --- t_FLSERA
    *     t_MVCODICE
    *     t_MVFLOMAG
    *     t_MVRIFCAC
    *     t_MVRIFKIT
    *     t_MVTIPRIG
    *     t_MVVALRIG
    * --- Calcola w_TOTMERCE
    * --- Il calcolo di w_TOTMERCE � stato spostato all'interno di questo batch per evitare la
    *     necessit� di ricalcolarlo da ogni batch che esegue la chiamata
    * --- La condizione (NVL(t_FLSERA,' ')<>'S' OR t_MVRIFCAC>0) indica che nel totale del documento
    *     occorre includere le righe di contributi accessori non a peso.
    *     (i contributi accessori gestiti a peso hanno MVRIFCAC=-1).
    * --- Poich� occorre escludere dal totale documento i contributi
    *     accessori legati al componente di un kit, si costruisce un primo 
    *     cursore con il campo MVRIFCAC, e un secondo cursore con 
    *     CPROWNUM e MVRIFKIT
    *     Se, legando MVRIFCAC a CPRWONUM risulta che MVRIFKIT � valorizzato, allora il 
    *     l'importo relativo non deve essere conteggiato nel totale perch�
    *     risultante dall'esplosione di un componente di kit
    this.w_CURSTOT1 = SYS( 2015 )
    this.w_CURSTOT3 = SYS( 2015 )
    this.w_CURSTOT1_SCONTI = SYS( 2015 )
    this.w_CURSTOT_GIARIPA = SYS( 2015 ) 
    this.w_TOTMERCE_GIARIPA = 0
    if this.w_MYSELF.GETTYPE("w_MVFLOMAG") = "N"
      this.w_FILTER_MVFLOMAG = "IIF(t_MVFLOMAG=1,'X',' ' )='X'"
    else
      this.w_FILTER_MVFLOMAG = "t_MVFLOMAG='X'"
    endif
    * --- Se esiste MVESCRIP lo utilizza, altrimenti usa il suo valore di default 'N'
    if this.w_MYSELF.GETTYPE("w_MVESCRIP") <> "U"
      this.w_FILTER_MVESCRIP = "t_MVESCRIP<>'S'"
      this.w_FILTER2_MVESCRIP = "t_MVESCRIP='S'"
    else
      this.w_FILTER_MVESCRIP = "'N'<>'S'"
      this.w_FILTER2_MVESCRIP = "'N'='S'"
    endif
    if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
      this.w_GSxx_MDV.Exec_Select(this.w_CURSTOT1,"CPROWNUM AS CPROWNUM, t_MVVALRIG As MVValRig, t_MVRIFCAC AS MVRIFCAC"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND " + this.w_FILTER_MVFLOMAG + " And (NVL(t_FLSERA,' ')<>'S' OR t_MVRIFCAC>0) AND t_MVVALRIG>0 AND t_MVRIFKIT=0 AND " + this.w_FILTER_MVESCRIP,"","","")     
      * --- Calcola il totale su cui la ripartizione � gia avvenuta
      this.w_GSxx_MDV.Exec_Select(this.w_CURSTOT3,"CPROWNUM AS CPROWNUM, t_MVVALRIG As MVValRig, t_MVRIFCAC AS MVRIFCAC"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND " + this.w_FILTER_MVFLOMAG + " And (NVL(t_FLSERA,' ')<>'S' OR t_MVRIFCAC>0) AND t_MVVALRIG>0 AND t_MVRIFKIT=0 AND " + this.w_FILTER2_MVESCRIP,"","","")     
      * --- calcola anche il totale su cui devono essere pesati gli sconti
      this.w_GSxx_MDV.Exec_Select(this.w_CURSTOT1_SCONTI,"CPROWNUM AS CPROWNUM, t_MVVALRIG As MVValRig, t_MVRIFCAC AS MVRIFCAC"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND " + this.w_FILTER_MVFLOMAG + " And NVL(t_FLSERA,' ')<>'S' AND t_MVVALRIG>0 AND t_MVRIFKIT=0","","","")     
      * --- calcola, inoltre, la somma gi� ripartita dall'utente (MVESCRIP='S'), che deve quindi essere sottratta dal totale da ripartire
      this.w_GSxx_MDV.Exec_Select(this.w_CURSTOT_GIARIPA,"CPROWNUM AS CPROWNUM, t_MVIMPACC As MVIMPACC, t_MVRIFCAC AS MVRIFCAC"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND " + this.w_FILTER_MVFLOMAG + " And NVL(t_FLSERA,' ')<>'S' AND t_MVVALRIG>0 AND t_MVRIFKIT=0 AND " + this.w_FILTER2_MVESCRIP,"","","")     
      this.w_CURSTOT2 = SYS( 2015 )
      this.w_GSxx_MDV.Exec_Select(this.w_CURSTOT2,"CPROWNUM As CPROWNUM1, t_MVRIFKIT AS MVRIFKIT","","","","")     
      SELECT SUM( MVVALRIG ) AS VALRIG FROM ( this.w_CURSTOT1 ) LEFT OUTER JOIN ( this.w_CURSTOT2 ) ; 
 ON MVRIFCAC = CPROWNUM1 WHERE NVL( MVRIFKIT , 0 ) = 0 INTO CURSOR _Tmp_BCC
      this.w_TOTMERCE = Nvl( _Tmp_BCC.ValRig , 0 )
      SELECT SUM( MVVALRIG ) AS VALRIG FROM ( this.w_CURSTOT1_SCONTI ) LEFT OUTER JOIN ( this.w_CURSTOT2 ) ; 
 ON MVRIFCAC = CPROWNUM1 WHERE NVL( MVRIFKIT , 0 ) = 0 INTO CURSOR _Tmp_BCC
      this.w_TOTMERCE_SCONTI = Nvl( _Tmp_BCC.ValRig , 0 )
      SELECT SUM( MVVALRIG ) AS VALRIG FROM ( this.w_CURSTOT3 ) LEFT OUTER JOIN ( this.w_CURSTOT2 ) ; 
 ON MVRIFCAC = CPROWNUM1 WHERE NVL( MVRIFKIT , 0 ) = 0 INTO CURSOR _Tmp_BCC
      this.w_TOTMERCE_GIARIPA = Nvl( _Tmp_BCC.ValRig , 0 )
      Use in _Tmp_BCC 
      SELECT ( this.w_CURSTOT_GIARIPA )
      SUM MVIMPACC TO L_CURSTOT_GIARIPA
      * --- calcola la somma gi� ripartita dall'utente (MVESCRIP='S'), che deve quindi essere sottratta dal totale da ripartire
      this.w_SPEACC_GIARIPA = NVL( L_CURSTOT_GIARIPA , 0 )
      * --- Se non ci sono spese da ripartire, memorizza questa informazione per sbiancare MVESCRIP
      this.w_AZZERA_MVESCRIP = .F.
      if this.w_SPEACC = 0
        this.w_AZZERA_MVESCRIP = .T.
      else
        * --- Se ci sono spese accessorie da ripartire, quelle assegnate manualmente non vengono considerate
        this.w_SPEACC = this.w_SPEACC - this.w_SPEACC_GIARIPA
      endif
      USE IN SELECT (this.w_CURSTOT3)
    else
      if this.w_MYSELF.GetType("w_MVRIFKIT") = "N"
        if this.w_MYSELF.GETTYPE("w_MVFLOMAG") = "N"
          this.w_GSxx_MDV.Exec_Select(this.w_CURSTOT1,"CPROWNUM AS CPROWNUM, t_MVVALRIG As MVValRig, t_MVRIFCAC AS MVRIFCAC"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND IIF(t_MVFLOMAG=1,'X',' ' )='X' And (t_FLSERA<>'S' OR t_MVRIFCAC>0) AND t_MVVALRIG>0 AND t_MVRIFKIT=0","","","")     
          * --- calcola anche il totale su cui devono essere pesati gli sconti
          this.w_GSxx_MDV.Exec_Select(this.w_CURSTOT1_SCONTI,"CPROWNUM AS CPROWNUM, t_MVVALRIG As MVValRig, t_MVRIFCAC AS MVRIFCAC"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND IIF(t_MVFLOMAG=1,'X',' ' )='X' And t_FLSERA<>'S' AND t_MVVALRIG>0 AND t_MVRIFKIT=0","","","")     
        else
          this.w_GSxx_MDV.Exec_Select(this.w_CURSTOT1,"CPROWNUM AS CPROWNUM, t_MVVALRIG As MVValRig, t_MVRIFCAC AS MVRIFCAC"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND t_MVFLOMAG='X' And (t_FLSERA<>'S' OR t_MVRIFCAC>0) AND t_MVVALRIG>0 AND t_MVRIFKIT=0","","","")     
          * --- calcola anche il totale su cui devono essere pesati gli sconti
          this.w_GSxx_MDV.Exec_Select(this.w_CURSTOT1_SCONTI,"CPROWNUM AS CPROWNUM, t_MVVALRIG As MVValRig, t_MVRIFCAC AS MVRIFCAC"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND t_MVFLOMAG='X' And t_FLSERA<>'S' AND t_MVVALRIG>0 AND t_MVRIFKIT=0","","","")     
        endif
        this.w_CURSTOT2 = SYS( 2015 )
        this.w_GSxx_MDV.Exec_Select(this.w_CURSTOT2,"CPROWNUM As CPROWNUM1, t_MVRIFKIT AS MVRIFKIT","","","","")     
        SELECT SUM( MVVALRIG ) AS VALRIG FROM ( this.w_CURSTOT1 ) LEFT OUTER JOIN ( this.w_CURSTOT2 ) ; 
 ON MVRIFCAC = CPROWNUM1 WHERE NVL( MVRIFKIT , 0 ) = 0 INTO CURSOR _Tmp_BCC
        SELECT SUM( MVVALRIG ) AS VALRIG FROM ( this.w_CURSTOT1 ) LEFT OUTER JOIN ( this.w_CURSTOT2 ) ; 
 ON MVRIFCAC = CPROWNUM1 WHERE NVL( MVRIFKIT , 0 ) = 0 INTO CURSOR _Tmp_BCC_SCONTI
      else
        if this.w_MYSELF.GETTYPE("w_MVRIFCAC") = "N"
          if this.w_MYSELF.GETTYPE("w_MVFLOMAG") = "N"
            this.w_GSxx_MDV.Exec_Select("_Tmp_BCC","SUM( t_MVVALRIG ) As ValRig"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND IIF(t_MVFLOMAG=1,'X',' ' )='X' And (t_FLSERA<>'S' OR t_MVRIFCAC>0) AND t_MVVALRIG>0","","","")     
            * --- calcola anche il totale su cui devono essere pesati gli sconti
            if !Isalt()
              this.w_GSxx_MDV.Exec_Select("_Tmp_BCC_SCONTI","SUM( t_MVVALRIG ) As ValRig"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND IIF(t_MVFLOMAG=1,'X',' ' )='X' And t_FLSERA<>'S' AND t_MVVALRIG>0","","","")     
            else
              this.w_GSxx_MDV.Exec_Select("_Tmp_BCC_SCONTI","SUM( t_MVVALRIG ) As ValRig"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND IIF(t_MVFLOMAG=1,'X',' ' )='X' And t_FLSERA<>'S' AND t_MVVALRIG>0 and !(t_PRESTA $ 'S-A')","","","")     
              if Vartype(this.pParent)="O" AND this.pParent.w_MVFLFOSC<>"S"
                this.w_TOTMERCE_SCONTI = Nvl( _Tmp_BCC_SCONTI.ValRig , 0 )
                this.w_SCONTI = CalSco(this.w_TOTMERCE_SCONTI , this.pParent.w_MVSCOCL1 , this.pParent.w_MVSCOCL2 , this.pParent.w_MVSCOPAG, this.w_DECTOT)
                this.pParent.w_MVSCONTI = this.w_SCONTI
              endif
            endif
          else
            this.w_GSxx_MDV.Exec_Select("_Tmp_BCC","SUM( t_MVVALRIG ) As ValRig"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND t_MVFLOMAG='X' And (t_FLSERA<>'S' OR t_MVRIFCAC>0) AND t_MVVALRIG>0","","","")     
            * --- calcola anche il totale su cui devono essere pesati gli sconti
            this.w_GSxx_MDV.Exec_Select("_Tmp_BCC_SCONTI","SUM( t_MVVALRIG ) As ValRig"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND t_MVFLOMAG='X' And t_FLSERA<>'S' AND t_MVVALRIG>0","","","")     
          endif
        else
          if this.w_MYSELF.GETTYPE("w_MVFLOMAG") = "N"
            this.w_GSxx_MDV.Exec_Select("_Tmp_BCC","SUM( t_MVVALRIG ) As ValRig"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND IIF(t_MVFLOMAG=1,'X',' ' )='X' And t_FLSERA<>'S' AND t_MVVALRIG>0","","","")     
            this.w_GSxx_MDV.Exec_Select("_Tmp_BCC_SCONTI","SUM( t_MVVALRIG ) As ValRig"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND IIF(t_MVFLOMAG=1,'X',' ' )='X' And t_FLSERA<>'S' AND t_MVVALRIG>0","","","")     
          else
            this.w_GSxx_MDV.Exec_Select("_Tmp_BCC","SUM( t_MVVALRIG ) As ValRig"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND t_MVFLOMAG='X' And t_FLSERA<>'S' AND t_MVVALRIG>0","","","")     
            this.w_GSxx_MDV.Exec_Select("_Tmp_BCC_SCONTI","SUM( t_MVVALRIG ) As ValRig"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG $ 'RFMA' AND t_MVFLOMAG='X' And t_FLSERA<>'S' AND t_MVVALRIG>0","","","")     
          endif
        endif
      endif
      this.w_TOTMERCE = Nvl( _Tmp_BCC.ValRig , 0 )
      Use in _Tmp_BCC 
      this.w_TOTMERCE_SCONTI = Nvl( _Tmp_BCC_SCONTI.ValRig , 0 )
      Use in _Tmp_BCC_SCONTI
    endif
    * --- Se w_TDRIPCON = "S" i contributi sono ripartiti e quindi il totale deve comprendere le righe di contributo accessorio
    if this.w_TDRIPCON = "S"
      this.w_TOTMERCE_SCONTI = this.w_TOTMERCE + this.w_TOTMERCE_GIARIPA
    endif
    if USED( this.w_CURSTOT1 )
      SELECT ( this.w_CURSTOT1 )
      USE
    endif
    if USED( this.w_CURSTOT1_SCONTI )
      SELECT ( this.w_CURSTOT1_SCONTI )
      USE
    endif
    if USED( this.w_CURSTOT2 )
      SELECT ( this.w_CURSTOT2 )
      USE
    endif
    if USED( this.w_CURSTOT_GIARIPA )
      SELECT ( this.w_CURSTOT_GIARIPA )
      USE
    endif
    if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if g_COAC = "S" AND this.w_TDRIPCON = "S"
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_MYSELF.FirstRow()     
    do while Not this.w_MYSELF.Eof_Trs()
      this.w_FLOMAG = IIF( this.w_MYSELF.GetType("w_MVFLOMAG") = "C" , this.w_MYSELF.Get("w_MVFLOMAG") , IIF( this.w_MYSELF.Get("w_MVFLOMAG")=1 , "X" , " " ) )
      this.w_TIPRIG = Nvl(this.w_MYSELF.Get("w_MVTIPRIG")," ")
      this.w_ESCRIP = "N" 
      if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
        this.w_RIFKIT = this.w_MYSELF.Get("w_MVRIFKIT")
        if this.w_MYSELF.GETTYPE("w_MVESCRIP") <> "U"
          if this.w_AZZERA_MVESCRIP
            * --- Non ci sono spese da ripartire
            this.w_MYSELF.Set("w_MVESCRIP" , "N")     
          endif
          this.w_ESCRIP = this.w_MYSELF.Get("w_MVESCRIP")
          * --- w_escrip = 'S' esclude la riga dalla ripartizione
        endif
      endif
      * --- Se riga sconto merce o descrittiva � scartata
      if this.w_MYSELF.FullRow()
        this.w_IMPACC = 0
        this.w_IMPSCO = 0
        this.w_IMPACC_IVA = 0
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
          * --- Gestione kit
          if this.w_TIPRIG = "A"
            * --- Gestione kit
            *     Dopo aver calcolato gli importi MVIMPACC e MVIMPSCO
            *     degli articoli padfre di kit, li scrive nel cursore w_CURSORE_KIT
            *     in modo che possano essere utilizzati per calcolare i corrispondenti importi da assegnare ai kit
            UPDATE (this.w_CURSORE_KIT) SET MVIMPACC = this.w_IMPACC + this.w_IMPACC_IVA - NORIPACC, ; 
 MVIMPSCO =this.w_IMPSCO ,; 
 RESTO_IMPACC = this.w_IMPACC + this.w_IMPACC_IVA - NORIPACC ,; 
 RESTO_IMPSCO = this.w_IMPSCO ; 
 WHERE MVRIFKIT = this.w_ROWNUM
          endif
        endif
      endif
      this.w_MYSELF.NextRow()     
    enddo
    * --- Se c'e' del Resto sulle spese Accessorie...
    if this.w_SPEACC + this.w_SPEACC_IVA<>this.w_TIMPACC AND this.w_PREC<>0
      * --- Lo Scrive sulla prima Riga
      this.w_ACCSPE = 1
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Ho del resto (spese valorizzate) e non sono riuscito a determinare
      *     una riga valida (tutte righe omaggio o di tipo spesa).
      *     Restituisco una segnalazione di errore...
      if this.w_SPEACC + this.w_SPEACC_IVA<>0 And this.w_PREC=0
        this.w_SBLOCCO = .T.
        if this.pPADRE = "E" AND UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
          * --- Se sono nei documenti ( pPADRE = "E" e non ho ancora confermato la testata, il messaggio non deve uscire
          this.w_SBLOCCO = this.w_GSxx_MDV.w_SBLOCCO
        endif
        if this.w_SBLOCCO
          this.w_MSG_ERR = ah_Msgformat("Impossibile ripartire le spese accessorie [%1]. Inserire eventuali spese utilizzando esclusivamente righe con servizio accessorio con tipo omaggio normale", Alltrim(Tran( this.w_SPEACC + this.w_SPEACC_IVA, v_PV[38+VVL] )) )
        endif
      endif
    endif
    * --- Se c'e' del Resto sugli Sconti Finali...
    if this.w_SCONTI<>this.w_TIMPSCO AND this.w_PRES<>0
      * --- Lo Scrive sulla prima Riga
      this.w_ACCSPE = 2
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Esegue il ciclo sulle righe componenti di kit che non sono state considerate nel ciclo precedente
    if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
      UPDATE ( this.w_CURSORE_KIT ) SET MVIMPSCO = MAX( MVIMPSCO, -( TOTVALRIG + MVIMPACC ) ), RESTO_IMPSCO = MAX( RESTO_IMPSCO, -( TOTVALRIG + MVIMPACC ) )
      this.w_MYSELF.FirstRow()     
      do while Not this.w_MYSELF.Eof_Trs()
        this.w_FLOMAG = IIF( this.w_MYSELF.GetType("w_MVFLOMAG") = "C" , this.w_MYSELF.Get("w_MVFLOMAG") , IIF( this.w_MYSELF.Get("w_MVFLOMAG")=1 , "X" , " " ) )
        this.w_TIPRIG = Nvl(this.w_MYSELF.Get("w_MVTIPRIG")," ")
        this.w_ESCRIP = "N" 
        this.w_RIFKIT = this.w_MYSELF.Get("w_MVRIFKIT")
        if this.w_AZZERA_MVESCRIP
          * --- Non ci sono spese da ripartire
          this.w_MYSELF.Set("w_MVESCRIP" , "N")     
        endif
        this.w_ESCRIP = this.w_MYSELF.Get("w_MVESCRIP")
        * --- w_escrip = 'S' esclude la riga dalla ripartizione
        * --- Se riga sconto merce o descrittiva � scartata
        if this.w_MYSELF.FullRow()
          this.w_IMPACC = 0
          this.w_IMPSCO = 0
          this.w_IMPACC_IVA = 0
          this.w_VALRIG = this.w_MYSELF.GET("w_MVVALRIG")
          this.w_FLSERA = NVL(this.w_MYSELF.Get("w_FLSERA"), " ")
          * --- Gestione kit
          if this.w_RIFKIT <> 0 AND this.w_FLOMAG="X" AND this.w_FLSERA<>"S" And this.w_TIPRIG<>"D" and this.w_VALRIG>0
            * --- Gestione kit
            *     Sulle righe dei componenti assegna ai campi MVIMPACC e MVIMPSCO
            *     il valore risultante dalla ripartizione dei corrispondenti importi dell'articolo padre
            this.Pag5()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        this.w_MYSELF.NextRow()     
      enddo
      * --- Ass resti relativi alla ripartizione sui kit
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_ERRORESURIPARTIZIONEKIT
        this.w_MSG_ERR = ah_Msgformat("Impossibile ripartire le spese accessorie sui componenti del kit.%0Aggiornare le spese accessorie sulle righe del documento oppure deselezionare la ripartizione manuale" )
      endif
    endif
    if USED( this.w_CURSORE_KIT )
      SELECT ( this.w_CURSORE_KIT )
      USE
    endif
    * --- Metto a null per problema class remove
    this.w_GSxx_MDV = .null.
    i_retcode = 'stop'
    i_retval = this.w_MSG_ERR
    return
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lo Scrive sulla prima Riga valida..
    this.w_MYSELF.SetRow(IIF( this.w_ACCSPE=1, this.w_PREC, this.w_PRES ))     
    this.w_RIMPACC = this.w_MYSELF.Get("w_MVIMPACC")
    this.w_RIMPSCO = this.w_MYSELF.Get("w_MVIMPSCO")
    if this.w_MYSELF.GetType("CPROWNUM") = "N"
      this.w_ROWNUM = this.w_MYSELF.Get("CPROWNUM")
    endif
    this.w_VALRIG = this.w_MYSELF.Get("w_MVVALRIG")
    if this.w_MYSELF.GetType("w_ARRIPCON") = "C"
      this.w_ARRIPCON = this.w_MYSELF.Get("w_ARRIPCON")
    else
      this.w_ARRIPCON = " "
    endif
    if this.w_MYSELF.GetType("w_IMP_RAEE") = "N"
      this.w_IMP_RAEE = this.w_MYSELF.Get("w_IMP_RAEE")
    else
      this.w_IMP_RAEE = 0
    endif
    this.w_PERIVA = this.w_MYSELF.Get("w_PERIVA")
    if this.w_ACCSPE=1
      this.w_IMPACC = this.w_RIMPACC + (this.w_SPEACC + this.w_SPEACC_IVA - this.w_TIMPACC)
    else
      this.w_IMPSCO = this.w_RIMPSCO + (this.w_SCONTI - this.w_TIMPSCO)
    endif
    this.w_VALMAG = CAVALMAG(this.w_FLSCOR, this.w_VALRIG, IIF( this.w_ACCSPE=1, this.w_RIMPSCO, this.w_IMPSCO ), IIF( this.w_ACCSPE=1, this.w_IMPACC, this.w_RIMPACC), NVL(this.w_PERIVA, 0), this.w_DECTOT, this.w_CODIVE, this.w_PERIVE, , this.w_TDRIPCON , this.w_ARRIPCON, this.w_IMP_RAEE)
    this.w_CODIVA = this.w_MYSELF.Get("w_MVCODIVA")
    * --- Read from VOCIIVA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VOCIIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IVPERIND"+;
        " from "+i_cTable+" VOCIIVA where ";
            +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IVPERIND;
        from (i_cTable) where;
            IVCODIVA = this.w_CODIVA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_INDIVA = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_IMPNAZ = CAIMPNAZ( this.w_FLVEAC, this.w_VALMAG, this.w_CAOVAL, this.w_CAONAZ, this.w_DATCOM, this.w_VALNAZ, this.w_CODVAL, this.w_CODIVE, this.w_PERIVE, this.w_INDIVE, Nvl(this.w_PERIVA,0), this.w_INDIVA )
    * --- Se specificata una commessa e diversa da quella della riga precedentemente esaminata, 
    *     Rileggo la valuta della commessa e i suoi decimali
    this.w_TCODCOM = this.w_MYSELF.Get("w_MVCODCOM")
    if Not Empty(Nvl(this.w_TCODCOM," ")) And this.w_CODCOM<>Nvl(this.w_TCODCOM," ")
      this.w_CODCOM = this.w_TCODCOM
      * --- Ricerca della valuta della commessa e dei suoi decimali
      * --- Read from CAN_TIER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CNCODVAL"+;
          " from "+i_cTable+" CAN_TIER where ";
              +"CNCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CNCODVAL;
          from (i_cTable) where;
              CNCODCAN = this.w_CODCOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_COCODVAL = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from VALUTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VALUTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "VADECTOT"+;
          " from "+i_cTable+" VALUTE where ";
              +"VACODVAL = "+cp_ToStrODBC(this.w_COCODVAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          VADECTOT;
          from (i_cTable) where;
              VACODVAL = this.w_COCODVAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DECCOM = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Ricalcolo Importo della Commessa:  CAIMPCOM:se vuoto w_MVCODCOM restituisce 0
    this.w_IMPCOM = CAIMPCOM( iif(Empty(Nvl( this.w_TCODCOM," "))," ", this.w_TFLEVAS), this.w_VALNAZ, this.w_COCODVAL, this.w_IMPNAZ, this.w_CAONAZ, this.w_DATCOM, this.w_DECCOM )
    * --- Carica il Temporaneo dei Dati
    if this.w_ACCSPE=1
      this.w_MYSELF.Set("w_MVIMPACC" , this.w_IMPACC)     
    else
      this.w_MYSELF.Set("w_MVIMPSCO" , this.w_IMPSCO)     
    endif
    if this.w_MYSELF.GetType("w_MVVALULT")="N"
      * --- Se nel temporaneo esiste il campo w_MVVALULT lo aggiorno
      this.w_TQTAUM1 = this.w_MYSELF.Get("w_MVQTAUM1")
      this.w_MYSELF.Set("w_MVVALULT" , IIF(this.w_TQTAUM1=0, 0, cp_ROUND(this.w_IMPNAZ/this.w_TQTAUM1, this.w_DECTOU)))     
    endif
    if this.w_PADRE="E" AND this.w_MYSELF.GetType("w_VISNAZ")<>"U"
      this.w_MYSELF.Set("w_VISNAZ" , cp_ROUND(this.w_IMPNAZ, this.w_DECTOP) )     
    endif
    * --- Carica il Temporaneo dei Dati
    this.w_MYSELF.Set("w_MVVALMAG" , this.w_VALMAG)     
    this.w_MYSELF.Set("w_MVIMPNAZ" , this.w_IMPNAZ)     
    this.w_MYSELF.Set("w_MVIMPCOM" , this.w_IMPCOM)     
    * --- Gestione kit
    this.w_TIPRIG = Nvl(this.w_MYSELF.Get("w_MVTIPRIG")," ")
    if this.w_TIPRIG = "A"
      * --- Gestione kit
      *     Dopo aver calcolato gli importi MVIMPACC e MVIMPSCO
      *     degli articoli padfre di kit, li scrive nel cursore w_CURSORE_KIT
      *     in modo che possano essere utilizzati per calcolare i corrispondenti importi da assegnare ai kit
      if this.w_ACCSPE=1
        * --- Spese accessorie
        UPDATE (this.w_CURSORE_KIT) SET MVIMPACC = this.w_IMPACC + this.w_IMPACC_IVA - NORIPACC, ; 
 RESTO_IMPACC = this.w_IMPACC + this.w_IMPACC_IVA - NORIPACC ; 
 WHERE MVRIFKIT = this.w_ROWNUM
      else
        * --- Sconti
        UPDATE (this.w_CURSORE_KIT) SET ; 
 MVIMPSCO =this.w_IMPSCO ,; 
 RESTO_IMPSCO = this.w_IMPSCO ; 
 WHERE MVRIFKIT = this.w_ROWNUM
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione kit
    * --- Il risultato della ripartizione deve essere distribuito sui componenti del kit
    * --- Si esegue una interrogazione per selezionare le informazioni relative ai padri dei kit
    *     CPROWNUM � il CPROWNUM della riga del padre,
    *     MVIMPACC � il risultato della ripartizione delle spese accessorie sulla riga padre (da ripartire sui componenti)
    *     MVIMPSCO � il risultato della ripartizione degli sconti sulla riga padre (da ripartire sui componenti)
    this.w_CURSORE_KIT_1 = SYS( 2015 )
    if this.w_MYSELF.GETTYPE("w_MVFLOMAG") = "N"
      this.w_FILTER_MVFLOMAG = "IIF(t_MVFLOMAG=1,'X',' ' )='X'"
    else
      this.w_FILTER_MVFLOMAG = "t_MVFLOMAG='X'"
    endif
    this.w_GSxx_MDV.Exec_Select(this.w_CURSORE_KIT_1,"CPROWNUM As CPROWNUM, t_MVIMPACC AS MVIMPACC, t_MVIMPSCO AS MVIMPSCO"," (NOT EMPTY(t_MVCODICE)) AND t_MVTIPRIG = 'A' AND " + this.w_FILTER_MVFLOMAG + " And NVL(t_FLSERA,' ')<>'S' AND t_MVVALRIG>0","","","")     
    * --- Si esegue una interrogazione per selezionare le informazioni relative alle informazioni comuni ai componenti di ogni kit
    *     TOTVALMAG � la somma di MVVALMAG per ogni kit
    *     MVRIFKIT � il campo su cui si esegue il raggruppamento
    *     TOVALRIG_GIARIPA � calcolato come TOTVALRIG, ma solo sulle righe aventi MVESCRIP='S', dove l'importo � ripartito manualmente. L'importo gi� ripartito
    *     viene messo nel campo NORIPACC e sar� sottratto nella update di pag 1
    this.w_CURSORE_KIT_2 = SYS( 2015 )
    if this.w_MYSELF.GETTYPE("w_MVESCRIP") <> "U"
      this.w_FIELD_MVESCRIP = "SUM(IIF(t_MVESCRIP<>'S',t_MVVALRIG-t_MVVALRIG,t_MVVALRIG)) AS TOTVALRIG_GIARIPA"
      this.w_FIELD2_MVESCRIP = "SUM(IIF(t_MVESCRIP<>'S',t_MVIMPACC-t_MVIMPACC,t_MVIMPACC) ) AS NORIPACC"
    else
      this.w_FIELD_MVESCRIP = "SUM( t_MVVALRIG ) AS TOTVALRIG_GIARIPA"
      this.w_FIELD2_MVESCRIP = "SUM( t_MVIMPACC ) AS NORIPACC"
    endif
    this.w_GSxx_MDV.Exec_Select(this.w_CURSORE_KIT_2,"SUM(t_MVVALRIG) AS TOTVALRIG, " + this.w_FIELD_MVESCRIP + ", " + this.w_FIELD2_MVESCRIP + ", t_MVRIFKIT AS MVRIFKIT"," (NOT EMPTY(t_MVCODICE)) AND t_MVRIFKIT <> 0 AND " + this.w_FILTER_MVFLOMAG + " AND NVL(t_FLSERA,' ')<>'S' AND t_MVVALRIG>0","t_MVRIFKIT","t_MVRIFKIT","")     
    * --- Mette insieme il risultato creando i campi RESTO_IMPACC e RESTO_IMPSCO
    *     adibiti a contenere il risultato della ripartizione
    *     e RIGARESTO che contiene la posizione del primo componente del kit su cui
    *     assegnare l'eventuale resto delle spese accessorie
    *     e RIGARESTOS che contiene la posizione del primo componente del kit su cui
    *     assegnare l'eventuale resto degli sconti
    this.w_CURSORE_KIT = SYS( 2015 )
    SELECT *,MVIMPACC AS RESTO_IMPACC, MVIMPSCO AS RESTO_IMPSCO, 9999999999 as RIGARESTO , 9999999999 as RIGARESTOS ; 
 FROM (this.w_CURSORE_KIT_1) INNER JOIN (this.w_CURSORE_KIT_2) ON CPROWNUM=MVRIFKIT ; 
 ORDER BY MVRIFKIT INTO CURSOR (this.w_CURSORE_KIT)
    * --- Al campo TOTVALRIG occorre aggiungere gli importi relativi a contributi
    *     RAEE su altre righe (TOTVALRIG1).
    *     Tali contributi avranno MVRIFCAC sulla loro riga 
    *     uguale a CPROWNUM sulla riga del componente del kit
    this.w_CURSORE_KIT_3 = SYS( 2015 )
    this.w_GSxx_MDV.Exec_Select(this.w_CURSORE_KIT_3,"SUM( t_MVVALRIG ) As TOTVALRIG1, t_MVRIFCAC AS MVRIFCAC","t_MVRIFCAC>0","","t_MVRIFCAC","")     
    this.w_CURSORE_KIT_4 = SYS( 2015 )
    this.w_GSxx_MDV.Exec_Select(this.w_CURSORE_KIT_4,"CPROWNUM AS CPROWNUM1, t_MVRIFKIT AS MVRIFKIT1","t_MVRIFKIT>0","","","")     
    SELECT SUM(TOTVALRIG1) AS TOTVALRIG1, MVRIFKIT1 ; 
 FROM (this.w_CURSORE_KIT_3) LEFT OUTER JOIN (this.w_CURSORE_KIT_4) ON MVRIFCAC=CPROWNUM1 ; 
 GROUP BY MVRIFKIT1 ; 
 INTO CURSOR (this.w_CURSORE_KIT_3)
    * --- Si inserisce il risultato su w_CURSORE_KIT
    SELECT * FROM ( this.w_CURSORE_KIT ) LEFT OUTER JOIN ( this.w_CURSORE_KIT_3 ) ; 
 ON CPROWNUM = MVRIFKIT1 ; 
 ORDER BY MVRIFKIT INTO CURSOR (this.w_CURSORE_KIT)
    * --- Si rende scrivibile il cursore w_CURSORE_KIT
    *     Ogni volta che il ciclo sul cursore del documento incontrer� un articolo padre di kit
    *     eseguir� l'aggiornamento dei campi MVIMPACC , MVIMPSCO , RESTO_MVIMPACC , RESTO_MVIMPSCO
    *     Tali campi saranno utilizzati sulle righe dei componenti per calcolare i relativi importi ripartiti
    WRCURSOR(this.w_CURSORE_KIT)
    UPDATE (this.w_CURSORE_KIT) SET RIGARESTO = 0, RIGARESTOS=0
    * --- Si assegna al componente del kit l'importo derivante dai contrributi accessori
    *     non a peso
    UPDATE (this.w_CURSORE_KIT) SET TOTVALRIG = TOTVALRIG + NVL( TOTVALRIG1 , 0 )
    * --- I cursori di appoggio non servono pi�
    * --- Se ci sono contributi accessori legati a componenti di kit, il loro importo, dovendo essere assegnato al padre del kit
    *     deve essere assegnato anche ai totali utilizzati per eseguire la ripartizione
    SELECT ( this.w_CURSORE_KIT )
    this.w_IMPORTO_RAEE_CONTRIBUTI_COMPONENTI = 0
    SUM TOTVALRIG1 TO this.w_IMPORTO_RAEE_CONTRIBUTI_COMPONENTI
    this.w_TOTMERCE = this.w_TOTMERCE + this.w_IMPORTO_RAEE_CONTRIBUTI_COMPONENTI
    this.w_TOTMERCE_SCONTI = this.w_TOTMERCE_SCONTI + this.w_IMPORTO_RAEE_CONTRIBUTI_COMPONENTI
    if USED(this.w_CURSORE_KIT_1)
      SELECT(this.w_CURSORE_KIT_1)
      USE
    endif
    if USED(this.w_CURSORE_KIT_2)
      SELECT(this.w_CURSORE_KIT_2)
      USE
    endif
    if USED(this.w_CURSORE_KIT_3)
      SELECT(this.w_CURSORE_KIT_3)
      USE
    endif
    if USED(this.w_CURSORE_KIT_4)
      SELECT(this.w_CURSORE_KIT_4)
      USE
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if g_COAC = "S"
      * --- Le righe dei contributi accessori non partecipano alla ripartizione, ma gli articoli
      *     a cui i contributi sono stati applicati devono essere aggiornatI assegnando
      *     a MVVALMAG il valore del corrispondente campo dei suoi contributi
      *     accessori.
      * --- Sono gestite solo le righe di contributi non a peso, in quanto per quelli a peso
      *     nno sono disponibili informazioni sufficienti per eseguire l'assegnamento
      this.w_CURSORE_COAC = SYS( 2015 )
      if this.w_MYSELF.GetType("w_ARRIPCON") = "C"
        if this.w_GSxx_MDV.GETTYPE("w_MVFLOMAG") = "N"
          this.w_GSxx_MDV.Exec_Select(this.w_CURSORE_COAC,"t_MVRIFCAC AS MVRIFCAC, SUM( t_MVVALRIG ) AS MVVALRIG"," (NOT EMPTY(t_MVCODICE)) AND IIF(t_MVFLOMAG=1,'X',' ' )='X' And t_FLSERA='S' AND t_MVVALRIG>0 AND t_MVRIFCAC>0 AND t_ARRIPCON='S'","t_MVRIFCAC","t_MVRIFCAC","")     
        else
          this.w_GSxx_MDV.Exec_Select(this.w_CURSORE_COAC,"t_MVRIFCAC AS MVRIFCAC, SUM( t_MVVALRIG ) AS MVVALRIG"," (NOT EMPTY(t_MVCODICE)) AND t_MVFLOMAG='X' And t_FLSERA='S' AND t_MVVALRIG>0 AND t_MVRIFCAC>0 AND t_ARRIPCON='S'","t_MVRIFCAC","t_MVRIFCAC","")     
        endif
      else
        * --- Se il flag ARRIPCON non � disponibile, i contributi accessori non vengono ripartiti
        this.w_GSxx_MDV.Exec_Select(this.w_CURSORE_COAC,"t_MVRIFCAC AS MVRIFCAC, SUM( t_MVVALRIG ) AS MVVALRIG"," 1=0 ","t_MVRIFCAC","t_MVRIFCAC","")     
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_IMPACC = this.w_MYSELF.GET("w_MVIMPACC")
    this.w_IMPSCO = this.w_MYSELF.GET("w_MVIMPSCO")
    this.w_VALRIG = this.w_MYSELF.GET("w_MVVALRIG")
    if this.w_MYSELF.GetType("w_IMP_RAEE") = "N"
      this.w_IMP_RAEE = this.w_MYSELF.GET("w_IMP_RAEE")
    else
      this.w_IMP_RAEE = 0
    endif
    this.w_RIGARESTO = 0
    if this.w_ESCRIP<>"S"
      this.w_RIGARESTO = this.w_MYSELF.ROWINDEX()
    endif
    this.w_RIGARESTOS = this.w_MYSELF.ROWINDEX()
    * --- Cerca il risultato della ripartizione precedentemente effettuato
    SELECT(this.w_CURSORE_KIT)
    GO TOP
    LOCATE FOR CPROWNUM = this.w_RIFKIT
    if FOUND()
      * --- w_IMP_RAEE deve partecipare 
      *     (insieme a MVVALRIG) al peso con cui � eseguita la ripartizione
      if this.w_ESCRIP<>"S" 
        this.w_IMPACC_KIT = cp_ROUND( NVL(MVIMPACC,0) * ( this.w_VALRIG + this.w_IMP_RAEE ) / ( TOTVALRIG - IIF( TOTVALRIG_GIARIPA <> TOTVALRIG , TOTVALRIG_GIARIPA, 0) ) , this.w_DECTOT )
      else
        this.w_IMPACC_KIT = this.w_MYSELF.Get("w_MVIMPACC")
      endif
      this.w_IMPSCO_KIT = cp_ROUND( NVL(MVIMPSCO,0) * ( this.w_VALRIG + this.w_IMP_RAEE ) / TOTVALRIG , this.w_DECTOT )
      this.w_IMPACC = this.w_IMPACC_KIT
      this.w_IMPSCO = this.w_IMPSCO_KIT
      if this.w_ESCRIP<>"S"
        * --- Spese accessorie calcolate solo in caso di ripartizione automatica
        * --- Gli importi assegnati sulle righe vengono sottratti dal totale da ripartire
        REPLACE RESTO_IMPACC WITH RESTO_IMPACC - this.w_IMPACC_KIT
        * --- Si indica la riga su cui assegnare l'eventuale resto
        if RIGARESTO = 0 AND this.w_RIGARESTO<>0
          * --- Viene memorizzato il primo componente elaborato
          REPLACE RIGARESTO WITH this.w_RIGARESTO
        endif
        * --- Si aggiornano gli importi del kit
        this.w_MYSELF.Set("w_MVIMPACC" , this.w_IMPACC)     
      endif
      * --- Sconti
      * --- Gli importi assegnati sulle righe vengono sottratti dal totale da ripartire
      REPLACE RESTO_IMPSCO WITH RESTO_IMPSCO - this.w_IMPSCO_KIT
      * --- Si indica la riga su cui assegnare l'eventuale resto
      if RIGARESTOS = 0 AND this.w_RIGARESTOS<>0
        * --- Viene memorizzato il primo componente elaborato
        REPLACE RIGARESTOS WITH this.w_RIGARESTOS
      endif
      * --- Si aggiornano gli importi del kit
      this.w_MYSELF.Set("w_MVIMPSCO" , this.w_IMPSCO)     
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Assegna eventuali resti dei kit
    SELECT ( this.w_CURSORE_KIT )
    GO TOP
    do while NOT EOF()
      * --- Spese accessorie
      if RESTO_IMPACC <> 0
        * --- La riga a cui bisogna assegnare il resto � stata calcolata a 
        this.w_RIGARESTO = RIGARESTO
        if this.w_MYSELF.GETTYPE("w_MVESCRIP") <> "U"
          this.w_ESCRIP = this.w_MYSELF.Get("w_MVESCRIP")
        endif
        if this.w_RIGARESTO <> 0
          this.w_MYSELF.SetRow(this.w_RIGARESTO)     
          this.w_IMPSCO = this.w_MYSELF.Get("w_MVIMPSCO")
          this.w_IMPACC = this.w_MYSELF.Get("w_MVIMPACC")
          if RESTO_IMPACC <> 0
            this.w_RIMPACC = RESTO_IMPACC
            this.w_IMPACC = this.w_MYSELF.Get("w_MVIMPACC")
            this.w_IMPACC = this.w_IMPACC + this.w_RIMPACC
            this.w_MYSELF.Set("w_MVIMPACC" , this.w_IMPACC)     
          endif
          * --- Esegue i calcoli di riga
          this.w_IMPACC_IVA = 0
          if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
            this.w_RIFKIT = this.w_MYSELF.Get("w_MVRIFKIT")
          endif
          this.Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.w_ERRORESURIPARTIZIONEKIT = .t.
        endif
      endif
      SELECT ( this.w_CURSORE_KIT )
      * --- Sconti
      if RESTO_IMPSCO <> 0
        * --- La riga a cui bisogna assegnare il resto � stata calcolata a 
        this.w_RIGARESTOS = RIGARESTOS
        if this.w_RIGARESTOS <> 0
          this.w_MYSELF.SetRow(this.w_RIGARESTOS)     
          this.w_IMPSCO = this.w_MYSELF.Get("w_MVIMPSCO")
          this.w_IMPACC = this.w_MYSELF.Get("w_MVIMPACC")
          if RESTO_IMPSCO <> 0
            this.w_RIMPSCO = RESTO_IMPSCO
            this.w_IMPSCO = this.w_MYSELF.Get("w_MVIMPSCO")
            this.w_IMPSCO = this.w_IMPSCO + this.w_RIMPSCO
            this.w_MYSELF.Set("w_MVIMPSCO" , this.w_IMPSCO)     
          endif
          * --- Esegue i calcoli di riga
          this.w_IMPACC_IVA = 0
          if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
            this.w_RIFKIT = this.w_MYSELF.Get("w_MVRIFKIT")
          endif
          this.Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.w_ERRORESURIPARTIZIONEKIT = .t.
        endif
      endif
      SELECT ( this.w_CURSORE_KIT )
      SKIP
    enddo
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_MYSELF.GetType("CPROWNUM") = "N"
      this.w_ROWNUM = this.w_MYSELF.Get("CPROWNUM")
    endif
    if this.w_MYSELF.GetType("w_MVRIFCAC") = "N"
      this.w_RIFCAC = this.w_MYSELF.Get("w_MVRIFCAC")
    else
      this.w_RIFCAC = 0
    endif
    this.w_FLSERA = NVL(this.w_MYSELF.Get("w_FLSERA"), " ")
    this.w_VALRIG = this.w_MYSELF.Get("w_MVVALRIG")
    if this.w_MYSELF.GetType("w_ARRIPCON") = "C"
      this.w_ARRIPCON = this.w_MYSELF.Get("w_ARRIPCON")
    else
      this.w_ARRIPCON = " "
    endif
    this.w_IMP_RAEE = 0
    if g_COAC = "S" AND this.w_TDRIPCON = "S" AND this.w_RIFCAC = 0
      if this.w_TIPRIG <> "A"
        * --- Se siamo su una riga di articolo soggetto a contributi accessori occorre aggiungere il valore di MVVALRIG
        *     dei contributi ad esso applicati
        *     (cio� delle righe di contributo che hanno MVRIFCAC = CPROWNUM della riga articolo).
        *     Se siamo su un contributo accessorio occorre evitare che l'importo assegnato all'articolo
        *     venga conteggiato due volte nell'analitica, e quindi la riga di contributo deve essere
        *     esclusa dall'analitica
        * --- Cerca se la riga in questione ha dei contributi accessori associati
        SELECT(this.w_CURSORE_COAC)
        GO TOP
        LOCATE FOR MVRIFCAC = this.w_ROWNUM
        if FOUND()
          * --- w_IMP_RAEE sar� sommato a w_VALRIG per eseguire le proporzioni
          this.w_IMP_RAEE = NVL(MVVALRIG,0)
        endif
      else
        * --- Sulla riga padre del kit occorre assegnare l'importo del contributo accessorio
        *     derivante dai comntributi assegnati ai suoi componenti
        SELECT(this.w_CURSORE_KIT)
        GO TOP
        LOCATE FOR CPROWNUM = this.w_ROWNUM
        if FOUND()
          * --- w_IMP_RAEE sar� sommato a w_VALRIG per eseguire le proporzioni
          this.w_IMP_RAEE = NVL(TOTVALRIG1,0)
        endif
      endif
    endif
    this.w_PERIVA = this.w_MYSELF.Get("w_PERIVA")
    * --- Riassegna Spese Accessorie senza codice Iva
    if (this.w_SPEACC<>0 Or this.w_SPEACC_IVA<>0) AND this.w_TOTMERCE<>0 AND this.w_FLOMAG="X" AND this.w_FLSERA<>"S" And this.w_TIPRIG<>"D" and this.w_VALRIG>0 AND this.w_RIFKIT=0
      * --- x determinare la prima riga valida non applico l'arrotondamento per evitare che
      *     importi sotto 0,005 non rendano la riga utilizzabile per il resto
      if this.w_ESCRIP<>"S"
        this.w_PREC = IIF(this.w_PREC=0 AND ( (( ( this.w_VALRIG + this.w_IMP_RAEE ) * this.w_SPEACC) / this.w_TOTMERCE)<>0 Or (( ( this.w_VALRIG + this.w_IMP_RAEE ) * this.w_SPEACC_IVA) / this.w_TOTMERCE)<>0 ) , this.w_MYSELF.RowIndex() , this.w_PREC)
        this.w_IMPACC = cp_ROUND(( ( this.w_VALRIG + this.w_IMP_RAEE ) * this.w_SPEACC) / this.w_TOTMERCE, this.w_DECTOT)
      endif
      this.w_IMPACC_IVA = cp_ROUND(( ( this.w_VALRIG + this.w_IMP_RAEE ) * this.w_SPEACC_IVA) / this.w_TOTMERCE, this.w_DECTOT)
      * --- Aggiorna Totalizzatore e Riga su cui ripartire l'eventuale Resto
      this.w_TIMPACC = this.w_TIMPACC + this.w_IMPACC + this.w_IMPACC_IVA
    endif
    if this.w_ESCRIP="S" AND this.w_FLOMAG="X" AND this.w_FLSERA<>"S" And this.w_TIPRIG<>"D" and this.w_VALRIG>0 AND this.w_RIFKIT=0
      this.w_IMPACC = this.w_MYSELF.Get("w_MVIMPACC")
    endif
    * --- Riassegna Sconti
    if Lower(this.oParentObject.Class)="tgsps_bgd"
      this.w_SCOVEN = Nvl ( this.w_MYSELF.Get("w_MVSCOVEN") , 0 )
    else
      * --- Se non da movimenti P.O.S. sconto ventilato a zero..
      this.w_SCOVEN = 0
    endif
    if Isalt()
      this.w_PRESTA = this.w_MYSELF.Get("w_PRESTA")
    endif
    if ( this.w_SCONTI<>0 Or this.w_SCOVEN<>0 ) AND this.w_TOTMERCE_SCONTI<>0 AND this.w_FLOMAG="X" AND this.w_FLSERA<>"S" And this.w_TIPRIG<>"D" and this.w_VALRIG>0 AND this.w_RIFKIT=0 AND ! this.w_PRESTA $ "S-A"
      * --- Nel POS assegno allo sconto ripartito su riga quello che ho gi� calcolato sulla vendita 
      *     al dettaglio, poich� gli sconti derivanti dalle promozioni devono essere ripartiti solo sulle 
      *     righe che attivano la promozione
      if this.oParentObject.Class="Tgsps_bgd"
        this.w_PRES = IIF(this.w_PRES=0 AND this.w_SCOVEN <>0 , this.w_MYSELF.RowIndex() , this.w_PRES)
        this.w_IMPSCO = this.w_SCOVEN
      else
        this.w_PRES = IIF(this.w_PRES=0 AND (( ( this.w_VALRIG + this.w_IMP_RAEE ) * this.w_SCONTI) / this.w_TOTMERCE_SCONTI) <>0 , this.w_MYSELF.RowIndex() , this.w_PRES)
        this.w_IMPSCO = cp_ROUND(( ( this.w_VALRIG + this.w_IMP_RAEE ) * this.w_SCONTI) / this.w_TOTMERCE_SCONTI, this.w_DECTOT)
      endif
      * --- Riga su cui ripartire l'eventuale Resto
      this.w_TIMPSCO = this.w_TIMPSCO + this.w_IMPSCO
      this.w_PRES = IIF(this.w_PRES=0 AND this.w_IMPSCO<>0, this.w_MYSELF.RowIndex() , this.w_PRES)
    endif
    if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
      this.w_FLSCOM = IIF(Vartype(g_FLSCOM)<>"U",g_FLSCOM," ")
    else
      this.w_FLSCOM = IIF(Vartype(g_FLSCOM)<>"U",IIF(g_FLSCOM="S" AND this.w_DATCOM<g_DTSCOM AND !empty(nvl(g_DTSCOM,cp_CharToDate("  /  /    ")))," ",g_FLSCOM)," ")
    endif
    if Vartype(this.pParent)="O" and this.w_FLSCOM="S" AND ((this.pParent.w_MVSCOPAG<>0 OR this.pParent.w_MVSCOCL1<>0 OR this.pParent.w_MVSCOCL2<>0) AND this.w_FLOMAG$"E-I-S" )
      this.w_IMPSCO = CalSco(this.w_VALRIG , this.pParent.w_MVSCOCL1 , this.pParent.w_MVSCOCL2 , this.pParent.w_MVSCOPAG, this.w_DECTOT)
    endif
    this.w_VALMAG = CAVALMAG( this.w_FLSCOR, this.w_VALRIG, this.w_IMPSCO, this.w_IMPACC, NVL( this.w_PERIVA, 0), this.w_DECTOT, this.w_CODIVE, this.w_PERIVE, this.w_IMPACC_IVA , this.w_TDRIPCON, this.w_ARRIPCON, this.w_IMP_RAEE)
    if Vartype(this.pParent)="O" and this.w_FLSCOM="S" AND ((this.pParent.w_MVSCOPAG<>0 OR this.pParent.w_MVSCOCL1<>0 OR this.pParent.w_MVSCOCL2<>0) AND this.w_FLOMAG$"E-I-S") 
      this.w_IMPSCO = 0
    endif
    * --- Codice Iva di Riga
    this.w_CODIVA = this.w_MYSELF.Get("w_MVCODIVA")
    if this.w_OLDCODIVA <> this.w_CODIVA And Not Empty(this.w_CODIVA)
      this.w_OLDCODIVA = this.w_CODIVA
      * --- Leggo la percentuale di Indetraibilit� dell'Iva di Riga
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_CODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_INDIVA = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Leggo la percentuale di Indetraibilit� dell'Iva di Riga
    this.w_IMPNAZ = CAIMPNAZ( this.w_FLVEAC, this.w_VALMAG, this.w_CAOVAL, this.w_CAONAZ, this.w_DATCOM, this.w_VALNAZ, this.w_CODVAL, this.w_CODIVE, this.w_PERIVE, this.w_INDIVE, Nvl( this.w_PERIVA ,0), this.w_INDIVA )
    if g_COMM="S"
      * --- Se specificata una commessa e diversa da quella della riga precedentemente esaminata, 
      *     Rileggo la valuta della commessa e i suoi decimali
      this.w_TCODCOM = this.w_MYSELF.Get("w_MVCODCOM")
      this.w_TFLORCO = this.w_MYSELF.Get("w_MVFLORCO")
      if Not Empty(Nvl(this.w_TCODCOM," ")) And this.w_CODCOM<>Nvl(this.w_TCODCOM," ")
        this.w_CODCOM = this.w_TCODCOM
        * --- Ricerca della valuta della commessa e dei suoi decimali
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNCODVAL"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.w_CODCOM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNCODVAL;
            from (i_cTable) where;
                CNCODCAN = this.w_CODCOM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_COCODVAL = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_COCODVAL<>g_PERVAL
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VADECTOT"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_COCODVAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VADECTOT;
              from (i_cTable) where;
                  VACODVAL = this.w_COCODVAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DECCOM = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          this.w_DECCOM = g_PERPVL
        endif
      endif
      if this.w_MYSELF.GetType("w_MVFLEVAS")<>"U"
        this.w_TFLEVAS = this.w_MYSELF.Get("w_MVFLEVAS")
        * --- Nel caso in cui la gestione usi MVFLEVAS come check � di tipo numerico sul transitorio
        if VarType( this.w_TFLEVAS )="N"
          this.w_TFLEVAS = IIF( this.w_TFLEVAS=1 , "S" ," " )
        endif
      else
        this.w_TFLEVAS = " "
      endif
      * --- Ricalcolo Importo della Commessa:  CAIMPCOM:se vuoto w_MVCODCOM restituisce 0
      this.w_IMPCOM = CAIMPCOM( IIF(Empty(this.w_TCODCOM),"S", iif(this.w_TFLEVAS="S" And Not Empty( this.w_TFLORCO ),"S","N") ), this.w_VALNAZ, this.w_COCODVAL, this.w_IMPNAZ, this.w_CAONAZ, this.w_DATCOM, this.w_DECCOM )
    else
      this.w_IMPCOM = 0
    endif
    * --- Flag Notifica Riga Variata solo da gestione
    this.w_TQTAUM1 = this.w_MYSELF.Get("w_MVQTAUM1")
    this.w_VALULT = IIF( this.w_TQTAUM1 =0, 0, cp_ROUND(this.w_IMPNAZ/ this.w_TQTAUM1, this.w_DECTOU))
    if this.w_MYSELF.GetType("w_MVVALULT")="N"
      * --- Se nel temporaneo esiste il campo w_MVVALULT lo aggiorno
      this.w_MYSELF.Set("w_MVVALULT" , this.w_VALULT)     
    endif
    if this.w_PADRE="E" AND this.w_MYSELF.GetType("w_VISNAZ")<>"U"
      this.w_MYSELF.Set("w_VISNAZ" , cp_ROUND(this.w_IMPNAZ, this.w_DECTOP) )     
    endif
    * --- Carica il Temporaneo dei Dati
    if this.w_ESCRIP<>"S"
      this.w_MYSELF.Set("w_MVIMPACC" , this.w_IMPACC + this.w_IMPACC_IVA)     
    endif
    this.w_MYSELF.Set("w_MVIMPSCO" , this.w_IMPSCO)     
    this.w_MYSELF.Set("w_MVVALMAG" , this.w_VALMAG)     
    this.w_MYSELF.Set("w_MVIMPCOM" , this.w_IMPCOM)     
    this.w_MYSELF.Set("w_MVIMPNAZ" , this.w_IMPNAZ)     
    if this.w_MYSELF.GetType("w_MVIMPAC2")<>"U"
      this.w_MYSELF.Set("w_MVIMPAC2" , this.w_IMPACC_IVA)     
    endif
    if this.w_MYSELF.GetType("w_IMP_RAEE") = "N"
      this.w_MYSELF.Set("w_IMP_RAEE" , this.w_IMP_RAEE)     
    endif
  endproc


  proc Init(oParentObject,pPadre,pCursor,pFlveac,pFlscor,pSpeacc,pSconti,pTotMerce,pCodive,pCaoval,pDatcom,pCaonaz,pValnaz,pCodval,pSpeaccIva,pParent,pContributi)
    this.pPadre=pPadre
    this.pCursor=pCursor
    this.pFlveac=pFlveac
    this.pFlscor=pFlscor
    this.pSpeacc=pSpeacc
    this.pSconti=pSconti
    this.pTotMerce=pTotMerce
    this.pCodive=pCodive
    this.pCaoval=pCaoval
    this.pDatcom=pDatcom
    this.pCaonaz=pCaonaz
    this.pValnaz=pValnaz
    this.pCodval=pCodval
    this.pSpeaccIva=pSpeaccIva
    this.pParent=pParent
    this.pContributi=pContributi
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='VOCIIVA'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- gsar_brs
  * Il batch gestisce sia il transitorio dei documenti
  * che un transitorio costruito da batch per la generazione
  * documenti (Es. GSVE_BFD)
  * Definiti quindi metodi propri di questo batch che in base
  * al parametro lanciano metodi sul transitorio della gestione
  * o su GENEAPP (cursore creato dalle generazioni)
  
  Procedure Exec_Select(cTmp,cFields,cWhere,cOrder,cGroupBy,cHaving)
  LOCAL cName_1,cName_2,cCmdSql,cOlderr,cMsgErr
   If This.w_PADRE='B'
  	* Assegnamenti..
  	cOlderr=ON('error')
  	cMsgErr=''
  	On error cMsgErr=Message(1)+': '+MESSAGE()
  	
  	cTmp=   IIF( EMPTY(cTmp)   , '__Tmp__', cTmp )
  	cFields=IIF( EMPTY(cFields), '*'      , cFields )
  	cWhere= IIF( EMPTY(cWhere) , '1=1'    , cWhere )	
  	cOrder= IIF( EMPTY(cOrder) , '1'      , cOrder )	
  	
  	cOrder=IIF( EMPTY(cGroupBy),'',' GROUP BY '+(cGroupBy)+IIF( EMPTY(cHaving),'',' HAVING '+(cHaving) ) )+' Order By '+cOrder
  	
  	cName_1=This.w_Cursor
  	
  	cCmdSql='Select '+cFields+' From '+cName_1			
  	cCmdSql=cCmdSql+' Where '
  	
  	cCmdSql=cCmdSql+cWhere+' &cOrder Into Cursor '+cTmp+' Nofilter '
  	* Eseguo la frase..
  	&cCmdSql
  
  	* Ripristino la vecchia gestione errori
  	ON ERROR &cOldErr
  
  	* Se Qualcosa va storto lo segnalo ed esco...
  	  IF NOT EMPTY(cMsgErr)
        ah_ErrorMsg("%1",'stop','', cMsgErr)
        * memorizzo nella clipboard la frase generata in caso di errore
        _ClipText=cCmdSql
      ELSE
  		  * mi posiziono sul temporaneo al primo record..
  		  SELECT (cTmp)
  		  GO Top
      ENDIF  
   Else
    This.w_GSxx_MDV.Exec_Select(cTmp,cFields,cWhere,cOrder,cGroupBy,cHaving)
   Endif
  Endproc
  
  
  Procedure FirstRow()
   LOCAL cOldArea
   If This.w_PADRE='B'	
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
    Select GENEAPP
    Go Top
  	* ripristino la vecchia area
  	SELECT(coldArea)  
   else
    this.w_GSxx_MDV.FirstRow()
   endif
  EndProc
  
  Function Eof_Trs() 
   if This.w_PADRE='B'
    RETURN (EOF(This.w_Cursor))
   else
  	 RETURN (this.w_GSxx_MDV.Eof_Trs())
   endif
  ENDFUNC
  
  PROCEDURE SetRow(id_row) 
  LOCAL cOldArea
   if This.w_PADRE='B'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se paramento valorizzato mi posiziono sulla riga passata 
  	* altrimenti sulla riga attuale...
  	IF TYPE( 'id_row' )='N'
  		SELECT( This.w_Cursor )
  		goto (id_row) 	
  	EndIf
  		
  	* ripristino la vecchia area
  	SELECT(coldArea)	  
   else
  	 this.w_GSxx_MDV.SetRow( id_Row )
   endif
  
  Endproc
  
  FUNCTION GET(Item)
  LOCAL cOldArea,cFldName
   if This.w_PADRE='B'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se le prima due lettere sono w_ le sostituisco con t_
  	* altrimenti lascio quanto trovo...
  	IF lower(LEFT(Item,2))='w_' 
  		cFldName='t_'+SUBSTR(Item,3,LEN(Item))
  	ELSE
  		cFldName=Item
  	ENDIF
  
  	SELECT( This.w_Cursor )
  	* Leggo il campo...
  	Result=eval(cFldName)
  	
  	* ripristino la vecchia area
  	SELECT(coldArea)	
  	
  	* Restituisco il valore recuperato..
  	Return(Result)
     
   else
    Return( This.w_GSxx_MDV.Get(Item) )
   Endif
  EndFunc
  
  Procedure NextRow()
  LOCAL cOldArea
   If This.w_PADRE='B'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
   
    Select (This.w_Cursor)
    Skip
    
  	* ripristino la vecchia area
  	SELECT(coldArea)  
    
   Else
    This.w_GSxx_MDV.NextRow()
   Endif
  	
  EndProc
  
  Function Search(Criterio,StartFrom)
  LOCAL cOldArea,cName_1, cCursName,nResult,cCond
   If This.w_PADRE='B'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	cName_1=This.w_Cursor
  	cCursName=SYS(2015)	
  	* Se ricerca a partire da un certo record aggiungo la condizione..
  	IF TYPE('StartFrom')='N' AND StartFrom<>0
  	cCond=' RECNO(cName_1)>'+ALLTRIM(STR(StartFrom))
  	ELSE
  	cCond=' 1=1 '
  	Endif
  		SELECT MIN(RECNO()) As Riga FROM (cName_1) WHERE  &cCond AND  		&Criterio INTO CURSOR &cCursName
  	* leggo il risultato
  	SELECT(cCursName)
  	GO Top
  	nResult=NVL(&cCursName..Riga,-1) 
  	nResult=IIF(nResult=0,-1, nResult)
  	* rimuovo il cursore di appoggio
      if used(cCursName)
        select (cCursName)
        use
      ENDIF    
  	* ripristino la vecchia area
  	SELECT(coldArea)
  	* restituisco il risultato
  	RETURN nResult
    
   Else
    Return(This.w_GSxx_MDV.Search(Criterio,StartFrom))
   Endif
  
  EndFunc
  
  
  Procedure SET(cItem,vValue,bNoUpd)
  LOCAL cOldArea,cFldName,bResult
   If This.w_PADRE='B'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se le prima due lettere sono w_ le sostituisco con t_
  	* altrimenti lascio quanto trovo...
  	IF lower(LEFT(cItem,2))='w_' 
  		cFldName='t_'+SUBSTR(cItem,3,LEN(cItem))
  	ELSE
  		cFldName=cItem
  	ENDIF
  
  	* Aggiorno il transitorio normale...
  	SELECT( This.w_Cursor )
  		
  	* Se cambio allora svolgo la Replace..
  	IF &cFldName<>vValue  or (isnull(&cFldName)and Not isnull(vValue))
  		Replace &cFldName WITH vValue
  	endif
  			
  	* ripristino la vecchia area
  	SELECT(coldArea) 
   Else
    This.w_GSxx_MDV.Set(cItem,vValue,bNoUpd)
   Endif
  EndProc
  
  Function FullRow()
   If This.w_PADRE='B'
    Select ( this.w_CURSOR )
    return ( t_MVTIPRIG<>' ' AND t_MVTIPRIG<>'D' AND NOT EMPTY(t_MVCODICE) )
    else
     return ( This.w_GSxx_MDV.Fullrow() )
   Endif
  Endfunc
  
  Function RowIndex()
   If This.w_PADRE='B'
    return ( RecNo( this.w_CURSOR ))
    else
     return ( This.w_GSxx_MDV.RowIndex() )
   Endif 
  EndFunc
  
  FUNCTION GetType(cFieldsName)
  	LOCAL cOldArea,cFldName,cResult
   If This.w_PADRE='B'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se le prima due lettere sono w_ le sostituisco con t_
  	* altrimenti lascio quanto trovo...
  	IF lower(LEFT(cFieldsName,2))='w_' 
  		cFldName='t_'+SUBSTR(cFieldsName,3,LEN(cFieldsName))
  	ELSE
  		cFldName=cFieldsName
  	ENDIF	
  	
    SELECT( this.w_CURSOR )
  	* Leggo il tipo del campo...
  	cResult=Type(cFldName)
  				
  	* ripristino la vecchia area
  	SELECT(coldArea)	
    Return (cResult)
    Else
     return ( This.w_GSxx_MDV.GetType(cFieldsName) )
   Endif   
  
  Endfunc
  
  Procedure Done()
   *Azzerro la variabile di comodo, se non lo facessi rimarebbe la classe
   *del batch appesa in memoria, impedendo tra l'altro la compilazione
   *all'interno di ad hoc
   this.w_MYSELF=.Null.
   DoDefault()
  EndpRoc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPadre,pCursor,pFlveac,pFlscor,pSpeacc,pSconti,pTotMerce,pCodive,pCaoval,pDatcom,pCaonaz,pValnaz,pCodval,pSpeaccIva,pParent,pContributi"
endproc
