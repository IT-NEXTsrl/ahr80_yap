* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bdg                                                        *
*              Manutenzione attributi duplicati                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-03-18                                                      *
* Last revis.: 2013-12-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bdg",oParentObject,m.pOPER)
return(i_retval)

define class tgsma_bdg as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_RECSEL = 0
  w_ARCODART = space(20)
  w_NEWGUID = space(14)
  w_CHK_ATTR = space(10)
  * --- WorkFile variables
  ASS_ATTR_idx=0
  ART_ICOL_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Manutenzione attributi duplicati su Articoli/Servizi (da GSMA_BDG)
    * --- Gli attributi risultano duplicati perch� il valore del campo GESTGUID non � univoco
    do case
      case this.pOPER="ASSEGNA"
        Select (this.oParentObject.w_ZARTICO.cCursor)
        sum(xchk) to this.w_RECSEL
        if this.w_RECSEL=0
          ah_errormsg("Nessun articolo/servizio selezionato.")
        else
          Select (this.oParentObject.w_ZATTRIB.cCursor)
          sum(xchk) to this.w_RECSEL
          if this.w_RECSEL=0
            if !ah_yesno("Nessun attributo selezionato, si desidera eliminare tutti gli attributi?")
              i_retcode = 'stop'
              return
            endif
          else
            * --- Controllo validit� attributi selezionati
            Select ASCODATT AS GRUPPO, ASCODFAM AS FAMIGLIA, ASVALATT AS ATTRIBUTO ; 
 FROM (this.oParentObject.w_ZATTRIB.cCursor) where xchk=1 order by GRUPPO, FAMIGLIA into cursor chk_attrib
            this.w_CHK_ATTR = CHK_ELE_ATTR("C", "chk_attrib")
            if not empty(this.w_CHK_ATTR)
              ah_errormsg(this.w_CHK_ATTR)
              i_retcode = 'stop'
              return
            endif
          endif
          Select (this.oParentObject.w_ZARTICO.cCursor)
          go top
          scan for xchk=1
          this.w_ARCODART = ARCODART
          this.w_NEWGUID = SUBSTR(DTOC(DATE(),1),4)+SUBSTR(SYS(2015),2)
          * --- Try
          local bErr_039502D0
          bErr_039502D0=bTrsErr
          this.Try_039502D0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_errormsg("Errore aggiornamento codice %1",,, alltrim(this.w_ARCODART))
          endif
          bTrsErr=bTrsErr or bErr_039502D0
          * --- End
          endscan
          this.w_RECSEL = 0
          * --- Select from ART_ICOL
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(*) AS NUMREC  from "+i_cTable+" ART_ICOL ";
                +" where GESTGUID="+cp_ToStrODBC(this.oParentObject.w_GESTGUID)+"";
                 ,"_Curs_ART_ICOL")
          else
            select COUNT(*) AS NUMREC from (i_cTable);
             where GESTGUID=this.oParentObject.w_GESTGUID;
              into cursor _Curs_ART_ICOL
          endif
          if used('_Curs_ART_ICOL')
            select _Curs_ART_ICOL
            locate for 1=1
            do while not(eof())
            this.w_RECSEL = NVL(_Curs_ART_ICOL.NUMREC,0)
              select _Curs_ART_ICOL
              continue
            enddo
            use
          endif
          if this.w_RECSEL=0
            * --- GESTGUID non pi� utilizzato
            * --- Delete from ASS_ATTR
            i_nConn=i_TableProp[this.ASS_ATTR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATTR_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"GUID = "+cp_ToStrODBC(this.oParentObject.w_GESTGUID);
                     )
            else
              delete from (i_cTable) where;
                    GUID = this.oParentObject.w_GESTGUID;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
          if isahe()
            ah_errormsg("Aggiornamento completato con successo. L'elenco del dettaglio attributi automatici potrebbe non essere stato aggiornato correttamente, se necessario operare mediante gestione aggiornamento degli attributi commerciali.")
          else
            ah_errormsg("Aggiornamento completato con successo.")
          endif
          this.oParentObject.notifyevent("Interroga")
        endif
    endcase
  endproc
  proc Try_039502D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Write into ART_ICOL
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"GESTGUID ="+cp_NullLink(cp_ToStrODBC(this.w_NEWGUID),'ART_ICOL','GESTGUID');
          +i_ccchkf ;
      +" where ";
          +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
          +" and GESTGUID = "+cp_ToStrODBC(this.oParentObject.w_GESTGUID);
             )
    else
      update (i_cTable) set;
          GESTGUID = this.w_NEWGUID;
          &i_ccchkf. ;
       where;
          ARCODART = this.w_ARCODART;
          and GESTGUID = this.oParentObject.w_GESTGUID;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if i_rows>0 and this.w_RECSEL>0
      Select (this.oParentObject.w_ZATTRIB.cCursor)
      scan for xchk=1
      scatter memvar
      * --- Insert into ASS_ATTR
      i_nConn=i_TableProp[this.ASS_ATTR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATTR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ASS_ATTR_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"GUID"+",CPROWNUM"+",CPROWORD"+",ASMODFAM"+",ASCODATT"+",ASCODFAM"+",ASTIPNUM"+",ASTIPDAT"+",ASTIPCAR"+",ASVALATT"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_NEWGUID),'ASS_ATTR','GUID');
        +","+cp_NullLink(cp_ToStrODBC(m.CPROWNUM),'ASS_ATTR','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(m.CPROWORD),'ASS_ATTR','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(m.ASMODFAM),'ASS_ATTR','ASMODFAM');
        +","+cp_NullLink(cp_ToStrODBC(m.ASCODATT),'ASS_ATTR','ASCODATT');
        +","+cp_NullLink(cp_ToStrODBC(m.ASCODFAM),'ASS_ATTR','ASCODFAM');
        +","+cp_NullLink(cp_ToStrODBC(m.ASTIPNUM),'ASS_ATTR','ASTIPNUM');
        +","+cp_NullLink(cp_ToStrODBC(m.ASTIPDAT),'ASS_ATTR','ASTIPDAT');
        +","+cp_NullLink(cp_ToStrODBC(m.ASTIPCAR),'ASS_ATTR','ASTIPCAR');
        +","+cp_NullLink(cp_ToStrODBC(m.ASVALATT),'ASS_ATTR','ASVALATT');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'GUID',this.w_NEWGUID,'CPROWNUM',m.CPROWNUM,'CPROWORD',m.CPROWORD,'ASMODFAM',m.ASMODFAM,'ASCODATT',m.ASCODATT,'ASCODFAM',m.ASCODFAM,'ASTIPNUM',m.ASTIPNUM,'ASTIPDAT',m.ASTIPDAT,'ASTIPCAR',m.ASTIPCAR,'ASVALATT',m.ASVALATT)
        insert into (i_cTable) (GUID,CPROWNUM,CPROWORD,ASMODFAM,ASCODATT,ASCODFAM,ASTIPNUM,ASTIPDAT,ASTIPCAR,ASVALATT &i_ccchkf. );
           values (;
             this.w_NEWGUID;
             ,m.CPROWNUM;
             ,m.CPROWORD;
             ,m.ASMODFAM;
             ,m.ASCODATT;
             ,m.ASCODFAM;
             ,m.ASTIPNUM;
             ,m.ASTIPDAT;
             ,m.ASTIPCAR;
             ,m.ASVALATT;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      endscan
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ASS_ATTR'
    this.cWorkTables[2]='ART_ICOL'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_ART_ICOL')
      use in _Curs_ART_ICOL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
