* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_sl2                                                        *
*              Aggiornamento progressivi L.G.                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_4]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2007-07-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_sl2",oParentObject))

* --- Class definition
define class tgsma_sl2 as StdForm
  Top    = 65
  Left   = 103

  * --- Standard Properties
  Width  = 388
  Height = 263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-13"
  HelpContextID=208305001
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsma_sl2"
  cComment = "Aggiornamento progressivi L.G."
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_lg_nrig = 0
  w_lg_data = ctod('  /  /  ')
  w_CONFERMA = space(1)
  w_PAGINE = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_sl2Pag1","gsma_sl2",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCONFERMA_1_9
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_lg_nrig=0
      .w_lg_data=ctod("  /  /  ")
      .w_CONFERMA=space(1)
      .w_PAGINE=0
      .w_lg_nrig=oParentObject.w_lg_nrig
      .w_lg_data=oParentObject.w_lg_data
      .w_CONFERMA=oParentObject.w_CONFERMA
      .w_PAGINE=oParentObject.w_PAGINE
          .DoRTCalc(1,2,.f.)
        .w_CONFERMA = 'N'
    endwith
    this.DoRTCalc(4,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_lg_nrig=.w_lg_nrig
      .oParentObject.w_lg_data=.w_lg_data
      .oParentObject.w_CONFERMA=.w_CONFERMA
      .oParentObject.w_PAGINE=.w_PAGINE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.olg_nrig_1_1.value==this.w_lg_nrig)
      this.oPgFrm.Page1.oPag.olg_nrig_1_1.value=this.w_lg_nrig
    endif
    if not(this.oPgFrm.Page1.oPag.olg_data_1_2.value==this.w_lg_data)
      this.oPgFrm.Page1.oPag.olg_data_1_2.value=this.w_lg_data
    endif
    if not(this.oPgFrm.Page1.oPag.oCONFERMA_1_9.value==this.w_CONFERMA)
      this.oPgFrm.Page1.oPag.oCONFERMA_1_9.value=this.w_CONFERMA
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGINE_1_13.value==this.w_PAGINE)
      this.oPgFrm.Page1.oPag.oPAGINE_1_13.value=this.w_PAGINE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CONFERMA)) or not(.w_CONFERMA $ 'SN'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONFERMA_1_9.SetFocus()
            i_bnoObbl = !empty(.w_CONFERMA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare S o N per confermare o meno l'elaborazione")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsma_sl2Pag1 as StdContainer
  Width  = 384
  height = 263
  stdWidth  = 384
  stdheight = 263
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object olg_nrig_1_1 as StdField with uid="CWLAXLXPRH",rtseq=1,rtrep=.f.,;
    cFormVar = "w_lg_nrig", cQueryName = "lg_nrig",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 69853878,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=226, Top=152, cSayPict='"999999999"', cGetPict='"999999999"'

  add object olg_data_1_2 as StdField with uid="FVJMLMYHMO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_lg_data", cQueryName = "lg_data",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 235922102,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=226, Top=177

  add object oCONFERMA_1_9 as StdField with uid="SRYPYATMNV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CONFERMA", cQueryName = "CONFERMA",;
    bObbl = .t. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    sErrorMsg = "Impostare S o N per confermare o meno l'elaborazione",;
    ToolTipText = "S per confermare N per annullare",;
    HelpContextID = 97470361,;
   bGlobalFont=.t.,;
    Height=21, Width=16, Left=226, Top=214, cSayPict='"!"', cGetPict='"!"', InputMask=replicate('X',1)

  func oCONFERMA_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_CONFERMA $ 'SN')
    endwith
    return bRes
  endfunc


  add object oBtn_1_11 as StdButton with uid="TAGRZVSCHZ",left=328, top=214, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 208276250;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPAGINE_1_13 as StdField with uid="ZSUVUNFALC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PAGINE", cQueryName = "PAGINE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37537034,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=226, Top=128, cSayPict='"9999999"', cGetPict='"9999999"'

  add object oStr_1_3 as StdString with uid="UYAIKOFHDN",Visible=.t., Left=39, Top=65,;
    Alignment=0, Width=330, Height=15,;
    Caption="L'aggiornamento dei progressivi deve essere eseguito"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="WOBAMHZUFC",Visible=.t., Left=39, Top=85,;
    Alignment=0, Width=330, Height=15,;
    Caption="dopo la stampa definitiva su modulo bollato"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="BXLIGWZKGS",Visible=.t., Left=39, Top=41,;
    Alignment=0, Width=299, Height=15,;
    Caption="Attenzione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="KJMZEUVEOH",Visible=.t., Left=39, Top=105,;
    Alignment=0, Width=330, Height=15,;
    Caption="in quanto ne inibisce la ristampa."  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="NFINLVJVMD",Visible=.t., Left=40, Top=177,;
    Alignment=1, Width=183, Height=15,;
    Caption="Ultima data stampata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="YCMVWASNIX",Visible=.t., Left=40, Top=152,;
    Alignment=1, Width=183, Height=15,;
    Caption="Ultima riga stampata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="FPTWPEHDXB",Visible=.t., Left=39, Top=214,;
    Alignment=1, Width=183, Height=15,;
    Caption="Confermi aggiornamento (S/N)?:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="FWGUERRDWJ",Visible=.t., Left=4, Top=10,;
    Alignment=2, Width=374, Height=15,;
    Caption="LIBRO GIORNALE"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_14 as StdString with uid="IQVAJCMJJE",Visible=.t., Left=32, Top=128,;
    Alignment=1, Width=192, Height=15,;
    Caption="Ultima pagina stampata:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_sl2','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
