* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bd3                                                        *
*              Valorizza castelletto banche                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_60]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-09                                                      *
* Last revis.: 2009-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bd3",oParentObject)
return(i_retval)

define class tgste_bd3 as StdBatch
  * --- Local variables
  w_CONT = .f.
  w_PADRE = .NULL.
  w_TEST = .f.
  * --- WorkFile variables
  VALUTE_idx=0
  CAU_CONT_idx=0
  COC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene lanciato dalla Compilazione distinte quando cambia la causale.
    this.w_PADRE = This.oParentObject
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT,VASIMVAL,VACAOVAL,VADESVAL,VADTOBSO"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_CODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT,VASIMVAL,VACAOVAL,VADESVAL,VADTOBSO;
        from (i_cTable) where;
            VACODVAL = this.oParentObject.w_CODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      this.oParentObject.w_SIMVAL = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      this.oParentObject.w_TESVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
      this.oParentObject.w_DESAPP = NVL(cp_ToDate(_read_.VADESVAL),cp_NullValue(_read_.VADESVAL))
      this.oParentObject.w_DTOBSO = NVL(cp_ToDate(_read_.VADTOBSO),cp_NullValue(_read_.VADTOBSO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CAU_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCDESCRI,CCTIPREG"+;
        " from "+i_cTable+" CAU_CONT where ";
            +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODCAU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCDESCRI,CCTIPREG;
        from (i_cTable) where;
            CCCODICE = this.oParentObject.w_CODCAU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_DESCAU = NVL(cp_ToDate(_read_.CCDESCRI),cp_NullValue(_read_.CCDESCRI))
      this.oParentObject.w_TIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.oParentObject.w_CAOVAL = IIF(EMPTY(this.oParentObject.w_CODVAL) OR EMPTY(this.oParentObject.w_DATVAL), 0, GETCAM(this.oParentObject.w_CODVAL, this.oParentObject.w_DATVAL, 7))
    * --- Il dettaglio non � salvato sul database, quindi posso svolgere una Zap
    *     (svuoto tutto il contenuto del transitorio)
     
 SELECT ( this.w_PADRE.cTrsName ) 
 ZAP
    this.w_TEST = .T.
    * --- Select from gste_bd3
    do vq_exec with 'gste_bd3',this,'_Curs_gste_bd3','',.f.,.t.
    if used('_Curs_gste_bd3')
      select _Curs_gste_bd3
      locate for 1=1
      do while not(eof())
      this.w_PADRE.InitRow()     
      if this.w_TEST
        this.w_PADRE.MarkPos()     
        this.w_TEST = .F.
      endif
      this.oParentObject.w_CAURIF = _Curs_gste_bd3.CBCAUDIS
      this.oParentObject.w_DICODCON = _Curs_gste_bd3.CBCODBAN
      this.oParentObject.w_DIIMPPRO = _Curs_gste_bd3.CBIMPPRO
      * --- Read from COC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "BADESCRI,BADTOBSO,BA__IBAN,BA__BBAN,BATIPCON,BACODABI"+;
          " from "+i_cTable+" COC_MAST where ";
              +"BACODBAN = "+cp_ToStrODBC(this.oParentObject.w_DICODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          BADESCRI,BADTOBSO,BA__IBAN,BA__BBAN,BATIPCON,BACODABI;
          from (i_cTable) where;
              BACODBAN = this.oParentObject.w_DICODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_DESBAC = NVL(cp_ToDate(_read_.BADESCRI),cp_NullValue(_read_.BADESCRI))
        this.oParentObject.w_BAOBSO = NVL(cp_ToDate(_read_.BADTOBSO),cp_NullValue(_read_.BADTOBSO))
        this.oParentObject.w_IBAN = NVL(cp_ToDate(_read_.BA__IBAN),cp_NullValue(_read_.BA__IBAN))
        this.oParentObject.w_BBAN = NVL(cp_ToDate(_read_.BA__BBAN),cp_NullValue(_read_.BA__BBAN))
        this.oParentObject.w_BATIPO = NVL(cp_ToDate(_read_.BATIPCON),cp_NullValue(_read_.BATIPCON))
        this.oParentObject.w_CODABI = NVL(cp_ToDate(_read_.BACODABI),cp_NullValue(_read_.BACODABI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Carica il Temporaneo dei Dati e skippa al record successivo
      this.w_PADRE.TrsFromWork()     
      * --- Creo una nuova riga per la prossima banca..
        select _Curs_gste_bd3
        continue
      enddo
      use
    endif
    * --- Se cancello tutto e non ho banche rimetto una riga vuota
    if this.w_TEST
      this.w_PADRE.InitRow()     
      this.w_PADRE.MarkPos()     
    endif
    * --- Impedisco l'esecuzione della mCalc all'uscita per evitare di sbiancare gli importi 
    *     (sono calcolati)
    this.bUpdateParentObject=.f.
    * --- MI riposiziono sul transitorio
    this.w_PADRE.REPOS()     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='COC_MAST'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_gste_bd3')
      use in _Curs_gste_bd3
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
