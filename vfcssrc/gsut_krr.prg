* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_krr                                                        *
*              Controllo report                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_4]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-09-06                                                      *
* Last revis.: 2015-03-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_krr
* Occorre essere amministratore
if not cp_IsAdministrator(.F.)
   Ah_ErrorMsg("Procedura utilizzabile solo da utenti con privilegi di amministratore%0Rivolgersi al proprio amministratore di sistema",'!')
   return
endif
* Verifica l'eventuale presenza di maschere aperte
Local L_Msg
L_Msg=openForm(.t.)
IF Not Empty(L_Msg)
   ah_ErrorMsg("Chiudere tutte le maschere; maschera %1 aperta",'!', '', L_MSG)
   return
Endif
* --- Fine Area Manuale
return(createobject("tgsut_krr",oParentObject))

* --- Class definition
define class tgsut_krr as StdForm
  Top    = 28
  Left   = 37

  * --- Standard Properties
  Width  = 381
  Height = 170+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-16"
  HelpContextID=115950441
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_krr"
  cComment = "Controllo report"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PATH = space(254)
  w_CHKTRIM = .F.
  w_CHKPRN = .F.
  w_CHKFONT = .F.
  w_PATHCMP = space(254)
  w_CHKREPVAL = .F.
  w_CMBCOLOR = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_krrPag1","gsut_krr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Controllo report")
      .Pages(2).addobject("oPag","tgsut_krrPag2","gsut_krr",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Compilazione report")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPATH_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSUT_BRR with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PATH=space(254)
      .w_CHKTRIM=.f.
      .w_CHKPRN=.f.
      .w_CHKFONT=.f.
      .w_PATHCMP=space(254)
      .w_CHKREPVAL=.f.
      .w_CMBCOLOR=0
        .w_PATH = alltrim(sys(5)+sys(2003))
        .w_CHKTRIM = .T.
        .w_CHKPRN = .T.
        .w_CHKFONT = .T.
        .w_PATHCMP = Left( alltrim(sys(5)+sys(2003)) , Len( alltrim(sys(5)+sys(2003)) ) -3 )+'vfcssrc\'
        .w_CHKREPVAL = .F.
    endwith
    this.DoRTCalc(7,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCMBCOLOR_1_11.enabled = this.oPgFrm.Page1.oPag.oCMBCOLOR_1_11.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPATH_1_1.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_1.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oCHKTRIM_1_2.RadioValue()==this.w_CHKTRIM)
      this.oPgFrm.Page1.oPag.oCHKTRIM_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHKPRN_1_3.RadioValue()==this.w_CHKPRN)
      this.oPgFrm.Page1.oPag.oCHKPRN_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHKFONT_1_4.RadioValue()==this.w_CHKFONT)
      this.oPgFrm.Page1.oPag.oCHKFONT_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPATHCMP_2_1.value==this.w_PATHCMP)
      this.oPgFrm.Page2.oPag.oPATHCMP_2_1.value=this.w_PATHCMP
    endif
    if not(this.oPgFrm.Page1.oPag.oCHKREPVAL_1_10.RadioValue()==this.w_CHKREPVAL)
      this.oPgFrm.Page1.oPag.oCHKREPVAL_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCMBCOLOR_1_11.RadioValue()==this.w_CMBCOLOR)
      this.oPgFrm.Page1.oPag.oCMBCOLOR_1_11.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(DIRECTORY(FULLPATH(ALLTRIM( .w_PATH ))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPATH_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il percorso indicato non � valido")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsut_krr
      *--- Controllo check
      If !.w_CHKTRIM AND !.w_CHKPRN AND !.w_CHKFONT
         AH_ErrorMsg("Selezionare almeno un check")
         i_bRes=.f.
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_krrPag1 as StdContainer
  Width  = 377
  height = 170
  stdWidth  = 377
  stdheight = 170
  resizeXpos=164
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPATH_1_1 as StdField with uid="YQRRLJERIX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Il percorso indicato non � valido",;
    ToolTipText = "Directory da analizzare",;
    HelpContextID = 110869770,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=68, Top=18, InputMask=replicate('X',254), bHasZoom = .t. 

  func oPATH_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (DIRECTORY(FULLPATH(ALLTRIM( .w_PATH ))))
    endwith
    return bRes
  endfunc

  proc oPATH_1_1.mZoom
    this.parent.oContained.w_PATH=Cp_getdir()
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCHKTRIM_1_2 as StdCheck with uid="FXFWWJSUYK",rtseq=2,rtrep=.f.,left=14, top=71, caption="Trim mode",;
    ToolTipText = "Imposta trim mode a trim nearest Word",;
    HelpContextID = 141575898,;
    cFormVar="w_CHKTRIM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHKTRIM_1_2.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oCHKTRIM_1_2.GetRadio()
    this.Parent.oContained.w_CHKTRIM = this.RadioValue()
    return .t.
  endfunc

  func oCHKTRIM_1_2.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CHKTRIM==.T.,1,;
      0)
  endfunc

  add object oCHKPRN_1_3 as StdCheck with uid="OYTBRWUEEF",rtseq=3,rtrep=.f.,left=128, top=71, caption="Printer environment",;
    ToolTipText = "Elimina print environment",;
    HelpContextID = 57951962,;
    cFormVar="w_CHKPRN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHKPRN_1_3.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oCHKPRN_1_3.GetRadio()
    this.Parent.oContained.w_CHKPRN = this.RadioValue()
    return .t.
  endfunc

  func oCHKPRN_1_3.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CHKPRN==.T.,1,;
      0)
  endfunc

  add object oCHKFONT_1_4 as StdCheck with uid="PLOOTWNMZZ",rtseq=4,rtrep=.f.,left=14, top=107, caption="Font",;
    ToolTipText = "Controllo font",;
    HelpContextID = 206682406,;
    cFormVar="w_CHKFONT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHKFONT_1_4.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oCHKFONT_1_4.GetRadio()
    this.Parent.oContained.w_CHKFONT = this.RadioValue()
    return .t.
  endfunc

  func oCHKFONT_1_4.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CHKFONT==.T.,1,;
      0)
  endfunc


  add object oBtn_1_5 as StdButton with uid="AVEBAEPVRV",left=275, top=124, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per analizzare i report";
    , HelpContextID = 115929882;
    , caption='\<OK';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        do GSUT_BRR with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="YQNCWGTNXV",left=325, top=124, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 206769926;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCHKREPVAL_1_10 as StdCheck with uid="ZTVLSVQMGE",rtseq=6,rtrep=.f.,left=128, top=107, caption="Print repeated value",;
    ToolTipText = "Controllo consistenza condizione Print repeated value",;
    HelpContextID = 230538791,;
    cFormVar="w_CHKREPVAL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHKREPVAL_1_10.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oCHKREPVAL_1_10.GetRadio()
    this.Parent.oContained.w_CHKREPVAL = this.RadioValue()
    return .t.
  endfunc

  func oCHKREPVAL_1_10.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CHKREPVAL==.T.,1,;
      0)
  endfunc


  add object oCMBCOLOR_1_11 as StdCombo with uid="HMHURGLZRU",value=1,rtseq=7,rtrep=.f.,left=60,top=139,width=109,height=22;
    , ToolTipText = "Colore del report";
    , HelpContextID = 95539592;
    , cFormVar="w_CMBCOLOR",RowSource=""+"Nessuna modifica,"+"Bianco e nero,"+"A colori", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCMBCOLOR_1_11.RadioValue()
    return(iif(this.value =1,0,;
    iif(this.value =2,1,;
    iif(this.value =3,2,;
    0))))
  endfunc
  func oCMBCOLOR_1_11.GetRadio()
    this.Parent.oContained.w_CMBCOLOR = this.RadioValue()
    return .t.
  endfunc

  func oCMBCOLOR_1_11.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_CMBCOLOR==0,1,;
      iif(this.Parent.oContained.w_CMBCOLOR==1,2,;
      iif(this.Parent.oContained.w_CMBCOLOR==2,3,;
      0)))
  endfunc

  func oCMBCOLOR_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CHKPRN)
    endwith
   endif
  endfunc

  add object oStr_1_6 as StdString with uid="BBQJEEQAEK",Visible=.t., Left=1, Top=18,;
    Alignment=1, Width=64, Height=18,;
    Caption="Percorso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="INSOLIQNXG",Visible=.t., Left=10, Top=47,;
    Alignment=0, Width=170, Height=18,;
    Caption="Parametri di controllo"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="GMCYYNVOYI",Visible=.t., Left=14, Top=143,;
    Alignment=1, Width=41, Height=18,;
    Caption="Colore:"  ;
  , bGlobalFont=.t.

  add object oBox_1_9 as StdBox with uid="DLRCNIUKZJ",left=9, top=63, width=256,height=107
enddefine
define class tgsut_krrPag2 as StdContainer
  Width  = 377
  height = 170
  stdWidth  = 377
  stdheight = 170
  resizeXpos=183
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPATHCMP_2_1 as StdField with uid="ARKCUYAAIK",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PATHCMP", cQueryName = "PATHCMP",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Directory da analizzare",;
    HelpContextID = 90946826,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=66, Top=18, InputMask=replicate('X',254), bHasZoom = .t. 

  proc oPATHCMP_2_1.mZoom
    this.parent.oContained.w_PATHCMP=getfile("prg")
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc


  add object oBtn_2_3 as StdButton with uid="IVHJIGOPIC",left=275, top=124, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per analizzare i report";
    , HelpContextID = 115929882;
    , caption='\<OK';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      with this.Parent.oContained
        GSUT_BRR(this.Parent.oContained,.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_PATHCMP ))
      endwith
    endif
  endfunc


  add object oBtn_2_4 as StdButton with uid="ZIDSWRUGTG",left=325, top=124, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 206769926;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_2 as StdString with uid="ELHBUTMHNC",Visible=.t., Left=2, Top=18,;
    Alignment=1, Width=61, Height=18,;
    Caption="File:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_krr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
