* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bsw                                                        *
*              Utilizzo librerie                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_284]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-12                                                      *
* Last revis.: 2002-03-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Chiave,w_Programma,w_Operazione,w_Scanner
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bsw",oParentObject,m.w_Chiave,m.w_Programma,m.w_Operazione,m.w_Scanner)
return(i_retval)

define class tgsut_bsw as StdBatch
  * --- Local variables
  w_PROG = .NULL.
  w_Chiave = space(20)
  w_Programma = space(10)
  w_Operazione = space(1)
  w_Scanner = .f.
  w_SingMult = 0
  w_LiCodice = space(254)
  w_Periodo = ctod("  /  /  ")
  w_Archivio = space(2)
  w_Descrizione = space(250)
  w_DATA = space(8)
  w_curdir = space(50)
  w_CATCH = space(250)
  w_posizpunto = 0
  w_posiz2punto = 0
  w_estensione = space(10)
  w_numfile = 0
  w_destinazione = space(250)
  w_sorgente = space(250)
  w_MAX = 0
  w_cont = 0
  w_indice = space(5)
  w_ImgPat = space(254)
  w_CritPeri = space(2)
  w_LIPATIMG = space(254)
  w_Volume = space(10)
  w_TmpPat = space(256)
  w_Ok = .f.
  w_range = 0
  w_FOLDER = space(254)
  w_FL_TIP = space(1)
  w_FL_CLA = space(1)
  w_GESTIONE = space(50)
  w_TIPDEF = space(5)
  w_CLADEF = space(5)
  w_TIPDEFEX = space(5)
  w_CLADEFEX = space(5)
  w_CARTLIB = space(200)
  w_CARTPERI = space(200)
  w_FILE = space(20)
  w_UTDC = ctod("  /  /  ")
  w_MVDATDOC = ctod("  /  /  ")
  w_PNDATREG = ctod("  /  /  ")
  w_CATEGORIA = space(1)
  w_EXTENS = space(10)
  w_ANNO = space(4)
  w_MESE = space(2)
  w_GIORNO = space(2)
  w_APPO = space(40)
  w_DUEKEY = .f.
  w_OBJCTRL = .NULL.
  w_OSOURCE = .NULL.
  w_LcFile = space(10)
  w_ImgHandle = 0
  w_Res = 0
  w_Abort = .f.
  * --- WorkFile variables
  LIB_IMMA_idx=0
  EXT_ENS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Permette di selezionare la libreria da utilizzare
    * --- Parametri
    if this.w_Scanner
       
 Local nCmd 
 nCmd= 0 
 DEFINE POPUP popCmd FROM MROW(),MCOL() SHORTCUT 
 DEFINE BAR 1 OF popCmd PROMPT ah_MsgFormat("Acquisizione singola") 
 ON SELECTION BAR 1 OF popCmd nCmd = 1 
 DEFINE BAR 2 OF popCmd PROMPT ah_MsgFormat("Acquisizione multipla") 
 ON SELECTION BAR 2 OF popCmd nCmd = 2 
 ACTIVATE POPUP popCmd 
 DEACTIVATE POPUP popCmd 
 RELEASE POPUPS popCmd
      * --- Variabile che indica se l'acquisizione da scanner � singola o multipla
      *     1 = singola 
      *     2 = multipla
      this.w_SingMult = nCmd
      if this.w_SingMult = 0
        Ah_ErrorMsg("Modalit� di acquisizione non impostata!",48,"")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- w_Scanner : se .F. o non passato esegue cattura da file
    *     se .T. lancia programma associato allo Scanner
    this.w_LiCodice = space(1)
    this.w_FILE = space(20)
    this.w_curdir = space(50)
    this.w_CATCH = space(250)
    this.w_posizpunto = 0
    this.w_posiz2punto = 0
    this.w_estensione = space(10)
    this.w_numfile = 0
    this.w_destinazione = space(250)
    this.w_sorgente = space(250)
    this.w_MAX = 0
    this.w_cont = 1
    this.w_indice = space(5)
    this.w_ImgPat = space(254)
    this.w_CritPeri = space(2)
    this.w_LIPATIMG = space(254)
    this.w_Volume = space(10)
    this.w_TmpPat = space(40)
    this.w_range = 0
    this.w_Ok = .T.
    this.w_FOLDER = space(254)
    this.w_TIPDEF = Space(5)
    this.w_CLADEF = Space(5)
    this.w_TIPDEFEX = Space(5)
    this.w_CLADEFEX = Space(5)
    this.w_CARTLIB = ""
    this.w_CARTPERI = ""
    * --- Variabili utilizzate per permettere l'esecuzione corretta del batch gsut_bsw che lavora su queste variabili utilizzando "this.oParentObject"
    do case
      case EMPTY(this.w_CHIAVE)
        * --- Esce se non selezionato alcun record
        ah_ErrorMsg("Selezionare un record","!","")
        i_retcode = 'stop'
        return
      case type("this.oParentObject.cFunction") ="C" and this.oParentObject.cFunction="Load"
        ah_ErrorMsg("Funzione non eseguibile durante il caricamento","!","")
        i_retcode = 'stop'
        return
    endcase
    * --- Inizializzazione variabili per controllo esistenza librerie
    * --- Determina dati in base al programma chiamante
    do case
      case this.w_Programma = "GSMA_AAR" OR this.w_Programma = "GSMA_AAS"
        * --- Il programma chiamante � l'anagrafica Articoli o l'anagrafica Servizi
        this.w_Archivio = "AR"
        this.w_FILE = "A_" + alltrim(this.w_Chiave)
        this.w_Periodo = this.oParentObject.w_UTDC
        this.w_UTDC = this.w_Periodo
        this.w_Descrizione = alltrim(this.w_Chiave) + " - " + this.oParentObject.w_ARDESART
      case this.w_Programma = "GSMA_ACA"
        this.w_Archivio = "AR"
        this.w_Periodo = this.oParentObject.w_UTDC
        this.w_FILE = "A_" + alltrim(this.w_Chiave)
        this.w_UTDC = this.w_Periodo
        this.w_Descrizione = alltrim(this.w_Chiave) + " - " + this.oParentObject.w_CADESART
      case this.w_Programma = "GSEC_KIC"
        this.w_Archivio = "AR"
        this.w_FILE = this.oParentObject.oParentObject.w_CATEGORIA+"_"+ alltrim(this.w_Chiave)
        this.w_CATEGORIA = this.oParentObject.oParentObject.w_CATEGORIA
        this.w_Periodo = cp_CharToDate("  -  -  ")
        this.w_Descrizione = alltrim(this.w_Chiave) + " - " + this.oParentObject.oParentObject.w_DESCRI
      case this.w_Programma = "GSVE_MDV" OR this.w_Programma = "GSOR_MDV" OR this.w_Programma = "GSAC_MDV" 
        this.w_Archivio = "DO"
        this.w_Periodo = this.oParentObject.w_MVDATDOC
        this.w_FILE = "D_" + alltrim(this.w_Chiave)
        this.w_DATA = alltrim(DateToChar(this.oParentObject.w_MVDATDOC))
        this.w_MVDATDOC = this.w_Periodo
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_Descrizione = ah_Msgformat( "%1 n. doc. %2 del %3", alltrim(this.oParentObject.w_DESDOC), alltrim(str(this.oParentObject.w_MVNUMDOC,15))+" / "+alltrim(this.oParentObject.w_MVALFDOC), this.w_DATA)
      case this.w_Programma = "GSCG_MPN"
        this.w_Archivio = "PN"
        this.w_Periodo = this.oParentObject.w_PNDATREG
        this.w_FILE = "P_" + alltrim(this.w_Chiave)
        this.w_DATA = alltrim(DateToChar(this.oParentObject.w_PNDATREG))
        this.w_PNDATREG = this.w_Periodo
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_Descrizione = ah_Msgformat("N. %1 del %2", alltrim(Str(this.oParentObject.w_PNNUMRER))+ " / "+alltrim(Str(this.oParentObject.w_PNCODUTE)), this.w_DATA)
      case this.w_Programma = "GSAR_ACL" Or this.w_Programma = "GSAR_AFR"
        * --- Il programma chiamante � l'anagrafica Clienti o Fornitori
        if this.w_Programma = "GSAR_AFR"
          this.w_Archivio = "FR"
          this.w_FILE = "F_" + alltrim(this.w_Chiave)
        else
          this.w_Archivio = "CL"
          this.w_FILE = "C_" + alltrim(this.w_Chiave)
        endif
        this.w_Periodo = this.oParentObject.w_UTDC
        this.w_UTDC = this.w_Periodo
        this.w_Descrizione = alltrim(this.w_Chiave) + " - " + this.oParentObject.w_ANDESCRI
      case this.w_Programma = "GSAR_AOM"
        * --- Categorie Omogenee
        this.w_Archivio = "CO"
        this.w_FILE = "O_" + alltrim(this.w_Chiave)
        this.w_Periodo = i_DATSYS
        this.w_UTDC = this.w_Periodo
        this.w_Descrizione = alltrim(this.w_Chiave) + " - " + this.oParentObject.w_OMDESCRI
      case this.w_Programma = "GSAR_AFA"
        * --- Famiglie Articoli
        this.w_Archivio = "FA"
        this.w_FILE = "K_" + alltrim(this.w_Chiave)
        this.w_Periodo = i_DATSYS
        this.w_UTDC = this.w_Periodo
        this.w_Descrizione = alltrim(this.w_Chiave) + " - " + this.oParentObject.w_FADESCRI
      case this.w_Programma = "GSAR_AMH"
        * --- Marchi
        this.w_Archivio = "MA"
        this.w_FILE = "M_" + alltrim(this.w_Chiave)
        this.w_Periodo = i_DATSYS
        this.w_UTDC = this.w_Periodo
        this.w_Descrizione = alltrim(this.w_Chiave) + " - " + this.oParentObject.w_MADESCRI
      case this.w_Programma = "GSAR_AGM"
        * --- Gruppi Merceologici
        this.w_Archivio = "GM"
        this.w_FILE = "G_" + alltrim(this.w_Chiave)
        this.w_Periodo = i_DATSYS
        this.w_UTDC = this.w_Periodo
        this.w_Descrizione = alltrim(this.w_Chiave) + " - " + this.oParentObject.w_GMDESCRI
    endcase
    * --- Eventuale conversione di caratteri particolari contenuti nel nome file (w_FILE)
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Cerca libreria GESTFILE
    * --- Read from LIB_IMMA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.LIB_IMMA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LIB_IMMA_idx,2],.t.,this.LIB_IMMA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LICODICE"+;
        " from "+i_cTable+" LIB_IMMA where ";
            +"LICODICE = "+cp_ToStrODBC("GESTFILE");
            +" and LITIPARC = "+cp_ToStrODBC(this.w_Archivio);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LICODICE;
        from (i_cTable) where;
            LICODICE = "GESTFILE";
            and LITIPARC = this.w_Archivio;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_LiCodice = NVL(cp_ToDate(_read_.LICODICE),cp_NullValue(_read_.LICODICE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se esiste la libreria GESTFILE viene utilizzata, altrimenti si usa la libreria SCANVIEW
    if not Empty(this.w_LiCodice)
      * --- Utilizza la  libreria GESTFILE
      do case
        case this.w_Operazione = "V"
          this.w_GESTIONE = this.oparentobject.cComment
          this.w_GESTIONE = LEFT(this.w_GESTIONE, iif(RAT("/",this.w_GESTIONE )=0,50,RAT("/",this.w_GESTIONE)-1))
          do gsut_kgf with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case this.w_Operazione = "C"
          * --- Read from LIB_IMMA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.LIB_IMMA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIB_IMMA_idx,2],.t.,this.LIB_IMMA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LIDATRIF"+;
              " from "+i_cTable+" LIB_IMMA where ";
                  +"LICODICE = "+cp_ToStrODBC("GESTFILE");
                  +" and LITIPARC = "+cp_ToStrODBC(this.w_Archivio);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LIDATRIF;
              from (i_cTable) where;
                  LICODICE = "GESTFILE";
                  and LITIPARC = this.w_Archivio;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CritPeri = NVL(cp_ToDate(_read_.LIDATRIF),cp_NullValue(_read_.LIDATRIF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if not Empty(this.w_CritPeri)
            * --- Le impostazioni vengono lette solo se ha trovato il record desiderato. Serve per non perdere i valori di default.
            * --- Read from LIB_IMMA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.LIB_IMMA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LIB_IMMA_idx,2],.t.,this.LIB_IMMA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "LIPATIMG,LIVOLUME,LITIPALL,LICLAALL,LIFL_TIP,LIFL_CLA"+;
                " from "+i_cTable+" LIB_IMMA where ";
                    +"LICODICE = "+cp_ToStrODBC("GESTFILE");
                    +" and LITIPARC = "+cp_ToStrODBC(this.w_Archivio);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                LIPATIMG,LIVOLUME,LITIPALL,LICLAALL,LIFL_TIP,LIFL_CLA;
                from (i_cTable) where;
                    LICODICE = "GESTFILE";
                    and LITIPARC = this.w_Archivio;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_LIPATIMG = NVL(cp_ToDate(_read_.LIPATIMG),cp_NullValue(_read_.LIPATIMG))
              this.w_Volume = NVL(cp_ToDate(_read_.LIVOLUME),cp_NullValue(_read_.LIVOLUME))
              this.w_TIPDEF = NVL(cp_ToDate(_read_.LITIPALL),cp_NullValue(_read_.LITIPALL))
              this.w_CLADEF = NVL(cp_ToDate(_read_.LICLAALL),cp_NullValue(_read_.LICLAALL))
              this.w_FL_TIP = NVL(cp_ToDate(_read_.LIFL_TIP),cp_NullValue(_read_.LIFL_TIP))
              this.w_FL_CLA = NVL(cp_ToDate(_read_.LIFL_CLA),cp_NullValue(_read_.LIFL_CLA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Acquisizione della directory corrente
          this.w_curdir = sys(5) + sys(2003)
          if this.w_Scanner
            * --- Cattura da scanner
            this.Pag8()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            * --- Seleziona file da disco
            this.w_CATCH = getfile()
          endif
          set default to (this.w_curdir)
          cd (this.w_curdir)
          if not(Empty(this.w_CATCH))
            this.w_posizpunto = rat(".", this.w_CATCH)
            this.w_estensione = Alltrim(lower(substr(this.w_CATCH,this.w_posizpunto)))
            this.w_EXTENS = Substr(Alltrim(this.w_estensione),2)
            * --- Leggo se esiste una Estensione e da questa prendo la Tipologia e la Classe di Default
            * --- Read from EXT_ENS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.EXT_ENS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.EXT_ENS_idx,2],.t.,this.EXT_ENS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "EXTIPALL,EXCLAALL"+;
                " from "+i_cTable+" EXT_ENS where ";
                    +"EXCODICE = "+cp_ToStrODBC(this.w_EXTENS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                EXTIPALL,EXCLAALL;
                from (i_cTable) where;
                    EXCODICE = this.w_EXTENS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_TIPDEFEX = NVL(cp_ToDate(_read_.EXTIPALL),cp_NullValue(_read_.EXTIPALL))
              this.w_CLADEFEX = NVL(cp_ToDate(_read_.EXCLAALL),cp_NullValue(_read_.EXCLAALL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Se esiste nella Estensione una Tipologia prendo questa
            *     altrimenti lascio quella caricata da Librerie Immagini
            if Not Empty(this.w_TIPDEFEX)
              this.w_TIPDEF = this.w_TIPDEFEX
            endif
            * --- Se esiste nella Estensione una Classe prendo questa
            *     altrimenti lascio quella caricata da Librerie Immagini
            if Not Empty(this.w_CLADEFEX)
              this.w_CLADEF = this.w_CLADEFEX
            endif
            this.w_FILE = UPPER(alltrim(this.w_FILE))
            this.Pag6()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
      endcase
    else
      * --- Non gestito Scan&View per Clienti Fornitori
      if Not(this.w_Archivio = "CL" Or this.w_Archivio = "FR")
        * --- Cerca libreria SCANVIEW
        * --- Read from LIB_IMMA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LIB_IMMA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIB_IMMA_idx,2],.t.,this.LIB_IMMA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LICODICE"+;
            " from "+i_cTable+" LIB_IMMA where ";
                +"LICODICE = "+cp_ToStrODBC("SCANVIEW");
                +" and LITIPARC = "+cp_ToStrODBC(this.w_Archivio);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LICODICE;
            from (i_cTable) where;
                LICODICE = "SCANVIEW";
                and LITIPARC = this.w_Archivio;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LiCodice = NVL(cp_ToDate(_read_.LICODICE),cp_NullValue(_read_.LICODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows > 0
          * --- Esiste libreria SCANVIEW, allora richiama il batch relativo alla libreria SCANVIEW.
          gsut_bas(this,this.w_Chiave, this.w_Programma, this.w_Operazione)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Non esiste n� la libreria GESTFILE, n� la libreria SCANVIEW
          ah_ErrorMsg("Libreria non trovata","i","")
        endif
      else
        ah_ErrorMsg("Libreria non gestita per clienti e fornitori","i","")
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Conversione formato data
    this.w_ANNO = substr(this.w_DATA,1,4)
    this.w_MESE = substr(this.w_DATA,5,2)
    this.w_GIORNO = substr(this.w_DATA,7,2)
    this.w_DATA = this.w_GIORNO+"-"+this.w_MESE +"-"+this.w_ANNO
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Periodo
    this.w_LIPATIMG = alltrim(this.w_LIPATIMG) + iif(right(alltrim(this.w_LIPATIMG),1) <>"\" , "\" , "" )
    * --- Cartella di Base Definita nelle Librerie Immagini
    this.w_CARTLIB = this.w_LIPATIMG
    * --- Completamento Path conTipologia e Classe allegato
    this.w_LIPATIMG = alltrim(this.w_LIPATIMG) + IIF(this.w_FL_TIP="S",alltrim(this.w_TIPDEF)+"\","") + IIF(this.w_FL_CLA="S",alltrim(this.w_CLADEF)+"\","")
    * --- Calcolo nome directory memorizzazione immagini
    this.w_ImgPat = this.w_LIPATIMG
    * --- Cartella del periodo
    do case
      case this.w_CritPeri = "M"
        * --- Raggruppamento per mesi
        this.w_CARTPERI = right("00"+alltrim(str(month(this.w_Periodo),2,0)), 2) + str(year(this.w_Periodo),4,0)
      case this.w_CritPeri = "T"
        * --- Raggruppamento per trimestri
        do case
          case month(this.w_Periodo) > 0 .and. month(this.w_Periodo) < 4
            this.w_CARTPERI = "T1" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 3 .and. month(this.w_Periodo) < 7
            this.w_CARTPERI = "T2" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 6 .and. month(this.w_Periodo) < 10
            this.w_CARTPERI = "T3" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 9
            this.w_CARTPERI = "T4" + str(year(this.w_Periodo),4,0)
        endcase
      case this.w_CritPeri = "Q"
        * --- Raggruppamento per quadrimestri
        do case
          case month(this.w_Periodo) > 0 .and. month(this.w_Periodo) < 5
            this.w_CARTPERI = "Q1" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 4 .and. month(this.w_Periodo) < 9
            this.w_CARTPERI = "Q2" + str(year(this.w_Periodo),4,0)
          case month(this.w_Periodo) > 8
            this.w_CARTPERI = "Q3" + str(year(this.w_Periodo),4,0)
        endcase
      case this.w_CritPeri = "S"
        * --- Raggruppamento per semestri
        this.w_CARTPERI = iif( month(this.w_Periodo) >6 , "S2", "S1") + str(year(this.w_Periodo),4,0)
      case this.w_CritPeri = "A"
        * --- Raggruppamento per anni
        this.w_CARTPERI = str(year(this.w_Periodo),4,0)
    endcase
    this.w_CARTPERI = Alltrim(i_Codazi) + Alltrim(this.w_CARTPERI)
    this.w_ImgPat = this.w_ImgPat+Alltrim(this.w_CARTPERI)+"\"
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea la cartella specificata nella variabile w_FOLDER
    *     Imposta la variabile w_ERRORE in caso di errore.
    w_ERRORE = .F.
    w_ErrorHandler = on("ERROR")
    on error w_ERRORE = .T.
    md (this.w_FOLDER)
    on error &w_ErrorHandler
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eseguo la sostituzione dei caratteri per i quali non � possibile salvare il nome di un file.
    *     \, /, :, *, ?, ", <, >, |
    *     
    this.w_APPO = this.w_FILE
    this.w_APPO = Strtran(this.w_APPO,"^","^^")
    this.w_APPO = Strtran(this.w_APPO,"\","^1")
    this.w_APPO = Strtran(this.w_APPO,"/","^2")
    this.w_APPO = Strtran(this.w_APPO,":","^3")
    this.w_APPO = Strtran(this.w_APPO,"*","^4")
    this.w_APPO = Strtran(this.w_APPO,"?","^5")
    this.w_APPO = Strtran(this.w_APPO,'"',"^6")
    this.w_APPO = Strtran(this.w_APPO,"<","^7")
    this.w_APPO = Strtran(this.w_APPO,">","^8")
    this.w_APPO = Strtran(this.w_APPO,"|","^9")
    this.w_APPO = Strtran(this.w_APPO,"&","^A")
    this.w_FILE = this.w_APPO
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_DUEKEY = .F.
    this.w_PROG = GSUT_AGF()
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_PROG.bSec1)
      Ah_ErrorMsg("Impossibile aprire la funzionalit� gestione file!",48,"")
      i_retcode = 'stop'
      return
    endif
    this.w_PROG.ecpLoad()     
    * --- Path file
    this.w_FILE = Alltrim(this.w_PROG.w_GFSERIAL) +"_"+ UPPER(alltrim(this.w_FILE))
    this.w_PROG.w_FILE = this.w_FILE
    this.w_PROG.w_NUMFILE = this.w_NUMFILE
    this.w_PROG.w_ESTENSIONE = this.w_ESTENSIONE
    this.w_PROG.w_COLLEG = alltrim(this.w_CATCH)
    this.w_PROG.w_COPIA = alltrim(this.w_ImgPat)
    this.w_PROG.w_GF__FILE = Alltrim(this.w_FILE) + Alltrim(this.w_ESTENSIONE)
    this.w_PROG.w_SCANNER = this.w_SCANNER
    * --- Cartella del periodo comprensiva del codice azienda
    this.w_PROG.w_PERIODO = Alltrim(this.w_CARTPERI)
    * --- Cartella Principale: praticamente quella definita nella Libreria Immagine
    this.w_PROG.w_CARTLIB = Alltrim(this.w_CARTLIB)
    * --- Completamento Path allegato con Tipologia/Classe Allegato
    this.w_PROG.w_FL_TIP = this.w_FL_TIP
    this.w_PROG.w_FL_CLA = this.w_FL_CLA
    * --- Valorizzazione Tipologia Allegato con valore di default
    this.w_PROG.w_GFTIPALL = this.w_TIPDEF
    L_NOMEVAR = "w_GFTIPALL"
    L_VALORE = this.w_TIPDEF
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Valorizzazione Classe Allegato con valore di default
    this.w_DUEKEY = .T.
    this.w_PROG.w_GFCLAALL = this.w_CLADEF
    L_NOMEVAR = "w_GFCLAALL"
    L_KEY1 = this.w_TIPDEF
    L_VALORE = this.w_CLADEF
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Ne caso di Clienti Fornitori con Chiave doppia
    this.w_DUEKEY = .F.
    * --- Data Registrazione
    this.w_PROG.w_GFDATREG = i_DATSYS
    this.w_PROG.w_GFDATINI = i_DATSYS
    this.w_PROG.w_GFMODALL = "F"
    this.w_PROG.w_GF__PATH = alltrim(this.w_ImgPat) + Alltrim(this.w_FILE) + Alltrim(this.w_ESTENSIONE)
    * --- Pe tutte le chiavi esterne il valore che devono assumere i campi � contenuto in w_CHIAVE
    L_VALORE = this.w_CHIAVE
    * --- Valorizzo la chiave esterna
    do case
      case this.w_ARCHIVIO = "AR"
        * --- Articoli
        this.w_PROG.w_GFCODPAD = "A"
        this.w_PROG.w_GFCODART = this.w_CHIAVE
        L_NOMEVAR = "w_GFCODART"
      case this.w_ARCHIVIO = "DO"
        * --- Documenti
        this.w_PROG.w_GFCODPAD = "D"
        this.w_PROG.w_GFSERDOC = this.w_CHIAVE
        L_NOMEVAR = "w_GFSERDOC"
      case this.w_ARCHIVIO = "PN"
        * --- Primanota
        this.w_PROG.w_GFCODPAD = "P"
        this.w_PROG.w_GFSERPNT = this.w_CHIAVE
        L_NOMEVAR = "w_GFSERPNT"
      case this.w_ARCHIVIO = "CL"
        * --- Clienti
        this.w_DUEKEY = .T.
        this.w_PROG.w_GFCODPAD = "C"
        this.w_PROG.w_GFTIPCON = "C"
        this.w_PROG.w_GFCODCON = this.w_CHIAVE
        L_NOMEVAR = "w_GFCODCON"
        L_KEY1 = this.w_PROG.w_GFTIPCON
      case this.w_ARCHIVIO = "FR"
        * --- Fornitori
        this.w_DUEKEY = .T.
        this.w_PROG.w_GFCODPAD = "C"
        this.w_PROG.w_GFTIPCON = "F"
        this.w_PROG.w_GFCODCON = this.w_CHIAVE
        L_NOMEVAR = "w_GFCODCON"
        L_KEY1 = this.w_PROG.w_GFTIPCON
      case this.w_ARCHIVIO = "CO"
        * --- Categorie Omogenee
        this.w_PROG.w_GFCODPAD = "O"
        this.w_PROG.w_GFCODCAT = this.w_CHIAVE
        L_NOMEVAR = "w_GFCODCAT"
      case this.w_ARCHIVIO = "FA"
        * --- Famiglie Articoli
        this.w_PROG.w_GFCODPAD = "F"
        this.w_PROG.w_GFCODFAM = this.w_CHIAVE
        L_NOMEVAR = "w_GFCODFAM"
      case this.w_ARCHIVIO = "MA"
        * --- Marchi
        this.w_PROG.w_GFCODPAD = "M"
        this.w_PROG.w_GFCODMAR = this.w_CHIAVE
        L_NOMEVAR = "w_GFCODMAR"
      case this.w_ARCHIVIO = "GM"
        * --- Gruppi merceologici
        this.w_PROG.w_GFCODPAD = "G"
        this.w_PROG.w_GFCODGRU = this.w_CHIAVE
        L_NOMEVAR = "w_GFCODGRU"
    endcase
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Clienti, Fornitori, Documenti, Primanota e articoli eseguo l'EcpDrop
    this.w_OBJCTRL = this.w_PROG.GetCtrl(L_NOMEVAR)
    if this.w_DUEKEY
      * --- Clienti e Fornitori valorizzo la doppia chiave
      this.w_OSOURCE.xKey( 1 ) = L_KEY1
      this.w_OSOURCE.xKey( 2 ) = L_VALORE
    else
      this.w_OSOURCE.xKey( 1 ) = L_VALORE
    endif
    this.w_OBJCTRL.ecpDrop(this.w_OSOURCE)     
  endproc


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Catturo immagine da Scanner: lancio programma di acquisizione immagini dello scanner
     
 DECLARE INTEGER TWAIN_LoadSourceManager IN Eztwain3.DLL 
 DECLARE INTEGER TWAIN_SelectImageSource IN Eztwain3.DLL INTEGER hWnd 
 DECLARE INTEGER TWAIN_GetSourceList IN Eztwain3.DLL 
 DECLARE INTEGER TWAIN_GetNextSourceName IN Eztwain3.DLL STRING @cSourceName 
 DECLARE INTEGER TWAIN_OpenSource IN Eztwain3.DLL STRING @ pStr 
 DECLARE INTEGER TWAIN_AcquireNative IN Eztwain3.DLL INTEGER nAppWind, INTEGER nPixelTypes 
 DECLARE INTEGER TWAIN_WriteNativeToFilename IN Eztwain3.DLL INTEGER nDIB, STRING cFilename 
 DECLARE INTEGER TWAIN_FreeNative IN Eztwain3.DLL INTEGER nDIB 
 DECLARE INTEGER TWAIN_SetMultiTransfer IN Eztwain3.DLL INTEGER nFlag 
 DECLARE LONG TWAIN_BeginMultipageFile IN Eztwain3.DLL STRING sFile 
 DECLARE LONG TWAIN_DibWritePage IN Eztwain3.DLL LONG hdib 
 DECLARE LONG TWAIN_EndMultipageFile IN Eztwain3.DLL 
 DECLARE LONG TWAIN_CloseSource IN Eztwain3.DLL
    * --- File sul quale memorizzero l'immagine acquisita
    if this.w_SingMult = 1
      this.w_LcFile = tempadhoc()+"\"+SYS(2015)+".BMP"
    else
      this.w_LcFile = tempadhoc()+"\"+SYS(2015)+".PDF"
    endif
    * --- Seleziono la periferica di acquisizione immagine
    if Type("g_SCANNER")<>"U" And TWAIN_OpenSource(Alltrim(g_SCANNER)) = 1
      this.w_Res = 1
    else
      this.w_Res = TWAIN_SelectImageSource(this.oParentObject.hWnd)
    endif
    * --- Se = 1 ho selezionato il dispositivo
    if this.w_Res <>0
      * --- Acquisisco l'immagine
      if this.w_SingMult = 1
        this.w_ImgHandle = TWAIN_AcquireNative(this.oParentObject.hWnd,0)
        * --- Salvo l'immagine con il nome del file sopra creato
        this.w_Res = TWAIN_WriteNativeToFilename(this.w_ImgHandle,this.w_LcFile)
        * --- Rilascio il file 
        TWAIN_FreeNative(this.w_ImgHandle)
      else
        this.w_Abort = .t.
        TWAIN_SetMultiTransfer(1)
        this.w_Res = TWAIN_BeginMultipageFile(this.w_LcFile)
        do while this.w_Res = 0
          this.w_ImgHandle = TWAIN_AcquireNative(0,0)
          if this.w_ImgHandle = 0
            Exit
          endif
          this.w_Abort = .f.
          this.w_Res = TWAIN_DibWritePage(this.w_ImgHandle)
        enddo
        this.w_Res = min(this.w_Res, TWAIN_EndMultipageFile())
        TWAIN_CloseSource()
      endif
      if this.w_Res <> 0 Or this.w_Abort
        * --- File non acquisito o non accedibile
        if File(alltrim(this.w_LcFile))
          Erase(this.w_LcFile)
        endif
        i_retcode = 'stop'
        return
      endif
      this.w_CATCH = Alltrim(this.w_LcFile)
    else
      Ah_ErrorMsg("Periferica di acquisizione immagine non selezionata!",48,"")
      i_retcode = 'stop'
      return
    endif
  endproc


  proc Init(oParentObject,w_Chiave,w_Programma,w_Operazione,w_Scanner)
    this.w_Chiave=w_Chiave
    this.w_Programma=w_Programma
    this.w_Operazione=w_Operazione
    this.w_Scanner=w_Scanner
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='LIB_IMMA'
    this.cWorkTables[2]='EXT_ENS'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Chiave,w_Programma,w_Operazione,w_Scanner"
endproc
