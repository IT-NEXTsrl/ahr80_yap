* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_kus                                                        *
*              Accorpamento scadenze                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_164]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-13                                                      *
* Last revis.: 2016-11-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_kus",oParentObject))

* --- Class definition
define class tgste_kus as StdForm
  Top    = 1
  Left   = 3

  * --- Standard Properties
  Width  = 822
  Height = 502
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-11-15"
  HelpContextID=65680489
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=79

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  COC_MAST_IDX = 0
  MOD_PAGA_IDX = 0
  BAN_CHE_IDX = 0
  VALUTE_IDX = 0
  PAR_TITE_IDX = 0
  AGENTI_IDX = 0
  BAN_CONTI_IDX = 0
  DATI_AGG_IDX = 0
  PNT_MAST_IDX = 0
  cPrg = "gste_kus"
  cComment = "Accorpamento scadenze"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_SERIALE = space(10)
  o_SERIALE = space(10)
  w_ROWORD = 0
  w_ROWRIF = 0
  w_SCAINI = ctod('  /  /  ')
  w_SCAFIN = ctod('  /  /  ')
  w_NUMPAR = space(14)
  w_NDOINI = 0
  w_NDOFIN = 0
  w_ADOINI = space(2)
  w_ADOFIN = space(2)
  w_DDOINI = ctod('  /  /  ')
  w_DDOFIN = ctod('  /  /  ')
  w_NEWSCA = ctod('  /  /  ')
  w_NEWAGE = space(5)
  o_NEWAGE = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_ZTIPCON = space(1)
  o_ZTIPCON = space(1)
  w_ZCODCON = space(15)
  w_DESCRI = space(40)
  w_FLSELE = 0
  w_SELEZI = space(1)
  w_ZBANAPP = space(10)
  w_DESBAN = space(50)
  w_ZBANNOS = space(15)
  w_DESNOS = space(35)
  w_ZCODVAL = space(3)
  o_ZCODVAL = space(3)
  w_CAMBIO = 0
  w_CAOVAL = 0
  w_DATOBSO = ctod('  /  /  ')
  w_ZMODPAG = space(10)
  w_DESPAG = space(35)
  w_ZCODAGE = space(5)
  w_ZREGINI = ctod('  /  /  ')
  w_ZREGFIN = ctod('  /  /  ')
  w_ZNDOFIN = 0
  w_ZADOINI = space(10)
  w_ZADOFIN = space(10)
  w_ZDDOINI = ctod('  /  /  ')
  w_ZDDOFIN = ctod('  /  /  ')
  w_ZNUMPAR = space(31)
  w_ZNDOINI = 0
  w_ZSCAINI = ctod('  /  /  ')
  w_ZSCAFIN = ctod('  /  /  ')
  w_DESVAL = space(35)
  w_NBANAPP = space(10)
  o_NBANAPP = space(10)
  w_NBANNOS = space(15)
  w_NMODPAG = space(10)
  w_TIPRIF = space(1)
  w_CONRIF = space(15)
  w_ZTIPRIF = space(1)
  w_ZCONRIF = space(15)
  w_NCODAGE = space(5)
  w_RIFRAG = space(50)
  w_NNUMDOC = 0
  w_NEWPAR = space(31)
  w_NALFDOC = space(10)
  w_NDATDOC = ctod('  /  /  ')
  w_NFLVABD = space(1)
  w_TOTALE1 = 0
  w_ZCSIMVAL = space(5)
  w_FLSALD = space(1)
  w_REGINI = ctod('  /  /  ')
  w_REGFIN = ctod('  /  /  ')
  w_CODAGE = space(5)
  w_NNUMCOR = space(25)
  w_PNAGG_01 = space(15)
  w_PNAGG_02 = space(15)
  w_PNAGG_03 = space(15)
  w_PNAGG_04 = space(15)
  w_PNAGG_05 = ctod('  /  /  ')
  w_PNAGG_06 = ctod('  /  /  ')
  w_DASERIAL = space(10)
  w_DACAM_01 = space(30)
  w_DACAM_02 = space(30)
  w_DACAM_03 = space(30)
  w_DACAM_04 = space(30)
  w_DACAM_05 = space(30)
  w_DACAM_06 = space(30)
  w_DATIAGG = space(10)
  w_PNNUMFAT = space(50)
  w_CalcZoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_kusPag1","gste_kus",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNEWSCA_1_13
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_CalcZoom = this.oPgFrm.Pages(1).oPag.CalcZoom
    DoDefault()
    proc Destroy()
      this.w_CalcZoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='COC_MAST'
    this.cWorkTables[3]='MOD_PAGA'
    this.cWorkTables[4]='BAN_CHE'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='PAR_TITE'
    this.cWorkTables[7]='AGENTI'
    this.cWorkTables[8]='BAN_CONTI'
    this.cWorkTables[9]='DATI_AGG'
    this.cWorkTables[10]='PNT_MAST'
    return(this.OpenAllTables(10))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SERIALE=space(10)
      .w_ROWORD=0
      .w_ROWRIF=0
      .w_SCAINI=ctod("  /  /  ")
      .w_SCAFIN=ctod("  /  /  ")
      .w_NUMPAR=space(14)
      .w_NDOINI=0
      .w_NDOFIN=0
      .w_ADOINI=space(2)
      .w_ADOFIN=space(2)
      .w_DDOINI=ctod("  /  /  ")
      .w_DDOFIN=ctod("  /  /  ")
      .w_NEWSCA=ctod("  /  /  ")
      .w_NEWAGE=space(5)
      .w_OBTEST=ctod("  /  /  ")
      .w_ZTIPCON=space(1)
      .w_ZCODCON=space(15)
      .w_DESCRI=space(40)
      .w_FLSELE=0
      .w_SELEZI=space(1)
      .w_ZBANAPP=space(10)
      .w_DESBAN=space(50)
      .w_ZBANNOS=space(15)
      .w_DESNOS=space(35)
      .w_ZCODVAL=space(3)
      .w_CAMBIO=0
      .w_CAOVAL=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_ZMODPAG=space(10)
      .w_DESPAG=space(35)
      .w_ZCODAGE=space(5)
      .w_ZREGINI=ctod("  /  /  ")
      .w_ZREGFIN=ctod("  /  /  ")
      .w_ZNDOFIN=0
      .w_ZADOINI=space(10)
      .w_ZADOFIN=space(10)
      .w_ZDDOINI=ctod("  /  /  ")
      .w_ZDDOFIN=ctod("  /  /  ")
      .w_ZNUMPAR=space(31)
      .w_ZNDOINI=0
      .w_ZSCAINI=ctod("  /  /  ")
      .w_ZSCAFIN=ctod("  /  /  ")
      .w_DESVAL=space(35)
      .w_NBANAPP=space(10)
      .w_NBANNOS=space(15)
      .w_NMODPAG=space(10)
      .w_TIPRIF=space(1)
      .w_CONRIF=space(15)
      .w_ZTIPRIF=space(1)
      .w_ZCONRIF=space(15)
      .w_NCODAGE=space(5)
      .w_RIFRAG=space(50)
      .w_NNUMDOC=0
      .w_NEWPAR=space(31)
      .w_NALFDOC=space(10)
      .w_NDATDOC=ctod("  /  /  ")
      .w_NFLVABD=space(1)
      .w_TOTALE1=0
      .w_ZCSIMVAL=space(5)
      .w_FLSALD=space(1)
      .w_REGINI=ctod("  /  /  ")
      .w_REGFIN=ctod("  /  /  ")
      .w_CODAGE=space(5)
      .w_NNUMCOR=space(25)
      .w_PNAGG_01=space(15)
      .w_PNAGG_02=space(15)
      .w_PNAGG_03=space(15)
      .w_PNAGG_04=space(15)
      .w_PNAGG_05=ctod("  /  /  ")
      .w_PNAGG_06=ctod("  /  /  ")
      .w_DASERIAL=space(10)
      .w_DACAM_01=space(30)
      .w_DACAM_02=space(30)
      .w_DACAM_03=space(30)
      .w_DACAM_04=space(30)
      .w_DACAM_05=space(30)
      .w_DACAM_06=space(30)
      .w_DATIAGG=space(10)
      .w_PNNUMFAT=space(50)
      .w_SERIALE=oParentObject.w_SERIALE
      .w_ROWORD=oParentObject.w_ROWORD
      .w_ROWRIF=oParentObject.w_ROWRIF
      .w_SCAINI=oParentObject.w_SCAINI
      .w_SCAFIN=oParentObject.w_SCAFIN
      .w_NUMPAR=oParentObject.w_NUMPAR
      .w_NDOINI=oParentObject.w_NDOINI
      .w_NDOFIN=oParentObject.w_NDOFIN
      .w_ADOINI=oParentObject.w_ADOINI
      .w_ADOFIN=oParentObject.w_ADOFIN
      .w_DDOINI=oParentObject.w_DDOINI
      .w_DDOFIN=oParentObject.w_DDOFIN
      .w_NEWSCA=oParentObject.w_NEWSCA
      .w_NEWAGE=oParentObject.w_NEWAGE
      .w_NEWPAR=oParentObject.w_NEWPAR
      .w_FLSALD=oParentObject.w_FLSALD
      .w_REGINI=oParentObject.w_REGINI
      .w_REGFIN=oParentObject.w_REGFIN
      .w_CODAGE=oParentObject.w_CODAGE
          .DoRTCalc(1,2,.f.)
        .w_ROWRIF = .w_ROWRIF
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_ROWRIF))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,12,.f.)
        .w_NEWSCA = .w_NEWSCA
          .DoRTCalc(14,14,.f.)
        .w_OBTEST = .w_SCAINI
          .DoRTCalc(16,16,.f.)
        .w_ZCODCON = .w_ZCODCON
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_ZCODCON))
          .link_1_17('Full')
        endif
          .DoRTCalc(18,18,.f.)
        .w_FLSELE = 0
        .w_SELEZI = 'D'
      .oPgFrm.Page1.oPag.CalcZoom.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .w_ZBANAPP = .w_ZBANAPP
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_ZBANAPP))
          .link_1_27('Full')
        endif
          .DoRTCalc(22,22,.f.)
        .w_ZBANNOS = .w_ZBANNOS
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_ZBANNOS))
          .link_1_29('Full')
        endif
          .DoRTCalc(24,24,.f.)
        .w_ZCODVAL = .w_ZCODVAL
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_ZCODVAL))
          .link_1_31('Full')
        endif
          .DoRTCalc(26,26,.f.)
        .w_CAOVAL = GETCAM(.w_ZCODVAL, i_DATSYS, 7)
          .DoRTCalc(28,28,.f.)
        .w_ZMODPAG = .w_ZMODPAG
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_ZMODPAG))
          .link_1_38('Full')
        endif
          .DoRTCalc(30,30,.f.)
        .w_ZCODAGE = .w_ZCODAGE
        .DoRTCalc(31,31,.f.)
        if not(empty(.w_ZCODAGE))
          .link_1_40('Full')
        endif
        .w_ZREGINI = .w_REGINI
        .w_ZREGFIN = .w_REGFIN
        .w_ZNDOFIN = .w_NDOFIN
        .w_ZADOINI = .w_ADOINI
        .w_ZADOFIN = .w_ADOFIN
        .w_ZDDOINI = .w_DDOINI
        .w_ZDDOFIN = .w_DDOFIN
        .w_ZNUMPAR = .w_NUMPAR
        .w_ZNDOINI = .w_NDOINI
        .w_ZSCAINI = .w_SCAINI
        .w_ZSCAFIN = .w_SCAFIN
      .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
          .DoRTCalc(43,43,.f.)
        .w_NBANAPP = .w_ZBANAPP
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_NBANAPP))
          .link_1_58('Full')
        endif
        .w_NBANNOS = .w_ZBANNOS
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_NBANNOS))
          .link_1_59('Full')
        endif
        .w_NMODPAG = .w_ZMODPAG
        .DoRTCalc(46,46,.f.)
        if not(empty(.w_NMODPAG))
          .link_1_60('Full')
        endif
          .DoRTCalc(47,48,.f.)
        .w_ZTIPRIF = IIF(.w_TIPRIF $ 'CF' AND NOT EMPTY(.w_CONRIF), .w_TIPRIF, 'X')
        .w_ZCONRIF = IIF(.w_TIPRIF $ 'CF' AND NOT EMPTY(.w_CONRIF), .w_CONRIF, 'zzzkkkzzzkkkzzz')
        .w_NCODAGE = .w_NEWAGE
        .DoRTCalc(51,51,.f.)
        if not(empty(.w_NCODAGE))
          .link_1_70('Full')
        endif
          .DoRTCalc(52,55,.f.)
        .w_NDATDOC = i_datsys
      .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
          .DoRTCalc(57,57,.f.)
        .w_TOTALE1 = .w_CalcZoom.getVar('TOTALE1')
        .DoRTCalc(59,64,.f.)
        if not(empty(.w_NNUMCOR))
          .link_1_94('Full')
        endif
          .DoRTCalc(65,70,.f.)
        .w_DASERIAL = '0000000001'
        .DoRTCalc(71,71,.f.)
        if not(empty(.w_DASERIAL))
          .link_1_102('Full')
        endif
          .DoRTCalc(72,77,.f.)
        .w_DATIAGG = .w_SERIALE
        .DoRTCalc(78,78,.f.)
        if not(empty(.w_DATIAGG))
          .link_1_109('Full')
        endif
    endwith
    this.DoRTCalc(79,79,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_81.enabled = this.oPgFrm.Page1.oPag.oBtn_1_81.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_83.enabled = this.oPgFrm.Page1.oPag.oBtn_1_83.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_85.enabled = this.oPgFrm.Page1.oPag.oBtn_1_85.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_SERIALE=.w_SERIALE
      .oParentObject.w_ROWORD=.w_ROWORD
      .oParentObject.w_ROWRIF=.w_ROWRIF
      .oParentObject.w_SCAINI=.w_SCAINI
      .oParentObject.w_SCAFIN=.w_SCAFIN
      .oParentObject.w_NUMPAR=.w_NUMPAR
      .oParentObject.w_NDOINI=.w_NDOINI
      .oParentObject.w_NDOFIN=.w_NDOFIN
      .oParentObject.w_ADOINI=.w_ADOINI
      .oParentObject.w_ADOFIN=.w_ADOFIN
      .oParentObject.w_DDOINI=.w_DDOINI
      .oParentObject.w_DDOFIN=.w_DDOFIN
      .oParentObject.w_NEWSCA=.w_NEWSCA
      .oParentObject.w_NEWAGE=.w_NEWAGE
      .oParentObject.w_NEWPAR=.w_NEWPAR
      .oParentObject.w_FLSALD=.w_FLSALD
      .oParentObject.w_REGINI=.w_REGINI
      .oParentObject.w_REGFIN=.w_REGFIN
      .oParentObject.w_CODAGE=.w_CODAGE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_SERIALE<>.w_SERIALE
          .link_1_3('Full')
        endif
        .DoRTCalc(4,16,.t.)
        if .o_ZTIPCON<>.w_ZTIPCON
          .link_1_17('Full')
        endif
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .DoRTCalc(18,26,.t.)
        if .o_ZCODVAL<>.w_ZCODVAL
            .w_CAOVAL = GETCAM(.w_ZCODVAL, i_DATSYS, 7)
        endif
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .DoRTCalc(28,48,.t.)
            .w_ZTIPRIF = IIF(.w_TIPRIF $ 'CF' AND NOT EMPTY(.w_CONRIF), .w_TIPRIF, 'X')
            .w_ZCONRIF = IIF(.w_TIPRIF $ 'CF' AND NOT EMPTY(.w_CONRIF), .w_CONRIF, 'zzzkkkzzzkkkzzz')
        if .o_NEWAGE<>.w_NEWAGE
            .w_NCODAGE = .w_NEWAGE
          .link_1_70('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .DoRTCalc(52,57,.t.)
            .w_TOTALE1 = .w_CalcZoom.getVar('TOTALE1')
        .DoRTCalc(59,63,.t.)
        if .o_NBANAPP<>.w_NBANAPP
          .link_1_94('Full')
        endif
        .DoRTCalc(65,70,.t.)
          .link_1_102('Full')
        .DoRTCalc(72,77,.t.)
            .w_DATIAGG = .w_SERIALE
          .link_1_109('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(79,79,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_56.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oZCODCON_1_17.enabled = this.oPgFrm.Page1.oPag.oZCODCON_1_17.mCond()
    this.oPgFrm.Page1.oPag.oZCODAGE_1_40.enabled = this.oPgFrm.Page1.oPag.oZCODAGE_1_40.mCond()
    this.oPgFrm.Page1.oPag.oNCODAGE_1_70.enabled = this.oPgFrm.Page1.oPag.oNCODAGE_1_70.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCAOVAL_1_33.visible=!this.oPgFrm.Page1.oPag.oCAOVAL_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_54.visible=!this.oPgFrm.Page1.oPag.oStr_1_54.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.CalcZoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_56.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_78.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ROWRIF
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_lTable = "PAR_TITE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2], .t., this.PAR_TITE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ROWRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ROWRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PTSERIAL,PTROWORD,CPROWNUM,PTTIPCON,PTCODCON,PTCODVAL,PTMODPAG,PTBANNOS,PTBANAPP,PTDESRIG,PTFLVABD,PTNUMCOR";
                   +" from "+i_cTable+" "+i_lTable+" where CPROWNUM="+cp_ToStrODBC(this.w_ROWRIF);
                   +" and PTSERIAL="+cp_ToStrODBC(this.w_SERIALE);
                   +" and PTROWORD="+cp_ToStrODBC(this.w_ROWORD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PTSERIAL',this.w_SERIALE;
                       ,'PTROWORD',this.w_ROWORD;
                       ,'CPROWNUM',this.w_ROWRIF)
            select PTSERIAL,PTROWORD,CPROWNUM,PTTIPCON,PTCODCON,PTCODVAL,PTMODPAG,PTBANNOS,PTBANAPP,PTDESRIG,PTFLVABD,PTNUMCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ROWRIF = NVL(_Link_.CPROWNUM,0)
      this.w_ZTIPCON = NVL(_Link_.PTTIPCON,space(1))
      this.w_ZCODCON = NVL(_Link_.PTCODCON,space(15))
      this.w_ZCODVAL = NVL(_Link_.PTCODVAL,space(3))
      this.w_ZMODPAG = NVL(_Link_.PTMODPAG,space(10))
      this.w_ZBANNOS = NVL(_Link_.PTBANNOS,space(15))
      this.w_ZBANAPP = NVL(_Link_.PTBANAPP,space(10))
      this.w_RIFRAG = NVL(_Link_.PTDESRIG,space(50))
      this.w_NFLVABD = NVL(_Link_.PTFLVABD,space(1))
      this.w_NNUMCOR = NVL(_Link_.PTNUMCOR,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_ROWRIF = 0
      endif
      this.w_ZTIPCON = space(1)
      this.w_ZCODCON = space(15)
      this.w_ZCODVAL = space(3)
      this.w_ZMODPAG = space(10)
      this.w_ZBANNOS = space(15)
      this.w_ZBANAPP = space(10)
      this.w_RIFRAG = space(50)
      this.w_NFLVABD = space(1)
      this.w_NNUMCOR = space(25)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])+'\'+cp_ToStr(_Link_.PTSERIAL,1)+'\'+cp_ToStr(_Link_.PTROWORD,1)+'\'+cp_ToStr(_Link_.CPROWNUM,1)
      cp_ShowWarn(i_cKey,this.PAR_TITE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ROWRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ZCODCON
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_ZCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ZTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPRIF,ANCONRIF,ANCODAG1,ANNUMCOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_ZTIPCON;
                     ,'ANCODICE',trim(this.w_ZCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPRIF,ANCONRIF,ANCODAG1,ANNUMCOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ZCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_ZCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ZTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPRIF,ANCONRIF,ANCODAG1,ANNUMCOR";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_ZCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_ZTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPRIF,ANCONRIF,ANCODAG1,ANNUMCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ZCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oZCODCON_1_17'),i_cWhere,'',"Clienti / fornitori /conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ZTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPRIF,ANCONRIF,ANCODAG1,ANNUMCOR";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPRIF,ANCONRIF,ANCODAG1,ANNUMCOR;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPRIF,ANCONRIF,ANCODAG1,ANNUMCOR";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_ZTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPRIF,ANCONRIF,ANCODAG1,ANNUMCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPRIF,ANCONRIF,ANCODAG1,ANNUMCOR";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_ZCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ZTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ZTIPCON;
                       ,'ANCODICE',this.w_ZCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPRIF,ANCONRIF,ANCODAG1,ANNUMCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPRIF = NVL(_Link_.ANTIPRIF,space(1))
      this.w_CONRIF = NVL(_Link_.ANCONRIF,space(15))
      this.w_NCODAGE = NVL(_Link_.ANCODAG1,space(5))
      this.w_NNUMCOR = NVL(_Link_.ANNUMCOR,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_ZCODCON = space(15)
      endif
      this.w_DESCRI = space(40)
      this.w_TIPRIF = space(1)
      this.w_CONRIF = space(15)
      this.w_NCODAGE = space(5)
      this.w_NNUMCOR = space(25)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ZBANAPP
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZBANAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_ZBANAPP)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_ZBANAPP))
          select BACODBAN,BADESBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ZBANAPP)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESBAN like "+cp_ToStrODBC(trim(this.w_ZBANAPP)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESBAN like "+cp_ToStr(trim(this.w_ZBANAPP)+"%");

            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ZBANAPP) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oZBANAPP_1_27'),i_cWhere,'GSAR_ABA',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZBANAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_ZBANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_ZBANAPP)
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZBANAPP = NVL(_Link_.BACODBAN,space(10))
      this.w_DESBAN = NVL(_Link_.BADESBAN,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ZBANAPP = space(10)
      endif
      this.w_DESBAN = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZBANAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ZBANNOS
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZBANNOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_ZBANNOS)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_ZBANNOS))
          select BACODBAN,BADESCRI,BADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ZBANNOS)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_ZBANNOS)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_ZBANNOS)+"%");

            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ZBANNOS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oZBANNOS_1_29'),i_cWhere,'GSTE_ACB',"Conti banca",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZBANNOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_ZBANNOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_ZBANNOS)
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZBANNOS = NVL(_Link_.BACODBAN,space(15))
      this.w_DESNOS = NVL(_Link_.BADESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ZBANNOS = space(15)
      endif
      this.w_DESNOS = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente oppure obsoleto")
        endif
        this.w_ZBANNOS = space(15)
        this.w_DESNOS = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZBANNOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ZCODVAL
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_ZCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_ZCODVAL))
          select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ZCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_ZCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_ZCODVAL)+"%");

            select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ZCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oZCODVAL_1_31'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_ZCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_ZCODVAL)
            select VACODVAL,VADESVAL,VACAOVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_CAMBIO = NVL(_Link_.VACAOVAL,0)
      this.w_ZCSIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ZCODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_CAMBIO = 0
      this.w_ZCSIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ZMODPAG
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
    i_lTable = "MOD_PAGA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2], .t., this.MOD_PAGA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZMODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMP',True,'MOD_PAGA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MPCODICE like "+cp_ToStrODBC(trim(this.w_ZMODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MPCODICE,MPDESCRI"+cp_TransInsFldName("MPDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MPCODICE',trim(this.w_ZMODPAG))
          select MPCODICE,MPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ZMODPAG)==trim(_Link_.MPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ZMODPAG) and !this.bDontReportError
            deferred_cp_zoom('MOD_PAGA','*','MPCODICE',cp_AbsName(oSource.parent,'oZMODPAG_1_38'),i_cWhere,'GSAR_AMP',"Tipi pagamento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MPDESCRI"+cp_TransInsFldName("MPDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',oSource.xKey(1))
            select MPCODICE,MPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZMODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MPDESCRI"+cp_TransInsFldName("MPDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(this.w_ZMODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',this.w_ZMODPAG)
            select MPCODICE,MPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZMODPAG = NVL(_Link_.MPCODICE,space(10))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.MPDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ZMODPAG = space(10)
      endif
      this.w_DESPAG = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])+'\'+cp_ToStr(_Link_.MPCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_PAGA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZMODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ZCODAGE
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_ZCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_ZCODAGE))
          select AGCODAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ZCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ZCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oZCODAGE_1_40'),i_cWhere,'GSAR_AGE',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_ZCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_ZCODAGE)
            select AGCODAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ZCODAGE = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice agente inesistente oppure obsoleto")
        endif
        this.w_ZCODAGE = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NBANAPP
  func Link_1_58(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NBANAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_NBANAPP)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_NBANAPP))
          select BACODBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NBANAPP)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"  like "+cp_ToStrODBC(trim(this.w_NBANAPP)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+"  like "+cp_ToStr(trim(this.w_NBANAPP)+"%");

            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NBANAPP) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oNBANAPP_1_58'),i_cWhere,'GSAR_ABA',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NBANAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_NBANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_NBANAPP)
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NBANAPP = NVL(_Link_.BACODBAN,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_NBANAPP = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NBANAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NBANNOS
  func Link_1_59(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NBANNOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_NBANNOS)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_NBANNOS))
          select BACODBAN,BADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NBANNOS)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADTOBSO like "+cp_ToStrODBC(trim(this.w_NBANNOS)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADTOBSO like "+cp_ToStr(trim(this.w_NBANNOS)+"%");

            select BACODBAN,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NBANNOS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oNBANNOS_1_59'),i_cWhere,'GSTE_ACB',"Conti banca",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NBANNOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_NBANNOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_NBANNOS)
            select BACODBAN,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NBANNOS = NVL(_Link_.BACODBAN,space(15))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NBANNOS = space(15)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente oppure obsoleto")
        endif
        this.w_NBANNOS = space(15)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NBANNOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NMODPAG
  func Link_1_60(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
    i_lTable = "MOD_PAGA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2], .t., this.MOD_PAGA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NMODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMP',True,'MOD_PAGA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MPCODICE like "+cp_ToStrODBC(trim(this.w_NMODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MPCODICE',trim(this.w_NMODPAG))
          select MPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NMODPAG)==trim(_Link_.MPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NMODPAG) and !this.bDontReportError
            deferred_cp_zoom('MOD_PAGA','*','MPCODICE',cp_AbsName(oSource.parent,'oNMODPAG_1_60'),i_cWhere,'GSAR_AMP',"Tipi pagamento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',oSource.xKey(1))
            select MPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NMODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(this.w_NMODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',this.w_NMODPAG)
            select MPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NMODPAG = NVL(_Link_.MPCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_NMODPAG = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])+'\'+cp_ToStr(_Link_.MPCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_PAGA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NMODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NCODAGE
  func Link_1_70(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_NCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_NCODAGE))
          select AGCODAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oNCODAGE_1_70'),i_cWhere,'',"Elenco agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_NCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_NCODAGE)
            select AGCODAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NCODAGE = NVL(_Link_.AGCODAGE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_NCODAGE = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NNUMCOR
  func Link_1_94(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CONTI_IDX,3]
    i_lTable = "BAN_CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2], .t., this.BAN_CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NNUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BAN_CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCONCOR like "+cp_ToStrODBC(trim(this.w_NNUMCOR)+"%");
                   +" and CCTIPCON="+cp_ToStrODBC(this.w_ZTIPCON);
                   +" and CCCODCON="+cp_ToStrODBC(this.w_ZCODCON);
                   +" and CCCODBAN="+cp_ToStrODBC(this.w_NBANAPP);

          i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCTIPCON',this.w_ZTIPCON;
                     ,'CCCODCON',this.w_ZCODCON;
                     ,'CCCODBAN',this.w_NBANAPP;
                     ,'CCCONCOR',trim(this.w_NNUMCOR))
          select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NNUMCOR)==trim(_Link_.CCCONCOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NNUMCOR) and !this.bDontReportError
            deferred_cp_zoom('BAN_CONTI','*','CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR',cp_AbsName(oSource.parent,'oNNUMCOR_1_94'),i_cWhere,'',"Elenco conti correnti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ZTIPCON<>oSource.xKey(1);
           .or. this.w_ZCODCON<>oSource.xKey(2);
           .or. this.w_NBANAPP<>oSource.xKey(3);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                     +" from "+i_cTable+" "+i_lTable+" where CCCONCOR="+cp_ToStrODBC(oSource.xKey(4));
                     +" and CCTIPCON="+cp_ToStrODBC(this.w_ZTIPCON);
                     +" and CCCODCON="+cp_ToStrODBC(this.w_ZCODCON);
                     +" and CCCODBAN="+cp_ToStrODBC(this.w_NBANAPP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPCON',oSource.xKey(1);
                       ,'CCCODCON',oSource.xKey(2);
                       ,'CCCODBAN',oSource.xKey(3);
                       ,'CCCONCOR',oSource.xKey(4))
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NNUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                   +" from "+i_cTable+" "+i_lTable+" where CCCONCOR="+cp_ToStrODBC(this.w_NNUMCOR);
                   +" and CCTIPCON="+cp_ToStrODBC(this.w_ZTIPCON);
                   +" and CCCODCON="+cp_ToStrODBC(this.w_ZCODCON);
                   +" and CCCODBAN="+cp_ToStrODBC(this.w_NBANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPCON',this.w_ZTIPCON;
                       ,'CCCODCON',this.w_ZCODCON;
                       ,'CCCODBAN',this.w_NBANAPP;
                       ,'CCCONCOR',this.w_NNUMCOR)
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NNUMCOR = NVL(_Link_.CCCONCOR,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_NNUMCOR = space(25)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPCON,1)+'\'+cp_ToStr(_Link_.CCCODCON,1)+'\'+cp_ToStr(_Link_.CCCODBAN,1)+'\'+cp_ToStr(_Link_.CCCONCOR,1)
      cp_ShowWarn(i_cKey,this.BAN_CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NNUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DASERIAL
  func Link_1_102(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DATI_AGG_IDX,3]
    i_lTable = "DATI_AGG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2], .t., this.DATI_AGG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DASERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DASERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DASERIAL,DACAM_01,DACAM_02,DACAM_03,DACAM_04,DACAM_05,DACAM_06";
                   +" from "+i_cTable+" "+i_lTable+" where DASERIAL="+cp_ToStrODBC(this.w_DASERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DASERIAL',this.w_DASERIAL)
            select DASERIAL,DACAM_01,DACAM_02,DACAM_03,DACAM_04,DACAM_05,DACAM_06;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DASERIAL = NVL(_Link_.DASERIAL,space(10))
      this.w_DACAM_01 = NVL(_Link_.DACAM_01,space(30))
      this.w_DACAM_02 = NVL(_Link_.DACAM_02,space(30))
      this.w_DACAM_03 = NVL(_Link_.DACAM_03,space(30))
      this.w_DACAM_04 = NVL(_Link_.DACAM_04,space(30))
      this.w_DACAM_05 = NVL(_Link_.DACAM_05,space(30))
      this.w_DACAM_06 = NVL(_Link_.DACAM_06,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_DASERIAL = space(10)
      endif
      this.w_DACAM_01 = space(30)
      this.w_DACAM_02 = space(30)
      this.w_DACAM_03 = space(30)
      this.w_DACAM_04 = space(30)
      this.w_DACAM_05 = space(30)
      this.w_DACAM_06 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2])+'\'+cp_ToStr(_Link_.DASERIAL,1)
      cp_ShowWarn(i_cKey,this.DATI_AGG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DASERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DATIAGG
  func Link_1_109(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
    i_lTable = "PNT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2], .t., this.PNT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DATIAGG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DATIAGG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PNSERIAL,PNAGG_01,PNAGG_02,PNAGG_03,PNAGG_04,PNAGG_05,PNAGG_06,PNNUMFAT";
                   +" from "+i_cTable+" "+i_lTable+" where PNSERIAL="+cp_ToStrODBC(this.w_DATIAGG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PNSERIAL',this.w_DATIAGG)
            select PNSERIAL,PNAGG_01,PNAGG_02,PNAGG_03,PNAGG_04,PNAGG_05,PNAGG_06,PNNUMFAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DATIAGG = NVL(_Link_.PNSERIAL,space(10))
      this.w_PNAGG_01 = NVL(_Link_.PNAGG_01,space(15))
      this.w_PNAGG_02 = NVL(_Link_.PNAGG_02,space(15))
      this.w_PNAGG_03 = NVL(_Link_.PNAGG_03,space(15))
      this.w_PNAGG_04 = NVL(_Link_.PNAGG_04,space(15))
      this.w_PNAGG_05 = NVL(cp_ToDate(_Link_.PNAGG_05),ctod("  /  /  "))
      this.w_PNAGG_06 = NVL(cp_ToDate(_Link_.PNAGG_06),ctod("  /  /  "))
      this.w_PNNUMFAT = NVL(_Link_.PNNUMFAT,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_DATIAGG = space(10)
      endif
      this.w_PNAGG_01 = space(15)
      this.w_PNAGG_02 = space(15)
      this.w_PNAGG_03 = space(15)
      this.w_PNAGG_04 = space(15)
      this.w_PNAGG_05 = ctod("  /  /  ")
      this.w_PNAGG_06 = ctod("  /  /  ")
      this.w_PNNUMFAT = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.PNSERIAL,1)
      cp_ShowWarn(i_cKey,this.PNT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DATIAGG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNEWSCA_1_13.value==this.w_NEWSCA)
      this.oPgFrm.Page1.oPag.oNEWSCA_1_13.value=this.w_NEWSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oZTIPCON_1_16.RadioValue()==this.w_ZTIPCON)
      this.oPgFrm.Page1.oPag.oZTIPCON_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oZCODCON_1_17.value==this.w_ZCODCON)
      this.oPgFrm.Page1.oPag.oZCODCON_1_17.value=this.w_ZCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_18.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_18.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_20.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oZBANAPP_1_27.value==this.w_ZBANAPP)
      this.oPgFrm.Page1.oPag.oZBANAPP_1_27.value=this.w_ZBANAPP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBAN_1_28.value==this.w_DESBAN)
      this.oPgFrm.Page1.oPag.oDESBAN_1_28.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oZBANNOS_1_29.value==this.w_ZBANNOS)
      this.oPgFrm.Page1.oPag.oZBANNOS_1_29.value=this.w_ZBANNOS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNOS_1_30.value==this.w_DESNOS)
      this.oPgFrm.Page1.oPag.oDESNOS_1_30.value=this.w_DESNOS
    endif
    if not(this.oPgFrm.Page1.oPag.oZCODVAL_1_31.value==this.w_ZCODVAL)
      this.oPgFrm.Page1.oPag.oZCODVAL_1_31.value=this.w_ZCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCAOVAL_1_33.value==this.w_CAOVAL)
      this.oPgFrm.Page1.oPag.oCAOVAL_1_33.value=this.w_CAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oZMODPAG_1_38.value==this.w_ZMODPAG)
      this.oPgFrm.Page1.oPag.oZMODPAG_1_38.value=this.w_ZMODPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAG_1_39.value==this.w_DESPAG)
      this.oPgFrm.Page1.oPag.oDESPAG_1_39.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oZCODAGE_1_40.value==this.w_ZCODAGE)
      this.oPgFrm.Page1.oPag.oZCODAGE_1_40.value=this.w_ZCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oZREGINI_1_41.value==this.w_ZREGINI)
      this.oPgFrm.Page1.oPag.oZREGINI_1_41.value=this.w_ZREGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oZREGFIN_1_42.value==this.w_ZREGFIN)
      this.oPgFrm.Page1.oPag.oZREGFIN_1_42.value=this.w_ZREGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNBANAPP_1_58.value==this.w_NBANAPP)
      this.oPgFrm.Page1.oPag.oNBANAPP_1_58.value=this.w_NBANAPP
    endif
    if not(this.oPgFrm.Page1.oPag.oNBANNOS_1_59.value==this.w_NBANNOS)
      this.oPgFrm.Page1.oPag.oNBANNOS_1_59.value=this.w_NBANNOS
    endif
    if not(this.oPgFrm.Page1.oPag.oNMODPAG_1_60.value==this.w_NMODPAG)
      this.oPgFrm.Page1.oPag.oNMODPAG_1_60.value=this.w_NMODPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oNCODAGE_1_70.value==this.w_NCODAGE)
      this.oPgFrm.Page1.oPag.oNCODAGE_1_70.value=this.w_NCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oRIFRAG_1_71.value==this.w_RIFRAG)
      this.oPgFrm.Page1.oPag.oRIFRAG_1_71.value=this.w_RIFRAG
    endif
    if not(this.oPgFrm.Page1.oPag.oNNUMDOC_1_72.value==this.w_NNUMDOC)
      this.oPgFrm.Page1.oPag.oNNUMDOC_1_72.value=this.w_NNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWPAR_1_73.value==this.w_NEWPAR)
      this.oPgFrm.Page1.oPag.oNEWPAR_1_73.value=this.w_NEWPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oNALFDOC_1_74.value==this.w_NALFDOC)
      this.oPgFrm.Page1.oPag.oNALFDOC_1_74.value=this.w_NALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oNDATDOC_1_75.value==this.w_NDATDOC)
      this.oPgFrm.Page1.oPag.oNDATDOC_1_75.value=this.w_NDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oNFLVABD_1_76.RadioValue()==this.w_NFLVABD)
      this.oPgFrm.Page1.oPag.oNFLVABD_1_76.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oZCSIMVAL_1_86.value==this.w_ZCSIMVAL)
      this.oPgFrm.Page1.oPag.oZCSIMVAL_1_86.value=this.w_ZCSIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oNNUMCOR_1_94.value==this.w_NNUMCOR)
      this.oPgFrm.Page1.oPag.oNNUMCOR_1_94.value=this.w_NNUMCOR
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_NEWSCA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNEWSCA_1_13.SetFocus()
            i_bnoObbl = !empty(.w_NEWSCA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ZTIPCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZTIPCON_1_16.SetFocus()
            i_bnoObbl = !empty(.w_ZTIPCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ZCODCON))  and (.w_ZTIPCON $ 'CF')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZCODCON_1_17.SetFocus()
            i_bnoObbl = !empty(.w_ZCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and not(empty(.w_ZBANNOS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZBANNOS_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente oppure obsoleto")
          case   (empty(.w_ZCODVAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZCODVAL_1_31.SetFocus()
            i_bnoObbl = !empty(.w_ZCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAOVAL))  and not(.w_CAMBIO<>0 OR EMPTY(.w_ZCODVAL))  and (.w_CAMBIO=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAOVAL_1_33.SetFocus()
            i_bnoObbl = !empty(.w_CAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and (.w_ZTIPCON='C')  and not(empty(.w_ZCODAGE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZCODAGE_1_40.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice agente inesistente oppure obsoleto")
          case   not((.w_ZREGINI<=.w_ZREGFIN) or (empty(.w_ZREGFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZREGINI_1_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data registrazione iniziale � maggiore della scadenza finale")
          case   not((.w_zregini<=.w_zregfin) or (empty(.w_zregini)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZREGFIN_1_42.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data registrazione iniziale � maggiore della scadenza finale")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and not(empty(.w_NBANNOS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNBANNOS_1_59.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente oppure obsoleto")
          case   (empty(.w_NMODPAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNMODPAG_1_60.SetFocus()
            i_bnoObbl = !empty(.w_NMODPAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NEWPAR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNEWPAR_1_73.SetFocus()
            i_bnoObbl = !empty(.w_NEWPAR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NDATDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNDATDOC_1_75.SetFocus()
            i_bnoObbl = !empty(.w_NDATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SERIALE = this.w_SERIALE
    this.o_NEWAGE = this.w_NEWAGE
    this.o_ZTIPCON = this.w_ZTIPCON
    this.o_ZCODVAL = this.w_ZCODVAL
    this.o_NBANAPP = this.w_NBANAPP
    return

enddefine

* --- Define pages as container
define class tgste_kusPag1 as StdContainer
  Width  = 818
  height = 502
  stdWidth  = 818
  stdheight = 502
  resizeXpos=560
  resizeYpos=373
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNEWSCA_1_13 as StdField with uid="WPZABKHXOG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_NEWSCA", cQueryName = "NEWSCA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data scadenza su cui accorpare le partite selezionate",;
    HelpContextID = 241269290,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=122, Top=30


  add object oZTIPCON_1_16 as StdCombo with uid="PJFYFYTHCJ",rtseq=16,rtrep=.f.,left=326,top=30,width=78,height=21;
    , ToolTipText = "Tipo conto di selezione";
    , HelpContextID = 6638186;
    , cFormVar="w_ZTIPCON",RowSource=""+"Cliente,"+"Fornitore", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oZTIPCON_1_16.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oZTIPCON_1_16.GetRadio()
    this.Parent.oContained.w_ZTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oZTIPCON_1_16.SetRadio()
    this.Parent.oContained.w_ZTIPCON=trim(this.Parent.oContained.w_ZTIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_ZTIPCON=='C',1,;
      iif(this.Parent.oContained.w_ZTIPCON=='F',2,;
      0))
  endfunc

  func oZTIPCON_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_ZCODCON)
        bRes2=.link_1_17('Full')
      endif
      if .not. empty(.w_NNUMCOR)
        bRes2=.link_1_94('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oZCODCON_1_17 as StdField with uid="JFFBAQAJYF",rtseq=17,rtrep=.f.,;
    cFormVar = "w_ZCODCON", cQueryName = "ZCODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente fornitore",;
    HelpContextID = 7404394,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=414, Top=30, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_ZTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_ZCODCON"

  func oZCODCON_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ZTIPCON $ 'CF')
    endwith
   endif
  endfunc

  func oZCODCON_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
      if .not. empty(.w_NNUMCOR)
        bRes2=.link_1_94('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oZCODCON_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oZCODCON_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_ZTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_ZTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oZCODCON_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti / fornitori /conti",'',this.parent.oContained
  endproc

  add object oDESCRI_1_18 as StdField with uid="XSUJZKTFRE",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 92388042,;
   bGlobalFont=.t.,;
    Height=21, Width=267, Left=545, Top=30, InputMask=replicate('X',40)

  add object oSELEZI_1_20 as StdRadio with uid="EBWPGKPETC",rtseq=20,rtrep=.f.,left=10, top=451, width=134,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_20.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 83896794
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 83896794
      this.Buttons(2).Top=15
      this.SetAll("Width",132)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_20.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_20.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object CalcZoom as cp_szoombox with uid="DYYDGSJXIR",left=3, top=251, width=809,height=197,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='PAR_TITE',cZoomFile='GSTE_KUS',bQueryOnLoad=.f.,bOptions=.F.,bAdvOptions=.F.,bReadOnly=.F.,;
    cEvent = "Accorpa",;
    nPag=1;
    , HelpContextID = 112317158


  add object oObj_1_22 as cp_runprogram with uid="SOMWCHUEEN",left=30, top=522, width=131,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSTE_BUS("CALC")',;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 112317158


  add object oObj_1_23 as cp_runprogram with uid="MYBCGXNZHO",left=164, top=522, width=181,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSTE_BUS("SELE")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 112317158

  add object oZBANAPP_1_27 as StdField with uid="YSITWNKSKA",rtseq=21,rtrep=.f.,;
    cFormVar = "w_ZBANAPP", cQueryName = "ZBANAPP",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Banca di appoggio del cliente/fornitore",;
    HelpContextID = 7873430,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=414, Top=55, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_ZBANAPP"

  func oZBANAPP_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oZBANAPP_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oZBANAPP_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oZBANAPP_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"Elenco banche",'',this.parent.oContained
  endproc
  proc oZBANAPP_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_ZBANAPP
     i_obj.ecpSave()
  endproc

  add object oDESBAN_1_28 as StdField with uid="AOXXGCEMNF",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 26393290,;
   bGlobalFont=.t.,;
    Height=21, Width=267, Left=545, Top=55, InputMask=replicate('X',50)

  add object oZBANNOS_1_29 as StdField with uid="AGXYNFCFDL",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ZBANNOS", cQueryName = "ZBANNOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente oppure obsoleto",;
    ToolTipText = "Nostro C/C. comunicato al fornitore per RB o al cliente per bonifico",;
    HelpContextID = 4727702,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=414, Top=76, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_ZBANNOS"

  func oZBANNOS_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oZBANNOS_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oZBANNOS_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oZBANNOS_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banca",'',this.parent.oContained
  endproc
  proc oZBANNOS_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_ZBANNOS
     i_obj.ecpSave()
  endproc

  add object oDESNOS_1_30 as StdField with uid="PPQDUASHBJ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESNOS", cQueryName = "DESNOS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 72959286,;
   bGlobalFont=.t.,;
    Height=21, Width=267, Left=545, Top=76, InputMask=replicate('X',35)

  add object oZCODVAL_1_31 as StdField with uid="GGGPBFUGRC",rtseq=25,rtrep=.f.,;
    cFormVar = "w_ZCODVAL", cQueryName = "ZCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta, se vuota tutte",;
    HelpContextID = 222362474,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=414, Top=97, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_ZCODVAL"

  func oZCODVAL_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oZCODVAL_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oZCODVAL_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oZCODVAL_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oZCODVAL_1_31.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_ZCODVAL
     i_obj.ecpSave()
  endproc

  add object oCAOVAL_1_33 as StdField with uid="BMRPLOILGG",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CAOVAL", cQueryName = "CAOVAL",enabled=.f.,;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio della valuta scadenze",;
    HelpContextID = 58654426,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=602, Top=97

  func oCAOVAL_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAMBIO=0)
    endwith
   endif
  endfunc

  func oCAOVAL_1_33.mHide()
    with this.Parent.oContained
      return (.w_CAMBIO<>0 OR EMPTY(.w_ZCODVAL))
    endwith
  endfunc

  add object oZMODPAG_1_38 as StdField with uid="YCZNWYIUZS",rtseq=29,rtrep=.f.,;
    cFormVar = "w_ZMODPAG", cQueryName = "ZMODPAG",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Tipo pagamento",;
    HelpContextID = 39784086,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=414, Top=119, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_PAGA", cZoomOnZoom="GSAR_AMP", oKey_1_1="MPCODICE", oKey_1_2="this.w_ZMODPAG"

  func oZMODPAG_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oZMODPAG_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oZMODPAG_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_PAGA','*','MPCODICE',cp_AbsName(this.parent,'oZMODPAG_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMP',"Tipi pagamento",'',this.parent.oContained
  endproc
  proc oZMODPAG_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MPCODICE=this.parent.oContained.w_ZMODPAG
     i_obj.ecpSave()
  endproc

  add object oDESPAG_1_39 as StdField with uid="CDJOQZXVIM",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 142916298,;
   bGlobalFont=.t.,;
    Height=21, Width=267, Left=545, Top=119, InputMask=replicate('X',35)

  add object oZCODAGE_1_40 as StdField with uid="HWIGIUUBCV",rtseq=31,rtrep=.f.,;
    cFormVar = "w_ZCODAGE", cQueryName = "ZCODAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice agente inesistente oppure obsoleto",;
    ToolTipText = "Codice agente",;
    HelpContextID = 124716182,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=414, Top=141, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_ZCODAGE"

  func oZCODAGE_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ZTIPCON='C')
    endwith
   endif
  endfunc

  func oZCODAGE_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oZCODAGE_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oZCODAGE_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oZCODAGE_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"",'',this.parent.oContained
  endproc
  proc oZCODAGE_1_40.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_ZCODAGE
     i_obj.ecpSave()
  endproc

  add object oZREGINI_1_41 as StdField with uid="VKYFJSOOKO",rtseq=32,rtrep=.f.,;
    cFormVar = "w_ZREGINI", cQueryName = "ZREGINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data registrazione iniziale � maggiore della scadenza finale",;
    ToolTipText = "Data registrazione iniziale della partita/scadenza",;
    HelpContextID = 17730666,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=735, Top=148

  func oZREGINI_1_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ZREGINI<=.w_ZREGFIN) or (empty(.w_ZREGFIN)))
    endwith
    return bRes
  endfunc

  add object oZREGFIN_1_42 as StdField with uid="HIALORWQIV",rtseq=33,rtrep=.f.,;
    cFormVar = "w_ZREGFIN", cQueryName = "ZREGFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data registrazione iniziale � maggiore della scadenza finale",;
    ToolTipText = "Data registrazione finale della partita/scadenza",;
    HelpContextID = 104762474,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=735, Top=170

  func oZREGFIN_1_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_zregini<=.w_zregfin) or (empty(.w_zregini)))
    endwith
    return bRes
  endfunc


  add object oObj_1_56 as cp_runprogram with uid="RSMVTMAYUQ",left=353, top=522, width=218,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSTE_BNS",;
    cEvent = "CALCZOOM row checked",;
    nPag=1;
    , HelpContextID = 112317158

  add object oNBANAPP_1_58 as StdField with uid="DPKVFEOVMF",rtseq=44,rtrep=.f.,;
    cFormVar = "w_NBANAPP", cQueryName = "NBANAPP",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Banca di appoggio del cliente/fornitore",;
    HelpContextID = 7873238,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=122, Top=55, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_NBANAPP"

  func oNBANAPP_1_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_58('Part',this)
      if .not. empty(.w_NNUMCOR)
        bRes2=.link_1_94('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oNBANAPP_1_58.ecpDrop(oSource)
    this.Parent.oContained.link_1_58('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNBANAPP_1_58.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oNBANAPP_1_58'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"Elenco banche",'',this.parent.oContained
  endproc
  proc oNBANAPP_1_58.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_NBANAPP
     i_obj.ecpSave()
  endproc

  add object oNBANNOS_1_59 as StdField with uid="LNTADDWOYB",rtseq=45,rtrep=.f.,;
    cFormVar = "w_NBANNOS", cQueryName = "NBANNOS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente oppure obsoleto",;
    ToolTipText = "Nostro C/C. comunicato al fornitore per RB o al cliente per bonifico",;
    HelpContextID = 4727510,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=122, Top=76, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_NBANNOS"

  func oNBANNOS_1_59.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_59('Part',this)
    endwith
    return bRes
  endfunc

  proc oNBANNOS_1_59.ecpDrop(oSource)
    this.Parent.oContained.link_1_59('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNBANNOS_1_59.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oNBANNOS_1_59'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banca",'',this.parent.oContained
  endproc
  proc oNBANNOS_1_59.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_NBANNOS
     i_obj.ecpSave()
  endproc

  add object oNMODPAG_1_60 as StdField with uid="WXWXGCQXMF",rtseq=46,rtrep=.f.,;
    cFormVar = "w_NMODPAG", cQueryName = "NMODPAG",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Tipo pagamento",;
    HelpContextID = 39783894,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=122, Top=119, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_PAGA", cZoomOnZoom="GSAR_AMP", oKey_1_1="MPCODICE", oKey_1_2="this.w_NMODPAG"

  func oNMODPAG_1_60.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_60('Part',this)
    endwith
    return bRes
  endfunc

  proc oNMODPAG_1_60.ecpDrop(oSource)
    this.Parent.oContained.link_1_60('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNMODPAG_1_60.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_PAGA','*','MPCODICE',cp_AbsName(this.parent,'oNMODPAG_1_60'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMP',"Tipi pagamento",'',this.parent.oContained
  endproc
  proc oNMODPAG_1_60.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MPCODICE=this.parent.oContained.w_NMODPAG
     i_obj.ecpSave()
  endproc

  add object oNCODAGE_1_70 as StdField with uid="BCPZDQSJZS",rtseq=51,rtrep=.f.,;
    cFormVar = "w_NCODAGE", cQueryName = "NCODAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente da assegnare alla partita riaperta",;
    HelpContextID = 124715990,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=122, Top=141, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_NCODAGE"

  func oNCODAGE_1_70.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ZTIPCON='C')
    endwith
   endif
  endfunc

  func oNCODAGE_1_70.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_70('Part',this)
    endwith
    return bRes
  endfunc

  proc oNCODAGE_1_70.ecpDrop(oSource)
    this.Parent.oContained.link_1_70('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNCODAGE_1_70.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oNCODAGE_1_70'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco agenti",'',this.parent.oContained
  endproc

  add object oRIFRAG_1_71 as StdField with uid="YVEFWPZUFY",rtseq=52,rtrep=.f.,;
    cFormVar = "w_RIFRAG", cQueryName = "RIFRAG",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Riferimenti raggruppamento",;
    HelpContextID = 142837226,;
   bGlobalFont=.t.,;
    Height=21, Width=368, Left=122, Top=163, InputMask=replicate('X',50)

  add object oNNUMDOC_1_72 as StdField with uid="XKOBIEZGUH",rtseq=53,rtrep=.f.,;
    cFormVar = "w_NNUMDOC", cQueryName = "NNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento da associare alla scadenza da raggruppare",;
    HelpContextID = 262696662,;
   bGlobalFont=.t.,;
    Height=21, Width=134, Left=122, Top=206, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxVlue = 999999999999999

  add object oNEWPAR_1_73 as StdField with uid="YSRCAZKKFD",rtseq=54,rtrep=.f.,;
    cFormVar = "w_NEWPAR", cQueryName = "NEWPAR",;
    bObbl = .t. , nPag = 1, value=space(31), bMultilanguage =  .f.,;
    HelpContextID = 41649622,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=122, Top=184, InputMask=replicate('X',31)

  add object oNALFDOC_1_74 as StdField with uid="GDOHOYQHBG",rtseq=55,rtrep=.f.,;
    cFormVar = "w_NALFDOC", cQueryName = "NALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento da associare alla scadenza da raggruppare",;
    HelpContextID = 262197718,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=269, Top=206, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oNDATDOC_1_75 as StdField with uid="STUNWXPZSC",rtseq=56,rtrep=.f.,;
    cFormVar = "w_NDATDOC", cQueryName = "NDATDOC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento da associare alla scadenza da raggruppare",;
    HelpContextID = 263070934,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=414, Top=206

  add object oNFLVABD_1_76 as StdCheck with uid="NOWQPHOLGG",rtseq=57,rtrep=.f.,left=122, top=230, caption="Beni deperibili",;
    ToolTipText = "Flag beni deperibili",;
    HelpContextID = 41998038,;
    cFormVar="w_NFLVABD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNFLVABD_1_76.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oNFLVABD_1_76.GetRadio()
    this.Parent.oContained.w_NFLVABD = this.RadioValue()
    return .t.
  endfunc

  func oNFLVABD_1_76.SetRadio()
    this.Parent.oContained.w_NFLVABD=trim(this.Parent.oContained.w_NFLVABD)
    this.value = ;
      iif(this.Parent.oContained.w_NFLVABD=='S',1,;
      0)
  endfunc


  add object oObj_1_78 as cp_runprogram with uid="BPVTDTPPOO",left=577, top=522, width=217,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSTE_BU1",;
    cEvent = "CALCZOOM row unchecked",;
    nPag=1;
    , HelpContextID = 112317158


  add object oBtn_1_81 as StdButton with uid="YLNTCJDSAX",left=763, top=200, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 111241750;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_81.Click()
      with this.Parent.oContained
        GSTE_BUS(this.Parent.oContained,"CALC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_83 as StdButton with uid="KMIOQVWMZO",left=713, top=452, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per raggruppare le scadenze selezionate";
    , HelpContextID = 65651738;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_83.Click()
      with this.Parent.oContained
        GSTE_BUS(this.Parent.oContained,"AGGIO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_85 as StdButton with uid="BSEPOHVZKE",left=763, top=452, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 58363066;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_85.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oZCSIMVAL_1_86 as StdField with uid="UMYMJFDXNT",rtseq=59,rtrep=.f.,;
    cFormVar = "w_ZCSIMVAL", cQueryName = "ZCSIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 120866018,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=467, Top=97, InputMask=replicate('X',5)

  add object oNNUMCOR_1_94 as StdField with uid="GAIUHHQBHQ",rtseq=64,rtrep=.f.,;
    cFormVar = "w_NNUMCOR", cQueryName = "NNUMCOR",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    HelpContextID = 261648086,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=122, Top=97, InputMask=replicate('X',25), bHasZoom = .t. , cLinkFile="BAN_CONTI", oKey_1_1="CCTIPCON", oKey_1_2="this.w_ZTIPCON", oKey_2_1="CCCODCON", oKey_2_2="this.w_ZCODCON", oKey_3_1="CCCODBAN", oKey_3_2="this.w_NBANAPP", oKey_4_1="CCCONCOR", oKey_4_2="this.w_NNUMCOR"

  func oNNUMCOR_1_94.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_94('Part',this)
    endwith
    return bRes
  endfunc

  proc oNNUMCOR_1_94.ecpDrop(oSource)
    this.Parent.oContained.link_1_94('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNNUMCOR_1_94.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BAN_CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_ZTIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODCON="+cp_ToStrODBC(this.Parent.oContained.w_ZCODCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODBAN="+cp_ToStrODBC(this.Parent.oContained.w_NBANAPP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPCON="+cp_ToStr(this.Parent.oContained.w_ZTIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODCON="+cp_ToStr(this.Parent.oContained.w_ZCODCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODBAN="+cp_ToStr(this.Parent.oContained.w_NBANAPP)
    endif
    do cp_zoom with 'BAN_CONTI','*','CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR',cp_AbsName(this.parent,'oNNUMCOR_1_94'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco conti correnti",'',this.parent.oContained
  endproc

  add object oStr_1_24 as StdString with uid="TWJTKCCZGP",Visible=.t., Left=312, Top=4,;
    Alignment=0, Width=113, Height=18,;
    Caption="Selezioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="SWSWHPDAOV",Visible=.t., Left=24, Top=30,;
    Alignment=1, Width=96, Height=18,;
    Caption="Data scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="RBIVGABTMX",Visible=.t., Left=48, Top=163,;
    Alignment=1, Width=72, Height=18,;
    Caption="Riferimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="APENMTGWKV",Visible=.t., Left=318, Top=97,;
    Alignment=1, Width=94, Height=15,;
    Caption="Codice valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="UTTBIGDRQK",Visible=.t., Left=328, Top=76,;
    Alignment=1, Width=84, Height=15,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="ZPGCYMLPSR",Visible=.t., Left=283, Top=55,;
    Alignment=1, Width=129, Height=15,;
    Caption="Banca di appoggio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="VDQPQEWGDZ",Visible=.t., Left=328, Top=119,;
    Alignment=1, Width=84, Height=15,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="IYJWGAVORA",Visible=.t., Left=509, Top=97,;
    Alignment=1, Width=91, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_54.mHide()
    with this.Parent.oContained
      return (.w_CAMBIO<>0 OR EMPTY(.w_ZCODVAL))
    endwith
  endfunc

  add object oStr_1_55 as StdString with uid="OEKVRVJREY",Visible=.t., Left=60, Top=184,;
    Alignment=1, Width=60, Height=15,;
    Caption="N.partita:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="YJQJBNFEKG",Visible=.t., Left=10, Top=4,;
    Alignment=0, Width=182, Height=18,;
    Caption="Raggruppa scadenze con"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_61 as StdString with uid="HMZWRNDKNJ",Visible=.t., Left=3, Top=55,;
    Alignment=1, Width=117, Height=15,;
    Caption="Banca di appoggio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="ZHTWVSXLCX",Visible=.t., Left=36, Top=76,;
    Alignment=1, Width=84, Height=15,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="EIESYAPTTN",Visible=.t., Left=40, Top=119,;
    Alignment=1, Width=80, Height=15,;
    Caption="Pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="GIYZBRFBGC",Visible=.t., Left=73, Top=141,;
    Alignment=1, Width=47, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_80 as StdString with uid="IELEUNOBUJ",Visible=.t., Left=35, Top=206,;
    Alignment=1, Width=85, Height=18,;
    Caption="N.documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="YZENFCQIGN",Visible=.t., Left=258, Top=206,;
    Alignment=2, Width=11, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_84 as StdString with uid="CHMLUZGCHI",Visible=.t., Left=377, Top=206,;
    Alignment=1, Width=35, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_91 as StdString with uid="PKQNAURQTT",Visible=.t., Left=371, Top=141,;
    Alignment=1, Width=41, Height=18,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_92 as StdString with uid="XVKRDOMNTT",Visible=.t., Left=635, Top=148,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_93 as StdString with uid="DPGAXRCCHO",Visible=.t., Left=656, Top=170,;
    Alignment=1, Width=75, Height=18,;
    Caption="A data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_95 as StdString with uid="FWNQQIDUMA",Visible=.t., Left=24, Top=97,;
    Alignment=1, Width=96, Height=18,;
    Caption="Conto corrente:"  ;
  , bGlobalFont=.t.

  add object oBox_1_64 as StdBox with uid="HFCTEHWJMU",left=4, top=22, width=248,height=1

  add object oBox_1_65 as StdBox with uid="PXLHHZSXBS",left=311, top=22, width=503,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_kus','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
