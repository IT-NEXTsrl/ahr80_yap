* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_btc                                                        *
*              Tracciabilità commessa PDA                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2016-09-02                                                      *
* Last revis.: 2016-09-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_TIPO,w_PDA,w_RIGAPDA
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_btc",oParentObject,m.w_TIPO,m.w_PDA,m.w_RIGAPDA)
return(i_retval)

define class tgsac_btc as StdBatch
  * --- Local variables
  w_TIPO = space(2)
  w_PDA = space(10)
  w_RIGAPDA = 0
  w_OBJECT = .NULL.
  w_PROG = .NULL.
  TMPc = space(10)
  w_OK = .f.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione GSAC_KTC - Tracciabilità commessa PDA
    do case
      case this.w_TIPO="MM"
        * --- Visualizzazione movimenti di magazzino
        this.w_OBJECT = GSMA_SZM()
        if !this.w_OBJECT.bSec1
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_CODART = this.oParentObject.w_MVCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODART")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        DO GSMA_BVM WITH this.w_OBJECT
        this.w_PROG = .null.
        this.w_OBJECT = .null.
      case this.w_TIPO="PD"
        * --- Gestione PDA
        this.w_OBJECT = GSAC_APD()
        this.w_OBJECT.w_PDSERIAL = this.w_PDA
        this.w_OBJECT.w_PDROWNUM = this.w_RIGAPDA
        this.TMPc = "PDSERIAL="+cp_ToStrODBC(this.w_PDA)+" and PDROWNUM="+cp_ToStrODBC(this.w_RIGAPDA)
        this.w_OK = TRUE
        if this.w_OK
          * --- Carica record e lascia in interrograzione ...
          this.w_OBJECT.QueryKeySet(this.TmpC,"")     
          this.w_OBJECT.LoadRecWarn()     
        endif
    endcase
  endproc


  proc Init(oParentObject,w_TIPO,w_PDA,w_RIGAPDA)
    this.w_TIPO=w_TIPO
    this.w_PDA=w_PDA
    this.w_RIGAPDA=w_RIGAPDA
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_TIPO,w_PDA,w_RIGAPDA"
endproc
