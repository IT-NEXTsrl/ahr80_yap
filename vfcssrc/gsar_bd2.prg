* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bd2                                                        *
*              Firma risorse umane                                             *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_25]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-27                                                      *
* Last revis.: 2011-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bd2",oParentObject,m.pPARAM)
return(i_retval)

define class tgsar_bd2 as StdBatch
  * --- Local variables
  pPARAM = space(3)
  w_ComodoPath = space(254)
  * --- WorkFile variables
  DIPENDEN_idx=0
  CPAR_DEF_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pPARAM == "INI"
        this.oParentObject.w_FIRMAIMG.CALCULATE(this.oParentObject.w_DP_FIRMA)     
      case this.pPARAM == "BUT"
        this.oParentObject.w_DP_FIRMA = GETFILE()
        this.oParentObject.w_FIRMAIMG.CALCULATE(this.oParentObject.w_DP_FIRMA)     
      case this.pPARAM == "PATH"
        this.w_ComodoPath = cp_GetDir(this.oParentObject.w_DP__PATH,ah_MsgFormat("Path per salvataggio files:"), ah_Msgformat("Selezionare path per salvataggio files"))
        if !EMPTY(this.w_ComodoPath)
          this.oParentObject.w_DP__PATH = this.w_ComodoPath
        endif
    endcase
  endproc


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DIPENDEN'
    this.cWorkTables[2]='CPAR_DEF'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
