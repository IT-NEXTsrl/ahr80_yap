* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mat                                                        *
*              Modello F24 sez. accise                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-04                                                      *
* Last revis.: 2010-01-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mat")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mat")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mat")
  return

* --- Class definition
define class tgscg_mat as StdPCForm
  Width  = 790
  Height = 175
  Top    = 3
  Left   = 5
  cComment = "Modello F24 sez. accise"
  cPrg = "gscg_mat"
  HelpContextID=130688361
  add object cnt as tcgscg_mat
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mat as PCContext
  w_AFTRIBUT = space(5)
  w_AFCODPRO = space(4)
  w_AF__MESE = space(2)
  w_AF__ANNO = space(4)
  w_AFCODICE = space(14)
  w_AFIMPORT = 0
  w_AFCODENT = space(1)
  w_AFSERIAL = space(10)
  w_DESCRIZI = space(10)
  w_CPROWNUM = 0
  w_TIPTRI = space(10)
  proc Save(i_oFrom)
    this.w_AFTRIBUT = i_oFrom.w_AFTRIBUT
    this.w_AFCODPRO = i_oFrom.w_AFCODPRO
    this.w_AF__MESE = i_oFrom.w_AF__MESE
    this.w_AF__ANNO = i_oFrom.w_AF__ANNO
    this.w_AFCODICE = i_oFrom.w_AFCODICE
    this.w_AFIMPORT = i_oFrom.w_AFIMPORT
    this.w_AFCODENT = i_oFrom.w_AFCODENT
    this.w_AFSERIAL = i_oFrom.w_AFSERIAL
    this.w_DESCRIZI = i_oFrom.w_DESCRIZI
    this.w_CPROWNUM = i_oFrom.w_CPROWNUM
    this.w_TIPTRI = i_oFrom.w_TIPTRI
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_AFTRIBUT = this.w_AFTRIBUT
    i_oTo.w_AFCODPRO = this.w_AFCODPRO
    i_oTo.w_AF__MESE = this.w_AF__MESE
    i_oTo.w_AF__ANNO = this.w_AF__ANNO
    i_oTo.w_AFCODICE = this.w_AFCODICE
    i_oTo.w_AFIMPORT = this.w_AFIMPORT
    i_oTo.w_AFCODENT = this.w_AFCODENT
    i_oTo.w_AFSERIAL = this.w_AFSERIAL
    i_oTo.w_DESCRIZI = this.w_DESCRIZI
    i_oTo.w_CPROWNUM = this.w_CPROWNUM
    i_oTo.w_TIPTRI = this.w_TIPTRI
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mat as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 790
  Height = 175
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-01-11"
  HelpContextID=130688361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  MODAPAG_IDX = 0
  ENT_ACCI_IDX = 0
  COD_TRIB_IDX = 0
  ENTI_COM_IDX = 0
  cFile = "MODAPAG"
  cKeySelect = "AFSERIAL"
  cKeyWhere  = "AFSERIAL=this.w_AFSERIAL"
  cKeyDetail  = "AFSERIAL=this.w_AFSERIAL and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"AFSERIAL="+cp_ToStrODBC(this.w_AFSERIAL)';

  cKeyDetailWhereODBC = '"AFSERIAL="+cp_ToStrODBC(this.w_AFSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';

  cKeyWhereODBCqualified = '"MODAPAG.AFSERIAL="+cp_ToStrODBC(this.w_AFSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gscg_mat"
  cComment = "Modello F24 sez. accise"
  i_nRowNum = 0
  i_nRowPerPage = 7
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AFTRIBUT = space(5)
  w_AFCODPRO = space(4)
  w_AF__MESE = space(2)
  w_AF__ANNO = space(4)
  w_AFCODICE = space(14)
  w_AFIMPORT = 0
  w_AFCODENT = space(1)
  w_AFSERIAL = space(10)
  w_DESCRIZI = space(10)
  w_CPROWNUM = 0
  w_TIPTRI = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_matPag1","gscg_mat",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='ENT_ACCI'
    this.cWorkTables[2]='COD_TRIB'
    this.cWorkTables[3]='ENTI_COM'
    this.cWorkTables[4]='MODAPAG'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MODAPAG_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MODAPAG_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_mat'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from MODAPAG where AFSERIAL=KeySet.AFSERIAL
    *                            and CPROWNUM=KeySet.CPROWNUM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.MODAPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODAPAG_IDX,2],this.bLoadRecFilter,this.MODAPAG_IDX,"gscg_mat")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MODAPAG')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MODAPAG.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MODAPAG '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AFSERIAL',this.w_AFSERIAL  )
      select * from (i_cTable) MODAPAG where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPTRI = space(10)
        .w_AFSERIAL = NVL(AFSERIAL,space(10))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        cp_LoadRecExtFlds(this,'MODAPAG')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESCRIZI = space(10)
          .w_AFTRIBUT = NVL(AFTRIBUT,space(5))
          if link_2_1_joined
            this.w_AFTRIBUT = NVL(TRCODICE201,NVL(this.w_AFTRIBUT,space(5)))
            this.w_TIPTRI = NVL(TRTIPTRI201,space(10))
          else
          .link_2_1('Load')
          endif
          .w_AFCODPRO = NVL(AFCODPRO,space(4))
          * evitabile
          *.link_2_2('Load')
          .w_AF__MESE = NVL(AF__MESE,space(2))
          .w_AF__ANNO = NVL(AF__ANNO,space(4))
          .w_AFCODICE = NVL(AFCODICE,space(14))
          .w_AFIMPORT = NVL(AFIMPORT,0)
          .w_AFCODENT = NVL(AFCODENT,space(1))
          .link_2_7('Load')
          .w_CPROWNUM = NVL(CPROWNUM,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with .w_CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_AFTRIBUT=space(5)
      .w_AFCODPRO=space(4)
      .w_AF__MESE=space(2)
      .w_AF__ANNO=space(4)
      .w_AFCODICE=space(14)
      .w_AFIMPORT=0
      .w_AFCODENT=space(1)
      .w_AFSERIAL=space(10)
      .w_DESCRIZI=space(10)
      .w_CPROWNUM=0
      .w_TIPTRI=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_AFTRIBUT))
         .link_2_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_AFCODPRO))
         .link_2_2('Full')
        endif
        .DoRTCalc(3,7,.f.)
        if not(empty(.w_AFCODENT))
         .link_2_7('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MODAPAG')
    this.DoRTCalc(8,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oObj_1_9.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'MODAPAG',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MODAPAG_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AFSERIAL,"AFSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_AFTRIBUT C(5);
      ,t_AFCODPRO C(4);
      ,t_AF__MESE C(2);
      ,t_AF__ANNO C(4);
      ,t_AFCODICE C(14);
      ,t_AFIMPORT N(15,2);
      ,t_AFCODENT C(1);
      ,t_DESCRIZI C(10);
      ,CPROWNUM N(4);
      ,t_CPROWNUM N(4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_matbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAFTRIBUT_2_1.controlsource=this.cTrsName+'.t_AFTRIBUT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAFCODPRO_2_2.controlsource=this.cTrsName+'.t_AFCODPRO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAF__MESE_2_3.controlsource=this.cTrsName+'.t_AF__MESE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAF__ANNO_2_4.controlsource=this.cTrsName+'.t_AF__ANNO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAFCODICE_2_5.controlsource=this.cTrsName+'.t_AFCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAFIMPORT_2_6.controlsource=this.cTrsName+'.t_AFIMPORT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAFCODENT_2_7.controlsource=this.cTrsName+'.t_AFCODENT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRIZI_2_9.controlsource=this.cTrsName+'.t_DESCRIZI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(73)
    this.AddVLine(309)
    this.AddVLine(374)
    this.AddVLine(432)
    this.AddVLine(554)
    this.AddVLine(584)
    this.AddVLine(629)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFTRIBUT_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MODAPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODAPAG_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MODAPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODAPAG_IDX,2])
      *
      * insert into MODAPAG
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MODAPAG')
        i_extval=cp_InsertValODBCExtFlds(this,'MODAPAG')
        i_cFldBody=" "+;
                  "(AFTRIBUT,AFCODPRO,AF__MESE,AF__ANNO,AFCODICE"+;
                  ",AFIMPORT,AFCODENT,AFSERIAL,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBCNull(this.w_AFTRIBUT)+","+cp_ToStrODBCNull(this.w_AFCODPRO)+","+cp_ToStrODBC(this.w_AF__MESE)+","+cp_ToStrODBC(this.w_AF__ANNO)+","+cp_ToStrODBC(this.w_AFCODICE)+;
             ","+cp_ToStrODBC(this.w_AFIMPORT)+","+cp_ToStrODBCNull(this.w_AFCODENT)+","+cp_ToStrODBC(this.w_AFSERIAL)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MODAPAG')
        i_extval=cp_InsertValVFPExtFlds(this,'MODAPAG')
        cp_CheckDeletedKey(i_cTable,0,'AFSERIAL',this.w_AFSERIAL,'CPROWNUM',this.w_CPROWNUM)
        INSERT INTO (i_cTable) (;
                   AFTRIBUT;
                  ,AFCODPRO;
                  ,AF__MESE;
                  ,AF__ANNO;
                  ,AFCODICE;
                  ,AFIMPORT;
                  ,AFCODENT;
                  ,AFSERIAL;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_AFTRIBUT;
                  ,this.w_AFCODPRO;
                  ,this.w_AF__MESE;
                  ,this.w_AF__ANNO;
                  ,this.w_AFCODICE;
                  ,this.w_AFIMPORT;
                  ,this.w_AFCODENT;
                  ,this.w_AFSERIAL;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.MODAPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODAPAG_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY (t_AFCODENT)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'MODAPAG')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'MODAPAG')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY (t_AFCODENT)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace CPROWNUM with this.w_CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MODAPAG
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'MODAPAG')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " AFTRIBUT="+cp_ToStrODBCNull(this.w_AFTRIBUT)+;
                     ",AFCODPRO="+cp_ToStrODBCNull(this.w_AFCODPRO)+;
                     ",AF__MESE="+cp_ToStrODBC(this.w_AF__MESE)+;
                     ",AF__ANNO="+cp_ToStrODBC(this.w_AF__ANNO)+;
                     ",AFCODICE="+cp_ToStrODBC(this.w_AFCODICE)+;
                     ",AFIMPORT="+cp_ToStrODBC(this.w_AFIMPORT)+;
                     ",AFCODENT="+cp_ToStrODBCNull(this.w_AFCODENT)+;
                     ",CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and CPROWNUM="+cp_ToStrODBC(CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'MODAPAG')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      AFTRIBUT=this.w_AFTRIBUT;
                     ,AFCODPRO=this.w_AFCODPRO;
                     ,AF__MESE=this.w_AF__MESE;
                     ,AF__ANNO=this.w_AF__ANNO;
                     ,AFCODICE=this.w_AFCODICE;
                     ,AFIMPORT=this.w_AFIMPORT;
                     ,AFCODENT=this.w_AFCODENT;
                     ,CPROWNUM=this.w_CPROWNUM;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MODAPAG_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MODAPAG_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY (t_AFCODENT)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete MODAPAG
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY (t_AFCODENT)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MODAPAG_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MODAPAG_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_CPROWNUM with this.w_CPROWNUM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AFTRIBUT
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_TRIB_IDX,3]
    i_lTable = "COD_TRIB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2], .t., this.COD_TRIB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AFTRIBUT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ATR',True,'COD_TRIB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODICE like "+cp_ToStrODBC(trim(this.w_AFTRIBUT)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODICE',trim(this.w_AFTRIBUT))
          select TRCODICE,TRTIPTRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AFTRIBUT)==trim(_Link_.TRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AFTRIBUT) and !this.bDontReportError
            deferred_cp_zoom('COD_TRIB','*','TRCODICE',cp_AbsName(oSource.parent,'oAFTRIBUT_2_1'),i_cWhere,'GSCG_ATR',"Codici tributi",'ACCISE.COD_TRIB_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',oSource.xKey(1))
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AFTRIBUT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODICE,TRTIPTRI";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODICE="+cp_ToStrODBC(this.w_AFTRIBUT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODICE',this.w_AFTRIBUT)
            select TRCODICE,TRTIPTRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AFTRIBUT = NVL(_Link_.TRCODICE,space(5))
      this.w_TIPTRI = NVL(_Link_.TRTIPTRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_AFTRIBUT = space(5)
      endif
      this.w_TIPTRI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPTRI='Accise'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un tributo di tipologia accise")
        endif
        this.w_AFTRIBUT = space(5)
        this.w_TIPTRI = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])+'\'+cp_ToStr(_Link_.TRCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_TRIB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AFTRIBUT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_TRIB_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_TRIB_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.TRCODICE as TRCODICE201"+ ",link_2_1.TRTIPTRI as TRTIPTRI201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on MODAPAG.AFTRIBUT=link_2_1.TRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and MODAPAG.AFTRIBUT=link_2_1.TRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AFCODPRO
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENTI_COM_IDX,3]
    i_lTable = "ENTI_COM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2], .t., this.ENTI_COM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AFCODPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AEC',True,'ENTI_COM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ECCODICE like "+cp_ToStrODBC(trim(this.w_AFCODPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select ECCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ECCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ECCODICE',trim(this.w_AFCODPRO))
          select ECCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ECCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AFCODPRO)==trim(_Link_.ECCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AFCODPRO) and !this.bDontReportError
            deferred_cp_zoom('ENTI_COM','*','ECCODICE',cp_AbsName(oSource.parent,'oAFCODPRO_2_2'),i_cWhere,'GSCG_AEC',"Province",'PROVINCE.ENTI_COM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',oSource.xKey(1))
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AFCODPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ECCODICE="+cp_ToStrODBC(this.w_AFCODPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCODICE',this.w_AFCODPRO)
            select ECCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AFCODPRO = NVL(_Link_.ECCODICE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_AFCODPRO = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENTI_COM_IDX,2])+'\'+cp_ToStr(_Link_.ECCODICE,1)
      cp_ShowWarn(i_cKey,this.ENTI_COM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AFCODPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AFCODENT
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENT_ACCI_IDX,3]
    i_lTable = "ENT_ACCI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_ACCI_IDX,2], .t., this.ENT_ACCI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_ACCI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AFCODENT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_AAI',True,'ENT_ACCI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ECCOENTE like "+cp_ToStrODBC(trim(this.w_AFCODENT)+"%");

          i_ret=cp_SQL(i_nConn,"select ECCOENTE,ECDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ECCOENTE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ECCOENTE',trim(this.w_AFCODENT))
          select ECCOENTE,ECDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ECCOENTE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AFCODENT)==trim(_Link_.ECCOENTE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AFCODENT) and !this.bDontReportError
            deferred_cp_zoom('ENT_ACCI','*','ECCOENTE',cp_AbsName(oSource.parent,'oAFCODENT_2_7'),i_cWhere,'GSCG_AAI',"Enti accise",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCOENTE,ECDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ECCOENTE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCOENTE',oSource.xKey(1))
            select ECCOENTE,ECDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AFCODENT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ECCOENTE,ECDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ECCOENTE="+cp_ToStrODBC(this.w_AFCODENT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ECCOENTE',this.w_AFCODENT)
            select ECCOENTE,ECDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AFCODENT = NVL(_Link_.ECCOENTE,space(1))
      this.w_DESCRIZI = NVL(_Link_.ECDESCRI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_AFCODENT = space(1)
      endif
      this.w_DESCRIZI = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENT_ACCI_IDX,2])+'\'+cp_ToStr(_Link_.ECCOENTE,1)
      cp_ShowWarn(i_cKey,this.ENT_ACCI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AFCODENT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFTRIBUT_2_1.value==this.w_AFTRIBUT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFTRIBUT_2_1.value=this.w_AFTRIBUT
      replace t_AFTRIBUT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFTRIBUT_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFCODPRO_2_2.value==this.w_AFCODPRO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFCODPRO_2_2.value=this.w_AFCODPRO
      replace t_AFCODPRO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFCODPRO_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAF__MESE_2_3.value==this.w_AF__MESE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAF__MESE_2_3.value=this.w_AF__MESE
      replace t_AF__MESE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAF__MESE_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAF__ANNO_2_4.value==this.w_AF__ANNO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAF__ANNO_2_4.value=this.w_AF__ANNO
      replace t_AF__ANNO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAF__ANNO_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFCODICE_2_5.value==this.w_AFCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFCODICE_2_5.value=this.w_AFCODICE
      replace t_AFCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFCODICE_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFIMPORT_2_6.value==this.w_AFIMPORT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFIMPORT_2_6.value=this.w_AFIMPORT
      replace t_AFIMPORT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFIMPORT_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFCODENT_2_7.value==this.w_AFCODENT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFCODENT_2_7.value=this.w_AFCODENT
      replace t_AFCODENT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFCODENT_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRIZI_2_9.value==this.w_DESCRIZI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRIZI_2_9.value=this.w_DESCRIZI
      replace t_DESCRIZI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRIZI_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'MODAPAG')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_TIPTRI='Accise') and not(empty(.w_AFTRIBUT)) and (NOT EMPTY (.w_AFCODENT))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAFTRIBUT_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Selezionare un tributo di tipologia accise")
        case   not(val (.w_AF__MESE) <13 AND val (.w_AF__MESE) >0) and (NOT EMPTY (.w_AFCODENT))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAF__MESE_2_3
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire un numero di mese compreso tra 1 e 12")
      endcase
      if NOT EMPTY (.w_AFCODENT)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY (t_AFCODENT))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_AFTRIBUT=space(5)
      .w_AFCODPRO=space(4)
      .w_AF__MESE=space(2)
      .w_AF__ANNO=space(4)
      .w_AFCODICE=space(14)
      .w_AFIMPORT=0
      .w_AFCODENT=space(1)
      .w_DESCRIZI=space(10)
      .w_CPROWNUM=0
      .DoRTCalc(1,1,.f.)
      if not(empty(.w_AFTRIBUT))
        .link_2_1('Full')
      endif
      .DoRTCalc(2,2,.f.)
      if not(empty(.w_AFCODPRO))
        .link_2_2('Full')
      endif
      .DoRTCalc(3,7,.f.)
      if not(empty(.w_AFCODENT))
        .link_2_7('Full')
      endif
    endwith
    this.DoRTCalc(8,11,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_AFTRIBUT = t_AFTRIBUT
    this.w_AFCODPRO = t_AFCODPRO
    this.w_AF__MESE = t_AF__MESE
    this.w_AF__ANNO = t_AF__ANNO
    this.w_AFCODICE = t_AFCODICE
    this.w_AFIMPORT = t_AFIMPORT
    this.w_AFCODENT = t_AFCODENT
    this.w_DESCRIZI = t_DESCRIZI
    this.w_CPROWNUM = t_CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_AFTRIBUT with this.w_AFTRIBUT
    replace t_AFCODPRO with this.w_AFCODPRO
    replace t_AF__MESE with this.w_AF__MESE
    replace t_AF__ANNO with this.w_AF__ANNO
    replace t_AFCODICE with this.w_AFCODICE
    replace t_AFIMPORT with this.w_AFIMPORT
    replace t_AFCODENT with this.w_AFCODENT
    replace t_DESCRIZI with this.w_DESCRIZI
    replace t_CPROWNUM with this.w_CPROWNUM
    if i_srv='A'
      replace CPROWNUM with this.w_CPROWNUM
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_matPag1 as StdContainer
  Width  = 786
  height = 175
  stdWidth  = 786
  stdheight = 175
  resizeXpos=521
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_9 as cp_runprogram with uid="HGINMBBZZD",left=1, top=178, width=314,height=34,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BTO('C')",;
    cEvent = "Delete row end, w_AFIMPORT Changed,Load",;
    nPag=1;
    , HelpContextID = 47309286

  add object oStr_1_1 as StdString with uid="RNHYUHRTYE",Visible=.t., Left=311, Top=15,;
    Alignment=0, Width=47, Height=18,;
    Caption="Tributo"  ;
  , bGlobalFont=.t.

  add object oStr_1_2 as StdString with uid="PMGBFEYKWQ",Visible=.t., Left=376, Top=2,;
    Alignment=0, Width=58, Height=15,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_1_3 as StdString with uid="IENLGGSGGW",Visible=.t., Left=561, Top=15,;
    Alignment=0, Width=53, Height=18,;
    Caption="MM/aaaa"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="KHCCEYGDVC",Visible=.t., Left=437, Top=15,;
    Alignment=0, Width=76, Height=18,;
    Caption="Codice"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="CEDVKCYBYT",Visible=.t., Left=7, Top=15,;
    Alignment=0, Width=69, Height=18,;
    Caption="Codice ente"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="SZBFTIRMDH",Visible=.t., Left=631, Top=15,;
    Alignment=0, Width=149, Height=18,;
    Caption="Importo a debito versati"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="QYJZSVVYEM",Visible=.t., Left=77, Top=15,;
    Alignment=0, Width=79, Height=18,;
    Caption="Descrizione"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="GJJXCGVAPL",Visible=.t., Left=376, Top=16,;
    Alignment=0, Width=29, Height=15,;
    Caption="prov."  ;
  , bGlobalFont=.t.

  add object oBox_1_11 as StdBox with uid="SAMBLIQYVJ",left=5, top=1, width=777,height=32

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=33,;
    width=768+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=34,width=767+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*7*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='COD_TRIB|ENTI_COM|ENT_ACCI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='COD_TRIB'
        oDropInto=this.oBodyCol.oRow.oAFTRIBUT_2_1
      case cFile='ENTI_COM'
        oDropInto=this.oBodyCol.oRow.oAFCODPRO_2_2
      case cFile='ENT_ACCI'
        oDropInto=this.oBodyCol.oRow.oAFCODENT_2_7
    endcase
    return(oDropInto)
  EndFunc


  add object oBox_2_11 as StdBox with uid="HTRENHOWQX",left=73, top=1, width=2,height=167

  add object oBox_2_12 as StdBox with uid="RLFLQXNFSG",left=308, top=1, width=2,height=167

  add object oBox_2_13 as StdBox with uid="ZJKEYXGPIZ",left=374, top=1, width=2,height=167

  add object oBox_2_14 as StdBox with uid="LBNXPWFNXJ",left=432, top=1, width=2,height=167

  add object oBox_2_15 as StdBox with uid="XBFWESZYIM",left=554, top=1, width=2,height=167

  add object oBox_2_16 as StdBox with uid="AKJALDZHMT",left=628, top=1, width=2,height=167

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_matBodyRow as CPBodyRowCnt
  Width=758
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oAFTRIBUT_2_1 as StdTrsField with uid="MGYTBTTKPC",rtseq=1,rtrep=.t.,;
    cFormVar="w_AFTRIBUT",value=space(5),;
    ToolTipText = "Codice tributo",;
    HelpContextID = 253584730,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un tributo di tipologia accise",;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=301, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_TRIB", cZoomOnZoom="GSCG_ATR", oKey_1_1="TRCODICE", oKey_1_2="this.w_AFTRIBUT"

  func oAFTRIBUT_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oAFTRIBUT_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oAFTRIBUT_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_TRIB','*','TRCODICE',cp_AbsName(this.parent,'oAFTRIBUT_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ATR',"Codici tributi",'ACCISE.COD_TRIB_VZM',this.parent.oContained
  endproc
  proc oAFTRIBUT_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ATR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODICE=this.parent.oContained.w_AFTRIBUT
    i_obj.ecpSave()
  endproc

  add object oAFCODPRO_2_2 as StdTrsField with uid="SXFRLDCFJJ",rtseq=2,rtrep=.t.,;
    cFormVar="w_AFCODPRO",value=space(4),;
    ToolTipText = "Codice provincia",;
    HelpContextID = 214521173,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=366, Top=0, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ENTI_COM", cZoomOnZoom="GSCG_AEC", oKey_1_1="ECCODICE", oKey_1_2="this.w_AFCODPRO"

  func oAFCODPRO_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oAFCODPRO_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oAFCODPRO_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENTI_COM','*','ECCODICE',cp_AbsName(this.parent,'oAFCODPRO_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AEC',"Province",'PROVINCE.ENTI_COM_VZM',this.parent.oContained
  endproc
  proc oAFCODPRO_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AEC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ECCODICE=this.parent.oContained.w_AFCODPRO
    i_obj.ecpSave()
  endproc

  add object oAF__MESE_2_3 as StdTrsField with uid="SMLWGFEGJQ",rtseq=3,rtrep=.t.,;
    cFormVar="w_AF__MESE",value=space(2),;
    ToolTipText = "Mese (mm)",;
    HelpContextID = 40572235,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un numero di mese compreso tra 1 e 12",;
   bGlobalFont=.t.,;
    Height=17, Width=27, Left=546, Top=0, cSayPict=["99"], cGetPict=["99"], InputMask=replicate('X',2)

  func oAF__MESE_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (val (.w_AF__MESE) <13 AND val (.w_AF__MESE) >0)
    endwith
    return bRes
  endfunc

  add object oAF__ANNO_2_4 as StdTrsField with uid="PXXDSDIKQW",rtseq=4,rtrep=.t.,;
    cFormVar="w_AF__ANNO",value=space(4),;
    ToolTipText = "Anno (aaaa)",;
    HelpContextID = 89451179,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=576, Top=0, cSayPict=["9999"], cGetPict=["9999"], InputMask=replicate('X',4)

  add object oAFCODICE_2_5 as StdTrsField with uid="NHERZPRTYV",rtseq=5,rtrep=.t.,;
    cFormVar="w_AFCODICE",value=space(14),;
    ToolTipText = "Codice identificativo",;
    HelpContextID = 97080651,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=424, Top=0, InputMask=replicate('X',14)

  add object oAFIMPORT_2_6 as StdTrsField with uid="IKKIVCVKOK",rtseq=6,rtrep=.t.,;
    cFormVar="w_AFIMPORT",value=0,;
    ToolTipText = "Importo a debito versato",;
    HelpContextID = 210220378,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=132, Left=621, Top=0, cSayPict=['@Z '+ v_PV(76)], cGetPict=['@Z '+ v_GV(76)]

  add object oAFCODENT_2_7 as StdTrsField with uid="NVGIZDMRMS",rtseq=7,rtrep=.t.,;
    cFormVar="w_AFCODENT",value=space(1),;
    ToolTipText = "Codice ente",;
    HelpContextID = 238463654,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=61, Left=-2, Top=0, InputMask=replicate('X',1), bHasZoom = .t. , cLinkFile="ENT_ACCI", cZoomOnZoom="GSCG_AAI", oKey_1_1="ECCOENTE", oKey_1_2="this.w_AFCODENT"

  func oAFCODENT_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oAFCODENT_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oAFCODENT_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ENT_ACCI','*','ECCOENTE',cp_AbsName(this.parent,'oAFCODENT_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_AAI',"Enti accise",'',this.parent.oContained
  endproc
  proc oAFCODENT_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSCG_AAI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ECCOENTE=this.parent.oContained.w_AFCODENT
    i_obj.ecpSave()
  endproc

  add object oDESCRIZI_2_9 as StdTrsField with uid="GFLYOJQLHV",rtseq=9,rtrep=.t.,;
    cFormVar="w_DESCRIZI",value=space(10),enabled=.f.,;
    ToolTipText = "Descrizione ente",;
    HelpContextID = 157395841,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=230, Left=67, Top=0, InputMask=replicate('X',10)
  add object oLast as LastKeyMover
  * ---
  func oAFTRIBUT_2_1.When()
    return(.t.)
  proc oAFTRIBUT_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oAFTRIBUT_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=6
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mat','MODAPAG','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AFSERIAL=MODAPAG.AFSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
