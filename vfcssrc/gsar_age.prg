* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_age                                                        *
*              Agenti                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_62]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2017-07-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_age"))

* --- Class definition
define class tgsar_age as StdForm
  Top    = 14
  Left   = 17

  * --- Standard Properties
  Width  = 585
  Height = 526+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-07-13"
  HelpContextID=42563433
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=49

  * --- Constant Properties
  AGENTI_IDX = 0
  GRUPRO_IDX = 0
  CONTI_IDX = 0
  NUMAUT_M_IDX = 0
  cFile = "AGENTI"
  cKeySelect = "AGCODAGE"
  cKeyWhere  = "AGCODAGE=this.w_AGCODAGE"
  cKeyWhereODBC = '"AGCODAGE="+cp_ToStrODBC(this.w_AGCODAGE)';

  cKeyWhereODBCqualified = '"AGENTI.AGCODAGE="+cp_ToStrODBC(this.w_AGCODAGE)';

  cPrg = "gsar_age"
  cComment = "Agenti"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0AGE'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AUTOAZI = space(5)
  w_AUTO = space(1)
  o_AUTO = space(1)
  w_TIPCON = space(1)
  w_AGCODAGE = space(5)
  o_AGCODAGE = space(5)
  w_AGDESAGE = space(35)
  w_AGINDAGE = space(25)
  w_AGFLGCPZ = space(1)
  w_FLGCPZ = space(1)
  w_AGAGECAP = space(8)
  w_AGCAPAGE = space(5)
  w_AGCITAGE = space(30)
  w_AGPROAGE = space(2)
  w_AGFISAGE = space(16)
  w_AGTELFAX = space(18)
  w_AGTELEFO = space(18)
  w_AG_EMAIL = space(254)
  w_AG_EMPEC = space(254)
  w_AGCATPRO = space(5)
  w_DESGPP = space(35)
  w_AGCODENA = space(15)
  w_AGTIPAGE = space(1)
  o_AGTIPAGE = space(1)
  w_AGCZOAGE = space(5)
  w_DESAGE = space(35)
  w_AGCODFOR = space(15)
  w_DESCRI = space(40)
  w_AGFLAZIE = space(1)
  w_AGFLESCL = space(1)
  w_AGFLFARI = space(1)
  w_AGSCOPAG = space(1)
  w_AGFLFIRR = space(1)
  w_AGRITIRP = 0
  o_AGRITIRP = 0
  w_AGPERIMP = 0
  w_AGINIENA = ctod('  /  /  ')
  w_AGFINENA = ctod('  /  /  ')
  w_FLPRO = space(1)
  w_TIPAGE = space(1)
  w_AGCHKSTA = space(1)
  w_AGCHKMAI = space(1)
  w_AGCHKPEC = space(1)
  w_AGCHKFAX = space(1)
  w_AGCHKCPZ = space(1)
  w_AGCHKPTL = space(1)
  w_AGDTINIT = ctod('  /  /  ')
  w_AGDTOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPCLFAN = space(1)
  w_OLDTOBSO = ctod('  /  /  ')
  w_CHKCODENA = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'AGENTI','gsar_age')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_agePag1","gsar_age",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Agente")
      .Pages(1).HelpContextID = 170607622
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAGCODAGE_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='GRUPRO'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='NUMAUT_M'
    this.cWorkTables[4]='AGENTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.AGENTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.AGENTI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_AGCODAGE = NVL(AGCODAGE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from AGENTI where AGCODAGE=KeySet.AGCODAGE
    *
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('AGENTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "AGENTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' AGENTI '
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AGCODAGE',this.w_AGCODAGE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AUTOAZI = i_CODAZI
        .w_AUTO = space(1)
        .w_DESGPP = space(35)
        .w_DESAGE = space(35)
        .w_DESCRI = space(40)
        .w_FLPRO = space(1)
        .w_TIPAGE = 'C'
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_TIPCLFAN = 'A'
        .w_OLDTOBSO = ctod("  /  /  ")
        .w_CHKCODENA = .f.
          .link_1_1('Load')
        .w_TIPCON = 'F'
        .w_AGCODAGE = NVL(AGCODAGE,space(5))
        .w_AGDESAGE = NVL(AGDESAGE,space(35))
        .w_AGINDAGE = NVL(AGINDAGE,space(25))
        .w_AGFLGCPZ = NVL(AGFLGCPZ,space(1))
        .w_FLGCPZ = .w_AGFLGCPZ
        .w_AGAGECAP = NVL(AGAGECAP,space(8))
        .w_AGCAPAGE = NVL(AGCAPAGE,space(5))
        .w_AGCITAGE = NVL(AGCITAGE,space(30))
        .w_AGPROAGE = NVL(AGPROAGE,space(2))
        .w_AGFISAGE = NVL(AGFISAGE,space(16))
        .w_AGTELFAX = NVL(AGTELFAX,space(18))
        .w_AGTELEFO = NVL(AGTELEFO,space(18))
        .w_AG_EMAIL = NVL(AG_EMAIL,space(254))
        .w_AG_EMPEC = NVL(AG_EMPEC,space(254))
        .w_AGCATPRO = NVL(AGCATPRO,space(5))
          if link_1_18_joined
            this.w_AGCATPRO = NVL(GPCODICE118,NVL(this.w_AGCATPRO,space(5)))
            this.w_DESGPP = NVL(GPDESCRI118,space(35))
            this.w_FLPRO = NVL(GPTIPPRO118,space(1))
          else
          .link_1_18('Load')
          endif
        .w_AGCODENA = NVL(AGCODENA,space(15))
        .w_AGTIPAGE = NVL(AGTIPAGE,space(1))
        .w_AGCZOAGE = NVL(AGCZOAGE,space(5))
          if link_1_22_joined
            this.w_AGCZOAGE = NVL(AGCODAGE122,NVL(this.w_AGCZOAGE,space(5)))
            this.w_DESAGE = NVL(AGDESAGE122,space(35))
            this.w_TIPAGE = NVL(AGTIPAGE122,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(AGDTOBSO122),ctod("  /  /  "))
          else
          .link_1_22('Load')
          endif
        .w_AGCODFOR = NVL(AGCODFOR,space(15))
          .link_1_24('Load')
        .w_AGFLAZIE = NVL(AGFLAZIE,space(1))
        .w_AGFLESCL = NVL(AGFLESCL,space(1))
        .w_AGFLFARI = NVL(AGFLFARI,space(1))
        .w_AGSCOPAG = NVL(AGSCOPAG,space(1))
        .w_AGFLFIRR = NVL(AGFLFIRR,space(1))
        .w_AGRITIRP = NVL(AGRITIRP,0)
        .w_AGPERIMP = NVL(AGPERIMP,0)
        .w_AGINIENA = NVL(cp_ToDate(AGINIENA),ctod("  /  /  "))
        .w_AGFINENA = NVL(cp_ToDate(AGFINENA),ctod("  /  /  "))
        .w_AGCHKSTA = NVL(AGCHKSTA,space(1))
        .w_AGCHKMAI = NVL(AGCHKMAI,space(1))
        .w_AGCHKPEC = NVL(AGCHKPEC,space(1))
        .w_AGCHKFAX = NVL(AGCHKFAX,space(1))
        .w_AGCHKCPZ = NVL(AGCHKCPZ,space(1))
        .w_AGCHKPTL = NVL(AGCHKPTL,space(1))
        .w_AGDTINIT = NVL(cp_ToDate(AGDTINIT),ctod("  /  /  "))
        .w_AGDTOBSO = NVL(cp_ToDate(AGDTOBSO),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        cp_LoadRecExtFlds(this,'AGENTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
    this.Calculate_MQNVYETNPN()
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AUTOAZI = space(5)
      .w_AUTO = space(1)
      .w_TIPCON = space(1)
      .w_AGCODAGE = space(5)
      .w_AGDESAGE = space(35)
      .w_AGINDAGE = space(25)
      .w_AGFLGCPZ = space(1)
      .w_FLGCPZ = space(1)
      .w_AGAGECAP = space(8)
      .w_AGCAPAGE = space(5)
      .w_AGCITAGE = space(30)
      .w_AGPROAGE = space(2)
      .w_AGFISAGE = space(16)
      .w_AGTELFAX = space(18)
      .w_AGTELEFO = space(18)
      .w_AG_EMAIL = space(254)
      .w_AG_EMPEC = space(254)
      .w_AGCATPRO = space(5)
      .w_DESGPP = space(35)
      .w_AGCODENA = space(15)
      .w_AGTIPAGE = space(1)
      .w_AGCZOAGE = space(5)
      .w_DESAGE = space(35)
      .w_AGCODFOR = space(15)
      .w_DESCRI = space(40)
      .w_AGFLAZIE = space(1)
      .w_AGFLESCL = space(1)
      .w_AGFLFARI = space(1)
      .w_AGSCOPAG = space(1)
      .w_AGFLFIRR = space(1)
      .w_AGRITIRP = 0
      .w_AGPERIMP = 0
      .w_AGINIENA = ctod("  /  /  ")
      .w_AGFINENA = ctod("  /  /  ")
      .w_FLPRO = space(1)
      .w_TIPAGE = space(1)
      .w_AGCHKSTA = space(1)
      .w_AGCHKMAI = space(1)
      .w_AGCHKPEC = space(1)
      .w_AGCHKFAX = space(1)
      .w_AGCHKCPZ = space(1)
      .w_AGCHKPTL = space(1)
      .w_AGDTINIT = ctod("  /  /  ")
      .w_AGDTOBSO = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_TIPCLFAN = space(1)
      .w_OLDTOBSO = ctod("  /  /  ")
      .w_CHKCODENA = .f.
      if .cFunction<>"Filter"
        .w_AUTOAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_AUTOAZI))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_TIPCON = 'F'
        .w_AGCODAGE = iif(.cFunction='Load' AND .w_AUTO='S', 'AUTO',.w_AGCODAGE)
          .DoRTCalc(5,6,.f.)
        .w_AGFLGCPZ = iif(g_REVI='S', 'S', 'N')
        .w_FLGCPZ = .w_AGFLGCPZ
        .DoRTCalc(9,18,.f.)
          if not(empty(.w_AGCATPRO))
          .link_1_18('Full')
          endif
          .DoRTCalc(19,20,.f.)
        .w_AGTIPAGE = 'A'
        .w_AGCZOAGE = SPACE(5)
        .DoRTCalc(22,22,.f.)
          if not(empty(.w_AGCZOAGE))
          .link_1_22('Full')
          endif
        .DoRTCalc(23,24,.f.)
          if not(empty(.w_AGCODFOR))
          .link_1_24('Full')
          endif
          .DoRTCalc(25,25,.f.)
        .w_AGFLAZIE = 'I'
        .w_AGFLESCL = 'M'
        .w_AGFLFARI = 'F'
        .w_AGSCOPAG = ' '
          .DoRTCalc(30,31,.f.)
        .w_AGPERIMP = IIF(NOT EMPTY(.w_AGRITIRP),.w_AGPERIMP,0)
          .DoRTCalc(33,35,.f.)
        .w_TIPAGE = 'C'
          .DoRTCalc(37,40,.f.)
        .w_AGCHKCPZ = 'N'
          .DoRTCalc(42,44,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(46,46,.f.)
        .w_TIPCLFAN = 'A'
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'AGENTI')
    this.DoRTCalc(48,49,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oAGCODAGE_1_4.enabled = i_bVal
      .Page1.oPag.oAGDESAGE_1_5.enabled = i_bVal
      .Page1.oPag.oAGINDAGE_1_6.enabled = i_bVal
      .Page1.oPag.oAGFLGCPZ_1_7.enabled = i_bVal
      .Page1.oPag.oAGAGECAP_1_9.enabled = i_bVal
      .Page1.oPag.oAGCITAGE_1_11.enabled = i_bVal
      .Page1.oPag.oAGPROAGE_1_12.enabled = i_bVal
      .Page1.oPag.oAGFISAGE_1_13.enabled = i_bVal
      .Page1.oPag.oAGTELFAX_1_14.enabled = i_bVal
      .Page1.oPag.oAGTELEFO_1_15.enabled = i_bVal
      .Page1.oPag.oAG_EMAIL_1_16.enabled = i_bVal
      .Page1.oPag.oAG_EMPEC_1_17.enabled = i_bVal
      .Page1.oPag.oAGCATPRO_1_18.enabled = i_bVal
      .Page1.oPag.oAGCODENA_1_20.enabled = i_bVal
      .Page1.oPag.oAGTIPAGE_1_21.enabled = i_bVal
      .Page1.oPag.oAGCZOAGE_1_22.enabled = i_bVal
      .Page1.oPag.oAGCODFOR_1_24.enabled = i_bVal
      .Page1.oPag.oAGFLAZIE_1_26.enabled_(i_bVal)
      .Page1.oPag.oAGFLESCL_1_27.enabled_(i_bVal)
      .Page1.oPag.oAGFLFARI_1_28.enabled_(i_bVal)
      .Page1.oPag.oAGSCOPAG_1_29.enabled = i_bVal
      .Page1.oPag.oAGFLFIRR_1_30.enabled = i_bVal
      .Page1.oPag.oAGRITIRP_1_31.enabled = i_bVal
      .Page1.oPag.oAGPERIMP_1_32.enabled = i_bVal
      .Page1.oPag.oAGINIENA_1_33.enabled = i_bVal
      .Page1.oPag.oAGFINENA_1_34.enabled = i_bVal
      .Page1.oPag.oAGCHKSTA_1_47.enabled = i_bVal
      .Page1.oPag.oAGCHKMAI_1_48.enabled = i_bVal
      .Page1.oPag.oAGCHKPEC_1_49.enabled = i_bVal
      .Page1.oPag.oAGCHKFAX_1_50.enabled = i_bVal
      .Page1.oPag.oAGCHKCPZ_1_51.enabled = i_bVal
      .Page1.oPag.oAGCHKPTL_1_52.enabled = i_bVal
      .Page1.oPag.oAGDTINIT_1_53.enabled = i_bVal
      .Page1.oPag.oAGDTOBSO_1_54.enabled = i_bVal
      .Page1.oPag.oObj_1_65.enabled = i_bVal
      .Page1.oPag.oObj_1_66.enabled = i_bVal
      .Page1.oPag.oObj_1_68.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oAGCODAGE_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oAGCODAGE_1_4.enabled = .t.
        .Page1.oPag.oAGDESAGE_1_5.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'AGENTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGCODAGE,"AGCODAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGDESAGE,"AGDESAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGINDAGE,"AGINDAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGFLGCPZ,"AGFLGCPZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGAGECAP,"AGAGECAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGCAPAGE,"AGCAPAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGCITAGE,"AGCITAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGPROAGE,"AGPROAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGFISAGE,"AGFISAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGTELFAX,"AGTELFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGTELEFO,"AGTELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AG_EMAIL,"AG_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AG_EMPEC,"AG_EMPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGCATPRO,"AGCATPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGCODENA,"AGCODENA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGTIPAGE,"AGTIPAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGCZOAGE,"AGCZOAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGCODFOR,"AGCODFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGFLAZIE,"AGFLAZIE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGFLESCL,"AGFLESCL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGFLFARI,"AGFLFARI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGSCOPAG,"AGSCOPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGFLFIRR,"AGFLFIRR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGRITIRP,"AGRITIRP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGPERIMP,"AGPERIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGINIENA,"AGINIENA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGFINENA,"AGFINENA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGCHKSTA,"AGCHKSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGCHKMAI,"AGCHKMAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGCHKPEC,"AGCHKPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGCHKFAX,"AGCHKFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGCHKCPZ,"AGCHKCPZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGCHKPTL,"AGCHKPTL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGDTINIT,"AGDTINIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AGDTOBSO,"AGDTOBSO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    i_lTable = "AGENTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.AGENTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SAG with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.AGENTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.AGENTI_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into AGENTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'AGENTI')
        i_extval=cp_InsertValODBCExtFlds(this,'AGENTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(AGCODAGE,AGDESAGE,AGINDAGE,AGFLGCPZ,AGAGECAP"+;
                  ",AGCAPAGE,AGCITAGE,AGPROAGE,AGFISAGE,AGTELFAX"+;
                  ",AGTELEFO,AG_EMAIL,AG_EMPEC,AGCATPRO,AGCODENA"+;
                  ",AGTIPAGE,AGCZOAGE,AGCODFOR,AGFLAZIE,AGFLESCL"+;
                  ",AGFLFARI,AGSCOPAG,AGFLFIRR,AGRITIRP,AGPERIMP"+;
                  ",AGINIENA,AGFINENA,AGCHKSTA,AGCHKMAI,AGCHKPEC"+;
                  ",AGCHKFAX,AGCHKCPZ,AGCHKPTL,AGDTINIT,AGDTOBSO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_AGCODAGE)+;
                  ","+cp_ToStrODBC(this.w_AGDESAGE)+;
                  ","+cp_ToStrODBC(this.w_AGINDAGE)+;
                  ","+cp_ToStrODBC(this.w_AGFLGCPZ)+;
                  ","+cp_ToStrODBC(this.w_AGAGECAP)+;
                  ","+cp_ToStrODBC(this.w_AGCAPAGE)+;
                  ","+cp_ToStrODBC(this.w_AGCITAGE)+;
                  ","+cp_ToStrODBC(this.w_AGPROAGE)+;
                  ","+cp_ToStrODBC(this.w_AGFISAGE)+;
                  ","+cp_ToStrODBC(this.w_AGTELFAX)+;
                  ","+cp_ToStrODBC(this.w_AGTELEFO)+;
                  ","+cp_ToStrODBC(this.w_AG_EMAIL)+;
                  ","+cp_ToStrODBC(this.w_AG_EMPEC)+;
                  ","+cp_ToStrODBCNull(this.w_AGCATPRO)+;
                  ","+cp_ToStrODBC(this.w_AGCODENA)+;
                  ","+cp_ToStrODBC(this.w_AGTIPAGE)+;
                  ","+cp_ToStrODBCNull(this.w_AGCZOAGE)+;
                  ","+cp_ToStrODBCNull(this.w_AGCODFOR)+;
                  ","+cp_ToStrODBC(this.w_AGFLAZIE)+;
                  ","+cp_ToStrODBC(this.w_AGFLESCL)+;
                  ","+cp_ToStrODBC(this.w_AGFLFARI)+;
                  ","+cp_ToStrODBC(this.w_AGSCOPAG)+;
                  ","+cp_ToStrODBC(this.w_AGFLFIRR)+;
                  ","+cp_ToStrODBC(this.w_AGRITIRP)+;
                  ","+cp_ToStrODBC(this.w_AGPERIMP)+;
                  ","+cp_ToStrODBC(this.w_AGINIENA)+;
                  ","+cp_ToStrODBC(this.w_AGFINENA)+;
                  ","+cp_ToStrODBC(this.w_AGCHKSTA)+;
                  ","+cp_ToStrODBC(this.w_AGCHKMAI)+;
                  ","+cp_ToStrODBC(this.w_AGCHKPEC)+;
                  ","+cp_ToStrODBC(this.w_AGCHKFAX)+;
                  ","+cp_ToStrODBC(this.w_AGCHKCPZ)+;
                  ","+cp_ToStrODBC(this.w_AGCHKPTL)+;
                  ","+cp_ToStrODBC(this.w_AGDTINIT)+;
                  ","+cp_ToStrODBC(this.w_AGDTOBSO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'AGENTI')
        i_extval=cp_InsertValVFPExtFlds(this,'AGENTI')
        cp_CheckDeletedKey(i_cTable,0,'AGCODAGE',this.w_AGCODAGE)
        INSERT INTO (i_cTable);
              (AGCODAGE,AGDESAGE,AGINDAGE,AGFLGCPZ,AGAGECAP,AGCAPAGE,AGCITAGE,AGPROAGE,AGFISAGE,AGTELFAX,AGTELEFO,AG_EMAIL,AG_EMPEC,AGCATPRO,AGCODENA,AGTIPAGE,AGCZOAGE,AGCODFOR,AGFLAZIE,AGFLESCL,AGFLFARI,AGSCOPAG,AGFLFIRR,AGRITIRP,AGPERIMP,AGINIENA,AGFINENA,AGCHKSTA,AGCHKMAI,AGCHKPEC,AGCHKFAX,AGCHKCPZ,AGCHKPTL,AGDTINIT,AGDTOBSO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_AGCODAGE;
                  ,this.w_AGDESAGE;
                  ,this.w_AGINDAGE;
                  ,this.w_AGFLGCPZ;
                  ,this.w_AGAGECAP;
                  ,this.w_AGCAPAGE;
                  ,this.w_AGCITAGE;
                  ,this.w_AGPROAGE;
                  ,this.w_AGFISAGE;
                  ,this.w_AGTELFAX;
                  ,this.w_AGTELEFO;
                  ,this.w_AG_EMAIL;
                  ,this.w_AG_EMPEC;
                  ,this.w_AGCATPRO;
                  ,this.w_AGCODENA;
                  ,this.w_AGTIPAGE;
                  ,this.w_AGCZOAGE;
                  ,this.w_AGCODFOR;
                  ,this.w_AGFLAZIE;
                  ,this.w_AGFLESCL;
                  ,this.w_AGFLFARI;
                  ,this.w_AGSCOPAG;
                  ,this.w_AGFLFIRR;
                  ,this.w_AGRITIRP;
                  ,this.w_AGPERIMP;
                  ,this.w_AGINIENA;
                  ,this.w_AGFINENA;
                  ,this.w_AGCHKSTA;
                  ,this.w_AGCHKMAI;
                  ,this.w_AGCHKPEC;
                  ,this.w_AGCHKFAX;
                  ,this.w_AGCHKCPZ;
                  ,this.w_AGCHKPTL;
                  ,this.w_AGDTINIT;
                  ,this.w_AGDTOBSO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.AGENTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.AGENTI_IDX,i_nConn)
      *
      * update AGENTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'AGENTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " AGDESAGE="+cp_ToStrODBC(this.w_AGDESAGE)+;
             ",AGINDAGE="+cp_ToStrODBC(this.w_AGINDAGE)+;
             ",AGFLGCPZ="+cp_ToStrODBC(this.w_AGFLGCPZ)+;
             ",AGAGECAP="+cp_ToStrODBC(this.w_AGAGECAP)+;
             ",AGCAPAGE="+cp_ToStrODBC(this.w_AGCAPAGE)+;
             ",AGCITAGE="+cp_ToStrODBC(this.w_AGCITAGE)+;
             ",AGPROAGE="+cp_ToStrODBC(this.w_AGPROAGE)+;
             ",AGFISAGE="+cp_ToStrODBC(this.w_AGFISAGE)+;
             ",AGTELFAX="+cp_ToStrODBC(this.w_AGTELFAX)+;
             ",AGTELEFO="+cp_ToStrODBC(this.w_AGTELEFO)+;
             ",AG_EMAIL="+cp_ToStrODBC(this.w_AG_EMAIL)+;
             ",AG_EMPEC="+cp_ToStrODBC(this.w_AG_EMPEC)+;
             ",AGCATPRO="+cp_ToStrODBCNull(this.w_AGCATPRO)+;
             ",AGCODENA="+cp_ToStrODBC(this.w_AGCODENA)+;
             ",AGTIPAGE="+cp_ToStrODBC(this.w_AGTIPAGE)+;
             ",AGCZOAGE="+cp_ToStrODBCNull(this.w_AGCZOAGE)+;
             ",AGCODFOR="+cp_ToStrODBCNull(this.w_AGCODFOR)+;
             ",AGFLAZIE="+cp_ToStrODBC(this.w_AGFLAZIE)+;
             ",AGFLESCL="+cp_ToStrODBC(this.w_AGFLESCL)+;
             ",AGFLFARI="+cp_ToStrODBC(this.w_AGFLFARI)+;
             ",AGSCOPAG="+cp_ToStrODBC(this.w_AGSCOPAG)+;
             ",AGFLFIRR="+cp_ToStrODBC(this.w_AGFLFIRR)+;
             ",AGRITIRP="+cp_ToStrODBC(this.w_AGRITIRP)+;
             ",AGPERIMP="+cp_ToStrODBC(this.w_AGPERIMP)+;
             ",AGINIENA="+cp_ToStrODBC(this.w_AGINIENA)+;
             ",AGFINENA="+cp_ToStrODBC(this.w_AGFINENA)+;
             ",AGCHKSTA="+cp_ToStrODBC(this.w_AGCHKSTA)+;
             ",AGCHKMAI="+cp_ToStrODBC(this.w_AGCHKMAI)+;
             ",AGCHKPEC="+cp_ToStrODBC(this.w_AGCHKPEC)+;
             ",AGCHKFAX="+cp_ToStrODBC(this.w_AGCHKFAX)+;
             ",AGCHKCPZ="+cp_ToStrODBC(this.w_AGCHKCPZ)+;
             ",AGCHKPTL="+cp_ToStrODBC(this.w_AGCHKPTL)+;
             ",AGDTINIT="+cp_ToStrODBC(this.w_AGDTINIT)+;
             ",AGDTOBSO="+cp_ToStrODBC(this.w_AGDTOBSO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'AGENTI')
        i_cWhere = cp_PKFox(i_cTable  ,'AGCODAGE',this.w_AGCODAGE  )
        UPDATE (i_cTable) SET;
              AGDESAGE=this.w_AGDESAGE;
             ,AGINDAGE=this.w_AGINDAGE;
             ,AGFLGCPZ=this.w_AGFLGCPZ;
             ,AGAGECAP=this.w_AGAGECAP;
             ,AGCAPAGE=this.w_AGCAPAGE;
             ,AGCITAGE=this.w_AGCITAGE;
             ,AGPROAGE=this.w_AGPROAGE;
             ,AGFISAGE=this.w_AGFISAGE;
             ,AGTELFAX=this.w_AGTELFAX;
             ,AGTELEFO=this.w_AGTELEFO;
             ,AG_EMAIL=this.w_AG_EMAIL;
             ,AG_EMPEC=this.w_AG_EMPEC;
             ,AGCATPRO=this.w_AGCATPRO;
             ,AGCODENA=this.w_AGCODENA;
             ,AGTIPAGE=this.w_AGTIPAGE;
             ,AGCZOAGE=this.w_AGCZOAGE;
             ,AGCODFOR=this.w_AGCODFOR;
             ,AGFLAZIE=this.w_AGFLAZIE;
             ,AGFLESCL=this.w_AGFLESCL;
             ,AGFLFARI=this.w_AGFLFARI;
             ,AGSCOPAG=this.w_AGSCOPAG;
             ,AGFLFIRR=this.w_AGFLFIRR;
             ,AGRITIRP=this.w_AGRITIRP;
             ,AGPERIMP=this.w_AGPERIMP;
             ,AGINIENA=this.w_AGINIENA;
             ,AGFINENA=this.w_AGFINENA;
             ,AGCHKSTA=this.w_AGCHKSTA;
             ,AGCHKMAI=this.w_AGCHKMAI;
             ,AGCHKPEC=this.w_AGCHKPEC;
             ,AGCHKFAX=this.w_AGCHKFAX;
             ,AGCHKCPZ=this.w_AGCHKCPZ;
             ,AGCHKPTL=this.w_AGCHKPTL;
             ,AGDTINIT=this.w_AGDTINIT;
             ,AGDTOBSO=this.w_AGDTOBSO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.AGENTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.AGENTI_IDX,i_nConn)
      *
      * delete AGENTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'AGCODAGE',this.w_AGCODAGE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
            .w_TIPCON = 'F'
        if .o_AUTO<>.w_AUTO
            .w_AGCODAGE = iif(.cFunction='Load' AND .w_AUTO='S', 'AUTO',.w_AGCODAGE)
        endif
        .DoRTCalc(5,7,.t.)
        if .o_AGCODAGE<>.w_AGCODAGE
            .w_FLGCPZ = .w_AGFLGCPZ
        endif
        .DoRTCalc(9,21,.t.)
        if .o_AGTIPAGE<>.w_AGTIPAGE
            .w_AGCZOAGE = SPACE(5)
          .link_1_22('Full')
        endif
        .DoRTCalc(23,31,.t.)
        if .o_AGRITIRP<>.w_AGRITIRP
            .w_AGPERIMP = IIF(NOT EMPTY(.w_AGRITIRP),.w_AGPERIMP,0)
        endif
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(33,49,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_65.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_66.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
    endwith
  return

  proc Calculate_MQNVYETNPN()
    with this
          * --- Controllo caratteri numerici nel codice ENASARCO
          .w_CHKCODENA = IS_NUMBER(ALLTRIM(.w_AGCODENA),'Valore del campo Codice Enasarco '+ALLTRIM(.w_AGCODENA)+' non corretto: Inserire solo 8 caratteri numerici')
          .w_AGCODENA = IIF( .w_CHKCODENA, .w_AGCODENA , '' )
    endwith
  endproc
  proc Calculate_OLYGVFQXTR()
    with this
          * --- Calcola Auto
          GSAR_BSS(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAGCODAGE_1_4.enabled = this.oPgFrm.Page1.oPag.oAGCODAGE_1_4.mCond()
    this.oPgFrm.Page1.oPag.oAGCZOAGE_1_22.enabled = this.oPgFrm.Page1.oPag.oAGCZOAGE_1_22.mCond()
    this.oPgFrm.Page1.oPag.oAGCODFOR_1_24.enabled = this.oPgFrm.Page1.oPag.oAGCODFOR_1_24.mCond()
    this.oPgFrm.Page1.oPag.oAGPERIMP_1_32.enabled = this.oPgFrm.Page1.oPag.oAGPERIMP_1_32.mCond()
    this.oPgFrm.Page1.oPag.oAGCHKCPZ_1_51.enabled = this.oPgFrm.Page1.oPag.oAGCHKCPZ_1_51.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oAGFLGCPZ_1_7.visible=!this.oPgFrm.Page1.oPag.oAGFLGCPZ_1_7.mHide()
    this.oPgFrm.Page1.oPag.oAGCHKSTA_1_47.visible=!this.oPgFrm.Page1.oPag.oAGCHKSTA_1_47.mHide()
    this.oPgFrm.Page1.oPag.oAGCHKMAI_1_48.visible=!this.oPgFrm.Page1.oPag.oAGCHKMAI_1_48.mHide()
    this.oPgFrm.Page1.oPag.oAGCHKPEC_1_49.visible=!this.oPgFrm.Page1.oPag.oAGCHKPEC_1_49.mHide()
    this.oPgFrm.Page1.oPag.oAGCHKFAX_1_50.visible=!this.oPgFrm.Page1.oPag.oAGCHKFAX_1_50.mHide()
    this.oPgFrm.Page1.oPag.oAGCHKCPZ_1_51.visible=!this.oPgFrm.Page1.oPag.oAGCHKCPZ_1_51.mHide()
    this.oPgFrm.Page1.oPag.oAGCHKPTL_1_52.visible=!this.oPgFrm.Page1.oPag.oAGCHKPTL_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_73.visible=!this.oPgFrm.Page1.oPag.oStr_1_73.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_65.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_66.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_68.Event(cEvent)
        if lower(cEvent)==lower("Insert start")
          .Calculate_OLYGVFQXTR()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AUTOAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NUMAUT_M_IDX,3]
    i_lTable = "NUMAUT_M"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2], .t., this.NUMAUT_M_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AUTOAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AUTOAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NATIPGES,NACODAZI,NAACTIVE";
                   +" from "+i_cTable+" "+i_lTable+" where NACODAZI="+cp_ToStrODBC(this.w_AUTOAZI);
                   +" and NATIPGES="+cp_ToStrODBC('AG');
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NATIPGES','AG';
                       ,'NACODAZI',this.w_AUTOAZI)
            select NATIPGES,NACODAZI,NAACTIVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AUTOAZI = NVL(_Link_.NACODAZI,space(5))
      this.w_AUTO = NVL(_Link_.NAACTIVE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AUTOAZI = space(5)
      endif
      this.w_AUTO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])+'\'+cp_ToStr(_Link_.NATIPGES,1)+'\'+cp_ToStr(_Link_.NACODAZI,1)
      cp_ShowWarn(i_cKey,this.NUMAUT_M_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AUTOAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AGCATPRO
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_lTable = "GRUPRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2], .t., this.GRUPRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGCATPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGP',True,'GRUPRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_AGCATPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_AGCATPRO))
          select GPCODICE,GPDESCRI,GPTIPPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AGCATPRO)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AGCATPRO) and !this.bDontReportError
            deferred_cp_zoom('GRUPRO','*','GPCODICE',cp_AbsName(oSource.parent,'oAGCATPRO_1_18'),i_cWhere,'GSAR_AGP',"Categorie provvigioni",'GSAR_AGE.GRUPRO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGCATPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_AGCATPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_AGCATPRO)
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGCATPRO = NVL(_Link_.GPCODICE,space(5))
      this.w_DESGPP = NVL(_Link_.GPDESCRI,space(35))
      this.w_FLPRO = NVL(_Link_.GPTIPPRO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AGCATPRO = space(5)
      endif
      this.w_DESGPP = space(35)
      this.w_FLPRO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLPRO $ 'G'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria provvigioni inesistente o non di tipo agenti")
        endif
        this.w_AGCATPRO = space(5)
        this.w_DESGPP = space(35)
        this.w_FLPRO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGCATPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUPRO_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.GPCODICE as GPCODICE118"+ ",link_1_18.GPDESCRI as GPDESCRI118"+ ",link_1_18.GPTIPPRO as GPTIPPRO118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on AGENTI.AGCATPRO=link_1_18.GPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and AGENTI.AGCATPRO=link_1_18.GPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AGCZOAGE
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGCZOAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_AGCZOAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGTIPAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_AGCZOAGE))
          select AGCODAGE,AGDESAGE,AGTIPAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AGCZOAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AGCZOAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oAGCZOAGE_1_22'),i_cWhere,'GSAR_AGE',"",'GSAR1AGE.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGTIPAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGTIPAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGCZOAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGTIPAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_AGCZOAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_AGCZOAGE)
            select AGCODAGE,AGDESAGE,AGTIPAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGCZOAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_TIPAGE = NVL(_Link_.AGTIPAGE,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AGCZOAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_TIPAGE = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_AGCZOAGE<>.w_AGCODAGE) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) And .w_TIPAGE='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice agente incongruente oppure obsoleto")
        endif
        this.w_AGCZOAGE = space(5)
        this.w_DESAGE = space(35)
        this.w_TIPAGE = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGCZOAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.AGCODAGE as AGCODAGE122"+ ",link_1_22.AGDESAGE as AGDESAGE122"+ ",link_1_22.AGTIPAGE as AGTIPAGE122"+ ",link_1_22.AGDTOBSO as AGDTOBSO122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on AGENTI.AGCZOAGE=link_1_22.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and AGENTI.AGCZOAGE=link_1_22.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AGCODFOR
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGCODFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_AGCODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPCLF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_AGCODFOR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPCLF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AGCODFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_AGCODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_AGCODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AGCODFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oAGCODFOR_1_24'),i_cWhere,'GSAR_BZC',"Elenco fornitori / agenti",'GSAR_AGE.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPCLF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPCLF";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGCODFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPCLF";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_AGCODFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_AGCODFOR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANTIPCLF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGCODFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_TIPCLFAN = NVL(_Link_.ANTIPCLF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AGCODFOR = space(15)
      endif
      this.w_DESCRI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_TIPCLFAN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPCLFAN='A') OR EMPTY(.w_AGCODFOR)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
        endif
        this.w_AGCODFOR = space(15)
        this.w_DESCRI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_TIPCLFAN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGCODFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAGCODAGE_1_4.value==this.w_AGCODAGE)
      this.oPgFrm.Page1.oPag.oAGCODAGE_1_4.value=this.w_AGCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oAGDESAGE_1_5.value==this.w_AGDESAGE)
      this.oPgFrm.Page1.oPag.oAGDESAGE_1_5.value=this.w_AGDESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oAGINDAGE_1_6.value==this.w_AGINDAGE)
      this.oPgFrm.Page1.oPag.oAGINDAGE_1_6.value=this.w_AGINDAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oAGFLGCPZ_1_7.RadioValue()==this.w_AGFLGCPZ)
      this.oPgFrm.Page1.oPag.oAGFLGCPZ_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGAGECAP_1_9.value==this.w_AGAGECAP)
      this.oPgFrm.Page1.oPag.oAGAGECAP_1_9.value=this.w_AGAGECAP
    endif
    if not(this.oPgFrm.Page1.oPag.oAGCITAGE_1_11.value==this.w_AGCITAGE)
      this.oPgFrm.Page1.oPag.oAGCITAGE_1_11.value=this.w_AGCITAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oAGPROAGE_1_12.value==this.w_AGPROAGE)
      this.oPgFrm.Page1.oPag.oAGPROAGE_1_12.value=this.w_AGPROAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oAGFISAGE_1_13.value==this.w_AGFISAGE)
      this.oPgFrm.Page1.oPag.oAGFISAGE_1_13.value=this.w_AGFISAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oAGTELFAX_1_14.value==this.w_AGTELFAX)
      this.oPgFrm.Page1.oPag.oAGTELFAX_1_14.value=this.w_AGTELFAX
    endif
    if not(this.oPgFrm.Page1.oPag.oAGTELEFO_1_15.value==this.w_AGTELEFO)
      this.oPgFrm.Page1.oPag.oAGTELEFO_1_15.value=this.w_AGTELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oAG_EMAIL_1_16.value==this.w_AG_EMAIL)
      this.oPgFrm.Page1.oPag.oAG_EMAIL_1_16.value=this.w_AG_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oAG_EMPEC_1_17.value==this.w_AG_EMPEC)
      this.oPgFrm.Page1.oPag.oAG_EMPEC_1_17.value=this.w_AG_EMPEC
    endif
    if not(this.oPgFrm.Page1.oPag.oAGCATPRO_1_18.value==this.w_AGCATPRO)
      this.oPgFrm.Page1.oPag.oAGCATPRO_1_18.value=this.w_AGCATPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGPP_1_19.value==this.w_DESGPP)
      this.oPgFrm.Page1.oPag.oDESGPP_1_19.value=this.w_DESGPP
    endif
    if not(this.oPgFrm.Page1.oPag.oAGCODENA_1_20.value==this.w_AGCODENA)
      this.oPgFrm.Page1.oPag.oAGCODENA_1_20.value=this.w_AGCODENA
    endif
    if not(this.oPgFrm.Page1.oPag.oAGTIPAGE_1_21.RadioValue()==this.w_AGTIPAGE)
      this.oPgFrm.Page1.oPag.oAGTIPAGE_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGCZOAGE_1_22.value==this.w_AGCZOAGE)
      this.oPgFrm.Page1.oPag.oAGCZOAGE_1_22.value=this.w_AGCZOAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE_1_23.value==this.w_DESAGE)
      this.oPgFrm.Page1.oPag.oDESAGE_1_23.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oAGCODFOR_1_24.value==this.w_AGCODFOR)
      this.oPgFrm.Page1.oPag.oAGCODFOR_1_24.value=this.w_AGCODFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_25.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_25.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oAGFLAZIE_1_26.RadioValue()==this.w_AGFLAZIE)
      this.oPgFrm.Page1.oPag.oAGFLAZIE_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGFLESCL_1_27.RadioValue()==this.w_AGFLESCL)
      this.oPgFrm.Page1.oPag.oAGFLESCL_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGFLFARI_1_28.RadioValue()==this.w_AGFLFARI)
      this.oPgFrm.Page1.oPag.oAGFLFARI_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGSCOPAG_1_29.RadioValue()==this.w_AGSCOPAG)
      this.oPgFrm.Page1.oPag.oAGSCOPAG_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGFLFIRR_1_30.RadioValue()==this.w_AGFLFIRR)
      this.oPgFrm.Page1.oPag.oAGFLFIRR_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGRITIRP_1_31.value==this.w_AGRITIRP)
      this.oPgFrm.Page1.oPag.oAGRITIRP_1_31.value=this.w_AGRITIRP
    endif
    if not(this.oPgFrm.Page1.oPag.oAGPERIMP_1_32.value==this.w_AGPERIMP)
      this.oPgFrm.Page1.oPag.oAGPERIMP_1_32.value=this.w_AGPERIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oAGINIENA_1_33.value==this.w_AGINIENA)
      this.oPgFrm.Page1.oPag.oAGINIENA_1_33.value=this.w_AGINIENA
    endif
    if not(this.oPgFrm.Page1.oPag.oAGFINENA_1_34.value==this.w_AGFINENA)
      this.oPgFrm.Page1.oPag.oAGFINENA_1_34.value=this.w_AGFINENA
    endif
    if not(this.oPgFrm.Page1.oPag.oAGCHKSTA_1_47.RadioValue()==this.w_AGCHKSTA)
      this.oPgFrm.Page1.oPag.oAGCHKSTA_1_47.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGCHKMAI_1_48.RadioValue()==this.w_AGCHKMAI)
      this.oPgFrm.Page1.oPag.oAGCHKMAI_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGCHKPEC_1_49.RadioValue()==this.w_AGCHKPEC)
      this.oPgFrm.Page1.oPag.oAGCHKPEC_1_49.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGCHKFAX_1_50.RadioValue()==this.w_AGCHKFAX)
      this.oPgFrm.Page1.oPag.oAGCHKFAX_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGCHKCPZ_1_51.RadioValue()==this.w_AGCHKCPZ)
      this.oPgFrm.Page1.oPag.oAGCHKCPZ_1_51.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGCHKPTL_1_52.RadioValue()==this.w_AGCHKPTL)
      this.oPgFrm.Page1.oPag.oAGCHKPTL_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAGDTINIT_1_53.value==this.w_AGDTINIT)
      this.oPgFrm.Page1.oPag.oAGDTINIT_1_53.value=this.w_AGDTINIT
    endif
    if not(this.oPgFrm.Page1.oPag.oAGDTOBSO_1_54.value==this.w_AGDTOBSO)
      this.oPgFrm.Page1.oPag.oAGDTOBSO_1_54.value=this.w_AGDTOBSO
    endif
    cp_SetControlsValueExtFlds(this,'AGENTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_AGCODAGE))  and (.w_AUTO<>'S' AND .cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGCODAGE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_AGCODAGE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_AGFISAGE, 'CF', 'G',0,.w_AGCODAGE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGFISAGE_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_AGCATPRO)) or not(.w_FLPRO $ 'G'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGCATPRO_1_18.SetFocus()
            i_bnoObbl = !empty(.w_AGCATPRO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria provvigioni inesistente o non di tipo agenti")
          case   not((.w_AGCZOAGE<>.w_AGCODAGE) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) And .w_TIPAGE='C')  and (.w_AGTIPAGE='A')  and not(empty(.w_AGCZOAGE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGCZOAGE_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice agente incongruente oppure obsoleto")
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPCLFAN='A') OR EMPTY(.w_AGCODFOR))  and (g_REVI='N' or .w_FLGCPZ<>'S' or .cFunction='Load')  and not(empty(.w_AGCODFOR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGCODFOR_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice fornitore inesistente oppure obsoleto")
          case   (empty(.w_AGPERIMP))  and (not empty(.w_AGRITIRP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGPERIMP_1_32.SetFocus()
            i_bnoObbl = !empty(.w_AGPERIMP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((.w_AGINIENA<=.w_AGFINENA) or (empty(.w_AGFINENA)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGINIENA_1_33.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data inizio mandato � maggiore della data di fine mandato")
          case   not(((.w_AGDTOBSO>=.w_AGFINENA) OR EMPTY(.w_AGDTOBSO)) AND ((.w_AGINIENA<=.w_AGFINENA) or (empty(.w_AGFINENA))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGFINENA_1_34.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di fine mandato non pu� essere maggiore della data obsolescenza o la data inizio mandato � maggiore della data di fine mandato")
          case   not((.w_AGFINENA<=.w_AGDTOBSO) OR EMPTY(.w_AGDTOBSO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAGDTOBSO_1_54.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di obsolescenza non pu� essere minore della data di fine mandato")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_age
      
      * Se � attivo il modulo revi si esegue il controllo per verificare che i codici di pagamento tra le diverse aziende siano uguali
      IF g_REVI='S' AND .w_AGFLGCPZ='S'  AND ALLTRIM(i_CODAZI) $ AssocLst()
      local risultato
      risultato=IAHR_CHK1BCP (this)
      IF not risultato
           i_bnoChk = .f.
           i_bRes = .f.
           i_cErrorMsg =Ah_MsgFormat( "Operazione annullata")
      ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AUTO = this.w_AUTO
    this.o_AGCODAGE = this.w_AGCODAGE
    this.o_AGTIPAGE = this.w_AGTIPAGE
    this.o_AGRITIRP = this.w_AGRITIRP
    return

enddefine

* --- Define pages as container
define class tgsar_agePag1 as StdContainer
  Width  = 581
  height = 526
  stdWidth  = 581
  stdheight = 526
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAGCODAGE_1_4 as StdField with uid="TRZIDZVBNP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_AGCODAGE", cQueryName = "AGCODAGE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente",;
    HelpContextID = 50988107,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=139, Top=11, InputMask=replicate('X',5)

  func oAGCODAGE_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AUTO<>'S' AND .cFunction='Load')
    endwith
   endif
  endfunc

  add object oAGDESAGE_1_5 as StdField with uid="NMATQWQXJM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_AGDESAGE", cQueryName = "AGDESAGE",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Nome dell'agente o del capo zona",;
    HelpContextID = 66065483,;
   bGlobalFont=.t.,;
    Height=21, Width=288, Left=208, Top=11, InputMask=replicate('X',35)

  add object oAGINDAGE_1_6 as StdField with uid="WYAUWUACTL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_AGINDAGE", cQueryName = "AGINDAGE",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo",;
    HelpContextID = 50947147,;
   bGlobalFont=.t.,;
    Height=21, Width=208, Left=139, Top=37, InputMask=replicate('X',25)

  add object oAGFLGCPZ_1_7 as StdCheck with uid="IHMDBVTZON",rtseq=7,rtrep=.f.,left=383, top=37, caption="Pubblica su web",;
    ToolTipText = "Se attivo: trasferisce anagrafica agente su Web Application",;
    HelpContextID = 87503968,;
    cFormVar="w_AGFLGCPZ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGFLGCPZ_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGFLGCPZ_1_7.GetRadio()
    this.Parent.oContained.w_AGFLGCPZ = this.RadioValue()
    return .t.
  endfunc

  func oAGFLGCPZ_1_7.SetRadio()
    this.Parent.oContained.w_AGFLGCPZ=trim(this.Parent.oContained.w_AGFLGCPZ)
    this.value = ;
      iif(this.Parent.oContained.w_AGFLGCPZ=='S',1,;
      0)
  endfunc

  func oAGFLGCPZ_1_7.mHide()
    with this.Parent.oContained
      return (Not(g_IZCP$'SA' OR g_CPIN='S' OR g_REVI='S'))
    endwith
  endfunc

  add object oAGAGECAP_1_9 as StdField with uid="VVNVAJKUKQ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_AGAGECAP", cQueryName = "AGAGECAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice di avviamento postale",;
    HelpContextID = 85058646,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=139, Top=63, InputMask=replicate('X',8), bHasZoom = .t. 

  proc oAGAGECAP_1_9.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_AGAGECAP",".w_AGCITAGE",".w_AGPROAGE")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAGCITAGE_1_11 as StdField with uid="VBUYFBLRJY",rtseq=11,rtrep=.f.,;
    cFormVar = "w_AGCITAGE", cQueryName = "AGCITAGE",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza",;
    HelpContextID = 67372107,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=208, Top=63, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oAGCITAGE_1_11.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_AGAGECAP",".w_AGCITAGE",".w_AGPROAGE")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAGPROAGE_1_12 as StdField with uid="IMLBHFYXNN",rtseq=12,rtrep=.f.,;
    cFormVar = "w_AGPROAGE", cQueryName = "AGPROAGE",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza",;
    HelpContextID = 62772299,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=505, Top=63, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2), bHasZoom = .t. 

  proc oAGPROAGE_1_12.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_AGPROAGE")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAGFISAGE_1_13 as StdField with uid="NACNITBWHC",rtseq=13,rtrep=.f.,;
    cFormVar = "w_AGFISAGE", cQueryName = "AGFISAGE",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 66335819,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=139, Top=89, InputMask=replicate('X',16)

  func oAGFISAGE_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_AGFISAGE, 'CF', 'G',0,.w_AGCODAGE))
    endwith
    return bRes
  endfunc

  add object oAGTELFAX_1_14 as StdField with uid="UAPUCKPUGN",rtseq=14,rtrep=.f.,;
    cFormVar = "w_AGTELFAX", cQueryName = "AGTELFAX",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di TELEX o FAX",;
    HelpContextID = 142677086,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=140, Top=115, InputMask=replicate('X',18)

  add object oAGTELEFO_1_15 as StdField with uid="AYACSJQTPD",rtseq=15,rtrep=.f.,;
    cFormVar = "w_AGTELEFO", cQueryName = "AGTELEFO",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Recapito telefonico dell'agente",;
    HelpContextID = 125899861,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=349, Top=115, InputMask=replicate('X',18)

  add object oAG_EMAIL_1_16 as StdField with uid="IYHXJXVAZA",rtseq=16,rtrep=.f.,;
    cFormVar = "w_AG_EMAIL", cQueryName = "AG_EMAIL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di posta elettronica",;
    HelpContextID = 208550830,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=139, Top=141, InputMask=replicate('X',254)

  add object oAG_EMPEC_1_17 as StdField with uid="LRQFCOFOOE",rtseq=17,rtrep=.f.,;
    cFormVar = "w_AG_EMPEC", cQueryName = "AG_EMPEC",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di posta elettronica certificata (PEC)",;
    HelpContextID = 43107401,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=139, Top=168, InputMask=replicate('X',254)

  add object oAGCATPRO_1_18 as StdField with uid="OYFZYRPFTW",rtseq=18,rtrep=.f.,;
    cFormVar = "w_AGCATPRO", cQueryName = "AGCATPRO",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria provvigioni inesistente o non di tipo agenti",;
    ToolTipText = "Codice della categoria provvigioni associata all'agente",;
    HelpContextID = 50070613,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=139, Top=198, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUPRO", cZoomOnZoom="GSAR_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_AGCATPRO"

  func oAGCATPRO_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGCATPRO_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAGCATPRO_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPRO','*','GPCODICE',cp_AbsName(this.parent,'oAGCATPRO_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGP',"Categorie provvigioni",'GSAR_AGE.GRUPRO_VZM',this.parent.oContained
  endproc
  proc oAGCATPRO_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_AGCATPRO
     i_obj.ecpSave()
  endproc

  add object oDESGPP_1_19 as StdField with uid="AGIXTHFCWX",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESGPP", cQueryName = "DESGPP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 46334518,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=208, Top=198, InputMask=replicate('X',35)

  add object oAGCODENA_1_20 as StdField with uid="HYVOPMQTYN",rtseq=20,rtrep=.f.,;
    cFormVar = "w_AGCODENA", cQueryName = "AGCODENA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice ENASARCO dell'agente",;
    HelpContextID = 150338489,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=139, Top=225, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',15)


  add object oAGTIPAGE_1_21 as StdCombo with uid="BCXWNPXTOM",rtseq=21,rtrep=.f.,left=345,top=225,width=117,height=21;
    , ToolTipText = "Agente o responsabile di area";
    , HelpContextID = 63247435;
    , cFormVar="w_AGTIPAGE",RowSource=""+"Agente,"+"Capo area", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAGTIPAGE_1_21.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oAGTIPAGE_1_21.GetRadio()
    this.Parent.oContained.w_AGTIPAGE = this.RadioValue()
    return .t.
  endfunc

  func oAGTIPAGE_1_21.SetRadio()
    this.Parent.oContained.w_AGTIPAGE=trim(this.Parent.oContained.w_AGTIPAGE)
    this.value = ;
      iif(this.Parent.oContained.w_AGTIPAGE=='A',1,;
      iif(this.Parent.oContained.w_AGTIPAGE=='C',2,;
      0))
  endfunc

  add object oAGCZOAGE_1_22 as StdField with uid="KQXRRZQLJK",rtseq=22,rtrep=.f.,;
    cFormVar = "w_AGCZOAGE", cQueryName = "AGCZOAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice agente incongruente oppure obsoleto",;
    ToolTipText = "Codice dell'eventuale capo area",;
    HelpContextID = 63243339,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=139, Top=252, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_AGCZOAGE"

  func oAGCZOAGE_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGTIPAGE='A')
    endwith
   endif
  endfunc

  func oAGCZOAGE_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGCZOAGE_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAGCZOAGE_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oAGCZOAGE_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"",'GSAR1AGE.AGENTI_VZM',this.parent.oContained
  endproc
  proc oAGCZOAGE_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_AGCZOAGE
     i_obj.ecpSave()
  endproc

  add object oDESAGE_1_23 as StdField with uid="EAFTBXGOPC",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 120390198,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=208, Top=252, InputMask=replicate('X',35)

  add object oAGCODFOR_1_24 as StdField with uid="IMYZZPBJWY",rtseq=24,rtrep=.f.,;
    cFormVar = "w_AGCODFOR", cQueryName = "AGCODFOR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice fornitore inesistente oppure obsoleto",;
    ToolTipText = "Eventuale codice fornitore di riferimento",;
    HelpContextID = 133561256,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=139, Top=288, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_AGCODFOR"

  func oAGCODFOR_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_REVI='N' or .w_FLGCPZ<>'S' or .cFunction='Load')
    endwith
   endif
  endfunc

  func oAGCODFOR_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGCODFOR_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAGCODFOR_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oAGCODFOR_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori / agenti",'GSAR_AGE.CONTI_VZM',this.parent.oContained
  endproc
  proc oAGCODFOR_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_AGCODFOR
     i_obj.ecpSave()
  endproc

  add object oDESCRI_1_25 as StdField with uid="ZYXQEKRFJE",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 199164470,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=264, Top=288, InputMask=replicate('X',40)

  add object oAGFLAZIE_1_26 as StdRadio with uid="NKQBMPUNFW",rtseq=26,rtrep=.f.,left=129, top=339, width=135,height=47;
    , ToolTipText = "Tipo di contributo dovuto";
    , cFormVar="w_AGFLAZIE", ButtonCount=3, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oAGFLAZIE_1_26.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Fondo previdenza"
      this.Buttons(1).HelpContextID = 69782453
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Fondo assistenza"
      this.Buttons(2).HelpContextID = 69782453
      this.Buttons(2).Top=15
      this.Buttons(3).Caption="Nessun contributo"
      this.Buttons(3).HelpContextID = 69782453
      this.Buttons(3).Top=30
      this.SetAll("Width",133)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Tipo di contributo dovuto")
      StdRadio::init()
    endproc

  func oAGFLAZIE_1_26.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oAGFLAZIE_1_26.GetRadio()
    this.Parent.oContained.w_AGFLAZIE = this.RadioValue()
    return .t.
  endfunc

  func oAGFLAZIE_1_26.SetRadio()
    this.Parent.oContained.w_AGFLAZIE=trim(this.Parent.oContained.w_AGFLAZIE)
    this.value = ;
      iif(this.Parent.oContained.w_AGFLAZIE=='I',1,;
      iif(this.Parent.oContained.w_AGFLAZIE=='S',2,;
      iif(this.Parent.oContained.w_AGFLAZIE=='N',3,;
      0)))
  endfunc

  add object oAGFLESCL_1_27 as StdRadio with uid="FSGTXYUFZT",rtseq=27,rtrep=.f.,left=267, top=339, width=117,height=32;
    , ToolTipText = "Monomandatario/esclusivo o plurimandatario/non esclusivo";
    , cFormVar="w_AGFLESCL", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oAGFLESCL_1_27.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Monomandatario"
      this.Buttons(1).HelpContextID = 85406802
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Plurimandatario"
      this.Buttons(2).HelpContextID = 85406802
      this.Buttons(2).Top=15
      this.SetAll("Width",115)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Monomandatario/esclusivo o plurimandatario/non esclusivo")
      StdRadio::init()
    endproc

  func oAGFLESCL_1_27.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oAGFLESCL_1_27.GetRadio()
    this.Parent.oContained.w_AGFLESCL = this.RadioValue()
    return .t.
  endfunc

  func oAGFLESCL_1_27.SetRadio()
    this.Parent.oContained.w_AGFLESCL=trim(this.Parent.oContained.w_AGFLESCL)
    this.value = ;
      iif(this.Parent.oContained.w_AGFLESCL=='M',1,;
      iif(this.Parent.oContained.w_AGFLESCL=='P',2,;
      0))
  endfunc

  add object oAGFLFARI_1_28 as StdRadio with uid="WZNVLXYLFZ",rtseq=28,rtrep=.f.,left=402, top=339, width=179,height=32;
    , ToolTipText = "Seleziona il tipo documento da emettere";
    , cFormVar="w_AGFLFARI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oAGFLFARI_1_28.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Emette fattura"
      this.Buttons(1).HelpContextID = 52900943
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Emette ricevuta"
      this.Buttons(2).HelpContextID = 52900943
      this.Buttons(2).Top=15
      this.SetAll("Width",177)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona il tipo documento da emettere")
      StdRadio::init()
    endproc

  func oAGFLFARI_1_28.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'R',;
    space(1))))
  endfunc
  func oAGFLFARI_1_28.GetRadio()
    this.Parent.oContained.w_AGFLFARI = this.RadioValue()
    return .t.
  endfunc

  func oAGFLFARI_1_28.SetRadio()
    this.Parent.oContained.w_AGFLFARI=trim(this.Parent.oContained.w_AGFLFARI)
    this.value = ;
      iif(this.Parent.oContained.w_AGFLFARI=='F',1,;
      iif(this.Parent.oContained.w_AGFLFARI=='R',2,;
      0))
  endfunc

  add object oAGSCOPAG_1_29 as StdCheck with uid="JLCXMEEFAF",rtseq=29,rtrep=.f.,left=267, top=375, caption="Sconto pagamento",;
    ToolTipText = "Se attivo: il calcolo delle provvigioni � fatto al netto dello sconto pagamento",;
    HelpContextID = 45024333,;
    cFormVar="w_AGSCOPAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGSCOPAG_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAGSCOPAG_1_29.GetRadio()
    this.Parent.oContained.w_AGSCOPAG = this.RadioValue()
    return .t.
  endfunc

  func oAGSCOPAG_1_29.SetRadio()
    this.Parent.oContained.w_AGSCOPAG=trim(this.Parent.oContained.w_AGSCOPAG)
    this.value = ;
      iif(this.Parent.oContained.w_AGSCOPAG=='S',1,;
      0)
  endfunc

  add object oAGFLFIRR_1_30 as StdCheck with uid="RQYVOJHDGO",rtseq=30,rtrep=.f.,left=402, top=376, caption="Soggetto a F.I.R.R.",;
    ToolTipText = "Soggetto a versamento F.I.R.R.",;
    HelpContextID = 187118680,;
    cFormVar="w_AGFLFIRR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGFLFIRR_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAGFLFIRR_1_30.GetRadio()
    this.Parent.oContained.w_AGFLFIRR = this.RadioValue()
    return .t.
  endfunc

  func oAGFLFIRR_1_30.SetRadio()
    this.Parent.oContained.w_AGFLFIRR=trim(this.Parent.oContained.w_AGFLFIRR)
    this.value = ;
      iif(this.Parent.oContained.w_AGFLFIRR=='S',1,;
      0)
  endfunc

  add object oAGRITIRP_1_31 as StdField with uid="BPIVIFIISR",rtseq=31,rtrep=.f.,;
    cFormVar = "w_AGRITIRP", cQueryName = "AGRITIRP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale ritenuta IRPEF",;
    HelpContextID = 201651286,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=230, Top=398, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oAGPERIMP_1_32 as StdField with uid="VIQXGCXQPR",rtseq=32,rtrep=.f.,;
    cFormVar = "w_AGPERIMP", cQueryName = "AGPERIMP",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale imponibile",;
    HelpContextID = 69151658,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=467, Top=398, cSayPict='"999.99"', cGetPict='"999.99"'

  func oAGPERIMP_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not empty(.w_AGRITIRP))
    endwith
   endif
  endfunc

  add object oAGINIENA_1_33 as StdField with uid="OZBAZHAEQG",rtseq=33,rtrep=.f.,;
    cFormVar = "w_AGINIENA", cQueryName = "AGINIENA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data inizio mandato � maggiore della data di fine mandato",;
    ToolTipText = "Data inizio mandato",;
    HelpContextID = 145136569,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=230, Top=425

  func oAGINIENA_1_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_AGINIENA<=.w_AGFINENA) or (empty(.w_AGFINENA)))
    endwith
    return bRes
  endfunc

  add object oAGFINENA_1_34 as StdField with uid="CAXXSVMBRR",rtseq=34,rtrep=.f.,;
    cFormVar = "w_AGFINENA", cQueryName = "AGFINENA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di fine mandato non pu� essere maggiore della data obsolescenza o la data inizio mandato � maggiore della data di fine mandato",;
    ToolTipText = "Data fine mandato",;
    HelpContextID = 140233657,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=467, Top=425

  func oAGFINENA_1_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_AGDTOBSO>=.w_AGFINENA) OR EMPTY(.w_AGDTOBSO)) AND ((.w_AGINIENA<=.w_AGFINENA) or (empty(.w_AGFINENA))))
    endwith
    return bRes
  endfunc

  add object oAGCHKSTA_1_47 as StdCheck with uid="HDBTXBPKLN",rtseq=37,rtrep=.f.,left=15, top=469, caption="Stampa",;
    HelpContextID = 91423815,;
    cFormVar="w_AGCHKSTA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGCHKSTA_1_47.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGCHKSTA_1_47.GetRadio()
    this.Parent.oContained.w_AGCHKSTA = this.RadioValue()
    return .t.
  endfunc

  func oAGCHKSTA_1_47.SetRadio()
    this.Parent.oContained.w_AGCHKSTA=trim(this.Parent.oContained.w_AGCHKSTA)
    this.value = ;
      iif(this.Parent.oContained.w_AGCHKSTA=='S',1,;
      0)
  endfunc

  func oAGCHKSTA_1_47.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc

  add object oAGCHKMAI_1_48 as StdCheck with uid="VNSLKUJWHZ",rtseq=38,rtrep=.f.,left=110, top=469, caption="E-mail",;
    HelpContextID = 259195983,;
    cFormVar="w_AGCHKMAI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGCHKMAI_1_48.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGCHKMAI_1_48.GetRadio()
    this.Parent.oContained.w_AGCHKMAI = this.RadioValue()
    return .t.
  endfunc

  func oAGCHKMAI_1_48.SetRadio()
    this.Parent.oContained.w_AGCHKMAI=trim(this.Parent.oContained.w_AGCHKMAI)
    this.value = ;
      iif(this.Parent.oContained.w_AGCHKMAI=='S',1,;
      0)
  endfunc

  func oAGCHKMAI_1_48.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc

  add object oAGCHKPEC_1_49 as StdCheck with uid="NMCEYCLFEW",rtseq=39,rtrep=.f.,left=191, top=469, caption="PEC",;
    HelpContextID = 41092169,;
    cFormVar="w_AGCHKPEC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGCHKPEC_1_49.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGCHKPEC_1_49.GetRadio()
    this.Parent.oContained.w_AGCHKPEC = this.RadioValue()
    return .t.
  endfunc

  func oAGCHKPEC_1_49.SetRadio()
    this.Parent.oContained.w_AGCHKPEC=trim(this.Parent.oContained.w_AGCHKPEC)
    this.value = ;
      iif(this.Parent.oContained.w_AGCHKPEC=='S',1,;
      0)
  endfunc

  func oAGCHKPEC_1_49.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc

  add object oAGCHKFAX_1_50 as StdCheck with uid="ZXITRVALRT",rtseq=40,rtrep=.f.,left=268, top=469, caption="FAX",;
    HelpContextID = 141755486,;
    cFormVar="w_AGCHKFAX", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGCHKFAX_1_50.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGCHKFAX_1_50.GetRadio()
    this.Parent.oContained.w_AGCHKFAX = this.RadioValue()
    return .t.
  endfunc

  func oAGCHKFAX_1_50.SetRadio()
    this.Parent.oContained.w_AGCHKFAX=trim(this.Parent.oContained.w_AGCHKFAX)
    this.value = ;
      iif(this.Parent.oContained.w_AGCHKFAX=='S',1,;
      0)
  endfunc

  func oAGCHKFAX_1_50.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc

  add object oAGCHKCPZ_1_51 as StdCheck with uid="VMADKYIRDV",rtseq=41,rtrep=.f.,left=346, top=469, caption="Web Application",;
    ToolTipText = "Se attivo: consente l'invio dei documenti all'agente su Web Application",;
    HelpContextID = 91423840,;
    cFormVar="w_AGCHKCPZ", bObbl = .f. , nPag = 1;
    , TABSTOP=.F.;
   , bGlobalFont=.t.


  func oAGCHKCPZ_1_51.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGCHKCPZ_1_51.GetRadio()
    this.Parent.oContained.w_AGCHKCPZ = this.RadioValue()
    return .t.
  endfunc

  func oAGCHKCPZ_1_51.SetRadio()
    this.Parent.oContained.w_AGCHKCPZ=trim(this.Parent.oContained.w_AGCHKCPZ)
    this.value = ;
      iif(this.Parent.oContained.w_AGCHKCPZ=='S',1,;
      0)
  endfunc

  func oAGCHKCPZ_1_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_IZCP$'SA'  or g_CPIN='S') and g_DMIP<>'S' and .w_AGFLGCPZ='S')
    endwith
   endif
  endfunc

  func oAGCHKCPZ_1_51.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc

  add object oAGCHKPTL_1_52 as StdCheck with uid="RFDRJEBWSG",rtseq=42,rtrep=.f.,left=473, top=469, caption="PostaLite",;
    HelpContextID = 41092178,;
    cFormVar="w_AGCHKPTL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGCHKPTL_1_52.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGCHKPTL_1_52.GetRadio()
    this.Parent.oContained.w_AGCHKPTL = this.RadioValue()
    return .t.
  endfunc

  func oAGCHKPTL_1_52.SetRadio()
    this.Parent.oContained.w_AGCHKPTL=trim(this.Parent.oContained.w_AGCHKPTL)
    this.value = ;
      iif(this.Parent.oContained.w_AGCHKPTL=='S',1,;
      0)
  endfunc

  func oAGCHKPTL_1_52.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc

  add object oAGDTINIT_1_53 as StdField with uid="DQWZFUJFRN",rtseq=43,rtrep=.f.,;
    cFormVar = "w_AGDTINIT", cQueryName = "AGDTINIT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio rapporto",;
    HelpContextID = 262204326,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=257, Top=503

  add object oAGDTOBSO_1_54 as StdField with uid="JFTBAFSCFS",rtseq=44,rtrep=.f.,;
    cFormVar = "w_AGDTOBSO", cQueryName = "AGDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di obsolescenza non pu� essere minore della data di fine mandato",;
    ToolTipText = "Data di fine rapporto",;
    HelpContextID = 79631445,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=467, Top=503

  func oAGDTOBSO_1_54.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_AGFINENA<=.w_AGDTOBSO) OR EMPTY(.w_AGDTOBSO))
    endwith
    return bRes
  endfunc


  add object oObj_1_65 as cp_runprogram with uid="KHDQSVKJDR",left=-4, top=540, width=172,height=17,;
    caption='gsar_bge(C)',;
   bGlobalFont=.t.,;
    prg="gsar_bge('C')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 169744309


  add object oObj_1_66 as cp_runprogram with uid="CYQATSPUKP",left=170, top=540, width=172,height=17,;
    caption='gsar_bge(O)',;
   bGlobalFont=.t.,;
    prg="gsar_bge('O')",;
    cEvent = "Update start",;
    nPag=1;
    , HelpContextID = 169741237


  add object oObj_1_68 as cp_runprogram with uid="TGMDUHSNGY",left=345, top=540, width=172,height=17,;
    caption='gsar_bge(M)',;
   bGlobalFont=.t.,;
    prg="gsar_bge('M')",;
    cEvent = "Edit Started",;
    nPag=1;
    , HelpContextID = 169741749

  add object oStr_1_37 as StdString with uid="WDUQMFPUCQ",Visible=.t., Left=5, Top=252,;
    Alignment=1, Width=133, Height=15,;
    Caption="Capo area:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="TYMGTFOUSV",Visible=.t., Left=5, Top=11,;
    Alignment=1, Width=133, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_39 as StdString with uid="KHWIVLEPSU",Visible=.t., Left=5, Top=37,;
    Alignment=1, Width=133, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="IFGTBEYLQP",Visible=.t., Left=5, Top=63,;
    Alignment=1, Width=133, Height=15,;
    Caption="CAP - localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="YHQMVCNMSU",Visible=.t., Left=466, Top=63,;
    Alignment=1, Width=36, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="PIQGQENOAF",Visible=.t., Left=5, Top=89,;
    Alignment=1, Width=133, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="JXRRLAEALM",Visible=.t., Left=5, Top=225,;
    Alignment=1, Width=133, Height=15,;
    Caption="Codice ENASARCO:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="MMLSFTRIEM",Visible=.t., Left=5, Top=288,;
    Alignment=1, Width=133, Height=15,;
    Caption="Rif.fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="XVUSGTETNT",Visible=.t., Left=288, Top=115,;
    Alignment=1, Width=57, Height=15,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="YVRWYURHSI",Visible=.t., Left=5, Top=198,;
    Alignment=1, Width=133, Height=15,;
    Caption="Categoria provvigioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="IFTTSMUGAL",Visible=.t., Left=162, Top=504,;
    Alignment=1, Width=92, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="WSGHYSYKKQ",Visible=.t., Left=343, Top=504,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="XKAHWBBODB",Visible=.t., Left=272, Top=225,;
    Alignment=1, Width=69, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="PKGTBTGYSO",Visible=.t., Left=128, Top=317,;
    Alignment=0, Width=260, Height=15,;
    Caption="Caratteristiche agente"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="TEXFPHQALV",Visible=.t., Left=120, Top=398,;
    Alignment=0, Width=108, Height=15,;
    Caption="% Ritenuta IRPEF"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="VJEAHIEBUB",Visible=.t., Left=370, Top=398,;
    Alignment=1, Width=94, Height=15,;
    Caption="% Imponibile"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="YJHHFGHRFB",Visible=.t., Left=87, Top=425,;
    Alignment=1, Width=141, Height=18,;
    Caption="Data inizio mandato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="NEWOZKXXSS",Visible=.t., Left=328, Top=425,;
    Alignment=1, Width=136, Height=18,;
    Caption="Data fine mandato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_73 as StdString with uid="RCGOPLUALE",Visible=.t., Left=10, Top=452,;
    Alignment=0, Width=157, Height=18,;
    Caption="Processi documentali"  ;
  , bGlobalFont=.t.

  func oStr_1_73.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S')
    endwith
  endfunc

  add object oStr_1_75 as StdString with uid="UTXAXLXMKU",Visible=.t., Left=5, Top=145,;
    Alignment=1, Width=133, Height=18,;
    Caption="E@mail addr.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_76 as StdString with uid="NIMSNEAEKN",Visible=.t., Left=5, Top=115,;
    Alignment=1, Width=133, Height=18,;
    Caption="Telefax:"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="UXSUAZJXDL",Visible=.t., Left=5, Top=172,;
    Alignment=1, Width=133, Height=18,;
    Caption="PEC addr:"  ;
  , bGlobalFont=.t.

  add object oBox_1_61 as StdBox with uid="XTFHNPBSUT",left=128, top=334, width=448,height=1

  add object oBox_1_74 as StdBox with uid="GJQSEDBYDA",left=1, top=469, width=577,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_age','AGENTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AGCODAGE=AGENTI.AGCODAGE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
