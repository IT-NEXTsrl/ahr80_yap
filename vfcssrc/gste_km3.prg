* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_km3                                                        *
*              Divisione scadenza                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_52]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-28                                                      *
* Last revis.: 2007-07-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_km3",oParentObject))

* --- Class definition
define class tgste_km3 as StdForm
  Top    = 84
  Left   = 32

  * --- Standard Properties
  Width  = 319
  Height = 286
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-12"
  HelpContextID=199898217
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  cPrg = "gste_km3"
  cComment = "Divisione scadenza"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_CPROWNUM = 0
  w_PTCODVAL = space(3)
  w_PTDATSCA = ctod('  /  /  ')
  w_PTTOTIMP = 0
  w_SIMVAL = space(5)
  w_DECTOT = 0
  w_CALCPIC = 0
  w_DATA1 = ctod('  /  /  ')
  w_IMPO1 = 0
  w_DATA2 = ctod('  /  /  ')
  o_DATA2 = ctod('  /  /  ')
  w_IMPO2 = 0
  w_DATA3 = ctod('  /  /  ')
  o_DATA3 = ctod('  /  /  ')
  w_IMPO3 = 0
  w_DATA4 = ctod('  /  /  ')
  o_DATA4 = ctod('  /  /  ')
  w_IMPO4 = 0
  w_DATA5 = ctod('  /  /  ')
  o_DATA5 = ctod('  /  /  ')
  w_IMPO5 = 0
  w_TIMPO = 0
  w_FLSALD1 = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_km3Pag1","gste_km3",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATA1_1_10
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PTSERIAL=space(10)
      .w_PTROWORD=0
      .w_CPROWNUM=0
      .w_PTCODVAL=space(3)
      .w_PTDATSCA=ctod("  /  /  ")
      .w_PTTOTIMP=0
      .w_SIMVAL=space(5)
      .w_DECTOT=0
      .w_CALCPIC=0
      .w_DATA1=ctod("  /  /  ")
      .w_IMPO1=0
      .w_DATA2=ctod("  /  /  ")
      .w_IMPO2=0
      .w_DATA3=ctod("  /  /  ")
      .w_IMPO3=0
      .w_DATA4=ctod("  /  /  ")
      .w_IMPO4=0
      .w_DATA5=ctod("  /  /  ")
      .w_IMPO5=0
      .w_TIMPO=0
      .w_FLSALD1=space(1)
      .w_PTSERIAL=oParentObject.w_PTSERIAL
      .w_PTROWORD=oParentObject.w_PTROWORD
      .w_CPROWNUM=oParentObject.w_CPROWNUM
      .w_PTCODVAL=oParentObject.w_PTCODVAL
      .w_PTDATSCA=oParentObject.w_PTDATSCA
      .w_PTTOTIMP=oParentObject.w_PTTOTIMP
      .w_FLSALD1=oParentObject.w_FLSALD1
          .DoRTCalc(1,3,.f.)
        .w_PTCODVAL = .w_PTCODVAL
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PTCODVAL))
          .link_1_4('Full')
        endif
          .DoRTCalc(5,8,.f.)
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        .w_DATA1 = .w_PTDATSCA
        .w_IMPO1 = .w_PTTOTIMP
          .DoRTCalc(12,12,.f.)
        .w_IMPO2 = IIF(EMPTY(.w_DATA2), .w_IMPO2, .w_PTTOTIMP-.w_TIMPO)
          .DoRTCalc(14,14,.f.)
        .w_IMPO3 = IIF(EMPTY(.w_DATA3), .w_IMPO3, .w_PTTOTIMP-.w_TIMPO)
          .DoRTCalc(16,16,.f.)
        .w_IMPO4 = IIF(EMPTY(.w_DATA4), .w_IMPO4, .w_PTTOTIMP-.w_TIMPO)
          .DoRTCalc(18,18,.f.)
        .w_IMPO5 = IIF(EMPTY(.w_DATA5),.w_IMPO5, .w_PTTOTIMP-.w_TIMPO)
        .w_TIMPO = .w_IMPO1+.w_IMPO2+.w_IMPO3+.w_IMPO4+.w_IMPO5
    endwith
    this.DoRTCalc(21,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PTSERIAL=.w_PTSERIAL
      .oParentObject.w_PTROWORD=.w_PTROWORD
      .oParentObject.w_CPROWNUM=.w_CPROWNUM
      .oParentObject.w_PTCODVAL=.w_PTCODVAL
      .oParentObject.w_PTDATSCA=.w_PTDATSCA
      .oParentObject.w_PTTOTIMP=.w_PTTOTIMP
      .oParentObject.w_FLSALD1=.w_FLSALD1
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_1_4('Full')
        .DoRTCalc(5,8,.t.)
            .w_CALCPIC = DEFPIC(.w_DECTOT)
        .DoRTCalc(10,12,.t.)
        if .o_DATA2<>.w_DATA2
            .w_IMPO2 = IIF(EMPTY(.w_DATA2), .w_IMPO2, .w_PTTOTIMP-.w_TIMPO)
        endif
        .DoRTCalc(14,14,.t.)
        if .o_DATA3<>.w_DATA3
            .w_IMPO3 = IIF(EMPTY(.w_DATA3), .w_IMPO3, .w_PTTOTIMP-.w_TIMPO)
        endif
        .DoRTCalc(16,16,.t.)
        if .o_DATA4<>.w_DATA4
            .w_IMPO4 = IIF(EMPTY(.w_DATA4), .w_IMPO4, .w_PTTOTIMP-.w_TIMPO)
        endif
        .DoRTCalc(18,18,.t.)
        if .o_DATA5<>.w_DATA5
            .w_IMPO5 = IIF(EMPTY(.w_DATA5),.w_IMPO5, .w_PTTOTIMP-.w_TIMPO)
        endif
            .w_TIMPO = .w_IMPO1+.w_IMPO2+.w_IMPO3+.w_IMPO4+.w_IMPO5
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDATA2_1_12.enabled = this.oPgFrm.Page1.oPag.oDATA2_1_12.mCond()
    this.oPgFrm.Page1.oPag.oIMPO2_1_13.enabled = this.oPgFrm.Page1.oPag.oIMPO2_1_13.mCond()
    this.oPgFrm.Page1.oPag.oDATA3_1_14.enabled = this.oPgFrm.Page1.oPag.oDATA3_1_14.mCond()
    this.oPgFrm.Page1.oPag.oIMPO3_1_15.enabled = this.oPgFrm.Page1.oPag.oIMPO3_1_15.mCond()
    this.oPgFrm.Page1.oPag.oDATA4_1_16.enabled = this.oPgFrm.Page1.oPag.oDATA4_1_16.mCond()
    this.oPgFrm.Page1.oPag.oIMPO4_1_17.enabled = this.oPgFrm.Page1.oPag.oIMPO4_1_17.mCond()
    this.oPgFrm.Page1.oPag.oDATA5_1_18.enabled = this.oPgFrm.Page1.oPag.oDATA5_1_18.mCond()
    this.oPgFrm.Page1.oPag.oIMPO5_1_19.enabled = this.oPgFrm.Page1.oPag.oIMPO5_1_19.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PTCODVAL
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PTCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PTCODVAL)
            select VACODVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PTCODVAL = space(3)
      endif
      this.w_SIMVAL = space(5)
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPTDATSCA_1_5.value==this.w_PTDATSCA)
      this.oPgFrm.Page1.oPag.oPTDATSCA_1_5.value=this.w_PTDATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oPTTOTIMP_1_6.value==this.w_PTTOTIMP)
      this.oPgFrm.Page1.oPag.oPTTOTIMP_1_6.value=this.w_PTTOTIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_7.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_7.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_10.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_10.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPO1_1_11.value==this.w_IMPO1)
      this.oPgFrm.Page1.oPag.oIMPO1_1_11.value=this.w_IMPO1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_12.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_12.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPO2_1_13.value==this.w_IMPO2)
      this.oPgFrm.Page1.oPag.oIMPO2_1_13.value=this.w_IMPO2
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA3_1_14.value==this.w_DATA3)
      this.oPgFrm.Page1.oPag.oDATA3_1_14.value=this.w_DATA3
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPO3_1_15.value==this.w_IMPO3)
      this.oPgFrm.Page1.oPag.oIMPO3_1_15.value=this.w_IMPO3
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA4_1_16.value==this.w_DATA4)
      this.oPgFrm.Page1.oPag.oDATA4_1_16.value=this.w_DATA4
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPO4_1_17.value==this.w_IMPO4)
      this.oPgFrm.Page1.oPag.oIMPO4_1_17.value=this.w_IMPO4
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA5_1_18.value==this.w_DATA5)
      this.oPgFrm.Page1.oPag.oDATA5_1_18.value=this.w_DATA5
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPO5_1_19.value==this.w_IMPO5)
      this.oPgFrm.Page1.oPag.oIMPO5_1_19.value=this.w_IMPO5
    endif
    if not(this.oPgFrm.Page1.oPag.oTIMPO_1_20.value==this.w_TIMPO)
      this.oPgFrm.Page1.oPag.oTIMPO_1_20.value=this.w_TIMPO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATA2 = this.w_DATA2
    this.o_DATA3 = this.w_DATA3
    this.o_DATA4 = this.w_DATA4
    this.o_DATA5 = this.w_DATA5
    return

enddefine

* --- Define pages as container
define class tgste_km3Pag1 as StdContainer
  Width  = 315
  height = 286
  stdWidth  = 315
  stdheight = 286
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPTDATSCA_1_5 as StdField with uid="JKRERRKNMX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PTDATSCA", cQueryName = "PTDATSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di scadenza",;
    HelpContextID = 211510583,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=10, Top=23

  add object oPTTOTIMP_1_6 as StdField with uid="OKDRNINZXP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PTTOTIMP", cQueryName = "PTTOTIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 223713978,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=109, Top=23, cSayPict="v_PV(40+VVL)", cGetPict="V_GV(40+VVL)"

  add object oSIMVAL_1_7 as StdField with uid="NHOUHGCIRU",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 192878042,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=260, Top=23, InputMask=replicate('X',5)

  add object oDATA1_1_10 as StdField with uid="TXHKQYWVHW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 143896266,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=10, Top=76

  add object oIMPO1_1_11 as StdField with uid="XPEZSMUJZZ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_IMPO1", cQueryName = "IMPO1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 142991994,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=109, Top=76, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oDATA2_1_12 as StdField with uid="MFEEGBSVAT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 142847690,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=10, Top=101

  func oDATA2_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DATA1))
    endwith
   endif
  endfunc

  add object oIMPO2_1_13 as StdField with uid="ZJIGBBTEDK",rtseq=13,rtrep=.f.,;
    cFormVar = "w_IMPO2", cQueryName = "IMPO2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 141943418,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=109, Top=101, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oIMPO2_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DATA2) AND .w_IMPO1<>0)
    endwith
   endif
  endfunc

  add object oDATA3_1_14 as StdField with uid="CUMCRAIAFY",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATA3", cQueryName = "DATA3",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 141799114,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=10, Top=126

  func oDATA3_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DATA2))
    endwith
   endif
  endfunc

  add object oIMPO3_1_15 as StdField with uid="UZSAKEWUDJ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_IMPO3", cQueryName = "IMPO3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 140894842,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=109, Top=126, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oIMPO3_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DATA3) AND .w_IMPO2<>0)
    endwith
   endif
  endfunc

  add object oDATA4_1_16 as StdField with uid="FVCTTNJRMY",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DATA4", cQueryName = "DATA4",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 140750538,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=10, Top=151

  func oDATA4_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DATA3))
    endwith
   endif
  endfunc

  add object oIMPO4_1_17 as StdField with uid="TBNWZCYNAP",rtseq=17,rtrep=.f.,;
    cFormVar = "w_IMPO4", cQueryName = "IMPO4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 139846266,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=109, Top=151, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oIMPO4_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DATA4) AND .w_IMPO3<>0)
    endwith
   endif
  endfunc

  add object oDATA5_1_18 as StdField with uid="YGVHSUQDLS",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DATA5", cQueryName = "DATA5",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 139701962,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=10, Top=176

  func oDATA5_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DATA4))
    endwith
   endif
  endfunc

  add object oIMPO5_1_19 as StdField with uid="HBOIKAABXY",rtseq=19,rtrep=.f.,;
    cFormVar = "w_IMPO5", cQueryName = "IMPO5",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 138797690,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=109, Top=176, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oIMPO5_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DATA5) AND .w_IMPO4<>0)
    endwith
   endif
  endfunc

  add object oTIMPO_1_20 as StdField with uid="NHMCVJZFNZ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_TIMPO", cQueryName = "TIMPO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 111482314,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=109, Top=209, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"


  add object oBtn_1_21 as StdButton with uid="EYZAFZSEKT",left=210, top=235, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per dividere la scadenza";
    , HelpContextID = 199869466;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        do GSTE_BM3 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="FGVPPXXLLE",left=261, top=235, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare";
    , HelpContextID = 192580794;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_24 as StdString with uid="LHMMNRXXWL",Visible=.t., Left=9, Top=57,;
    Alignment=2, Width=84, Height=15,;
    Caption="Nuove date"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="MZKNJLMAOJ",Visible=.t., Left=114, Top=57,;
    Alignment=2, Width=134, Height=15,;
    Caption="Nuovi importi"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="NXFFBMKRSB",Visible=.t., Left=12, Top=6,;
    Alignment=2, Width=75, Height=15,;
    Caption="Scadenza"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="AYLIUHDSRB",Visible=.t., Left=112, Top=6,;
    Alignment=2, Width=135, Height=15,;
    Caption="Importo"  ;
  , bGlobalFont=.t.

  add object oBox_1_23 as StdBox with uid="KMBCQUQRPH",left=3, top=48, width=308,height=1

  add object oBox_1_28 as StdBox with uid="PKKGJIZYYA",left=3, top=204, width=308,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_km3','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
