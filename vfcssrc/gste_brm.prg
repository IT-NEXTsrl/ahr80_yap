* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_brm                                                        *
*              Emissione M.AV. -scrittura                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_71]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-02-20                                                      *
* Last revis.: 2015-06-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_brm",oParentObject)
return(i_retval)

define class tgste_brm as StdBatch
  * --- Local variables
  w_SepRec = space(2)
  w_DatPre = ctod("  /  /  ")
  w_NomeFile = space(40)
  w_NomeFile5 = space(40)
  w_NomeFile4 = space(40)
  w_NomeFile7 = space(40)
  w_FLIBAN = space(1)
  w_Separa = space(2)
  w_Decimali = 0
  w_Handle = 0
  w_Record = space(120)
  w_AziAtt = space(1)
  w_AziSia = space(5)
  w_BanAbi = space(5)
  w_BanCab = space(5)
  w_CliAbi = space(5)
  w_CliCab = space(5)
  w_CliFis = space(16)
  w_CliCod = space(16)
  w_DatCre = space(6)
  w_DatSca = space(6)
  w_NomSup = space(20)
  w_CodDiv = space(1)
  w_Totale = 0
  w_NumDis = space(7)
  w_NumRec = space(7)
  w_Valore = 0
  w_Lunghezza = 0
  w_StrInt = space(1)
  w_StrDec = space(1)
  w_RifDoc = space(1)
  w_TotDoc = space(1)
  w_CodFis = space(16)
  w_ParIva = space(12)
  w_CLIConCor = space(12)
  w_ConCor = space(12)
  w_MESS = space(200)
  w_OK = .f.
  w_OK1 = .f.
  w_CLIFIS = space(16)
  w_LSERIAL = space(10)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_SEGNO = space(1)
  w_NUMPAR = space(31)
  w_LDATSCA = ctod("  /  /  ")
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_RIF = space(40)
  w_CINABI = space(1)
  w_BBAN = space(30)
  w_BA__IBAN = space(35)
  w_ChekIban = space(2)
  w_AN__IBAN = space(35)
  w_Chek = space(2)
  w_BANCA = space(35)
  w_BANCA1 = space(35)
  w_PTNUMEFF = 0
  w_DESPAR = space(1)
  w_oERRORLOG = .NULL.
  w_FILDIS = space(20)
  w_DATIAGG = space(254)
  w_PTDESCRI = space(40)
  * --- WorkFile variables
  ATTIMAST_idx=0
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione file M.AV. secondo tracciato CBI
    * --- Variabili della maschera
    this.w_SepRec = this.oParentObject.oParentObject.w_SepRec
    this.w_DatPre = this.oParentObject.oParentObject.w_DatPre
    this.w_NomeFile = this.oParentObject.oParentObject.w_NomeFile
    this.w_NomeFile5 = this.oParentObject.oParentObject.w_NomeFile5
    this.w_NomeFile4 = this.oParentObject.oParentObject.w_NomeFile4
    this.w_NomeFile7 = this.oParentObject.oParentObject.w_NomeFile7
    this.w_FLIBAN = this.oParentObject.oParentObject.w_FLIBAN
    * --- Variabili del batch precedente
    * --- Variabili locali
    this.w_Separa = iif(this.w_SEPREC="S", chr(13)+chr(10), "")
    this.w_Decimali = GETVALUT( this.oParentObject.w_Valuta, "VADECTOT")
    this.w_Handle = 0
    this.w_Record = space(120)
    this.w_AziAtt = space(35)
    this.w_AziSia = this.oParentObject.w_CodSia
    this.w_BanAbi = space(5)
    this.w_BanCab = space(5)
    this.w_CliAbi = space(5)
    this.w_CliCab = space(5)
    this.w_CliFis = space(16)
    this.w_CliCod = space(16)
    this.w_DatCre = space(6)
    this.w_DatSca = space(6)
    this.w_NomSup = space(20)
    this.w_CodDiv = space(1)
    this.w_Totale = 0
    this.w_NumDis = "0000000"
    this.w_NumRec = "0000000"
    this.w_Valore = 0
    this.w_Lunghezza = 0
    this.w_StrInt = space(1)
    this.w_StrDec = space(1)
    this.w_RifDoc = space(1)
    this.w_TotDoc = space(1)
    this.w_CodFis = space(16)
    this.w_ParIva = space(12)
    this.w_CLIConCor = space(12)
    this.w_ConCor = space(12)
    this.w_OK = .F.
    this.w_OK1 = .F.
    this.w_CINABI = SPACE(1)
    this.w_BBAN = SPACE(30)
    this.w_BA__IBAN = SPACE(35)
    this.w_ChekIban = space(2)
    this.w_AN__IBAN = SPACE(35)
    this.w_Chek = space(2)
    this.w_BANCA = this.oParentObject.w_BADESC
    this.w_BANCA1 = STRTRAN(this.w_BANCA, "'", " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1, '"', " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"?" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"!" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"/" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"\" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"*" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"," , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,":" , " ")
    this.w_DESPAR = This.oParentObject.oParentObject.w_DESPAR
    this.oParentObject.w_GENERA = .F.
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- Creazione file M.AV
    ah_Msg("Creazione file M.AV....",.T.)
    do case
      case this.oParentObject.w_TIPOBAN="C" AND EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
        if !DIRECTORY(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.oParentObject.w_COBANC))
          MKDIR(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.oParentObject.w_COBANC))
        endif
        this.oParentObject.w_PATHFILE = ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.oParentObject.w_COBANC)+ ALLTRIM(IIF(EMPTY(this.oParentObject.w_COBANC), " ", "\"))
      case this.oParentObject.w_TIPOBAN="D" AND EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
        if !DIRECTORY(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.w_BANCA1))
          MKDIR(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.w_BANCA1))
        endif
        this.oParentObject.w_PATHFILE = ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.w_BANCA1)+ ALLTRIM(IIF(EMPTY(this.w_BANCA1), " ", "\"))
    endcase
    if this.oParentObject.w_TIPOPAG="S" AND EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
      if !DIRECTORY(ALLTRIM(this.oParentObject.w_PATHFILE) +"\MAV")
        MKDIR(ALLTRIM(this.oParentObject.w_PATHFILE) +"\MAV")
      endif
      this.oParentObject.w_PATHFILE = ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM("MAV") +"\"
    endif
    if EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
      this.w_Handle =fcreate(ALLTRIM(this.oParentObject.w_PATHFILE) + this.oParentObject.w_PATHFILE1, 0)
    else
      this.w_Handle = fcreate(ALLTRIM(this.oParentObject.w_PATHFILE) + this.oParentObject.w_PATHFILE2 , 0)
    endif
    if this.w_Handle = -1
      ah_ErrorMsg("Non � possibile creare il file delle M.AV.",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Lettura attivita' principale azienda
    * --- Read from ATTIMAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ATTIMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2],.t.,this.ATTIMAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATDESATT"+;
        " from "+i_cTable+" ATTIMAST where ";
            +"ATCODATT = "+cp_ToStrODBC(g_CATAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATDESATT;
        from (i_cTable) where;
            ATCODATT = g_CATAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AziAtt = NVL(cp_ToDate(_read_.ATDESATT),cp_NullValue(_read_.ATDESATT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura degli effetti presenti sul temporaneo e scrittura del file ascii
    select __tmp__
    go top
    * --- Record - IM (record di testa)
    * --- Codice assegnato dalla Sia all'Azienda Mittente
    this.w_AziSia = left( this.w_AziSia + space(5) , 5)
    * --- Controllo su codice assegnato dalla SIa all'Azienda Mittente (creditrice)
    if empty(NVL(this.w_AziSia,""))
      this.w_oERRORLOG.AddMsgLog("Codice Sia per azienda mittente non inserito")     
    endif
    * --- Dati banca ricevente
    this.w_BanAbi = right("00000" + alltrim(NVL(__tmp__.bacodabi,SPACE(5))), 5)
    this.w_BanCab = right(space(5) + alltrim(NVL(__tmp__.bacodcab,SPACE(5))), 5)
    this.w_CINABI = alltrim(nvl(__tmp__.bacinabi, " "))
    this.w_ConCor = Right("000000000000" + alltrim(NVL(UPPER( __tmp__.baconcor),space(12))) , 12)
    this.w_BA__IBAN = alltrim(nvl(__tmp__.ba__iban, space(35)))
    * --- Controllo su codice banca a cui devono essere inviate le disposizioni
    if this.w_BanAbi="00000"
      this.w_oERRORLOG.AddMsgLog("Codice ABI della banca a cui devono essere inviate le disposizioni di incasso (ns. banca) non inserito")     
    endif
    * --- Data creazione file
    this.w_DatCre = dtoc(CP_TODATE(this.w_DatPre))
    this.w_DatCre = left(this.w_DatCre,2) + substr(this.w_DatCre,4,2) + right(this.w_DatCre,2)
    * --- Nome supporto
    * --- Campo di libera composizione. Deve comunque essere univoco: data,ora,minuti,secondi,SIA
    this.w_NomSup = time()
    this.w_NomSup = substr(this.w_NomSup,1,2) + substr(this.w_NomSup,4,2) + substr(this.w_NomSup,7,2)
    this.w_NomSup = left( this.w_DatCre + this.w_NomSup + this.w_AziSia +space(20), 20)
    this.oParentObject.w_FILDIS = this.w_NomSup
    * --- Codice divisa (valuta)
    this.w_CodDiv = iif( this.oParentObject.w_Valuta=g_PERVAL , "E" , "I")
    * --- Composizione record e scrittura
    this.w_Record = space(1)+"IM"
    this.w_Record = this.w_Record + this.w_AziSia + this.w_BanAbi + this.w_DatCre + this.w_NomSup
    this.w_Record = this.w_Record + space(6) + space(68) + this.w_CodDiv + space(1) +space(5)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Ciclo sugli effetti da presentare
    ah_Msg("Scrittura file M.AV....",.T.)
    scan
    * --- Messaggio
    ah_Msg("Lettura effetto n. %1 del %2",.T.,.F.,.F., str(NVL(__tmp__.PTNUMEFF,0),6,0), dtoc(CP_TODATE(__tmp__.PTDATSCA)) )
    * --- Totale Importi Effetti
    this.w_TOTALE = this.w_TOTALE + NVL(__tmp__.PTTOTIMP,0)
    * --- Record - (14)
    * --- Calcolo numero progressivo disposizione (progressivo M.AV.)
    this.w_NumDis = right( "0000000" + alltrim( str(val(this.w_NumDis)+1) ) , 7)
    * --- Calcolo data scadenza
    if empty(CP_TODATE(__tmp__.PTDATSCA))
      this.w_DatSca = space(8)
    else
      this.w_DatSca = dtoc(CP_TODATE(__tmp__.PTDATSCA))
      this.w_DatSca = left(this.w_DatSca,2) + substr(this.w_DatSca,4,2) + right(this.w_DatSca,2)
    endif
    * --- Calcolo importo effetto
    this.w_Valore = NVL(__tmp__.PTTOTIMP,0)
    this.w_Lunghezza = 13
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Dati banca domiciliataria
    this.w_CliAbi = right("00000" + alltrim(NVL(__tmp__.clcodabi,space(5))), 5)
    this.w_CliCab = right("00000" + alltrim(NVL(__tmp__.clcodcab,space(5))), 5)
    * --- Codice cliente
    this.w_CliCod = left( alltrim(NVL( __tmp__.codclien,space(16))) + space(16) , 16)
    * --- Codice Fiscale
    this.w_CodFis = right(alltrim(NVL(__tmp__.ancodfis,space(16))), 16)
    * --- Partita Iva
    this.w_ParIva = right(alltrim(NVL(__tmp__.anpariva,space(12))), 12)
    * --- Conto Corrente del Cliente
    this.w_CLIConCor = left(alltrim(NVL(__tmp__.annumcor,space(12))) + space(12), 12)
    * --- Conto Corrente della banca
    this.w_ConCor = left( alltrim(NVL( __tmp__.baconcor,space(12))) + space(12) , 12)
    * --- Composizione record e scrittura
    this.w_Record = " 14" + this.w_NumDis + space(12) + this.w_DatSca + "07000" + this.w_Valore + "-"
    this.w_Record = this.w_Record + this.w_BanAbi + this.w_BanCab + this.w_ConCor + space(22)
    this.w_Record = this.w_Record + this.w_AziSia + "4" + this.w_CliCod + space(6) + this.w_CodDiv
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_FLIBAN="S"
      * --- Record - (16)
      * --- Coordinate Ordinante
      * --- Composizione codice BBAN
      this.w_BBAN = CALCBBAN(this.w_CINABI," ",this.w_BANABI, this.w_BANCAB, this.w_CONCOR)
      this.w_ChekIban = SUBSTR(this.w_BA__IBAN, 3,2)
      this.w_Record = " 16" + this.w_NumDis +"IT"+ this.w_ChekIban + this.w_CINABI + this.w_BanAbi +this.w_BanCab + this.w_ConCor + space(8) + space(75)
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Record - (20)
    * --- Controllo Formale su descrizione del creditore.
    if empty(NVL(g_RAGAZI,""))
      if this.w_OK1=.F.
        this.w_oERRORLOG.AddMsgLog("Descrizione del creditore non inserita")     
        this.w_OK1 = .T.
      endif
    endif
    * --- Composizione record e scrittura
    this.w_Record = " 20" + this.w_NumDis + left(g_RagAzi+space(24),24) + left(g_IndAzi+space(24),24)
    this.w_Record = this.w_Record + left( trim(g_CapAzi) + " " + trim(g_LocAzi) + " " + trim(g_ProAzi) + space(24) ,24)
    this.w_Record = this.w_Record + left( this.w_AziAtt + space(24) ,24) + space(14)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (30)
    * --- Controllo Formale sul codice fiscale (se persona fisica) oppure su partita IVA.
    if empty(NVL(ANCODFIS,""))
      this.w_CLIFIS = LEFT(NVL(ANPARIVA,space(16))+ space(16), 16)
    else
      this.w_CLIFIS = LEFT(NVL(ANCODFIS,space(16))+ space(16), 16)
    endif
    * --- Controllo su descrizione del cliente debitore
    if empty(NVL(andescri,""))
      this.w_oERRORLOG.AddMsgLog("Descrizione del debitore non inserita")     
    endif
    * --- Composizione record e scrittura
    * --- Controlla che la tipologia della sede sia Pagamento(PA)
    if NVL(__TMP__.DDTIPRIF, SPACE(2))="PA"
      this.w_Record = " 30" + this.w_NumDis + left( NVL(__tmp__.ddnomdes,space(60)) + space(60) , 60) + this.w_CliFis + space(34)
    else
      this.w_Record = " 30" + this.w_NumDis + left( NVL(__tmp__.andescri,space(60)) + space(60) , 60) + this.w_CliFis + space(34)
    endif
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (40)
    * --- Controllo Formale sull'indirizzo, il cap, comune, sigla della provincia.
    if empty(NVL(anindiri,""))
      this.w_oERRORLOG.AddMsgLog("Indirizzo del debitore: %1 non inserito", alltrim(andescri))     
    endif
    if empty(NVL(an___cap,""))
      this.w_oERRORLOG.AddMsgLog("Codice avv. postale del debitore: %1 non inserito", alltrim(andescri))     
    endif
    if empty(NVL(anlocali,""))
      this.w_oERRORLOG.AddMsgLog("Comune del debitore: %1 non inserito", alltrim(andescri))     
    endif
    if empty(NVL(anprovin,""))
      this.w_oERRORLOG.AddMsgLog("Provincia del debitore: %1 non inserita", alltrim(andescri))     
    endif
    * --- Composizione record e scrittura
    * --- Controlla che la tipologia della sede sia Pagamento(PA)
    if nvl(__tmp__.ddtiprif, space(2))="PA"
      this.w_Record = " 40"+ this.w_NumDis + left( NVL(__tmp__.ddindiri,space(30)) + space(30) ,30)
      this.w_Record = this.w_Record + right( "00000" + trim(NVL(__tmp__.dd___cap,space(5))) , 5)
      this.w_Record = this.w_Record + left( NVL(__tmp__.ddlocali,space(22)) + space(22) , 22)
      this.w_Record = this.w_Record + " " + left( NVL(__tmp__.ddprovin,space(2)) + space(2), 2)
      this.w_Record = this.w_Record + space(50) 
    else
      this.w_Record = " 40"+ this.w_NumDis + left( NVL(__tmp__.anindiri,space(30)) + space(30) ,30)
      this.w_Record = this.w_Record + right( "00000" + trim(NVL(__tmp__.an___cap,space(5))) , 5)
      this.w_Record = this.w_Record + left( NVL(__tmp__.anlocali,space(22)) + space(22) , 22)
      this.w_Record = this.w_Record + " " + left( NVL(__tmp__.anprovin,space(2)) + space(2), 2)
      this.w_Record = this.w_Record + space(50) 
    endif
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (50)
    * --- Se � settato il flag ANFLRAGG digito la dicitura 'DOCUMENTI DIVERSI'
    if this.oParentObject.w_MAXAGG<>this.oParentObject.w_MINAGG
      this.w_DATIAGG = ""
    else
      this.w_DATIAGG = __TMP__.DESCRIZ
    endif
    if NVL(__TMP__.CAFLDSPR, " ")="S"
      if  this.w_DESPAR="S"
        this.w_PTDESCRI = __TMP__.PTDESRIG
        this.w_RifDoc = LEFT(alltrim(this.w_DATIAGG) +space(1)+alltrim(this.w_PTDESCRI) +space(90),90)
      else
        this.w_RifDoc = LEFT(this.w_DATIAGG+space(90),90)
      endif
    else
      do case
        case Not Empty(NVL(__TMP__.ANFLRAGG,"")) AND NUMGRUP>1
          this.w_SEGNO = IIF(NVL(__TMP__.PT_SEGNO, " ")="D","A","D")
          this.w_NUMPAR = NVL(__TMP__.PTNUMPAR, SPACE(31))
          this.w_LDATSCA = CP_TODATE(__TMP__.PTDATSCA)
          this.w_CODCON = NVL(__TMP__.CODCLIEN, SPACE(15))
          this.w_TIPCON = NVL(__TMP__.PNTIPCON, " ")
          this.w_LSERIAL = NVL(__TMP__.PTSERIAL, SPACE(10))
          this.w_PTNUMEFF = __TMP__.PTNUMEFF
          this.w_RifDoc = "DOC: "
          VQ_EXEC("QUERY\GSTE4BRF.VQR",this,"temp1")
          SELECT TEMP1
          GO TOP
          SCAN
          this.w_SEGNO = IIF(NVL(PT_SEGNO," ")="A","D","A")
          this.w_RifDoc = alltrim(this.w_RifDoc) + alltrim(str(NVL(temp1.pnnumdoc, 0),15,0))
          this.w_RifDoc = alltrim(this.w_RifDoc)+ iif( empty(NVL(temp1.pnalfdoc,space(10))) , "", "/" +Alltrim( NVL(temp1.pnalfdoc,space(10)) ) )
          this.w_RifDoc = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(temp1.pndatdoc)), 90)
          ENDSCAN
          if USED("TEMP1")
            SELECT TEMP1
            USE
          endif
        case NVL(__TMP__.PTFLRAGG, "")="S" 
          if NVL(__TMP__.PNNUMDOC, 0)>0
            this.w_RifDoc = "DOC: "
            this.w_RifDoc = alltrim(this.w_RifDoc) + alltrim(str(NVL(__tmp__.PNNUMDOC, 0),15,0))
            this.w_RifDoc = alltrim(this.w_RifDoc)+ iif( empty(NVL(__tmp__.PNALFDOC,space(10))) , "", "/" +Alltrim( NVL(__tmp__.PNALFDOC,space(10)) ) )
            this.w_RifDoc = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(__tmp__.PNDATDOC)), 90)
          else
            this.w_SEGNO = IIF(NVL(__TMP__.PT_SEGNO, " ")="D","A","D")
            this.w_CODCON = NVL(__TMP__.CODCLIEN, SPACE(15))
            this.w_TIPCON = NVL(__TMP__.PNTIPCON, " ")
            this.w_NUMPAR = NVL(__TMP__.PTNUMPAR, SPACE(31))
            this.w_LDATSCA = CP_TODATE(__TMP__.PTDATSCA)
            * --- Select from PAR_TITE
            i_nConn=i_TableProp[this.PAR_TITE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select PTSERIAL  from "+i_cTable+" PAR_TITE ";
                  +" where PTNUMPAR="+cp_ToStrODBC(this.w_NUMPAR)+" AND PTDATSCA="+cp_ToStrODBC(this.w_LDATSCA)+" AND PTTIPCON="+cp_ToStrODBC(this.w_TIPCON)+" AND PTCODCON="+cp_ToStrODBC(this.w_CODCON)+" AND PT_SEGNO="+cp_ToStrODBC(this.w_SEGNO)+" AND PTROWORD =-1 AND PTFLRAGG='S'";
                  +" order by PTFLRAGG ";
                   ,"_Curs_PAR_TITE")
            else
              select PTSERIAL from (i_cTable);
               where PTNUMPAR=this.w_NUMPAR AND PTDATSCA=this.w_LDATSCA AND PTTIPCON=this.w_TIPCON AND PTCODCON=this.w_CODCON AND PT_SEGNO=this.w_SEGNO AND PTROWORD =-1 AND PTFLRAGG="S";
               order by PTFLRAGG ;
                into cursor _Curs_PAR_TITE
            endif
            if used('_Curs_PAR_TITE')
              select _Curs_PAR_TITE
              locate for 1=1
              do while not(eof())
              this.w_LSERIAL = _Curs_PAR_TITE.PTSERIAL
                select _Curs_PAR_TITE
                continue
              enddo
              use
            endif
            this.w_RifDoc = ah_Msgformat("DOC:") +" "
            VQ_EXEC("QUERY\GSTE2BRF.VQR",this,"temp")
            SELECT TEMP
            GO TOP
            SCAN
            this.w_SEGNO = IIF(NVL(PT_SEGNO," ")="A","D","A")
            this.w_RifDoc = alltrim(this.w_RifDoc) + alltrim(str(NVL(temp.ptnumdoc, 0),15,0))
            this.w_RifDoc = alltrim(this.w_RifDoc)+ iif( empty(NVL(temp.ptalfdoc,space(10))) , "", "/" + Alltrim( NVL(temp.ptalfdoc,space(10)) ) )
            this.w_RifDoc = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(temp.ptdatdoc)), 90)
            ENDSCAN
            if USED("TEMP")
              SELECT TEMP
              USE
            endif
          endif
        otherwise
          this.w_RifDoc = left(ah_Msgformat("PER LA FATTURA N. %1 DEL %2", alltrim(str(NVL(__tmp__.pnnumdoc,0),15,0)) + iif( empty(NVL(__tmp__.pnalfdoc,space(10))) , "", "/" +Alltrim( NVL(__tmp__.pnalfdoc,space(10))) ), dtoc(CP_TODATE(__tmp__.pndatdoc)) + space(90)), 90) 
          * --- Riferimenti documento
          * --- Controllo su numero ricevuta attribuita dal creditore
          if pnnumdoc=0
            this.w_oERRORLOG.AddMsgLog("Numero ricevuta attribuita dal creditore non inserito")     
          endif
      endcase
    endif
    * --- Totale documento
    if Not Empty(this.oParentObject.w_DESCRIZI)
      this.w_TotDoc = ""
    else
      this.w_TotDoc = ah_Msgformat("IMP:")
      this.w_TotDoc = alltrim(this.w_TotDoc) + alltrim(str(NVL(__tmp__.pttotimp,0),18,this.w_Decimali))
    endif
    this.w_RifDoc = left(alltrim(this.w_RifDoc) + this.w_TotDoc + SPACE(80), 80)
    * --- Composizione record e scrittura
    this.w_Record = " 50" + this.w_NumDis + this.w_RifDoc + space(10) + space(20)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (51)
    * --- Composizione record e scrittura
    this.w_Record = " 51" + this.w_NumDis + right("0000000000"+alltrim(str(NVL(__tmp__.PTNUMEFF,0))), 10)
    this.w_Record = this.w_Record +space(100)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (70)
    * --- Composizione record e scrittura
    this.w_Record = " 70" + this.w_NumDis + space(110)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    endscan
    * --- Record - EF (Record di coda)
    * --- Totale importi negativi (totale delle disposizioni : somma importi M.AV.)
    this.w_Valore = this.w_Totale
    this.w_Lunghezza = 15
    this.Page_3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_Totale = this.w_Valore
    * --- Numero disposizioni (numero totale delle M.AV.)
    if this.w_FLIBAN="S"
      this.w_NumRec = right( "0000000" + alltrim( str( (val(this.w_NumDis) * 8) + 2 , 7, 0) ) , 7)
    else
      this.w_NumRec = right( "0000000" + alltrim( str( (val(this.w_NumDis) * 7) + 2 , 7, 0) ) , 7)
    endif
    * --- Composizione record e scrittura
    this.w_Record = space(1)
    this.w_Record = space(1) + "EF" + this.w_AziSia + this.w_BanAbi + this.w_DatCre + this.w_NomSup + space(6)
    this.w_Record = this.w_Record + this.w_NumDis + this.w_Totale + repl("0",15) + this.w_NumRec + space(24)
    this.w_Record = this.w_Record + this.w_CodDiv + space(6)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if .not. fclose(this.w_Handle) OR this.w_oErrorLog.IsFullLog()
      this.w_oERRORLOG.PrintLog(this,"Errori riscontrati",.T., "File M.AV. generato con errori%0Stampo situazione messaggi di errore?")     
      if NOT ah_YesNo("Mantengo comunque il file generato?")
        NAMEFILE=ALLTRIM(this.oParentObject.w_PATHFILE) + IIF(EMPTY(ALLTRIM(this.w_NOMEFILE5)), ALLTRIM(this.w_NOMEFILE4), ALLTRIM(this.w_NOMEFILE7))
        DELETE FILE (NAMEFILE)
        this.oParentObject.w_GENERA = .T.
      endif
    else
      ah_ErrorMsg("Generazione file M.AV. terminata","","")
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura record su file ascii
    * --- Scrittura record su File RIBA
    w_t = fwrite(this.w_Handle, this.w_Record+this.w_Separa)
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Trasformazione degli importi nel formato richiesto dal tracciato Ri.Ba
    * --- UTILIZZO
    * --- Prima della chiamata a questa pagina del batch devono essere impostate le variabili:
    * --- w_Valore          = importo da convertire in stringa
    * --- w_Lunghezza  = lunghezza della stringa
    * --- Il risultato della conversione � disponibile nella variabile w_Valore.
    * --- CONVENZIONI
    * --- La virgola deve essere eliminata
    * --- Se la valuta � Euro le ultime due cifre rappresentano i decimali
    * --- Il valore deve essere allineato a destra
    * --- E' richiesto il riempimento con 0 delle cifre non significative
    * --- Calcolo parte intera
    this.w_StrInt = alltrim( str( int(this.w_Valore), this.w_Lunghezza, 0 ) )
    * --- Calcolo parte decimale
    this.w_StrDec = ""
    if this.w_Decimali > 0
      this.w_StrDec = right( str( this.w_Valore, this.w_Lunghezza, this.w_Decimali ) , this.w_Decimali )
    endif
    * --- Risultato
    this.w_Valore = right( repl("0",this.w_Lunghezza) + this.w_StrInt + this.w_StrDec , this.w_Lunghezza )
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ATTIMAST'
    this.cWorkTables[2]='PAR_TITE'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
