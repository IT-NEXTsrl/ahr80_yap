* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bm2                                                        *
*              Mov.magazzino, cambia dati tes                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_55]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-18                                                      *
* Last revis.: 2000-02-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bm2",oParentObject)
return(i_retval)

define class tgsma_bm2 as StdBatch
  * --- Local variables
  w_AGGRIG = .f.
  w_DATA = ctod("  /  /  ")
  w_DATSCO = ctod("  /  /  ")
  w_MESS = space(10)
  w_AGG = 0
  w_RECO = 0
  w_OK = .f.
  w_GSMA_MVM = .NULL.
  * --- WorkFile variables
  TAB_SCON_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna le Righe Mov.magazzino al Variare del Listino, Centro di Costo o Commessa (da GSMA_MVM)
    this.w_GSMA_MVM = this.oParentObject
    * --- Segno la posizione sul transitorio
    this.w_GSMA_MVM.MarkPos()     
    this.w_RECO = RECCOUNT(this.w_GSMA_MVM.cTrsName)
    this.w_AGG = 0
    if this.w_RECO>1 OR (this.w_RECO=1 AND NOT EMPTY(this.oParentObject.w_MMCODICE))
      do case
        case this.oParentObject.w_MMTCOLIS<>this.oParentObject.o_MMTCOLIS AND NOT EMPTY(this.oParentObject.w_MMTCOLIS)
          this.w_AGG = 1
          this.w_MESS = "Aggiorno i prezzi sulle righe in base al listino impostato?%0Nel caso il listino non sia gestito a sconti,%0questi verranno ricalcolati da tabella sconti/maggiorazioni"
        case this.oParentObject.w_MMCAOVAL<>this.oParentObject.o_MMCAOVAL AND this.oParentObject.w_MMVALNAZ<>this.oParentObject.w_MMCODVAL
          this.w_AGG = 6
          * --- Modificato il cambio in testata
          *     Aggiorno direttamente il valore fiscale
        case this.oParentObject.w_MMTCOLIS<>this.oParentObject.o_MMTCOLIS And EMPTY(this.oParentObject.w_MMTCOLIS)
          this.w_MESS = "Aggiorno gli sconti secondo la tabella sconti/maggiorazioni?%0Se le nuove impostazioni non trovano sconti validi,%0verranno mantenuti quelli precedentemente calcolati"
          this.w_AGG = 5
      endcase
      if this.w_AGG>0
        * --- Se modificato il cambio in testata eseguo l'aggiornamento 
        *     del valore fiscale senza fare nessuna domanda. 
        *     Se cambio il listino invece chiedo se devo aggiornare i valori sulle righe
        this.w_OK = (this.w_AGG=1 Or this.w_AGG=5)And ah_YesNo(this.w_MESS)
        SELECT (this.w_GSMA_MVM.cTrsName)
        GO TOP
        SCAN FOR t_CPROWORD<>0 AND NOT EMPTY(t_MMCODICE) AND NOT DELETED()
        * --- Legge i Dati del Temporaneo
        this.w_GSMA_MVM.WorkFromTrs()     
        this.w_GSMA_MVM.SaveDependsOn()     
        do case
          case this.w_AGG=1 Or this.w_AGG=5
            this.oParentObject.w_MMCODLIS = this.oParentObject.w_MMTCOLIS
            SELECT (this.w_GSMA_MVM.cTrsName)
            if this.w_OK
              if this.w_AGG=1
                this.w_GSMA_MVM.NotifyEvent("Ricalcola")     
              else
                this.w_DATSCO = IIF(Empty(this.oParentObject.w_MMDATDOC),this.oParentObject.w_MMDATREG,this.oParentObject.w_MMDATDOC)
                * --- Select from TAB_SCON
                i_nConn=i_TableProp[this.TAB_SCON_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TAB_SCON_idx,2],.t.,this.TAB_SCON_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select * from "+i_cTable+" TAB_SCON ";
                      +" where TSCATCLI="+cp_ToStrODBC(this.oParentObject.w_CATSCC)+" AND TSCATARR="+cp_ToStrODBC(this.oParentObject.w_CATSCA)+" ";
                      +" order by TSDATINI";
                       ,"_Curs_TAB_SCON")
                else
                  select * from (i_cTable);
                   where TSCATCLI=this.oParentObject.w_CATSCC AND TSCATARR=this.oParentObject.w_CATSCA ;
                   order by TSDATINI;
                    into cursor _Curs_TAB_SCON
                endif
                if used('_Curs_TAB_SCON')
                  select _Curs_TAB_SCON
                  locate for 1=1
                  do while not(eof())
                  if (_Curs_TAB_SCON.TSDATINI<=this.w_DATSCO OR EMPTY(CP_TODATE(TSDATINI))) AND (_Curs_TAB_SCON.TSDATFIN>=this.w_DATSCO OR EMPTY(CP_TODATE(TSDATFIN)))
                    this.oParentObject.w_MMSCONT1 = _Curs_TAB_SCON.TSSCONT1
                    this.oParentObject.w_MMSCONT2 = _Curs_TAB_SCON.TSSCONT2
                    this.oParentObject.w_MMSCONT3 = _Curs_TAB_SCON.TSSCONT3
                    this.oParentObject.w_MMSCONT4 = _Curs_TAB_SCON.TSSCONT4
                  endif
                    select _Curs_TAB_SCON
                    continue
                  enddo
                  use
                endif
                this.oParentObject.w_LIPREZZO = this.oParentObject.w_MMPREZZO
              endif
              SELECT (this.w_GSMA_MVM.cTrsName)
            endif
            this.oParentObject.o_MMCODLIS = this.oParentObject.w_MMTCOLIS
          case this.w_AGG=6
            this.w_DATA = IIF(EMPTY(this.oParentObject.w_MMDATDOC), this.oParentObject.w_MMDATREG, this.oParentObject.w_MMDATDOC)
            this.oParentObject.w_MMIMPNAZ = CALCNAZ(this.oParentObject.w_MMVALMAG,this.oParentObject.w_MMCAOVAL,this.oParentObject.w_CAONAZ, this.w_DATA,this.oParentObject.w_MMVALNAZ,this.oParentObject.w_MMCODVAL,IIF(this.oParentObject.w_FLAVA1="A",(this.oParentObject.w_PERIVA*this.oParentObject.w_INDIVA),0))
            this.oParentObject.w_MMVALULT = IIF(this.oParentObject.w_MMQTAUM1=0, 0, cp_ROUND(this.oParentObject.w_MMIMPNAZ/this.oParentObject.w_MMQTAUM1, 2))
            this.oParentObject.w_VISNAZ = cp_ROUND(this.oParentObject.w_MMIMPNAZ, this.oParentObject.w_DECTOP)
        endcase
        * --- Carica il Temporaneo dei Dati
        this.w_GSMA_MVM.TrsFromWork()     
        * --- Flag Notifica Riga Variata
        if i_SRV<>"A"
          replace i_SRV with "U"
        endif
        ENDSCAN
        * --- Riposiziono il temporaneo
        this.w_GSMA_MVM.RePos()     
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TAB_SCON'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_TAB_SCON')
      use in _Curs_TAB_SCON
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
