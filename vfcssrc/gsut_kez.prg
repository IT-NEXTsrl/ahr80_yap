* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kez                                                        *
*              Esportazione documenti archiviati in archivio zip               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-06-16                                                      *
* Last revis.: 2010-05-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kez",oParentObject))

* --- Class definition
define class tgsut_kez as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 783
  Height = 152
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-05-27"
  HelpContextID=202816663
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cpusers_IDX = 0
  cPrg = "gsut_kez"
  cComment = "Esportazione documenti archiviati in archivio zip"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_IDUTEEXP = 0
  w_FILEDEST = space(254)
  o_FILEDEST = space(254)
  w_FILESELEZ = space(254)
  w_FL_SAVE = .F.
  w_IDUTEDES = space(20)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kezPag1","gsut_kez",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIDUTEEXP_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='cpusers'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_IDUTEEXP=0
      .w_FILEDEST=space(254)
      .w_FILESELEZ=space(254)
      .w_FL_SAVE=.f.
      .w_IDUTEDES=space(20)
      .w_IDUTEEXP=oParentObject.w_IDUTEEXP
      .w_FILEDEST=oParentObject.w_FILEDEST
      .w_FL_SAVE=oParentObject.w_FL_SAVE
        .w_IDUTEEXP = i_codute
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_IDUTEEXP))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,3,.f.)
        .w_FL_SAVE = .T.
    endwith
    this.DoRTCalc(5,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_IDUTEEXP=.w_IDUTEEXP
      .oParentObject.w_FILEDEST=.w_FILEDEST
      .oParentObject.w_FL_SAVE=.w_FL_SAVE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_FILEDEST<>.w_FILEDEST
          .Calculate_QZVJYPNGGJ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_RLXSMAJPQK()
    with this
          * --- Seleziona file esportazione
          .w_FILESELEZ = PUTFILE(ah_MsgFormat("Nome archivio:"), .w_FILEDEST , "7z")
          .w_FILEDEST = IIF(!EMPTY(.w_FILESELEZ), .w_FILESELEZ, .w_FILEDEST)
    endwith
  endproc
  proc Calculate_QZVJYPNGGJ()
    with this
          * --- Verifica estensione
          .w_FILEDEST = FORCEEXT(.w_FILEDEST,"7z")
    endwith
  endproc
  proc Calculate_SFIDTXRXSD()
    with this
          * --- Ricalcolo nome file al cambiare del propietario
          .w_FILEDEST = FORCEEXT(TTOC(DATETIME(),1)+"_"+alltrim(str(.w_IDUTEEXP)),"7z")
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("SelDir")
          .Calculate_RLXSMAJPQK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_IDUTEEXP Changed")
          .Calculate_SFIDTXRXSD()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IDUTEEXP
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IDUTEEXP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_IDUTEEXP);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_IDUTEEXP)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_IDUTEEXP) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oIDUTEEXP_1_1'),i_cWhere,'',"Lista utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IDUTEEXP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_IDUTEEXP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_IDUTEEXP)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IDUTEEXP = NVL(_Link_.code,0)
      this.w_IDUTEDES = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_IDUTEEXP = 0
      endif
      this.w_IDUTEDES = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IDUTEEXP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIDUTEEXP_1_1.value==this.w_IDUTEEXP)
      this.oPgFrm.Page1.oPag.oIDUTEEXP_1_1.value=this.w_IDUTEEXP
    endif
    if not(this.oPgFrm.Page1.oPag.oFILEDEST_1_3.value==this.w_FILEDEST)
      this.oPgFrm.Page1.oPag.oFILEDEST_1_3.value=this.w_FILEDEST
    endif
    if not(this.oPgFrm.Page1.oPag.oIDUTEDES_1_12.value==this.w_IDUTEDES)
      this.oPgFrm.Page1.oPag.oIDUTEDES_1_12.value=this.w_IDUTEDES
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FILEDEST = this.w_FILEDEST
    return

enddefine

* --- Define pages as container
define class tgsut_kezPag1 as StdContainer
  Width  = 779
  height = 152
  stdWidth  = 779
  stdheight = 152
  resizeXpos=605
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIDUTEEXP_1_1 as StdField with uid="EAWLUZLOBQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_IDUTEEXP", cQueryName = "IDUTEEXP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 171944490,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=213, Top=34, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_IDUTEEXP"

  func oIDUTEEXP_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oIDUTEEXP_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIDUTEEXP_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oIDUTEEXP_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lista utenti",'',this.parent.oContained
  endproc

  add object oFILEDEST_1_3 as StdField with uid="ZRUSRSHNUW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FILEDEST", cQueryName = "FILEDEST",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Inserire il nome e il percorso dell'archivio zip da creare",;
    HelpContextID = 174011734,;
   bGlobalFont=.t.,;
    Height=21, Width=529, Left=213, Top=63, InputMask=replicate('X',254)


  add object oBtn_1_4 as StdButton with uid="EGSLJKOSDU",left=748, top=64, width=18,height=17,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il file zip d'esportazione";
    , HelpContextID = 203017686;
  , bGlobalFont=.t.

    proc oBtn_1_4.Click()
      this.parent.oContained.NotifyEvent("SelDir")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="YCTTRQWDEB",left=667, top=100, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per continuare con l'esportazione dei documenti selezionati";
    , HelpContextID = 202845414;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_FILEDEST) AND !EMPTY(JUSTFNAME(FORCEEXT(.w_FILEDEST,"7z"))) AND !EMPTY(.w_IDUTEEXP) AND !EMPTY(JUSTPATH(.w_FILEDEST)))
      endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="TAPXNQTUEA",left=718, top=100, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 210134086;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oIDUTEDES_1_12 as StdField with uid="ODECPVBVBQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_IDUTEDES", cQueryName = "IDUTEDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 188721703,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=307, Top=34, InputMask=replicate('X',20)

  add object oStr_1_2 as StdString with uid="CNUYRQGMCH",Visible=.t., Left=15, Top=67,;
    Alignment=1, Width=193, Height=18,;
    Caption="File zip di destinazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="MYERFQNMOL",Visible=.t., Left=22, Top=9,;
    Alignment=0, Width=735, Height=18,;
    Caption="Selezionare o indicare il percorso ed il nome del file zip su cui esportare i documenti selezionati"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="KQUXKWUGMH",Visible=.t., Left=15, Top=37,;
    Alignment=1, Width=193, Height=18,;
    Caption="Utente proprietario esportazione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kez','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
