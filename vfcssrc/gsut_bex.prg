* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bex                                                        *
*              Export dati azienda                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-19                                                      *
* Last revis.: 2008-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bex",oParentObject,m.pOper)
return(i_retval)

define class tgsut_bex as StdBatch
  * --- Local variables
  pOper = space(1)
  w_CODAZI = space(5)
  w_RESULT = space(10)
  w_PADRE = .NULL.
  w_TABELLA = space(30)
  w_IDXTABLE = 0
  w_CONN = 0
  w_PHNAME = space(50)
  w_TMPPATH = space(254)
  w_RESARCH = space(10)
  w_CAMPO = space(30)
  w_LOOP = 0
  w_MULTIAZI = .f.
  w_DESCRI = space(50)
  w_NTOT = 0
  w_NERR = 0
  w_NMAZI = 0
  w_NAZNOSEL = 0
  w_OK = .f.
  w_TMPPATHZIP = space(254)
  w_NREC = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scorre le tabelle dell'azienda e per ognuna produce un DBF che confluir�
    *     in un file compresso
    *     pOper='Z' carica lo zoom a pagina 2
    *     pOper='E' svolge l'export...
    =cp_ReadXdc()
    this.w_PADRE = This.oParentObject
    this.w_CODAZI = i_CODAZI
    do case
      case this.pOper="E"
        this.oParentObject.w_Msg = ""
        AddMsgNL("Inizio procedura di esportazione %1", this , time() )
        * --- Interogo CPTTBLS per sapere le tabelle sul database...
        * --- Cartella temporanea in cui vado a salvare i DBF da mettere nel
        *     file compresso (aggiungo una cartella affinch� possa essere visualizzato 
        *     il contenuto dalla visualizzazione pacchetto)
        this.w_TMPPATHZIP = tempadhoc()+"\"+Sys( 2015 )+"\"
        this.w_TMPPATH = this.w_TMPPATHZIP+"export\"
        * --- Tento di cancellarla...
        if Not DeleteFolder( this.w_TMPPATH )
          AddMsgNL("Impossibile cancellare cartella %1 ", this , this.w_TMPPATH , , ,, ,)
          i_retcode = 'stop'
          return
        endif
        * --- La creo..
        if Not Directory( this.w_TMPPATH ) 
          * --- Creo la cartella..
          if Not MakeDir(this.w_TMPPATH)
            AddMsgNL("Impossibile creare cartella %1 ", this , this.w_TMPPATH , , ,, ,)
            i_retcode = 'stop'
            return
          endif
        endif
        this.w_RESULT = Sys(2015)
        * --- Leggo le tabelle aziendali e multiaziendali da esportare
         
 Select Filename, Space(50) as CAMPO from (this.oParentObject.w_ZOOMA.cCursor) where XCHK=1 Union All; 
 Select Filename, CAMPO from (this.oParentObject.w_ZOOM.cCursor) where XCHK=1 into Cursor (this.w_RESULT)
        this.w_NTOT = 0
        this.w_NERR = 0
        this.w_NMAZI = 0
        this.w_NAZNOSEL = 0
         
 Local L_Nrec 
 L_Nrec=0
        if not Empty( this.w_RESULT )
          do while Not Eof( this.w_RESULT )
            this.w_NTOT = this.w_NTOT + 1
            Select ( this.w_RESULT )
            this.w_TABELLA = Upper( Alltrim( Filename ) )
            this.w_IDXTABLE = cp_OpenTable( Alltrim(this.w_TABELLA) ,.T.)
            * --- Se non nell'XDC tabella da non passare...
            if this.w_IDXTABLE<>0
              this.w_CONN = i_TableProp[ this.w_IDXTABLE , 3 ] 
              * --- Determino il nome fisico...
              this.w_PHNAME = cp_SetAzi(i_TableProp[ this.w_IDXTABLE ,2]) 
              * --- La tabella esiste ?
              if Not cp_ExistTable( this.w_PHNAME , this.w_CONN ,"","","")
                * --- Tabella da leggere assente...
                AddMsgNL("Errore lettura archivio %1 non esistente ", this , this.w_PHNAME , , ,, ,)
                this.w_NERR = this.w_NERR + 1
              else
                * --- Faccio una READ se tabella aziendale...
                this.w_MULTIAZI = .F.
                this.w_OK = .T.
                this.w_DESCRI = i_dcx.GetTabledescr(I_DCX.GETTABLEIDX( this.w_TABELLA ))
                if Upper( Alltrim( this.w_TABELLA ) ) = Upper( Alltrim( this.w_PHNAME ) ) 
                  this.w_DESCRI = "MultiAzienda "+this.w_DESCRI
                  * --- Archivio multi aziendale, quindi devo verificare se � tra quelli
                  *     selezionati e se prevede un campo di filtro...
                   
 Select( this.oParentObject.w_ZOOM.cCursor ) 
 Go Top 
 Locate For Upper( Alltrim( FILENAME ) )= Upper( Alltrim( this.w_TABELLA ) ) And XCHK=1
                  if Found()
                    this.w_NMAZI = this.w_NMAZI + 1
                    this.w_CAMPO = CAMPO
                    * --- Solo se nel cursore esiste un campo sul quale filtrare posso dichiararla mutltiazienda
                    *     e prendere solo il record relativo all'azienda corrente. Altrimenti prendo tutto
                    this.w_MULTIAZI = Not Empty(this.w_CAMPO)
                  else
                    * --- L'archivio non si passa
                    this.w_NAZNOSEL = this.w_NAZNOSEL + 1
                    AddMsgNL("Archivio multi aziendale %1 ( %2 ) non selezionato", this , this.w_TABELLA , this.w_DESCRI )
                    this.w_OK = .F.
                  endif
                endif
                if this.w_OK
                  * --- Se ho trovato svolgo il filtro altrimenti passo tutto..
                  if this.w_MULTIAZI
                     
 Dimension ArrDb[1,2] 
 ArrDb[1,1]=this.w_CAMPO 
 ArrDb[1,2]=this.w_CODAZI
                    AddMsgNL("Esportazione porzione aziendale archivio %1 ( %2 )", this , this.w_TABELLA , this.w_DESCRI )
                    this.w_RESARCH = READTABLE( this.w_TABELLA , "*" , @ArrDb , this.w_PADRE , this.oParentObject.w_VERBOSE , "" ,@L_NREC) 
                  else
                    AddMsgNL("%1 ( %2 )", this , this.w_TABELLA , this.w_DESCRI )
                    this.w_RESARCH = READTABLE( this.w_TABELLA , "*" , , this.w_PADRE , this.oParentObject.w_VERBOSE , "1=1" ,@L_NREC) 
                  endif
                  if Empty( this.w_RESARCH )
                    AddMsgNL("Errore lettura archivio %1 ( %2 )", this , this.w_PHNAME , this.w_DESCRI )
                    this.w_NERR = this.w_NERR + 1
                  else
                    AddMsgNL("Esportate %1 registrazioni ", this , L_NREC )
                     
 Select( this.w_RESARCH ) 
 Copy To (this.w_TMPPATH + this.w_TABELLA) 
 Use
                  endif
                endif
              endif
            endif
             
 Select ( this.w_RESULT) 
 Skip
          enddo
          Select ( this.w_RESULT ) 
 Use
          * --- Solo se attivo check tabella progressivi
          if this.oParentObject.w_CPWARN="S"
             
 Dimension ArrDb[1,2] 
 ArrDb[1,1]="warncode" 
 ArrDb[1,2]=this.w_CODAZI
            this.w_NTOT = this.w_NTOT + 1
            this.w_RESARCH = READTABLE( "cpwarn" , "*" , @ArrDb , this.w_PADRE , this.oParentObject.w_VERBOSE , "" , @L_NREC ) 
            AddMsgNL("Esportazione porzione aziendale progressivi ( %1 registrazioni )", this , L_NREC )
            if Empty( this.w_RESARCH )
              AddMsgNL("Errore lettura archivio progressivi ", this )
              this.w_NERR = this.w_NERR + 1
            else
               
 Select( this.w_RESARCH ) 
 Copy To (this.w_TMPPATH + "cpwarn") 
 Use
            endif
          endif
          * --- Faccio lo zip..
          AddMsgNL("Avvio creazione pacchetto %1 ", this , this.oParentObject.w_PATH , , ,, ,)
          Local L_RetMsg
          L_RetMsg = ""
          if ! ZIPUNZIP("Z", this.oParentObject.w_PATH , this.w_TMPPATHZIP, this.oParentObject.w_PWD, @L_RetMsg)
            AddMsgNL("Impossibile creare il pacchetto.%0%1", this , L_RetMsg)
            i_retcode = 'stop'
            return
          endif
          * --- Alla fine rimuovo la cartella temporanea comprensiva della sottocartella Export
          AddMsgNL("Rimuovo cartella temporanea %1 ", this , this.w_TMPPATH , , ,, ,)
          if Not DeleteFolder( this.w_TMPPATHZIP )
            AddMsgNL("Impossibile cancellare cartella %1 in chiusura di elaborazione ", this , this.w_TMPPATH , , ,, ,)
            i_retcode = 'stop'
            return
          endif
          AddMsgNL("Procedura di esportazione terminata alle %1", this , time() )
          AddMsgNL("Archivi scanditi %1 ", this , Alltrim(Str(this.w_NTOT)) )
          if this.w_NERR<>0
            AddMsgNL("Archivi con errori %1 ", this , Alltrim(Str(this.w_NERR )))
          endif
          AddMsgNL("Archivi scanditi di tipo aziendale %1 ", this ,Alltrim(Str( this.w_NMAZI)) )
          AddMsgNL("Archivi scanditi di tipo aziendale non selezionati %1 ", this , Alltrim(Str(this.w_NAZNOSEL)) )
        endif
      case this.pOper="Z"
        * --- Riempio lo zoom con le tabelle multi aziendali
        this.w_RESULT = READTABLE( "CPTTBLS" , "Filename, 0  As STATO,"+cp_tostrodbc(REPLICATE(" ",30))+" As DESCRIZIONE, "+cp_tostrodbc(REPLICATE(" ",30))+" As CAMPO, 0 As XDCTABLE" , , this.w_PADRE , this.oParentObject.w_VERBOSE , "filename=phname" ) 
        * --- Rendo il cursore scrivibile..
        = WrCursor ( this.w_RESULT )
        if not Empty( this.w_RESULT )
          do while Not Eof( this.w_RESULT )
            Select ( this.w_RESULT )
            this.w_TABELLA = Upper( Alltrim( Filename ) )
            this.w_IDXTABLE = cp_OpenTable( Alltrim(this.w_TABELLA) ,.T.)
            * --- Se non nell'XDC tabella da non passare...
            if this.w_IDXTABLE<>0
              * --- La tabella esiste ?
              this.w_CONN = i_TableProp[ this.w_IDXTABLE , 3 ] 
              * --- Determino il nome fisico...
              this.w_PHNAME = cp_SetAzi(i_TableProp[ this.w_IDXTABLE ,2]) 
              if cp_ExistTable( this.w_PHNAME , this.w_CONN ,"","","")
                this.w_DESCRI = i_dcx.GetTabledescr(I_DCX.GETTABLEIDX( this.w_TABELLA ))
                 
 Select ( this.w_RESULT ) 
 Replace DESCRIZIONE with this.w_DESCRI
                * --- Ricerco il campo nella chiave che pu� contenere il codice AZIENDA
                this.w_CAMPO = GETFLDAZ( this.w_TABELLA )
                this.w_MULTIAZI = not Empty( this.w_CAMPO )
                if this.w_MULTIAZI
                  * --- Se lo trovo modifico lo stato e valorizzo il campo da filtrare...
                   
 Select ( this.w_RESULT ) 
 Replace STATO with 1, CAMPO with this.w_CAMPO
                endif
              endif
            else
               
 Select ( this.w_RESULT ) 
 Replace XDCTABLE with 1
            endif
             
 Select ( this.w_RESULT) 
 Skip
          enddo
           
 Select ( this.w_RESULT ) 
 Delete For XDCTABLE=1
          this.w_NREC = CURTOTAB( this.w_RESULT , "TMPVEND1" )
          Select ( this.w_RESULT ) 
 Use
        endif
        * --- Tabelle aziendali
        this.w_RESULT = READTABLE( "CPTTBLS" , "Filename," +cp_tostrodbc(REPLICATE(" ",30))+ " As DESCRIZIONE" , , this.w_PADRE , this.oParentObject.w_VERBOSE , "FILENAME<>PHNAME" ) 
        * --- Rendo il cursore scrivibile..
        = WrCursor ( this.w_RESULT )
        if not Empty( this.w_RESULT )
          do while Not Eof( this.w_RESULT )
            Select ( this.w_RESULT )
            this.w_TABELLA = Upper( Alltrim( Filename ) )
            this.w_IDXTABLE = cp_OpenTable( Alltrim(this.w_TABELLA) ,.T.)
            * --- Se non nell'XDC tabella da non passare...
            if this.w_IDXTABLE<>0
              * --- La tabella esiste ?
              this.w_CONN = i_TableProp[ this.w_IDXTABLE , 3 ] 
              * --- Determino il nome fisico...
              this.w_PHNAME = cp_SetAzi(i_TableProp[ this.w_IDXTABLE ,2]) 
              if cp_ExistTable( this.w_PHNAME , this.w_CONN ,"","","")
                this.w_DESCRI = i_dcx.GetTabledescr(I_DCX.GETTABLEIDX( this.w_TABELLA ))
                 
 Select ( this.w_RESULT ) 
 Replace DESCRIZIONE with this.w_DESCRI
              endif
            endif
             
 Select ( this.w_RESULT) 
 Skip
          enddo
          this.w_NREC = CURTOTAB( this.w_RESULT , "TMPVEND2" )
          Select ( this.w_RESULT ) 
 Use
        endif
      case this.pOper="D"
        * --- Drop temporary table TMPVEND1
        i_nIdx=cp_GetTableDefIdx('TMPVEND1')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPVEND1')
        endif
        * --- Drop temporary table TMPVEND2
        i_nIdx=cp_GetTableDefIdx('TMPVEND2')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPVEND2')
        endif
      case this.pOper="S"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="I"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Modifico cursore aziendale
         
 Update ( this.oParentObject.w_ZOOMA.cCursor ) Set XCHK=1 
 Go Top
      case this.pOper="1"
        if this.oParentObject.w_SELEZ1="S"
          * --- Modifico cursore aziendale
           
 Update ( this.oParentObject.w_ZOOMA.cCursor ) Set XCHK=1
        else
           
 Update ( this.oParentObject.w_ZOOMA.cCursor ) Set XCHK=0
        endif
        Go top
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cursore multi azienda
    do case
      case this.oParentObject.w_SELEZI="N"
         
 Update ( this.oParentObject.w_ZOOM.cCursor ) Set XCHK=0
      case this.oParentObject.w_SELEZI="F"
         
 Update ( this.oParentObject.w_ZOOM.cCursor ) Set XCHK=STATO
      case this.oParentObject.w_SELEZI="G"
         
 Update ( this.oParentObject.w_ZOOM.cCursor ) Set XCHK=IIF(STATO=0,1,0)
      case this.oParentObject.w_SELEZI="T"
         
 Update ( this.oParentObject.w_ZOOM.cCursor ) Set XCHK=1
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
