* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bsd                                                        *
*              Selezione directory                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_3]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-10                                                      *
* Last revis.: 2000-01-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Variabile
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bsd",oParentObject,m.w_Variabile)
return(i_retval)

define class tgsut_bsd as StdBatch
  * --- Local variables
  w_Variabile = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Richiama la funzione fox che permette di selezionare una directory
    * --- Parametri
    * --- Selezione directory
    w_Oggetto = "This.oParentObject"+this.w_Variabile
    &w_Oggetto=cp_getdir()
    i_retcode = 'stop'
    i_retval = .F.
    return
  endproc


  proc Init(oParentObject,w_Variabile)
    this.w_Variabile=w_Variabile
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Variabile"
endproc
