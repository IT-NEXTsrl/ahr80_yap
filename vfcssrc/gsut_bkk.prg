* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bkk                                                        *
*              Cambio path classe docm                                         *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-19                                                      *
* Last revis.: 2013-09-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bkk",oParentObject)
return(i_retval)

define class tgsut_bkk as StdBatch
  * --- Local variables
  w_DIRINIPAR = space(254)
  w_NEWPATH = space(254)
  w_CurDir = space(254)
  w_PATH = space(254)
  w_NEWDIR = space(254)
  w_OLDDIR = space(254)
  * --- WorkFile variables
  PROMINDI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura invocata da GSUT_ACL al cambio del percorso standard di archiviazione allegati. 
    *     Cambia il percorso assoluto all'interno degli allegati appartenenti alla classe in cui si � operato la modifica.
    * --- Segno la posizione di Default
    this.w_CurDir = Alltrim(Sys(5)+Sys(2003))
    * --- Ricavo il vecchio path standard
    this.w_DIRINIPAR = upper(alltrim(this.oParentObject.oldpath))+"%"
    * --- Verifico l'esistenza del nuovo  path
    this.w_PATH = this.oParentObject.w_CDPATSTD
    if Isalt() and Empty(JUSTDRIVE(this.w_PATH))
      this.w_PATH = Fullpath(this.w_PATH)
    endif
    if DIRECTORY(Alltrim(this.w_PATH))
      this.w_NEWDIR = ADDBS(alltrim(this.oParentObject.w_CDPATSTD))
      this.w_OLDDIR = ADDBS(alltrim(this.oParentObject.oldpath))
      * --- Estraggo tutti gli indici aventi il vecchio path standard
      * --- Select from GSUT_BKS
      do vq_exec with 'GSUT_BKS',this,'_Curs_GSUT_BKS','',.f.,.t.
      if used('_Curs_GSUT_BKS')
        select _Curs_GSUT_BKS
        locate for 1=1
        do while not(eof())
        * --- Aggiorno il  path assoluto con il nuovo
        if not ADDBS(alltrim(_Curs_GSUT_BKS.IDPATALL))==this.w_NEWDIR
          this.w_NEWPATH = strtran(alltrim(_Curs_GSUT_BKS.IDPATALL), this.w_OLDDIR, this.w_NEWDIR, -1, -1, 1)
          * --- Write into PROMINDI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PROMINDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"IDPATALL ="+cp_NullLink(cp_ToStrODBC(this.w_NEWPATH),'PROMINDI','IDPATALL');
                +i_ccchkf ;
            +" where ";
                +"IDSERIAL = "+cp_ToStrODBC(_Curs_GSUT_BKS.IDSERIAL);
                   )
          else
            update (i_cTable) set;
                IDPATALL = this.w_NEWPATH;
                &i_ccchkf. ;
             where;
                IDSERIAL = _Curs_GSUT_BKS.IDSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
          select _Curs_GSUT_BKS
          continue
        enddo
        use
      endif
    else
      this.oParentObject.w_CDPATSTD = Alltrim(this.oParentObject.oldpath)
      AH_ERRORMSG("La cartella di destinazione non esiste ",48,"Attenzione !!!")
    endif
    * --- Ripristino la cartella di Default
    cd (this.w_CurDir)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PROMINDI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSUT_BKS')
      use in _Curs_GSUT_BKS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
