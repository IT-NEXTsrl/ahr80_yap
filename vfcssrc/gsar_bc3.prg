* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bc3                                                        *
*              Controlli su cancellazione c.C.                                 *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_42]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-06-27                                                      *
* Last revis.: 2013-05-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bc3",oParentObject)
return(i_retval)

define class tgsar_bc3 as StdBatch
  * --- Local variables
  w_BOK = .f.
  w_PADRE = .NULL.
  w_CCCODBAN = space(10)
  w_CCCONCOR = space(24)
  w_TIPO = space(1)
  w_NONNO = .NULL.
  * --- WorkFile variables
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invocato dall'area manuale Declare Variables alla procedure F6
    this.w_PADRE = this.oParentObject
    this.w_PADRE.MarkPos()     
    this.w_CCCONCOR = this.w_PADRE.Get("w_CCCONCOR")
    this.w_CCCODBAN = this.w_PADRE.Get("w_CCCODBAN")
    if Upper( this.w_PADRE.Class )="TCGSAR_MCC"
      this.w_NONNO = this.w_PADRE.oParentObject
      this.w_TIPO = IIF( lower( this.w_NONNO.class)="tgsar_ano" , this.w_NONNO.w_NOTIPBAN , this.w_NONNO.w_ANTIPCON )
    else
      this.w_TIPO = this.oParentObject.w_CCTIPCON
    endif
    * --- Controllo da dove viene chiamato il batch.
    this.w_BOK = .F.
    * --- Select from QUERY\GSAR_BC3
    do vq_exec with 'QUERY\GSAR_BC3',this,'_Curs_QUERY_GSAR_BC3','',.f.,.t.
    if used('_Curs_QUERY_GSAR_BC3')
      select _Curs_QUERY_GSAR_BC3
      locate for 1=1
      do while not(eof())
      this.w_BOK = NVL(CONTA,0)<>0 Or this.w_BOK
        select _Curs_QUERY_GSAR_BC3
        continue
      enddo
      use
    endif
    * --- Controllo se esiste un movimento con stesso codice e stesso conto corrente.
    *     Questa operazione deve essere effettuata solo nel caso in cui la riga � cancellabile.
    if ! this.w_BOK
      do case
        case Upper( this.w_PADRE.Class )="TCGSAR_MCC" and g_APPLICATION<>"ADHOC REVOLUTION"
          * --- Sbianco il conto corrente presente nel cliente
          if lower( this.w_NONNO.class)="tgsar_ano"
            if (this.w_CCCODBAN=this.w_NONNO.w_NOCODBAN) and (this.w_CCCONCOR=this.w_NONNO.w_NONUMCOR)
              this.w_NONNO.w_NONUMCOR = space(24)
              this.w_NONNO.w_NOCODBAN = space(10)
              this.w_NONNO.bUpdated = .t.
              this.w_NONNO.SetControlsValue(.)     
            endif
          else
            if (this.w_CCCODBAN=this.w_NONNO.w_ANCODBAN) and (this.w_CCCONCOR=this.w_NONNO.w_ANNUMCOR)
              this.w_NONNO.w_ANNUMCOR = space(24)
              this.w_NONNO.w_ANCODBAN = space(10)
              this.w_NONNO.bUpdated = .t.
              this.w_NONNO.SetControlsValue(.)     
            endif
          endif
        case Upper( this.w_PADRE.Class )="TGSAR_MC1"
          if (this.w_CCCODBAN=this.oParentObject.w_ANCODBAN) and (this.w_CCCONCOR=this.oParentObject.w_ANNUMCOR)
            this.oParentObject.w_ANNUMCOR = space(24)
            this.oParentObject.w_ANCODBAN = SPACE(10)
            this.oParentObject.w_NOCODBAN = SPACE(10)
            this.oParentObject.w_CODBAN = SPACE(10)
            * --- Sbianco il conto corrente presente in GSAR_MC1
            this.w_PADRE.bHeaderUpdated = .T.
          endif
      endcase
    endif
    if g_APPLICATION="ADHOC REVOLUTION" and !this.w_BOK and this.w_PADRE.GET("w_DEFAULT") = 1 and INLIST(lower(this.w_NONNO.class),"tgsar_acl","tgsar_afr")
      * --- Provvedo a sbiancare i campi del 'nonno' che hanno riferimenti alla riga che st� per cancellare
      this.w_NONNO.w_AN__IBAN = " "
      this.w_NONNO.w_AN__BBAN = " "
      this.w_NONNO.w_ANCODBAN = " "
      this.w_NONNO.w_ANNUMCOR = " "
      this.w_NONNO.w_ANCINABI = " "
    endif
    * --- Riposizionamento sul Transitorio effettuando la SaveDependsOn()
    this.w_PADRE.RePos(.F.)     
    * --- Variabile locale alle aree manuali che invocano la routine
    bOk=Not this.w_BOK
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PAR_TITE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_QUERY_GSAR_BC3')
      use in _Curs_QUERY_GSAR_BC3
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
