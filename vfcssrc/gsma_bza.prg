* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bza                                                        *
*              Lancia anagrafiche articoli                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-18                                                      *
* Last revis.: 2008-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bza",oParentObject)
return(i_retval)

define class tgsma_bza as StdBatch
  * --- Local variables
  w_CODART = space(20)
  w_TIPART = space(2)
  w_PROG = space(8)
  w_DXBTN = .f.
  w_CODICE = space(20)
  * --- WorkFile variables
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia l'anagrafica Articoli/Servizi a seconda del Tipo Articolo (da GSMA_MVM ,zoom on zoom)
    this.w_DXBTN = .F.
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_DXBTN = .T.
      this.w_CODICE = Nvl(g_oMenu.oKey(1,3),"")
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODART"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODART;
          from (i_cTable) where;
              CACODICE = this.w_CODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      * --- Tipo di Conto
      * --- Se non trovo CACODART significa che lo Zoom � sugli articoli (ARCODART)
      if TYPE("&cCurs..CACODART")<>"U"
        this.w_CODART = &cCurs..CACODART
      else
        this.w_CODART = &cCurs..ARCODART
      endif
    endif
    this.w_TIPART = "XX"
    if NOT EMPTY(NVL(this.w_CODART," "))
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARTIPART"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARTIPART;
          from (i_cTable) where;
              ARCODART = this.w_CODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPART = NVL(cp_ToDate(_read_.ARTIPART),cp_NullValue(_read_.ARTIPART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      do case
        case this.w_TIPART $ "FM-FO-DE"
          * --- Lancia Servizi
          this.w_PROG = "GSMA_AAS"
        case .T.
          * --- Lancia Articoli
          this.w_PROG = "GSMA_AAR"
      endcase
    else
      if ISALT()
        * --- Lancia Servizi
        this.w_PROG = "GSMA_AAS"
      else
        * --- Lancia Articoli
        this.w_PROG = "GSMA_AAR"
      endif
    endif
    * --- Nel caso ho premuto tasto destro sul controll apro la anagrafica sul codice 
    *     presente nella variabile
    OpenGest(IIF(this.w_DXBTN,g_oMenu.cBatchType,"S"),this.w_PROG,"ARCODART",this.w_CODART)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='KEY_ARTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
