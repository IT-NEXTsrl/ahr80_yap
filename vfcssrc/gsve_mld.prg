* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_mld                                                        *
*              Lista documenti generati                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-10-26                                                      *
* Last revis.: 2012-01-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsve_mld")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsve_mld")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsve_mld")
  return

* --- Class definition
define class tgsve_mld as StdPCForm
  Width  = 776
  Height = 289
  Top    = 9
  Left   = 13
  cComment = "Lista documenti generati"
  cPrg = "gsve_mld"
  HelpContextID=214577769
  add object cnt as tcgsve_mld
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsve_mld as PCContext
  w_LDSERIAL = space(10)
  w_CPROWNUM = 0
  w_LDSERDOC = space(10)
  w_LDTIPDOC = space(5)
  w_LDNUMDOC = 0
  w_LDALFDOC = space(10)
  w_LDDATDOC = space(8)
  w_LDCODCLF = space(15)
  w_LDPARAME = space(3)
  w_LDTIPCLF = space(1)
  w_DESCON = space(40)
  w_APPO = space(5)
  proc Save(i_oFrom)
    this.w_LDSERIAL = i_oFrom.w_LDSERIAL
    this.w_CPROWNUM = i_oFrom.w_CPROWNUM
    this.w_LDSERDOC = i_oFrom.w_LDSERDOC
    this.w_LDTIPDOC = i_oFrom.w_LDTIPDOC
    this.w_LDNUMDOC = i_oFrom.w_LDNUMDOC
    this.w_LDALFDOC = i_oFrom.w_LDALFDOC
    this.w_LDDATDOC = i_oFrom.w_LDDATDOC
    this.w_LDCODCLF = i_oFrom.w_LDCODCLF
    this.w_LDPARAME = i_oFrom.w_LDPARAME
    this.w_LDTIPCLF = i_oFrom.w_LDTIPCLF
    this.w_DESCON = i_oFrom.w_DESCON
    this.w_APPO = i_oFrom.w_APPO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_LDSERIAL = this.w_LDSERIAL
    i_oTo.w_CPROWNUM = this.w_CPROWNUM
    i_oTo.w_LDSERDOC = this.w_LDSERDOC
    i_oTo.w_LDTIPDOC = this.w_LDTIPDOC
    i_oTo.w_LDNUMDOC = this.w_LDNUMDOC
    i_oTo.w_LDALFDOC = this.w_LDALFDOC
    i_oTo.w_LDDATDOC = this.w_LDDATDOC
    i_oTo.w_LDCODCLF = this.w_LDCODCLF
    i_oTo.w_LDPARAME = this.w_LDPARAME
    i_oTo.w_LDTIPCLF = this.w_LDTIPCLF
    i_oTo.w_DESCON = this.w_DESCON
    i_oTo.w_APPO = this.w_APPO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsve_mld as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 776
  Height = 289
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-01-12"
  HelpContextID=214577769
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  LDOCGENE_IDX = 0
  DOC_MAST_IDX = 0
  CONTI_IDX = 0
  cFile = "LDOCGENE"
  cKeySelect = "LDSERIAL"
  cKeyWhere  = "LDSERIAL=this.w_LDSERIAL"
  cKeyDetail  = "LDSERIAL=this.w_LDSERIAL and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"LDSERIAL="+cp_ToStrODBC(this.w_LDSERIAL)';

  cKeyDetailWhereODBC = '"LDSERIAL="+cp_ToStrODBC(this.w_LDSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"LDOCGENE.LDSERIAL="+cp_ToStrODBC(this.w_LDSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'LDOCGENE.CPROWNUM '
  cPrg = "gsve_mld"
  cComment = "Lista documenti generati"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 11
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LDSERIAL = space(10)
  w_CPROWNUM = 0
  w_LDSERDOC = space(10)
  w_LDTIPDOC = space(5)
  w_LDNUMDOC = 0
  w_LDALFDOC = space(10)
  w_LDDATDOC = ctod('  /  /  ')
  w_LDCODCLF = space(15)
  w_LDPARAME = space(3)
  w_LDTIPCLF = space(1)
  w_DESCON = space(40)
  w_APPO = space(5)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_mldPag1","gsve_mld",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='LDOCGENE'
    * --- Area Manuale = Open Work Table
    * --- gsve_mld
    * colora le righe dei documenti che sono eliminati in modo differente
              This.oPgFrm.Page1.oPAg.oBody.oBodyCol.DynamicBackColor= ;
                "IIF(EMPTY(NVL(t_LDSERDOC,' ')),IIF(EMPTY(NVL(t_CPROWNUM,0)),RGB(255,255,255),RGB(255,0,0)),RGB(255 , 255, 255))"
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.LDOCGENE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.LDOCGENE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsve_mld'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from LDOCGENE where LDSERIAL=KeySet.LDSERIAL
    *                            and CPROWNUM=KeySet.CPROWNUM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.LDOCGENE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LDOCGENE_IDX,2],this.bLoadRecFilter,this.LDOCGENE_IDX,"gsve_mld")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('LDOCGENE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "LDOCGENE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' LDOCGENE '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LDSERIAL',this.w_LDSERIAL  )
      select * from (i_cTable) LDOCGENE where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_LDSERIAL = NVL(LDSERIAL,space(10))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'LDOCGENE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESCON = space(40)
          .w_APPO = space(5)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWNUM = NVL(CPROWNUM,0)
          .w_LDSERDOC = NVL(LDSERDOC,space(10))
          if link_2_2_joined
            this.w_LDSERDOC = NVL(MVSERIAL202,NVL(this.w_LDSERDOC,space(10)))
            this.w_APPO = NVL(MVTIPDOC202,space(5))
          else
          .link_2_2('Load')
          endif
          .w_LDTIPDOC = NVL(LDTIPDOC,space(5))
          .w_LDNUMDOC = NVL(LDNUMDOC,0)
          .w_LDALFDOC = NVL(LDALFDOC,space(10))
          .w_LDDATDOC = NVL(cp_ToDate(LDDATDOC),ctod("  /  /  "))
          .w_LDCODCLF = NVL(LDCODCLF,space(15))
          if link_2_7_joined
            this.w_LDCODCLF = NVL(ANCODICE207,NVL(this.w_LDCODCLF,space(15)))
            this.w_DESCON = NVL(ANDESCRI207,space(40))
          else
          .link_2_7('Load')
          endif
          .w_LDPARAME = NVL(LDPARAME,space(3))
          .w_LDTIPCLF = NVL(LDTIPCLF,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_12.enabled = .oPgFrm.Page1.oPag.oBtn_2_12.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_13.enabled = .oPgFrm.Page1.oPag.oBtn_2_13.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_LDSERIAL=space(10)
      .w_CPROWNUM=0
      .w_LDSERDOC=space(10)
      .w_LDTIPDOC=space(5)
      .w_LDNUMDOC=0
      .w_LDALFDOC=space(10)
      .w_LDDATDOC=ctod("  /  /  ")
      .w_LDCODCLF=space(15)
      .w_LDPARAME=space(3)
      .w_LDTIPCLF=space(1)
      .w_DESCON=space(40)
      .w_APPO=space(5)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_LDSERDOC))
         .link_2_2('Full')
        endif
        .DoRTCalc(4,8,.f.)
        if not(empty(.w_LDCODCLF))
         .link_2_7('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'LDOCGENE')
    this.DoRTCalc(9,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_12.enabled = this.oPgFrm.Page1.oPag.oBtn_2_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_13.enabled = this.oPgFrm.Page1.oPag.oBtn_2_13.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_2_12.enabled = .Page1.oPag.oBtn_2_12.mCond()
      .Page1.oPag.oBtn_2_13.enabled = .Page1.oPag.oBtn_2_13.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'LDOCGENE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.LDOCGENE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LDSERIAL,"LDSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWNUM N(6);
      ,t_LDTIPDOC C(5);
      ,t_LDNUMDOC N(15);
      ,t_LDALFDOC C(10);
      ,t_LDDATDOC D(8);
      ,t_LDCODCLF C(15);
      ,t_DESCON C(40);
      ,CPROWNUM N(10);
      ,t_LDSERDOC C(10);
      ,t_LDPARAME C(3);
      ,t_LDTIPCLF C(1);
      ,t_APPO C(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsve_mldbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWNUM_2_1.controlsource=this.cTrsName+'.t_CPROWNUM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLDTIPDOC_2_3.controlsource=this.cTrsName+'.t_LDTIPDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLDNUMDOC_2_4.controlsource=this.cTrsName+'.t_LDNUMDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLDALFDOC_2_5.controlsource=this.cTrsName+'.t_LDALFDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLDDATDOC_2_6.controlsource=this.cTrsName+'.t_LDDATDOC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLDCODCLF_2_7.controlsource=this.cTrsName+'.t_LDCODCLF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCON_2_10.controlsource=this.cTrsName+'.t_DESCON'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(69)
    this.AddVLine(131)
    this.AddVLine(193)
    this.AddVLine(228)
    this.AddVLine(313)
    this.AddVLine(437)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWNUM_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.LDOCGENE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LDOCGENE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.LDOCGENE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LDOCGENE_IDX,2])
      *
      * insert into LDOCGENE
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'LDOCGENE')
        i_extval=cp_InsertValODBCExtFlds(this,'LDOCGENE')
        i_cFldBody=" "+;
                  "(LDSERIAL,LDSERDOC,LDTIPDOC,LDNUMDOC,LDALFDOC"+;
                  ",LDDATDOC,LDCODCLF,LDPARAME,LDTIPCLF,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_LDSERIAL)+","+cp_ToStrODBCNull(this.w_LDSERDOC)+","+cp_ToStrODBC(this.w_LDTIPDOC)+","+cp_ToStrODBC(this.w_LDNUMDOC)+","+cp_ToStrODBC(this.w_LDALFDOC)+;
             ","+cp_ToStrODBC(this.w_LDDATDOC)+","+cp_ToStrODBCNull(this.w_LDCODCLF)+","+cp_ToStrODBC(this.w_LDPARAME)+","+cp_ToStrODBC(this.w_LDTIPCLF)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'LDOCGENE')
        i_extval=cp_InsertValVFPExtFlds(this,'LDOCGENE')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'LDSERIAL',this.w_LDSERIAL,'CPROWNUM',this.w_CPROWNUM)
        INSERT INTO (i_cTable) (;
                   LDSERIAL;
                  ,LDSERDOC;
                  ,LDTIPDOC;
                  ,LDNUMDOC;
                  ,LDALFDOC;
                  ,LDDATDOC;
                  ,LDCODCLF;
                  ,LDPARAME;
                  ,LDTIPCLF;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_LDSERIAL;
                  ,this.w_LDSERDOC;
                  ,this.w_LDTIPDOC;
                  ,this.w_LDNUMDOC;
                  ,this.w_LDALFDOC;
                  ,this.w_LDDATDOC;
                  ,this.w_LDCODCLF;
                  ,this.w_LDPARAME;
                  ,this.w_LDTIPCLF;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.LDOCGENE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LDOCGENE_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_CPROWNUM > 0 AND NOT EMPTY(t_LDSERDOC)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'LDOCGENE')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'LDOCGENE')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_CPROWNUM > 0 AND NOT EMPTY(t_LDSERDOC)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update LDOCGENE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'LDOCGENE')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " LDSERDOC="+cp_ToStrODBCNull(this.w_LDSERDOC)+;
                     ",LDTIPDOC="+cp_ToStrODBC(this.w_LDTIPDOC)+;
                     ",LDNUMDOC="+cp_ToStrODBC(this.w_LDNUMDOC)+;
                     ",LDALFDOC="+cp_ToStrODBC(this.w_LDALFDOC)+;
                     ",LDDATDOC="+cp_ToStrODBC(this.w_LDDATDOC)+;
                     ",LDCODCLF="+cp_ToStrODBCNull(this.w_LDCODCLF)+;
                     ",LDPARAME="+cp_ToStrODBC(this.w_LDPARAME)+;
                     ",LDTIPCLF="+cp_ToStrODBC(this.w_LDTIPCLF)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'LDOCGENE')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      LDSERDOC=this.w_LDSERDOC;
                     ,LDTIPDOC=this.w_LDTIPDOC;
                     ,LDNUMDOC=this.w_LDNUMDOC;
                     ,LDALFDOC=this.w_LDALFDOC;
                     ,LDDATDOC=this.w_LDDATDOC;
                     ,LDCODCLF=this.w_LDCODCLF;
                     ,LDPARAME=this.w_LDPARAME;
                     ,LDTIPCLF=this.w_LDTIPCLF;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsve_mld
    if not(bTrsErr)
       * --- Elimina Documenti Associati
       this.NotifyEvent('CancellaRighe')
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.LDOCGENE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LDOCGENE_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWNUM > 0 AND NOT EMPTY(t_LDSERDOC)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete LDOCGENE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWNUM > 0 AND NOT EMPTY(t_LDSERDOC)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsve_mld
    if not(bTrsErr)
       * --- Elimina Documenti Associati
       this.NotifyEvent('CancellaTutto')
    endif
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.LDOCGENE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LDOCGENE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .link_2_2('Full')
        .DoRTCalc(4,7,.t.)
          .link_2_7('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_LDSERDOC with this.w_LDSERDOC
      replace t_LDPARAME with this.w_LDPARAME
      replace t_LDTIPCLF with this.w_LDTIPCLF
      replace t_APPO with this.w_APPO
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBtn_2_12.enabled =this.oPgFrm.Page1.oPag.oBtn_2_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_13.enabled =this.oPgFrm.Page1.oPag.oBtn_2_13.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBtn_2_12.visible=!this.oPgFrm.Page1.oPag.oBtn_2_12.mHide()
    this.oPgFrm.Page1.oPag.oBtn_2_13.visible=!this.oPgFrm.Page1.oPag.oBtn_2_13.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LDSERDOC
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LDSERDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LDSERDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_LDSERDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_LDSERDOC)
            select MVSERIAL,MVTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LDSERDOC = NVL(_Link_.MVSERIAL,space(10))
      this.w_APPO = NVL(_Link_.MVTIPDOC,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_LDSERDOC = space(10)
      endif
      this.w_APPO = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LDSERDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DOC_MAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.MVSERIAL as MVSERIAL202"+ ",link_2_2.MVTIPDOC as MVTIPDOC202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on LDOCGENE.LDSERDOC=link_2_2.MVSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and LDOCGENE.LDSERDOC=link_2_2.MVSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LDCODCLF
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LDCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LDCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_LDCODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LDTIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_LDTIPCLF;
                       ,'ANCODICE',this.w_LDCODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LDCODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LDCODCLF = space(15)
      endif
      this.w_DESCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LDCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.ANCODICE as ANCODICE207"+ ",link_2_7.ANDESCRI as ANDESCRI207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on LDOCGENE.LDCODCLF=link_2_7.ANCODICE"+" and LDOCGENE.LDTIPCLF=link_2_7.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and LDOCGENE.LDCODCLF=link_2_7.ANCODICE(+)"'+'+" and LDOCGENE.LDTIPCLF=link_2_7.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWNUM_2_1.value==this.w_CPROWNUM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWNUM_2_1.value=this.w_CPROWNUM
      replace t_CPROWNUM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWNUM_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLDTIPDOC_2_3.value==this.w_LDTIPDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLDTIPDOC_2_3.value=this.w_LDTIPDOC
      replace t_LDTIPDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLDTIPDOC_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLDNUMDOC_2_4.value==this.w_LDNUMDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLDNUMDOC_2_4.value=this.w_LDNUMDOC
      replace t_LDNUMDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLDNUMDOC_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLDALFDOC_2_5.value==this.w_LDALFDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLDALFDOC_2_5.value=this.w_LDALFDOC
      replace t_LDALFDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLDALFDOC_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLDDATDOC_2_6.value==this.w_LDDATDOC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLDDATDOC_2_6.value=this.w_LDDATDOC
      replace t_LDDATDOC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLDDATDOC_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLDCODCLF_2_7.value==this.w_LDCODCLF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLDCODCLF_2_7.value=this.w_LDCODCLF
      replace t_LDCODCLF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLDCODCLF_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCON_2_10.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCON_2_10.value=this.w_DESCON
      replace t_DESCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCON_2_10.value
    endif
    cp_SetControlsValueExtFlds(this,'LDOCGENE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(lower(this.oParentObject.class)<>"tgsve_kld")
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("Impossibile salvare modifiche"))
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case not(.F.)
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = cp_Translate("impossibile modificare righe")
      endcase
      if .w_CPROWNUM > 0 AND NOT EMPTY(.w_LDSERDOC)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWNUM > 0 AND NOT EMPTY(t_LDSERDOC))
    select(i_nArea)
    return(i_bRes)
  Endfunc
  Func CanDeleteRow()
    local i_bRes
    i_bRes=lower(this.oParentObject.class)<>"tgsve_kld"
    if !i_bRes
      cp_ErrorMsg("Impossibile eliminare righe","stop","")
    endif
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_LDSERDOC=space(10)
      .w_LDTIPDOC=space(5)
      .w_LDNUMDOC=0
      .w_LDALFDOC=space(10)
      .w_LDDATDOC=ctod("  /  /  ")
      .w_LDCODCLF=space(15)
      .w_LDPARAME=space(3)
      .w_LDTIPCLF=space(1)
      .w_DESCON=space(40)
      .w_APPO=space(5)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_LDSERDOC))
        .link_2_2('Full')
      endif
      .DoRTCalc(4,8,.f.)
      if not(empty(.w_LDCODCLF))
        .link_2_7('Full')
      endif
    endwith
    this.DoRTCalc(9,12,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWNUM = t_CPROWNUM
    this.w_LDSERDOC = t_LDSERDOC
    this.w_LDTIPDOC = t_LDTIPDOC
    this.w_LDNUMDOC = t_LDNUMDOC
    this.w_LDALFDOC = t_LDALFDOC
    this.w_LDDATDOC = t_LDDATDOC
    this.w_LDCODCLF = t_LDCODCLF
    this.w_LDPARAME = t_LDPARAME
    this.w_LDTIPCLF = t_LDTIPCLF
    this.w_DESCON = t_DESCON
    this.w_APPO = t_APPO
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWNUM with this.w_CPROWNUM
    replace t_LDSERDOC with this.w_LDSERDOC
    replace t_LDTIPDOC with this.w_LDTIPDOC
    replace t_LDNUMDOC with this.w_LDNUMDOC
    replace t_LDALFDOC with this.w_LDALFDOC
    replace t_LDDATDOC with this.w_LDDATDOC
    replace t_LDCODCLF with this.w_LDCODCLF
    replace t_LDPARAME with this.w_LDPARAME
    replace t_LDTIPCLF with this.w_LDTIPCLF
    replace t_DESCON with this.w_DESCON
    replace t_APPO with this.w_APPO
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
  func CanEdit()
    local i_res
    i_res=.F.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("impossibile modificare righe"))
    endif
    return(i_res)
  func CanAdd()
    local i_res
    i_res=.F.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile aggiungere righe"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=lower(this.oParentObject.class)<>"tgsve_kld"
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile eliminare"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsve_mldPag1 as StdContainer
  Width  = 772
  height = 289
  stdWidth  = 772
  stdheight = 289
  resizeXpos=523
  resizeYpos=84
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=11, top=0, width=738,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWNUM",Label1="Riga",Field2="LDTIPDOC",Label2="Cau. Doc.",Field3="LDNUMDOC",Label3="Doc. n�",Field4="LDALFDOC",Label4="Serie",Field5="LDDATDOC",Label5="Del",Field6="LDCODCLF",Label6="Intestatario",Field7="DESCON",Label7="Descrizione intestatario",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 101513082

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=1,top=19,;
    width=734+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=2,top=20,width=733+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_12 as StdButton with uid="FHZTLQDQLB",width=48,height=45,;
   left=11, top=237,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere al documento selezionato";
    , HelpContextID = 255756385;
    , Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_12.Click()
      with this.Parent.oContained
        IIF(EMPTY(.w_LDSERDOC),"",OpenGest('A', "GSVE_MDV('"+.w_LDPARAME+"')",'MVSERIAL',.w_LDSERDOC ))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_12.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_LDSERDOC))
    endwith
  endfunc

  func oBtn_2_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION='ADHOC REVOLUTION')
    endwith
   endif
  endfunc

  add object oBtn_2_13 as StdButton with uid="LPMLYRHPLK",width=48,height=45,;
   left=11, top=237,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere al documento selezionato";
    , HelpContextID = 255756385;
    , Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_13.Click()
      with this.Parent.oContained
        GSAR_BZM(this.Parent.oContained,.w_LDSERDOC,-20)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_13.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_LDSERDOC))
    endwith
  endfunc

  func oBtn_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION='ad hoc ENTERPRISE')
    endwith
   endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsve_mldBodyRow as CPBodyRowCnt
  Width=724
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWNUM_2_1 as StdTrsField with uid="CZBLGTKCBH",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWNUM",value=0,isprimarykey=.t.,;
    HelpContextID = 117064307,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=-2, Top=0

  add object oLDTIPDOC_2_3 as StdTrsField with uid="IQFMIGFGXZ",rtseq=4,rtrep=.t.,;
    cFormVar="w_LDTIPDOC",value=space(5),enabled=.f.,;
    HelpContextID = 58435847,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=57, Left=59, Top=0, InputMask=replicate('X',5)

  add object oLDNUMDOC_2_4 as StdTrsField with uid="MJSYMISOVC",rtseq=5,rtrep=.t.,;
    cFormVar="w_LDNUMDOC",value=0,enabled=.f.,;
    HelpContextID = 60819719,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=57, Left=120, Top=0

  add object oLDALFDOC_2_5 as StdTrsField with uid="LLGIGUNUOT",rtseq=6,rtrep=.t.,;
    cFormVar="w_LDALFDOC",value=space(10),enabled=.f.,;
    HelpContextID = 68802823,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=29, Left=183, Top=0, InputMask=replicate('X',10)

  add object oLDDATDOC_2_6 as StdTrsField with uid="OQCHDHEXAH",rtseq=7,rtrep=.t.,;
    cFormVar="w_LDDATDOC",value=ctod("  /  /  "),enabled=.f.,;
    HelpContextID = 54831367,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=78, Left=219, Top=0

  add object oLDCODCLF_2_7 as StdTrsField with uid="UILLZHXLIH",rtseq=8,rtrep=.t.,;
    cFormVar="w_LDCODCLF",value=space(15),enabled=.f.,;
    HelpContextID = 87472388,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=118, Left=304, Top=0, InputMask=replicate('X',15), cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_LDTIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_LDCODCLF"

  func oLDCODCLF_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESCON_2_10 as StdTrsField with uid="HRGJIVUFYW",rtseq=11,rtrep=.t.,;
    cFormVar="w_DESCON",value=space(40),enabled=.f.,;
    HelpContextID = 107890486,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=293, Left=426, Top=0, InputMask=replicate('X',40)
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWNUM_2_1.When()
    return(.t.)
  proc oCPROWNUM_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWNUM_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=10
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_mld','LDOCGENE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LDSERIAL=LDOCGENE.LDSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
