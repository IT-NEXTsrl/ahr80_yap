* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_agr                                                        *
*              Gruppi intestatari EDI                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [39][VRS_2]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-12                                                      *
* Last revis.: 2007-10-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_agr"))

* --- Class definition
define class tgsar_agr as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 431
  Height = 78+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-10-22"
  HelpContextID=42563433
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=2

  * --- Constant Properties
  GRU_INTE_IDX = 0
  cFile = "GRU_INTE"
  cKeySelect = "GRCODICE"
  cKeyWhere  = "GRCODICE=this.w_GRCODICE"
  cKeyWhereODBC = '"GRCODICE="+cp_ToStrODBC(this.w_GRCODICE)';

  cKeyWhereODBCqualified = '"GRU_INTE.GRCODICE="+cp_ToStrODBC(this.w_GRCODICE)';

  cPrg = "gsar_agr"
  cComment = "Gruppi intestatari EDI"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_GRCODICE = space(10)
  w_GRDESCRI = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GRU_INTE','gsar_agr')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_agrPag1","gsar_agr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Gruppo intestatari EDI")
      .Pages(1).HelpContextID = 97547472
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGRCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='GRU_INTE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GRU_INTE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GRU_INTE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_GRCODICE = NVL(GRCODICE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from GRU_INTE where GRCODICE=KeySet.GRCODICE
    *
    i_nConn = i_TableProp[this.GRU_INTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GRU_INTE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GRU_INTE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GRU_INTE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GRCODICE',this.w_GRCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_GRCODICE = NVL(GRCODICE,space(10))
        .w_GRDESCRI = NVL(GRDESCRI,space(35))
        cp_LoadRecExtFlds(this,'GRU_INTE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_GRCODICE = space(10)
      .w_GRDESCRI = space(35)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'GRU_INTE')
    this.DoRTCalc(1,2,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGRCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oGRDESCRI_1_3.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oGRCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oGRCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'GRU_INTE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GRU_INTE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GRCODICE,"GRCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GRDESCRI,"GRDESCRI",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GRU_INTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2])
    i_lTable = "GRU_INTE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GRU_INTE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSVA_SGI with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GRU_INTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.GRU_INTE_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into GRU_INTE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GRU_INTE')
        i_extval=cp_InsertValODBCExtFlds(this,'GRU_INTE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(GRCODICE,GRDESCRI "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_GRCODICE)+;
                  ","+cp_ToStrODBC(this.w_GRDESCRI)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GRU_INTE')
        i_extval=cp_InsertValVFPExtFlds(this,'GRU_INTE')
        cp_CheckDeletedKey(i_cTable,0,'GRCODICE',this.w_GRCODICE)
        INSERT INTO (i_cTable);
              (GRCODICE,GRDESCRI  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_GRCODICE;
                  ,this.w_GRDESCRI;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.GRU_INTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.GRU_INTE_IDX,i_nConn)
      *
      * update GRU_INTE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'GRU_INTE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " GRDESCRI="+cp_ToStrODBC(this.w_GRDESCRI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'GRU_INTE')
        i_cWhere = cp_PKFox(i_cTable  ,'GRCODICE',this.w_GRCODICE  )
        UPDATE (i_cTable) SET;
              GRDESCRI=this.w_GRDESCRI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GRU_INTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.GRU_INTE_IDX,i_nConn)
      *
      * delete GRU_INTE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'GRCODICE',this.w_GRCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GRU_INTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,2,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGRCODICE_1_1.value==this.w_GRCODICE)
      this.oPgFrm.Page1.oPag.oGRCODICE_1_1.value=this.w_GRCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oGRDESCRI_1_3.value==this.w_GRDESCRI)
      this.oPgFrm.Page1.oPag.oGRDESCRI_1_3.value=this.w_GRDESCRI
    endif
    cp_SetControlsValueExtFlds(this,'GRU_INTE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_agrPag1 as StdContainer
  Width  = 427
  height = 78
  stdWidth  = 427
  stdheight = 78
  resizeXpos=380
  resizeYpos=74
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGRCODICE_1_1 as StdField with uid="BRIRUHVJKJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_GRCODICE", cQueryName = "GRCODICE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo",;
    HelpContextID = 185208747,;
    FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=22, Width=94, Left=159, Top=24, InputMask=replicate('X',10)

  add object oGRDESCRI_1_3 as StdField with uid="RLUESVMNKS",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GRDESCRI", cQueryName = "GRDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione gruppo",;
    HelpContextID = 168812625,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=159, Top=48, InputMask=replicate('X',35)

  add object oStr_1_2 as StdString with uid="YGLEHKPJNT",Visible=.t., Left=6, Top=26,;
    Alignment=1, Width=152, Height=19,;
    Caption="Codice gruppo:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="NQAUVJSQUM",Visible=.t., Left=6, Top=52,;
    Alignment=1, Width=152, Height=19,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_agr','GRU_INTE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GRCODICE=GRU_INTE.GRCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
