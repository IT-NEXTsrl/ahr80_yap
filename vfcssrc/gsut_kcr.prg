* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kcr                                                        *
*              Controllo relazioni tabella                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-26                                                      *
* Last revis.: 2016-05-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kcr",oParentObject))

* --- Class definition
define class tgsut_kcr as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 701
  Height = 561
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-19"
  HelpContextID=99173225
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  XDC_TABLE_IDX = 0
  cPrg = "gsut_kcr"
  cComment = "Controllo relazioni tabella"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TABELLA = space(15)
  w_DESTAB = space(60)
  w_RELAZ = space(254)
  w_CURSOR = space(10)
  w_DIREZ = space(1)
  w_TreeView = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kcrPag1","gsut_kcr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTABELLA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_TreeView = this.oPgFrm.Pages(1).oPag.TreeView
    DoDefault()
    proc Destroy()
      this.w_TreeView = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='XDC_TABLE'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TABELLA=space(15)
      .w_DESTAB=space(60)
      .w_RELAZ=space(254)
      .w_CURSOR=space(10)
      .w_DIREZ=space(1)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_TABELLA))
          .link_1_1('Full')
        endif
      .oPgFrm.Page1.oPag.TreeView.Calculate()
          .DoRTCalc(2,4,.f.)
        .w_DIREZ = 'F'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.TreeView.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.TreeView.Calculate()
    endwith
  return

  proc Calculate_THIVYPJATY()
    with this
          * --- Assegna relazione
          .w_RELAZ = Alltrim(Nvl( .w_TREEVIEW.GetVar('TABREL'),''))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.TreeView.Event(cEvent)
        if lower(cEvent)==lower("w_treeview NodeSelected")
          .Calculate_THIVYPJATY()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TABELLA
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TABELLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBNAME like "+cp_ToStrODBC(trim(this.w_TABELLA)+"%");

          i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',trim(this.w_TABELLA))
          select TBNAME,TBCOMMENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TABELLA)==trim(_Link_.TBNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TABELLA) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBNAME',cp_AbsName(oSource.parent,'oTABELLA_1_1'),i_cWhere,'',"Tabelle",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                     +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1))
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TABELLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(this.w_TABELLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_TABELLA)
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TABELLA = NVL(_Link_.TBNAME,space(15))
      this.w_DESTAB = NVL(_Link_.TBCOMMENT,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_TABELLA = space(15)
      endif
      this.w_DESTAB = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TABELLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTABELLA_1_1.value==this.w_TABELLA)
      this.oPgFrm.Page1.oPag.oTABELLA_1_1.value=this.w_TABELLA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTAB_1_2.value==this.w_DESTAB)
      this.oPgFrm.Page1.oPag.oDESTAB_1_2.value=this.w_DESTAB
    endif
    if not(this.oPgFrm.Page1.oPag.oRELAZ_1_7.value==this.w_RELAZ)
      this.oPgFrm.Page1.oPag.oRELAZ_1_7.value=this.w_RELAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oDIREZ_1_14.RadioValue()==this.w_DIREZ)
      this.oPgFrm.Page1.oPag.oDIREZ_1_14.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kcrPag1 as StdContainer
  Width  = 697
  height = 561
  stdWidth  = 697
  stdheight = 561
  resizeXpos=376
  resizeYpos=367
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTABELLA_1_1 as StdField with uid="XPOPJYCQME",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TABELLA", cQueryName = "TABELLA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Tabella della quale si vogliono controllare le integrita referenziali",;
    HelpContextID = 81779914,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=126, Top=37, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="XDC_TABLE", oKey_1_1="TBNAME", oKey_1_2="this.w_TABELLA"

  func oTABELLA_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oTABELLA_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTABELLA_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'XDC_TABLE','*','TBNAME',cp_AbsName(this.parent,'oTABELLA_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tabelle",'',this.parent.oContained
  endproc

  add object oDESTAB_1_2 as StdField with uid="LYCTBJNVTP",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESTAB", cQueryName = "DESTAB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 260032970,;
   bGlobalFont=.t.,;
    Height=21, Width=440, Left=246, Top=37, InputMask=replicate('X',60)


  add object oBtn_1_4 as StdButton with uid="SKOOVQACPP",left=638, top=64, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 77749014;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        GSUT1BCR(this.Parent.oContained,"Open")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_TABELLA))
      endwith
    endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="AUWPCNUDEI",left=638, top=508, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 223547142;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object TreeView as cp_Treeview with uid="GYODZTEHZL",left=59, top=65, width=573,height=411,;
    caption='Object',;
   bGlobalFont=.t.,;
    cNodeBmp="mtree.bmp",cLeafBmp="master.bmp",nIndent=20,cCursor="Vista",cShowFields="ALLTRIM(TABSON)+' - '+ALLTRIM(DESTAB)",cNodeShowField="",cLeafShowField="",;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 78824422

  add object oRELAZ_1_7 as StdField with uid="WGCOBCEROT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RELAZ", cQueryName = "RELAZ",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Relazione con tabella padre",;
    HelpContextID = 211178,;
   bGlobalFont=.t.,;
    Height=21, Width=506, Left=126, Top=480, InputMask=replicate('X',254), ReadOnly=.T.


  add object oBtn_1_8 as StdButton with uid="XJTSIFJRQC",left=638, top=457, width=48,height=45,;
    CpPicture="COPY.BMP", caption="", nPag=1;
    , ToolTipText = "copia relazione nella clipboard";
    , HelpContextID = 98972202;
    , caption='\<Copia';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        _cliptext=.w_RELAZ
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_11 as StdButton with uid="RQSKFAEHAZ",left=638, top=114, width=48,height=45,;
    CpPicture="BMP\ESPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per esplodere la treeview";
    , HelpContextID = 176540602;
    , caption='\<Esplodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_TABELLA))
      endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="POVNMSFBDI",left=638, top=163, width=48,height=45,;
    CpPicture="BMP\IMPLODI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per implodere la treeview";
    , HelpContextID = 176542074;
    , caption='\<Implodi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        .w_TREEVIEW.ExpandAll(.F.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_TABELLA))
      endwith
    endif
  endfunc


  add object oDIREZ_1_14 as StdCombo with uid="ZXBUARTTJR",rtseq=5,rtrep=.f.,left=126,top=9,width=116,height=21;
    , ToolTipText = "Indica se si cercano le tabelle da cui dipende la tabella selezionata o quelle che dipendono dalla tabella selezionata";
    , HelpContextID = 268359114;
    , cFormVar="w_DIREZ",RowSource=""+"che dipendono da,"+"da cui dipende", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDIREZ_1_14.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oDIREZ_1_14.GetRadio()
    this.Parent.oContained.w_DIREZ = this.RadioValue()
    return .t.
  endfunc

  func oDIREZ_1_14.SetRadio()
    this.Parent.oContained.w_DIREZ=trim(this.Parent.oContained.w_DIREZ)
    this.value = ;
      iif(this.Parent.oContained.w_DIREZ=='I',1,;
      iif(this.Parent.oContained.w_DIREZ=='F',2,;
      0))
  endfunc

  add object oStr_1_3 as StdString with uid="MFMYRXBUAX",Visible=.t., Left=9, Top=37,;
    Alignment=1, Width=114, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="IHRDCGPYUW",Visible=.t., Left=9, Top=481,;
    Alignment=1, Width=114, Height=18,;
    Caption="Relazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="OMHXRZDTQU",Visible=.t., Left=9, Top=10,;
    Alignment=1, Width=114, Height=18,;
    Caption="Tabelle:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kcr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
