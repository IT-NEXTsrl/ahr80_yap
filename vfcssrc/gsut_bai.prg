* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bai                                                        *
*              Aggiorna indici documentali                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-09-02                                                      *
* Last revis.: 2014-09-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bai",oParentObject,m.pOPER)
return(i_retval)

define class tgsut_bai as StdBatch
  * --- Local variables
  pOPER = space(10)
  NC = space(10)
  w_IDSERIAL = space(15)
  w_IDCODATT = space(20)
  w_SEARCHKEY = space(40)
  w_SEARCHODBC = space(40)
  w_CPROWNUM = 0
  w_CPROWMAX = 0
  w_CPROWORD = 0
  w_KEYVALUE = space(10)
  i_nTable_idx = 0
  i_cTable = space(10)
  w_SQLCMD = space(10)
  w_MSGERR = space(10)
  w_VALATTR = space(100)
  w_IDVALDAT = ctod("  /  /  ")
  w_IDVALNUM = 0
  w_CHKOBBL = .f.
  w_TIPOATTR = space(1)
  w_CDVLPRED = space(100)
  w_CHKVALPRED = .f.
  w_VALPRED = space(100)
  w_CDCAMCUR = space(100)
  w_TMPT = space(1)
  w_OLDAREA = 0
  * --- WorkFile variables
  PRODINDI_idx=0
  PRODCLAS_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione aggiornamento attributi indici documentali (da GSUT_KAI)
    do case
      case this.pOPER="A"
        if "S" $ this.oParentObject.w_AGGIUNGI+this.oParentObject.w_ELIMINA+this.oParentObject.w_MODIFICA
          Create Cursor LOGERR (IDSERIAL C(20), CPROWORD N(6,0), IDCODATT C(15), MSGG C(254))
          VQ_EXEC("query\gsut_bai", this, "PRODCLAS")
          Select PRODCLAS
          Locate for CDTABKEY=alltrim(this.oParentObject.w_ARCHIVIO)
          if found()
            this.w_SEARCHKEY = RTRIM(nvl(CDCODATT,""))
            this.w_SEARCHODBC = RTRIM(nvl(CDCAMCUR,""))
          endif
          if empty(this.w_SEARCHKEY) OR empty(this.w_SEARCHODBC)
            ah_ERRORMSG("Nessun campo chiave inserito")
          else
            this.i_nTable_idx = cp_OpenTable(this.oParentObject.w_ARCHIVIO, .T.)
            if this.i_nTable_idx>0
              this.i_cTable = cp_SetAzi(i_TableProp[this.i_nTable_idx,2])
            endif
            if not empty(this.i_cTable)
              this.NC = this.oParentObject.w_ZoomGF.cCursor
              Select (this.NC)
              SCAN FOR XCHK=1
              this.w_IDSERIAL = GFKEYINDIC
              update PRODCLAS set inserisci=1
              * --- Try
              local bErr_03978758
              bErr_03978758=bTrsErr
              this.Try_03978758()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- rollback
                bTrsErr=.t.
                cp_EndTrs(.t.)
                Delete from LOGERR where IDSERIAL=this.w_IDSERIAL
                this.Aggiorna_log(this.w_IDSERIAL,this.w_CPROWORD,this.w_IDCODATT,i_ErrMsg+this.w_MSGERR)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              bTrsErr=bTrsErr or bErr_03978758
              * --- End
              use in select("_ATTTRIB_")
              Select (this.NC)
              ENDSCAN
            endif
          endif
          use in Select("PRODCLAS")
          if reccount("LOGERR")=0
            ah_ErrorMsg("Nessun indice documentale processato")
          else
            if ah_YesNo("Si desidera stampare il log degli aggiornamenti eseguiti?")
              Select * from LOGERR order by IDSERIAL,CPROWORD into cursor __TMP__ nofilter
              Select __TMP__ 
              cp_chprn("query\gsut_bai"," ", this)
            endif
          endif
          use in Select("LOGERR")
        endif
    endcase
  endproc
  proc Try_03978758()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Select from PRODINDI
    i_nConn=i_TableProp[this.PRODINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PRODINDI ";
          +" where IDSERIAL="+cp_ToStrODBC(this.w_IDSERIAL)+"";
          +" order by CPROWORD";
           ,"_Curs_PRODINDI")
    else
      select * from (i_cTable);
       where IDSERIAL=this.w_IDSERIAL;
       order by CPROWORD;
        into cursor _Curs_PRODINDI
    endif
    if used('_Curs_PRODINDI')
      select _Curs_PRODINDI
      locate for 1=1
      do while not(eof())
      if RECNO()=1
        * --- Leggo l'attibuto campo chiave
        this.w_KEYVALUE = ""
        Locate for IDTABKEY=alltrim(this.oParentObject.w_ARCHIVIO)
        if FOUND()
          do case
            case _Curs_PRODINDI.IDTIPATT="N"
              this.w_KEYVALUE = NVL(_Curs_PRODINDI.IDVALNUM,0)
            case _Curs_PRODINDI.IDTIPATT="D"
              this.w_KEYVALUE = CTOD(NVL(_Curs_PRODINDI.IDVALATT,{}))
            otherwise
              this.w_KEYVALUE = NVL(_Curs_PRODINDI.IDVALATT,"")
          endcase
          GO TOP
          locate for 1=1
        else
          * --- Valore per campo chiave non trovato: esco
          GO BOTTOM
          this.Aggiorna_log(this.w_IDSERIAL,0,"","Valore per campo chiave non trovato")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if NOT EMPTY(this.w_KEYVALUE)
          * --- Costruisco il cursore con i dati
          this.w_SQLCMD = "Select * from " + this.i_cTable + " where "
          this.w_SQLCMD = this.w_SQLCMD + iif(CP_DBTYPE="SQLServer", this.w_SEARCHODBC, strtran(this.w_SEARCHODBC,"+","||")) + " = " + cp_tostrODBC(alltrim(this.w_KEYVALUE))
          CP_SQLEXEC(1, this.w_SQLCMD, "_ATTTRIB_")
          if USED("_ATTTRIB_")
            if reccount("_ATTTRIB_")=0
              * --- nessun record trovato
              this.Aggiorna_log(this.w_IDSERIAL,0,"",ah_MsgFormat("Nessun record trovato:%0%1",this.w_SQLCMD))
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          else
            * --- Errore frase sql
            this.Aggiorna_log(this.w_IDSERIAL,0,"",ah_MsgFormat("Errore esecuzione query:%0%1%0Errore: %2",this.w_SQLCMD,message()))
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
      if USED("_ATTTRIB_") AND reccount("_ATTTRIB_")>0
        this.w_CPROWNUM = _Curs_PRODINDI.CPROWNUM
        this.w_CPROWMAX = MAX(this.w_CPROWNUM,this.w_CPROWMAX)
        this.w_CPROWORD = _Curs_PRODINDI.CPROWORD
        this.w_IDCODATT = _Curs_PRODINDI.IDCODATT
        * --- Cerco l'attributo nel dettaglio della classe documentale
        Select PRODCLAS
        Locate for CDCODATT=this.w_IDCODATT
        if found()
          Replace inserisci with 0
          if this.oParentObject.w_NRIGACLAS="S"
            this.w_CPROWORD = CPROWORD
          endif
          if this.oParentObject.w_MODIFICA="S"
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if EMPTY(this.w_MSGERR)
              * --- Write into PRODINDI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PRODINDI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PRODINDI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PRODINDI','CPROWORD');
                +",IDTIPATT ="+cp_NullLink(cp_ToStrODBC(PRODCLAS.CDTIPATT),'PRODINDI','IDTIPATT');
                +",IDCHKOBB ="+cp_NullLink(cp_ToStrODBC(PRODCLAS.CDCHKOBB),'PRODINDI','IDCHKOBB');
                +",IDVALATT ="+cp_NullLink(cp_ToStrODBC(this.w_VALATTR),'PRODINDI','IDVALATT');
                +",IDTABKEY ="+cp_NullLink(cp_ToStrODBC(PRODCLAS.CDTABKEY),'PRODINDI','IDTABKEY');
                +",IDVALDAT ="+cp_NullLink(cp_ToStrODBC(this.w_IDVALDAT),'PRODINDI','IDVALDAT');
                +",IDDESATT ="+cp_NullLink(cp_ToStrODBC(PRODCLAS.CDDESATT),'PRODINDI','IDDESATT');
                +",IDVALNUM ="+cp_NullLink(cp_ToStrODBC(this.w_IDVALNUM),'PRODINDI','IDVALNUM');
                    +i_ccchkf ;
                +" where ";
                    +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                       )
              else
                update (i_cTable) set;
                    CPROWORD = this.w_CPROWORD;
                    ,IDTIPATT = PRODCLAS.CDTIPATT;
                    ,IDCHKOBB = PRODCLAS.CDCHKOBB;
                    ,IDVALATT = this.w_VALATTR;
                    ,IDTABKEY = PRODCLAS.CDTABKEY;
                    ,IDVALDAT = this.w_IDVALDAT;
                    ,IDDESATT = PRODCLAS.CDDESATT;
                    ,IDVALNUM = this.w_IDVALNUM;
                    &i_ccchkf. ;
                 where;
                    IDSERIAL = this.w_IDSERIAL;
                    and CPROWNUM = this.w_CPROWNUM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              if i_Rows=0
                this.w_MSGERR = Message()
                if this.oParentObject.w_IGN_ERR="S"
                  this.Aggiorna_log(this.w_IDSERIAL,this.w_CPROWORD,this.w_IDCODATT,"write error: "+this.w_MSGERR)
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  * --- accept error
                  bTrsErr=.f.
                else
                  * --- Raise
                  i_Error="write error: "
                  return
                endif
              endif
              this.Aggiorna_log(this.w_IDSERIAL,this.w_CPROWORD,this.w_IDCODATT,"Aggiornato: "+this.w_VALATTR)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              if this.oParentObject.w_IGN_ERR="S"
                this.Aggiorna_log(this.w_IDSERIAL,this.w_CPROWORD,this.w_IDCODATT,"write error: "+this.w_MSGERR)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                * --- Raise
                i_Error="write error: "
                return
              endif
            endif
          endif
        else
          if this.oParentObject.w_ELIMINA="S"
            * --- Delete from PRODINDI
            i_nConn=i_TableProp[this.PRODINDI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                     )
            else
              delete from (i_cTable) where;
                    IDSERIAL = this.w_IDSERIAL;
                    and CPROWNUM = this.w_CPROWNUM;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            if i_Rows=0
              this.w_MSGERR = Message()
              if this.oParentObject.w_IGN_ERR="S"
                this.Aggiorna_log(this.w_IDSERIAL,this.w_CPROWORD,this.w_IDCODATT,i_ErrMsg+this.w_MSGERR)
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                * --- accept error
                bTrsErr=.f.
              else
                * --- Raise
                i_Error="delete error: "
                return
              endif
            endif
            this.Aggiorna_log(this.w_IDSERIAL,this.w_CPROWORD,this.w_IDCODATT,"Eliminato")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
      endif
        select _Curs_PRODINDI
        continue
      enddo
      use
    endif
    if this.oParentObject.w_AGGIUNGI="S" and USED("_ATTTRIB_") AND reccount("_ATTTRIB_")>0
      this.w_CPROWNUM = this.w_CPROWMAX
      Select PRODCLAS
      SCAN FOR inserisci=1
      this.w_CPROWNUM = this.w_CPROWNUM+1
      if this.oParentObject.w_NRIGACLAS="S"
        this.w_CPROWORD = CPROWORD
      else
        this.w_CPROWORD = this.w_CPROWORD+10
      endif
      this.w_IDCODATT = CDCODATT
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if EMPTY(this.w_MSGERR)
        * --- Insert into PRODINDI
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
        i_commit = .f.
        local bErr_03979538
        bErr_03979538=bTrsErr
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDCODATT"+",IDTIPATT"+",IDCHKOBB"+",IDVALATT"+",IDTABKEY"+",IDVALDAT"+",IDDESATT"+",IDVALNUM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_IDSERIAL),'PRODINDI','IDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PRODINDI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PRODINDI','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(PRODCLAS.CDCODATT),'PRODINDI','IDCODATT');
          +","+cp_NullLink(cp_ToStrODBC(PRODCLAS.CDTIPATT),'PRODINDI','IDTIPATT');
          +","+cp_NullLink(cp_ToStrODBC(PRODCLAS.CDCHKOBB),'PRODINDI','IDCHKOBB');
          +","+cp_NullLink(cp_ToStrODBC(this.w_VALATTR),'PRODINDI','IDVALATT');
          +","+cp_NullLink(cp_ToStrODBC(PRODCLAS.CDTABKEY),'PRODINDI','IDTABKEY');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IDVALDAT),'PRODINDI','IDVALDAT');
          +","+cp_NullLink(cp_ToStrODBC(PRODCLAS.CDDESATT),'PRODINDI','IDDESATT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IDVALNUM),'PRODINDI','IDVALNUM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',this.w_IDSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'IDCODATT',PRODCLAS.CDCODATT,'IDTIPATT',PRODCLAS.CDTIPATT,'IDCHKOBB',PRODCLAS.CDCHKOBB,'IDVALATT',this.w_VALATTR,'IDTABKEY',PRODCLAS.CDTABKEY,'IDVALDAT',this.w_IDVALDAT,'IDDESATT',PRODCLAS.CDDESATT,'IDVALNUM',this.w_IDVALNUM)
          insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDCODATT,IDTIPATT,IDCHKOBB,IDVALATT,IDTABKEY,IDVALDAT,IDDESATT,IDVALNUM &i_ccchkf. );
             values (;
               this.w_IDSERIAL;
               ,this.w_CPROWNUM;
               ,this.w_CPROWORD;
               ,PRODCLAS.CDCODATT;
               ,PRODCLAS.CDTIPATT;
               ,PRODCLAS.CDCHKOBB;
               ,this.w_VALATTR;
               ,PRODCLAS.CDTABKEY;
               ,this.w_IDVALDAT;
               ,PRODCLAS.CDDESATT;
               ,this.w_IDVALNUM;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          bTrsErr=bErr_03979538
          this.w_MSGERR = Message()
          if this.oParentObject.w_IGN_ERR="S"
            this.Aggiorna_log(this.w_IDSERIAL,this.w_CPROWORD,this.w_IDCODATT,"insert error: "+this.w_MSGERR)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- accept error
            bTrsErr=.f.
          else
            * --- Raise
            i_Error="insert error: "
            return
          endif
        endif
        this.Aggiorna_log(this.w_IDSERIAL,this.w_CPROWORD,this.w_IDCODATT,"Inserito: "+this.w_VALATTR)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        if this.oParentObject.w_IGN_ERR="S"
          this.Aggiorna_log(this.w_IDSERIAL,this.w_CPROWORD,this.w_IDCODATT,"insert error: "+this.w_MSGERR)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Raise
          i_Error="insert error: "
          return
        endif
      endif
      ENDSCAN
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    Select PRODCLAS
    * --- Attributo obbligatorio ?!?
    this.w_CHKOBBL = ( NVL(PRODCLAS.CDCHKOBB, "N") = "S" )
    this.w_TIPOATTR = nvl(PRODCLAS.CDTIPATT, " ")
    this.w_CDVLPRED = nvl(PRODCLAS.CDVLPRED, " ")
    if empty( this.w_CDVLPRED )
      this.w_CHKVALPRED = .F.
      this.w_VALPRED = IIF(this.w_TIPOATTR="N",0,IIF(this.w_TIPOATTR="D",{}," "))
    else
      this.w_CHKVALPRED = .T.
      this.w_VALPRED = eval(this.w_CDVLPRED)
    endif
    * --- Prova a determinare il valore dell'attributo
    w_MSGERRORE = ""
    this.w_MSGERR = ""
    this.w_CDCAMCUR = nvl(PRODCLAS.CDCAMCUR," ")
    if empty(this.w_CDCAMCUR)
      * --- L'attributo prende il valore predefinito
      this.w_VALATTR = this.w_VALPRED
    else
      * --- Legge il dato dalle propriet� o dal cursore padre
      this.w_VALATTR = ""
      w_ErrorHandler = on("ERROR")
      on error w_MSGERRORE = MESSAGE()
      * --- Salvo area di lavoro altrimenti la use del painter chiude il cursore
      this.w_OLDAREA = SELECT()
      select _ATTTRIB_
      this.w_VALATTR = eval(this.w_CDCAMCUR)
      * --- Ripristino la vecchia area di lavoro
      SELECT(this.w_OLDAREA)
      on error &w_ErrorHandler
    endif
    * --- L'attributo � stato valorizzato adeguatamente ?!?!
    this.w_VALATTR = IIF(empty(this.w_VALATTR), this.w_VALPRED, this.w_VALATTR)
    * --- Per sicurezza, ritipizzo e pulisco valori null
    this.w_VALATTR = IIF(isnull(this.w_VALATTR), IIF(this.w_TIPOATTR="N",0,IIF(this.w_TIPOATTR="D",cp_CharToDate("  -  -  ")," ")), this.w_VALATTR)
    this.w_IDVALNUM = 0
    this.w_IDVALDAT = {}
    if EMPTY(w_MSGERRORE)
      * --- Controlla Tipo Attributo:
      this.w_TMPT = VarType(this.w_VALATTR)
      this.w_TMPT = IIF(this.w_TMPT="M","C",IIF(this.w_TMPT="T","D",IIF(this.w_TMPT="Y","N",this.w_TMPT)))
      if this.w_TIPOATTR <> this.w_TMPT
        this.w_MSGERR = ah_MSGFORMAT("Tipo impostato sull'attributo non corrispondente")
      else
        if this.w_TMPT="N"
          this.w_IDVALNUM = this.w_VALATTR
          this.w_VALATTR = str(this.w_VALATTR,20,5)
        endif
        if this.w_TMPT="D"
          this.w_IDVALDAT = this.w_VALATTR
          this.w_VALATTR = DTOC(this.w_VALATTR)
        endif
      endif
    else
      this.w_MSGERR = ah_MSGFORMAT("Espressione non valutabile%0Errore: %1", w_MSGERRORE)
    endif
  endproc


  procedure Aggiorna_log
    param pIDSERIAL,pCPROWORD,pIDCODATT,pMSGG
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna log
    Select LOGERR
    append blank
    replace IDSERIAL with m.pIDSERIAL, CPROWORD with m.pCPROWORD, IDCODATT with m.pIDCODATT, MSGG with m.pMSGG
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PRODINDI'
    this.cWorkTables[2]='PRODCLAS'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_PRODINDI')
      use in _Curs_PRODINDI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
