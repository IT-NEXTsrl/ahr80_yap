* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kcd                                                        *
*              Contabilizzazione distinte effetti parziale                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [116] [VRS_78]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-08                                                      *
* Last revis.: 2013-02-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kcd",oParentObject))

* --- Class definition
define class tgscg_kcd as StdForm
  Top    = 9
  Left   = 38

  * --- Standard Properties
  Width  = 556
  Height = 359+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-02-21"
  HelpContextID=99231081
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=31

  * --- Constant Properties
  _IDX = 0
  CONTROPA_IDX = 0
  CONTI_IDX = 0
  DIS_TINT_IDX = 0
  cPrg = "gscg_kcd"
  cComment = "Contabilizzazione distinte effetti parziale"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_CODAZI = space(5)
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_FLDATC = space(1)
  w_DATCON = ctod('  /  /  ')
  w_FLCOEFF = space(1)
  w_NUMERO = 0
  w_CONBAN = space(15)
  w_DESBAN = space(40)
  w_CONSCO = space(15)
  w_DESSCO = space(40)
  w_CONDCA = space(15)
  w_DESDCA = space(40)
  w_CONDCP = space(15)
  w_DESDCP = space(40)
  w_CONEFA = space(15)
  w_DESEFA = space(40)
  w_CONCES = space(15)
  w_DESCES = space(40)
  w_PARTSN = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DIFCON = space(15)
  w_DATVAL1 = ctod('  /  /  ')
  w_DATVAL2 = ctod('  /  /  ')
  w_CONTSINEFF = space(1)
  w_SELEZI = space(1)
  w_TIPDIS = space(2)
  w_FLSELE = 0
  w_NUMDIS = space(10)
  w_NUMEFF = 0
  w_ZoomCalc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kcdPag1","gscg_kcd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Filtri/contropartite")
      .Pages(2).addobject("oPag","tgscg_kcdPag2","gscg_kcd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Elenco partite")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATFIN_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomCalc = this.oPgFrm.Pages(2).oPag.ZoomCalc
    DoDefault()
    proc Destroy()
      this.w_ZoomCalc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='DIS_TINT'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPCON=space(1)
      .w_CODAZI=space(5)
      .w_DATFIN=ctod("  /  /  ")
      .w_FLDATC=space(1)
      .w_DATCON=ctod("  /  /  ")
      .w_FLCOEFF=space(1)
      .w_NUMERO=0
      .w_CONBAN=space(15)
      .w_DESBAN=space(40)
      .w_CONSCO=space(15)
      .w_DESSCO=space(40)
      .w_CONDCA=space(15)
      .w_DESDCA=space(40)
      .w_CONDCP=space(15)
      .w_DESDCP=space(40)
      .w_CONEFA=space(15)
      .w_DESEFA=space(40)
      .w_CONCES=space(15)
      .w_DESCES=space(40)
      .w_PARTSN=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DIFCON=space(15)
      .w_DATVAL1=ctod("  /  /  ")
      .w_DATVAL2=ctod("  /  /  ")
      .w_CONTSINEFF=space(1)
      .w_SELEZI=space(1)
      .w_TIPDIS=space(2)
      .w_FLSELE=0
      .w_NUMDIS=space(10)
      .w_NUMEFF=0
        .w_TIPCON = 'G'
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODAZI))
          .link_1_2('Full')
        endif
        .w_DATFIN = i_datsys
        .w_FLDATC = 'D'
        .w_DATCON = .w_DATFIN
        .w_FLCOEFF = 'S'
          .DoRTCalc(7,7,.f.)
        .w_CONBAN = .w_CONBAN
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CONBAN))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_CONSCO = .w_CONSCO
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CONSCO))
          .link_1_11('Full')
        endif
          .DoRTCalc(11,11,.f.)
        .w_CONDCA = .w_CONDCA
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CONDCA))
          .link_1_13('Full')
        endif
          .DoRTCalc(13,13,.f.)
        .w_CONDCP = .w_CONDCP
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CONDCP))
          .link_1_15('Full')
        endif
          .DoRTCalc(15,15,.f.)
        .w_CONEFA = .w_CONEFA
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CONEFA))
          .link_1_17('Full')
        endif
          .DoRTCalc(17,17,.f.)
        .w_CONCES = .w_CONCES
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_CONCES))
          .link_1_19('Full')
        endif
          .DoRTCalc(19,20,.f.)
        .w_OBTEST = .w_DATFIN
          .DoRTCalc(22,25,.f.)
        .w_CONTSINEFF = 'N'
      .oPgFrm.Page2.oPag.ZoomCalc.Calculate()
        .w_SELEZI = 'D'
          .DoRTCalc(28,28,.f.)
        .w_FLSELE = 0
      .oPgFrm.Page2.oPag.oObj_2_8.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_9.Calculate()
      .oPgFrm.Page2.oPag.oObj_2_14.Calculate()
          .DoRTCalc(30,30,.f.)
        .w_NUMEFF = .w_ZoomCalc.getVar('PTNUMEFF')
      .oPgFrm.Page2.oPag.oObj_2_16.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_4.enabled = this.oPgFrm.Page2.oPag.oBtn_2_4.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_10.enabled = this.oPgFrm.Page2.oPag.oBtn_2_10.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_11.enabled = this.oPgFrm.Page2.oPag.oBtn_2_11.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_kcd
    If Type('This.oParentobject')='O'and upper(This.oParentobject.class)="TGSTE_ADI"
      This.w_NUMDIS=this.oparentobject.w_DINUMDIS
      This.w_NUMERO=this.oparentobject.w_DINUMERO
      This.Notifyevent('Carica')
      This.oPgFrm.ActivePage = 2
     Endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_TIPCON<>.w_TIPCON
          .link_1_2('Full')
        endif
        .DoRTCalc(3,4,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_DATCON = .w_DATFIN
        endif
        .DoRTCalc(6,20,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_OBTEST = .w_DATFIN
        endif
        .oPgFrm.Page2.oPag.ZoomCalc.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_8.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_14.Calculate()
        .DoRTCalc(22,30,.t.)
            .w_NUMEFF = .w_ZoomCalc.getVar('PTNUMEFF')
        .oPgFrm.Page2.oPag.oObj_2_16.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZoomCalc.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_8.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_14.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_16.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDATCON_1_5.enabled = this.oPgFrm.Page1.oPag.oDATCON_1_5.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_10.enabled = this.oPgFrm.Page2.oPag.oBtn_2_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDATCON_1_5.visible=!this.oPgFrm.Page1.oPag.oDATCON_1_5.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZoomCalc.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_8.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_9.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_14.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_2_16.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COCONBAN,COCONSTR,COSADIFC,COSADIFN,COCONEFA,COSCACES,CODIFCON";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,COCONBAN,COCONSTR,COSADIFC,COSADIFN,COCONEFA,COSCACES,CODIFCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_CONBAN = NVL(_Link_.COCONBAN,space(15))
      this.w_CONSCO = NVL(_Link_.COCONSTR,space(15))
      this.w_CONDCA = NVL(_Link_.COSADIFC,space(15))
      this.w_CONDCP = NVL(_Link_.COSADIFN,space(15))
      this.w_CONEFA = NVL(_Link_.COCONEFA,space(15))
      this.w_CONCES = NVL(_Link_.COSCACES,space(15))
      this.w_DIFCON = NVL(_Link_.CODIFCON,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_CONBAN = space(15)
      this.w_CONSCO = space(15)
      this.w_CONDCA = space(15)
      this.w_CONDCP = space(15)
      this.w_CONEFA = space(15)
      this.w_CONCES = space(15)
      this.w_DIFCON = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONBAN
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CONBAN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CONBAN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONBAN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CONBAN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CONBAN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONBAN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCONBAN_1_8'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CONBAN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CONBAN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONBAN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESBAN = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CONBAN = space(15)
      endif
      this.w_DESBAN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_CONBAN = space(15)
        this.w_DESBAN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONSCO
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONSCO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CONSCO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CONSCO))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONSCO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CONSCO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CONSCO)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONSCO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCONSCO_1_11'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONSCO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CONSCO);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CONSCO)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONSCO = NVL(_Link_.ANCODICE,space(15))
      this.w_DESSCO = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CONSCO = space(15)
      endif
      this.w_DESSCO = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_CONSCO = space(15)
        this.w_DESSCO = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONSCO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONDCA
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONDCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CONDCA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CONDCA))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONDCA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CONDCA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CONDCA)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONDCA) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCONDCA_1_13'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONDCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CONDCA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CONDCA)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONDCA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESDCA = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CONDCA = space(15)
      endif
      this.w_DESDCA = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_CONDCA = space(15)
        this.w_DESDCA = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONDCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONDCP
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONDCP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CONDCP)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CONDCP))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONDCP)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CONDCP)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CONDCP)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONDCP) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCONDCP_1_15'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONDCP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CONDCP);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CONDCP)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONDCP = NVL(_Link_.ANCODICE,space(15))
      this.w_DESDCP = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CONDCP = space(15)
      endif
      this.w_DESDCP = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso o obsoleto")
        endif
        this.w_CONDCP = space(15)
        this.w_DESDCP = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONDCP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONEFA
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONEFA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CONEFA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CONEFA))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONEFA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CONEFA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CONEFA)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONEFA) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCONEFA_1_17'),i_cWhere,'GSAR_BZC',"Conti",'CONCOEFA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONEFA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CONEFA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CONEFA)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONEFA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESEFA = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CONEFA = space(15)
      endif
      this.w_DESEFA = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PARTSN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_PARTSN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_CONEFA = space(15)
        this.w_DESEFA = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PARTSN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONEFA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONCES
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONCES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CONCES)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CONCES))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONCES)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CONCES)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CONCES)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONCES) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCONCES_1_19'),i_cWhere,'GSAR_BZC',"Conti",'CONCOEFA.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONCES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CONCES);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CONCES)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONCES = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCES = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CONCES = space(15)
      endif
      this.w_DESCES = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PARTSN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_PARTSN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
        endif
        this.w_CONCES = space(15)
        this.w_DESCES = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PARTSN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONCES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_3.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_3.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDATC_1_4.RadioValue()==this.w_FLDATC)
      this.oPgFrm.Page1.oPag.oFLDATC_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCON_1_5.value==this.w_DATCON)
      this.oPgFrm.Page1.oPag.oDATCON_1_5.value=this.w_DATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCOEFF_1_6.RadioValue()==this.w_FLCOEFF)
      this.oPgFrm.Page1.oPag.oFLCOEFF_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_7.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_7.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oCONBAN_1_8.value==this.w_CONBAN)
      this.oPgFrm.Page1.oPag.oCONBAN_1_8.value=this.w_CONBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBAN_1_10.value==this.w_DESBAN)
      this.oPgFrm.Page1.oPag.oDESBAN_1_10.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oCONSCO_1_11.value==this.w_CONSCO)
      this.oPgFrm.Page1.oPag.oCONSCO_1_11.value=this.w_CONSCO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSCO_1_12.value==this.w_DESSCO)
      this.oPgFrm.Page1.oPag.oDESSCO_1_12.value=this.w_DESSCO
    endif
    if not(this.oPgFrm.Page1.oPag.oCONDCA_1_13.value==this.w_CONDCA)
      this.oPgFrm.Page1.oPag.oCONDCA_1_13.value=this.w_CONDCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDCA_1_14.value==this.w_DESDCA)
      this.oPgFrm.Page1.oPag.oDESDCA_1_14.value=this.w_DESDCA
    endif
    if not(this.oPgFrm.Page1.oPag.oCONDCP_1_15.value==this.w_CONDCP)
      this.oPgFrm.Page1.oPag.oCONDCP_1_15.value=this.w_CONDCP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDCP_1_16.value==this.w_DESDCP)
      this.oPgFrm.Page1.oPag.oDESDCP_1_16.value=this.w_DESDCP
    endif
    if not(this.oPgFrm.Page1.oPag.oCONEFA_1_17.value==this.w_CONEFA)
      this.oPgFrm.Page1.oPag.oCONEFA_1_17.value=this.w_CONEFA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESEFA_1_18.value==this.w_DESEFA)
      this.oPgFrm.Page1.oPag.oDESEFA_1_18.value=this.w_DESEFA
    endif
    if not(this.oPgFrm.Page1.oPag.oCONCES_1_19.value==this.w_CONCES)
      this.oPgFrm.Page1.oPag.oCONCES_1_19.value=this.w_CONCES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCES_1_20.value==this.w_DESCES)
      this.oPgFrm.Page1.oPag.oDESCES_1_20.value=this.w_DESCES
    endif
    if not(this.oPgFrm.Page2.oPag.oDATVAL1_2_1.value==this.w_DATVAL1)
      this.oPgFrm.Page2.oPag.oDATVAL1_2_1.value=this.w_DATVAL1
    endif
    if not(this.oPgFrm.Page2.oPag.oDATVAL2_2_2.value==this.w_DATVAL2)
      this.oPgFrm.Page2.oPag.oDATVAL2_2_2.value=this.w_DATVAL2
    endif
    if not(this.oPgFrm.Page2.oPag.oCONTSINEFF_2_3.RadioValue()==this.w_CONTSINEFF)
      this.oPgFrm.Page2.oPag.oCONTSINEFF_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oSELEZI_2_6.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page2.oPag.oSELEZI_2_6.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATCON)) or not(.w_DATCON>=.w_DATFIN))  and not(.w_FLDATC<>'U')  and (.w_FLDATC='U')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATCON_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DATCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data registrazione distinte maggiore della data di contabilizzazione")
          case   not(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)  and not(empty(.w_CONBAN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONBAN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)  and not(empty(.w_CONSCO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONSCO_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)  and not(empty(.w_CONDCA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONDCA_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)  and not(empty(.w_CONDCP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONDCP_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_PARTSN))  and not(empty(.w_CONEFA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONEFA_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST AND EMPTY(.w_PARTSN))  and not(empty(.w_CONCES))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONCES_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto o gestito a partite")
          case   not((.w_DATVAL1<=.w_DATVAL2) or (empty(.w_DATVAL2)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATVAL1_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data valuta iniziale � maggiore della data valuta finale")
          case   not((.w_DATVAL1<=.w_DATVAL2) or (empty(.w_DATVAL1)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATVAL2_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data valuta iniziale � maggiore della data valuta finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPCON = this.w_TIPCON
    this.o_DATFIN = this.w_DATFIN
    return

enddefine

* --- Define pages as container
define class tgscg_kcdPag1 as StdContainer
  Width  = 552
  height = 359
  stdWidth  = 552
  stdheight = 359
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATFIN_1_3 as StdField with uid="OYBQGNOKCE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine selezione delle distinte da contabilizzare",;
    HelpContextID = 51290058,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=193, Top=8


  add object oFLDATC_1_4 as StdCombo with uid="PNCPTCYFMF",rtseq=4,rtrep=.f.,left=193,top=35,width=128,height=21;
    , ToolTipText = "Data di registrazione: unica per tutti i documenti o uguale alla data documento";
    , HelpContextID = 224695466;
    , cFormVar="w_FLDATC",RowSource=""+"Data presentazione,"+"Unica", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLDATC_1_4.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'U',;
    space(1))))
  endfunc
  func oFLDATC_1_4.GetRadio()
    this.Parent.oContained.w_FLDATC = this.RadioValue()
    return .t.
  endfunc

  func oFLDATC_1_4.SetRadio()
    this.Parent.oContained.w_FLDATC=trim(this.Parent.oContained.w_FLDATC)
    this.value = ;
      iif(this.Parent.oContained.w_FLDATC=='D',1,;
      iif(this.Parent.oContained.w_FLDATC=='U',2,;
      0))
  endfunc

  add object oDATCON_1_5 as StdField with uid="QXLIXZYIXE",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATCON", cQueryName = "DATCON",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data registrazione distinte maggiore della data di contabilizzazione",;
    ToolTipText = "Data di contabilizzazione dei documenti selezionati",;
    HelpContextID = 45195210,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=324, Top=35

  func oDATCON_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLDATC='U')
    endwith
   endif
  endfunc

  func oDATCON_1_5.mHide()
    with this.Parent.oContained
      return (.w_FLDATC<>'U')
    endwith
  endfunc

  func oDATCON_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATCON>=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oFLCOEFF_1_6 as StdCheck with uid="UWAYRUSESH",rtseq=6,rtrep=.f.,left=324, top=54, caption="Conto indiretta effetti",;
    ToolTipText = "Se attivo, consente l'utilizzo del conto effetti specificato nella relativa contabilizzazione indiretta",;
    HelpContextID = 79256406,;
    cFormVar="w_FLCOEFF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCOEFF_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLCOEFF_1_6.GetRadio()
    this.Parent.oContained.w_FLCOEFF = this.RadioValue()
    return .t.
  endfunc

  func oFLCOEFF_1_6.SetRadio()
    this.Parent.oContained.w_FLCOEFF=trim(this.Parent.oContained.w_FLCOEFF)
    this.value = ;
      iif(this.Parent.oContained.w_FLCOEFF=='S',1,;
      0)
  endfunc

  add object oNUMERO_1_7 as StdField with uid="XAAECVNHKW",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero distinta selezionata",;
    HelpContextID = 25164586,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=193, Top=79, bHasZoom = .t. 

  proc oNUMERO_1_7.mZoom
  endproc

  add object oCONBAN_1_8 as StdField with uid="DNOVFYWZCN",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CONBAN", cQueryName = "CONBAN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Conto per la contabilizzazione delle spese bancarie (commissioni)",;
    HelpContextID = 59961818,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=176, Top=133, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CONBAN"

  func oCONBAN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONBAN_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONBAN_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCONBAN_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCONBAN_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CONBAN
     i_obj.ecpSave()
  endproc


  add object oBtn_1_9 as StdButton with uid="CQEHICTOFE",left=273, top=80, width=19,height=19,;
    caption="...", nPag=1;
    , HelpContextID = 99030058;
  , bGlobalFont=.t.

    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSCG_BC7(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESBAN_1_10 as StdField with uid="KDHEXQXAAW",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 59943882,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=315, Top=133, InputMask=replicate('X',40)

  add object oCONSCO_1_11 as StdField with uid="AKSDAYXUMQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CONSCO", cQueryName = "CONSCO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Conto per la contabilizzazione sconti tratte",;
    HelpContextID = 39973338,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=176, Top=161, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CONSCO"

  func oCONSCO_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONSCO_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONSCO_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCONSCO_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCONSCO_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CONSCO
     i_obj.ecpSave()
  endproc

  add object oDESSCO_1_12 as StdField with uid="PBXMTFEKLP",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESSCO", cQueryName = "DESSCO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 39955402,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=315, Top=161, InputMask=replicate('X',40)

  add object oCONDCA_1_13 as StdField with uid="ZKCVEPRKGI",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CONDCA", cQueryName = "CONDCA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Conto per la contabilizzazione differenze cambi attive",;
    HelpContextID = 7401946,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=176, Top=189, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CONDCA"

  func oCONDCA_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONDCA_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONDCA_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCONDCA_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCONDCA_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CONDCA
     i_obj.ecpSave()
  endproc

  add object oDESDCA_1_14 as StdField with uid="ZTMLJIKQHA",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESDCA", cQueryName = "DESDCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 7384010,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=315, Top=189, InputMask=replicate('X',40)

  add object oCONDCP_1_15 as StdField with uid="TQRMLJHTPU",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CONDCP", cQueryName = "CONDCP",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o obsoleto",;
    ToolTipText = "Conto per la contabilizzazione differenze cambi passive",;
    HelpContextID = 24179162,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=176, Top=217, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CONDCP"

  func oCONDCP_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONDCP_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONDCP_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCONDCP_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCONDCP_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CONDCP
     i_obj.ecpSave()
  endproc

  add object oDESDCP_1_16 as StdField with uid="XLXGAWSWJX",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESDCP", cQueryName = "DESDCP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 24161226,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=315, Top=217, InputMask=replicate('X',40)

  add object oCONEFA_1_17 as StdField with uid="VTTKDHXFOK",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CONEFA", cQueryName = "CONEFA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Conto effetti attivi per movimenti da contabilizzazione indiretta",;
    HelpContextID = 4190682,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=176, Top=245, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CONEFA"

  func oCONEFA_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONEFA_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONEFA_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCONEFA_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'CONCOEFA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCONEFA_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CONEFA
     i_obj.ecpSave()
  endproc

  add object oDESEFA_1_18 as StdField with uid="CPENGSYRKK",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESEFA", cQueryName = "DESEFA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 4172746,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=315, Top=245, InputMask=replicate('X',40)

  add object oCONCES_1_19 as StdField with uid="XCAQZBGKOH",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CONCES", cQueryName = "CONCES",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto o gestito a partite",;
    ToolTipText = "Conto scarto da cessione per distinte di tipo cessione crediti",;
    HelpContextID = 28184102,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=176, Top=273, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CONCES"

  func oCONCES_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONCES_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONCES_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCONCES_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'CONCOEFA.CONTI_VZM',this.parent.oContained
  endproc
  proc oCONCES_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CONCES
     i_obj.ecpSave()
  endproc

  add object oDESCES_1_20 as StdField with uid="KREZJBIIAB",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESCES", cQueryName = "DESCES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 28202038,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=315, Top=273, InputMask=replicate('X',40)

  add object oStr_1_22 as StdString with uid="SORSGBGXJI",Visible=.t., Left=6, Top=103,;
    Alignment=0, Width=188, Height=15,;
    Caption="Contropartite"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_23 as StdString with uid="AFAYOBZWVD",Visible=.t., Left=60, Top=133,;
    Alignment=1, Width=115, Height=15,;
    Caption="Spese bancarie:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="AURUUULXZN",Visible=.t., Left=60, Top=161,;
    Alignment=1, Width=115, Height=15,;
    Caption="Sconto tratte:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="RLFMDEUKBO",Visible=.t., Left=18, Top=189,;
    Alignment=1, Width=157, Height=15,;
    Caption="Dif.cambi attiva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="EZEQCKYYET",Visible=.t., Left=5, Top=217,;
    Alignment=1, Width=170, Height=15,;
    Caption="Dif.cambi passiva:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="BJMBFHLZBK",Visible=.t., Left=60, Top=245,;
    Alignment=1, Width=115, Height=15,;
    Caption="Effetti attivi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="NFMZXMKZEP",Visible=.t., Left=5, Top=273,;
    Alignment=1, Width=170, Height=15,;
    Caption="Scarti da cessione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="OKTJIBVAYX",Visible=.t., Left=7, Top=8,;
    Alignment=1, Width=181, Height=18,;
    Caption="Distinte presentate fino al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="YCYBYHFIKD",Visible=.t., Left=37, Top=35,;
    Alignment=1, Width=151, Height=15,;
    Caption="Data contabilizzazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="TAJDSDEIWG",Visible=.t., Left=89, Top=80,;
    Alignment=1, Width=99, Height=18,;
    Caption="Numero distinta:"  ;
  , bGlobalFont=.t.

  add object oBox_1_32 as StdBox with uid="MJUQGQVRZA",left=4, top=120, width=544,height=2
enddefine
define class tgscg_kcdPag2 as StdContainer
  Width  = 552
  height = 359
  stdWidth  = 552
  stdheight = 359
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATVAL1_2_1 as StdField with uid="TFCFRMCMTO",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DATVAL1", cQueryName = "DATVAL1",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data valuta iniziale � maggiore della data valuta finale",;
    ToolTipText = "Data valuta di inizio selezione",;
    HelpContextID = 176250934,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=98, Top=8

  func oDATVAL1_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATVAL1<=.w_DATVAL2) or (empty(.w_DATVAL2)))
    endwith
    return bRes
  endfunc

  add object oDATVAL2_2_2 as StdField with uid="CMZCLYXUWM",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DATVAL2", cQueryName = "DATVAL2",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data valuta iniziale � maggiore della data valuta finale",;
    ToolTipText = "Data valuta di fine selezione",;
    HelpContextID = 176250934,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=264, Top=8

  func oDATVAL2_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATVAL1<=.w_DATVAL2) or (empty(.w_DATVAL1)))
    endwith
    return bRes
  endfunc

  add object oCONTSINEFF_2_3 as StdCheck with uid="KWIOSHOTBD",rtseq=26,rtrep=.f.,left=100, top=31, caption="Contabilizzazione per singolo effetto",;
    HelpContextID = 123774773,;
    cFormVar="w_CONTSINEFF", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oCONTSINEFF_2_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCONTSINEFF_2_3.GetRadio()
    this.Parent.oContained.w_CONTSINEFF = this.RadioValue()
    return .t.
  endfunc

  func oCONTSINEFF_2_3.SetRadio()
    this.Parent.oContained.w_CONTSINEFF=trim(this.Parent.oContained.w_CONTSINEFF)
    this.value = ;
      iif(this.Parent.oContained.w_CONTSINEFF=='S',1,;
      0)
  endfunc


  add object oBtn_2_4 as StdButton with uid="XISIDBAEUZ",left=500, top=8, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , HelpContextID = 77691158;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_4.Click()
      with this.Parent.oContained
        .notifyevent('Carica')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomCalc as cp_szoombox with uid="ADNAFEWFPG",left=2, top=56, width=548,height=253,;
    caption='Object',;
   bGlobalFont=.t.,;
    bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cTable="DIS_TINT",cZoomFile="GSCG_KCD",cZoomOnZoom="GSTE_ADI",cMenuFile="",;
    cEvent = "Carica,Blank",;
    nPag=2;
    , HelpContextID = 78766566

  add object oSELEZI_2_6 as StdRadio with uid="ZBKHPAZPPX",rtseq=27,rtrep=.f.,left=6, top=313, width=133,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=2;
  , bGlobalFont=.t.

    proc oSELEZI_2_6.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 117447386
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 117447386
      this.Buttons(2).Top=15
      this.SetAll("Width",131)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_2_6.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_2_6.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_2_6.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oObj_2_8 as cp_runprogram with uid="NEEHCLSOAU",left=77, top=390, width=165,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BC7('S')",;
    cEvent = "w_SELEZI Changed,Init, Carica",;
    nPag=2;
    , HelpContextID = 78766566


  add object oObj_2_9 as cp_runprogram with uid="ROKUJRGROP",left=249, top=391, width=165,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BC7('D')",;
    cEvent = "w_DATFIN Changed",;
    nPag=2;
    , HelpContextID = 78766566


  add object oBtn_2_10 as StdButton with uid="BLTPCMPVXN",left=450, top=313, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Inizio contabilizzazione";
    , HelpContextID = 99202330;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_10.Click()
      with this.Parent.oContained
        GSCG_BCT(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_NUMERO))
      endwith
    endif
  endfunc


  add object oBtn_2_11 as StdButton with uid="PRNOHTAQGM",left=500, top=313, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 91913658;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_2_14 as cp_runprogram with uid="SLLAEXKSZR",left=76, top=415, width=165,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BC7('G')",;
    cEvent = "ZOOMCALC row checked",;
    nPag=2;
    , HelpContextID = 78766566


  add object oObj_2_16 as cp_runprogram with uid="AJTZSLPMBR",left=250, top=416, width=165,height=24,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BC7('A')",;
    cEvent = "ZOOMCALC row unchecked",;
    nPag=2;
    , HelpContextID = 78766566

  add object oStr_2_12 as StdString with uid="VFYSYMBWID",Visible=.t., Left=-29, Top=8,;
    Alignment=1, Width=125, Height=18,;
    Caption="Da data valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="BLETVBOUKQ",Visible=.t., Left=144, Top=8,;
    Alignment=1, Width=118, Height=18,;
    Caption="A data valuta:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kcd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
