* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_acb                                                        *
*              Conti banche                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_131]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-04                                                      *
* Last revis.: 2016-07-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_acb"))

* --- Class definition
define class tgste_acb as StdForm
  Top    = 5
  Left   = 10

  * --- Standard Properties
  Width  = 625
  Height = 450+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-07-29"
  HelpContextID=109720681
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=59

  * --- Constant Properties
  COC_MAST_IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  FES_MAST_IDX = 0
  COD_ABI_IDX = 0
  COD_CAB_IDX = 0
  cFile = "COC_MAST"
  cKeySelect = "BACODBAN"
  cKeyWhere  = "BACODBAN=this.w_BACODBAN"
  cKeyWhereODBC = '"BACODBAN="+cp_ToStrODBC(this.w_BACODBAN)';

  cKeyWhereODBCqualified = '"COC_MAST.BACODBAN="+cp_ToStrODBC(this.w_BACODBAN)';

  cPrg = "gste_acb"
  cComment = "Conti banche"
  icon = "anag.ico"
  cAutoZoom = 'GSTE0ACB'
  * --- Area Manuale = Properties
  * --- gste_acb
  cComment = IIF(g_BANC="S", "Conti correnti", "Conti banche")
  
  * --- Fine Area Manuale

  * --- Local Variables
  w_BACODBAN = space(15)
  w_BADESCRI = space(35)
  w_BAFLPREF = space(1)
  w_BATIPCON = space(1)
  o_BATIPCON = space(1)
  w_BAPAESEU = space(2)
  o_BAPAESEU = space(2)
  w_BACINEUR = space(2)
  o_BACINEUR = space(2)
  w_BACINCAB = space(1)
  w_FLBANEST = space(1)
  o_FLBANEST = space(1)
  w_BACINABI = space(1)
  o_BACINABI = space(1)
  w_BACODABI = space(5)
  o_BACODABI = space(5)
  w_BACODCAB = space(5)
  o_BACODCAB = space(5)
  w_BACONCOR = space(12)
  o_BACONCOR = space(12)
  w_BACODBIC = space(11)
  w_BACODSIA = space(5)
  w_BACONSBF = space(1)
  o_BACONSBF = space(1)
  w_BACONASS = space(15)
  w_BACODCUC = space(8)
  w_BA__BBAN = space(30)
  o_BA__BBAN = space(30)
  w_CODPA = space(2)
  o_CODPA = space(2)
  w_CIFRA = space(2)
  o_CIFRA = space(2)
  w_IBAN = space(30)
  o_IBAN = space(30)
  w_CODIBAN = space(34)
  o_CODIBAN = space(34)
  w_BA__IBAN = space(35)
  o_BA__IBAN = space(35)
  w_BAIDCRED = space(23)
  w_BACODVAL = space(3)
  w_DESVAL = space(35)
  w_BACALFES = space(3)
  w_DESCAL = space(35)
  w_BATIPCOL = space(1)
  w_BACONCOL = space(15)
  w_DESCON = space(40)
  w_BANUMTEL = space(18)
  w_BANUMFAX = space(18)
  w_BA__NOTE = space(0)
  w_BADTINVA = ctod('  /  /  ')
  w_BADTOBSO = ctod('  /  /  ')
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPSOT = space(1)
  w_BABASINC = space(1)
  w_BABASIND = space(1)
  w_BABASLIQ = space(1)
  w_BAGIOANN = 0
  w_BAPERRIT = 0
  w_BASALCRE = 0
  w_BASALDEB = 0
  w_SALDO = 0
  w_VSALDO = 0
  w_VSALDO1 = 0
  w_DECTOT = 0
  w_CALCPICT = 0
  w_PARTSN = space(1)
  w_CHECKCIN = space(1)
  w_BANSBTES = space(8)
  o_BANSBTES = space(8)
  w_BARBNTES = space(4)

  * --- Children pointers
  GSTE_MCB = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'COC_MAST','gste_acb')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_acbPag1","gste_acb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principale")
      .Pages(1).HelpContextID = 115654889
      .Pages(2).addobject("oPag","tgste_acbPag2","gste_acb",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati conto corrente")
      .Pages(2).HelpContextID = 138588969
      .Pages(3).addobject("oPag","tgste_acbPag3","gste_acb",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Dati DocFinance")
      .Pages(3).HelpContextID = 170835705
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oBACODBAN_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gste_acb
    This.Pages(2).Enabled = (g_BANC='S')
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='FES_MAST'
    this.cWorkTables[4]='COD_ABI'
    this.cWorkTables[5]='COD_CAB'
    this.cWorkTables[6]='COC_MAST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.COC_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.COC_MAST_IDX,3]
  return

  function CreateChildren()
    this.GSTE_MCB = CREATEOBJECT('stdDynamicChild',this,'GSTE_MCB',this.oPgFrm.Page2.oPag.oLinkPC_2_11)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSTE_MCB)
      this.GSTE_MCB.DestroyChildrenChain()
      this.GSTE_MCB=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_11')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSTE_MCB.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSTE_MCB.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSTE_MCB.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSTE_MCB.SetKey(;
            .w_BACODBAN,"BACODBAN";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSTE_MCB.ChangeRow(this.cRowID+'      1',1;
             ,.w_BACODBAN,"BACODBAN";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSTE_MCB)
        i_f=.GSTE_MCB.BuildFilter()
        if !(i_f==.GSTE_MCB.cQueryFilter)
          i_fnidx=.GSTE_MCB.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSTE_MCB.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSTE_MCB.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSTE_MCB.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSTE_MCB.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_BACODBAN = NVL(BACODBAN,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_27_joined
    link_1_27_joined=.f.
    local link_1_29_joined
    link_1_29_joined=.f.
    local link_1_32_joined
    link_1_32_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from COC_MAST where BACODBAN=KeySet.BACODBAN
    *
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('COC_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "COC_MAST.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' COC_MAST '
      link_1_27_joined=this.AddJoinedLink_1_27(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_29_joined=this.AddJoinedLink_1_29(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_32_joined=this.AddJoinedLink_1_32(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'BACODBAN',this.w_BACODBAN  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESVAL = space(35)
        .w_DESCAL = space(35)
        .w_DESCON = space(40)
        .w_OBTEST = i_DATSYS
        .w_DATOBSO = ctod("  /  /  ")
        .w_TIPSOT = space(1)
        .w_DECTOT = 0
        .w_PARTSN = space(1)
        .w_CHECKCIN = space(1)
        .w_BACODBAN = NVL(BACODBAN,space(15))
        .w_BADESCRI = NVL(BADESCRI,space(35))
        .w_BAFLPREF = NVL(BAFLPREF,space(1))
        .w_BATIPCON = NVL(BATIPCON,space(1))
        .w_BAPAESEU = NVL(BAPAESEU,space(2))
        .w_BACINEUR = NVL(BACINEUR,space(2))
        .w_BACINCAB = NVL(BACINCAB,space(1))
        .w_FLBANEST = iif(.w_BAPAESEU == 'IT' OR .w_BAPAESEU == 'SM' or ISLESP() ,'N','S')
        .w_BACINABI = NVL(BACINABI,space(1))
        .w_BACODABI = NVL(BACODABI,space(5))
          * evitabile
          *.link_1_10('Load')
        .w_BACODCAB = NVL(BACODCAB,space(5))
          * evitabile
          *.link_1_11('Load')
        .w_BACONCOR = NVL(BACONCOR,space(12))
        .w_BACODBIC = NVL(BACODBIC,space(11))
        .w_BACODSIA = NVL(BACODSIA,space(5))
        .w_BACONSBF = NVL(BACONSBF,space(1))
        .w_BACONASS = NVL(BACONASS,space(15))
          * evitabile
          *.link_1_17('Load')
        .w_BACODCUC = NVL(BACODCUC,space(8))
        .w_BA__BBAN = NVL(BA__BBAN,space(30))
        .w_CODPA = iif(! empty(.w_BAPAESEU) AND ISLITA(),.w_BAPAESEU,.w_CODPA)
        .w_CIFRA = iif(! empty(.w_BACINEUR) AND ISLITA(),.w_BACINEUR,.w_CIFRA)
        .w_IBAN = IIF(ISLITA(),.w_BA__BBAN,SPACE(30))
        .w_CODIBAN = .w_BA__IBAN
        .w_BA__IBAN = NVL(BA__IBAN,space(35))
        .w_BAIDCRED = NVL(BAIDCRED,space(23))
        .w_BACODVAL = NVL(BACODVAL,space(3))
          if link_1_27_joined
            this.w_BACODVAL = NVL(VACODVAL127,NVL(this.w_BACODVAL,space(3)))
            this.w_DESVAL = NVL(VADESVAL127,space(35))
            this.w_DECTOT = NVL(VADECTOT127,0)
          else
          .link_1_27('Load')
          endif
        .w_BACALFES = NVL(BACALFES,space(3))
          if link_1_29_joined
            this.w_BACALFES = NVL(FECODICE129,NVL(this.w_BACALFES,space(3)))
            this.w_DESCAL = NVL(FEDESCRI129,space(35))
          else
          .link_1_29('Load')
          endif
        .w_BATIPCOL = NVL(BATIPCOL,space(1))
        .w_BACONCOL = NVL(BACONCOL,space(15))
          if link_1_32_joined
            this.w_BACONCOL = NVL(ANCODICE132,NVL(this.w_BACONCOL,space(15)))
            this.w_DESCON = NVL(ANDESCRI132,space(40))
            this.w_TIPSOT = NVL(ANTIPSOT132,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO132),ctod("  /  /  "))
            this.w_PARTSN = NVL(ANPARTSN132,space(1))
          else
          .link_1_32('Load')
          endif
        .w_BANUMTEL = NVL(BANUMTEL,space(18))
        .w_BANUMFAX = NVL(BANUMFAX,space(18))
        .w_BA__NOTE = NVL(BA__NOTE,space(0))
        .w_BADTINVA = NVL(cp_ToDate(BADTINVA),ctod("  /  /  "))
        .w_BADTOBSO = NVL(cp_ToDate(BADTOBSO),ctod("  /  /  "))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_BABASINC = NVL(BABASINC,space(1))
        .w_BABASIND = NVL(BABASIND,space(1))
        .w_BABASLIQ = NVL(BABASLIQ,space(1))
        .w_BAGIOANN = NVL(BAGIOANN,0)
        .w_BAPERRIT = NVL(BAPERRIT,0)
        .w_BASALCRE = NVL(BASALCRE,0)
        .w_BASALDEB = NVL(BASALDEB,0)
        .w_SALDO = .w_BASALCRE-.w_BASALDEB
        .w_VSALDO = ABS(.w_SALDO)
        .w_VSALDO1 = ABS(.w_SALDO)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate(AH_MsgFormat(iif(.w_BATIPCON # 'S',iif(.w_FLBANEST='S' or ISLESP(), "IBAN/Cod. int.:","Codice IBAN:"),'')))
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        .w_BANSBTES = NVL(BANSBTES,space(8))
        .w_BARBNTES = NVL(BARBNTES,space(4))
        cp_LoadRecExtFlds(this,'COC_MAST')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gste_acb
    this.w_CODPA=SUBSTR(this.w_BA__IBAN,1,2)
    this.w_CIFRA=SUBSTR(this.w_BA__IBAN,3,2)
    this.w_IBAN=(this.w_BA__BBAN)
    This.SetControlsValue()
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_BACODBAN = space(15)
      .w_BADESCRI = space(35)
      .w_BAFLPREF = space(1)
      .w_BATIPCON = space(1)
      .w_BAPAESEU = space(2)
      .w_BACINEUR = space(2)
      .w_BACINCAB = space(1)
      .w_FLBANEST = space(1)
      .w_BACINABI = space(1)
      .w_BACODABI = space(5)
      .w_BACODCAB = space(5)
      .w_BACONCOR = space(12)
      .w_BACODBIC = space(11)
      .w_BACODSIA = space(5)
      .w_BACONSBF = space(1)
      .w_BACONASS = space(15)
      .w_BACODCUC = space(8)
      .w_BA__BBAN = space(30)
      .w_CODPA = space(2)
      .w_CIFRA = space(2)
      .w_IBAN = space(30)
      .w_CODIBAN = space(34)
      .w_BA__IBAN = space(35)
      .w_BAIDCRED = space(23)
      .w_BACODVAL = space(3)
      .w_DESVAL = space(35)
      .w_BACALFES = space(3)
      .w_DESCAL = space(35)
      .w_BATIPCOL = space(1)
      .w_BACONCOL = space(15)
      .w_DESCON = space(40)
      .w_BANUMTEL = space(18)
      .w_BANUMFAX = space(18)
      .w_BA__NOTE = space(0)
      .w_BADTINVA = ctod("  /  /  ")
      .w_BADTOBSO = ctod("  /  /  ")
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_TIPSOT = space(1)
      .w_BABASINC = space(1)
      .w_BABASIND = space(1)
      .w_BABASLIQ = space(1)
      .w_BAGIOANN = 0
      .w_BAPERRIT = 0
      .w_BASALCRE = 0
      .w_BASALDEB = 0
      .w_SALDO = 0
      .w_VSALDO = 0
      .w_VSALDO1 = 0
      .w_DECTOT = 0
      .w_CALCPICT = 0
      .w_PARTSN = space(1)
      .w_CHECKCIN = space(1)
      .w_BANSBTES = space(8)
      .w_BARBNTES = space(4)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_BAFLPREF = 'N'
          .DoRTCalc(4,4,.f.)
        .w_BAPAESEU = SPACE(2)
        .w_BACINEUR = "  "
          .DoRTCalc(7,7,.f.)
        .w_FLBANEST = iif(.w_BAPAESEU == 'IT' OR .w_BAPAESEU == 'SM' or ISLESP() ,'N','S')
        .w_BACINABI = IIF(.w_BACONSBF='C', " ", .w_BACINABI)
        .w_BACODABI = IIF(.w_BACONSBF='C', SPACE(5), .w_BACODABI)
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_BACODABI))
          .link_1_10('Full')
          endif
        .w_BACODCAB = IIF(.w_BACONSBF='C', SPACE(5), .w_BACODCAB)
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_BACODCAB))
          .link_1_11('Full')
          endif
          .DoRTCalc(12,13,.f.)
        .w_BACODSIA = SPACE(5)
        .w_BACONSBF = 'C'
        .w_BACONASS = iif(.w_BACONSBF='C','',.w_BACONASS)
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_BACONASS))
          .link_1_17('Full')
          endif
          .DoRTCalc(17,17,.f.)
        .w_BA__BBAN = IIF(.w_FLBANEST # 'S',CALCBBAN(.w_BACINABI, .w_BACINEUR, .w_BACODABI, .w_BACODCAB, .w_BACONCOR),' ')
        .w_CODPA = iif(! empty(.w_BAPAESEU) AND ISLITA(),.w_BAPAESEU,.w_CODPA)
        .w_CIFRA = iif(! empty(.w_BACINEUR) AND ISLITA(),.w_BACINEUR,.w_CIFRA)
        .w_IBAN = IIF(ISLITA(),.w_BA__BBAN,SPACE(30))
        .w_CODIBAN = .w_BA__IBAN
          .DoRTCalc(23,24,.f.)
        .w_BACODVAL = IIF(g_BANC<>'S', SPACE(3), g_PERVAL)
        .DoRTCalc(25,25,.f.)
          if not(empty(.w_BACODVAL))
          .link_1_27('Full')
          endif
        .DoRTCalc(26,27,.f.)
          if not(empty(.w_BACALFES))
          .link_1_29('Full')
          endif
          .DoRTCalc(28,28,.f.)
        .w_BATIPCOL = 'G'
        .DoRTCalc(30,30,.f.)
          if not(empty(.w_BACONCOL))
          .link_1_32('Full')
          endif
          .DoRTCalc(31,40,.f.)
        .w_OBTEST = i_DATSYS
          .DoRTCalc(42,43,.f.)
        .w_BABASINC = 'A'
        .w_BABASIND = 'A'
        .w_BABASLIQ = 'A'
          .DoRTCalc(47,50,.f.)
        .w_SALDO = .w_BASALCRE-.w_BASALDEB
        .w_VSALDO = ABS(.w_SALDO)
        .w_VSALDO1 = ABS(.w_SALDO)
          .DoRTCalc(54,54,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate(AH_MsgFormat(iif(.w_BATIPCON # 'S',iif(.w_FLBANEST='S' or ISLESP(), "IBAN/Cod. int.:","Codice IBAN:"),'')))
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
          .DoRTCalc(56,58,.f.)
        .w_BARBNTES = Space(4)
      endif
    endwith
    cp_BlankRecExtFlds(this,'COC_MAST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oBACODBAN_1_1.enabled = i_bVal
      .Page1.oPag.oBADESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oBAFLPREF_1_3.enabled = i_bVal
      .Page1.oPag.oBATIPCON_1_4.enabled = i_bVal
      .Page1.oPag.oBAPAESEU_1_5.enabled = i_bVal
      .Page1.oPag.oBACINEUR_1_6.enabled = i_bVal
      .Page1.oPag.oBACINABI_1_9.enabled = i_bVal
      .Page1.oPag.oBACODABI_1_10.enabled = i_bVal
      .Page1.oPag.oBACODCAB_1_11.enabled = i_bVal
      .Page1.oPag.oBACONCOR_1_12.enabled = i_bVal
      .Page1.oPag.oBACODBIC_1_14.enabled = i_bVal
      .Page1.oPag.oBACODSIA_1_15.enabled = i_bVal
      .Page1.oPag.oBACONSBF_1_16.enabled = i_bVal
      .Page1.oPag.oBACONASS_1_17.enabled = i_bVal
      .Page1.oPag.oBACODCUC_1_18.enabled = i_bVal
      .Page1.oPag.oCODPA_1_21.enabled = i_bVal
      .Page1.oPag.oCIFRA_1_22.enabled = i_bVal
      .Page1.oPag.oIBAN_1_23.enabled = i_bVal
      .Page1.oPag.oCODIBAN_1_24.enabled = i_bVal
      .Page1.oPag.oBAIDCRED_1_26.enabled = i_bVal
      .Page1.oPag.oBACODVAL_1_27.enabled = i_bVal
      .Page1.oPag.oBACALFES_1_29.enabled = i_bVal
      .Page1.oPag.oBACONCOL_1_32.enabled = i_bVal
      .Page1.oPag.oBANUMTEL_1_34.enabled = i_bVal
      .Page1.oPag.oBANUMFAX_1_35.enabled = i_bVal
      .Page1.oPag.oBA__NOTE_1_36.enabled = i_bVal
      .Page1.oPag.oBADTINVA_1_37.enabled = i_bVal
      .Page1.oPag.oBADTOBSO_1_38.enabled = i_bVal
      .Page2.oPag.oBABASINC_2_1.enabled = i_bVal
      .Page2.oPag.oBABASIND_2_2.enabled = i_bVal
      .Page2.oPag.oBABASLIQ_2_3.enabled = i_bVal
      .Page2.oPag.oBAGIOANN_2_4.enabled = i_bVal
      .Page2.oPag.oBAPERRIT_2_5.enabled = i_bVal
      .Page2.oPag.oBASALCRE_2_6.enabled = i_bVal
      .Page2.oPag.oBASALDEB_2_7.enabled = i_bVal
      .Page3.oPag.oBANSBTES_3_1.enabled = i_bVal
      .Page3.oPag.oBARBNTES_3_2.enabled = i_bVal
      .Page1.oPag.oBtn_1_75.enabled = i_bVal
      .Page1.oPag.oObj_1_67.enabled = i_bVal
      .Page1.oPag.oObj_1_80.enabled = i_bVal
      .Page1.oPag.oObj_1_83.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oBACODBAN_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oBACODBAN_1_1.enabled = .t.
        .Page1.oPag.oBADESCRI_1_2.enabled = .t.
      endif
    endwith
    this.GSTE_MCB.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'COC_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSTE_MCB.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACODBAN,"BACODBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BADESCRI,"BADESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BAFLPREF,"BAFLPREF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BATIPCON,"BATIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BAPAESEU,"BAPAESEU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACINEUR,"BACINEUR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACINCAB,"BACINCAB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACINABI,"BACINABI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACODABI,"BACODABI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACODCAB,"BACODCAB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACONCOR,"BACONCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACODBIC,"BACODBIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACODSIA,"BACODSIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACONSBF,"BACONSBF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACONASS,"BACONASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACODCUC,"BACODCUC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BA__BBAN,"BA__BBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BA__IBAN,"BA__IBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BAIDCRED,"BAIDCRED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACODVAL,"BACODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACALFES,"BACALFES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BATIPCOL,"BATIPCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BACONCOL,"BACONCOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BANUMTEL,"BANUMTEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BANUMFAX,"BANUMFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BA__NOTE,"BA__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BADTINVA,"BADTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BADTOBSO,"BADTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BABASINC,"BABASINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BABASIND,"BABASIND",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BABASLIQ,"BABASLIQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BAGIOANN,"BAGIOANN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BAPERRIT,"BAPERRIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BASALCRE,"BASALCRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BASALDEB,"BASALDEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BANSBTES,"BANSBTES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_BARBNTES,"BARBNTES",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    i_lTable = "COC_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.COC_MAST_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCG_SCO with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.COC_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.COC_MAST_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into COC_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'COC_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'COC_MAST')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(BACODBAN,BADESCRI,BAFLPREF,BATIPCON,BAPAESEU"+;
                  ",BACINEUR,BACINCAB,BACINABI,BACODABI,BACODCAB"+;
                  ",BACONCOR,BACODBIC,BACODSIA,BACONSBF,BACONASS"+;
                  ",BACODCUC,BA__BBAN,BA__IBAN,BAIDCRED,BACODVAL"+;
                  ",BACALFES,BATIPCOL,BACONCOL,BANUMTEL,BANUMFAX"+;
                  ",BA__NOTE,BADTINVA,BADTOBSO,UTCC,UTCV"+;
                  ",UTDC,UTDV,BABASINC,BABASIND,BABASLIQ"+;
                  ",BAGIOANN,BAPERRIT,BASALCRE,BASALDEB,BANSBTES"+;
                  ",BARBNTES "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_BACODBAN)+;
                  ","+cp_ToStrODBC(this.w_BADESCRI)+;
                  ","+cp_ToStrODBC(this.w_BAFLPREF)+;
                  ","+cp_ToStrODBC(this.w_BATIPCON)+;
                  ","+cp_ToStrODBC(this.w_BAPAESEU)+;
                  ","+cp_ToStrODBC(this.w_BACINEUR)+;
                  ","+cp_ToStrODBC(this.w_BACINCAB)+;
                  ","+cp_ToStrODBC(this.w_BACINABI)+;
                  ","+cp_ToStrODBCNull(this.w_BACODABI)+;
                  ","+cp_ToStrODBCNull(this.w_BACODCAB)+;
                  ","+cp_ToStrODBC(this.w_BACONCOR)+;
                  ","+cp_ToStrODBC(this.w_BACODBIC)+;
                  ","+cp_ToStrODBC(this.w_BACODSIA)+;
                  ","+cp_ToStrODBC(this.w_BACONSBF)+;
                  ","+cp_ToStrODBCNull(this.w_BACONASS)+;
                  ","+cp_ToStrODBC(this.w_BACODCUC)+;
                  ","+cp_ToStrODBC(this.w_BA__BBAN)+;
                  ","+cp_ToStrODBC(this.w_BA__IBAN)+;
                  ","+cp_ToStrODBC(this.w_BAIDCRED)+;
                  ","+cp_ToStrODBCNull(this.w_BACODVAL)+;
                  ","+cp_ToStrODBCNull(this.w_BACALFES)+;
                  ","+cp_ToStrODBC(this.w_BATIPCOL)+;
                  ","+cp_ToStrODBCNull(this.w_BACONCOL)+;
                  ","+cp_ToStrODBC(this.w_BANUMTEL)+;
                  ","+cp_ToStrODBC(this.w_BANUMFAX)+;
                  ","+cp_ToStrODBC(this.w_BA__NOTE)+;
                  ","+cp_ToStrODBC(this.w_BADTINVA)+;
                  ","+cp_ToStrODBC(this.w_BADTOBSO)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_BABASINC)+;
                  ","+cp_ToStrODBC(this.w_BABASIND)+;
                  ","+cp_ToStrODBC(this.w_BABASLIQ)+;
                  ","+cp_ToStrODBC(this.w_BAGIOANN)+;
                  ","+cp_ToStrODBC(this.w_BAPERRIT)+;
                  ","+cp_ToStrODBC(this.w_BASALCRE)+;
                  ","+cp_ToStrODBC(this.w_BASALDEB)+;
                  ","+cp_ToStrODBC(this.w_BANSBTES)+;
                  ","+cp_ToStrODBC(this.w_BARBNTES)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'COC_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'COC_MAST')
        cp_CheckDeletedKey(i_cTable,0,'BACODBAN',this.w_BACODBAN)
        INSERT INTO (i_cTable);
              (BACODBAN,BADESCRI,BAFLPREF,BATIPCON,BAPAESEU,BACINEUR,BACINCAB,BACINABI,BACODABI,BACODCAB,BACONCOR,BACODBIC,BACODSIA,BACONSBF,BACONASS,BACODCUC,BA__BBAN,BA__IBAN,BAIDCRED,BACODVAL,BACALFES,BATIPCOL,BACONCOL,BANUMTEL,BANUMFAX,BA__NOTE,BADTINVA,BADTOBSO,UTCC,UTCV,UTDC,UTDV,BABASINC,BABASIND,BABASLIQ,BAGIOANN,BAPERRIT,BASALCRE,BASALDEB,BANSBTES,BARBNTES  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_BACODBAN;
                  ,this.w_BADESCRI;
                  ,this.w_BAFLPREF;
                  ,this.w_BATIPCON;
                  ,this.w_BAPAESEU;
                  ,this.w_BACINEUR;
                  ,this.w_BACINCAB;
                  ,this.w_BACINABI;
                  ,this.w_BACODABI;
                  ,this.w_BACODCAB;
                  ,this.w_BACONCOR;
                  ,this.w_BACODBIC;
                  ,this.w_BACODSIA;
                  ,this.w_BACONSBF;
                  ,this.w_BACONASS;
                  ,this.w_BACODCUC;
                  ,this.w_BA__BBAN;
                  ,this.w_BA__IBAN;
                  ,this.w_BAIDCRED;
                  ,this.w_BACODVAL;
                  ,this.w_BACALFES;
                  ,this.w_BATIPCOL;
                  ,this.w_BACONCOL;
                  ,this.w_BANUMTEL;
                  ,this.w_BANUMFAX;
                  ,this.w_BA__NOTE;
                  ,this.w_BADTINVA;
                  ,this.w_BADTOBSO;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_BABASINC;
                  ,this.w_BABASIND;
                  ,this.w_BABASLIQ;
                  ,this.w_BAGIOANN;
                  ,this.w_BAPERRIT;
                  ,this.w_BASALCRE;
                  ,this.w_BASALDEB;
                  ,this.w_BANSBTES;
                  ,this.w_BARBNTES;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.COC_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.COC_MAST_IDX,i_nConn)
      *
      * update COC_MAST
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'COC_MAST')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " BADESCRI="+cp_ToStrODBC(this.w_BADESCRI)+;
             ",BAFLPREF="+cp_ToStrODBC(this.w_BAFLPREF)+;
             ",BATIPCON="+cp_ToStrODBC(this.w_BATIPCON)+;
             ",BAPAESEU="+cp_ToStrODBC(this.w_BAPAESEU)+;
             ",BACINEUR="+cp_ToStrODBC(this.w_BACINEUR)+;
             ",BACINCAB="+cp_ToStrODBC(this.w_BACINCAB)+;
             ",BACINABI="+cp_ToStrODBC(this.w_BACINABI)+;
             ",BACODABI="+cp_ToStrODBCNull(this.w_BACODABI)+;
             ",BACODCAB="+cp_ToStrODBCNull(this.w_BACODCAB)+;
             ",BACONCOR="+cp_ToStrODBC(this.w_BACONCOR)+;
             ",BACODBIC="+cp_ToStrODBC(this.w_BACODBIC)+;
             ",BACODSIA="+cp_ToStrODBC(this.w_BACODSIA)+;
             ",BACONSBF="+cp_ToStrODBC(this.w_BACONSBF)+;
             ",BACONASS="+cp_ToStrODBCNull(this.w_BACONASS)+;
             ",BACODCUC="+cp_ToStrODBC(this.w_BACODCUC)+;
             ",BA__BBAN="+cp_ToStrODBC(this.w_BA__BBAN)+;
             ",BA__IBAN="+cp_ToStrODBC(this.w_BA__IBAN)+;
             ",BAIDCRED="+cp_ToStrODBC(this.w_BAIDCRED)+;
             ",BACODVAL="+cp_ToStrODBCNull(this.w_BACODVAL)+;
             ",BACALFES="+cp_ToStrODBCNull(this.w_BACALFES)+;
             ",BATIPCOL="+cp_ToStrODBC(this.w_BATIPCOL)+;
             ",BACONCOL="+cp_ToStrODBCNull(this.w_BACONCOL)+;
             ",BANUMTEL="+cp_ToStrODBC(this.w_BANUMTEL)+;
             ",BANUMFAX="+cp_ToStrODBC(this.w_BANUMFAX)+;
             ",BA__NOTE="+cp_ToStrODBC(this.w_BA__NOTE)+;
             ",BADTINVA="+cp_ToStrODBC(this.w_BADTINVA)+;
             ",BADTOBSO="+cp_ToStrODBC(this.w_BADTOBSO)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",BABASINC="+cp_ToStrODBC(this.w_BABASINC)+;
             ",BABASIND="+cp_ToStrODBC(this.w_BABASIND)+;
             ",BABASLIQ="+cp_ToStrODBC(this.w_BABASLIQ)+;
             ",BAGIOANN="+cp_ToStrODBC(this.w_BAGIOANN)+;
             ",BAPERRIT="+cp_ToStrODBC(this.w_BAPERRIT)+;
             ",BASALCRE="+cp_ToStrODBC(this.w_BASALCRE)+;
             ",BASALDEB="+cp_ToStrODBC(this.w_BASALDEB)+;
             ",BANSBTES="+cp_ToStrODBC(this.w_BANSBTES)+;
             ",BARBNTES="+cp_ToStrODBC(this.w_BARBNTES)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'COC_MAST')
        i_cWhere = cp_PKFox(i_cTable  ,'BACODBAN',this.w_BACODBAN  )
        UPDATE (i_cTable) SET;
              BADESCRI=this.w_BADESCRI;
             ,BAFLPREF=this.w_BAFLPREF;
             ,BATIPCON=this.w_BATIPCON;
             ,BAPAESEU=this.w_BAPAESEU;
             ,BACINEUR=this.w_BACINEUR;
             ,BACINCAB=this.w_BACINCAB;
             ,BACINABI=this.w_BACINABI;
             ,BACODABI=this.w_BACODABI;
             ,BACODCAB=this.w_BACODCAB;
             ,BACONCOR=this.w_BACONCOR;
             ,BACODBIC=this.w_BACODBIC;
             ,BACODSIA=this.w_BACODSIA;
             ,BACONSBF=this.w_BACONSBF;
             ,BACONASS=this.w_BACONASS;
             ,BACODCUC=this.w_BACODCUC;
             ,BA__BBAN=this.w_BA__BBAN;
             ,BA__IBAN=this.w_BA__IBAN;
             ,BAIDCRED=this.w_BAIDCRED;
             ,BACODVAL=this.w_BACODVAL;
             ,BACALFES=this.w_BACALFES;
             ,BATIPCOL=this.w_BATIPCOL;
             ,BACONCOL=this.w_BACONCOL;
             ,BANUMTEL=this.w_BANUMTEL;
             ,BANUMFAX=this.w_BANUMFAX;
             ,BA__NOTE=this.w_BA__NOTE;
             ,BADTINVA=this.w_BADTINVA;
             ,BADTOBSO=this.w_BADTOBSO;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,BABASINC=this.w_BABASINC;
             ,BABASIND=this.w_BABASIND;
             ,BABASLIQ=this.w_BABASLIQ;
             ,BAGIOANN=this.w_BAGIOANN;
             ,BAPERRIT=this.w_BAPERRIT;
             ,BASALCRE=this.w_BASALCRE;
             ,BASALDEB=this.w_BASALDEB;
             ,BANSBTES=this.w_BANSBTES;
             ,BARBNTES=this.w_BARBNTES;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSTE_MCB : Saving
      this.GSTE_MCB.ChangeRow(this.cRowID+'      1',0;
             ,this.w_BACODBAN,"BACODBAN";
             )
      this.GSTE_MCB.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gste_acb
    * --- Esegue Controlli Finali
       this.NotifyEvent('ControlliFinali')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- gste_acb
    * --- Esegue Controlli Finali
       this.NotifyEvent('ControlliFinali')
    * --- Fine Area Manuale
    * --- GSTE_MCB : Deleting
    this.GSTE_MCB.ChangeRow(this.cRowID+'      1',0;
           ,this.w_BACODBAN,"BACODBAN";
           )
    this.GSTE_MCB.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.COC_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.COC_MAST_IDX,i_nConn)
      *
      * delete COC_MAST
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'BACODBAN',this.w_BACODBAN  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_BATIPCON<>.w_BATIPCON
            .w_BAPAESEU = SPACE(2)
        endif
        if .o_BACODCAB<>.w_BACODCAB.or. .o_BATIPCON<>.w_BATIPCON
            .w_BACINEUR = "  "
        endif
        .DoRTCalc(7,7,.t.)
        if .o_BAPAESEU<>.w_BAPAESEU
            .w_FLBANEST = iif(.w_BAPAESEU == 'IT' OR .w_BAPAESEU == 'SM' or ISLESP() ,'N','S')
        endif
        if .o_BACODABI<>.w_BACODABI.or. .o_BATIPCON<>.w_BATIPCON
            .w_BACINABI = IIF(.w_BACONSBF='C', " ", .w_BACINABI)
        endif
        if .o_BATIPCON<>.w_BATIPCON
            .w_BACODABI = IIF(.w_BACONSBF='C', SPACE(5), .w_BACODABI)
          .link_1_10('Full')
        endif
        if .o_BACODABI<>.w_BACODABI.or. .o_BATIPCON<>.w_BATIPCON
            .w_BACODCAB = IIF(.w_BACONSBF='C', SPACE(5), .w_BACODCAB)
          .link_1_11('Full')
        endif
        if .o_BACODABI<>.w_BACODABI.or. .o_BACODCAB<>.w_BACODCAB.or. .o_BACONCOR<>.w_BACONCOR
          .Calculate_GBMRVUGQRR()
        endif
        .DoRTCalc(12,13,.t.)
        if .o_BACODABI<>.w_BACODABI.or. .o_BATIPCON<>.w_BATIPCON
            .w_BACODSIA = SPACE(5)
        endif
        .DoRTCalc(15,15,.t.)
        if .o_BACONSBF<>.w_BACONSBF
            .w_BACONASS = iif(.w_BACONSBF='C','',.w_BACONASS)
          .link_1_17('Full')
        endif
        .DoRTCalc(17,17,.t.)
        if .o_BACODABI<>.w_BACODABI.or. .o_BACODCAB<>.w_BACODCAB.or. .o_BACINABI<>.w_BACINABI.or. .o_BACONCOR<>.w_BACONCOR.or. .o_FLBANEST<>.w_FLBANEST.or. .o_BACINEUR<>.w_BACINEUR
            .w_BA__BBAN = IIF(.w_FLBANEST # 'S',CALCBBAN(.w_BACINABI, .w_BACINEUR, .w_BACODABI, .w_BACODCAB, .w_BACONCOR),' ')
        endif
        if .o_BAPAESEU<>.w_BAPAESEU.or. .o_BA__BBAN<>.w_BA__BBAN
          .Calculate_NOSKZLIJZB()
        endif
        if .o_BA__BBAN<>.w_BA__BBAN.or. .o_BAPAESEU<>.w_BAPAESEU
            .w_CODPA = iif(! empty(.w_BAPAESEU) AND ISLITA(),.w_BAPAESEU,.w_CODPA)
        endif
        if .o_BACINEUR<>.w_BACINEUR
            .w_CIFRA = iif(! empty(.w_BACINEUR) AND ISLITA(),.w_BACINEUR,.w_CIFRA)
        endif
        if .o_BA__BBAN<>.w_BA__BBAN
            .w_IBAN = IIF(ISLITA(),.w_BA__BBAN,SPACE(30))
        endif
        if .o_BA__IBAN<>.w_BA__IBAN
            .w_CODIBAN = .w_BA__IBAN
        endif
        .DoRTCalc(23,50,.t.)
            .w_SALDO = .w_BASALCRE-.w_BASALDEB
            .w_VSALDO = ABS(.w_SALDO)
            .w_VSALDO1 = ABS(.w_SALDO)
        if .o_BAPAESEU<>.w_BAPAESEU
          .Calculate_BTRBSWMTLR()
        endif
        .DoRTCalc(54,54,.t.)
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        if .o_CODPA<>.w_CODPA.or. .o_CIFRA<>.w_CIFRA.or. .o_BACONCOR<>.w_BACONCOR.or. .o_BACINABI<>.w_BACINABI.or. .o_CODIBAN<>.w_CODIBAN.or. .o_BA__BBAN<>.w_BA__BBAN
          .Calculate_BYDCUHYZOQ()
        endif
        if .o_BACINABI<>.w_BACINABI
          .Calculate_HNZJANCFIL()
        endif
        if .o_BACONSBF<>.w_BACONSBF
          .Calculate_FFAYWCZIWO()
        endif
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate(AH_MsgFormat(iif(.w_BATIPCON # 'S',iif(.w_FLBANEST='S' or ISLESP(), "IBAN/Cod. int.:","Codice IBAN:"),'')))
        if .o_FLBANEST<>.w_FLBANEST
          .Calculate_YNHLGUBZHO()
        endif
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        .DoRTCalc(56,58,.t.)
        if .o_BANSBTES<>.w_BANSBTES
            .w_BARBNTES = Space(4)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate(AH_MsgFormat(iif(.w_BATIPCON # 'S',iif(.w_FLBANEST='S' or ISLESP(), "IBAN/Cod. int.:","Codice IBAN:"),'')))
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
    endwith
  return

  proc Calculate_GBMRVUGQRR()
    with this
          * --- Calcolo codice CIN
          .w_BACINABI = IIF(.w_FLBANEST#'S' AND ISLITA(),CALCCIN('C',.w_BACODABI,.w_BACODCAB,.w_BACONCOR),.w_BACINABI)
    endwith
  endproc
  proc Calculate_NOSKZLIJZB()
    with this
          * --- Calcolo caratteri di controllo
          .w_BACINEUR = IIF(.w_FLBANEST#'S' AND ISLITA(),CHECKIBAN('C',.w_BAPAESEU+'00'+.w_BA__BBAN),.w_BACINEUR)
    endwith
  endproc
  proc Calculate_BTRBSWMTLR()
    with this
          * --- Sbianco il codice IBAN banca estera
          .w_CODIBAN = ' '
    endwith
  endproc
  proc Calculate_BYDCUHYZOQ()
    with this
          * --- Calcolo del codice IBAN
          .w_BA__IBAN = iif(.w_FLBANEST='S' OR ! ISLITA(),.w_CODIBAN,CALCIBAN(.w_CODPA, .w_CIFRA, .w_BACINABI, .w_BACODABI, .w_BACODCAB,.w_BACONCOR))
    endwith
  endproc
  proc Calculate_HNZJANCFIL()
    with this
          * --- Controllo codice CIN
          .w_CHECKCIN = IIF(ISLITA(),CALCCIN('V',.w_BACODABI,.w_BACODCAB,.w_BACONCOR,.w_BACINABI,.w_BATIPCON='S'),' ')
    endwith
  endproc
  proc Calculate_IPGCSBLMCC()
    with this
          * --- Gestione campi codice IBAN
          .w_CODPA = iif(empty(.w_CODPA),'IT',.w_CODPA)
          .w_CIFRA = .w_BACINEUR
          .w_IBAN = .w_BA__BBAN
    endwith
  endproc
  proc Calculate_FFAYWCZIWO()
    with this
          * --- Sbianco il codice CIN EUR
          .w_BACINEUR = '  '
    endwith
  endproc
  proc Calculate_YNHLGUBZHO()
    with this
          * --- Aggiorno i campi relativi ai dati conto corrente
          .w_BA__BBAN = iif(.w_FLBANEST='S','  ',.w_BA__BBAN)
          .w_BACINEUR = iif(.w_FLBANEST='S',' ',.w_BACINEUR)
          .w_BACINABI = iif(.w_FLBANEST='S',' ',.w_BACINABI)
          .w_BACODABI = iif(.w_FLBANEST='S',space(5),.w_BACODABI)
          .w_BACODCAB = iif(.w_FLBANEST='S',space(5),.w_BACODCAB)
          .w_BA__IBAN = iif(.w_FLBANEST='S' and not empty(.w_CODIBAN),.w_CODIBAN,.w_BA__IBAN)
    endwith
  endproc
  proc Calculate_TOBKJEWZGD()
    with this
          * --- Valorizzo CODIBAN alla selezione del record
          .w_CODIBAN = iif(IsLesp(),.w_BA__IBAN,' ')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBATIPCON_1_4.enabled = this.oPgFrm.Page1.oPag.oBATIPCON_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBAPAESEU_1_5.enabled = this.oPgFrm.Page1.oPag.oBAPAESEU_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBACINEUR_1_6.enabled = this.oPgFrm.Page1.oPag.oBACINEUR_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBACINABI_1_9.enabled = this.oPgFrm.Page1.oPag.oBACINABI_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBACODABI_1_10.enabled = this.oPgFrm.Page1.oPag.oBACODABI_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBACODCAB_1_11.enabled = this.oPgFrm.Page1.oPag.oBACODCAB_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBACODSIA_1_15.enabled = this.oPgFrm.Page1.oPag.oBACODSIA_1_15.mCond()
    this.oPgFrm.Page1.oPag.oCODPA_1_21.enabled = this.oPgFrm.Page1.oPag.oCODPA_1_21.mCond()
    this.oPgFrm.Page1.oPag.oCIFRA_1_22.enabled = this.oPgFrm.Page1.oPag.oCIFRA_1_22.mCond()
    this.oPgFrm.Page1.oPag.oIBAN_1_23.enabled = this.oPgFrm.Page1.oPag.oIBAN_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBACALFES_1_29.enabled = this.oPgFrm.Page1.oPag.oBACALFES_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBACONCOL_1_32.enabled = this.oPgFrm.Page1.oPag.oBACONCOL_1_32.mCond()
    this.oPgFrm.Page2.oPag.oBAPERRIT_2_5.enabled = this.oPgFrm.Page2.oPag.oBAPERRIT_2_5.mCond()
    this.oPgFrm.Page3.oPag.oBARBNTES_3_2.enabled = this.oPgFrm.Page3.oPag.oBARBNTES_3_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_75.enabled = this.oPgFrm.Page1.oPag.oBtn_1_75.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show2
    i_show2=not(g_ISDF<>'S')
    this.oPgFrm.Pages(3).enabled=i_show2
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Dati DocFinance"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    this.oPgFrm.Page1.oPag.oBAFLPREF_1_3.visible=!this.oPgFrm.Page1.oPag.oBAFLPREF_1_3.mHide()
    this.oPgFrm.Page1.oPag.oBATIPCON_1_4.visible=!this.oPgFrm.Page1.oPag.oBATIPCON_1_4.mHide()
    this.oPgFrm.Page1.oPag.oBAPAESEU_1_5.visible=!this.oPgFrm.Page1.oPag.oBAPAESEU_1_5.mHide()
    this.oPgFrm.Page1.oPag.oBACINEUR_1_6.visible=!this.oPgFrm.Page1.oPag.oBACINEUR_1_6.mHide()
    this.oPgFrm.Page1.oPag.oBACINABI_1_9.visible=!this.oPgFrm.Page1.oPag.oBACINABI_1_9.mHide()
    this.oPgFrm.Page1.oPag.oBACODABI_1_10.visible=!this.oPgFrm.Page1.oPag.oBACODABI_1_10.mHide()
    this.oPgFrm.Page1.oPag.oBACODCAB_1_11.visible=!this.oPgFrm.Page1.oPag.oBACODCAB_1_11.mHide()
    this.oPgFrm.Page1.oPag.oBACODSIA_1_15.visible=!this.oPgFrm.Page1.oPag.oBACODSIA_1_15.mHide()
    this.oPgFrm.Page1.oPag.oBACONASS_1_17.visible=!this.oPgFrm.Page1.oPag.oBACONASS_1_17.mHide()
    this.oPgFrm.Page1.oPag.oBA__BBAN_1_19.visible=!this.oPgFrm.Page1.oPag.oBA__BBAN_1_19.mHide()
    this.oPgFrm.Page1.oPag.oCODPA_1_21.visible=!this.oPgFrm.Page1.oPag.oCODPA_1_21.mHide()
    this.oPgFrm.Page1.oPag.oCIFRA_1_22.visible=!this.oPgFrm.Page1.oPag.oCIFRA_1_22.mHide()
    this.oPgFrm.Page1.oPag.oIBAN_1_23.visible=!this.oPgFrm.Page1.oPag.oIBAN_1_23.mHide()
    this.oPgFrm.Page1.oPag.oCODIBAN_1_24.visible=!this.oPgFrm.Page1.oPag.oCODIBAN_1_24.mHide()
    this.oPgFrm.Page1.oPag.oBACALFES_1_29.visible=!this.oPgFrm.Page1.oPag.oBACALFES_1_29.mHide()
    this.oPgFrm.Page1.oPag.oDESCAL_1_30.visible=!this.oPgFrm.Page1.oPag.oDESCAL_1_30.mHide()
    this.oPgFrm.Page1.oPag.oBACONCOL_1_32.visible=!this.oPgFrm.Page1.oPag.oBACONCOL_1_32.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_33.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page2.oPag.oBAPERRIT_2_5.visible=!this.oPgFrm.Page2.oPag.oBAPERRIT_2_5.mHide()
    this.oPgFrm.Page2.oPag.oVSALDO_2_9.visible=!this.oPgFrm.Page2.oPag.oVSALDO_2_9.mHide()
    this.oPgFrm.Page2.oPag.oVSALDO1_2_10.visible=!this.oPgFrm.Page2.oPag.oVSALDO1_2_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_56.visible=!this.oPgFrm.Page1.oPag.oStr_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_16.visible=!this.oPgFrm.Page2.oPag.oStr_2_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_62.visible=!this.oPgFrm.Page1.oPag.oStr_1_62.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_23.visible=!this.oPgFrm.Page2.oPag.oStr_2_23.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_24.visible=!this.oPgFrm.Page2.oPag.oStr_2_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_66.visible=!this.oPgFrm.Page1.oPag.oStr_1_66.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_69.visible=!this.oPgFrm.Page1.oPag.oStr_1_69.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_71.visible=!this.oPgFrm.Page1.oPag.oStr_1_71.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_75.visible=!this.oPgFrm.Page1.oPag.oBtn_1_75.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Ricalcola")
          .Calculate_GBMRVUGQRR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Ricalcola")
          .Calculate_NOSKZLIJZB()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_67.Event(cEvent)
        if lower(cEvent)==lower("Ricalcola")
          .Calculate_IPGCSBLMCC()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_80.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_83.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_TOBKJEWZGD()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=BACODABI
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_ABI_IDX,3]
    i_lTable = "COD_ABI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2], .t., this.COD_ABI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BACODABI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABI',True,'COD_ABI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ABCODABI like "+cp_ToStrODBC(trim(this.w_BACODABI)+"%");

          i_ret=cp_SQL(i_nConn,"select ABCODABI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ABCODABI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ABCODABI',trim(this.w_BACODABI))
          select ABCODABI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ABCODABI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BACODABI)==trim(_Link_.ABCODABI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BACODABI) and !this.bDontReportError
            deferred_cp_zoom('COD_ABI','*','ABCODABI',cp_AbsName(oSource.parent,'oBACODABI_1_10'),i_cWhere,'GSAR_ABI',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI";
                     +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',oSource.xKey(1))
            select ABCODABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BACODABI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ABCODABI";
                   +" from "+i_cTable+" "+i_lTable+" where ABCODABI="+cp_ToStrODBC(this.w_BACODABI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ABCODABI',this.w_BACODABI)
            select ABCODABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BACODABI = NVL(_Link_.ABCODABI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_BACODABI = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_ABI_IDX,2])+'\'+cp_ToStr(_Link_.ABCODABI,1)
      cp_ShowWarn(i_cKey,this.COD_ABI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BACODABI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BACODCAB
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_CAB_IDX,3]
    i_lTable = "COD_CAB"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2], .t., this.COD_CAB_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BACODCAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFI',True,'COD_CAB')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FICODCAB like "+cp_ToStrODBC(trim(this.w_BACODCAB)+"%");
                   +" and FICODABI="+cp_ToStrODBC(this.w_BACODABI);

          i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FICODABI,FICODCAB","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FICODABI',this.w_BACODABI;
                     ,'FICODCAB',trim(this.w_BACODCAB))
          select FICODABI,FICODCAB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FICODABI,FICODCAB into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BACODCAB)==trim(_Link_.FICODCAB) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BACODCAB) and !this.bDontReportError
            deferred_cp_zoom('COD_CAB','*','FICODABI,FICODCAB',cp_AbsName(oSource.parent,'oBACODCAB_1_11'),i_cWhere,'GSAR_AFI',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_BACODABI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FICODABI,FICODCAB;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB";
                     +" from "+i_cTable+" "+i_lTable+" where FICODCAB="+cp_ToStrODBC(oSource.xKey(2));
                     +" and FICODABI="+cp_ToStrODBC(this.w_BACODABI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODABI',oSource.xKey(1);
                       ,'FICODCAB',oSource.xKey(2))
            select FICODABI,FICODCAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BACODCAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODABI,FICODCAB";
                   +" from "+i_cTable+" "+i_lTable+" where FICODCAB="+cp_ToStrODBC(this.w_BACODCAB);
                   +" and FICODABI="+cp_ToStrODBC(this.w_BACODABI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODABI',this.w_BACODABI;
                       ,'FICODCAB',this.w_BACODCAB)
            select FICODABI,FICODCAB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BACODCAB = NVL(_Link_.FICODCAB,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_BACODCAB = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_CAB_IDX,2])+'\'+cp_ToStr(_Link_.FICODABI,1)+'\'+cp_ToStr(_Link_.FICODCAB,1)
      cp_ShowWarn(i_cKey,this.COD_CAB_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BACODCAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BACONASS
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BACONASS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_BACONASS)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BACINEUR,BAPAESEU,BACINABI,BACODABI,BACODCAB,BACONCOR,BACODBIC,BACODSIA,BA__BBAN,BA__IBAN,BACODVAL,BACALFES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_BACONASS))
          select BACODBAN,BACINEUR,BAPAESEU,BACINABI,BACODABI,BACODCAB,BACONCOR,BACODBIC,BACODSIA,BA__BBAN,BA__IBAN,BACODVAL,BACALFES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BACONASS)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BACONASS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oBACONASS_1_17'),i_cWhere,'GSTE_ACB',"Conti correnti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACINEUR,BAPAESEU,BACINABI,BACODABI,BACODCAB,BACONCOR,BACODBIC,BACODSIA,BA__BBAN,BA__IBAN,BACODVAL,BACALFES";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BACINEUR,BAPAESEU,BACINABI,BACODABI,BACODCAB,BACONCOR,BACODBIC,BACODSIA,BA__BBAN,BA__IBAN,BACODVAL,BACALFES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BACONASS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACINEUR,BAPAESEU,BACINABI,BACODABI,BACODCAB,BACONCOR,BACODBIC,BACODSIA,BA__BBAN,BA__IBAN,BACODVAL,BACALFES";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BACONASS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BACONASS)
            select BACODBAN,BACINEUR,BAPAESEU,BACINABI,BACODABI,BACODCAB,BACONCOR,BACODBIC,BACODSIA,BA__BBAN,BA__IBAN,BACODVAL,BACALFES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BACONASS = NVL(_Link_.BACODBAN,space(15))
      this.w_BACINEUR = NVL(_Link_.BACINEUR,space(2))
      this.w_BAPAESEU = NVL(_Link_.BAPAESEU,space(2))
      this.w_BACINABI = NVL(_Link_.BACINABI,space(1))
      this.w_BACODABI = NVL(_Link_.BACODABI,space(5))
      this.w_BACODCAB = NVL(_Link_.BACODCAB,space(5))
      this.w_BACONCOR = NVL(_Link_.BACONCOR,space(12))
      this.w_BACODBIC = NVL(_Link_.BACODBIC,space(11))
      this.w_BACODSIA = NVL(_Link_.BACODSIA,space(5))
      this.w_BA__BBAN = NVL(_Link_.BA__BBAN,space(30))
      this.w_BA__IBAN = NVL(_Link_.BA__IBAN,space(35))
      this.w_BACODVAL = NVL(_Link_.BACODVAL,space(3))
      this.w_BACALFES = NVL(_Link_.BACALFES,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_BACONASS = space(15)
      endif
      this.w_BACINEUR = space(2)
      this.w_BAPAESEU = space(2)
      this.w_BACINABI = space(1)
      this.w_BACODABI = space(5)
      this.w_BACODCAB = space(5)
      this.w_BACONCOR = space(12)
      this.w_BACODBIC = space(11)
      this.w_BACODSIA = space(5)
      this.w_BA__BBAN = space(30)
      this.w_BA__IBAN = space(35)
      this.w_BACODVAL = space(3)
      this.w_BACALFES = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BACONASS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BACODVAL
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BACODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_BACODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_BACODVAL))
          select VACODVAL,VADESVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BACODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BACODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oBACODVAL_1_27'),i_cWhere,'GSAR_AVL',"",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BACODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_BACODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_BACODVAL)
            select VACODVAL,VADESVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BACODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_BACODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=g_BANC<>'S' OR NOT EMPTY(.w_BACODVAL)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice valuta conto inesistente o incongruente")
        endif
        this.w_BACODVAL = space(3)
        this.w_DESVAL = space(35)
        this.w_DECTOT = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BACODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_27(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_27.VACODVAL as VACODVAL127"+ ",link_1_27.VADESVAL as VADESVAL127"+ ",link_1_27.VADECTOT as VADECTOT127"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_27 on COC_MAST.BACODVAL=link_1_27.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_27"
          i_cKey=i_cKey+'+" and COC_MAST.BACODVAL=link_1_27.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=BACALFES
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FES_MAST_IDX,3]
    i_lTable = "FES_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2], .t., this.FES_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BACALFES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSBA_MFE',True,'FES_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FECODICE like "+cp_ToStrODBC(trim(this.w_BACALFES)+"%");

          i_ret=cp_SQL(i_nConn,"select FECODICE,FEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FECODICE',trim(this.w_BACALFES))
          select FECODICE,FEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BACALFES)==trim(_Link_.FECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BACALFES) and !this.bDontReportError
            deferred_cp_zoom('FES_MAST','*','FECODICE',cp_AbsName(oSource.parent,'oBACALFES_1_29'),i_cWhere,'GSBA_MFE',"Calendario festivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FECODICE,FEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FECODICE',oSource.xKey(1))
            select FECODICE,FEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BACALFES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FECODICE,FEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FECODICE="+cp_ToStrODBC(this.w_BACALFES);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FECODICE',this.w_BACALFES)
            select FECODICE,FEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BACALFES = NVL(_Link_.FECODICE,space(3))
      this.w_DESCAL = NVL(_Link_.FEDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_BACALFES = space(3)
      endif
      this.w_DESCAL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2])+'\'+cp_ToStr(_Link_.FECODICE,1)
      cp_ShowWarn(i_cKey,this.FES_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BACALFES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_29(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.FES_MAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_29.FECODICE as FECODICE129"+ ",link_1_29.FEDESCRI as FEDESCRI129"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_29 on COC_MAST.BACALFES=link_1_29.FECODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_29"
          i_cKey=i_cKey+'+" and COC_MAST.BACALFES=link_1_29.FECODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=BACONCOL
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BACONCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_BACONCOL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_BATIPCOL);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_BATIPCOL;
                     ,'ANCODICE',trim(this.w_BACONCOL))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BACONCOL)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_BACONCOL)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_BATIPCOL);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_BACONCOL)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_BATIPCOL);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_BACONCOL) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oBACONCOL_1_32'),i_cWhere,'GSAR_BZC',"Conti contabili banche",'GSAR1ACL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_BATIPCOL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Inserire un conto congruente (tipo=banche) o gestito a partite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_BATIPCOL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BACONCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_BACONCOL);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_BATIPCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_BATIPCOL;
                       ,'ANCODICE',this.w_BACONCOL)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BACONCOL = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSOT = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_BACONCOL = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_TIPSOT = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PARTSN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPSOT='B' AND (((.w_PARTSN='S' AND .w_BACONSBF='S') Or g_ISDF='S') OR .w_BACONSBF<>'S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire un conto congruente (tipo=banche) o gestito a partite")
        endif
        this.w_BACONCOL = space(15)
        this.w_DESCON = space(40)
        this.w_TIPSOT = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PARTSN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BACONCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_32(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_32.ANCODICE as ANCODICE132"+ ",link_1_32.ANDESCRI as ANDESCRI132"+ ",link_1_32.ANTIPSOT as ANTIPSOT132"+ ",link_1_32.ANDTOBSO as ANDTOBSO132"+ ",link_1_32.ANPARTSN as ANPARTSN132"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_32 on COC_MAST.BACONCOL=link_1_32.ANCODICE"+" and COC_MAST.BATIPCOL=link_1_32.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_32"
          i_cKey=i_cKey+'+" and COC_MAST.BACONCOL=link_1_32.ANCODICE(+)"'+'+" and COC_MAST.BATIPCOL=link_1_32.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oBACODBAN_1_1.value==this.w_BACODBAN)
      this.oPgFrm.Page1.oPag.oBACODBAN_1_1.value=this.w_BACODBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oBADESCRI_1_2.value==this.w_BADESCRI)
      this.oPgFrm.Page1.oPag.oBADESCRI_1_2.value=this.w_BADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oBAFLPREF_1_3.RadioValue()==this.w_BAFLPREF)
      this.oPgFrm.Page1.oPag.oBAFLPREF_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBATIPCON_1_4.RadioValue()==this.w_BATIPCON)
      this.oPgFrm.Page1.oPag.oBATIPCON_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBAPAESEU_1_5.value==this.w_BAPAESEU)
      this.oPgFrm.Page1.oPag.oBAPAESEU_1_5.value=this.w_BAPAESEU
    endif
    if not(this.oPgFrm.Page1.oPag.oBACINEUR_1_6.value==this.w_BACINEUR)
      this.oPgFrm.Page1.oPag.oBACINEUR_1_6.value=this.w_BACINEUR
    endif
    if not(this.oPgFrm.Page1.oPag.oBACINABI_1_9.value==this.w_BACINABI)
      this.oPgFrm.Page1.oPag.oBACINABI_1_9.value=this.w_BACINABI
    endif
    if not(this.oPgFrm.Page1.oPag.oBACODABI_1_10.value==this.w_BACODABI)
      this.oPgFrm.Page1.oPag.oBACODABI_1_10.value=this.w_BACODABI
    endif
    if not(this.oPgFrm.Page1.oPag.oBACODCAB_1_11.value==this.w_BACODCAB)
      this.oPgFrm.Page1.oPag.oBACODCAB_1_11.value=this.w_BACODCAB
    endif
    if not(this.oPgFrm.Page1.oPag.oBACONCOR_1_12.value==this.w_BACONCOR)
      this.oPgFrm.Page1.oPag.oBACONCOR_1_12.value=this.w_BACONCOR
    endif
    if not(this.oPgFrm.Page1.oPag.oBACODBIC_1_14.value==this.w_BACODBIC)
      this.oPgFrm.Page1.oPag.oBACODBIC_1_14.value=this.w_BACODBIC
    endif
    if not(this.oPgFrm.Page1.oPag.oBACODSIA_1_15.value==this.w_BACODSIA)
      this.oPgFrm.Page1.oPag.oBACODSIA_1_15.value=this.w_BACODSIA
    endif
    if not(this.oPgFrm.Page1.oPag.oBACONSBF_1_16.RadioValue()==this.w_BACONSBF)
      this.oPgFrm.Page1.oPag.oBACONSBF_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBACONASS_1_17.value==this.w_BACONASS)
      this.oPgFrm.Page1.oPag.oBACONASS_1_17.value=this.w_BACONASS
    endif
    if not(this.oPgFrm.Page1.oPag.oBACODCUC_1_18.value==this.w_BACODCUC)
      this.oPgFrm.Page1.oPag.oBACODCUC_1_18.value=this.w_BACODCUC
    endif
    if not(this.oPgFrm.Page1.oPag.oBA__BBAN_1_19.value==this.w_BA__BBAN)
      this.oPgFrm.Page1.oPag.oBA__BBAN_1_19.value=this.w_BA__BBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPA_1_21.value==this.w_CODPA)
      this.oPgFrm.Page1.oPag.oCODPA_1_21.value=this.w_CODPA
    endif
    if not(this.oPgFrm.Page1.oPag.oCIFRA_1_22.value==this.w_CIFRA)
      this.oPgFrm.Page1.oPag.oCIFRA_1_22.value=this.w_CIFRA
    endif
    if not(this.oPgFrm.Page1.oPag.oIBAN_1_23.value==this.w_IBAN)
      this.oPgFrm.Page1.oPag.oIBAN_1_23.value=this.w_IBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODIBAN_1_24.value==this.w_CODIBAN)
      this.oPgFrm.Page1.oPag.oCODIBAN_1_24.value=this.w_CODIBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oBAIDCRED_1_26.value==this.w_BAIDCRED)
      this.oPgFrm.Page1.oPag.oBAIDCRED_1_26.value=this.w_BAIDCRED
    endif
    if not(this.oPgFrm.Page1.oPag.oBACODVAL_1_27.value==this.w_BACODVAL)
      this.oPgFrm.Page1.oPag.oBACODVAL_1_27.value=this.w_BACODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVAL_1_28.value==this.w_DESVAL)
      this.oPgFrm.Page1.oPag.oDESVAL_1_28.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oBACALFES_1_29.value==this.w_BACALFES)
      this.oPgFrm.Page1.oPag.oBACALFES_1_29.value=this.w_BACALFES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAL_1_30.value==this.w_DESCAL)
      this.oPgFrm.Page1.oPag.oDESCAL_1_30.value=this.w_DESCAL
    endif
    if not(this.oPgFrm.Page1.oPag.oBACONCOL_1_32.value==this.w_BACONCOL)
      this.oPgFrm.Page1.oPag.oBACONCOL_1_32.value=this.w_BACONCOL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_33.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_33.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oBANUMTEL_1_34.value==this.w_BANUMTEL)
      this.oPgFrm.Page1.oPag.oBANUMTEL_1_34.value=this.w_BANUMTEL
    endif
    if not(this.oPgFrm.Page1.oPag.oBANUMFAX_1_35.value==this.w_BANUMFAX)
      this.oPgFrm.Page1.oPag.oBANUMFAX_1_35.value=this.w_BANUMFAX
    endif
    if not(this.oPgFrm.Page1.oPag.oBA__NOTE_1_36.value==this.w_BA__NOTE)
      this.oPgFrm.Page1.oPag.oBA__NOTE_1_36.value=this.w_BA__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oBADTINVA_1_37.value==this.w_BADTINVA)
      this.oPgFrm.Page1.oPag.oBADTINVA_1_37.value=this.w_BADTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oBADTOBSO_1_38.value==this.w_BADTOBSO)
      this.oPgFrm.Page1.oPag.oBADTOBSO_1_38.value=this.w_BADTOBSO
    endif
    if not(this.oPgFrm.Page2.oPag.oBABASINC_2_1.RadioValue()==this.w_BABASINC)
      this.oPgFrm.Page2.oPag.oBABASINC_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oBABASIND_2_2.RadioValue()==this.w_BABASIND)
      this.oPgFrm.Page2.oPag.oBABASIND_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oBABASLIQ_2_3.RadioValue()==this.w_BABASLIQ)
      this.oPgFrm.Page2.oPag.oBABASLIQ_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oBAGIOANN_2_4.value==this.w_BAGIOANN)
      this.oPgFrm.Page2.oPag.oBAGIOANN_2_4.value=this.w_BAGIOANN
    endif
    if not(this.oPgFrm.Page2.oPag.oBAPERRIT_2_5.value==this.w_BAPERRIT)
      this.oPgFrm.Page2.oPag.oBAPERRIT_2_5.value=this.w_BAPERRIT
    endif
    if not(this.oPgFrm.Page2.oPag.oBASALCRE_2_6.value==this.w_BASALCRE)
      this.oPgFrm.Page2.oPag.oBASALCRE_2_6.value=this.w_BASALCRE
    endif
    if not(this.oPgFrm.Page2.oPag.oBASALDEB_2_7.value==this.w_BASALDEB)
      this.oPgFrm.Page2.oPag.oBASALDEB_2_7.value=this.w_BASALDEB
    endif
    if not(this.oPgFrm.Page2.oPag.oVSALDO_2_9.value==this.w_VSALDO)
      this.oPgFrm.Page2.oPag.oVSALDO_2_9.value=this.w_VSALDO
    endif
    if not(this.oPgFrm.Page2.oPag.oVSALDO1_2_10.value==this.w_VSALDO1)
      this.oPgFrm.Page2.oPag.oVSALDO1_2_10.value=this.w_VSALDO1
    endif
    if not(this.oPgFrm.Page3.oPag.oBANSBTES_3_1.value==this.w_BANSBTES)
      this.oPgFrm.Page3.oPag.oBANSBTES_3_1.value=this.w_BANSBTES
    endif
    if not(this.oPgFrm.Page3.oPag.oBARBNTES_3_2.value==this.w_BARBNTES)
      this.oPgFrm.Page3.oPag.oBARBNTES_3_2.value=this.w_BARBNTES
    endif
    cp_SetControlsValueExtFlds(this,'COC_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_BACODBAN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBACODBAN_1_1.SetFocus()
            i_bnoObbl = !empty(.w_BACODBAN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(g_BANC<>'S' OR NOT EMPTY(.w_BACODVAL))  and not(empty(.w_BACODVAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBACODVAL_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice valuta conto inesistente o incongruente")
          case   ((empty(.w_BACONCOL)) or not(.w_TIPSOT='B' AND (((.w_PARTSN='S' AND .w_BACONSBF='S') Or g_ISDF='S') OR .w_BACONSBF<>'S')))  and not(g_COGE<>'S' AND !IsAlt())  and (g_COGE='S' OR IsAlt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBACONCOL_1_32.SetFocus()
            i_bnoObbl = !empty(.w_BACONCOL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un conto congruente (tipo=banche) o gestito a partite")
          case   not((Not Empty(.w_Bansbtes) And Not Empty(.w_Barbntes)) Or Empty(.w_Bansbtes))  and (Not Empty(.w_Bansbtes))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oBARBNTES_3_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice Rbn non inserito")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSTE_MCB.CheckForm()
      if i_bres
        i_bres=  .GSTE_MCB.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gste_acb
      * --- Controllo esistenza Codice Valuta
      IF i_bRes AND g_BANC='S' AND EMPTY(.w_BACODVAL)
         i_bnoChk = .f.
         i_bRes = .f.
         i_cErrorMsg = ah_MsgFormat("Codice valuta conto inesistente o incongruente")
      ENDIF
      IF i_bRes And Not((.w_BACONSBF<>'S') OR (.w_BACONSBF='S' AND NOT EMPTY(NVL(.w_BACONASS,''))))
         i_bnoChk = .f.
         i_bRes = .f.
         i_cErrorMsg = ah_MsgFormat("Inserire un conto corrente associato")
      Endif
      
      *-- Controllo non bloccante sul codice IBAN --*
      if . w_FLBANEST#'S' and . w_BATIPCON # 'S' and CHECKIBAN('V',.w_CODPA+.w_CIFRA+.w_IBAN) # 'OK' AND ISLITA()
         Ah_ErrorMsg('Codice IBAN errato','48','')  
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_BATIPCON = this.w_BATIPCON
    this.o_BAPAESEU = this.w_BAPAESEU
    this.o_BACINEUR = this.w_BACINEUR
    this.o_FLBANEST = this.w_FLBANEST
    this.o_BACINABI = this.w_BACINABI
    this.o_BACODABI = this.w_BACODABI
    this.o_BACODCAB = this.w_BACODCAB
    this.o_BACONCOR = this.w_BACONCOR
    this.o_BACONSBF = this.w_BACONSBF
    this.o_BA__BBAN = this.w_BA__BBAN
    this.o_CODPA = this.w_CODPA
    this.o_CIFRA = this.w_CIFRA
    this.o_IBAN = this.w_IBAN
    this.o_CODIBAN = this.w_CODIBAN
    this.o_BA__IBAN = this.w_BA__IBAN
    this.o_BANSBTES = this.w_BANSBTES
    * --- GSTE_MCB : Depends On
    this.GSTE_MCB.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgste_acbPag1 as StdContainer
  Width  = 621
  height = 450
  stdWidth  = 621
  stdheight = 450
  resizeXpos=278
  resizeYpos=328
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oBACODBAN_1_1 as StdField with uid="DFVISKXWYN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_BACODBAN", cQueryName = "BACODBAN",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice del conto banche",;
    HelpContextID = 267828892,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=133, Top=13, InputMask=replicate('X',15)

  add object oBADESCRI_1_2 as StdField with uid="VLOIHENDKG",rtseq=2,rtrep=.f.,;
    cFormVar = "w_BADESCRI", cQueryName = "BADESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 235974305,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=275, Top=13, InputMask=replicate('X',35)

  add object oBAFLPREF_1_3 as StdCheck with uid="PCDRSEIUGH",rtseq=3,rtrep=.f.,left=276, top=36, caption="Preferenziale",;
    ToolTipText = "Conto banca preferenziale",;
    HelpContextID = 13005148,;
    cFormVar="w_BAFLPREF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oBAFLPREF_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oBAFLPREF_1_3.GetRadio()
    this.Parent.oContained.w_BAFLPREF = this.RadioValue()
    return .t.
  endfunc

  func oBAFLPREF_1_3.SetRadio()
    this.Parent.oContained.w_BAFLPREF=trim(this.Parent.oContained.w_BAFLPREF)
    this.value = ;
      iif(this.Parent.oContained.w_BAFLPREF=='S',1,;
      0)
  endfunc

  func oBAFLPREF_1_3.mHide()
    with this.Parent.oContained
      return (NOT ISALT())
    endwith
  endfunc

  add object oBATIPCON_1_4 as StdCheck with uid="NLOVXRJXZY",rtseq=4,rtrep=.f.,left=442, top=36, caption="Conto compensazione",;
    ToolTipText = "Se attivo: il conto corrente � considerato di compensazione nei rapporti tra clienti e fornitori",;
    HelpContextID = 238792348,;
    cFormVar="w_BATIPCON", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oBATIPCON_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oBATIPCON_1_4.GetRadio()
    this.Parent.oContained.w_BATIPCON = this.RadioValue()
    return .t.
  endfunc

  func oBATIPCON_1_4.SetRadio()
    this.Parent.oContained.w_BATIPCON=trim(this.Parent.oContained.w_BATIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_BATIPCON=='S',1,;
      0)
  endfunc

  func oBATIPCON_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_BANC='S')
    endwith
   endif
  endfunc

  func oBATIPCON_1_4.mHide()
    with this.Parent.oContained
      return (g_BANC<>'S')
    endwith
  endfunc

  add object oBAPAESEU_1_5 as StdField with uid="XOPOWGIUZQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_BAPAESEU", cQueryName = "BAPAESEU",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice paese europeo",;
    HelpContextID = 17568107,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=133, Top=66, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2)

  func oBAPAESEU_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_BATIPCON<>'S')
    endwith
   endif
  endfunc

  func oBAPAESEU_1_5.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S')
    endwith
  endfunc

  add object oBACINEUR_1_6 as StdField with uid="XEWOKYAAPO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_BACINEUR", cQueryName = "BACINEUR",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice CIN europeo",;
    HelpContextID = 61030760,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=238, Top=66, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2)

  func oBACINEUR_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_BACODCAB<>SPACE(5) AND .w_BATIPCON<>'S' and .w_FLBANEST#'S')
    endwith
   endif
  endfunc

  func oBACINEUR_1_6.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S')
    endwith
  endfunc

  add object oBACINABI_1_9 as StdField with uid="SHVLKRTMXT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_BACINABI", cQueryName = "BACINABI",;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Codice CIN italiano",;
    HelpContextID = 6078113,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=333, Top=66, cSayPict="'!'", cGetPict="'!'", InputMask=replicate('X',1)

  func oBACINABI_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_BACODABI<>SPACE(5) AND .w_BATIPCON<>'S' and .w_FLBANEST#'S')
    endwith
   endif
  endfunc

  func oBACINABI_1_9.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S')
    endwith
  endfunc

  add object oBACODABI_1_10 as StdField with uid="SVUWYULTXJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_BACODABI", cQueryName = "BACODABI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice ABI della banca",;
    HelpContextID = 16170657,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=416, Top=66, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_ABI", cZoomOnZoom="GSAR_ABI", oKey_1_1="ABCODABI", oKey_1_2="this.w_BACODABI"

  func oBACODABI_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_BATIPCON<>'S' and .w_FLBANEST#'S')
    endwith
   endif
  endfunc

  func oBACODABI_1_10.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S')
    endwith
  endfunc

  func oBACODABI_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
      if .not. empty(.w_BACODCAB)
        bRes2=.link_1_11('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oBACODABI_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBACODABI_1_10.mZoom
      with this.Parent.oContained
        GSAR_BBA(this.Parent.oContained,"D")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oBACODABI_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ABCODABI=this.parent.oContained.w_BACODABI
     i_obj.ecpSave()
  endproc

  add object oBACODCAB_1_11 as StdField with uid="MMAFDVGNZK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_BACODCAB", cQueryName = "BACODCAB",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice CAB della filiale banca",;
    HelpContextID = 251051688,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=550, Top=66, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="COD_CAB", cZoomOnZoom="GSAR_AFI", oKey_1_1="FICODABI", oKey_1_2="this.w_BACODABI", oKey_2_1="FICODCAB", oKey_2_2="this.w_BACODCAB"

  func oBACODCAB_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_BACODABI<>SPACE(5) AND .w_BATIPCON<>'S' and .w_FLBANEST#'S')
    endwith
   endif
  endfunc

  func oBACODCAB_1_11.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S')
    endwith
  endfunc

  func oBACODCAB_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oBACODCAB_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBACODCAB_1_11.mZoom
      with this.Parent.oContained
        GSAR_BBA(this.Parent.oContained,"E")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oBACODCAB_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.FICODABI=w_BACODABI
     i_obj.w_FICODCAB=this.parent.oContained.w_BACODCAB
     i_obj.ecpSave()
  endproc

  add object oBACONCOR_1_12 as StdField with uid="NGXAFVGSQU",rtseq=12,rtrep=.f.,;
    cFormVar = "w_BACONCOR", cQueryName = "BACONCOR",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    ToolTipText = "Numero conto corrente",;
    HelpContextID = 240565912,;
   bGlobalFont=.t.,;
    Height=21, Width=110, Left=133, Top=92, InputMask=replicate('X',12)

  add object oBACODBIC_1_14 as StdField with uid="NDRTNZZHWB",rtseq=13,rtrep=.f.,;
    cFormVar = "w_BACODBIC", cQueryName = "BACODBIC",;
    bObbl = .f. , nPag = 1, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Codice BIC",;
    HelpContextID = 606553,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=351, Top=91, InputMask=replicate('X',11)

  add object oBACODSIA_1_15 as StdField with uid="VXUOCQSEEO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_BACODSIA", cQueryName = "BACODSIA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Nostro Codice SIA assegnato dalla banca",;
    HelpContextID = 17383767,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=550, Top=92, InputMask=replicate('X',5)

  func oBACODSIA_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_BATIPCON<>'S')
    endwith
   endif
  endfunc

  func oBACODSIA_1_15.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S')
    endwith
  endfunc


  add object oBACONSBF_1_16 as StdCombo with uid="MJQQFCLIMV",rtseq=15,rtrep=.f.,left=133,top=118,width=112,height=21;
    , ToolTipText = "Tipo conto (salvo buon fine, conto corrente)";
    , HelpContextID = 27869532;
    , cFormVar="w_BACONSBF",RowSource=""+"Conto corrente,"+"Salvo buon fine", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oBACONSBF_1_16.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oBACONSBF_1_16.GetRadio()
    this.Parent.oContained.w_BACONSBF = this.RadioValue()
    return .t.
  endfunc

  func oBACONSBF_1_16.SetRadio()
    this.Parent.oContained.w_BACONSBF=trim(this.Parent.oContained.w_BACONSBF)
    this.value = ;
      iif(this.Parent.oContained.w_BACONSBF=='C',1,;
      iif(this.Parent.oContained.w_BACONSBF=='S',2,;
      0))
  endfunc

  add object oBACONASS_1_17 as StdField with uid="XQWWHBJPZZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_BACONASS", cQueryName = "BACONASS",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto corrente associato",;
    HelpContextID = 5684887,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=351, Top=116, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_BACONASS"

  func oBACONASS_1_17.mHide()
    with this.Parent.oContained
      return (.w_BACONSBF='C')
    endwith
  endfunc

  func oBACONASS_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oBACONASS_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBACONASS_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oBACONASS_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti correnti",'',this.parent.oContained
  endproc
  proc oBACONASS_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_BACONASS
     i_obj.ecpSave()
  endproc

  add object oBACODCUC_1_18 as StdField with uid="SBNBQAPWTW",rtseq=17,rtrep=.f.,;
    cFormVar = "w_BACODCUC", cQueryName = "BACODCUC",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice CUC",;
    HelpContextID = 17383769,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=550, Top=116, cSayPict="'!!!!!!!!'", cGetPict="'!!!!!!!!'", InputMask=replicate('X',8)

  add object oBA__BBAN_1_19 as StdField with uid="UDIJKFMPQW",rtseq=18,rtrep=.f.,;
    cFormVar = "w_BA__BBAN", cQueryName = "BA__BBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Codice BBAN",;
    HelpContextID = 327324,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=133, Top=148, InputMask=replicate('X',30)

  func oBA__BBAN_1_19.mHide()
    with this.Parent.oContained
      return (.w_FLBANEST = 'S' or .w_BATIPCON = 'S')
    endwith
  endfunc

  add object oCODPA_1_21 as StdField with uid="VSCVVVXFHG",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODPA", cQueryName = "CODPA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice paese (2)",;
    HelpContextID = 36020442,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=133, Top=174, InputMask=replicate('X',2)

  func oCODPA_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_BAPAESEU<>'IT')
    endwith
   endif
  endfunc

  func oCODPA_1_21.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S' or .w_FLBANEST = 'S' or ISLESP())
    endwith
  endfunc

  add object oCIFRA_1_22 as StdField with uid="XMBQBSZSRZ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CIFRA", cQueryName = "CIFRA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Cifra di controllo numerica (2)",;
    HelpContextID = 35882714,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=170, Top=174, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2)

  func oCIFRA_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! ISLITA())
    endwith
   endif
  endfunc

  func oCIFRA_1_22.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S' or .w_FLBANEST = 'S' or ISLESP())
    endwith
  endfunc

  add object oIBAN_1_23 as StdField with uid="KAZGWTHLCV",rtseq=21,rtrep=.f.,;
    cFormVar = "w_IBAN", cQueryName = "IBAN",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "BBAN",;
    HelpContextID = 104324474,;
   bGlobalFont=.t.,;
    Height=21, Width=250, Left=207, Top=174, InputMask=replicate('X',30)

  func oIBAN_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! ISLITA())
    endwith
   endif
  endfunc

  func oIBAN_1_23.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S' or .w_FLBANEST = 'S' or ISLESP())
    endwith
  endfunc

  add object oCODIBAN_1_24 as StdField with uid="SZTHKLGZXM",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CODIBAN", cQueryName = "CODIBAN",;
    bObbl = .f. , nPag = 1, value=space(34), bMultilanguage =  .f.,;
    ToolTipText = "Codice IBAN",;
    HelpContextID = 18653402,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=134, Top=174, InputMask=replicate('X',34)

  func oCODIBAN_1_24.mHide()
    with this.Parent.oContained
      return ((.w_FLBANEST # 'S' and !ISLESP()) or .w_BATIPCON = 'S')
    endwith
  endfunc

  add object oBAIDCRED_1_26 as StdField with uid="VUWHENXCIX",rtseq=24,rtrep=.f.,;
    cFormVar = "w_BAIDCRED", cQueryName = "BAIDCRED",;
    bObbl = .f. , nPag = 1, value=space(23), bMultilanguage =  .f.,;
    ToolTipText = "Codice identificativo del creditore",;
    HelpContextID = 267297114,;
   bGlobalFont=.t.,;
    Height=21, Width=174, Left=133, Top=200, InputMask=replicate('X',23)

  add object oBACODVAL_1_27 as StdField with uid="ZYHWNUPAUO",rtseq=25,rtrep=.f.,;
    cFormVar = "w_BACODVAL", cQueryName = "BACODVAL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice valuta conto inesistente o incongruente",;
    ToolTipText = "Codice della valuta del conto corrente",;
    HelpContextID = 200720030,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=133, Top=224, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_BACODVAL"

  func oBACODVAL_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oBACODVAL_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBACODVAL_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oBACODVAL_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oBACODVAL_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_BACODVAL
     i_obj.ecpSave()
  endproc

  add object oDESVAL_1_28 as StdField with uid="UZCCNBYWNQ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 102677194,;
   bGlobalFont=.t.,;
    Height=21, Width=276, Left=181, Top=224, InputMask=replicate('X',35)

  add object oBACALFES_1_29 as StdField with uid="HOIEQXFHTZ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_BACALFES", cQueryName = "BACALFES",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Calendario della piazza in cui risiede la banca",;
    HelpContextID = 75186537,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=133, Top=247, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="FES_MAST", cZoomOnZoom="GSBA_MFE", oKey_1_1="FECODICE", oKey_1_2="this.w_BACALFES"

  func oBACALFES_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_BANC='S')
    endwith
   endif
  endfunc

  func oBACALFES_1_29.mHide()
    with this.Parent.oContained
      return (g_BANC<>'S')
    endwith
  endfunc

  func oBACALFES_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oBACALFES_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBACALFES_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FES_MAST','*','FECODICE',cp_AbsName(this.parent,'oBACALFES_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSBA_MFE',"Calendario festivit�",'',this.parent.oContained
  endproc
  proc oBACALFES_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSBA_MFE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FECODICE=this.parent.oContained.w_BACALFES
     i_obj.ecpSave()
  endproc

  add object oDESCAL_1_30 as StdField with uid="CBOVQBLRNA",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESCAL", cQueryName = "DESCAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 103922378,;
   bGlobalFont=.t.,;
    Height=21, Width=276, Left=181, Top=247, InputMask=replicate('X',35)

  func oDESCAL_1_30.mHide()
    with this.Parent.oContained
      return (g_BANC<>'S')
    endwith
  endfunc

  add object oBACONCOL_1_32 as StdField with uid="XNMKYJICXT",rtseq=30,rtrep=.f.,;
    cFormVar = "w_BACONCOL", cQueryName = "BACONCOL",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un conto congruente (tipo=banche) o gestito a partite",;
    ToolTipText = "Conto di contabilit� collegato al conto banca",;
    HelpContextID = 240565918,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=133, Top=273, cSayPict="p_CON", cGetPict="p_CON", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_BATIPCOL", oKey_2_1="ANCODICE", oKey_2_2="this.w_BACONCOL"

  func oBACONCOL_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE='S' OR IsAlt())
    endwith
   endif
  endfunc

  func oBACONCOL_1_32.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S' AND !IsAlt())
    endwith
  endfunc

  func oBACONCOL_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oBACONCOL_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBACONCOL_1_32.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_BATIPCOL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_BATIPCOL)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oBACONCOL_1_32'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti contabili banche",'GSAR1ACL.CONTI_VZM',this.parent.oContained
  endproc
  proc oBACONCOL_1_32.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_BATIPCOL
     i_obj.w_ANCODICE=this.parent.oContained.w_BACONCOL
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_33 as StdField with uid="FFVDHPYFBV",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 55687882,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=275, Top=273, InputMask=replicate('X',40)

  func oDESCON_1_33.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S' AND !IsAlt())
    endwith
  endfunc

  add object oBANUMTEL_1_34 as StdField with uid="RGBCLWCLGE",rtseq=32,rtrep=.f.,;
    cFormVar = "w_BANUMTEL", cQueryName = "BANUMTEL",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero telefonico di riferimento del conto",;
    HelpContextID = 44036450,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=133, Top=299, InputMask=replicate('X',18)

  add object oBANUMFAX_1_35 as StdField with uid="XBBMXDPAXY",rtseq=33,rtrep=.f.,;
    cFormVar = "w_BANUMFAX", cQueryName = "BANUMFAX",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero FAX di riferimento del conto",;
    HelpContextID = 190844562,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=457, Top=299, InputMask=replicate('X',18)

  add object oBA__NOTE_1_36 as StdMemo with uid="JKNEKUVNRX",rtseq=34,rtrep=.f.,;
    cFormVar = "w_BA__NOTE", cQueryName = "BA__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 230359387,;
   bGlobalFont=.t.,;
    Height=99, Width=576, Left=20, Top=323

  add object oBADTINVA_1_37 as StdField with uid="VMPBVXULKO",rtseq=35,rtrep=.f.,;
    cFormVar = "w_BADTINVA", cQueryName = "BADTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio validit�",;
    HelpContextID = 207507799,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=285, Top=428, tabstop=.f.

  add object oBADTOBSO_1_38 as StdField with uid="VUSXFTFHKM",rtseq=36,rtrep=.f.,;
    cFormVar = "w_BADTOBSO", cQueryName = "BADTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine attivit�",;
    HelpContextID = 12472677,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=517, Top=428, tabstop=.f.


  add object oObj_1_67 as cp_runprogram with uid="UTMUKFCDVA",left=427, top=466, width=188,height=19,;
    caption='GSTE_BCC',;
   bGlobalFont=.t.,;
    prg="GSTE_BCC",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 28337065


  add object oBtn_1_75 as StdButton with uid="ZUJGQQFCGW",left=551, top=143, width=48,height=45,;
    CpPicture="bmp\refresh.ico", caption="", nPag=1;
    , ToolTipText = "Se premuto viene ricalcolato il codice CIN\IBAN";
    , HelpContextID = 109720586;
    , caption='\<Ricalcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_75.Click()
      with this.Parent.oContained
        .NotifyEvent('Ricalcola')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_75.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (inlist(.cFunction,'Edit','Load'))
      endwith
    endif
  endfunc

  func oBtn_1_75.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_BATIPCON = 'S' or .w_FLBANEST='S' OR ! ISLITA())
     endwith
    endif
  endfunc


  add object oObj_1_80 as cp_calclbl with uid="UOGYQEHZXZ",left=32, top=174, width=100,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",alignment =1,fontsize=9,fontBold=.f.,fontItalic=.f.,fontUnderline=.f.,bGlobalFont=.t.,alignment=1,fontname="Arial",;
    nPag=1;
    , HelpContextID = 68276966


  add object oObj_1_83 as cp_runprogram with uid="ROAAWEFSEL",left=427, top=498, width=188,height=19,;
    caption='GSAR_BCB',;
   bGlobalFont=.t.,;
    prg="GSAR_BCB",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 29111208

  add object oStr_1_39 as StdString with uid="CXUREVQLIP",Visible=.t., Left=387, Top=428,;
    Alignment=1, Width=129, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="NLBKDJGLES",Visible=.t., Left=368, Top=66,;
    Alignment=1, Width=46, Height=15,;
    Caption="ABI:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S')
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="UMDIRWKKQB",Visible=.t., Left=498, Top=66,;
    Alignment=1, Width=50, Height=15,;
    Caption="CAB:"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S')
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="EAHLFDJYGS",Visible=.t., Left=274, Top=66,;
    Alignment=1, Width=56, Height=18,;
    Caption="CIN Ita:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="AYYGCJNGOO",Visible=.t., Left=170, Top=66,;
    Alignment=1, Width=65, Height=18,;
    Caption="CIN EUR.:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S')
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="QPBROINONZ",Visible=.t., Left=154, Top=428,;
    Alignment=1, Width=128, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="JJGDGDIGUQ",Visible=.t., Left=480, Top=92,;
    Alignment=1, Width=68, Height=15,;
    Caption="Codice SIA:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S')
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="AJWABRNWEG",Visible=.t., Left=13, Top=275,;
    Alignment=1, Width=117, Height=15,;
    Caption="Conto contabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S' AND !IsAlt())
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="IZEYDESDMU",Visible=.t., Left=13, Top=13,;
    Alignment=1, Width=117, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_55 as StdString with uid="CHLUVQNXPP",Visible=.t., Left=15, Top=92,;
    Alignment=1, Width=117, Height=18,;
    Caption="Num.C/Corrente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="XJMVJINVIW",Visible=.t., Left=21, Top=41,;
    Alignment=0, Width=173, Height=18,;
    Caption="Coordinate bancarie"  ;
  , bGlobalFont=.t.

  func oStr_1_56.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S')
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="EEVKSRXAAD",Visible=.t., Left=15, Top=66,;
    Alignment=1, Width=117, Height=18,;
    Caption="Paese:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S')
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="NMHNQSLNXP",Visible=.t., Left=13, Top=299,;
    Alignment=1, Width=117, Height=15,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="MNWKCOBRIY",Visible=.t., Left=350, Top=299,;
    Alignment=1, Width=106, Height=15,;
    Caption="FAX:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="PNUAARHROS",Visible=.t., Left=13, Top=228,;
    Alignment=1, Width=117, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="ZSBLQYWBTE",Visible=.t., Left=3, Top=245,;
    Alignment=1, Width=127, Height=18,;
    Caption="Calendario festivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_62.mHide()
    with this.Parent.oContained
      return (g_BANC<>'S')
    endwith
  endfunc

  add object oStr_1_66 as StdString with uid="BURTNOIWVE",Visible=.t., Left=85, Top=148,;
    Alignment=1, Width=47, Height=18,;
    Caption="BBAN:"  ;
  , bGlobalFont=.t.

  func oStr_1_66.mHide()
    with this.Parent.oContained
      return (.w_FLBANEST = 'S' or .w_BATIPCON = 'S')
    endwith
  endfunc

  add object oStr_1_68 as StdString with uid="YOWUERXHZR",Visible=.t., Left=165, Top=176,;
    Alignment=0, Width=6, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S'  or .w_FLBANEST = 'S' or ISLESP())
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="XXRGDQXGQN",Visible=.t., Left=199, Top=176,;
    Alignment=0, Width=6, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  func oStr_1_69.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S' or .w_FLBANEST = 'S' or ISLESP())
    endwith
  endfunc

  add object oStr_1_70 as StdString with uid="XDZJQSEVAS",Visible=.t., Left=59, Top=117,;
    Alignment=1, Width=73, Height=18,;
    Caption="Tipo conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_71 as StdString with uid="APKOPZURJI",Visible=.t., Left=246, Top=116,;
    Alignment=1, Width=102, Height=18,;
    Caption="C/C associato:"  ;
  , bGlobalFont=.t.

  func oStr_1_71.mHide()
    with this.Parent.oContained
      return (.w_BACONSBF='C')
    endwith
  endfunc

  add object oStr_1_72 as StdString with uid="NZAVFCHVEX",Visible=.t., Left=280, Top=93,;
    Alignment=1, Width=68, Height=18,;
    Caption="Codice BIC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="USMFLNLLAV",Visible=.t., Left=-2, Top=200,;
    Alignment=1, Width=132, Height=18,;
    Caption="Identificativo creditore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_84 as StdString with uid="LCHYTWGIDY",Visible=.t., Left=469, Top=116,;
    Alignment=1, Width=79, Height=18,;
    Caption="Codice CUC:"  ;
  , bGlobalFont=.t.

  add object oBox_1_58 as StdBox with uid="LNSTZJRBFD",left=18, top=60, width=559,height=2
enddefine
define class tgste_acbPag2 as StdContainer
  Width  = 621
  height = 450
  stdWidth  = 621
  stdheight = 450
  resizeYpos=280
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBABASINC_2_1 as StdCombo with uid="DMXLTYYPVB",rtseq=44,rtrep=.f.,left=124,top=36,width=128,height=21;
    , ToolTipText = "Frequenza di calcolo interessi attivi";
    , HelpContextID = 135581351;
    , cFormVar="w_BABASINC",RowSource=""+"Mensile,"+"Bimestrale,"+"Trimestrale,"+"Quadrimestrale,"+"Semestrale,"+"Annuale", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oBABASINC_2_1.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'B',;
    iif(this.value =3,'T',;
    iif(this.value =4,'Q',;
    iif(this.value =5,'S',;
    iif(this.value =6,'A',;
    space(1))))))))
  endfunc
  func oBABASINC_2_1.GetRadio()
    this.Parent.oContained.w_BABASINC = this.RadioValue()
    return .t.
  endfunc

  func oBABASINC_2_1.SetRadio()
    this.Parent.oContained.w_BABASINC=trim(this.Parent.oContained.w_BABASINC)
    this.value = ;
      iif(this.Parent.oContained.w_BABASINC=='M',1,;
      iif(this.Parent.oContained.w_BABASINC=='B',2,;
      iif(this.Parent.oContained.w_BABASINC=='T',3,;
      iif(this.Parent.oContained.w_BABASINC=='Q',4,;
      iif(this.Parent.oContained.w_BABASINC=='S',5,;
      iif(this.Parent.oContained.w_BABASINC=='A',6,;
      0))))))
  endfunc


  add object oBABASIND_2_2 as StdCombo with uid="EXAVAMZHMA",rtseq=45,rtrep=.f.,left=124,top=63,width=128,height=21;
    , ToolTipText = "Frequenza di calcolo interessi passivi";
    , HelpContextID = 135581350;
    , cFormVar="w_BABASIND",RowSource=""+"Mensile,"+"Bimestrale,"+"Trimestrale,"+"Quadrimestrale,"+"Semestrale,"+"Annuale", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oBABASIND_2_2.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'B',;
    iif(this.value =3,'T',;
    iif(this.value =4,'Q',;
    iif(this.value =5,'S',;
    iif(this.value =6,'A',;
    space(1))))))))
  endfunc
  func oBABASIND_2_2.GetRadio()
    this.Parent.oContained.w_BABASIND = this.RadioValue()
    return .t.
  endfunc

  func oBABASIND_2_2.SetRadio()
    this.Parent.oContained.w_BABASIND=trim(this.Parent.oContained.w_BABASIND)
    this.value = ;
      iif(this.Parent.oContained.w_BABASIND=='M',1,;
      iif(this.Parent.oContained.w_BABASIND=='B',2,;
      iif(this.Parent.oContained.w_BABASIND=='T',3,;
      iif(this.Parent.oContained.w_BABASIND=='Q',4,;
      iif(this.Parent.oContained.w_BABASIND=='S',5,;
      iif(this.Parent.oContained.w_BABASIND=='A',6,;
      0))))))
  endfunc


  add object oBABASLIQ_2_3 as StdCombo with uid="PXAZXGZIJX",rtseq=46,rtrep=.f.,left=481,top=36,width=128,height=21;
    , ToolTipText = "Frequenza per il calcolo delle spese di tenuta conto e costo operazioni";
    , HelpContextID = 183185767;
    , cFormVar="w_BABASLIQ",RowSource=""+"Mensile,"+"Bimestrale,"+"Trimestrale,"+"Quadrimestrale,"+"Semestrale,"+"Annuale", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oBABASLIQ_2_3.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'B',;
    iif(this.value =3,'T',;
    iif(this.value =4,'Q',;
    iif(this.value =5,'S',;
    iif(this.value =6,'A',;
    space(1))))))))
  endfunc
  func oBABASLIQ_2_3.GetRadio()
    this.Parent.oContained.w_BABASLIQ = this.RadioValue()
    return .t.
  endfunc

  func oBABASLIQ_2_3.SetRadio()
    this.Parent.oContained.w_BABASLIQ=trim(this.Parent.oContained.w_BABASLIQ)
    this.value = ;
      iif(this.Parent.oContained.w_BABASLIQ=='M',1,;
      iif(this.Parent.oContained.w_BABASLIQ=='B',2,;
      iif(this.Parent.oContained.w_BABASLIQ=='T',3,;
      iif(this.Parent.oContained.w_BABASLIQ=='Q',4,;
      iif(this.Parent.oContained.w_BABASLIQ=='S',5,;
      iif(this.Parent.oContained.w_BABASLIQ=='A',6,;
      0))))))
  endfunc

  add object oBAGIOANN_2_4 as StdField with uid="ZOXLHAHAQO",rtseq=47,rtrep=.f.,;
    cFormVar = "w_BAGIOANN", cQueryName = "BAGIOANN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni anno considerati per il calcolo interessi ( es. 360 o 365 )",;
    HelpContextID = 5013148,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=404, Top=63

  add object oBAPERRIT_2_5 as StdField with uid="WXESQPPKHM",rtseq=48,rtrep=.f.,;
    cFormVar = "w_BAPERRIT", cQueryName = "BAPERRIT",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale ritenuta fiscale su interessi attivi",;
    HelpContextID = 14684522,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=554, Top=65, cSayPict='"999.99"', cGetPict='"999.99"'

  func oBAPERRIT_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_BATIPCON<>'S')
    endwith
   endif
  endfunc

  func oBAPERRIT_2_5.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S')
    endwith
  endfunc

  add object oBASALCRE_2_6 as StdField with uid="FTKBMJRBHV",rtseq=49,rtrep=.f.,;
    cFormVar = "w_BASALCRE", cQueryName = "BASALCRE",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo totale crediti",;
    HelpContextID = 243515045,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=293, Top=120, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oBASALDEB_2_7 as StdField with uid="LRXLVUWNHE",rtseq=50,rtrep=.f.,;
    cFormVar = "w_BASALDEB", cQueryName = "BASALDEB",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo totale debiti",;
    HelpContextID = 41697624,;
   bGlobalFont=.t.,;
    Height=21, Width=156, Left=453, Top=120, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oVSALDO_2_9 as StdField with uid="VXPKKHJWRO",rtseq=52,rtrep=.f.,;
    cFormVar = "w_VSALDO", cQueryName = "VSALDO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo totale a debito",;
    HelpContextID = 49925034,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=156, Left=453, Top=146, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oVSALDO_2_9.mHide()
    with this.Parent.oContained
      return (NOT .w_SALDO<0)
    endwith
  endfunc

  add object oVSALDO1_2_10 as StdField with uid="EJVTQUVDWU",rtseq=53,rtrep=.f.,;
    cFormVar = "w_VSALDO1", cQueryName = "VSALDO1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo totale a credito",;
    HelpContextID = 49925034,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=156, Left=293, Top=146, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oVSALDO1_2_10.mHide()
    with this.Parent.oContained
      return (.w_SALDO<0)
    endwith
  endfunc


  add object oLinkPC_2_11 as stdDynamicChildContainer with uid="UROSRFLXQI",left=-1, top=180, width=617, height=242, bOnScreen=.t.;


  add object oStr_2_12 as StdString with uid="QVAKHBFIQC",Visible=.t., Left=315, Top=63,;
    Alignment=1, Width=85, Height=18,;
    Caption="Giorni anno:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="ETZTWSKIDW",Visible=.t., Left=4, Top=36,;
    Alignment=1, Width=117, Height=18,;
    Caption="Interessi attivi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="QWKMNLHJJH",Visible=.t., Left=4, Top=63,;
    Alignment=1, Width=117, Height=18,;
    Caption="Interessi passivi:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="UNVQLTTQPS",Visible=.t., Left=367, Top=36,;
    Alignment=1, Width=109, Height=18,;
    Caption="Addebito spese:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="BGYUBSRBUL",Visible=.t., Left=454, Top=63,;
    Alignment=1, Width=98, Height=18,;
    Caption="% Rit.fiscale:"  ;
  , bGlobalFont=.t.

  func oStr_2_16.mHide()
    with this.Parent.oContained
      return (.w_BATIPCON='S')
    endwith
  endfunc

  add object oStr_2_17 as StdString with uid="RGPXMBPQPA",Visible=.t., Left=9, Top=10,;
    Alignment=0, Width=156, Height=18,;
    Caption="Criteri di capitalizzazione"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="MLKTNCVABH",Visible=.t., Left=6, Top=94,;
    Alignment=0, Width=199, Height=18,;
    Caption="Saldo contabile del conto corrente"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="PHUKZARZJE",Visible=.t., Left=293, Top=94,;
    Alignment=0, Width=92, Height=18,;
    Caption="Crediti"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="OZKGWATCRA",Visible=.t., Left=453, Top=94,;
    Alignment=0, Width=93, Height=18,;
    Caption="Debiti"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="VFMCVNHGNT",Visible=.t., Left=160, Top=146,;
    Alignment=1, Width=130, Height=18,;
    Caption="Saldo a credito:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_23.mHide()
    with this.Parent.oContained
      return (.w_SALDO<0)
    endwith
  endfunc

  add object oStr_2_24 as StdString with uid="WSSEDYPCKE",Visible=.t., Left=313, Top=146,;
    Alignment=1, Width=138, Height=18,;
    Caption="Saldo a debito:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_2_24.mHide()
    with this.Parent.oContained
      return (.w_SALDO>=0)
    endwith
  endfunc

  add object oBox_2_18 as StdBox with uid="BEFVBZIXCO",left=4, top=28, width=252,height=2

  add object oBox_2_22 as StdBox with uid="DXOMQMCOPV",left=5, top=115, width=608,height=2
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gste_mcb",lower(this.oContained.GSTE_MCB.class))=0
        this.oContained.GSTE_MCB.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgste_acbPag3 as StdContainer
  Width  = 621
  height = 450
  stdWidth  = 621
  stdheight = 450
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oBANSBTES_3_1 as StdField with uid="XPQEPTAAAA",rtseq=58,rtrep=.f.,;
    cFormVar = "w_BANSBTES", cQueryName = "BANSBTES",;
    bObbl = .f. , nPag = 3, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Nostra banca per gestire la canalizzazione",;
    HelpContextID = 32371049,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=186, Top=32, InputMask=replicate('X',8)

  add object oBARBNTES_3_2 as StdField with uid="MMKLSUIGIN",rtseq=59,rtrep=.f.,;
    cFormVar = "w_BARBNTES", cQueryName = "BARBNTES",;
    bObbl = .f. , nPag = 3, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Codice Rbn non inserito",;
    ToolTipText = "Codice conto di nostra banca",;
    HelpContextID = 43856233,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=186, Top=70, InputMask=replicate('X',4)

  func oBARBNTES_3_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_Bansbtes))
    endwith
   endif
  endfunc

  func oBARBNTES_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((Not Empty(.w_Bansbtes) And Not Empty(.w_Barbntes)) Or Empty(.w_Bansbtes))
    endwith
    return bRes
  endfunc

  add object oStr_3_3 as StdString with uid="EWPGUWVNXD",Visible=.t., Left=6, Top=34,;
    Alignment=1, Width=175, Height=18,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oStr_3_4 as StdString with uid="YZNNAADDBD",Visible=.t., Left=123, Top=72,;
    Alignment=1, Width=58, Height=18,;
    Caption="Rbn:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_acb','COC_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".BACODBAN=COC_MAST.BACODBAN";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
