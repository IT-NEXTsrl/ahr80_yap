* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bkl                                                        *
*              Carica lotto da mov.lotti                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_19]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-07-14                                                      *
* Last revis.: 2015-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bkl",oParentObject)
return(i_retval)

define class tgsma_bkl as StdBatch
  * --- Local variables
  w_LONUMCOM = space(20)
  w_LOCODICE = space(20)
  w_LOLOTFOR = space(20)
  w_LOCODART = space(20)
  w_LOCODESE = space(4)
  w_LOSERIAL = space(10)
  w_LODATCRE = ctod("  /  /  ")
  w_LO__NOTE = space(10)
  w_LODATSCA = ctod("  /  /  ")
  w_LOCODCON = space(15)
  w_LOTIPCON = space(1)
  * --- WorkFile variables
  LOTTIART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce un Nuovo Lotto da:
    *     GSMA_MVM                                                    w_FLPRG='M'
    *     GSVE_MDV,GSAC_MDV,GSOR_MDV      w_FLPRG='D'
    *     GSCO_MMT                                                   w_FLPRG='Z'
    *     GSMD_KCR                                                   w_FLPRG='P'
    *     GSAR_MPG                                                   w_FLPRG='C'
    *     GSCO_ADP                                                    w_FLPRG='K'
    this.w_LOCODICE = SPACE(20)
    this.w_LODATSCA = cp_CharToDate("  -  -  ")
    this.w_LOCODCON = SPACE(15)
    this.w_LONUMCOM = SPACE(20)
    this.w_LOLOTFOR = SPACE(20)
    this.w_LOCODESE = g_CODESE
    this.w_LOSERIAL = SPACE(10)
    this.w_LO__NOTE = SPACE(10)
    do case
      case this.oParentObject.w_FLPRG="M"
        * --- Siamo entrati dai Movimenti di Magazzino
        this.w_LOCODART = this.oParentObject.w_MMCODART
        this.w_LODATCRE = this.oParentObject.w_MMDATREG
        this.w_LOTIPCON = this.oParentObject.w_MMTIPCON
        this.w_LOCODCON = IIF(this.w_LOTIPCON="F", this.oParentObject.w_MMCODCON,SPACE(15))
        * --- Se la causale del magazzino come riferimento ha 'Nessuno', nel caso l'esistenza aumenti
        *     occorre abilitare lo zoom dei fornitori
        if this.oParentObject.w_MMFLCASC = "+"
          this.w_LOTIPCON = "F"
        endif
      case this.oParentObject.w_FLPRG="D"
        * --- Siamo entrati dai Documenti
        this.w_LOCODART = this.oParentObject.w_MVCODART
        this.w_LODATCRE = this.oParentObject.w_MVDATREG
        this.w_LOTIPCON = this.oParentObject.w_MVTIPCON
        this.w_LOCODCON = IIF(this.w_LOTIPCON="F", this.oParentObject.w_MVCODCON ,SPACE(15))
        * --- Se la causale del documento come riferimento ha 'Nessuno', nel caso l'esistenza aumenti
        *     occorre abilitare lo zoom dei fornitori
        if this.oParentObject.w_MVFLCASC = "+"
          this.w_LOTIPCON = "F"
        endif
      case this.oParentObject.w_FLPRG$ "Z-I" 
        * --- Siamo entrati dalle Dichiarazioni di Produzione o Inserimento Rapido Matricole
        this.w_LOCODART = this.oParentObject.w_CODART
        this.w_LODATCRE = this.oParentObject.w_DATREG
        this.w_LOTIPCON = " "
        this.w_LOCODCON = SPACE(15)
      case this.oParentObject.w_FLPRG="P"
        * --- Siamo entrati da Caricamento Rapido Documenti
        this.w_LOCODART = this.oParentObject.w_CODART
        this.w_LODATCRE = this.oParentObject.w_DATREG
        this.w_LOTIPCON = this.oParentObject.w_TIPCON
        this.w_LOCODCON = IIF(this.w_LOTIPCON="F", this.oParentObject.w_CODCON,SPACE(15))
      case this.oParentObject.w_FLPRG="K"
        if this.oParentObject.w_DPULTFAS="S"
          * --- Articolo di fase, ultima fase
          this.w_LOCODART = this.oParentObject.w_DPARTPAD
        else
          this.w_LOCODART = this.oParentObject.w_DPCODART
        endif
        this.w_LODATCRE = this.oParentObject.w_DPDATREG
        this.w_LOTIPCON = " "
        this.w_LOCODCON = SPACE(15)
    endcase
    * --- Maschera Carica Lotto
    do GSMD_KLO with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if NOT EMPTY(this.w_LOCODICE) 
      * --- Try
      local bErr_03C87EE8
      bErr_03C87EE8=bTrsErr
      this.Try_03C87EE8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        ah_ErrorMsg("Impossibile inserire anagrafica lotto")
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_03C87EE8
      * --- End
      * --- Aggiorno la movimentazione
      do case
        case this.oParentObject.w_FLPRG="M"
          this.oParentObject.w_MMCODLOT = this.w_LOCODICE
        case this.oParentObject.w_FLPRG="D"
          this.oParentObject.w_MVCODLOT = this.w_LOCODICE
        case this.oParentObject.w_FLPRG="Z"
          this.oParentObject.w_MTCODLOT = this.w_LOCODICE
          this.oParentObject.w_ARTLOTTES = this.w_LOCODART
          this.oParentObject.w_CODLOTTES = this.w_LOCODICE
          this.oParentObject.w_ARTLOT = this.w_LOCODART
        case this.oParentObject.w_FLPRG="P"
          this.oParentObject.w_GPCODLOT = this.w_LOCODICE
        case this.oParentObject.w_FLPRG="K"
          this.oParentObject.w_DPCODLOT = this.w_LOCODICE
          this.oParentObject.w_ARTLOT = this.w_LOCODART
      endcase
      if this.oParentObject.w_FLPRG $ "Z-I"
        this.oParentObject.w_STALOT = "D"
      else
        this.oParentObject.w_FLSTAT = "D"
      endif
    endif
  endproc
  proc Try_03C87EE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.LOTTIART_IDX, 3]
    cp_NextTableProg(this, i_Conn, "PRLOT", "i_codazi,w_LOCODESE,w_LOSERIAL")
    * --- Insert into LOTTIART
    i_nConn=i_TableProp[this.LOTTIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOTTIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LOCODICE"+",LOCODESE"+",LOSERIAL"+",LOCODART"+",LODATCRE"+",LODATSCA"+",LONUMCOM"+",LOTIPCON"+",LOCODCON"+",LOLOTFOR"+",LOFLSTAT"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",LO__NOTE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_LOCODICE),'LOTTIART','LOCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOCODESE),'LOTTIART','LOCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOSERIAL),'LOTTIART','LOSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOCODART),'LOTTIART','LOCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LODATCRE),'LOTTIART','LODATCRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LODATSCA),'LOTTIART','LODATSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LONUMCOM),'LOTTIART','LONUMCOM');
      +","+cp_NullLink(cp_ToStrODBC("F"),'LOTTIART','LOTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOCODCON),'LOTTIART','LOCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOLOTFOR),'LOTTIART','LOLOTFOR');
      +","+cp_NullLink(cp_ToStrODBC("D"),'LOTTIART','LOFLSTAT');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'LOTTIART','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(0),'LOTTIART','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'LOTTIART','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'LOTTIART','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LO__NOTE),'LOTTIART','LO__NOTE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LOCODICE',this.w_LOCODICE,'LOCODESE',this.w_LOCODESE,'LOSERIAL',this.w_LOSERIAL,'LOCODART',this.w_LOCODART,'LODATCRE',this.w_LODATCRE,'LODATSCA',this.w_LODATSCA,'LONUMCOM',this.w_LONUMCOM,'LOTIPCON',"F",'LOCODCON',this.w_LOCODCON,'LOLOTFOR',this.w_LOLOTFOR,'LOFLSTAT',"D",'UTCC',i_CODUTE)
      insert into (i_cTable) (LOCODICE,LOCODESE,LOSERIAL,LOCODART,LODATCRE,LODATSCA,LONUMCOM,LOTIPCON,LOCODCON,LOLOTFOR,LOFLSTAT,UTCC,UTCV,UTDC,UTDV,LO__NOTE &i_ccchkf. );
         values (;
           this.w_LOCODICE;
           ,this.w_LOCODESE;
           ,this.w_LOSERIAL;
           ,this.w_LOCODART;
           ,this.w_LODATCRE;
           ,this.w_LODATSCA;
           ,this.w_LONUMCOM;
           ,"F";
           ,this.w_LOCODCON;
           ,this.w_LOLOTFOR;
           ,"D";
           ,i_CODUTE;
           ,0;
           ,SetInfoDate( g_CALUTD );
           ,cp_CharToDate("  -  -    ");
           ,this.w_LO__NOTE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LOTTIART'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
