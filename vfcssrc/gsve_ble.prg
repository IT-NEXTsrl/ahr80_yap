* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_ble                                                        *
*              Gestione maschera log errori                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_10]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-12-02                                                      *
* Last revis.: 2016-07-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_ble",oParentObject)
return(i_retval)

define class tgsve_ble as StdBatch
  * --- Local variables
  w_OK = .NULL.
  w_ANNULLA = .NULL.
  w_STAMPA = .NULL.
  w_BLOK = .NULL.
  w_RESOC = .NULL.
  w_GSVE_KLG = .NULL.
  w_Delta = 0
  w_Oldheight = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSVE_KLG (Maschera di Log errori lanciata da GSVE_BI2 e GSVE_BI3)
    *     Se non presenti Messaggi Bloccanti (da GSVE_BI3 sempre) rimpicciolisco la maschera
    *     GSVE_KLG, non visualizzo la variabile MESBLOK e sposto i bottoni Ok e Stampa pi� in alto
    this.w_OK = this.oParentObject.GETCTRL(ah_Msgformat("\<OK"))
    this.w_ANNULLA = this.oParentObject.GETCTRL(ah_Msgformat("\<Esci"))
    this.w_STAMPA = this.oParentObject.GETCTRL(ah_Msgformat("S\<tampa"))
    this.w_BLOK = this.oParentObject.GETCTRL("w_MESBLOK")
    this.w_RESOC = this.oParentObject.GETCTRL("w_RESOCON1")
    * --- Rimuovo la propriet� che non rende possibile il riposizionamento richiesto
    RemoveProperty(this.w_OK, "bAnchorFormDefault")
    RemoveProperty(this.w_ANNULLA, "bAnchorFormDefault")
    RemoveProperty(this.w_STAMPA, "bAnchorFormDefault")
    RemoveProperty(this.w_BLOK, "bAnchorFormDefault")
    RemoveProperty(this.w_RESOC, "bAnchorFormDefault")
    this.w_OK.Anchor = 0
    this.w_ANNULLA.Anchor = 0
    this.w_STAMPA.Anchor = 0
    this.w_BLOK.Anchor = 0
    this.w_RESOC.Anchor = 0
    this.w_GSVE_KLG = this.oParentObject
    this.w_Oldheight = this.w_GSVE_KLG.Height
    if Empty(this.oParentObject.w_RESOCON1)
      this.w_OK.Top = 225
      this.w_ANNULLA.Top = 225
      this.w_STAMPA.Top = 225
      this.w_GSVE_KLG.Height = 310
    endif
    if Empty(this.oParentObject.w_MESBLOK)
      this.w_OK.Top = 225
      this.w_ANNULLA.Top = 225
      this.w_STAMPA.Top = 225
      this.w_RESOC.Top = 20
      this.w_GSVE_KLG.Height = 310
    endif
    this.w_Delta = IIF(!EMPTY(this.oParentObject.w_RESOCON1) AND !EMPTY(this.oParentObject.w_MESBLOK),0,this.w_GSVE_KLG.Height - this.w_oldheight)
    this.w_GSVE_KLG.oPgFrm.Height = this.w_GSVE_KLG.oPgFrm.Height + this.w_Delta
    this.w_GSVE_KLG.oPgFrm.Page1.oPag.Height = this.w_GSVE_KLG.oPgFrm.Page1.oPag.Height + this.w_Delta
    this.w_GSVE_KLG.oPgFrm.Page1.oPag.StdHeight = this.w_GSVE_KLG.oPgFrm.Page1.oPag.Height
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
