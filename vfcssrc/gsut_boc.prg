* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_boc                                                        *
*              Recupera contatti Outlook                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-05-21                                                      *
* Last revis.: 2009-06-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_boc",oParentObject)
return(i_retval)

define class tgsut_boc as StdBatch
  * --- Local variables
  w_oOutlook = .NULL.
  w_oNameSpace = .NULL.
  w_oFolderContacts = .NULL.
  w_oContacts = .NULL.
  w_oContactItem = .NULL.
  w_OCCODICE = space(50)
  w_OCDESCRI = space(50)
  w_OC_EMAIL = space(254)
  w_OCTIPCON = space(1)
  w_OC__TIPO = space(1)
  w_OCPERSON = space(40)
  * --- WorkFile variables
  GSUT_BOC_idx=0
  ACTKEY_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Istanzio oggetto Outlook
    private L_bErr, L_OLDERR
    L_OLDERR = ON("ERROR")
    ON ERROR L_bErr=.T.
    L_bErr = .F.
    this.w_oOutlook = CreateObject("Outlook.Application")
    if L_bERR or VARTYPE(this.w_oOutlook)<>"O"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_oNameSpace = this.w_oOutlook.GetNamespace("MAPI")
    if L_bErr OR VARTYPE(this.w_oNameSpace)<>"O"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_oFolderContacts = this.w_oNameSpace.GetDefaultfolder(10)
    if L_bErr OR VARTYPE(this.w_oFolderContacts)<>"O"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_oContacts = this.w_oFolderContacts.Items
    if L_bErr OR VARTYPE(this.w_oContacts)<>"O"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Creo il cursore per l'output nella visual query
    * --- Create temporary table GSUT_BOC
    i_nIdx=cp_AddTableDef('GSUT_BOC') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ACTKEY_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ACTKEY_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"MOADDATT AS ANCODICE, MORAGCLI AS ANDESCRI, MOMES001 AS AN_EMAIL, MOMANUTE AS ANTIPCON, MOADDPRI AS TIPO, MORAGINS AS PERSON "," from "+i_cTable;
          +" where 1=0";
          )
    this.GSUT_BOC_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_oContactItem = this.w_oContacts.GetFirst()
    * --- Scansione dei contatti
    do while VARTYPE(this.w_oContactItem)=="O"
      this.w_OCCODICE = ALLTRIM(SYS(2007, this.w_oContactItem.EntryID))
      this.w_OCDESCRI = this.w_oContactItem.FullName()
      this.w_OC_EMAIL = this.w_oContactItem.Email1Address()
      this.w_OCTIPCON = "C"
      this.w_OC__TIPO = ah_MsgFormat("Outlook")
      this.w_OCPERSON = SPACE(40)
      if !EMPTY(this.w_OC_EMAIL)
        * --- Insert into GSUT_BOC
        i_nConn=i_TableProp[this.GSUT_BOC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GSUT_BOC_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.GSUT_BOC_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"ANCODICE"+",ANDESCRI"+",AN_EMAIL"+",ANTIPCON"+",TIPO"+",PERSON"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_OCCODICE),'GSUT_BOC','ANCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OCDESCRI),'GSUT_BOC','ANDESCRI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OC_EMAIL),'GSUT_BOC','AN_EMAIL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OCTIPCON),'GSUT_BOC','ANTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OC__TIPO),'GSUT_BOC','TIPO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_OCPERSON),'GSUT_BOC','PERSON');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'ANCODICE',this.w_OCCODICE,'ANDESCRI',this.w_OCDESCRI,'AN_EMAIL',this.w_OC_EMAIL,'ANTIPCON',this.w_OCTIPCON,'TIPO',this.w_OC__TIPO,'PERSON',this.w_OCPERSON)
          insert into (i_cTable) (ANCODICE,ANDESCRI,AN_EMAIL,ANTIPCON,TIPO,PERSON &i_ccchkf. );
             values (;
               this.w_OCCODICE;
               ,this.w_OCDESCRI;
               ,this.w_OC_EMAIL;
               ,this.w_OCTIPCON;
               ,this.w_OC__TIPO;
               ,this.w_OCPERSON;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      this.w_oContactItem = this.w_oContacts.GetNext()
    enddo
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_oContactItem = .NULL.
    this.w_oFolderContacts = .NULL.
    this.w_oNameSpace = .NULL.
    this.w_oOutlook = .NULL.
    ON ERROR &L_OLDERR
    i_retcode = 'stop'
    return
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*GSUT_BOC'
    this.cWorkTables[2]='ACTKEY'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
