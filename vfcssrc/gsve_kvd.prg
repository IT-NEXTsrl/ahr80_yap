* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kvd                                                        *
*              Variazione dati descrittivi del documento                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_198]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-23                                                      *
* Last revis.: 2012-05-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kvd",oParentObject))

* --- Class definition
define class tgsve_kvd as StdForm
  Top    = 8
  Left   = 12

  * --- Standard Properties
  Width  = 827
  Height = 515
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-05-16"
  HelpContextID=48902761
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=44

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  TIP_DOCU_IDX = 0
  cPrg = "gsve_kvd"
  cComment = "Variazione dati descrittivi del documento"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_FLVEAC = space(1)
  o_FLVEAC = space(1)
  w_DOCFOR = space(1)
  o_DOCFOR = space(1)
  w_CICLO = space(1)
  o_CICLO = space(1)
  w_CATEG1 = space(2)
  o_CATEG1 = space(2)
  w_CATEG3 = space(2)
  o_CATEG3 = space(2)
  w_CATEG2 = space(2)
  o_CATEG2 = space(2)
  w_CATEGO = space(2)
  o_CATEGO = space(2)
  w_ORIGINE = space(1)
  w_CAUDOC = space(5)
  w_TIPCONTO = space(1)
  w_CODCON = space(15)
  w_DESCRI = space(40)
  w_TIPCON = space(1)
  w_OBTEST = space(10)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_NUMINI = 0
  w_ALFINI = space(10)
  w_NUMFIN = 0
  w_ALFFIN = space(10)
  w_SERIALE = space(10)
  w_PARAME = space(3)
  w_DESTIP = space(35)
  w_PROWR = 0
  o_PROWR = 0
  w_NOTEPN = space(0)
  w_MEMO = space(0)
  w_NUMDO1 = 0
  w_ALFDO1 = space(10)
  w_DATDO1 = ctod('  /  /  ')
  w_MVSERIAL = space(10)
  w_MVCLADOC = space(2)
  o_MVCLADOC = space(2)
  w_OLDNOTE = space(30)
  w_FASE = 0
  w_OBJDOC = space(10)
  w_TIPO = space(1)
  w_CAUSALE = space(5)
  w_NUMRIF = 0
  w_NOTE = space(50)
  w_OLDNOTEPN = space(0)
  w_CATEG1 = space(2)
  w_CATEG3 = space(2)
  w_ANDES1 = space(40)
  w_DESTIPDO1 = space(35)
  w_CODCON1 = space(15)
  w_Zoom = .NULL.
  w_COLORE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsve_kvd
  * --- In questa A.M. viene inizializzata la Variabilei Globale del Form che Identifica
  * --- La Categoria Documento in base al Valore del Parametro.
  * --- tipo Documento
  pflveac = ''
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    local l_CICLO
    l_CICLO=this.oParentObject
    return(lower('GSVE_KVD'+IIF(Type('l_CICLO')='C',','+l_CICLO,'')))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kvdPag1","gsve_kvd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDOCFOR_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoom = this.oPgFrm.Pages(1).oPag.Zoom
    this.w_COLORE = this.oPgFrm.Pages(1).oPag.COLORE
    DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      this.w_COLORE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='TIP_DOCU'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_FLVEAC=space(1)
      .w_DOCFOR=space(1)
      .w_CICLO=space(1)
      .w_CATEG1=space(2)
      .w_CATEG3=space(2)
      .w_CATEG2=space(2)
      .w_CATEGO=space(2)
      .w_ORIGINE=space(1)
      .w_CAUDOC=space(5)
      .w_TIPCONTO=space(1)
      .w_CODCON=space(15)
      .w_DESCRI=space(40)
      .w_TIPCON=space(1)
      .w_OBTEST=space(10)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_NUMINI=0
      .w_ALFINI=space(10)
      .w_NUMFIN=0
      .w_ALFFIN=space(10)
      .w_SERIALE=space(10)
      .w_PARAME=space(3)
      .w_DESTIP=space(35)
      .w_PROWR=0
      .w_NOTEPN=space(0)
      .w_MEMO=space(0)
      .w_NUMDO1=0
      .w_ALFDO1=space(10)
      .w_DATDO1=ctod("  /  /  ")
      .w_MVSERIAL=space(10)
      .w_MVCLADOC=space(2)
      .w_OLDNOTE=space(30)
      .w_FASE=0
      .w_OBJDOC=space(10)
      .w_TIPO=space(1)
      .w_CAUSALE=space(5)
      .w_NUMRIF=0
      .w_NOTE=space(50)
      .w_OLDNOTEPN=space(0)
      .w_CATEG1=space(2)
      .w_CATEG3=space(2)
      .w_ANDES1=space(40)
      .w_DESTIPDO1=space(35)
      .w_CODCON1=space(15)
        .w_FLVEAC = this.oParentObject
        .w_DOCFOR = 'N'
        .w_CICLO = IIF(.w_DOCFOR='N',.w_FLVEAC,'A')
        .w_CATEG1 = 'XX'
        .w_CATEG3 = 'XX'
        .w_CATEG2 = 'DT'
        .w_CATEGO = IIF(.w_FLVEAC='A', .w_CATEG3,IIF(.w_DOCFOR='S',.w_CATEG2,.w_CATEG1))
          .DoRTCalc(8,8,.f.)
        .w_CAUDOC = IIF(.w_ORIGINE='D',.w_CAUDOC,SPACE(5))
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CAUDOC))
          .link_1_9('Full')
        endif
        .w_TIPCONTO = IIF(.w_FLVEAC='A' OR .w_DOCFOR='S','F','C')
        .w_CODCON = ''
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CODCON))
          .link_1_13('Full')
        endif
          .DoRTCalc(12,13,.f.)
        .w_OBTEST = i_INIDAT
        .w_DATINI = g_iniese
        .w_DATFIN = g_finese
        .w_NUMINI = 1
        .w_ALFINI = ''
        .w_NUMFIN = 999999999999999
        .w_ALFFIN = ''
      .oPgFrm.Page1.oPag.Zoom.Calculate()
          .DoRTCalc(21,21,.f.)
        .w_PARAME = .w_CICLO+.w_MVCLADOC
          .DoRTCalc(23,23,.f.)
        .w_PROWR = .w_Zoom.getVar('CPROWORD')
          .DoRTCalc(25,25,.f.)
        .w_MEMO = .w_Zoom.getVar('MVDESSUP')
          .DoRTCalc(27,28,.f.)
        .w_DATDO1 = cp_CharToDate('  -  -  ')
          .DoRTCalc(30,31,.f.)
        .w_OLDNOTE = .w_NOTE
      .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
      .oPgFrm.Page1.oPag.COLORE.Calculate('     ', RGB(0,0,0), RGB(255,255,0))
        .DoRTCalc(33,36,.f.)
        if not(empty(.w_CAUSALE))
          .link_1_65('Full')
        endif
        .w_NUMRIF = .w_Zoom.getVar('MVNUMRIF')
      .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
          .DoRTCalc(38,38,.f.)
        .w_OLDNOTEPN = .w_NOTE
        .w_CATEG1 = 'XX'
        .w_CATEG3 = 'XX'
        .DoRTCalc(42,44,.f.)
        if not(empty(.w_CODCON1))
          .link_1_77('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_FLVEAC<>.w_FLVEAC.or. .o_DOCFOR<>.w_DOCFOR
            .w_CICLO = IIF(.w_DOCFOR='N',.w_FLVEAC,'A')
        endif
        .DoRTCalc(4,6,.t.)
        if .o_DOCFOR<>.w_DOCFOR.or. .o_CATEG1<>.w_CATEG1.or. .o_CATEG2<>.w_CATEG2.or. .o_CATEG3<>.w_CATEG3
            .w_CATEGO = IIF(.w_FLVEAC='A', .w_CATEG3,IIF(.w_DOCFOR='S',.w_CATEG2,.w_CATEG1))
        endif
        .DoRTCalc(8,8,.t.)
        if .o_FLVEAC<>.w_FLVEAC.or. .o_CATEGO<>.w_CATEGO
            .w_CAUDOC = IIF(.w_ORIGINE='D',.w_CAUDOC,SPACE(5))
          .link_1_9('Full')
        endif
        if .o_FLVEAC<>.w_FLVEAC.or. .o_DOCFOR<>.w_DOCFOR
            .w_TIPCONTO = IIF(.w_FLVEAC='A' OR .w_DOCFOR='S','F','C')
        endif
        if .o_FLVEAC<>.w_FLVEAC
            .w_CODCON = ''
          .link_1_13('Full')
        endif
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .DoRTCalc(12,21,.t.)
        if .o_CICLO<>.w_CICLO.or. .o_MVCLADOC<>.w_MVCLADOC.or. .o_DOCFOR<>.w_DOCFOR
            .w_PARAME = .w_CICLO+.w_MVCLADOC
        endif
        .DoRTCalc(23,23,.t.)
            .w_PROWR = .w_Zoom.getVar('CPROWORD')
        .DoRTCalc(25,25,.t.)
        if .o_PROWR<>.w_PROWR
            .w_MEMO = .w_Zoom.getVar('MVDESSUP')
        endif
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .oPgFrm.Page1.oPag.COLORE.Calculate('     ', RGB(0,0,0), RGB(255,255,0))
        .DoRTCalc(27,35,.t.)
          .link_1_65('Full')
            .w_NUMRIF = .w_Zoom.getVar('MVNUMRIF')
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .DoRTCalc(38,43,.t.)
          .link_1_77('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_58.Calculate()
        .oPgFrm.Page1.oPag.COLORE.Calculate('     ', RGB(0,0,0), RGB(255,255,0))
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDOCFOR_1_2.visible=!this.oPgFrm.Page1.oPag.oDOCFOR_1_2.mHide()
    this.oPgFrm.Page1.oPag.oCATEG1_1_4.visible=!this.oPgFrm.Page1.oPag.oCATEG1_1_4.mHide()
    this.oPgFrm.Page1.oPag.oCATEG3_1_5.visible=!this.oPgFrm.Page1.oPag.oCATEG3_1_5.mHide()
    this.oPgFrm.Page1.oPag.oCATEG2_1_6.visible=!this.oPgFrm.Page1.oPag.oCATEG2_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oNOTEPN_1_36.visible=!this.oPgFrm.Page1.oPag.oNOTEPN_1_36.mHide()
    this.oPgFrm.Page1.oPag.oNOTE_1_68.visible=!this.oPgFrm.Page1.oPag.oNOTE_1_68.mHide()
    this.oPgFrm.Page1.oPag.oCATEG1_1_70.visible=!this.oPgFrm.Page1.oPag.oCATEG1_1_70.mHide()
    this.oPgFrm.Page1.oPag.oCATEG3_1_71.visible=!this.oPgFrm.Page1.oPag.oCATEG3_1_71.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_72.visible=!this.oPgFrm.Page1.oPag.oStr_1_72.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_73.visible=!this.oPgFrm.Page1.oPag.oStr_1_73.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_58.Event(cEvent)
      .oPgFrm.Page1.oPag.COLORE.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_67.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CAUDOC
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CAUDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CAUDOC))
          select TDTIPDOC,TDDESDOC,TDFLVEAC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCAUDOC_1_9'),i_cWhere,'GSVE_ATD',"Causali documenti",'GSVE1SVD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUDOC)
            select TDTIPDOC,TDDESDOC,TDFLVEAC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESTIP = NVL(_Link_.TDDESDOC,space(35))
      this.w_TIPO = NVL(_Link_.TDFLVEAC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUDOC = space(5)
      endif
      this.w_DESTIP = space(35)
      this.w_TIPO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPO=.w_CICLO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CAUDOC = space(5)
        this.w_DESTIP = space(35)
        this.w_TIPO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCONTO);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCONTO;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_13'),i_cWhere,'',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCONTO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCONTO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCONTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCONTO;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPCON = NVL(_Link_.ANTIPCON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCRI = space(40)
      this.w_TIPCON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUSALE
  func Link_1_65(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUSALE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUSALE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUSALE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUSALE)
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUSALE = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESTIPDO1 = NVL(_Link_.TDDESDOC,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CAUSALE = space(5)
      endif
      this.w_DESTIPDO1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUSALE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON1
  func Link_1_77(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON1);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCONTO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCONTO;
                       ,'ANCODICE',this.w_CODCON1)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON1 = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDES1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON1 = space(15)
      endif
      this.w_ANDES1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDOCFOR_1_2.RadioValue()==this.w_DOCFOR)
      this.oPgFrm.Page1.oPag.oDOCFOR_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATEG1_1_4.RadioValue()==this.w_CATEG1)
      this.oPgFrm.Page1.oPag.oCATEG1_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATEG3_1_5.RadioValue()==this.w_CATEG3)
      this.oPgFrm.Page1.oPag.oCATEG3_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATEG2_1_6.RadioValue()==this.w_CATEG2)
      this.oPgFrm.Page1.oPag.oCATEG2_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUDOC_1_9.value==this.w_CAUDOC)
      this.oPgFrm.Page1.oPag.oCAUDOC_1_9.value=this.w_CAUDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_13.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_13.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_14.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_14.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_19.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_19.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_21.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_21.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_1_22.value==this.w_NUMINI)
      this.oPgFrm.Page1.oPag.oNUMINI_1_22.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oALFINI_1_23.value==this.w_ALFINI)
      this.oPgFrm.Page1.oPag.oALFINI_1_23.value=this.w_ALFINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_1_24.value==this.w_NUMFIN)
      this.oPgFrm.Page1.oPag.oNUMFIN_1_24.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oALFFIN_1_25.value==this.w_ALFFIN)
      this.oPgFrm.Page1.oPag.oALFFIN_1_25.value=this.w_ALFFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIP_1_31.value==this.w_DESTIP)
      this.oPgFrm.Page1.oPag.oDESTIP_1_31.value=this.w_DESTIP
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTEPN_1_36.value==this.w_NOTEPN)
      this.oPgFrm.Page1.oPag.oNOTEPN_1_36.value=this.w_NOTEPN
    endif
    if not(this.oPgFrm.Page1.oPag.oMEMO_1_37.value==this.w_MEMO)
      this.oPgFrm.Page1.oPag.oMEMO_1_37.value=this.w_MEMO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDO1_1_47.value==this.w_NUMDO1)
      this.oPgFrm.Page1.oPag.oNUMDO1_1_47.value=this.w_NUMDO1
    endif
    if not(this.oPgFrm.Page1.oPag.oALFDO1_1_48.value==this.w_ALFDO1)
      this.oPgFrm.Page1.oPag.oALFDO1_1_48.value=this.w_ALFDO1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDO1_1_49.value==this.w_DATDO1)
      this.oPgFrm.Page1.oPag.oDATDO1_1_49.value=this.w_DATDO1
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUSALE_1_65.value==this.w_CAUSALE)
      this.oPgFrm.Page1.oPag.oCAUSALE_1_65.value=this.w_CAUSALE
    endif
    if not(this.oPgFrm.Page1.oPag.oNOTE_1_68.value==this.w_NOTE)
      this.oPgFrm.Page1.oPag.oNOTE_1_68.value=this.w_NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCATEG1_1_70.RadioValue()==this.w_CATEG1)
      this.oPgFrm.Page1.oPag.oCATEG1_1_70.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCATEG3_1_71.RadioValue()==this.w_CATEG3)
      this.oPgFrm.Page1.oPag.oCATEG3_1_71.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANDES1_1_74.value==this.w_ANDES1)
      this.oPgFrm.Page1.oPag.oANDES1_1_74.value=this.w_ANDES1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIPDO1_1_76.value==this.w_DESTIPDO1)
      this.oPgFrm.Page1.oPag.oDESTIPDO1_1_76.value=this.w_DESTIPDO1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON1_1_77.value==this.w_CODCON1)
      this.oPgFrm.Page1.oPag.oCODCON1_1_77.value=this.w_CODCON1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_TIPO=.w_CICLO)  and not(empty(.w_CAUDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUDOC_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DATINI)) or not(.w_DATINI<=.w_DATFIN or empty(.w_DATFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_19.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   ((empty(.w_DATFIN)) or not(.w_DATINI<=.w_DATFIN or empty(.w_DATFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_21.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_NUMINI<=.w_NUMFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMINI_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((empty(.w_ALFFIN) OR  (.w_ALFINI<=.w_ALFFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oALFINI_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not(.w_NUMINI<=.w_NUMFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMFIN_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((.w_ALFFIN>=.w_ALFINI) or (empty(.w_ALFINI)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oALFFIN_1_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggiore della serie finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLVEAC = this.w_FLVEAC
    this.o_DOCFOR = this.w_DOCFOR
    this.o_CICLO = this.w_CICLO
    this.o_CATEG1 = this.w_CATEG1
    this.o_CATEG3 = this.w_CATEG3
    this.o_CATEG2 = this.w_CATEG2
    this.o_CATEGO = this.w_CATEGO
    this.o_PROWR = this.w_PROWR
    this.o_MVCLADOC = this.w_MVCLADOC
    return

enddefine

* --- Define pages as container
define class tgsve_kvdPag1 as StdContainer
  Width  = 823
  height = 515
  stdWidth  = 823
  stdheight = 515
  resizeXpos=561
  resizeYpos=379
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDOCFOR_1_2 as StdCheck with uid="ODTJSCYAXV",rtseq=2,rtrep=.f.,left=306, top=49, caption="Documenti a fornitori",;
    ToolTipText = "Permette di selezionare anche i documenti a fornitore",;
    HelpContextID = 72372534,;
    cFormVar="w_DOCFOR", bObbl = .f. , nPag = 1;
    , TabStop=.f.;
   , bGlobalFont=.t.


  func oDOCFOR_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDOCFOR_1_2.GetRadio()
    this.Parent.oContained.w_DOCFOR = this.RadioValue()
    return .t.
  endfunc

  func oDOCFOR_1_2.SetRadio()
    this.Parent.oContained.w_DOCFOR=trim(this.Parent.oContained.w_DOCFOR)
    this.value = ;
      iif(this.Parent.oContained.w_DOCFOR=='S',1,;
      0)
  endfunc

  func oDOCFOR_1_2.mHide()
    with this.Parent.oContained
      return (UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE" or g_ACQU='S' )
    endwith
  endfunc


  add object oCATEG1_1_4 as StdCombo with uid="QDJUHAEQBR",rtseq=4,rtrep=.f.,left=88,top=49,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 221228250;
    , cFormVar="w_CATEG1",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ricevute fiscali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATEG1_1_4.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    iif(this.value =6,'RF',;
    space(2))))))))
  endfunc
  func oCATEG1_1_4.GetRadio()
    this.Parent.oContained.w_CATEG1 = this.RadioValue()
    return .t.
  endfunc

  func oCATEG1_1_4.SetRadio()
    this.Parent.oContained.w_CATEG1=trim(this.Parent.oContained.w_CATEG1)
    this.value = ;
      iif(this.Parent.oContained.w_CATEG1=='XX',1,;
      iif(this.Parent.oContained.w_CATEG1=='DI',2,;
      iif(this.Parent.oContained.w_CATEG1=='DT',3,;
      iif(this.Parent.oContained.w_CATEG1=='FA',4,;
      iif(this.Parent.oContained.w_CATEG1=='NC',5,;
      iif(this.Parent.oContained.w_CATEG1=='RF',6,;
      0))))))
  endfunc

  func oCATEG1_1_4.mHide()
    with this.Parent.oContained
      return (UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE" OR .w_FLVEAC<>'V' OR .w_DOCFOR='S')
    endwith
  endfunc


  add object oCATEG3_1_5 as StdCombo with uid="AZFEXNVYDI",rtseq=5,rtrep=.f.,left=88,top=49,width=130,height=21;
    , ToolTipText = "Categoria di appartenenza del documento selezionata";
    , HelpContextID = 187673818;
    , cFormVar="w_CATEG3",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATEG3_1_5.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'DT',;
    iif(this.value =4,'FA',;
    iif(this.value =5,'NC',;
    space(2)))))))
  endfunc
  func oCATEG3_1_5.GetRadio()
    this.Parent.oContained.w_CATEG3 = this.RadioValue()
    return .t.
  endfunc

  func oCATEG3_1_5.SetRadio()
    this.Parent.oContained.w_CATEG3=trim(this.Parent.oContained.w_CATEG3)
    this.value = ;
      iif(this.Parent.oContained.w_CATEG3=='XX',1,;
      iif(this.Parent.oContained.w_CATEG3=='DI',2,;
      iif(this.Parent.oContained.w_CATEG3=='DT',3,;
      iif(this.Parent.oContained.w_CATEG3=='FA',4,;
      iif(this.Parent.oContained.w_CATEG3=='NC',5,;
      0)))))
  endfunc

  func oCATEG3_1_5.mHide()
    with this.Parent.oContained
      return (UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE" OR .w_FLVEAC<>'A')
    endwith
  endfunc


  add object oCATEG2_1_6 as StdCombo with uid="MXEMUHNBIO",rtseq=6,rtrep=.f.,left=88,top=49,width=130,height=21;
    , ToolTipText = "Categoria di appartenenza del documento selezionata";
    , HelpContextID = 204451034;
    , cFormVar="w_CATEG2",RowSource=""+"Carichi a fornitore,"+"DDT a fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATEG2_1_6.RadioValue()
    return(iif(this.value =1,'DI',;
    iif(this.value =2,'DT',;
    space(2))))
  endfunc
  func oCATEG2_1_6.GetRadio()
    this.Parent.oContained.w_CATEG2 = this.RadioValue()
    return .t.
  endfunc

  func oCATEG2_1_6.SetRadio()
    this.Parent.oContained.w_CATEG2=trim(this.Parent.oContained.w_CATEG2)
    this.value = ;
      iif(this.Parent.oContained.w_CATEG2=='DI',1,;
      iif(this.Parent.oContained.w_CATEG2=='DT',2,;
      0))
  endfunc

  func oCATEG2_1_6.mHide()
    with this.Parent.oContained
      return (.w_DOCFOR<>'S')
    endwith
  endfunc

  add object oCAUDOC_1_9 as StdField with uid="HMSULLRIJD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CAUDOC", cQueryName = "CAUDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale del documento",;
    HelpContextID = 89088806,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=88, Top=77, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CAUDOC"

  func oCAUDOC_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUDOC_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUDOC_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCAUDOC_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'GSVE1SVD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oCAUDOC_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_CAUDOC
     i_obj.ecpSave()
  endproc

  add object oCODCON_1_13 as StdField with uid="TGLUNQDANV",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente/fornitore intestatario del documento",;
    HelpContextID = 5071142,;
   bGlobalFont=.t.,;
    Height=21, Width=130, Left=88, Top=105, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCONTO", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCONTO)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCONTO)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCRI_1_14 as StdField with uid="IBJUQRYNKK",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 192825142,;
   bGlobalFont=.t.,;
    Height=21, Width=236, Left=222, Top=105, InputMask=replicate('X',40)

  add object oDATINI_1_19 as StdField with uid="OEOUAEOZRW",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data di inizio selezione dei documenti",;
    HelpContextID = 189027126,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=88, Top=133

  func oDATINI_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_21 as StdField with uid="GQLQIWEQSK",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data di fine selezione dei documenti",;
    HelpContextID = 267473718,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=88, Top=161

  func oDATFIN_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN or empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oNUMINI_1_22 as StdField with uid="CGTAYVCLKJ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento iniziale selezionato",;
    HelpContextID = 189003734,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=244, Top=133, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMINI_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMINI<=.w_NUMFIN)
    endwith
    return bRes
  endfunc

  add object oALFINI_1_23 as StdField with uid="IKCHOCFECU",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ALFINI", cQueryName = "ALFINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Serie del documento iniziale selezionato",;
    HelpContextID = 188972550,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=375, Top=133, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oALFINI_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_ALFFIN) OR  (.w_ALFINI<=.w_ALFFIN)))
    endwith
    return bRes
  endfunc

  add object oNUMFIN_1_24 as StdField with uid="PPVPLBFEDU",rtseq=19,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento finale selezionato",;
    HelpContextID = 267450326,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=244, Top=161, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNUMFIN_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NUMINI<=.w_NUMFIN)
    endwith
    return bRes
  endfunc

  add object oALFFIN_1_25 as StdField with uid="SNGKDXOYPG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ALFFIN", cQueryName = "ALFFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggiore della serie finale",;
    ToolTipText = "Serie del documento finale selezionato",;
    HelpContextID = 267419142,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=375, Top=161, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oALFFIN_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ALFFIN>=.w_ALFINI) or (empty(.w_ALFINI)))
    endwith
    return bRes
  endfunc


  add object Zoom as cp_zoombox with uid="PKTVWVNFHA",left=1, top=238, width=484,height=230,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,cZoomFile="GSVE0KVD",bOptions=.f.,cTable="DOC_DETT",bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="",cZoomOnZoom="GSZM_BZM",bRetriveAllRows=.f.,;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 129094886


  add object oBtn_1_27 as StdButton with uid="YLNTCJDSAX",left=410, top=201, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 128019478;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      do GSVE_KV1 with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_27.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ORIGINE<>'D')
      endwith
    endif
  endfunc


  add object oBtn_1_30 as StdButton with uid="FRSHLUKCSC",left=761, top=49, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento associato alla riga selezionata";
    , HelpContextID = 233581338;
    , TabStop=.f.,Caption='\<Origine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_30.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_SERIALE, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc

  add object oDESTIP_1_31 as StdField with uid="HPOPOHEGCT",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESTIP", cQueryName = "DESTIP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 33507126,;
   bGlobalFont=.t.,;
    Height=21, Width=180, Left=166, Top=77, InputMask=replicate('X',35)

  add object oNOTEPN_1_36 as StdMemo with uid="VWBZQAQARC",rtseq=25,rtrep=.f.,;
    cFormVar = "w_NOTEPN", cQueryName = "NOTEPN",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Modifica le note del documento che verranno portate in contabilit�",;
    HelpContextID = 6316502,;
   bGlobalFont=.t.,;
    Height=39, Width=316, Left=497, Top=176

  func oNOTEPN_1_36.mHide()
    with this.Parent.oContained
      return (g_APPLICATION = "ADHOC REVOLUTION")
    endwith
  endfunc

  add object oMEMO_1_37 as StdMemo with uid="RENLUACIME",rtseq=26,rtrep=.f.,;
    cFormVar = "w_MEMO", cQueryName = "MEMO",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione supplementare",;
    HelpContextID = 43391034,;
   bGlobalFont=.t.,;
    Height=225, Width=316, Left=497, Top=235


  add object oBtn_1_39 as StdButton with uid="ULWOZFBKPU",left=714, top=466, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Aggiorna il documento con le modifiche apportate";
    , HelpContextID = 97722759;
    , caption='\<Conferma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      with this.Parent.oContained
        gsve_bwd(this.Parent.oContained,"AGGIO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_39.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc


  add object oBtn_1_41 as StdButton with uid="ABDPRCJXMN",left=768, top=466, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 41585338;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_42 as StdButton with uid="IKGAKYQXTI",left=63, top=466, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Elimina la riga selezionata del documento";
    , HelpContextID = 35245638;
    , TabStop=.f., Caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      with this.Parent.oContained
        gsve_bwd(this.Parent.oContained,"ELIMI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_42.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc


  add object oBtn_1_43 as StdButton with uid="NVMXAXHQAT",left=11, top=466, width=48,height=45,;
    CpPicture="bmp\INS_RAP.bmp", caption="", nPag=1;
    , ToolTipText = "Inserisce una riga descrittiva nel documento";
    , HelpContextID = 228751161;
    , TabStop=.f.,Caption='\<Inserisce';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      with this.Parent.oContained
        gsve_bwd(this.Parent.oContained,"INSER")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_43.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc

  add object oNUMDO1_1_47 as StdField with uid="EDHNNJPCXK",rtseq=27,rtrep=.f.,;
    cFormVar = "w_NUMDO1", cQueryName = "NUMDO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento selezionato",;
    HelpContextID = 212928554,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=540, Top=49

  add object oALFDO1_1_48 as StdField with uid="JXVZFOHZEM",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ALFDO1", cQueryName = "ALFDO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie del documento selezionato",;
    HelpContextID = 212959738,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=672, Top=49, InputMask=replicate('X',10)

  add object oDATDO1_1_49 as StdField with uid="HGNHBNUHDL",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DATDO1", cQueryName = "DATDO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del documento selezionato",;
    HelpContextID = 212905162,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=540, Top=76


  add object oObj_1_58 as cp_runprogram with uid="USRABMPJFG",left=545, top=524, width=275,height=19,;
    caption='gsve_bwd(MARCA)',;
   bGlobalFont=.t.,;
    prg="gsve_bwd('MARCA')",;
    cEvent = "w_MEMO Changed",;
    nPag=1;
    , HelpContextID = 184092490


  add object COLORE as cp_calclbl with uid="XWXGVBVQIE",left=199, top=469, width=34,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=1;
    , HelpContextID = 129094886

  add object oCAUSALE_1_65 as StdField with uid="NMZWMNMUHX",rtseq=36,rtrep=.f.,;
    cFormVar = "w_CAUSALE", cQueryName = "CAUSALE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale del documento selezionato",;
    HelpContextID = 226386726,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=540, Top=105, InputMask=replicate('X',5), cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CAUSALE"

  func oCAUSALE_1_65.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc


  add object oObj_1_67 as cp_runprogram with uid="AJMHPAQZVA",left=17, top=521, width=334,height=19,;
    caption='GSVE_BZM(w_SERIALE, w_PARAME)',;
   bGlobalFont=.t.,;
    prg="GSVE_BZM(w_SERIALE, w_PARAME)",;
    cEvent = "w_zoom selected",;
    nPag=1;
    , HelpContextID = 177795179

  add object oNOTE_1_68 as StdField with uid="PFUSFOTOVK",rtseq=38,rtrep=.f.,;
    cFormVar = "w_NOTE", cQueryName = "NOTE",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Modifica le note generali del documento",;
    HelpContextID = 44015146,;
   bGlobalFont=.t.,;
    Height=21, Width=316, Left=497, Top=176, InputMask=replicate('X',50)

  func oNOTE_1_68.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION")
    endwith
  endfunc


  add object oCATEG1_1_70 as StdCombo with uid="XLFSYIBLHL",rtseq=40,rtrep=.f.,left=88,top=49,width=130,height=21;
    , ToolTipText = "Categoria documento di selezione";
    , HelpContextID = 221228250;
    , cFormVar="w_CATEG1",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Ordini,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ordini previsionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATEG1_1_70.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'OR',;
    iif(this.value =4,'DT',;
    iif(this.value =5,'FA',;
    iif(this.value =6,'NC',;
    iif(this.value =7,'OP',;
    space(2)))))))))
  endfunc
  func oCATEG1_1_70.GetRadio()
    this.Parent.oContained.w_CATEG1 = this.RadioValue()
    return .t.
  endfunc

  func oCATEG1_1_70.SetRadio()
    this.Parent.oContained.w_CATEG1=trim(this.Parent.oContained.w_CATEG1)
    this.value = ;
      iif(this.Parent.oContained.w_CATEG1=='XX',1,;
      iif(this.Parent.oContained.w_CATEG1=='DI',2,;
      iif(this.Parent.oContained.w_CATEG1=='OR',3,;
      iif(this.Parent.oContained.w_CATEG1=='DT',4,;
      iif(this.Parent.oContained.w_CATEG1=='FA',5,;
      iif(this.Parent.oContained.w_CATEG1=='NC',6,;
      iif(this.Parent.oContained.w_CATEG1=='OP',7,;
      0)))))))
  endfunc

  func oCATEG1_1_70.mHide()
    with this.Parent.oContained
      return (!UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE" OR .w_FLVEAC<>'V' OR .w_DOCFOR='S')
    endwith
  endfunc


  add object oCATEG3_1_71 as StdCombo with uid="OZDVKSRXUC",rtseq=41,rtrep=.f.,left=88,top=49,width=130,height=21;
    , ToolTipText = "Categoria di appartenenza del documento selezionata";
    , HelpContextID = 187673818;
    , cFormVar="w_CATEG3",RowSource=""+"Tutti i documenti,"+"Documenti interni,"+"Ordini,"+"Doc. di trasporto,"+"Fatture,"+"Note di credito,"+"Ordini previsionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCATEG3_1_71.RadioValue()
    return(iif(this.value =1,'XX',;
    iif(this.value =2,'DI',;
    iif(this.value =3,'OR',;
    iif(this.value =4,'DT',;
    iif(this.value =5,'FA',;
    iif(this.value =6,'NC',;
    iif(this.value =7,'OP',;
    space(2)))))))))
  endfunc
  func oCATEG3_1_71.GetRadio()
    this.Parent.oContained.w_CATEG3 = this.RadioValue()
    return .t.
  endfunc

  func oCATEG3_1_71.SetRadio()
    this.Parent.oContained.w_CATEG3=trim(this.Parent.oContained.w_CATEG3)
    this.value = ;
      iif(this.Parent.oContained.w_CATEG3=='XX',1,;
      iif(this.Parent.oContained.w_CATEG3=='DI',2,;
      iif(this.Parent.oContained.w_CATEG3=='OR',3,;
      iif(this.Parent.oContained.w_CATEG3=='DT',4,;
      iif(this.Parent.oContained.w_CATEG3=='FA',5,;
      iif(this.Parent.oContained.w_CATEG3=='NC',6,;
      iif(this.Parent.oContained.w_CATEG3=='OP',7,;
      0)))))))
  endfunc

  func oCATEG3_1_71.mHide()
    with this.Parent.oContained
      return (!UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE" OR .w_FLVEAC<>'A')
    endwith
  endfunc

  add object oANDES1_1_74 as StdField with uid="QTYBCLUWQJ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_ANDES1", cQueryName = "ANDES1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 208707578,;
   bGlobalFont=.t.,;
    Height=21, Width=155, Left=661, Top=133, InputMask=replicate('X',40)

  add object oDESTIPDO1_1_76 as StdField with uid="DXGLKMWDVX",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESTIPDO1", cQueryName = "DESTIPDO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 33507989,;
   bGlobalFont=.t.,;
    Height=21, Width=225, Left=591, Top=105, InputMask=replicate('X',35)

  add object oCODCON1_1_77 as StdField with uid="SRCMGQAISW",rtseq=44,rtrep=.f.,;
    cFormVar = "w_CODCON1", cQueryName = "CODCON1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Intestatario del documento selezionato",;
    HelpContextID = 5071142,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=540, Top=133, InputMask=replicate('X',15), cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCONTO", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON1"

  func oCODCON1_1_77.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oStr_1_11 as StdString with uid="AUUWIYKKDR",Visible=.t., Left=23, Top=105,;
    Alignment=1, Width=61, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (.w_TIPCONTO='C')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="RKUSFJMUEB",Visible=.t., Left=24, Top=105,;
    Alignment=1, Width=60, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.w_TIPCONTO='F')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="ATVDFBYPXD",Visible=.t., Left=9, Top=77,;
    Alignment=1, Width=76, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="KROUETJXRP",Visible=.t., Left=24, Top=133,;
    Alignment=1, Width=61, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="RYLLYYAOED",Visible=.t., Left=40, Top=161,;
    Alignment=1, Width=45, Height=18,;
    Caption="a data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="ZOTHAXBXGW",Visible=.t., Left=497, Top=158,;
    Alignment=0, Width=38, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="JIPRXJTPCJ",Visible=.t., Left=185, Top=133,;
    Alignment=1, Width=55, Height=18,;
    Caption="Da num:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="TVMPXBRQTI",Visible=.t., Left=192, Top=161,;
    Alignment=1, Width=48, Height=18,;
    Caption="A num:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="RGJNSAKFNJ",Visible=.t., Left=363, Top=135,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="TQXDLWGEQQ",Visible=.t., Left=363, Top=164,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="ZGNURWUQXJ",Visible=.t., Left=497, Top=218,;
    Alignment=0, Width=126, Height=18,;
    Caption="Descr.supplementare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="UKLYXPUCZR",Visible=.t., Left=6, Top=14,;
    Alignment=0, Width=436, Height=15,;
    Caption="Parametri di selezione documento"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="OITGWXOZKU",Visible=.t., Left=489, Top=49,;
    Alignment=1, Width=50, Height=18,;
    Caption="Doc. n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="OOTSBCFEDK",Visible=.t., Left=664, Top=49,;
    Alignment=0, Width=7, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="DZNIVNJNTP",Visible=.t., Left=509, Top=76,;
    Alignment=1, Width=30, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="BKAOPWPHZR",Visible=.t., Left=486, Top=14,;
    Alignment=0, Width=280, Height=15,;
    Caption="Estremi documento di partenza"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="OOAUBIWOYB",Visible=.t., Left=25, Top=49,;
    Alignment=1, Width=60, Height=15,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="LUGNFBYLPF",Visible=.t., Left=238, Top=468,;
    Alignment=0, Width=126, Height=18,;
    Caption="Riga da eliminare"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="TUUBGZGLNE",Visible=.t., Left=478, Top=133,;
    Alignment=1, Width=61, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_72.mHide()
    with this.Parent.oContained
      return (.w_TIPCONTO='C')
    endwith
  endfunc

  add object oStr_1_73 as StdString with uid="QNKTJMCOKV",Visible=.t., Left=479, Top=133,;
    Alignment=1, Width=60, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_73.mHide()
    with this.Parent.oContained
      return (.w_TIPCONTO='F')
    endwith
  endfunc

  add object oStr_1_75 as StdString with uid="CVFYLBWZQB",Visible=.t., Left=463, Top=107,;
    Alignment=1, Width=76, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oBox_1_45 as StdBox with uid="XKFADLCDCH",left=9, top=33, width=449,height=1

  add object oBox_1_53 as StdBox with uid="PXBWCLDVXD",left=486, top=33, width=316,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kvd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
