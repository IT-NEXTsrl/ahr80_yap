* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bk1                                                        *
*              Inserimento massivo dati intestatari                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-13                                                      *
* Last revis.: 2014-11-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEVENT
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bk1",oParentObject,m.pEVENT)
return(i_retval)

define class tgsar_bk1 as StdBatch
  * --- Local variables
  pEVENT = space(1)
  w_PADRE = .NULL.
  w_OSTAMPA = space(1)
  w_OEMAIL = space(1)
  w_OEMPEC = space(1)
  w_OFAX = space(1)
  w_OCPZ = space(1)
  w_OWE = space(1)
  w_OPOSTEL = space(1)
  w_OCODCUC = space(2)
  w_OFLAPCA = space(1)
  w_OFLGCPZ = space(1)
  w_CONTA = 0
  w_CPROWNUM = 0
  w_ERRORE = space(250)
  w_OANCATFIN = space(5)
  w_OANCATDER = space(5)
  w_OANCATRAT = space(5)
  w_OANCHKFIR = space(1)
  w_CODCON = space(15)
  w_ZOOM = .NULL.
  w_ZOOM1 = .NULL.
  * --- WorkFile variables
  CONTRINT_idx=0
  CONTI_idx=0
  OFF_NOMI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- G generazione
    *     S seleziona deseleziona
    *     H rende Hide oggetto stampa
    this.w_PADRE = This.oParentObject
    if this.oParentObject.w_TIPAGG="D"
      this.w_ZOOM = this.w_PADRE.w_Zoom
    else
      this.w_ZOOM = this.w_PADRE.w_Zoom1
    endif
    do case
      case this.pEVENT="S"
        Update (this.w_ZOOM.cCursor) Set XCHK= IIF( this.oParentObject.w_SELEZI="D" , 0 , 1 )
      case this.pEVENT="G"
        * --- Aggiornamento archivi...
        this.w_ERRORE = ""
        * --- Try
        local bErr_038DBF98
        bErr_038DBF98=bTrsErr
        this.Try_038DBF98()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_errormsg("Errore in aggiornamento %1",48,, i_ERRMSG)
        endif
        bTrsErr=bTrsErr or bErr_038DBF98
        * --- End
      case this.pEVENT="H"
        this.oParentObject.w_SELEZI = "D"
        this.w_ZOOM = this.w_PADRE.w_Zoom
        this.w_ZOOM1 = this.w_PADRE.w_Zoom1
        if this.oParentObject.w_TIPAGG="D"
          select ( this.w_ZOOM.cCursor )
          DELETE FROM ( this.w_ZOOM.cCursor )
          this.w_zoom1.visible = .F.
          this.w_zoom.visible = .T.
          go top
          this.w_zoom.refresh()
        else
          select ( this.w_ZOOM1.cCursor )
          DELETE FROM ( this.w_ZOOM1.cCursor )
          this.w_zoom.visible = .F.
          this.w_zoom1.visible = .T.
          go top
          this.w_zoom1.refresh()
        endif
        this.w_PADRE.Notifyevent("Ricerca")     
        this.oParentObject.w_outputCombo.Visible = (this.oParentObject.w_TIPAGG="P")
    endcase
  endproc
  proc Try_038DBF98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_CONTA = 0
    * --- begin transaction
    cp_BeginTrs()
    Select (this.w_ZOOM.cCursor)
    Go Top
    Locate for XCHK=1
    do while Not Eof( this.w_ZOOM.cCursor )
      this.w_CODCON = ANCODICE
      this.w_CPROWNUM = Nvl( CP_MAX , 0 ) + 1
      if this.oParentObject.w_FLAPCA="S" AND not(Empty(this.oParentObject.w_MCTIPCAT)) and not Empty( this.oParentObject.w_MCDATATT ) and not Empty( this.oParentObject.w_MCDATSCA ) and this.oParentObject.w_TIPAGG="C"
        * --- Insert into CONTRINT
        i_nConn=i_TableProp[this.CONTRINT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTRINT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTRINT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"MCTIPINT"+",MCCODINT"+",CPROWNUM"+",MCTIPCAT"+",MCCODSIS"+",MCCODCAT"+",MCPERAPP"+",MCFLARID"+",MCDATATT"+",MCDATSCA"+",MC__PRNT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ANTIPCON),'CONTRINT','MCTIPINT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'CONTRINT','MCCODINT');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'CONTRINT','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCTIPCAT),'CONTRINT','MCTIPCAT');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCCODSIS),'CONTRINT','MCCODSIS');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCCODCAT),'CONTRINT','MCCODCAT');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCPERAPP),'CONTRINT','MCPERAPP');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCFLARID),'CONTRINT','MCFLARID');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCDATATT),'CONTRINT','MCDATATT');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCDATSCA),'CONTRINT','MCDATSCA');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MC__PRNT),'CONTRINT','MC__PRNT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'MCTIPINT',this.oParentObject.w_ANTIPCON,'MCCODINT',this.w_CODCON,'CPROWNUM',this.w_CPROWNUM,'MCTIPCAT',this.oParentObject.w_MCTIPCAT,'MCCODSIS',this.oParentObject.w_MCCODSIS,'MCCODCAT',this.oParentObject.w_MCCODCAT,'MCPERAPP',this.oParentObject.w_MCPERAPP,'MCFLARID',this.oParentObject.w_MCFLARID,'MCDATATT',this.oParentObject.w_MCDATATT,'MCDATSCA',this.oParentObject.w_MCDATSCA,'MC__PRNT',this.oParentObject.w_MC__PRNT)
          insert into (i_cTable) (MCTIPINT,MCCODINT,CPROWNUM,MCTIPCAT,MCCODSIS,MCCODCAT,MCPERAPP,MCFLARID,MCDATATT,MCDATSCA,MC__PRNT &i_ccchkf. );
             values (;
               this.oParentObject.w_ANTIPCON;
               ,this.w_CODCON;
               ,this.w_CPROWNUM;
               ,this.oParentObject.w_MCTIPCAT;
               ,this.oParentObject.w_MCCODSIS;
               ,this.oParentObject.w_MCCODCAT;
               ,this.oParentObject.w_MCPERAPP;
               ,this.oParentObject.w_MCFLARID;
               ,this.oParentObject.w_MCDATATT;
               ,this.oParentObject.w_MCDATSCA;
               ,this.oParentObject.w_MC__PRNT;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Dopo aver inserito verifico se il dato � valido...
        GSAR_BE2(this, this.oParentObject.w_ANTIPCON , this.w_CODCON )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if Not Empty( this.w_ERRORE )
          * --- Raise
          i_Error="Per le combinazioni inserite sono stati rilevati periodi di validit� sovrapposti"
          return
        endif
      endif
      if ISAHE()
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANFLAPCA,ANCHKSTA,ANCHKMAI,ANCHKPEC,ANCHKFAX,ANCHKWWP,ANCHKPTL,ANCHKCPZ,ANFLGCPZ,ANCODCUC,ANCATFIN,ANCATDER,ANCATRAT,ANCHKFIR"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_ANTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANFLAPCA,ANCHKSTA,ANCHKMAI,ANCHKPEC,ANCHKFAX,ANCHKWWP,ANCHKPTL,ANCHKCPZ,ANFLGCPZ,ANCODCUC,ANCATFIN,ANCATDER,ANCATRAT,ANCHKFIR;
            from (i_cTable) where;
                ANTIPCON = this.oParentObject.w_ANTIPCON;
                and ANCODICE = this.w_CODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OFLAPCA = NVL(cp_ToDate(_read_.ANFLAPCA),cp_NullValue(_read_.ANFLAPCA))
          this.w_OSTAMPA = NVL(cp_ToDate(_read_.ANCHKSTA),cp_NullValue(_read_.ANCHKSTA))
          this.w_OEMAIL = NVL(cp_ToDate(_read_.ANCHKMAI),cp_NullValue(_read_.ANCHKMAI))
          this.w_OEMPEC = NVL(cp_ToDate(_read_.ANCHKPEC),cp_NullValue(_read_.ANCHKPEC))
          this.w_OFAX = NVL(cp_ToDate(_read_.ANCHKFAX),cp_NullValue(_read_.ANCHKFAX))
          this.w_OWE = NVL(cp_ToDate(_read_.ANCHKWWP),cp_NullValue(_read_.ANCHKWWP))
          this.w_OPOSTEL = NVL(cp_ToDate(_read_.ANCHKPTL),cp_NullValue(_read_.ANCHKPTL))
          this.w_OCPZ = NVL(cp_ToDate(_read_.ANCHKCPZ),cp_NullValue(_read_.ANCHKCPZ))
          this.w_OFLGCPZ = NVL(cp_ToDate(_read_.ANFLGCPZ),cp_NullValue(_read_.ANFLGCPZ))
          this.w_OCODCUC = NVL(cp_ToDate(_read_.ANCODCUC),cp_NullValue(_read_.ANCODCUC))
          this.w_OANCATFIN = NVL(cp_ToDate(_read_.ANCATFIN),cp_NullValue(_read_.ANCATFIN))
          this.w_OANCATDER = NVL(cp_ToDate(_read_.ANCATDER),cp_NullValue(_read_.ANCATDER))
          this.w_OANCATRAT = NVL(cp_ToDate(_read_.ANCATRAT),cp_NullValue(_read_.ANCATRAT))
          this.w_OANCHKFIR = NVL(cp_ToDate(_read_.ANCHKFIR),cp_NullValue(_read_.ANCHKFIR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANFLAPCA,ANCHKSTA,ANCHKMAI,ANCHKPEC,ANCHKFAX,ANCHKWWP,ANCHKPTL,ANCHKCPZ,ANFLGCPZ,ANCODCUC,ANCHKFIR"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_ANTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANFLAPCA,ANCHKSTA,ANCHKMAI,ANCHKPEC,ANCHKFAX,ANCHKWWP,ANCHKPTL,ANCHKCPZ,ANFLGCPZ,ANCODCUC,ANCHKFIR;
            from (i_cTable) where;
                ANTIPCON = this.oParentObject.w_ANTIPCON;
                and ANCODICE = this.w_CODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OFLAPCA = NVL(cp_ToDate(_read_.ANFLAPCA),cp_NullValue(_read_.ANFLAPCA))
          this.w_OSTAMPA = NVL(cp_ToDate(_read_.ANCHKSTA),cp_NullValue(_read_.ANCHKSTA))
          this.w_OEMAIL = NVL(cp_ToDate(_read_.ANCHKMAI),cp_NullValue(_read_.ANCHKMAI))
          this.w_OEMPEC = NVL(cp_ToDate(_read_.ANCHKPEC),cp_NullValue(_read_.ANCHKPEC))
          this.w_OFAX = NVL(cp_ToDate(_read_.ANCHKFAX),cp_NullValue(_read_.ANCHKFAX))
          this.w_OWE = NVL(cp_ToDate(_read_.ANCHKWWP),cp_NullValue(_read_.ANCHKWWP))
          this.w_OPOSTEL = NVL(cp_ToDate(_read_.ANCHKPTL),cp_NullValue(_read_.ANCHKPTL))
          this.w_OCPZ = NVL(cp_ToDate(_read_.ANCHKCPZ),cp_NullValue(_read_.ANCHKCPZ))
          this.w_OFLGCPZ = NVL(cp_ToDate(_read_.ANFLGCPZ),cp_NullValue(_read_.ANFLGCPZ))
          this.w_OCODCUC = NVL(cp_ToDate(_read_.ANCODCUC),cp_NullValue(_read_.ANCODCUC))
          this.w_OANCHKFIR = NVL(cp_ToDate(_read_.ANCHKFIR),cp_NullValue(_read_.ANCHKFIR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      do case
        case this.oParentObject.w_TIPAGG= "C"
          * --- Attivo il flag contributi accessori..
          this.w_OFLAPCA = this.oParentObject.w_FLAPCA
        case this.oParentObject.w_TIPAGG= "P"
          this.w_OFLGCPZ = this.oParentObject.w_FLGCPZ
        case this.oParentObject.w_TIPAGG= "F"
          this.w_OCODCUC = this.oParentObject.w_CODCUC
        case this.oParentObject.w_TIPAGG= "D"
          this.w_OEMAIL = IIF(this.oParentObject.w_EMAIL="A",this.w_OEMAIL,this.oParentObject.w_EMAIL)
          this.w_OEMPEC = IIF(this.oParentObject.w_EMPEC="A",this.w_OEMPEC,this.oParentObject.w_EMPEC)
          this.w_OANCHKFIR = IIF(this.oParentObject.w_FIRDIG="A",this.w_OANCHKFIR,this.oParentObject.w_FIRDIG)
          this.w_OSTAMPA = IIF(this.oParentObject.w_STAMPA="A",this.w_OSTAMPA,this.oParentObject.w_STAMPA)
          this.w_OFAX = IIF(this.oParentObject.w_FAX="A",this.w_OFAX,this.oParentObject.w_FAX)
          this.w_OWE = IIF(this.oParentObject.w_WE="A",this.w_OWE,this.oParentObject.w_WE)
          this.w_OPOSTEL = IIF(this.oParentObject.w_POSTEL="A",this.w_OPOSTEL,this.oParentObject.w_POSTEL)
          this.w_OCPZ = IIF(this.oParentObject.w_CPZ="A",this.w_OCPZ,this.oParentObject.w_CPZ)
        case this.oParentObject.w_TIPAGG="E"
          this.w_OANCATFIN = this.oParentObject.w_ANCATFIN
          this.w_OANCATDER = this.oParentObject.w_ANCATDER
          this.w_OANCATRAT = this.oParentObject.w_ANCATRAT
      endcase
      this.w_OCPZ = IIF(this.w_OFLGCPZ="S", this.w_OCPZ, "N")
      if this.w_OANCHKFIR="S" AND this.w_OFLGCPZ<>"S" AND this.w_OEMAIL<>"S" AND this.w_OEMPEC<>"S"
        this.w_OANCHKFIR = "N"
      endif
      if ISAHE()
        * --- Write into CONTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANFLAPCA ="+cp_NullLink(cp_ToStrODBC(this.w_OFLAPCA),'CONTI','ANFLAPCA');
          +",ANCHKSTA ="+cp_NullLink(cp_ToStrODBC(this.w_OSTAMPA),'CONTI','ANCHKSTA');
          +",ANCHKMAI ="+cp_NullLink(cp_ToStrODBC(this.w_OEMAIL),'CONTI','ANCHKMAI');
          +",ANCHKPEC ="+cp_NullLink(cp_ToStrODBC(this.w_OEMPEC),'CONTI','ANCHKPEC');
          +",ANCHKFAX ="+cp_NullLink(cp_ToStrODBC(this.w_OFAX),'CONTI','ANCHKFAX');
          +",ANCHKWWP ="+cp_NullLink(cp_ToStrODBC(this.w_OWE),'CONTI','ANCHKWWP');
          +",ANCHKPTL ="+cp_NullLink(cp_ToStrODBC(this.w_OPOSTEL),'CONTI','ANCHKPTL');
          +",ANCHKCPZ ="+cp_NullLink(cp_ToStrODBC(this.w_OCPZ),'CONTI','ANCHKCPZ');
          +",ANFLGCPZ ="+cp_NullLink(cp_ToStrODBC(this.w_OFLGCPZ),'CONTI','ANFLGCPZ');
          +",ANCODCUC ="+cp_NullLink(cp_ToStrODBC(this.w_OCODCUC),'CONTI','ANCODCUC');
          +",ANCATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_OANCATFIN),'CONTI','ANCATFIN');
          +",ANCATDER ="+cp_NullLink(cp_ToStrODBC(this.w_OANCATDER),'CONTI','ANCATDER');
          +",ANCATRAT ="+cp_NullLink(cp_ToStrODBC(this.w_OANCATRAT),'CONTI','ANCATRAT');
          +",ANCHKFIR ="+cp_NullLink(cp_ToStrODBC(this.w_OANCHKFIR),'CONTI','ANCHKFIR');
              +i_ccchkf ;
          +" where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_ANTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                 )
        else
          update (i_cTable) set;
              ANFLAPCA = this.w_OFLAPCA;
              ,ANCHKSTA = this.w_OSTAMPA;
              ,ANCHKMAI = this.w_OEMAIL;
              ,ANCHKPEC = this.w_OEMPEC;
              ,ANCHKFAX = this.w_OFAX;
              ,ANCHKWWP = this.w_OWE;
              ,ANCHKPTL = this.w_OPOSTEL;
              ,ANCHKCPZ = this.w_OCPZ;
              ,ANFLGCPZ = this.w_OFLGCPZ;
              ,ANCODCUC = this.w_OCODCUC;
              ,ANCATFIN = this.w_OANCATFIN;
              ,ANCATDER = this.w_OANCATDER;
              ,ANCATRAT = this.w_OANCATRAT;
              ,ANCHKFIR = this.w_OANCHKFIR;
              &i_ccchkf. ;
           where;
              ANTIPCON = this.oParentObject.w_ANTIPCON;
              and ANCODICE = this.w_CODCON;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- Write into CONTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANFLAPCA ="+cp_NullLink(cp_ToStrODBC(this.w_OFLAPCA),'CONTI','ANFLAPCA');
          +",ANCHKSTA ="+cp_NullLink(cp_ToStrODBC(this.w_OSTAMPA),'CONTI','ANCHKSTA');
          +",ANCHKMAI ="+cp_NullLink(cp_ToStrODBC(this.w_OEMAIL),'CONTI','ANCHKMAI');
          +",ANCHKPEC ="+cp_NullLink(cp_ToStrODBC(this.w_OEMPEC),'CONTI','ANCHKPEC');
          +",ANCHKFAX ="+cp_NullLink(cp_ToStrODBC(this.w_OFAX),'CONTI','ANCHKFAX');
          +",ANCHKWWP ="+cp_NullLink(cp_ToStrODBC(this.w_OWE),'CONTI','ANCHKWWP');
          +",ANCHKPTL ="+cp_NullLink(cp_ToStrODBC(this.w_OPOSTEL),'CONTI','ANCHKPTL');
          +",ANCHKCPZ ="+cp_NullLink(cp_ToStrODBC(this.w_OCPZ),'CONTI','ANCHKCPZ');
          +",ANFLGCPZ ="+cp_NullLink(cp_ToStrODBC(this.w_OFLGCPZ),'CONTI','ANFLGCPZ');
          +",ANCODCUC ="+cp_NullLink(cp_ToStrODBC(this.w_OCODCUC),'CONTI','ANCODCUC');
          +",ANCHKFIR ="+cp_NullLink(cp_ToStrODBC(this.w_OANCHKFIR),'CONTI','ANCHKFIR');
              +i_ccchkf ;
          +" where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_ANTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                 )
        else
          update (i_cTable) set;
              ANFLAPCA = this.w_OFLAPCA;
              ,ANCHKSTA = this.w_OSTAMPA;
              ,ANCHKMAI = this.w_OEMAIL;
              ,ANCHKPEC = this.w_OEMPEC;
              ,ANCHKFAX = this.w_OFAX;
              ,ANCHKWWP = this.w_OWE;
              ,ANCHKPTL = this.w_OPOSTEL;
              ,ANCHKCPZ = this.w_OCPZ;
              ,ANFLGCPZ = this.w_OFLGCPZ;
              ,ANCODCUC = this.w_OCODCUC;
              ,ANCHKFIR = this.w_OANCHKFIR;
              &i_ccchkf. ;
           where;
              ANTIPCON = this.oParentObject.w_ANTIPCON;
              and ANCODICE = this.w_CODCON;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      if this.oParentObject.w_ANTIPCON="C" OR (IsAlt() AND this.oParentObject.w_ANTIPCON="F")
        * --- Aggiorno anche nominativo collegato
        * --- Write into OFF_NOMI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"NOCHKSTA ="+cp_NullLink(cp_ToStrODBC(this.w_OSTAMPA),'OFF_NOMI','NOCHKSTA');
          +",NOCHKMAI ="+cp_NullLink(cp_ToStrODBC(this.w_OEMAIL),'OFF_NOMI','NOCHKMAI');
          +",NOCHKPEC ="+cp_NullLink(cp_ToStrODBC(this.w_OEMPEC),'OFF_NOMI','NOCHKPEC');
          +",NOCHKFAX ="+cp_NullLink(cp_ToStrODBC(this.w_OFAX),'OFF_NOMI','NOCHKFAX');
              +i_ccchkf ;
          +" where ";
              +"NOCODCLI = "+cp_ToStrODBC(this.w_CODCON);
              +" and NOTIPCLI = "+cp_ToStrODBC(this.oParentObject.w_ANTIPCON);
                 )
        else
          update (i_cTable) set;
              NOCHKSTA = this.w_OSTAMPA;
              ,NOCHKMAI = this.w_OEMAIL;
              ,NOCHKPEC = this.w_OEMPEC;
              ,NOCHKFAX = this.w_OFAX;
              &i_ccchkf. ;
           where;
              NOCODCLI = this.w_CODCON;
              and NOTIPCLI = this.oParentObject.w_ANTIPCON;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      this.w_CONTA = this.w_CONTA + 1
      Select (this.w_ZOOM.cCursor) 
 Continue
    enddo
    * --- commit
    cp_EndTrs(.t.)
    * --- Aggiorno lo zoom...
    this.oParentObject.w_FLAPCA = iif(this.oParentObject.w_TIPAGG="C","S","N")
    this.w_PADRE.Notifyevent("Ricerca")     
    * --- Segnalo il numero di record aggiornati
    ah_errormsg("Aggiornati %1 intestatari ",48,, Alltrim( Str ( this.w_CONTA ) ) )
    this.oParentObject.w_SELEZI = "D"
    if this.oParentObject.w_TIPAGG="D"
      this.oParentObject.w_FIRDIG = "A"
      this.oParentObject.w_EMAIL = "A"
      this.oParentObject.w_EMPEC = "A"
      this.oParentObject.w_STAMPA = "A"
      this.oParentObject.w_FAX = "A"
      this.oParentObject.w_WE = "A"
      this.oParentObject.w_POSTEL = "A"
      this.oParentObject.w_CPZ = "A"
      this.oParentObject.w_FLGCPZ = "A"
    endif
    return


  proc Init(oParentObject,pEVENT)
    this.pEVENT=pEVENT
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CONTRINT'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='OFF_NOMI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEVENT"
endproc
