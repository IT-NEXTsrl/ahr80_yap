* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar1bgp                                                        *
*              Zoom gruppi apparnenenza risorsa                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-07-10                                                      *
* Last revis.: 2012-01-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOPE,pCODRIS,pCODGRU,pVALOBJ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar1bgp",oParentObject,m.pTIPOPE,m.pCODRIS,m.pCODGRU,m.pVALOBJ)
return(i_retval)

define class tgsar1bgp as StdBatch
  * --- Local variables
  pTIPOPE = space(3)
  pCODRIS = space(5)
  pCODGRU = space(5)
  pVALOBJ = space(10)
  w_PADRE = .NULL.
  w_OBJCTRL = .NULL.
  w_OSOURCE = .NULL.
  w_CODRIS = space(5)
  w_DPCODICE = space(5)
  w_DPDESCRI = space(30)
  w_NUMMARK = 0
  w_bIsBodyCtrl = .f.
  * --- WorkFile variables
  STRU_RIS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.pTIPOPE = IIF( VARTYPE(this.pTIPOPE)<>"C", "", this.pTIPOPE )
    do case
      case EMPTY(this.pTIPOPE)
        this.w_PADRE = this.oParentObject
        this.w_CODRIS = ALLTRIM(this.pCODRIS)
        this.w_DPCODICE = ""
        vx_exec("gsar_bgp.vzm",this)
        if !EMPTY(this.w_DPCODICE)
          this.w_bIsBodyCtrl = .F.
          this.w_OBJCTRL = this.w_PADRE.GetCtrl( "w_"+ALLTRIM(this.pVALOBJ) )
          if VARTYPE(this.w_OBJCTRL) <>"O"
            this.w_OBJCTRL = this.w_PADRE.GetBodyCtrl( "w_"+ALLTRIM(this.pVALOBJ) )
            this.w_bIsBodyCtrl = .T.
          endif
          if VARTYPE(this.w_OBJCTRL) =="O" AND this.w_OBJCTRL.mCond()
            this.w_OSOURCE.xKey( 1 ) = this.w_DPCODICE
            if this.w_bIsBodyCtrl
              this.w_NUMMARK = this.w_PADRE.nMarkpos
              this.w_PADRE.nMarkpos = 0
            endif
            this.w_OBJCTRL.ecpDrop(this.w_OSOURCE)     
            if this.w_bIsBodyCtrl
              this.w_PADRE.nMarkpos = this.w_NUMMARK
            endif
          endif
        endif
      case this.pTIPOPE=="CHK"
        this.bUpdateParentObject = .F.
        * --- Read from STRU_RIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.STRU_RIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STRU_RIS_idx,2],.t.,this.STRU_RIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" STRU_RIS where ";
                +"SRCODICE = "+cp_ToStrODBC(this.pCODGRU);
                +" and SRTIPCOL = "+cp_ToStrODBC("P");
                +" and SRRISCOL = "+cp_ToStrODBC(this.pCODRIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                SRCODICE = this.pCODGRU;
                and SRTIPCOL = "P";
                and SRRISCOL = this.pCODRIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        i_retcode = 'stop'
        i_retval = i_Rows > 0
        return
    endcase
    if UPPER(OPARENTOBJECT.CLASS)="TGSAG_KPR" OR UPPER(OPARENTOBJECT.CLASS)="TGSAG_KPP"
      * --- Per evitare che alla mcalc del batch riscatti il messaggio vuoi salvare le modifiche pi�
      *     volte nel caso in cui il codice gruppo � chiave
      *     Aggiorno la o_ con la w_
      oparentobject.o_GRUPART=oparentobject.w_GRUPART
    endif
  endproc


  proc Init(oParentObject,pTIPOPE,pCODRIS,pCODGRU,pVALOBJ)
    this.pTIPOPE=pTIPOPE
    this.pCODRIS=pCODRIS
    this.pCODGRU=pCODGRU
    this.pVALOBJ=pVALOBJ
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='STRU_RIS'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOPE,pCODRIS,pCODGRU,pVALOBJ"
endproc
