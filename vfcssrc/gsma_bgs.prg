* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bgs                                                        *
*              Lancia programma collegato mag                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_8]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-16                                                      *
* Last revis.: 2013-04-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO,pCODI
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bgs",oParentObject,m.pTIPO,m.pCODI)
return(i_retval)

define class tgsma_bgs as StdBatch
  * --- Local variables
  pTIPO = space(1)
  pCODI = space(20)
  w_CONTO = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia un Programma (da GSMA_AAR, GSMA_KCA)
    * --- chiave...
    PUBLIC i_ExactSearch
    i_ExactSearch = .T.
    do case
      case this.pTIPO="L"
        * --- Gestione Listini Prezzi
        this.w_CONTO = GSMA_MLS()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_CONTO.bSec1)
          Ah_ErrorMsg("Impossibile aprire la funzionalitą selezionata!",48,"")
          i_retcode = 'stop'
          return
        endif
        this.w_CONTO.ecpFilter()     
        this.w_CONTO.w_LICODART = this.pCODI
      case this.pTIPO="K"
        * --- Gestione Chiavi Articoli
        this.w_CONTO = GSMA_ACA()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_CONTO.bSec1)
          Ah_ErrorMsg("Impossibile aprire la funzionalitą selezionata!",48,"")
          i_retcode = 'stop'
          return
        endif
        this.w_CONTO.ecpFilter()     
        this.w_CONTO.w_CACODICE = this.pCODI
    endcase
    * --- carico il record
    this.w_CONTO.ecpSave()     
    i_ExactSearch = .F.
  endproc


  proc Init(oParentObject,pTIPO,pCODI)
    this.pTIPO=pTIPO
    this.pCODI=pCODI
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO,pCODI"
endproc
