* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg1bvt                                                        *
*              Query calcolo quadro vt                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39][VRS_12]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-08                                                      *
* Last revis.: 2008-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_INIANNO,w_FINANNO,w_ANNDIC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg1bvt",oParentObject,m.w_INIANNO,m.w_FINANNO,m.w_ANNDIC)
return(i_retval)

define class tgscg1bvt as StdBatch
  * --- Local variables
  w_INIANNO = ctod("  /  /  ")
  w_FINANNO = ctod("  /  /  ")
  w_ANNDIC = space(4)
  * --- WorkFile variables
  TAB_MISU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch eseguito nella query GSCG3BST utilizzata nel calcolo totalizzatori IVa
    *     le variabili definite non son altro che i parametri passati attraverso la query
    GSCG_BVT(this,"F")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  proc Init(oParentObject,w_INIANNO,w_FINANNO,w_ANNDIC)
    this.w_INIANNO=w_INIANNO
    this.w_FINANNO=w_FINANNO
    this.w_ANNDIC=w_ANNDIC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TAB_MISU'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_INIANNO,w_FINANNO,w_ANNDIC"
endproc
