* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bpz                                                        *
*              Calcola prezzi da calprzli                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-30                                                      *
* Last revis.: 2014-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODSER,pCODCON,pTIPCON,pCODLIS,pQTAMOV,pCODVAL,pDATINI,pUNIMIS,pCCONTR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bpz",oParentObject,m.pCODSER,m.pCODCON,m.pTIPCON,m.pCODLIS,m.pQTAMOV,m.pCODVAL,m.pDATINI,m.pUNIMIS,m.pCCONTR)
return(i_retval)

define class tgsar_bpz as StdBatch
  * --- Local variables
  pCODSER = space(20)
  pCODCON = space(15)
  pTIPCON = space(1)
  pCODLIS = space(5)
  pQTAMOV = 0
  pCODVAL = space(5)
  pDATINI = ctod("  /  /  ")
  pUNIMIS = space(3)
  pCCONTR = space(15)
  w_UNIMIS = space(3)
  w_ARUNIMIS = space(3)
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_UNMIS3 = space(3)
  w_UNMIS2 = space(3)
  w_MOLTI3 = 0
  w_FLUSEP = space(1)
  w_QTAUM1 = 0
  w_FLFRAZ1 = space(1)
  w_MODUM2 = space(2)
  w_CODART = space(20)
  w_PROG = space(3)
  w_MOLTIP = 0
  w_CATART = space(5)
  w_CATCLI = space(5)
  w_CATCOM = space(3)
  w_CHKTEMP = space(1)
  w_QTAUM2 = 0
  w_QTAUM3 = 0
  w_CLUNIMIS = space(3)
  w_CALPRZ = 0
  w_PRZVAC = space(1)
  w_FLPREV = space(1)
  w_FLQRIO = space(1)
  w_SCOLIS = space(1)
  w_CODIVA = space(5)
  w_PERIVA = 0
  w_PREZUM = space(1)
  w_QTALIS = 0
  w_LIPREZZO = 0
  * --- WorkFile variables
  OFF_NOMI_idx=0
  LISTINI_idx=0
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  UNIMIS_idx=0
  VOCIIVA_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Codice di ricerca
    * --- Inteatatario
    * --- Tipo Inteatatario
    * --- listino
    * --- Quantit� movimentata
    * --- Valuta
    * --- Data listino
    * --- Unit� di misura
    this.pDATINI = IIF(VARTYPE(this.pDATINI)="T",TTOD(this.pDATINI),this.pDATINI)
    * --- Data iniziale
    this.pCCONTR = IIF(VARTYPE(this.pCCONTR)="C", this.pCCONTR, " " )
    * --- Codice contratto di vendita
    this.w_PROG = "V" + IIF(Empty(this.w_FLQRIO)," ",this.w_FLQRIO) + IIF(Empty(this.w_FLPREV)," ",this.w_FLPREV)+" "
    DECLARE ARRCALC (16,1)
    * --- Azzero l'Array che verr� riempito dalla Funzione
     
 ARRCALC(1)=0
    * --- Se parametro='Z' allora non devo calcolare il contratto...
    *     Al posto del Gruppo merceologico passo 'XXXXXX' per eliminazione variabile GRUMER dal Body dei documenti.
    *     In questo caso rileggo il gruppo merceologico all'interno di CALPRZLI
    *     Stessa cosa per Categoria Sconti maggiorazioni dell'articolo
    * --- Read from KEY_ARTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP"+;
        " from "+i_cTable+" KEY_ARTI where ";
            +"CACODICE = "+cp_ToStrODBC(this.pCODSER);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP;
        from (i_cTable) where;
            CACODICE = this.pCODSER;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
      this.w_UNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
      this.w_OPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
      this.w_MOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARFLUSEP,ARMOLTIP,AROPERAT,ARPREZUM,ARUNMIS1,ARUNMIS2,ARCODIVA,ARCATSCM"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARFLUSEP,ARMOLTIP,AROPERAT,ARPREZUM,ARUNMIS1,ARUNMIS2,ARCODIVA,ARCATSCM;
        from (i_cTable) where;
            ARCODART = this.w_CODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLUSEP = NVL(cp_ToDate(_read_.ARFLUSEP),cp_NullValue(_read_.ARFLUSEP))
      this.w_MOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
      this.w_OPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
      this.w_PREZUM = NVL(cp_ToDate(_read_.ARPREZUM),cp_NullValue(_read_.ARPREZUM))
      this.w_ARUNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
      this.w_UNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
      this.w_CODIVA = NVL(cp_ToDate(_read_.ARCODIVA),cp_NullValue(_read_.ARCODIVA))
      this.w_CATART = NVL(cp_ToDate(_read_.ARCATSCM),cp_NullValue(_read_.ARCATSCM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_CATCOM = " "
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCATSCM,ANCATCOM"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.pTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.pCODCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCATSCM,ANCATCOM;
        from (i_cTable) where;
            ANTIPCON = this.pTIPCON;
            and ANCODICE = this.pCODCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CATCLI = NVL(cp_ToDate(_read_.ANCATSCM),cp_NullValue(_read_.ANCATSCM))
      this.w_CATCOM = NVL(cp_ToDate(_read_.ANCATCOM),cp_NullValue(_read_.ANCATCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_UNIMIS = iif(Empty(this.pUNIMIS) ,this.w_ARUNIMIS,this.pUNIMIS)
    * --- Read from UNIMIS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.UNIMIS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "UMFLTEMP,UMFLFRAZ,UMMODUM2"+;
        " from "+i_cTable+" UNIMIS where ";
            +"UMCODICE = "+cp_ToStrODBC(this.w_UNIMIS);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        UMFLTEMP,UMFLFRAZ,UMMODUM2;
        from (i_cTable) where;
            UMCODICE = this.w_UNIMIS;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CHKTEMP = NVL(cp_ToDate(_read_.UMFLTEMP),cp_NullValue(_read_.UMFLTEMP))
      this.w_FLFRAZ1 = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
      this.w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_QTAUM1 = CALQTA(this.pQTAMOV,this.w_UNIMIS,this.w_UNMIS2,this.w_OPERAT, this.w_MOLTIP, this.w_FLUSEP, this.w_FLFRAZ1, this.w_MODUM2, " ", this.w_UNMIS3, this.w_OPERA3, this.w_MOLTI3, IIF(this.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", 10, .F.), IIF(this.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", "D", .F.))
    * --- Read from LISTINI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.LISTINI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LSFLSCON"+;
        " from "+i_cTable+" LISTINI where ";
            +"LSCODLIS = "+cp_ToStrODBC(this.pCODLIS);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LSFLSCON;
        from (i_cTable) where;
            LSCODLIS = this.pCODLIS;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SCOLIS = NVL(cp_ToDate(_read_.LSFLSCON),cp_NullValue(_read_.LSFLSCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from VOCIIVA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VOCIIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IVPERIVA"+;
        " from "+i_cTable+" VOCIIVA where ";
            +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IVPERIVA;
        from (i_cTable) where;
            IVCODIVA = this.w_CODIVA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_QTAUM3 = CALQTA( this.w_QTAUM1, this.w_UNMIS3, Space(3),IIF( this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF( this.w_OPERA3="/","*","/"), this.w_MOLTI3, IIF(this.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", 10, .F.), IIF(this.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", "D", .F.))
    this.w_QTAUM2 = CALQTA( this.w_QTAUM1, this.w_UNMIS2, this.w_UNMIS2,IIF( this.w_OPERAT="/","*","/"), this.w_MOLTIP, "", "", "", , this.w_UNMIS3, IIF( this.w_OPERA3="/","*","/"), this.w_MOLTI3, IIF(this.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", 10, .F.), IIF(this.w_FLFRAZ1="S" OR this.w_CHKTEMP="S", "D", .F.))
    DIMENSION pArrUm[9]
    pArrUm [1] = this.w_PREZUM 
 pArrUm [2] = this.w_UNIMIS 
 pArrUm [3] = this.pQTAMOV 
 pArrUm [4] = this.w_ARUNIMIS 
 pArrUm [5] = this.w_QTAUM1 
 pArrUm [6] =this.w_UNMIS2 
 pArrUm [7] = this.w_QTAUM2 
 pArrUm [8] = this.w_UNMIS3 
 pArrUm[9] = this.w_QTAUM3
    this.w_CALPRZ = CalPrzli( this.pCCONTR , this.pTIPCON , this.pCODLIS , this.w_CODART , "XXXXXX" , this.w_QTAUM1 , this.pCODVAL , g_PERVAL, this.pDATINI , this.w_CATCLI , this.w_CATART, this.pCODVAL, this.pCODCON,this.w_CATCOM, "N", this.w_SCOLIS, 0,this.w_PROG, @ARRCALC, this.w_PRZVAC, "V" ,"N", @pArrUm)
    if VARTYPE(L_ArrRet)<>"U"
      ACOPY(ARRCALC, L_ArrRet)
    endif
    this.w_LIPREZZO = ARRCALC(5)
    if this.w_UNIMIS<>ARRCALC(16) AND NOT EMPTY(ARRCALC(16))
      this.w_QTALIS = IIF(ARRCALC(16)=this.w_ARUNIMIS,this.w_QTAUM1, IIF(ARRCALC(16)=this.w_UNMIS2, this.w_QTAUM2, this.w_QTAUM3))
      this.w_LIPREZZO = cp_Round(CALMMPZ(ARRCALC(5), this.pQTAMOV, this.w_QTALIS, "N", this.w_PERIVA, g_perpul),g_perpul)
    endif
    i_retcode = 'stop'
    i_retval = this.w_LIPREZZO
    return
  endproc


  proc Init(oParentObject,pCODSER,pCODCON,pTIPCON,pCODLIS,pQTAMOV,pCODVAL,pDATINI,pUNIMIS,pCCONTR)
    this.pCODSER=pCODSER
    this.pCODCON=pCODCON
    this.pTIPCON=pTIPCON
    this.pCODLIS=pCODLIS
    this.pQTAMOV=pQTAMOV
    this.pCODVAL=pCODVAL
    this.pDATINI=pDATINI
    this.pUNIMIS=pUNIMIS
    this.pCCONTR=pCCONTR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='OFF_NOMI'
    this.cWorkTables[2]='LISTINI'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='UNIMIS'
    this.cWorkTables[6]='VOCIIVA'
    this.cWorkTables[7]='CONTI'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODSER,pCODCON,pTIPCON,pCODLIS,pQTAMOV,pCODVAL,pDATINI,pUNIMIS,pCCONTR"
endproc
