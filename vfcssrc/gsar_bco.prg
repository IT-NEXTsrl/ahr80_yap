* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bco                                                        *
*              Calcola numero confezioni                                       *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_19]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-22                                                      *
* Last revis.: 2007-11-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pACTION
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bco",oParentObject,m.pACTION)
return(i_retval)

define class tgsar_bco as StdBatch
  * --- Local variables
  pACTION = space(1)
  w_PADRE = .NULL.
  w_PC = 0
  w_MESS = space(200)
  w_CODART = space(20)
  w_CODICE = space(20)
  w_UNIMIS = space(3)
  w_UNIMIS1 = space(3)
  w_UNIMIS2 = space(3)
  w_UNIMIS3 = space(3)
  w_CONF = space(3)
  w_TIPRIG = space(1)
  w_NUMCOL = 0
  w_QTAMOV = 0
  w_ROWORD = 0
  w_OK = .f.
  w_RESOCON1 = space(0)
  w_MESBLOK = space(0)
  w_ANNULLA = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricalcola Numero Confezioni (da GSVE_MDV, GSAC_MDV, GSOR_MDV)
    *     pACTION = 'R' lavora sulla riga corrente
    *     pACTION = 'D' lavora su tutte le righe
    DECLARE ARRAYRET(9,1)
    * --- La variabile w_OK segnala un eventuale problema nel calcolo del numero
    *     di confezioni
    this.w_OK = .T.
    this.w_PADRE = this.oParentObject
    * --- Decido se devo lavorare su tutto il transitorio o su solo la riga corrente
    do case
      case this.pACTION = "R"
        * --- Aggiorno solo la riga corrente
        do case
          case Lower ( this.w_PADRE.class ) = "tgsar_mpg"
            this.w_CODART = this.w_PADRE.w_CODART
            this.w_CODICE = this.oParentObject.w_GPCODICE
            this.w_UNIMIS = this.oParentObject.w_GPUNIMIS
            this.w_UNIMIS1 = this.oParentObject.w_UNMIS1
            this.w_UNIMIS2 = this.oParentObject.w_UNMIS2
            this.w_UNIMIS3 = this.oParentObject.w_UNMIS3
            this.w_CONF = this.oParentObject.w_CODCONF
            this.w_TIPRIG = this.w_PADRE.w_TIPRIG
            this.w_NUMCOL = this.oParentObject.w_GPNUMCOL
            this.w_QTAMOV = this.oParentObject.w_GPQTAMOV
            this.w_ROWORD = this.oParentObject.w_CPROWORD
          otherwise
            * --- Lower ( w_OBJ_GEST.class ) = 'tgsve_mdv' Or Lower ( w_OBJ_GEST.class ) = 'tgsor_mdv' or Lower ( w_OBJ_GEST.class ) = 'tgsac_mdv'
            this.w_CODART = this.oParentObject.w_MVCODART
            this.w_CODICE = this.oParentObject.w_MVCODICE
            this.w_UNIMIS = this.oParentObject.w_MVUNIMIS
            this.w_UNIMIS1 = this.oParentObject.w_UNMIS1
            this.w_UNIMIS2 = this.oParentObject.w_UNMIS2
            this.w_UNIMIS3 = this.oParentObject.w_UNMIS3
            this.w_CONF = this.oParentObject.w_CODCONF
            this.w_TIPRIG = this.oParentObject.w_MVTIPRIG
            this.w_NUMCOL = this.oParentObject.w_MVNUMCOL
            this.w_QTAMOV = this.oParentObject.w_MVQTAMOV
            this.w_ROWORD = this.oParentObject.w_CPROWORD
        endcase
        * --- Ricalcolo Numero Confezioni
        this.w_OK = CalNumCo(this.w_CODICE, this.w_CODART, this.w_QTAMOV, this.w_UNIMIS, this.w_UNIMIS1, this.w_UNIMIS2, this.w_UNIMIS3, this.w_TIPRIG, this.w_ROWORD, this.w_CONF, this.w_NUMCOL, @ARRAYRET)
        if this.w_OK = 0 OR this.w_OK = 2
          * --- Aggiorno il documento
          do case
            case Lower ( this.w_PADRE.class ) = "tgsar_mpg"
              this.oParentObject.w_GPNUMCOL = ARRAYRET[1]
            otherwise
              * --- Lower ( w_OBJ_GEST.class ) = 'tgsve_mdv' Or Lower ( w_OBJ_GEST.class ) = 'tgsor_mdv' or Lower ( w_OBJ_GEST.class ) = 'tgsac_mdv'
              this.oParentObject.w_MVNUMCOL = ARRAYRET[1]
          endcase
        endif
        do case
          case this.w_OK = 1
            * --- Visualizzo il messaggio di errore
            ah_ErrorMsg("%1","stop","",ARRAYRET[8])
          case this.w_OK = 2
            * --- Visualizzo il messaggio di warning
            ah_ErrorMsg("%1","!","",ARRAYRET[9])
        endcase
      otherwise
        * --- Aggiorno tutto il transitorio
        this.w_PADRE = this.oParentObject
        this.w_PADRE.MarkPos()     
        this.w_PADRE.FirstRow()     
        do while Not this.w_PADRE.Eof_Trs()
          if this.w_PADRE.FullRow()
            this.w_PADRE.SetRow()     
            * --- Ricalcolo Numero Confezioni
            this.w_OK = CalNumCo(this.oParentObject.w_MVCODICE, this.oParentObject.w_MVCODART, this.oParentObject.w_MVQTAMOV, this.oParentObject.w_MVUNIMIS, this.oParentObject.w_UNMIS1, this.oParentObject.w_UNMIS2, this.oParentObject.w_UNMIS3, this.oParentObject.w_MVTIPRIG, this.oParentObject.w_CPROWORD, this.oParentObject.w_CODCONF, this.oParentObject.w_MVNUMCOL, @ARRAYRET)
            if this.w_OK = 0 OR this.w_OK = 2
              * --- Aggiorno il documento
              this.w_NUMCOL = ARRAYRET[1]
              this.w_PADRE.Set("w_MVNUMCOL" , this.w_NUMCOL)     
            endif
            if this.w_OK <> 0
              * --- Concateno i messaggi di errore, utilizzerò GSVE_KLE
              do case
                case this.w_OK = 1
                  * --- Concateno il messaggio bloccante
                  this.w_MESBLOK = this.w_MESBLOK + CHR(10) + CHR(13) + ARRAYRET[8]
                case this.w_OK = 2
                  * --- Concateno il messaggio di warning
                  this.w_RESOCON1 = this.w_RESOCON1 + CHR(10) + CHR(13) + ARRAYRET[9]
              endcase
            endif
          endif
          this.w_PADRE.NextRow()     
        enddo
        this.w_PADRE.RePos()     
        * --- Visualizzo gli errori
        if NOT EMPTY( this.w_RESOCON1 ) OR NOT EMPTY( this.w_MESBLOK )
          do GSVE_KLG with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pACTION)
    this.pACTION=pACTION
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pACTION"
endproc
