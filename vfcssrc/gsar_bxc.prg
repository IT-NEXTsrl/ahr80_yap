* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bxc                                                        *
*              Aggiorna CAP/province                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-03-10                                                      *
* Last revis.: 2009-08-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPOPE,pCODCAP,pLOCALI,pCODPRO,pDESPRO,pGIUDIZIALE,pCODCOM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bxc",oParentObject,m.pTIPOPE,m.pCODCAP,m.pLOCALI,m.pCODPRO,m.pDESPRO,m.pGIUDIZIALE,m.pCODCOM)
return(i_retval)

define class tgsar_bxc as StdBatch
  * --- Local variables
  pTIPOPE = space(1)
  pCODCAP = space(9)
  pLOCALI = space(30)
  pCODPRO = space(2)
  pDESPRO = space(30)
  pGIUDIZIALE = .f.
  pCODCOM = space(4)
  w_CODCAP = space(9)
  w_LOCALITA = space(50)
  w_FRAZIONE = space(50)
  w_PROVINCIA = space(2)
  w_DESCRIZIONE = space(30)
  w_APPCODICE = space(9)
  w_CODCOM = space(4)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Zoom su campi provincia e campi localita'
    *     - pTIPOPE: scelta tra zoom CAP (C) o Province (P)
    *     - pCODCAP: nome variabile CAP
    *     - pLOCALI: nome variabile LOCALITA
    *     - pCODPRO: nome variabile PROVINCIA
    *     - pDESPRO: nome variabile DESCRIZIONE PROVINCIA
    * --- Scelta tra zoom CAP (C) o Province (P)
    Local pPARENTPROP
    * --- Inizializza variabili locali
    this.w_CODCAP = SPACE(9)
    this.w_LOCALITA = SPACE(50)
    this.w_FRAZIONE = SPACE(50)
    this.w_PROVINCIA = SPACE(2)
    this.w_DESCRIZIONE = SPACE(30)
    * --- Esegue ...
    do case
      case this.pTIPOPE="C"
        if this.pGIUDIZIALE
          vx_exec("QUERY\GSAR_BXG.VZM",this)
        else
          vx_exec("QUERY\GSAR_BXC.VZM",this)
        endif
      case this.pTIPOPE="P"
        vx_exec("QUERY\GSAR_BXP.VZM",this)
      case this.pTIPOPE="T"
        vx_exec("QUERY\GSAR_BXT.VZM",this)
    endcase
    * --- Passa risultato al parent
    if (this.pTIPOPE$"P -T" and NOT EMPTY(NVL(this.w_PROVINCIA,""))) or (this.pTIPOPE="C" and NOT EMPTY(NVL(this.w_CODCAP,"")))
      * --- CAP
      if vartype(this.pCODCAP)="C" and not(Empty(this.pCODCAP))
        pPARENTPROP = "this.oParentObject"+alltrim(this.pCODCAP)
        if g_APPLICATION = "ad hoc ENTERPRISE"
          &pPARENTPROP = this.w_CODCAP
        else
          &pPARENTPROP = LEFT(ALLTRIM(this.w_CODCAP),8)
        endif
      endif
      * --- Localit�
      if vartype(this.pLOCALI)="C" and not(Empty(this.pLOCALI))
        pPARENTPROP = "this.oParentObject"+alltrim(this.pLOCALI)
        &pPARENTPROP = LEFT(IIF(EMPTY(NVL(this.w_FRAZIONE," ")),this.w_LOCALITA,ALLTRIM(this.w_FRAZIONE)+" - ")+this.w_LOCALITA,30)
      endif
      * --- Codice comune per il calcolo del codice fiscale
      if vartype(this.pCODCOM)="C" and not(Empty(this.pCODCOM))
        pPARENTPROP = "this.oParentObject"+alltrim(this.pCODCOM)
        &pPARENTPROP = this.w_CODCOM
      endif
      * --- Provincia
      if vartype(this.pCODPRO)="C" and not(Empty(this.pCODPRO))
        pPARENTPROP = "this.oParentObject"+alltrim(this.pCODPRO)
        &pPARENTPROP = this.w_PROVINCIA
      endif
      * --- Descrizione Provincia
      if vartype(this.pDESPRO)="C" and not(Empty(this.pDESPRO))
        pPARENTPROP = "this.oParentObject"+alltrim(this.pDESPRO)
        &pPARENTPROP = this.w_DESCRIZIONE
      endif
      * --- Verfico se la gestione che ha lanciato la routine � un detail
      if cp_getEntityType(this.oParentobject.cprg)="Detail File"
        * --- Controllo se il parametro vale "C" - Zoom direttamente sul codice CAP oppure
        *     P - Provincie
        if this.pTIPOPE="C"
          this.w_APPCODICE = STRTRAN(this.pCODCAP,".","")
        else
          this.w_APPCODICE = STRTRAN(this.pCODPRO,".","")
        endif
        LOCAL obj
        obj=this.oParentobject.GetBodyCtrl(this.w_APPCODICE)
        if varTYPE(obj)<>"O"
          obj=this.oParentobject.GetCtrl(this.w_APPCODICE)
        endif
        if varTYPE(obj)="O" AND lower(obj.Class)="stdtrsfield"
           this.oParentobject.SetupdateRow()
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pTIPOPE,pCODCAP,pLOCALI,pCODPRO,pDESPRO,pGIUDIZIALE,pCODCOM)
    this.pTIPOPE=pTIPOPE
    this.pCODCAP=pCODCAP
    this.pLOCALI=pLOCALI
    this.pCODPRO=pCODPRO
    this.pDESPRO=pDESPRO
    this.pGIUDIZIALE=pGIUDIZIALE
    this.pCODCOM=pCODCOM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPOPE,pCODCAP,pLOCALI,pCODPRO,pDESPRO,pGIUDIZIALE,pCODCOM"
endproc
