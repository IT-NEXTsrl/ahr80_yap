* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kiz                                                        *
*              Importazione documenti archiviati in archivio zip               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-06-17                                                      *
* Last revis.: 2011-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kiz",oParentObject))

* --- Class definition
define class tgsut_kiz as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 783
  Height = 202
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-10-21"
  HelpContextID=1490071
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  cpusers_IDX = 0
  cPrg = "gsut_kiz"
  cComment = "Importazione documenti archiviati in archivio zip"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_IDUTEEXP = 0
  w_IDUTEDES = space(20)
  w_FLTYPIMP = space(1)
  o_FLTYPIMP = space(1)
  w_FILEDEST = space(254)
  o_FILEDEST = space(254)
  w_FLSUBFLD = .F.
  w_FILESELEZ = space(254)
  w_FL_SAVE = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kizPag1","gsut_kiz",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oIDUTEEXP_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='cpusers'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_IDUTEEXP=0
      .w_IDUTEDES=space(20)
      .w_FLTYPIMP=space(1)
      .w_FILEDEST=space(254)
      .w_FLSUBFLD=.f.
      .w_FILESELEZ=space(254)
      .w_FL_SAVE=.f.
      .w_IDUTEEXP=oParentObject.w_IDUTEEXP
      .w_FLTYPIMP=oParentObject.w_FLTYPIMP
      .w_FILEDEST=oParentObject.w_FILEDEST
      .w_FLSUBFLD=oParentObject.w_FLSUBFLD
      .w_FL_SAVE=oParentObject.w_FL_SAVE
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_IDUTEEXP))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_FLTYPIMP = 'F'
          .DoRTCalc(4,4,.f.)
        .w_FLSUBFLD = .F.
          .DoRTCalc(6,6,.f.)
        .w_FL_SAVE = .T.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_IDUTEEXP=.w_IDUTEEXP
      .oParentObject.w_FLTYPIMP=.w_FLTYPIMP
      .oParentObject.w_FILEDEST=.w_FILEDEST
      .oParentObject.w_FLSUBFLD=.w_FLSUBFLD
      .oParentObject.w_FL_SAVE=.w_FL_SAVE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_FILEDEST<>.w_FILEDEST
          .Calculate_QZVJYPNGGJ()
        endif
        if .o_FLTYPIMP<>.w_FLTYPIMP
          .Calculate_CDOPBJFFHD()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_RLXSMAJPQK()
    with this
          * --- Seleziona sorgenti importazione
          .w_FILESELEZ = IIF(.w_FLTYPIMP='C', cp_GetDir("",ah_MsgFormat("Cartella da analizzare:"), ah_MsgFormat("Selezionare cartella da analizzare")) , GETFILE(ah_MsgFormat("Archivio 7z (*.7z):7z"), "","",0,ah_Msgformat("Selezionare percorso e file archivio zip da importare")))
          .w_FILEDEST = IIF(!EMPTY(.w_FILESELEZ), .w_FILESELEZ, .w_FILEDEST)
    endwith
  endproc
  proc Calculate_QZVJYPNGGJ()
    with this
          * --- Verifica estensione
          .w_FILEDEST = IIF( .w_FLTYPIMP<>'C', FORCEEXT(.w_FILEDEST,"7z"), .w_FILEDEST)
    endwith
  endproc
  proc Calculate_CDOPBJFFHD()
    with this
          * --- Elimino percorso/file da importare su cambio tipo
          .w_FILEDEST = ""
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oFLSUBFLD_1_9.visible=!this.oPgFrm.Page1.oPag.oFLSUBFLD_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("SelDir")
          .Calculate_RLXSMAJPQK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=IDUTEEXP
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_IDUTEEXP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_IDUTEEXP);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_IDUTEEXP)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_IDUTEEXP) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oIDUTEEXP_1_1'),i_cWhere,'',"Lista utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_IDUTEEXP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_IDUTEEXP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_IDUTEEXP)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_IDUTEEXP = NVL(_Link_.code,0)
      this.w_IDUTEDES = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_IDUTEEXP = 0
      endif
      this.w_IDUTEDES = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_IDUTEEXP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oIDUTEEXP_1_1.value==this.w_IDUTEEXP)
      this.oPgFrm.Page1.oPag.oIDUTEEXP_1_1.value=this.w_IDUTEEXP
    endif
    if not(this.oPgFrm.Page1.oPag.oIDUTEDES_1_2.value==this.w_IDUTEDES)
      this.oPgFrm.Page1.oPag.oIDUTEDES_1_2.value=this.w_IDUTEDES
    endif
    if not(this.oPgFrm.Page1.oPag.oFLTYPIMP_1_5.RadioValue()==this.w_FLTYPIMP)
      this.oPgFrm.Page1.oPag.oFLTYPIMP_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILEDEST_1_7.value==this.w_FILEDEST)
      this.oPgFrm.Page1.oPag.oFILEDEST_1_7.value=this.w_FILEDEST
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSUBFLD_1_9.RadioValue()==this.w_FLSUBFLD)
      this.oPgFrm.Page1.oPag.oFLSUBFLD_1_9.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLTYPIMP = this.w_FLTYPIMP
    this.o_FILEDEST = this.w_FILEDEST
    return

enddefine

* --- Define pages as container
define class tgsut_kizPag1 as StdContainer
  Width  = 779
  height = 202
  stdWidth  = 779
  stdheight = 202
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oIDUTEEXP_1_1 as StdField with uid="EAWLUZLOBQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_IDUTEEXP", cQueryName = "IDUTEEXP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 104835626,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=216, Top=55, cSayPict='"@Z 9999"', cGetPict='"@Z 9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_IDUTEEXP"

  func oIDUTEEXP_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oIDUTEEXP_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oIDUTEEXP_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oIDUTEEXP_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lista utenti",'',this.parent.oContained
  endproc

  add object oIDUTEDES_1_2 as StdField with uid="ODECPVBVBQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_IDUTEDES", cQueryName = "IDUTEDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 121612839,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=310, Top=55, InputMask=replicate('X',20)


  add object oFLTYPIMP_1_5 as StdCombo with uid="XWITELUWTU",rtseq=3,rtrep=.f.,left=216,top=88,width=237,height=21;
    , ToolTipText = "Selezionare il tipo di importazione da effettuare, analisi di una cartella indicata o importazione di un singolo archivio zip";
    , HelpContextID = 242568614;
    , cFormVar="w_FLTYPIMP",RowSource=""+"cartella indicata,"+"file indicato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLTYPIMP_1_5.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oFLTYPIMP_1_5.GetRadio()
    this.Parent.oContained.w_FLTYPIMP = this.RadioValue()
    return .t.
  endfunc

  func oFLTYPIMP_1_5.SetRadio()
    this.Parent.oContained.w_FLTYPIMP=trim(this.Parent.oContained.w_FLTYPIMP)
    this.value = ;
      iif(this.Parent.oContained.w_FLTYPIMP=='C',1,;
      iif(this.Parent.oContained.w_FLTYPIMP=='F',2,;
      0))
  endfunc

  add object oFILEDEST_1_7 as StdField with uid="ZRUSRSHNUW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FILEDEST", cQueryName = "FILEDEST",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso o file da importare",;
    HelpContextID = 106902870,;
   bGlobalFont=.t.,;
    Height=21, Width=529, Left=216, Top=113, InputMask=replicate('X',254)


  add object oBtn_1_8 as StdButton with uid="EGSLJKOSDU",left=751, top=114, width=18,height=17,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il file o il percorso da importare";
    , HelpContextID = 1691094;
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      this.parent.oContained.NotifyEvent("SelDir")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFLSUBFLD_1_9 as StdCheck with uid="QTNCVONEBI",rtseq=5,rtrep=.f.,left=216, top=141, caption="Analizza anche sottocartelle",;
    ToolTipText = "Se attivo analizza anche le sottocartelle del percorso indicato",;
    HelpContextID = 177290650,;
    cFormVar="w_FLSUBFLD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSUBFLD_1_9.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLSUBFLD_1_9.GetRadio()
    this.Parent.oContained.w_FLSUBFLD = this.RadioValue()
    return .t.
  endfunc

  func oFLSUBFLD_1_9.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLSUBFLD==.T.,1,;
      0)
  endfunc

  func oFLSUBFLD_1_9.mHide()
    with this.Parent.oContained
      return (.w_FLTYPIMP<>'C')
    endwith
  endfunc


  add object oBtn_1_11 as StdButton with uid="YCTTRQWDEB",left=670, top=148, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per continuare con l'esportazione dei documenti selezionati";
    , HelpContextID = 1518822;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_FILEDEST) AND (IIF(.w_FLTYPIMP='C', !EMPTY(JUSTPATH(ADDBS(.w_FILEDEST))) , !EMPTY(JUSTFNAME(FORCEEXT(.w_FILEDEST,"7z"))) )) AND !EMPTY(.w_IDUTEEXP))
      endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="TAPXNQTUEA",left=721, top=148, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 8807494;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="KQUXKWUGMH",Visible=.t., Left=11, Top=58,;
    Alignment=1, Width=200, Height=18,;
    Caption="Utente proprietario esportazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="UDYKJOJCAZ",Visible=.t., Left=43, Top=88,;
    Alignment=1, Width=164, Height=18,;
    Caption="Analizza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="CNUYRQGMCH",Visible=.t., Left=18, Top=117,;
    Alignment=1, Width=193, Height=18,;
    Caption="File zip da importare:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (.w_FLTYPIMP='C')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="NDDXRKYSMI",Visible=.t., Left=18, Top=117,;
    Alignment=1, Width=193, Height=18,;
    Caption="Cartella da analizzare:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_FLTYPIMP<>'C')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="ERQAJSAICQ",Visible=.t., Left=13, Top=10,;
    Alignment=0, Width=743, Height=18,;
    Caption="Indicare l'utente proprietario dei file esportati e la cartella da cui importare gli archivi in formato zip conosciuti."  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="RHQJJSHEWP",Visible=.t., Left=13, Top=29,;
    Alignment=0, Width=743, Height=18,;
    Caption="In alternativa � possibile indicare un singolo archivio zip da importare"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kiz','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
