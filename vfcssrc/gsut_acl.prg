* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_acl                                                        *
*              Classi librerie allegati                                        *
*                                                                              *
*      Author: 116                                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [101] [VRS_45]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-08-23                                                      *
* Last revis.: 2013-09-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_acl"))

* --- Class definition
define class tgsut_acl as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 749
  Height = 288+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-09-16"
  HelpContextID=158776471
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  PROMCLAS_IDX = 0
  TIP_ALLE_IDX = 0
  CLA_ALLE_IDX = 0
  cFile = "PROMCLAS"
  cKeySelect = "CDCODCLA"
  cKeyWhere  = "CDCODCLA=this.w_CDCODCLA"
  cKeyWhereODBC = '"CDCODCLA="+cp_ToStrODBC(this.w_CDCODCLA)';

  cKeyWhereODBCqualified = '"PROMCLAS.CDCODCLA="+cp_ToStrODBC(this.w_CDCODCLA)';

  cPrg = "gsut_acl"
  cComment = "Classi librerie allegati"
  icon = "anag.ico"
  cAutoZoom = 'gsut_acl'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CDCODCLA = space(15)
  w_CDDESCLA = space(50)
  w_CDMODALL = space(1)
  o_CDMODALL = space(1)
  w_CDRIFDEF = space(1)
  o_CDRIFDEF = space(1)
  w_CDTIPRAG = space(1)
  w_CDCAMRAG = space(50)
  w_CDPATSTD = space(254)
  o_CDPATSTD = space(254)
  w_CDPUBWEB = space(1)
  w_CDNOMFIL = space(200)
  w_CDERASEF = space(1)
  w_CDTIPALL = space(5)
  o_CDTIPALL = space(5)
  w_CDCLAALL = space(5)
  o_CDCLAALL = space(5)
  w_DESTIPALL = space(40)
  w_DESCLALL = space(40)
  w_CDCOMTIP = space(1)
  w_CDCOMCLA = space(1)
  w_CDRIFTAB = space(20)
  * --- Area Manuale = Declare Variables
  * --- gsut_acl
  * Definizioni variabili per il controllo sulla modifica dei parametri
  * di definizione del path 'base' di archiviazione allegati.
  oldpath=' '
  addcla=' '
  addtip=' '
  oldmod=' '
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PROMCLAS','gsut_acl')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_aclPag1","gsut_acl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Classe libreria allegati")
      .Pages(1).HelpContextID = 24577861
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCDCODCLA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='TIP_ALLE'
    this.cWorkTables[2]='CLA_ALLE'
    this.cWorkTables[3]='PROMCLAS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PROMCLAS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PROMCLAS_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CDCODCLA = NVL(CDCODCLA,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PROMCLAS where CDCODCLA=KeySet.CDCODCLA
    *
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PROMCLAS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PROMCLAS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PROMCLAS '
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CDCODCLA',this.w_CDCODCLA  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESTIPALL = space(40)
        .w_DESCLALL = space(40)
        .w_CDCODCLA = NVL(CDCODCLA,space(15))
        .w_CDDESCLA = NVL(CDDESCLA,space(50))
        .w_CDMODALL = NVL(CDMODALL,space(1))
        .w_CDRIFDEF = NVL(CDRIFDEF,space(1))
        .w_CDTIPRAG = NVL(CDTIPRAG,space(1))
        .w_CDCAMRAG = NVL(CDCAMRAG,space(50))
        .w_CDPATSTD = NVL(CDPATSTD,space(254))
        .w_CDPUBWEB = NVL(CDPUBWEB,space(1))
        .w_CDNOMFIL = NVL(CDNOMFIL,space(200))
        .w_CDERASEF = NVL(CDERASEF,space(1))
        .w_CDTIPALL = NVL(CDTIPALL,space(5))
          if link_1_19_joined
            this.w_CDTIPALL = NVL(TACODICE119,NVL(this.w_CDTIPALL,space(5)))
            this.w_DESTIPALL = NVL(TADESCRI119,space(40))
          else
          .link_1_19('Load')
          endif
        .w_CDCLAALL = NVL(CDCLAALL,space(5))
          if link_1_20_joined
            this.w_CDCLAALL = NVL(TACODCLA120,NVL(this.w_CDCLAALL,space(5)))
            this.w_DESCLALL = NVL(TACLADES120,space(40))
          else
          .link_1_20('Load')
          endif
        .w_CDCOMTIP = NVL(CDCOMTIP,space(1))
        .w_CDCOMCLA = NVL(CDCOMCLA,space(1))
        .w_CDRIFTAB = NVL(CDRIFTAB,space(20))
        cp_LoadRecExtFlds(this,'PROMCLAS')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsut_acl
    this.oldpath = this.w_CDPATSTD
    this.addcla = this.w_CDCOMCLA
    this.addtip = this.w_CDCOMTIP
    this.oldmod = this.w_CDMODALL
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CDCODCLA = space(15)
      .w_CDDESCLA = space(50)
      .w_CDMODALL = space(1)
      .w_CDRIFDEF = space(1)
      .w_CDTIPRAG = space(1)
      .w_CDCAMRAG = space(50)
      .w_CDPATSTD = space(254)
      .w_CDPUBWEB = space(1)
      .w_CDNOMFIL = space(200)
      .w_CDERASEF = space(1)
      .w_CDTIPALL = space(5)
      .w_CDCLAALL = space(5)
      .w_DESTIPALL = space(40)
      .w_DESCLALL = space(40)
      .w_CDCOMTIP = space(1)
      .w_CDCOMCLA = space(1)
      .w_CDRIFTAB = space(20)
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_CDMODALL = 'F'
        .w_CDRIFDEF = IIF(IsAlt(), 'S', 'N')
        .w_CDTIPRAG = 'N'
          .DoRTCalc(6,6,.f.)
        .w_CDPATSTD = iif(.w_CDMODALL='F',.w_CDPATSTD,' ')
        .w_CDPUBWEB = 'N'
        .w_CDNOMFIL = space(200)
        .w_CDERASEF = 'N'
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_CDTIPALL))
          .link_1_19('Full')
          endif
        .w_CDCLAALL = space(5)
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_CDCLAALL))
          .link_1_20('Full')
          endif
          .DoRTCalc(13,14,.f.)
        .w_CDCOMTIP = iif(! empty(.w_CDTIPALL) and .w_CDMODALL='F',.w_CDCOMTIP,'N')
        .w_CDCOMCLA = iif(! empty(.w_CDCLAALL) and .w_CDMODALL='F',.w_CDCOMCLA,'N')
      endif
    endwith
    cp_BlankRecExtFlds(this,'PROMCLAS')
    this.DoRTCalc(17,17,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCDCODCLA_1_1.enabled = !i_bVal
      .Page1.oPag.oCDMODALL_1_4.enabled = i_bVal
      .Page1.oPag.oCDRIFDEF_1_6.enabled = i_bVal
      .Page1.oPag.oCDTIPRAG_1_8.enabled = i_bVal
      .Page1.oPag.oCDCAMRAG_1_10.enabled = i_bVal
      .Page1.oPag.oCDPATSTD_1_12.enabled = i_bVal
      .Page1.oPag.oCDPUBWEB_1_13.enabled = i_bVal
      .Page1.oPag.oCDNOMFIL_1_14.enabled = i_bVal
      .Page1.oPag.oCDERASEF_1_15.enabled = i_bVal
      .Page1.oPag.oCDTIPALL_1_19.enabled = i_bVal
      .Page1.oPag.oCDCLAALL_1_20.enabled = i_bVal
      .Page1.oPag.oCDCOMTIP_1_23.enabled = i_bVal
      .Page1.oPag.oCDCOMCLA_1_24.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PROMCLAS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCODCLA,"CDCODCLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDDESCLA,"CDDESCLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDMODALL,"CDMODALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDRIFDEF,"CDRIFDEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDTIPRAG,"CDTIPRAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCAMRAG,"CDCAMRAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDPATSTD,"CDPATSTD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDPUBWEB,"CDPUBWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDNOMFIL,"CDNOMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDERASEF,"CDERASEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDTIPALL,"CDTIPALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCLAALL,"CDCLAALL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCOMTIP,"CDCOMTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDCOMCLA,"CDCOMCLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CDRIFTAB,"CDRIFTAB",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    i_lTable = "PROMCLAS"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PROMCLAS_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PROMCLAS_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PROMCLAS
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PROMCLAS')
        i_extval=cp_InsertValODBCExtFlds(this,'PROMCLAS')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CDCODCLA,CDDESCLA,CDMODALL,CDRIFDEF,CDTIPRAG"+;
                  ",CDCAMRAG,CDPATSTD,CDPUBWEB,CDNOMFIL,CDERASEF"+;
                  ",CDTIPALL,CDCLAALL,CDCOMTIP,CDCOMCLA,CDRIFTAB "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CDCODCLA)+;
                  ","+cp_ToStrODBC(this.w_CDDESCLA)+;
                  ","+cp_ToStrODBC(this.w_CDMODALL)+;
                  ","+cp_ToStrODBC(this.w_CDRIFDEF)+;
                  ","+cp_ToStrODBC(this.w_CDTIPRAG)+;
                  ","+cp_ToStrODBC(this.w_CDCAMRAG)+;
                  ","+cp_ToStrODBC(this.w_CDPATSTD)+;
                  ","+cp_ToStrODBC(this.w_CDPUBWEB)+;
                  ","+cp_ToStrODBC(this.w_CDNOMFIL)+;
                  ","+cp_ToStrODBC(this.w_CDERASEF)+;
                  ","+cp_ToStrODBCNull(this.w_CDTIPALL)+;
                  ","+cp_ToStrODBCNull(this.w_CDCLAALL)+;
                  ","+cp_ToStrODBC(this.w_CDCOMTIP)+;
                  ","+cp_ToStrODBC(this.w_CDCOMCLA)+;
                  ","+cp_ToStrODBC(this.w_CDRIFTAB)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PROMCLAS')
        i_extval=cp_InsertValVFPExtFlds(this,'PROMCLAS')
        cp_CheckDeletedKey(i_cTable,0,'CDCODCLA',this.w_CDCODCLA)
        INSERT INTO (i_cTable);
              (CDCODCLA,CDDESCLA,CDMODALL,CDRIFDEF,CDTIPRAG,CDCAMRAG,CDPATSTD,CDPUBWEB,CDNOMFIL,CDERASEF,CDTIPALL,CDCLAALL,CDCOMTIP,CDCOMCLA,CDRIFTAB  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CDCODCLA;
                  ,this.w_CDDESCLA;
                  ,this.w_CDMODALL;
                  ,this.w_CDRIFDEF;
                  ,this.w_CDTIPRAG;
                  ,this.w_CDCAMRAG;
                  ,this.w_CDPATSTD;
                  ,this.w_CDPUBWEB;
                  ,this.w_CDNOMFIL;
                  ,this.w_CDERASEF;
                  ,this.w_CDTIPALL;
                  ,this.w_CDCLAALL;
                  ,this.w_CDCOMTIP;
                  ,this.w_CDCOMCLA;
                  ,this.w_CDRIFTAB;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PROMCLAS_IDX,i_nConn)
      *
      * update PROMCLAS
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PROMCLAS')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CDDESCLA="+cp_ToStrODBC(this.w_CDDESCLA)+;
             ",CDMODALL="+cp_ToStrODBC(this.w_CDMODALL)+;
             ",CDRIFDEF="+cp_ToStrODBC(this.w_CDRIFDEF)+;
             ",CDTIPRAG="+cp_ToStrODBC(this.w_CDTIPRAG)+;
             ",CDCAMRAG="+cp_ToStrODBC(this.w_CDCAMRAG)+;
             ",CDPATSTD="+cp_ToStrODBC(this.w_CDPATSTD)+;
             ",CDPUBWEB="+cp_ToStrODBC(this.w_CDPUBWEB)+;
             ",CDNOMFIL="+cp_ToStrODBC(this.w_CDNOMFIL)+;
             ",CDERASEF="+cp_ToStrODBC(this.w_CDERASEF)+;
             ",CDTIPALL="+cp_ToStrODBCNull(this.w_CDTIPALL)+;
             ",CDCLAALL="+cp_ToStrODBCNull(this.w_CDCLAALL)+;
             ",CDCOMTIP="+cp_ToStrODBC(this.w_CDCOMTIP)+;
             ",CDCOMCLA="+cp_ToStrODBC(this.w_CDCOMCLA)+;
             ",CDRIFTAB="+cp_ToStrODBC(this.w_CDRIFTAB)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PROMCLAS')
        i_cWhere = cp_PKFox(i_cTable  ,'CDCODCLA',this.w_CDCODCLA  )
        UPDATE (i_cTable) SET;
              CDDESCLA=this.w_CDDESCLA;
             ,CDMODALL=this.w_CDMODALL;
             ,CDRIFDEF=this.w_CDRIFDEF;
             ,CDTIPRAG=this.w_CDTIPRAG;
             ,CDCAMRAG=this.w_CDCAMRAG;
             ,CDPATSTD=this.w_CDPATSTD;
             ,CDPUBWEB=this.w_CDPUBWEB;
             ,CDNOMFIL=this.w_CDNOMFIL;
             ,CDERASEF=this.w_CDERASEF;
             ,CDTIPALL=this.w_CDTIPALL;
             ,CDCLAALL=this.w_CDCLAALL;
             ,CDCOMTIP=this.w_CDCOMTIP;
             ,CDCOMCLA=this.w_CDCOMCLA;
             ,CDRIFTAB=this.w_CDRIFTAB;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PROMCLAS_IDX,i_nConn)
      *
      * delete PROMCLAS
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CDCODCLA',this.w_CDCODCLA  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
        if .o_CDMODALL<>.w_CDMODALL
            .w_CDPATSTD = iif(.w_CDMODALL='F',.w_CDPATSTD,' ')
        endif
        .DoRTCalc(8,11,.t.)
        if .o_CDTIPALL<>.w_CDTIPALL
            .w_CDCLAALL = space(5)
          .link_1_20('Full')
        endif
        .DoRTCalc(13,14,.t.)
        if .o_CDTIPALL<>.w_CDTIPALL.or. .o_CDMODALL<>.w_CDMODALL
            .w_CDCOMTIP = iif(! empty(.w_CDTIPALL) and .w_CDMODALL='F',.w_CDCOMTIP,'N')
        endif
        if .o_CDCLAALL<>.w_CDCLAALL.or. .o_CDMODALL<>.w_CDMODALL
            .w_CDCOMCLA = iif(! empty(.w_CDCLAALL) and .w_CDMODALL='F',.w_CDCOMCLA,'N')
        endif
        if .o_CDPATSTD<>.w_CDPATSTD
          .Calculate_METOIHJHER()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,17,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_METOIHJHER()
    with this
          * --- Controllo su cambio path di archiviazione
          conferma(this;
              ,"WARN";
             )
    endwith
  endproc
  proc Calculate_JBAMVHBNLN()
    with this
          * --- Aggiorno path di archiviazione
          conferma(this;
              ,"SAVE";
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCDRIFDEF_1_6.enabled = this.oPgFrm.Page1.oPag.oCDRIFDEF_1_6.mCond()
    this.oPgFrm.Page1.oPag.oCDTIPRAG_1_8.enabled = this.oPgFrm.Page1.oPag.oCDTIPRAG_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCDCAMRAG_1_10.enabled = this.oPgFrm.Page1.oPag.oCDCAMRAG_1_10.mCond()
    this.oPgFrm.Page1.oPag.oCDCOMTIP_1_23.enabled = this.oPgFrm.Page1.oPag.oCDCOMTIP_1_23.mCond()
    this.oPgFrm.Page1.oPag.oCDCOMCLA_1_24.enabled = this.oPgFrm.Page1.oPag.oCDCOMCLA_1_24.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCDTIPRAG_1_8.visible=!this.oPgFrm.Page1.oPag.oCDTIPRAG_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCDCAMRAG_1_10.visible=!this.oPgFrm.Page1.oPag.oCDCAMRAG_1_10.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_11.visible=!this.oPgFrm.Page1.oPag.oStr_1_11.mHide()
    this.oPgFrm.Page1.oPag.oCDPATSTD_1_12.visible=!this.oPgFrm.Page1.oPag.oCDPATSTD_1_12.mHide()
    this.oPgFrm.Page1.oPag.oCDPUBWEB_1_13.visible=!this.oPgFrm.Page1.oPag.oCDPUBWEB_1_13.mHide()
    this.oPgFrm.Page1.oPag.oCDNOMFIL_1_14.visible=!this.oPgFrm.Page1.oPag.oCDNOMFIL_1_14.mHide()
    this.oPgFrm.Page1.oPag.oCDERASEF_1_15.visible=!this.oPgFrm.Page1.oPag.oCDERASEF_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Record Updated")
          .Calculate_JBAMVHBNLN()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CDTIPALL
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
    i_lTable = "TIP_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2], .t., this.TIP_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CDTIPALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MTA',True,'TIP_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODICE like "+cp_ToStrODBC(trim(this.w_CDTIPALL)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',trim(this.w_CDTIPALL))
          select TACODICE,TADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CDTIPALL)==trim(_Link_.TACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CDTIPALL) and !this.bDontReportError
            deferred_cp_zoom('TIP_ALLE','*','TACODICE',cp_AbsName(oSource.parent,'oCDTIPALL_1_19'),i_cWhere,'GSUT_MTA',"Tipologie allegato",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1))
            select TACODICE,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CDTIPALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(this.w_CDTIPALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_CDTIPALL)
            select TACODICE,TADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CDTIPALL = NVL(_Link_.TACODICE,space(5))
      this.w_DESTIPALL = NVL(_Link_.TADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CDTIPALL = space(5)
      endif
      this.w_DESTIPALL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CDTIPALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_ALLE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.TACODICE as TACODICE119"+ ",link_1_19.TADESCRI as TADESCRI119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on PROMCLAS.CDTIPALL=link_1_19.TACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and PROMCLAS.CDTIPALL=link_1_19.TACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CDCLAALL
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_ALLE_IDX,3]
    i_lTable = "CLA_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2], .t., this.CLA_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CDCLAALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MTA',True,'CLA_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODCLA like "+cp_ToStrODBC(trim(this.w_CDCLAALL)+"%");
                   +" and TACODICE="+cp_ToStrODBC(this.w_CDTIPALL);

          i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE,TACODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',this.w_CDTIPALL;
                     ,'TACODCLA',trim(this.w_CDCLAALL))
          select TACODICE,TACODCLA,TACLADES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE,TACODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CDCLAALL)==trim(_Link_.TACODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CDCLAALL) and !this.bDontReportError
            deferred_cp_zoom('CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(oSource.parent,'oCDCLAALL_1_20'),i_cWhere,'GSUT_MTA',"Classi allegato",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CDTIPALL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TACODICE,TACODCLA,TACLADES;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                     +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TACODICE="+cp_ToStrODBC(this.w_CDTIPALL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1);
                       ,'TACODCLA',oSource.xKey(2))
            select TACODICE,TACODCLA,TACLADES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CDCLAALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES";
                   +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(this.w_CDCLAALL);
                   +" and TACODICE="+cp_ToStrODBC(this.w_CDTIPALL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_CDTIPALL;
                       ,'TACODCLA',this.w_CDCLAALL)
            select TACODICE,TACODCLA,TACLADES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CDCLAALL = NVL(_Link_.TACODCLA,space(5))
      this.w_DESCLALL = NVL(_Link_.TACLADES,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CDCLAALL = space(5)
      endif
      this.w_DESCLALL = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)+'\'+cp_ToStr(_Link_.TACODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CDCLAALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CLA_ALLE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.TACODCLA as TACODCLA120"+ ",link_1_20.TACLADES as TACLADES120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on PROMCLAS.CDCLAALL=link_1_20.TACODCLA"+" and PROMCLAS.CDTIPALL=link_1_20.TACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and PROMCLAS.CDCLAALL=link_1_20.TACODCLA(+)"'+'+" and PROMCLAS.CDTIPALL=link_1_20.TACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCDCODCLA_1_1.value==this.w_CDCODCLA)
      this.oPgFrm.Page1.oPag.oCDCODCLA_1_1.value=this.w_CDCODCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oCDDESCLA_1_3.value==this.w_CDDESCLA)
      this.oPgFrm.Page1.oPag.oCDDESCLA_1_3.value=this.w_CDDESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oCDMODALL_1_4.RadioValue()==this.w_CDMODALL)
      this.oPgFrm.Page1.oPag.oCDMODALL_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDRIFDEF_1_6.RadioValue()==this.w_CDRIFDEF)
      this.oPgFrm.Page1.oPag.oCDRIFDEF_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDTIPRAG_1_8.RadioValue()==this.w_CDTIPRAG)
      this.oPgFrm.Page1.oPag.oCDTIPRAG_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCAMRAG_1_10.value==this.w_CDCAMRAG)
      this.oPgFrm.Page1.oPag.oCDCAMRAG_1_10.value=this.w_CDCAMRAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCDPATSTD_1_12.value==this.w_CDPATSTD)
      this.oPgFrm.Page1.oPag.oCDPATSTD_1_12.value=this.w_CDPATSTD
    endif
    if not(this.oPgFrm.Page1.oPag.oCDPUBWEB_1_13.RadioValue()==this.w_CDPUBWEB)
      this.oPgFrm.Page1.oPag.oCDPUBWEB_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDNOMFIL_1_14.value==this.w_CDNOMFIL)
      this.oPgFrm.Page1.oPag.oCDNOMFIL_1_14.value=this.w_CDNOMFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oCDERASEF_1_15.RadioValue()==this.w_CDERASEF)
      this.oPgFrm.Page1.oPag.oCDERASEF_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDTIPALL_1_19.value==this.w_CDTIPALL)
      this.oPgFrm.Page1.oPag.oCDTIPALL_1_19.value=this.w_CDTIPALL
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCLAALL_1_20.value==this.w_CDCLAALL)
      this.oPgFrm.Page1.oPag.oCDCLAALL_1_20.value=this.w_CDCLAALL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIPALL_1_21.value==this.w_DESTIPALL)
      this.oPgFrm.Page1.oPag.oDESTIPALL_1_21.value=this.w_DESTIPALL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLALL_1_22.value==this.w_DESCLALL)
      this.oPgFrm.Page1.oPag.oDESCLALL_1_22.value=this.w_DESCLALL
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCOMTIP_1_23.RadioValue()==this.w_CDCOMTIP)
      this.oPgFrm.Page1.oPag.oCDCOMTIP_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDCOMCLA_1_24.RadioValue()==this.w_CDCOMCLA)
      this.oPgFrm.Page1.oPag.oCDCOMCLA_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCDRIFTAB_1_26.value==this.w_CDRIFTAB)
      this.oPgFrm.Page1.oPag.oCDRIFTAB_1_26.value=this.w_CDRIFTAB
    endif
    cp_SetControlsValueExtFlds(this,'PROMCLAS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- gsut_acl
      if empty(this.w_CDPATSTD) and this.w_CDMODALL='F'
        i_bRes=.f.
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CDMODALL = this.w_CDMODALL
    this.o_CDRIFDEF = this.w_CDRIFDEF
    this.o_CDPATSTD = this.w_CDPATSTD
    this.o_CDTIPALL = this.w_CDTIPALL
    this.o_CDCLAALL = this.w_CDCLAALL
    return

  func CanAdd()
    local i_res
    i_res=1=0
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione non consentita!"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=1=0
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione non consentita!"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsut_aclPag1 as StdContainer
  Width  = 745
  height = 288
  stdWidth  = 745
  stdheight = 288
  resizeXpos=240
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCDCODCLA_1_1 as StdField with uid="FBGIVQSQKP",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CDCODCLA", cQueryName = "CDCODCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice della classe libreria immagini",;
    HelpContextID = 17446247,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=151, Top=9, InputMask=replicate('X',15)

  add object oCDDESCLA_1_3 as StdField with uid="DRMKSOOZCI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CDDESCLA", cQueryName = "CDDESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 32523623,;
   bGlobalFont=.t.,;
    Height=21, Width=364, Left=279, Top=9, InputMask=replicate('X',50)


  add object oCDMODALL_1_4 as StdCombo with uid="YTHKJQZCGY",rtseq=3,rtrep=.f.,left=151,top=42,width=147,height=21;
    , ToolTipText = "Tipo archiviazione: copia file o collegamento";
    , HelpContextID = 252368242;
    , cFormVar="w_CDMODALL",RowSource=""+"Collegamento,"+"Copia file", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCDMODALL_1_4.RadioValue()
    return(iif(this.value =1,'L',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oCDMODALL_1_4.GetRadio()
    this.Parent.oContained.w_CDMODALL = this.RadioValue()
    return .t.
  endfunc

  func oCDMODALL_1_4.SetRadio()
    this.Parent.oContained.w_CDMODALL=trim(this.Parent.oContained.w_CDMODALL)
    this.value = ;
      iif(this.Parent.oContained.w_CDMODALL=='L',1,;
      iif(this.Parent.oContained.w_CDMODALL=='F',2,;
      0))
  endfunc


  add object oCDRIFDEF_1_6 as StdCombo with uid="DUUHJPUDNE",rtseq=4,rtrep=.f.,left=151,top=73,width=147,height=21;
    , ToolTipText = "Indicare se la classe � di riferimento per una tabella per l'archiviazione rapida";
    , HelpContextID = 232446612;
    , cFormVar="w_CDRIFDEF",RowSource=""+"Non presente,"+"Selezionabile,"+"Default,"+"Standard", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oCDRIFDEF_1_6.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'A',;
    iif(this.value =3,'D',;
    iif(this.value =4,'S',;
    space(1))))))
  endfunc
  func oCDRIFDEF_1_6.GetRadio()
    this.Parent.oContained.w_CDRIFDEF = this.RadioValue()
    return .t.
  endfunc

  func oCDRIFDEF_1_6.SetRadio()
    this.Parent.oContained.w_CDRIFDEF=trim(this.Parent.oContained.w_CDRIFDEF)
    this.value = ;
      iif(this.Parent.oContained.w_CDRIFDEF=='N',1,;
      iif(this.Parent.oContained.w_CDRIFDEF=='A',2,;
      iif(this.Parent.oContained.w_CDRIFDEF=='D',3,;
      iif(this.Parent.oContained.w_CDRIFDEF=='S',4,;
      0))))
  endfunc

  func oCDRIFDEF_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!IsAlt())
    endwith
   endif
  endfunc


  add object oCDTIPRAG_1_8 as StdCombo with uid="FHIGXNUHBQ",rtseq=5,rtrep=.f.,left=151,top=103,width=147,height=21;
    , ToolTipText = "Tipo di raggruppamento";
    , HelpContextID = 255507091;
    , cFormVar="w_CDTIPRAG",RowSource=""+"Settimana,"+"Mese,"+"Trimestre,"+"Quadrimestre,"+"Semestre,"+"Anno,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCDTIPRAG_1_8.RadioValue()
    return(iif(this.value =1,'W',;
    iif(this.value =2,'M',;
    iif(this.value =3,'T',;
    iif(this.value =4,'Q',;
    iif(this.value =5,'S',;
    iif(this.value =6,'A',;
    iif(this.value =7,'N',;
    space(1)))))))))
  endfunc
  func oCDTIPRAG_1_8.GetRadio()
    this.Parent.oContained.w_CDTIPRAG = this.RadioValue()
    return .t.
  endfunc

  func oCDTIPRAG_1_8.SetRadio()
    this.Parent.oContained.w_CDTIPRAG=trim(this.Parent.oContained.w_CDTIPRAG)
    this.value = ;
      iif(this.Parent.oContained.w_CDTIPRAG=='W',1,;
      iif(this.Parent.oContained.w_CDTIPRAG=='M',2,;
      iif(this.Parent.oContained.w_CDTIPRAG=='T',3,;
      iif(this.Parent.oContained.w_CDTIPRAG=='Q',4,;
      iif(this.Parent.oContained.w_CDTIPRAG=='S',5,;
      iif(this.Parent.oContained.w_CDTIPRAG=='A',6,;
      iif(this.Parent.oContained.w_CDTIPRAG=='N',7,;
      0)))))))
  endfunc

  func oCDTIPRAG_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDMODALL='F')
    endwith
   endif
  endfunc

  func oCDTIPRAG_1_8.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL<>'F')
    endwith
  endfunc

  add object oCDCAMRAG_1_10 as StdField with uid="HAQHPKHUCF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CDCAMRAG", cQueryName = "CDCAMRAG",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Campo su cui eseguire il raggruppamento",;
    HelpContextID = 259246739,;
   bGlobalFont=.t.,;
    Height=21, Width=256, Left=379, Top=103, InputMask=replicate('X',50)

  func oCDCAMRAG_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CDTIPRAG<>'N')
    endwith
   endif
  endfunc

  func oCDCAMRAG_1_10.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL<>'F')
    endwith
  endfunc

  add object oCDPATSTD_1_12 as StdField with uid="MFHLYHPRPT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CDPATSTD", cQueryName = "CDPATSTD",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso standard di archiviazione",;
    HelpContextID = 235076246,;
   bGlobalFont=.t.,;
    Height=21, Width=484, Left=151, Top=135, InputMask=replicate('X',254)

  func oCDPATSTD_1_12.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL<>'F')
    endwith
  endfunc

  proc oCDPATSTD_1_12.mAfter
    with this.Parent.oContained
      .w_CDPATSTD=addbs(.w_CDPATSTD)
    endwith
  endproc

  add object oCDPUBWEB_1_13 as StdCheck with uid="RWQSQMBOSF",rtseq=8,rtrep=.f.,left=151, top=164, caption="Pubblica su Web",;
    ToolTipText = "Se attivo nell'associazione manuale da file l'indice creato potr� essere pubblicato su web",;
    HelpContextID = 185531032,;
    cFormVar="w_CDPUBWEB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCDPUBWEB_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCDPUBWEB_1_13.GetRadio()
    this.Parent.oContained.w_CDPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oCDPUBWEB_1_13.SetRadio()
    this.Parent.oContained.w_CDPUBWEB=trim(this.Parent.oContained.w_CDPUBWEB)
    this.value = ;
      iif(this.Parent.oContained.w_CDPUBWEB=='S',1,;
      0)
  endfunc

  func oCDPUBWEB_1_13.mHide()
    with this.Parent.oContained
      return (g_CPIN<>'S')
    endwith
  endfunc

  add object oCDNOMFIL_1_14 as StdField with uid="WWRITYAJJQ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CDNOMFIL", cQueryName = "CDNOMFIL",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Espressione per determinazione del nome file (es.: dtos (mvdatdoc) +mvalfdoc+...)",;
    HelpContextID = 77260146,;
   bGlobalFont=.t.,;
    Height=21, Width=460, Left=151, Top=194, InputMask=replicate('X',200)

  func oCDNOMFIL_1_14.mHide()
    with this.Parent.oContained
      return (g_CPIN<>'S' OR .w_CDPUBWEB<>'S')
    endwith
  endfunc

  add object oCDERASEF_1_15 as StdCheck with uid="FVRXNBLLQH",rtseq=10,rtrep=.f.,left=626, top=194, caption="Cancella file",;
    ToolTipText = "Se attivo: cancella il file dopo la pubblicazione",;
    HelpContextID = 253930132,;
    cFormVar="w_CDERASEF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCDERASEF_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCDERASEF_1_15.GetRadio()
    this.Parent.oContained.w_CDERASEF = this.RadioValue()
    return .t.
  endfunc

  func oCDERASEF_1_15.SetRadio()
    this.Parent.oContained.w_CDERASEF=trim(this.Parent.oContained.w_CDERASEF)
    this.value = ;
      iif(this.Parent.oContained.w_CDERASEF=='S',1,;
      0)
  endfunc

  func oCDERASEF_1_15.mHide()
    with this.Parent.oContained
      return (g_CPIN<>'S' OR .w_CDPUBWEB<>'S')
    endwith
  endfunc

  add object oCDTIPALL_1_19 as StdField with uid="UEVFILDVYG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CDTIPALL", cQueryName = "CDTIPALL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia dell'allegato",;
    HelpContextID = 264586610,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=151, Top=223, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_ALLE", cZoomOnZoom="GSUT_MTA", oKey_1_1="TACODICE", oKey_1_2="this.w_CDTIPALL"

  func oCDTIPALL_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
      if .not. empty(.w_CDCLAALL)
        bRes2=.link_1_20('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCDTIPALL_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCDTIPALL_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_ALLE','*','TACODICE',cp_AbsName(this.parent,'oCDTIPALL_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MTA',"Tipologie allegato",'',this.parent.oContained
  endproc
  proc oCDTIPALL_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MTA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TACODICE=this.parent.oContained.w_CDTIPALL
     i_obj.ecpSave()
  endproc

  add object oCDCLAALL_1_20 as StdField with uid="TUJJXRDSPW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CDCLAALL", cQueryName = "CDCLAALL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Classe dell'allegato",;
    HelpContextID = 248984946,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=151, Top=252, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_ALLE", cZoomOnZoom="GSUT_MTA", oKey_1_1="TACODICE", oKey_1_2="this.w_CDTIPALL", oKey_2_1="TACODCLA", oKey_2_2="this.w_CDCLAALL"

  func oCDCLAALL_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCDCLAALL_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCDCLAALL_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CLA_ALLE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStrODBC(this.Parent.oContained.w_CDTIPALL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStr(this.Parent.oContained.w_CDTIPALL)
    endif
    do cp_zoom with 'CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(this.parent,'oCDCLAALL_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MTA',"Classi allegato",'',this.parent.oContained
  endproc
  proc oCDCLAALL_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MTA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TACODICE=w_CDTIPALL
     i_obj.w_TACODCLA=this.parent.oContained.w_CDCLAALL
     i_obj.ecpSave()
  endproc

  add object oDESTIPALL_1_21 as StdField with uid="LPWCVJXHHY",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESTIPALL", cQueryName = "DESTIPALL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 27247806,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=224, Top=223, InputMask=replicate('X',40)

  add object oDESCLALL_1_22 as StdField with uid="PJIJPQLEBY",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESCLALL", cQueryName = "DESCLALL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 259995266,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=224, Top=252, InputMask=replicate('X',40)

  add object oCDCOMTIP_1_23 as StdCheck with uid="BSXYTQKUXO",rtseq=15,rtrep=.f.,left=521, top=223, caption="Completa path file",;
    ToolTipText = "Se attivo completa il path di archiviazione file con il codice tipologia allegato",;
    HelpContextID = 43660662,;
    cFormVar="w_CDCOMTIP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCDCOMTIP_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCDCOMTIP_1_23.GetRadio()
    this.Parent.oContained.w_CDCOMTIP = this.RadioValue()
    return .t.
  endfunc

  func oCDCOMTIP_1_23.SetRadio()
    this.Parent.oContained.w_CDCOMTIP=trim(this.Parent.oContained.w_CDCOMTIP)
    this.value = ;
      iif(this.Parent.oContained.w_CDCOMTIP=='S',1,;
      0)
  endfunc

  func oCDCOMTIP_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! empty(.w_CDTIPALL) and .w_CDMODALL='F')
    endwith
   endif
  endfunc

  add object oCDCOMCLA_1_24 as StdCheck with uid="ITXLWKWRBG",rtseq=16,rtrep=.f.,left=521, top=252, caption="Completa path file",;
    ToolTipText = "Se attivo completa il path di archiviazione file con il codice classe allegato",;
    HelpContextID = 26883431,;
    cFormVar="w_CDCOMCLA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCDCOMCLA_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCDCOMCLA_1_24.GetRadio()
    this.Parent.oContained.w_CDCOMCLA = this.RadioValue()
    return .t.
  endfunc

  func oCDCOMCLA_1_24.SetRadio()
    this.Parent.oContained.w_CDCOMCLA=trim(this.Parent.oContained.w_CDCOMCLA)
    this.value = ;
      iif(this.Parent.oContained.w_CDCOMCLA=='S',1,;
      0)
  endfunc

  func oCDCOMCLA_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! empty(.w_CDCLAALL) and .w_CDMODALL='F')
    endwith
   endif
  endfunc

  add object oCDRIFTAB_1_26 as StdField with uid="WDIHKDTNJF",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CDRIFTAB", cQueryName = "CDRIFTAB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tabella di riferimento per classe (x archiviazione rapida)",;
    HelpContextID = 232446616,;
   bGlobalFont=.t.,;
    Height=21, Width=256, Left=379, Top=73, InputMask=replicate('X',20)

  add object oStr_1_2 as StdString with uid="GYOKEJIGRR",Visible=.t., Left=24, Top=9,;
    Alignment=1, Width=125, Height=18,;
    Caption="Libreria allegati:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="TWNZSAFEMM",Visible=.t., Left=69, Top=44,;
    Alignment=1, Width=80, Height=18,;
    Caption="Modalit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="SBVAXGBCVF",Visible=.t., Left=15, Top=105,;
    Alignment=1, Width=134, Height=18,;
    Caption="Tipo raggruppamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL<>'F')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="YGYVNGNEEA",Visible=.t., Left=317, Top=103,;
    Alignment=1, Width=59, Height=18,;
    Caption="Su campo:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL<>'F')
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="NGRPSRBCES",Visible=.t., Left=15, Top=137,;
    Alignment=1, Width=134, Height=18,;
    Caption="Percorso archiviazione:"  ;
  , bGlobalFont=.t.

  func oStr_1_11.mHide()
    with this.Parent.oContained
      return (.w_CDMODALL<>'F')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="XHVFALXOJJ",Visible=.t., Left=14, Top=196,;
    Alignment=1, Width=135, Height=18,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (g_CPIN<>'S' OR .w_CDPUBWEB<>'S')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="HOAOVKTDQW",Visible=.t., Left=15, Top=225,;
    Alignment=1, Width=134, Height=18,;
    Caption="Tipologia allegato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="JJRIMPQWFX",Visible=.t., Left=15, Top=254,;
    Alignment=1, Width=134, Height=18,;
    Caption="Classe allegato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="QNKCHOEUBP",Visible=.t., Left=5, Top=73,;
    Alignment=1, Width=144, Height=18,;
    Caption="Archiviazione rapida:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="UMYKNFSBCW",Visible=.t., Left=301, Top=73,;
    Alignment=1, Width=75, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_acl','PROMCLAS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CDCODCLA=PROMCLAS.CDCODCLA";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_acl
Procedure CONFERMA(obj,cparam)
  Local risposta
  If Empty(cparam)
    ah_msg("Paramentro non valido")
  Else
    If obj.cfunction='Edit' And obj.w_CDMODALL = 'F' and obj.w_CDPATSTD<>obj.oldpath
      risposta = ah_yesno("La modifica del path standard comporta l'aggiornamento del path assoluto degli indici.%0Confermi l'operazione di aggiornamento?",48)
      If risposta
        If cparam="SAVE"
        * Record salvato, aggiorno l'indice
          Do GSUT_BKK With obj
        Endif
      Else
        If cparam="WARN"
        * ripristino i valori originari
          obj.w_CDPATSTD = obj.oldpath
          obj.w_CDCOMCLA = obj.addcla
          obj.w_CDCOMTIP = obj.addtip
          obj.w_CDMODALL = obj.oldmod
        Endif
      Endif
    Endif
  Endif
Endproc
* --- Fine Area Manuale
