* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bcv                                                        *
*              Duplicazione contratti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_205]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-04-27                                                      *
* Last revis.: 2011-11-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bcv",oParentObject)
return(i_retval)

define class tgsve_bcv as StdBatch
  * --- Local variables
  w_ROWNUM = 0
  w_CODCLF = space(15)
  w_CATCOM = space(3)
  w_DESCON = space(50)
  w_NOTE = space(0)
  w_CODVAL = space(3)
  w_GRUPMER = space(5)
  w_CODART = space(20)
  w_PREZZO = 0
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_PERPRO = 0
  w_QUANTI = 0
  w_PREZZO1 = 0
  w_SCONT11 = 0
  w_SCONT21 = 0
  w_SCONT31 = 0
  w_SCONT41 = 0
  w_PERPRO1 = 0
  w_PERCAP1 = 0
  w_RIGA = 0
  w_ESISTE = .f.
  w_LOTMUL = 0
  w_QTAMIN = 0
  w_GIOAPP = 0
  w_PRIORI = 0
  w_PERRIP = 0
  w_IVALIS = space(1)
  w_FLUCOA = space(1)
  w_CONTA = 0
  w_ARTTEST = space(15)
  w_GRUTEST = space(5)
  w_FLARTI = space(1)
  w_CONTRA = space(15)
  w_CO__TIPO = space(1)
  w_PERCAP = 0
  w_PRCOST = space(1)
  w_UNIMIS = space(3)
  * --- WorkFile variables
  CON_COSC_idx=0
  CON_TRAD_idx=0
  CON_TRAM_idx=0
  CON_MACL_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dalle maschera "Duplicazione contratti" : GSVE_KDC e GSAC_KDC
    * --- Definizione variabili
    * --- Controlli sui campi della maschera
    * --- Bisogna specificare il numero di contratto da modificare o creare
    if NOT CHKDTOBS(this.oParentObject.w_DTOBSO,this.oParentObject.w_CODATFIN,"Valuta obsoleta alla data attuale", .F.)
      ah_ErrorMsg("Valuta contratto da generare obsoleta alla data di fine validit�")
      i_retcode = 'stop'
      return
    endif
    if empty(this.oParentObject.w_CONUMERO)
      ah_ErrorMsg("Inserire il numero del contratto")
      i_retcode = 'stop'
      return
    endif
    if empty(this.oParentObject.w_NUMERO)
      ah_ErrorMsg("Inserire il numero del contratto di riferimento")
      i_retcode = 'stop'
      return
    endif
    if empty(this.oParentObject.w_CODATCON) or empty(this.oParentObject.w_CODATINI) or empty(this.oParentObject.w_CODATFIN)
      ah_ErrorMsg("Inserire data contratto e di inizio-fine validit�")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_COTIPCON="C"
      * --- Non devono essere valorizzati sia il cliente che la categoria commerciale
      if !EMPTY(this.oParentObject.w_COCODCLF) and !EMPTY(this.oParentObject.w_COCATCOM)
        ah_ErrorMsg("Inserire o cliente o categoria commerciale")
        i_retcode = 'stop'
        return
      endif
      * --- Bisogna specificare un cliente o una categoria commerciale
      if empty(this.oParentObject.w_COCODCLF) and empty(this.oParentObject.w_COCATCOM)
        ah_ErrorMsg("Inserire un cliente o una categoria commerciale")
        i_retcode = 'stop'
        return
      endif
    else
      if empty(this.oParentObject.w_COCODCLF)
        ah_ErrorMsg("Inserire un cliente")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Controllo se il contratto esiste gi�
    vq_exec("query\GSVE_KDC.VQR",this,"__tmp__")
    if RECCOUNT("__tmp__")=0
      if ah_YesNo("Il contratto non esiste! Vuoi crearlo?")
        this.w_ESISTE = .F.
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        i_retcode = 'stop'
        return
      endif
    else
      if COTIPCLF=this.oParentObject.w_COTIPCON
        if ah_YesNo("Il contratto esiste gi�! vuoi modificarlo?")
          this.w_ESISTE = .T.
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          i_retcode = 'stop'
          return
        endif
      else
        ah_ErrorMsg("Numero contratto gi� esistente e non modificabile")
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo modifica valuta
    if this.oParentObject.w_COCODVAL<>this.oParentObject.w_VALUTA
      this.w_PREZZO = VAL2VAL(NVL(COPREZZO,0),this.oParentObject.w_CAMBIO1, this.oParentObject.w_DATACON, this.oParentObject.w_OBTEST,this.oParentObject.w_CAMBIO2,this.oParentObject.w_DECTOT)
      this.w_PREZZO1 = VAL2VAL(NVL(COPREZZO1,0), this.oParentObject.w_CAMBIO1, this.oParentObject.w_DATACON, this.oParentObject.w_OBTEST,this.oParentObject.w_CAMBIO2,this.oParentObject.w_DECTOT)
    else
      * --- Viene mantenuta la valuta del contratto di riferimento
      this.w_PREZZO = NVL(COPREZZO,0)
      this.w_PREZZO1 = NVL(COPREZZO1,0)
    endif
    if this.oParentObject.w_FLSCAG=" "
      * --- Nel contratto di riferimento � disattivato il flag scaglioni
      if this.oParentObject.w_RICPE1<>0
        * --- Ricalcolo dei prezzi per percentuale
        this.w_PREZZO = this.w_PREZZO*(100+this.oParentObject.w_RICPE1)/100
      else
        if this.oParentObject.w_RICVA1<>0
          * --- Ricalcolo dei prezzi per valore solo se per articoli. Ai gruppi merceologici non si pu� ricalcolare per valore
          this.w_PREZZO = this.w_PREZZO+this.oParentObject.w_RICVA1
        endif
      endif
      if this.oParentObject.w_ARROT1<>0
        * --- Controllo se il prezzo finale � minore del primo arrotondamento
        if this.w_PREZZO<=this.oParentObject.w_VALORIN
          * --- Applico l'arrotondamento selezionato.
          this.w_PREZZO = cp_ROUND((this.w_PREZZO/this.oParentObject.w_ARROT1),0)*this.oParentObject.w_ARROT1
        else
          * --- Controllo se il prezzo finale � minore del secondo arrotondamento
          if this.w_PREZZO<=this.oParentObject.w_VALOR2IN
            * --- Applico l'arrotondamento selezionato.
            this.w_PREZZO = cp_ROUND((this.w_PREZZO/this.oParentObject.w_ARROT2),0)*this.oParentObject.w_ARROT2
          else
            * --- Controllo se il prezzo finale � minore del terzo arrotondamento
            if this.w_PREZZO<=this.oParentObject.w_VALOR3IN
              * --- Applico l'arrotondamento selezionato.
              this.w_PREZZO = cp_ROUND((this.w_PREZZO/this.oParentObject.w_ARROT3),0)*this.oParentObject.w_ARROT3
            else
              * --- Controllo se il prezzo finale � minore del quarto arrotondamento
              if this.oParentObject.w_ARROT4<>0
                * --- Applico l'ultimo arrotondamento selezionato.
                this.w_PREZZO = cp_ROUND((this.w_PREZZO/this.oParentObject.w_ARROT4),0)*this.oParentObject.w_ARROT4
              else
                * --- Il prezzo � maggiore dei vari valori degli arrotondamenti quindi non applico nessun arrotondamento.
                this.w_PREZZO = cp_ROUND((this.w_PREZZO),this.oParentObject.w_DECTOT)
              endif
            endif
          endif
        endif
      else
        * --- Non ho applicato nessun arrotondamento.Il prezzo finale viene calcolato in base alla valuta del listino finale
        this.w_PREZZO = cp_ROUND((this.w_PREZZO),this.oParentObject.w_DECTOT)
      endif
    else
      if this.oParentObject.w_RICPE1<>0
        * --- Ricalcolo dei prezzi dello scaglione per percentuale
        this.w_PREZZO1 = (this.w_PREZZO1*(100+this.oParentObject.w_RICPE1))/100
      else
        if this.oParentObject.w_RICVA1<>0
          * --- Ricalcolo dei prezzi dello scaglione per valore solo se per articoli. Ai gruppi merceologici non si pu� ricalcolare per valore
          this.w_PREZZO1 = this.w_PREZZO1+this.oParentObject.w_RICVA1
        endif
      endif
      * --- Nel contratto di riferimento e attivato il flag scaglioni
      if this.oParentObject.w_ARROT1<>0
        * --- Controllo se il prezzo finale � minore del primo arrotondamento
        if this.w_PREZZO1<=this.oParentObject.w_VALORIN
          * --- Applico l'arrotondamento selezionato.
          this.w_PREZZO1 = cp_ROUND((this.w_PREZZO1/this.oParentObject.w_ARROT1),0)*this.oParentObject.w_ARROT1
        else
          * --- Controllo se il prezzo finale � minore del secondo arrotondamento
          if this.w_PREZZO1<=this.oParentObject.w_VALOR2IN
            if this.oParentObject.w_ARROT2<>0
              * --- Applico l'arrotondamento selezionato.
              this.w_PREZZO1 = cp_ROUND((this.w_PREZZO1/this.oParentObject.w_ARROT2),0)*this.oParentObject.w_ARROT2
            endif
          else
            * --- Controllo se il prezzo finale � minore del terzo arrotondamento
            if this.w_PREZZO1<=this.oParentObject.w_VALOR3IN
              if this.oParentObject.w_ARROT3<>0
                * --- Applico l'arrotondamento selezionato.
                this.w_PREZZO1 = cp_ROUND((this.w_PREZZO1/this.oParentObject.w_ARROT3),0)*this.oParentObject.w_ARROT3
              endif
            else
              * --- Controllo se il prezzo finale � minore del quarto arrotondamento
              if this.oParentObject.w_ARROT4<>0
                * --- Applico l'ultimo arrotondamento selezionato.
                this.w_PREZZO1 = cp_ROUND((this.w_PREZZO1/this.oParentObject.w_ARROT4),0)*this.oParentObject.w_ARROT4
              else
                * --- Il prezzo � maggiore dei vari valori degli arrotondamenti quindi non applico nessun arrotondamento.
                this.w_PREZZO1 = cp_ROUND((this.w_PREZZO1),this.oParentObject.w_DECTOT)
              endif
            endif
          endif
        endif
      else
        * --- Non ho applicato nessun arrotondamento.Il prezzo finale viene calcolato in base alla valuta  finale
        this.w_PREZZO1 = cp_ROUND((this.w_PREZZO1),this.oParentObject.w_DECTOT)
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione o aggiornamento del contratto 
    * --- Eseguo la query sul contratto di riferimento
    vq_exec("query\GSVE2KDC.VQR",this,"CONTRIFE")
    * --- Nel caso in cui esiste il contratto elimino tutti i dati esistenti 
    * --- Try
    local bErr_05169168
    bErr_05169168=bTrsErr
    this.Try_05169168()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Errore in inserimento dati")
    endif
    bTrsErr=bTrsErr or bErr_05169168
    * --- End
  endproc
  proc Try_05169168()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inizio elaborazione cursore.
    * --- begin transaction
    cp_BeginTrs()
    if this.w_ESISTE
      * --- Delete from CON_COSC
      i_nConn=i_TableProp[this.CON_COSC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_COSC_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"COCODICE = "+cp_ToStrODBC(this.oParentObject.w_CONUMERO);
               )
      else
        delete from (i_cTable) where;
              COCODICE = this.oParentObject.w_CONUMERO;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from CON_TRAD
      i_nConn=i_TableProp[this.CON_TRAD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAD_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CONUMERO = "+cp_ToStrODBC(this.oParentObject.w_CONUMERO);
               )
      else
        delete from (i_cTable) where;
              CONUMERO = this.oParentObject.w_CONUMERO;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from CON_TRAM
      i_nConn=i_TableProp[this.CON_TRAM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CONUMERO = "+cp_ToStrODBC(this.oParentObject.w_CONUMERO);
               )
      else
        delete from (i_cTable) where;
              CONUMERO = this.oParentObject.w_CONUMERO;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- Apertura delle transazioni
    SELECT CONTRIFE
    GO TOP
    * --- Assegnamento dei dati inseriti dalla maschera alle variabili locali
    this.w_ROWNUM = 0
    this.w_RIGA = CPROWNUM
    this.w_CONTRA = CONUMERO
    this.w_CODCLF = this.oParentObject.w_COCODCLF
    this.w_CATCOM = this.oParentObject.w_COCATCOM
    this.w_DESCON = this.oParentObject.w_CODESCON
    this.w_NOTE = this.oParentObject.w_CO_NOTE
    this.w_CODVAL = this.oParentObject.w_COCODVAL
    this.w_FLUCOA = this.oParentObject.w_COFLUCOA
    this.w_IVALIS = this.oParentObject.w_COIVALIS
    this.w_FLARTI = this.oParentObject.w_COFLARTI
    this.w_CO__TIPO = "C"
    * --- Inserimento in tabella dei dati di testata
    * --- Insert into CON_TRAM
    i_nConn=i_TableProp[this.CON_TRAM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_TRAM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CONUMERO"+",CODATCON"+",COTIPCLF"+",COCODCLF"+",COCATCOM"+",CODESCON"+",CO__NOTE"+",CODATINI"+",CODATFIN"+",COCODVAL"+",COIVALIS"+",COFLUCOA"+",COAFFIDA"+",COFLARTI"+",CO__TIPO"+",COQUANTI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONUMERO),'CON_TRAM','CONUMERO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODATCON),'CON_TRAM','CODATCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COTIPCON),'CON_TRAM','COTIPCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCLF),'CON_TRAM','COCODCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CATCOM),'CON_TRAM','COCATCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESCON),'CON_TRAM','CODESCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOTE),'CON_TRAM','CO__NOTE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODATINI),'CON_TRAM','CODATINI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODATFIN),'CON_TRAM','CODATFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'CON_TRAM','COCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IVALIS),'CON_TRAM','COIVALIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLUCOA),'CON_TRAM','COFLUCOA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COAFFIDA),'CON_TRAM','COAFFIDA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLARTI),'CON_TRAM','COFLARTI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CO__TIPO),'CON_TRAM','CO__TIPO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FLSCAG),'CON_TRAM','COQUANTI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CONUMERO',this.oParentObject.w_CONUMERO,'CODATCON',this.oParentObject.w_CODATCON,'COTIPCLF',this.oParentObject.w_COTIPCON,'COCODCLF',this.w_CODCLF,'COCATCOM',this.w_CATCOM,'CODESCON',this.w_DESCON,'CO__NOTE',this.w_NOTE,'CODATINI',this.oParentObject.w_CODATINI,'CODATFIN',this.oParentObject.w_CODATFIN,'COCODVAL',this.w_CODVAL,'COIVALIS',this.w_IVALIS,'COFLUCOA',this.w_FLUCOA)
      insert into (i_cTable) (CONUMERO,CODATCON,COTIPCLF,COCODCLF,COCATCOM,CODESCON,CO__NOTE,CODATINI,CODATFIN,COCODVAL,COIVALIS,COFLUCOA,COAFFIDA,COFLARTI,CO__TIPO,COQUANTI &i_ccchkf. );
         values (;
           this.oParentObject.w_CONUMERO;
           ,this.oParentObject.w_CODATCON;
           ,this.oParentObject.w_COTIPCON;
           ,this.w_CODCLF;
           ,this.w_CATCOM;
           ,this.w_DESCON;
           ,this.w_NOTE;
           ,this.oParentObject.w_CODATINI;
           ,this.oParentObject.w_CODATFIN;
           ,this.w_CODVAL;
           ,this.w_IVALIS;
           ,this.w_FLUCOA;
           ,this.oParentObject.w_COAFFIDA;
           ,this.w_FLARTI;
           ,this.w_CO__TIPO;
           ,this.oParentObject.w_FLSCAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    SELECT CONTRIFE
    * --- Ciclo per dati di riga
    do while NOT EOF()
      * --- Assegnamento dei dati del cursore alle variabili locali
      this.w_GRUPMER = NVL(COGRUMER," ")
      this.w_CODART = NVL(COCODART," ")
      this.w_SCONT1 = NVL(COSCONT1,0)
      this.w_SCONT2 = NVL(COSCONT2,0)
      this.w_SCONT3 = NVL(COSCONT3,0)
      this.w_SCONT4 = NVL(COSCONT4,0)
      this.w_PERPRO = NVL(COPERPRO,0)
      this.w_GIOAPP = NVL(COGIOAPP,0)
      this.w_LOTMUL = NVL(COLOTMUL,0)
      this.w_PRIORI = NVL(COPRIORI,0)
      this.w_PERRIP = NVL(COPERRIP,0)
      this.w_PERCAP = Nvl(COPERCAP,0)
      this.w_PRCOST = Nvl(COPRCOST," ")
      this.w_PREZZO = 0
      this.w_PREZZO1 = 0
      this.w_QTAMIN = NVL(COQTAMIN, 0)
      this.w_UNIMIS = NVL(COUNIMIS,SPACE(3))
      if NOT EMPTY(this.w_CODART) OR NOT EMPTY (this.w_GRUPMER)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Inserimento nella tabella dettaglo-contratti
      this.w_ROWNUM = this.w_ROWNUM+1
      if not empty(this.w_CODART) and empty(this.w_UNIMIS)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARUNMIS1"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARUNMIS1;
            from (i_cTable) where;
                ARCODART = this.w_CODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UNIMIS = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if this.oParentObject.w_COTIPCON="C" 
        * --- Caso di contratti di vendita
        * --- Insert into CON_TRAD
        i_nConn=i_TableProp[this.CON_TRAD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAD_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_TRAD_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CONUMERO"+",CPROWNUM"+",COGRUMER"+",COCODART"+",COPREZZO"+",COSCONT1"+",COSCONT2"+",COSCONT3"+",COSCONT4"+",COPERPRO"+",COUNIMIS"+",COPRCOST"+",COPERCAP"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONUMERO),'CON_TRAD','CONUMERO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'CON_TRAD','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_GRUPMER),'CON_TRAD','COGRUMER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'CON_TRAD','COCODART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZO),'CON_TRAD','COPREZZO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT1),'CON_TRAD','COSCONT1');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT2),'CON_TRAD','COSCONT2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT3),'CON_TRAD','COSCONT3');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT4),'CON_TRAD','COSCONT4');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PERPRO),'CON_TRAD','COPERPRO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'CON_TRAD','COUNIMIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRCOST),'CON_TRAD','COPRCOST');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PERCAP),'CON_TRAD','COPERCAP');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CONUMERO',this.oParentObject.w_CONUMERO,'CPROWNUM',this.w_ROWNUM,'COGRUMER',this.w_GRUPMER,'COCODART',this.w_CODART,'COPREZZO',this.w_PREZZO,'COSCONT1',this.w_SCONT1,'COSCONT2',this.w_SCONT2,'COSCONT3',this.w_SCONT3,'COSCONT4',this.w_SCONT4,'COPERPRO',this.w_PERPRO,'COUNIMIS',this.w_UNIMIS,'COPRCOST',this.w_PRCOST)
          insert into (i_cTable) (CONUMERO,CPROWNUM,COGRUMER,COCODART,COPREZZO,COSCONT1,COSCONT2,COSCONT3,COSCONT4,COPERPRO,COUNIMIS,COPRCOST,COPERCAP &i_ccchkf. );
             values (;
               this.oParentObject.w_CONUMERO;
               ,this.w_ROWNUM;
               ,this.w_GRUPMER;
               ,this.w_CODART;
               ,this.w_PREZZO;
               ,this.w_SCONT1;
               ,this.w_SCONT2;
               ,this.w_SCONT3;
               ,this.w_SCONT4;
               ,this.w_PERPRO;
               ,this.w_UNIMIS;
               ,this.w_PRCOST;
               ,this.w_PERCAP;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      else
        * --- Caso di contratti di acquisto
        * --- Insert into CON_TRAD
        i_nConn=i_TableProp[this.CON_TRAD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAD_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_TRAD_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"CONUMERO"+",CPROWNUM"+",COGRUMER"+",COCODART"+",COQTAMIN"+",COLOTMUL"+",COGIOAPP"+",COPRIORI"+",COPERRIP"+",COUNIMIS"+",COPRCOST"+",COPERCAP"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONUMERO),'CON_TRAD','CONUMERO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'CON_TRAD','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_GRUPMER),'CON_TRAD','COGRUMER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'CON_TRAD','COCODART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_QTAMIN),'CON_TRAD','COQTAMIN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_LOTMUL),'CON_TRAD','COLOTMUL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_GIOAPP),'CON_TRAD','COGIOAPP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRIORI),'CON_TRAD','COPRIORI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PERRIP),'CON_TRAD','COPERRIP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_UNIMIS),'CON_TRAD','COUNIMIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PRCOST),'CON_TRAD','COPRCOST');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PERCAP),'CON_TRAD','COPERCAP');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'CONUMERO',this.oParentObject.w_CONUMERO,'CPROWNUM',this.w_ROWNUM,'COGRUMER',this.w_GRUPMER,'COCODART',this.w_CODART,'COQTAMIN',this.w_QTAMIN,'COLOTMUL',this.w_LOTMUL,'COGIOAPP',this.w_GIOAPP,'COPRIORI',this.w_PRIORI,'COPERRIP',this.w_PERRIP,'COUNIMIS',this.w_UNIMIS,'COPRCOST',this.w_PRCOST,'COPERCAP',this.w_PERCAP)
          insert into (i_cTable) (CONUMERO,CPROWNUM,COGRUMER,COCODART,COQTAMIN,COLOTMUL,COGIOAPP,COPRIORI,COPERRIP,COUNIMIS,COPRCOST,COPERCAP &i_ccchkf. );
             values (;
               this.oParentObject.w_CONUMERO;
               ,this.w_ROWNUM;
               ,this.w_GRUPMER;
               ,this.w_CODART;
               ,this.w_QTAMIN;
               ,this.w_LOTMUL;
               ,this.w_GIOAPP;
               ,this.w_PRIORI;
               ,this.w_PERRIP;
               ,this.w_UNIMIS;
               ,this.w_PRCOST;
               ,this.w_PERCAP;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Insert into CON_MACL
        i_nConn=i_TableProp[this.CON_MACL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CON_MACL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\gsve4kdc",this.CON_MACL_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      SELECT CONTRIFE
      * --- Ciclo sulla tabella scaglioni solo se il flag � attivato
      if this.oParentObject.w_FLSCAG="S"
        * --- Se presenti gli scaglioni eseguo il ciclo sul cursore per inserirli.....
        do while CONTRIFE.CPROWNUM=this.w_RIGA
          * --- Ciclo per valorizzazione Scaglioni
          * --- Assegnamento valori del cursore alle variabili locali
          if this.w_RIGA<>0
            if NOT EMPTY(this.w_CODART) OR NOT EMPTY(this.w_GRUPMER)
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            this.w_QUANTI = NVL(COQUANTI,0)
            this.w_PERPRO1 = NVL(COPERPRO1,0)
            this.w_PERCAP1 = NVL(COPERCAP1,0)
            this.w_SCONT11 = NVL(COSCONT11,0)
            this.w_SCONT21 = NVL(COSCONT21,0)
            this.w_SCONT31 = NVL(COSCONT31,0)
            this.w_SCONT41 = NVL(COSCONT41,0)
            if this.w_QUANTI<>0
              * --- Inserimento nella tabella scaglioni
              * --- Insert into CON_COSC
              i_nConn=i_TableProp[this.CON_COSC_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CON_COSC_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CON_COSC_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"COCODICE"+",CONUMROW"+",COQUANTI"+",COPREZZO"+",COSCONT1"+",COSCONT2"+",COSCONT3"+",COSCONT4"+",COPERPRO"+",COPERCAP"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONUMERO),'CON_COSC','COCODICE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'CON_COSC','CONUMROW');
                +","+cp_NullLink(cp_ToStrODBC(this.w_QUANTI),'CON_COSC','COQUANTI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZO1),'CON_COSC','COPREZZO');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT11),'CON_COSC','COSCONT1');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT21),'CON_COSC','COSCONT2');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT31),'CON_COSC','COSCONT3');
                +","+cp_NullLink(cp_ToStrODBC(this.w_SCONT41),'CON_COSC','COSCONT4');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PERPRO1),'CON_COSC','COPERPRO');
                +","+cp_NullLink(cp_ToStrODBC(this.w_PERCAP1),'CON_COSC','COPERCAP');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'COCODICE',this.oParentObject.w_CONUMERO,'CONUMROW',this.w_ROWNUM,'COQUANTI',this.w_QUANTI,'COPREZZO',this.w_PREZZO1,'COSCONT1',this.w_SCONT11,'COSCONT2',this.w_SCONT21,'COSCONT3',this.w_SCONT31,'COSCONT4',this.w_SCONT41,'COPERPRO',this.w_PERPRO1,'COPERCAP',this.w_PERCAP1)
                insert into (i_cTable) (COCODICE,CONUMROW,COQUANTI,COPREZZO,COSCONT1,COSCONT2,COSCONT3,COSCONT4,COPERPRO,COPERCAP &i_ccchkf. );
                   values (;
                     this.oParentObject.w_CONUMERO;
                     ,this.w_ROWNUM;
                     ,this.w_QUANTI;
                     ,this.w_PREZZO1;
                     ,this.w_SCONT11;
                     ,this.w_SCONT21;
                     ,this.w_SCONT31;
                     ,this.w_SCONT41;
                     ,this.w_PERPRO1;
                     ,this.w_PERCAP1;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              SELECT CONTRIFE
            endif
          endif
          SKIP
        enddo
        this.w_RIGA = CPROWNUM
      else
        * --- ....altrimenti vado al record successivo (nuovo articolo/gruppo merceologico)
        SKIP
        this.w_RIGA = CPROWNUM
      endif
    enddo
    * --- Rilascio cursore
    if USED ("CONTRIFE")
      SELECT CONTRIFE
      USE
    endif
    * --- Fine transazione
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Elaborazione terminata")
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CON_COSC'
    this.cWorkTables[2]='CON_TRAD'
    this.cWorkTables[3]='CON_TRAM'
    this.cWorkTables[4]='CON_MACL'
    this.cWorkTables[5]='ART_ICOL'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
