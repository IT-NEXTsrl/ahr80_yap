* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_buk                                                        *
*              Untitled                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-07-31                                                      *
* Last revis.: 2009-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_buk",oParentObject)
return(i_retval)

define class tgsut_buk as StdBatch
  * --- Local variables
  w_ZOOM = space(10)
  w_UCCODCCK = space(20)
  w_UCCODCON = space(20)
  w_UCORAUCK = space(8)
  w_UCDATUCK = ctod("  /  /  ")
  * --- WorkFile variables
  AHEUSRCK_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina i Codici Selezionati
    this.w_ZOOM = this.oParentObject.w_ZUTENTI
    NC = this.w_ZOOM.cCursor
    * --- Cicla sui record Selezionati
    if ah_YesNo ("Confermi richiesta di cancellazione dei dati selezionati?")
      SELECT &NC
      GO TOP
      SCAN FOR XCHK=1
      this.w_UCCODCCK = nvl( &NC..UCCODCCK , space(20) )
      this.w_UCCODCON = nvl( &NC..UCCODCON , space(20) )
      this.w_UCORAUCK = nvl( &NC..UCORAUCK , space(8) )
      this.w_UCDATUCK = &NC..UCDATUCK
      * --- Try
      local bErr_03A48F30
      bErr_03A48F30=bTrsErr
      this.Try_03A48F30()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03A48F30
      * --- End
      ENDSCAN
      * --- Aggiorna lista
      This.OparentObject.NotifyEvent("Aggiorna")
    endif
  endproc
  proc Try_03A48F30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from AHEUSRCK
    i_nConn=i_TableProp[this.AHEUSRCK_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AHEUSRCK_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"UCCODCCK = "+cp_ToStrODBC(this.w_UCCODCCK);
            +" and UCCODCON = "+cp_ToStrODBC(this.w_UCCODCON);
            +" and UCORAUCK = "+cp_ToStrODBC(this.w_UCORAUCK);
            +" and UCDATUCK = "+cp_ToStrODBC(this.w_UCDATUCK);
             )
    else
      delete from (i_cTable) where;
            UCCODCCK = this.w_UCCODCCK;
            and UCCODCON = this.w_UCCODCON;
            and UCORAUCK = this.w_UCORAUCK;
            and UCDATUCK = this.w_UCDATUCK;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AHEUSRCK'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
