* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kia                                                        *
*              Giroconto IVA autotrasportatori                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_28]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-31                                                      *
* Last revis.: 2009-12-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kia",oParentObject))

* --- Class definition
define class tgscg_kia as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 675
  Height = 520
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-15"
  HelpContextID=267003241
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_kia"
  cComment = "Giroconto IVA autotrasportatori"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ANNO = space(4)
  w_NUMPER = 0
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_TOTIMP = 0
  w_TOTIVA = 0
  w_SERIALE = space(10)
  w_DATAUT = ctod('  /  /  ')
  w_IVAAUTR = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kiaPag1","gscg_kia",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANNO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_IVAAUTR = this.oPgFrm.Pages(1).oPag.IVAAUTR
    DoDefault()
    proc Destroy()
      this.w_IVAAUTR = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ANNO=space(4)
      .w_NUMPER=0
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_TOTIMP=0
      .w_TOTIVA=0
      .w_SERIALE=space(10)
      .w_DATAUT=ctod("  /  /  ")
      .oPgFrm.Page1.oPag.IVAAUTR.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
          .DoRTCalc(1,6,.f.)
        .w_SERIALE = .w_IVAAUTR.getVar('PNSERIAL')
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
    endwith
    this.DoRTCalc(8,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.IVAAUTR.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .DoRTCalc(1,6,.t.)
            .w_SERIALE = .w_IVAAUTR.getVar('PNSERIAL')
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.IVAAUTR.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.IVAAUTR.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANNO_1_1.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_1.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMPER_1_2.value==this.w_NUMPER)
      this.oPgFrm.Page1.oPag.oNUMPER_1_2.value=this.w_NUMPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_4.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_4.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_6.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_6.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIMP_1_12.value==this.w_TOTIMP)
      this.oPgFrm.Page1.oPag.oTOTIMP_1_12.value=this.w_TOTIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIVA_1_14.value==this.w_TOTIVA)
      this.oPgFrm.Page1.oPag.oTOTIVA_1_14.value=this.w_TOTIVA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ANNO)) or not(VAL(.w_ANNO)>1900))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_1.SetFocus()
            i_bnoObbl = !empty(.w_ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NUMPER)) or not((g_TIPDEN='T' AND .w_NUMPER<5) OR (g_TIPDEN="M" AND MOD(.w_NUMPER, 3)=0 AND .w_NUMPER<13)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMPER_1_2.SetFocus()
            i_bnoObbl = !empty(.w_NUMPER)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero periodo non congruente (se mensile deve essere il mese di fine trimestre)")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_kiaPag1 as StdContainer
  Width  = 671
  height = 520
  stdWidth  = 671
  stdheight = 520
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANNO_1_1 as StdField with uid="DITKXJLBMK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",nZero=4,;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento del periodo da considerare",;
    HelpContextID = 261485306,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=56, Top=8, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oANNO_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_ANNO)>1900)
    endwith
    return bRes
  endfunc

  add object oNUMPER_1_2 as StdField with uid="GNUQORRVDZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_NUMPER", cQueryName = "NUMPER",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero periodo non congruente (se mensile deve essere il mese di fine trimestre)",;
    ToolTipText = "Periodo: 1..4 per trimestrale; 3, 6, 9, 12 per mensile",;
    HelpContextID = 155515690,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=169, Top=8, cSayPict='"99"', cGetPict='"99"'

  func oNUMPER_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((g_TIPDEN='T' AND .w_NUMPER<5) OR (g_TIPDEN="M" AND MOD(.w_NUMPER, 3)=0 AND .w_NUMPER<13))
    endwith
    return bRes
  endfunc

  add object oDATINI_1_4 as StdField with uid="WBXPRDTUMT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio ricerca delle registrazioni aventi il flag IVA autotrasportatori",;
    HelpContextID = 29073354,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=224, Top=34

  add object oDATFIN_1_6 as StdField with uid="BRRWZJJLWH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine ricerca delle registrazioni aventi il flag IVA autotrasportatori",;
    HelpContextID = 219062218,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=484, Top=34


  add object oBtn_1_7 as StdButton with uid="DWNEZPEAGL",left=568, top=11, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per ricercare le registrazioni aventi il flag IVA autotrasportatori";
    , HelpContextID = 90081002;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        do GSCG_BIA with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="FITGOQGPMA",left=618, top=11, width=48,height=45,;
    CpPicture="BMP\APPLICA.BMP", caption="", nPag=1;
    , ToolTipText = "Crea la registrazione di giroconto IVA autotrasportatori relativa al periodo selezionato";
    , HelpContextID = 172199990;
    , Caption='\<Giroconto';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        do GSCG_BGI with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DATINI) AND NOT EMPTY(.w_DATFIN))
      endwith
    endif
  endfunc


  add object IVAAUTR as cp_zoombox with uid="JBLBFMUOTN",left=5, top=64, width=655,height=399,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSCG_KIA",cTable="PNT_MAST",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,bRetriveAllRows=.f.,cZoomOnZoom="GSZM_BCC",cMenuFile="",;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 89005594


  add object oBtn_1_10 as StdButton with uid="PGWDUJBTRZ",left=8, top=467, width=48,height=45,;
    CpPicture="BMP\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza la registrazione associata alla riga selezionata";
    , HelpContextID = 85189094;
    , Caption='Or\<igine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_SERIALE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(NVL(.w_SERIALE,'')))
      endwith
    endif
  endfunc

  add object oTOTIMP_1_12 as StdField with uid="JVCAQPXBBF",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TOTIMP", cQueryName = "TOTIMP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imponibile delle registrazioni aventi il flag IVA autotrasportatori",;
    HelpContextID = 181113034,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=325, Top=466, cSayPict="v_PV[20]", cGetPict="v_PV[20]"

  add object oTOTIVA_1_14 as StdField with uid="IUWUWFXVSS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_TOTIVA", cQueryName = "TOTIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale imposta delle registrazioni aventi il flag IVA autotrasportatori",;
    HelpContextID = 154898634,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=520, Top=466, cSayPict="v_PV[20]", cGetPict="v_PV[20]"


  add object oObj_1_17 as cp_runprogram with uid="NFVEDDPLLN",left=157, top=545, width=138,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BCD('C')",;
    cEvent = "w_ANNO Changed,w_NUMPER Changed",;
    nPag=1;
    , HelpContextID = 89005594


  add object oObj_1_19 as cp_runprogram with uid="DNLHTYCTOP",left=16, top=545, width=138,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BCD('I')",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 89005594


  add object oObj_1_21 as cp_runprogram with uid="QRVKRSHVRL",left=16, top=568, width=251,height=21,;
    caption='GSAR_BZP(w_SERIALE)',;
   bGlobalFont=.t.,;
    prg="GSAR_BZP(w_SERIALE)",;
    cEvent = "w_ivaautr selected",;
    nPag=1;
    , HelpContextID = 252216887

  add object oStr_1_3 as StdString with uid="QJKJHTCPHM",Visible=.t., Left=7, Top=37,;
    Alignment=1, Width=215, Height=18,;
    Caption="Data inizio trimestre precedente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="KIGOSMKJON",Visible=.t., Left=306, Top=37,;
    Alignment=1, Width=178, Height=18,;
    Caption="Data fine trimestre precedente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="TBMMABWSJV",Visible=.t., Left=241, Top=469,;
    Alignment=1, Width=84, Height=18,;
    Caption="Tot. imponibile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="NJQSZYXHOB",Visible=.t., Left=470, Top=469,;
    Alignment=1, Width=49, Height=18,;
    Caption="Tot. IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="RDYUWVGEWE",Visible=.t., Left=114, Top=11,;
    Alignment=1, Width=53, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="PUHMGWINIU",Visible=.t., Left=13, Top=11,;
    Alignment=1, Width=42, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kia','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
