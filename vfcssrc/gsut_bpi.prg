* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bpi                                                        *
*              Esportazione su infinity                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-11-13                                                      *
* Last revis.: 2016-04-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSTEP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bpi",oParentObject,m.pSTEP)
return(i_retval)

define class tgsut_bpi as StdBatch
  * --- Local variables
  pSTEP = 0
  w_ARCHIVIO = space(20)
  w_CLASSE = space(15)
  w_LICLADOC = space(15)
  w_CONFMASK = .f.
  w_INNOMEFILE = space(255)
  w_CODAZI = space(5)
  w_DESCRI = space(40)
  w_NumRep = 0
  w_NomRep = space(100)
  w_NAMECUR = space(15)
  w_INFINITY = space(1)
  w_IDMSSA = space(1)
  w_INARCHIVIO = space(1)
  w_NONNO = .NULL.
  w_OBJXML = .NULL.
  w_UploadDocumentsXML = space(254)
  w_XMLdescrittore = space(254)
  w_XMLFILE = space(200)
  w_COPATHDS = space(200)
  w_oObjInfinity = .NULL.
  w_CDCODATT = space(20)
  w_ERRINF = 0
  w_OBJXMLRES = .NULL.
  w_FILETOSTR = space(254)
  w_NUMNODI = 0
  w_oNODE = .NULL.
  w_iChild = 0
  w_nDocumentUploaded = 0
  w_NameError = space(254)
  w_FILENAME = space(254)
  w_PATH = space(254)
  * --- WorkFile variables
  PROMCLAS_idx=0
  CONTROPA_idx=0
  PRO_LOGP_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esportazione Infinity
    do case
      case this.pSTEP = 1
        * --- Richiede classe documentale
        this.w_IDMSSA = This.oParentObject.w_IDMSSA
        this.w_NumRep = This.oParentObject.oParentObject.cNomeReport.GetMainReport()
        if this.w_NumRep<>-1
          this.w_NAMECUR = This.oParentObject.oParentObject.cNomeReport.GetCursorName(this.w_NumRep)
        else
          this.w_NAMECUR = "__TMP__"
        endif
        do GSUT_KCW with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if !this.w_CONFMASK
          * --- Fermo la pubblicazione
          i_retcode = 'stop'
          i_retval = -1
          return
        else
          * --- Classe documentale selezionata
          this.oParentObject.w_INCLASSEDOC = this.w_CLASSE
        endif
      case this.pSTEP = 2
        if this.oParentObject.w_FDFFile
          this.oParentObject.w_NomeFile = this.oParentObject.w_NomeRep
        endif
        * --- Nome del file vuoto
        if Empty(this.oParentObject.w_NomeFile)
          i_retcode = 'stop'
          i_retval = -1
          return
        else
          this.oParentObject.w_NomeFile = FORCEEXT(alltrim(this.oParentObject.w_NomeFile)+".", iif(this.oParentObject.w_FDFFile, "FDF", this.oParentObject.w_EXT))
        endif
      case this.pSTEP = 3
        * --- Legge cartella dove depositare il file .PDF
        this.w_CODAZI = i_CODAZI
        * --- Read from CONTROPA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTROPA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "COPATHDO,COPATHDS"+;
            " from "+i_cTable+" CONTROPA where ";
                +"COCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            COPATHDO,COPATHDS;
            from (i_cTable) where;
                COCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DIRLAV = NVL(cp_ToDate(_read_.COPATHDO),cp_NullValue(_read_.COPATHDO))
          this.w_COPATHDS = NVL(cp_ToDate(_read_.COPATHDS),cp_NullValue(_read_.COPATHDS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.oParentObject.w_MODALLEG="S"
          this.oParentObject.w_DIRLAV = ADDBS(this.w_COPATHDS)
        else
          this.oParentObject.w_DIRLAV = ADDBS(this.oParentObject.w_DIRLAV)
          if g_DMIP="S"
            this.oParentObject.w_DIRLAV = ADDBS(this.oParentObject.w_DIRLAV+SYS(2015))
          endif
        endif
        * --- Se non c'� crea la cartella ...
        if !DIRECTORY(this.oParentObject.w_DIRLAV)
          if Not(this.AH_CreateFolder(this.oParentObject.w_DIRLAV))
            i_retcode = 'stop'
            i_retval = -1
            return
          endif
        endif
      case this.pSTEP = 4
        this.w_INARCHIVIO = This.oParentObject.w_INARCHIVIO
        if Type("This.oParentObject.oParentObject.cNomeReport") = "O"
          this.w_NumRep = This.oParentObject.oParentObject.cNomeReport.GetMainReport()
          if this.w_NumRep<>-1
            this.w_NAMECUR = This.oParentObject.oParentObject.cNomeReport.GetCursorName(this.w_NumRep)
          else
            this.w_NAMECUR = "__TMP__"
          endif
          Private L_ArrParam
          dimension L_ArrParam(30)
          L_ArrParam(1)="AR"
          L_ArrParam(2)=this.oParentObject.w_NomeFile
          L_ArrParam(3)=this.oParentObject.w_INCLASSEDOC
          L_ArrParam(4)=i_CODUTE
          L_ArrParam(5)=this.w_NAMECUR
          L_ArrParam(6)=""
          L_ArrParam(7)=""
          L_ArrParam(8)=""
          L_ArrParam(9)=" "
          L_ArrParam(10)=( g_DMIP<>"S" )
          L_ArrParam(11)=( g_DMIP<>"S" )
          L_ArrParam(12)=" "
          L_ArrParam(13)=" "
          L_ArrParam(14)=.null.
          L_ArrParam(15)=" "
          L_ArrParam(16)=" "
          L_ArrParam(17)=" "
          L_ArrParam(18)=" "
          L_ArrParam(19)=" "
          L_ArrParam(21)=.F.
          L_ArrParam(22)=" "
          L_ArrParam(23)=.T.
          L_ArrParam(24)=( g_DMIP="S" )
          * --- Valorizzo la variabile w_NomRep che verr� utilizzata dal bba per verificare l'eventuale associazione di un processo documentale a questo report.
          *     Questo server a determinare il corretto ExternalCode
          this.w_NomRep = ForceExt(Alltrim(this.oParentObject.w_NomeRep),"FRX")
          GSUT_BBA(this,@L_ArrParam)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          if this.oParentObject.w_NomeRep="GSUT_BCV.PDF" or this.oParentObject.w_MODALLEG="S"
            * --- Importazione da Infinity con associazione tramite barcode
            this.w_ARCHIVIO = This.oParentObject.w_ARCHIVIO
            this.w_NONNO = this.oParentObject.oParentObject
            Private L_ArrParam
            dimension L_ArrParam(30)
            L_ArrParam(1)="AR"
            L_ArrParam(2)=this.oParentObject.w_NomeFile
            L_ArrParam(3)=this.oParentObject.w_INCLASSEDOC
            L_ArrParam(4)=i_CODUTE
            L_ArrParam(5)=this.w_NONNO
            L_ArrParam(6)=this.w_ARCHIVIO
            L_ArrParam(7)=this.oParentObject.w_CHIAVE
            L_ArrParam(8)=this.oParentObject.w_DESBRE
            L_ArrParam(9)=this.oParentObject.w_DESLUN
            L_ArrParam(10)=.F.
            L_ArrParam(11)=.F.
            L_ArrParam(12)=" "
            L_ArrParam(13)=" "
            L_ArrParam(14)=this.w_NONNO
            L_ArrParam(15)=this.oParentObject.w_TIPOALLE
            L_ArrParam(16)=this.oParentObject.w_CLASSEALL
            L_ArrParam(17)=" "
            L_ArrParam(18)=this.oParentObject.w_QUEMOD
            L_ArrParam(19)=this.oParentObject.w_OPERAZIONE+this.oParentObject.w_ASBAUTOM+this.oParentObject.w_ASNOREPS
            L_ArrParam(21)=.F.
            L_ArrParam(22)=" "
            L_ArrParam(23)=(NVL(this.oParentObject.w_CDPUBWEB, "N")="S")
            L_ArrParam(24)=(this.oParentObject.w_MODALLEG="S" or this.oParentObject.w_OPERAZIONE="B")
            GSUT_BBA(this,@L_ArrParam)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            Private L_ArrParam
            dimension L_ArrParam(30)
            L_ArrParam(1)="AR"
            L_ArrParam(2)=this.oParentObject.w_NomeFile
            L_ArrParam(3)=this.oParentObject.w_INCLASSEDOC
            L_ArrParam(4)=i_CODUTE
            L_ArrParam(5)="__TMP__"
            L_ArrParam(6)=""
            L_ArrParam(7)=""
            L_ArrParam(8)=""
            L_ArrParam(9)=""
            L_ArrParam(10)=(g_DMIP<>"S")
            L_ArrParam(11)=(g_DMIP<>"S")
            L_ArrParam(12)=""
            L_ArrParam(13)=""
            L_ArrParam(14)=.NULL.
            L_ArrParam(15)=""
            L_ArrParam(16)=""
            L_ArrParam(17)=""
            L_ArrParam(18)=""
            L_ArrParam(19)=""
            L_ArrParam(21)=.F.
            L_ArrParam(22)=""
            L_ArrParam(23)=.T.
            L_ArrParam(24)=(g_DMIP="S")
            GSUT_BBA(this,@L_ArrParam)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        if g_DMIP="S" or this.oParentObject.w_MODALLEG="S"
          * --- Nel caso di integrazione infinity easy devo completare descrittore e inviare lo zip
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Messaggio Finale
          AH_ErrorMsg("Preparazione file per invio a Infinity completata con successo",48)
        endif
    endcase
    * --- Tutto OK
    i_retcode = 'stop'
    i_retval = 0
    return
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    *     Chiudo XML descrittore per invio a Infinity
    if not empty(this.w_UploadDocumentsXML)
      if this.oParentObject.w_MODALLEG="S"
        * --- Infinity StandAlone
        if empty(this.w_XMLdescrittore)
          ah_errormsg("Errore nell'invio a Infinity stand alone: descrittore non creato.","!")
        else
          this.w_CODAZI = i_CODAZI
          this.w_XMLFILE = FORCEEXT(this.w_UploadDocumentsXML,"xml")
          * --- Read from CONTROPA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTROPA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "COPATHDS"+;
              " from "+i_cTable+" CONTROPA where ";
                  +"COCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              COPATHDS;
              from (i_cTable) where;
                  COCODAZI = this.w_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_COPATHDS = NVL(cp_ToDate(_read_.COPATHDS),cp_NullValue(_read_.COPATHDS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_COPATHDS = addbs(this.w_COPATHDS)
          if not empty(this.w_COPATHDS) and directory(this.w_COPATHDS)
            * --- Memorizza ... e imposta
             w_CreateObjErr = .F. 
 w_OldErr=on("ERROR")
            on error w_CreateObjErr=.t.
            this.w_XMLFILE = this.w_COPATHDS+justfname(this.w_XMLFILE)
            STRTOFILE(this.w_XMLdescrittore, this.w_XMLFILE)
            if w_CreateObjErr
              ah_errormsg("Errore nell'invio a Infinity stand alone del documento%0%1","!","", message())
            else
              AH_ErrorMsg("Invio a Infinity stand alone completato con successo",48)
            endif
            * --- Ripristina OnError
            on error &w_OldErr
          else
            ah_errormsg("Errore nell'invio a Infinity stand alone del documento%0Percorso non valido","!")
          endif
        endif
      else
        this.w_OBJXML.Save(this.w_UploadDocumentsXML)     
        * --- Faccio lo zip della cartella con il descrittore e i file da inviare
        local L_ERROREZIP
        if ZIPUNZIP("Z", FORCEEXT(this.w_UploadDocumentsXML,"ZIP"), ADDBS(JUSTPATH(this.w_UploadDocumentsXML)), "", @L_ERROREZIP, "7zzip")
          * --- Chiamo procedura per invio del file zippato
          * --- variabili Infinity D.M.S.
          * --- Memorizza ... e imposta
           w_CreateObjErr = .F. 
 w_OldErr=on("ERROR")
          on error w_CreateObjErr=.t.
          this.w_oObjInfinity = InfinityConnect(.F., g_REVI="S", g_REVI="S")
          if vartype(this.w_oObjInfinity)="O"
            this.w_ERRINF = this.w_oObjInfinity.UploadDocuments(FORCEEXT(this.w_UploadDocumentsXML,"ZIP"))
            if this.w_ERRINF = 0
              this.w_ERRINF = this.w_oObjInfinity.GetUploadResponse(FORCEEXT(this.w_UploadDocumentsXML,"")+"_response.xml","generic")
              if this.w_ERRINF = 0
                * --- Gestione xml di risposta
                this.w_FILETOSTR = FILETOSTR(FORCEEXT(this.w_UploadDocumentsXML,"")+"_response.xml")
                this.w_OBJXMLRES = createobject("Msxml2.DOMDocument")
                this.w_OBJXMLRES.LoadXML(this.w_FILETOSTR)     
                this.w_NUMNODI = this.w_OBJXMLRES.childNodes.item(1).childNodes.length
                this.w_iChild = 0
                * --- * recupera tutti i nodi figli
                do while this.w_iChild < this.w_NUMNODI
                  * --- recupero nodo Process
                  if this.w_OBJXMLRES.childNodes.item(1).childNodes.item(this.w_iChild).nodeName="Add_UploadDocuments"
                    this.w_oNODE = this.w_OBJXMLRES.childNodes.item(1).childNodes.item(this.w_iChild)
                    this.w_nDocumentUploaded = this.w_nDocumentUploaded + 1
                    this.w_NameError = this.w_oNODE.GetAttribute("Error")
                    if empty(nvl(this.w_NameError,""))
                      AH_ErrorMsg("Invio a Infinity completato con successo",48)
                    else
                      ah_errormsg("Errore nell'invio a Infinity del documento%0%1","!","",this.w_NameError)
                    endif
                  endif
                  this.w_iChild = this.w_iChild + 1
                enddo
              else
                ah_errormsg("Errore nell'invio a Infinity del documento%0Codice errore:%1","!","", str(this.w_ERRINF, 6,0))
              endif
            else
              ah_errormsg("Errore nell'invio a Infinity del documento%0Codice errore:%1","!","", str(this.w_ERRINF, 6,0))
            endif
            this.w_oObjInfinity.DisConnect()     
          else
            ah_errormsg("Errore nell'invio a Infinity del documento%0Impossibile creare connessione","!")
          endif
          * --- Ripristina OnError
          on error &w_OldErr
          if empty(nvl(this.w_NameError,"")) AND this.oParentObject.w_NomeRep="GSUT_BCV.PDF"
            * --- Elimino la cartella temporanea
            this.w_PATH = JUSTPATH(this.w_UploadDocumentsXML)
            if DIRECTORY(this.w_PATH)
              this.w_FILENAME = this.w_PATH+"\*.*"
              DELETE FILE (this.w_FILENAME)
              RD (this.w_PATH)
            endif
          endif
        else
          ah_errormsg("Errore nella creazione dello zip per l'invio a Infinity dei documenti%0%1","!","",L_ERROREZIP)
        endif
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pSTEP)
    this.pSTEP=pSTEP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PROMCLAS'
    this.cWorkTables[2]='CONTROPA'
    this.cWorkTables[3]='PRO_LOGP'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- gsut_bpi
  * ===============================================================================
    * AH_CreateFolder() - Crea la cartella passata come parametro
    *
    Function AH_CreateFolder(pDirectory)
    private lOldErr,lRet
    * On Error
    lOldErr=ON("Error")
    * Init Risultato
    lRet = True
    * Crea
    on error lRet=False
    *
    pDirectory=JUSTPATH(ADDBS(pDirectory))
    *
    MD (pDirectory)
    *
    if not(Empty(lOldErr))
      on error &lOldErr
    else
      on error
    endif   
    * 
    if not(lRet)
      = Ah_ErrorMsg("Impossibile creare la cartella %1",16,,pDirectory)
    endif
    * Risultato
    Return (lRet)
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSTEP"
endproc
