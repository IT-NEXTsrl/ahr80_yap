* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_but                                                        *
*              Gestione formato allegato da parametri utente                   *
*                                                                              *
*      Author: Pollina Fabrizio                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-24                                                      *
* Last revis.: 2005-05-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_but",oParentObject,m.pAzione)
return(i_retval)

define class tgsut_but as StdBatch
  * --- Local variables
  pAzione = space(3)
  w_PCNAME = space(20)
  w_MODOGG = .NULL.
  * --- WorkFile variables
  TMPSALDI_idx=0
  STMPFILE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PCNAME = Left( Sys(0), Rat("#",Sys( 0 ))-1)
    this.w_MODOGG = this.oParentObject.GetCtrl("w_TYPEALL")
    do case
      case this.pAzione="DEL"
        * --- Drop temporary table TMPSALDI
        i_nIdx=cp_GetTableDefIdx('TMPSALDI')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPSALDI')
        endif
      case this.pAzione="INI"
        * --- Read from STMPFILE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.STMPFILE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2],.t.,this.STMPFILE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" STMPFILE where ";
                +"SFFLGSEL = "+cp_ToStrODBC("S");
                +" and SFPCNAME = "+cp_ToStrODBC(this.w_PCNAME);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                SFFLGSEL = "S";
                and SFPCNAME = this.w_PCNAME;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows>0
          * --- Create temporary table TMPSALDI
          i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          i_nConn=i_TableProp[this.STMPFILE_idx,3] && recupera la connessione
          i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2])
          cp_CreateTempTable(i_nConn,i_cTempTable,"SFFORMAT "," from "+i_cTable;
                +" where SFFLGSEL = 'S' AND SFPCNAME = "+cp_ToStrODBC(this.w_PCNAME)+"";
                +" order by CPROWNUM";
                )
          this.TMPSALDI_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        else
          * --- Create temporary table TMPSALDI
          i_nIdx=cp_AddTableDef('TMPSALDI') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          i_nConn=i_TableProp[this.STMPFILE_idx,3] && recupera la connessione
          i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2])
          cp_CreateTempTable(i_nConn,i_cTempTable,"SFFORMAT "," from "+i_cTable;
                +" where SFFLGSEL = 'S' AND SFPCNAME = 'INSTALLAZIONE'";
                +" order by CPROWNUM";
                )
          this.TMPSALDI_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        endif
        this.w_MODOGG.Clear()     
        this.w_MODOGG.cTable = "TMPSALDI"
        this.w_MODOGG.cKey = "SFFORMAT"
        this.w_MODOGG.cValue = "SFFORMAT"
        this.w_MODOGG.Init()     
    endcase
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='*TMPSALDI'
    this.cWorkTables[2]='STMPFILE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
