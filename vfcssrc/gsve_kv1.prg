* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kv1                                                        *
*              Zoom documenti                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_124]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-21                                                      *
* Last revis.: 2011-06-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_kv1",oParentObject))

* --- Class definition
define class tgsve_kv1 as StdForm
  Top    = 36
  Left   = 49

  * --- Standard Properties
  Width  = 485
  Height = 310
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-06-03"
  HelpContextID=48902761
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsve_kv1"
  cComment = "Zoom documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CAUDOC = space(5)
  w_DOCFOR = space(1)
  w_CODCON = space(15)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_FLVEAC = space(1)
  w_CATEGO = space(2)
  w_TIPCON = space(1)
  w_NUMINI = 0
  w_NUMFIN = 0
  w_ALFINI = space(10)
  w_ALFFIN = space(10)
  w_SERIALE = space(10)
  w_NUMDO1 = 0
  w_ALFDO1 = space(10)
  w_DATDO1 = ctod('  /  /  ')
  w_NOTE = space(50)
  w_MVCLADOC = space(2)
  w_CAUSALE = space(5)
  w_NOTEPN = space(50)
  w_DESTIPDO1 = space(35)
  w_CODCON1 = space(15)
  w_ANDES1 = space(35)
  w_docu = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kv1Pag1","gsve_kv1",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_docu = this.oPgFrm.Pages(1).oPag.docu
    DoDefault()
    proc Destroy()
      this.w_docu = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CAUDOC=space(5)
      .w_DOCFOR=space(1)
      .w_CODCON=space(15)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_FLVEAC=space(1)
      .w_CATEGO=space(2)
      .w_TIPCON=space(1)
      .w_NUMINI=0
      .w_NUMFIN=0
      .w_ALFINI=space(10)
      .w_ALFFIN=space(10)
      .w_SERIALE=space(10)
      .w_NUMDO1=0
      .w_ALFDO1=space(10)
      .w_DATDO1=ctod("  /  /  ")
      .w_NOTE=space(50)
      .w_MVCLADOC=space(2)
      .w_CAUSALE=space(5)
      .w_NOTEPN=space(50)
      .w_DESTIPDO1=space(35)
      .w_CODCON1=space(15)
      .w_ANDES1=space(35)
      .w_CAUDOC=oParentObject.w_CAUDOC
      .w_DOCFOR=oParentObject.w_DOCFOR
      .w_CODCON=oParentObject.w_CODCON
      .w_DATINI=oParentObject.w_DATINI
      .w_DATFIN=oParentObject.w_DATFIN
      .w_FLVEAC=oParentObject.w_FLVEAC
      .w_CATEGO=oParentObject.w_CATEGO
      .w_TIPCON=oParentObject.w_TIPCON
      .w_NUMINI=oParentObject.w_NUMINI
      .w_NUMFIN=oParentObject.w_NUMFIN
      .w_ALFINI=oParentObject.w_ALFINI
      .w_ALFFIN=oParentObject.w_ALFFIN
      .w_NUMDO1=oParentObject.w_NUMDO1
      .w_ALFDO1=oParentObject.w_ALFDO1
      .w_DATDO1=oParentObject.w_DATDO1
      .w_NOTE=oParentObject.w_NOTE
      .w_MVCLADOC=oParentObject.w_MVCLADOC
      .w_CAUSALE=oParentObject.w_CAUSALE
      .w_NOTEPN=oParentObject.w_NOTEPN
      .w_DESTIPDO1=oParentObject.w_DESTIPDO1
      .w_CODCON1=oParentObject.w_CODCON1
      .w_ANDES1=oParentObject.w_ANDES1
      .oPgFrm.Page1.oPag.docu.Calculate()
          .DoRTCalc(1,12,.f.)
        .w_SERIALE = .w_docu.getVar('MVSERIAL')
        .w_NUMDO1 = .w_docu.getVar('MVNUMDOC')
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .w_ALFDO1 = .w_docu.getVar('MVALFDOC')
        .w_DATDO1 = .w_docu.getVar('MVDATDOC')
        .w_NOTE = .w_Docu.getVar('MVDESDOC')
        .w_MVCLADOC = .w_docu.getVar('MVCLADOC')
        .w_CAUSALE = .w_docu.getVar('MVTIPDOC')
        .w_NOTEPN = .w_Docu.getVar('MVNOTEPN')
        .w_DESTIPDO1 = .w_docu.getVar('TDDESDOC ')
        .w_CODCON1 = .w_docu.getVar('MVCODCON')
        .w_ANDES1 = .w_docu.getVar('ANDESCRI')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CAUDOC=.w_CAUDOC
      .oParentObject.w_DOCFOR=.w_DOCFOR
      .oParentObject.w_CODCON=.w_CODCON
      .oParentObject.w_DATINI=.w_DATINI
      .oParentObject.w_DATFIN=.w_DATFIN
      .oParentObject.w_FLVEAC=.w_FLVEAC
      .oParentObject.w_CATEGO=.w_CATEGO
      .oParentObject.w_TIPCON=.w_TIPCON
      .oParentObject.w_NUMINI=.w_NUMINI
      .oParentObject.w_NUMFIN=.w_NUMFIN
      .oParentObject.w_ALFINI=.w_ALFINI
      .oParentObject.w_ALFFIN=.w_ALFFIN
      .oParentObject.w_NUMDO1=.w_NUMDO1
      .oParentObject.w_ALFDO1=.w_ALFDO1
      .oParentObject.w_DATDO1=.w_DATDO1
      .oParentObject.w_NOTE=.w_NOTE
      .oParentObject.w_MVCLADOC=.w_MVCLADOC
      .oParentObject.w_CAUSALE=.w_CAUSALE
      .oParentObject.w_NOTEPN=.w_NOTEPN
      .oParentObject.w_DESTIPDO1=.w_DESTIPDO1
      .oParentObject.w_CODCON1=.w_CODCON1
      .oParentObject.w_ANDES1=.w_ANDES1
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.docu.Calculate()
        .DoRTCalc(1,12,.t.)
            .w_SERIALE = .w_docu.getVar('MVSERIAL')
            .w_NUMDO1 = .w_docu.getVar('MVNUMDOC')
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
            .w_ALFDO1 = .w_docu.getVar('MVALFDOC')
            .w_DATDO1 = .w_docu.getVar('MVDATDOC')
            .w_NOTE = .w_Docu.getVar('MVDESDOC')
            .w_MVCLADOC = .w_docu.getVar('MVCLADOC')
            .w_CAUSALE = .w_docu.getVar('MVTIPDOC')
            .w_NOTEPN = .w_Docu.getVar('MVNOTEPN')
            .w_DESTIPDO1 = .w_docu.getVar('TDDESDOC ')
            .w_CODCON1 = .w_docu.getVar('MVCODCON')
            .w_ANDES1 = .w_docu.getVar('ANDESCRI')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.docu.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.docu.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsve_kv1Pag1 as StdContainer
  Width  = 481
  height = 310
  stdWidth  = 481
  stdheight = 310
  resizeXpos=284
  resizeYpos=200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object docu as cp_zoombox with uid="TRPLKGPBRJ",left=0, top=3, width=476,height=305,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSVE_KVD",bOptions=.t.,bAdvOptions=.t.,bQueryOnLoad=.t.,bReadOnly=.t.,bRetriveAllRows=.f.,cZoomOnZoom="",cMenuFile="",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 129094886


  add object oObj_1_16 as cp_runprogram with uid="PLAQTHLMHF",left=32, top=310, width=214,height=25,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='gsve_bwd("SELEZ")',;
    cEvent = "w_docu selected",;
    nPag=1;
    , HelpContextID = 129094886
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kv1','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
