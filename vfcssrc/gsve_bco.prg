* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bco                                                        *
*              Controlla dati spedizione                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_47]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-10-09                                                      *
* Last revis.: 2017-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bco",oParentObject,m.pOper)
return(i_retval)

define class tgsve_bco as StdBatch
  * --- Local variables
  pOper = space(1)
  w_DATOBSO = ctod("  /  /  ")
  w_CODAGE = space(5)
  w_CODAG2 = space(5)
  w_AGOBSO = ctod("  /  /  ")
  w_SEDAGE = .f.
  w_AGEPR1 = space(5)
  w_SCOPAG = space(1)
  w_OK = .f.
  w_RIFDIC = space(15)
  w_RIFDICH = space(15)
  w_OKMES = .f.
  w_NUMRIG = 0
  w_OK_LET = .f.
  * --- WorkFile variables
  DES_DIVE_idx=0
  AGENTI_idx=0
  CONTI_idx=0
  VOCIIVA_idx=0
  PORTI_idx=0
  MODASPED_idx=0
  VETTORI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo\Riassegnamento dati Spedizione
    do case
      case this.pOper="D"
        * --- da GSVE_MDV, GSOR_MDV, GSAC_MDV
        * --- Read from DES_DIVE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DDDTOBSO,DDCODAGE,DDTIPOPE,DDRFDICH"+;
            " from "+i_cTable+" DES_DIVE where ";
                +"DDTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                +" and DDCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                +" and DDCODDES = "+cp_ToStrODBC(this.oParentObject.w_MVCODDES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DDDTOBSO,DDCODAGE,DDTIPOPE,DDRFDICH;
            from (i_cTable) where;
                DDTIPCON = this.oParentObject.w_MVTIPCON;
                and DDCODICE = this.oParentObject.w_MVCODCON;
                and DDCODDES = this.oParentObject.w_MVCODDES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATOBSO = NVL(cp_ToDate(_read_.DDDTOBSO),cp_NullValue(_read_.DDDTOBSO))
          this.w_CODAGE = NVL(cp_ToDate(_read_.DDCODAGE),cp_NullValue(_read_.DDCODAGE))
          this.oParentObject.w_MVTIPOPE = NVL(cp_ToDate(_read_.DDTIPOPE),cp_NullValue(_read_.DDTIPOPE))
          this.w_RIFDICH = NVL(cp_ToDate(_read_.DDRFDICH),cp_NullValue(_read_.DDRFDICH))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_DATOBSO<=this.oParentObject.w_MVDATDOC AND NOT EMPTY(this.w_DATOBSO)
          ah_ErrorMsg("Attenzione! La sede di consegna specificata � obsoleta")
        endif
        * --- sostituisce l'agente collegato alla sede nei dati di testata
        if this.oParentObject.w_MVTIPCON="C"
          if NOT EMPTY(this.w_CODAGE)
            this.w_SEDAGE = .T.
          else
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANCODAG1"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANCODAG1;
                from (i_cTable) where;
                    ANTIPCON = this.oParentObject.w_MVTIPCON;
                    and ANCODICE = this.oParentObject.w_MVCODCON;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CODAGE = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          * --- Read from AGENTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AGENTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AGCZOAGE,AGDTOBSO,AGCATPRO,AGSCOPAG"+;
              " from "+i_cTable+" AGENTI where ";
                  +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AGCZOAGE,AGDTOBSO,AGCATPRO,AGSCOPAG;
              from (i_cTable) where;
                  AGCODAGE = this.w_CODAGE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODAG2 = NVL(cp_ToDate(_read_.AGCZOAGE),cp_NullValue(_read_.AGCZOAGE))
            this.w_AGOBSO = NVL(cp_ToDate(_read_.AGDTOBSO),cp_NullValue(_read_.AGDTOBSO))
            this.w_AGEPR1 = NVL(cp_ToDate(_read_.AGCATPRO),cp_NullValue(_read_.AGCATPRO))
            this.w_SCOPAG = NVL(cp_ToDate(_read_.AGSCOPAG),cp_NullValue(_read_.AGSCOPAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_AGOBSO>this.oParentObject.w_MVDATDOC OR EMPTY(this.w_AGOBSO) And Not Empty(this.w_CODAGE)
            if this.w_CODAGE<>this.oParentObject.w_MVCODAGE
              if this.w_SEDAGE
                this.w_OKMES = ah_YesNo("Vuoi aggiornare il codice agente con quello associato alla sede di destinazione?")
              else
                this.w_OKMES = ah_YesNo("Vuoi aggiornare il codice agente con quello associato al cliente?")
              endif
              if this.w_OKMES
                this.oParentObject.w_MVCODAGE = this.w_CODAGE
                this.oParentObject.w_MVCODAG2 = this.w_CODAG2
                this.oParentObject.w_AGEPRO = this.w_AGEPR1
                this.oParentObject.w_AGSCOPAG = this.w_SCOPAG
                if this.w_SEDAGE
                  this.oParentObject.NotifyEvent("CalcprovAllRow")
                endif
              endif
            endif
          endif
        endif
        this.w_NUMRIG = this.oparentobject.NumRow ()
        if this.w_NUMRIG > 0 AND (this.oParentObject.w_MVCODDES<>this.oParentObject.o_MVCODDES)
          if AH_YESNO ("Si vuole procedere al ricalcolo delle voci iva presenti nel documento a seguito del cambiamento della sede?")
            do GSVE_BRI with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        * --- Lettura delle desrizioni dei campi perch� non scattano i link
        * --- Read from PORTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PORTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PORTI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PODESPOR"+;
            " from "+i_cTable+" PORTI where ";
                +"POCODPOR = "+cp_ToStrODBC(this.oParentObject.w_MVCODPOR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PODESPOR;
            from (i_cTable) where;
                POCODPOR = this.oParentObject.w_MVCODPOR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESPOR = NVL(cp_ToDate(_read_.PODESPOR),cp_NullValue(_read_.PODESPOR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from MODASPED
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MODASPED_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MODASPED_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SPDESSPE"+;
            " from "+i_cTable+" MODASPED where ";
                +"SPCODSPE = "+cp_ToStrODBC(this.oParentObject.w_MVCODSPE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SPDESSPE;
            from (i_cTable) where;
                SPCODSPE = this.oParentObject.w_MVCODSPE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESSPE = NVL(cp_ToDate(_read_.SPDESSPE),cp_NullValue(_read_.SPDESSPE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from VETTORI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VETTORI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VETTORI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VTDESVET"+;
            " from "+i_cTable+" VETTORI where ";
                +"VTCODVET = "+cp_ToStrODBC(this.oParentObject.w_MVCODVET);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VTDESVET;
            from (i_cTable) where;
                VTCODVET = this.oParentObject.w_MVCODVET;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESVET = NVL(cp_ToDate(_read_.VTDESVET),cp_NullValue(_read_.VTDESVET))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from VETTORI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VETTORI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VETTORI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VTDESVET"+;
            " from "+i_cTable+" VETTORI where ";
                +"VTCODVET = "+cp_ToStrODBC(this.oParentObject.w_MVCODVE2);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VTDESVET;
            from (i_cTable) where;
                VTCODVET = this.oParentObject.w_MVCODVE2;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESVE2 = NVL(cp_ToDate(_read_.VTDESVET),cp_NullValue(_read_.VTDESVET))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from VETTORI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VETTORI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VETTORI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VTDESVET"+;
            " from "+i_cTable+" VETTORI where ";
                +"VTCODVET = "+cp_ToStrODBC(this.oParentObject.w_MVCODVE3);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VTDESVET;
            from (i_cTable) where;
                VTCODVET = this.oParentObject.w_MVCODVE3;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESVE3 = NVL(cp_ToDate(_read_.VTDESVET),cp_NullValue(_read_.VTDESVET))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.oParentObject.w_MVTIPCON $ "CF"
          * --- Verifica se esiste una Dichiarazione di Intento Valida
          this.w_OK = .F.
          this.w_RIFDIC = SPACE(10)
          if this.oParentObject.w_MVCODDES<>this.oParentObject.o_MVCODDES 
            * --- Lettura lettera di intento valida
            DECLARE ARRDIC (14,1)
            * --- Azzero l'Array che verr� riempito dalla Funzione
            ARRDIC(1)=0
            this.w_OK_LET = CAL_LETT(this.oParentObject.w_MVDATDOC,this.oParentObject.w_MVTIPCON,this.oParentObject.w_XCONORN, @ArrDic, this.oParentObject.w_MVRIFDIC,this.oParentObject.w_MVCODDES)
            if this.w_OK_LET
              * --- Parametri
              *     pDatRif : Data di Riferimento per filtro su Lettere di intento
              *     pTipCon : Tipo Conto : 'C' Clienti, 'F' : Fornitori
              *     pCodCon : Codice Cliente/Fornitore
              *     pArrDic : Array passato per riferimento: conterr� anche i dati letti dalla lettera di intento
              *     
              *     pArrDic[ 1 ]   = Numero Dichiarazione
              *     pArrDic[ 2 ]   = Tipo Operazione
              *     pArrDic[ 3 ]   = Anno Dichiarazione
              *     pArrDic[ 4 ]   = Importo Dichiarazione
              *     pArrDic[ 5 ]   = Data Dichiarazione
              *     pArrDic[ 6 ]   = Importo Utilizzato
              *     pArrDic[ 7 ]   = Tipo conto: Cliente/Fornitore
              *     pArrDic[ 8 ]   = Codice Iva Agevolata
              *     pArrDic[ 9 ]   = Tipo Iva (Agevolata o senza)
              *     pArrDic[ 10 ] = Data Inizio Validit�
              *     pArrDic[ 11 ] = Codice Dichiarazione (Se a clienti = codice Cliente, Se Fornitori= codice progressivo)
              *     pArrDic[ 12 ] = Data Obsolescenza
              this.w_RIFDIC = ArrDic(11)
              if this.w_RIFDIC <> this.oParentObject.w_MVRIFDIC AND ah_YesNo("Vuoi aggiornare la lettera di intento?")
                * --- Se Ok riporta i dati dell'Esenzione
                this.oParentObject.w_MVRIFDIC = this.w_RIFDIC
                this.oParentObject.w_NUMDIC = ArrDic(1)
                this.oParentObject.w_ANNDIC = ArrDic(3)
                this.oParentObject.w_DATDIC = ArrDic(5)
                this.oParentObject.w_TIPDIC = ArrDic(7)
                this.oParentObject.w_OPEDIC = ArrDic(2)
                this.oParentObject.w_UTIDIC = ArrDic(6)
                this.oParentObject.w_TIPIVA = ArrDic(9)
                this.oParentObject.w_MVCODIVE = ArrDic(8)
                this.oParentObject.w_ALFDIC = ArrDic(13)
                * --- Read from VOCIIVA
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.VOCIIVA_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "IVPERIVA,IVBOLIVA,IVPERIND"+;
                    " from "+i_cTable+" VOCIIVA where ";
                        +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MVCODIVE);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    IVPERIVA,IVBOLIVA,IVPERIND;
                    from (i_cTable) where;
                        IVCODIVA = this.oParentObject.w_MVCODIVE;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  w_PERIVE = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
                  w_BOLIVE = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
                  w_LINDIVE = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
            endif
          endif
        endif
      case this.pOper="P"
        * --- da GSMA_MPK
        * --- Legge anche il tipo operazione IVA
        * --- Read from DES_DIVE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DDDTOBSO"+;
            " from "+i_cTable+" DES_DIVE where ";
                +"DDTIPCON = "+cp_ToStrODBC(this.oParentObject.w_PLTIPCON);
                +" and DDCODICE = "+cp_ToStrODBC(this.oParentObject.w_PLCODCON);
                +" and DDCODDES = "+cp_ToStrODBC(this.oParentObject.w_PLCODDES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DDDTOBSO;
            from (i_cTable) where;
                DDTIPCON = this.oParentObject.w_PLTIPCON;
                and DDCODICE = this.oParentObject.w_PLCODCON;
                and DDCODDES = this.oParentObject.w_PLCODDES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DATOBSO = NVL(cp_ToDate(_read_.DDDTOBSO),cp_NullValue(_read_.DDDTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_DATOBSO<=this.oParentObject.w_OBTEST AND NOT EMPTY(this.w_DATOBSO)
          ah_ErrorMsg("Attenzione! La sede di consegna specificata � obsoleta")
        endif
      case this.pOper="A"
        * --- da GSVE_MDV, GSOR_MDV, GSAC_MDV
        *     Esegue Traduzione Aspetto Esteriore dei Beni
        * --- Legge la Traduzione
        DECLARE ARRRIS (2) 
 a=Tradlin(this.oParentObject.w_MVCODASP,this.oParentObject.w_CODLIN,"TRADASPE",@ARRRIS,"B")
        if Not Empty(ARRRIS(1))
          this.oParentObject.w_MVASPEST = ARRRIS(1)
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='DES_DIVE'
    this.cWorkTables[2]='AGENTI'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='VOCIIVA'
    this.cWorkTables[5]='PORTI'
    this.cWorkTables[6]='MODASPED'
    this.cWorkTables[7]='VETTORI'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
