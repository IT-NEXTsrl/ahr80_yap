* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_aid                                                        *
*              Dettaglio inventari                                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_184]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-28                                                      *
* Last revis.: 2015-03-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_aid"))

* --- Class definition
define class tgsma_aid as StdForm
  Top    = 15
  Left   = 10

  * --- Standard Properties
  Width  = 639
  Height = 354+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-19"
  HelpContextID=9075561
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=46

  * --- Constant Properties
  INVEDETT_IDX = 0
  MAGAZZIN_IDX = 0
  ART_ICOL_IDX = 0
  PAR_RIOR_IDX = 0
  FIFOCONT_IDX = 0
  LIFOCONT_IDX = 0
  LIFOSCAT_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  INVENTAR_IDX = 0
  cFile = "INVEDETT"
  cKeySelect = "DINUMINV,DICODESE,DICODICE"
  cKeyWhere  = "DINUMINV=this.w_DINUMINV and DICODESE=this.w_DICODESE and DICODICE=this.w_DICODICE"
  cKeyWhereODBC = '"DINUMINV="+cp_ToStrODBC(this.w_DINUMINV)';
      +'+" and DICODESE="+cp_ToStrODBC(this.w_DICODESE)';
      +'+" and DICODICE="+cp_ToStrODBC(this.w_DICODICE)';

  cKeyWhereODBCqualified = '"INVEDETT.DINUMINV="+cp_ToStrODBC(this.w_DINUMINV)';
      +'+" and INVEDETT.DICODESE="+cp_ToStrODBC(this.w_DICODESE)';
      +'+" and INVEDETT.DICODICE="+cp_ToStrODBC(this.w_DICODICE)';

  cPrg = "gsma_aid"
  cComment = "Dettaglio inventari"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DINUMINV = space(6)
  w_AZIENDA = space(5)
  w_DICODESE = space(4)
  o_DICODESE = space(4)
  w_VALNAZ = space(3)
  w_DECUNI = 0
  w_CALCPIU = 0
  w_DICODICE = space(20)
  w_TIPO = space(2)
  w_ARDESART = space(40)
  w_ARTIPART = space(2)
  w_DIQTAESI = 0
  w_DIQTAVEN = 0
  w_DIQTAACQ = 0
  w_DIQTAESR = 0
  w_DIQTAVER = 0
  w_DIQTAACR = 0
  w_DIPRZMPA = 0
  w_DIPRZMPP = 0
  w_DIPRZULT = 0
  w_DICOSMPA = 0
  w_DICOSMPP = 0
  w_DICOSULT = 0
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_ARUNMIS1 = space(3)
  w_ARUNMIS1 = space(3)
  w_ARUNMIS1 = space(3)
  w_DICOSSTA = 0
  w_DICODMAG = space(5)
  w_DICOSFCO = 0
  w_DICOSLCO = 0
  w_DICOSLSC = 0
  w_ARUNMIS1 = space(3)
  w_ARUNMIS1 = space(3)
  w_ARUNMIS1 = space(3)
  w_RESCHK = 0
  w_LEGGI = space(4)
  w_STAINV = space(1)
  w_DIQTACVR = 0
  w_DIQTASVR = 0
  w_ARUNMIS1 = space(3)
  w_ARUNMIS1 = space(3)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')

  * --- Children pointers
  gsma_mif = .NULL.
  gsma_mil = .NULL.
  gsma_mis = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsma_aid
  l_NumInv = space(6)
  l_CodEse = space(4)
  
  proc ecpZoom()
     * -- Disabilito l'elenco
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'INVEDETT','gsma_aid')
    stdPageFrame::Init()
    *set procedure to gsma_mif additive
    *set procedure to gsma_mil additive
    *set procedure to gsma_mis additive
    with this
      .Pages(1).addobject("oPag","tgsma_aidPag1","gsma_aid",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dettaglio inventari")
      .Pages(1).HelpContextID = 165473811
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDICODICE_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure gsma_mif
    *release procedure gsma_mil
    *release procedure gsma_mis
    * --- Area Manuale = Init Page Frame
    * --- gsma_aid
      * Nascondo le tabs in vecchia configurazione interfaccia
    *non � stato messo in blanck Record End perch� in modifica i campi sono editabili
    if i_cMenuTab<>"T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[10]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='PAR_RIOR'
    this.cWorkTables[4]='FIFOCONT'
    this.cWorkTables[5]='LIFOCONT'
    this.cWorkTables[6]='LIFOSCAT'
    this.cWorkTables[7]='ESERCIZI'
    this.cWorkTables[8]='VALUTE'
    this.cWorkTables[9]='INVENTAR'
    this.cWorkTables[10]='INVEDETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(10))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.INVEDETT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.INVEDETT_IDX,3]
  return

  function CreateChildren()
    this.gsma_mif = CREATEOBJECT('stdLazyChild',this,'gsma_mif')
    this.gsma_mil = CREATEOBJECT('stdLazyChild',this,'gsma_mil')
    this.gsma_mis = CREATEOBJECT('stdLazyChild',this,'gsma_mis')
    return

  procedure DestroyChildren()
    if !ISNULL(this.gsma_mif)
      this.gsma_mif.DestroyChildrenChain()
      this.gsma_mif=.NULL.
    endif
    if !ISNULL(this.gsma_mil)
      this.gsma_mil.DestroyChildrenChain()
      this.gsma_mil=.NULL.
    endif
    if !ISNULL(this.gsma_mis)
      this.gsma_mis.DestroyChildrenChain()
      this.gsma_mis=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.gsma_mif.IsAChildUpdated()
      i_bRes = i_bRes .or. this.gsma_mil.IsAChildUpdated()
      i_bRes = i_bRes .or. this.gsma_mis.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.gsma_mif.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.gsma_mil.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.gsma_mis.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.gsma_mif.NewDocument()
    this.gsma_mil.NewDocument()
    this.gsma_mis.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.gsma_mif.SetKey(;
            .w_DINUMINV,"ISNUMINV";
            ,.w_DICODESE,"ISCODESE";
            ,.w_DICODICE,"ISCODICE";
            )
      this.gsma_mil.SetKey(;
            .w_DINUMINV,"ISNUMINV";
            ,.w_DICODESE,"ISCODESE";
            ,.w_DICODICE,"ISCODICE";
            )
      this.gsma_mis.SetKey(;
            .w_DINUMINV,"ISNUMINV";
            ,.w_DICODESE,"ISCODESE";
            ,.w_DICODICE,"ISCODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .gsma_mif.ChangeRow(this.cRowID+'      1',1;
             ,.w_DINUMINV,"ISNUMINV";
             ,.w_DICODESE,"ISCODESE";
             ,.w_DICODICE,"ISCODICE";
             )
      .gsma_mil.ChangeRow(this.cRowID+'      1',1;
             ,.w_DINUMINV,"ISNUMINV";
             ,.w_DICODESE,"ISCODESE";
             ,.w_DICODICE,"ISCODICE";
             )
      .gsma_mis.ChangeRow(this.cRowID+'      1',1;
             ,.w_DINUMINV,"ISNUMINV";
             ,.w_DICODESE,"ISCODESE";
             ,.w_DICODICE,"ISCODICE";
             )
    endwith
    return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DINUMINV = NVL(DINUMINV,space(6))
      .w_DICODESE = NVL(DICODESE,space(4))
      .w_DICODICE = NVL(DICODICE,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_7_joined
    link_1_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- gsma_aid
    * ---  se entro la prima volta non attiva il Bottone Varia
    this.SetCPToolBar()
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from INVEDETT where DINUMINV=KeySet.DINUMINV
    *                            and DICODESE=KeySet.DICODESE
    *                            and DICODICE=KeySet.DICODICE
    *
    i_nConn = i_TableProp[this.INVEDETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INVEDETT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('INVEDETT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "INVEDETT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' INVEDETT '
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DINUMINV',this.w_DINUMINV  ,'DICODESE',this.w_DICODESE  ,'DICODICE',this.w_DICODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AZIENDA = i_CODAZI
        .w_VALNAZ = space(3)
        .w_DECUNI = 0
        .w_TIPO = space(2)
        .w_ARDESART = space(40)
        .w_ARTIPART = space(2)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_ARUNMIS1 = space(3)
        .w_ARUNMIS1 = space(3)
        .w_ARUNMIS1 = space(3)
        .w_DICODMAG = space(5)
        .w_ARUNMIS1 = space(3)
        .w_ARUNMIS1 = space(3)
        .w_ARUNMIS1 = space(3)
        .w_RESCHK = 0
        .w_STAINV = space(1)
        .w_ARUNMIS1 = space(3)
        .w_ARUNMIS1 = space(3)
        .w_DINUMINV = NVL(DINUMINV,space(6))
        .w_DICODESE = NVL(DICODESE,space(4))
          .link_1_3('Load')
          .link_1_4('Load')
        .w_CALCPIU = DEFPIU(.w_DECUNI)
        .w_DICODICE = NVL(DICODICE,space(20))
          if link_1_7_joined
            this.w_DICODICE = NVL(ARCODART107,NVL(this.w_DICODICE,space(20)))
            this.w_ARDESART = NVL(ARDESART107,space(40))
            this.w_ARUNMIS1 = NVL(ARUNMIS1107,space(3))
            this.w_TIPO = NVL(ARTIPART107,space(2))
            this.w_DATOBSO = NVL(cp_ToDate(ARDTOBSO107),ctod("  /  /  "))
          else
          .link_1_7('Load')
          endif
        .w_DIQTAESI = NVL(DIQTAESI,0)
        .w_DIQTAVEN = NVL(DIQTAVEN,0)
        .w_DIQTAACQ = NVL(DIQTAACQ,0)
        .w_DIQTAESR = NVL(DIQTAESR,0)
        .w_DIQTAVER = NVL(DIQTAVER,0)
        .w_DIQTAACR = NVL(DIQTAACR,0)
        .w_DIPRZMPA = NVL(DIPRZMPA,0)
        .w_DIPRZMPP = NVL(DIPRZMPP,0)
        .w_DIPRZULT = NVL(DIPRZULT,0)
        .w_DICOSMPA = NVL(DICOSMPA,0)
        .w_DICOSMPP = NVL(DICOSMPP,0)
        .w_DICOSULT = NVL(DICOSULT,0)
        .w_DICOSSTA = NVL(DICOSSTA,0)
        .w_DICOSFCO = NVL(DICOSFCO,0)
        .w_DICOSLCO = NVL(DICOSLCO,0)
        .w_DICOSLSC = NVL(DICOSLSC,0)
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .w_LEGGI = .w_DICODESE
          .link_1_63('Load')
        .w_DIQTACVR = NVL(DIQTACVR,0)
        .w_DIQTASVR = NVL(DIQTASVR,0)
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        cp_LoadRecExtFlds(this,'INVEDETT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsma_aid
    this.l_NumInv=this.w_DINUMINV
    this.l_CodEse=this.w_DICODESE
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DINUMINV = space(6)
      .w_AZIENDA = space(5)
      .w_DICODESE = space(4)
      .w_VALNAZ = space(3)
      .w_DECUNI = 0
      .w_CALCPIU = 0
      .w_DICODICE = space(20)
      .w_TIPO = space(2)
      .w_ARDESART = space(40)
      .w_ARTIPART = space(2)
      .w_DIQTAESI = 0
      .w_DIQTAVEN = 0
      .w_DIQTAACQ = 0
      .w_DIQTAESR = 0
      .w_DIQTAVER = 0
      .w_DIQTAACR = 0
      .w_DIPRZMPA = 0
      .w_DIPRZMPP = 0
      .w_DIPRZULT = 0
      .w_DICOSMPA = 0
      .w_DICOSMPP = 0
      .w_DICOSULT = 0
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_ARUNMIS1 = space(3)
      .w_ARUNMIS1 = space(3)
      .w_ARUNMIS1 = space(3)
      .w_DICOSSTA = 0
      .w_DICODMAG = space(5)
      .w_DICOSFCO = 0
      .w_DICOSLCO = 0
      .w_DICOSLSC = 0
      .w_ARUNMIS1 = space(3)
      .w_ARUNMIS1 = space(3)
      .w_ARUNMIS1 = space(3)
      .w_RESCHK = 0
      .w_LEGGI = space(4)
      .w_STAINV = space(1)
      .w_DIQTACVR = 0
      .w_DIQTASVR = 0
      .w_ARUNMIS1 = space(3)
      .w_ARUNMIS1 = space(3)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
        .w_DINUMINV = .l_NumInv
        .w_AZIENDA = i_CODAZI
        .w_DICODESE = .l_CodEse
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_DICODESE))
          .link_1_3('Full')
          endif
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_VALNAZ))
          .link_1_4('Full')
          endif
          .DoRTCalc(5,5,.f.)
        .w_CALCPIU = DEFPIU(.w_DECUNI)
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_DICODICE))
          .link_1_7('Full')
          endif
          .DoRTCalc(8,22,.f.)
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
          .DoRTCalc(24,36,.f.)
        .w_LEGGI = .w_DICODESE
        .DoRTCalc(37,37,.f.)
          if not(empty(.w_LEGGI))
          .link_1_63('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'INVEDETT')
    this.DoRTCalc(38,46,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsma_aid
    * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDICODICE_1_7.enabled = i_bVal
      .Page1.oPag.oDIQTAESI_1_12.enabled = i_bVal
      .Page1.oPag.oDIQTAVEN_1_14.enabled = i_bVal
      .Page1.oPag.oDIQTAACQ_1_16.enabled = i_bVal
      .Page1.oPag.oDIQTAESR_1_18.enabled = i_bVal
      .Page1.oPag.oDIQTAVER_1_19.enabled = i_bVal
      .Page1.oPag.oDIQTAACR_1_20.enabled = i_bVal
      .Page1.oPag.oDIPRZMPA_1_21.enabled = i_bVal
      .Page1.oPag.oDIPRZMPP_1_23.enabled = i_bVal
      .Page1.oPag.oDIPRZULT_1_25.enabled = i_bVal
      .Page1.oPag.oDICOSMPA_1_27.enabled = i_bVal
      .Page1.oPag.oDICOSMPP_1_29.enabled = i_bVal
      .Page1.oPag.oDICOSULT_1_31.enabled = i_bVal
      .Page1.oPag.oDICOSSTA_1_49.enabled = i_bVal
      .Page1.oPag.oDICOSFCO_1_51.enabled = i_bVal
      .Page1.oPag.oDICOSLCO_1_53.enabled = i_bVal
      .Page1.oPag.oDICOSLSC_1_55.enabled = i_bVal
      .Page1.oPag.oDIQTACVR_1_65.enabled = i_bVal
      .Page1.oPag.oDIQTASVR_1_66.enabled = i_bVal
      .Page1.oPag.oObj_1_61.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oDICODICE_1_7.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oDICODICE_1_7.enabled = .t.
      endif
    endwith
    this.gsma_mif.SetStatus(i_cOp)
    this.gsma_mil.SetStatus(i_cOp)
    this.gsma_mis.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'INVEDETT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.gsma_mif.SetChildrenStatus(i_cOp)
  *  this.gsma_mil.SetChildrenStatus(i_cOp)
  *  this.gsma_mis.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.INVEDETT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DINUMINV,"DINUMINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODESE,"DICODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICODICE,"DICODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIQTAESI,"DIQTAESI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIQTAVEN,"DIQTAVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIQTAACQ,"DIQTAACQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIQTAESR,"DIQTAESR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIQTAVER,"DIQTAVER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIQTAACR,"DIQTAACR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIPRZMPA,"DIPRZMPA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIPRZMPP,"DIPRZMPP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIPRZULT,"DIPRZULT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICOSMPA,"DICOSMPA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICOSMPP,"DICOSMPP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICOSULT,"DICOSULT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICOSSTA,"DICOSSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICOSFCO,"DICOSFCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICOSLCO,"DICOSLCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICOSLSC,"DICOSLSC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIQTACVR,"DIQTACVR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DIQTASVR,"DIQTASVR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.INVEDETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INVEDETT_IDX,2])
    i_lTable = "INVEDETT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.INVEDETT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.INVEDETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INVEDETT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.INVEDETT_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into INVEDETT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'INVEDETT')
        i_extval=cp_InsertValODBCExtFlds(this,'INVEDETT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DINUMINV,DICODESE,DICODICE,DIQTAESI,DIQTAVEN"+;
                  ",DIQTAACQ,DIQTAESR,DIQTAVER,DIQTAACR,DIPRZMPA"+;
                  ",DIPRZMPP,DIPRZULT,DICOSMPA,DICOSMPP,DICOSULT"+;
                  ",DICOSSTA,DICOSFCO,DICOSLCO,DICOSLSC,DIQTACVR"+;
                  ",DIQTASVR,UTCC,UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DINUMINV)+;
                  ","+cp_ToStrODBCNull(this.w_DICODESE)+;
                  ","+cp_ToStrODBCNull(this.w_DICODICE)+;
                  ","+cp_ToStrODBC(this.w_DIQTAESI)+;
                  ","+cp_ToStrODBC(this.w_DIQTAVEN)+;
                  ","+cp_ToStrODBC(this.w_DIQTAACQ)+;
                  ","+cp_ToStrODBC(this.w_DIQTAESR)+;
                  ","+cp_ToStrODBC(this.w_DIQTAVER)+;
                  ","+cp_ToStrODBC(this.w_DIQTAACR)+;
                  ","+cp_ToStrODBC(this.w_DIPRZMPA)+;
                  ","+cp_ToStrODBC(this.w_DIPRZMPP)+;
                  ","+cp_ToStrODBC(this.w_DIPRZULT)+;
                  ","+cp_ToStrODBC(this.w_DICOSMPA)+;
                  ","+cp_ToStrODBC(this.w_DICOSMPP)+;
                  ","+cp_ToStrODBC(this.w_DICOSULT)+;
                  ","+cp_ToStrODBC(this.w_DICOSSTA)+;
                  ","+cp_ToStrODBC(this.w_DICOSFCO)+;
                  ","+cp_ToStrODBC(this.w_DICOSLCO)+;
                  ","+cp_ToStrODBC(this.w_DICOSLSC)+;
                  ","+cp_ToStrODBC(this.w_DIQTACVR)+;
                  ","+cp_ToStrODBC(this.w_DIQTASVR)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'INVEDETT')
        i_extval=cp_InsertValVFPExtFlds(this,'INVEDETT')
        cp_CheckDeletedKey(i_cTable,0,'DINUMINV',this.w_DINUMINV,'DICODESE',this.w_DICODESE,'DICODICE',this.w_DICODICE)
        INSERT INTO (i_cTable);
              (DINUMINV,DICODESE,DICODICE,DIQTAESI,DIQTAVEN,DIQTAACQ,DIQTAESR,DIQTAVER,DIQTAACR,DIPRZMPA,DIPRZMPP,DIPRZULT,DICOSMPA,DICOSMPP,DICOSULT,DICOSSTA,DICOSFCO,DICOSLCO,DICOSLSC,DIQTACVR,DIQTASVR,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DINUMINV;
                  ,this.w_DICODESE;
                  ,this.w_DICODICE;
                  ,this.w_DIQTAESI;
                  ,this.w_DIQTAVEN;
                  ,this.w_DIQTAACQ;
                  ,this.w_DIQTAESR;
                  ,this.w_DIQTAVER;
                  ,this.w_DIQTAACR;
                  ,this.w_DIPRZMPA;
                  ,this.w_DIPRZMPP;
                  ,this.w_DIPRZULT;
                  ,this.w_DICOSMPA;
                  ,this.w_DICOSMPP;
                  ,this.w_DICOSULT;
                  ,this.w_DICOSSTA;
                  ,this.w_DICOSFCO;
                  ,this.w_DICOSLCO;
                  ,this.w_DICOSLSC;
                  ,this.w_DIQTACVR;
                  ,this.w_DIQTASVR;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.INVEDETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INVEDETT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.INVEDETT_IDX,i_nConn)
      *
      * update INVEDETT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'INVEDETT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DIQTAESI="+cp_ToStrODBC(this.w_DIQTAESI)+;
             ",DIQTAVEN="+cp_ToStrODBC(this.w_DIQTAVEN)+;
             ",DIQTAACQ="+cp_ToStrODBC(this.w_DIQTAACQ)+;
             ",DIQTAESR="+cp_ToStrODBC(this.w_DIQTAESR)+;
             ",DIQTAVER="+cp_ToStrODBC(this.w_DIQTAVER)+;
             ",DIQTAACR="+cp_ToStrODBC(this.w_DIQTAACR)+;
             ",DIPRZMPA="+cp_ToStrODBC(this.w_DIPRZMPA)+;
             ",DIPRZMPP="+cp_ToStrODBC(this.w_DIPRZMPP)+;
             ",DIPRZULT="+cp_ToStrODBC(this.w_DIPRZULT)+;
             ",DICOSMPA="+cp_ToStrODBC(this.w_DICOSMPA)+;
             ",DICOSMPP="+cp_ToStrODBC(this.w_DICOSMPP)+;
             ",DICOSULT="+cp_ToStrODBC(this.w_DICOSULT)+;
             ",DICOSSTA="+cp_ToStrODBC(this.w_DICOSSTA)+;
             ",DICOSFCO="+cp_ToStrODBC(this.w_DICOSFCO)+;
             ",DICOSLCO="+cp_ToStrODBC(this.w_DICOSLCO)+;
             ",DICOSLSC="+cp_ToStrODBC(this.w_DICOSLSC)+;
             ",DIQTACVR="+cp_ToStrODBC(this.w_DIQTACVR)+;
             ",DIQTASVR="+cp_ToStrODBC(this.w_DIQTASVR)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'INVEDETT')
        i_cWhere = cp_PKFox(i_cTable  ,'DINUMINV',this.w_DINUMINV  ,'DICODESE',this.w_DICODESE  ,'DICODICE',this.w_DICODICE  )
        UPDATE (i_cTable) SET;
              DIQTAESI=this.w_DIQTAESI;
             ,DIQTAVEN=this.w_DIQTAVEN;
             ,DIQTAACQ=this.w_DIQTAACQ;
             ,DIQTAESR=this.w_DIQTAESR;
             ,DIQTAVER=this.w_DIQTAVER;
             ,DIQTAACR=this.w_DIQTAACR;
             ,DIPRZMPA=this.w_DIPRZMPA;
             ,DIPRZMPP=this.w_DIPRZMPP;
             ,DIPRZULT=this.w_DIPRZULT;
             ,DICOSMPA=this.w_DICOSMPA;
             ,DICOSMPP=this.w_DICOSMPP;
             ,DICOSULT=this.w_DICOSULT;
             ,DICOSSTA=this.w_DICOSSTA;
             ,DICOSFCO=this.w_DICOSFCO;
             ,DICOSLCO=this.w_DICOSLCO;
             ,DICOSLSC=this.w_DICOSLSC;
             ,DIQTACVR=this.w_DIQTACVR;
             ,DIQTASVR=this.w_DIQTASVR;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- gsma_mif : Saving
      this.gsma_mif.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DINUMINV,"ISNUMINV";
             ,this.w_DICODESE,"ISCODESE";
             ,this.w_DICODICE,"ISCODICE";
             )
      this.gsma_mif.mReplace()
      * --- gsma_mil : Saving
      this.gsma_mil.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DINUMINV,"ISNUMINV";
             ,this.w_DICODESE,"ISCODESE";
             ,this.w_DICODICE,"ISCODICE";
             )
      this.gsma_mil.mReplace()
      * --- gsma_mis : Saving
      this.gsma_mis.ChangeRow(this.cRowID+'      1',0;
             ,this.w_DINUMINV,"ISNUMINV";
             ,this.w_DICODESE,"ISCODESE";
             ,this.w_DICODICE,"ISCODICE";
             )
      this.gsma_mis.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- gsma_mif : Deleting
    this.gsma_mif.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DINUMINV,"ISNUMINV";
           ,this.w_DICODESE,"ISCODESE";
           ,this.w_DICODICE,"ISCODICE";
           )
    this.gsma_mif.mDelete()
    * --- gsma_mil : Deleting
    this.gsma_mil.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DINUMINV,"ISNUMINV";
           ,this.w_DICODESE,"ISCODESE";
           ,this.w_DICODICE,"ISCODICE";
           )
    this.gsma_mil.mDelete()
    * --- gsma_mis : Deleting
    this.gsma_mis.ChangeRow(this.cRowID+'      1',0;
           ,this.w_DINUMINV,"ISNUMINV";
           ,this.w_DICODESE,"ISCODESE";
           ,this.w_DICODICE,"ISCODICE";
           )
    this.gsma_mis.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.INVEDETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.INVEDETT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.INVEDETT_IDX,i_nConn)
      *
      * delete INVEDETT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DINUMINV',this.w_DINUMINV  ,'DICODESE',this.w_DICODESE  ,'DICODICE',this.w_DICODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.INVEDETT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.INVEDETT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_DICODESE<>.w_DICODESE
          .link_1_3('Full')
        endif
          .link_1_4('Full')
        .DoRTCalc(5,5,.t.)
            .w_CALCPIU = DEFPIU(.w_DECUNI)
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .DoRTCalc(7,36,.t.)
            .w_LEGGI = .w_DICODESE
          .link_1_63('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(38,46,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_61.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DICODESE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_DICODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_DICODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_VALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_DICODESE = space(4)
      endif
      this.w_VALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALNAZ
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALNAZ)
            select VACODVAL,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALNAZ = space(3)
      endif
      this.w_DECUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICODICE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_DICODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARTIPART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_DICODICE))
          select ARCODART,ARDESART,ARUNMIS1,ARTIPART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODICE)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_DICODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARTIPART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_DICODICE)+"%");

            select ARCODART,ARDESART,ARUNMIS1,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DICODICE) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oDICODICE_1_7'),i_cWhere,'GSMA_BZA',"Articoli",'GSMA0ADI.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARTIPART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARUNMIS1,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1,ARTIPART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_DICODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_DICODICE)
            select ARCODART,ARDESART,ARUNMIS1,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODICE = NVL(_Link_.ARCODART,space(20))
      this.w_ARDESART = NVL(_Link_.ARDESART,space(40))
      this.w_ARUNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_TIPO = NVL(_Link_.ARTIPART,space(2))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DICODICE = space(20)
      endif
      this.w_ARDESART = space(40)
      this.w_ARUNMIS1 = space(3)
      this.w_TIPO = space(2)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO $ 'PF-SE-MP-PH-MC-MA-IM-FS'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice di tipo non articolo oppure codice obsoleto o inesistente")
        endif
        this.w_DICODICE = space(20)
        this.w_ARDESART = space(40)
        this.w_ARUNMIS1 = space(3)
        this.w_TIPO = space(2)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.ARCODART as ARCODART107"+ ",link_1_7.ARDESART as ARDESART107"+ ",link_1_7.ARUNMIS1 as ARUNMIS1107"+ ",link_1_7.ARTIPART as ARTIPART107"+ ",link_1_7.ARDTOBSO as ARDTOBSO107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on INVEDETT.DICODICE=link_1_7.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and INVEDETT.DICODICE=link_1_7.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LEGGI
  func Link_1_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LEGGI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LEGGI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INNUMINV,INCODESE,INSTAINV";
                   +" from "+i_cTable+" "+i_lTable+" where INCODESE="+cp_ToStrODBC(this.w_LEGGI);
                   +" and INNUMINV="+cp_ToStrODBC(this.w_DINUMINV);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INNUMINV',this.w_DINUMINV;
                       ,'INCODESE',this.w_LEGGI)
            select INNUMINV,INCODESE,INSTAINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LEGGI = NVL(_Link_.INCODESE,space(4))
      this.w_STAINV = NVL(_Link_.INSTAINV,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_LEGGI = space(4)
      endif
      this.w_STAINV = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INNUMINV,1)+'\'+cp_ToStr(_Link_.INCODESE,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LEGGI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDICODICE_1_7.value==this.w_DICODICE)
      this.oPgFrm.Page1.oPag.oDICODICE_1_7.value=this.w_DICODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oARDESART_1_9.value==this.w_ARDESART)
      this.oPgFrm.Page1.oPag.oARDESART_1_9.value=this.w_ARDESART
    endif
    if not(this.oPgFrm.Page1.oPag.oDIQTAESI_1_12.value==this.w_DIQTAESI)
      this.oPgFrm.Page1.oPag.oDIQTAESI_1_12.value=this.w_DIQTAESI
    endif
    if not(this.oPgFrm.Page1.oPag.oDIQTAVEN_1_14.value==this.w_DIQTAVEN)
      this.oPgFrm.Page1.oPag.oDIQTAVEN_1_14.value=this.w_DIQTAVEN
    endif
    if not(this.oPgFrm.Page1.oPag.oDIQTAACQ_1_16.value==this.w_DIQTAACQ)
      this.oPgFrm.Page1.oPag.oDIQTAACQ_1_16.value=this.w_DIQTAACQ
    endif
    if not(this.oPgFrm.Page1.oPag.oDIQTAESR_1_18.value==this.w_DIQTAESR)
      this.oPgFrm.Page1.oPag.oDIQTAESR_1_18.value=this.w_DIQTAESR
    endif
    if not(this.oPgFrm.Page1.oPag.oDIQTAVER_1_19.value==this.w_DIQTAVER)
      this.oPgFrm.Page1.oPag.oDIQTAVER_1_19.value=this.w_DIQTAVER
    endif
    if not(this.oPgFrm.Page1.oPag.oDIQTAACR_1_20.value==this.w_DIQTAACR)
      this.oPgFrm.Page1.oPag.oDIQTAACR_1_20.value=this.w_DIQTAACR
    endif
    if not(this.oPgFrm.Page1.oPag.oDIPRZMPA_1_21.value==this.w_DIPRZMPA)
      this.oPgFrm.Page1.oPag.oDIPRZMPA_1_21.value=this.w_DIPRZMPA
    endif
    if not(this.oPgFrm.Page1.oPag.oDIPRZMPP_1_23.value==this.w_DIPRZMPP)
      this.oPgFrm.Page1.oPag.oDIPRZMPP_1_23.value=this.w_DIPRZMPP
    endif
    if not(this.oPgFrm.Page1.oPag.oDIPRZULT_1_25.value==this.w_DIPRZULT)
      this.oPgFrm.Page1.oPag.oDIPRZULT_1_25.value=this.w_DIPRZULT
    endif
    if not(this.oPgFrm.Page1.oPag.oDICOSMPA_1_27.value==this.w_DICOSMPA)
      this.oPgFrm.Page1.oPag.oDICOSMPA_1_27.value=this.w_DICOSMPA
    endif
    if not(this.oPgFrm.Page1.oPag.oDICOSMPP_1_29.value==this.w_DICOSMPP)
      this.oPgFrm.Page1.oPag.oDICOSMPP_1_29.value=this.w_DICOSMPP
    endif
    if not(this.oPgFrm.Page1.oPag.oDICOSULT_1_31.value==this.w_DICOSULT)
      this.oPgFrm.Page1.oPag.oDICOSULT_1_31.value=this.w_DICOSULT
    endif
    if not(this.oPgFrm.Page1.oPag.oARUNMIS1_1_46.value==this.w_ARUNMIS1)
      this.oPgFrm.Page1.oPag.oARUNMIS1_1_46.value=this.w_ARUNMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oARUNMIS1_1_47.value==this.w_ARUNMIS1)
      this.oPgFrm.Page1.oPag.oARUNMIS1_1_47.value=this.w_ARUNMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oARUNMIS1_1_48.value==this.w_ARUNMIS1)
      this.oPgFrm.Page1.oPag.oARUNMIS1_1_48.value=this.w_ARUNMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oDICOSSTA_1_49.value==this.w_DICOSSTA)
      this.oPgFrm.Page1.oPag.oDICOSSTA_1_49.value=this.w_DICOSSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oDICOSFCO_1_51.value==this.w_DICOSFCO)
      this.oPgFrm.Page1.oPag.oDICOSFCO_1_51.value=this.w_DICOSFCO
    endif
    if not(this.oPgFrm.Page1.oPag.oDICOSLCO_1_53.value==this.w_DICOSLCO)
      this.oPgFrm.Page1.oPag.oDICOSLCO_1_53.value=this.w_DICOSLCO
    endif
    if not(this.oPgFrm.Page1.oPag.oDICOSLSC_1_55.value==this.w_DICOSLSC)
      this.oPgFrm.Page1.oPag.oDICOSLSC_1_55.value=this.w_DICOSLSC
    endif
    if not(this.oPgFrm.Page1.oPag.oARUNMIS1_1_58.value==this.w_ARUNMIS1)
      this.oPgFrm.Page1.oPag.oARUNMIS1_1_58.value=this.w_ARUNMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oARUNMIS1_1_59.value==this.w_ARUNMIS1)
      this.oPgFrm.Page1.oPag.oARUNMIS1_1_59.value=this.w_ARUNMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oARUNMIS1_1_60.value==this.w_ARUNMIS1)
      this.oPgFrm.Page1.oPag.oARUNMIS1_1_60.value=this.w_ARUNMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oDIQTACVR_1_65.value==this.w_DIQTACVR)
      this.oPgFrm.Page1.oPag.oDIQTACVR_1_65.value=this.w_DIQTACVR
    endif
    if not(this.oPgFrm.Page1.oPag.oDIQTASVR_1_66.value==this.w_DIQTASVR)
      this.oPgFrm.Page1.oPag.oDIQTASVR_1_66.value=this.w_DIQTASVR
    endif
    if not(this.oPgFrm.Page1.oPag.oARUNMIS1_1_69.value==this.w_ARUNMIS1)
      this.oPgFrm.Page1.oPag.oARUNMIS1_1_69.value=this.w_ARUNMIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oARUNMIS1_1_70.value==this.w_ARUNMIS1)
      this.oPgFrm.Page1.oPag.oARUNMIS1_1_70.value=this.w_ARUNMIS1
    endif
    cp_SetControlsValueExtFlds(this,'INVEDETT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO $ 'PF-SE-MP-PH-MC-MA-IM-FS')  and not(empty(.w_DICODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDICODICE_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice di tipo non articolo oppure codice obsoleto o inesistente")
        endcase
      endif
      *i_bRes = i_bRes .and. .gsma_mif.CheckForm()
      if i_bres
        i_bres=  .gsma_mif.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .gsma_mil.CheckForm()
      if i_bres
        i_bres=  .gsma_mil.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .gsma_mis.CheckForm()
      if i_bres
        i_bres=  .gsma_mis.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DICODESE = this.w_DICODESE
    * --- gsma_mif : Depends On
    this.gsma_mif.SaveDependsOn()
    * --- gsma_mil : Depends On
    this.gsma_mil.SaveDependsOn()
    * --- gsma_mis : Depends On
    this.gsma_mis.SaveDependsOn()
    return

  func CanEdit()
    local i_res
    i_res=this.w_STAINV<>'S' AND this.w_RESCHK=0
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile variare un inventario storico o confermato utilizzato come riferimento per il calcolo di altri inventari"))
    endif
    return(i_res)
  func CanAdd()
    local i_res
    i_res=this.w_STAINV<>'S' AND this.w_RESCHK=0
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile effettuare caricamenti in un inventario storico o confermato utilizzato come riferimento per il calcolo di altri inventari"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=this.w_STAINV<>'S' AND this.w_RESCHK=0
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile cancellare un inventario storico o confermato utilizzato come riferimento per il calcolo di altri inventari"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsma_aidPag1 as StdContainer
  Width  = 635
  height = 354
  stdWidth  = 635
  stdheight = 354
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDICODICE_1_7 as StdField with uid="VVBPDOOTXH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DICODICE", cQueryName = "DINUMINV,DICODESE,DICODICE",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice di tipo non articolo oppure codice obsoleto o inesistente",;
    ToolTipText = "Codice articolo",;
    HelpContextID = 218694267,;
   bGlobalFont=.t.,;
    Height=21, Width=168, Left=73, Top=11, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_DICODICE"

  func oDICODICE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICODICE_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDICODICE_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oDICODICE_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'GSMA0ADI.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oDICODICE_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_DICODICE
     i_obj.ecpSave()
  endproc

  add object oARDESART_1_9 as StdField with uid="BKFLCYXLCK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ARDESART", cQueryName = "ARDESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 99556186,;
   bGlobalFont=.t.,;
    Height=21, Width=296, Left=251, Top=11, InputMask=replicate('X',40)

  add object oDIQTAESI_1_12 as StdField with uid="HIOLKMUBFG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DIQTAESI", cQueryName = "DIQTAESI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Esistenza alla data inventario",;
    HelpContextID = 148824703,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=151, Top=65, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oDIQTAVEN_1_14 as StdField with uid="YGLWMGTHQS",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DIQTAVEN", cQueryName = "DIQTAVEN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Vendite dall'inizio esercizio fino alla data inventario",;
    HelpContextID = 165601924,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=151, Top=89, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oDIQTAACQ_1_16 as StdField with uid="ENHXXLZTFP",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DIQTAACQ", cQueryName = "DIQTAACQ",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Acquisti dall'inizio esercizio fino alla data inventario",;
    HelpContextID = 81715847,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=151, Top=113, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oDIQTAESR_1_18 as StdField with uid="PEBGPZDNUM",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DIQTAESR", cQueryName = "DIQTAESR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Esistenza alla data inventario",;
    HelpContextID = 148824712,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=151, Top=166, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oDIQTAVER_1_19 as StdField with uid="HTTEBACNDV",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DIQTAVER", cQueryName = "DIQTAVER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Scarichi dall'inizio esercizio fino alla data inventario",;
    HelpContextID = 165601928,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=151, Top=189, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oDIQTAACR_1_20 as StdField with uid="MWSTYHPAXF",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DIQTAACR", cQueryName = "DIQTAACR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Acquisti dall'inizio esercizio",;
    HelpContextID = 81715848,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=151, Top=237, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oDIPRZMPA_1_21 as StdField with uid="ZBRFKURAMO",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DIPRZMPA", cQueryName = "DIPRZMPA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore venduto dall'inizio esercizio / q.t� venduta dall'inizio esercizio",;
    HelpContextID = 40686199,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=477, Top=65, cSayPict="v_PU(40+VVU)", cGetPict="v_GU(40+VVU)"

  add object oDIPRZMPP_1_23 as StdField with uid="BTBWJDYTCB",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DIPRZMPP", cQueryName = "DIPRZMPP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore venduto nel periodo / q.t� venduta nel periodo",;
    HelpContextID = 40686214,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=477, Top=89, cSayPict="v_PU(40+VVU)", cGetPict="v_GU(40+VVU)"

  add object oDIPRZULT_1_25 as StdField with uid="RWVNYOGHUB",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DIPRZULT", cQueryName = "DIPRZULT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Prezzo ultima vendita",;
    HelpContextID = 93531510,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=477, Top=113, cSayPict="v_PU(40+VVU)", cGetPict="v_GU(40+VVU)"

  add object oDICOSMPA_1_27 as StdField with uid="RLWNUAKWIX",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DICOSMPA", cQueryName = "DICOSMPA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore acquistato dall'inizio esercizio / q.t� acquistata dall'inizio esercizio",;
    HelpContextID = 33096311,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=477, Top=165, cSayPict="v_PU(40+VVU)", cGetPict="v_GU(40+VVU)"

  add object oDICOSMPP_1_29 as StdField with uid="TNGYGJQKMG",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DICOSMPP", cQueryName = "DICOSMPP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore acquistato nel periodo / q.t� acquistata nel periodo",;
    HelpContextID = 33096326,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=477, Top=189, cSayPict="v_PU(40+VVU)", cGetPict="v_GU(40+VVU)"

  add object oDICOSULT_1_31 as StdField with uid="PRKVOHOLXJ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DICOSULT", cQueryName = "DICOSULT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo ultimo acquisto",;
    HelpContextID = 101121398,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=477, Top=213, cSayPict="v_PU(40+VVU)", cGetPict="v_GU(40+VVU)"


  add object oLinkPC_1_40 as StdButton with uid="OQALSIBLET",left=21, top=308, width=48,height=45,;
    CpPicture="BMP\FIFOCONT.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere agli scaglioni di calcolo del FIFO continuo";
    , HelpContextID = 85676746;
    , Caption='\<Fifo cont.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_40.Click()
      this.Parent.oContained.gsma_mif.LinkPCClick()
    endproc


  add object oLinkPC_1_41 as StdButton with uid="GTDYAABYKO",left=75, top=308, width=48,height=45,;
    CpPicture="BMP\LIFOCONT.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere agli scaglioni di calcolo del LIFO continuo";
    , HelpContextID = 85676842;
    , Caption='Lifo \<cont.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_41.Click()
      this.Parent.oContained.gsma_mil.LinkPCClick()
    endproc


  add object oLinkPC_1_42 as StdButton with uid="ZBWOXVTQRF",left=129, top=308, width=48,height=45,;
    CpPicture="BMP\LIFOSCAT.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere agli scaglioni di calcolo del LIFO a scatti";
    , HelpContextID = 169562922;
    , Caption='Lifo \<scatti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_42.Click()
      this.Parent.oContained.gsma_mis.LinkPCClick()
    endproc

  add object oARUNMIS1_1_46 as StdField with uid="XRZKEXFJSH",rtseq=25,rtrep=.f.,;
    cFormVar = "w_ARUNMIS1", cQueryName = "ARUNMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 228141879,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=261, Top=65, InputMask=replicate('X',3)

  add object oARUNMIS1_1_47 as StdField with uid="UKDHSYIMUL",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ARUNMIS1", cQueryName = "ARUNMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 228141879,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=261, Top=113, InputMask=replicate('X',3)

  add object oARUNMIS1_1_48 as StdField with uid="XSIVDCLGNY",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ARUNMIS1", cQueryName = "ARUNMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 228141879,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=261, Top=89, InputMask=replicate('X',3)

  add object oDICOSSTA_1_49 as StdField with uid="YTUVMKHOMB",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DICOSSTA", cQueryName = "DICOSSTA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultimo costo standard",;
    HelpContextID = 133759607,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=477, Top=237, cSayPict="v_PU(40+VVU)", cGetPict="v_GU(40+VVU)"

  add object oDICOSFCO_1_51 as StdField with uid="XPFRXISXFG",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DICOSFCO", cQueryName = "DICOSFCO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo unitario a FIFO continuo",;
    HelpContextID = 184091269,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=477, Top=261, cSayPict="v_PU(40+VVU)", cGetPict="v_GU(40+VVU)"

  add object oDICOSLCO_1_53 as StdField with uid="KSTSCFSUCN",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DICOSLCO", cQueryName = "DICOSLCO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo unitario a LIFO continuo",;
    HelpContextID = 16319109,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=477, Top=285, cSayPict="v_PU(40+VVU)", cGetPict="v_GU(40+VVU)"

  add object oDICOSLSC_1_55 as StdField with uid="STJLZVLSYQ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DICOSLSC", cQueryName = "DICOSLSC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo unitario a LIFO a scatti",;
    HelpContextID = 16319097,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=477, Top=309, cSayPict="v_PU(40+VVU)", cGetPict="v_GU(40+VVU)"

  add object oARUNMIS1_1_58 as StdField with uid="YBGLDSWDXL",rtseq=33,rtrep=.f.,;
    cFormVar = "w_ARUNMIS1", cQueryName = "ARUNMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 228141879,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=261, Top=165, InputMask=replicate('X',3)

  add object oARUNMIS1_1_59 as StdField with uid="WCQFYZQAAR",rtseq=34,rtrep=.f.,;
    cFormVar = "w_ARUNMIS1", cQueryName = "ARUNMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 228141879,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=261, Top=237, InputMask=replicate('X',3)

  add object oARUNMIS1_1_60 as StdField with uid="JFJUYVQZOF",rtseq=35,rtrep=.f.,;
    cFormVar = "w_ARUNMIS1", cQueryName = "ARUNMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 228141879,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=261, Top=189, InputMask=replicate('X',3)


  add object oObj_1_61 as cp_runprogram with uid="SVFQMQNDVX",left=734, top=243, width=131,height=19,;
    caption='GSMA_BDI',;
   bGlobalFont=.t.,;
    prg="GSMA_BDI",;
    cEvent = "Load",;
    nPag=1;
    , HelpContextID = 128691375

  add object oDIQTACVR_1_65 as StdField with uid="MPNRMTQSKR",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DIQTACVR", cQueryName = "DIQTACVR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Carichi di tipo aggiornamento valori",;
    HelpContextID = 115270280,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=151, Top=261, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oDIQTASVR_1_66 as StdField with uid="GUZMZGQPZT",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DIQTASVR", cQueryName = "DIQTASVR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Scarichi di tipo aggiornamento valori",;
    HelpContextID = 115270280,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=151, Top=213, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oARUNMIS1_1_69 as StdField with uid="HFWKWOSDVE",rtseq=41,rtrep=.f.,;
    cFormVar = "w_ARUNMIS1", cQueryName = "ARUNMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 228141879,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=261, Top=213, InputMask=replicate('X',3)

  add object oARUNMIS1_1_70 as StdField with uid="HMHNPFFVQQ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_ARUNMIS1", cQueryName = "ARUNMIS1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 228141879,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=261, Top=261, InputMask=replicate('X',3)

  add object oStr_1_11 as StdString with uid="XNFANCWEJF",Visible=.t., Left=2, Top=11,;
    Alignment=1, Width=69, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="TJDSEUTJRG",Visible=.t., Left=79, Top=65,;
    Alignment=1, Width=70, Height=15,;
    Caption="Esistenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="CRGPEUTBLU",Visible=.t., Left=79, Top=89,;
    Alignment=1, Width=70, Height=15,;
    Caption="Vendite:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="YAGPYQTRXU",Visible=.t., Left=79, Top=113,;
    Alignment=1, Width=70, Height=15,;
    Caption="Acquisti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="WZDTIGAIGL",Visible=.t., Left=311, Top=65,;
    Alignment=1, Width=163, Height=15,;
    Caption="Medio ponderato esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="GKYPVPLPNP",Visible=.t., Left=311, Top=89,;
    Alignment=1, Width=163, Height=15,;
    Caption="Medio ponderato periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="DUZTHQROGP",Visible=.t., Left=311, Top=113,;
    Alignment=1, Width=163, Height=15,;
    Caption="Ultimo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="LDNZSVOYRX",Visible=.t., Left=311, Top=165,;
    Alignment=1, Width=163, Height=15,;
    Caption="Medio ponderato esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="XDNVAKRUNB",Visible=.t., Left=311, Top=189,;
    Alignment=1, Width=163, Height=15,;
    Caption="Medio ponderato periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="JYNGNOUSNC",Visible=.t., Left=311, Top=213,;
    Alignment=1, Width=163, Height=15,;
    Caption="Ultimo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="KLUUQDYBSL",Visible=.t., Left=311, Top=237,;
    Alignment=1, Width=163, Height=15,;
    Caption="Ultimo standard:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="TRNTOYTDKW",Visible=.t., Left=311, Top=285,;
    Alignment=1, Width=163, Height=15,;
    Caption="LIFO continuo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="GZBUIZZKTV",Visible=.t., Left=311, Top=309,;
    Alignment=1, Width=163, Height=15,;
    Caption="LIFO scatti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="KRKYJEDKAH",Visible=.t., Left=324, Top=146,;
    Alignment=0, Width=306, Height=15,;
    Caption="Costi unitari"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_37 as StdString with uid="JTNLUMNPQE",Visible=.t., Left=324, Top=46,;
    Alignment=0, Width=304, Height=15,;
    Caption="Prezzi unitari"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_38 as StdString with uid="NQSNTEYUWU",Visible=.t., Left=53, Top=46,;
    Alignment=0, Width=207, Height=15,;
    Caption="Quantit� movimentate"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_39 as StdString with uid="HNNTCDEXUM",Visible=.t., Left=311, Top=261,;
    Alignment=1, Width=163, Height=15,;
    Caption="FIFO continuo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="OIKYAVPXQA",Visible=.t., Left=53, Top=146,;
    Alignment=0, Width=225, Height=15,;
    Caption="Quantit� movimentate sul raggr. fiscale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_54 as StdString with uid="OWVSTAHHAP",Visible=.t., Left=79, Top=165,;
    Alignment=1, Width=70, Height=15,;
    Caption="Esistenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="JGKZMKPGHU",Visible=.t., Left=65, Top=189,;
    Alignment=1, Width=84, Height=15,;
    Caption="Tot. scarichi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="RCJREYJAWQ",Visible=.t., Left=79, Top=237,;
    Alignment=1, Width=70, Height=15,;
    Caption="Tot. carichi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="VFFDMHAIJW",Visible=.t., Left=19, Top=261,;
    Alignment=1, Width=130, Height=15,;
    Caption="di cui carichi valoriz.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="FLVPPPMNNQ",Visible=.t., Left=5, Top=213,;
    Alignment=1, Width=144, Height=15,;
    Caption="di cui scarichi valoriz.:"  ;
  , bGlobalFont=.t.

  add object oBox_1_43 as StdBox with uid="VTNDTQPYDJ",left=2, top=39, width=608,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_aid','INVEDETT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DINUMINV=INVEDETT.DINUMINV";
  +" and "+i_cAliasName2+".DICODESE=INVEDETT.DICODESE";
  +" and "+i_cAliasName2+".DICODICE=INVEDETT.DICODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
