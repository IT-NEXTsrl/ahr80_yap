* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_slg                                                        *
*              Stampa giornale di magazzino                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_12]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2014-10-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_slg",oParentObject))

* --- Class definition
define class tgsma_slg as StdForm
  Top    = 28
  Left   = 51

  * --- Standard Properties
  Width  = 566
  Height = 302
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-27"
  HelpContextID=208305001
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  AZIENDA_IDX = 0
  MAGAZZIN_IDX = 0
  GIORMAGA_IDX = 0
  cPrg = "gsma_slg"
  cComment = "Stampa giornale di magazzino"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_SELCON = space(10)
  w_SELFOR = space(10)
  w_SELPER = space(10)
  w_AZIENDA = space(10)
  w_CODESE = space(4)
  o_CODESE = space(4)
  w_CODMAG = space(5)
  o_CODMAG = space(5)
  w_DESMAG = space(30)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_CLASSE = space(1)
  w_SELMOD = space(1)
  o_SELMOD = space(1)
  w_ULTDAT = ctod('  /  /  ')
  w_ULTRIG = 0
  w_INIZESER = ctod('  /  /  ')
  w_FINEESER = ctod('  /  /  ')
  w_GMCODMAG = space(10)
  w_OBTEST = ctod('  /  /  ')
  w_PRPAGM = 0
  w_PREFIS = space(20)
  w_INTLGM = space(1)
  w_FISMAG = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_slgPag1","gsma_slg",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODESE_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='MAGAZZIN'
    this.cWorkTables[4]='GIORMAGA'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do gsma_blg with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsma_slg
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SELCON=space(10)
      .w_SELFOR=space(10)
      .w_SELPER=space(10)
      .w_AZIENDA=space(10)
      .w_CODESE=space(4)
      .w_CODMAG=space(5)
      .w_DESMAG=space(30)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_CLASSE=space(1)
      .w_SELMOD=space(1)
      .w_ULTDAT=ctod("  /  /  ")
      .w_ULTRIG=0
      .w_INIZESER=ctod("  /  /  ")
      .w_FINEESER=ctod("  /  /  ")
      .w_GMCODMAG=space(10)
      .w_OBTEST=ctod("  /  /  ")
      .w_PRPAGM=0
      .w_PREFIS=space(20)
      .w_INTLGM=space(1)
      .w_FISMAG=space(1)
        .w_SELCON = 'C'
        .w_SELFOR = 'R'
        .w_SELPER = 'M'
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_AZIENDA))
          .link_1_4('Full')
        endif
        .w_CODESE = g_CODESE
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODESE))
          .link_1_5('Full')
        endif
        .w_CODMAG = g_MAGAZI
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODMAG))
          .link_1_6('Full')
        endif
          .DoRTCalc(7,7,.f.)
        .w_DATINI = iif( empty(nvl(.w_UltDat,cp_CharToDate('  -  -  '))) .or. .w_SelMod='R', .w_InizEser, .w_UltDat+1)
          .DoRTCalc(9,9,.f.)
        .w_CLASSE = 'N'
        .w_SELMOD = 'S'
          .DoRTCalc(12,15,.f.)
        .w_GMCODMAG = .w_CODMAG
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_GMCODMAG))
          .link_1_32('Full')
        endif
        .w_OBTEST = i_INIDAT
        .w_PRPAGM = IIF(YEAR(.w_DATFIN)<>YEAR(.w_ULTDAT) AND NOT EMPTY(.w_ULTDAT),0,.w_PRPAGM)
    endwith
    this.DoRTCalc(19,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_1_4('Full')
        .DoRTCalc(5,7,.t.)
        if .o_CODESE<>.w_CODESE.or. .o_CODMAG<>.w_CODMAG.or. .o_SELMOD<>.w_SELMOD
            .w_DATINI = iif( empty(nvl(.w_UltDat,cp_CharToDate('  -  -  '))) .or. .w_SelMod='R', .w_InizEser, .w_UltDat+1)
        endif
        .DoRTCalc(9,15,.t.)
        if .o_CODESE<>.w_CODESE.or. .o_CODMAG<>.w_CODMAG
            .w_GMCODMAG = .w_CODMAG
          .link_1_32('Full')
        endif
        .DoRTCalc(17,17,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_PRPAGM = IIF(YEAR(.w_DATFIN)<>YEAR(.w_ULTDAT) AND NOT EMPTY(.w_ULTDAT),0,.w_PRPAGM)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(19,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPRPAGM_1_34.enabled = this.oPgFrm.Page1.oPag.oPRPAGM_1_34.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZGMGCON,AZGMGFOR,AZGMGPER";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZGMGCON,AZGMGFOR,AZGMGPER;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(10))
      this.w_SELCON = NVL(_Link_.AZGMGCON,space(10))
      this.w_SELFOR = NVL(_Link_.AZGMGFOR,space(10))
      this.w_SELPER = NVL(_Link_.AZGMGPER,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(10)
      endif
      this.w_SELCON = space(10)
      this.w_SELFOR = space(10)
      this.w_SELPER = space(10)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_5'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_datini = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_datfin = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_INIZESER = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINEESER = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_datini = ctod("  /  /  ")
      this.w_datfin = ctod("  /  /  ")
      this.w_INIZESER = ctod("  /  /  ")
      this.w_FINEESER = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGSTAINT,MGPRPAGM,MGPREFIS,MGFISMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG,MGSTAINT,MGPRPAGM,MGPREFIS,MGFISMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGSTAINT,MGPRPAGM,MGPREFIS,MGFISMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGSTAINT,MGPRPAGM,MGPREFIS,MGFISMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_6'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGSTAINT,MGPRPAGM,MGPREFIS,MGFISMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGSTAINT,MGPRPAGM,MGPREFIS,MGFISMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGSTAINT,MGPRPAGM,MGPREFIS,MGFISMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG,MGSTAINT,MGPRPAGM,MGPREFIS,MGFISMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_INTLGM = NVL(_Link_.MGSTAINT,space(1))
      this.w_PRPAGM = NVL(_Link_.MGPRPAGM,0)
      this.w_PREFIS = NVL(_Link_.MGPREFIS,space(20))
      this.w_FISMAG = NVL(_Link_.MGFISMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_INTLGM = space(1)
      this.w_PRPAGM = 0
      this.w_PREFIS = space(20)
      this.w_FISMAG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GMCODMAG
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GIORMAGA_IDX,3]
    i_lTable = "GIORMAGA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GIORMAGA_IDX,2], .t., this.GIORMAGA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GIORMAGA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GMCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GMCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODAZI,GMCODESE,GMCODMAG,GMDATGIO,GMRIGGIO";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODMAG="+cp_ToStrODBC(this.w_GMCODMAG);
                   +" and GMCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   +" and GMCODESE="+cp_ToStrODBC(this.w_CODESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODAZI',this.w_AZIENDA;
                       ,'GMCODESE',this.w_CODESE;
                       ,'GMCODMAG',this.w_GMCODMAG)
            select GMCODAZI,GMCODESE,GMCODMAG,GMDATGIO,GMRIGGIO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GMCODMAG = NVL(_Link_.GMCODMAG,space(10))
      this.w_ULTDAT = NVL(cp_ToDate(_Link_.GMDATGIO),ctod("  /  /  "))
      this.w_ULTRIG = NVL(_Link_.GMRIGGIO,0)
    else
      if i_cCtrl<>'Load'
        this.w_GMCODMAG = space(10)
      endif
      this.w_ULTDAT = ctod("  /  /  ")
      this.w_ULTRIG = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GIORMAGA_IDX,2])+'\'+cp_ToStr(_Link_.GMCODAZI,1)+'\'+cp_ToStr(_Link_.GMCODESE,1)+'\'+cp_ToStr(_Link_.GMCODMAG,1)
      cp_ShowWarn(i_cKey,this.GIORMAGA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GMCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELCON_1_1.RadioValue()==this.w_SELCON)
      this.oPgFrm.Page1.oPag.oSELCON_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELFOR_1_2.RadioValue()==this.w_SELFOR)
      this.oPgFrm.Page1.oPag.oSELFOR_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELPER_1_3.RadioValue()==this.w_SELPER)
      this.oPgFrm.Page1.oPag.oSELPER_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_5.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_5.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_6.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_6.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_7.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_7.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_8.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_8.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_9.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_9.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASSE_1_10.RadioValue()==this.w_CLASSE)
      this.oPgFrm.Page1.oPag.oCLASSE_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELMOD_1_11.RadioValue()==this.w_SELMOD)
      this.oPgFrm.Page1.oPag.oSELMOD_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oULTDAT_1_12.value==this.w_ULTDAT)
      this.oPgFrm.Page1.oPag.oULTDAT_1_12.value=this.w_ULTDAT
    endif
    if not(this.oPgFrm.Page1.oPag.oULTRIG_1_13.value==this.w_ULTRIG)
      this.oPgFrm.Page1.oPag.oULTRIG_1_13.value=this.w_ULTRIG
    endif
    if not(this.oPgFrm.Page1.oPag.oPRPAGM_1_34.value==this.w_PRPAGM)
      this.oPgFrm.Page1.oPag.oPRPAGM_1_34.value=this.w_PRPAGM
    endif
    if not(this.oPgFrm.Page1.oPag.oPREFIS_1_36.value==this.w_PREFIS)
      this.oPgFrm.Page1.oPag.oPREFIS_1_36.value=this.w_PREFIS
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(empty(.w_datfin) or .w_datini<=.w_datfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(empty(.w_datini) or .w_datini<=.w_datfin)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODESE = this.w_CODESE
    this.o_CODMAG = this.w_CODMAG
    this.o_DATFIN = this.w_DATFIN
    this.o_SELMOD = this.w_SELMOD
    return

enddefine

* --- Define pages as container
define class tgsma_slgPag1 as StdContainer
  Width  = 562
  height = 302
  stdWidth  = 562
  stdheight = 302
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oSELCON_1_1 as StdCombo with uid="WUPEDYLQED",rtseq=1,rtrep=.f.,left=88,top=154,width=192,height=21, enabled=.f.;
    , ToolTipText = "Tipo";
    , HelpContextID = 114134822;
    , cFormVar="w_SELCON",RowSource=""+"Beni singoli,"+"Categorie omogenee", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSELCON_1_1.RadioValue()
    return(iif(this.value =1,'B',;
    iif(this.value =2,'C',;
    space(10))))
  endfunc
  func oSELCON_1_1.GetRadio()
    this.Parent.oContained.w_SELCON = this.RadioValue()
    return .t.
  endfunc

  func oSELCON_1_1.SetRadio()
    this.Parent.oContained.w_SELCON=trim(this.Parent.oContained.w_SELCON)
    this.value = ;
      iif(this.Parent.oContained.w_SELCON=='B',1,;
      iif(this.Parent.oContained.w_SELCON=='C',2,;
      0))
  endfunc


  add object oSELFOR_1_2 as StdCombo with uid="ALOFDOKQCV",rtseq=2,rtrep=.f.,left=88,top=182,width=192,height=21, enabled=.f.;
    , ToolTipText = "Forma";
    , HelpContextID = 181440294;
    , cFormVar="w_SELFOR",RowSource=""+"Riepilogativa,"+"Dettagliata", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSELFOR_1_2.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'D',;
    space(10))))
  endfunc
  func oSELFOR_1_2.GetRadio()
    this.Parent.oContained.w_SELFOR = this.RadioValue()
    return .t.
  endfunc

  func oSELFOR_1_2.SetRadio()
    this.Parent.oContained.w_SELFOR=trim(this.Parent.oContained.w_SELFOR)
    this.value = ;
      iif(this.Parent.oContained.w_SELFOR=='R',1,;
      iif(this.Parent.oContained.w_SELFOR=='D',2,;
      0))
  endfunc


  add object oSELPER_1_3 as StdCombo with uid="LGSEOUGDUD",rtseq=3,rtrep=.f.,left=88,top=212,width=192,height=21, enabled=.f.;
    , ToolTipText = "Periodicit�";
    , HelpContextID = 171609894;
    , cFormVar="w_SELPER",RowSource=""+"Mese,"+"Quindicina,"+"Decade,"+"Settimana", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oSELPER_1_3.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'Q',;
    iif(this.value =3,'D',;
    iif(this.value =4,'S',;
    space(10))))))
  endfunc
  func oSELPER_1_3.GetRadio()
    this.Parent.oContained.w_SELPER = this.RadioValue()
    return .t.
  endfunc

  func oSELPER_1_3.SetRadio()
    this.Parent.oContained.w_SELPER=trim(this.Parent.oContained.w_SELPER)
    this.value = ;
      iif(this.Parent.oContained.w_SELPER=='M',1,;
      iif(this.Parent.oContained.w_SELPER=='Q',2,;
      iif(this.Parent.oContained.w_SELPER=='D',3,;
      iif(this.Parent.oContained.w_SELPER=='S',4,;
      0))))
  endfunc

  add object oCODESE_1_5 as StdField with uid="OVGETKJRNL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza dei movimenti da stampare",;
    HelpContextID = 235870246,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=88, Top=12, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
      if .not. empty(.w_GMCODMAG)
        bRes2=.link_1_32('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODESE_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oCODMAG_1_6 as StdField with uid="DICLTYREKI",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino selezionato",;
    HelpContextID = 251074598,;
   bGlobalFont=.t.,;
    Height=21, Width=61, Left=88, Top=40, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_7 as StdField with uid="GHDNZGQDUT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 251133494,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=155, Top=40, InputMask=replicate('X',30)

  add object oDATINI_1_8 as StdField with uid="MVUTTSZRSQ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data di registrazione di inizio stampa",;
    HelpContextID = 29624886,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=467, Top=12

  func oDATINI_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_datfin) or .w_datini<=.w_datfin)
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_9 as StdField with uid="KRFQJCFRDT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data di registrazione di fine stampa",;
    HelpContextID = 108071478,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=467, Top=40

  func oDATFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_datini) or .w_datini<=.w_datfin)
    endwith
    return bRes
  endfunc


  add object oCLASSE_1_10 as StdCombo with uid="XTVMAIGQBV",rtseq=10,rtrep=.f.,left=88,top=96,width=107,height=21;
    , ToolTipText = "Filtro sulla classe degli articoli";
    , HelpContextID = 236774694;
    , cFormVar="w_CLASSE",RowSource=""+"Tutte,"+"Solo A e B", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLASSE_1_10.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oCLASSE_1_10.GetRadio()
    this.Parent.oContained.w_CLASSE = this.RadioValue()
    return .t.
  endfunc

  func oCLASSE_1_10.SetRadio()
    this.Parent.oContained.w_CLASSE=trim(this.Parent.oContained.w_CLASSE)
    this.value = ;
      iif(this.Parent.oContained.w_CLASSE=='N',1,;
      iif(this.Parent.oContained.w_CLASSE=='S',2,;
      0))
  endfunc


  add object oSELMOD_1_11 as StdCombo with uid="PCJMSUPCSM",rtseq=11,rtrep=.f.,left=88,top=127,width=192,height=21;
    , ToolTipText = "Modalit� di stampa";
    , HelpContextID = 215453478;
    , cFormVar="w_SELMOD",RowSource=""+"Simulata,"+"Definitiva,"+"Ristampa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELMOD_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'B',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oSELMOD_1_11.GetRadio()
    this.Parent.oContained.w_SELMOD = this.RadioValue()
    return .t.
  endfunc

  func oSELMOD_1_11.SetRadio()
    this.Parent.oContained.w_SELMOD=trim(this.Parent.oContained.w_SELMOD)
    this.value = ;
      iif(this.Parent.oContained.w_SELMOD=='S',1,;
      iif(this.Parent.oContained.w_SELMOD=='B',2,;
      iif(this.Parent.oContained.w_SELMOD=='R',3,;
      0)))
  endfunc

  add object oULTDAT_1_12 as StdField with uid="WOEUFUWVPW",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ULTDAT", cQueryName = "ULTDAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data ultima stampa libro giornale",;
    HelpContextID = 200218182,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=401, Top=127

  add object oULTRIG_1_13 as StdField with uid="ICHBLATEFH",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ULTRIG", cQueryName = "ULTRIG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultima riga stampata sul libro giornale",;
    HelpContextID = 259855942,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=401, Top=154, cSayPict='"@Z 9999999999"', cGetPict='"@Z 9999999999"'


  add object oBtn_1_14 as StdButton with uid="WALATIUIYF",left=451, top=250, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 208276250;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        do gsma_blg with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="DLWHTINPCY",left=503, top=250, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 200987578;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPRPAGM_1_34 as StdField with uid="GZVSBCMEQD",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PRPAGM", cQueryName = "PRPAGM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultima pagina stampata nel precedente libro giornale",;
    HelpContextID = 88857590,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=401, Top=182, cSayPict='"9999999"', cGetPict='"9999999"'

  func oPRPAGM_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_INTLGM='S' AND .w_FISMAG='S')
    endwith
   endif
  endfunc

  add object oPREFIS_1_36 as StdField with uid="SJBUMXHJGU",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PREFIS", cQueryName = "PREFIS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso che comparir� nella stampa prima del numero di pagina",;
    HelpContextID = 191900662,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=401, Top=212, InputMask=replicate('X',20)

  add object oStr_1_16 as StdString with uid="FWGTPYHWEC",Visible=.t., Left=382, Top=12,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="HMEGUCCUML",Visible=.t., Left=382, Top=40,;
    Alignment=1, Width=83, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="LQRRUEQLWW",Visible=.t., Left=3, Top=12,;
    Alignment=1, Width=83, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="PHXGMSADGW",Visible=.t., Left=308, Top=74,;
    Alignment=0, Width=183, Height=15,;
    Caption="Progressivi"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="QOBKCBYMGA",Visible=.t., Left=285, Top=127,;
    Alignment=1, Width=113, Height=15,;
    Caption="Ultima stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="TPXQIIBWED",Visible=.t., Left=285, Top=154,;
    Alignment=1, Width=113, Height=15,;
    Caption="Ultima riga:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="QTMGJYSTKH",Visible=.t., Left=4, Top=154,;
    Alignment=1, Width=82, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="SFVSVIALDO",Visible=.t., Left=4, Top=212,;
    Alignment=1, Width=82, Height=15,;
    Caption="Periodicit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="RTGRENANXL",Visible=.t., Left=4, Top=182,;
    Alignment=1, Width=82, Height=15,;
    Caption="Forma:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="HCWIVVPMHT",Visible=.t., Left=16, Top=74,;
    Alignment=0, Width=267, Height=15,;
    Caption="Opzioni di stampa"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="YVBCIWSUXJ",Visible=.t., Left=3, Top=40,;
    Alignment=1, Width=83, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="KRWDBORTGW",Visible=.t., Left=4, Top=127,;
    Alignment=1, Width=82, Height=15,;
    Caption="Modalit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="FWNKOATCCI",Visible=.t., Left=314, Top=182,;
    Alignment=1, Width=84, Height=15,;
    Caption="Ultima pagina:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="QBIQLOVEJE",Visible=.t., Left=288, Top=212,;
    Alignment=1, Width=110, Height=15,;
    Caption="Pref. num. pag.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="NCBXFOIYAX",Visible=.t., Left=12, Top=98,;
    Alignment=1, Width=74, Height=15,;
    Caption="Classe ABC:"  ;
  , bGlobalFont=.t.

  add object oBox_1_19 as StdBox with uid="VTRAPWTVIJ",left=302, top=90, width=239,height=1

  add object oBox_1_26 as StdBox with uid="AWWWHFSQSU",left=9, top=90, width=280,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_slg','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
