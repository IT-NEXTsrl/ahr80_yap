* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_afd                                                        *
*              Generazione fatture differite                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_393]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-03                                                      *
* Last revis.: 2018-06-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_afd"))

* --- Class definition
define class tgsve_afd as StdFormNoConf
  Top    = -1
  Left   = 6

  * --- Standard Properties
  Width  = 687
  Height = 397+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-06-08"
  HelpContextID=59388521
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=185

  * --- Constant Properties
  FAT_DIFF_IDX = 0
  ESERCIZI_IDX = 0
  TIP_DOCU_IDX = 0
  VALUTE_IDX = 0
  CONTI_IDX = 0
  DES_DIVE_IDX = 0
  ZONE_IDX = 0
  AGENTI_IDX = 0
  DET_DIFF_IDX = 0
  CAU_CONT_IDX = 0
  PAG_AMEN_IDX = 0
  CACOCLFO_IDX = 0
  MASTRI_IDX = 0
  CATECOMM_IDX = 0
  azienda_IDX = 0
  AZIENDA_IDX = 0
  cFile = "FAT_DIFF"
  cKeySelect = "PSSERIAL"
  cKeyWhere  = "PSSERIAL=this.w_PSSERIAL"
  cKeyWhereODBC = '"PSSERIAL="+cp_ToStrODBC(this.w_PSSERIAL)';

  cKeyWhereODBCqualified = '"FAT_DIFF.PSSERIAL="+cp_ToStrODBC(this.w_PSSERIAL)';

  cPrg = "gsve_afd"
  cComment = "Generazione fatture differite"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PSSERIAL = space(10)
  o_PSSERIAL = space(10)
  w_CODAZI = space(5)
  w_PSTIPCLF = space(1)
  w_MVTIPCON = space(1)
  w_PSNUMREG = 0
  w_PSDATREG = ctod('  /  /  ')
  o_PSDATREG = ctod('  /  /  ')
  w_PSANNREG = space(4)
  w_PSCODESE = space(4)
  w_PSDESCRI = space(40)
  w_PSSTATUS = space(1)
  w_PSTIPDOC = space(5)
  o_PSTIPDOC = space(5)
  w_DESDOC = space(35)
  w_PSNUMDOC = 0
  w_PSALFDOC = space(10)
  w_PSDATDOC = ctod('  /  /  ')
  o_PSDATDOC = ctod('  /  /  ')
  w_PSDATCIV = ctod('  /  /  ')
  w_PSDATDIV = ctod('  /  /  ')
  w_PSPAGFOR = space(5)
  w_PSFLRAGG = space(1)
  w_PSFLVEBD = space(1)
  w_PSDATINI = ctod('  /  /  ')
  w_FLRICIVA = .F.
  o_FLRICIVA = .F.
  w_PSDATRIF = ctod('  /  /  ')
  w_PSNUMINI = 0
  w_PSALFDOI = space(10)
  w_PSNUMFIN = 0
  w_PSALFDOF = space(10)
  w_PSCODCLI = space(15)
  w_DESCLI = space(40)
  w_PSCODCLF = space(15)
  o_PSCODCLF = space(15)
  w_DESCLF = space(40)
  w_PSFLVALO = space(1)
  w_STADOC = space(1)
  w_MVCODESE = space(4)
  w_DATRIF = ctod('  /  /  ')
  w_MVTIPDOC = space(5)
  w_MVFLVEAC = space(1)
  w_FLGEFA = space(1)
  w_MVPRD = space(2)
  w_MVCLADOC = space(2)
  w_MVFLACCO = space(1)
  w_MVFLINTE = space(1)
  w_MVTCAMAG = space(5)
  w_MVTFRAGG = space(1)
  w_MVCAUCON = space(5)
  w_STAMPA = 0
  w_FLNSRI = space(1)
  w_TPNSRI = space(3)
  w_FLDTPR = space(1)
  w_FLGCOM = space(1)
  w_FLANAL = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_MVALFDOC = space(10)
  w_MVNUMDOC = 0
  w_DATDOC = ctod('  /  /  ')
  w_MVDATCIV = ctod('  /  /  ')
  w_DATDIV = ctod('  /  /  ')
  w_FLVEAC1 = space(1)
  w_FLGEFA1 = space(1)
  w_CAUDOC = space(5)
  w_CODCLI = space(15)
  w_CODZON = space(3)
  w_CODAGE = space(5)
  w_CODVAL = space(3)
  w_FLVALO = space(1)
  w_MVDATTRA = ctod('  /  /  ')
  w_MVORATRA = space(2)
  w_MVMINTRA = space(2)
  w_MVNOTAGG = space(50)
  w_MVANNPRO = space(4)
  w_MVNUMEST = 0
  w_MVPRP = space(2)
  w_MVALFEST = space(10)
  w_MVANNDOC = space(4)
  w_MVFLPROV = space(1)
  w_MVCODUTE = 0
  w_MVNUMREG = 0
  w_MVDATREG = ctod('  /  /  ')
  w_FLPACK = space(1)
  w_XVALO = 0
  w_MVSERIAL = space(10)
  w_OSTATUS = space(1)
  w_MVRIFPIA = space(10)
  w_FLPROV = space(1)
  w_MVRIFFAD = space(10)
  w_TIPOIN = space(5)
  w_DATAIN = ctod('  /  /  ')
  w_DATAFI = ctod('  /  /  ')
  w_SERIE1 = space(2)
  w_SERIE2 = space(2)
  w_CODESE = space(4)
  w_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_NUMINI = 0
  w_NUMFIN = 0
  w_CLIFOR = space(15)
  w_CATDOC = space(2)
  w_DEFALF = space(10)
  w_FLVEAC = space(1)
  w_ESCL1 = space(3)
  w_ESCL2 = space(3)
  w_ESCL3 = space(3)
  w_ESCL4 = space(3)
  w_ESCL5 = space(3)
  w_FLPPRO = space(1)
  w_FLVSRI = space(1)
  w_FLRIDE = space(1)
  w_TPRDES = space(3)
  w_TPVSRI = space(3)
  w_PSTIPORI = space(5)
  w_DESORI = space(35)
  w_PSCODPAG = space(5)
  o_PSCODPAG = space(5)
  w_DESPAG = space(30)
  w_PSCATCON = space(5)
  o_PSCATCON = space(5)
  w_DESCON = space(35)
  w_PSCONSUP = space(15)
  o_PSCONSUP = space(15)
  w_DESSUP = space(40)
  w_PSCATCOM = space(3)
  o_PSCATCOM = space(3)
  w_DESCOM = space(35)
  w_PSCODDES = space(5)
  o_PSCODDES = space(5)
  w_NOMDES = space(40)
  w_PSCODZON = space(3)
  o_PSCODZON = space(3)
  w_DESZON = space(35)
  w_PSCODAGE = space(5)
  o_PSCODAGE = space(5)
  w_DESAGE = space(35)
  w_PSCODVAL = space(3)
  w_DESVAL = space(35)
  w_CODCLF = space(15)
  w_CODPAG = space(5)
  w_CATCON = space(5)
  w_CONSUP = space(15)
  w_CATCOM = space(3)
  w_ALFDO2 = space(10)
  w_ALFDO1 = space(10)
  w_NUMDO2 = 0
  w_NUMDO1 = 0
  w_DATINI = ctod('  /  /  ')
  w_TIPOFAT = space(1)
  w_FLRAGG = space(1)
  w_CCTIPDOC = space(2)
  w_PSTIPFAT = space(1)
  w_MVRIFODL = space(10)
  w_TIPCON = space(1)
  w_OBTEST1 = ctod('  /  /  ')
  w_TIPRIF = space(2)
  w_FLELAN = space(1)
  w_FLCAMB = space(1)
  w_MV_SEGNO = space(1)
  w_OKAGG = .F.
  w_OALFDOC = space(10)
  w_GENPRO = space(1)
  w_FLSPIN = space(1)
  w_DTOBBL = space(1)
  w_CCTIPREG = space(1)
  w_REPSEC = space(1)
  w_CODART = space(10)
  w_PRZVAC = space(1)
  w_PRZDES = space(1)
  w_PSMINIMP = 0
  w_PSMINVAL = space(3)
  w_SIMVAL = space(5)
  w_GESRIT = space(1)
  w_FLIVDF = space(1)
  w_RIIVDTIN = ctod('  /  /  ')
  w_RIIVDTFI = ctod('  /  /  ')
  w_RIIVLSIV = space(0)
  w_CATEGO = space(2)
  w_NOSBAN = space(5)
  w_CODVET = space(5)
  w_LINGUA = space(3)
  w_TDFLIA01 = space(1)
  w_TDFLIA02 = space(1)
  w_TDFLIA03 = space(1)
  w_TDFLIA04 = space(1)
  w_TDFLIA05 = space(1)
  w_TDFLIA06 = space(1)
  w_TDFLRA01 = space(1)
  w_TDFLRA02 = space(1)
  w_TDFLRA03 = space(1)
  w_TDFLRA04 = space(1)
  w_TDFLRA05 = space(1)
  w_TDFLRA06 = space(1)
  w_PADESFOR = space(30)
  w_CODDES = space(5)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_PSSERIAL = this.W_PSSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_PSANNREG = this.W_PSANNREG
  op_PSNUMREG = this.W_PSNUMREG

  * --- Children pointers
  GSVE_MFD = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'FAT_DIFF','gsve_afd')
    stdPageFrame::Init()
    *set procedure to GSVE_MFD additive
    with this
      .Pages(1).addobject("oPag","tgsve_afdPag1","gsve_afd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principali")
      .Pages(1).HelpContextID = 203113751
      .Pages(2).addobject("oPag","tgsve_afdPag2","gsve_afd",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).HelpContextID = 36962350
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSVE_MFD
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[16]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='DES_DIVE'
    this.cWorkTables[6]='ZONE'
    this.cWorkTables[7]='AGENTI'
    this.cWorkTables[8]='DET_DIFF'
    this.cWorkTables[9]='CAU_CONT'
    this.cWorkTables[10]='PAG_AMEN'
    this.cWorkTables[11]='CACOCLFO'
    this.cWorkTables[12]='MASTRI'
    this.cWorkTables[13]='CATECOMM'
    this.cWorkTables[14]='azienda'
    this.cWorkTables[15]='AZIENDA'
    this.cWorkTables[16]='FAT_DIFF'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(16))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.FAT_DIFF_IDX,5],7]
    this.nPostItConn=i_TableProp[this.FAT_DIFF_IDX,3]
  return

  function CreateChildren()
    this.GSVE_MFD = CREATEOBJECT('stdLazyChild',this,'GSVE_MFD')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSVE_MFD)
      this.GSVE_MFD.DestroyChildrenChain()
      this.GSVE_MFD=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSVE_MFD.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSVE_MFD.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSVE_MFD.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSVE_MFD.SetKey(;
            .w_PSSERIAL,"DPSERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSVE_MFD.ChangeRow(this.cRowID+'      1',1;
             ,.w_PSSERIAL,"DPSERIAL";
             )
    endwith
    return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PSSERIAL = NVL(PSSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_11_joined
    link_1_11_joined=.f.
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_29_joined
    link_1_29_joined=.f.
    local link_1_31_joined
    link_1_31_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    local link_2_9_joined
    link_2_9_joined=.f.
    local link_2_11_joined
    link_2_11_joined=.f.
    local link_2_15_joined
    link_2_15_joined=.f.
    local link_2_17_joined
    link_2_17_joined=.f.
    local link_2_19_joined
    link_2_19_joined=.f.
    local link_2_38_joined
    link_2_38_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from FAT_DIFF where PSSERIAL=KeySet.PSSERIAL
    *
    i_nConn = i_TableProp[this.FAT_DIFF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FAT_DIFF_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('FAT_DIFF')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "FAT_DIFF.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' FAT_DIFF '
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_29_joined=this.AddJoinedLink_1_29(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_31_joined=this.AddJoinedLink_1_31(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_9_joined=this.AddJoinedLink_2_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_11_joined=this.AddJoinedLink_2_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_15_joined=this.AddJoinedLink_2_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_17_joined=this.AddJoinedLink_2_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_19_joined=this.AddJoinedLink_2_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_38_joined=this.AddJoinedLink_2_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PSSERIAL',this.w_PSSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESDOC = space(35)
        .w_FLRICIVA = .F.
        .w_DESCLI = space(40)
        .w_DESCLF = space(40)
        .w_STADOC = 'S'
        .w_MVFLVEAC = space(1)
        .w_FLGEFA = space(1)
        .w_MVPRD = space(2)
        .w_MVCLADOC = space(2)
        .w_MVFLACCO = space(1)
        .w_MVFLINTE = space(1)
        .w_MVTCAMAG = space(5)
        .w_MVTFRAGG = space(1)
        .w_MVCAUCON = space(5)
        .w_STAMPA = 0
        .w_FLNSRI = space(1)
        .w_TPNSRI = space(3)
        .w_FLDTPR = space(1)
        .w_FLGCOM = space(1)
        .w_FLANAL = space(1)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_FLVEAC1 = space(1)
        .w_FLGEFA1 = space(1)
        .w_MVNUMEST = 0
        .w_MVCODUTE = IIF(g_MAGUTE='S', 0, i_CODUTE)
        .w_MVNUMREG = 0
        .w_FLPACK = ' '
        .w_MVSERIAL = SPACE(10)
        .w_SERIE1 = space(2)
        .w_SERIE2 = space(2)
        .w_CODESE = space(4)
        .w_NUMINI = 0
        .w_NUMFIN = 0
        .w_CLIFOR = space(15)
        .w_DEFALF = space(10)
        .w_FLVEAC = 'V'
        .w_ESCL1 = space(3)
        .w_ESCL2 = space(3)
        .w_ESCL3 = space(3)
        .w_ESCL4 = space(3)
        .w_ESCL5 = space(3)
        .w_FLPPRO = space(1)
        .w_FLVSRI = space(1)
        .w_FLRIDE = space(1)
        .w_TPRDES = space(3)
        .w_TPVSRI = space(3)
        .w_DESORI = space(35)
        .w_DESPAG = space(30)
        .w_DESCON = space(35)
        .w_DESSUP = space(40)
        .w_DESCOM = space(35)
        .w_NOMDES = space(40)
        .w_DESZON = space(35)
        .w_DESAGE = space(35)
        .w_DESVAL = space(35)
        .w_CCTIPDOC = space(2)
        .w_MVRIFODL = SPACE(10)
        .w_TIPCON = space(1)
        .w_TIPRIF = space(2)
        .w_FLELAN = space(1)
        .w_FLCAMB = space(1)
        .w_MV_SEGNO = space(1)
        .w_OKAGG = .T.
        .w_OALFDOC = space(10)
        .w_GENPRO = space(1)
        .w_FLSPIN = space(1)
        .w_DTOBBL = space(1)
        .w_CCTIPREG = space(1)
        .w_REPSEC = 'S'
        .w_CODART = space(10)
        .w_PRZVAC = space(1)
        .w_PRZDES = space(1)
        .w_SIMVAL = space(5)
        .w_GESRIT = space(1)
        .w_FLIVDF = space(1)
        .w_CATEGO = space(2)
        .w_NOSBAN = space(5)
        .w_CODVET = space(5)
        .w_LINGUA = space(3)
        .w_TDFLIA01 = space(1)
        .w_TDFLIA02 = space(1)
        .w_TDFLIA03 = space(1)
        .w_TDFLIA04 = space(1)
        .w_TDFLIA05 = space(1)
        .w_TDFLIA06 = space(1)
        .w_TDFLRA01 = space(1)
        .w_TDFLRA02 = space(1)
        .w_TDFLRA03 = space(1)
        .w_TDFLRA04 = space(1)
        .w_TDFLRA05 = space(1)
        .w_TDFLRA06 = space(1)
        .w_PADESFOR = space(30)
        .w_PSSERIAL = NVL(PSSERIAL,space(10))
        .op_PSSERIAL = .w_PSSERIAL
        .w_CODAZI = i_CODAZI
          .link_1_2('Load')
        .w_PSTIPCLF = NVL(PSTIPCLF,space(1))
        .w_MVTIPCON = 'C'
        .w_PSNUMREG = NVL(PSNUMREG,0)
        .op_PSNUMREG = .w_PSNUMREG
        .w_PSDATREG = NVL(cp_ToDate(PSDATREG),ctod("  /  /  "))
        .w_PSANNREG = NVL(PSANNREG,space(4))
        .op_PSANNREG = .w_PSANNREG
        .w_PSCODESE = NVL(PSCODESE,space(4))
          * evitabile
          *.link_1_8('Load')
        .w_PSDESCRI = NVL(PSDESCRI,space(40))
        .w_PSSTATUS = NVL(PSSTATUS,space(1))
        .w_PSTIPDOC = NVL(PSTIPDOC,space(5))
          if link_1_11_joined
            this.w_PSTIPDOC = NVL(TDTIPDOC111,NVL(this.w_PSTIPDOC,space(5)))
            this.w_DESDOC = NVL(TDDESDOC111,space(35))
            this.w_MVFLVEAC = NVL(TDFLVEAC111,space(1))
            this.w_FLGEFA = NVL(TFFLGEFA111,space(1))
            this.w_MVPRD = NVL(TDPRODOC111,space(2))
            this.w_MVCLADOC = NVL(TDCATDOC111,space(2))
            this.w_MVFLACCO = NVL(TDFLACCO111,space(1))
            this.w_MVFLINTE = NVL(TDFLINTE111,space(1))
            this.w_MVTCAMAG = NVL(TDCAUMAG111,space(5))
            this.w_MVTFRAGG = NVL(TFFLRAGG111,space(1))
            this.w_MVCAUCON = NVL(TDCAUCON111,space(5))
            this.w_STAMPA = NVL(TDPRGSTA111,0)
            this.w_FLNSRI = NVL(TDFLNSRI111,space(1))
            this.w_TPNSRI = NVL(TDTPNDOC111,space(3))
            this.w_FLDTPR = NVL(TDFLDTPR111,space(1))
            this.w_PSALFDOC = NVL(TDALFDOC111,space(10))
            this.w_DEFALF = NVL(TDALFDOC111,space(10))
            this.w_ESCL1 = NVL(TDESCCL1111,space(3))
            this.w_ESCL2 = NVL(TDESCCL2111,space(3))
            this.w_ESCL3 = NVL(TDESCCL3111,space(3))
            this.w_ESCL4 = NVL(TDESCCL4111,space(3))
            this.w_ESCL5 = NVL(TDESCCL5111,space(3))
            this.w_FLGCOM = NVL(TDFLCOMM111,space(1))
            this.w_FLANAL = NVL(TDFLANAL111,space(1))
            this.w_FLRIDE = NVL(TDFLRIDE111,space(1))
            this.w_FLVSRI = NVL(TDFLVSRI111,space(1))
            this.w_TPVSRI = NVL(TDTPVDOC111,space(3))
            this.w_TPRDES = NVL(TDTPRDES111,space(3))
            this.w_FLELAN = NVL(TDFLELAN111,space(1))
            this.w_MV_SEGNO = NVL(TD_SEGNO111,space(1))
            this.w_OALFDOC = NVL(TDALFDOC111,space(10))
            this.w_GENPRO = NVL(TDFLPROV111,space(1))
            this.w_FLSPIN = NVL(TDFLSPIN111,space(1))
            this.w_PRZVAC = NVL(TDPRZVAC111,space(1))
            this.w_PRZDES = NVL(TDPRZDES111,space(1))
            this.w_PSMINIMP = NVL(TDMINIMP111,0)
            this.w_PSMINVAL = NVL(TDMINVAL111,space(3))
            this.w_TDFLIA01 = NVL(TDFLIA01111,space(1))
            this.w_TDFLIA02 = NVL(TDFLIA02111,space(1))
            this.w_TDFLIA03 = NVL(TDFLIA03111,space(1))
            this.w_TDFLIA04 = NVL(TDFLIA04111,space(1))
            this.w_TDFLIA05 = NVL(TDFLIA05111,space(1))
            this.w_TDFLIA06 = NVL(TDFLIA06111,space(1))
            this.w_TDFLRA01 = NVL(TDFLRA01111,space(1))
            this.w_TDFLRA02 = NVL(TDFLRA02111,space(1))
            this.w_TDFLRA03 = NVL(TDFLRA03111,space(1))
            this.w_TDFLRA04 = NVL(TDFLRA04111,space(1))
            this.w_TDFLRA05 = NVL(TDFLRA05111,space(1))
            this.w_TDFLRA06 = NVL(TDFLRA06111,space(1))
          else
          .link_1_11('Load')
          endif
        .w_PSNUMDOC = NVL(PSNUMDOC,0)
        .w_PSALFDOC = NVL(PSALFDOC,space(10))
        .w_PSDATDOC = NVL(cp_ToDate(PSDATDOC),ctod("  /  /  "))
        .w_PSDATCIV = NVL(cp_ToDate(PSDATCIV),ctod("  /  /  "))
        .w_PSDATDIV = NVL(cp_ToDate(PSDATDIV),ctod("  /  /  "))
        .w_PSPAGFOR = NVL(PSPAGFOR,space(5))
          if link_1_18_joined
            this.w_PSPAGFOR = NVL(PACODICE118,NVL(this.w_PSPAGFOR,space(5)))
            this.w_PADESFOR = NVL(PADESCRI118,space(30))
          else
          .link_1_18('Load')
          endif
        .w_PSFLRAGG = NVL(PSFLRAGG,space(1))
        .w_PSFLVEBD = NVL(PSFLVEBD,space(1))
        .w_PSDATINI = NVL(cp_ToDate(PSDATINI),ctod("  /  /  "))
        .w_PSDATRIF = NVL(cp_ToDate(PSDATRIF),ctod("  /  /  "))
        .w_PSNUMINI = NVL(PSNUMINI,0)
        .w_PSALFDOI = NVL(PSALFDOI,space(10))
        .w_PSNUMFIN = NVL(PSNUMFIN,0)
        .w_PSALFDOF = NVL(PSALFDOF,space(10))
        .w_PSCODCLI = NVL(PSCODCLI,space(15))
          if link_1_29_joined
            this.w_PSCODCLI = NVL(ANCODICE129,NVL(this.w_PSCODCLI,space(15)))
            this.w_DESCLI = NVL(ANDESCRI129,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO129),ctod("  /  /  "))
          else
          .link_1_29('Load')
          endif
        .w_PSCODCLF = NVL(PSCODCLF,space(15))
          if link_1_31_joined
            this.w_PSCODCLF = NVL(ANCODICE131,NVL(this.w_PSCODCLF,space(15)))
            this.w_DESCLF = NVL(ANDESCRI131,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO131),ctod("  /  /  "))
          else
          .link_1_31('Load')
          endif
        .w_PSFLVALO = NVL(PSFLVALO,space(1))
        .w_MVCODESE = .w_PSCODESE
        .w_DATRIF = .w_PSDATRIF
        .w_MVTIPDOC = .w_PSTIPDOC
          .link_1_46('Load')
        .w_MVALFDOC = .w_PSALFDOC
        .w_MVNUMDOC = .w_PSNUMDOC
        .w_DATDOC = .w_PSDATDOC
        .w_MVDATCIV = .w_PSDATCIV
        .w_DATDIV = .w_PSDATDIV
        .w_CAUDOC = .w_PSTIPORI
        .w_CODCLI = .w_PSCODCLI
        .w_CODZON = .w_PSCODZON
        .w_CODAGE = .w_PSCODAGE
        .w_CODVAL = .w_PSCODVAL
        .w_FLVALO = .w_PSFLVALO
        .w_MVDATTRA = cp_CharToDate('  -  -  ')
        .w_MVORATRA = '  '
        .w_MVMINTRA = '  '
        .w_MVNOTAGG = ' '
        .w_MVANNPRO = CALPRO(.w_MVDATREG,.w_MVCODESE,.w_FLPPRO)
        .w_MVPRP = 'NN'
        .w_MVALFEST = '  '
        .w_MVANNDOC = STR(YEAR(.w_PSDATDOC), 4, 0)
        .w_MVFLPROV = .w_PSSTATUS
        .w_MVDATREG = .w_PSDATDOC
        .w_XVALO = IIF(.w_FLVALO='T', 1, 2)
        .w_OSTATUS = .w_PSSTATUS
        .w_MVRIFPIA = .w_PSSERIAL
        .w_FLPROV = .w_PSSTATUS
        .w_MVRIFFAD = .w_PSSERIAL
        .w_TIPOIN = .w_PSTIPDOC
        .w_DATAIN = .w_PSDATDOC
        .w_DATAFI = .w_PSDATDOC
        .w_DATA1 = .w_PSDATDOC
        .w_DATA2 = .w_PSDATDOC
        .w_CATDOC = .w_MVCLADOC
        .w_PSTIPORI = NVL(PSTIPORI,space(5))
          if link_2_3_joined
            this.w_PSTIPORI = NVL(TDTIPDOC203,NVL(this.w_PSTIPORI,space(5)))
            this.w_DESORI = NVL(TDDESDOC203,space(35))
            this.w_FLVEAC1 = NVL(TDFLVEAC203,space(1))
            this.w_FLGEFA1 = NVL(TFFLGEFA203,space(1))
          else
          .link_2_3('Load')
          endif
        .w_PSCODPAG = NVL(PSCODPAG,space(5))
          if link_2_5_joined
            this.w_PSCODPAG = NVL(PACODICE205,NVL(this.w_PSCODPAG,space(5)))
            this.w_DESPAG = NVL(PADESCRI205,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(PADTOBSO205),ctod("  /  /  "))
          else
          .link_2_5('Load')
          endif
        .w_PSCATCON = NVL(PSCATCON,space(5))
          if link_2_7_joined
            this.w_PSCATCON = NVL(C2CODICE207,NVL(this.w_PSCATCON,space(5)))
            this.w_DESCON = NVL(C2DESCRI207,space(35))
          else
          .link_2_7('Load')
          endif
        .w_PSCONSUP = NVL(PSCONSUP,space(15))
          if link_2_9_joined
            this.w_PSCONSUP = NVL(MCCODICE209,NVL(this.w_PSCONSUP,space(15)))
            this.w_DESSUP = NVL(MCDESCRI209,space(40))
          else
          .link_2_9('Load')
          endif
        .w_PSCATCOM = NVL(PSCATCOM,space(3))
          if link_2_11_joined
            this.w_PSCATCOM = NVL(CTCODICE211,NVL(this.w_PSCATCOM,space(3)))
            this.w_DESCOM = NVL(CTDESCRI211,space(35))
          else
          .link_2_11('Load')
          endif
        .w_PSCODDES = NVL(PSCODDES,space(5))
          .link_2_13('Load')
        .w_PSCODZON = NVL(PSCODZON,space(3))
          if link_2_15_joined
            this.w_PSCODZON = NVL(ZOCODZON215,NVL(this.w_PSCODZON,space(3)))
            this.w_DESZON = NVL(ZODESZON215,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(ZODTOBSO215),ctod("  /  /  "))
          else
          .link_2_15('Load')
          endif
        .w_PSCODAGE = NVL(PSCODAGE,space(5))
          if link_2_17_joined
            this.w_PSCODAGE = NVL(AGCODAGE217,NVL(this.w_PSCODAGE,space(5)))
            this.w_DESAGE = NVL(AGDESAGE217,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(AGDTOBSO217),ctod("  /  /  "))
          else
          .link_2_17('Load')
          endif
        .w_PSCODVAL = NVL(PSCODVAL,space(3))
          if link_2_19_joined
            this.w_PSCODVAL = NVL(VACODVAL219,NVL(this.w_PSCODVAL,space(3)))
            this.w_DESVAL = NVL(VADESVAL219,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(VADTOBSO219),ctod("  /  /  "))
          else
          .link_2_19('Load')
          endif
        .w_CODCLF = .w_PSCODCLF
        .w_CODPAG = .w_PSCODPAG
        .w_CATCON = .w_PSCATCON
        .w_CONSUP = .w_PSCONSUP
        .w_CATCOM = .w_PSCATCOM
        .w_ALFDO2 = .w_PSALFDOF
        .w_ALFDO1 = .w_PSALFDOI
        .w_NUMDO2 = .w_PSNUMFIN
        .w_NUMDO1 = .w_PSNUMINI
        .w_DATINI = .w_PSDATINI
        .w_TIPOFAT = IIF(.w_PSTIPFAT='T',' ',.w_PSTIPFAT)
        .w_FLRAGG = .w_PSFLRAGG
        .w_PSTIPFAT = NVL(PSTIPFAT,space(1))
        .w_OBTEST1 = IIF(EMPTY(.w_PSDATDOC), i_datsys, .w_PSDATDOC)
        .oPgFrm.Page1.oPag.oObj_1_152.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_153.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_154.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_155.Calculate()
        .w_PSMINIMP = NVL(PSMINIMP,0)
        .w_PSMINVAL = NVL(PSMINVAL,space(3))
          if link_2_38_joined
            this.w_PSMINVAL = NVL(VACODVAL238,NVL(this.w_PSMINVAL,space(3)))
            this.w_SIMVAL = NVL(VASIMVAL238,space(5))
          else
          .link_2_38('Load')
          endif
        .w_RIIVDTIN = IIF(NOT .w_FLRICIVA, CP_CHARTODATE('  -  -    '), .w_RIIVDTIN)
        .w_RIIVDTFI = IIF(NOT .w_FLRICIVA, CP_CHARTODATE('  -  -    '), .w_RIIVDTFI)
        .w_RIIVLSIV = IIF(NOT .w_FLRICIVA, '', .w_RIIVLSIV)
        .w_CODDES = .w_PSCODDES
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'FAT_DIFF')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_92.enabled = this.oPgFrm.Page1.oPag.oBtn_1_92.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_105.enabled = this.oPgFrm.Page1.oPag.oBtn_1_105.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PSSERIAL = space(10)
      .w_CODAZI = space(5)
      .w_PSTIPCLF = space(1)
      .w_MVTIPCON = space(1)
      .w_PSNUMREG = 0
      .w_PSDATREG = ctod("  /  /  ")
      .w_PSANNREG = space(4)
      .w_PSCODESE = space(4)
      .w_PSDESCRI = space(40)
      .w_PSSTATUS = space(1)
      .w_PSTIPDOC = space(5)
      .w_DESDOC = space(35)
      .w_PSNUMDOC = 0
      .w_PSALFDOC = space(10)
      .w_PSDATDOC = ctod("  /  /  ")
      .w_PSDATCIV = ctod("  /  /  ")
      .w_PSDATDIV = ctod("  /  /  ")
      .w_PSPAGFOR = space(5)
      .w_PSFLRAGG = space(1)
      .w_PSFLVEBD = space(1)
      .w_PSDATINI = ctod("  /  /  ")
      .w_FLRICIVA = .f.
      .w_PSDATRIF = ctod("  /  /  ")
      .w_PSNUMINI = 0
      .w_PSALFDOI = space(10)
      .w_PSNUMFIN = 0
      .w_PSALFDOF = space(10)
      .w_PSCODCLI = space(15)
      .w_DESCLI = space(40)
      .w_PSCODCLF = space(15)
      .w_DESCLF = space(40)
      .w_PSFLVALO = space(1)
      .w_STADOC = space(1)
      .w_MVCODESE = space(4)
      .w_DATRIF = ctod("  /  /  ")
      .w_MVTIPDOC = space(5)
      .w_MVFLVEAC = space(1)
      .w_FLGEFA = space(1)
      .w_MVPRD = space(2)
      .w_MVCLADOC = space(2)
      .w_MVFLACCO = space(1)
      .w_MVFLINTE = space(1)
      .w_MVTCAMAG = space(5)
      .w_MVTFRAGG = space(1)
      .w_MVCAUCON = space(5)
      .w_STAMPA = 0
      .w_FLNSRI = space(1)
      .w_TPNSRI = space(3)
      .w_FLDTPR = space(1)
      .w_FLGCOM = space(1)
      .w_FLANAL = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_MVALFDOC = space(10)
      .w_MVNUMDOC = 0
      .w_DATDOC = ctod("  /  /  ")
      .w_MVDATCIV = ctod("  /  /  ")
      .w_DATDIV = ctod("  /  /  ")
      .w_FLVEAC1 = space(1)
      .w_FLGEFA1 = space(1)
      .w_CAUDOC = space(5)
      .w_CODCLI = space(15)
      .w_CODZON = space(3)
      .w_CODAGE = space(5)
      .w_CODVAL = space(3)
      .w_FLVALO = space(1)
      .w_MVDATTRA = ctod("  /  /  ")
      .w_MVORATRA = space(2)
      .w_MVMINTRA = space(2)
      .w_MVNOTAGG = space(50)
      .w_MVANNPRO = space(4)
      .w_MVNUMEST = 0
      .w_MVPRP = space(2)
      .w_MVALFEST = space(10)
      .w_MVANNDOC = space(4)
      .w_MVFLPROV = space(1)
      .w_MVCODUTE = 0
      .w_MVNUMREG = 0
      .w_MVDATREG = ctod("  /  /  ")
      .w_FLPACK = space(1)
      .w_XVALO = 0
      .w_MVSERIAL = space(10)
      .w_OSTATUS = space(1)
      .w_MVRIFPIA = space(10)
      .w_FLPROV = space(1)
      .w_MVRIFFAD = space(10)
      .w_TIPOIN = space(5)
      .w_DATAIN = ctod("  /  /  ")
      .w_DATAFI = ctod("  /  /  ")
      .w_SERIE1 = space(2)
      .w_SERIE2 = space(2)
      .w_CODESE = space(4)
      .w_DATA1 = ctod("  /  /  ")
      .w_DATA2 = ctod("  /  /  ")
      .w_NUMINI = 0
      .w_NUMFIN = 0
      .w_CLIFOR = space(15)
      .w_CATDOC = space(2)
      .w_DEFALF = space(10)
      .w_FLVEAC = space(1)
      .w_ESCL1 = space(3)
      .w_ESCL2 = space(3)
      .w_ESCL3 = space(3)
      .w_ESCL4 = space(3)
      .w_ESCL5 = space(3)
      .w_FLPPRO = space(1)
      .w_FLVSRI = space(1)
      .w_FLRIDE = space(1)
      .w_TPRDES = space(3)
      .w_TPVSRI = space(3)
      .w_PSTIPORI = space(5)
      .w_DESORI = space(35)
      .w_PSCODPAG = space(5)
      .w_DESPAG = space(30)
      .w_PSCATCON = space(5)
      .w_DESCON = space(35)
      .w_PSCONSUP = space(15)
      .w_DESSUP = space(40)
      .w_PSCATCOM = space(3)
      .w_DESCOM = space(35)
      .w_PSCODDES = space(5)
      .w_NOMDES = space(40)
      .w_PSCODZON = space(3)
      .w_DESZON = space(35)
      .w_PSCODAGE = space(5)
      .w_DESAGE = space(35)
      .w_PSCODVAL = space(3)
      .w_DESVAL = space(35)
      .w_CODCLF = space(15)
      .w_CODPAG = space(5)
      .w_CATCON = space(5)
      .w_CONSUP = space(15)
      .w_CATCOM = space(3)
      .w_ALFDO2 = space(10)
      .w_ALFDO1 = space(10)
      .w_NUMDO2 = 0
      .w_NUMDO1 = 0
      .w_DATINI = ctod("  /  /  ")
      .w_TIPOFAT = space(1)
      .w_FLRAGG = space(1)
      .w_CCTIPDOC = space(2)
      .w_PSTIPFAT = space(1)
      .w_MVRIFODL = space(10)
      .w_TIPCON = space(1)
      .w_OBTEST1 = ctod("  /  /  ")
      .w_TIPRIF = space(2)
      .w_FLELAN = space(1)
      .w_FLCAMB = space(1)
      .w_MV_SEGNO = space(1)
      .w_OKAGG = .f.
      .w_OALFDOC = space(10)
      .w_GENPRO = space(1)
      .w_FLSPIN = space(1)
      .w_DTOBBL = space(1)
      .w_CCTIPREG = space(1)
      .w_REPSEC = space(1)
      .w_CODART = space(10)
      .w_PRZVAC = space(1)
      .w_PRZDES = space(1)
      .w_PSMINIMP = 0
      .w_PSMINVAL = space(3)
      .w_SIMVAL = space(5)
      .w_GESRIT = space(1)
      .w_FLIVDF = space(1)
      .w_RIIVDTIN = ctod("  /  /  ")
      .w_RIIVDTFI = ctod("  /  /  ")
      .w_RIIVLSIV = space(0)
      .w_CATEGO = space(2)
      .w_NOSBAN = space(5)
      .w_CODVET = space(5)
      .w_LINGUA = space(3)
      .w_TDFLIA01 = space(1)
      .w_TDFLIA02 = space(1)
      .w_TDFLIA03 = space(1)
      .w_TDFLIA04 = space(1)
      .w_TDFLIA05 = space(1)
      .w_TDFLIA06 = space(1)
      .w_TDFLRA01 = space(1)
      .w_TDFLRA02 = space(1)
      .w_TDFLRA03 = space(1)
      .w_TDFLRA04 = space(1)
      .w_TDFLRA05 = space(1)
      .w_TDFLRA06 = space(1)
      .w_PADESFOR = space(30)
      .w_CODDES = space(5)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_CODAZI))
          .link_1_2('Full')
          endif
        .w_PSTIPCLF = 'C'
        .w_MVTIPCON = 'C'
          .DoRTCalc(5,5,.f.)
        .w_PSDATREG = i_DATSYS
        .w_PSANNREG = STR(YEAR(.w_PSDATREG),4,0)
        .w_PSCODESE = CALCESER(.w_PSDATREG, g_CODESE)
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_PSCODESE))
          .link_1_8('Full')
          endif
          .DoRTCalc(9,9,.f.)
        .w_PSSTATUS = 'N'
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_PSTIPDOC))
          .link_1_11('Full')
          endif
          .DoRTCalc(12,14,.f.)
        .w_PSDATDOC = i_datsys
        .w_PSDATCIV = .w_PSDATDOC
        .w_PSDATDIV = i_datsys
        .w_PSPAGFOR = IIF(!EMPTY(.w_PSTIPDOC) AND .w_FLGEFA='B', .w_PSPAGFOR, SPACE(3) )
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_PSPAGFOR))
          .link_1_18('Full')
          endif
        .w_PSFLRAGG = ' '
        .w_PSFLVEBD = 'D'
        .w_PSDATINI = i_DATSYS - g_DTINFA
        .w_FLRICIVA = .F.
        .w_PSDATRIF = i_DATSYS + g_DTFIFA
          .DoRTCalc(24,24,.f.)
        .w_PSALFDOI = ''
          .DoRTCalc(26,26,.f.)
        .w_PSALFDOF = ''
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_PSCODCLI))
          .link_1_29('Full')
          endif
        .DoRTCalc(29,30,.f.)
          if not(empty(.w_PSCODCLF))
          .link_1_31('Full')
          endif
          .DoRTCalc(31,31,.f.)
        .w_PSFLVALO = 'T'
        .w_STADOC = 'S'
        .w_MVCODESE = .w_PSCODESE
        .w_DATRIF = .w_PSDATRIF
        .w_MVTIPDOC = .w_PSTIPDOC
        .DoRTCalc(37,45,.f.)
          if not(empty(.w_MVCAUCON))
          .link_1_46('Full')
          endif
          .DoRTCalc(46,51,.f.)
        .w_OBTEST = i_datsys
          .DoRTCalc(53,53,.f.)
        .w_MVALFDOC = .w_PSALFDOC
        .w_MVNUMDOC = .w_PSNUMDOC
        .w_DATDOC = .w_PSDATDOC
        .w_MVDATCIV = .w_PSDATCIV
        .w_DATDIV = .w_PSDATDIV
          .DoRTCalc(59,60,.f.)
        .w_CAUDOC = .w_PSTIPORI
        .w_CODCLI = .w_PSCODCLI
        .w_CODZON = .w_PSCODZON
        .w_CODAGE = .w_PSCODAGE
        .w_CODVAL = .w_PSCODVAL
        .w_FLVALO = .w_PSFLVALO
        .w_MVDATTRA = cp_CharToDate('  -  -  ')
        .w_MVORATRA = '  '
        .w_MVMINTRA = '  '
        .w_MVNOTAGG = ' '
        .w_MVANNPRO = CALPRO(.w_MVDATREG,.w_MVCODESE,.w_FLPPRO)
        .w_MVNUMEST = 0
        .w_MVPRP = 'NN'
        .w_MVALFEST = '  '
        .w_MVANNDOC = STR(YEAR(.w_PSDATDOC), 4, 0)
        .w_MVFLPROV = .w_PSSTATUS
        .w_MVCODUTE = IIF(g_MAGUTE='S', 0, i_CODUTE)
        .w_MVNUMREG = 0
        .w_MVDATREG = .w_PSDATDOC
        .w_FLPACK = ' '
        .w_XVALO = IIF(.w_FLVALO='T', 1, 2)
        .w_MVSERIAL = SPACE(10)
        .w_OSTATUS = .w_PSSTATUS
        .w_MVRIFPIA = .w_PSSERIAL
        .w_FLPROV = .w_PSSTATUS
        .w_MVRIFFAD = .w_PSSERIAL
        .w_TIPOIN = .w_PSTIPDOC
        .w_DATAIN = .w_PSDATDOC
        .w_DATAFI = .w_PSDATDOC
          .DoRTCalc(90,92,.f.)
        .w_DATA1 = .w_PSDATDOC
        .w_DATA2 = .w_PSDATDOC
          .DoRTCalc(95,97,.f.)
        .w_CATDOC = .w_MVCLADOC
          .DoRTCalc(99,99,.f.)
        .w_FLVEAC = 'V'
        .DoRTCalc(101,111,.f.)
          if not(empty(.w_PSTIPORI))
          .link_2_3('Full')
          endif
        .DoRTCalc(112,113,.f.)
          if not(empty(.w_PSCODPAG))
          .link_2_5('Full')
          endif
        .DoRTCalc(114,115,.f.)
          if not(empty(.w_PSCATCON))
          .link_2_7('Full')
          endif
        .DoRTCalc(116,117,.f.)
          if not(empty(.w_PSCONSUP))
          .link_2_9('Full')
          endif
        .DoRTCalc(118,119,.f.)
          if not(empty(.w_PSCATCOM))
          .link_2_11('Full')
          endif
        .DoRTCalc(120,121,.f.)
          if not(empty(.w_PSCODDES))
          .link_2_13('Full')
          endif
        .DoRTCalc(122,123,.f.)
          if not(empty(.w_PSCODZON))
          .link_2_15('Full')
          endif
        .DoRTCalc(124,125,.f.)
          if not(empty(.w_PSCODAGE))
          .link_2_17('Full')
          endif
        .DoRTCalc(126,127,.f.)
          if not(empty(.w_PSCODVAL))
          .link_2_19('Full')
          endif
          .DoRTCalc(128,128,.f.)
        .w_CODCLF = .w_PSCODCLF
        .w_CODPAG = .w_PSCODPAG
        .w_CATCON = .w_PSCATCON
        .w_CONSUP = .w_PSCONSUP
        .w_CATCOM = .w_PSCATCOM
        .w_ALFDO2 = .w_PSALFDOF
        .w_ALFDO1 = .w_PSALFDOI
        .w_NUMDO2 = .w_PSNUMFIN
        .w_NUMDO1 = .w_PSNUMINI
        .w_DATINI = .w_PSDATINI
        .w_TIPOFAT = IIF(.w_PSTIPFAT='T',' ',.w_PSTIPFAT)
        .w_FLRAGG = .w_PSFLRAGG
          .DoRTCalc(141,141,.f.)
        .w_PSTIPFAT = "T"
        .w_MVRIFODL = SPACE(10)
          .DoRTCalc(144,144,.f.)
        .w_OBTEST1 = IIF(EMPTY(.w_PSDATDOC), i_datsys, .w_PSDATDOC)
        .oPgFrm.Page1.oPag.oObj_1_152.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_153.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_154.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_155.Calculate()
          .DoRTCalc(146,149,.f.)
        .w_OKAGG = .T.
          .DoRTCalc(151,155,.f.)
        .w_REPSEC = 'S'
        .DoRTCalc(157,161,.f.)
          if not(empty(.w_PSMINVAL))
          .link_2_38('Full')
          endif
          .DoRTCalc(162,164,.f.)
        .w_RIIVDTIN = IIF(NOT .w_FLRICIVA, CP_CHARTODATE('  -  -    '), .w_RIIVDTIN)
        .w_RIIVDTFI = IIF(NOT .w_FLRICIVA, CP_CHARTODATE('  -  -    '), .w_RIIVDTFI)
        .w_RIIVLSIV = IIF(NOT .w_FLRICIVA, '', .w_RIIVLSIV)
          .DoRTCalc(168,184,.f.)
        .w_CODDES = .w_PSCODDES
      endif
    endwith
    cp_BlankRecExtFlds(this,'FAT_DIFF')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_92.enabled = this.oPgFrm.Page1.oPag.oBtn_1_92.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_105.enabled = this.oPgFrm.Page1.oPag.oBtn_1_105.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.FAT_DIFF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FAT_DIFF_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEFATDIF","i_codazi,w_PSSERIAL")
      cp_AskTableProg(this,i_nConn,"PRFATDIF","i_codazi,w_PSANNREG,w_PSNUMREG")
      .op_codazi = .w_codazi
      .op_PSSERIAL = .w_PSSERIAL
      .op_codazi = .w_codazi
      .op_PSANNREG = .w_PSANNREG
      .op_PSNUMREG = .w_PSNUMREG
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPSNUMREG_1_5.enabled = i_bVal
      .Page1.oPag.oPSDATREG_1_6.enabled = i_bVal
      .Page1.oPag.oPSCODESE_1_8.enabled = i_bVal
      .Page1.oPag.oPSDESCRI_1_9.enabled = i_bVal
      .Page1.oPag.oPSSTATUS_1_10.enabled = i_bVal
      .Page1.oPag.oPSTIPDOC_1_11.enabled = i_bVal
      .Page1.oPag.oPSNUMDOC_1_13.enabled = i_bVal
      .Page1.oPag.oPSALFDOC_1_14.enabled = i_bVal
      .Page1.oPag.oPSDATDOC_1_15.enabled = i_bVal
      .Page1.oPag.oPSDATCIV_1_16.enabled = i_bVal
      .Page1.oPag.oPSDATDIV_1_17.enabled = i_bVal
      .Page1.oPag.oPSPAGFOR_1_18.enabled = i_bVal
      .Page1.oPag.oPSFLRAGG_1_20.enabled = i_bVal
      .Page1.oPag.oPSFLVEBD_1_21.enabled = i_bVal
      .Page1.oPag.oPSDATINI_1_22.enabled = i_bVal
      .Page1.oPag.oFLRICIVA_1_23.enabled = i_bVal
      .Page1.oPag.oPSDATRIF_1_24.enabled = i_bVal
      .Page1.oPag.oPSNUMINI_1_25.enabled = i_bVal
      .Page1.oPag.oPSALFDOI_1_26.enabled = i_bVal
      .Page1.oPag.oPSNUMFIN_1_27.enabled = i_bVal
      .Page1.oPag.oPSALFDOF_1_28.enabled = i_bVal
      .Page1.oPag.oPSCODCLI_1_29.enabled = i_bVal
      .Page1.oPag.oPSCODCLF_1_31.enabled = i_bVal
      .Page1.oPag.oPSFLVALO_1_33.enabled = i_bVal
      .Page1.oPag.oSTADOC_1_34.enabled = i_bVal
      .Page2.oPag.oPSTIPORI_2_3.enabled = i_bVal
      .Page2.oPag.oPSCODPAG_2_5.enabled = i_bVal
      .Page2.oPag.oPSCATCON_2_7.enabled = i_bVal
      .Page2.oPag.oPSCONSUP_2_9.enabled = i_bVal
      .Page2.oPag.oPSCATCOM_2_11.enabled = i_bVal
      .Page2.oPag.oPSCODDES_2_13.enabled = i_bVal
      .Page2.oPag.oPSCODZON_2_15.enabled = i_bVal
      .Page2.oPag.oPSCODAGE_2_17.enabled = i_bVal
      .Page2.oPag.oPSCODVAL_2_19.enabled = i_bVal
      .Page2.oPag.oPSTIPFAT_2_34.enabled = i_bVal
      .Page1.oPag.oREPSEC_1_162.enabled = i_bVal
      .Page2.oPag.oPSMINIMP_2_37.enabled = i_bVal
      .Page2.oPag.oPSMINVAL_2_38.enabled = i_bVal
      .Page1.oPag.oBtn_1_19.enabled = i_bVal
      .Page1.oPag.oBtn_1_91.enabled = i_bVal
      .Page1.oPag.oBtn_1_92.enabled = .Page1.oPag.oBtn_1_92.mCond()
      .Page1.oPag.oBtn_1_100.enabled = i_bVal
      .Page1.oPag.oBtn_1_105.enabled = .Page1.oPag.oBtn_1_105.mCond()
      .Page1.oPag.oObj_1_152.enabled = i_bVal
      .Page1.oPag.oObj_1_153.enabled = i_bVal
      .Page1.oPag.oObj_1_154.enabled = i_bVal
      .Page1.oPag.oObj_1_155.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oPSNUMREG_1_5.enabled = .t.
      endif
    endwith
    this.GSVE_MFD.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'FAT_DIFF',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSVE_MFD.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.FAT_DIFF_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSSERIAL,"PSSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSTIPCLF,"PSTIPCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSNUMREG,"PSNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSDATREG,"PSDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSANNREG,"PSANNREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODESE,"PSCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSDESCRI,"PSDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSSTATUS,"PSSTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSTIPDOC,"PSTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSNUMDOC,"PSNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSALFDOC,"PSALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSDATDOC,"PSDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSDATCIV,"PSDATCIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSDATDIV,"PSDATDIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSPAGFOR,"PSPAGFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSFLRAGG,"PSFLRAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSFLVEBD,"PSFLVEBD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSDATINI,"PSDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSDATRIF,"PSDATRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSNUMINI,"PSNUMINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSALFDOI,"PSALFDOI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSNUMFIN,"PSNUMFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSALFDOF,"PSALFDOF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODCLI,"PSCODCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODCLF,"PSCODCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSFLVALO,"PSFLVALO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSTIPORI,"PSTIPORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODPAG,"PSCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCATCON,"PSCATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCONSUP,"PSCONSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCATCOM,"PSCATCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODDES,"PSCODDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODZON,"PSCODZON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODAGE,"PSCODAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSCODVAL,"PSCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSTIPFAT,"PSTIPFAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSMINIMP,"PSMINIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PSMINVAL,"PSMINVAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.FAT_DIFF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FAT_DIFF_IDX,2])
    i_lTable = "FAT_DIFF"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.FAT_DIFF_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.FAT_DIFF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FAT_DIFF_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.FAT_DIFF_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEFATDIF","i_codazi,w_PSSERIAL")
          cp_NextTableProg(this,i_nConn,"PRFATDIF","i_codazi,w_PSANNREG,w_PSNUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into FAT_DIFF
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'FAT_DIFF')
        i_extval=cp_InsertValODBCExtFlds(this,'FAT_DIFF')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PSSERIAL,PSTIPCLF,PSNUMREG,PSDATREG,PSANNREG"+;
                  ",PSCODESE,PSDESCRI,PSSTATUS,PSTIPDOC,PSNUMDOC"+;
                  ",PSALFDOC,PSDATDOC,PSDATCIV,PSDATDIV,PSPAGFOR"+;
                  ",PSFLRAGG,PSFLVEBD,PSDATINI,PSDATRIF,PSNUMINI"+;
                  ",PSALFDOI,PSNUMFIN,PSALFDOF,PSCODCLI,PSCODCLF"+;
                  ",PSFLVALO,PSTIPORI,PSCODPAG,PSCATCON,PSCONSUP"+;
                  ",PSCATCOM,PSCODDES,PSCODZON,PSCODAGE,PSCODVAL"+;
                  ",PSTIPFAT,PSMINIMP,PSMINVAL "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PSSERIAL)+;
                  ","+cp_ToStrODBC(this.w_PSTIPCLF)+;
                  ","+cp_ToStrODBC(this.w_PSNUMREG)+;
                  ","+cp_ToStrODBC(this.w_PSDATREG)+;
                  ","+cp_ToStrODBC(this.w_PSANNREG)+;
                  ","+cp_ToStrODBCNull(this.w_PSCODESE)+;
                  ","+cp_ToStrODBC(this.w_PSDESCRI)+;
                  ","+cp_ToStrODBC(this.w_PSSTATUS)+;
                  ","+cp_ToStrODBCNull(this.w_PSTIPDOC)+;
                  ","+cp_ToStrODBC(this.w_PSNUMDOC)+;
                  ","+cp_ToStrODBC(this.w_PSALFDOC)+;
                  ","+cp_ToStrODBC(this.w_PSDATDOC)+;
                  ","+cp_ToStrODBC(this.w_PSDATCIV)+;
                  ","+cp_ToStrODBC(this.w_PSDATDIV)+;
                  ","+cp_ToStrODBCNull(this.w_PSPAGFOR)+;
                  ","+cp_ToStrODBC(this.w_PSFLRAGG)+;
                  ","+cp_ToStrODBC(this.w_PSFLVEBD)+;
                  ","+cp_ToStrODBC(this.w_PSDATINI)+;
                  ","+cp_ToStrODBC(this.w_PSDATRIF)+;
                  ","+cp_ToStrODBC(this.w_PSNUMINI)+;
                  ","+cp_ToStrODBC(this.w_PSALFDOI)+;
                  ","+cp_ToStrODBC(this.w_PSNUMFIN)+;
                  ","+cp_ToStrODBC(this.w_PSALFDOF)+;
                  ","+cp_ToStrODBCNull(this.w_PSCODCLI)+;
                  ","+cp_ToStrODBCNull(this.w_PSCODCLF)+;
                  ","+cp_ToStrODBC(this.w_PSFLVALO)+;
                  ","+cp_ToStrODBCNull(this.w_PSTIPORI)+;
                  ","+cp_ToStrODBCNull(this.w_PSCODPAG)+;
                  ","+cp_ToStrODBCNull(this.w_PSCATCON)+;
                  ","+cp_ToStrODBCNull(this.w_PSCONSUP)+;
                  ","+cp_ToStrODBCNull(this.w_PSCATCOM)+;
                  ","+cp_ToStrODBCNull(this.w_PSCODDES)+;
                  ","+cp_ToStrODBCNull(this.w_PSCODZON)+;
                  ","+cp_ToStrODBCNull(this.w_PSCODAGE)+;
                  ","+cp_ToStrODBCNull(this.w_PSCODVAL)+;
                  ","+cp_ToStrODBC(this.w_PSTIPFAT)+;
                  ","+cp_ToStrODBC(this.w_PSMINIMP)+;
                  ","+cp_ToStrODBCNull(this.w_PSMINVAL)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'FAT_DIFF')
        i_extval=cp_InsertValVFPExtFlds(this,'FAT_DIFF')
        cp_CheckDeletedKey(i_cTable,0,'PSSERIAL',this.w_PSSERIAL)
        INSERT INTO (i_cTable);
              (PSSERIAL,PSTIPCLF,PSNUMREG,PSDATREG,PSANNREG,PSCODESE,PSDESCRI,PSSTATUS,PSTIPDOC,PSNUMDOC,PSALFDOC,PSDATDOC,PSDATCIV,PSDATDIV,PSPAGFOR,PSFLRAGG,PSFLVEBD,PSDATINI,PSDATRIF,PSNUMINI,PSALFDOI,PSNUMFIN,PSALFDOF,PSCODCLI,PSCODCLF,PSFLVALO,PSTIPORI,PSCODPAG,PSCATCON,PSCONSUP,PSCATCOM,PSCODDES,PSCODZON,PSCODAGE,PSCODVAL,PSTIPFAT,PSMINIMP,PSMINVAL  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PSSERIAL;
                  ,this.w_PSTIPCLF;
                  ,this.w_PSNUMREG;
                  ,this.w_PSDATREG;
                  ,this.w_PSANNREG;
                  ,this.w_PSCODESE;
                  ,this.w_PSDESCRI;
                  ,this.w_PSSTATUS;
                  ,this.w_PSTIPDOC;
                  ,this.w_PSNUMDOC;
                  ,this.w_PSALFDOC;
                  ,this.w_PSDATDOC;
                  ,this.w_PSDATCIV;
                  ,this.w_PSDATDIV;
                  ,this.w_PSPAGFOR;
                  ,this.w_PSFLRAGG;
                  ,this.w_PSFLVEBD;
                  ,this.w_PSDATINI;
                  ,this.w_PSDATRIF;
                  ,this.w_PSNUMINI;
                  ,this.w_PSALFDOI;
                  ,this.w_PSNUMFIN;
                  ,this.w_PSALFDOF;
                  ,this.w_PSCODCLI;
                  ,this.w_PSCODCLF;
                  ,this.w_PSFLVALO;
                  ,this.w_PSTIPORI;
                  ,this.w_PSCODPAG;
                  ,this.w_PSCATCON;
                  ,this.w_PSCONSUP;
                  ,this.w_PSCATCOM;
                  ,this.w_PSCODDES;
                  ,this.w_PSCODZON;
                  ,this.w_PSCODAGE;
                  ,this.w_PSCODVAL;
                  ,this.w_PSTIPFAT;
                  ,this.w_PSMINIMP;
                  ,this.w_PSMINVAL;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.FAT_DIFF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FAT_DIFF_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.FAT_DIFF_IDX,i_nConn)
      *
      * update FAT_DIFF
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'FAT_DIFF')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PSTIPCLF="+cp_ToStrODBC(this.w_PSTIPCLF)+;
             ",PSNUMREG="+cp_ToStrODBC(this.w_PSNUMREG)+;
             ",PSDATREG="+cp_ToStrODBC(this.w_PSDATREG)+;
             ",PSANNREG="+cp_ToStrODBC(this.w_PSANNREG)+;
             ",PSCODESE="+cp_ToStrODBCNull(this.w_PSCODESE)+;
             ",PSDESCRI="+cp_ToStrODBC(this.w_PSDESCRI)+;
             ",PSSTATUS="+cp_ToStrODBC(this.w_PSSTATUS)+;
             ",PSTIPDOC="+cp_ToStrODBCNull(this.w_PSTIPDOC)+;
             ",PSNUMDOC="+cp_ToStrODBC(this.w_PSNUMDOC)+;
             ",PSALFDOC="+cp_ToStrODBC(this.w_PSALFDOC)+;
             ",PSDATDOC="+cp_ToStrODBC(this.w_PSDATDOC)+;
             ",PSDATCIV="+cp_ToStrODBC(this.w_PSDATCIV)+;
             ",PSDATDIV="+cp_ToStrODBC(this.w_PSDATDIV)+;
             ",PSPAGFOR="+cp_ToStrODBCNull(this.w_PSPAGFOR)+;
             ",PSFLRAGG="+cp_ToStrODBC(this.w_PSFLRAGG)+;
             ",PSFLVEBD="+cp_ToStrODBC(this.w_PSFLVEBD)+;
             ",PSDATINI="+cp_ToStrODBC(this.w_PSDATINI)+;
             ",PSDATRIF="+cp_ToStrODBC(this.w_PSDATRIF)+;
             ",PSNUMINI="+cp_ToStrODBC(this.w_PSNUMINI)+;
             ",PSALFDOI="+cp_ToStrODBC(this.w_PSALFDOI)+;
             ",PSNUMFIN="+cp_ToStrODBC(this.w_PSNUMFIN)+;
             ",PSALFDOF="+cp_ToStrODBC(this.w_PSALFDOF)+;
             ",PSCODCLI="+cp_ToStrODBCNull(this.w_PSCODCLI)+;
             ",PSCODCLF="+cp_ToStrODBCNull(this.w_PSCODCLF)+;
             ",PSFLVALO="+cp_ToStrODBC(this.w_PSFLVALO)+;
             ",PSTIPORI="+cp_ToStrODBCNull(this.w_PSTIPORI)+;
             ",PSCODPAG="+cp_ToStrODBCNull(this.w_PSCODPAG)+;
             ",PSCATCON="+cp_ToStrODBCNull(this.w_PSCATCON)+;
             ",PSCONSUP="+cp_ToStrODBCNull(this.w_PSCONSUP)+;
             ",PSCATCOM="+cp_ToStrODBCNull(this.w_PSCATCOM)+;
             ",PSCODDES="+cp_ToStrODBCNull(this.w_PSCODDES)+;
             ",PSCODZON="+cp_ToStrODBCNull(this.w_PSCODZON)+;
             ",PSCODAGE="+cp_ToStrODBCNull(this.w_PSCODAGE)+;
             ",PSCODVAL="+cp_ToStrODBCNull(this.w_PSCODVAL)+;
             ",PSTIPFAT="+cp_ToStrODBC(this.w_PSTIPFAT)+;
             ",PSMINIMP="+cp_ToStrODBC(this.w_PSMINIMP)+;
             ",PSMINVAL="+cp_ToStrODBCNull(this.w_PSMINVAL)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'FAT_DIFF')
        i_cWhere = cp_PKFox(i_cTable  ,'PSSERIAL',this.w_PSSERIAL  )
        UPDATE (i_cTable) SET;
              PSTIPCLF=this.w_PSTIPCLF;
             ,PSNUMREG=this.w_PSNUMREG;
             ,PSDATREG=this.w_PSDATREG;
             ,PSANNREG=this.w_PSANNREG;
             ,PSCODESE=this.w_PSCODESE;
             ,PSDESCRI=this.w_PSDESCRI;
             ,PSSTATUS=this.w_PSSTATUS;
             ,PSTIPDOC=this.w_PSTIPDOC;
             ,PSNUMDOC=this.w_PSNUMDOC;
             ,PSALFDOC=this.w_PSALFDOC;
             ,PSDATDOC=this.w_PSDATDOC;
             ,PSDATCIV=this.w_PSDATCIV;
             ,PSDATDIV=this.w_PSDATDIV;
             ,PSPAGFOR=this.w_PSPAGFOR;
             ,PSFLRAGG=this.w_PSFLRAGG;
             ,PSFLVEBD=this.w_PSFLVEBD;
             ,PSDATINI=this.w_PSDATINI;
             ,PSDATRIF=this.w_PSDATRIF;
             ,PSNUMINI=this.w_PSNUMINI;
             ,PSALFDOI=this.w_PSALFDOI;
             ,PSNUMFIN=this.w_PSNUMFIN;
             ,PSALFDOF=this.w_PSALFDOF;
             ,PSCODCLI=this.w_PSCODCLI;
             ,PSCODCLF=this.w_PSCODCLF;
             ,PSFLVALO=this.w_PSFLVALO;
             ,PSTIPORI=this.w_PSTIPORI;
             ,PSCODPAG=this.w_PSCODPAG;
             ,PSCATCON=this.w_PSCATCON;
             ,PSCONSUP=this.w_PSCONSUP;
             ,PSCATCOM=this.w_PSCATCOM;
             ,PSCODDES=this.w_PSCODDES;
             ,PSCODZON=this.w_PSCODZON;
             ,PSCODAGE=this.w_PSCODAGE;
             ,PSCODVAL=this.w_PSCODVAL;
             ,PSTIPFAT=this.w_PSTIPFAT;
             ,PSMINIMP=this.w_PSMINIMP;
             ,PSMINVAL=this.w_PSMINVAL;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSVE_MFD : Saving
      this.GSVE_MFD.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PSSERIAL,"DPSERIAL";
             )
      this.GSVE_MFD.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSVE_MFD : Deleting
    this.GSVE_MFD.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PSSERIAL,"DPSERIAL";
           )
    this.GSVE_MFD.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.FAT_DIFF_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.FAT_DIFF_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.FAT_DIFF_IDX,i_nConn)
      *
      * delete FAT_DIFF
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PSSERIAL',this.w_PSSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.FAT_DIFF_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.FAT_DIFF_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_CODAZI = i_CODAZI
          .link_1_2('Full')
            .w_PSTIPCLF = 'C'
            .w_MVTIPCON = 'C'
        .DoRTCalc(5,6,.t.)
            .w_PSANNREG = STR(YEAR(.w_PSDATREG),4,0)
        if .o_PSDATREG<>.w_PSDATREG
            .w_PSCODESE = CALCESER(.w_PSDATREG, g_CODESE)
          .link_1_8('Full')
        endif
        .DoRTCalc(9,15,.t.)
        if .o_PSDATDOC<>.w_PSDATDOC
            .w_PSDATCIV = .w_PSDATDOC
        endif
        .DoRTCalc(17,17,.t.)
        if .o_PSTIPDOC<>.w_PSTIPDOC
            .w_PSPAGFOR = IIF(!EMPTY(.w_PSTIPDOC) AND .w_FLGEFA='B', .w_PSPAGFOR, SPACE(3) )
          .link_1_18('Full')
        endif
        .DoRTCalc(19,33,.t.)
            .w_MVCODESE = .w_PSCODESE
            .w_DATRIF = .w_PSDATRIF
            .w_MVTIPDOC = .w_PSTIPDOC
        .DoRTCalc(37,44,.t.)
        if .o_PSTIPDOC<>.w_PSTIPDOC
          .link_1_46('Full')
        endif
        .DoRTCalc(46,53,.t.)
            .w_MVALFDOC = .w_PSALFDOC
            .w_MVNUMDOC = .w_PSNUMDOC
            .w_DATDOC = .w_PSDATDOC
            .w_MVDATCIV = .w_PSDATCIV
            .w_DATDIV = .w_PSDATDIV
        .DoRTCalc(59,60,.t.)
            .w_CAUDOC = .w_PSTIPORI
            .w_CODCLI = .w_PSCODCLI
        if .o_PSCODZON<>.w_PSCODZON
            .w_CODZON = .w_PSCODZON
        endif
        if .o_PSCODAGE<>.w_PSCODAGE
            .w_CODAGE = .w_PSCODAGE
        endif
            .w_CODVAL = .w_PSCODVAL
            .w_FLVALO = .w_PSFLVALO
            .w_MVDATTRA = cp_CharToDate('  -  -  ')
            .w_MVORATRA = '  '
            .w_MVMINTRA = '  '
            .w_MVNOTAGG = ' '
            .w_MVANNPRO = CALPRO(.w_MVDATREG,.w_MVCODESE,.w_FLPPRO)
        .DoRTCalc(72,72,.t.)
            .w_MVPRP = 'NN'
            .w_MVALFEST = '  '
            .w_MVANNDOC = STR(YEAR(.w_PSDATDOC), 4, 0)
            .w_MVFLPROV = .w_PSSTATUS
        .DoRTCalc(77,78,.t.)
            .w_MVDATREG = .w_PSDATDOC
        .DoRTCalc(80,80,.t.)
            .w_XVALO = IIF(.w_FLVALO='T', 1, 2)
        .DoRTCalc(82,82,.t.)
        if .o_PSSERIAL<>.w_PSSERIAL
            .w_OSTATUS = .w_PSSTATUS
        endif
            .w_MVRIFPIA = .w_PSSERIAL
            .w_FLPROV = .w_PSSTATUS
            .w_MVRIFFAD = .w_PSSERIAL
            .w_TIPOIN = .w_PSTIPDOC
            .w_DATAIN = .w_PSDATDOC
            .w_DATAFI = .w_PSDATDOC
        .DoRTCalc(90,92,.t.)
            .w_DATA1 = .w_PSDATDOC
            .w_DATA2 = .w_PSDATDOC
        .DoRTCalc(95,97,.t.)
            .w_CATDOC = .w_MVCLADOC
        .DoRTCalc(99,128,.t.)
            .w_CODCLF = .w_PSCODCLF
        if .o_PSCODPAG<>.w_PSCODPAG
            .w_CODPAG = .w_PSCODPAG
        endif
        if .o_PSCATCON<>.w_PSCATCON
            .w_CATCON = .w_PSCATCON
        endif
        if .o_PSCONSUP<>.w_PSCONSUP
            .w_CONSUP = .w_PSCONSUP
        endif
        if .o_PSCATCOM<>.w_PSCATCOM
            .w_CATCOM = .w_PSCATCOM
        endif
            .w_ALFDO2 = .w_PSALFDOF
            .w_ALFDO1 = .w_PSALFDOI
            .w_NUMDO2 = .w_PSNUMFIN
            .w_NUMDO1 = .w_PSNUMINI
            .w_DATINI = .w_PSDATINI
            .w_TIPOFAT = IIF(.w_PSTIPFAT='T',' ',.w_PSTIPFAT)
            .w_FLRAGG = .w_PSFLRAGG
        .DoRTCalc(141,144,.t.)
        if .o_PSDATDOC<>.w_PSDATDOC
            .w_OBTEST1 = IIF(EMPTY(.w_PSDATDOC), i_datsys, .w_PSDATDOC)
        endif
        .oPgFrm.Page1.oPag.oObj_1_152.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_153.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_154.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_155.Calculate()
        .DoRTCalc(146,160,.t.)
        if .o_PSTIPDOC<>.w_PSTIPDOC
          .link_2_38('Full')
        endif
        if .o_FLRICIVA<>.w_FLRICIVA
          .Calculate_WUICMSNHXF()
        endif
        .DoRTCalc(162,164,.t.)
        if .o_FLRICIVA<>.w_FLRICIVA
            .w_RIIVDTIN = IIF(NOT .w_FLRICIVA, CP_CHARTODATE('  -  -    '), .w_RIIVDTIN)
        endif
        if .o_FLRICIVA<>.w_FLRICIVA
            .w_RIIVDTFI = IIF(NOT .w_FLRICIVA, CP_CHARTODATE('  -  -    '), .w_RIIVDTFI)
        endif
        if .o_FLRICIVA<>.w_FLRICIVA
            .w_RIIVLSIV = IIF(NOT .w_FLRICIVA, '', .w_RIIVLSIV)
        endif
        .DoRTCalc(168,184,.t.)
        if .o_PSCODDES<>.w_PSCODDES
            .w_CODDES = .w_PSCODDES
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEFATDIF","i_codazi,w_PSSERIAL")
          .op_PSSERIAL = .w_PSSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_PSANNREG<>.w_PSANNREG
           cp_AskTableProg(this,i_nConn,"PRFATDIF","i_codazi,w_PSANNREG,w_PSNUMREG")
          .op_PSNUMREG = .w_PSNUMREG
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_PSANNREG = .w_PSANNREG
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_152.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_153.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_154.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_155.Calculate()
    endwith
  return

  proc Calculate_ECGEUBLJGD()
    with this
          * --- Abilita repsec
          AbilitaREPSEC(this;
             )
    endwith
  endproc
  proc Calculate_WUICMSNHXF()
    with this
          * --- Apertura maschera conferma
          .w_FLRICIVA = GSVE_BFI(this, "OPEN")
    endwith
  endproc
  proc Calculate_YXVFZFURAP()
    with this
          * --- Stampa
          .w_CODDES = ' '
          .w_CATCOM = ' '
          .w_CODZON = ' '
          .w_CODAGE = ' '
          .w_CODPAG = ' '
          GSVE_BRD(this;
              ,'F';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPSSTATUS_1_10.enabled = this.oPgFrm.Page1.oPag.oPSSTATUS_1_10.mCond()
    this.oPgFrm.Page1.oPag.oPSDATDOC_1_15.enabled = this.oPgFrm.Page1.oPag.oPSDATDOC_1_15.mCond()
    this.oPgFrm.Page1.oPag.oPSDATCIV_1_16.enabled = this.oPgFrm.Page1.oPag.oPSDATCIV_1_16.mCond()
    this.oPgFrm.Page1.oPag.oPSDATDIV_1_17.enabled = this.oPgFrm.Page1.oPag.oPSDATDIV_1_17.mCond()
    this.oPgFrm.Page1.oPag.oPSPAGFOR_1_18.enabled = this.oPgFrm.Page1.oPag.oPSPAGFOR_1_18.mCond()
    this.oPgFrm.Page1.oPag.oFLRICIVA_1_23.enabled = this.oPgFrm.Page1.oPag.oFLRICIVA_1_23.mCond()
    this.oPgFrm.Page1.oPag.oSTADOC_1_34.enabled = this.oPgFrm.Page1.oPag.oSTADOC_1_34.mCond()
    this.oPgFrm.Page2.oPag.oPSCODDES_2_13.enabled = this.oPgFrm.Page2.oPag.oPSCODDES_2_13.mCond()
    this.oPgFrm.Page2.oPag.oPSMINVAL_2_38.enabled = this.oPgFrm.Page2.oPag.oPSMINVAL_2_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_91.enabled = this.oPgFrm.Page1.oPag.oBtn_1_91.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_100.enabled = this.oPgFrm.Page1.oPag.oBtn_1_100.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_105.enabled = this.oPgFrm.Page1.oPag.oBtn_1_105.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_93.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_93.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_19.visible=!this.oPgFrm.Page1.oPag.oBtn_1_19.mHide()
    this.oPgFrm.Page1.oPag.oSTADOC_1_34.visible=!this.oPgFrm.Page1.oPag.oSTADOC_1_34.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_93.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_93.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_100.visible=!this.oPgFrm.Page1.oPag.oBtn_1_100.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_105.visible=!this.oPgFrm.Page1.oPag.oBtn_1_105.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_152.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_153.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_154.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_155.Event(cEvent)
        if lower(cEvent)==lower("Load")
          .Calculate_ECGEUBLJGD()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Stampa")
          .Calculate_YXVFZFURAP()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLCAMB,AZDTOBBL";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZFLCAMB,AZDTOBBL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_FLCAMB = NVL(_Link_.AZFLCAMB,space(1))
      this.w_DTOBBL = NVL(_Link_.AZDTOBBL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_FLCAMB = space(1)
      this.w_DTOBBL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PSCODESE
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_PSCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_PSCODESE))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oPSCODESE_1_8'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_PSCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_PSCODESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCODESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_PSCODESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PSTIPDOC
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PSTIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select *";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PSTIPDOC))
          select *;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPSTIPDOC_1_11'),i_cWhere,'GSVE_ATD',"Causali documenti",'GSVE1KFD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select *";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PSTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PSTIPDOC)
            select *;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESDOC = NVL(_Link_.TDDESDOC,space(35))
      this.w_MVFLVEAC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLGEFA = NVL(_Link_.TFFLGEFA,space(1))
      this.w_MVPRD = NVL(_Link_.TDPRODOC,space(2))
      this.w_MVCLADOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_MVFLACCO = NVL(_Link_.TDFLACCO,space(1))
      this.w_MVFLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_MVTCAMAG = NVL(_Link_.TDCAUMAG,space(5))
      this.w_MVTFRAGG = NVL(_Link_.TFFLRAGG,space(1))
      this.w_MVCAUCON = NVL(_Link_.TDCAUCON,space(5))
      this.w_STAMPA = NVL(_Link_.TDPRGSTA,0)
      this.w_FLNSRI = NVL(_Link_.TDFLNSRI,space(1))
      this.w_TPNSRI = NVL(_Link_.TDTPNDOC,space(3))
      this.w_FLDTPR = NVL(_Link_.TDFLDTPR,space(1))
      this.w_PSALFDOC = NVL(_Link_.TDALFDOC,space(10))
      this.w_DEFALF = NVL(_Link_.TDALFDOC,space(10))
      this.w_ESCL1 = NVL(_Link_.TDESCCL1,space(3))
      this.w_ESCL2 = NVL(_Link_.TDESCCL2,space(3))
      this.w_ESCL3 = NVL(_Link_.TDESCCL3,space(3))
      this.w_ESCL4 = NVL(_Link_.TDESCCL4,space(3))
      this.w_ESCL5 = NVL(_Link_.TDESCCL5,space(3))
      this.w_FLGCOM = NVL(_Link_.TDFLCOMM,space(1))
      this.w_FLANAL = NVL(_Link_.TDFLANAL,space(1))
      this.w_FLRIDE = NVL(_Link_.TDFLRIDE,space(1))
      this.w_FLVSRI = NVL(_Link_.TDFLVSRI,space(1))
      this.w_TPVSRI = NVL(_Link_.TDTPVDOC,space(3))
      this.w_TPRDES = NVL(_Link_.TDTPRDES,space(3))
      this.w_FLELAN = NVL(_Link_.TDFLELAN,space(1))
      this.w_MV_SEGNO = NVL(_Link_.TD_SEGNO,space(1))
      this.w_OALFDOC = NVL(_Link_.TDALFDOC,space(10))
      this.w_GENPRO = NVL(_Link_.TDFLPROV,space(1))
      this.w_FLSPIN = NVL(_Link_.TDFLSPIN,space(1))
      this.w_PRZVAC = NVL(_Link_.TDPRZVAC,space(1))
      this.w_PRZDES = NVL(_Link_.TDPRZDES,space(1))
      this.w_PSMINIMP = NVL(_Link_.TDMINIMP,0)
      this.w_PSMINVAL = NVL(_Link_.TDMINVAL,space(3))
      this.w_TDFLIA01 = NVL(_Link_.TDFLIA01,space(1))
      this.w_TDFLIA02 = NVL(_Link_.TDFLIA02,space(1))
      this.w_TDFLIA03 = NVL(_Link_.TDFLIA03,space(1))
      this.w_TDFLIA04 = NVL(_Link_.TDFLIA04,space(1))
      this.w_TDFLIA05 = NVL(_Link_.TDFLIA05,space(1))
      this.w_TDFLIA06 = NVL(_Link_.TDFLIA06,space(1))
      this.w_TDFLRA01 = NVL(_Link_.TDFLRA01,space(1))
      this.w_TDFLRA02 = NVL(_Link_.TDFLRA02,space(1))
      this.w_TDFLRA03 = NVL(_Link_.TDFLRA03,space(1))
      this.w_TDFLRA04 = NVL(_Link_.TDFLRA04,space(1))
      this.w_TDFLRA05 = NVL(_Link_.TDFLRA05,space(1))
      this.w_TDFLRA06 = NVL(_Link_.TDFLRA06,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PSTIPDOC = space(5)
      endif
      this.w_DESDOC = space(35)
      this.w_MVFLVEAC = space(1)
      this.w_FLGEFA = space(1)
      this.w_MVPRD = space(2)
      this.w_MVCLADOC = space(2)
      this.w_MVFLACCO = space(1)
      this.w_MVFLINTE = space(1)
      this.w_MVTCAMAG = space(5)
      this.w_MVTFRAGG = space(1)
      this.w_MVCAUCON = space(5)
      this.w_STAMPA = 0
      this.w_FLNSRI = space(1)
      this.w_TPNSRI = space(3)
      this.w_FLDTPR = space(1)
      this.w_PSALFDOC = space(10)
      this.w_DEFALF = space(10)
      this.w_ESCL1 = space(3)
      this.w_ESCL2 = space(3)
      this.w_ESCL3 = space(3)
      this.w_ESCL4 = space(3)
      this.w_ESCL5 = space(3)
      this.w_FLGCOM = space(1)
      this.w_FLANAL = space(1)
      this.w_FLRIDE = space(1)
      this.w_FLVSRI = space(1)
      this.w_TPVSRI = space(3)
      this.w_TPRDES = space(3)
      this.w_FLELAN = space(1)
      this.w_MV_SEGNO = space(1)
      this.w_OALFDOC = space(10)
      this.w_GENPRO = space(1)
      this.w_FLSPIN = space(1)
      this.w_PRZVAC = space(1)
      this.w_PRZDES = space(1)
      this.w_PSMINIMP = 0
      this.w_PSMINVAL = space(3)
      this.w_TDFLIA01 = space(1)
      this.w_TDFLIA02 = space(1)
      this.w_TDFLIA03 = space(1)
      this.w_TDFLIA04 = space(1)
      this.w_TDFLIA05 = space(1)
      this.w_TDFLIA06 = space(1)
      this.w_TDFLRA01 = space(1)
      this.w_TDFLRA02 = space(1)
      this.w_TDFLRA03 = space(1)
      this.w_TDFLRA04 = space(1)
      this.w_TDFLRA05 = space(1)
      this.w_TDFLRA06 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MVFLVEAC='V' AND .w_FLGEFA='B'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente , incongruente")
        endif
        this.w_PSTIPDOC = space(5)
        this.w_DESDOC = space(35)
        this.w_MVFLVEAC = space(1)
        this.w_FLGEFA = space(1)
        this.w_MVPRD = space(2)
        this.w_MVCLADOC = space(2)
        this.w_MVFLACCO = space(1)
        this.w_MVFLINTE = space(1)
        this.w_MVTCAMAG = space(5)
        this.w_MVTFRAGG = space(1)
        this.w_MVCAUCON = space(5)
        this.w_STAMPA = 0
        this.w_FLNSRI = space(1)
        this.w_TPNSRI = space(3)
        this.w_FLDTPR = space(1)
        this.w_PSALFDOC = space(10)
        this.w_DEFALF = space(10)
        this.w_ESCL1 = space(3)
        this.w_ESCL2 = space(3)
        this.w_ESCL3 = space(3)
        this.w_ESCL4 = space(3)
        this.w_ESCL5 = space(3)
        this.w_FLGCOM = space(1)
        this.w_FLANAL = space(1)
        this.w_FLRIDE = space(1)
        this.w_FLVSRI = space(1)
        this.w_TPVSRI = space(3)
        this.w_TPRDES = space(3)
        this.w_FLELAN = space(1)
        this.w_MV_SEGNO = space(1)
        this.w_OALFDOC = space(10)
        this.w_GENPRO = space(1)
        this.w_FLSPIN = space(1)
        this.w_PRZVAC = space(1)
        this.w_PRZDES = space(1)
        this.w_PSMINIMP = 0
        this.w_PSMINVAL = space(3)
        this.w_TDFLIA01 = space(1)
        this.w_TDFLIA02 = space(1)
        this.w_TDFLIA03 = space(1)
        this.w_TDFLIA04 = space(1)
        this.w_TDFLIA05 = space(1)
        this.w_TDFLIA06 = space(1)
        this.w_TDFLRA01 = space(1)
        this.w_TDFLRA02 = space(1)
        this.w_TDFLRA03 = space(1)
        this.w_TDFLRA04 = space(1)
        this.w_TDFLRA05 = space(1)
        this.w_TDFLRA06 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 49 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+49<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.TDTIPDOC as TDTIPDOC111"+ ",link_1_11.TDDESDOC as TDDESDOC111"+ ",link_1_11.TDFLVEAC as TDFLVEAC111"+ ",link_1_11.TFFLGEFA as TFFLGEFA111"+ ",link_1_11.TDPRODOC as TDPRODOC111"+ ",link_1_11.TDCATDOC as TDCATDOC111"+ ",link_1_11.TDFLACCO as TDFLACCO111"+ ",link_1_11.TDFLINTE as TDFLINTE111"+ ",link_1_11.TDCAUMAG as TDCAUMAG111"+ ",link_1_11.TFFLRAGG as TFFLRAGG111"+ ",link_1_11.TDCAUCON as TDCAUCON111"+ ",link_1_11.TDPRGSTA as TDPRGSTA111"+ ",link_1_11.TDFLNSRI as TDFLNSRI111"+ ",link_1_11.TDTPNDOC as TDTPNDOC111"+ ",link_1_11.TDFLDTPR as TDFLDTPR111"+ ",link_1_11.TDALFDOC as TDALFDOC111"+ ",link_1_11.TDALFDOC as TDALFDOC111"+ ",link_1_11.TDESCCL1 as TDESCCL1111"+ ",link_1_11.TDESCCL2 as TDESCCL2111"+ ",link_1_11.TDESCCL3 as TDESCCL3111"+ ",link_1_11.TDESCCL4 as TDESCCL4111"+ ",link_1_11.TDESCCL5 as TDESCCL5111"+ ",link_1_11.TDFLCOMM as TDFLCOMM111"+ ",link_1_11.TDFLANAL as TDFLANAL111"+ ",link_1_11.TDFLRIDE as TDFLRIDE111"+ ",link_1_11.TDFLVSRI as TDFLVSRI111"+ ",link_1_11.TDTPVDOC as TDTPVDOC111"+ ",link_1_11.TDTPRDES as TDTPRDES111"+ ",link_1_11.TDFLELAN as TDFLELAN111"+ ",link_1_11.TD_SEGNO as TD_SEGNO111"+ ",link_1_11.TDALFDOC as TDALFDOC111"+ ",link_1_11.TDFLPROV as TDFLPROV111"+ ",link_1_11.TDFLSPIN as TDFLSPIN111"+ ",link_1_11.TDPRZVAC as TDPRZVAC111"+ ",link_1_11.TDPRZDES as TDPRZDES111"+ ",link_1_11.TDMINIMP as TDMINIMP111"+ ",link_1_11.TDMINVAL as TDMINVAL111"+ ",link_1_11.TDFLIA01 as TDFLIA01111"+ ",link_1_11.TDFLIA02 as TDFLIA02111"+ ",link_1_11.TDFLIA03 as TDFLIA03111"+ ",link_1_11.TDFLIA04 as TDFLIA04111"+ ",link_1_11.TDFLIA05 as TDFLIA05111"+ ",link_1_11.TDFLIA06 as TDFLIA06111"+ ",link_1_11.TDFLRA01 as TDFLRA01111"+ ",link_1_11.TDFLRA02 as TDFLRA02111"+ ",link_1_11.TDFLRA03 as TDFLRA03111"+ ",link_1_11.TDFLRA04 as TDFLRA04111"+ ",link_1_11.TDFLRA05 as TDFLRA05111"+ ",link_1_11.TDFLRA06 as TDFLRA06111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on FAT_DIFF.PSTIPDOC=link_1_11.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+49
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and FAT_DIFF.PSTIPDOC=link_1_11.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+49
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSPAGFOR
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSPAGFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_PSPAGFOR)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_PSPAGFOR))
          select PACODICE,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSPAGFOR)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStrODBC(trim(this.w_PSPAGFOR)+"%");

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_PSPAGFOR)+"%");

            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PSPAGFOR) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oPSPAGFOR_1_18'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSPAGFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_PSPAGFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_PSPAGFOR)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSPAGFOR = NVL(_Link_.PACODICE,space(5))
      this.w_PADESFOR = NVL(_Link_.PADESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PSPAGFOR = space(5)
      endif
      this.w_PADESFOR = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSPAGFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.PACODICE as PACODICE118"+ ",link_1_18.PADESCRI as PADESCRI118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on FAT_DIFF.PSPAGFOR=link_1_18.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and FAT_DIFF.PSPAGFOR=link_1_18.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSCODCLI
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PSCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_PSTIPCLF;
                     ,'ANCODICE',trim(this.w_PSCODCLI))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCODCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PSCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PSCODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_PSTIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PSCODCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPSCODCLI_1_29'),i_cWhere,'GSAR_BZC',"Clienti",'GSAR_SCL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PSTIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale, inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PSCODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PSTIPCLF;
                       ,'ANCODICE',this.w_PSCODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PSCODCLI = space(15)
      endif
      this.w_DESCLI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PSCODCLF)) OR  (.w_PSCODCLI<=.w_PSCODCLF)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale, inesistente oppure obsoleto")
        endif
        this.w_PSCODCLI = space(15)
        this.w_DESCLI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_29(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_29.ANCODICE as ANCODICE129"+ ",link_1_29.ANDESCRI as ANDESCRI129"+ ",link_1_29.ANDTOBSO as ANDTOBSO129"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_29 on FAT_DIFF.PSCODCLI=link_1_29.ANCODICE"+" and FAT_DIFF.PSTIPCLF=link_1_29.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_29"
          i_cKey=i_cKey+'+" and FAT_DIFF.PSCODCLI=link_1_29.ANCODICE(+)"'+'+" and FAT_DIFF.PSTIPCLF=link_1_29.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSCODCLF
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PSCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_PSTIPCLF;
                     ,'ANCODICE',trim(this.w_PSCODCLF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PSCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PSCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_PSTIPCLF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PSCODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPSCODCLF_1_31'),i_cWhere,'GSAR_BZC',"Clienti",'GSAR_SCL.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PSTIPCLF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale, inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PSCODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PSTIPCLF;
                       ,'ANCODICE',this.w_PSCODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PSCODCLF = space(15)
      endif
      this.w_DESCLF = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PSCODCLF>=.w_PSCODCLI) or (empty(.w_PSCODCLI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale, inesistente oppure obsoleto")
        endif
        this.w_PSCODCLF = space(15)
        this.w_DESCLF = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_31(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_31.ANCODICE as ANCODICE131"+ ",link_1_31.ANDESCRI as ANDESCRI131"+ ",link_1_31.ANDTOBSO as ANDTOBSO131"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_31 on FAT_DIFF.PSCODCLF=link_1_31.ANCODICE"+" and FAT_DIFF.PSTIPCLF=link_1_31.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_31"
          i_cKey=i_cKey+'+" and FAT_DIFF.PSCODCLF=link_1_31.ANCODICE(+)"'+'+" and FAT_DIFF.PSTIPCLF=link_1_31.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MVCAUCON
  func Link_1_46(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCFLPPRO,CCTIPDOC,CCTIPREG,CCGESRIT,CCFLIVDF";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_MVCAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_MVCAUCON)
            select CCCODICE,CCFLPPRO,CCTIPDOC,CCTIPREG,CCGESRIT,CCFLIVDF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_FLPPRO = NVL(_Link_.CCFLPPRO,space(1))
      this.w_CCTIPDOC = NVL(_Link_.CCTIPDOC,space(2))
      this.w_CCTIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_GESRIT = NVL(_Link_.CCGESRIT,space(1))
      this.w_FLIVDF = NVL(_Link_.CCFLIVDF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MVCAUCON = space(5)
      endif
      this.w_FLPPRO = space(1)
      this.w_CCTIPDOC = space(2)
      this.w_CCTIPREG = space(1)
      this.w_GESRIT = space(1)
      this.w_FLIVDF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PSTIPORI
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSTIPORI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PSTIPORI)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TFFLGEFA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PSTIPORI))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TFFLGEFA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSTIPORI)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSTIPORI) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPSTIPORI_2_3'),i_cWhere,'GSVE_ATD',"Causali documenti",'GSVE2KFD.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TFFLGEFA";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TFFLGEFA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSTIPORI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TFFLGEFA";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PSTIPORI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PSTIPORI)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TFFLGEFA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSTIPORI = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESORI = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAC1 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_FLGEFA1 = NVL(_Link_.TFFLGEFA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PSTIPORI = space(5)
      endif
      this.w_DESORI = space(35)
      this.w_FLVEAC1 = space(1)
      this.w_FLGEFA1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CAUDOC) OR (.w_FLVEAC1='V' AND .w_FLGEFA1='S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_PSTIPORI = space(5)
        this.w_DESORI = space(35)
        this.w_FLVEAC1 = space(1)
        this.w_FLGEFA1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSTIPORI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.TDTIPDOC as TDTIPDOC203"+ ",link_2_3.TDDESDOC as TDDESDOC203"+ ",link_2_3.TDFLVEAC as TDFLVEAC203"+ ",link_2_3.TFFLGEFA as TFFLGEFA203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on FAT_DIFF.PSTIPORI=link_2_3.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and FAT_DIFF.PSTIPORI=link_2_3.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSCODPAG
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_PSCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_PSCODPAG))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oPSCODPAG_2_5'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_PSCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_PSCODPAG)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PSCODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_PSCODPAG = space(5)
        this.w_DESPAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.PACODICE as PACODICE205"+ ","+cp_TransLinkFldName('link_2_5.PADESCRI')+" as PADESCRI205"+ ",link_2_5.PADTOBSO as PADTOBSO205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on FAT_DIFF.PSCODPAG=link_2_5.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and FAT_DIFF.PSCODPAG=link_2_5.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSCATCON
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_PSCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_PSCATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSCATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oPSCATCON_2_7'),i_cWhere,'GSAR_AC2',"Categorie contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_PSCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_PSCATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCON = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PSCATCON = space(5)
      endif
      this.w_DESCON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria contabile inesistente oppure obsoleta")
        endif
        this.w_PSCATCON = space(5)
        this.w_DESCON = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CACOCLFO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.C2CODICE as C2CODICE207"+ ",link_2_7.C2DESCRI as C2DESCRI207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on FAT_DIFF.PSCATCON=link_2_7.C2CODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and FAT_DIFF.PSCATCON=link_2_7.C2CODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSCONSUP
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCONSUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_PSCONSUP)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_PSCONSUP))
          select MCCODICE,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCONSUP)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSCONSUP) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oPSCONSUP_2_9'),i_cWhere,'GSAR_AMC',"Mastri contabili (clienti)",'GSAR_ACL.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCONSUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_PSCONSUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_PSCONSUP)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCONSUP = NVL(_Link_.MCCODICE,space(15))
      this.w_DESSUP = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PSCONSUP = space(15)
      endif
      this.w_DESSUP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Mastro di raggruppamento inesistente oppure obsoleto")
        endif
        this.w_PSCONSUP = space(15)
        this.w_DESSUP = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCONSUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MASTRI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_9.MCCODICE as MCCODICE209"+ ",link_2_9.MCDESCRI as MCDESCRI209"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_9 on FAT_DIFF.PSCONSUP=link_2_9.MCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_9"
          i_cKey=i_cKey+'+" and FAT_DIFF.PSCONSUP=link_2_9.MCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSCATCOM
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_PSCATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_PSCATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSCATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oPSCATCOM_2_11'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_PSCATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_PSCATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCOM = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PSCATCOM = space(3)
      endif
      this.w_DESCOM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria commerciale inesistente oppure obsoleta")
        endif
        this.w_PSCATCOM = space(3)
        this.w_DESCOM = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATECOMM_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_11.CTCODICE as CTCODICE211"+ ",link_2_11.CTDESCRI as CTDESCRI211"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_11 on FAT_DIFF.PSCATCOM=link_2_11.CTCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_11"
          i_cKey=i_cKey+'+" and FAT_DIFF.PSCATCOM=link_2_11.CTCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSCODDES
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCODDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_PSCODDES)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_PSCODCLF);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_PSTIPCLF;
                     ,'DDCODICE',this.w_PSCODCLF;
                     ,'DDCODDES',trim(this.w_PSCODDES))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCODDES)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSCODDES) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oPSCODDES_2_13'),i_cWhere,'',"Destinazioni diverse",'GSVE1KFD.DES_DIVE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PSTIPCLF<>oSource.xKey(1);
           .or. this.w_PSCODCLF<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_PSCODCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCODDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_PSCODDES);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_PSTIPCLF);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_PSCODCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_PSTIPCLF;
                       ,'DDCODICE',this.w_PSCODCLF;
                       ,'DDCODDES',this.w_PSCODDES)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCODDES = NVL(_Link_.DDCODDES,space(5))
      this.w_NOMDES = NVL(_Link_.DDNOMDES,space(40))
      this.w_TIPRIF = NVL(_Link_.DDTIPRIF,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PSCODDES = space(5)
      endif
      this.w_NOMDES = space(40)
      this.w_TIPRIF = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  AND .w_TIPRIF='CO'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
        endif
        this.w_PSCODDES = space(5)
        this.w_NOMDES = space(40)
        this.w_TIPRIF = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCODDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PSCODZON
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_PSCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_PSCODZON))
          select ZOCODZON,ZODESZON,ZODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oPSCODZON_2_15'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_PSCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_PSCODZON)
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZON = NVL(_Link_.ZODESZON,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PSCODZON = space(3)
      endif
      this.w_DESZON = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice zona inesistente oppure obsoleta")
        endif
        this.w_PSCODZON = space(3)
        this.w_DESZON = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ZONE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_15.ZOCODZON as ZOCODZON215"+ ",link_2_15.ZODESZON as ZODESZON215"+ ",link_2_15.ZODTOBSO as ZODTOBSO215"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_15 on FAT_DIFF.PSCODZON=link_2_15.ZOCODZON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_15"
          i_cKey=i_cKey+'+" and FAT_DIFF.PSCODZON=link_2_15.ZOCODZON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSCODAGE
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_PSCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_PSCODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oPSCODAGE_2_17'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_PSCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_PSCODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PSCODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice agente inesistente oppure obsoleto")
        endif
        this.w_PSCODAGE = space(5)
        this.w_DESAGE = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_17.AGCODAGE as AGCODAGE217"+ ",link_2_17.AGDESAGE as AGDESAGE217"+ ",link_2_17.AGDTOBSO as AGDTOBSO217"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_17 on FAT_DIFF.PSCODAGE=link_2_17.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_17"
          i_cKey=i_cKey+'+" and FAT_DIFF.PSCODAGE=link_2_17.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSCODVAL
  func Link_2_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_PSCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_PSCODVAL))
          select VACODVAL,VADESVAL,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_PSCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_PSCODVAL)+"%");

            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PSCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oPSCODVAL_2_19'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PSCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PSCODVAL)
            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PSCODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice valuta inesistente oppure obsoleto")
        endif
        this.w_PSCODVAL = space(3)
        this.w_DESVAL = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_19.VACODVAL as VACODVAL219"+ ",link_2_19.VADESVAL as VADESVAL219"+ ",link_2_19.VADTOBSO as VADTOBSO219"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_19 on FAT_DIFF.PSCODVAL=link_2_19.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_19"
          i_cKey=i_cKey+'+" and FAT_DIFF.PSCODVAL=link_2_19.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PSMINVAL
  func Link_2_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PSMINVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_PSMINVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_PSMINVAL))
          select VACODVAL,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PSMINVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PSMINVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oPSMINVAL_2_38'),i_cWhere,'GSAR_AVL',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PSMINVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PSMINVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PSMINVAL)
            select VACODVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PSMINVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PSMINVAL = space(3)
      endif
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PSMINVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_38.VACODVAL as VACODVAL238"+ ",link_2_38.VASIMVAL as VASIMVAL238"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_38 on FAT_DIFF.PSMINVAL=link_2_38.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_38"
          i_cKey=i_cKey+'+" and FAT_DIFF.PSMINVAL=link_2_38.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPSNUMREG_1_5.value==this.w_PSNUMREG)
      this.oPgFrm.Page1.oPag.oPSNUMREG_1_5.value=this.w_PSNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPSDATREG_1_6.value==this.w_PSDATREG)
      this.oPgFrm.Page1.oPag.oPSDATREG_1_6.value=this.w_PSDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPSANNREG_1_7.value==this.w_PSANNREG)
      this.oPgFrm.Page1.oPag.oPSANNREG_1_7.value=this.w_PSANNREG
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODESE_1_8.value==this.w_PSCODESE)
      this.oPgFrm.Page1.oPag.oPSCODESE_1_8.value=this.w_PSCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oPSDESCRI_1_9.value==this.w_PSDESCRI)
      this.oPgFrm.Page1.oPag.oPSDESCRI_1_9.value=this.w_PSDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPSSTATUS_1_10.RadioValue()==this.w_PSSTATUS)
      this.oPgFrm.Page1.oPag.oPSSTATUS_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSTIPDOC_1_11.value==this.w_PSTIPDOC)
      this.oPgFrm.Page1.oPag.oPSTIPDOC_1_11.value=this.w_PSTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESDOC_1_12.value==this.w_DESDOC)
      this.oPgFrm.Page1.oPag.oDESDOC_1_12.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPSNUMDOC_1_13.value==this.w_PSNUMDOC)
      this.oPgFrm.Page1.oPag.oPSNUMDOC_1_13.value=this.w_PSNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPSALFDOC_1_14.value==this.w_PSALFDOC)
      this.oPgFrm.Page1.oPag.oPSALFDOC_1_14.value=this.w_PSALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPSDATDOC_1_15.value==this.w_PSDATDOC)
      this.oPgFrm.Page1.oPag.oPSDATDOC_1_15.value=this.w_PSDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPSDATCIV_1_16.value==this.w_PSDATCIV)
      this.oPgFrm.Page1.oPag.oPSDATCIV_1_16.value=this.w_PSDATCIV
    endif
    if not(this.oPgFrm.Page1.oPag.oPSDATDIV_1_17.value==this.w_PSDATDIV)
      this.oPgFrm.Page1.oPag.oPSDATDIV_1_17.value=this.w_PSDATDIV
    endif
    if not(this.oPgFrm.Page1.oPag.oPSPAGFOR_1_18.value==this.w_PSPAGFOR)
      this.oPgFrm.Page1.oPag.oPSPAGFOR_1_18.value=this.w_PSPAGFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oPSFLRAGG_1_20.RadioValue()==this.w_PSFLRAGG)
      this.oPgFrm.Page1.oPag.oPSFLRAGG_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSFLVEBD_1_21.RadioValue()==this.w_PSFLVEBD)
      this.oPgFrm.Page1.oPag.oPSFLVEBD_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSDATINI_1_22.value==this.w_PSDATINI)
      this.oPgFrm.Page1.oPag.oPSDATINI_1_22.value=this.w_PSDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oFLRICIVA_1_23.RadioValue()==this.w_FLRICIVA)
      this.oPgFrm.Page1.oPag.oFLRICIVA_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPSDATRIF_1_24.value==this.w_PSDATRIF)
      this.oPgFrm.Page1.oPag.oPSDATRIF_1_24.value=this.w_PSDATRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oPSNUMINI_1_25.value==this.w_PSNUMINI)
      this.oPgFrm.Page1.oPag.oPSNUMINI_1_25.value=this.w_PSNUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPSALFDOI_1_26.value==this.w_PSALFDOI)
      this.oPgFrm.Page1.oPag.oPSALFDOI_1_26.value=this.w_PSALFDOI
    endif
    if not(this.oPgFrm.Page1.oPag.oPSNUMFIN_1_27.value==this.w_PSNUMFIN)
      this.oPgFrm.Page1.oPag.oPSNUMFIN_1_27.value=this.w_PSNUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPSALFDOF_1_28.value==this.w_PSALFDOF)
      this.oPgFrm.Page1.oPag.oPSALFDOF_1_28.value=this.w_PSALFDOF
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODCLI_1_29.value==this.w_PSCODCLI)
      this.oPgFrm.Page1.oPag.oPSCODCLI_1_29.value=this.w_PSCODCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLI_1_30.value==this.w_DESCLI)
      this.oPgFrm.Page1.oPag.oDESCLI_1_30.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page1.oPag.oPSCODCLF_1_31.value==this.w_PSCODCLF)
      this.oPgFrm.Page1.oPag.oPSCODCLF_1_31.value=this.w_PSCODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_32.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_32.value=this.w_DESCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oPSFLVALO_1_33.RadioValue()==this.w_PSFLVALO)
      this.oPgFrm.Page1.oPag.oPSFLVALO_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSTADOC_1_34.RadioValue()==this.w_STADOC)
      this.oPgFrm.Page1.oPag.oSTADOC_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPSTIPORI_2_3.value==this.w_PSTIPORI)
      this.oPgFrm.Page2.oPag.oPSTIPORI_2_3.value=this.w_PSTIPORI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESORI_2_4.value==this.w_DESORI)
      this.oPgFrm.Page2.oPag.oDESORI_2_4.value=this.w_DESORI
    endif
    if not(this.oPgFrm.Page2.oPag.oPSCODPAG_2_5.value==this.w_PSCODPAG)
      this.oPgFrm.Page2.oPag.oPSCODPAG_2_5.value=this.w_PSCODPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPAG_2_6.value==this.w_DESPAG)
      this.oPgFrm.Page2.oPag.oDESPAG_2_6.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oPSCATCON_2_7.value==this.w_PSCATCON)
      this.oPgFrm.Page2.oPag.oPSCATCON_2_7.value=this.w_PSCATCON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON_2_8.value==this.w_DESCON)
      this.oPgFrm.Page2.oPag.oDESCON_2_8.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oPSCONSUP_2_9.value==this.w_PSCONSUP)
      this.oPgFrm.Page2.oPag.oPSCONSUP_2_9.value=this.w_PSCONSUP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSUP_2_10.value==this.w_DESSUP)
      this.oPgFrm.Page2.oPag.oDESSUP_2_10.value=this.w_DESSUP
    endif
    if not(this.oPgFrm.Page2.oPag.oPSCATCOM_2_11.value==this.w_PSCATCOM)
      this.oPgFrm.Page2.oPag.oPSCATCOM_2_11.value=this.w_PSCATCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOM_2_12.value==this.w_DESCOM)
      this.oPgFrm.Page2.oPag.oDESCOM_2_12.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oPSCODDES_2_13.value==this.w_PSCODDES)
      this.oPgFrm.Page2.oPag.oPSCODDES_2_13.value=this.w_PSCODDES
    endif
    if not(this.oPgFrm.Page2.oPag.oNOMDES_2_14.value==this.w_NOMDES)
      this.oPgFrm.Page2.oPag.oNOMDES_2_14.value=this.w_NOMDES
    endif
    if not(this.oPgFrm.Page2.oPag.oPSCODZON_2_15.value==this.w_PSCODZON)
      this.oPgFrm.Page2.oPag.oPSCODZON_2_15.value=this.w_PSCODZON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESZON_2_16.value==this.w_DESZON)
      this.oPgFrm.Page2.oPag.oDESZON_2_16.value=this.w_DESZON
    endif
    if not(this.oPgFrm.Page2.oPag.oPSCODAGE_2_17.value==this.w_PSCODAGE)
      this.oPgFrm.Page2.oPag.oPSCODAGE_2_17.value=this.w_PSCODAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE_2_18.value==this.w_DESAGE)
      this.oPgFrm.Page2.oPag.oDESAGE_2_18.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oPSCODVAL_2_19.value==this.w_PSCODVAL)
      this.oPgFrm.Page2.oPag.oPSCODVAL_2_19.value=this.w_PSCODVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVAL_2_20.value==this.w_DESVAL)
      this.oPgFrm.Page2.oPag.oDESVAL_2_20.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oPSTIPFAT_2_34.RadioValue()==this.w_PSTIPFAT)
      this.oPgFrm.Page2.oPag.oPSTIPFAT_2_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREPSEC_1_162.RadioValue()==this.w_REPSEC)
      this.oPgFrm.Page1.oPag.oREPSEC_1_162.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPSMINIMP_2_37.value==this.w_PSMINIMP)
      this.oPgFrm.Page2.oPag.oPSMINIMP_2_37.value=this.w_PSMINIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oPSMINVAL_2_38.value==this.w_PSMINVAL)
      this.oPgFrm.Page2.oPag.oPSMINVAL_2_38.value=this.w_PSMINVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oSIMVAL_2_41.value==this.w_SIMVAL)
      this.oPgFrm.Page2.oPag.oSIMVAL_2_41.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPADESFOR_1_190.value==this.w_PADESFOR)
      this.oPgFrm.Page1.oPag.oPADESFOR_1_190.value=this.w_PADESFOR
    endif
    cp_SetControlsValueExtFlds(this,'FAT_DIFF')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PSNUMREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSNUMREG_1_5.SetFocus()
            i_bnoObbl = !empty(.w_PSNUMREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PSDATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSDATREG_1_6.SetFocus()
            i_bnoObbl = !empty(.w_PSDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PSCODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSCODESE_1_8.SetFocus()
            i_bnoObbl = !empty(.w_PSCODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PSTIPDOC)) or not(.w_MVFLVEAC='V' AND .w_FLGEFA='B'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSTIPDOC_1_11.SetFocus()
            i_bnoObbl = !empty(.w_PSTIPDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente , incongruente")
          case   not(.w_PSSTATUS<>'S' OR NOT EMPTY(.w_PSALFDOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSALFDOC_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire la serie dei documenti provvisori")
          case   ((empty(.w_PSDATDOC)) or not(CALCESER(.w_PSDATDOC, 'ZZZZ')=.w_PSCODESE))  and (.cFunction = 'Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSDATDOC_1_15.SetFocus()
            i_bnoObbl = !empty(.w_PSDATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data fatturazione fuori dall'esercizio del documento")
          case   ((empty(.w_PSDATCIV)) or not(.w_PSDATDOC>=.w_PSDATCIV AND CALCESER(.w_PSDATCIV, 'ZZZZ')=.w_PSCODESE))  and (.cFunction = 'Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSDATCIV_1_16.SetFocus()
            i_bnoObbl = !empty(.w_PSDATCIV)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di competenza IVA deve essere minore o uguale della data documento e all'interno dell'esercizio")
          case   (empty(.w_PSDATDIV))  and (.cFunction = 'Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSDATDIV_1_17.SetFocus()
            i_bnoObbl = !empty(.w_PSDATDIV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PSDATRIF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSDATRIF_1_24.SetFocus()
            i_bnoObbl = !empty(.w_PSDATRIF)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((empty(.w_PSALFDOF)) OR  (.w_PSALFDOI<=.w_PSALFDOF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSALFDOI_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � pi� grande della serie finale")
          case   not(.w_PSNUMINI<=.w_PSNUMFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSNUMFIN_1_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero iniziale � pi� grande di quello finale")
          case   not((.w_PSALFDOF>=.w_PSALFDOI) or (empty(.w_PSALFDOI)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSALFDOF_1_28.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La serie iniziale � maggire della serie finale")
          case   not(((empty(.w_PSCODCLF)) OR  (.w_PSCODCLI<=.w_PSCODCLF)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_PSCODCLI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSCODCLI_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale, inesistente oppure obsoleto")
          case   not(((.w_PSCODCLF>=.w_PSCODCLI) or (empty(.w_PSCODCLI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_PSCODCLF))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPSCODCLF_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale, inesistente oppure obsoleto")
          case   not(EMPTY(.w_CAUDOC) OR (.w_FLVEAC1='V' AND .w_FLGEFA1='S'))  and not(empty(.w_PSTIPORI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPSTIPORI_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PSCODPAG))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPSCODPAG_2_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PSCATCON))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPSCATCON_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria contabile inesistente oppure obsoleta")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PSCONSUP))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPSCONSUP_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Mastro di raggruppamento inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PSCATCOM))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPSCATCOM_2_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria commerciale inesistente oppure obsoleta")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  AND .w_TIPRIF='CO')  and ((NOT EMPTY(.w_PSCODCLI) AND NOT EMPTY(.w_PSCODCLF)) AND (.w_PSCODCLI=.w_PSCODCLF))  and not(empty(.w_PSCODDES))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPSCODDES_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PSCODZON))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPSCODZON_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice zona inesistente oppure obsoleta")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PSCODAGE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPSCODAGE_2_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice agente inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_PSCODVAL))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPSCODVAL_2_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice valuta inesistente oppure obsoleto")
          case   not(.w_PSMINIMP>=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPSMINIMP_2_37.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Impostare un valore positivo")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSVE_MFD.CheckForm()
      if i_bres
        i_bres=  .GSVE_MFD.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsve_afd
      * --- Check incrociati sui Control
      * --- Impostati qui per agevolare la navigazione sulla maschera
      Do Case
        Case Empty(This.w_PSTIPDOC)
      		i_bnoChk = .f.
      		i_bRes = .f.
      		i_cErrorMsg = Ah_MsgFormat("Impostare causale fatture da generare")		
      	Case This.w_PSDATINI>This.w_PSDATRIF And Not Empty(This.w_PSDATINI)
      		i_bnoChk = .f.
      		i_bRes = .f.
      		i_cErrorMsg = Ah_MsgFormat("La data di inizio selezione � pi� grande di quella di fine")
        Case  Not Empty(CHKCONS(iif(.w_FLANAL='S','VC','V'),.w_PSDATDOC,'B','N'))
          * --- Check Controllo Data Consolidamento
          i_bnoChk=.F.
          i_bRes=.F.
          i_cErrorMsg=CHKCONS(iif(.w_FLANAL='S','VC','V'),.w_PSDATDOC,'B','N')
      	Case Not empty(this.w_PSDATRIF) AND  this.w_PSDATRIF>this.w_PSDATDOC
      		i_bnoChk = .f.
      		i_bRes = .f.
      		i_cErrorMsg = Ah_MsgFormat("La data finale � pi� grande della data fattura")		
      	Case This.w_PSDATCIV>This.w_PSDATDOC
      		i_bnoChk = .f.
      		i_bRes = .f.
      		i_cErrorMsg = Ah_MsgFormat("Data competenza IVA superiore alla data documento")
      	Case Not EMPTY(NVL(.w_PSPAGFOR, ' '))
          i_bRes = ah_YesNo('Confermando la generazione verr� impostato il pagamento forzato indicato in tutti i documenti generati con flag "Scadenze confermate" non attivo.%0'+"Confermi l'elaborazione?")
          if !i_bRes
           i_bnoChk = .f.
           i_cErrorMsg = "Operazione interrotta come richiesto"
          Endif 	
      endcase
      * --- Check Numerazione
      if i_bRes
         this.w_OKAGG=.t.
         This.NotifyEvent('ChkNum')
         i_bRes=this.w_OKAGG
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PSSERIAL = this.w_PSSERIAL
    this.o_PSDATREG = this.w_PSDATREG
    this.o_PSTIPDOC = this.w_PSTIPDOC
    this.o_PSDATDOC = this.w_PSDATDOC
    this.o_FLRICIVA = this.w_FLRICIVA
    this.o_PSCODCLF = this.w_PSCODCLF
    this.o_PSCODPAG = this.w_PSCODPAG
    this.o_PSCATCON = this.w_PSCATCON
    this.o_PSCONSUP = this.w_PSCONSUP
    this.o_PSCATCOM = this.w_PSCATCOM
    this.o_PSCODDES = this.w_PSCODDES
    this.o_PSCODZON = this.w_PSCODZON
    this.o_PSCODAGE = this.w_PSCODAGE
    * --- GSVE_MFD : Depends On
    this.GSVE_MFD.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsve_afdPag1 as StdContainer
  Width  = 683
  height = 397
  stdWidth  = 683
  stdheight = 397
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPSNUMREG_1_5 as StdField with uid="GJDVREWVXD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PSNUMREG", cQueryName = "PSNUMREG",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero progressivo della fatturazione differita",;
    HelpContextID = 60819005,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=154, Top=19, cSayPict='"999999"', cGetPict='"999999"'

  add object oPSDATREG_1_6 as StdField with uid="VHAPLUZQER",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PSDATREG", cQueryName = "PSDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 66807357,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=299, Top=19, tabstop=.f.

  add object oPSANNREG_1_7 as StdField with uid="BYWDJBHVAH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PSANNREG", cQueryName = "PSANNREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento",;
    HelpContextID = 61355581,;
   bGlobalFont=.t.,;
    Height=21, Width=40, Left=231, Top=19, InputMask=replicate('X',4)

  add object oPSCODESE_1_8 as StdField with uid="UPFFWFALXX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PSCODESE", cQueryName = "PSCODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio dei documenti da generare",;
    HelpContextID = 101275195,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=451, Top=19, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_PSCODESE"

  func oPSCODESE_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSCODESE_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCODESE_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oPSCODESE_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oPSDESCRI_1_9 as StdField with uid="RUZJOVOWUK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PSDESCRI", cQueryName = "PSDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali note descrittive",;
    HelpContextID = 82798143,;
   bGlobalFont=.t.,;
    Height=21, Width=352, Left=154, Top=46, InputMask=replicate('X',40)


  add object oPSSTATUS_1_10 as StdCombo with uid="CVABYDRHKK",rtseq=10,rtrep=.f.,left=578,top=19,width=100,height=21;
    , ToolTipText = "Status dei documenti da generare: provvisori o confermati";
    , HelpContextID = 81745481;
    , cFormVar="w_PSSTATUS",RowSource=""+"Confermati,"+"Provvisori", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPSSTATUS_1_10.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oPSSTATUS_1_10.GetRadio()
    this.Parent.oContained.w_PSSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oPSSTATUS_1_10.SetRadio()
    this.Parent.oContained.w_PSSTATUS=trim(this.Parent.oContained.w_PSSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_PSSTATUS=='N',1,;
      iif(this.Parent.oContained.w_PSSTATUS=='S',2,;
      0))
  endfunc

  func oPSSTATUS_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load' AND NOT EMPTY(.w_PSTIPDOC))
    endwith
   endif
  endfunc

  add object oPSTIPDOC_1_11 as StdField with uid="OTMUPEKCDO",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PSTIPDOC", cQueryName = "PSTIPDOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente , incongruente",;
    ToolTipText = "Causale documenti fatture differite",;
    HelpContextID = 171678151,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=154, Top=73, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PSTIPDOC"

  func oPSTIPDOC_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSTIPDOC_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSTIPDOC_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPSTIPDOC_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'GSVE1KFD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oPSTIPDOC_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PSTIPDOC
     i_obj.ecpSave()
  endproc

  add object oDESDOC_1_12 as StdField with uid="YJWVXOBEWE",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 189839562,;
   bGlobalFont=.t.,;
    Height=21, Width=283, Left=222, Top=73, InputMask=replicate('X',35)

  add object oPSNUMDOC_1_13 as StdField with uid="CFTVCGMCDK",rtseq=13,rtrep=.f.,;
    cFormVar = "w_PSNUMDOC", cQueryName = "PSNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Primo numero di fattura da generare",;
    HelpContextID = 174062023,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=154, Top=100, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oPSALFDOC_1_14 as StdField with uid="NOJXWYSZQO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PSALFDOC", cQueryName = "PSALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire la serie dei documenti provvisori",;
    ToolTipText = "Serie documenti da generare",;
    HelpContextID = 182045127,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=281, Top=100, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oPSALFDOC_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PSSTATUS<>'S' OR NOT EMPTY(.w_PSALFDOC))
    endwith
    return bRes
  endfunc

  add object oPSDATDOC_1_15 as StdField with uid="HEPMYBLWIJ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_PSDATDOC", cQueryName = "PSDATDOC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data fatturazione fuori dall'esercizio del documento",;
    ToolTipText = "Data di registrazione delle fatture differite",;
    HelpContextID = 168073671,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=602, Top=100

  func oPSDATDOC_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction = 'Load')
    endwith
   endif
  endfunc

  func oPSDATDOC_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CALCESER(.w_PSDATDOC, 'ZZZZ')=.w_PSCODESE)
    endwith
    return bRes
  endfunc

  add object oPSDATCIV_1_16 as StdField with uid="LTMDKJRAHW",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PSDATCIV", cQueryName = "PSDATCIV",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di competenza IVA deve essere minore o uguale della data documento e all'interno dell'esercizio",;
    ToolTipText = "Data di competenza IVA",;
    HelpContextID = 184850868,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=154, Top=127

  func oPSDATCIV_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction = 'Load')
    endwith
   endif
  endfunc

  func oPSDATCIV_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PSDATDOC>=.w_PSDATCIV AND CALCESER(.w_PSDATCIV, 'ZZZZ')=.w_PSCODESE)
    endwith
    return bRes
  endfunc

  add object oPSDATDIV_1_17 as StdField with uid="MSZDVVFCPZ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PSDATDIV", cQueryName = "PSDATDIV",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale data di inizio scadenze per pagamenti in data diversa",;
    HelpContextID = 168073652,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=602, Top=127

  func oPSDATDIV_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction = 'Load')
    endwith
   endif
  endfunc

  add object oPSPAGFOR_1_18 as StdField with uid="NFRHTZDTOP",rtseq=18,rtrep=.f.,;
    cFormVar = "w_PSPAGFOR", cQueryName = "PSPAGFOR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice pagamento da utilizzare per l'evasione dei documenti privi del flag scadenze confermate attivo",;
    HelpContextID = 148101560,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=154, Top=154, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_PSPAGFOR"

  func oPSPAGFOR_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!EMPTY(.w_PSTIPDOC) AND .w_FLGEFA='B')
    endwith
   endif
  endfunc

  func oPSPAGFOR_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSPAGFOR_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSPAGFOR_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oPSPAGFOR_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oPSPAGFOR_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_PSPAGFOR
     i_obj.ecpSave()
  endproc


  add object oBtn_1_19 as StdButton with uid="DZEPUTHDCZ",left=630, top=46, width=48,height=45,;
    CpPicture="BMP\DOCCON.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per rendere definitivi i documenti associati";
    , HelpContextID = 181198457;
    , tabstop=.f.,caption='\<Conferma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      do GSVE_KFD with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_PSSTATUS='S' AND .cFunction='Edit')
      endwith
    endif
  endfunc

  func oBtn_1_19.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT (.w_PSSTATUS='S' AND .cFunction='Edit'))
     endwith
    endif
  endfunc

  add object oPSFLRAGG_1_20 as StdCheck with uid="YLAWOBGZFY",rtseq=19,rtrep=.f.,left=473, top=156, caption="Raggruppa per articolo",;
    ToolTipText = "Se attivo: raggruppa tutte le righe con gli stessi articoli",;
    HelpContextID = 48662077,;
    cFormVar="w_PSFLRAGG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPSFLRAGG_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPSFLRAGG_1_20.GetRadio()
    this.Parent.oContained.w_PSFLRAGG = this.RadioValue()
    return .t.
  endfunc

  func oPSFLRAGG_1_20.SetRadio()
    this.Parent.oContained.w_PSFLRAGG=trim(this.Parent.oContained.w_PSFLRAGG)
    this.value = ;
      iif(this.Parent.oContained.w_PSFLRAGG=='S',1,;
      0)
  endfunc


  add object oPSFLVEBD_1_21 as StdCombo with uid="PYKLIQDMHE",value=3,rtseq=20,rtrep=.f.,left=154,top=180,width=117,height=20;
    , ToolTipText = "Imposta il flag beni deperibili sui documenti generati. Da documento=valore presente su documento origine, attivo=attiva flag su documenti generati, disattivo=disattiva flag su documenti generati";
    , HelpContextID = 119965242;
    , cFormVar="w_PSFLVEBD",RowSource=""+"Da documento,"+"Attivo,"+"Disattivo", bObbl = .f. , nPag = 1;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oPSFLVEBD_1_21.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'S',;
    iif(this.value =3,' ',;
    space(1)))))
  endfunc
  func oPSFLVEBD_1_21.GetRadio()
    this.Parent.oContained.w_PSFLVEBD = this.RadioValue()
    return .t.
  endfunc

  func oPSFLVEBD_1_21.SetRadio()
    this.Parent.oContained.w_PSFLVEBD=trim(this.Parent.oContained.w_PSFLVEBD)
    this.value = ;
      iif(this.Parent.oContained.w_PSFLVEBD=='D',1,;
      iif(this.Parent.oContained.w_PSFLVEBD=='S',2,;
      iif(this.Parent.oContained.w_PSFLVEBD=='',3,;
      0)))
  endfunc

  add object oPSDATINI_1_22 as StdField with uid="NQGKMVULQK",rtseq=21,rtrep=.f.,;
    cFormVar = "w_PSDATINI", cQueryName = "PSDATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio selezione",;
    HelpContextID = 84187585,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=154, Top=228

  add object oFLRICIVA_1_23 as StdCheck with uid="KJZMSHAWAR",rtseq=22,rtrep=.f.,left=472, top=180, caption="Ricalcola codici I.V.A.",;
    ToolTipText = "Se attivato effettua un ricalcolo dei codici I.V.A. delle righe in evasione",;
    HelpContextID = 167001751,;
    cFormVar="w_FLRICIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLRICIVA_1_23.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oFLRICIVA_1_23.GetRadio()
    this.Parent.oContained.w_FLRICIVA = this.RadioValue()
    return .t.
  endfunc

  func oFLRICIVA_1_23.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLRICIVA==.T.,1,;
      0)
  endfunc

  func oFLRICIVA_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  add object oPSDATRIF_1_24 as StdField with uid="DJZZJNTGMD",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PSDATRIF", cQueryName = "PSDATRIF",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documenti di fine selezione",;
    HelpContextID = 201628100,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=154, Top=256

  add object oPSNUMINI_1_25 as StdField with uid="QNZKXCKOOQ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PSNUMINI", cQueryName = "PSNUMINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento iniziale",;
    HelpContextID = 90175937,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=325, Top=228, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oPSALFDOI_1_26 as StdField with uid="KTCNMFOERC",rtseq=25,rtrep=.f.,;
    cFormVar = "w_PSALFDOI", cQueryName = "PSALFDOI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � pi� grande della serie finale",;
    ToolTipText = "Serie documento iniziale",;
    HelpContextID = 182045121,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=455, Top=228, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oPSALFDOI_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((empty(.w_PSALFDOF)) OR  (.w_PSALFDOI<=.w_PSALFDOF))
    endwith
    return bRes
  endfunc

  add object oPSNUMFIN_1_27 as StdField with uid="BAMEQKYAJE",rtseq=26,rtrep=.f.,;
    cFormVar = "w_PSNUMFIN", cQueryName = "PSNUMFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero iniziale � pi� grande di quello finale",;
    ToolTipText = "Numero documento finale",;
    HelpContextID = 140507580,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=325, Top=257, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oPSNUMFIN_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PSNUMINI<=.w_PSNUMFIN)
    endwith
    return bRes
  endfunc

  add object oPSALFDOF_1_28 as StdField with uid="VEOCLKNLPZ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PSALFDOF", cQueryName = "PSALFDOF",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "La serie iniziale � maggire della serie finale",;
    ToolTipText = "Serie documento finale",;
    HelpContextID = 182045124,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=455, Top=257, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oPSALFDOF_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PSALFDOF>=.w_PSALFDOI) or (empty(.w_PSALFDOI)))
    endwith
    return bRes
  endfunc

  add object oPSCODCLI_1_29 as StdField with uid="DWRWZFOUDS",rtseq=28,rtrep=.f.,;
    cFormVar = "w_PSCODCLI", cQueryName = "PSCODCLI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale, inesistente oppure obsoleto",;
    ToolTipText = "Eventuale cliente di selezione (vuoto = no selezione)",;
    HelpContextID = 200714689,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=154, Top=284, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_PSTIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_PSCODCLI"

  func oPSCODCLI_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSCODCLI_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCODCLI_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PSTIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_PSTIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPSCODCLI_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'GSAR_SCL.CONTI_VZM',this.parent.oContained
  endproc
  proc oPSCODCLI_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_PSTIPCLF
     i_obj.w_ANCODICE=this.parent.oContained.w_PSCODCLI
     i_obj.ecpSave()
  endproc

  add object oDESCLI_1_30 as StdField with uid="ZYBMMUGYND",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 92387530,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=286, Top=284, InputMask=replicate('X',40)

  add object oPSCODCLF_1_31 as StdField with uid="YRRRHIGIFZ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_PSCODCLF", cQueryName = "PSCODCLF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale, inesistente oppure obsoleto",;
    ToolTipText = "Eventuale cliente di selezione (vuoto = no selezione)",;
    HelpContextID = 200714692,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=154, Top=312, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_PSTIPCLF", oKey_2_1="ANCODICE", oKey_2_2="this.w_PSCODCLF"

  func oPSCODCLF_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
      if .not. empty(.w_PSCODDES)
        bRes2=.link_2_13('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPSCODCLF_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCODCLF_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PSTIPCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_PSTIPCLF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPSCODCLF_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'GSAR_SCL.CONTI_VZM',this.parent.oContained
  endproc
  proc oPSCODCLF_1_31.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_PSTIPCLF
     i_obj.w_ANCODICE=this.parent.oContained.w_PSCODCLF
     i_obj.ecpSave()
  endproc

  add object oDESCLF_1_32 as StdField with uid="ZVZHIJLULK",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 142719178,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=286, Top=312, InputMask=replicate('X',40)


  add object oPSFLVALO_1_33 as StdCombo with uid="FTZRMWMAPR",rtseq=32,rtrep=.f.,left=154,top=342,width=128,height=21;
    , HelpContextID = 215579067;
    , cFormVar="w_PSFLVALO",RowSource=""+"Tutti,"+"Righe valorizzate,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPSFLVALO_1_33.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'R',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oPSFLVALO_1_33.GetRadio()
    this.Parent.oContained.w_PSFLVALO = this.RadioValue()
    return .t.
  endfunc

  func oPSFLVALO_1_33.SetRadio()
    this.Parent.oContained.w_PSFLVALO=trim(this.Parent.oContained.w_PSFLVALO)
    this.value = ;
      iif(this.Parent.oContained.w_PSFLVALO=='T',1,;
      iif(this.Parent.oContained.w_PSFLVALO=='R',2,;
      iif(this.Parent.oContained.w_PSFLVALO=='N',3,;
      0)))
  endfunc

  add object oSTADOC_1_34 as StdCheck with uid="MWDUROXYAH",rtseq=33,rtrep=.f.,left=299, top=342, caption="Stampa immediata",;
    ToolTipText = "Se attivo: stampa immediata dei documenti al termine della elaborazione",;
    HelpContextID = 189909210,;
    cFormVar="w_STADOC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSTADOC_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oSTADOC_1_34.GetRadio()
    this.Parent.oContained.w_STADOC = this.RadioValue()
    return .t.
  endfunc

  func oSTADOC_1_34.SetRadio()
    this.Parent.oContained.w_STADOC=trim(this.Parent.oContained.w_STADOC)
    this.value = ;
      iif(this.Parent.oContained.w_STADOC=='S',1,;
      0)
  endfunc

  func oSTADOC_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc

  func oSTADOC_1_34.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Load')
    endwith
  endfunc


  add object oBtn_1_91 as StdButton with uid="QJZVDJKIQR",left=581, top=352, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 59359770;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_91.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_91.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MVTIPDOC) AND (.w_PSSTATUS<>'S' OR NOT EMPTY(.w_PSALFDOC) OR .cFunction<>'Load'))
      endwith
    endif
  endfunc


  add object oBtn_1_92 as StdButton with uid="KZSQCYUNAO",left=632, top=352, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare";
    , HelpContextID = 52071098;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_92.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oLinkPC_1_93 as StdButton with uid="CDVPQMYGOD",left=632, top=303, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al dettaglio dei documenti generati";
    , HelpContextID = 100567137;
    , tabstop=.f.,caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_93.Click()
      this.Parent.oContained.GSVE_MFD.LinkPCClick()
    endproc

  func oLinkPC_1_93.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_PSSERIAL)  AND .cFunction<>'Load')
      endwith
    endif
  endfunc

  func oLinkPC_1_93.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Load')
     endwith
    endif
  endfunc


  add object oBtn_1_100 as StdButton with uid="JEXJCQGJFB",left=632, top=303, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza dettaglio dei documenti da generare in base alle selezioni effettuate";
    , HelpContextID = 100567137;
    , tabstop=.f.,caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_100.Click()
      do GSVE_KSF with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_100.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_MVTIPDOC) AND .cFunction='Load' AND (.w_PSSTATUS<>'S' OR NOT EMPTY(.w_PSALFDOC)))
      endwith
    endif
  endfunc

  func oBtn_1_100.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction<>'Load')
     endwith
    endif
  endfunc


  add object oBtn_1_105 as StdButton with uid="ELNSICJVGH",left=632, top=254, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per lanciare la stampa dei documenti associati alla fatturazione";
    , HelpContextID = 82401062;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_105.Click()
      this.parent.oContained.NotifyEvent("Stampa")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_105.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction='Query' AND NOT EMPTY(.w_PSSERIAL))
      endwith
    endif
  endfunc

  func oBtn_1_105.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction<>'Query')
     endwith
    endif
  endfunc


  add object oObj_1_152 as cp_runprogram with uid="EPYDMIDTRS",left=10, top=410, width=478,height=18,;
    caption='GSVE_BS3(D)',;
   bGlobalFont=.t.,;
    prg='GSVE_BS3("D")',;
    cEvent = "w_PSTIPDOC Changed,w_PSALFDOC Changed,w_PSDATDOC Changed",;
    nPag=1;
    , ToolTipText = "Ricalcolo il nuovo progressivo";
    , HelpContextID = 78863385


  add object oObj_1_153 as cp_runprogram with uid="MNWXUTUMZL",left=497, top=410, width=187,height=19,;
    caption='GSVE_BFD(I)',;
   bGlobalFont=.t.,;
    prg='GSVE_BFD("I")',;
    cEvent = "Record Inserted",;
    nPag=1;
    , ToolTipText = "Lancio l'elaborazione...";
    , HelpContextID = 78864682


  add object oObj_1_154 as cp_runprogram with uid="PFOCBOPLAK",left=11, top=433, width=243,height=18,;
    caption='GSVE_BS3(S)',;
   bGlobalFont=.t.,;
    prg='GSVE_BS3("S")',;
    cEvent = "w_PSSTATUS Changed",;
    nPag=1;
    , ToolTipText = "Cambio alfa e ricalcolo progressivo...";
    , HelpContextID = 78867225


  add object oObj_1_155 as cp_runprogram with uid="VESMNCKGII",left=258, top=434, width=171,height=18,;
    caption='GSVE_BS3(C)',;
   bGlobalFont=.t.,;
    prg="GSVE_BS3('C')",;
    cEvent = "ChkNum",;
    nPag=1;
    , ToolTipText = "Controllo numerazione....";
    , HelpContextID = 78863129


  add object oREPSEC_1_162 as StdCombo with uid="ILFBHAZQOE",rtseq=156,rtrep=.f.,left=154,top=372,width=170,height=21;
    , ToolTipText = "Stampa report secondari";
    , HelpContextID = 199354346;
    , cFormVar="w_REPSEC",RowSource=""+"Tutti i report secondari,"+"Nessun report secondario,"+"Escludi report opzionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oREPSEC_1_162.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'C',;
    space(1)))))
  endfunc
  func oREPSEC_1_162.GetRadio()
    this.Parent.oContained.w_REPSEC = this.RadioValue()
    return .t.
  endfunc

  func oREPSEC_1_162.SetRadio()
    this.Parent.oContained.w_REPSEC=trim(this.Parent.oContained.w_REPSEC)
    this.value = ;
      iif(this.Parent.oContained.w_REPSEC=='S',1,;
      iif(this.Parent.oContained.w_REPSEC=='N',2,;
      iif(this.Parent.oContained.w_REPSEC=='C',3,;
      0)))
  endfunc

  add object oPADESFOR_1_190 as StdField with uid="RPAHPAPJUJ",rtseq=184,rtrep=.f.,;
    cFormVar = "w_PADESFOR", cQueryName = "PADESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 135310264,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=223, Top=154, InputMask=replicate('X',30)

  add object oStr_1_84 as StdString with uid="EWZADIARTM",Visible=.t., Left=74, Top=19,;
    Alignment=1, Width=79, Height=15,;
    Caption="Numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_85 as StdString with uid="NBGCCKSHSK",Visible=.t., Left=224, Top=19,;
    Alignment=0, Width=8, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_86 as StdString with uid="WCRKNSCOUR",Visible=.t., Left=269, Top=19,;
    Alignment=1, Width=31, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_87 as StdString with uid="RZKNRQNAJF",Visible=.t., Left=376, Top=19,;
    Alignment=1, Width=72, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_88 as StdString with uid="MMXZKGVFVI",Visible=.t., Left=514, Top=19,;
    Alignment=1, Width=62, Height=15,;
    Caption="Status:"  ;
  , bGlobalFont=.t.

  add object oStr_1_89 as StdString with uid="OLTDBREGEF",Visible=.t., Left=274, Top=100,;
    Alignment=0, Width=8, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="PXLWDNVYSR",Visible=.t., Left=81, Top=312,;
    Alignment=1, Width=72, Height=18,;
    Caption="A cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_94 as StdString with uid="YUBBLHBEQI",Visible=.t., Left=34, Top=73,;
    Alignment=1, Width=119, Height=15,;
    Caption="Fatture da generare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_95 as StdString with uid="KJZVETXSIP",Visible=.t., Left=26, Top=100,;
    Alignment=1, Width=127, Height=15,;
    Caption="Primo numero fattura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_96 as StdString with uid="UDSSRQLMKB",Visible=.t., Left=20, Top=127,;
    Alignment=1, Width=133, Height=15,;
    Caption="Data competenza IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_97 as StdString with uid="YNFIWBOZKK",Visible=.t., Left=442, Top=127,;
    Alignment=1, Width=157, Height=15,;
    Caption="Scadenze in data diversa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_98 as StdString with uid="KNREOGIPAC",Visible=.t., Left=2, Top=342,;
    Alignment=1, Width=151, Height=15,;
    Caption="Documenti non valorizzati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_99 as StdString with uid="INNWOCHDNG",Visible=.t., Left=123, Top=255,;
    Alignment=1, Width=30, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_101 as StdString with uid="GSKNZQPTXF",Visible=.t., Left=518, Top=100,;
    Alignment=1, Width=81, Height=15,;
    Caption="Data fattura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_102 as StdString with uid="JBFGBYXGPV",Visible=.t., Left=5, Top=213,;
    Alignment=0, Width=142, Height=15,;
    Caption="Selezioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_131 as StdString with uid="ICMZEQGCPD",Visible=.t., Left=92, Top=284,;
    Alignment=1, Width=61, Height=18,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_133 as StdString with uid="MUMNIURUON",Visible=.t., Left=242, Top=228,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_134 as StdString with uid="LLXBBAIFUB",Visible=.t., Left=444, Top=231,;
    Alignment=2, Width=10, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_135 as StdString with uid="BQBXXFXEKR",Visible=.t., Left=444, Top=257,;
    Alignment=2, Width=10, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_136 as StdString with uid="CYEMUBEZOW",Visible=.t., Left=257, Top=257,;
    Alignment=1, Width=68, Height=18,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_137 as StdString with uid="IMSNXQNFAC",Visible=.t., Left=64, Top=228,;
    Alignment=1, Width=89, Height=18,;
    Caption="Documenti dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_163 as StdString with uid="YQPOXLZVJK",Visible=.t., Left=60, Top=372,;
    Alignment=1, Width=93, Height=18,;
    Caption="Stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_191 as StdString with uid="SPUXBYISWM",Visible=.t., Left=18, Top=154,;
    Alignment=1, Width=135, Height=15,;
    Caption="Pagamento forzato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_192 as StdString with uid="BMIKTHSFRG",Visible=.t., Left=18, Top=181,;
    Alignment=1, Width=135, Height=18,;
    Caption="Vendita beni deperibili:"  ;
  , bGlobalFont=.t.

  add object oBox_1_104 as StdBox with uid="HLHELWJRAH",left=2, top=210, width=675,height=2
enddefine
define class tgsve_afdPag2 as StdContainer
  Width  = 683
  height = 397
  stdWidth  = 683
  stdheight = 397
  resizeXpos=408
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPSTIPORI_2_3 as StdField with uid="ICEQEMVMLF",rtseq=111,rtrep=.f.,;
    cFormVar = "w_PSTIPORI", cQueryName = "PSTIPORI",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Codice causale di eventuale selezione documento (vuoto = no selezione)",;
    HelpContextID = 12871231,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=177, Top=21, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PSTIPORI"

  func oPSTIPORI_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSTIPORI_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSTIPORI_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPSTIPORI_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'GSVE2KFD.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oPSTIPORI_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PSTIPORI
     i_obj.ecpSave()
  endproc

  add object oDESORI_2_4 as StdField with uid="COPMHIGGBN",rtseq=112,rtrep=.f.,;
    cFormVar = "w_DESORI", cQueryName = "DESORI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 85309642,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=243, Top=21, InputMask=replicate('X',35)

  add object oPSCODPAG_2_5 as StdField with uid="QAHJJXLGKS",rtseq=113,rtrep=.f.,;
    cFormVar = "w_PSCODPAG", cQueryName = "PSCODPAG",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice pagamento di eventuale selezione (vuoto = no selezione)",;
    HelpContextID = 17389117,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=177, Top=49, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_PSCODPAG"

  func oPSCODPAG_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSCODPAG_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCODPAG_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oPSCODPAG_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oPSCODPAG_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_PSCODPAG
     i_obj.ecpSave()
  endproc

  add object oDESPAG_2_6 as StdField with uid="TPAZBJXNCF",rtseq=114,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 136624330,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=243, Top=49, InputMask=replicate('X',30)

  add object oPSCATCON_2_7 as StdField with uid="TGBHECAWZQ",rtseq=115,rtrep=.f.,;
    cFormVar = "w_PSCATCON", cQueryName = "PSCATCON",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria contabile inesistente oppure obsoleta",;
    ToolTipText = "Categoria contabile di eventuale selezione (vuoto = no selezione)",;
    HelpContextID = 184854972,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=177, Top=77, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_PSCATCON"

  func oPSCATCON_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSCATCON_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCATCON_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oPSCATCON_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili",'',this.parent.oContained
  endproc
  proc oPSCATCON_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_PSCATCON
     i_obj.ecpSave()
  endproc

  add object oDESCON_2_8 as StdField with uid="MGNMPJAXUU",rtseq=116,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 5355722,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=243, Top=77, InputMask=replicate('X',35)

  add object oPSCONSUP_2_9 as StdField with uid="XLLHBQNNRK",rtseq=117,rtrep=.f.,;
    cFormVar = "w_PSCONSUP", cQueryName = "PSCONSUP",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Mastro di raggruppamento inesistente oppure obsoleto",;
    ToolTipText = "Codice mastro di eventuale selezione (vuoto = no selezione)",;
    HelpContextID = 78206534,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=177, Top=105, cSayPict="p_MAS", cGetPict="p_MAS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_PSCONSUP"

  func oPSCONSUP_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSCONSUP_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCONSUP_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oPSCONSUP_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili (clienti)",'GSAR_ACL.MASTRI_VZM',this.parent.oContained
  endproc
  proc oPSCONSUP_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_PSCONSUP
     i_obj.ecpSave()
  endproc

  add object oDESSUP_2_10 as StdField with uid="KYFYCWGHZP",rtseq=118,rtrep=.f.,;
    cFormVar = "w_DESSUP", cQueryName = "DESSUP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 35538742,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=303, Top=105, InputMask=replicate('X',40)

  add object oPSCATCOM_2_11 as StdField with uid="SNDBZUAWXD",rtseq=119,rtrep=.f.,;
    cFormVar = "w_PSCATCOM", cQueryName = "PSCATCOM",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria commerciale inesistente oppure obsoleta",;
    ToolTipText = "Categoria commerciale di eventuale selezione (vuota = no selezione)",;
    HelpContextID = 184854973,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=177, Top=133, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_PSCATCOM"

  func oPSCATCOM_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSCATCOM_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCATCOM_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oPSCATCOM_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oPSCATCOM_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_PSCATCOM
     i_obj.ecpSave()
  endproc

  add object oDESCOM_2_12 as StdField with uid="EOAWMJDKNR",rtseq=120,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 22132938,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=243, Top=133, InputMask=replicate('X',35)

  add object oPSCODDES_2_13 as StdField with uid="QJYFHGZFQF",rtseq=121,rtrep=.f.,;
    cFormVar = "w_PSCODDES", cQueryName = "PSCODDES",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice destinazione inesistente o di tipo diverso da consegna",;
    ToolTipText = "Eventuale destinazione particolare di selezione (vuoto = no selezione)",;
    HelpContextID = 84497993,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=177, Top=161, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_PSTIPCLF", oKey_2_1="DDCODICE", oKey_2_2="this.w_PSCODCLF", oKey_3_1="DDCODDES", oKey_3_2="this.w_PSCODDES"

  func oPSCODDES_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((NOT EMPTY(.w_PSCODCLI) AND NOT EMPTY(.w_PSCODCLF)) AND (.w_PSCODCLI=.w_PSCODCLF))
    endwith
   endif
  endfunc

  proc oPSCODDES_2_13.mAfter
    with this.Parent.oContained
      .w_PSCODDES=chkdesdiv(.w_PSCODDES, .w_PSTIPCLF, .w_PSCODCLF)
    endwith
  endproc

  func oPSCODDES_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSCODDES_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCODDES_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PSTIPCLF)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_PSCODCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_PSTIPCLF)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_PSCODCLF)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oPSCODDES_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Destinazioni diverse",'GSVE1KFD.DES_DIVE_VZM',this.parent.oContained
  endproc

  add object oNOMDES_2_14 as StdField with uid="OULTLXHFAU",rtseq=122,rtrep=.f.,;
    cFormVar = "w_NOMDES", cQueryName = "NOMDES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 68088278,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=243, Top=161, InputMask=replicate('X',40)

  add object oPSCODZON_2_15 as StdField with uid="CJKRPYZGLR",rtseq=123,rtrep=.f.,;
    cFormVar = "w_PSCODZON", cQueryName = "PSCODZON",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente oppure obsoleta",;
    ToolTipText = "Codice zona di eventuale selezione (vuoto = no selezione)",;
    HelpContextID = 83274172,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=177, Top=189, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_PSCODZON"

  func oPSCODZON_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSCODZON_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCODZON_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oPSCODZON_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oPSCODZON_2_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_PSCODZON
     i_obj.ecpSave()
  endproc

  add object oDESZON_2_16 as StdField with uid="TRLMNKNKBM",rtseq=124,rtrep=.f.,;
    cFormVar = "w_DESZON", cQueryName = "DESZON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 3848394,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=243, Top=189, InputMask=replicate('X',35)

  add object oPSCODAGE_2_17 as StdField with uid="UZTIUVMOEC",rtseq=125,rtrep=.f.,;
    cFormVar = "w_PSCODAGE", cQueryName = "PSCODAGE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice agente inesistente oppure obsoleto",;
    ToolTipText = "Codice eventuale agente di selezione (vuoto = no selezione)",;
    HelpContextID = 34166331,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=177, Top=217, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_PSCODAGE"

  func oPSCODAGE_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSCODAGE_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCODAGE_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oPSCODAGE_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oPSCODAGE_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_PSCODAGE
     i_obj.ecpSave()
  endproc

  add object oDESAGE_2_18 as StdField with uid="ONBEGFHERP",rtseq=126,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 164870346,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=243, Top=217, InputMask=replicate('X',35)

  add object oPSCODVAL_2_19 as StdField with uid="ADJCVDFLMW",rtseq=127,rtrep=.f.,;
    cFormVar = "w_PSCODVAL", cQueryName = "PSCODVAL",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice valuta inesistente oppure obsoleto",;
    ToolTipText = "Codice eventuale valuta di selezione (vuoto = no selezione)",;
    HelpContextID = 118052418,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=177, Top=245, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_PSCODVAL"

  func oPSCODVAL_2_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSCODVAL_2_19.ecpDrop(oSource)
    this.Parent.oContained.link_2_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSCODVAL_2_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oPSCODVAL_2_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oPSCODVAL_2_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_PSCODVAL
     i_obj.ecpSave()
  endproc

  add object oDESVAL_2_20 as StdField with uid="HWZGTOWBQH",rtseq=128,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 52345034,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=243, Top=245, InputMask=replicate('X',35)


  add object oPSTIPFAT_2_34 as StdCombo with uid="ZVQFJAYXZJ",rtseq=142,rtrep=.f.,left=177,top=273,width=152,height=21;
    , ToolTipText = "Filtra solo i clienti con la stessa tipologia di fatturazione selezionata (tutti =no selezione)";
    , HelpContextID = 130311754;
    , cFormVar="w_PSTIPFAT",RowSource=""+"Riepilogativa,"+"Per destinazione,"+"Per singolo DDT,"+"Per ordine,"+"Per ordine+destinazione,"+"Tutti", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oPSTIPFAT_2_34.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'D',;
    iif(this.value =3,'S',;
    iif(this.value =4,'C',;
    iif(this.value =5,'E',;
    iif(this.value =6,'T',;
    space(1))))))))
  endfunc
  func oPSTIPFAT_2_34.GetRadio()
    this.Parent.oContained.w_PSTIPFAT = this.RadioValue()
    return .t.
  endfunc

  func oPSTIPFAT_2_34.SetRadio()
    this.Parent.oContained.w_PSTIPFAT=trim(this.Parent.oContained.w_PSTIPFAT)
    this.value = ;
      iif(this.Parent.oContained.w_PSTIPFAT=='R',1,;
      iif(this.Parent.oContained.w_PSTIPFAT=='D',2,;
      iif(this.Parent.oContained.w_PSTIPFAT=='S',3,;
      iif(this.Parent.oContained.w_PSTIPFAT=='C',4,;
      iif(this.Parent.oContained.w_PSTIPFAT=='E',5,;
      iif(this.Parent.oContained.w_PSTIPFAT=='T',6,;
      0))))))
  endfunc

  add object oPSMINIMP_2_37 as StdField with uid="UJJJJCEYXL",rtseq=160,rtrep=.f.,;
    cFormVar = "w_PSMINIMP", cQueryName = "PSMINIMP",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Impostare un valore positivo",;
    ToolTipText = "Importo minimo fatturabile. Se il totale delle rate risulter� inferiore a questo importo, il loro valore verr� considerato come sconto finanziario e il totale documento impostato a 0",;
    HelpContextID = 89917882,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=177, Top=300, cSayPict="v_PV(38+VVL)", cGetPict="v_PV(38+VVL)"

  func oPSMINIMP_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PSMINIMP>=0)
    endwith
    return bRes
  endfunc

  add object oPSMINVAL_2_38 as StdField with uid="VDOHAULMLA",rtseq=161,rtrep=.f.,;
    cFormVar = "w_PSMINVAL", cQueryName = "PSMINVAL",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 128185922,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=353, Top=300, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_PSMINVAL"

  func oPSMINVAL_2_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PSMINIMP<>0)
    endwith
   endif
  endfunc

  func oPSMINVAL_2_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oPSMINVAL_2_38.ecpDrop(oSource)
    this.Parent.oContained.link_2_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPSMINVAL_2_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oPSMINVAL_2_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"",'',this.parent.oContained
  endproc
  proc oPSMINVAL_2_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_PSMINVAL
     i_obj.ecpSave()
  endproc

  add object oSIMVAL_2_41 as StdField with uid="FCWVDJDHRU",rtseq=162,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione valuta",;
    HelpContextID = 52368346,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=402, Top=300, InputMask=replicate('X',5)

  add object oStr_2_21 as StdString with uid="YQERZIALHG",Visible=.t., Left=7, Top=21,;
    Alignment=1, Width=168, Height=15,;
    Caption="Documento di origine:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="SOCDURSYVT",Visible=.t., Left=4, Top=245,;
    Alignment=1, Width=168, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="IZWNQYZQRN",Visible=.t., Left=4, Top=161,;
    Alignment=1, Width=168, Height=15,;
    Caption="Destinazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="PBQZKSGEDV",Visible=.t., Left=4, Top=189,;
    Alignment=1, Width=168, Height=15,;
    Caption="Codice zona:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="YGTHPBELDF",Visible=.t., Left=4, Top=217,;
    Alignment=1, Width=168, Height=15,;
    Caption="Codice 1^agente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="SRGLBYWTQF",Visible=.t., Left=66, Top=49,;
    Alignment=1, Width=109, Height=18,;
    Caption="Codice pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="TFEVRFKPDC",Visible=.t., Left=62, Top=77,;
    Alignment=1, Width=113, Height=18,;
    Caption="Categoria contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="ODELGCNZUN",Visible=.t., Left=20, Top=105,;
    Alignment=1, Width=155, Height=18,;
    Caption="Mastro di raggruppamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="FPLYRPZBNP",Visible=.t., Left=40, Top=133,;
    Alignment=1, Width=135, Height=18,;
    Caption="Categoria commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="KCAIIUIRMU",Visible=.t., Left=4, Top=273,;
    Alignment=1, Width=168, Height=15,;
    Caption="Tipo di fatturazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="CJWPJWLJAE",Visible=.t., Left=10, Top=301,;
    Alignment=1, Width=162, Height=18,;
    Caption="Imp. min. fatturabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_40 as StdString with uid="IRLOSCGHRT",Visible=.t., Left=323, Top=301,;
    Alignment=1, Width=26, Height=18,;
    Caption="in:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_afd','FAT_DIFF','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PSSERIAL=FAT_DIFF.PSSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsve_afd
define class StdFormNoConf as StdForm
  * --- Ridefinita la ESC per evitare la richiesta "abbandoni le modifiche?" al termine della elaborazione
  * --- Esc
  proc ecpQuit()
    local i_oldfunc
    i_oldfunc=this.cFunction
    if inlist(this.cFunction,'Edit','Load')
      this.NotifyEvent('Edit Aborted')
    endif
    *--- Activity Logger traccio ESC
    If i_nACTIVATEPROFILER>0
    	cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "UES", "", This, i_ServerConn(1,2), 0, Iif(bTrsErr, i_TrsMsg,""),This)
    Endif
    *--- Activity Logger traccio ESC
    * --- si sposta senza attivare i controlli
    this.cFunction='Filter'
    local i_err
    i_err=on('error')
    on error =.t.
    this.activecontrol.parent.oContained.bDontReportError=.t.
    on error &i_err
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    * ---
    do case
      case i_oldfunc="Query"
        this.NotifyEvent("Done")
        this.Hide()
        this.Release()
      case i_oldfunc="Load"
        *this.QueryKeySet(this.cLastWhere,this.cLastOrderBy)
        *this.ecpQuery()
        *this.nLoadTime=0
        This.cFunction="Query"
        This.BlankRec()
        This.bLoaded=.F.
        This.SetStatus()
        This.SetFocusOnFirstControl()
      case i_oldfunc="Edit"
        this.ecpQuery()
        this.nLoadTime=0
      otherwise
        this.ecpQuery()
    endcase
    return
enddefine

Proc AbilitaREPSEC(pParent)
*--- Abilita combo REPSEC in interroga
   Local obj
   obj = pParent.GetCtrl("w_REPSEC")
   obj.Enabled=.t.
EndProc
* --- Fine Area Manuale
