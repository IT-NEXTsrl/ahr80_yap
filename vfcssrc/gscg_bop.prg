* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bop                                                        *
*              Aggiornamento stato primanota                                   *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-18                                                      *
* Last revis.: 2011-09-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL,pGESTIONE,pCAMPO,pCHIAVE,pVAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bop",oParentObject,m.pSERIAL,m.pGESTIONE,m.pCAMPO,m.pCHIAVE,m.pVAL)
return(i_retval)

define class tgscg_bop as StdBatch
  * --- Local variables
  pSERIAL = space(10)
  pGESTIONE = space(10)
  pCAMPO = space(15)
  pCHIAVE = space(10)
  pVAL = space(10)
  w_PROG = .NULL.
  w_TEXTB = .NULL.
  w_SERIALE = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento massivo del campo stato
    * --- Tipo  'w_PNFLPROV'
    * --- PNSERIAL
    * --- Valore da attribuire al campo passato
     
 exec="this.w_PROG="+Alltrim(this.pGESTIONE) 
 &exec
    * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
    if !(this.w_PROG.bSec1)
      i_retcode = 'stop'
      return
    endif
    this.w_SERIALE = this.pSERIAL
    exec="this.w_PROG.w_"+Alltrim(this.pCHIAVE)+"='"+Alltrim(this.w_SERIALE) +"'" 
 &exec
    this.w_PROG.QueryKeySet(Alltrim(this.pCHIAVE)+"='"+Alltrim(this.w_SERIALE)+"'"," ") 
 
    this.w_PROG.LoadRecWarn()     
    * --- Verifico se posso modificare la registrazione
    if this.w_PROG.HAsCpEvents("EcpEdit")
      this.w_PROG.ecpEdit()     
      this.w_TEXTB = this.w_PROG.GetCtrl(this.pCAMPO)
      this.w_TEXTB.value = this.pVAL
      this.w_TEXTB.Valid()     
      this.w_PROG.SetControlsValue()     
      this.w_PROG.ecpSave()     
    endif
    this.w_PROG.ecpQuit()     
    if TYPE("THIS.W_PROG.CLASS")="C"
      this.w_PROG.ecpQuit()     
    endif
    this.w_PROG = .Null.
  endproc


  proc Init(oParentObject,pSERIAL,pGESTIONE,pCAMPO,pCHIAVE,pVAL)
    this.pSERIAL=pSERIAL
    this.pGESTIONE=pGESTIONE
    this.pCAMPO=pCAMPO
    this.pCHIAVE=pCHIAVE
    this.pVAL=pVAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL,pGESTIONE,pCAMPO,pCHIAVE,pVAL"
endproc
