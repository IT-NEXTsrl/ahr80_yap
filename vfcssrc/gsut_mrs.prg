* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_mrs                                                        *
*              RSS DeskMenu                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-25                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsut_mrs")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsut_mrs")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsut_mrs")
  return

* --- Class definition
define class tgsut_mrs as StdPCForm
  Width  = 499
  Height = 224
  Top    = 22
  Left   = 16
  cComment = "RSS DeskMenu"
  cPrg = "gsut_mrs"
  HelpContextID=113853289
  add object cnt as tcgsut_mrs
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsut_mrs as PCContext
  w_RDCODICE = 0
  w_RDUTEGRP = space(1)
  w_RDRSSCOD = space(15)
  w_RDFLGACT = space(1)
  w_FLGDSK = space(1)
  w_DESCRI = space(254)
  proc Save(i_oFrom)
    this.w_RDCODICE = i_oFrom.w_RDCODICE
    this.w_RDUTEGRP = i_oFrom.w_RDUTEGRP
    this.w_RDRSSCOD = i_oFrom.w_RDRSSCOD
    this.w_RDFLGACT = i_oFrom.w_RDFLGACT
    this.w_FLGDSK = i_oFrom.w_FLGDSK
    this.w_DESCRI = i_oFrom.w_DESCRI
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_RDCODICE = this.w_RDCODICE
    i_oTo.w_RDUTEGRP = this.w_RDUTEGRP
    i_oTo.w_RDRSSCOD = this.w_RDRSSCOD
    i_oTo.w_RDFLGACT = this.w_RDFLGACT
    i_oTo.w_FLGDSK = this.w_FLGDSK
    i_oTo.w_DESCRI = this.w_DESCRI
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsut_mrs as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 499
  Height = 224
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=113853289
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  RSS_DESK_IDX = 0
  RSS_FEED_IDX = 0
  cFile = "RSS_DESK"
  cKeySelect = "RDCODICE,RDUTEGRP"
  cKeyWhere  = "RDCODICE=this.w_RDCODICE and RDUTEGRP=this.w_RDUTEGRP"
  cKeyDetail  = "RDCODICE=this.w_RDCODICE and RDUTEGRP=this.w_RDUTEGRP"
  cKeyWhereODBC = '"RDCODICE="+cp_ToStrODBC(this.w_RDCODICE)';
      +'+" and RDUTEGRP="+cp_ToStrODBC(this.w_RDUTEGRP)';

  cKeyDetailWhereODBC = '"RDCODICE="+cp_ToStrODBC(this.w_RDCODICE)';
      +'+" and RDUTEGRP="+cp_ToStrODBC(this.w_RDUTEGRP)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"RSS_DESK.RDCODICE="+cp_ToStrODBC(this.w_RDCODICE)';
      +'+" and RSS_DESK.RDUTEGRP="+cp_ToStrODBC(this.w_RDUTEGRP)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'RSS_DESK.CPROWNUM '
  cPrg = "gsut_mrs"
  cComment = "RSS DeskMenu"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_RDCODICE = 0
  w_RDUTEGRP = space(1)
  o_RDUTEGRP = space(1)
  w_RDRSSCOD = space(15)
  w_RDFLGACT = space(1)
  w_FLGDSK = space(1)
  w_DESCRI = space(254)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_mrsPag1","gsut_mrs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("RSS Feed")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='RSS_FEED'
    this.cWorkTables[2]='RSS_DESK'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RSS_DESK_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RSS_DESK_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsut_mrs'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from RSS_DESK where RDCODICE=KeySet.RDCODICE
    *                            and RDUTEGRP=KeySet.RDUTEGRP
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.RSS_DESK_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RSS_DESK_IDX,2],this.bLoadRecFilter,this.RSS_DESK_IDX,"gsut_mrs")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RSS_DESK')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RSS_DESK.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RSS_DESK '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'RDCODICE',this.w_RDCODICE  ,'RDUTEGRP',this.w_RDUTEGRP  )
      select * from (i_cTable) RSS_DESK where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_RDCODICE = NVL(RDCODICE,0)
        .w_RDUTEGRP = NVL(RDUTEGRP,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'RSS_DESK')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_FLGDSK = space(1)
          .w_DESCRI = space(254)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_RDRSSCOD = NVL(RDRSSCOD,space(15))
          if link_2_1_joined
            this.w_RDRSSCOD = NVL(RFRSSNAM201,NVL(this.w_RDRSSCOD,space(15)))
            this.w_FLGDSK = NVL(RFFLGDSK201,space(1))
            this.w_DESCRI = NVL(RFDESCRI201,space(254))
          else
          .link_2_1('Load')
          endif
          .w_RDFLGACT = NVL(RDFLGACT,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_RDCODICE=0
      .w_RDUTEGRP=space(1)
      .w_RDRSSCOD=space(15)
      .w_RDFLGACT=space(1)
      .w_FLGDSK=space(1)
      .w_DESCRI=space(254)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        if not(empty(.w_RDRSSCOD))
         .link_2_1('Full')
        endif
        .w_RDFLGACT = 'S'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'RSS_DESK')
    this.DoRTCalc(5,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'RSS_DESK',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RSS_DESK_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDCODICE,"RDCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_RDUTEGRP,"RDUTEGRP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_RDRSSCOD C(15);
      ,t_RDFLGACT N(3);
      ,t_DESCRI C(254);
      ,CPROWNUM N(10);
      ,t_FLGDSK C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsut_mrsbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRDRSSCOD_2_1.controlsource=this.cTrsName+'.t_RDRSSCOD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oRDFLGACT_2_2.controlsource=this.cTrsName+'.t_RDFLGACT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_4.controlsource=this.cTrsName+'.t_DESCRI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(114)
    this.AddVLine(373)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDRSSCOD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RSS_DESK_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RSS_DESK_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RSS_DESK_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RSS_DESK_IDX,2])
      *
      * insert into RSS_DESK
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RSS_DESK')
        i_extval=cp_InsertValODBCExtFlds(this,'RSS_DESK')
        i_cFldBody=" "+;
                  "(RDCODICE,RDUTEGRP,RDRSSCOD,RDFLGACT,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_RDCODICE)+","+cp_ToStrODBC(this.w_RDUTEGRP)+","+cp_ToStrODBCNull(this.w_RDRSSCOD)+","+cp_ToStrODBC(this.w_RDFLGACT)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RSS_DESK')
        i_extval=cp_InsertValVFPExtFlds(this,'RSS_DESK')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'RDCODICE',this.w_RDCODICE,'RDUTEGRP',this.w_RDUTEGRP)
        INSERT INTO (i_cTable) (;
                   RDCODICE;
                  ,RDUTEGRP;
                  ,RDRSSCOD;
                  ,RDFLGACT;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_RDCODICE;
                  ,this.w_RDUTEGRP;
                  ,this.w_RDRSSCOD;
                  ,this.w_RDFLGACT;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- gsut_mrs
    *--- Non salvo se minore di 1
    If this.w_RDCODICE<1
       ah_ErrorMsg("Impossibile salvare%0Codice utente/gruppo non valorizzato")
       Return
    Endif
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.RSS_DESK_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RSS_DESK_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_RDRSSCOD))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'RSS_DESK')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'RSS_DESK')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_RDRSSCOD))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update RSS_DESK
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'RSS_DESK')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " RDRSSCOD="+cp_ToStrODBCNull(this.w_RDRSSCOD)+;
                     ",RDFLGACT="+cp_ToStrODBC(this.w_RDFLGACT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'RSS_DESK')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      RDRSSCOD=this.w_RDRSSCOD;
                     ,RDFLGACT=this.w_RDFLGACT;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RSS_DESK_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RSS_DESK_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_RDRSSCOD))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete RSS_DESK
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_RDRSSCOD))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RSS_DESK_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RSS_DESK_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_FLGDSK with this.w_FLGDSK
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RDRSSCOD
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RSS_FEED_IDX,3]
    i_lTable = "RSS_FEED"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RSS_FEED_IDX,2], .t., this.RSS_FEED_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RSS_FEED_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RDRSSCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_ARS',True,'RSS_FEED')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RFRSSNAM like "+cp_ToStrODBC(trim(this.w_RDRSSCOD)+"%");

          i_ret=cp_SQL(i_nConn,"select RFRSSNAM,RFFLGDSK,RFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RFRSSNAM","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RFRSSNAM',trim(this.w_RDRSSCOD))
          select RFRSSNAM,RFFLGDSK,RFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RFRSSNAM into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RDRSSCOD)==trim(_Link_.RFRSSNAM) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RDRSSCOD) and !this.bDontReportError
            deferred_cp_zoom('RSS_FEED','*','RFRSSNAM',cp_AbsName(oSource.parent,'oRDRSSCOD_2_1'),i_cWhere,'GSUT_ARS',"RSS Feed",'GSUT_MRS.RSS_FEED_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RFRSSNAM,RFFLGDSK,RFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RFRSSNAM="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RFRSSNAM',oSource.xKey(1))
            select RFRSSNAM,RFFLGDSK,RFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RDRSSCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RFRSSNAM,RFFLGDSK,RFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RFRSSNAM="+cp_ToStrODBC(this.w_RDRSSCOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RFRSSNAM',this.w_RDRSSCOD)
            select RFRSSNAM,RFFLGDSK,RFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RDRSSCOD = NVL(_Link_.RFRSSNAM,space(15))
      this.w_FLGDSK = NVL(_Link_.RFFLGDSK,space(1))
      this.w_DESCRI = NVL(_Link_.RFDESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_RDRSSCOD = space(15)
      endif
      this.w_FLGDSK = space(1)
      this.w_DESCRI = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLGDSK<>'N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_RDRSSCOD = space(15)
        this.w_FLGDSK = space(1)
        this.w_DESCRI = space(254)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RSS_FEED_IDX,2])+'\'+cp_ToStr(_Link_.RFRSSNAM,1)
      cp_ShowWarn(i_cKey,this.RSS_FEED_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RDRSSCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.RSS_FEED_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.RSS_FEED_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.RFRSSNAM as RFRSSNAM201"+ ",link_2_1.RFFLGDSK as RFFLGDSK201"+ ",link_2_1.RFDESCRI as RFDESCRI201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on RSS_DESK.RDRSSCOD=link_2_1.RFRSSNAM"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and RSS_DESK.RDRSSCOD=link_2_1.RFRSSNAM(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDRSSCOD_2_1.value==this.w_RDRSSCOD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDRSSCOD_2_1.value=this.w_RDRSSCOD
      replace t_RDRSSCOD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDRSSCOD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDFLGACT_2_2.RadioValue()==this.w_RDFLGACT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDFLGACT_2_2.SetRadio()
      replace t_RDFLGACT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDFLGACT_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_4.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_4.value=this.w_DESCRI
      replace t_DESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESCRI_2_4.value
    endif
    cp_SetControlsValueExtFlds(this,'RSS_DESK')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(.w_FLGDSK<>'N') and not(empty(.w_RDRSSCOD)) and (not(Empty(.w_RDRSSCOD)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDRSSCOD_2_1
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_RDRSSCOD))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RDUTEGRP = this.w_RDUTEGRP
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_RDRSSCOD)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_RDRSSCOD=space(15)
      .w_RDFLGACT=space(1)
      .w_FLGDSK=space(1)
      .w_DESCRI=space(254)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_RDRSSCOD))
        .link_2_1('Full')
      endif
        .w_RDFLGACT = 'S'
    endwith
    this.DoRTCalc(5,6,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_RDRSSCOD = t_RDRSSCOD
    this.w_RDFLGACT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDFLGACT_2_2.RadioValue(.t.)
    this.w_FLGDSK = t_FLGDSK
    this.w_DESCRI = t_DESCRI
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_RDRSSCOD with this.w_RDRSSCOD
    replace t_RDFLGACT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oRDFLGACT_2_2.ToRadio()
    replace t_FLGDSK with this.w_FLGDSK
    replace t_DESCRI with this.w_DESCRI
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsut_mrsPag1 as StdContainer
  Width  = 495
  height = 224
  stdWidth  = 495
  stdheight = 224
  resizeXpos=250
  resizeYpos=111
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=8, top=6, width=479,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="RDRSSCOD",Label1="RSS Feed",Field2="DESCRI",Label2="Descrizione",Field3="RDFLGACT",Label3="Visibile",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 202237562

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=25,;
    width=475+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=26,width=474+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='RSS_FEED|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='RSS_FEED'
        oDropInto=this.oBodyCol.oRow.oRDRSSCOD_2_1
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsut_mrsBodyRow as CPBodyRowCnt
  Width=465
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oRDRSSCOD_2_1 as StdTrsField with uid="LMEJLRLWIP",rtseq=3,rtrep=.t.,;
    cFormVar="w_RDRSSCOD",value=space(15),;
    HelpContextID = 239131046,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=105, Left=-2, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="RSS_FEED", cZoomOnZoom="GSUT_ARS", oKey_1_1="RFRSSNAM", oKey_1_2="this.w_RDRSSCOD"

  func oRDRSSCOD_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oRDRSSCOD_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oRDRSSCOD_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'RSS_FEED','*','RFRSSNAM',cp_AbsName(this.parent,'oRDRSSCOD_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_ARS',"RSS Feed",'GSUT_MRS.RSS_FEED_VZM',this.parent.oContained
  endproc
  proc oRDRSSCOD_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSUT_ARS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RFRSSNAM=this.parent.oContained.w_RDRSSCOD
    i_obj.ecpSave()
  endproc

  add object oRDFLGACT_2_2 as StdTrsCombo with uid="TZTHDHTEMK",rtrep=.t.,;
    cFormVar="w_RDFLGACT", RowSource=""+"Si,"+"No,"+"No tassativo" , ;
    ToolTipText = "RSS Feed visibile in RSS Viewer",;
    HelpContextID = 251094634,;
    Height=21, Width=94, Left=366, Top=1,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oRDFLGACT_2_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..RDFLGACT,&i_cF..t_RDFLGACT),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    iif(xVal =3,'X',;
    space(1)))))
  endfunc
  func oRDFLGACT_2_2.GetRadio()
    this.Parent.oContained.w_RDFLGACT = this.RadioValue()
    return .t.
  endfunc

  func oRDFLGACT_2_2.ToRadio()
    this.Parent.oContained.w_RDFLGACT=trim(this.Parent.oContained.w_RDFLGACT)
    return(;
      iif(this.Parent.oContained.w_RDFLGACT=='S',1,;
      iif(this.Parent.oContained.w_RDFLGACT=='N',2,;
      iif(this.Parent.oContained.w_RDFLGACT=='X',3,;
      0))))
  endfunc

  func oRDFLGACT_2_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDESCRI_2_4 as StdTrsField with uid="CNMUYJXNYE",rtseq=6,rtrep=.t.,;
    cFormVar="w_DESCRI",value=space(254),enabled=.f.,;
    HelpContextID = 140560842,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=256, Left=105, Top=0, InputMask=replicate('X',254)
  add object oLast as LastKeyMover
  * ---
  func oRDRSSCOD_2_1.When()
    return(.t.)
  proc oRDRSSCOD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oRDRSSCOD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_mrs','RSS_DESK','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".RDCODICE=RSS_DESK.RDCODICE";
  +" and "+i_cAliasName2+".RDUTEGRP=RSS_DESK.RDUTEGRP";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
