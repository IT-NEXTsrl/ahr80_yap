* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_szr                                                        *
*              Visualizza punto e lotto di riordino                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-07-14                                                      *
* Last revis.: 2014-12-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_szr",oParentObject))

* --- Class definition
define class tgsma_szr as StdForm
  Top    = -2
  Left   = 6

  * --- Standard Properties
  Width  = 763
  Height = 490
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-11"
  HelpContextID=241859433
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  INVEDETT_IDX = 0
  INVENTAR_IDX = 0
  MAGAZZIN_IDX = 0
  ESERCIZI_IDX = 0
  RIORDINO_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gsma_szr"
  cComment = "Visualizza punto e lotto di riordino"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(10)
  w_RINUMRIO = space(6)
  o_RINUMRIO = space(6)
  w_NUMRIO = space(6)
  w_DESRIO = space(40)
  w_CODART = space(20)
  w_TIPVAR = space(5)
  w_CODESE = space(4)
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_TIPO = space(2)
  w_DESART = space(40)
  w_DICODVAR = space(20)
  w_ZICODICE = space(20)
  w_DESARTI = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_CODICE = space(40)
  w_TIPART = space(10)
  w_CODFAM = space(5)
  w_CODGRU = space(5)
  w_CODCAT = space(5)
  w_CODMAR = space(5)
  w_COEFFI = 0
  w_COSMED = 0
  w_COSBEN = 0
  w_PERINT = 0
  w_PERINC = 0
  w_AGGIORNA = .F.
  w_SELEZI = space(1)
  w_TIPAGG = space(1)
  w_TIPGES = space(1)
  w_ZoomRio = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_szrPag1","gsma_szr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRINUMRIO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomRio = this.oPgFrm.Pages(1).oPag.ZoomRio
    DoDefault()
    proc Destroy()
      this.w_ZoomRio = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='INVEDETT'
    this.cWorkTables[3]='INVENTAR'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='ESERCIZI'
    this.cWorkTables[6]='RIORDINO'
    this.cWorkTables[7]='AZIENDA'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(10)
      .w_RINUMRIO=space(6)
      .w_NUMRIO=space(6)
      .w_DESRIO=space(40)
      .w_CODART=space(20)
      .w_TIPVAR=space(5)
      .w_CODESE=space(4)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_TIPO=space(2)
      .w_DESART=space(40)
      .w_DICODVAR=space(20)
      .w_ZICODICE=space(20)
      .w_DESARTI=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_CODICE=space(40)
      .w_TIPART=space(10)
      .w_CODFAM=space(5)
      .w_CODGRU=space(5)
      .w_CODCAT=space(5)
      .w_CODMAR=space(5)
      .w_COEFFI=0
      .w_COSMED=0
      .w_COSBEN=0
      .w_PERINT=0
      .w_PERINC=0
      .w_AGGIORNA=.f.
      .w_SELEZI=space(1)
      .w_TIPAGG=space(1)
      .w_TIPGES=space(1)
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_RINUMRIO))
          .link_1_2('Full')
        endif
        .w_NUMRIO = .w_RINUMRIO
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_CODART))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_CODESE = g_CODESE
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODESE))
          .link_1_7('Full')
        endif
          .DoRTCalc(8,9,.f.)
        .w_DATINI = .w_INIESE
        .w_DATFIN = .w_FINESE
      .oPgFrm.Page1.oPag.ZoomRio.Calculate()
          .DoRTCalc(12,14,.f.)
        .w_ZICODICE = .w_ZoomRio.getVar('DICODART')
        .w_DESARTI = .w_ZoomRio.getVar('DESART')
        .w_OBTEST = .w_DATINI
        .w_CODICE = .w_CODART
        .w_TIPART = ''
      .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
          .DoRTCalc(20,28,.f.)
        .w_AGGIORNA = .F.
        .w_SELEZI = "D"
        .w_TIPAGG = 'E'
        .w_TIPGES = 'T'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_55.enabled = this.oPgFrm.Page1.oPag.oBtn_1_55.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_56.enabled = this.oPgFrm.Page1.oPag.oBtn_1_56.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_RINUMRIO<>.w_RINUMRIO
            .w_NUMRIO = .w_RINUMRIO
        endif
        .oPgFrm.Page1.oPag.ZoomRio.Calculate()
        .DoRTCalc(4,14,.t.)
            .w_ZICODICE = .w_ZoomRio.getVar('DICODART')
            .w_DESARTI = .w_ZoomRio.getVar('DESART')
        .DoRTCalc(17,17,.t.)
            .w_CODICE = .w_CODART
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(19,32,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomRio.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_55.enabled = this.oPgFrm.Page1.oPag.oBtn_1_55.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomRio.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=RINUMRIO
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.RIORDINO_IDX,3]
    i_lTable = "RIORDINO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.RIORDINO_IDX,2], .t., this.RIORDINO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.RIORDINO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RINUMRIO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'RIORDINO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RINUMRIO like "+cp_ToStrODBC(trim(this.w_RINUMRIO)+"%");
                   +" and RICODESE="+cp_ToStrODBC(this.w_CODESE);

          i_ret=cp_SQL(i_nConn,"select RICODESE,RINUMRIO,RIDESRIO,RIDATINI,RIDATFIN,RICOEFFI,RICOSORD,RICOSBEN,RIPERINT,RIPERIZA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RICODESE,RINUMRIO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RICODESE',this.w_CODESE;
                     ,'RINUMRIO',trim(this.w_RINUMRIO))
          select RICODESE,RINUMRIO,RIDESRIO,RIDATINI,RIDATFIN,RICOEFFI,RICOSORD,RICOSBEN,RIPERINT,RIPERIZA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RICODESE,RINUMRIO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RINUMRIO)==trim(_Link_.RINUMRIO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_RINUMRIO) and !this.bDontReportError
            deferred_cp_zoom('RIORDINO','*','RICODESE,RINUMRIO',cp_AbsName(oSource.parent,'oRINUMRIO_1_2'),i_cWhere,'',"Numero riordino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODESE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODESE,RINUMRIO,RIDESRIO,RIDATINI,RIDATFIN,RICOEFFI,RICOSORD,RICOSBEN,RIPERINT,RIPERIZA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select RICODESE,RINUMRIO,RIDESRIO,RIDATINI,RIDATFIN,RICOEFFI,RICOSORD,RICOSBEN,RIPERINT,RIPERIZA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODESE,RINUMRIO,RIDESRIO,RIDATINI,RIDATFIN,RICOEFFI,RICOSORD,RICOSBEN,RIPERINT,RIPERIZA";
                     +" from "+i_cTable+" "+i_lTable+" where RINUMRIO="+cp_ToStrODBC(oSource.xKey(2));
                     +" and RICODESE="+cp_ToStrODBC(this.w_CODESE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODESE',oSource.xKey(1);
                       ,'RINUMRIO',oSource.xKey(2))
            select RICODESE,RINUMRIO,RIDESRIO,RIDATINI,RIDATFIN,RICOEFFI,RICOSORD,RICOSBEN,RIPERINT,RIPERIZA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RINUMRIO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODESE,RINUMRIO,RIDESRIO,RIDATINI,RIDATFIN,RICOEFFI,RICOSORD,RICOSBEN,RIPERINT,RIPERIZA";
                   +" from "+i_cTable+" "+i_lTable+" where RINUMRIO="+cp_ToStrODBC(this.w_RINUMRIO);
                   +" and RICODESE="+cp_ToStrODBC(this.w_CODESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODESE',this.w_CODESE;
                       ,'RINUMRIO',this.w_RINUMRIO)
            select RICODESE,RINUMRIO,RIDESRIO,RIDATINI,RIDATFIN,RICOEFFI,RICOSORD,RICOSBEN,RIPERINT,RIPERIZA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RINUMRIO = NVL(_Link_.RINUMRIO,space(6))
      this.w_DESRIO = NVL(_Link_.RIDESRIO,space(40))
      this.w_DATINI = NVL(cp_ToDate(_Link_.RIDATINI),ctod("  /  /  "))
      this.w_DATFIN = NVL(cp_ToDate(_Link_.RIDATFIN),ctod("  /  /  "))
      this.w_COEFFI = NVL(_Link_.RICOEFFI,0)
      this.w_COSMED = NVL(_Link_.RICOSORD,0)
      this.w_COSBEN = NVL(_Link_.RICOSBEN,0)
      this.w_PERINT = NVL(_Link_.RIPERINT,0)
      this.w_PERINC = NVL(_Link_.RIPERIZA,0)
    else
      if i_cCtrl<>'Load'
        this.w_RINUMRIO = space(6)
      endif
      this.w_DESRIO = space(40)
      this.w_DATINI = ctod("  /  /  ")
      this.w_DATFIN = ctod("  /  /  ")
      this.w_COEFFI = 0
      this.w_COSMED = 0
      this.w_COSBEN = 0
      this.w_PERINT = 0
      this.w_PERINC = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.RIORDINO_IDX,2])+'\'+cp_ToStr(_Link_.RICODESE,1)+'\'+cp_ToStr(_Link_.RINUMRIO,1)
      cp_ShowWarn(i_cKey,this.RIORDINO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RINUMRIO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARTIPART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_5'),i_cWhere,'',"Codici articoli/servizi",'GSMA_ZRA.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARTIPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_TIPO = NVL(_Link_.ARTIPART,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_TIPO = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPO$'PF-SE-MP-PH-MC-MA-IM-FS'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido")
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
        this.w_TIPO = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_7'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRINUMRIO_1_2.value==this.w_RINUMRIO)
      this.oPgFrm.Page1.oPag.oRINUMRIO_1_2.value=this.w_RINUMRIO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIO_1_4.value==this.w_DESRIO)
      this.oPgFrm.Page1.oPag.oDESRIO_1_4.value=this.w_DESRIO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_5.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_5.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_7.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_7.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_10.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_10.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_11.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_11.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_13.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_13.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESARTI_1_21.value==this.w_DESARTI)
      this.oPgFrm.Page1.oPag.oDESARTI_1_21.value=this.w_DESARTI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOEFFI_1_35.value==this.w_COEFFI)
      this.oPgFrm.Page1.oPag.oCOEFFI_1_35.value=this.w_COEFFI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSMED_1_38.value==this.w_COSMED)
      this.oPgFrm.Page1.oPag.oCOSMED_1_38.value=this.w_COSMED
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSBEN_1_40.value==this.w_COSBEN)
      this.oPgFrm.Page1.oPag.oCOSBEN_1_40.value=this.w_COSBEN
    endif
    if not(this.oPgFrm.Page1.oPag.oPERINT_1_43.value==this.w_PERINT)
      this.oPgFrm.Page1.oPag.oPERINT_1_43.value=this.w_PERINT
    endif
    if not(this.oPgFrm.Page1.oPag.oPERINC_1_44.value==this.w_PERINC)
      this.oPgFrm.Page1.oPag.oPERINC_1_44.value=this.w_PERINC
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_52.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_52.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPAGG_1_53.RadioValue()==this.w_TIPAGG)
      this.oPgFrm.Page1.oPag.oTIPAGG_1_53.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPGES_1_59.RadioValue()==this.w_TIPGES)
      this.oPgFrm.Page1.oPag.oTIPGES_1_59.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_RINUMRIO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oRINUMRIO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_RINUMRIO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPO$'PF-SE-MP-PH-MC-MA-IM-FS')  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_RINUMRIO = this.w_RINUMRIO
    return

enddefine

* --- Define pages as container
define class tgsma_szrPag1 as StdContainer
  Width  = 759
  height = 490
  stdWidth  = 759
  stdheight = 490
  resizeXpos=455
  resizeYpos=289
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRINUMRIO_1_2 as StdField with uid="PFYDRLHUFL",rtseq=2,rtrep=.f.,;
    cFormVar = "w_RINUMRIO", cQueryName = "RINUMRIO",nZero=6,;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Numero riordino",;
    HelpContextID = 121654427,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=93, Top=10, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="RIORDINO", oKey_1_1="RICODESE", oKey_1_2="this.w_CODESE", oKey_2_1="RINUMRIO", oKey_2_2="this.w_RINUMRIO"

  func oRINUMRIO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oRINUMRIO_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRINUMRIO_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.RIORDINO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RICODESE="+cp_ToStrODBC(this.Parent.oContained.w_CODESE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"RICODESE="+cp_ToStr(this.Parent.oContained.w_CODESE)
    endif
    do cp_zoom with 'RIORDINO','*','RICODESE,RINUMRIO',cp_AbsName(this.parent,'oRINUMRIO_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Numero riordino",'',this.parent.oContained
  endproc

  add object oDESRIO_1_4 as StdField with uid="UQOYJMWKLQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESRIO", cQueryName = "DESRIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 176357834,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=165, Top=10, InputMask=replicate('X',40)

  add object oCODART_1_5 as StdField with uid="LMQKKOXCJV",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido",;
    ToolTipText = "Codice dell'articolo da visualizzare",;
    HelpContextID = 84207578,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=93, Top=33, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici articoli/servizi",'GSMA_ZRA.ART_ICOL_VZM',this.parent.oContained
  endproc

  add object oCODESE_1_7 as StdField with uid="OSCLXMSDGD",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio di riferimento",;
    HelpContextID = 66119642,;
   bGlobalFont=.t.,;
    Height=21, Width=56, Left=606, Top=10, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
      if .not. empty(.w_RINUMRIO)
        bRes2=.link_1_2('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODESE_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oDATINI_1_10 as StdField with uid="AIYFWHRFTY",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data iniziale dell'esercizio di riferimento",;
    HelpContextID = 3929546,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=606, Top=33

  add object oDATFIN_1_11 as StdField with uid="RIWUHZFDZX",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data finale dell'esercizio di riferimento",;
    HelpContextID = 193918410,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=606, Top=56

  add object oDESART_1_13 as StdField with uid="HILPYIWGQW",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 84148682,;
   bGlobalFont=.t.,;
    Height=21, Width=260, Left=262, Top=33, InputMask=replicate('X',40)


  add object oBtn_1_14 as StdButton with uid="USWVWXLGBS",left=702, top=34, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per rieseguire la ricerca con le nuove selezioni";
    , HelpContextID = 39013446;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      this.parent.oContained.NotifyEvent("Esegui")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomRio as cp_szoombox with uid="OIXARDUJXF",left=3, top=160, width=752,height=269,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="RIORDETT",cZoomFile="GSMA_ZRI",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bAdvOptions=.t.,cMenuFile="",cZoomOnZoom="",bRetriveAllRows=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 204573670

  add object oDESARTI_1_21 as StdField with uid="HJPFIKFZAZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESARTI", cQueryName = "DESARTI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 84148682,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=104, Top=459, InputMask=replicate('X',40)

  add object oCOEFFI_1_35 as StdField with uid="OEOBVFYAHE",rtseq=24,rtrep=.f.,;
    cFormVar = "w_COEFFI", cQueryName = "COEFFI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Coefficiente di correzione",;
    HelpContextID = 12572634,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=175, Top=113, cSayPict='"99.99"', cGetPict='"99.99"'

  add object oCOSMED_1_38 as StdField with uid="UFJMLJDXYF",rtseq=25,rtrep=.f.,;
    cFormVar = "w_COSMED", cQueryName = "COSMED",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo medio unitario di un ordine (espresso in valuta di conto)",;
    HelpContextID = 96991194,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=399, Top=112, cSayPict="V_GU(20)", cGetPict="V_GU(20)"

  add object oCOSBEN_1_40 as StdField with uid="CNGNXAQPDU",rtseq=26,rtrep=.f.,;
    cFormVar = "w_COSBEN", cQueryName = "COSBEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Costo del bene (espresso in valuta di conto)",;
    HelpContextID = 198375386,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=399, Top=133, cSayPict="V_GU(20)", cGetPict="V_GU(20)"

  add object oPERINT_1_43 as StdField with uid="KZASSJFTPX",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PERINT", cQueryName = "PERINT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale interesse sul capitale immobilizzato",;
    HelpContextID = 87822602,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=632, Top=112, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oPERINC_1_44 as StdField with uid="CPWAFHDQJX",rtseq=28,rtrep=.f.,;
    cFormVar = "w_PERINC", cQueryName = "PERINC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Incidenza del costo di gestione del magazzino",;
    HelpContextID = 104599818,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=632, Top=132, cSayPict='"999.99"', cGetPict='"999.99"'


  add object oObj_1_49 as cp_runprogram with uid="MCVVMPGLZS",left=-1, top=513, width=247,height=22,;
    caption='GSMA_BLP(SEL)',;
   bGlobalFont=.t.,;
    prg='GSMA_BLP("SEL")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 55815626


  add object oObj_1_50 as cp_runprogram with uid="JPYBAVPPKF",left=-2, top=493, width=247,height=19,;
    caption='GSMA_BLP("CHE")',;
   bGlobalFont=.t.,;
    prg='GSMA_BLP("CHE")',;
    cEvent = "w_zoomrio row checked,w_zoomrio row unchecked",;
    nPag=1;
    , HelpContextID = 6816054

  add object oSELEZI_1_52 as StdRadio with uid="MBUITXAEWO",rtseq=30,rtrep=.f.,left=6, top=434, width=239,height=23;
    , ToolTipText = "Seleziona\deseleziona tutti";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_52.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 260075738
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Seleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 260075738
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Deseleziona tutte","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona\deseleziona tutti")
      StdRadio::init()
    endproc

  func oSELEZI_1_52.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"D",;
    space(1))))
  endfunc
  func oSELEZI_1_52.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_52.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=="S",1,;
      iif(this.Parent.oContained.w_SELEZI=="D",2,;
      0))
  endfunc


  add object oTIPAGG_1_53 as StdCombo with uid="NPWXGYYMNK",rtseq=31,rtrep=.f.,left=503,top=459,width=125,height=21;
    , HelpContextID = 45362378;
    , cFormVar="w_TIPAGG",RowSource=""+"Entrambi,"+"Punto di riordino,"+"Lotto di riordino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPAGG_1_53.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    iif(this.value =3,'L',;
    space(1)))))
  endfunc
  func oTIPAGG_1_53.GetRadio()
    this.Parent.oContained.w_TIPAGG = this.RadioValue()
    return .t.
  endfunc

  func oTIPAGG_1_53.SetRadio()
    this.Parent.oContained.w_TIPAGG=trim(this.Parent.oContained.w_TIPAGG)
    this.value = ;
      iif(this.Parent.oContained.w_TIPAGG=='E',1,;
      iif(this.Parent.oContained.w_TIPAGG=='P',2,;
      iif(this.Parent.oContained.w_TIPAGG=='L',3,;
      0)))
  endfunc


  add object oBtn_1_55 as StdButton with uid="ZPSYQDEREF",left=649, top=437, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare";
    , HelpContextID = 241859338;
    , caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_55.Click()
      with this.Parent.oContained
        GSMA_BLP(this.Parent.oContained,"AGG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_55.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_AGGIORNA)
      endwith
    endif
  endfunc


  add object oBtn_1_56 as StdButton with uid="OGAKEGIOPL",left=701, top=437, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 117521942;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_56.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oTIPGES_1_59 as StdCombo with uid="VZEIERUVRF",rtseq=32,rtrep=.f.,left=93,top=60,width=141,height=22;
    , HelpContextID = 114175178;
    , cFormVar="w_TIPGES",RowSource=""+"a fabbisogno,"+"a scorta,"+"a consumo,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPGES_1_59.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oTIPGES_1_59.GetRadio()
    this.Parent.oContained.w_TIPGES = this.RadioValue()
    return .t.
  endfunc

  func oTIPGES_1_59.SetRadio()
    this.Parent.oContained.w_TIPGES=trim(this.Parent.oContained.w_TIPGES)
    this.value = ;
      iif(this.Parent.oContained.w_TIPGES=='F',1,;
      iif(this.Parent.oContained.w_TIPGES=='S',2,;
      iif(this.Parent.oContained.w_TIPGES=='C',3,;
      iif(this.Parent.oContained.w_TIPGES=='T',4,;
      0))))
  endfunc

  add object oStr_1_16 as StdString with uid="PLHMTIVSHK",Visible=.t., Left=32, Top=35,;
    Alignment=1, Width=56, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="APJQUMPIOW",Visible=.t., Left=32, Top=12,;
    Alignment=1, Width=56, Height=15,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="OOQWRLEFMF",Visible=.t., Left=2, Top=461,;
    Alignment=1, Width=97, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="RAVLMMIYTL",Visible=.t., Left=540, Top=12,;
    Alignment=1, Width=56, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="HLRHHBHBXL",Visible=.t., Left=530, Top=35,;
    Alignment=1, Width=66, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="QDNIZGQSXW",Visible=.t., Left=533, Top=58,;
    Alignment=1, Width=63, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="VXIILJIGTL",Visible=.t., Left=13, Top=90,;
    Alignment=0, Width=137, Height=15,;
    Caption="Punto di riordino"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="HNEHDOZWGC",Visible=.t., Left=18, Top=113,;
    Alignment=1, Width=152, Height=15,;
    Caption="Coefficiente di correzione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="WGPVFCRZCO",Visible=.t., Left=255, Top=90,;
    Alignment=0, Width=106, Height=15,;
    Caption="Lotto di riordino"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="YPIREOOMTH",Visible=.t., Left=252, Top=112,;
    Alignment=1, Width=141, Height=15,;
    Caption="Costo unitario ordine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="PKYXGLPKIQ",Visible=.t., Left=305, Top=133,;
    Alignment=1, Width=88, Height=15,;
    Caption="Costo del bene:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="WZTXNASBNS",Visible=.t., Left=573, Top=112,;
    Alignment=1, Width=55, Height=15,;
    Caption="Interesse:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="CGESIXRTKB",Visible=.t., Left=567, Top=133,;
    Alignment=1, Width=61, Height=15,;
    Caption="Incidenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="ROGEGGISIZ",Visible=.t., Left=550, Top=112,;
    Alignment=0, Width=15, Height=15,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="QYNKJHJXWF",Visible=.t., Left=550, Top=134,;
    Alignment=0, Width=15, Height=15,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="XXETGOWZMK",Visible=.t., Left=437, Top=461,;
    Alignment=1, Width=66, Height=18,;
    Caption="Aggiorna:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="VROWNUIQGZ",Visible=.t., Left=339, Top=433,;
    Alignment=0, Width=182, Height=18,;
    Caption="Prov. interna/Conto lavoro"    , ForeColor=RGB(215,89,208);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_58 as StdString with uid="PCLKLTNIWC",Visible=.t., Left=531, Top=433,;
    Alignment=0, Width=91, Height=18,;
    Caption="Prov. esterna"    , ForeColor=RGB(0,0,255);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_60 as StdString with uid="UWCKJHVKFY",Visible=.t., Left=-6, Top=62,;
    Alignment=1, Width=94, Height=18,;
    Caption="Tipo gestione:"  ;
  , bGlobalFont=.t.

  add object oBox_1_17 as StdBox with uid="LTOQBCUJXT",left=2, top=158, width=754,height=1

  add object oBox_1_45 as StdBox with uid="PYZNCEKZVD",left=15, top=106, width=216,height=1

  add object oBox_1_46 as StdBox with uid="AMVEUICCVJ",left=256, top=106, width=433,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_szr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
