* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bes                                                        *
*              Legge dati cli/for                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_287]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-09                                                      *
* Last revis.: 2018-02-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bes",oParentObject,m.pOper)
return(i_retval)

define class tgsve_bes as StdBatch
  * --- Local variables
  pOper = space(1)
  w_TIPOPE = space(1)
  w_NDIC = 0
  w_FID1 = 0
  w_IMPDIC = 0
  w_ADIC = space(4)
  w_FID2 = 0
  w_IMPUTI = 0
  w_DDIC = ctod("  /  /  ")
  w_FID3 = 0
  w_CODIVE = space(5)
  w_TDIC = space(1)
  w_FID4 = 0
  w_OK = .f.
  w_RIFDIC = space(15)
  w_FID5 = 0
  w_TIVA = space(1)
  w_FID6 = 0
  w_DATINI = ctod("  /  /  ")
  w_RECO = 0
  w_FIDRES = 0
  w_DATFIN = ctod("  /  /  ")
  w_FIDD = ctod("  /  /  ")
  w_AGGRIG = .f.
  w_CHGVAL = .f.
  w_DTOBS1 = ctod("  /  /  ")
  w_CODAGE = space(5)
  w_ANCODAG = space(5)
  w_AGDTOBSO = ctod("  /  /  ")
  w_OKCONTRA = .f.
  w_CT = space(1)
  w_CC = space(15)
  w_CM = space(3)
  w_CI = ctod("  /  /  ")
  w_CF = ctod("  /  /  ")
  w_CV = space(3)
  w_CL = space(1)
  w_PADRE = .NULL.
  w_LCLIORNUPD = .f.
  w_LCODDES = space(5)
  w_LNOMDES = space(36)
  w_LCAPDES = space(8)
  w_LEMAIL2 = space(25)
  w_LINDDES = space(36)
  w_LLOCDES = space(28)
  w_LCODPOR = space(1)
  w_LCODSPE = space(3)
  w_LCODVET = space(5)
  w_LPREDEF = space(1)
  w_LPRODES = space(2)
  w_LTIPRIF = space(2)
  w_LCODNAZ = space(3)
  w_LCODDESPRECEDENTE = space(5)
  w_OBJCTRL = .NULL.
  w_LMCALST4 = space(5)
  w_LMCALSI4 = space(5)
  w_OK_LET = .f.
  w_RECTRS = 0
  w_WARN = space(1)
  w_oMESS = .NULL.
  w_oPART = .NULL.
  * --- WorkFile variables
  DES_DIVE_idx=0
  LISTINI_idx=0
  PAG_AMEN_idx=0
  SIT_FIDI_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  CONTI_idx=0
  AGENTI_idx=0
  CON_TRAM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Legge Eventuale Cod.IVA dalla Dichiarazione di Esenzione (da GSVE_MDV, GSOR_MDV, GSAC_MDV)
    * --- Inoltre se Documento Accompagnatorio, legge i Dati di Consegna.
    * --- D = Cambio Documento ; C = Cambio Cli/For
    if this.pOper="C" And this.oParentObject.o_MVCODCON==this.oParentObject.w_MVCODCON AND this.oParentObject.o_MVCODORN <> this.oParentObject.w_MVCODORN And Empty(g_XCONDI)
      * --- Se non ho attivato la gestione Per Conto Di nei dati azienda 
      *     e questo batch � stato lanciato per la modifica del campo MVCODORN sul documento
      *     non deve fare niente
      i_retcode = 'stop'
      return
    endif
    if this.pOper="C" And this.oParentObject.o_MVCODCON<>this.oParentObject.w_MVCODCON
      * --- Se questo batch � stato lanciato per la modifica del campo MVCODICE sul documento
      *     non valorizzo il per conto di se sono in fattura, nota di credito e corrispettivi
      this.oParentObject.w_MVCODORN = IIF(this.oParentObject.w_MVCLADOC$"FA-NC-RF" OR g_PERORN<>"S", space(15), this.oParentObject.w_MVCODORN)
    endif
    if this.pOper="C" And this.oParentObject.o_MVCODORN <> this.oParentObject.w_MVCODORN And ( g_XCONDI="S" OR g_PERORN="S") And Not Empty(this.oParentObject.w_MVCODORN)
      * --- Se attiva la gestione per conto di in azienda e inserisco un per conto di 
      *     non posso gestire gli acconti. Quindi svuoto il campo
      if g_XCONDI="S"
        this.oParentObject.w_MVACCONT = 0
      endif
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI"+;
          " from "+i_cTable+" CONTI where ";
              +"ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODORN);
              +" and ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPORN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI;
          from (i_cTable) where;
              ANCODICE = this.oParentObject.w_MVCODORN;
              and ANTIPCON = this.oParentObject.w_MVTIPORN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_DESORN = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
        this.oParentObject.w_INDORN = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
        this.oParentObject.w_CAPORN = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
        this.oParentObject.w_LOCORN = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Assegna il padre
    this.w_PADRE = This.oParentObject
    * --- ATTENZIONE
    *     Aggiorno le o_ sulla dipendenza delle quali viene lanciato questo batch.
    *     Visto che parte alla fine della mCalc dei documenti, tutti i batch lanciati da questo (GSVE_BCD, GSVE_BCP)
    *     lo rilancerebbero andando in Loop.
    *     Questi assegnamenti non devono essere quindi ne Spostati ne Eliminati
    this.w_LCLIORNUPD = this.pOper="C" AND (this.oParentObject.o_MVCODCON<>this.oParentObject.w_MVCODCON OR this.oParentObject.o_MVCODORN<>this.oParentObject.w_MVCODORN)
    this.oParentObject.o_MVCODCON = this.oParentObject.w_MVCODCON
    this.oParentObject.o_MVCODORN = this.oParentObject.w_MVCODORN
    * --- Se w_FLCHKNUMD <> true non scatta in controllo numerazione doc.
    this.oParentObject.w_FLCHKNUMD = this.oParentObject.w_FLCHKNUMD or this.oParentObject.o_MVDATDOC<>this.oParentObject.w_MVDATDOC
    this.oParentObject.o_MVDATDOC = this.oParentObject.w_MVDATDOC
    if this.pOper = "C" And Empty(this.oParentObject.w_MVCODCON)
      * --- Nel caso parta il cp_Zoom per aver inserito le prime lettere del codice 
      *     questo batch parte lo stesso ma non ha senso eseguirlo fino a che non sia
      *     stato selezionato il codice cliente
      i_retcode = 'stop'
      return
    endif
    * --- Inizializzo campi quando in modifica data o cliente entro ancora dentro
    this.oParentObject.w_MVCODIVE = this.oParentObject.w_IVACLI
    this.oParentObject.w_BOLIVE = this.oParentObject.w_BOLICF
    this.oParentObject.w_PERIVE = this.oParentObject.w_PERICF
    this.oParentObject.w_INDIVE = this.oParentObject.w_INDIVCF
    this.oParentObject.w_MVRIFDIC = space(10)
    this.oParentObject.w_NUMDIC = 0
    this.oParentObject.w_ANNDIC = SPACE(4)
    this.oParentObject.w_DATDIC = cp_CharToDate("  -  -  ")
    this.oParentObject.w_TIPDIC = space(1)
    this.oParentObject.w_OPEDIC = space(1)
    this.oParentObject.w_ALFDIC = Space(2)
    this.oParentObject.w_UTIDIC = 0
    this.oParentObject.w_TIPIVA = space(1)
    this.w_CHGVAL = .F.
    if this.pOper="D"
      if this.oParentObject.w_MVTIPCON = "F"
        this.oParentObject.w_MVDATCIV = this.oParentObject.w_MVDATREG
      else
        this.oParentObject.w_MVDATCIV = this.oParentObject.w_MVDATDOC
      endif
      this.oParentObject.w_MV__MESE = MONTH(this.oParentObject.w_MVDATDOC)
      this.oParentObject.w_MV__ANNO = YEAR(this.oParentObject.w_MVDATDOC)
    endif
    * --- Se Cliente/Fornitore
    if this.oParentObject.w_MVTIPCON $ "CF" AND NOT EMPTY(this.oParentObject.w_MVCODCON) 
      * --- Memorizza la sede prima che venga modificata
      this.w_LCODDESPRECEDENTE = this.oParentObject.w_MVCODDES
      * --- Propone dati Accompagnatori (Riferimento: 'CO' = Consegna)
      *     Nel caso cambio il cliente riporto sempre i valori letti (anche se sono vuoti)
      *     Nel caso cambio la data faccio solo il controllo sulla data di obsolescenza
      *     Non devo in questo caso leggere solo le destinazioni predefinite perch� potrei averla
      *     messa a mano
      *     
      *     Nel caso cambio data ma la destinazione � vuota devo cercare quella predefinita: 
      *     potrebbe essere che con la data precedente la destinazione fosse obsoleta
      if this.pOper = "C" Or Empty(this.oParentObject.w_MVCODDES)
        * --- Read from DES_DIVE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDCODVET,DDCODSPE,DDCODPOR,DDTIPRIF,DDPREDEF,DDDTOBSO,DDCODAGE,DDTIPOPE,DDCODNAZ,DDMCCODI,DDMCCODT"+;
            " from "+i_cTable+" DES_DIVE where ";
                +"DDTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                +" and DDCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                +" and DDTIPRIF = "+cp_ToStrODBC("CO");
                +" and DDPREDEF = "+cp_ToStrODBC("S");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDCODVET,DDCODSPE,DDCODPOR,DDTIPRIF,DDPREDEF,DDDTOBSO,DDCODAGE,DDTIPOPE,DDCODNAZ,DDMCCODI,DDMCCODT;
            from (i_cTable) where;
                DDTIPCON = this.oParentObject.w_MVTIPCON;
                and DDCODICE = this.oParentObject.w_MVCODCON;
                and DDTIPRIF = "CO";
                and DDPREDEF = "S";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LCODDES = NVL(cp_ToDate(_read_.DDCODDES),cp_NullValue(_read_.DDCODDES))
          this.w_LNOMDES = NVL(cp_ToDate(_read_.DDNOMDES),cp_NullValue(_read_.DDNOMDES))
          this.w_LINDDES = NVL(cp_ToDate(_read_.DDINDIRI),cp_NullValue(_read_.DDINDIRI))
          this.w_LCAPDES = NVL(cp_ToDate(_read_.DD___CAP),cp_NullValue(_read_.DD___CAP))
          this.w_LLOCDES = NVL(cp_ToDate(_read_.DDLOCALI),cp_NullValue(_read_.DDLOCALI))
          this.w_LPRODES = NVL(cp_ToDate(_read_.DDPROVIN),cp_NullValue(_read_.DDPROVIN))
          this.w_LCODVET = NVL(cp_ToDate(_read_.DDCODVET),cp_NullValue(_read_.DDCODVET))
          this.w_LCODSPE = NVL(cp_ToDate(_read_.DDCODSPE),cp_NullValue(_read_.DDCODSPE))
          this.w_LCODPOR = NVL(cp_ToDate(_read_.DDCODPOR),cp_NullValue(_read_.DDCODPOR))
          this.w_LTIPRIF = NVL(cp_ToDate(_read_.DDTIPRIF),cp_NullValue(_read_.DDTIPRIF))
          this.w_LPREDEF = NVL(cp_ToDate(_read_.DDPREDEF),cp_NullValue(_read_.DDPREDEF))
          this.w_DTOBS1 = NVL(cp_ToDate(_read_.DDDTOBSO),cp_NullValue(_read_.DDDTOBSO))
          this.w_CODAGE = NVL(cp_ToDate(_read_.DDCODAGE),cp_NullValue(_read_.DDCODAGE))
          this.oParentObject.w_MVTIPOPE = NVL(cp_ToDate(_read_.DDTIPOPE),cp_NullValue(_read_.DDTIPOPE))
          this.w_LCODNAZ = NVL(cp_ToDate(_read_.DDCODNAZ),cp_NullValue(_read_.DDCODNAZ))
          this.w_LMCALSI4 = NVL(cp_ToDate(_read_.DDMCCODI),cp_NullValue(_read_.DDMCCODI))
          this.w_LMCALST4 = NVL(cp_ToDate(_read_.DDMCCODT),cp_NullValue(_read_.DDMCCODT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        * --- Read from DES_DIVE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDCODVET,DDCODSPE,DDCODPOR,DDTIPRIF,DDPREDEF,DDDTOBSO,DDCODAGE,DDTIPOPE,DDCODNAZ,DDMCCODI,DDMCCODT"+;
            " from "+i_cTable+" DES_DIVE where ";
                +"DDTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                +" and DDCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                +" and DDCODDES = "+cp_ToStrODBC(this.oParentObject.w_MVCODDES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDCODVET,DDCODSPE,DDCODPOR,DDTIPRIF,DDPREDEF,DDDTOBSO,DDCODAGE,DDTIPOPE,DDCODNAZ,DDMCCODI,DDMCCODT;
            from (i_cTable) where;
                DDTIPCON = this.oParentObject.w_MVTIPCON;
                and DDCODICE = this.oParentObject.w_MVCODCON;
                and DDCODDES = this.oParentObject.w_MVCODDES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LCODDES = NVL(cp_ToDate(_read_.DDCODDES),cp_NullValue(_read_.DDCODDES))
          this.w_LNOMDES = NVL(cp_ToDate(_read_.DDNOMDES),cp_NullValue(_read_.DDNOMDES))
          this.w_LINDDES = NVL(cp_ToDate(_read_.DDINDIRI),cp_NullValue(_read_.DDINDIRI))
          this.w_LCAPDES = NVL(cp_ToDate(_read_.DD___CAP),cp_NullValue(_read_.DD___CAP))
          this.w_LLOCDES = NVL(cp_ToDate(_read_.DDLOCALI),cp_NullValue(_read_.DDLOCALI))
          this.w_LPRODES = NVL(cp_ToDate(_read_.DDPROVIN),cp_NullValue(_read_.DDPROVIN))
          this.w_LCODVET = NVL(cp_ToDate(_read_.DDCODVET),cp_NullValue(_read_.DDCODVET))
          this.w_LCODSPE = NVL(cp_ToDate(_read_.DDCODSPE),cp_NullValue(_read_.DDCODSPE))
          this.w_LCODPOR = NVL(cp_ToDate(_read_.DDCODPOR),cp_NullValue(_read_.DDCODPOR))
          this.w_LTIPRIF = NVL(cp_ToDate(_read_.DDTIPRIF),cp_NullValue(_read_.DDTIPRIF))
          this.w_LPREDEF = NVL(cp_ToDate(_read_.DDPREDEF),cp_NullValue(_read_.DDPREDEF))
          this.w_DTOBS1 = NVL(cp_ToDate(_read_.DDDTOBSO),cp_NullValue(_read_.DDDTOBSO))
          this.w_CODAGE = NVL(cp_ToDate(_read_.DDCODAGE),cp_NullValue(_read_.DDCODAGE))
          this.oParentObject.w_MVTIPOPE = NVL(cp_ToDate(_read_.DDTIPOPE),cp_NullValue(_read_.DDTIPOPE))
          this.w_LCODNAZ = NVL(cp_ToDate(_read_.DDCODNAZ),cp_NullValue(_read_.DDCODNAZ))
          this.w_LMCALSI4 = NVL(cp_ToDate(_read_.DDMCCODI),cp_NullValue(_read_.DDMCCODI))
          this.w_LMCALST4 = NVL(cp_ToDate(_read_.DDMCCODT),cp_NullValue(_read_.DDMCCODT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- VENGONO AGGIORNATI SEMPRE TUTTI I DATI CHE DIPENDONO DALLA SEDE DI CONSEGNA
      *     ESCLUSO VETTORE, PORTO E SPEDIZIONE, GESTITI NELLA SEGUENTE ISTR. IF
      if i_Rows=0 OR (this.w_DTOBS1<=this.oParentObject.w_MVDATDOC AND NOT EMPTY(this.w_DTOBS1))
        * --- Se non trovato dato valido Azzera tutto
        this.oParentObject.w_MVCODDES = SPACE(5)
        this.oParentObject.w_NOMDES = SPACE(40)
        this.oParentObject.w_INDDES = SPACE(35)
        this.oParentObject.w_CAPDES = SPACE(8)
        this.oParentObject.w_LOCDES = SPACE(30)
        this.oParentObject.w_PRODES = SPACE(2)
        this.oParentObject.w_TIPRIF = SPACE(2)
        this.oParentObject.w_PREDEF = SPACE(1)
        this.oParentObject.w_CODNAZ1 = SPACE( 3 )
        this.oParentObject.w_MCALST4 = SPACE(5)
        this.oParentObject.w_MCALSI4 = SPACE(5)
      else
        * --- Se ho trovato qualcosa...
        if Not Empty(this.w_LCODDES)
          this.oParentObject.w_MVCODDES = this.w_LCODDES
          this.oParentObject.w_NOMDES = this.w_LNOMDES
          this.oParentObject.w_INDDES = this.w_LINDDES
          this.oParentObject.w_CAPDES = this.w_LCAPDES
          this.oParentObject.w_LOCDES = this.w_LLOCDES
          this.oParentObject.w_PRODES = this.w_LPRODES
          this.oParentObject.w_TIPRIF = this.w_LTIPRIF
          this.oParentObject.w_PREDEF = this.w_LPREDEF
          this.oParentObject.w_CODNAZ1 = this.w_LCODNAZ
          this.oParentObject.w_MCALST4 = this.w_LMCALST4
          this.oParentObject.w_MCALSI4 = this.w_LMCALSI4
        endif
      endif
    endif
    if this.oParentObject.w_MVTIPCON $ "CF" AND NOT EMPTY(this.oParentObject.w_XCONORN)
      * --- Controllo contenzioso cliente: devo farlo nel batch perch� la variabile FLGCON 
      *     � letta da XCONORN
      if this.oParentObject.w_FLGCON="S" And this.w_PADRE.cFunction="Load"
        ah_ErrorMsg("Attivato blocco contenzioso")
        this.oParentObject.w_MVCODCON = Space(15)
        this.oParentObject.w_XCONORN = Space(15)
        this.oParentObject.o_MVCODCON = SPACE(15)
        this.oParentObject.w_DESCLF = Space(40)
        i_retcode = 'stop'
        return
      endif
      * --- Controlli Listino/VAluta
      if this.pOper="C"
        this.oParentObject.w_VALCLF = IIF(this.oParentObject.cFunction<>"Load", this.oParentObject.w_MVCODVAL, this.oParentObject.w_VALCLF)
        this.oParentObject.w_MVCODVAL = IIF(EMPTY(this.oParentObject.w_VALCLF), g_PERVAL, this.oParentObject.w_VALCLF)
        if this.oParentObject.w_MVCODVAL<>this.oParentObject.o_MVCODVAL
          this.w_CHGVAL = .T.
          this.oParentObject.o_MVCODVAL = this.oParentObject.w_MVCODVAL
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VADESVAL,VASIMVAL,VADECUNI,VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VACAOVAL"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.oParentObject.w_MVCODVAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VADESVAL,VASIMVAL,VADECUNI,VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VACAOVAL;
              from (i_cTable) where;
                  VACODVAL = this.oParentObject.w_MVCODVAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESAPP = NVL(cp_ToDate(_read_.VADESVAL),cp_NullValue(_read_.VADESVAL))
            this.oParentObject.w_SIMVAL = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
            this.oParentObject.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
            this.oParentObject.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
            this.oParentObject.w_BOLESE = NVL(cp_ToDate(_read_.VABOLESE),cp_NullValue(_read_.VABOLESE))
            this.oParentObject.w_BOLSUP = NVL(cp_ToDate(_read_.VABOLSUP),cp_NullValue(_read_.VABOLSUP))
            this.oParentObject.w_BOLCAM = NVL(cp_ToDate(_read_.VABOLCAM),cp_NullValue(_read_.VABOLCAM))
            this.oParentObject.w_BOLARR = NVL(cp_ToDate(_read_.VABOLARR),cp_NullValue(_read_.VABOLARR))
            this.oParentObject.w_BOLMIN = NVL(cp_ToDate(_read_.VABOLMIM),cp_NullValue(_read_.VABOLMIM))
            this.oParentObject.w_CAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if lower(this.w_PADRE.class)="tgsac_mdv"
            this.oParentObject.w_CAMBGIOR = GETCAM(this.oParentObject.w_MVCODVAL, iif(this.oParentObject.w_AZDATDOC="S" and Not Empty(this.oParentObject.w_MVDATDOC) ,this.oParentObject.w_MVDATDOC,this.oParentObject.w_MVDATREG), 0)
          else
            this.oParentObject.w_CAMBGIOR = GETCAM(this.oParentObject.w_MVCODVAL, this.oParentObject.w_MVDATDOC, 0)
          endif
          this.oParentObject.w_MVCAOVAL = this.oParentObject.w_CAMBGIOR
          this.oParentObject.w_CALCPICT = DEFPIC(this.oParentObject.w_DECTOT)
          this.oParentObject.w_CALCPICU = DEFPIU(this.oParentObject.w_DECUNI)
          this.oParentObject.w_CALCPICP = DEFPIP(this.oParentObject.w_DECTOP)
        endif
        this.oParentObject.w_MVTCOLIS = IIF(EMPTY(this.oParentObject.w_CLFLIS), IIF((EMPTY(this.oParentObject.w_DOCLIS) and IsAlt()),this.oParentObject.w_CODLIS,this.oParentObject.w_DOCLIS), this.oParentObject.w_CLFLIS)
        * --- Read from LISTINI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LISTINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LSDESLIS,LSIVALIS,LSVALLIS,LSQUANTI,LSDTINVA,LSDTOBSO,LSFLSCON"+;
            " from "+i_cTable+" LISTINI where ";
                +"LSCODLIS = "+cp_ToStrODBC(this.oParentObject.w_MVTCOLIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LSDESLIS,LSIVALIS,LSVALLIS,LSQUANTI,LSDTINVA,LSDTOBSO,LSFLSCON;
            from (i_cTable) where;
                LSCODLIS = this.oParentObject.w_MVTCOLIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESLIS = NVL(cp_ToDate(_read_.LSDESLIS),cp_NullValue(_read_.LSDESLIS))
          this.oParentObject.w_IVALIS = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
          this.oParentObject.w_VALLIS = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
          this.oParentObject.w_QUALIS = NVL(cp_ToDate(_read_.LSQUANTI),cp_NullValue(_read_.LSQUANTI))
          this.oParentObject.w_INILIS = NVL(cp_ToDate(_read_.LSDTINVA),cp_NullValue(_read_.LSDTINVA))
          this.oParentObject.w_FINLIS = NVL(cp_ToDate(_read_.LSDTOBSO),cp_NullValue(_read_.LSDTOBSO))
          this.oParentObject.w_SCOLIS = NVL(cp_ToDate(_read_.LSFLSCON),cp_NullValue(_read_.LSFLSCON))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if NOT CHKLISD(this.oParentObject.w_MVTCOLIS,this.oParentObject.w_IVALIS,this.oParentObject.w_VALLIS,this.oParentObject.w_INILIS,this.oParentObject.w_FINLIS,this.oParentObject.w_MVFLSCOR, this.oParentObject.w_MVCODVAL, this.oParentObject.w_MVDATDOC) 
        this.oParentObject.w_DESLIS = SPACE(40)
        this.oParentObject.w_VALLIS = SPACE(3)
        this.oParentObject.w_IVALIS = space(1)
        this.oParentObject.w_QUALIS = space(1)
        this.oParentObject.w_MVTCOLIS = SPACE(5)
        this.oParentObject.w_FINLIS = cp_CharToDate("  -  -  ")
        this.oParentObject.w_INILIS = cp_CharToDate("  -  -  ")
      endif
      * --- Verifica se esiste una Dichiarazione di Intento Valida
      this.w_OK = .F.
      this.w_NDIC = 0
      this.w_TIPOPE = "X"
      this.w_ADIC = SPACE(4)
      this.w_IMPDIC = 0
      this.w_DDIC = cp_CharToDate("  -  -  ")
      this.w_IMPUTI = 0
      this.w_TDIC = "X"
      this.w_CODIVE = SPACE(5)
      this.w_TIVA = " "
      this.w_DATINI = cp_CharToDate("  -  -  ")
      this.w_RIFDIC = SPACE(10)
      this.w_DATFIN = cp_CharToDate("  -  -  ")
      * --- Lettura lettera di intento valida
      DECLARE ARRDIC (14,1)
      * --- Azzero l'Array che verr� riempito dalla Funzione
      ARRDIC(1)=0
      this.w_OK_LET = CAL_LETT(this.oParentObject.w_MVDATDOC,this.oParentObject.w_MVTIPCON,this.oParentObject.w_XCONORN, @ArrDic, SPACE(10),this.oParentObject.w_MVCODDES)
      if this.w_OK_LET
        * --- Parametri
        *     pDatRif : Data di Riferimento per filtro su Lettere di intento
        *     pTipCon : Tipo Conto : 'C' Clienti, 'F' : Fornitori
        *     pCodCon : Codice Cliente/Fornitore
        *     pArrDic : Array passato per riferimento: conterr� anche i dati letti dalla lettera di intento
        *     
        *     pArrDic[ 1 ]   = Numero Dichiarazione
        *     pArrDic[ 2 ]   = Tipo Operazione
        *     pArrDic[ 3 ]   = Anno Dichiarazione
        *     pArrDic[ 4 ]   = Importo Dichiarazione
        *     pArrDic[ 5 ]   = Data Dichiarazione
        *     pArrDic[ 6 ]   = Importo Utilizzato
        *     pArrDic[ 7 ]   = Tipo conto: Cliente/Fornitore
        *     pArrDic[ 8 ]   = Codice Iva Agevolata
        *     pArrDic[ 9 ]   = Tipo Iva (Agevolata o senza)
        *     pArrDic[ 10 ] = Data Inizio Validit�
        *     pArrDic[ 11 ] = Codice Dichiarazione (Se a clienti = codice Cliente, Se Fornitori= codice progressivo)
        *     pArrDic[ 12 ] = Data Obsolescenza
        this.w_NDIC = ArrDic(1)
        this.w_TIPOPE = ArrDic(2)
        this.w_ADIC = ArrDic(3)
        this.w_IMPDIC = ArrDic(4)
        this.w_DDIC = ArrDic(5)
        this.w_IMPUTI = ArrDic(6)
        this.w_TDIC = ArrDic(7)
        this.w_CODIVE = ArrDic(8)
        this.w_TIVA = ArrDic(9)
        this.w_DATINI = ArrDic(10)
        this.w_RIFDIC = ArrDic(11)
        this.w_DATFIN = ArrDic(12)
        this.oParentObject.w_ALFDIC = ArrDic(13)
      endif
      * --- Se compreso nelle date di validita'
      if (this.oParentObject.w_MVDATDOC>=this.w_DATINI OR EMPTY (this.w_DATINI ) ) AND (this.oParentObject.w_MVDATDOC<=this.w_DATFIN OR EMPTY (this.w_DATFIN ) )
        if NOT EMPTY(this.oParentObject.w_MVCODIVE)
          if NOT EMPTY(this.w_CODIVE)
            * --- Sono Presenti entrambi i Codici Esenzione
            ah_ErrorMsg("Sono presenti sia il codice IVA di esenzione: (%1)%0sia il codice IVA relativo alla lettera di intenti: (%2)%0Ai fini del documento verr� considerato il solo codice IVA esenzione",,"", this.oParentObject.w_MVCODIVE, this.w_CODIVE, )
          endif
        else
          * --- Prende la Lettera di Intenti solo se Cli/For non esente
          do case
            case this.w_TIPOPE = "I"
              * --- Importo Definito OK Se Importo Dichiarato>Importo Utilizzato
              if this.w_IMPDIC>this.w_IMPUTI
                this.w_OK = .T.
              else
                ah_ErrorMsg("Importo disponibile della dichiarazione di esenzione esaurito%0Rif. n.: %1/%2 del %3",,"", ALLTRIM(STR(this.w_NDIC))+"/"+this.w_ADIC, this.oParentObject.w_ALFDIC ,DTOC(this.w_DDIC) )
              endif
            case this.w_TIPOPE = "O"
              * --- Operazione Specifica
              this.w_OK = .T.
              ah_ErrorMsg("Verificare dichiarazione di intento! (Operazione specifica)")
            case this.w_TIPOPE = "D"
              * --- Periodo Definito
              this.w_OK = .T.
          endcase
          * --- Se Ok riporta i dati dell'Esenzione
          this.oParentObject.w_MVCODIVE = this.w_CODIVE
          this.oParentObject.w_MVRIFDIC = this.w_RIFDIC
          this.oParentObject.w_NUMDIC = this.w_NDIC
          this.oParentObject.w_ANNDIC = this.w_ADIC
          this.oParentObject.w_DATDIC = this.w_DDIC
          this.oParentObject.w_TIPDIC = this.w_TDIC
          this.oParentObject.w_OPEDIC = this.w_TIPOPE
          this.oParentObject.w_UTIDIC = this.w_IMPUTI
          this.oParentObject.w_TIPIVA = this.w_TIVA
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVPERIVA,IVBOLIVA,IVPERIND"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_MVCODIVE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVPERIVA,IVBOLIVA,IVPERIND;
              from (i_cTable) where;
                  IVCODIVA = this.oParentObject.w_MVCODIVE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_PERIVE = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
            this.oParentObject.w_BOLIVE = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
            this.oParentObject.w_INDIVE = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      endif
    endif
    if this.oParentObject.w_MVTIPCON $ "CF" AND NOT EMPTY(this.oParentObject.w_MVCODCON) 
      * --- Rileggo l'agente dal Cliente o dalla Destinazione
      *     Alla fine controllo sempre l'obsolescenza
      this.w_DTOBS1 = cp_CharToDate("  -  -  ")
      this.w_ANCODAG = SPACE(5)
      if this.oParentObject.w_MVTIPCON="C" AND this.pOPER="C"
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODAG1"+;
            " from "+i_cTable+" CONTI where ";
                +"ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                +" and ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODAG1;
            from (i_cTable) where;
                ANCODICE = this.oParentObject.w_MVCODCON;
                and ANTIPCON = this.oParentObject.w_MVTIPCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANCODAG = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_MVCODAGE = this.w_ANCODAG
      endif
      this.oParentObject.NotifyEvent( "w_MVCODDES Changed" )
      * --- IL VETTORE, IL PORTO E LA SPEDIZIONE, IN QUANTO MODIFICABILI ANCHE
      *     DALL'UTENTE, VENGONO AGGIORNATI SOLO SE LA SEDE � VARIATA
      *     E LA SEDE PRECEDENTE ERA VALORIZZATA ( SE PRIMA ERA VUOTA, PRESUMIBILMENTE L'UTENTE HA
      *     INSERITO I DATI MANUALMENTE), OPPURE SE I VALORI CORRENTI SONO VUOTI E QUELLI APPENA LETTI SONO NON VUOTI
      if (this.w_LCODDESPRECEDENTE <> this.oParentObject.w_MVCODDES OR this.w_LCLIORNUPD ) AND !EMPTY( this.w_LCODDESPRECEDENTE )
        this.oParentObject.w_MVCODVET = this.w_LCODVET
        this.oParentObject.w_MVCODSPE = this.w_LCODSPE
        this.oParentObject.w_MVCODPOR = this.w_LCODPOR
      else
        if EMPTY( this.oParentObject.w_MVCODVET )
          this.oParentObject.w_MVCODVET = this.w_LCODVET
        endif
        if EMPTY( this.oParentObject.w_MVCODSPE )
          this.oParentObject.w_MVCODSPE = this.w_LCODSPE
        endif
        if EMPTY( this.oParentObject.w_MVCODPOR )
          this.oParentObject.w_MVCODPOR = this.w_LCODPOR
        endif
      endif
      * --- AGGIORNA LE DESCRIZIONI
      this.w_OBJCTRL = this.oParentObject.GETCTRL( "w_MVCODVET" )
      this.w_OBJCTRL.CHECK()     
      this.w_OBJCTRL = this.oParentObject.GETCTRL( "w_MVCODSPE" )
      this.w_OBJCTRL.CHECK()     
      this.w_OBJCTRL = this.oParentObject.GETCTRL( "w_MVCODPOR" )
      this.w_OBJCTRL.CHECK()     
      this.w_OBJCTRL = this.oParentObject.GETCTRL( "w_MVCODBAN" )
      this.w_OBJCTRL.CHECK()     
      this.w_OBJCTRL = this.oParentObject.GETCTRL( "w_MVNUMCOR" )
      this.w_OBJCTRL.CHECK()     
      * --- Se cambiando la data documento la ns banca diventa obsoleta la ricalcolo
      if this.pOPER="C" or (this.oParentObject.w_DTOBSOBC>this.oParentObject.w_MVDATDOC AND NOT EMPTY(this.oParentObject.w_DTOBSOBC) )
        if Vartype(this.oParentObject.w_CODBA2)="C"
          setvaluelinked("M",this.oparentobject,"w_MVCODBA2",IIF(NOT EMPTY(this.oParentObject.w_CODBA2) OR NOT ISALT(), this.oParentObject.w_CODBA2, NsBancaPref() ))
        else
          this.w_OBJCTRL = this.oParentObject.GETCTRL( "w_MVCODBA2" )
          this.w_OBJCTRL.CHECK()     
        endif
      endif
      * --- Se ho trovato un agente...
      if this.pOPER="C"
        if this.oParentObject.w_MVTIPCON = "C" 
          this.oParentObject.w_MVCODAG2 = space(5)
          this.oParentObject.w_AGEPRO = space(5)
          this.oParentObject.w_AGSCOPAG = " "
        endif
        if Not Empty(this.oParentObject.w_MVCODAGE)
          * --- Read from AGENTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AGENTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AGDTOBSO,AGCZOAGE,AGCATPRO,AGSCOPAG"+;
              " from "+i_cTable+" AGENTI where ";
                  +"AGCODAGE = "+cp_ToStrODBC(this.oParentObject.w_MVCODAGE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AGDTOBSO,AGCZOAGE,AGCATPRO,AGSCOPAG;
              from (i_cTable) where;
                  AGCODAGE = this.oParentObject.w_MVCODAGE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_AGDTOBSO = NVL(cp_ToDate(_read_.AGDTOBSO),cp_NullValue(_read_.AGDTOBSO))
            this.oParentObject.w_MVCODAG2 = NVL(cp_ToDate(_read_.AGCZOAGE),cp_NullValue(_read_.AGCZOAGE))
            this.oParentObject.w_AGEPRO = NVL(cp_ToDate(_read_.AGCATPRO),cp_NullValue(_read_.AGCATPRO))
            this.oParentObject.w_AGSCOPAG = NVL(cp_ToDate(_read_.AGSCOPAG),cp_NullValue(_read_.AGSCOPAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      endif
      * --- Controllo data obsolescenza Agente 
      *     AGDTOBSO � letto dall'agente indicato sul Cliente oppure su quello della destinazione
      if this.w_AGDTOBSO<=this.oParentObject.w_MVDATDOC AND NOT EMPTY(this.w_AGDTOBSO)
        this.oParentObject.w_MVCODAGE = space(5)
        this.oParentObject.w_MVCODAG2 = space(5)
        this.oParentObject.w_AGEPRO = space(5)
        this.oParentObject.w_AGSCOPAG = " "
      endif
    endif
    if this.oParentObject.w_MVTIPCON = "C" AND NOT EMPTY(this.oParentObject.w_XCONORN) 
      * --- Controllo se cliente con rischio non ancora calcolato
      * --- Read from SIT_FIDI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2],.t.,this.SIT_FIDI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" SIT_FIDI where ";
              +"FICODCLI = "+cp_ToStrODBC(this.oParentObject.w_XCONORN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              FICODCLI = this.oParentObject.w_XCONORN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows<>0
        this.oParentObject.w_NEWUSER = "N"
      else
        this.oParentObject.w_NEWUSER = "S"
      endif
      * --- Assegna Codice Pagamento (solo se cambio il Cliente o Cliente in Moratoria)
      if this.oParentObject.w_DATMOR>=this.oParentObject.w_MVDATDOC AND NOT EMPTY(this.oParentObject.w_PAGMOR) OR this.pOper="C"
        this.oParentObject.w_MVCODPAG = IIF(this.oParentObject.w_DATMOR>=this.oParentObject.w_MVDATDOC AND NOT EMPTY(this.oParentObject.w_PAGMOR), this.oParentObject.w_PAGMOR, IIF(this.oParentObject.w_MVCLADOC="RF", g_PAGRIC, this.oParentObject.w_CLFPAG))
        * --- Read from PAG_AMEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2],.t.,this.PAG_AMEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PADESCRI"+cp_TransInsFldName("PADESCRI")+",PAVALINC,PASPEINC,PAVALIN2,PASPEIN2,PASCONTO,PADTOBSO,PAINCASS,PASALDO"+;
            " from "+i_cTable+" PAG_AMEN where ";
                +"PACODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODPAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PADESCRI,PADESCRI,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2,PASCONTO,PADTOBSO,PAINCASS,PASALDO;
            from (i_cTable) where;
                PACODICE = this.oParentObject.w_MVCODPAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESPAG = NVL(cp_ToDate(cp_TransLoadField('_read_.PADESCRI')),cp_NullValue(cp_TransLoadField('_read_.PADESCRI')))
          this.oParentObject.w_VALINC = NVL(cp_ToDate(_read_.PAVALINC),cp_NullValue(_read_.PAVALINC))
          this.oParentObject.w_SPEINC = NVL(cp_ToDate(_read_.PASPEINC),cp_NullValue(_read_.PASPEINC))
          this.oParentObject.w_VALIN2 = NVL(cp_ToDate(_read_.PAVALIN2),cp_NullValue(_read_.PAVALIN2))
          this.oParentObject.w_SPEIN2 = NVL(cp_ToDate(_read_.PASPEIN2),cp_NullValue(_read_.PASPEIN2))
          this.oParentObject.w_MVSCOPAG = NVL(cp_ToDate(_read_.PASCONTO),cp_NullValue(_read_.PASCONTO))
          this.oParentObject.w_DTOBSO = NVL(cp_ToDate(_read_.PADTOBSO),cp_NullValue(_read_.PADTOBSO))
          this.oParentObject.w_PAINCASS = NVL(cp_ToDate(_read_.PAINCASS),cp_NullValue(_read_.PAINCASS))
          this.oParentObject.w_PASALDO = NVL(cp_ToDate(_read_.PASALDO),cp_NullValue(_read_.PASALDO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.oParentObject.w_DATMOR>=this.oParentObject.w_MVDATDOC AND NOT EMPTY(this.oParentObject.w_PAGMOR)
          ah_ErrorMsg("Applico pagamento alternativo: %1%0(%2)",,"", this.oParentObject.w_PAGMOR, ALLTRIM(this.oParentObject.w_DESPAG) )
        endif
        if NOT EMPTY(this.oParentObject.w_DTOBSO) AND this.oParentObject.w_DTOBSO=<this.oParentObject.w_MVDATDOC
          ah_ErrorMsg("Codice pagamento obsoleto")
          this.oParentObject.w_MVCODPAG = SPACE(5)
        endif
      endif
      if g_PERFID="S" AND this.oParentObject.w_FLCRIS="S" AND this.oParentObject.w_FLFIDO="S" AND this.pOper="C"
        * --- Controllo del Rischio
        this.w_FIDRES = this.oParentObject.w_MAXFID
        this.w_FID1 = 0
        this.w_FID2 = 0
        this.w_FID3 = 0
        this.w_FID4 = 0
        this.w_FID5 = 0
        this.w_FID6 = 0
        this.w_FIDD = cp_CharToDate("  -  -  ")
        * --- Read from SIT_FIDI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2],.t.,this.SIT_FIDI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "FIIMPPAP,FIIMPESC,FIIMPESO,FIIMPORD,FIIMPDDT,FIIMPFAT,FIDATELA"+;
            " from "+i_cTable+" SIT_FIDI where ";
                +"FICODCLI = "+cp_ToStrODBC(this.oParentObject.w_XCONORN);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            FIIMPPAP,FIIMPESC,FIIMPESO,FIIMPORD,FIIMPDDT,FIIMPFAT,FIDATELA;
            from (i_cTable) where;
                FICODCLI = this.oParentObject.w_XCONORN;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FID1 = NVL(cp_ToDate(_read_.FIIMPPAP),cp_NullValue(_read_.FIIMPPAP))
          this.w_FID2 = NVL(cp_ToDate(_read_.FIIMPESC),cp_NullValue(_read_.FIIMPESC))
          this.w_FID3 = NVL(cp_ToDate(_read_.FIIMPESO),cp_NullValue(_read_.FIIMPESO))
          this.w_FID4 = NVL(cp_ToDate(_read_.FIIMPORD),cp_NullValue(_read_.FIIMPORD))
          this.w_FID5 = NVL(cp_ToDate(_read_.FIIMPDDT),cp_NullValue(_read_.FIIMPDDT))
          this.w_FID6 = NVL(cp_ToDate(_read_.FIIMPFAT),cp_NullValue(_read_.FIIMPFAT))
          this.w_FIDD = NVL(cp_ToDate(_read_.FIDATELA),cp_NullValue(_read_.FIDATELA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          this.w_FIDRES = this.w_FIDRES - (this.w_FID1+this.w_FID2+this.w_FID3+this.w_FID4+this.w_FID5+this.w_FID6)
        endif
        if this.w_FIDRES<=0
          ah_ErrorMsg("Fido cliente esaurito: (%1 - al %2)%0Potrebbe essere impossibile confermare il documento",,"", ALLTRIM(STR(this.w_FIDRES,18+this.oParentObject.w_DECTOP,this.oParentObject.w_DECTOP)), DTOC(this.w_FIDD))
        endif
      endif
    else
      if this.oParentObject.w_MVTIPCON="F" AND NOT EMPTY(this.oParentObject.w_MVCODCON) 
        * --- Assegna Codice Pagamento (solo se cambio il Fornitore)
        if this.pOper="C"
          this.oParentObject.w_MVCODPAG = this.oParentObject.w_CLFPAG
          * --- Read from PAG_AMEN
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PAG_AMEN_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PAG_AMEN_idx,2],.t.,this.PAG_AMEN_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PADESCRI"+cp_TransInsFldName("PADESCRI")+",PAVALINC,PASPEINC,PAVALIN2,PASPEIN2,PASCONTO,PADTOBSO,PAINCASS,PASALDO"+;
              " from "+i_cTable+" PAG_AMEN where ";
                  +"PACODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODPAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PADESCRI,PADESCRI,PAVALINC,PASPEINC,PAVALIN2,PASPEIN2,PASCONTO,PADTOBSO,PAINCASS,PASALDO;
              from (i_cTable) where;
                  PACODICE = this.oParentObject.w_MVCODPAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESPAG = NVL(cp_ToDate(cp_TransLoadField('_read_.PADESCRI')),cp_NullValue(cp_TransLoadField('_read_.PADESCRI')))
            this.oParentObject.w_VALINC = NVL(cp_ToDate(_read_.PAVALINC),cp_NullValue(_read_.PAVALINC))
            this.oParentObject.w_SPEINC = NVL(cp_ToDate(_read_.PASPEINC),cp_NullValue(_read_.PASPEINC))
            this.oParentObject.w_VALIN2 = NVL(cp_ToDate(_read_.PAVALIN2),cp_NullValue(_read_.PAVALIN2))
            this.oParentObject.w_SPEIN2 = NVL(cp_ToDate(_read_.PASPEIN2),cp_NullValue(_read_.PASPEIN2))
            this.oParentObject.w_MVSCOPAG = NVL(cp_ToDate(_read_.PASCONTO),cp_NullValue(_read_.PASCONTO))
            this.oParentObject.w_DTOBSO = NVL(cp_ToDate(_read_.PADTOBSO),cp_NullValue(_read_.PADTOBSO))
            this.oParentObject.w_PAINCASS = NVL(cp_ToDate(_read_.PAINCASS),cp_NullValue(_read_.PAINCASS))
            this.oParentObject.w_PASALDO = NVL(cp_ToDate(_read_.PASALDO),cp_NullValue(_read_.PASALDO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT EMPTY(this.oParentObject.w_DTOBSO) AND this.oParentObject.w_DTOBSO=<this.oParentObject.w_MVDATDOC
            ah_ErrorMsg("Codice pagamento obsoleto")
            this.oParentObject.w_MVCODPAG = SPACE(5)
          endif
        endif
      endif
    endif
    this.oParentObject.w_MVTCONTR = SPACE(15)
    * --- Solo se presente gestione contratti, � presente l'intestatario/per conto di 
    *     e se documento di acquisto (MVTIPCON = 'F') deve essere installato il modulo acquisti.
    *     Nel caso in cui non ci sia il modulo acquisti e si lanciano i documenti di acquisto dal modulo
    *     vendite non deve gestire i contratti
    if g_GESCON="S" AND NOT EMPTY(this.oParentObject.w_XCONORN) AND this.oParentObject.w_MVTIPCON = "C" Or (this.oParentObject.w_MVTIPCON = "F" And g_ACQU = "S")
      * --- Legge contratto associato
      * --- Select from GSVE_BES
      do vq_exec with 'GSVE_BES',this,'_Curs_GSVE_BES','',.f.,.t.
      if used('_Curs_GSVE_BES')
        select _Curs_GSVE_BES
        locate for 1=1
        do while not(eof())
        if NOT EMPTY(NVL(CONUMERO," ")) And ( this.oParentObject.w_MVFLSCOR<>"S" OR NVL(_Curs_GSVE_BES.COIVALIS," ")="L" )
          * --- Ordinato per data piu' recente (prende l'ultimo)
          this.oParentObject.w_MVTCONTR = _Curs_GSVE_BES.CONUMERO
        endif
          select _Curs_GSVE_BES
          continue
        enddo
        use
      endif
    endif
    * --- Se esiste il Cliente, Ricalcola i Prezzi
    if this.oParentObject.w_MVTIPCON $ "CF" AND NOT EMPTY(this.oParentObject.w_XCONORN)
      this.w_PADRE = this.oParentObject
      * --- Forza i Calcoli prima di rielaborare le Righe Documento
      this.w_PADRE.mCalc(.T.)     
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Riaggiorna i Prezzi e il Listino sulle Righe
    * --- Conto le righe piene sul documento...
    this.w_RECTRS = this.w_PADRE.NumRow()
    if this.w_RECTRS>0
      if g_FLCESC = "S"
        * --- Codifica Cliente/Fornitore Esclusiva attiva in Azienda
        ah_ErrorMsg("Attiva gestione codici clienti/fornitori esclusivi%0Sulle righe potrebbero essere presenti codici incongruenti con questo intestatario","i")
      endif
      this.w_PADRE.MarkPos()     
      * --- istanzio oggetto per mess. incrementali
      this.w_oMESS=createobject("ah_message")
      this.w_oMESS.AddMsgPartNL("Aggiorno anche i prezzi sulle righe?")     
      if g_XCONDI="S" And (Not Empty(this.oParentObject.w_MVCODORN) Or Not Empty(this.oParentObject.w_MVCODCON))
        if Not Empty(this.oParentObject.w_MVCODORN)
          this.w_oMESS.AddMsgPartNL("Prezzi calcolati in base al per conto di..")     
        else
          this.w_oMESS.AddMsgPartNL("Prezzi calcolati in base all'intestatario")     
        endif
      endif
      if this.oParentObject.w_FLQRIO="S"
        this.w_oMESS.AddMsgPartNL("Ricalcolo qt� da lotto di riordino attivo: verr� ricalcolata anche la quantit�")     
      endif
      if this.oParentObject.w_FLPREV="S"
        this.w_oMESS.AddMsgPartNL("Ricalcolo data prevista evasione attivo: verr� ricalcolata anche la data di prevista evasione")     
      endif
      if this.oParentObject.w_NOPRSC <>"S" AND NOT ISALT()
        this.w_AGGRIG = this.w_oMESS.ah_YesNo()
      else
        * --- Se la causale documento non gestisce i prezzi/sconti, non bisogna eseguire il ricalcolo dei prezzi.
        this.w_AGGRIG = .F.
      endif
      this.w_PADRE.FirstRow()     
      do while Not this.w_PADRE.Eof_Trs()
        this.w_PADRE.SetRow()     
        if this.w_PADRE.FullRow()
          this.oParentObject.w_MVVALMAG = CAVALMAG(this.oParentObject.w_MVFLSCOR, this.oParentObject.w_MVVALRIG, this.oParentObject.w_MVIMPSCO, this.oParentObject.w_MVIMPACC, this.oParentObject.w_PERIVA, this.oParentObject.w_DECTOT, this.oParentObject.w_MVCODIVE, this.oParentObject.w_PERIVE )
          this.oParentObject.w_MVIMPNAZ = CAIMPNAZ(this.oParentObject.w_MVFLVEAC, this.oParentObject.w_MVVALMAG, this.oParentObject.w_MVCAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVVALNAZ, this.oParentObject.w_MVCODVAL, this.oParentObject.w_MVCODIVE, this.oParentObject.w_PERIVE, this.oParentObject.w_INDIVE, this.oParentObject.w_PERIVA, this.oParentObject.w_INDIVA )
          this.oParentObject.w_MVVALULT = IIF(this.oParentObject.w_MVQTAUM1=0, 0, cp_ROUND(this.oParentObject.w_MVIMPNAZ/this.oParentObject.w_MVQTAUM1, this.oParentObject.w_DECTOU))
          this.oParentObject.w_MVIMPCOM = CAIMPCOM( IIF(Empty(this.oParentObject.w_MVCODCOM),"S", iif(this.oParentObject.w_MVFLEVAS="S" And Not Empty( this.oParentObject.w_MVFLORCO ),"S","N") ), this.oParentObject.w_MVVALNAZ, this.oParentObject.w_COCODVAL, this.oParentObject.w_MVIMPNAZ, this.oParentObject.w_CAONAZ, IIF(EMPTY(this.oParentObject.w_MVDATDOC), this.oParentObject.w_MVDATREG, this.oParentObject.w_MVDATDOC), this.oParentObject.w_DECCOM )
          this.oParentObject.w_VISNAZ = cp_ROUND(this.oParentObject.w_MVIMPNAZ, this.oParentObject.w_DECTOP)
          this.oParentObject.w_MVCODLIS = this.oParentObject.w_MVTCOLIS
          this.w_OKCONTRA = .T.
          if NOT EMPTY(this.oParentObject.w_MVCONTRA)
            * --- Se contratto gi� presente controllo la sua effettiva validit� altrimenti lo sbianco (anche nel caso cambi il cliente ma il contratto sia cmq valido per entrambi)
            * --- Read from CON_TRAM
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CON_TRAM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CON_TRAM_idx,2],.t.,this.CON_TRAM_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS"+;
                " from "+i_cTable+" CON_TRAM where ";
                    +"CONUMERO = "+cp_ToStrODBC(this.oParentObject.w_MVCONTRA);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                COTIPCLF,COCODCLF,COCATCOM,CODATINI,CODATFIN,COCODVAL,COIVALIS;
                from (i_cTable) where;
                    CONUMERO = this.oParentObject.w_MVCONTRA;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CT = NVL(cp_ToDate(_read_.COTIPCLF),cp_NullValue(_read_.COTIPCLF))
              this.w_CC = NVL(cp_ToDate(_read_.COCODCLF),cp_NullValue(_read_.COCODCLF))
              this.w_CM = NVL(cp_ToDate(_read_.COCATCOM),cp_NullValue(_read_.COCATCOM))
              this.w_CI = NVL(cp_ToDate(_read_.CODATINI),cp_NullValue(_read_.CODATINI))
              this.w_CF = NVL(cp_ToDate(_read_.CODATFIN),cp_NullValue(_read_.CODATFIN))
              this.w_CV = NVL(cp_ToDate(_read_.COCODVAL),cp_NullValue(_read_.COCODVAL))
              this.w_CL = NVL(cp_ToDate(_read_.COIVALIS),cp_NullValue(_read_.COIVALIS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_OKCONTRA = CHKCONTR(this.oParentObject.w_MVCONTRA,this.oParentObject.w_MVTIPCON,this.oParentObject.w_XCONORN,this.oParentObject.w_CATCOM,this.oParentObject.w_MVFLSCOR,this.oParentObject.w_MVCODVAL,this.oParentObject.w_MVDATREG,this.w_CT,this.w_CC,this.w_CM,this.w_CV,this.w_CI,this.w_CF,this.w_CL,.T.)
            this.oParentObject.w_MVCONTRA = IIF(this.w_OKCONTRA and this.pOper<>"C", this.oParentObject.w_MVCONTRA,SPACE(15))
          endif
          if this.w_PADRE.cFunction = "Edit"
            * --- In Modifica documento LIPREZZO � uguale a 0. Lanciando la mCalc() in uscita di questo batch
            *     verrebbe azzerato il prezzo di riga. 
            *     Per evitare questo riassegno a LIPREZZO il Prezzo di riga.
            this.oParentObject.w_LIPREZZO = this.oParentObject.w_MVPREZZO
          endif
          * --- Per Impedire che la mCalc lanciata dalla NotifyEvent('Calcperc1') o ('Ricalcola') faccia eseguire i ricalcoli
          *     Per esempio cambio dell'unit� di misura sulla riga se movimentata la seconda nel documento di origine che sto importando
          this.oParentObject.o_MVCODLIS = this.oParentObject.w_MVTCOLIS
          this.oParentObject.o_MVCODICE = this.oParentObject.w_MVCODICE
          if this.w_AGGRIG
            if this.oParentObject.w_FLDTPR="S" and this.pOper="D"
              * --- Dati Consegna Attivati (In Ordini sempre 'S') e se cambio data documento
              *     aggiorno data evasione delle righe
              if this.oParentObject.w_MVDATEVA<this.oParentObject.w_MVDATDOC OR EMPTY(NVL(this.oParentObject.w_MVSERRIF,SPACE(10))) 
 
                this.oParentObject.w_MVDATEVA = this.oParentObject.w_MVDATDOC
              endif
            endif
            * --- Forzo il ricalcolo se nel GSVE_BCD viene trovato un prezzo
            this.oParentObject.w_LIPREZZO = 0
            this.oParentObject.o_LIPREZZO = 0
            this.w_PADRE.NotifyEvent("Ricalcola")     
          else
            * --- Nel caso in cui rispondo No alla domanda di aggiornamento prezzi su riga,
            *     l'mCalc data dalla NotifyEvent('Clcperc1') aggiornerebbe comunque i prezzi sulle righe diverse da quella su cui
            *     � posizionato sul tranzitorio. Quindi aggiorno la o_ in modo che non vengano aggiornati
            *     o_MVQTAMOV e o_MVUNIMIS per il campo MVQTAUM1
            this.oParentObject.o_MVQTAMOV = this.oParentObject.w_MVQTAMOV
            this.oParentObject.o_MVUNIMIS = this.oParentObject.w_MVUNIMIS
            this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO
          endif
          * --- Carica il Temporaneo dei Dati
          this.w_PADRE.SaveRow()     
        endif
        this.w_PADRE.NextRow()     
      enddo
      * --- Riposizionamento sul Transitorio.
      *     La SaveDependsOn() viene effettuata solo se rispondo No 
      *     alla domanda di aggiornamento prezzi per evitare che vengano ricalcolate le o_ gi� assegnate
      this.w_PADRE.RePos(this.w_AGGRIG)     
      * --- lancia il calcolo Provvigioni solo per vendite
      *     Riesegue una scansione del transitorio e anche il riposizionemanto
      if not empty(this.oParentObject.w_MVCODAGE) And this.oParentObject.w_MVFLVEAC = "V"
        this.w_PADRE.NotifyEvent("CalcprovAllRow")     
      endif
      if this.w_AGGRIG
        * --- Poich� la Repos non ha aggiornato le o_  allineo o_mvcodice al valore della prima riga per 
        *     evitare che vengano erroneamente ricalcolate le variabili che dipendono da mvcodice
        *     (Per esempio cambio dell'unit� di misura sulla riga se movimentata la seconda)
        this.oParentObject.o_MVCODICE = this.oParentObject.w_MVCODICE
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='DES_DIVE'
    this.cWorkTables[2]='LISTINI'
    this.cWorkTables[3]='PAG_AMEN'
    this.cWorkTables[4]='SIT_FIDI'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='VOCIIVA'
    this.cWorkTables[7]='CONTI'
    this.cWorkTables[8]='AGENTI'
    this.cWorkTables[9]='CON_TRAM'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_GSVE_BES')
      use in _Curs_GSVE_BES
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
