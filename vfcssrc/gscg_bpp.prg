* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bpp                                                        *
*              Carica partite da primanota                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_59]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2014-07-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bpp",oParentObject)
return(i_retval)

define class tgscg_bpp as StdBatch
  * --- Local variables
  w_DATREG = ctod("  /  /  ")
  w_TROV = .f.
  w_NURATE = 0
  w_DATDOC = ctod("  /  /  ")
  w_DECTOT = 0
  w_NETTO = 0
  w_BANAPP = space(10)
  w_DESRIG = space(50)
  w_TOTDOC = 0
  w_IVA = 0
  w_VALNAZ = space(3)
  w_CALDOC = space(1)
  w_DATINI = ctod("  /  /  ")
  w_BANNOS = space(15)
  w_TOTIVA = 0
  w_ESCL1 = space(6)
  w_IMPDAR = 0
  w_CAONAZ = 0
  w_ESCL2 = space(4)
  w_IMPAVE = 0
  w_GIOFIS = 0
  w_GIORN1 = 0
  w_CODESE = space(4)
  w_MESE1 = 0
  w_GIORN2 = 0
  w_TIPCLF = space(1)
  w_MESE2 = 0
  w_CODCLF = space(15)
  w_FLPART = space(1)
  w_RIGIVA = 0
  w_PERIVA = 0
  w_APPO = 0
  w_APPO1 = 0
  w_CODAGE = space(5)
  w_FLVABD = space(1)
  w_NUMCOR = space(25)
  w_GEST = .NULL.
  w_FLINSO = space(1)
  w_RIFCES = space(50)
  w_ARRSCA = space(10)
  * --- WorkFile variables
  MOD_PAGA_idx=0
  BAN_CONTI_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica temporaneo Partite dai Movimenti di Primanota (da GSCG_MPA)
    this.w_GEST = this.oParentObject.oParentObject
    WITH this.w_GEST
    this.oParentObject.w_TIPCON = .w_PNTIPCON
    this.oParentObject.w_CODCON = .w_PNCODCON
    this.oParentObject.w_CODPAG = IIF( .w_PNFLPART="A" AND NOT EMPTY(g_SAPAGA), g_SAPAGA, this.oParentObject.w_CODPAG)
    this.oParentObject.w_NUMDOC = .w_PNNUMDOC
    this.oParentObject.w_ALFDOC = .w_PNALFDOC
    this.w_FLPART = .w_PNFLPART
    this.w_VALNAZ = .w_PNVALNAZ
    this.w_CAONAZ = .w_CAONAZ
    this.w_DECTOT = .w_DECTOT
    this.w_DATREG = .w_PNDATREG
    this.w_DATDOC = .w_PNDATDOC
    this.w_IMPDAR = .w_PNIMPDAR
    this.w_IMPAVE = .w_PNIMPAVE
    this.w_FLINSO = .w_FLINSO
    * --- Verifico quante righe sono presenti dello stesso conto 
    *     perch� se c'� pi� di una riga il totale delle partite deve essere calcolato
    *     sulla base del valore della riga di primanota e non dal totale documento
    this.w_GEST.Markpos()     
    .Exec_Select("TMP_RIG","Count(*) As Conta","NVL(t_PNCODCON, SPACE(10)) = "+cp_tostr( .w_PNCODCLF) + " AND NVL(t_PNTIPCON, 0) = "+cp_ToStrODBC( .w_PNTIPCLF ),"","")
    this.w_GEST.Repos()     
    if TMP_RIG.Conta<2 and .w_PNTOTDOC <>0
      this.w_TOTDOC = .w_PNTOTDOC
    else
      if .w_PNVALNAZ<>.w_PNCODVAL
        this.w_TOTDOC = MON2VAL(this.w_IMPDAR-this.w_IMPAVE, this.oParentObject.w_PTCAOVAL, this.w_CAONAZ, this.w_DATINI, this.w_VALNAZ, this.w_DECTOT)
      else
        this.w_TOTDOC = this.w_IMPDAR-this.w_IMPAVE
      endif
    endif
    Use in TMP_RIG
    this.w_CODESE = .w_PNCOMPET
    this.w_MESE1 = .w_CLMES1
    this.w_MESE2 = .w_CLMES2
    this.w_GIORN1 = .w_CLGIO1
    this.w_GIORN2 = .w_CLGIO2
    this.w_GIOFIS = .w_GIOFIS
    this.w_BANAPP = .w_BANAPP
    this.w_DESRIG = .w_PNDESRIG
    this.w_TIPCLF = .w_PNTIPCLF
    this.w_CODCLF = .w_PNCODCLF
    this.w_CALDOC = .w_CALDOC
    this.w_TOTIVA = .GSCG_MIV.w_TOTIVA
    this.w_BANNOS = .w_BANNOS
    this.w_CODAGE = .w_PNCODAGE
    this.w_FLVABD = .w_PNFLVABD
    this.w_RIFCES = .w_PNRIFCES
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANNUMCOR"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODCON);
            +" and ANCODBAN = "+cp_ToStrODBC(this.w_BANAPP);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANNUMCOR;
        from (i_cTable) where;
            ANTIPCON = this.oParentObject.w_TIPCON;
            and ANCODICE = this.oParentObject.w_CODCON;
            and ANCODBAN = this.w_BANAPP;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NUMCOR = NVL(cp_ToDate(_read_.ANNUMCOR),cp_NullValue(_read_.ANNUMCOR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if Empty(this.w_NUMCOR)
      * --- Read from BAN_CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.BAN_CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.BAN_CONTI_idx,2],.t.,this.BAN_CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCCONCOR"+;
          " from "+i_cTable+" BAN_CONTI where ";
              +"CCTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCON);
              +" and CCCODCON = "+cp_ToStrODBC(this.oParentObject.w_CODCON);
              +" and CCCODBAN = "+cp_ToStrODBC(this.w_BANAPP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCCONCOR;
          from (i_cTable) where;
              CCTIPCON = this.oParentObject.w_TIPCON;
              and CCCODCON = this.oParentObject.w_CODCON;
              and CCCODBAN = this.w_BANAPP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NUMCOR = NVL(cp_ToDate(_read_.CCCONCOR),cp_NullValue(_read_.CCCONCOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    ENDWITH
    if this.w_FLPART="S"
      * --- Se operazione di saldo non deve fare il calcola partita
      i_retcode = 'stop'
      return
    endif
    if this.w_FLINSO="S" and this.w_RIFCES="C"
      ah_ErrorMsg("Registrazione proveniente da contabilizzazione insoluti. %0Impossibile modificare",,"")
      * --- Se operazione di saldo non deve fare il calcola partita
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_CODPAG) 
      ah_ErrorMsg("Codice pagamento non definito",,"")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.w_DATDOC) AND this.oParentObject.w_FLNDOC="O"
      ah_ErrorMsg("Inserire la data documento",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Azzera il Transitorio
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    DELETE ALL
    this.w_TROV = .F.
    * --- Vettore contenente i Dati Delle Scadenze
    DIMENSION w_ARRSCA[999,6]
    this.w_IVA = IIF(this.w_CALDOC$"EN", this.w_TOTIVA, 0)
    this.w_NETTO = ABS(this.w_IMPDAR-this.w_IMPAVE)
    this.w_DATINI = IIF(EMPTY(this.w_DATDOC), this.w_DATREG, this.w_DATDOC)
    this.w_ESCL1 = STR(this.w_MESE1, 2, 0) + STR(this.w_GIORN1, 2, 0) + STR(this.w_GIOFIS, 2, 0)
    this.w_ESCL2 = STR(this.w_MESE2, 2, 0) + STR(this.w_GIORN2, 2, 0)
    this.oParentObject.w_PTCODVAL = this.oParentObject.w_CODVAL
    this.oParentObject.w_PTCAOVAL = this.oParentObject.w_CAOVAL
    if this.oParentObject.w_PTCODVAL<>this.w_VALNAZ AND this.oParentObject.w_PTCAOVAL<>0
      * --- Partite in Valuta (se Esiste il Documento ed e' riferito al Cli/For di Riga prende Quello...
      if this.w_TOTDOC<>0 AND ( (this.oParentObject.w_TIPCON=this.w_TIPCLF AND this.oParentObject.w_CODCON=this.w_CODCLF) OR EMPTY(this.w_CODCLF))
        this.w_NETTO = ABS(this.w_TOTDOC)
        if this.w_CALDOC$"EN" AND this.w_IVA<>0
          * --- Converte l'IVA alla Valuta del Documento di Origine
          this.w_APPO = this.w_IVA
          this.w_IVA = cp_ROUND(MON2VAL(this.w_IVA, this.oParentObject.w_PTCAOVAL, this.w_CAONAZ, this.w_DATINI, this.w_VALNAZ)+IIF(this.oParentObject.w_PTCODVAL=g_CODLIR, .499, 0), this.w_DECTOT)
          SELECT (this.oParentObject.oParentObject.GSCG_MIV.cTrsName)
          this.w_APPO1 = RECNO()
          GO TOP
          this.w_RIGIVA = NVL(t_IVIMPIVA, 0)
          this.w_PERIVA = NVL(t_PERIVA, 0)
          if this.w_APPO1>0 AND this.w_APPO1<=RECCOUNT()
            GOTO this.w_APPO1
          endif
          if this.w_APPO=this.w_RIGIVA AND this.w_PERIVA<>0
            * --- Se presente una sola aliquota, ricalcola direttamente sulla percentuale IVA
            this.w_IVA = cp_ROUND(this.w_NETTO - (this.w_NETTO / (1 + (this.w_PERIVA / 100)))+IIF(this.oParentObject.w_PTCODVAL=g_CODLIR, .499, 0), this.w_DECTOT)
          endif
        endif
      else
        this.w_NETTO = MON2VAL(this.w_NETTO, this.oParentObject.w_PTCAOVAL, this.w_CAONAZ, this.w_DATINI, this.w_VALNAZ, this.w_DECTOT)
        if this.w_CALDOC$"EN" AND this.w_IVA<>0
          * --- Converte l'IVA alla Valuta del Documento di Origine
          this.w_IVA = cp_ROUND(MON2VAL(this.w_IVA, this.oParentObject.w_PTCAOVAL, this.w_CAONAZ, this.w_DATINI, this.w_VALNAZ)+IIF(this.oParentObject.w_PTCODVAL=g_CODLIR, .499, 0), this.w_DECTOT)
        endif
      endif
    endif
    this.oParentObject.w_PTIMPDOC = ABS(this.w_NETTO)
    this.w_NETTO = this.w_NETTO - this.w_IVA
    this.w_NURATE = SCADENZE("w_ARRSCA", this.oParentObject.w_CODPAG, this.w_DATINI, this.w_NETTO, this.w_IVA, 0, this.w_ESCL1, this.w_ESCL2, this.w_DECTOT)
    * --- Cicla Sulle Rate
    FOR L_i = 1 to this.w_NURATE
    * --- Append
    this.oParentObject.InitRow()
    this.oParentObject.w_PTDATSCA = w_ARRSCA[L_i, 1]
    if this.w_FLPART="A"
      * --- Acconto
      this.oParentObject.w_PTNUMPAR = CANUMPAR("S", this.w_CODESE)
    else
      this.oParentObject.w_PTNUMPAR = CANUMPAR("N", this.w_CODESE, this.oParentObject.w_NUMDOC, this.oParentObject.w_ALFDOC)
    endif
    this.oParentObject.w_PT_SEGNO = IIF(this.w_IMPAVE>0 OR this.w_IMPDAR<0 , "A", "D")
    this.oParentObject.w_PTTOTIMP = ABS(w_ARRSCA[L_i, 2] +w_ARRSCA[L_i, 3])
    this.oParentObject.w_PTIMPDOC = ABS(this.oParentObject.w_PTIMPDOC)
    this.oParentObject.w_PTMODPAG = w_ARRSCA[L_i, 5]
    this.oParentObject.w_PTDESRIG = this.w_DESRIG
    this.oParentObject.w_PTCODAGE = this.w_CODAGE
    this.oParentObject.w_PTFLVABD = this.w_FLVABD
    if this.oParentObject.w_TIPCON $ "CF"
      * --- Banca di Appoggio Cli/For + ev.le Nostra Banca di Appoggio
      this.oParentObject.w_PTBANAPP = this.w_BANAPP
      this.oParentObject.w_PTBANNOS = this.w_BANNOS
      this.oParentObject.w_PTNUMCOR = this.w_NUMCOR
    endif
    if NOT EMPTY(this.oParentObject.w_PTMODPAG)
      * --- Read from MOD_PAGA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MOD_PAGA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAGA_idx,2],.t.,this.MOD_PAGA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MPTIPPAG"+;
          " from "+i_cTable+" MOD_PAGA where ";
              +"MPCODICE = "+cp_ToStrODBC(this.oParentObject.w_PTMODPAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MPTIPPAG;
          from (i_cTable) where;
              MPCODICE = this.oParentObject.w_PTMODPAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_TIPPAG = NVL(cp_ToDate(_read_.MPTIPPAG),cp_NullValue(_read_.MPTIPPAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Carica il Temporaneo dei Dati e skippa al record successivo
    this.oParentObject.TrsFromWork()
    this.w_TROV = .T.
    ENDFOR
    if this.w_TROV=.F.
      * --- Carica almeno la prima Riga
      this.oParentObject.BlankRec()
    endif
    * --- Questa Parte derivata dal Metodo LoadRec
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    With this.oParentObject
    .WorkFromTrs()
    .SetControlsValue()
    .ChildrenChangeRow()
    .oPgFrm.Page1.oPag.oBody.Refresh()
    .oPgFrm.Page1.oPag.oBody.nAbsRow=1
    .oPgFrm.Page1.oPag.oBody.nRelRow=1
    EndWith
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='MOD_PAGA'
    this.cWorkTables[2]='BAN_CONTI'
    this.cWorkTables[3]='CONTI'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
