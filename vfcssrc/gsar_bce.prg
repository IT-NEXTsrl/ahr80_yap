* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bce                                                        *
*              Controllo cancellazione esercizi                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-04                                                      *
* Last revis.: 2014-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bce",oParentObject)
return(i_retval)

define class tgsar_bce as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo il totale delle registrazione stampate in definitiva
    * --- Select from Gsar_Bce
    do vq_exec with 'Gsar_Bce',this,'_Curs_Gsar_Bce','',.f.,.t.
    if used('_Curs_Gsar_Bce')
      select _Curs_Gsar_Bce
      locate for 1=1
      do while not(eof())
      this.oParentObject.w_TOTREGDEF = _Curs_Gsar_Bce.Num_Reg
        select _Curs_Gsar_Bce
        continue
      enddo
      use
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_Gsar_Bce')
      use in _Curs_Gsar_Bce
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
