* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bpp                                                        *
*              Carica scadenze varie                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_32]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-28                                                      *
* Last revis.: 2012-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bpp",oParentObject)
return(i_retval)

define class tgste_bpp as StdBatch
  * --- Local variables
  w_MESE1 = 0
  w_NURATE = 0
  w_MESE2 = 0
  w_DECTOT = 0
  w_GIORN1 = 0
  w_ESCL1 = space(6)
  w_GIORN2 = 0
  w_ESCL2 = space(4)
  w_GIOFIS = 0
  w_NUMCOR = space(25)
  w_ARRSCA = space(10)
  * --- WorkFile variables
  MOD_PAGA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica temporaneo Partite dalla Gestione Scadenze Varie (da GSTE_MPA)
    WITH this.oParentObject.oParentObject
    this.oParentObject.w_IMPSCA = .w_SCIMPSCA
    this.oParentObject.w_CODVAL = .w_SCCODVAL
    this.oParentObject.w_DATVAL = IIF(EMPTY(.w_SCDATDOC), .w_SCDATREG, .w_SCDATDOC)
    this.oParentObject.w_CODPAG = .w_SCCODPAG
    this.oParentObject.w_TIPSCA = .w_SCTIPSCA
    this.oParentObject.w_FLIMPG = .w_SCFLIMPG
    this.oParentObject.w_TIPCLF = .w_SCTIPCLF
    this.oParentObject.w_SIMBVA = .w_SIMVAL
    this.oParentObject.w_CODCON = .w_SCCODCLF
    this.oParentObject.w_NUMDOC = .w_SCNUMDOC
    this.oParentObject.w_ALFDOC = .w_SCALFDOC
    this.oParentObject.w_PTIMPDOC = .w_SCIMPSCA
    this.oParentObject.w_PTDESRIG = .w_SCDESCRI
    this.w_MESE1 = .w_MESCL1
    this.w_MESE2 = .w_MESCL2
    this.w_GIORN1 = .w_GIOSC1
    this.w_GIORN2 = .w_GIOSC2
    this.w_GIOFIS = .w_GIOFIS
    this.w_NUMCOR = .w_NUMCOR
    this.w_DECTOT = .w_DECTOT
    ENDWITH
    this.oParentObject.w_TOTPAR = 0
    if EMPTY(this.oParentObject.w_CODPAG)
      ah_ErrorMsg("Codice pagamento non definito",,"")
      i_retcode = 'stop'
      return
    endif
    if this.oParentObject.w_IMPSCA=0
      ah_ErrorMsg("Importo totale scadenze non definito",,"")
    endif
    if this.oParentObject.w_IMPSCA<0
      ah_ErrorMsg("Importo totale scadenze negativo",,"")
      i_retcode = 'stop'
      return
    endif
    * --- Azzera il Transitorio
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    DELETE ALL
    * --- Vettore contenente i Dati Delle Scadenze
    DIMENSION w_ARRSCA[999,6]
    this.w_ESCL1 = SPACE(6)
    this.w_ESCL2 = SPACE(4)
    if this.oParentObject.w_TIPCLF $ "CF"
      this.w_ESCL1 = STR(this.w_MESE1, 2, 0) + STR(this.w_GIORN1, 2, 0) + STR(this.w_GIOFIS, 2, 0)
      this.w_ESCL2 = STR(this.w_MESE2, 2, 0) + STR(this.w_GIORN2, 2, 0)
    endif
    this.w_NURATE = SCADENZE("w_ARRSCA", this.oParentObject.w_CODPAG, this.oParentObject.w_DATVAL, this.oParentObject.w_IMPSCA, 0, 0, this.w_ESCL1, this.w_ESCL2, this.w_DECTOT)
    * --- Cicla Sulle Rate
    FOR L_i = 1 to this.w_NURATE
    * --- Nuova Riga del Temporaneo
    this.oParentObject.InitRow()
    this.oParentObject.w_PTDATSCA = w_ARRSCA[L_i, 1]
    this.oParentObject.w_PTNUMPAR = CANUMPAR("N", g_CODESE, this.oParentObject.w_NUMDOC, this.oParentObject.w_ALFDOC)
    * --- nel caso di Conti Generici il segno � invertito rispetto ai Clienti / fornitori
    * --- w_PT_SEGNO <= IIF((w_TIPSCA='F' AND w_TIPCLF<>'G') OR (w_TIPSCA='C' AND w_TIPCLF='G') , 'A', 'D')
    this.oParentObject.w_PT_SEGNO = iif(this.oParentObject.w_TIPSCA="F","A","D")
    this.oParentObject.w_PTTOTIMP = IIF(this.oParentObject.w_FLIMPG="G", w_ARRSCA[L_I, 2], this.oParentObject.w_IMPSCA)
    this.oParentObject.w_IMPPAR = this.oParentObject.w_PTTOTIMP*IIF(this.oParentObject.w_PT_SEGNO="D", 1,-1)*IIF(this.oParentObject.w_PTTIPCON="C", 1,-1)*IIF( this.oParentObject.oParentObject.w_PFLCRSA="S",-1,1)
    this.oParentObject.w_PTIMPDOC = ABS(this.oParentObject.w_PTIMPDOC)
    this.oParentObject.w_PTMODPAG = w_ARRSCA[L_I, 5]
    this.oParentObject.w_PTCODVAL = this.oParentObject.w_CODVAL
    this.oParentObject.w_PTCAOVAL = this.oParentObject.oParentObject.w_SCCAOVAL
    this.oParentObject.w_PTCAOAPE = this.oParentObject.oParentObject.w_SCCAOVAL
    this.oParentObject.w_PTDATAPE = this.oParentObject.w_DATVAL
    this.oParentObject.w_PTBANAPP = SPACE(10)
    this.oParentObject.w_PTBANNOS = SPACE(15)
    this.oParentObject.w_PTNUMCOR = SPACE(25)
    if this.oParentObject.oParentObject.w_SCTIPCLF $ "CF"
      this.oParentObject.w_PTBANAPP = this.oParentObject.oParentObject.w_SCCODBAN
      this.oParentObject.w_PTBANNOS = this.oParentObject.oParentObject.w_SCBANNOS
      this.oParentObject.w_PTNUMCOR = this.oParentObject.oParentObject.w_NUMCOR
    endif
    this.oParentObject.w_TIPPAG = "  "
    * --- Se il Tipo Pagamento = RB,CA devo inserire la banca di appoggio
    if NOT EMPTY(this.oParentObject.w_PTMODPAG)
      * --- Read from MOD_PAGA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.MOD_PAGA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAGA_idx,2],.t.,this.MOD_PAGA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MPTIPPAG"+;
          " from "+i_cTable+" MOD_PAGA where ";
              +"MPCODICE = "+cp_ToStrODBC(this.oParentObject.w_PTMODPAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MPTIPPAG;
          from (i_cTable) where;
              MPCODICE = this.oParentObject.w_PTMODPAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.oParentObject.w_TIPPAG = NVL(cp_ToDate(_read_.MPTIPPAG),cp_NullValue(_read_.MPTIPPAG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.oParentObject.w_PTDESRIG = this.oParentObject.oParentObject.w_SCDESCRI
    this.oParentObject.w_PTCODAGE = this.oParentObject.oParentObject.w_SCCODAGE
    this.oParentObject.w_PTFLCRSA = this.oParentObject.oParentObject.w_PFLCRSA
    * --- Carica il Temporaneo dei Dati e skippa al record successivo
    this.oParentObject.w_TOTPAR = this.oParentObject.w_TOTPAR + this.oParentObject.w_IMPPAR
    this.oParentObject.TrsFromWork()
    ENDFOR
    * --- Questa Parte derivata dal Metodo LoadRec
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    With this.oParentObject
    .WorkFromTrs()
    .SetControlsValue()
    .ChildrenChangeRow()
    .oPgFrm.Page1.oPag.oBody.Refresh()
    .oPgFrm.Page1.oPag.oBody.nAbsRow=1
    .oPgFrm.Page1.oPag.oBody.nRelRow=1
    EndWith
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='MOD_PAGA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
