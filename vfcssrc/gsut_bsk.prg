* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bsk                                                        *
*              Gestione chiamate skype/centralino                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-26                                                      *
* Last revis.: 2012-06-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCONTACT,pCOMMTYP,pNOMINATIVO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bsk",oParentObject,m.pCONTACT,m.pCOMMTYP,m.pNOMINATIVO)
return(i_retval)

define class tgsut_bsk as StdBatch
  * --- Local variables
  w_RETVAL = 0
  pCONTACT = space(50)
  pCOMMTYP = space(2)
  pNOMINATIVO = space(15)
  w_RETVAL = 0
  w_ERRLOG = .NULL.
  w_CONTSTAT = 0
  w_RETVAL = 0
  w_DHPROGRA = space(18)
  w_DHPROPAR = space(254)
  * --- WorkFile variables
  DIS_HARD_idx=0
  UTE_NTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pCONTACT = Contatto da utilizzare per la chiamata/Chat pu� essere un numero telefonico oppure un contatto skype
    *     pCOMMTYP = Tipo comunicazione. Pu� valere:
    *                            SO = per chiamate in uscita con utilizzo del credito
    *                            CC = per permettere la selezione di chiamata/videochiamata skype oppure chat
    *                            CH = per avviare direttamente la chat
    *                            CS = per avviare direttamente una chiamata skype
    *                            VS = per avviare direttamente una videochiamata skype
    *                            CT = per chiamata tramite centralino (telefono)
    *                            CM = per chiamata tramite centralino (cellulare)
    *                            MN = Menu centralino/skype (telefono)
    *                            MM = Menu centralino/skype (cellulare)
    *                            SK = Chiamata tramite skype
    *     pNOMINATIVO = Nominativo
    do case
      case this.pCOMMTYP=="MN" Or this.pCOMMTYP=="MM" Or this.pCOMMTYP=="SK"
        * --- Menu centralino/skype
        if g_SkypeService="C" And !Empty(g_PhoneService) And g_AGFA = "S" AND this.pCOMMTYP<>"SK"
          if i_VisualTheme = -1
            L_RETVAL=0
            DEFINE POPUP popCmd FROM MROW(),MCOL() SHORTCUT
            DEFINE BAR 1 OF popCmd PROMPT ah_MsgFormat("Chiama") + " " + Alltrim(this.pCONTACT)
            ON SELECTION BAR 1 OF popCmd L_RETVAL = 1
            DEFINE BAR 2 OF popCmd PROMPT ah_MsgFormat("Chiama via Skype") PICTURE "BMP\SKYPE.ico"
            ON SELECTION BAR 2 OF popCmd L_RETVAL = 2
            ACTIVATE POPUP popCmd
            DEACTIVATE POPUP popCmd
            RELEASE POPUPS popCmd
            this.w_RETVAL = L_RETVAL
            Release L_RETVAL
          else
            local oBtnMenu, oMenuItem
            oBtnMenu = CreateObject("cbPopupMenu")
            oBtnMenu.oParentObject = This
            oBtnMenu.oPopupMenu.cOnClickProc="ExecScript(NAMEPROC)"
            oMenuItem = oBtnMenu.AddMenuItem()
            oMenuItem.Caption = Ah_Msgformat("Chiama") + " " + Alltrim(this.pCONTACT)
            oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=1"
            oMenuItem.Visible=.T.
            oMenuItem = oBtnMenu.AddMenuItem()
            oMenuItem.Caption = Ah_Msgformat("Chiama via Skype")
            oMenuItem.Picture = "BMP\SKYPE.ico"
            oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=2"
            oMenuItem.Visible=.T.
            oBtnMenu.InitMenu()
            oBtnMenu.ShowMenu()
            oBtnMenu = .NULL.
          endif
        else
          * --- Se uno dei due servizi non � attivo, non mostro il menu e lancio subito la voce
          if this.pCOMMTYP=="SK"
            this.w_RETVAL = 2
          else
            this.w_RETVAL = IIF(g_SkypeService="C", 2, 1)
          endif
        endif
        do case
          case this.w_RETVAL = 1
            if this.pCOMMTYP=="MN"
              * --- Telefono
              GSUT_BSK(this,this.pCONTACT, "CT", this.pNOMINATIVO)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- Cellulare
              GSUT_BSK(this,this.pCONTACT, "CM", this.pNOMINATIVO)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          case this.w_RETVAL = 2
            GSUT_BSK(this,this.pCONTACT, "SO", this.pNOMINATIVO)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
        endcase
        i_retcode = 'stop'
        return
      case this.pCOMMTYP=="CT" Or this.pCOMMTYP=="CM"
        * --- Chiamata con centralino
        if g_AGFA="S" And !Empty(this.pCONTACT)
          * --- Read from DIS_HARD
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIS_HARD_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIS_HARD_idx,2],.t.,this.DIS_HARD_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DHPROGRA,DHPROPAR"+;
              " from "+i_cTable+" DIS_HARD where ";
                  +"DHCODICE = "+cp_ToStrODBC(g_PhoneService);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DHPROGRA,DHPROPAR;
              from (i_cTable) where;
                  DHCODICE = g_PhoneService;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DHPROGRA = NVL(cp_ToDate(_read_.DHPROGRA),cp_NullValue(_read_.DHPROGRA))
            this.w_DHPROPAR = NVL(cp_ToDate(_read_.DHPROPAR),cp_NullValue(_read_.DHPROPAR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if !Empty(this.w_DHPROGRA)
            * --- Gestione errori
             
 Local l_OldErr, l_Err 
 l_OldErr = On("Error") 
 l_Err=.F. 
 On Error l_Err=.T.
             
 l_macro =Alltrim(this.w_DHPROGRA) + "(this, pCONTACT" + IIF(!Empty(this.w_DHPROPAR), ", " + this.w_DHPROPAR,"") + ")" 
 &l_macro
            if l_Err
              ah_ErrorMsg("Errore nel lancio della routine associata al centralino")
            else
              * --- Apro attivit�
              GSFA_BSK(this,this.pCONTACT, this.pCOMMTYP, this.pNOMINATIVO)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- Ripristino controllo errori
            On Error &l_OldErr
          else
            ah_ErrorMsg("Driver per centralino non definito")
          endif
        endif
        i_retcode = 'stop'
        return
      case this.pCOMMTYP=="CC"
        * --- Tento di recuperare lo stato del contatto
        this.w_CONTSTAT = SkypeOp( "GS", ALLTRIM(this.pCONTACT) )
        * --- Mostro il men� per la selezione della tipologia di chiamata desiderata.
        *     In base alla selezione cambio il valore di pCOMMTYP per eseguire
        *     l'operazione Skype selezionata dall'utente
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    this.w_RETVAL = 0
    this.w_ERRLOG = CREATEOBJECT("ah_errorlog")
    this.w_RETVAL = SkypeOp( this.pCOMMTYP, ALLTRIM(this.pCONTACT), this.w_ERRLOG )
    if g_AGFA="S" And !Empty(this.pCONTACT) AND this.pCOMMTYP=="SO"
      * --- Apro attivit�
      GSFA_BSK(this,this.pCONTACT, this.pCOMMTYP, this.pNOMINATIVO)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_RETVAL<0
      this.w_ERRLOG.PrintLog()     
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if i_VisualTheme = -1
      L_RETVAL=0
      DEFINE POPUP popCmd FROM MROW(),MCOL() SHORTCUT
      DEFINE BAR 1 OF popCmd PROMPT ah_MsgFormat("Chat")
      DEFINE BAR 2 OF popCmd PROMPT ah_MsgFormat("Chiamata")
      DEFINE BAR 3 OF popCmd PROMPT ah_MsgFormat("Videochiamata")
      ON SELECTION BAR 1 OF popCmd L_RETVAL = 1
      ON SELECTION BAR 2 OF popCmd L_RETVAL = 2
      ON SELECTION BAR 3 OF popCmd L_RETVAL = 3
      ACTIVATE POPUP popCmd
      DEACTIVATE POPUP popCmd
      RELEASE POPUPS popCmd
      this.w_RETVAL = L_RETVAL
      Release L_RETVAL
    else
      local oBtnMenu, oMenuItem
      oBtnMenu = CreateObject("cbPopupMenu")
      oBtnMenu.oParentObject = This
      oBtnMenu.oPopupMenu.cOnClickProc="ExecScript(NAMEPROC)"
      oMenuItem = oBtnMenu.AddMenuItem()
      oMenuItem.Caption = Ah_Msgformat("Chat")
      oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=1"
      oMenuItem.Picture = SkypeOp( "GI", ALLTRIM(this.pCONTACT) )
      oMenuItem.Visible=.T.
      oMenuItem = oBtnMenu.AddMenuItem()
      oMenuItem.Caption = Ah_Msgformat("Chiamata")
      oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=2"
      oMenuItem.Visible=.T.
      oMenuItem = oBtnMenu.AddMenuItem()
      oMenuItem.Caption = Ah_Msgformat("Videochiamata")
      oMenuItem.OnClick = ".Parent.oParentObject.w_RETVAL=3"
      oMenuItem.Visible=.T.
      oBtnMenu.InitMenu()
      oBtnMenu.ShowMenu()
      oBtnMenu = .NULL.
    endif
    if this.w_RETVAL>0
      do case
        case this.w_RETVAL = 1
          this.pCOMMTYP = "CH"
        case this.w_RETVAL = 2
          this.pCOMMTYP = "CS"
        case this.w_RETVAL = 3
          this.pCOMMTYP = "VS"
      endcase
    endif
  endproc


  proc Init(oParentObject,pCONTACT,pCOMMTYP,pNOMINATIVO)
    this.pCONTACT=pCONTACT
    this.pCOMMTYP=pCOMMTYP
    this.pNOMINATIVO=pNOMINATIVO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DIS_HARD'
    this.cWorkTables[2]='UTE_NTI'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCONTACT,pCOMMTYP,pNOMINATIVO"
endproc
