* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_krf                                                        *
*              Generazione file CBI/SEPA                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_190]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-03                                                      *
* Last revis.: 2016-07-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_krf",oParentObject))

* --- Class definition
define class tgste_krf as StdForm
  Top    = 2
  Left   = 16

  * --- Standard Properties
  Width  = 691
  Height = 560
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-07-29"
  HelpContextID=116012137
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=44

  * --- Constant Properties
  _IDX = 0
  CONTROPA_IDX = 0
  COC_MAST_IDX = 0
  cPrg = "gste_krf"
  cComment = "Generazione file CBI/SEPA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DATPRE = ctod('  /  /  ')
  o_DATPRE = ctod('  /  /  ')
  w_FLEURO = space(1)
  w_TIPDIS_1 = space(2)
  o_TIPDIS_1 = space(2)
  w_TIPDIS_2 = space(2)
  o_TIPDIS_2 = space(2)
  w_TIPDIS = space(2)
  o_TIPDIS = space(2)
  w_DISERIAL = space(10)
  w_NUMERO1 = 0
  o_NUMERO1 = 0
  w_ANNO1 = space(4)
  o_ANNO1 = space(4)
  w_DATA1 = ctod('  /  /  ')
  o_DATA1 = ctod('  /  /  ')
  w_DESCRI1 = space(35)
  w_NUMERO = 0
  w_DATDIS = ctod('  /  /  ')
  w_TIPGEN = space(1)
  w_FLIBAN = space(1)
  w_DADATA = ctod('  /  /  ')
  w_FLQUALI = space(1)
  w_DESPAR = space(1)
  w_TIPRID = space(10)
  w_CABONURG = space(1)
  w_FLSEPA = space(1)
  o_FLSEPA = space(1)
  w_CODRIF = space(1)
  w_INFMAND = space(1)
  w_TIPRIC = space(2)
  w_ANNO = space(4)
  w_BANCA = space(15)
  w_DESBAN = space(35)
  w_CODAZI = space(5)
  w_TIPBAN = space(1)
  w_TIPPAG = space(1)
  w_NOMEFILE5 = space(254)
  o_NOMEFILE5 = space(254)
  w_NOMEFILE1 = space(254)
  o_NOMEFILE1 = space(254)
  w_NOMEFILE2 = space(254)
  o_NOMEFILE2 = space(254)
  w_NOMEFILE3 = space(254)
  o_NOMEFILE3 = space(254)
  w_NOMEFILE4 = space(254)
  o_NOMEFILE4 = space(254)
  w_NOMEFILE = space(254)
  w_PROG = 0
  w_NOMEFILE6 = space(254)
  o_NOMEFILE6 = space(254)
  w_SEPREC = space(1)
  w_TESTFILE = space(254)
  w_NOME = space(10)
  w_NOMEFILE7 = space(8)
  w_DICAURIF = space(5)
  w_DI__PATH = space(254)
  w_HTML_URL = space(10)
  w_ZoomEF = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_krfPag1","gste_krf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATPRE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomEF = this.oPgFrm.Pages(1).oPag.ZoomEF
    DoDefault()
    proc Destroy()
      this.w_ZoomEF = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='COC_MAST'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATPRE=ctod("  /  /  ")
      .w_FLEURO=space(1)
      .w_TIPDIS_1=space(2)
      .w_TIPDIS_2=space(2)
      .w_TIPDIS=space(2)
      .w_DISERIAL=space(10)
      .w_NUMERO1=0
      .w_ANNO1=space(4)
      .w_DATA1=ctod("  /  /  ")
      .w_DESCRI1=space(35)
      .w_NUMERO=0
      .w_DATDIS=ctod("  /  /  ")
      .w_TIPGEN=space(1)
      .w_FLIBAN=space(1)
      .w_DADATA=ctod("  /  /  ")
      .w_FLQUALI=space(1)
      .w_DESPAR=space(1)
      .w_TIPRID=space(10)
      .w_CABONURG=space(1)
      .w_FLSEPA=space(1)
      .w_CODRIF=space(1)
      .w_INFMAND=space(1)
      .w_TIPRIC=space(2)
      .w_ANNO=space(4)
      .w_BANCA=space(15)
      .w_DESBAN=space(35)
      .w_CODAZI=space(5)
      .w_TIPBAN=space(1)
      .w_TIPPAG=space(1)
      .w_NOMEFILE5=space(254)
      .w_NOMEFILE1=space(254)
      .w_NOMEFILE2=space(254)
      .w_NOMEFILE3=space(254)
      .w_NOMEFILE4=space(254)
      .w_NOMEFILE=space(254)
      .w_PROG=0
      .w_NOMEFILE6=space(254)
      .w_SEPREC=space(1)
      .w_TESTFILE=space(254)
      .w_NOME=space(10)
      .w_NOMEFILE7=space(8)
      .w_DICAURIF=space(5)
      .w_DI__PATH=space(254)
      .w_HTML_URL=space(10)
        .w_DATPRE = i_datsys
        .w_FLEURO = IIF(.w_DATPRE>cp_CharToDate('31-12-2001'), 'S', ' ')
        .w_TIPDIS_1 = 'RB'
        .w_TIPDIS_2 = 'RB'
        .w_TIPDIS = IIF(g_ISONAZ='ESP',.w_TIPDIS_2,.w_TIPDIS_1)
        .w_DISERIAL = Nvl( .w_zoomef.getvar('DINUMDIS') , Space(10) )
      .oPgFrm.Page1.oPag.ZoomEF.Calculate(.F.)
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
        .w_NUMERO1 = Nvl( .w_zoomef.getvar('DINUMERO') , 0 )
        .w_ANNO1 = Nvl( .w_zoomef.getvar('DI__ANNO') , Space(4))
        .w_DATA1 = CP_TODATE(.w_zoomef.getvar('DIDATDIS'))
        .w_DESCRI1 = Nvl ( .w_zoomef.getvar('DIDESCRI') , Space(35))
          .DoRTCalc(11,12,.f.)
        .w_TIPGEN = 'N'
        .w_FLIBAN = IIF(.w_TIPDIS='BO' AND g_ISONAZ<>'ESP','S',' ')
        .w_DADATA = G_INIESE
        .w_FLQUALI = ' '
        .w_DESPAR = 'N'
        .w_TIPRID = 'N'
        .w_CABONURG = 'N'
        .w_FLSEPA = IIF(.w_TIPDIS $ 'SC-SD-SE','S','N')
        .w_CODRIF = 'S'
        .w_INFMAND = IIF(.w_TIPDIS = 'SD','S','N')
        .w_TIPRIC = '0'
          .DoRTCalc(24,24,.f.)
        .w_BANCA = Nvl( .w_zoomef.getvar('DIBANRIF') , Space(15) )
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_BANCA))
          .link_1_43('Full')
        endif
          .DoRTCalc(26,26,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_CODAZI))
          .link_1_45('Full')
        endif
          .DoRTCalc(28,29,.f.)
        .w_NOMEFILE5 = IIF(LEN(.w_NOMEFILE6) <=8,SPACE(254),.w_NOMEFILE6)
        .w_NOMEFILE1 = ALLTRIM(IIF(.w_TIPPAG='S', icase(.w_TIPDIS='RB',"RIBA\", .w_TIPDIS$ 'RI' ,'RID\', .w_TIPDIS='MA' ,'MAV\' ,.w_TIPDIS='SD','SDD\',.w_TIPDIS='SC','SCT\',.w_TIPDIS='SE','SCE\',"BONI\"),' '))
        .w_NOMEFILE2 = IIF(EMPTY(.w_TESTFILE), left(sys(5)+sys(2003),40) +'\', .w_TESTFILE)
        .w_NOMEFILE3 = ALLTRIM(IIF(.w_TIPBAN $ 'CD', iif(.w_TIPBAN='C', ALLTRIM(.w_BANCA) +ALLTRIM(IIF(EMPTY(.w_BANCA), ' ', '\')),ALLTRIM(.w_DESBAN) +ALLTRIM(IIF(EMPTY(.w_DESBAN), ' ', '\'))), ' '))
        .w_NOMEFILE4 = IIF(.w_NOMEFILE4='0', ' ', icase(.w_TIPDIS='RB','R',.w_TIPDIS $ 'RI' ,'D', .w_TIPDIS='MA' ,'M' ,.w_TIPDIS = 'BO','B','') +iif(.w_TIPDIS$ 'SE-SC-SD',Alltrim(.w_TIPDIS)+.w_DISERIAL,RIGHT('0'+ALLTRIM(STR(DAY(.w_DATA1))),2)+RIGHT('0'+ALLTRIM(STR(MONTH(.w_DATA1))),2)+ RIGHT(ALLTRIM(STR(YEAR(.w_DATA1))), 2) +ALLTRIM(STR(.w_PROG))))
        .w_NOMEFILE = IIF(NOT EMPTY(.w_NOMEFILE5) AND NOT LEN(.w_NOMEFILE5)<=8, .w_NOMEFILE5, ALLTRIM(.w_NOMEFILE2)+ALLTRIM(.w_NOMEFILE3)+ALLTRIM(.w_NOMEFILE1))
        .w_PROG = 1
          .DoRTCalc(37,37,.f.)
        .w_SEPREC = 'S'
          .DoRTCalc(39,39,.f.)
        .w_NOME = IIF(.w_NOMEFILE4='0', ' ', iif(.w_TIPDIS='RB','R', iif(.w_TIPDIS='RI' ,'D', iif(.w_TIPDIS='MA' ,'M' ,'B'))) +RIGHT('0'+ALLTRIM(STR(DAY(.w_DATA1))),2)+RIGHT('0'+ALLTRIM(STR(MONTH(.w_DATA1))),2)+ RIGHT(ALLTRIM(STR(YEAR(.w_DATA1))), 2) +ALLTRIM(STR(.w_PROG)))
        .w_NOMEFILE7 = IIF(NOT EMPTY(.w_NOMEFILE5), .w_NOMEFILE4, SPACE(8))
        .w_DICAURIF = Nvl( .w_zoomef.getvar('DICAURIF') , Space(5) )
    endwith
    this.DoRTCalc(43,44,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_DATPRE<>.w_DATPRE
            .w_FLEURO = IIF(.w_DATPRE>cp_CharToDate('31-12-2001'), 'S', ' ')
        endif
        .DoRTCalc(3,4,.t.)
        if .o_TIPDIS_1<>.w_TIPDIS_1.or. .o_TIPDIS_2<>.w_TIPDIS_2
            .w_TIPDIS = IIF(g_ISONAZ='ESP',.w_TIPDIS_2,.w_TIPDIS_1)
        endif
            .w_DISERIAL = Nvl( .w_zoomef.getvar('DINUMDIS') , Space(10) )
        .oPgFrm.Page1.oPag.ZoomEF.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
            .w_NUMERO1 = Nvl( .w_zoomef.getvar('DINUMERO') , 0 )
            .w_ANNO1 = Nvl( .w_zoomef.getvar('DI__ANNO') , Space(4))
            .w_DATA1 = CP_TODATE(.w_zoomef.getvar('DIDATDIS'))
            .w_DESCRI1 = Nvl ( .w_zoomef.getvar('DIDESCRI') , Space(35))
        .DoRTCalc(11,13,.t.)
        if .o_TIPDIS<>.w_TIPDIS
            .w_FLIBAN = IIF(.w_TIPDIS='BO' AND g_ISONAZ<>'ESP','S',' ')
        endif
        .DoRTCalc(15,16,.t.)
        if .o_TIPDIS<>.w_TIPDIS
            .w_DESPAR = 'N'
        endif
        if .o_TIPDIS<>.w_TIPDIS
            .w_TIPRID = 'N'
        endif
        if .o_TIPDIS<>.w_TIPDIS
            .w_CABONURG = 'N'
        endif
        if .o_TIPDIS<>.w_TIPDIS
            .w_FLSEPA = IIF(.w_TIPDIS $ 'SC-SD-SE','S','N')
        endif
        .DoRTCalc(21,21,.t.)
        if .o_FLSEPA<>.w_FLSEPA.or. .o_TIPDIS<>.w_TIPDIS
            .w_INFMAND = IIF(.w_TIPDIS = 'SD','S','N')
        endif
        if .o_TIPDIS_1<>.w_TIPDIS_1.or. .o_TIPDIS_2<>.w_TIPDIS_2
            .w_TIPRIC = '0'
        endif
        .DoRTCalc(24,24,.t.)
            .w_BANCA = Nvl( .w_zoomef.getvar('DIBANRIF') , Space(15) )
          .link_1_43('Full')
        .DoRTCalc(26,26,.t.)
          .link_1_45('Full')
        .DoRTCalc(28,29,.t.)
        if .o_NOMEFILE1<>.w_NOMEFILE1.or. .o_NOMEFILE6<>.w_NOMEFILE6
            .w_NOMEFILE5 = IIF(LEN(.w_NOMEFILE6) <=8,SPACE(254),.w_NOMEFILE6)
        endif
        if .o_TIPDIS<>.w_TIPDIS.or. .o_NUMERO1<>.w_NUMERO1.or. .o_ANNO1<>.w_ANNO1.or. .o_DATA1<>.w_DATA1
            .w_NOMEFILE1 = ALLTRIM(IIF(.w_TIPPAG='S', icase(.w_TIPDIS='RB',"RIBA\", .w_TIPDIS$ 'RI' ,'RID\', .w_TIPDIS='MA' ,'MAV\' ,.w_TIPDIS='SD','SDD\',.w_TIPDIS='SC','SCT\',.w_TIPDIS='SE','SCE\',"BONI\"),' '))
        endif
        if .o_NOMEFILE1<>.w_NOMEFILE1.or. .o_NOMEFILE5<>.w_NOMEFILE5
            .w_NOMEFILE2 = IIF(EMPTY(.w_TESTFILE), left(sys(5)+sys(2003),40) +'\', .w_TESTFILE)
        endif
            .w_NOMEFILE3 = ALLTRIM(IIF(.w_TIPBAN $ 'CD', iif(.w_TIPBAN='C', ALLTRIM(.w_BANCA) +ALLTRIM(IIF(EMPTY(.w_BANCA), ' ', '\')),ALLTRIM(.w_DESBAN) +ALLTRIM(IIF(EMPTY(.w_DESBAN), ' ', '\'))), ' '))
        if .o_NUMERO1<>.w_NUMERO1.or. .o_DATA1<>.w_DATA1.or. .o_TIPDIS<>.w_TIPDIS
            .w_NOMEFILE4 = IIF(.w_NOMEFILE4='0', ' ', icase(.w_TIPDIS='RB','R',.w_TIPDIS $ 'RI' ,'D', .w_TIPDIS='MA' ,'M' ,.w_TIPDIS = 'BO','B','') +iif(.w_TIPDIS$ 'SE-SC-SD',Alltrim(.w_TIPDIS)+.w_DISERIAL,RIGHT('0'+ALLTRIM(STR(DAY(.w_DATA1))),2)+RIGHT('0'+ALLTRIM(STR(MONTH(.w_DATA1))),2)+ RIGHT(ALLTRIM(STR(YEAR(.w_DATA1))), 2) +ALLTRIM(STR(.w_PROG))))
        endif
        Local l_Dep1,l_Dep2
        l_Dep1= .o_TIPDIS<>.w_TIPDIS .or. .o_NOMEFILE3<>.w_NOMEFILE3 .or. .o_NOMEFILE1<>.w_NOMEFILE1 .or. .o_NOMEFILE5<>.w_NOMEFILE5 .or. .o_NOMEFILE2<>.w_NOMEFILE2        l_Dep2= .o_NUMERO1<>.w_NUMERO1 .or. .o_NOMEFILE4<>.w_NOMEFILE4
        if m.l_Dep1 .or. m.l_Dep2
            .w_NOMEFILE = IIF(NOT EMPTY(.w_NOMEFILE5) AND NOT LEN(.w_NOMEFILE5)<=8, .w_NOMEFILE5, ALLTRIM(.w_NOMEFILE2)+ALLTRIM(.w_NOMEFILE3)+ALLTRIM(.w_NOMEFILE1))
        endif
        .DoRTCalc(36,39,.t.)
            .w_NOME = IIF(.w_NOMEFILE4='0', ' ', iif(.w_TIPDIS='RB','R', iif(.w_TIPDIS='RI' ,'D', iif(.w_TIPDIS='MA' ,'M' ,'B'))) +RIGHT('0'+ALLTRIM(STR(DAY(.w_DATA1))),2)+RIGHT('0'+ALLTRIM(STR(MONTH(.w_DATA1))),2)+ RIGHT(ALLTRIM(STR(YEAR(.w_DATA1))), 2) +ALLTRIM(STR(.w_PROG)))
        if .o_NOMEFILE4<>.w_NOMEFILE4.or. .o_NOMEFILE5<>.w_NOMEFILE5
            .w_NOMEFILE7 = IIF(NOT EMPTY(.w_NOMEFILE5), .w_NOMEFILE4, SPACE(8))
        endif
        if .o_FLSEPA<>.w_FLSEPA
          .Calculate_DQDDPSJZZC()
        endif
            .w_DICAURIF = Nvl( .w_zoomef.getvar('DICAURIF') , Space(5) )
        * --- Area Manuale = Calculate
        * --- gste_krf
        if .w_NOMEFILE4<>.w_NOME
           .w_NOMEFILE4 = .w_NOMEFILE4
        endif
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(43,44,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomEF.Calculate(.F.)
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate()
    endwith
  return

  proc Calculate_DQDDPSJZZC()
    with this
          * --- Inizializza FLIBAN
          .w_FLIBAN = IIF (.w_FLSEPA='S','S',.w_FLIBAN)
    endwith
  endproc
  proc Calculate_WOKYEHMGNI()
    with this
          * --- Apro distinta
          .a = opengest('A','GSTE_ADI','DINUMDIS',.w_DISERIAL)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLIBAN_1_27.enabled = this.oPgFrm.Page1.oPag.oFLIBAN_1_27.mCond()
    this.oPgFrm.Page1.oPag.oFLQUALI_1_29.enabled = this.oPgFrm.Page1.oPag.oFLQUALI_1_29.mCond()
    this.oPgFrm.Page1.oPag.oFLSEPA_1_33.enabled = this.oPgFrm.Page1.oPag.oFLSEPA_1_33.mCond()
    this.oPgFrm.Page1.oPag.oCODRIF_1_34.enabled = this.oPgFrm.Page1.oPag.oCODRIF_1_34.mCond()
    this.oPgFrm.Page1.oPag.oINFMAND_1_35.enabled = this.oPgFrm.Page1.oPag.oINFMAND_1_35.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPDIS_1_1_3.visible=!this.oPgFrm.Page1.oPag.oTIPDIS_1_1_3.mHide()
    this.oPgFrm.Page1.oPag.oTIPDIS_2_1_4.visible=!this.oPgFrm.Page1.oPag.oTIPDIS_2_1_4.mHide()
    this.oPgFrm.Page1.oPag.oFLIBAN_1_27.visible=!this.oPgFrm.Page1.oPag.oFLIBAN_1_27.mHide()
    this.oPgFrm.Page1.oPag.oFLQUALI_1_29.visible=!this.oPgFrm.Page1.oPag.oFLQUALI_1_29.mHide()
    this.oPgFrm.Page1.oPag.oDESPAR_1_30.visible=!this.oPgFrm.Page1.oPag.oDESPAR_1_30.mHide()
    this.oPgFrm.Page1.oPag.oTIPRID_1_31.visible=!this.oPgFrm.Page1.oPag.oTIPRID_1_31.mHide()
    this.oPgFrm.Page1.oPag.oCABONURG_1_32.visible=!this.oPgFrm.Page1.oPag.oCABONURG_1_32.mHide()
    this.oPgFrm.Page1.oPag.oFLSEPA_1_33.visible=!this.oPgFrm.Page1.oPag.oFLSEPA_1_33.mHide()
    this.oPgFrm.Page1.oPag.oCODRIF_1_34.visible=!this.oPgFrm.Page1.oPag.oCODRIF_1_34.mHide()
    this.oPgFrm.Page1.oPag.oINFMAND_1_35.visible=!this.oPgFrm.Page1.oPag.oINFMAND_1_35.mHide()
    this.oPgFrm.Page1.oPag.oTIPRIC_1_36.visible=!this.oPgFrm.Page1.oPag.oTIPRIC_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_60.visible=!this.oPgFrm.Page1.oPag.oStr_1_60.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_65.visible=!this.oPgFrm.Page1.oPag.oBtn_1_65.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomEF.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
        if lower(cEvent)==lower("w_ZoomEF selected")
          .Calculate_WOKYEHMGNI()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gste_krf
    if Cevent ='w_TIPDIS_1 Changed' or Cevent ='w_TIPDIS_2 Changed'
       This.w_HTML_URL=' '
       this.mHideControls()
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=BANCA
  func Link_1_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BANCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BANCA)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANCA = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBAN = NVL(_Link_.BADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_BANCA = space(15)
      endif
      this.w_DESBAN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,CO__PATH,COTIPBAN,COTIPPAG";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,CO__PATH,COTIPBAN,COTIPPAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_TESTFILE = NVL(_Link_.CO__PATH,space(254))
      this.w_TIPBAN = NVL(_Link_.COTIPBAN,space(1))
      this.w_TIPPAG = NVL(_Link_.COTIPPAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_TESTFILE = space(254)
      this.w_TIPBAN = space(1)
      this.w_TIPPAG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATPRE_1_1.value==this.w_DATPRE)
      this.oPgFrm.Page1.oPag.oDATPRE_1_1.value=this.w_DATPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oFLEURO_1_2.RadioValue()==this.w_FLEURO)
      this.oPgFrm.Page1.oPag.oFLEURO_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDIS_1_1_3.RadioValue()==this.w_TIPDIS_1)
      this.oPgFrm.Page1.oPag.oTIPDIS_1_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDIS_2_1_4.RadioValue()==this.w_TIPDIS_2)
      this.oPgFrm.Page1.oPag.oTIPDIS_2_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO1_1_18.value==this.w_NUMERO1)
      this.oPgFrm.Page1.oPag.oNUMERO1_1_18.value=this.w_NUMERO1
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO1_1_20.value==this.w_ANNO1)
      this.oPgFrm.Page1.oPag.oANNO1_1_20.value=this.w_ANNO1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_21.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_21.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_23.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_23.value=this.w_DESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_24.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_24.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDIS_1_25.value==this.w_DATDIS)
      this.oPgFrm.Page1.oPag.oDATDIS_1_25.value=this.w_DATDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPGEN_1_26.RadioValue()==this.w_TIPGEN)
      this.oPgFrm.Page1.oPag.oTIPGEN_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLIBAN_1_27.RadioValue()==this.w_FLIBAN)
      this.oPgFrm.Page1.oPag.oFLIBAN_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDADATA_1_28.value==this.w_DADATA)
      this.oPgFrm.Page1.oPag.oDADATA_1_28.value=this.w_DADATA
    endif
    if not(this.oPgFrm.Page1.oPag.oFLQUALI_1_29.RadioValue()==this.w_FLQUALI)
      this.oPgFrm.Page1.oPag.oFLQUALI_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAR_1_30.RadioValue()==this.w_DESPAR)
      this.oPgFrm.Page1.oPag.oDESPAR_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPRID_1_31.RadioValue()==this.w_TIPRID)
      this.oPgFrm.Page1.oPag.oTIPRID_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCABONURG_1_32.RadioValue()==this.w_CABONURG)
      this.oPgFrm.Page1.oPag.oCABONURG_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSEPA_1_33.RadioValue()==this.w_FLSEPA)
      this.oPgFrm.Page1.oPag.oFLSEPA_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODRIF_1_34.RadioValue()==this.w_CODRIF)
      this.oPgFrm.Page1.oPag.oCODRIF_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINFMAND_1_35.RadioValue()==this.w_INFMAND)
      this.oPgFrm.Page1.oPag.oINFMAND_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPRIC_1_36.RadioValue()==this.w_TIPRIC)
      this.oPgFrm.Page1.oPag.oTIPRIC_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_38.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_38.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMEFILE4_1_52.value==this.w_NOMEFILE4)
      this.oPgFrm.Page1.oPag.oNOMEFILE4_1_52.value=this.w_NOMEFILE4
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMEFILE_1_53.value==this.w_NOMEFILE)
      this.oPgFrm.Page1.oPag.oNOMEFILE_1_53.value=this.w_NOMEFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oSEPREC_1_56.RadioValue()==this.w_SEPREC)
      this.oPgFrm.Page1.oPag.oSEPREC_1_56.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATPRE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATPRE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DATPRE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NOMEFILE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOMEFILE_1_53.SetFocus()
            i_bnoObbl = !empty(.w_NOMEFILE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATPRE = this.w_DATPRE
    this.o_TIPDIS_1 = this.w_TIPDIS_1
    this.o_TIPDIS_2 = this.w_TIPDIS_2
    this.o_TIPDIS = this.w_TIPDIS
    this.o_NUMERO1 = this.w_NUMERO1
    this.o_ANNO1 = this.w_ANNO1
    this.o_DATA1 = this.w_DATA1
    this.o_FLSEPA = this.w_FLSEPA
    this.o_NOMEFILE5 = this.w_NOMEFILE5
    this.o_NOMEFILE1 = this.w_NOMEFILE1
    this.o_NOMEFILE2 = this.w_NOMEFILE2
    this.o_NOMEFILE3 = this.w_NOMEFILE3
    this.o_NOMEFILE4 = this.w_NOMEFILE4
    this.o_NOMEFILE6 = this.w_NOMEFILE6
    return

enddefine

* --- Define pages as container
define class tgste_krfPag1 as StdContainer
  Width  = 687
  height = 560
  stdWidth  = 687
  stdheight = 560
  resizeXpos=579
  resizeYpos=321
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATPRE_1_1 as StdField with uid="MHYQZWAOLP",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATPRE", cQueryName = "DATPRE",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di presentazione degli effetti",;
    HelpContextID = 59461942,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=464, Top=6

  add object oFLEURO_1_2 as StdCheck with uid="XKNGRAJSLT",rtseq=2,rtrep=.f.,left=564, top=6, caption="Importi in Euro",;
    ToolTipText = "Se attivo: gli importi delle scadenze in valuta EMU saranno stampati convertiti in Euro",;
    HelpContextID = 227503190,;
    cFormVar="w_FLEURO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLEURO_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLEURO_1_2.GetRadio()
    this.Parent.oContained.w_FLEURO = this.RadioValue()
    return .t.
  endfunc

  func oFLEURO_1_2.SetRadio()
    this.Parent.oContained.w_FLEURO=trim(this.Parent.oContained.w_FLEURO)
    this.value = ;
      iif(this.Parent.oContained.w_FLEURO=='S',1,;
      0)
  endfunc


  add object oTIPDIS_1_1_3 as StdCombo with uid="FUMJWALANW",rtseq=3,rtrep=.f.,left=38,top=29,width=157,height=21;
    , ToolTipText = "Tipo di distinta da selezionare";
    , HelpContextID = 15669863;
    , cFormVar="w_TIPDIS_1",RowSource=""+"Ric.bancarie,"+"Bonifici nazionali,"+"Bonifici esteri,"+"R.I.D.,"+"M.AV.,"+"SDD - Sepa Direct Debit,"+"SCT - Sepa Credit Transfer,"+"SCT estero - Sepa Credit Transfer", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPDIS_1_1_3.RadioValue()
    return(iif(this.value =1,'RB',;
    iif(this.value =2,'BO',;
    iif(this.value =3,'BE',;
    iif(this.value =4,'RI',;
    iif(this.value =5,'MA',;
    iif(this.value =6,'SD',;
    iif(this.value =7,'SC',;
    iif(this.value =8,'SE',;
    space(2))))))))))
  endfunc
  func oTIPDIS_1_1_3.GetRadio()
    this.Parent.oContained.w_TIPDIS_1 = this.RadioValue()
    return .t.
  endfunc

  func oTIPDIS_1_1_3.SetRadio()
    this.Parent.oContained.w_TIPDIS_1=trim(this.Parent.oContained.w_TIPDIS_1)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDIS_1=='RB',1,;
      iif(this.Parent.oContained.w_TIPDIS_1=='BO',2,;
      iif(this.Parent.oContained.w_TIPDIS_1=='BE',3,;
      iif(this.Parent.oContained.w_TIPDIS_1=='RI',4,;
      iif(this.Parent.oContained.w_TIPDIS_1=='MA',5,;
      iif(this.Parent.oContained.w_TIPDIS_1=='SD',6,;
      iif(this.Parent.oContained.w_TIPDIS_1=='SC',7,;
      iif(this.Parent.oContained.w_TIPDIS_1=='SE',8,;
      0))))))))
  endfunc

  func oTIPDIS_1_1_3.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ITA')
    endwith
  endfunc


  add object oTIPDIS_2_1_4 as StdCombo with uid="EDLZBCDXEM",rtseq=4,rtrep=.f.,left=36,top=29,width=157,height=21;
    , ToolTipText = "Tipo de nota (trasferencias, pagos certificados, recibos domiciliados a la vista, remesas/anticipos de efectos )";
    , HelpContextID = 15669864;
    , cFormVar="w_TIPDIS_2",RowSource=""+"Recibos domiciliados a la vista,"+"Transferencias,"+"Remesas/anticipos de efectos,"+"SDD - Sepa Direct Debit", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPDIS_2_1_4.RadioValue()
    return(iif(this.value =1,'RI',;
    iif(this.value =2,'BO',;
    iif(this.value =3,'RB',;
    iif(this.value =4,'SD',;
    space(2))))))
  endfunc
  func oTIPDIS_2_1_4.GetRadio()
    this.Parent.oContained.w_TIPDIS_2 = this.RadioValue()
    return .t.
  endfunc

  func oTIPDIS_2_1_4.SetRadio()
    this.Parent.oContained.w_TIPDIS_2=trim(this.Parent.oContained.w_TIPDIS_2)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDIS_2=='RI',1,;
      iif(this.Parent.oContained.w_TIPDIS_2=='BO',2,;
      iif(this.Parent.oContained.w_TIPDIS_2=='RB',3,;
      iif(this.Parent.oContained.w_TIPDIS_2=='SD',4,;
      0))))
  endfunc

  func oTIPDIS_2_1_4.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc


  add object oBtn_1_6 as StdButton with uid="UPCNOTQLZP",left=631, top=29, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare";
    , HelpContextID = 60910102;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        .NotifyEvent('Esegui')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_8 as StdButton with uid="DFZTGVXEZM",left=657, top=478, width=22,height=22,;
    caption="...", nPag=1;
    , HelpContextID = 115811114;
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      with this.Parent.oContained
        .w_NOMEFILE6=left(cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+space(40),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="NGLHNSSAFP",left=581, top=504, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 115983386;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        do GSTE_BRF with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((NOT EMPTY(.w_NOMEFILE)) AND (NOT EMPTY(.w_DISERIAL)))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="UHWKAPPYLM",left=631, top=504, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 108694714;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomEF as cp_zoombox with uid="LIBHNDZVAX",left=3, top=178, width=678,height=268,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DIS_TINT",cZoomFile="GSTE_ZRF",bOptions=.f.,bQueryOnLoad=.f.,bAdvOptions=.t.,cZoomOnZoom="",cMenuFile="",bReadOnly=.t.,bRetriveAllRows=.f.,;
    cEvent = "Esegui,Init",;
    nPag=1;
    , HelpContextID = 61985510


  add object oObj_1_16 as cp_runprogram with uid="MPVMMBDZNE",left=3, top=572, width=212,height=16,;
    caption='GSTE_BR1',;
   bGlobalFont=.t.,;
    prg="GSTE_BR1",;
    cEvent = "w_TIPDIS Changed",;
    nPag=1;
    , HelpContextID = 22045591

  add object oNUMERO1_1_18 as StdField with uid="XJGRHZGYTP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMERO1", cQueryName = "NUMERO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero della distinta selezionata",;
    HelpContextID = 226489814,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=145, Top=453, cSayPict='"999999"', cGetPict='"999999"'

  add object oANNO1_1_20 as StdField with uid="QPDCJTFGDU",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ANNO1", cQueryName = "ANNO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno della distinta selezionata",;
    HelpContextID = 59113978,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=221, Top=453, InputMask=replicate('X',4)

  add object oDATA1_1_21 as StdField with uid="SNFCYWKJTE",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della distinta selezionata",;
    HelpContextID = 60010186,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=298, Top=453

  add object oDESCRI1_1_23 as StdField with uid="QIUDSOAQYH",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della distinta selezionata",;
    HelpContextID = 125715766,;
   bGlobalFont=.t.,;
    Height=21, Width=302, Left=377, Top=453, InputMask=replicate('X',35)

  add object oNUMERO_1_24 as StdField with uid="WJPVMCWSUZ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero distinta selezionata",;
    HelpContextID = 226489814,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=292, Top=29, cSayPict='"999999"', cGetPict='"999999"'

  add object oDATDIS_1_25 as StdField with uid="DKGOFIFZSV",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATDIS", cQueryName = "DATDIS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data distinta selezionata",;
    HelpContextID = 15683894,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=464, Top=29


  add object oTIPGEN_1_26 as StdCombo with uid="ZVWLBCLUXT",rtseq=13,rtrep=.f.,left=367,top=84,width=139,height=21;
    , ToolTipText = "Tipo generazione";
    , HelpContextID = 196221494;
    , cFormVar="w_TIPGEN",RowSource=""+"Nuovo file,"+"Rigenera file", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPGEN_1_26.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oTIPGEN_1_26.GetRadio()
    this.Parent.oContained.w_TIPGEN = this.RadioValue()
    return .t.
  endfunc

  func oTIPGEN_1_26.SetRadio()
    this.Parent.oContained.w_TIPGEN=trim(this.Parent.oContained.w_TIPGEN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPGEN=='N',1,;
      iif(this.Parent.oContained.w_TIPGEN=='S',2,;
      0))
  endfunc

  add object oFLIBAN_1_27 as StdCheck with uid="EPWPJMHBJO",rtseq=14,rtrep=.f.,left=38, top=55, caption="Generazione codice IBAN (record 16/17)",;
    ToolTipText = "Se attivo genera anche i record 16 e 17 nel tracciato CBI",;
    HelpContextID = 191671382,;
    cFormVar="w_FLIBAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLIBAN_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLIBAN_1_27.GetRadio()
    this.Parent.oContained.w_FLIBAN = this.RadioValue()
    return .t.
  endfunc

  func oFLIBAN_1_27.SetRadio()
    this.Parent.oContained.w_FLIBAN=trim(this.Parent.oContained.w_FLIBAN)
    this.value = ;
      iif(this.Parent.oContained.w_FLIBAN=='S',1,;
      0)
  endfunc

  func oFLIBAN_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDIS $ 'RI-MA' AND .w_FLSEPA<>'S')
    endwith
   endif
  endfunc

  func oFLIBAN_1_27.mHide()
    with this.Parent.oContained
      return (NOT(.w_TIPDIS $ 'BO-RI-MA') OR g_ISONAZ='ESP')
    endwith
  endfunc

  add object oDADATA_1_28 as StdField with uid="MNBEIRUAQH",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DADATA", cQueryName = "DADATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Filtra le distinte a partire da una certa data",;
    HelpContextID = 261837110,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=602, Top=84

  add object oFLQUALI_1_29 as StdCheck with uid="GLZKJBZKKH",rtseq=16,rtrep=.f.,left=38, top=74, caption="Valorizzazione qualificatore di flusso",;
    ToolTipText = "Se attivo valorizza il qualificatore flusso",;
    HelpContextID = 109040554,;
    cFormVar="w_FLQUALI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLQUALI_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLQUALI_1_29.GetRadio()
    this.Parent.oContained.w_FLQUALI = this.RadioValue()
    return .t.
  endfunc

  func oFLQUALI_1_29.SetRadio()
    this.Parent.oContained.w_FLQUALI=trim(this.Parent.oContained.w_FLQUALI)
    this.value = ;
      iif(this.Parent.oContained.w_FLQUALI=='S',1,;
      0)
  endfunc

  func oFLQUALI_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDIS='BO')
    endwith
   endif
  endfunc

  func oFLQUALI_1_29.mHide()
    with this.Parent.oContained
      return (.w_TIPDIS<>'BO')
    endwith
  endfunc

  add object oDESPAR_1_30 as StdCheck with uid="LTLHTASJWR",rtseq=17,rtrep=.f.,left=38, top=74, caption="Riporta descrizione partita",;
    ToolTipText = "Se attivo riporta la descrizione della partita nel file generato",;
    HelpContextID = 259736886,;
    cFormVar="w_DESPAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDESPAR_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDESPAR_1_30.GetRadio()
    this.Parent.oContained.w_DESPAR = this.RadioValue()
    return .t.
  endfunc

  func oDESPAR_1_30.SetRadio()
    this.Parent.oContained.w_DESPAR=trim(this.Parent.oContained.w_DESPAR)
    this.value = ;
      iif(this.Parent.oContained.w_DESPAR=='S',1,;
      0)
  endfunc

  func oDESPAR_1_30.mHide()
    with this.Parent.oContained
      return (Not (.w_TIPDIS='RI' OR .w_TIPDIS='RB'))
    endwith
  endfunc

  add object oTIPRID_1_31 as StdCheck with uid="IONOTNIDCI",rtseq=18,rtrep=.f.,left=38, top=94, caption="Genera R.I.D. veloce",;
    ToolTipText = "Se attivato, nella generazione flussi identifica tipologia di incasso R.I.D. veloce",;
    HelpContextID = 33364534,;
    cFormVar="w_TIPRID", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPRID_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    space(10)))
  endfunc
  func oTIPRID_1_31.GetRadio()
    this.Parent.oContained.w_TIPRID = this.RadioValue()
    return .t.
  endfunc

  func oTIPRID_1_31.SetRadio()
    this.Parent.oContained.w_TIPRID=trim(this.Parent.oContained.w_TIPRID)
    this.value = ;
      iif(this.Parent.oContained.w_TIPRID=='S',1,;
      0)
  endfunc

  func oTIPRID_1_31.mHide()
    with this.Parent.oContained
      return (!g_ISONAZ $ 'ITA-ESP' Or ! .w_TIPDIS = 'RI')
    endwith
  endfunc

  add object oCABONURG_1_32 as StdCheck with uid="HBGKGRRQIQ",rtseq=19,rtrep=.f.,left=38, top=94, caption="Genera bonifico urgente",;
    ToolTipText = "Se attivato, nella generazione flussi idetifica tipologia di pagamento bonifico urgente",;
    HelpContextID = 55128429,;
    cFormVar="w_CABONURG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCABONURG_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCABONURG_1_32.GetRadio()
    this.Parent.oContained.w_CABONURG = this.RadioValue()
    return .t.
  endfunc

  func oCABONURG_1_32.SetRadio()
    this.Parent.oContained.w_CABONURG=trim(this.Parent.oContained.w_CABONURG)
    this.value = ;
      iif(this.Parent.oContained.w_CABONURG=='S',1,;
      0)
  endfunc

  func oCABONURG_1_32.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ITA' Or ! .w_TIPDIS $ 'BO-SC')
    endwith
  endfunc

  add object oFLSEPA_1_33 as StdCheck with uid="SOSTDXPPIT",rtseq=20,rtrep=.f.,left=38, top=113, caption="SEPA",;
    ToolTipText = "Se attivo viene generato un file secondo lo standard CBI compatibile con le specifiche SEPA",;
    HelpContextID = 257969238,;
    cFormVar="w_FLSEPA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSEPA_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLSEPA_1_33.GetRadio()
    this.Parent.oContained.w_FLSEPA = this.RadioValue()
    return .t.
  endfunc

  func oFLSEPA_1_33.SetRadio()
    this.Parent.oContained.w_FLSEPA=trim(this.Parent.oContained.w_FLSEPA)
    this.value = ;
      iif(this.Parent.oContained.w_FLSEPA=='S',1,;
      0)
  endfunc

  func oFLSEPA_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDIS $ 'RI-BO' )
    endwith
   endif
  endfunc

  func oFLSEPA_1_33.mHide()
    with this.Parent.oContained
      return (NOT(.w_TIPDIS $ 'BO-RI') OR g_ISONAZ='ESP')
    endwith
  endfunc

  add object oCODRIF_1_34 as StdCheck with uid="DGJAMRVTXI",rtseq=21,rtrep=.f.,left=38, top=133, caption="Codice di riferimento",;
    ToolTipText = "Se attivo, viene riportato nel record 70 il seriale della distinta invece del numero effetto",;
    HelpContextID = 66871078,;
    cFormVar="w_CODRIF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCODRIF_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCODRIF_1_34.GetRadio()
    this.Parent.oContained.w_CODRIF = this.RadioValue()
    return .t.
  endfunc

  func oCODRIF_1_34.SetRadio()
    this.Parent.oContained.w_CODRIF=trim(this.Parent.oContained.w_CODRIF)
    this.value = ;
      iif(this.Parent.oContained.w_CODRIF=='S',1,;
      0)
  endfunc

  func oCODRIF_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDIS <> 'SD')
    endwith
   endif
  endfunc

  func oCODRIF_1_34.mHide()
    with this.Parent.oContained
      return (.w_FLSEPA<>'S' OR .w_TIPDIS $ 'BO-SC-SE-SD' )
    endwith
  endfunc

  add object oINFMAND_1_35 as StdCheck with uid="PCDSHWIBQD",rtseq=22,rtrep=.f.,left=38, top=153, caption="Informazioni mandato",;
    ToolTipText = "Se attivo, vengono riportati nel file il tipo sequenza e la data di sottoscrizione del mandato",;
    HelpContextID = 192380550,;
    cFormVar="w_INFMAND", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oINFMAND_1_35.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oINFMAND_1_35.GetRadio()
    this.Parent.oContained.w_INFMAND = this.RadioValue()
    return .t.
  endfunc

  func oINFMAND_1_35.SetRadio()
    this.Parent.oContained.w_INFMAND=trim(this.Parent.oContained.w_INFMAND)
    this.value = ;
      iif(this.Parent.oContained.w_INFMAND=='S',1,;
      0)
  endfunc

  func oINFMAND_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDIS <> 'SD')
    endwith
   endif
  endfunc

  func oINFMAND_1_35.mHide()
    with this.Parent.oContained
      return (.w_FLSEPA<>'S' OR .w_TIPDIS $ 'BO-SC-SE-SD' )
    endwith
  endfunc


  add object oTIPRIC_1_36 as StdCombo with uid="LLVPPRGILU",rtseq=23,rtrep=.f.,left=367,top=56,width=174,height=21;
    , ToolTipText = "Nel caso di bonifico nazionale identifica il tipo di esito delle disposizioni di pagamento richiesto dal cliente ordinante";
    , HelpContextID = 16587318;
    , cFormVar="w_TIPRIC",RowSource=""+"Richiesta ordinato,"+"Richiesta storno,"+"Entrambi,"+"Esito positivo non richiesto,"+"Blank", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPRIC_1_36.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'0',;
    space(2)))))))
  endfunc
  func oTIPRIC_1_36.GetRadio()
    this.Parent.oContained.w_TIPRIC = this.RadioValue()
    return .t.
  endfunc

  func oTIPRIC_1_36.SetRadio()
    this.Parent.oContained.w_TIPRIC=trim(this.Parent.oContained.w_TIPRIC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPRIC=='1',1,;
      iif(this.Parent.oContained.w_TIPRIC=='2',2,;
      iif(this.Parent.oContained.w_TIPRIC=='3',3,;
      iif(this.Parent.oContained.w_TIPRIC=='4',4,;
      iif(this.Parent.oContained.w_TIPRIC=='0',5,;
      0)))))
  endfunc

  func oTIPRIC_1_36.mHide()
    with this.Parent.oContained
      return (.w_TIPDIS <>'BO' OR g_ISONAZ='ESP')
    endwith
  endfunc

  add object oANNO_1_38 as StdField with uid="BYAHSNRWJA",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di riferimento selezionato",;
    HelpContextID = 110494202,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=367, Top=29, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  add object oNOMEFILE4_1_52 as StdField with uid="PQMMADIBLN",rtseq=34,rtrep=.f.,;
    cFormVar = "w_NOMEFILE4", cQueryName = "NOMEFILE4",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nome del file ASCII da generare",;
    HelpContextID = 155192485,;
   bGlobalFont=.t.,;
    Height=21, Width=103, Left=552, Top=477, InputMask=replicate('X',254)

  add object oNOMEFILE_1_53 as StdField with uid="TTPJUXOLLP",rtseq=35,rtrep=.f.,;
    cFormVar = "w_NOMEFILE", cQueryName = "NOMEFILE",;
    bObbl = .t. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso del file ASCII da generare",;
    HelpContextID = 155193317,;
   bGlobalFont=.t.,;
    Height=21, Width=403, Left=145, Top=477, InputMask=replicate('X',254), readonly=.t.,TabStop=.f.


  add object oSEPREC_1_56 as StdCombo with uid="JBRPDLYBVK",rtseq=38,rtrep=.f.,left=145,top=504,width=130,height=21;
    , ToolTipText = "Caratteri utilizzati come separatori dei records";
    , HelpContextID = 12391974;
    , cFormVar="w_SEPREC",RowSource=""+"Cr + linefeed,"+"Nessun separatore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSEPREC_1_56.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oSEPREC_1_56.GetRadio()
    this.Parent.oContained.w_SEPREC = this.RadioValue()
    return .t.
  endfunc

  func oSEPREC_1_56.SetRadio()
    this.Parent.oContained.w_SEPREC=trim(this.Parent.oContained.w_SEPREC)
    this.value = ;
      iif(this.Parent.oContained.w_SEPREC=='S',1,;
      iif(this.Parent.oContained.w_SEPREC=='N',2,;
      0))
  endfunc


  add object oBtn_1_65 as StdButton with uid="ZQIEYZWFVL",left=525, top=504, width=48,height=45,;
    CpPicture="..\exe\bmp\GENERA.ico", caption="", nPag=1;
    , ToolTipText = "Apri file CBI/SEPA";
    , HelpContextID = 108634106;
    , Caption='A\<pri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_65.Click()
      do GSTE_KSX with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_65.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_HTML_URL))
     endwith
    endif
  endfunc

  add object oStr_1_12 as StdString with uid="LZFVOQHZFL",Visible=.t., Left=12, Top=504,;
    Alignment=1, Width=130, Height=15,;
    Caption="Separatore records:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="UEXWYUOMBJ",Visible=.t., Left=346, Top=6,;
    Alignment=1, Width=117, Height=15,;
    Caption="Data elaborazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="GEXOBCNFOR",Visible=.t., Left=6, Top=28,;
    Alignment=1, Width=29, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="OHKRYAMIWK",Visible=.t., Left=20, Top=478,;
    Alignment=1, Width=122, Height=18,;
    Caption="Percorso/nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="OGZXOYCHYY",Visible=.t., Left=3, Top=453,;
    Alignment=1, Width=139, Height=15,;
    Caption="Distinta selezionata:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="LFOGFWZYGB",Visible=.t., Left=213, Top=453,;
    Alignment=0, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="SBCEUSXCHT",Visible=.t., Left=270, Top=453,;
    Alignment=1, Width=26, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="WXHGRILXTE",Visible=.t., Left=357, Top=29,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="KYFXBISHEB",Visible=.t., Left=208, Top=29,;
    Alignment=1, Width=80, Height=15,;
    Caption="Distinta n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="EQAEBAQMTO",Visible=.t., Left=423, Top=29,;
    Alignment=1, Width=40, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="NZLMQDIGTZ",Visible=.t., Left=8, Top=6,;
    Alignment=0, Width=187, Height=15,;
    Caption="Criteri di selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_60 as StdString with uid="KSNCFBYIHK",Visible=.t., Left=288, Top=56,;
    Alignment=1, Width=77, Height=18,;
    Caption="Tipo richiesta:"  ;
  , bGlobalFont=.t.

  func oStr_1_60.mHide()
    with this.Parent.oContained
      return (.w_TIPDIS <>'BO' OR g_ISONAZ='ESP')
    endwith
  endfunc

  add object oStr_1_62 as StdString with uid="SQUXNUWLEN",Visible=.t., Left=542, Top=84,;
    Alignment=1, Width=59, Height=18,;
    Caption="Dalla data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="ZYTAQXRNDP",Visible=.t., Left=283, Top=84,;
    Alignment=1, Width=83, Height=18,;
    Caption="Tipo gener.:"  ;
  , bGlobalFont=.t.

  add object oBox_1_42 as StdBox with uid="EFOXPTJDUC",left=8, top=24, width=227,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_krf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
