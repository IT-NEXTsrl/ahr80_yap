* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bm5                                                        *
*              Gestione campi fuori sequenza                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-09-29                                                      *
* Last revis.: 2010-10-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bm5",oParentObject)
return(i_retval)

define class tgsve_bm5 as StdBatch
  * --- Local variables
  w_GSVE_MDV = .NULL.
  w_TDSEQPRE = space(1)
  w_TDSEQSCO = space(1)
  w_TDSEQMA1 = space(1)
  w_TDSEQMA2 = space(1)
  w_TDSEQVALUE = space(1)
  w_TDSEQFIELD = space(10)
  w_CTRL = .NULL.
  * --- WorkFile variables
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione dei flag 'fuori sequenza' della causale del documento
    this.w_GSVE_MDV = this.oParentObject
    if this.w_GSVE_MDV.CURRENTEVENT <> "w_MVCODICE Changed" OR this.w_GSVE_MDV.ROWINDEX() = 1
      * --- L'operazione viene eseguita alla variazione della causale documento (w_GSVE_MDV.CURRENTEVENT <> "w_MVCODICE Changed")
      *     e alla prima variazione dell'articolo (w_GSVE_MDV.ROWINDEX() = 1)
      * --- Read from TIP_DOCU
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TDSEQPRE,TDSEQSCO,TDSEQMA1,TDSEQMA2"+;
          " from "+i_cTable+" TIP_DOCU where ";
              +"TDTIPDOC = "+cp_ToStrODBC(this.w_GSVE_MDV.w_MVTIPDOC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TDSEQPRE,TDSEQSCO,TDSEQMA1,TDSEQMA2;
          from (i_cTable) where;
              TDTIPDOC = this.w_GSVE_MDV.w_MVTIPDOC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TDSEQPRE = NVL(cp_ToDate(_read_.TDSEQPRE),cp_NullValue(_read_.TDSEQPRE))
        this.w_TDSEQSCO = NVL(cp_ToDate(_read_.TDSEQSCO),cp_NullValue(_read_.TDSEQSCO))
        this.w_TDSEQMA1 = NVL(cp_ToDate(_read_.TDSEQMA1),cp_NullValue(_read_.TDSEQMA1))
        this.w_TDSEQMA2 = NVL(cp_ToDate(_read_.TDSEQMA2),cp_NullValue(_read_.TDSEQMA2))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Per ogni oggetto si controlla la sua presenza fuori dalla griglia o nella griglia per gestire 
      *     - le differenze tra le varie gestioni
      *     - eventuali personalizzazioni o future modifiche
      * --- Prezzo
      this.w_TDSEQVALUE = NVL( this.w_TDSEQPRE, " " )
      this.w_TDSEQFIELD = "w_MVPREZZO"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Sconti
      this.w_TDSEQVALUE = NVL( this.w_TDSEQSCO, " " )
      this.w_TDSEQFIELD = "w_MVSCONT1"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_TDSEQFIELD = "w_MVSCONT2"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_TDSEQFIELD = "w_MVSCONT3"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_TDSEQFIELD = "w_MVSCONT4"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Magazzino principale
      this.w_TDSEQVALUE = NVL( this.w_TDSEQMA1, " " )
      this.w_TDSEQFIELD = "w_MVCODMAG"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Magazzino secondario
      this.w_TDSEQVALUE = NVL( this.w_TDSEQMA2, " " )
      this.w_TDSEQFIELD = "w_MVCODMAT"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione campi fuoi sequenza
    * --- w_TDSEQFIELD � il campo da mettere in sequenza o togliere dalla sequenza
    * --- w_TDSEQVALUE � il relativo flag della causale documento
    this.w_CTRL = this.w_GSVE_MDV.GETCTRL( this.w_TDSEQFIELD )
    if VARTYPE( this.w_CTRL ) <> "O"
      this.w_CTRL = this.w_GSVE_MDV.GETBODYCTRL( this.w_TDSEQFIELD )
    endif
    if VARTYPE( this.w_CTRL ) = "O"
      if this.w_TDSEQVALUE = "S"
        this.w_CTRL.TABSTOP = .F.
      else
        this.w_CTRL.TABSTOP = .T.
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='TIP_DOCU'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
