* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bds                                                        *
*              Chek cancelazione categoria provv.                              *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_3]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-06-01                                                      *
* Last revis.: 2006-03-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bds",oParentObject)
return(i_retval)

define class tgsar_bds as StdBatch
  * --- Local variables
  w_OK = .f.
  w_MESS = space(1)
  * --- WorkFile variables
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo cancellazione Categoria Provvigione da (GSAR_AGP)
    *     per mancanza di integrit� referenziale negli Articoli 
    if this.oParentObject.w_GPTIPPRO="A"
      this.w_OK = .F.
      * --- Select from ART_ICOL
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ARCODART  from "+i_cTable+" ART_ICOL ";
            +" where ARGRUPRO = "+cp_ToStrODBC(this.oParentObject.w_GPCODICE)+"";
             ,"_Curs_ART_ICOL")
      else
        select ARCODART from (i_cTable);
         where ARGRUPRO = this.oParentObject.w_GPCODICE;
          into cursor _Curs_ART_ICOL
      endif
      if used('_Curs_ART_ICOL')
        select _Curs_ART_ICOL
        locate for 1=1
        do while not(eof())
        this.w_OK = .T.
        Exit
          select _Curs_ART_ICOL
          continue
        enddo
        use
      endif
      if this.w_OK
        this.w_MESS = "Impossibile cancellare: la categoria provvigione � associata ad articoli"
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=ah_MsgFormat(this.w_MESS)
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ART_ICOL'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_ART_ICOL')
      use in _Curs_ART_ICOL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
