* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kcf                                                        *
*              Aggiornamento intestatari                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-05-03                                                      *
* Last revis.: 2018-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kcf",oParentObject))

* --- Class definition
define class tgsar_kcf as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 788
  Height = 647+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-17"
  HelpContextID=99186537
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=140

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  CATECOMM_IDX = 0
  ZONE_IDX = 0
  NAZIONI_IDX = 0
  CACOCLFO_IDX = 0
  MASTRI_IDX = 0
  VOCIIVA_IDX = 0
  TIPCODIV_IDX = 0
  TRI_BUTI_IDX = 0
  CAT_SCMA_IDX = 0
  GRUPRO_IDX = 0
  LISTINI_IDX = 0
  AGENTI_IDX = 0
  PAG_AMEN_IDX = 0
  COC_MAST_IDX = 0
  MAGAZZIN_IDX = 0
  cPrg = "gsar_kcf"
  cComment = "Aggiornamento intestatari"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_OBTEST = ctod('  /  /  ')
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_PBANCODI = space(15)
  w_PEANCODI = space(15)
  w_SEZIONE = space(1)
  o_SEZIONE = space(1)
  w_SEZIONE = space(1)
  w_PANCONSU = space(15)
  w_PANCONSU = space(15)
  w_CATCON = space(5)
  w_ANCODIVA = space(5)
  w_ANCATOPE = space(2)
  w_ANTIPOPE = space(10)
  w_ANFLRITE = space(1)
  o_ANFLRITE = space(1)
  w_ANCODIRP = space(5)
  w_ANCATCOM = space(3)
  w_ANCATSCM = space(5)
  w_ANCODZON = space(3)
  w_ANGRUPRO = space(5)
  w_ANCODAG1 = space(5)
  w_ANMAGTER = space(5)
  w_ANGESCON = space(1)
  w_ANPAGPAR = space(5)
  w_ANFLESIM = space(1)
  w_ANCODPAG = space(5)
  w_ANCODBA2 = space(15)
  w_ANFLESIM = space(1)
  w_ANRITENU = space(1)
  o_ANRITENU = space(1)
  w_ANCODIRP = space(5)
  w_ANCODTR2 = space(5)
  w_DESCR1 = space(40)
  w_DESCR2 = space(40)
  w_DESCCC = space(35)
  w_AGGMASCON = space(1)
  o_AGGMASCON = space(1)
  w_NEWMASCON = space(15)
  w_NEWMASCON = space(15)
  w_NULIV = 0
  w_TIPMAS = space(1)
  w_AGGCATCON = space(1)
  o_AGGCATCON = space(1)
  w_NEWCATCON = space(5)
  w_AGGANCODIVA = space(1)
  o_AGGANCODIVA = space(1)
  w_NEWANCODIVA = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_AGGANTIPOPE = space(1)
  o_AGGANTIPOPE = space(1)
  w_NEWANTIPOPE = space(10)
  w_TIDTOBSO = ctod('  /  /  ')
  w_AGGANPARTSN = space(1)
  w_AGGANFLRITE = space(1)
  o_AGGANFLRITE = space(1)
  w_AGGANCODIRP = space(1)
  o_AGGANCODIRP = space(1)
  w_NEWANCODIRP = space(5)
  o_NEWANCODIRP = space(5)
  w_AGGANCAURIT = space(1)
  o_AGGANCAURIT = space(1)
  w_NEWANCAURIT = space(1)
  w_AGGANFLFIDO = space(1)
  w_AGGANCATCOM = space(1)
  o_AGGANCATCOM = space(1)
  w_NEWANCATCOM = space(3)
  w_AGGANCATSCM = space(1)
  o_AGGANCATSCM = space(1)
  w_NEWANCATSCM = space(5)
  w_AGGANCODZON = space(1)
  o_AGGANCODZON = space(1)
  w_NEWANCODZON = space(3)
  w_AGGANGRUPRO = space(1)
  o_AGGANGRUPRO = space(1)
  w_NEWANGRUPRO = space(5)
  w_DATOBSOA = ctod('  /  /  ')
  w_AGGANCODAG1 = space(1)
  o_AGGANCODAG1 = space(1)
  w_NEWANCODAG1 = space(5)
  w_AGGANMAGTER = space(1)
  o_AGGANMAGTER = space(1)
  w_NEWANMAGTER = space(5)
  w_NEWANMAGTER = space(5)
  w_AGGANCODPAG = space(1)
  o_AGGANCODPAG = space(1)
  w_NEWANCODPAG = space(5)
  w_AGGANCODBA2 = space(1)
  o_AGGANCODBA2 = space(1)
  w_NEWANCODBA2 = space(15)
  w_AGGANGESCON = space(1)
  o_AGGANGESCON = space(1)
  w_AGGANFLGCON = space(1)
  w_AGGANPAGPAR = space(1)
  o_AGGANPAGPAR = space(1)
  w_NEWANPAGPAR = space(5)
  w_AGGANDATMOR = space(1)
  o_AGGANDATMOR = space(1)
  w_NEWANDATMOR = ctod('  /  /  ')
  w_AGGANFLESIM = space(1)
  o_AGGANFLESIM = space(1)
  w_AGGANSAGINT = space(1)
  o_AGGANSAGINT = space(1)
  w_NEWANSAGINT = 0
  w_AGGANSPRINT = space(1)
  w_AGGANRITENU = space(1)
  o_AGGANRITENU = space(1)
  w_AGGANCODIRPF = space(1)
  o_AGGANCODIRPF = space(1)
  w_NEWANCODIRPF = space(5)
  o_NEWANCODIRPF = space(5)
  w_AGGANCODTR2 = space(1)
  o_AGGANCODTR2 = space(1)
  w_NEWANCODTR2 = space(5)
  w_AGGANCAURITF = space(1)
  o_AGGANCAURITF = space(1)
  w_NEWANCAURITF = space(1)
  w_AGGANPEINPS = space(1)
  o_AGGANPEINPS = space(1)
  w_NEWANPEINPS = 0
  w_AGGANRIINPS = space(1)
  o_AGGANRIINPS = space(1)
  w_NEWANRIINPS = 0
  w_AGGANCOINPS = space(1)
  o_AGGANCOINPS = space(1)
  w_NEWANCOINPS = 0
  w_AGGANCASPRO = space(1)
  o_AGGANCASPRO = space(1)
  w_NEWANCASPRO = 0
  w_AGGANCODATT = space(1)
  o_AGGANCODATT = space(1)
  w_NEWANCODATT = 0
  w_AGGANCODASS = space(1)
  o_AGGANCODASS = space(1)
  w_NEWANCODASS = space(3)
  w_AGGANCOIMPS = space(1)
  o_AGGANCOIMPS = space(1)
  w_NEWANCOIMPS = space(20)
  w_NEWFLSCM = space(1)
  w_DESAPP = space(40)
  w_DESIVA = space(35)
  w_TIDESCRI = space(30)
  w_DESIRPEF = space(60)
  w_FLIRPE = space(1)
  w_DESCAC = space(35)
  w_DESSCM = space(35)
  w_DESGPP = space(35)
  w_FLSCM = space(1)
  w_FLPRO = space(1)
  w_DESZON = space(35)
  w_DESAGE1 = space(35)
  w_DESPAG = space(30)
  w_DESBA2 = space(42)
  w_CONSBF = space(1)
  w_DESPAR = space(30)
  w_DESIRPEF = space(35)
  w_DESTR2 = space(35)
  w_FLACO2 = space(1)
  w_NEWFLIRPE = space(1)
  w_CAUPRE2 = space(1)
  w_DATOBSOZ = ctod('  /  /  ')
  w_NEWFLPRO = space(1)
  w_MAGWIP = space(1)
  w_DATOBSOP = ctod('  /  /  ')
  w_DATOBSOB = ctod('  /  /  ')
  w_DATOBSOPM = ctod('  /  /  ')
  w_FLIRPENEW = space(1)
  w_NEWFLACO2 = space(1)
  w_CAUPRE2F = space(1)
  w_DESMAG = space(30)
  w_ANMAGTER = space(5)
  w_DESMAG = space(30)
  w_ANMAGWIP = space(1)
  w_AGGANCODEST = space(1)
  o_AGGANCODEST = space(1)
  w_NEWCODEST = space(7)
  w_AGGANCODCLA = space(1)
  o_AGGANCODCLA = space(1)
  w_NEWCODCLA = space(5)
  w_ZoomAggC = .NULL.
  w_ZoomAggPf = .NULL.
  w_ZoomAggR = .NULL.
  w_ZoomAggV = .NULL.
  w_ZoomAggF = .NULL.
  w_ZoomAggCo = .NULL.
  w_ZoomAggA = .NULL.
  w_ZoomAggPc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kcfPag1","gsar_kcf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgsar_kcfPag2","gsar_kcf",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Aggiornamento")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPCON_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomAggC = this.oPgFrm.Pages(2).oPag.ZoomAggC
    this.w_ZoomAggPf = this.oPgFrm.Pages(2).oPag.ZoomAggPf
    this.w_ZoomAggR = this.oPgFrm.Pages(2).oPag.ZoomAggR
    this.w_ZoomAggV = this.oPgFrm.Pages(2).oPag.ZoomAggV
    this.w_ZoomAggF = this.oPgFrm.Pages(2).oPag.ZoomAggF
    this.w_ZoomAggCo = this.oPgFrm.Pages(2).oPag.ZoomAggCo
    this.w_ZoomAggA = this.oPgFrm.Pages(2).oPag.ZoomAggA
    this.w_ZoomAggPc = this.oPgFrm.Pages(2).oPag.ZoomAggPc
    DoDefault()
    proc Destroy()
      this.w_ZoomAggC = .NULL.
      this.w_ZoomAggPf = .NULL.
      this.w_ZoomAggR = .NULL.
      this.w_ZoomAggV = .NULL.
      this.w_ZoomAggF = .NULL.
      this.w_ZoomAggCo = .NULL.
      this.w_ZoomAggA = .NULL.
      this.w_ZoomAggPc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[16]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CATECOMM'
    this.cWorkTables[3]='ZONE'
    this.cWorkTables[4]='NAZIONI'
    this.cWorkTables[5]='CACOCLFO'
    this.cWorkTables[6]='MASTRI'
    this.cWorkTables[7]='VOCIIVA'
    this.cWorkTables[8]='TIPCODIV'
    this.cWorkTables[9]='TRI_BUTI'
    this.cWorkTables[10]='CAT_SCMA'
    this.cWorkTables[11]='GRUPRO'
    this.cWorkTables[12]='LISTINI'
    this.cWorkTables[13]='AGENTI'
    this.cWorkTables[14]='PAG_AMEN'
    this.cWorkTables[15]='COC_MAST'
    this.cWorkTables[16]='MAGAZZIN'
    return(this.OpenAllTables(16))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_OBTEST=ctod("  /  /  ")
      .w_TIPCON=space(1)
      .w_PBANCODI=space(15)
      .w_PEANCODI=space(15)
      .w_SEZIONE=space(1)
      .w_SEZIONE=space(1)
      .w_PANCONSU=space(15)
      .w_PANCONSU=space(15)
      .w_CATCON=space(5)
      .w_ANCODIVA=space(5)
      .w_ANCATOPE=space(2)
      .w_ANTIPOPE=space(10)
      .w_ANFLRITE=space(1)
      .w_ANCODIRP=space(5)
      .w_ANCATCOM=space(3)
      .w_ANCATSCM=space(5)
      .w_ANCODZON=space(3)
      .w_ANGRUPRO=space(5)
      .w_ANCODAG1=space(5)
      .w_ANMAGTER=space(5)
      .w_ANGESCON=space(1)
      .w_ANPAGPAR=space(5)
      .w_ANFLESIM=space(1)
      .w_ANCODPAG=space(5)
      .w_ANCODBA2=space(15)
      .w_ANFLESIM=space(1)
      .w_ANRITENU=space(1)
      .w_ANCODIRP=space(5)
      .w_ANCODTR2=space(5)
      .w_DESCR1=space(40)
      .w_DESCR2=space(40)
      .w_DESCCC=space(35)
      .w_AGGMASCON=space(1)
      .w_NEWMASCON=space(15)
      .w_NEWMASCON=space(15)
      .w_NULIV=0
      .w_TIPMAS=space(1)
      .w_AGGCATCON=space(1)
      .w_NEWCATCON=space(5)
      .w_AGGANCODIVA=space(1)
      .w_NEWANCODIVA=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_AGGANTIPOPE=space(1)
      .w_NEWANTIPOPE=space(10)
      .w_TIDTOBSO=ctod("  /  /  ")
      .w_AGGANPARTSN=space(1)
      .w_AGGANFLRITE=space(1)
      .w_AGGANCODIRP=space(1)
      .w_NEWANCODIRP=space(5)
      .w_AGGANCAURIT=space(1)
      .w_NEWANCAURIT=space(1)
      .w_AGGANFLFIDO=space(1)
      .w_AGGANCATCOM=space(1)
      .w_NEWANCATCOM=space(3)
      .w_AGGANCATSCM=space(1)
      .w_NEWANCATSCM=space(5)
      .w_AGGANCODZON=space(1)
      .w_NEWANCODZON=space(3)
      .w_AGGANGRUPRO=space(1)
      .w_NEWANGRUPRO=space(5)
      .w_DATOBSOA=ctod("  /  /  ")
      .w_AGGANCODAG1=space(1)
      .w_NEWANCODAG1=space(5)
      .w_AGGANMAGTER=space(1)
      .w_NEWANMAGTER=space(5)
      .w_NEWANMAGTER=space(5)
      .w_AGGANCODPAG=space(1)
      .w_NEWANCODPAG=space(5)
      .w_AGGANCODBA2=space(1)
      .w_NEWANCODBA2=space(15)
      .w_AGGANGESCON=space(1)
      .w_AGGANFLGCON=space(1)
      .w_AGGANPAGPAR=space(1)
      .w_NEWANPAGPAR=space(5)
      .w_AGGANDATMOR=space(1)
      .w_NEWANDATMOR=ctod("  /  /  ")
      .w_AGGANFLESIM=space(1)
      .w_AGGANSAGINT=space(1)
      .w_NEWANSAGINT=0
      .w_AGGANSPRINT=space(1)
      .w_AGGANRITENU=space(1)
      .w_AGGANCODIRPF=space(1)
      .w_NEWANCODIRPF=space(5)
      .w_AGGANCODTR2=space(1)
      .w_NEWANCODTR2=space(5)
      .w_AGGANCAURITF=space(1)
      .w_NEWANCAURITF=space(1)
      .w_AGGANPEINPS=space(1)
      .w_NEWANPEINPS=0
      .w_AGGANRIINPS=space(1)
      .w_NEWANRIINPS=0
      .w_AGGANCOINPS=space(1)
      .w_NEWANCOINPS=0
      .w_AGGANCASPRO=space(1)
      .w_NEWANCASPRO=0
      .w_AGGANCODATT=space(1)
      .w_NEWANCODATT=0
      .w_AGGANCODASS=space(1)
      .w_NEWANCODASS=space(3)
      .w_AGGANCOIMPS=space(1)
      .w_NEWANCOIMPS=space(20)
      .w_NEWFLSCM=space(1)
      .w_DESAPP=space(40)
      .w_DESIVA=space(35)
      .w_TIDESCRI=space(30)
      .w_DESIRPEF=space(60)
      .w_FLIRPE=space(1)
      .w_DESCAC=space(35)
      .w_DESSCM=space(35)
      .w_DESGPP=space(35)
      .w_FLSCM=space(1)
      .w_FLPRO=space(1)
      .w_DESZON=space(35)
      .w_DESAGE1=space(35)
      .w_DESPAG=space(30)
      .w_DESBA2=space(42)
      .w_CONSBF=space(1)
      .w_DESPAR=space(30)
      .w_DESIRPEF=space(35)
      .w_DESTR2=space(35)
      .w_FLACO2=space(1)
      .w_NEWFLIRPE=space(1)
      .w_CAUPRE2=space(1)
      .w_DATOBSOZ=ctod("  /  /  ")
      .w_NEWFLPRO=space(1)
      .w_MAGWIP=space(1)
      .w_DATOBSOP=ctod("  /  /  ")
      .w_DATOBSOB=ctod("  /  /  ")
      .w_DATOBSOPM=ctod("  /  /  ")
      .w_FLIRPENEW=space(1)
      .w_NEWFLACO2=space(1)
      .w_CAUPRE2F=space(1)
      .w_DESMAG=space(30)
      .w_ANMAGTER=space(5)
      .w_DESMAG=space(30)
      .w_ANMAGWIP=space(1)
      .w_AGGANCODEST=space(1)
      .w_NEWCODEST=space(7)
      .w_AGGANCODCLA=space(1)
      .w_NEWCODCLA=space(5)
        .w_OBTEST = i_DATSYS
        .w_TIPCON = 'C'
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_PBANCODI))
          .link_1_4('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PEANCODI))
          .link_1_5('Full')
        endif
        .w_SEZIONE = 'C'
        .w_SEZIONE = 'C'
        .w_PANCONSU = Space(15)
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_PANCONSU))
          .link_1_8('Full')
        endif
        .w_PANCONSU = Space(15)
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_PANCONSU))
          .link_1_9('Full')
        endif
        .w_CATCON = Space(5)
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CATCON))
          .link_1_10('Full')
        endif
        .w_ANCODIVA = Space(5)
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_ANCODIVA))
          .link_1_11('Full')
        endif
        .w_ANCATOPE = 'CF'
        .w_ANTIPOPE = Space(10)
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_ANTIPOPE))
          .link_1_13('Full')
        endif
        .w_ANFLRITE = 'T'
        .w_ANCODIRP = Space(5)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_ANCODIRP))
          .link_1_15('Full')
        endif
        .w_ANCATCOM = Space(3)
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_ANCATCOM))
          .link_1_16('Full')
        endif
        .w_ANCATSCM = Space(5)
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_ANCATSCM))
          .link_1_17('Full')
        endif
        .w_ANCODZON = Space(3)
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_ANCODZON))
          .link_1_18('Full')
        endif
        .w_ANGRUPRO = Space(5)
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_ANGRUPRO))
          .link_1_19('Full')
        endif
        .w_ANCODAG1 = Space(5)
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_ANCODAG1))
          .link_1_20('Full')
        endif
        .w_ANMAGTER = Space(5)
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_ANMAGTER))
          .link_1_21('Full')
        endif
        .w_ANGESCON = 'T'
        .w_ANPAGPAR = Space(5)
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_ANPAGPAR))
          .link_1_23('Full')
        endif
        .w_ANFLESIM = 'T'
        .w_ANCODPAG = Space(5)
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_ANCODPAG))
          .link_1_25('Full')
        endif
        .w_ANCODBA2 = Space(15)
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_ANCODBA2))
          .link_1_26('Full')
        endif
        .w_ANFLESIM = 'T'
        .w_ANRITENU = 'T'
        .w_ANCODIRP = Space(5)
        .DoRTCalc(28,28,.f.)
        if not(empty(.w_ANCODIRP))
          .link_1_29('Full')
        endif
        .w_ANCODTR2 = SPACE(5)
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_ANCODTR2))
          .link_1_30('Full')
        endif
      .oPgFrm.Page2.oPag.ZoomAggC.Calculate()
      .oPgFrm.Page2.oPag.ZoomAggPf.Calculate()
      .oPgFrm.Page2.oPag.ZoomAggR.Calculate()
      .oPgFrm.Page2.oPag.ZoomAggV.Calculate()
      .oPgFrm.Page2.oPag.ZoomAggF.Calculate()
      .oPgFrm.Page2.oPag.ZoomAggCo.Calculate()
      .oPgFrm.Page2.oPag.ZoomAggA.Calculate()
      .oPgFrm.Page2.oPag.ZoomAggPc.Calculate()
          .DoRTCalc(30,32,.f.)
        .w_AGGMASCON = 'N'
        .w_NEWMASCON = Space(15)
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_NEWMASCON))
          .link_2_13('Full')
        endif
        .w_NEWMASCON = Space(15)
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_NEWMASCON))
          .link_2_14('Full')
        endif
          .DoRTCalc(36,37,.f.)
        .w_AGGCATCON = 'N'
        .w_NEWCATCON = Space(5)
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_NEWCATCON))
          .link_2_18('Full')
        endif
        .w_AGGANCODIVA = 'N'
        .w_NEWANCODIVA = Space(5)
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_NEWANCODIVA))
          .link_2_20('Full')
        endif
          .DoRTCalc(42,42,.f.)
        .w_AGGANTIPOPE = 'N'
        .w_NEWANTIPOPE = Space(10)
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_NEWANTIPOPE))
          .link_2_23('Full')
        endif
          .DoRTCalc(45,45,.f.)
        .w_AGGANPARTSN = 'X'
        .w_AGGANFLRITE = 'X'
        .w_AGGANCODIRP = 'N'
        .w_NEWANCODIRP = Space(5)
        .DoRTCalc(49,49,.f.)
        if not(empty(.w_NEWANCODIRP))
          .link_2_28('Full')
        endif
        .w_AGGANCAURIT = 'N'
        .w_NEWANCAURIT = iif(.w_AGGANCAURIT='S',IIF(Empty(.w_CAUPRE2),'A',.w_CAUPRE2),Space(1))
        .w_AGGANFLFIDO = 'X'
        .w_AGGANCATCOM = 'N'
        .w_NEWANCATCOM = Space(3)
        .DoRTCalc(54,54,.f.)
        if not(empty(.w_NEWANCATCOM))
          .link_2_33('Full')
        endif
        .w_AGGANCATSCM = 'N'
        .w_NEWANCATSCM = Space(5)
        .DoRTCalc(56,56,.f.)
        if not(empty(.w_NEWANCATSCM))
          .link_2_35('Full')
        endif
        .w_AGGANCODZON = 'N'
        .w_NEWANCODZON = Space(3)
        .DoRTCalc(58,58,.f.)
        if not(empty(.w_NEWANCODZON))
          .link_2_37('Full')
        endif
        .w_AGGANGRUPRO = 'N'
        .w_NEWANGRUPRO = Space(5)
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_NEWANGRUPRO))
          .link_2_39('Full')
        endif
          .DoRTCalc(61,61,.f.)
        .w_AGGANCODAG1 = 'N'
        .w_NEWANCODAG1 = Space(5)
        .DoRTCalc(63,63,.f.)
        if not(empty(.w_NEWANCODAG1))
          .link_2_42('Full')
        endif
        .w_AGGANMAGTER = 'N'
        .w_NEWANMAGTER = Space(5)
        .DoRTCalc(65,65,.f.)
        if not(empty(.w_NEWANMAGTER))
          .link_2_44('Full')
        endif
        .w_NEWANMAGTER = Space(5)
        .DoRTCalc(66,66,.f.)
        if not(empty(.w_NEWANMAGTER))
          .link_2_45('Full')
        endif
        .w_AGGANCODPAG = 'N'
        .w_NEWANCODPAG = Space(5)
        .DoRTCalc(68,68,.f.)
        if not(empty(.w_NEWANCODPAG))
          .link_2_47('Full')
        endif
        .w_AGGANCODBA2 = 'N'
        .w_NEWANCODBA2 = Space(15)
        .DoRTCalc(70,70,.f.)
        if not(empty(.w_NEWANCODBA2))
          .link_2_49('Full')
        endif
        .w_AGGANGESCON = 'X'
        .w_AGGANFLGCON = 'X'
        .w_AGGANPAGPAR = 'N'
        .w_NEWANPAGPAR = Space(5)
        .DoRTCalc(74,74,.f.)
        if not(empty(.w_NEWANPAGPAR))
          .link_2_53('Full')
        endif
        .w_AGGANDATMOR = 'N'
        .w_NEWANDATMOR = Cp_CharToDate('  -  -    ')
        .w_AGGANFLESIM = 'X'
        .w_AGGANSAGINT = 'N'
        .w_NEWANSAGINT = 0
        .w_AGGANSPRINT = 'X'
        .w_AGGANRITENU = 'X'
        .w_AGGANCODIRPF = 'N'
        .w_NEWANCODIRPF = Space(5)
        .DoRTCalc(83,83,.f.)
        if not(empty(.w_NEWANCODIRPF))
          .link_2_62('Full')
        endif
        .w_AGGANCODTR2 = 'N'
        .w_NEWANCODTR2 = SPACE(5)
        .DoRTCalc(85,85,.f.)
        if not(empty(.w_NEWANCODTR2))
          .link_2_64('Full')
        endif
        .w_AGGANCAURITF = 'N'
        .w_NEWANCAURITF = iif(.w_AGGANCAURITF='S',IIF(Empty(.w_CAUPRE2F),'A',.w_CAUPRE2F),Space(1))
        .w_AGGANPEINPS = 'N'
        .w_NEWANPEINPS = 0
        .w_AGGANRIINPS = 'N'
        .w_NEWANRIINPS = 0
        .w_AGGANCOINPS = 'N'
        .w_NEWANCOINPS = 0
        .w_AGGANCASPRO = 'N'
        .w_NEWANCASPRO = 0
        .w_AGGANCODATT = 'N'
        .w_NEWANCODATT = 0
        .w_AGGANCODASS = 'N'
        .w_NEWANCODASS = SPACE(3)
        .w_AGGANCOIMPS = 'N'
        .w_NEWANCOIMPS = SPACE(20)
          .DoRTCalc(102,133,.f.)
        .w_ANMAGTER = Space(5)
        .DoRTCalc(134,134,.f.)
        if not(empty(.w_ANMAGTER))
          .link_1_79('Full')
        endif
          .DoRTCalc(135,136,.f.)
        .w_AGGANCODEST = 'N'
        .w_NEWCODEST = space(7)
        .w_AGGANCODCLA = 'N'
        .w_NEWCODCLA = space(5)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page2.oPag.oBtn_2_9.enabled = this.oPgFrm.Page2.oPag.oBtn_2_9.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_10.enabled = this.oPgFrm.Page2.oPag.oBtn_2_10.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_11.enabled = this.oPgFrm.Page2.oPag.oBtn_2_11.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_84.enabled = this.oPgFrm.Page2.oPag.oBtn_2_84.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_85.enabled = this.oPgFrm.Page2.oPag.oBtn_2_85.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_OBTEST = i_DATSYS
        .DoRTCalc(2,4,.t.)
        if .o_TIPCON<>.w_TIPCON
            .w_SEZIONE = 'C'
        endif
        if .o_TIPCON<>.w_TIPCON
            .w_SEZIONE = 'C'
        endif
        if .o_TIPCON<>.w_TIPCON.or. .o_SEZIONE<>.w_SEZIONE
            .w_PANCONSU = Space(15)
          .link_1_8('Full')
        endif
        if .o_TIPCON<>.w_TIPCON.or. .o_SEZIONE<>.w_SEZIONE
            .w_PANCONSU = Space(15)
          .link_1_9('Full')
        endif
        if .o_SEZIONE<>.w_SEZIONE
            .w_CATCON = Space(5)
          .link_1_10('Full')
        endif
        if .o_SEZIONE<>.w_SEZIONE
            .w_ANCODIVA = Space(5)
          .link_1_11('Full')
        endif
        .DoRTCalc(11,11,.t.)
        if .o_SEZIONE<>.w_SEZIONE
            .w_ANTIPOPE = Space(10)
          .link_1_13('Full')
        endif
        if .o_TIPCON<>.w_TIPCON.or. .o_SEZIONE<>.w_SEZIONE
            .w_ANFLRITE = 'T'
        endif
        if .o_ANFLRITE<>.w_ANFLRITE.or. .o_SEZIONE<>.w_SEZIONE.or. .o_TIPCON<>.w_TIPCON
            .w_ANCODIRP = Space(5)
          .link_1_15('Full')
        endif
        if .o_SEZIONE<>.w_SEZIONE
            .w_ANCATCOM = Space(3)
          .link_1_16('Full')
        endif
        if .o_SEZIONE<>.w_SEZIONE
            .w_ANCATSCM = Space(5)
          .link_1_17('Full')
        endif
        if .o_SEZIONE<>.w_SEZIONE
            .w_ANCODZON = Space(3)
          .link_1_18('Full')
        endif
        if .o_SEZIONE<>.w_SEZIONE
            .w_ANGRUPRO = Space(5)
          .link_1_19('Full')
        endif
        if .o_SEZIONE<>.w_SEZIONE
            .w_ANCODAG1 = Space(5)
          .link_1_20('Full')
        endif
        if .o_SEZIONE<>.w_SEZIONE
            .w_ANMAGTER = Space(5)
          .link_1_21('Full')
        endif
        if .o_SEZIONE<>.w_SEZIONE.or. .o_TIPCON<>.w_TIPCON
            .w_ANGESCON = 'T'
        endif
        if .o_SEZIONE<>.w_SEZIONE.or. .o_TIPCON<>.w_TIPCON
            .w_ANPAGPAR = Space(5)
          .link_1_23('Full')
        endif
        if .o_SEZIONE<>.w_SEZIONE.or. .o_TIPCON<>.w_TIPCON
            .w_ANFLESIM = 'T'
        endif
        if .o_SEZIONE<>.w_SEZIONE
            .w_ANCODPAG = Space(5)
          .link_1_25('Full')
        endif
        if .o_SEZIONE<>.w_SEZIONE
            .w_ANCODBA2 = Space(15)
          .link_1_26('Full')
        endif
        if .o_SEZIONE<>.w_SEZIONE.or. .o_TIPCON<>.w_TIPCON
            .w_ANFLESIM = 'T'
        endif
        if .o_SEZIONE<>.w_SEZIONE.or. .o_TIPCON<>.w_TIPCON
            .w_ANRITENU = 'T'
        endif
        if .o_ANRITENU<>.w_ANRITENU.or. .o_SEZIONE<>.w_SEZIONE.or. .o_TIPCON<>.w_TIPCON
            .w_ANCODIRP = Space(5)
          .link_1_29('Full')
        endif
        if .o_ANRITENU<>.w_ANRITENU.or. .o_SEZIONE<>.w_SEZIONE.or. .o_TIPCON<>.w_TIPCON
            .w_ANCODTR2 = SPACE(5)
          .link_1_30('Full')
        endif
        .oPgFrm.Page2.oPag.ZoomAggC.Calculate()
        .oPgFrm.Page2.oPag.ZoomAggPf.Calculate()
        .oPgFrm.Page2.oPag.ZoomAggR.Calculate()
        .oPgFrm.Page2.oPag.ZoomAggV.Calculate()
        .oPgFrm.Page2.oPag.ZoomAggF.Calculate()
        .oPgFrm.Page2.oPag.ZoomAggCo.Calculate()
        .oPgFrm.Page2.oPag.ZoomAggA.Calculate()
        .oPgFrm.Page2.oPag.ZoomAggPc.Calculate()
        .DoRTCalc(30,33,.t.)
        if .o_AGGMASCON<>.w_AGGMASCON
            .w_NEWMASCON = Space(15)
          .link_2_13('Full')
        endif
        if .o_AGGMASCON<>.w_AGGMASCON
            .w_NEWMASCON = Space(15)
          .link_2_14('Full')
        endif
        .DoRTCalc(36,38,.t.)
        if .o_AGGCATCON<>.w_AGGCATCON
            .w_NEWCATCON = Space(5)
          .link_2_18('Full')
        endif
        .DoRTCalc(40,40,.t.)
        if .o_SEZIONE<>.w_SEZIONE.or. .o_AGGANCODIVA<>.w_AGGANCODIVA
            .w_NEWANCODIVA = Space(5)
          .link_2_20('Full')
        endif
        .DoRTCalc(42,43,.t.)
        if .o_AGGANTIPOPE<>.w_AGGANTIPOPE
            .w_NEWANTIPOPE = Space(10)
          .link_2_23('Full')
        endif
        .DoRTCalc(45,48,.t.)
        if .o_AGGANCODIRP<>.w_AGGANCODIRP
            .w_NEWANCODIRP = Space(5)
          .link_2_28('Full')
        endif
        .DoRTCalc(50,50,.t.)
        if .o_AGGANCAURIT<>.w_AGGANCAURIT.or. .o_NEWANCODIRP<>.w_NEWANCODIRP
            .w_NEWANCAURIT = iif(.w_AGGANCAURIT='S',IIF(Empty(.w_CAUPRE2),'A',.w_CAUPRE2),Space(1))
        endif
        .DoRTCalc(52,53,.t.)
        if .o_AGGANCATCOM<>.w_AGGANCATCOM
            .w_NEWANCATCOM = Space(3)
          .link_2_33('Full')
        endif
        .DoRTCalc(55,55,.t.)
        if .o_AGGANCATSCM<>.w_AGGANCATSCM
            .w_NEWANCATSCM = Space(5)
          .link_2_35('Full')
        endif
        .DoRTCalc(57,57,.t.)
        if .o_AGGANCODZON<>.w_AGGANCODZON
            .w_NEWANCODZON = Space(3)
          .link_2_37('Full')
        endif
        .DoRTCalc(59,59,.t.)
        if .o_AGGANGRUPRO<>.w_AGGANGRUPRO
            .w_NEWANGRUPRO = Space(5)
          .link_2_39('Full')
        endif
        .DoRTCalc(61,62,.t.)
        if .o_AGGANCODAG1<>.w_AGGANCODAG1
            .w_NEWANCODAG1 = Space(5)
          .link_2_42('Full')
        endif
        .DoRTCalc(64,64,.t.)
        if .o_AGGANMAGTER<>.w_AGGANMAGTER
            .w_NEWANMAGTER = Space(5)
          .link_2_44('Full')
        endif
        if .o_AGGANMAGTER<>.w_AGGANMAGTER
            .w_NEWANMAGTER = Space(5)
          .link_2_45('Full')
        endif
        .DoRTCalc(67,67,.t.)
        if .o_AGGANCODPAG<>.w_AGGANCODPAG
            .w_NEWANCODPAG = Space(5)
          .link_2_47('Full')
        endif
        .DoRTCalc(69,69,.t.)
        if .o_AGGANCODBA2<>.w_AGGANCODBA2
            .w_NEWANCODBA2 = Space(15)
          .link_2_49('Full')
        endif
        .DoRTCalc(71,73,.t.)
        if .o_AGGANPAGPAR<>.w_AGGANPAGPAR
            .w_NEWANPAGPAR = Space(5)
          .link_2_53('Full')
        endif
        .DoRTCalc(75,75,.t.)
        if .o_AGGANDATMOR<>.w_AGGANDATMOR
            .w_NEWANDATMOR = Cp_CharToDate('  -  -    ')
        endif
        .DoRTCalc(77,78,.t.)
        if .o_AGGANSAGINT<>.w_AGGANSAGINT
            .w_NEWANSAGINT = 0
        endif
        .DoRTCalc(80,80,.t.)
        if .o_SEZIONE<>.w_SEZIONE.or. .o_TIPCON<>.w_TIPCON
            .w_AGGANRITENU = 'X'
        endif
        .DoRTCalc(82,82,.t.)
        if .o_AGGANCODIRPF<>.w_AGGANCODIRPF
            .w_NEWANCODIRPF = Space(5)
          .link_2_62('Full')
        endif
        .DoRTCalc(84,84,.t.)
        if .o_AGGANCODTR2<>.w_AGGANCODTR2
            .w_NEWANCODTR2 = SPACE(5)
          .link_2_64('Full')
        endif
        .DoRTCalc(86,86,.t.)
        if .o_AGGANCAURITF<>.w_AGGANCAURITF.or. .o_NEWANCODIRPF<>.w_NEWANCODIRPF
            .w_NEWANCAURITF = iif(.w_AGGANCAURITF='S',IIF(Empty(.w_CAUPRE2F),'A',.w_CAUPRE2F),Space(1))
        endif
        .DoRTCalc(88,88,.t.)
        if .o_AGGANPEINPS<>.w_AGGANPEINPS
            .w_NEWANPEINPS = 0
        endif
        .DoRTCalc(90,90,.t.)
        if .o_AGGANRIINPS<>.w_AGGANRIINPS
            .w_NEWANRIINPS = 0
        endif
        .DoRTCalc(92,92,.t.)
        if .o_AGGANCOINPS<>.w_AGGANCOINPS
            .w_NEWANCOINPS = 0
        endif
        .DoRTCalc(94,94,.t.)
        if .o_AGGANCASPRO<>.w_AGGANCASPRO
            .w_NEWANCASPRO = 0
        endif
        .DoRTCalc(96,96,.t.)
        if .o_AGGANCODATT<>.w_AGGANCODATT
            .w_NEWANCODATT = 0
        endif
        .DoRTCalc(98,98,.t.)
        if .o_AGGANCODASS<>.w_AGGANCODASS
            .w_NEWANCODASS = SPACE(3)
        endif
        .DoRTCalc(100,100,.t.)
        if .o_AGGANCOIMPS<>.w_AGGANCOIMPS
            .w_NEWANCOIMPS = SPACE(20)
        endif
        if .o_AGGANFLRITE<>.w_AGGANFLRITE
          .Calculate_ZAXGACRYHO()
        endif
        if .o_AGGANGESCON<>.w_AGGANGESCON
          .Calculate_ICXAQLEJNR()
        endif
        if .o_AGGANFLESIM<>.w_AGGANFLESIM
          .Calculate_TRTPJLXBNY()
        endif
        if .o_AGGANRITENU<>.w_AGGANRITENU
          .Calculate_MGKOYQGYXV()
        endif
        .DoRTCalc(102,133,.t.)
        if .o_SEZIONE<>.w_SEZIONE
            .w_ANMAGTER = Space(5)
          .link_1_79('Full')
        endif
        .DoRTCalc(135,137,.t.)
        if .o_AGGANCODEST<>.w_AGGANCODEST
            .w_NEWCODEST = space(7)
        endif
        .DoRTCalc(139,139,.t.)
        if .o_AGGANCODCLA<>.w_AGGANCODCLA
            .w_NEWCODCLA = space(5)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZoomAggC.Calculate()
        .oPgFrm.Page2.oPag.ZoomAggPf.Calculate()
        .oPgFrm.Page2.oPag.ZoomAggR.Calculate()
        .oPgFrm.Page2.oPag.ZoomAggV.Calculate()
        .oPgFrm.Page2.oPag.ZoomAggF.Calculate()
        .oPgFrm.Page2.oPag.ZoomAggCo.Calculate()
        .oPgFrm.Page2.oPag.ZoomAggA.Calculate()
        .oPgFrm.Page2.oPag.ZoomAggPc.Calculate()
    endwith
  return

  proc Calculate_VVKKLMAOPJ()
    with this
          * --- ActivatePage 2
          Gsar_Bkf(this;
              ,'Z';
             )
    endwith
  endproc
  proc Calculate_ZAXGACRYHO()
    with this
          * --- Aggiornamento dati ritenute
          .w_AGGANCODIRP = IIF(.w_AGGANFLRITE='N','N',.w_AGGANCODIRP)
          .w_NEWANCODIRP = IIF(.w_AGGANFLRITE='N',Space(5),.w_NEWANCODIRP)
          .w_AGGANCAURIT = IIF(.w_AGGANFLRITE='N','N',.w_AGGANCAURIT)
          .w_NEWANCAURIT = IIF(.w_AGGANFLRITE='N',Space(1),.w_NEWANCAURIT)
    endwith
  endproc
  proc Calculate_ENBBBCOFID()
    with this
          * --- ActivatePage 1
          GSAR_BKF(this;
              ,'P';
             )
    endwith
  endproc
  proc Calculate_ICXAQLEJNR()
    with this
          * --- Aggiornamento contenzioso clienti
          .w_AGGANFLGCON = IIF(.w_AGGANGESCON$'NM','X',.w_AGGANFLGCON)
          .w_AGGANPAGPAR = IIF(.w_AGGANGESCON$'NB','X',.w_AGGANPAGPAR)
          .w_NEWANPAGPAR = IIF(.w_AGGANGESCON$'NB',Space(5),.w_NEWANPAGPAR)
          .w_AGGANDATMOR = IIF(.w_AGGANGESCON$'NB','X',.w_AGGANPAGPAR)
          .w_NEWANDATMOR = IIF(.w_AGGANGESCON$'NB',Cp_CharToDate('  -  -    '),.w_NEWANDATMOR)
    endwith
  endproc
  proc Calculate_TRTPJLXBNY()
    with this
          * --- Aggiornamento interessi contenzioso
          .w_AGGANSAGINT = IIF(.w_AGGANFLESIM='S','X',.w_AGGANSAGINT)
          .w_NEWANSAGINT = IIF(.w_AGGANFLESIM='S',0,.w_NEWANSAGINT)
          .w_AGGANSPRINT = IIF(.w_AGGANFLESIM='S','X',.w_AGGANSPRINT)
    endwith
  endproc
  proc Calculate_MGKOYQGYXV()
    with this
          * --- Aggiornamento dati ritenute fornitore
          .w_AGGANCODIRPF = IIF(.w_AGGANRITENU='N','N',.w_AGGANCODIRPF)
          .w_NEWANCODIRPF = IIF(.w_AGGANRITENU='N',Space(5),.w_NEWANCODIRPF)
          .w_AGGANCODTR2 = IIF(.w_AGGANRITENU$'NS','N',.w_AGGANCODTR2)
          .w_NEWANCODTR2 = IIF(.w_AGGANRITENU$'NS',Space(5),.w_NEWANCODTR2)
          .w_AGGANPEINPS = IIF(.w_AGGANRITENU$'NS','N',.w_AGGANPEINPS)
          .w_NEWANPEINPS = IIF(.w_AGGANRITENU$'NS',0,.w_NEWANPEINPS)
          .w_AGGANRIINPS = IIF(.w_AGGANRITENU$'NS','N',.w_AGGANRIINPS)
          .w_NEWANRIINPS = IIF(.w_AGGANRITENU$'NS',0,.w_NEWANRIINPS)
          .w_AGGANCAURITF = IIF(.w_AGGANRITENU='N','N',.w_AGGANCAURITF)
          .w_NEWANCAURITF = IIF(.w_AGGANRITENU='N',Space(1),.w_NEWANCAURITF)
          .w_AGGANCOINPS = IIF(.w_AGGANRITENU$'NS','N',.w_AGGANCOINPS)
          .w_NEWANCOINPS = IIF(.w_AGGANRITENU$'NS',0,.w_NEWANCOINPS)
          .w_AGGANCASPRO = IIF(.w_AGGANRITENU='N','N',.w_AGGANCASPRO)
          .w_NEWANCASPRO = IIF(.w_AGGANRITENU='N',0,.w_NEWANCASPRO)
          .w_AGGANCODATT = IIF(.w_AGGANRITENU$'NS','N',.w_AGGANCODATT)
          .w_NEWANCODATT = IIF(.w_AGGANRITENU$'NS',0,.w_NEWANCODATT)
          .w_AGGANCODASS = IIF(.w_AGGANRITENU$'NS','N',.w_AGGANCODASS)
          .w_NEWANCODASS = IIF(.w_AGGANRITENU$'NS',Space(3),.w_NEWANCODASS)
          .w_AGGANCOIMPS = IIF(.w_AGGANRITENU$'NS','N',.w_AGGANCOIMPS)
          .w_NEWANCOIMPS = IIF(.w_AGGANRITENU$'NS',Space(20),.w_NEWANCOIMPS)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oANFLRITE_1_14.enabled = this.oPgFrm.Page1.oPag.oANFLRITE_1_14.mCond()
    this.oPgFrm.Page1.oPag.oANCODIRP_1_15.enabled = this.oPgFrm.Page1.oPag.oANCODIRP_1_15.mCond()
    this.oPgFrm.Page1.oPag.oANCODIRP_1_29.enabled = this.oPgFrm.Page1.oPag.oANCODIRP_1_29.mCond()
    this.oPgFrm.Page1.oPag.oANCODTR2_1_30.enabled = this.oPgFrm.Page1.oPag.oANCODTR2_1_30.mCond()
    this.oPgFrm.Page2.oPag.oNEWMASCON_2_13.enabled = this.oPgFrm.Page2.oPag.oNEWMASCON_2_13.mCond()
    this.oPgFrm.Page2.oPag.oNEWMASCON_2_14.enabled = this.oPgFrm.Page2.oPag.oNEWMASCON_2_14.mCond()
    this.oPgFrm.Page2.oPag.oNEWCATCON_2_18.enabled = this.oPgFrm.Page2.oPag.oNEWCATCON_2_18.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCODIVA_2_20.enabled = this.oPgFrm.Page2.oPag.oNEWANCODIVA_2_20.mCond()
    this.oPgFrm.Page2.oPag.oNEWANTIPOPE_2_23.enabled = this.oPgFrm.Page2.oPag.oNEWANTIPOPE_2_23.mCond()
    this.oPgFrm.Page2.oPag.oAGGANPARTSN_2_25.enabled = this.oPgFrm.Page2.oPag.oAGGANPARTSN_2_25.mCond()
    this.oPgFrm.Page2.oPag.oAGGANFLRITE_2_26.enabled = this.oPgFrm.Page2.oPag.oAGGANFLRITE_2_26.mCond()
    this.oPgFrm.Page2.oPag.oAGGANCODIRP_2_27.enabled = this.oPgFrm.Page2.oPag.oAGGANCODIRP_2_27.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCODIRP_2_28.enabled = this.oPgFrm.Page2.oPag.oNEWANCODIRP_2_28.mCond()
    this.oPgFrm.Page2.oPag.oAGGANCAURIT_2_29.enabled = this.oPgFrm.Page2.oPag.oAGGANCAURIT_2_29.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCAURIT_2_30.enabled = this.oPgFrm.Page2.oPag.oNEWANCAURIT_2_30.mCond()
    this.oPgFrm.Page2.oPag.oAGGANFLFIDO_2_31.enabled = this.oPgFrm.Page2.oPag.oAGGANFLFIDO_2_31.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCATCOM_2_33.enabled = this.oPgFrm.Page2.oPag.oNEWANCATCOM_2_33.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCATSCM_2_35.enabled = this.oPgFrm.Page2.oPag.oNEWANCATSCM_2_35.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCODZON_2_37.enabled = this.oPgFrm.Page2.oPag.oNEWANCODZON_2_37.mCond()
    this.oPgFrm.Page2.oPag.oNEWANGRUPRO_2_39.enabled = this.oPgFrm.Page2.oPag.oNEWANGRUPRO_2_39.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCODAG1_2_42.enabled = this.oPgFrm.Page2.oPag.oNEWANCODAG1_2_42.mCond()
    this.oPgFrm.Page2.oPag.oNEWANMAGTER_2_44.enabled = this.oPgFrm.Page2.oPag.oNEWANMAGTER_2_44.mCond()
    this.oPgFrm.Page2.oPag.oNEWANMAGTER_2_45.enabled = this.oPgFrm.Page2.oPag.oNEWANMAGTER_2_45.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCODPAG_2_47.enabled = this.oPgFrm.Page2.oPag.oNEWANCODPAG_2_47.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCODBA2_2_49.enabled = this.oPgFrm.Page2.oPag.oNEWANCODBA2_2_49.mCond()
    this.oPgFrm.Page2.oPag.oAGGANFLGCON_2_51.enabled = this.oPgFrm.Page2.oPag.oAGGANFLGCON_2_51.mCond()
    this.oPgFrm.Page2.oPag.oAGGANPAGPAR_2_52.enabled = this.oPgFrm.Page2.oPag.oAGGANPAGPAR_2_52.mCond()
    this.oPgFrm.Page2.oPag.oNEWANPAGPAR_2_53.enabled = this.oPgFrm.Page2.oPag.oNEWANPAGPAR_2_53.mCond()
    this.oPgFrm.Page2.oPag.oAGGANDATMOR_2_54.enabled = this.oPgFrm.Page2.oPag.oAGGANDATMOR_2_54.mCond()
    this.oPgFrm.Page2.oPag.oNEWANDATMOR_2_55.enabled = this.oPgFrm.Page2.oPag.oNEWANDATMOR_2_55.mCond()
    this.oPgFrm.Page2.oPag.oAGGANSAGINT_2_57.enabled = this.oPgFrm.Page2.oPag.oAGGANSAGINT_2_57.mCond()
    this.oPgFrm.Page2.oPag.oNEWANSAGINT_2_58.enabled = this.oPgFrm.Page2.oPag.oNEWANSAGINT_2_58.mCond()
    this.oPgFrm.Page2.oPag.oAGGANSPRINT_2_59.enabled = this.oPgFrm.Page2.oPag.oAGGANSPRINT_2_59.mCond()
    this.oPgFrm.Page2.oPag.oAGGANCODIRPF_2_61.enabled = this.oPgFrm.Page2.oPag.oAGGANCODIRPF_2_61.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCODIRPF_2_62.enabled = this.oPgFrm.Page2.oPag.oNEWANCODIRPF_2_62.mCond()
    this.oPgFrm.Page2.oPag.oAGGANCODTR2_2_63.enabled = this.oPgFrm.Page2.oPag.oAGGANCODTR2_2_63.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCODTR2_2_64.enabled = this.oPgFrm.Page2.oPag.oNEWANCODTR2_2_64.mCond()
    this.oPgFrm.Page2.oPag.oAGGANCAURITF_2_65.enabled = this.oPgFrm.Page2.oPag.oAGGANCAURITF_2_65.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCAURITF_2_66.enabled = this.oPgFrm.Page2.oPag.oNEWANCAURITF_2_66.mCond()
    this.oPgFrm.Page2.oPag.oAGGANPEINPS_2_67.enabled = this.oPgFrm.Page2.oPag.oAGGANPEINPS_2_67.mCond()
    this.oPgFrm.Page2.oPag.oNEWANPEINPS_2_68.enabled = this.oPgFrm.Page2.oPag.oNEWANPEINPS_2_68.mCond()
    this.oPgFrm.Page2.oPag.oAGGANRIINPS_2_69.enabled = this.oPgFrm.Page2.oPag.oAGGANRIINPS_2_69.mCond()
    this.oPgFrm.Page2.oPag.oNEWANRIINPS_2_70.enabled = this.oPgFrm.Page2.oPag.oNEWANRIINPS_2_70.mCond()
    this.oPgFrm.Page2.oPag.oAGGANCOINPS_2_71.enabled = this.oPgFrm.Page2.oPag.oAGGANCOINPS_2_71.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCOINPS_2_72.enabled = this.oPgFrm.Page2.oPag.oNEWANCOINPS_2_72.mCond()
    this.oPgFrm.Page2.oPag.oAGGANCASPRO_2_73.enabled = this.oPgFrm.Page2.oPag.oAGGANCASPRO_2_73.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCASPRO_2_74.enabled = this.oPgFrm.Page2.oPag.oNEWANCASPRO_2_74.mCond()
    this.oPgFrm.Page2.oPag.oAGGANCODATT_2_75.enabled = this.oPgFrm.Page2.oPag.oAGGANCODATT_2_75.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCODATT_2_76.enabled = this.oPgFrm.Page2.oPag.oNEWANCODATT_2_76.mCond()
    this.oPgFrm.Page2.oPag.oAGGANCODASS_2_77.enabled = this.oPgFrm.Page2.oPag.oAGGANCODASS_2_77.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCODASS_2_78.enabled = this.oPgFrm.Page2.oPag.oNEWANCODASS_2_78.mCond()
    this.oPgFrm.Page2.oPag.oAGGANCOIMPS_2_79.enabled = this.oPgFrm.Page2.oPag.oAGGANCOIMPS_2_79.mCond()
    this.oPgFrm.Page2.oPag.oNEWANCOIMPS_2_80.enabled = this.oPgFrm.Page2.oPag.oNEWANCOIMPS_2_80.mCond()
    this.oPgFrm.Page2.oPag.oNEWCODEST_2_141.enabled = this.oPgFrm.Page2.oPag.oNEWCODEST_2_141.mCond()
    this.oPgFrm.Page2.oPag.oNEWCODCLA_2_143.enabled = this.oPgFrm.Page2.oPag.oNEWCODCLA_2_143.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSEZIONE_1_6.visible=!this.oPgFrm.Page1.oPag.oSEZIONE_1_6.mHide()
    this.oPgFrm.Page1.oPag.oSEZIONE_1_7.visible=!this.oPgFrm.Page1.oPag.oSEZIONE_1_7.mHide()
    this.oPgFrm.Page1.oPag.oPANCONSU_1_8.visible=!this.oPgFrm.Page1.oPag.oPANCONSU_1_8.mHide()
    this.oPgFrm.Page1.oPag.oPANCONSU_1_9.visible=!this.oPgFrm.Page1.oPag.oPANCONSU_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCATCON_1_10.visible=!this.oPgFrm.Page1.oPag.oCATCON_1_10.mHide()
    this.oPgFrm.Page1.oPag.oANCODIVA_1_11.visible=!this.oPgFrm.Page1.oPag.oANCODIVA_1_11.mHide()
    this.oPgFrm.Page1.oPag.oANTIPOPE_1_13.visible=!this.oPgFrm.Page1.oPag.oANTIPOPE_1_13.mHide()
    this.oPgFrm.Page1.oPag.oANFLRITE_1_14.visible=!this.oPgFrm.Page1.oPag.oANFLRITE_1_14.mHide()
    this.oPgFrm.Page1.oPag.oANCODIRP_1_15.visible=!this.oPgFrm.Page1.oPag.oANCODIRP_1_15.mHide()
    this.oPgFrm.Page1.oPag.oANCATCOM_1_16.visible=!this.oPgFrm.Page1.oPag.oANCATCOM_1_16.mHide()
    this.oPgFrm.Page1.oPag.oANCATSCM_1_17.visible=!this.oPgFrm.Page1.oPag.oANCATSCM_1_17.mHide()
    this.oPgFrm.Page1.oPag.oANCODZON_1_18.visible=!this.oPgFrm.Page1.oPag.oANCODZON_1_18.mHide()
    this.oPgFrm.Page1.oPag.oANGRUPRO_1_19.visible=!this.oPgFrm.Page1.oPag.oANGRUPRO_1_19.mHide()
    this.oPgFrm.Page1.oPag.oANCODAG1_1_20.visible=!this.oPgFrm.Page1.oPag.oANCODAG1_1_20.mHide()
    this.oPgFrm.Page1.oPag.oANMAGTER_1_21.visible=!this.oPgFrm.Page1.oPag.oANMAGTER_1_21.mHide()
    this.oPgFrm.Page1.oPag.oANGESCON_1_22.visible=!this.oPgFrm.Page1.oPag.oANGESCON_1_22.mHide()
    this.oPgFrm.Page1.oPag.oANPAGPAR_1_23.visible=!this.oPgFrm.Page1.oPag.oANPAGPAR_1_23.mHide()
    this.oPgFrm.Page1.oPag.oANFLESIM_1_24.visible=!this.oPgFrm.Page1.oPag.oANFLESIM_1_24.mHide()
    this.oPgFrm.Page1.oPag.oANCODPAG_1_25.visible=!this.oPgFrm.Page1.oPag.oANCODPAG_1_25.mHide()
    this.oPgFrm.Page1.oPag.oANCODBA2_1_26.visible=!this.oPgFrm.Page1.oPag.oANCODBA2_1_26.mHide()
    this.oPgFrm.Page1.oPag.oANFLESIM_1_27.visible=!this.oPgFrm.Page1.oPag.oANFLESIM_1_27.mHide()
    this.oPgFrm.Page1.oPag.oANRITENU_1_28.visible=!this.oPgFrm.Page1.oPag.oANRITENU_1_28.mHide()
    this.oPgFrm.Page1.oPag.oANCODIRP_1_29.visible=!this.oPgFrm.Page1.oPag.oANCODIRP_1_29.mHide()
    this.oPgFrm.Page1.oPag.oANCODTR2_1_30.visible=!this.oPgFrm.Page1.oPag.oANCODTR2_1_30.mHide()
    this.oPgFrm.Page1.oPag.oDESCR1_1_33.visible=!this.oPgFrm.Page1.oPag.oDESCR1_1_33.mHide()
    this.oPgFrm.Page1.oPag.oDESCR2_1_34.visible=!this.oPgFrm.Page1.oPag.oDESCR2_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oDESCCC_1_36.visible=!this.oPgFrm.Page1.oPag.oDESCCC_1_36.mHide()
    this.oPgFrm.Page2.oPag.oAGGMASCON_2_12.visible=!this.oPgFrm.Page2.oPag.oAGGMASCON_2_12.mHide()
    this.oPgFrm.Page2.oPag.oNEWMASCON_2_13.visible=!this.oPgFrm.Page2.oPag.oNEWMASCON_2_13.mHide()
    this.oPgFrm.Page2.oPag.oNEWMASCON_2_14.visible=!this.oPgFrm.Page2.oPag.oNEWMASCON_2_14.mHide()
    this.oPgFrm.Page2.oPag.oAGGCATCON_2_17.visible=!this.oPgFrm.Page2.oPag.oAGGCATCON_2_17.mHide()
    this.oPgFrm.Page2.oPag.oNEWCATCON_2_18.visible=!this.oPgFrm.Page2.oPag.oNEWCATCON_2_18.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCODIVA_2_19.visible=!this.oPgFrm.Page2.oPag.oAGGANCODIVA_2_19.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCODIVA_2_20.visible=!this.oPgFrm.Page2.oPag.oNEWANCODIVA_2_20.mHide()
    this.oPgFrm.Page2.oPag.oAGGANTIPOPE_2_22.visible=!this.oPgFrm.Page2.oPag.oAGGANTIPOPE_2_22.mHide()
    this.oPgFrm.Page2.oPag.oNEWANTIPOPE_2_23.visible=!this.oPgFrm.Page2.oPag.oNEWANTIPOPE_2_23.mHide()
    this.oPgFrm.Page2.oPag.oAGGANPARTSN_2_25.visible=!this.oPgFrm.Page2.oPag.oAGGANPARTSN_2_25.mHide()
    this.oPgFrm.Page2.oPag.oAGGANFLRITE_2_26.visible=!this.oPgFrm.Page2.oPag.oAGGANFLRITE_2_26.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCODIRP_2_27.visible=!this.oPgFrm.Page2.oPag.oAGGANCODIRP_2_27.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCODIRP_2_28.visible=!this.oPgFrm.Page2.oPag.oNEWANCODIRP_2_28.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCAURIT_2_29.visible=!this.oPgFrm.Page2.oPag.oAGGANCAURIT_2_29.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCAURIT_2_30.visible=!this.oPgFrm.Page2.oPag.oNEWANCAURIT_2_30.mHide()
    this.oPgFrm.Page2.oPag.oAGGANFLFIDO_2_31.visible=!this.oPgFrm.Page2.oPag.oAGGANFLFIDO_2_31.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCATCOM_2_32.visible=!this.oPgFrm.Page2.oPag.oAGGANCATCOM_2_32.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCATCOM_2_33.visible=!this.oPgFrm.Page2.oPag.oNEWANCATCOM_2_33.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCATSCM_2_34.visible=!this.oPgFrm.Page2.oPag.oAGGANCATSCM_2_34.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCATSCM_2_35.visible=!this.oPgFrm.Page2.oPag.oNEWANCATSCM_2_35.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCODZON_2_36.visible=!this.oPgFrm.Page2.oPag.oAGGANCODZON_2_36.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCODZON_2_37.visible=!this.oPgFrm.Page2.oPag.oNEWANCODZON_2_37.mHide()
    this.oPgFrm.Page2.oPag.oAGGANGRUPRO_2_38.visible=!this.oPgFrm.Page2.oPag.oAGGANGRUPRO_2_38.mHide()
    this.oPgFrm.Page2.oPag.oNEWANGRUPRO_2_39.visible=!this.oPgFrm.Page2.oPag.oNEWANGRUPRO_2_39.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCODAG1_2_41.visible=!this.oPgFrm.Page2.oPag.oAGGANCODAG1_2_41.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCODAG1_2_42.visible=!this.oPgFrm.Page2.oPag.oNEWANCODAG1_2_42.mHide()
    this.oPgFrm.Page2.oPag.oAGGANMAGTER_2_43.visible=!this.oPgFrm.Page2.oPag.oAGGANMAGTER_2_43.mHide()
    this.oPgFrm.Page2.oPag.oNEWANMAGTER_2_44.visible=!this.oPgFrm.Page2.oPag.oNEWANMAGTER_2_44.mHide()
    this.oPgFrm.Page2.oPag.oNEWANMAGTER_2_45.visible=!this.oPgFrm.Page2.oPag.oNEWANMAGTER_2_45.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCODPAG_2_46.visible=!this.oPgFrm.Page2.oPag.oAGGANCODPAG_2_46.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCODPAG_2_47.visible=!this.oPgFrm.Page2.oPag.oNEWANCODPAG_2_47.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCODBA2_2_48.visible=!this.oPgFrm.Page2.oPag.oAGGANCODBA2_2_48.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCODBA2_2_49.visible=!this.oPgFrm.Page2.oPag.oNEWANCODBA2_2_49.mHide()
    this.oPgFrm.Page2.oPag.oAGGANGESCON_2_50.visible=!this.oPgFrm.Page2.oPag.oAGGANGESCON_2_50.mHide()
    this.oPgFrm.Page2.oPag.oAGGANFLGCON_2_51.visible=!this.oPgFrm.Page2.oPag.oAGGANFLGCON_2_51.mHide()
    this.oPgFrm.Page2.oPag.oAGGANPAGPAR_2_52.visible=!this.oPgFrm.Page2.oPag.oAGGANPAGPAR_2_52.mHide()
    this.oPgFrm.Page2.oPag.oNEWANPAGPAR_2_53.visible=!this.oPgFrm.Page2.oPag.oNEWANPAGPAR_2_53.mHide()
    this.oPgFrm.Page2.oPag.oAGGANDATMOR_2_54.visible=!this.oPgFrm.Page2.oPag.oAGGANDATMOR_2_54.mHide()
    this.oPgFrm.Page2.oPag.oNEWANDATMOR_2_55.visible=!this.oPgFrm.Page2.oPag.oNEWANDATMOR_2_55.mHide()
    this.oPgFrm.Page2.oPag.oAGGANFLESIM_2_56.visible=!this.oPgFrm.Page2.oPag.oAGGANFLESIM_2_56.mHide()
    this.oPgFrm.Page2.oPag.oAGGANSAGINT_2_57.visible=!this.oPgFrm.Page2.oPag.oAGGANSAGINT_2_57.mHide()
    this.oPgFrm.Page2.oPag.oNEWANSAGINT_2_58.visible=!this.oPgFrm.Page2.oPag.oNEWANSAGINT_2_58.mHide()
    this.oPgFrm.Page2.oPag.oAGGANSPRINT_2_59.visible=!this.oPgFrm.Page2.oPag.oAGGANSPRINT_2_59.mHide()
    this.oPgFrm.Page2.oPag.oAGGANRITENU_2_60.visible=!this.oPgFrm.Page2.oPag.oAGGANRITENU_2_60.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCODIRPF_2_61.visible=!this.oPgFrm.Page2.oPag.oAGGANCODIRPF_2_61.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCODIRPF_2_62.visible=!this.oPgFrm.Page2.oPag.oNEWANCODIRPF_2_62.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCODTR2_2_63.visible=!this.oPgFrm.Page2.oPag.oAGGANCODTR2_2_63.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCODTR2_2_64.visible=!this.oPgFrm.Page2.oPag.oNEWANCODTR2_2_64.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCAURITF_2_65.visible=!this.oPgFrm.Page2.oPag.oAGGANCAURITF_2_65.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCAURITF_2_66.visible=!this.oPgFrm.Page2.oPag.oNEWANCAURITF_2_66.mHide()
    this.oPgFrm.Page2.oPag.oAGGANPEINPS_2_67.visible=!this.oPgFrm.Page2.oPag.oAGGANPEINPS_2_67.mHide()
    this.oPgFrm.Page2.oPag.oNEWANPEINPS_2_68.visible=!this.oPgFrm.Page2.oPag.oNEWANPEINPS_2_68.mHide()
    this.oPgFrm.Page2.oPag.oAGGANRIINPS_2_69.visible=!this.oPgFrm.Page2.oPag.oAGGANRIINPS_2_69.mHide()
    this.oPgFrm.Page2.oPag.oNEWANRIINPS_2_70.visible=!this.oPgFrm.Page2.oPag.oNEWANRIINPS_2_70.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCOINPS_2_71.visible=!this.oPgFrm.Page2.oPag.oAGGANCOINPS_2_71.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCOINPS_2_72.visible=!this.oPgFrm.Page2.oPag.oNEWANCOINPS_2_72.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCASPRO_2_73.visible=!this.oPgFrm.Page2.oPag.oAGGANCASPRO_2_73.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCASPRO_2_74.visible=!this.oPgFrm.Page2.oPag.oNEWANCASPRO_2_74.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCODATT_2_75.visible=!this.oPgFrm.Page2.oPag.oAGGANCODATT_2_75.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCODATT_2_76.visible=!this.oPgFrm.Page2.oPag.oNEWANCODATT_2_76.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCODASS_2_77.visible=!this.oPgFrm.Page2.oPag.oAGGANCODASS_2_77.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCODASS_2_78.visible=!this.oPgFrm.Page2.oPag.oNEWANCODASS_2_78.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCOIMPS_2_79.visible=!this.oPgFrm.Page2.oPag.oAGGANCOIMPS_2_79.mHide()
    this.oPgFrm.Page2.oPag.oNEWANCOIMPS_2_80.visible=!this.oPgFrm.Page2.oPag.oNEWANCOIMPS_2_80.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oDESAPP_1_39.visible=!this.oPgFrm.Page1.oPag.oDESAPP_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oDESIVA_1_41.visible=!this.oPgFrm.Page1.oPag.oDESIVA_1_41.mHide()
    this.oPgFrm.Page1.oPag.oTIDESCRI_1_42.visible=!this.oPgFrm.Page1.oPag.oTIDESCRI_1_42.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oDESIRPEF_1_45.visible=!this.oPgFrm.Page1.oPag.oDESIRPEF_1_45.mHide()
    this.oPgFrm.Page1.oPag.oDESCAC_1_47.visible=!this.oPgFrm.Page1.oPag.oDESCAC_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oDESSCM_1_49.visible=!this.oPgFrm.Page1.oPag.oDESSCM_1_49.mHide()
    this.oPgFrm.Page1.oPag.oDESGPP_1_50.visible=!this.oPgFrm.Page1.oPag.oDESGPP_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oDESZON_1_55.visible=!this.oPgFrm.Page1.oPag.oDESZON_1_55.mHide()
    this.oPgFrm.Page1.oPag.oDESAGE1_1_56.visible=!this.oPgFrm.Page1.oPag.oDESAGE1_1_56.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_57.visible=!this.oPgFrm.Page1.oPag.oStr_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_60.visible=!this.oPgFrm.Page1.oPag.oStr_1_60.mHide()
    this.oPgFrm.Page1.oPag.oDESPAG_1_61.visible=!this.oPgFrm.Page1.oPag.oDESPAG_1_61.mHide()
    this.oPgFrm.Page1.oPag.oDESBA2_1_62.visible=!this.oPgFrm.Page1.oPag.oDESBA2_1_62.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_64.visible=!this.oPgFrm.Page1.oPag.oStr_1_64.mHide()
    this.oPgFrm.Page1.oPag.oDESPAR_1_65.visible=!this.oPgFrm.Page1.oPag.oDESPAR_1_65.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_66.visible=!this.oPgFrm.Page1.oPag.oStr_1_66.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_67.visible=!this.oPgFrm.Page1.oPag.oStr_1_67.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    this.oPgFrm.Page1.oPag.oDESIRPEF_1_69.visible=!this.oPgFrm.Page1.oPag.oDESIRPEF_1_69.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_71.visible=!this.oPgFrm.Page1.oPag.oStr_1_71.mHide()
    this.oPgFrm.Page1.oPag.oDESTR2_1_72.visible=!this.oPgFrm.Page1.oPag.oDESTR2_1_72.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_73.visible=!this.oPgFrm.Page1.oPag.oStr_1_73.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_86.visible=!this.oPgFrm.Page2.oPag.oStr_2_86.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_87.visible=!this.oPgFrm.Page2.oPag.oStr_2_87.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_88.visible=!this.oPgFrm.Page2.oPag.oStr_2_88.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_89.visible=!this.oPgFrm.Page2.oPag.oStr_2_89.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_90.visible=!this.oPgFrm.Page2.oPag.oStr_2_90.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_91.visible=!this.oPgFrm.Page2.oPag.oStr_2_91.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_92.visible=!this.oPgFrm.Page2.oPag.oStr_2_92.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_93.visible=!this.oPgFrm.Page2.oPag.oStr_2_93.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_94.visible=!this.oPgFrm.Page2.oPag.oStr_2_94.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_98.visible=!this.oPgFrm.Page2.oPag.oStr_2_98.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_99.visible=!this.oPgFrm.Page2.oPag.oStr_2_99.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_100.visible=!this.oPgFrm.Page2.oPag.oStr_2_100.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_101.visible=!this.oPgFrm.Page2.oPag.oStr_2_101.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_104.visible=!this.oPgFrm.Page2.oPag.oStr_2_104.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_105.visible=!this.oPgFrm.Page2.oPag.oStr_2_105.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_106.visible=!this.oPgFrm.Page2.oPag.oStr_2_106.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_108.visible=!this.oPgFrm.Page2.oPag.oStr_2_108.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_109.visible=!this.oPgFrm.Page2.oPag.oStr_2_109.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_113.visible=!this.oPgFrm.Page2.oPag.oStr_2_113.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_114.visible=!this.oPgFrm.Page2.oPag.oStr_2_114.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_115.visible=!this.oPgFrm.Page2.oPag.oStr_2_115.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_116.visible=!this.oPgFrm.Page2.oPag.oStr_2_116.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_119.visible=!this.oPgFrm.Page2.oPag.oStr_2_119.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_120.visible=!this.oPgFrm.Page2.oPag.oStr_2_120.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_122.visible=!this.oPgFrm.Page2.oPag.oStr_2_122.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_123.visible=!this.oPgFrm.Page2.oPag.oStr_2_123.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_126.visible=!this.oPgFrm.Page2.oPag.oStr_2_126.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_128.visible=!this.oPgFrm.Page2.oPag.oStr_2_128.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_129.visible=!this.oPgFrm.Page2.oPag.oStr_2_129.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_130.visible=!this.oPgFrm.Page2.oPag.oStr_2_130.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_131.visible=!this.oPgFrm.Page2.oPag.oStr_2_131.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_132.visible=!this.oPgFrm.Page2.oPag.oStr_2_132.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_134.visible=!this.oPgFrm.Page2.oPag.oStr_2_134.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_135.visible=!this.oPgFrm.Page2.oPag.oStr_2_135.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_136.visible=!this.oPgFrm.Page2.oPag.oStr_2_136.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_76.visible=!this.oPgFrm.Page1.oPag.oStr_1_76.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_77.visible=!this.oPgFrm.Page1.oPag.oStr_1_77.mHide()
    this.oPgFrm.Page1.oPag.oDESMAG_1_78.visible=!this.oPgFrm.Page1.oPag.oDESMAG_1_78.mHide()
    this.oPgFrm.Page1.oPag.oANMAGTER_1_79.visible=!this.oPgFrm.Page1.oPag.oANMAGTER_1_79.mHide()
    this.oPgFrm.Page1.oPag.oDESMAG_1_80.visible=!this.oPgFrm.Page1.oPag.oDESMAG_1_80.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_81.visible=!this.oPgFrm.Page1.oPag.oStr_1_81.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_137.visible=!this.oPgFrm.Page2.oPag.oStr_2_137.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_138.visible=!this.oPgFrm.Page2.oPag.oStr_2_138.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_139.visible=!this.oPgFrm.Page2.oPag.oStr_2_139.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCODEST_2_140.visible=!this.oPgFrm.Page2.oPag.oAGGANCODEST_2_140.mHide()
    this.oPgFrm.Page2.oPag.oNEWCODEST_2_141.visible=!this.oPgFrm.Page2.oPag.oNEWCODEST_2_141.mHide()
    this.oPgFrm.Page2.oPag.oAGGANCODCLA_2_142.visible=!this.oPgFrm.Page2.oPag.oAGGANCODCLA_2_142.mHide()
    this.oPgFrm.Page2.oPag.oNEWCODCLA_2_143.visible=!this.oPgFrm.Page2.oPag.oNEWCODCLA_2_143.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZoomAggC.Event(cEvent)
      .oPgFrm.Page2.oPag.ZoomAggPf.Event(cEvent)
      .oPgFrm.Page2.oPag.ZoomAggR.Event(cEvent)
      .oPgFrm.Page2.oPag.ZoomAggV.Event(cEvent)
      .oPgFrm.Page2.oPag.ZoomAggF.Event(cEvent)
      .oPgFrm.Page2.oPag.ZoomAggCo.Event(cEvent)
      .oPgFrm.Page2.oPag.ZoomAggA.Event(cEvent)
      .oPgFrm.Page2.oPag.ZoomAggPc.Event(cEvent)
        if lower(cEvent)==lower("ActivatePage 2")
          .Calculate_VVKKLMAOPJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 1")
          .Calculate_ENBBBCOFID()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PBANCODI
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PBANCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PBANCODI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PBANCODI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PBANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PBANCODI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPBANCODI_1_4'),i_cWhere,'GSAR_BZC',"ELENCO CONTI",'CONTIZOOM.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PBANCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PBANCODI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PBANCODI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PBANCODI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR1 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PBANCODI = space(15)
      endif
      this.w_DESCR1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_PEANCODI)) OR  (.w_PBANCODI<=.w_PEANCODI)) 
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_PBANCODI = space(15)
        this.w_DESCR1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PBANCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PEANCODI
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PEANCODI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_PEANCODI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PEANCODI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PEANCODI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PEANCODI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPEANCODI_1_5'),i_cWhere,'GSAR_BZC',"ELENCO CONTI",'CONTIZOOM.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PEANCODI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PEANCODI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_PEANCODI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PEANCODI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCR2 = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PEANCODI = space(15)
      endif
      this.w_DESCR2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_PEANCODI>=.w_PBANCODI) or (empty(.w_PBANCODI)))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_PEANCODI = space(15)
        this.w_DESCR2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PEANCODI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PANCONSU
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANCONSU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_PANCONSU)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_PANCONSU))
          select MCCODICE,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PANCONSU)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_PANCONSU)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_PANCONSU)+"%");

            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PANCONSU) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oPANCONSU_1_8'),i_cWhere,'GSAR_AMC',"Mastri",'GSAR_ACL.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANCONSU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_PANCONSU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_PANCONSU)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANCONSU = NVL(_Link_.MCCODICE,space(15))
      this.w_DESAPP = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PANCONSU = space(15)
      endif
      this.w_DESAPP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANCONSU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PANCONSU
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PANCONSU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_PANCONSU)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_PANCONSU))
          select MCCODICE,MCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PANCONSU)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_PANCONSU)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_PANCONSU)+"%");

            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PANCONSU) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oPANCONSU_1_9'),i_cWhere,'GSAR_AMC',"Mastri",'GSAR_AFR.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PANCONSU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_PANCONSU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_PANCONSU)
            select MCCODICE,MCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PANCONSU = NVL(_Link_.MCCODICE,space(15))
      this.w_DESAPP = NVL(_Link_.MCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PANCONSU = space(15)
      endif
      this.w_DESAPP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PANCONSU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCON
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCATCON_1_10'),i_cWhere,'',"CATEGORIE CONTABILI",'GSAR0AC2.CACOCLFO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCCC = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCON = space(5)
      endif
      this.w_DESCCC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCODIVA
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_ANCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_ANCODIVA))
          select IVCODIVA,IVDESIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_ANCODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_ANCODIVA)+"%");

            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oANCODIVA_1_11'),i_cWhere,'GSAR_AIV',"Codici IVA",'GSVE0MDV.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_ANCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_ANCODIVA)
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANTIPOPE
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCODIV_IDX,3]
    i_lTable = "TIPCODIV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2], .t., this.TIPCODIV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANTIPOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATO',True,'TIPCODIV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_ANTIPOPE)+"%");
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_ANCATOPE);

          i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TI__TIPO,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TI__TIPO',this.w_ANCATOPE;
                     ,'TICODICE',trim(this.w_ANTIPOPE))
          select TI__TIPO,TICODICE,TIDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TI__TIPO,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANTIPOPE)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANTIPOPE) and !this.bDontReportError
            deferred_cp_zoom('TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(oSource.parent,'oANTIPOPE_1_13'),i_cWhere,'GSAR_ATO',"Operazioni IVA",'GSVE_ZTI.TIPCODIV_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ANCATOPE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TI__TIPO,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TI__TIPO="+cp_ToStrODBC(this.w_ANCATOPE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',oSource.xKey(1);
                       ,'TICODICE',oSource.xKey(2))
            select TI__TIPO,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANTIPOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_ANTIPOPE);
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_ANCATOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',this.w_ANCATOPE;
                       ,'TICODICE',this.w_ANTIPOPE)
            select TI__TIPO,TICODICE,TIDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANTIPOPE = NVL(_Link_.TICODICE,space(10))
      this.w_TIDESCRI = NVL(_Link_.TIDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ANTIPOPE = space(10)
      endif
      this.w_TIDESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])+'\'+cp_ToStr(_Link_.TI__TIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCODIV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANTIPOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCODIRP
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODIRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_ANCODIRP)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_ANCODIRP))
          select TRCODTRI,TRDESTRI,TRFLACON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODIRP)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODIRP) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oANCODIRP_1_15'),i_cWhere,'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODIRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_ANCODIRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_ANCODIRP)
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODIRP = NVL(_Link_.TRCODTRI,space(5))
      this.w_DESIRPEF = NVL(_Link_.TRDESTRI,space(35))
      this.w_FLIRPE = NVL(_Link_.TRFLACON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODIRP = space(5)
      endif
      this.w_DESIRPEF = space(35)
      this.w_FLIRPE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLIRPE='R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire un codice tributo I.R.PE.F.")
        endif
        this.w_ANCODIRP = space(5)
        this.w_DESIRPEF = space(35)
        this.w_FLIRPE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODIRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCATCOM
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_ANCATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_ANCATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oANCATCOM_1_16'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_ANCATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_ANCATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCAC = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ANCATCOM = space(3)
      endif
      this.w_DESCAC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCATSCM
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
    i_lTable = "CAT_SCMA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2], .t., this.CAT_SCMA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCATSCM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASM',True,'CAT_SCMA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_ANCATSCM)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_ANCATSCM))
          select CSCODICE,CSDESCRI,CSTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCATSCM)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCATSCM) and !this.bDontReportError
            deferred_cp_zoom('CAT_SCMA','*','CSCODICE',cp_AbsName(oSource.parent,'oANCATSCM_1_17'),i_cWhere,'GSAR_ASM',"Categorie sconti/maggiorazioni",'GSAR_ACL.CAT_SCMA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCATSCM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_ANCATSCM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_ANCATSCM)
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCATSCM = NVL(_Link_.CSCODICE,space(5))
      this.w_DESSCM = NVL(_Link_.CSDESCRI,space(35))
      this.w_FLSCM = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANCATSCM = space(5)
      endif
      this.w_DESSCM = space(35)
      this.w_FLSCM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLSCM $ ' C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria sconti / maggiorazioni inesistente")
        endif
        this.w_ANCATSCM = space(5)
        this.w_DESSCM = space(35)
        this.w_FLSCM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_SCMA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCATSCM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCODZON
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_ANCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_ANCODZON))
          select ZOCODZON,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oANCODZON_1_18'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_ANCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_ANCODZON)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZON = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODZON = space(3)
      endif
      this.w_DESZON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANGRUPRO
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_lTable = "GRUPRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2], .t., this.GRUPRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANGRUPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGP',True,'GRUPRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_ANGRUPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_ANGRUPRO))
          select GPCODICE,GPDESCRI,GPTIPPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANGRUPRO)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANGRUPRO) and !this.bDontReportError
            deferred_cp_zoom('GRUPRO','*','GPCODICE',cp_AbsName(oSource.parent,'oANGRUPRO_1_19'),i_cWhere,'GSAR_AGP',"Categorie provvigioni",'GSAR_ACL.GRUPRO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANGRUPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_ANGRUPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_ANGRUPRO)
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANGRUPRO = NVL(_Link_.GPCODICE,space(5))
      this.w_DESGPP = NVL(_Link_.GPDESCRI,space(35))
      this.w_FLPRO = NVL(_Link_.GPTIPPRO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANGRUPRO = space(5)
      endif
      this.w_DESGPP = space(35)
      this.w_FLPRO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLPRO $ ' C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria provvigioni inesistente o non di tipo cliente")
        endif
        this.w_ANGRUPRO = space(5)
        this.w_DESGPP = space(35)
        this.w_FLPRO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANGRUPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCODAG1
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODAG1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_ANCODAG1)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_ANCODAG1))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODAG1)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_ANCODAG1)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_ANCODAG1)+"%");

            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCODAG1) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oANCODAG1_1_20'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODAG1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_ANCODAG1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_ANCODAG1)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODAG1 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE1 = NVL(_Link_.AGDESAGE,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODAG1 = space(5)
      endif
      this.w_DESAGE1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODAG1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANMAGTER
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANMAGTER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_ANMAGTER)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_ANMAGTER))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANMAGTER)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANMAGTER) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oANMAGTER_1_21'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANMAGTER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_ANMAGTER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_ANMAGTER)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANMAGTER = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ANMAGTER = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANMAGTER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANPAGPAR
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANPAGPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_ANPAGPAR)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_ANPAGPAR))
          select PACODICE,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANPAGPAR)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_ANPAGPAR)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_ANPAGPAR))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_ANPAGPAR)+"%");

            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANPAGPAR) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oANPAGPAR_1_23'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANPAGPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_ANPAGPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_ANPAGPAR)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANPAGPAR = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAR = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ANPAGPAR = space(5)
      endif
      this.w_DESPAR = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANPAGPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCODPAG
  func Link_1_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_ANCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_ANCODPAG))
          select PACODICE,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_ANCODPAG)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_ANCODPAG))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_ANCODPAG)+"%");

            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oANCODPAG_1_25'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_ANCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_ANCODPAG)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCODBA2
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODBA2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_ANCODBA2)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONSBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_ANCODBA2))
          select BACODBAN,BADESCRI,BACONSBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODBA2)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_ANCODBA2)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_ANCODBA2)+"%");

            select BACODBAN,BADESCRI,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCODBA2) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oANCODBA2_1_26'),i_cWhere,'GSTE_ACB',"Elenco conti banche",'GSAR_ACL.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONSBF";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODBA2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_ANCODBA2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_ANCODBA2)
            select BACODBAN,BADESCRI,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODBA2 = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBA2 = NVL(_Link_.BADESCRI,space(42))
      this.w_CONSBF = NVL(_Link_.BACONSBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODBA2 = space(15)
      endif
      this.w_DESBA2 = space(42)
      this.w_CONSBF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CONSBF='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente  o di tipo salvo buon fine")
        endif
        this.w_ANCODBA2 = space(15)
        this.w_DESBA2 = space(42)
        this.w_CONSBF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODBA2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCODIRP
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODIRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_ANCODIRP)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_ANCODIRP))
          select TRCODTRI,TRDESTRI,TRFLACON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODIRP)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODIRP) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oANCODIRP_1_29'),i_cWhere,'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODIRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_ANCODIRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_ANCODIRP)
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODIRP = NVL(_Link_.TRCODTRI,space(5))
      this.w_DESIRPEF = NVL(_Link_.TRDESTRI,space(35))
      this.w_FLIRPE = NVL(_Link_.TRFLACON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODIRP = space(5)
      endif
      this.w_DESIRPEF = space(35)
      this.w_FLIRPE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLIRPE='R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire un codice tributo I.R.PE.F.")
        endif
        this.w_ANCODIRP = space(5)
        this.w_DESIRPEF = space(35)
        this.w_FLIRPE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODIRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCODTR2
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODTR2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_ANCODTR2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_ANCODTR2))
          select TRCODTRI,TRDESTRI,TRFLACON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODTR2)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODTR2) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oANCODTR2_1_30'),i_cWhere,'GSAR_ATB',"",'GSRI2AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODTR2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_ANCODTR2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_ANCODTR2)
            select TRCODTRI,TRDESTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODTR2 = NVL(_Link_.TRCODTRI,space(5))
      this.w_DESTR2 = NVL(_Link_.TRDESTRI,space(35))
      this.w_FLACO2 = NVL(_Link_.TRFLACON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODTR2 = space(5)
      endif
      this.w_DESTR2 = space(35)
      this.w_FLACO2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLACO2<>'R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ANCODTR2 = space(5)
        this.w_DESTR2 = space(35)
        this.w_FLACO2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODTR2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWMASCON
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWMASCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_NEWMASCON)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCNUMLIV,MCTIPMAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_NEWMASCON))
          select MCCODICE,MCNUMLIV,MCTIPMAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWMASCON)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWMASCON) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oNEWMASCON_2_13'),i_cWhere,'GSAR_AMC',"Mastri",'GSAR_ACL.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCNUMLIV,MCTIPMAS";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWMASCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCNUMLIV,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_NEWMASCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_NEWMASCON)
            select MCCODICE,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWMASCON = NVL(_Link_.MCCODICE,space(15))
      this.w_NULIV = NVL(_Link_.MCNUMLIV,0)
      this.w_TIPMAS = NVL(_Link_.MCTIPMAS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NEWMASCON = space(15)
      endif
      this.w_NULIV = 0
      this.w_TIPMAS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NULIV=1 AND .w_TIPMAS='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NEWMASCON = space(15)
        this.w_NULIV = 0
        this.w_TIPMAS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWMASCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWMASCON
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWMASCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_NEWMASCON)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCNUMLIV,MCTIPMAS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_NEWMASCON))
          select MCCODICE,MCNUMLIV,MCTIPMAS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWMASCON)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWMASCON) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oNEWMASCON_2_14'),i_cWhere,'GSAR_AMC',"Mastri",'GSAR_AFR.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCNUMLIV,MCTIPMAS";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWMASCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCNUMLIV,MCTIPMAS";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_NEWMASCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_NEWMASCON)
            select MCCODICE,MCNUMLIV,MCTIPMAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWMASCON = NVL(_Link_.MCCODICE,space(15))
      this.w_NULIV = NVL(_Link_.MCNUMLIV,0)
      this.w_TIPMAS = NVL(_Link_.MCTIPMAS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NEWMASCON = space(15)
      endif
      this.w_NULIV = 0
      this.w_TIPMAS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NULIV=1 AND .w_TIPMAS='F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NEWMASCON = space(15)
        this.w_NULIV = 0
        this.w_TIPMAS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWMASCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWCATCON
  func Link_2_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_NEWCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_NEWCATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWCATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWCATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oNEWCATCON_2_18'),i_cWhere,'',"CATEGORIE CONTABILI",'GSAR0AC2.CACOCLFO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_NEWCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_NEWCATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWCATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCCC = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NEWCATCON = space(5)
      endif
      this.w_DESCCC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWANCODIVA
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWANCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_NEWANCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_NEWANCODIVA))
          select IVCODIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWANCODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDTOBSO like "+cp_ToStrODBC(trim(this.w_NEWANCODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDTOBSO like "+cp_ToStr(trim(this.w_NEWANCODIVA)+"%");

            select IVCODIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_NEWANCODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oNEWANCODIVA_2_20'),i_cWhere,'GSAR_AIV',"Codici IVA",'GSVE0MDV.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWANCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_NEWANCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_NEWANCODIVA)
            select IVCODIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWANCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NEWANCODIVA = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NEWANCODIVA = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWANCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWANTIPOPE
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCODIV_IDX,3]
    i_lTable = "TIPCODIV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2], .t., this.TIPCODIV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWANTIPOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATO',True,'TIPCODIV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_NEWANTIPOPE)+"%");
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_ANCATOPE);

          i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TI__TIPO,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TI__TIPO',this.w_ANCATOPE;
                     ,'TICODICE',trim(this.w_NEWANTIPOPE))
          select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TI__TIPO,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWANTIPOPE)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWANTIPOPE) and !this.bDontReportError
            deferred_cp_zoom('TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(oSource.parent,'oNEWANTIPOPE_2_23'),i_cWhere,'GSAR_ATO',"Operazioni IVA",'GSVE_ZTI.TIPCODIV_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ANCATOPE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice incongruente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TI__TIPO="+cp_ToStrODBC(this.w_ANCATOPE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',oSource.xKey(1);
                       ,'TICODICE',oSource.xKey(2))
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWANTIPOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_NEWANTIPOPE);
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_ANCATOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',this.w_ANCATOPE;
                       ,'TICODICE',this.w_NEWANTIPOPE)
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWANTIPOPE = NVL(_Link_.TICODICE,space(10))
      this.w_TIDESCRI = NVL(_Link_.TIDESCRI,space(30))
      this.w_TIDTOBSO = NVL(cp_ToDate(_Link_.TIDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NEWANTIPOPE = space(10)
      endif
      this.w_TIDESCRI = space(30)
      this.w_TIDTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_TIDTOBSO) OR .w_TIDTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice incongruente")
        endif
        this.w_NEWANTIPOPE = space(10)
        this.w_TIDESCRI = space(30)
        this.w_TIDTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])+'\'+cp_ToStr(_Link_.TI__TIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCODIV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWANTIPOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWANCODIRP
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWANCODIRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_NEWANCODIRP)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRFLACON,TRCAUPRE2";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_NEWANCODIRP))
          select TRCODTRI,TRFLACON,TRCAUPRE2;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWANCODIRP)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWANCODIRP) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oNEWANCODIRP_2_28'),i_cWhere,'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRFLACON,TRCAUPRE2";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRFLACON,TRCAUPRE2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWANCODIRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRFLACON,TRCAUPRE2";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_NEWANCODIRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_NEWANCODIRP)
            select TRCODTRI,TRFLACON,TRCAUPRE2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWANCODIRP = NVL(_Link_.TRCODTRI,space(5))
      this.w_NEWFLIRPE = NVL(_Link_.TRFLACON,space(1))
      this.w_CAUPRE2 = NVL(_Link_.TRCAUPRE2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NEWANCODIRP = space(5)
      endif
      this.w_NEWFLIRPE = space(1)
      this.w_CAUPRE2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NEWFLIRPE='R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire un codice tributo I.R.PE.F.")
        endif
        this.w_NEWANCODIRP = space(5)
        this.w_NEWFLIRPE = space(1)
        this.w_CAUPRE2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWANCODIRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWANCATCOM
  func Link_2_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWANCATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_NEWANCATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_NEWANCATCOM))
          select CTCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWANCATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWANCATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oNEWANCATCOM_2_33'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWANCATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_NEWANCATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_NEWANCATCOM)
            select CTCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWANCATCOM = NVL(_Link_.CTCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_NEWANCATCOM = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWANCATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWANCATSCM
  func Link_2_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
    i_lTable = "CAT_SCMA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2], .t., this.CAT_SCMA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWANCATSCM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASM',True,'CAT_SCMA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_NEWANCATSCM)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_NEWANCATSCM))
          select CSCODICE,CSTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWANCATSCM)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWANCATSCM) and !this.bDontReportError
            deferred_cp_zoom('CAT_SCMA','*','CSCODICE',cp_AbsName(oSource.parent,'oNEWANCATSCM_2_35'),i_cWhere,'GSAR_ASM',"Categorie sconti/maggiorazioni",'GSAR_ACL.CAT_SCMA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWANCATSCM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_NEWANCATSCM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_NEWANCATSCM)
            select CSCODICE,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWANCATSCM = NVL(_Link_.CSCODICE,space(5))
      this.w_NEWFLSCM = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NEWANCATSCM = space(5)
      endif
      this.w_NEWFLSCM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NEWFLSCM $ ' C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria sconti / maggiorazioni inesistente")
        endif
        this.w_NEWANCATSCM = space(5)
        this.w_NEWFLSCM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_SCMA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWANCATSCM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWANCODZON
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWANCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_NEWANCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_NEWANCODZON))
          select ZOCODZON,ZODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWANCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWANCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oNEWANCODZON_2_37'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWANCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_NEWANCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_NEWANCODZON)
            select ZOCODZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWANCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DATOBSOZ = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NEWANCODZON = space(3)
      endif
      this.w_DATOBSOZ = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSOZ) OR .w_DATOBSOZ>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice zona inesistente oppure obsoleto")
        endif
        this.w_NEWANCODZON = space(3)
        this.w_DATOBSOZ = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWANCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWANGRUPRO
  func Link_2_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_lTable = "GRUPRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2], .t., this.GRUPRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWANGRUPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGP',True,'GRUPRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_NEWANGRUPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPTIPPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_NEWANGRUPRO))
          select GPCODICE,GPTIPPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWANGRUPRO)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWANGRUPRO) and !this.bDontReportError
            deferred_cp_zoom('GRUPRO','*','GPCODICE',cp_AbsName(oSource.parent,'oNEWANGRUPRO_2_39'),i_cWhere,'GSAR_AGP',"Categorie provvigioni",'GSAR_ACL.GRUPRO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPTIPPRO";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWANGRUPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPTIPPRO";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_NEWANGRUPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_NEWANGRUPRO)
            select GPCODICE,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWANGRUPRO = NVL(_Link_.GPCODICE,space(5))
      this.w_NEWFLPRO = NVL(_Link_.GPTIPPRO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NEWANGRUPRO = space(5)
      endif
      this.w_NEWFLPRO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NEWFLPRO $ ' C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria provvigioni inesistente o non di tipo cliente")
        endif
        this.w_NEWANGRUPRO = space(5)
        this.w_NEWFLPRO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWANGRUPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWANCODAG1
  func Link_2_42(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWANCODAG1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_NEWANCODAG1)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_NEWANCODAG1))
          select AGCODAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWANCODAG1)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWANCODAG1) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oNEWANCODAG1_2_42'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWANCODAG1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_NEWANCODAG1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_NEWANCODAG1)
            select AGCODAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWANCODAG1 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DATOBSOA = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NEWANCODAG1 = space(5)
      endif
      this.w_DATOBSOA = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSOA) OR .w_DATOBSOA>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_NEWANCODAG1 = space(5)
        this.w_DATOBSOA = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWANCODAG1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWANMAGTER
  func Link_2_44(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWANMAGTER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_NEWANMAGTER)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_NEWANMAGTER))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWANMAGTER)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWANMAGTER) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oNEWANMAGTER_2_44'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWANMAGTER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_NEWANMAGTER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_NEWANMAGTER)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWANMAGTER = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_NEWANMAGTER = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWANMAGTER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWANMAGTER
  func Link_2_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWANMAGTER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_NEWANMAGTER)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGTIPMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_NEWANMAGTER))
          select MGCODMAG,MGTIPMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWANMAGTER)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWANMAGTER) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oNEWANMAGTER_2_45'),i_cWhere,'GSAR_AMA',"Magazzini",'GSCOWMAG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGTIPMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWANMAGTER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGTIPMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_NEWANMAGTER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_NEWANMAGTER)
            select MGCODMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWANMAGTER = NVL(_Link_.MGCODMAG,space(5))
      this.w_MAGWIP = NVL(_Link_.MGTIPMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NEWANMAGTER = space(5)
      endif
      this.w_MAGWIP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_MAGWIP='W'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino C/Lavoro inesistente o non tipo WIP")
        endif
        this.w_NEWANMAGTER = space(5)
        this.w_MAGWIP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWANMAGTER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWANCODPAG
  func Link_2_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWANCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_NEWANCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_NEWANCODPAG))
          select PACODICE,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWANCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWANCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oNEWANCODPAG_2_47'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWANCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_NEWANCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_NEWANCODPAG)
            select PACODICE,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWANCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DATOBSOP = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NEWANCODPAG = space(5)
      endif
      this.w_DATOBSOP = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSOP) OR .w_DATOBSOP>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente")
        endif
        this.w_NEWANCODPAG = space(5)
        this.w_DATOBSOP = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWANCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWANCODBA2
  func Link_2_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWANCODBA2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_NEWANCODBA2)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BACONSBF,BADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_NEWANCODBA2))
          select BACODBAN,BACONSBF,BADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWANCODBA2)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWANCODBA2) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oNEWANCODBA2_2_49'),i_cWhere,'GSTE_ACB',"Elenco conti banche",'GSAR_ACL.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACONSBF,BADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BACONSBF,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWANCODBA2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACONSBF,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_NEWANCODBA2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_NEWANCODBA2)
            select BACODBAN,BACONSBF,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWANCODBA2 = NVL(_Link_.BACODBAN,space(15))
      this.w_CONSBF = NVL(_Link_.BACONSBF,space(1))
      this.w_DATOBSOB = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NEWANCODBA2 = space(15)
      endif
      this.w_CONSBF = space(1)
      this.w_DATOBSOB = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSOB) OR .w_DATOBSOB>.w_OBTEST) AND .w_CONSBF='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente  o di tipo salvo buon fine")
        endif
        this.w_NEWANCODBA2 = space(15)
        this.w_CONSBF = space(1)
        this.w_DATOBSOB = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWANCODBA2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWANPAGPAR
  func Link_2_53(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWANPAGPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_NEWANPAGPAR)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_NEWANPAGPAR))
          select PACODICE,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWANPAGPAR)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWANPAGPAR) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oNEWANPAGPAR_2_53'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWANPAGPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_NEWANPAGPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_NEWANPAGPAR)
            select PACODICE,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWANPAGPAR = NVL(_Link_.PACODICE,space(5))
      this.w_DATOBSOPM = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_NEWANPAGPAR = space(5)
      endif
      this.w_DATOBSOPM = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSOPM) OR .w_DATOBSOPM>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente")
        endif
        this.w_NEWANPAGPAR = space(5)
        this.w_DATOBSOPM = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWANPAGPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWANCODIRPF
  func Link_2_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWANCODIRPF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_NEWANCODIRPF)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRFLACON,TRCAUPRE2";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_NEWANCODIRPF))
          select TRCODTRI,TRFLACON,TRCAUPRE2;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWANCODIRPF)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWANCODIRPF) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oNEWANCODIRPF_2_62'),i_cWhere,'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRFLACON,TRCAUPRE2";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRFLACON,TRCAUPRE2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWANCODIRPF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRFLACON,TRCAUPRE2";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_NEWANCODIRPF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_NEWANCODIRPF)
            select TRCODTRI,TRFLACON,TRCAUPRE2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWANCODIRPF = NVL(_Link_.TRCODTRI,space(5))
      this.w_FLIRPENEW = NVL(_Link_.TRFLACON,space(1))
      this.w_CAUPRE2F = NVL(_Link_.TRCAUPRE2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NEWANCODIRPF = space(5)
      endif
      this.w_FLIRPENEW = space(1)
      this.w_CAUPRE2F = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLIRPENEW='R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire un codice tributo I.R.PE.F.")
        endif
        this.w_NEWANCODIRPF = space(5)
        this.w_FLIRPENEW = space(1)
        this.w_CAUPRE2F = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWANCODIRPF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=NEWANCODTR2
  func Link_2_64(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NEWANCODTR2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_NEWANCODTR2)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRFLACON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_NEWANCODTR2))
          select TRCODTRI,TRFLACON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NEWANCODTR2)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NEWANCODTR2) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oNEWANCODTR2_2_64'),i_cWhere,'GSAR_ATB',"",'GSRI2AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRFLACON";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NEWANCODTR2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRFLACON";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_NEWANCODTR2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_NEWANCODTR2)
            select TRCODTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NEWANCODTR2 = NVL(_Link_.TRCODTRI,space(5))
      this.w_NEWFLACO2 = NVL(_Link_.TRFLACON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NEWANCODTR2 = space(5)
      endif
      this.w_NEWFLACO2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NEWFLACO2<>'R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NEWANCODTR2 = space(5)
        this.w_NEWFLACO2 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NEWANCODTR2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANMAGTER
  func Link_1_79(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANMAGTER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_ANMAGTER)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_ANMAGTER))
          select MGCODMAG,MGDESMAG,MGTIPMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANMAGTER)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANMAGTER) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oANMAGTER_1_79'),i_cWhere,'GSAR_AMA',"Magazzini",'GSCOWMAG.MAGAZZIN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANMAGTER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGTIPMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_ANMAGTER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_ANMAGTER)
            select MGCODMAG,MGDESMAG,MGTIPMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANMAGTER = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_ANMAGWIP = NVL(_Link_.MGTIPMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANMAGTER = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_ANMAGWIP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ANMAGWIP='W'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino C/Lavoro non tipo WIP")
        endif
        this.w_ANMAGTER = space(5)
        this.w_DESMAG = space(30)
        this.w_ANMAGWIP = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANMAGTER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_2.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPBANCODI_1_4.value==this.w_PBANCODI)
      this.oPgFrm.Page1.oPag.oPBANCODI_1_4.value=this.w_PBANCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oPEANCODI_1_5.value==this.w_PEANCODI)
      this.oPgFrm.Page1.oPag.oPEANCODI_1_5.value=this.w_PEANCODI
    endif
    if not(this.oPgFrm.Page1.oPag.oSEZIONE_1_6.RadioValue()==this.w_SEZIONE)
      this.oPgFrm.Page1.oPag.oSEZIONE_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSEZIONE_1_7.RadioValue()==this.w_SEZIONE)
      this.oPgFrm.Page1.oPag.oSEZIONE_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPANCONSU_1_8.value==this.w_PANCONSU)
      this.oPgFrm.Page1.oPag.oPANCONSU_1_8.value=this.w_PANCONSU
    endif
    if not(this.oPgFrm.Page1.oPag.oPANCONSU_1_9.value==this.w_PANCONSU)
      this.oPgFrm.Page1.oPag.oPANCONSU_1_9.value=this.w_PANCONSU
    endif
    if not(this.oPgFrm.Page1.oPag.oCATCON_1_10.value==this.w_CATCON)
      this.oPgFrm.Page1.oPag.oCATCON_1_10.value=this.w_CATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODIVA_1_11.value==this.w_ANCODIVA)
      this.oPgFrm.Page1.oPag.oANCODIVA_1_11.value=this.w_ANCODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oANTIPOPE_1_13.value==this.w_ANTIPOPE)
      this.oPgFrm.Page1.oPag.oANTIPOPE_1_13.value=this.w_ANTIPOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oANFLRITE_1_14.RadioValue()==this.w_ANFLRITE)
      this.oPgFrm.Page1.oPag.oANFLRITE_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODIRP_1_15.value==this.w_ANCODIRP)
      this.oPgFrm.Page1.oPag.oANCODIRP_1_15.value=this.w_ANCODIRP
    endif
    if not(this.oPgFrm.Page1.oPag.oANCATCOM_1_16.value==this.w_ANCATCOM)
      this.oPgFrm.Page1.oPag.oANCATCOM_1_16.value=this.w_ANCATCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oANCATSCM_1_17.value==this.w_ANCATSCM)
      this.oPgFrm.Page1.oPag.oANCATSCM_1_17.value=this.w_ANCATSCM
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODZON_1_18.value==this.w_ANCODZON)
      this.oPgFrm.Page1.oPag.oANCODZON_1_18.value=this.w_ANCODZON
    endif
    if not(this.oPgFrm.Page1.oPag.oANGRUPRO_1_19.value==this.w_ANGRUPRO)
      this.oPgFrm.Page1.oPag.oANGRUPRO_1_19.value=this.w_ANGRUPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODAG1_1_20.value==this.w_ANCODAG1)
      this.oPgFrm.Page1.oPag.oANCODAG1_1_20.value=this.w_ANCODAG1
    endif
    if not(this.oPgFrm.Page1.oPag.oANMAGTER_1_21.value==this.w_ANMAGTER)
      this.oPgFrm.Page1.oPag.oANMAGTER_1_21.value=this.w_ANMAGTER
    endif
    if not(this.oPgFrm.Page1.oPag.oANGESCON_1_22.RadioValue()==this.w_ANGESCON)
      this.oPgFrm.Page1.oPag.oANGESCON_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANPAGPAR_1_23.value==this.w_ANPAGPAR)
      this.oPgFrm.Page1.oPag.oANPAGPAR_1_23.value=this.w_ANPAGPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oANFLESIM_1_24.RadioValue()==this.w_ANFLESIM)
      this.oPgFrm.Page1.oPag.oANFLESIM_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODPAG_1_25.value==this.w_ANCODPAG)
      this.oPgFrm.Page1.oPag.oANCODPAG_1_25.value=this.w_ANCODPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODBA2_1_26.value==this.w_ANCODBA2)
      this.oPgFrm.Page1.oPag.oANCODBA2_1_26.value=this.w_ANCODBA2
    endif
    if not(this.oPgFrm.Page1.oPag.oANFLESIM_1_27.RadioValue()==this.w_ANFLESIM)
      this.oPgFrm.Page1.oPag.oANFLESIM_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANRITENU_1_28.RadioValue()==this.w_ANRITENU)
      this.oPgFrm.Page1.oPag.oANRITENU_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODIRP_1_29.value==this.w_ANCODIRP)
      this.oPgFrm.Page1.oPag.oANCODIRP_1_29.value=this.w_ANCODIRP
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODTR2_1_30.value==this.w_ANCODTR2)
      this.oPgFrm.Page1.oPag.oANCODTR2_1_30.value=this.w_ANCODTR2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR1_1_33.value==this.w_DESCR1)
      this.oPgFrm.Page1.oPag.oDESCR1_1_33.value=this.w_DESCR1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCR2_1_34.value==this.w_DESCR2)
      this.oPgFrm.Page1.oPag.oDESCR2_1_34.value=this.w_DESCR2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCCC_1_36.value==this.w_DESCCC)
      this.oPgFrm.Page1.oPag.oDESCCC_1_36.value=this.w_DESCCC
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGMASCON_2_12.RadioValue()==this.w_AGGMASCON)
      this.oPgFrm.Page2.oPag.oAGGMASCON_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWMASCON_2_13.value==this.w_NEWMASCON)
      this.oPgFrm.Page2.oPag.oNEWMASCON_2_13.value=this.w_NEWMASCON
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWMASCON_2_14.value==this.w_NEWMASCON)
      this.oPgFrm.Page2.oPag.oNEWMASCON_2_14.value=this.w_NEWMASCON
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGCATCON_2_17.RadioValue()==this.w_AGGCATCON)
      this.oPgFrm.Page2.oPag.oAGGCATCON_2_17.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWCATCON_2_18.value==this.w_NEWCATCON)
      this.oPgFrm.Page2.oPag.oNEWCATCON_2_18.value=this.w_NEWCATCON
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCODIVA_2_19.RadioValue()==this.w_AGGANCODIVA)
      this.oPgFrm.Page2.oPag.oAGGANCODIVA_2_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCODIVA_2_20.value==this.w_NEWANCODIVA)
      this.oPgFrm.Page2.oPag.oNEWANCODIVA_2_20.value=this.w_NEWANCODIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANTIPOPE_2_22.RadioValue()==this.w_AGGANTIPOPE)
      this.oPgFrm.Page2.oPag.oAGGANTIPOPE_2_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANTIPOPE_2_23.value==this.w_NEWANTIPOPE)
      this.oPgFrm.Page2.oPag.oNEWANTIPOPE_2_23.value=this.w_NEWANTIPOPE
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANPARTSN_2_25.RadioValue()==this.w_AGGANPARTSN)
      this.oPgFrm.Page2.oPag.oAGGANPARTSN_2_25.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANFLRITE_2_26.RadioValue()==this.w_AGGANFLRITE)
      this.oPgFrm.Page2.oPag.oAGGANFLRITE_2_26.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCODIRP_2_27.RadioValue()==this.w_AGGANCODIRP)
      this.oPgFrm.Page2.oPag.oAGGANCODIRP_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCODIRP_2_28.value==this.w_NEWANCODIRP)
      this.oPgFrm.Page2.oPag.oNEWANCODIRP_2_28.value=this.w_NEWANCODIRP
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCAURIT_2_29.RadioValue()==this.w_AGGANCAURIT)
      this.oPgFrm.Page2.oPag.oAGGANCAURIT_2_29.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCAURIT_2_30.RadioValue()==this.w_NEWANCAURIT)
      this.oPgFrm.Page2.oPag.oNEWANCAURIT_2_30.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANFLFIDO_2_31.RadioValue()==this.w_AGGANFLFIDO)
      this.oPgFrm.Page2.oPag.oAGGANFLFIDO_2_31.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCATCOM_2_32.RadioValue()==this.w_AGGANCATCOM)
      this.oPgFrm.Page2.oPag.oAGGANCATCOM_2_32.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCATCOM_2_33.value==this.w_NEWANCATCOM)
      this.oPgFrm.Page2.oPag.oNEWANCATCOM_2_33.value=this.w_NEWANCATCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCATSCM_2_34.RadioValue()==this.w_AGGANCATSCM)
      this.oPgFrm.Page2.oPag.oAGGANCATSCM_2_34.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCATSCM_2_35.value==this.w_NEWANCATSCM)
      this.oPgFrm.Page2.oPag.oNEWANCATSCM_2_35.value=this.w_NEWANCATSCM
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCODZON_2_36.RadioValue()==this.w_AGGANCODZON)
      this.oPgFrm.Page2.oPag.oAGGANCODZON_2_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCODZON_2_37.value==this.w_NEWANCODZON)
      this.oPgFrm.Page2.oPag.oNEWANCODZON_2_37.value=this.w_NEWANCODZON
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANGRUPRO_2_38.RadioValue()==this.w_AGGANGRUPRO)
      this.oPgFrm.Page2.oPag.oAGGANGRUPRO_2_38.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANGRUPRO_2_39.value==this.w_NEWANGRUPRO)
      this.oPgFrm.Page2.oPag.oNEWANGRUPRO_2_39.value=this.w_NEWANGRUPRO
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCODAG1_2_41.RadioValue()==this.w_AGGANCODAG1)
      this.oPgFrm.Page2.oPag.oAGGANCODAG1_2_41.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCODAG1_2_42.value==this.w_NEWANCODAG1)
      this.oPgFrm.Page2.oPag.oNEWANCODAG1_2_42.value=this.w_NEWANCODAG1
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANMAGTER_2_43.RadioValue()==this.w_AGGANMAGTER)
      this.oPgFrm.Page2.oPag.oAGGANMAGTER_2_43.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANMAGTER_2_44.value==this.w_NEWANMAGTER)
      this.oPgFrm.Page2.oPag.oNEWANMAGTER_2_44.value=this.w_NEWANMAGTER
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANMAGTER_2_45.value==this.w_NEWANMAGTER)
      this.oPgFrm.Page2.oPag.oNEWANMAGTER_2_45.value=this.w_NEWANMAGTER
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCODPAG_2_46.RadioValue()==this.w_AGGANCODPAG)
      this.oPgFrm.Page2.oPag.oAGGANCODPAG_2_46.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCODPAG_2_47.value==this.w_NEWANCODPAG)
      this.oPgFrm.Page2.oPag.oNEWANCODPAG_2_47.value=this.w_NEWANCODPAG
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCODBA2_2_48.RadioValue()==this.w_AGGANCODBA2)
      this.oPgFrm.Page2.oPag.oAGGANCODBA2_2_48.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCODBA2_2_49.value==this.w_NEWANCODBA2)
      this.oPgFrm.Page2.oPag.oNEWANCODBA2_2_49.value=this.w_NEWANCODBA2
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANGESCON_2_50.RadioValue()==this.w_AGGANGESCON)
      this.oPgFrm.Page2.oPag.oAGGANGESCON_2_50.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANFLGCON_2_51.RadioValue()==this.w_AGGANFLGCON)
      this.oPgFrm.Page2.oPag.oAGGANFLGCON_2_51.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANPAGPAR_2_52.RadioValue()==this.w_AGGANPAGPAR)
      this.oPgFrm.Page2.oPag.oAGGANPAGPAR_2_52.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANPAGPAR_2_53.value==this.w_NEWANPAGPAR)
      this.oPgFrm.Page2.oPag.oNEWANPAGPAR_2_53.value=this.w_NEWANPAGPAR
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANDATMOR_2_54.RadioValue()==this.w_AGGANDATMOR)
      this.oPgFrm.Page2.oPag.oAGGANDATMOR_2_54.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANDATMOR_2_55.value==this.w_NEWANDATMOR)
      this.oPgFrm.Page2.oPag.oNEWANDATMOR_2_55.value=this.w_NEWANDATMOR
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANFLESIM_2_56.RadioValue()==this.w_AGGANFLESIM)
      this.oPgFrm.Page2.oPag.oAGGANFLESIM_2_56.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANSAGINT_2_57.RadioValue()==this.w_AGGANSAGINT)
      this.oPgFrm.Page2.oPag.oAGGANSAGINT_2_57.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANSAGINT_2_58.value==this.w_NEWANSAGINT)
      this.oPgFrm.Page2.oPag.oNEWANSAGINT_2_58.value=this.w_NEWANSAGINT
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANSPRINT_2_59.RadioValue()==this.w_AGGANSPRINT)
      this.oPgFrm.Page2.oPag.oAGGANSPRINT_2_59.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANRITENU_2_60.RadioValue()==this.w_AGGANRITENU)
      this.oPgFrm.Page2.oPag.oAGGANRITENU_2_60.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCODIRPF_2_61.RadioValue()==this.w_AGGANCODIRPF)
      this.oPgFrm.Page2.oPag.oAGGANCODIRPF_2_61.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCODIRPF_2_62.value==this.w_NEWANCODIRPF)
      this.oPgFrm.Page2.oPag.oNEWANCODIRPF_2_62.value=this.w_NEWANCODIRPF
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCODTR2_2_63.RadioValue()==this.w_AGGANCODTR2)
      this.oPgFrm.Page2.oPag.oAGGANCODTR2_2_63.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCODTR2_2_64.value==this.w_NEWANCODTR2)
      this.oPgFrm.Page2.oPag.oNEWANCODTR2_2_64.value=this.w_NEWANCODTR2
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCAURITF_2_65.RadioValue()==this.w_AGGANCAURITF)
      this.oPgFrm.Page2.oPag.oAGGANCAURITF_2_65.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCAURITF_2_66.RadioValue()==this.w_NEWANCAURITF)
      this.oPgFrm.Page2.oPag.oNEWANCAURITF_2_66.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANPEINPS_2_67.RadioValue()==this.w_AGGANPEINPS)
      this.oPgFrm.Page2.oPag.oAGGANPEINPS_2_67.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANPEINPS_2_68.value==this.w_NEWANPEINPS)
      this.oPgFrm.Page2.oPag.oNEWANPEINPS_2_68.value=this.w_NEWANPEINPS
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANRIINPS_2_69.RadioValue()==this.w_AGGANRIINPS)
      this.oPgFrm.Page2.oPag.oAGGANRIINPS_2_69.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANRIINPS_2_70.value==this.w_NEWANRIINPS)
      this.oPgFrm.Page2.oPag.oNEWANRIINPS_2_70.value=this.w_NEWANRIINPS
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCOINPS_2_71.RadioValue()==this.w_AGGANCOINPS)
      this.oPgFrm.Page2.oPag.oAGGANCOINPS_2_71.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCOINPS_2_72.value==this.w_NEWANCOINPS)
      this.oPgFrm.Page2.oPag.oNEWANCOINPS_2_72.value=this.w_NEWANCOINPS
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCASPRO_2_73.RadioValue()==this.w_AGGANCASPRO)
      this.oPgFrm.Page2.oPag.oAGGANCASPRO_2_73.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCASPRO_2_74.value==this.w_NEWANCASPRO)
      this.oPgFrm.Page2.oPag.oNEWANCASPRO_2_74.value=this.w_NEWANCASPRO
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCODATT_2_75.RadioValue()==this.w_AGGANCODATT)
      this.oPgFrm.Page2.oPag.oAGGANCODATT_2_75.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCODATT_2_76.value==this.w_NEWANCODATT)
      this.oPgFrm.Page2.oPag.oNEWANCODATT_2_76.value=this.w_NEWANCODATT
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCODASS_2_77.RadioValue()==this.w_AGGANCODASS)
      this.oPgFrm.Page2.oPag.oAGGANCODASS_2_77.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCODASS_2_78.value==this.w_NEWANCODASS)
      this.oPgFrm.Page2.oPag.oNEWANCODASS_2_78.value=this.w_NEWANCODASS
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCOIMPS_2_79.RadioValue()==this.w_AGGANCOIMPS)
      this.oPgFrm.Page2.oPag.oAGGANCOIMPS_2_79.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWANCOIMPS_2_80.value==this.w_NEWANCOIMPS)
      this.oPgFrm.Page2.oPag.oNEWANCOIMPS_2_80.value=this.w_NEWANCOIMPS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAPP_1_39.value==this.w_DESAPP)
      this.oPgFrm.Page1.oPag.oDESAPP_1_39.value=this.w_DESAPP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_41.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_41.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oTIDESCRI_1_42.value==this.w_TIDESCRI)
      this.oPgFrm.Page1.oPag.oTIDESCRI_1_42.value=this.w_TIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIRPEF_1_45.value==this.w_DESIRPEF)
      this.oPgFrm.Page1.oPag.oDESIRPEF_1_45.value=this.w_DESIRPEF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAC_1_47.value==this.w_DESCAC)
      this.oPgFrm.Page1.oPag.oDESCAC_1_47.value=this.w_DESCAC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSCM_1_49.value==this.w_DESSCM)
      this.oPgFrm.Page1.oPag.oDESSCM_1_49.value=this.w_DESSCM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGPP_1_50.value==this.w_DESGPP)
      this.oPgFrm.Page1.oPag.oDESGPP_1_50.value=this.w_DESGPP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESZON_1_55.value==this.w_DESZON)
      this.oPgFrm.Page1.oPag.oDESZON_1_55.value=this.w_DESZON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAGE1_1_56.value==this.w_DESAGE1)
      this.oPgFrm.Page1.oPag.oDESAGE1_1_56.value=this.w_DESAGE1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAG_1_61.value==this.w_DESPAG)
      this.oPgFrm.Page1.oPag.oDESPAG_1_61.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBA2_1_62.value==this.w_DESBA2)
      this.oPgFrm.Page1.oPag.oDESBA2_1_62.value=this.w_DESBA2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAR_1_65.value==this.w_DESPAR)
      this.oPgFrm.Page1.oPag.oDESPAR_1_65.value=this.w_DESPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIRPEF_1_69.value==this.w_DESIRPEF)
      this.oPgFrm.Page1.oPag.oDESIRPEF_1_69.value=this.w_DESIRPEF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTR2_1_72.value==this.w_DESTR2)
      this.oPgFrm.Page1.oPag.oDESTR2_1_72.value=this.w_DESTR2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_78.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_78.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oANMAGTER_1_79.value==this.w_ANMAGTER)
      this.oPgFrm.Page1.oPag.oANMAGTER_1_79.value=this.w_ANMAGTER
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_80.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_80.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCODEST_2_140.RadioValue()==this.w_AGGANCODEST)
      this.oPgFrm.Page2.oPag.oAGGANCODEST_2_140.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWCODEST_2_141.value==this.w_NEWCODEST)
      this.oPgFrm.Page2.oPag.oNEWCODEST_2_141.value=this.w_NEWCODEST
    endif
    if not(this.oPgFrm.Page2.oPag.oAGGANCODCLA_2_142.RadioValue()==this.w_AGGANCODCLA)
      this.oPgFrm.Page2.oPag.oAGGANCODCLA_2_142.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNEWCODCLA_2_143.value==this.w_NEWCODCLA)
      this.oPgFrm.Page2.oPag.oNEWCODCLA_2_143.value=this.w_NEWCODCLA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_PEANCODI)) OR  (.w_PBANCODI<=.w_PEANCODI)) )  and not(empty(.w_PBANCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPBANCODI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not(((.w_PEANCODI>=.w_PBANCODI) or (empty(.w_PBANCODI))))  and not(empty(.w_PEANCODI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPEANCODI_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not(.w_FLIRPE='R')  and not(.w_SEZIONE<>'C'  Or .w_TIPCON='F')  and (.w_ANFLRITE<>'N' AND (g_RITE='S' OR IsAlt()))  and not(empty(.w_ANCODIRP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANCODIRP_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un codice tributo I.R.PE.F.")
          case   not(.w_FLSCM $ ' C')  and not(IsAlt() Or .w_SEZIONE$'CPRZ')  and not(empty(.w_ANCATSCM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANCATSCM_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria sconti / maggiorazioni inesistente")
          case   not(.w_FLPRO $ ' C')  and not(IsAlt() Or .w_SEZIONE$'CPRZA')  and not(empty(.w_ANGRUPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANGRUPRO_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria provvigioni inesistente o non di tipo cliente")
          case   not(.w_CONSBF='C')  and not(.w_SEZIONE<>'P' )  and not(empty(.w_ANCODBA2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANCODBA2_1_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente  o di tipo salvo buon fine")
          case   not(.w_FLIRPE='R')  and not(.w_SEZIONE<>'R'  Or .w_TIPCON='C')  and (.w_ANRITENU$'CST' and g_RITE='S')  and not(empty(.w_ANCODIRP))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANCODIRP_1_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un codice tributo I.R.PE.F.")
          case   not(.w_FLACO2<>'R')  and not(.w_SEZIONE<>'R'  Or .w_TIPCON='C')  and (.w_ANRITENU$'CT' and g_RITE='S')  and not(empty(.w_ANCODTR2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANCODTR2_1_30.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NEWMASCON)) or not(.w_NULIV=1 AND .w_TIPMAS='C'))  and not(.w_TIPCON='F' Or .w_SEZIONE<>'C')  and (.w_AGGMASCON='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWMASCON_2_13.SetFocus()
            i_bnoObbl = !empty(.w_NEWMASCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NEWMASCON)) or not(.w_NULIV=1 AND .w_TIPMAS='F'))  and not(.w_TIPCON='C' Or .w_SEZIONE<>'C')  and (.w_AGGMASCON='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWMASCON_2_14.SetFocus()
            i_bnoObbl = !empty(.w_NEWMASCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NEWCATCON))  and not(.w_SEZIONE<>'C')  and (.w_AGGCATCON='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWCATCON_2_18.SetFocus()
            i_bnoObbl = !empty(.w_NEWCATCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria contabile inesistente")
          case   ((empty(.w_NEWANCODIVA)) or not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)))  and not(.w_SEZIONE<>'C')  and (.w_AGGANCODIVA='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANCODIVA_2_20.SetFocus()
            i_bnoObbl = !empty(.w_NEWANCODIVA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NEWANTIPOPE)) or not(EMPTY(.w_TIDTOBSO) OR .w_TIDTOBSO>.w_OBTEST))  and not(.w_SEZIONE<>'C' )  and (.w_AGGANTIPOPE='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANTIPOPE_2_23.SetFocus()
            i_bnoObbl = !empty(.w_NEWANTIPOPE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice incongruente")
          case   ((empty(.w_NEWANCODIRP)) or not(.w_NEWFLIRPE='R'))  and not(.w_SEZIONE<>'C'  Or .w_TIPCON='F')  and (.w_AGGANCODIRP='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANCODIRP_2_28.SetFocus()
            i_bnoObbl = !empty(.w_NEWANCODIRP)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un codice tributo I.R.PE.F.")
          case   (empty(.w_NEWANCATCOM))  and not(.w_SEZIONE$'CPRZ' )  and (.w_AGGANCATCOM='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANCATCOM_2_33.SetFocus()
            i_bnoObbl = !empty(.w_NEWANCATCOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NEWANCATSCM)) or not(.w_NEWFLSCM $ ' C'))  and not(IsAlt() Or .w_SEZIONE$'CPRZ')  and (.w_AGGANCATSCM='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANCATSCM_2_35.SetFocus()
            i_bnoObbl = !empty(.w_NEWANCATSCM)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria sconti / maggiorazioni inesistente")
          case   ((empty(.w_NEWANCODZON)) or not(EMPTY(.w_DATOBSOZ) OR .w_DATOBSOZ>.w_OBTEST))  and not(.w_SEZIONE$'CPRZ' )  and (.w_AGGANCODZON='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANCODZON_2_37.SetFocus()
            i_bnoObbl = !empty(.w_NEWANCODZON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice zona inesistente oppure obsoleto")
          case   ((empty(.w_NEWANGRUPRO)) or not(.w_NEWFLPRO $ ' C'))  and not(IsAlt() Or .w_SEZIONE$'CPRZA')  and (.w_AGGANGRUPRO='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANGRUPRO_2_39.SetFocus()
            i_bnoObbl = !empty(.w_NEWANGRUPRO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria provvigioni inesistente o non di tipo cliente")
          case   ((empty(.w_NEWANCODAG1)) or not(EMPTY(.w_DATOBSOA) OR .w_DATOBSOA>.w_OBTEST))  and not(IsAlt() Or .w_SEZIONE$'CPRZA')  and (.w_AGGANCODAG1='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANCODAG1_2_42.SetFocus()
            i_bnoObbl = !empty(.w_NEWANCODAG1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   (empty(.w_NEWANMAGTER))  and not(IsAlt() Or .w_SEZIONE$'CPRZA')  and (.w_AGGANMAGTER='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANMAGTER_2_44.SetFocus()
            i_bnoObbl = !empty(.w_NEWANMAGTER)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente")
          case   ((empty(.w_NEWANMAGTER)) or not(.w_MAGWIP='W'))  and not(Isalt() Or .w_SEZIONE$'CPRZV')  and (.w_AGGANMAGTER='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANMAGTER_2_45.SetFocus()
            i_bnoObbl = !empty(.w_NEWANMAGTER)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino C/Lavoro inesistente o non tipo WIP")
          case   ((empty(.w_NEWANCODPAG)) or not(EMPTY(.w_DATOBSOP) OR .w_DATOBSOP>.w_OBTEST))  and not(.w_SEZIONE$'CRAVZ' )  and (.w_AGGANCODPAG='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANCODPAG_2_47.SetFocus()
            i_bnoObbl = !empty(.w_NEWANCODPAG)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente")
          case   ((empty(.w_NEWANCODBA2)) or not((EMPTY(.w_DATOBSOB) OR .w_DATOBSOB>.w_OBTEST) AND .w_CONSBF='C'))  and not(.w_SEZIONE$'CRAVZ' )  and (.w_AGGANCODBA2='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANCODBA2_2_49.SetFocus()
            i_bnoObbl = !empty(.w_NEWANCODBA2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente  o di tipo salvo buon fine")
          case   ((empty(.w_NEWANPAGPAR)) or not(EMPTY(.w_DATOBSOPM) OR .w_DATOBSOPM>.w_OBTEST))  and not(.w_SEZIONE<>'Z'  Or .w_TIPCON='F')  and (.w_AGGANPAGPAR='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANPAGPAR_2_53.SetFocus()
            i_bnoObbl = !empty(.w_NEWANPAGPAR)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente")
          case   (empty(.w_NEWANDATMOR))  and not(.w_SEZIONE<>'Z'  Or .w_TIPCON='F')  and (.w_AGGANDATMOR='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANDATMOR_2_55.SetFocus()
            i_bnoObbl = !empty(.w_NEWANDATMOR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NEWANSAGINT))  and not(!(.w_SEZIONE='Z' AND .w_TIPCON='C') AND !(.w_SEZIONE='P' AND .w_TIPCON='F'))  and (.w_AGGANSAGINT='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANSAGINT_2_58.SetFocus()
            i_bnoObbl = !empty(.w_NEWANSAGINT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_NEWANCODIRPF)) or not(.w_FLIRPENEW='R'))  and not(.w_SEZIONE<>'R')  and (.w_AGGANCODIRPF='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANCODIRPF_2_62.SetFocus()
            i_bnoObbl = !empty(.w_NEWANCODIRPF)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un codice tributo I.R.PE.F.")
          case   ((empty(.w_NEWANCODTR2)) or not(.w_NEWFLACO2<>'R'))  and not(.w_SEZIONE<>'R')  and (.w_AGGANCODTR2='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANCODTR2_2_64.SetFocus()
            i_bnoObbl = !empty(.w_NEWANCODTR2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NEWANPEINPS))  and not(.w_SEZIONE<>'R')  and (.w_AGGANPEINPS='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANPEINPS_2_68.SetFocus()
            i_bnoObbl = !empty(.w_NEWANPEINPS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NEWANRIINPS))  and not(.w_SEZIONE<>'R')  and (.w_AGGANRIINPS='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANRIINPS_2_70.SetFocus()
            i_bnoObbl = !empty(.w_NEWANRIINPS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_NEWANCODATT>=1 AND .w_NEWANCODATT<=28)  and not(.w_SEZIONE<>'R')  and (.w_AGGANCODATT='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWANCODATT_2_76.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice attivit� non valido (inserire 1..28)")
          case   not(.w_ANMAGWIP='W')  and not(Isalt() Or .w_SEZIONE$'CPRZV')  and not(empty(.w_ANMAGTER))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANMAGTER_1_79.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino C/Lavoro non tipo WIP")
          case   (empty(.w_NEWCODEST))  and not(.w_SEZIONE$'CPRZ' )  and (.w_AGGANCODEST='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWCODEST_2_141.SetFocus()
            i_bnoObbl = !empty(.w_NEWCODEST)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NEWCODCLA))  and not(.w_SEZIONE$'CPRZ' )  and (.w_AGGANCODCLA='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNEWCODCLA_2_143.SetFocus()
            i_bnoObbl = !empty(.w_NEWCODCLA)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPCON = this.w_TIPCON
    this.o_SEZIONE = this.w_SEZIONE
    this.o_ANFLRITE = this.w_ANFLRITE
    this.o_ANRITENU = this.w_ANRITENU
    this.o_AGGMASCON = this.w_AGGMASCON
    this.o_AGGCATCON = this.w_AGGCATCON
    this.o_AGGANCODIVA = this.w_AGGANCODIVA
    this.o_AGGANTIPOPE = this.w_AGGANTIPOPE
    this.o_AGGANFLRITE = this.w_AGGANFLRITE
    this.o_AGGANCODIRP = this.w_AGGANCODIRP
    this.o_NEWANCODIRP = this.w_NEWANCODIRP
    this.o_AGGANCAURIT = this.w_AGGANCAURIT
    this.o_AGGANCATCOM = this.w_AGGANCATCOM
    this.o_AGGANCATSCM = this.w_AGGANCATSCM
    this.o_AGGANCODZON = this.w_AGGANCODZON
    this.o_AGGANGRUPRO = this.w_AGGANGRUPRO
    this.o_AGGANCODAG1 = this.w_AGGANCODAG1
    this.o_AGGANMAGTER = this.w_AGGANMAGTER
    this.o_AGGANCODPAG = this.w_AGGANCODPAG
    this.o_AGGANCODBA2 = this.w_AGGANCODBA2
    this.o_AGGANGESCON = this.w_AGGANGESCON
    this.o_AGGANPAGPAR = this.w_AGGANPAGPAR
    this.o_AGGANDATMOR = this.w_AGGANDATMOR
    this.o_AGGANFLESIM = this.w_AGGANFLESIM
    this.o_AGGANSAGINT = this.w_AGGANSAGINT
    this.o_AGGANRITENU = this.w_AGGANRITENU
    this.o_AGGANCODIRPF = this.w_AGGANCODIRPF
    this.o_NEWANCODIRPF = this.w_NEWANCODIRPF
    this.o_AGGANCODTR2 = this.w_AGGANCODTR2
    this.o_AGGANCAURITF = this.w_AGGANCAURITF
    this.o_AGGANPEINPS = this.w_AGGANPEINPS
    this.o_AGGANRIINPS = this.w_AGGANRIINPS
    this.o_AGGANCOINPS = this.w_AGGANCOINPS
    this.o_AGGANCASPRO = this.w_AGGANCASPRO
    this.o_AGGANCODATT = this.w_AGGANCODATT
    this.o_AGGANCODASS = this.w_AGGANCODASS
    this.o_AGGANCOIMPS = this.w_AGGANCOIMPS
    this.o_AGGANCODEST = this.w_AGGANCODEST
    this.o_AGGANCODCLA = this.w_AGGANCODCLA
    return

enddefine

* --- Define pages as container
define class tgsar_kcfPag1 as StdContainer
  Width  = 784
  height = 647
  stdWidth  = 784
  stdheight = 647
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPCON_1_2 as StdCombo with uid="UUDBDOJQXS",rtseq=2,rtrep=.f.,left=199,top=28,width=126,height=21;
    , ToolTipText = "Tipo: cliente o fornitore";
    , HelpContextID = 223270710;
    , cFormVar="w_TIPCON",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_2.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oTIPCON_1_2.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_2.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      0))
  endfunc

  func oTIPCON_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_PBANCODI)
        bRes2=.link_1_4('Full')
      endif
      if .not. empty(.w_PEANCODI)
        bRes2=.link_1_5('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oPBANCODI_1_4 as StdField with uid="DFGMOVYBEF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PBANCODI", cQueryName = "PBANCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Codice di inizio selezione",;
    HelpContextID = 228122687,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=199, Top=83, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PBANCODI"

  func oPBANCODI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPBANCODI_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPBANCODI_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPBANCODI_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"ELENCO CONTI",'CONTIZOOM.CONTI_VZM',this.parent.oContained
  endproc
  proc oPBANCODI_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PBANCODI
     i_obj.ecpSave()
  endproc

  add object oPEANCODI_1_5 as StdField with uid="MRCFGLSJLJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PEANCODI", cQueryName = "PEANCODI",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Codice di fine selezione",;
    HelpContextID = 228123455,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=199, Top=121, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PEANCODI"

  func oPEANCODI_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPEANCODI_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPEANCODI_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPEANCODI_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"ELENCO CONTI",'CONTIZOOM.CONTI_VZM',this.parent.oContained
  endproc
  proc oPEANCODI_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PEANCODI
     i_obj.ecpSave()
  endproc


  add object oSEZIONE_1_6 as StdCombo with uid="KTKRERAHDR",rtseq=5,rtrep=.f.,left=199,top=160,width=135,height=22;
    , ToolTipText = "Tipologia dati da aggiornare";
    , HelpContextID = 223703846;
    , cFormVar="w_SEZIONE",RowSource=""+"Contabili,"+"Vendite,"+"Pagamenti,"+"Contenzioso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSEZIONE_1_6.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'V',;
    iif(this.value =3,'P',;
    iif(this.value =4,'Z',;
    space(1))))))
  endfunc
  func oSEZIONE_1_6.GetRadio()
    this.Parent.oContained.w_SEZIONE = this.RadioValue()
    return .t.
  endfunc

  func oSEZIONE_1_6.SetRadio()
    this.Parent.oContained.w_SEZIONE=trim(this.Parent.oContained.w_SEZIONE)
    this.value = ;
      iif(this.Parent.oContained.w_SEZIONE=='C',1,;
      iif(this.Parent.oContained.w_SEZIONE=='V',2,;
      iif(this.Parent.oContained.w_SEZIONE=='P',3,;
      iif(this.Parent.oContained.w_SEZIONE=='Z',4,;
      0))))
  endfunc

  func oSEZIONE_1_6.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='F')
    endwith
  endfunc


  add object oSEZIONE_1_7 as StdCombo with uid="MZEDBCHKGQ",rtseq=6,rtrep=.f.,left=199,top=160,width=135,height=22;
    , ToolTipText = "Tipologia dati da aggiornare";
    , HelpContextID = 223703846;
    , cFormVar="w_SEZIONE",RowSource=""+"Contabili,"+"Percipiente,"+"Acquisti,"+"Pagamenti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSEZIONE_1_7.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'R',;
    iif(this.value =3,'A',;
    iif(this.value =4,'P',;
    space(1))))))
  endfunc
  func oSEZIONE_1_7.GetRadio()
    this.Parent.oContained.w_SEZIONE = this.RadioValue()
    return .t.
  endfunc

  func oSEZIONE_1_7.SetRadio()
    this.Parent.oContained.w_SEZIONE=trim(this.Parent.oContained.w_SEZIONE)
    this.value = ;
      iif(this.Parent.oContained.w_SEZIONE=='C',1,;
      iif(this.Parent.oContained.w_SEZIONE=='R',2,;
      iif(this.Parent.oContained.w_SEZIONE=='A',3,;
      iif(this.Parent.oContained.w_SEZIONE=='P',4,;
      0))))
  endfunc

  func oSEZIONE_1_7.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='C')
    endwith
  endfunc

  add object oPANCONSU_1_8 as StdField with uid="UCPMPLCJTH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PANCONSU", cQueryName = "PANCONSU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggruppamento selezionato",;
    HelpContextID = 223260491,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=199, Top=200, cSayPict="p_MAS", cGetPict="p_MAS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_PANCONSU"

  func oPANCONSU_1_8.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='F' Or .w_SEZIONE<>'C')
    endwith
  endfunc

  func oPANCONSU_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPANCONSU_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPANCONSU_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oPANCONSU_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri",'GSAR_ACL.MASTRI_VZM',this.parent.oContained
  endproc
  proc oPANCONSU_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_PANCONSU
     i_obj.ecpSave()
  endproc

  add object oPANCONSU_1_9 as StdField with uid="EXMJWATBSW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PANCONSU", cQueryName = "PANCONSU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro selezionato",;
    HelpContextID = 223260491,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=199, Top=200, cSayPict="p_MAS", cGetPict="p_MAS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_PANCONSU"

  func oPANCONSU_1_9.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='C' Or .w_SEZIONE<>'C')
    endwith
  endfunc

  func oPANCONSU_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oPANCONSU_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPANCONSU_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oPANCONSU_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri",'GSAR_AFR.MASTRI_VZM',this.parent.oContained
  endproc
  proc oPANCONSU_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_PANCONSU
     i_obj.ecpSave()
  endproc

  add object oCATCON_1_10 as StdField with uid="HVTGPSGIMF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CATCON", cQueryName = "CATCON",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria contabile inesistente",;
    ToolTipText = "Categoria contabile selezionata",;
    HelpContextID = 223284774,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=199, Top=243, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", oKey_1_1="C2CODICE", oKey_1_2="this.w_CATCON"

  func oCATCON_1_10.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C')
    endwith
  endfunc

  func oCATCON_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCON_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCON_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCATCON_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE CONTABILI",'GSAR0AC2.CACOCLFO_VZM',this.parent.oContained
  endproc

  add object oANCODIVA_1_11 as StdField with uid="VANWCRADIA",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ANCODIVA", cQueryName = "ANCODIVA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice iva di esenzione/agevolazione selezionato",;
    HelpContextID = 128584519,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=199, Top=295, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_ANCODIVA"

  func oANCODIVA_1_11.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C')
    endwith
  endfunc

  func oANCODIVA_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODIVA_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODIVA_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oANCODIVA_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'GSVE0MDV.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oANCODIVA_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_ANCODIVA
     i_obj.ecpSave()
  endproc

  add object oANTIPOPE_1_13 as StdField with uid="BUDUNVSVAR",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ANTIPOPE", cQueryName = "ANTIPOPE",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente",;
    ToolTipText = "Tipo operazione IVA selezionata",;
    HelpContextID = 241507147,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=199, Top=350, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPCODIV", cZoomOnZoom="GSAR_ATO", oKey_1_1="TI__TIPO", oKey_1_2="this.w_ANCATOPE", oKey_2_1="TICODICE", oKey_2_2="this.w_ANTIPOPE"

  func oANTIPOPE_1_13.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  func oANTIPOPE_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oANTIPOPE_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANTIPOPE_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIPCODIV_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_ANCATOPE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStr(this.Parent.oContained.w_ANCATOPE)
    endif
    do cp_zoom with 'TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(this.parent,'oANTIPOPE_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATO',"Operazioni IVA",'GSVE_ZTI.TIPCODIV_VZM',this.parent.oContained
  endproc
  proc oANTIPOPE_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TI__TIPO=w_ANCATOPE
     i_obj.w_TICODICE=this.parent.oContained.w_ANTIPOPE
     i_obj.ecpSave()
  endproc


  add object oANFLRITE_1_14 as StdCombo with uid="BKMTXPTZEQ",rtseq=13,rtrep=.f.,left=199,top=404,width=102,height=22;
    , ToolTipText = "Filtro su gestione dati ritenute subite";
    , HelpContextID = 143080267;
    , cFormVar="w_ANFLRITE",RowSource=""+"Nessun filtro,"+"Attivo,"+"Disattivo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oANFLRITE_1_14.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oANFLRITE_1_14.GetRadio()
    this.Parent.oContained.w_ANFLRITE = this.RadioValue()
    return .t.
  endfunc

  func oANFLRITE_1_14.SetRadio()
    this.Parent.oContained.w_ANFLRITE=trim(this.Parent.oContained.w_ANFLRITE)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLRITE=='T',1,;
      iif(this.Parent.oContained.w_ANFLRITE=='S',2,;
      iif(this.Parent.oContained.w_ANFLRITE=='N',3,;
      0)))
  endfunc

  func oANFLRITE_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_RITE='S' OR IsAlt())
    endwith
   endif
  endfunc

  func oANFLRITE_1_14.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oANCODIRP_1_15 as StdField with uid="LRDFFOSDTV",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ANCODIRP", cQueryName = "ANCODIRP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un codice tributo I.R.PE.F.",;
    ToolTipText = "Codice tributo selezionato",;
    HelpContextID = 128584534,;
   bGlobalFont=.t.,;
    Height=21, Width=67, Left=199, Top=457, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_ANCODIRP"

  func oANCODIRP_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLRITE<>'N' AND (g_RITE='S' OR IsAlt()))
    endwith
   endif
  endfunc

  func oANCODIRP_1_15.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C'  Or .w_TIPCON='F')
    endwith
  endfunc

  func oANCODIRP_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODIRP_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODIRP_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oANCODIRP_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oANCODIRP_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_ANCODIRP
     i_obj.ecpSave()
  endproc

  add object oANCATCOM_1_16 as StdField with uid="EABMHGYNOV",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ANCATCOM", cQueryName = "ANCATCOM",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale associata al cliente / fornitore",;
    HelpContextID = 43780947,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=199, Top=200, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_ANCATCOM"

  func oANCATCOM_1_16.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  func oANCATCOM_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCATCOM_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCATCOM_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oANCATCOM_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oANCATCOM_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_ANCATCOM
     i_obj.ecpSave()
  endproc

  add object oANCATSCM_1_17 as StdField with uid="YPVAFYDCXY",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ANCATSCM", cQueryName = "ANCATSCM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria sconti / maggiorazioni inesistente",;
    ToolTipText = "Codice della categoria sconti / maggiorazioni associata al cliente / fornitore",;
    HelpContextID = 43780947,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=199, Top=243, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_SCMA", cZoomOnZoom="GSAR_ASM", oKey_1_1="CSCODICE", oKey_1_2="this.w_ANCATSCM"

  func oANCATSCM_1_17.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZ')
    endwith
  endfunc

  func oANCATSCM_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCATSCM_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCATSCM_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_SCMA','*','CSCODICE',cp_AbsName(this.parent,'oANCATSCM_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASM',"Categorie sconti/maggiorazioni",'GSAR_ACL.CAT_SCMA_VZM',this.parent.oContained
  endproc
  proc oANCATSCM_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCODICE=this.parent.oContained.w_ANCATSCM
     i_obj.ecpSave()
  endproc

  add object oANCODZON_1_18 as StdField with uid="USHUBMASZS",rtseq=17,rtrep=.f.,;
    cFormVar = "w_ANCODZON", cQueryName = "ANCODZON",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente",;
    ToolTipText = "Codice della zona di appartenenza",;
    HelpContextID = 145361748,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=199, Top=295, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_ANCODZON"

  func oANCODZON_1_18.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  func oANCODZON_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODZON_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODZON_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oANCODZON_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oANCODZON_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_ANCODZON
     i_obj.ecpSave()
  endproc

  add object oANGRUPRO_1_19 as StdField with uid="YMPGGNTBZE",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ANGRUPRO", cQueryName = "ANGRUPRO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria provvigioni inesistente o non di tipo cliente",;
    ToolTipText = "Codice della categoria provvigioni associata al cliente",;
    HelpContextID = 264063829,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=199, Top=350, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUPRO", cZoomOnZoom="GSAR_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_ANGRUPRO"

  func oANGRUPRO_1_19.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  func oANGRUPRO_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oANGRUPRO_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANGRUPRO_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPRO','*','GPCODICE',cp_AbsName(this.parent,'oANGRUPRO_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGP',"Categorie provvigioni",'GSAR_ACL.GRUPRO_VZM',this.parent.oContained
  endproc
  proc oANGRUPRO_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_ANGRUPRO
     i_obj.ecpSave()
  endproc

  add object oANCODAG1_1_20 as StdField with uid="ABUWGLDDZP",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ANCODAG1", cQueryName = "ANCODAG1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente",;
    ToolTipText = "Codice agente che cura i rapporti con il cliente",;
    HelpContextID = 262802231,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=199, Top=404, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_ANCODAG1"

  func oANCODAG1_1_20.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  func oANCODAG1_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODAG1_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODAG1_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oANCODAG1_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oANCODAG1_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_ANCODAG1
     i_obj.ecpSave()
  endproc

  add object oANMAGTER_1_21 as StdField with uid="CBYAYXONSV",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ANMAGTER", cQueryName = "ANMAGTER",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente",;
    ToolTipText = "Codice magazzino utilizzato per l'eventuale assegnazione dei documenti",;
    HelpContextID = 46967640,;
   bGlobalFont=.t.,;
    Height=21, Width=67, Left=199, Top=457, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_ANMAGTER"

  func oANMAGTER_1_21.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  func oANMAGTER_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oANMAGTER_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANMAGTER_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oANMAGTER_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oANMAGTER_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_ANMAGTER
     i_obj.ecpSave()
  endproc


  add object oANGESCON_1_22 as StdCombo with uid="NYQOTWVXPC",rtseq=21,rtrep=.f.,left=199,top=200,width=127,height=21;
    , ToolTipText = "Filtro su tipo di gestione del contenzioso";
    , HelpContextID = 43010900;
    , cFormVar="w_ANGESCON",RowSource=""+"Nessun blocco,"+"Moratoria attivabile,"+"Blocco attivabile,"+"Nessun filtro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oANGESCON_1_22.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'M',;
    iif(this.value =3,'B',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oANGESCON_1_22.GetRadio()
    this.Parent.oContained.w_ANGESCON = this.RadioValue()
    return .t.
  endfunc

  func oANGESCON_1_22.SetRadio()
    this.Parent.oContained.w_ANGESCON=trim(this.Parent.oContained.w_ANGESCON)
    this.value = ;
      iif(this.Parent.oContained.w_ANGESCON=='N',1,;
      iif(this.Parent.oContained.w_ANGESCON=='M',2,;
      iif(this.Parent.oContained.w_ANGESCON=='B',3,;
      iif(this.Parent.oContained.w_ANGESCON=='T',4,;
      0))))
  endfunc

  func oANGESCON_1_22.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oANPAGPAR_1_23 as StdField with uid="WZJVBONSIB",rtseq=22,rtrep=.f.,;
    cFormVar = "w_ANPAGPAR", cQueryName = "ANPAGPAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente",;
    ToolTipText = "Codice pagamento praticato al cliente in particolari situazioni (es.moratoria fido ecc.)",;
    HelpContextID = 248306520,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=199, Top=243, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_ANPAGPAR"

  func oANPAGPAR_1_23.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc

  func oANPAGPAR_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oANPAGPAR_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANPAGPAR_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oANPAGPAR_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oANPAGPAR_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_ANPAGPAR
     i_obj.ecpSave()
  endproc


  add object oANFLESIM_1_24 as StdCombo with uid="YWJNOPGXNM",rtseq=23,rtrep=.f.,left=199,top=295,width=104,height=22;
    , ToolTipText = "Filtro sull'applicazione degli interessi di mora al cliente";
    , HelpContextID = 239649965;
    , cFormVar="w_ANFLESIM",RowSource=""+"Si,"+"No,"+"Nessun filtro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oANFLESIM_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oANFLESIM_1_24.GetRadio()
    this.Parent.oContained.w_ANFLESIM = this.RadioValue()
    return .t.
  endfunc

  func oANFLESIM_1_24.SetRadio()
    this.Parent.oContained.w_ANFLESIM=trim(this.Parent.oContained.w_ANFLESIM)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLESIM=='S',1,;
      iif(this.Parent.oContained.w_ANFLESIM=='N',2,;
      iif(this.Parent.oContained.w_ANFLESIM=='T',3,;
      0)))
  endfunc

  func oANFLESIM_1_24.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oANCODPAG_1_25 as StdField with uid="AKEPZSFPPD",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ANCODPAG", cQueryName = "ANCODPAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente",;
    ToolTipText = "Codice del pagamento selezionato",;
    HelpContextID = 246025037,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=199, Top=200, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_ANCODPAG"

  func oANCODPAG_1_25.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'P' )
    endwith
  endfunc

  func oANCODPAG_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODPAG_1_25.ecpDrop(oSource)
    this.Parent.oContained.link_1_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODPAG_1_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oANCODPAG_1_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oANCODPAG_1_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_ANCODPAG
     i_obj.ecpSave()
  endproc

  add object oANCODBA2_1_26 as StdField with uid="UYWAYFXXOY",rtseq=25,rtrep=.f.,;
    cFormVar = "w_ANCODBA2", cQueryName = "ANCODBA2",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente  o di tipo salvo buon fine",;
    ToolTipText = "Codice del conto corrente della nostra banca comunicato al cliente/fornitore",;
    HelpContextID = 11143992,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=199, Top=243, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_ANCODBA2"

  func oANCODBA2_1_26.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'P' )
    endwith
  endfunc

  func oANCODBA2_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODBA2_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODBA2_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oANCODBA2_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Elenco conti banche",'GSAR_ACL.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oANCODBA2_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_ANCODBA2
     i_obj.ecpSave()
  endproc


  add object oANFLESIM_1_27 as StdCombo with uid="INJJLZSDCA",rtseq=26,rtrep=.f.,left=199,top=295,width=104,height=22;
    , ToolTipText = "Filtro sull'applicazione degli interessi di mora al fornitore";
    , HelpContextID = 239649965;
    , cFormVar="w_ANFLESIM",RowSource=""+"Si,"+"No,"+"Nessun filtro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oANFLESIM_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oANFLESIM_1_27.GetRadio()
    this.Parent.oContained.w_ANFLESIM = this.RadioValue()
    return .t.
  endfunc

  func oANFLESIM_1_27.SetRadio()
    this.Parent.oContained.w_ANFLESIM=trim(this.Parent.oContained.w_ANFLESIM)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLESIM=='S',1,;
      iif(this.Parent.oContained.w_ANFLESIM=='N',2,;
      iif(this.Parent.oContained.w_ANFLESIM=='T',3,;
      0)))
  endfunc

  func oANFLESIM_1_27.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'P'   Or .w_TIPCON='C')
    endwith
  endfunc


  add object oANRITENU_1_28 as StdCombo with uid="JCZDGVFATC",rtseq=27,rtrep=.f.,left=199,top=200,width=134,height=21;
    , ToolTipText = "Soggetto o meno a ritenuta/e";
    , HelpContextID = 190514341;
    , cFormVar="w_ANRITENU",RowSource=""+"Non soggetto,"+"Solo IRPEF,"+"IRPEF e contr. prev.,"+"Nessun filtro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oANRITENU_1_28.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    iif(this.value =4,'T',;
    space(1))))))
  endfunc
  func oANRITENU_1_28.GetRadio()
    this.Parent.oContained.w_ANRITENU = this.RadioValue()
    return .t.
  endfunc

  func oANRITENU_1_28.SetRadio()
    this.Parent.oContained.w_ANRITENU=trim(this.Parent.oContained.w_ANRITENU)
    this.value = ;
      iif(this.Parent.oContained.w_ANRITENU=='N',1,;
      iif(this.Parent.oContained.w_ANRITENU=='S',2,;
      iif(this.Parent.oContained.w_ANRITENU=='C',3,;
      iif(this.Parent.oContained.w_ANRITENU=='T',4,;
      0))))
  endfunc

  func oANRITENU_1_28.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R'  Or .w_TIPCON='C')
    endwith
  endfunc

  add object oANCODIRP_1_29 as StdField with uid="STPBUQUNOR",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ANCODIRP", cQueryName = "ANCODIRP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un codice tributo I.R.PE.F.",;
    ToolTipText = "Codice tributo selezionato",;
    HelpContextID = 128584534,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=199, Top=243, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_ANCODIRP"

  func oANCODIRP_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANRITENU$'CST' and g_RITE='S')
    endwith
   endif
  endfunc

  func oANCODIRP_1_29.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R'  Or .w_TIPCON='C')
    endwith
  endfunc

  func oANCODIRP_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODIRP_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODIRP_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oANCODIRP_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oANCODIRP_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_ANCODIRP
     i_obj.ecpSave()
  endproc

  add object oANCODTR2_1_30 as StdField with uid="EYFFUYUWBJ",rtseq=29,rtrep=.f.,;
    cFormVar = "w_ANCODTR2", cQueryName = "ANCODTR2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo per contributo previdenziale selezionato",;
    HelpContextID = 44698424,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=199, Top=295, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_ANCODTR2"

  func oANCODTR2_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANRITENU$'CT' and g_RITE='S')
    endwith
   endif
  endfunc

  func oANCODTR2_1_30.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R'  Or .w_TIPCON='C')
    endwith
  endfunc

  func oANCODTR2_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODTR2_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODTR2_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oANCODTR2_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"",'GSRI2AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oANCODTR2_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_ANCODTR2
     i_obj.ecpSave()
  endproc

  add object oDESCR1_1_33 as StdField with uid="CHLOPGXLBM",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESCR1", cQueryName = "DESCR1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 8323638,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=340, Top=83, InputMask=replicate('X',40)

  func oDESCR1_1_33.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc

  add object oDESCR2_1_34 as StdField with uid="MINMVUHPXL",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESCR2", cQueryName = "DESCR2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 25100854,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=340, Top=121, InputMask=replicate('X',40)

  func oDESCR2_1_34.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='A')
    endwith
  endfunc

  add object oDESCCC_1_36 as StdField with uid="KHIMEAPONY",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESCCC", cQueryName = "DESCCC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 26149430,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=340, Top=243, InputMask=replicate('X',35)

  func oDESCCC_1_36.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  add object oDESAPP_1_39 as StdField with uid="LXNCFWNKDK",rtseq=103,rtrep=.f.,;
    cFormVar = "w_DESAPP", cQueryName = "DESAPP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 257753654,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=340, Top=200, InputMask=replicate('X',40)

  func oDESAPP_1_39.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C')
    endwith
  endfunc

  add object oDESIVA_1_41 as StdField with uid="IMVNNLYOWB",rtseq=104,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 12911158,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=340, Top=295, InputMask=replicate('X',35)

  func oDESIVA_1_41.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C')
    endwith
  endfunc

  add object oTIDESCRI_1_42 as StdField with uid="TNOOFYXTGO",rtseq=105,rtrep=.f.,;
    cFormVar = "w_TIDESCRI", cQueryName = "TIDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 42997631,;
   bGlobalFont=.t.,;
    Height=21, Width=323, Left=340, Top=350, InputMask=replicate('X',30)

  func oTIDESCRI_1_42.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  add object oDESIRPEF_1_45 as StdField with uid="SWCQZMXUAI",rtseq=106,rtrep=.f.,;
    cFormVar = "w_DESIRPEF", cQueryName = "DESIRPEF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 260375164,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=340, Top=457, InputMask=replicate('X',60)

  func oDESIRPEF_1_45.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oDESCAC_1_47 as StdField with uid="TFNKVTIPBP",rtseq=108,rtrep=.f.,;
    cFormVar = "w_DESCAC", cQueryName = "DESCAC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 24052278,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=340, Top=200, InputMask=replicate('X',35)

  func oDESCAC_1_47.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oDESSCM_1_49 as StdField with uid="EXFRMZOCSP",rtseq=109,rtrep=.f.,;
    cFormVar = "w_DESSCM", cQueryName = "DESSCM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 194970166,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=340, Top=243, InputMask=replicate('X',35)

  func oDESSCM_1_49.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oDESGPP_1_50 as StdField with uid="COHMBSMALT",rtseq=110,rtrep=.f.,;
    cFormVar = "w_DESGPP", cQueryName = "DESGPP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 258146870,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=340, Top=350, InputMask=replicate('X',35)

  func oDESGPP_1_50.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  add object oDESZON_1_55 as StdField with uid="ENFHFGPEKI",rtseq=113,rtrep=.f.,;
    cFormVar = "w_DESZON", cQueryName = "DESZON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 224789046,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=340, Top=295, InputMask=replicate('X',35)

  func oDESZON_1_55.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oDESAGE1_1_56 as StdField with uid="DPGOZUSHFG",rtseq=114,rtrep=.f.,;
    cFormVar = "w_DESAGE1", cQueryName = "DESAGE1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 63767094,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=340, Top=404, InputMask=replicate('X',35)

  func oDESAGE1_1_56.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  add object oDESPAG_1_61 as StdField with uid="LNHIRUXQRP",rtseq=115,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 92013110,;
   bGlobalFont=.t.,;
    Height=21, Width=226, Left=340, Top=200, InputMask=replicate('X',30)

  func oDESPAG_1_61.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'P' )
    endwith
  endfunc

  add object oDESBA2_1_62 as StdField with uid="TJMOSEPKNW",rtseq=116,rtrep=.f.,;
    cFormVar = "w_DESBA2", cQueryName = "DESBA2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(42), bMultilanguage =  .f.,;
    HelpContextID = 7209526,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=340, Top=243, InputMask=replicate('X',42)

  func oDESBA2_1_62.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'P' )
    endwith
  endfunc

  add object oDESPAR_1_65 as StdField with uid="BDMFKRSUFI",rtseq=118,rtrep=.f.,;
    cFormVar = "w_DESPAR", cQueryName = "DESPAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 8127030,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=340, Top=243, InputMask=replicate('X',30)

  func oDESPAR_1_65.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oDESIRPEF_1_69 as StdField with uid="RZTGGJNGMP",rtseq=119,rtrep=.f.,;
    cFormVar = "w_DESIRPEF", cQueryName = "DESIRPEF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 260375164,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=340, Top=243, InputMask=replicate('X',35)

  func oDESIRPEF_1_69.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R'  Or .w_TIPCON='C')
    endwith
  endfunc

  add object oDESTR2_1_72 as StdField with uid="ZKRIRCJNNX",rtseq=120,rtrep=.f.,;
    cFormVar = "w_DESTR2", cQueryName = "DESTR2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 26214966,;
   bGlobalFont=.t.,;
    Height=21, Width=233, Left=340, Top=295, InputMask=replicate('X',35)

  func oDESTR2_1_72.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R'  Or .w_TIPCON='C')
    endwith
  endfunc

  add object oDESMAG_1_78 as StdField with uid="TRZBTUIJDE",rtseq=133,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 91816502,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=340, Top=457, InputMask=replicate('X',30)

  func oDESMAG_1_78.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  add object oANMAGTER_1_79 as StdField with uid="AMZSBWJMYP",rtseq=134,rtrep=.f.,;
    cFormVar = "w_ANMAGTER", cQueryName = "ANMAGTER",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino C/Lavoro non tipo WIP",;
    ToolTipText = "Eventuale codice mag. utilizzato per il conto lavoro e/o per eventuale assegnazione dei documenti",;
    HelpContextID = 46967640,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=199, Top=350, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_ANMAGTER"

  func oANMAGTER_1_79.mHide()
    with this.Parent.oContained
      return (Isalt() Or .w_SEZIONE$'CPRZV')
    endwith
  endfunc

  func oANMAGTER_1_79.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_79('Part',this)
    endwith
    return bRes
  endfunc

  proc oANMAGTER_1_79.ecpDrop(oSource)
    this.Parent.oContained.link_1_79('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANMAGTER_1_79.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oANMAGTER_1_79'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'GSCOWMAG.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oANMAGTER_1_79.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_ANMAGTER
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_80 as StdField with uid="NQXKEZAIBO",rtseq=135,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 91816502,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=340, Top=350, InputMask=replicate('X',30)

  func oDESMAG_1_80.mHide()
    with this.Parent.oContained
      return (Isalt() Or .w_SEZIONE$'CPRZV')
    endwith
  endfunc

  add object oStr_1_3 as StdString with uid="OPZMNBZJGE",Visible=.t., Left=95, Top=31,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="MAJRIEMKEU",Visible=.t., Left=130, Top=83,;
    Alignment=1, Width=65, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="XJEFRRYNMW",Visible=.t., Left=137, Top=123,;
    Alignment=1, Width=58, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="HSWSDVDMII",Visible=.t., Left=74, Top=245,;
    Alignment=1, Width=121, Height=18,;
    Caption="Categoria contabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="IGHSOBKIVW",Visible=.t., Left=50, Top=202,;
    Alignment=1, Width=145, Height=18,;
    Caption="Mastro contabile:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="OYHGIBNUXI",Visible=.t., Left=96, Top=163,;
    Alignment=1, Width=99, Height=18,;
    Caption="Aggiorna dati:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="AVBKNBPWUK",Visible=.t., Left=67, Top=297,;
    Alignment=1, Width=128, Height=18,;
    Caption="Cod. IVA esenz./agev.:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="GCCUOCCYZL",Visible=.t., Left=82, Top=353,;
    Alignment=1, Width=113, Height=18,;
    Caption="Tipo operazione IVA:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="GVMYSYKYSK",Visible=.t., Left=57, Top=459,;
    Alignment=1, Width=138, Height=18,;
    Caption="Codice Tributo I.R.PE.F.:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="RBLORRHYWX",Visible=.t., Left=69, Top=202,;
    Alignment=1, Width=126, Height=18,;
    Caption="Cat.commerciale:"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="EHKLCELNXS",Visible=.t., Left=62, Top=353,;
    Alignment=1, Width=133, Height=18,;
    Caption="Categoria provvigioni:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  add object oStr_1_52 as StdString with uid="ATFNCMUKQP",Visible=.t., Left=69, Top=245,;
    Alignment=1, Width=126, Height=18,;
    Caption="Categoria sconti/mag.:"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZ')
    endwith
  endfunc

  add object oStr_1_57 as StdString with uid="AABGXQEKMR",Visible=.t., Left=69, Top=297,;
    Alignment=1, Width=126, Height=18,;
    Caption="Cod. zona:"  ;
  , bGlobalFont=.t.

  func oStr_1_57.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="IYBNYQFLZW",Visible=.t., Left=69, Top=406,;
    Alignment=1, Width=126, Height=18,;
    Caption="Codice agente:"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="MGNYTKXQCV",Visible=.t., Left=88, Top=202,;
    Alignment=1, Width=107, Height=18,;
    Caption="Cod. pagamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'P' )
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="GANIXFARCK",Visible=.t., Left=88, Top=245,;
    Alignment=1, Width=107, Height=15,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  func oStr_1_60.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'P' )
    endwith
  endfunc

  add object oStr_1_64 as StdString with uid="WUGOBUGLYC",Visible=.t., Left=120, Top=406,;
    Alignment=1, Width=75, Height=18,;
    Caption="IRPEF/IRES:"  ;
  , bGlobalFont=.t.

  func oStr_1_64.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oStr_1_66 as StdString with uid="RBLDDINGKL",Visible=.t., Left=73, Top=245,;
    Alignment=1, Width=122, Height=18,;
    Caption="Pagam.moratoria:"  ;
  , bGlobalFont=.t.

  func oStr_1_66.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oStr_1_67 as StdString with uid="QXBOSGKGHC",Visible=.t., Left=70, Top=202,;
    Alignment=1, Width=125, Height=18,;
    Caption="Gestione contenzioso:"  ;
  , bGlobalFont=.t.

  func oStr_1_67.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oStr_1_68 as StdString with uid="ZDDRGZTETX",Visible=.t., Left=12, Top=297,;
    Alignment=1, Width=183, Height=18,;
    Caption="Escludi appl. interessi di mora:"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oStr_1_70 as StdString with uid="UIPWQWFZJO",Visible=.t., Left=63, Top=245,;
    Alignment=1, Width=132, Height=18,;
    Caption="Codice tributo I.R.PE.F.:"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R'  Or .w_TIPCON='C')
    endwith
  endfunc

  add object oStr_1_71 as StdString with uid="GUBTDRNKFX",Visible=.t., Left=67, Top=297,;
    Alignment=1, Width=128, Height=18,;
    Caption="Contrib. previdenziale:"  ;
  , bGlobalFont=.t.

  func oStr_1_71.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R'  Or .w_TIPCON='C')
    endwith
  endfunc

  add object oStr_1_73 as StdString with uid="ZDVJJHTPKV",Visible=.t., Left=71, Top=202,;
    Alignment=1, Width=124, Height=18,;
    Caption="Ritenute:"  ;
  , bGlobalFont=.t.

  func oStr_1_73.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R'  Or .w_TIPCON='C')
    endwith
  endfunc

  add object oStr_1_76 as StdString with uid="TSMDHAJDQD",Visible=.t., Left=12, Top=297,;
    Alignment=1, Width=183, Height=18,;
    Caption="Escludi appl. interessi di mora:"  ;
  , bGlobalFont=.t.

  func oStr_1_76.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'P'   Or .w_TIPCON='C')
    endwith
  endfunc

  add object oStr_1_77 as StdString with uid="WLLINKOFCH",Visible=.t., Left=70, Top=459,;
    Alignment=1, Width=125, Height=18,;
    Caption="Mag. preferenziale:"  ;
  , bGlobalFont=.t.

  func oStr_1_77.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  add object oStr_1_81 as StdString with uid="PWDEJWOMJM",Visible=.t., Left=54, Top=353,;
    Alignment=1, Width=141, Height=18,;
    Caption="Magazzino C/Lavoro:"  ;
  , bGlobalFont=.t.

  func oStr_1_81.mHide()
    with this.Parent.oContained
      return (Isalt() Or .w_SEZIONE$'CPRZV')
    endwith
  endfunc
enddefine
define class tgsar_kcfPag2 as StdContainer
  Width  = 784
  height = 647
  stdWidth  = 784
  stdheight = 647
  resizeXpos=373
  resizeYpos=150
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomAggC as cp_szoombox with uid="SFIVKVVAUJ",left=3, top=4, width=780,height=304,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,cZoomFile="GSAR_KCF",bOptions=.f.,cTable="CONTI",bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="GSAR_KCF",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "ActivatePage 2,Interroga",;
    nPag=2;
    , HelpContextID = 78811110


  add object ZoomAggPf as cp_szoombox with uid="DNZZGOIFNJ",left=3, top=4, width=780,height=304,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,cZoomFile="GSARPKCFF",bOptions=.f.,cTable="CONTI",bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="GSAR_KCF",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "ActivatePage 2,Interroga",;
    nPag=2;
    , HelpContextID = 78811110


  add object ZoomAggR as cp_szoombox with uid="SDPYIMCVUO",left=3, top=4, width=780,height=304,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,cZoomFile="GSARRKCF",bOptions=.f.,cTable="CONTI",bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="GSAR_KCF",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "ActivatePage 2,Interroga",;
    nPag=2;
    , HelpContextID = 78811110


  add object ZoomAggV as cp_szoombox with uid="MQCXLZMSXV",left=3, top=4, width=780,height=304,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,cZoomFile="GSARVKCF",bOptions=.f.,cTable="CONTI",bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="GSAR_KCF",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "ActivatePage 2,Interroga",;
    nPag=2;
    , HelpContextID = 78811110


  add object ZoomAggF as cp_szoombox with uid="PBYZIEYJQX",left=3, top=4, width=780,height=304,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,cZoomFile="GSARFKCF",bOptions=.f.,cTable="CONTI",bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="GSAR_KCF",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "ActivatePage 2,Interroga",;
    nPag=2;
    , HelpContextID = 78811110


  add object ZoomAggCo as cp_szoombox with uid="IFWCHLGDAJ",left=3, top=4, width=780,height=304,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,cZoomFile="GSARZKCF",bOptions=.f.,cTable="CONTI",bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="GSAR_KCF",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "ActivatePage 2,Interroga",;
    nPag=2;
    , HelpContextID = 78811110


  add object ZoomAggA as cp_szoombox with uid="NPFOEMROSB",left=3, top=4, width=780,height=304,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,cZoomFile="GSARAKCF",bOptions=.f.,cTable="CONTI",bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="GSAR_KCF",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "ActivatePage 2,Interroga",;
    nPag=2;
    , HelpContextID = 78811110


  add object ZoomAggPc as cp_szoombox with uid="HMSHOTWXCB",left=3, top=4, width=780,height=304,;
    caption='Object',;
   bGlobalFont=.t.,;
    bAdvOptions=.f.,cZoomFile="GSARPKCF",bOptions=.f.,cTable="CONTI",bReadOnly=.f.,bQueryOnLoad=.f.,cMenuFile="GSAR_KCF",cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    cEvent = "ActivatePage 2,Interroga",;
    nPag=2;
    , HelpContextID = 78811110


  add object oBtn_2_9 as StdButton with uid="GQMUPTTTCY",left=1, top=313, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per selezionare i conti";
    , HelpContextID = 260194838;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_9.Click()
      with this.Parent.oContained
        GSAR_BKF(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_10 as StdButton with uid="ZCYHLDNLVF",left=49, top=313, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per deselezionare i conti";
    , HelpContextID = 260194838;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_10.Click()
      with this.Parent.oContained
        GSAR_BKF(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_11 as StdButton with uid="GUQBLQMZBT",left=97, top=313, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per invertire la selezione di tutti i conti";
    , HelpContextID = 260194838;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_11.Click()
      with this.Parent.oContained
        GSAR_BKF(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oAGGMASCON_2_12 as StdCheck with uid="LQVMEHOEYX",rtseq=33,rtrep=.f.,left=157, top=389, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 24660277,;
    cFormVar="w_AGGMASCON", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGMASCON_2_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGMASCON_2_12.GetRadio()
    this.Parent.oContained.w_AGGMASCON = this.RadioValue()
    return .t.
  endfunc

  func oAGGMASCON_2_12.SetRadio()
    this.Parent.oContained.w_AGGMASCON=trim(this.Parent.oContained.w_AGGMASCON)
    this.value = ;
      iif(this.Parent.oContained.w_AGGMASCON=='S',1,;
      0)
  endfunc

  func oAGGMASCON_2_12.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  add object oNEWMASCON_2_13 as StdField with uid="BDGCQRNEBL",rtseq=34,rtrep=.f.,;
    cFormVar = "w_NEWMASCON", cQueryName = "NEWMASCON",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggruppamento del cliente",;
    HelpContextID = 24725509,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=236, Top=390, cSayPict="p_MAS", cGetPict="p_MAS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_NEWMASCON"

  func oNEWMASCON_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGMASCON='S')
    endwith
   endif
  endfunc

  func oNEWMASCON_2_13.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='F' Or .w_SEZIONE<>'C')
    endwith
  endfunc

  func oNEWMASCON_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWMASCON_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWMASCON_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oNEWMASCON_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri",'GSAR_ACL.MASTRI_VZM',this.parent.oContained
  endproc
  proc oNEWMASCON_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_NEWMASCON
     i_obj.ecpSave()
  endproc

  add object oNEWMASCON_2_14 as StdField with uid="DHQMBKUVQV",rtseq=35,rtrep=.f.,;
    cFormVar = "w_NEWMASCON", cQueryName = "NEWMASCON",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggruppamento del fornitore",;
    HelpContextID = 24725509,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=236, Top=390, cSayPict="p_MAS", cGetPict="p_MAS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_NEWMASCON"

  func oNEWMASCON_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGMASCON='S')
    endwith
   endif
  endfunc

  func oNEWMASCON_2_14.mHide()
    with this.Parent.oContained
      return (.w_TIPCON='C' Or .w_SEZIONE<>'C')
    endwith
  endfunc

  func oNEWMASCON_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWMASCON_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWMASCON_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oNEWMASCON_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri",'GSAR_AFR.MASTRI_VZM',this.parent.oContained
  endproc
  proc oNEWMASCON_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_NEWMASCON
     i_obj.ecpSave()
  endproc

  add object oAGGCATCON_2_17 as StdCheck with uid="SALXNFINLP",rtseq=38,rtrep=.f.,left=157, top=415, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 40782133,;
    cFormVar="w_AGGCATCON", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGCATCON_2_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGCATCON_2_17.GetRadio()
    this.Parent.oContained.w_AGGCATCON = this.RadioValue()
    return .t.
  endfunc

  func oAGGCATCON_2_17.SetRadio()
    this.Parent.oContained.w_AGGCATCON=trim(this.Parent.oContained.w_AGGCATCON)
    this.value = ;
      iif(this.Parent.oContained.w_AGGCATCON=='S',1,;
      0)
  endfunc

  func oAGGCATCON_2_17.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  add object oNEWCATCON_2_18 as StdField with uid="IFIWKNEPCK",rtseq=39,rtrep=.f.,;
    cFormVar = "w_NEWCATCON", cQueryName = "NEWCATCON",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria contabile inesistente",;
    ToolTipText = "Categoria contabile selezionata",;
    HelpContextID = 40847365,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=236, Top=417, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", oKey_1_1="C2CODICE", oKey_1_2="this.w_NEWCATCON"

  func oNEWCATCON_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGCATCON='S')
    endwith
   endif
  endfunc

  func oNEWCATCON_2_18.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C')
    endwith
  endfunc

  func oNEWCATCON_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWCATCON_2_18.ecpDrop(oSource)
    this.Parent.oContained.link_2_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWCATCON_2_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oNEWCATCON_2_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"CATEGORIE CONTABILI",'GSAR0AC2.CACOCLFO_VZM',this.parent.oContained
  endproc

  add object oAGGANCODIVA_2_19 as StdCheck with uid="CWYUFDOYQW",rtseq=40,rtrep=.f.,left=157, top=447, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37793498,;
    cFormVar="w_AGGANCODIVA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCODIVA_2_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCODIVA_2_19.GetRadio()
    this.Parent.oContained.w_AGGANCODIVA = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCODIVA_2_19.SetRadio()
    this.Parent.oContained.w_AGGANCODIVA=trim(this.Parent.oContained.w_AGGANCODIVA)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCODIVA=='S',1,;
      0)
  endfunc

  func oAGGANCODIVA_2_19.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  add object oNEWANCODIVA_2_20 as StdField with uid="BIFAPCGQQC",rtseq=41,rtrep=.f.,;
    cFormVar = "w_NEWANCODIVA", cQueryName = "NEWANCODIVA",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice iva di esenzione/agevolazione da aggiornare",;
    HelpContextID = 37858730,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=236, Top=449, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_NEWANCODIVA"

  func oNEWANCODIVA_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCODIVA='S')
    endwith
   endif
  endfunc

  func oNEWANCODIVA_2_20.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C')
    endwith
  endfunc

  func oNEWANCODIVA_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWANCODIVA_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWANCODIVA_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oNEWANCODIVA_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'GSVE0MDV.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oNEWANCODIVA_2_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_NEWANCODIVA
     i_obj.ecpSave()
  endproc

  add object oAGGANTIPOPE_2_22 as StdCheck with uid="OVCAFKZPJX",rtseq=43,rtrep=.f.,left=157, top=480, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 213849786,;
    cFormVar="w_AGGANTIPOPE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANTIPOPE_2_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANTIPOPE_2_22.GetRadio()
    this.Parent.oContained.w_AGGANTIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oAGGANTIPOPE_2_22.SetRadio()
    this.Parent.oContained.w_AGGANTIPOPE=trim(this.Parent.oContained.w_AGGANTIPOPE)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANTIPOPE=='S',1,;
      0)
  endfunc

  func oAGGANTIPOPE_2_22.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  add object oNEWANTIPOPE_2_23 as StdField with uid="YVYKNUCKUX",rtseq=44,rtrep=.f.,;
    cFormVar = "w_NEWANTIPOPE", cQueryName = "NEWANTIPOPE",;
    bObbl = .t. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente",;
    ToolTipText = "Tipo operazione IVA da aggiornare",;
    HelpContextID = 213784554,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=236, Top=482, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPCODIV", cZoomOnZoom="GSAR_ATO", oKey_1_1="TI__TIPO", oKey_1_2="this.w_ANCATOPE", oKey_2_1="TICODICE", oKey_2_2="this.w_NEWANTIPOPE"

  func oNEWANTIPOPE_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANTIPOPE='S')
    endwith
   endif
  endfunc

  func oNEWANTIPOPE_2_23.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  func oNEWANTIPOPE_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWANTIPOPE_2_23.ecpDrop(oSource)
    this.Parent.oContained.link_2_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWANTIPOPE_2_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIPCODIV_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_ANCATOPE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStr(this.Parent.oContained.w_ANCATOPE)
    endif
    do cp_zoom with 'TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(this.parent,'oNEWANTIPOPE_2_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATO',"Operazioni IVA",'GSVE_ZTI.TIPCODIV_VZM',this.parent.oContained
  endproc
  proc oNEWANTIPOPE_2_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TI__TIPO=w_ANCATOPE
     i_obj.w_TICODICE=this.parent.oContained.w_NEWANTIPOPE
     i_obj.ecpSave()
  endproc


  add object oAGGANPARTSN_2_25 as StdCombo with uid="ELXPNCIQHX",rtseq=46,rtrep=.f.,left=157,top=516,width=140,height=21;
    , ToolTipText = "Aggiornamento gestione partite";
    , HelpContextID = 255949976;
    , cFormVar="w_AGGANPARTSN",RowSource=""+"Nessun aggiornamento,"+"Si", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAGGANPARTSN_2_25.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oAGGANPARTSN_2_25.GetRadio()
    this.Parent.oContained.w_AGGANPARTSN = this.RadioValue()
    return .t.
  endfunc

  func oAGGANPARTSN_2_25.SetRadio()
    this.Parent.oContained.w_AGGANPARTSN=trim(this.Parent.oContained.w_AGGANPARTSN)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANPARTSN=='X',1,;
      iif(this.Parent.oContained.w_AGGANPARTSN=='S',2,;
      0))
  endfunc

  func oAGGANPARTSN_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERPAR='S')
    endwith
   endif
  endfunc

  func oAGGANPARTSN_2_25.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc


  add object oAGGANFLRITE_2_26 as StdCombo with uid="IKDHNLQWFT",rtseq=47,rtrep=.f.,left=522,top=387,width=140,height=23;
    , ToolTipText = "Aggiornamento dati ritenute subite";
    , HelpContextID = 180294424;
    , cFormVar="w_AGGANFLRITE",RowSource=""+"Nessun aggiornamento,"+"Attivo,"+"Disattivo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAGGANFLRITE_2_26.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oAGGANFLRITE_2_26.GetRadio()
    this.Parent.oContained.w_AGGANFLRITE = this.RadioValue()
    return .t.
  endfunc

  func oAGGANFLRITE_2_26.SetRadio()
    this.Parent.oContained.w_AGGANFLRITE=trim(this.Parent.oContained.w_AGGANFLRITE)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANFLRITE=='X',1,;
      iif(this.Parent.oContained.w_AGGANFLRITE=='S',2,;
      iif(this.Parent.oContained.w_AGGANFLRITE=='N',3,;
      0)))
  endfunc

  func oAGGANFLRITE_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_RITE='S' OR IsAlt())
    endwith
   endif
  endfunc

  func oAGGANFLRITE_2_26.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oAGGANCODIRP_2_27 as StdCheck with uid="DTPHQFTPTC",rtseq=48,rtrep=.f.,left=522, top=415, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37853914,;
    cFormVar="w_AGGANCODIRP", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCODIRP_2_27.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCODIRP_2_27.GetRadio()
    this.Parent.oContained.w_AGGANCODIRP = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCODIRP_2_27.SetRadio()
    this.Parent.oContained.w_AGGANCODIRP=trim(this.Parent.oContained.w_AGGANCODIRP)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCODIRP=='S',1,;
      0)
  endfunc

  func oAGGANCODIRP_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_RITE='S' OR IsAlt()) And .w_AGGANFLRITE<>'N')
    endwith
   endif
  endfunc

  func oAGGANCODIRP_2_27.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oNEWANCODIRP_2_28 as StdField with uid="RFYKWBZFXX",rtseq=49,rtrep=.f.,;
    cFormVar = "w_NEWANCODIRP", cQueryName = "NEWANCODIRP",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un codice tributo I.R.PE.F.",;
    ToolTipText = "Codice tributo da aggiornare",;
    HelpContextID = 37919146,;
   bGlobalFont=.t.,;
    Height=21, Width=67, Left=607, Top=417, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_NEWANCODIRP"

  func oNEWANCODIRP_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCODIRP='S')
    endwith
   endif
  endfunc

  func oNEWANCODIRP_2_28.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C'  Or .w_TIPCON='F')
    endwith
  endfunc

  func oNEWANCODIRP_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWANCODIRP_2_28.ecpDrop(oSource)
    this.Parent.oContained.link_2_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWANCODIRP_2_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oNEWANCODIRP_2_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oNEWANCODIRP_2_28.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_NEWANCODIRP
     i_obj.ecpSave()
  endproc

  add object oAGGANCAURIT_2_29 as StdCheck with uid="WYPVNZGRZT",rtseq=50,rtrep=.f.,left=522, top=447, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37868155,;
    cFormVar="w_AGGANCAURIT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCAURIT_2_29.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCAURIT_2_29.GetRadio()
    this.Parent.oContained.w_AGGANCAURIT = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCAURIT_2_29.SetRadio()
    this.Parent.oContained.w_AGGANCAURIT=trim(this.Parent.oContained.w_AGGANCAURIT)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCAURIT=='S',1,;
      0)
  endfunc

  func oAGGANCAURIT_2_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_RITE='S' OR IsAlt()) And .w_AGGANFLRITE<>'N')
    endwith
   endif
  endfunc

  func oAGGANCAURIT_2_29.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C'  Or .w_TIPCON='F')
    endwith
  endfunc


  add object oNEWANCAURIT_2_30 as StdCombo with uid="WSDCCQWIEQ",rtseq=51,rtrep=.f.,left=607,top=450,width=76,height=21;
    , ToolTipText = "Valore causale prestazione da aggiornare";
    , HelpContextID = 37933387;
    , cFormVar="w_NEWANCAURIT",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo F,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo J,"+"Tipo K,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo Z,"+"Tipo ZO", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oNEWANCAURIT_2_30.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'I',;
    iif(this.value =10,'J',;
    iif(this.value =11,'K',;
    iif(this.value =12,'L',;
    iif(this.value =13,'L1',;
    iif(this.value =14,'M',;
    iif(this.value =15,'M1',;
    iif(this.value =16,'M2',;
    iif(this.value =17,'N',;
    iif(this.value =18,'O',;
    iif(this.value =19,'O1',;
    iif(this.value =20,'P',;
    iif(this.value =21,'Q',;
    iif(this.value =22,'R',;
    iif(this.value =23,'S',;
    iif(this.value =24,'T',;
    iif(this.value =25,'U',;
    iif(this.value =26,'V',;
    iif(this.value =27,'V1',;
    iif(this.value =28,'V2',;
    iif(this.value =29,'W',;
    iif(this.value =30,'X',;
    iif(this.value =31,'Y',;
    iif(this.value =32,'Z',;
    iif(this.value =33,'ZO',;
    space(1)))))))))))))))))))))))))))))))))))
  endfunc
  func oNEWANCAURIT_2_30.GetRadio()
    this.Parent.oContained.w_NEWANCAURIT = this.RadioValue()
    return .t.
  endfunc

  func oNEWANCAURIT_2_30.SetRadio()
    this.Parent.oContained.w_NEWANCAURIT=trim(this.Parent.oContained.w_NEWANCAURIT)
    this.value = ;
      iif(this.Parent.oContained.w_NEWANCAURIT=='A',1,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='B',2,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='C',3,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='D',4,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='E',5,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='F',6,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='G',7,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='H',8,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='I',9,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='J',10,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='K',11,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='L',12,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='L1',13,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='M',14,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='M1',15,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='M2',16,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='N',17,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='O',18,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='O1',19,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='P',20,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='Q',21,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='R',22,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='S',23,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='T',24,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='U',25,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='V',26,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='V1',27,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='V2',28,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='W',29,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='X',30,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='Y',31,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='Z',32,;
      iif(this.Parent.oContained.w_NEWANCAURIT=='ZO',33,;
      0)))))))))))))))))))))))))))))))))
  endfunc

  func oNEWANCAURIT_2_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCAURIT='S')
    endwith
   endif
  endfunc

  func oNEWANCAURIT_2_30.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C'  Or .w_TIPCON='F')
    endwith
  endfunc


  add object oAGGANFLFIDO_2_31 as StdCombo with uid="ILCAMOAQMR",rtseq=52,rtrep=.f.,left=522,top=481,width=140,height=22;
    , ToolTipText = "Aggiornamento dati controllo fido cliente";
    , HelpContextID = 180257572;
    , cFormVar="w_AGGANFLFIDO",RowSource=""+"Nessun aggiornamento,"+"Si", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAGGANFLFIDO_2_31.RadioValue()
    return(iif(this.value =1,'X',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oAGGANFLFIDO_2_31.GetRadio()
    this.Parent.oContained.w_AGGANFLFIDO = this.RadioValue()
    return .t.
  endfunc

  func oAGGANFLFIDO_2_31.SetRadio()
    this.Parent.oContained.w_AGGANFLFIDO=trim(this.Parent.oContained.w_AGGANFLFIDO)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANFLFIDO=='X',1,;
      iif(this.Parent.oContained.w_AGGANFLFIDO=='S',2,;
      0))
  endfunc

  func oAGGANFLFIDO_2_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((NOT IsAlt() AND g_PERFID='S') OR (IsAlt() AND g_COGE='S' AND g_PERFID='S'))
    endwith
   endif
  endfunc

  func oAGGANFLFIDO_2_31.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oAGGANCATCOM_2_32 as StdCheck with uid="KHATMFLWYO",rtseq=53,rtrep=.f.,left=157, top=389, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37840778,;
    cFormVar="w_AGGANCATCOM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCATCOM_2_32.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCATCOM_2_32.GetRadio()
    this.Parent.oContained.w_AGGANCATCOM = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCATCOM_2_32.SetRadio()
    this.Parent.oContained.w_AGGANCATCOM=trim(this.Parent.oContained.w_AGGANCATCOM)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCATCOM=='S',1,;
      0)
  endfunc

  func oAGGANCATCOM_2_32.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oNEWANCATCOM_2_33 as StdField with uid="SPACJGVKLW",rtseq=54,rtrep=.f.,;
    cFormVar = "w_NEWANCATCOM", cQueryName = "NEWANCATCOM",;
    bObbl = .t. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale associata al cliente",;
    HelpContextID = 37906010,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=236, Top=390, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_NEWANCATCOM"

  func oNEWANCATCOM_2_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCATCOM='S')
    endwith
   endif
  endfunc

  func oNEWANCATCOM_2_33.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  func oNEWANCATCOM_2_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWANCATCOM_2_33.ecpDrop(oSource)
    this.Parent.oContained.link_2_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWANCATCOM_2_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oNEWANCATCOM_2_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oNEWANCATCOM_2_33.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_NEWANCATCOM
     i_obj.ecpSave()
  endproc

  add object oAGGANCATSCM_2_34 as StdCheck with uid="COHBXWPJKV",rtseq=55,rtrep=.f.,left=157, top=415, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37837962,;
    cFormVar="w_AGGANCATSCM", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCATSCM_2_34.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCATSCM_2_34.GetRadio()
    this.Parent.oContained.w_AGGANCATSCM = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCATSCM_2_34.SetRadio()
    this.Parent.oContained.w_AGGANCATSCM=trim(this.Parent.oContained.w_AGGANCATSCM)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCATSCM=='S',1,;
      0)
  endfunc

  func oAGGANCATSCM_2_34.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oNEWANCATSCM_2_35 as StdField with uid="OFGGTAKOVD",rtseq=56,rtrep=.f.,;
    cFormVar = "w_NEWANCATSCM", cQueryName = "NEWANCATSCM",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria sconti / maggiorazioni inesistente",;
    ToolTipText = "Codice della categoria sconti / maggiorazioni associata al cliente",;
    HelpContextID = 37903194,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=236, Top=417, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_SCMA", cZoomOnZoom="GSAR_ASM", oKey_1_1="CSCODICE", oKey_1_2="this.w_NEWANCATSCM"

  func oNEWANCATSCM_2_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCATSCM='S')
    endwith
   endif
  endfunc

  func oNEWANCATSCM_2_35.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZ')
    endwith
  endfunc

  func oNEWANCATSCM_2_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWANCATSCM_2_35.ecpDrop(oSource)
    this.Parent.oContained.link_2_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWANCATSCM_2_35.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_SCMA','*','CSCODICE',cp_AbsName(this.parent,'oNEWANCATSCM_2_35'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASM',"Categorie sconti/maggiorazioni",'GSAR_ACL.CAT_SCMA_VZM',this.parent.oContained
  endproc
  proc oNEWANCATSCM_2_35.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCODICE=this.parent.oContained.w_NEWANCATSCM
     i_obj.ecpSave()
  endproc

  add object oAGGANCODZON_2_36 as StdCheck with uid="HAGJCEUQXN",rtseq=57,rtrep=.f.,left=157, top=447, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37845226,;
    cFormVar="w_AGGANCODZON", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCODZON_2_36.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCODZON_2_36.GetRadio()
    this.Parent.oContained.w_AGGANCODZON = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCODZON_2_36.SetRadio()
    this.Parent.oContained.w_AGGANCODZON=trim(this.Parent.oContained.w_AGGANCODZON)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCODZON=='S',1,;
      0)
  endfunc

  func oAGGANCODZON_2_36.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oNEWANCODZON_2_37 as StdField with uid="HDYFZBEUQG",rtseq=58,rtrep=.f.,;
    cFormVar = "w_NEWANCODZON", cQueryName = "NEWANCODZON",;
    bObbl = .t. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente oppure obsoleto",;
    ToolTipText = "Codice della zona di appartenenza",;
    HelpContextID = 37910458,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=236, Top=449, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_NEWANCODZON"

  func oNEWANCODZON_2_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCODZON='S')
    endwith
   endif
  endfunc

  func oNEWANCODZON_2_37.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  func oNEWANCODZON_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWANCODZON_2_37.ecpDrop(oSource)
    this.Parent.oContained.link_2_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWANCODZON_2_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oNEWANCODZON_2_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oNEWANCODZON_2_37.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_NEWANCODZON
     i_obj.ecpSave()
  endproc

  add object oAGGANGRUPRO_2_38 as StdCheck with uid="TUBLDPAOQF",rtseq=59,rtrep=.f.,left=157, top=480, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 104958811,;
    cFormVar="w_AGGANGRUPRO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANGRUPRO_2_38.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANGRUPRO_2_38.GetRadio()
    this.Parent.oContained.w_AGGANGRUPRO = this.RadioValue()
    return .t.
  endfunc

  func oAGGANGRUPRO_2_38.SetRadio()
    this.Parent.oContained.w_AGGANGRUPRO=trim(this.Parent.oContained.w_AGGANGRUPRO)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANGRUPRO=='S',1,;
      0)
  endfunc

  func oAGGANGRUPRO_2_38.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  add object oNEWANGRUPRO_2_39 as StdField with uid="PVEDIZRMAT",rtseq=60,rtrep=.f.,;
    cFormVar = "w_NEWANGRUPRO", cQueryName = "NEWANGRUPRO",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria provvigioni inesistente o non di tipo cliente",;
    ToolTipText = "Codice della categoria provvigioni associata al cliente",;
    HelpContextID = 105024043,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=236, Top=482, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUPRO", cZoomOnZoom="GSAR_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_NEWANGRUPRO"

  func oNEWANGRUPRO_2_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANGRUPRO='S')
    endwith
   endif
  endfunc

  func oNEWANGRUPRO_2_39.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  func oNEWANGRUPRO_2_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWANGRUPRO_2_39.ecpDrop(oSource)
    this.Parent.oContained.link_2_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWANGRUPRO_2_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPRO','*','GPCODICE',cp_AbsName(this.parent,'oNEWANGRUPRO_2_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGP',"Categorie provvigioni",'GSAR_ACL.GRUPRO_VZM',this.parent.oContained
  endproc
  proc oNEWANGRUPRO_2_39.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_NEWANGRUPRO
     i_obj.ecpSave()
  endproc

  add object oAGGANCODAG1_2_41 as StdCheck with uid="PLUAZJQJCJ",rtseq=62,rtrep=.f.,left=157, top=515, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37723994,;
    cFormVar="w_AGGANCODAG1", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCODAG1_2_41.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCODAG1_2_41.GetRadio()
    this.Parent.oContained.w_AGGANCODAG1 = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCODAG1_2_41.SetRadio()
    this.Parent.oContained.w_AGGANCODAG1=trim(this.Parent.oContained.w_AGGANCODAG1)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCODAG1=='S',1,;
      0)
  endfunc

  func oAGGANCODAG1_2_41.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  add object oNEWANCODAG1_2_42 as StdField with uid="MVUHBMROWA",rtseq=63,rtrep=.f.,;
    cFormVar = "w_NEWANCODAG1", cQueryName = "NEWANCODAG1",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice agente che cura i rapporti con il cliente",;
    HelpContextID = 37789226,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=236, Top=516, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_NEWANCODAG1"

  func oNEWANCODAG1_2_42.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCODAG1='S')
    endwith
   endif
  endfunc

  func oNEWANCODAG1_2_42.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  func oNEWANCODAG1_2_42.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_42('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWANCODAG1_2_42.ecpDrop(oSource)
    this.Parent.oContained.link_2_42('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWANCODAG1_2_42.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oNEWANCODAG1_2_42'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oNEWANCODAG1_2_42.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_NEWANCODAG1
     i_obj.ecpSave()
  endproc

  add object oAGGANMAGTER_2_43 as StdCheck with uid="FAJGJQOXAL",rtseq=64,rtrep=.f.,left=522, top=389, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 205631117,;
    cFormVar="w_AGGANMAGTER", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANMAGTER_2_43.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANMAGTER_2_43.GetRadio()
    this.Parent.oContained.w_AGGANMAGTER = this.RadioValue()
    return .t.
  endfunc

  func oAGGANMAGTER_2_43.SetRadio()
    this.Parent.oContained.w_AGGANMAGTER=trim(this.Parent.oContained.w_AGGANMAGTER)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANMAGTER=='S',1,;
      0)
  endfunc

  func oAGGANMAGTER_2_43.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZ')
    endwith
  endfunc

  add object oNEWANMAGTER_2_44 as StdField with uid="NIWSZSBHAF",rtseq=65,rtrep=.f.,;
    cFormVar = "w_NEWANMAGTER", cQueryName = "NEWANMAGTER",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente",;
    ToolTipText = "Codice magazzino utilizzato per l'eventuale assegnazione dei documenti",;
    HelpContextID = 205696349,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=607, Top=388, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_NEWANMAGTER"

  func oNEWANMAGTER_2_44.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANMAGTER='S')
    endwith
   endif
  endfunc

  func oNEWANMAGTER_2_44.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  func oNEWANMAGTER_2_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_44('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWANMAGTER_2_44.ecpDrop(oSource)
    this.Parent.oContained.link_2_44('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWANMAGTER_2_44.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oNEWANMAGTER_2_44'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oNEWANMAGTER_2_44.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_NEWANMAGTER
     i_obj.ecpSave()
  endproc

  add object oNEWANMAGTER_2_45 as StdField with uid="ICKHWYMXXJ",rtseq=66,rtrep=.f.,;
    cFormVar = "w_NEWANMAGTER", cQueryName = "NEWANMAGTER",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino C/Lavoro inesistente o non tipo WIP",;
    ToolTipText = "Eventuale codice mag. utilizzato per il conto lavoro e/o per eventuale assegnazione dei documenti",;
    HelpContextID = 205696349,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=607, Top=388, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_NEWANMAGTER"

  func oNEWANMAGTER_2_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANMAGTER='S')
    endwith
   endif
  endfunc

  func oNEWANMAGTER_2_45.mHide()
    with this.Parent.oContained
      return (Isalt() Or .w_SEZIONE$'CPRZV')
    endwith
  endfunc

  func oNEWANMAGTER_2_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWANMAGTER_2_45.ecpDrop(oSource)
    this.Parent.oContained.link_2_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWANMAGTER_2_45.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oNEWANMAGTER_2_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'GSCOWMAG.MAGAZZIN_VZM',this.parent.oContained
  endproc
  proc oNEWANMAGTER_2_45.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_NEWANMAGTER
     i_obj.ecpSave()
  endproc

  add object oAGGANCODPAG_2_46 as StdCheck with uid="LQPWOHJZJN",rtseq=67,rtrep=.f.,left=157, top=389, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37812810,;
    cFormVar="w_AGGANCODPAG", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCODPAG_2_46.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCODPAG_2_46.GetRadio()
    this.Parent.oContained.w_AGGANCODPAG = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCODPAG_2_46.SetRadio()
    this.Parent.oContained.w_AGGANCODPAG=trim(this.Parent.oContained.w_AGGANCODPAG)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCODPAG=='S',1,;
      0)
  endfunc

  func oAGGANCODPAG_2_46.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CRAVZ' )
    endwith
  endfunc

  add object oNEWANCODPAG_2_47 as StdField with uid="WCRONHUGSA",rtseq=68,rtrep=.f.,;
    cFormVar = "w_NEWANCODPAG", cQueryName = "NEWANCODPAG",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente",;
    ToolTipText = "Codice del pagamento da aggiornare",;
    HelpContextID = 37878042,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=236, Top=390, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_NEWANCODPAG"

  func oNEWANCODPAG_2_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCODPAG='S')
    endwith
   endif
  endfunc

  func oNEWANCODPAG_2_47.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CRAVZ' )
    endwith
  endfunc

  func oNEWANCODPAG_2_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_47('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWANCODPAG_2_47.ecpDrop(oSource)
    this.Parent.oContained.link_2_47('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWANCODPAG_2_47.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oNEWANCODPAG_2_47'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oNEWANCODPAG_2_47.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_NEWANCODPAG
     i_obj.ecpSave()
  endproc

  add object oAGGANCODBA2_2_48 as StdCheck with uid="FDGPCTYRFJ",rtseq=69,rtrep=.f.,left=157, top=415, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37726570,;
    cFormVar="w_AGGANCODBA2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCODBA2_2_48.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCODBA2_2_48.GetRadio()
    this.Parent.oContained.w_AGGANCODBA2 = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCODBA2_2_48.SetRadio()
    this.Parent.oContained.w_AGGANCODBA2=trim(this.Parent.oContained.w_AGGANCODBA2)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCODBA2=='S',1,;
      0)
  endfunc

  func oAGGANCODBA2_2_48.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CRAVZ' )
    endwith
  endfunc

  add object oNEWANCODBA2_2_49 as StdField with uid="ZNRLPBFFTO",rtseq=70,rtrep=.f.,;
    cFormVar = "w_NEWANCODBA2", cQueryName = "NEWANCODBA2",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente  o di tipo salvo buon fine",;
    ToolTipText = "Codice del conto corrente della nostra banca comunicato al cliente/fornitore da aggiornare",;
    HelpContextID = 37791802,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=236, Top=417, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_NEWANCODBA2"

  func oNEWANCODBA2_2_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCODBA2='S')
    endwith
   endif
  endfunc

  func oNEWANCODBA2_2_49.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CRAVZ' )
    endwith
  endfunc

  func oNEWANCODBA2_2_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_49('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWANCODBA2_2_49.ecpDrop(oSource)
    this.Parent.oContained.link_2_49('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWANCODBA2_2_49.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oNEWANCODBA2_2_49'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Elenco conti banche",'GSAR_ACL.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oNEWANCODBA2_2_49.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_NEWANCODBA2
     i_obj.ecpSave()
  endproc


  add object oAGGANGESCON_2_50 as StdCombo with uid="MUAZZQDKAB",rtseq=71,rtrep=.f.,left=157,top=389,width=140,height=21;
    , ToolTipText = "Filtro su tipologia di gestione del contenzioso";
    , HelpContextID = 104953737;
    , cFormVar="w_AGGANGESCON",RowSource=""+"Nessun blocco,"+"Moratoria attivabile,"+"Blocco attivabile,"+"Nessun aggiornamento", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAGGANGESCON_2_50.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'M',;
    iif(this.value =3,'B',;
    iif(this.value =4,'X',;
    space(1))))))
  endfunc
  func oAGGANGESCON_2_50.GetRadio()
    this.Parent.oContained.w_AGGANGESCON = this.RadioValue()
    return .t.
  endfunc

  func oAGGANGESCON_2_50.SetRadio()
    this.Parent.oContained.w_AGGANGESCON=trim(this.Parent.oContained.w_AGGANGESCON)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANGESCON=='N',1,;
      iif(this.Parent.oContained.w_AGGANGESCON=='M',2,;
      iif(this.Parent.oContained.w_AGGANGESCON=='B',3,;
      iif(this.Parent.oContained.w_AGGANGESCON=='X',4,;
      0))))
  endfunc

  func oAGGANGESCON_2_50.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc


  add object oAGGANFLGCON_2_51 as StdCombo with uid="ULESGBHXSY",value=1,rtseq=72,rtrep=.f.,left=157,top=415,width=140,height=21;
    , ToolTipText = "Aggiornamento stato del blocco per contenzioso";
    , HelpContextID = 180258947;
    , cFormVar="w_AGGANFLGCON",RowSource=""+"No,"+"Si,"+"Nessun aggiornamento", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAGGANFLGCON_2_51.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'S',;
    iif(this.value =3,'X',;
    space(1)))))
  endfunc
  func oAGGANFLGCON_2_51.GetRadio()
    this.Parent.oContained.w_AGGANFLGCON = this.RadioValue()
    return .t.
  endfunc

  func oAGGANFLGCON_2_51.SetRadio()
    this.Parent.oContained.w_AGGANFLGCON=trim(this.Parent.oContained.w_AGGANFLGCON)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANFLGCON=='',1,;
      iif(this.Parent.oContained.w_AGGANFLGCON=='S',2,;
      iif(this.Parent.oContained.w_AGGANFLGCON=='X',3,;
      0)))
  endfunc

  func oAGGANFLGCON_2_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANGESCON $ 'XB')
    endwith
   endif
  endfunc

  func oAGGANFLGCON_2_51.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oAGGANPAGPAR_2_52 as StdCheck with uid="EYZXDDNCNS",rtseq=73,rtrep=.f.,left=157, top=447, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 255961677,;
    cFormVar="w_AGGANPAGPAR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANPAGPAR_2_52.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANPAGPAR_2_52.GetRadio()
    this.Parent.oContained.w_AGGANPAGPAR = this.RadioValue()
    return .t.
  endfunc

  func oAGGANPAGPAR_2_52.SetRadio()
    this.Parent.oContained.w_AGGANPAGPAR=trim(this.Parent.oContained.w_AGGANPAGPAR)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANPAGPAR=='S',1,;
      0)
  endfunc

  func oAGGANPAGPAR_2_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANGESCON $ 'MX')
    endwith
   endif
  endfunc

  func oAGGANPAGPAR_2_52.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oNEWANPAGPAR_2_53 as StdField with uid="KDSGPSKHQS",rtseq=74,rtrep=.f.,;
    cFormVar = "w_NEWANPAGPAR", cQueryName = "NEWANPAGPAR",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente",;
    ToolTipText = "Codice pagamento praticato al cliente in particolari situazioni (es.moratoria fido ecc.)",;
    HelpContextID = 256026909,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=236, Top=449, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_NEWANPAGPAR"

  func oNEWANPAGPAR_2_53.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANPAGPAR='S')
    endwith
   endif
  endfunc

  func oNEWANPAGPAR_2_53.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc

  func oNEWANPAGPAR_2_53.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_53('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWANPAGPAR_2_53.ecpDrop(oSource)
    this.Parent.oContained.link_2_53('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWANPAGPAR_2_53.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oNEWANPAGPAR_2_53'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oNEWANPAGPAR_2_53.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_NEWANPAGPAR
     i_obj.ecpSave()
  endproc

  add object oAGGANDATMOR_2_54 as StdCheck with uid="FXSMKXWTPU",rtseq=75,rtrep=.f.,left=157, top=480, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 54638634,;
    cFormVar="w_AGGANDATMOR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANDATMOR_2_54.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANDATMOR_2_54.GetRadio()
    this.Parent.oContained.w_AGGANDATMOR = this.RadioValue()
    return .t.
  endfunc

  func oAGGANDATMOR_2_54.SetRadio()
    this.Parent.oContained.w_AGGANDATMOR=trim(this.Parent.oContained.w_AGGANDATMOR)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANDATMOR=='S',1,;
      0)
  endfunc

  func oAGGANDATMOR_2_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANGESCON $ 'MX')
    endwith
   endif
  endfunc

  func oAGGANDATMOR_2_54.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oNEWANDATMOR_2_55 as StdField with uid="SLXVFLEVIJ",rtseq=76,rtrep=.f.,;
    cFormVar = "w_NEWANDATMOR", cQueryName = "NEWANDATMOR",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data limite di applicazione della condizione di pagamento particolare",;
    HelpContextID = 54703866,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=236, Top=482

  func oNEWANDATMOR_2_55.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANDATMOR='S')
    endwith
   endif
  endfunc

  func oNEWANDATMOR_2_55.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc


  add object oAGGANFLESIM_2_56 as StdCombo with uid="YYHLZHJPAU",value=2,rtseq=77,rtrep=.f.,left=522,top=387,width=140,height=23;
    , ToolTipText = "Filtro sull'applicazione degli interessi di mora al cliente/fornitore";
    , HelpContextID = 180264325;
    , cFormVar="w_AGGANFLESIM",RowSource=""+"Si,"+"No,"+"Nessun aggiornamento", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAGGANFLESIM_2_56.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,' ',;
    iif(this.value =3,'X',;
    space(1)))))
  endfunc
  func oAGGANFLESIM_2_56.GetRadio()
    this.Parent.oContained.w_AGGANFLESIM = this.RadioValue()
    return .t.
  endfunc

  func oAGGANFLESIM_2_56.SetRadio()
    this.Parent.oContained.w_AGGANFLESIM=trim(this.Parent.oContained.w_AGGANFLESIM)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANFLESIM=='S',1,;
      iif(this.Parent.oContained.w_AGGANFLESIM=='',2,;
      iif(this.Parent.oContained.w_AGGANFLESIM=='X',3,;
      0)))
  endfunc

  func oAGGANFLESIM_2_56.mHide()
    with this.Parent.oContained
      return (!(.w_SEZIONE='Z' AND .w_TIPCON='C') AND !(.w_SEZIONE='P' AND .w_TIPCON='F'))
    endwith
  endfunc

  add object oAGGANSAGINT_2_57 as StdCheck with uid="WJBDZXPKWU",rtseq=78,rtrep=.f.,left=522, top=415, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37869277,;
    cFormVar="w_AGGANSAGINT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANSAGINT_2_57.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANSAGINT_2_57.GetRadio()
    this.Parent.oContained.w_AGGANSAGINT = this.RadioValue()
    return .t.
  endfunc

  func oAGGANSAGINT_2_57.SetRadio()
    this.Parent.oContained.w_AGGANSAGINT=trim(this.Parent.oContained.w_AGGANSAGINT)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANSAGINT=='S',1,;
      0)
  endfunc

  func oAGGANSAGINT_2_57.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANFLESIM <> 'S')
    endwith
   endif
  endfunc

  func oAGGANSAGINT_2_57.mHide()
    with this.Parent.oContained
      return (!(.w_SEZIONE='Z' AND .w_TIPCON='C') AND !(.w_SEZIONE='P' AND .w_TIPCON='F'))
    endwith
  endfunc

  add object oNEWANSAGINT_2_58 as StdField with uid="HVHEIXSTIQ",rtseq=79,rtrep=.f.,;
    cFormVar = "w_NEWANSAGINT", cQueryName = "NEWANSAGINT",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saggio di interesse",;
    HelpContextID = 37934509,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=607, Top=417, cSayPict='"999.99"', cGetPict='"999.99"'

  func oNEWANSAGINT_2_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANSAGINT='S')
    endwith
   endif
  endfunc

  func oNEWANSAGINT_2_58.mHide()
    with this.Parent.oContained
      return (!(.w_SEZIONE='Z' AND .w_TIPCON='C') AND !(.w_SEZIONE='P' AND .w_TIPCON='F'))
    endwith
  endfunc


  add object oAGGANSPRINT_2_59 as StdCombo with uid="FORHTYHRYG",rtseq=80,rtrep=.f.,left=522,top=449,width=140,height=22;
    , ToolTipText = "Aggiornamento spread su interesse di mora";
    , HelpContextID = 37869288;
    , cFormVar="w_AGGANSPRINT",RowSource=""+"Si,"+"No,"+"Nessun aggiornamento", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAGGANSPRINT_2_59.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'X',;
    space(1)))))
  endfunc
  func oAGGANSPRINT_2_59.GetRadio()
    this.Parent.oContained.w_AGGANSPRINT = this.RadioValue()
    return .t.
  endfunc

  func oAGGANSPRINT_2_59.SetRadio()
    this.Parent.oContained.w_AGGANSPRINT=trim(this.Parent.oContained.w_AGGANSPRINT)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANSPRINT=='S',1,;
      iif(this.Parent.oContained.w_AGGANSPRINT=='N',2,;
      iif(this.Parent.oContained.w_AGGANSPRINT=='X',3,;
      0)))
  endfunc

  func oAGGANSPRINT_2_59.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANFLESIM <> 'S')
    endwith
   endif
  endfunc

  func oAGGANSPRINT_2_59.mHide()
    with this.Parent.oContained
      return (!(.w_SEZIONE='Z' AND .w_TIPCON='C') AND !(.w_SEZIONE='P' AND .w_TIPCON='F'))
    endwith
  endfunc


  add object oAGGANRITENU_2_60 as StdCombo with uid="DYKMFVBCQV",rtseq=81,rtrep=.f.,left=157,top=389,width=140,height=21;
    , ToolTipText = "Filtro su gestione dati percipiente";
    , HelpContextID = 247339350;
    , cFormVar="w_AGGANRITENU",RowSource=""+"Non soggetto,"+"Solo IRPEF,"+"IRPEF e contr. prev.,"+"Nessun aggiornamento", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oAGGANRITENU_2_60.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    iif(this.value =4,'X',;
    space(1))))))
  endfunc
  func oAGGANRITENU_2_60.GetRadio()
    this.Parent.oContained.w_AGGANRITENU = this.RadioValue()
    return .t.
  endfunc

  func oAGGANRITENU_2_60.SetRadio()
    this.Parent.oContained.w_AGGANRITENU=trim(this.Parent.oContained.w_AGGANRITENU)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANRITENU=='N',1,;
      iif(this.Parent.oContained.w_AGGANRITENU=='S',2,;
      iif(this.Parent.oContained.w_AGGANRITENU=='C',3,;
      iif(this.Parent.oContained.w_AGGANRITENU=='X',4,;
      0))))
  endfunc

  func oAGGANRITENU_2_60.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oAGGANCODIRPF_2_61 as StdCheck with uid="FFJKNDKWSP",rtseq=82,rtrep=.f.,left=157, top=415, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 42441434,;
    cFormVar="w_AGGANCODIRPF", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCODIRPF_2_61.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCODIRPF_2_61.GetRadio()
    this.Parent.oContained.w_AGGANCODIRPF = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCODIRPF_2_61.SetRadio()
    this.Parent.oContained.w_AGGANCODIRPF=trim(this.Parent.oContained.w_AGGANCODIRPF)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCODIRPF=='S',1,;
      0)
  endfunc

  func oAGGANCODIRPF_2_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANRITENU $ 'CSX')
    endwith
   endif
  endfunc

  func oAGGANCODIRPF_2_61.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oNEWANCODIRPF_2_62 as StdField with uid="IDPIVCZRMR",rtseq=83,rtrep=.f.,;
    cFormVar = "w_NEWANCODIRPF", cQueryName = "NEWANCODIRPF",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un codice tributo I.R.PE.F.",;
    ToolTipText = "Codice tributo da aggiornare",;
    HelpContextID = 42506666,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=236, Top=417, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_NEWANCODIRPF"

  func oNEWANCODIRPF_2_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCODIRPF='S')
    endwith
   endif
  endfunc

  func oNEWANCODIRPF_2_62.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  func oNEWANCODIRPF_2_62.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_62('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWANCODIRPF_2_62.ecpDrop(oSource)
    this.Parent.oContained.link_2_62('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWANCODIRPF_2_62.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oNEWANCODIRPF_2_62'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oNEWANCODIRPF_2_62.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_NEWANCODIRPF
     i_obj.ecpSave()
  endproc

  add object oAGGANCODTR2_2_63 as StdCheck with uid="QKOFIVTTRZ",rtseq=84,rtrep=.f.,left=157, top=447, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37731210,;
    cFormVar="w_AGGANCODTR2", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCODTR2_2_63.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCODTR2_2_63.GetRadio()
    this.Parent.oContained.w_AGGANCODTR2 = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCODTR2_2_63.SetRadio()
    this.Parent.oContained.w_AGGANCODTR2=trim(this.Parent.oContained.w_AGGANCODTR2)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCODTR2=='S',1,;
      0)
  endfunc

  func oAGGANCODTR2_2_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANRITENU $ 'CX')
    endwith
   endif
  endfunc

  func oAGGANCODTR2_2_63.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oNEWANCODTR2_2_64 as StdField with uid="GHBRNJJKRH",rtseq=85,rtrep=.f.,;
    cFormVar = "w_NEWANCODTR2", cQueryName = "NEWANCODTR2",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tributo per contributo previdenziale da aggiornare",;
    HelpContextID = 37796442,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=236, Top=449, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_NEWANCODTR2"

  func oNEWANCODTR2_2_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCODTR2='S')
    endwith
   endif
  endfunc

  func oNEWANCODTR2_2_64.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  func oNEWANCODTR2_2_64.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_64('Part',this)
    endwith
    return bRes
  endfunc

  proc oNEWANCODTR2_2_64.ecpDrop(oSource)
    this.Parent.oContained.link_2_64('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNEWANCODTR2_2_64.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oNEWANCODTR2_2_64'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"",'GSRI2AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oNEWANCODTR2_2_64.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_NEWANCODTR2
     i_obj.ecpSave()
  endproc

  add object oAGGANCAURITF_2_65 as StdCheck with uid="GNBUYIHYHC",rtseq=86,rtrep=.f.,left=157, top=480, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 42455675,;
    cFormVar="w_AGGANCAURITF", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCAURITF_2_65.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCAURITF_2_65.GetRadio()
    this.Parent.oContained.w_AGGANCAURITF = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCAURITF_2_65.SetRadio()
    this.Parent.oContained.w_AGGANCAURITF=trim(this.Parent.oContained.w_AGGANCAURITF)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCAURITF=='S',1,;
      0)
  endfunc

  func oAGGANCAURITF_2_65.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANRITENU$'CSX')
    endwith
   endif
  endfunc

  func oAGGANCAURITF_2_65.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc


  add object oNEWANCAURITF_2_66 as StdCombo with uid="MXXUIZRLQL",rtseq=87,rtrep=.f.,left=236,top=482,width=76,height=21;
    , ToolTipText = "Valore causale prestazione da aggiornare";
    , HelpContextID = 42520907;
    , cFormVar="w_NEWANCAURITF",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo F,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo J,"+"Tipo K,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo Z,"+"Tipo ZO", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oNEWANCAURITF_2_66.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'I',;
    iif(this.value =10,'J',;
    iif(this.value =11,'K',;
    iif(this.value =12,'L',;
    iif(this.value =13,'L1',;
    iif(this.value =14,'M',;
    iif(this.value =15,'M1',;
    iif(this.value =16,'M2',;
    iif(this.value =17,'N',;
    iif(this.value =18,'O',;
    iif(this.value =19,'O1',;
    iif(this.value =20,'P',;
    iif(this.value =21,'Q',;
    iif(this.value =22,'R',;
    iif(this.value =23,'S',;
    iif(this.value =24,'T',;
    iif(this.value =25,'U',;
    iif(this.value =26,'V',;
    iif(this.value =27,'V1',;
    iif(this.value =28,'V2',;
    iif(this.value =29,'W',;
    iif(this.value =30,'X',;
    iif(this.value =31,'Y',;
    iif(this.value =32,'Z',;
    iif(this.value =33,'ZO',;
    space(1)))))))))))))))))))))))))))))))))))
  endfunc
  func oNEWANCAURITF_2_66.GetRadio()
    this.Parent.oContained.w_NEWANCAURITF = this.RadioValue()
    return .t.
  endfunc

  func oNEWANCAURITF_2_66.SetRadio()
    this.Parent.oContained.w_NEWANCAURITF=trim(this.Parent.oContained.w_NEWANCAURITF)
    this.value = ;
      iif(this.Parent.oContained.w_NEWANCAURITF=='A',1,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='B',2,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='C',3,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='D',4,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='E',5,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='F',6,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='G',7,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='H',8,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='I',9,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='J',10,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='K',11,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='L',12,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='L1',13,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='M',14,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='M1',15,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='M2',16,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='N',17,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='O',18,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='O1',19,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='P',20,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='Q',21,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='R',22,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='S',23,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='T',24,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='U',25,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='V',26,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='V1',27,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='V2',28,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='W',29,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='X',30,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='Y',31,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='Z',32,;
      iif(this.Parent.oContained.w_NEWANCAURITF=='ZO',33,;
      0)))))))))))))))))))))))))))))))))
  endfunc

  func oNEWANCAURITF_2_66.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCAURITF='S')
    endwith
   endif
  endfunc

  func oNEWANCAURITF_2_66.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oAGGANPEINPS_2_67 as StdCheck with uid="LHIGPVWZMS",rtseq=88,rtrep=.f.,left=157, top=515, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 255969583,;
    cFormVar="w_AGGANPEINPS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANPEINPS_2_67.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANPEINPS_2_67.GetRadio()
    this.Parent.oContained.w_AGGANPEINPS = this.RadioValue()
    return .t.
  endfunc

  func oAGGANPEINPS_2_67.SetRadio()
    this.Parent.oContained.w_AGGANPEINPS=trim(this.Parent.oContained.w_AGGANPEINPS)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANPEINPS=='S',1,;
      0)
  endfunc

  func oAGGANPEINPS_2_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANRITENU $ 'CX')
    endwith
   endif
  endfunc

  func oAGGANPEINPS_2_67.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oNEWANPEINPS_2_68 as StdField with uid="LWMAAZXRAD",rtseq=89,rtrep=.f.,;
    cFormVar = "w_NEWANPEINPS", cQueryName = "NEWANPEINPS",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale imponibile ritenute previdenziali",;
    HelpContextID = 256034815,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=236, Top=516, cSayPict='"999.99"', cGetPict='"999.99"'

  func oNEWANPEINPS_2_68.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANPEINPS='S')
    endwith
   endif
  endfunc

  func oNEWANPEINPS_2_68.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oAGGANRIINPS_2_69 as StdCheck with uid="QQHSZJOSIY",rtseq=90,rtrep=.f.,left=157, top=548, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 247346897,;
    cFormVar="w_AGGANRIINPS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANRIINPS_2_69.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANRIINPS_2_69.GetRadio()
    this.Parent.oContained.w_AGGANRIINPS = this.RadioValue()
    return .t.
  endfunc

  func oAGGANRIINPS_2_69.SetRadio()
    this.Parent.oContained.w_AGGANRIINPS=trim(this.Parent.oContained.w_AGGANRIINPS)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANRIINPS=='S',1,;
      0)
  endfunc

  func oAGGANRIINPS_2_69.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANRITENU $ 'CX')
    endwith
   endif
  endfunc

  func oAGGANRIINPS_2_69.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oNEWANRIINPS_2_70 as StdField with uid="VECPKLHHLY",rtseq=91,rtrep=.f.,;
    cFormVar = "w_NEWANRIINPS", cQueryName = "NEWANRIINPS",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale ritenute previdenziali",;
    HelpContextID = 247281665,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=236, Top=548, cSayPict='"999.99"', cGetPict='"999.99"'

  func oNEWANRIINPS_2_70.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANRIINPS='S')
    endwith
   endif
  endfunc

  func oNEWANRIINPS_2_70.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oAGGANCOINPS_2_71 as StdCheck with uid="PGTKHCHKQO",rtseq=92,rtrep=.f.,left=522, top=415, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37865775,;
    cFormVar="w_AGGANCOINPS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCOINPS_2_71.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCOINPS_2_71.GetRadio()
    this.Parent.oContained.w_AGGANCOINPS = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCOINPS_2_71.SetRadio()
    this.Parent.oContained.w_AGGANCOINPS=trim(this.Parent.oContained.w_AGGANCOINPS)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCOINPS=='S',1,;
      0)
  endfunc

  func oAGGANCOINPS_2_71.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANRITENU $ 'CX')
    endwith
   endif
  endfunc

  func oAGGANCOINPS_2_71.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oNEWANCOINPS_2_72 as StdField with uid="BUBUNBPBVX",rtseq=93,rtrep=.f.,;
    cFormVar = "w_NEWANCOINPS", cQueryName = "NEWANCOINPS",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale ritenute previdenziali a carico del percipiente",;
    HelpContextID = 37931007,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=607, Top=417, cSayPict='"999.99"', cGetPict='"999.99"'

  func oNEWANCOINPS_2_72.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCOINPS='S')
    endwith
   endif
  endfunc

  func oNEWANCOINPS_2_72.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oAGGANCASPRO_2_73 as StdCheck with uid="NNJEFDCQDS",rtseq=94,rtrep=.f.,left=522, top=447, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37849945,;
    cFormVar="w_AGGANCASPRO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCASPRO_2_73.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCASPRO_2_73.GetRadio()
    this.Parent.oContained.w_AGGANCASPRO = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCASPRO_2_73.SetRadio()
    this.Parent.oContained.w_AGGANCASPRO=trim(this.Parent.oContained.w_AGGANCASPRO)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCASPRO=='S',1,;
      0)
  endfunc

  func oAGGANCASPRO_2_73.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANRITENU $ 'CSX')
    endwith
   endif
  endfunc

  func oAGGANCASPRO_2_73.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oNEWANCASPRO_2_74 as StdField with uid="FQKDTOMIXN",rtseq=95,rtrep=.f.,;
    cFormVar = "w_NEWANCASPRO", cQueryName = "NEWANCASPRO",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale contributo cassa ordine",;
    HelpContextID = 37915177,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=607, Top=449, cSayPict='"999.99"', cGetPict='"999.99"'

  func oNEWANCASPRO_2_74.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCASPRO='S')
    endwith
   endif
  endfunc

  func oNEWANCASPRO_2_74.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oAGGANCODATT_2_75 as StdCheck with uid="MVHOLPYAJG",rtseq=96,rtrep=.f.,left=522, top=483, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37870682,;
    cFormVar="w_AGGANCODATT", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCODATT_2_75.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCODATT_2_75.GetRadio()
    this.Parent.oContained.w_AGGANCODATT = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCODATT_2_75.SetRadio()
    this.Parent.oContained.w_AGGANCODATT=trim(this.Parent.oContained.w_AGGANCODATT)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCODATT=='S',1,;
      0)
  endfunc

  func oAGGANCODATT_2_75.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANRITENU $ 'CX')
    endwith
   endif
  endfunc

  func oAGGANCODATT_2_75.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oNEWANCODATT_2_76 as StdField with uid="FPOPDWFMMT",rtseq=97,rtrep=.f.,;
    cFormVar = "w_NEWANCODATT", cQueryName = "NEWANCODATT",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Codice attivit� non valido (inserire 1..28)",;
    ToolTipText = "Codice attivit� (1-28)",;
    HelpContextID = 37935914,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=607, Top=481, cSayPict='"99"', cGetPict='"99"'

  func oNEWANCODATT_2_76.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCODATT='S')
    endwith
   endif
  endfunc

  func oNEWANCODATT_2_76.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  func oNEWANCODATT_2_76.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NEWANCODATT>=1 AND .w_NEWANCODATT<=28)
    endwith
    return bRes
  endfunc

  add object oAGGANCODASS_2_77 as StdCheck with uid="AGKFQAYMVZ",rtseq=98,rtrep=.f.,left=522, top=513, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37866330,;
    cFormVar="w_AGGANCODASS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCODASS_2_77.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCODASS_2_77.GetRadio()
    this.Parent.oContained.w_AGGANCODASS = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCODASS_2_77.SetRadio()
    this.Parent.oContained.w_AGGANCODASS=trim(this.Parent.oContained.w_AGGANCODASS)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCODASS=='S',1,;
      0)
  endfunc

  func oAGGANCODASS_2_77.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANRITENU$'CX')
    endwith
   endif
  endfunc

  func oAGGANCODASS_2_77.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oNEWANCODASS_2_78 as StdField with uid="INQUGXNTZD",rtseq=99,rtrep=.f.,;
    cFormVar = "w_NEWANCODASS", cQueryName = "NEWANCODASS",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice altra assicurazione previdenziale",;
    HelpContextID = 37931562,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=607, Top=516, InputMask=replicate('X',3)

  func oNEWANCODASS_2_78.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCODASS='S')
    endwith
   endif
  endfunc

  func oNEWANCODASS_2_78.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oAGGANCOIMPS_2_79 as StdCheck with uid="USEKTOOYXC",rtseq=100,rtrep=.f.,left=522, top=548, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37865759,;
    cFormVar="w_AGGANCOIMPS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCOIMPS_2_79.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCOIMPS_2_79.GetRadio()
    this.Parent.oContained.w_AGGANCOIMPS = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCOIMPS_2_79.SetRadio()
    this.Parent.oContained.w_AGGANCOIMPS=trim(this.Parent.oContained.w_AGGANCOIMPS)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCOIMPS=='S',1,;
      0)
  endfunc

  func oAGGANCOIMPS_2_79.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANRITENU$'CX')
    endwith
   endif
  endfunc

  func oAGGANCOIMPS_2_79.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oNEWANCOIMPS_2_80 as StdField with uid="MNYBNNXVEG",rtseq=101,rtrep=.f.,;
    cFormVar = "w_NEWANCOIMPS", cQueryName = "NEWANCOIMPS",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice INPS",;
    HelpContextID = 37930991,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=607, Top=548, InputMask=replicate('X',20)

  func oNEWANCOIMPS_2_80.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCOIMPS='S')
    endwith
   endif
  endfunc

  func oNEWANCOIMPS_2_80.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc


  add object oBtn_2_84 as StdButton with uid="TOYTSIORUM",left=675, top=599, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per aggiornare i dati";
    , HelpContextID = 260194838;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_84.Click()
      with this.Parent.oContained
        GSAR_BKF(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_85 as StdButton with uid="EGNAIUHSLT",left=730, top=599, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 260194838;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_85.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oAGGANCODEST_2_140 as StdCheck with uid="CEFPGYVAGB",rtseq=137,rtrep=.f.,left=157, top=580, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37870490,;
    cFormVar="w_AGGANCODEST", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCODEST_2_140.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCODEST_2_140.GetRadio()
    this.Parent.oContained.w_AGGANCODEST = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCODEST_2_140.SetRadio()
    this.Parent.oContained.w_AGGANCODEST=trim(this.Parent.oContained.w_AGGANCODEST)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCODEST=='S',1,;
      0)
  endfunc

  func oAGGANCODEST_2_140.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oNEWCODEST_2_141 as StdField with uid="IDWOETPJYQ",rtseq=138,rtrep=.f.,;
    cFormVar = "w_NEWCODEST", cQueryName = "NEWCODEST",;
    bObbl = .t. , nPag = 2, value=space(7), bMultilanguage =  .f.,;
    ToolTipText = "Per le fatture PA contiene il codice, di 6 caratteri, dell'ufficio destinatario della fattura, riportato nella rubrica Indice PA,  per le fatture privati contiene il codice, di 7 caratteri, assegnato dal Sdi ai sogg. che hanno accreditato un canale",;
    HelpContextID = 55527529,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=236, Top=580, InputMask=replicate('X',7)

  func oNEWCODEST_2_141.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCODEST='S')
    endwith
   endif
  endfunc

  func oNEWCODEST_2_141.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oAGGANCODCLA_2_142 as StdCheck with uid="KVVRXUANSK",rtseq=139,rtrep=.f.,left=522, top=579, caption="Aggiorna",;
    ToolTipText = "Selezionare per aggiornare il dato",;
    HelpContextID = 37790842,;
    cFormVar="w_AGGANCODCLA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAGGANCODCLA_2_142.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGANCODCLA_2_142.GetRadio()
    this.Parent.oContained.w_AGGANCODCLA = this.RadioValue()
    return .t.
  endfunc

  func oAGGANCODCLA_2_142.SetRadio()
    this.Parent.oContained.w_AGGANCODCLA=trim(this.Parent.oContained.w_AGGANCODCLA)
    this.value = ;
      iif(this.Parent.oContained.w_AGGANCODCLA=='S',1,;
      0)
  endfunc

  func oAGGANCODCLA_2_142.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oNEWCODCLA_2_143 as StdField with uid="GKGBIVASZT",rtseq=140,rtrep=.f.,;
    cFormVar = "w_NEWCODCLA", cQueryName = "NEWCODCLA",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice di classificazione per fatturazione elettronica",;
    HelpContextID = 55527218,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=607, Top=580, InputMask=replicate('X',5)

  func oNEWCODCLA_2_143.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AGGANCODCLA='S')
    endwith
   endif
  endfunc

  func oNEWCODCLA_2_143.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oStr_2_82 as StdString with uid="SAJYSCUIOK",Visible=.t., Left=-2, Top=365,;
    Alignment=1, Width=162, Height=18,;
    Caption="Opzioni aggiornamento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_86 as StdString with uid="RBNFKEKUKQ",Visible=.t., Left=31, Top=417,;
    Alignment=1, Width=121, Height=18,;
    Caption="Categoria contabile:"  ;
  , bGlobalFont=.t.

  func oStr_2_86.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  add object oStr_2_87 as StdString with uid="OMKJCSANYN",Visible=.t., Left=7, Top=391,;
    Alignment=1, Width=145, Height=18,;
    Caption="Mastro contabile:"  ;
  , bGlobalFont=.t.

  func oStr_2_87.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  add object oStr_2_88 as StdString with uid="ZYKOKWCBFN",Visible=.t., Left=24, Top=450,;
    Alignment=1, Width=128, Height=18,;
    Caption="Cod. IVA esenz./agev.:"  ;
  , bGlobalFont=.t.

  func oStr_2_88.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  add object oStr_2_89 as StdString with uid="ZMZMULMSMJ",Visible=.t., Left=443, Top=391,;
    Alignment=1, Width=75, Height=18,;
    Caption="IRPEF/IRES:"  ;
  , bGlobalFont=.t.

  func oStr_2_89.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oStr_2_90 as StdString with uid="QEQUAYUCRX",Visible=.t., Left=380, Top=417,;
    Alignment=1, Width=138, Height=18,;
    Caption="Codice Tributo I.R.PE.F.:"  ;
  , bGlobalFont=.t.

  func oStr_2_90.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oStr_2_91 as StdString with uid="RTEPEIPRID",Visible=.t., Left=39, Top=482,;
    Alignment=1, Width=113, Height=18,;
    Caption="Tipo operazione IVA:"  ;
  , bGlobalFont=.t.

  func oStr_2_91.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  add object oStr_2_92 as StdString with uid="MJQMOJZPUT",Visible=.t., Left=39, Top=517,;
    Alignment=1, Width=113, Height=18,;
    Caption="Gestione partite:"  ;
  , bGlobalFont=.t.

  func oStr_2_92.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C' )
    endwith
  endfunc

  add object oStr_2_93 as StdString with uid="YHVABFXIRB",Visible=.t., Left=380, Top=450,;
    Alignment=1, Width=138, Height=18,;
    Caption="Causale prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_2_93.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oStr_2_94 as StdString with uid="FELHKWNKLI",Visible=.t., Left=380, Top=485,;
    Alignment=1, Width=138, Height=18,;
    Caption="Controllo fido:"  ;
  , bGlobalFont=.t.

  func oStr_2_94.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'C'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oStr_2_98 as StdString with uid="BAACDVFWRB",Visible=.t., Left=26, Top=391,;
    Alignment=1, Width=126, Height=18,;
    Caption="Cat.commerciale:"  ;
  , bGlobalFont=.t.

  func oStr_2_98.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oStr_2_99 as StdString with uid="CLOSBHFOZK",Visible=.t., Left=26, Top=417,;
    Alignment=1, Width=126, Height=18,;
    Caption="Categoria sconti/mag.:"  ;
  , bGlobalFont=.t.

  func oStr_2_99.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZ')
    endwith
  endfunc

  add object oStr_2_100 as StdString with uid="KGGQJFEOFQ",Visible=.t., Left=26, Top=450,;
    Alignment=1, Width=126, Height=18,;
    Caption="Cod. zona:"  ;
  , bGlobalFont=.t.

  func oStr_2_100.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oStr_2_101 as StdString with uid="HXSVWMSZCI",Visible=.t., Left=19, Top=482,;
    Alignment=1, Width=133, Height=18,;
    Caption="Categoria provvigioni:"  ;
  , bGlobalFont=.t.

  func oStr_2_101.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  add object oStr_2_104 as StdString with uid="DZMXSOCOSW",Visible=.t., Left=26, Top=517,;
    Alignment=1, Width=126, Height=18,;
    Caption="Codice agente:"  ;
  , bGlobalFont=.t.

  func oStr_2_104.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  add object oStr_2_105 as StdString with uid="QJHESAJQJY",Visible=.t., Left=393, Top=391,;
    Alignment=1, Width=125, Height=18,;
    Caption="Mag. preferenziale:"  ;
  , bGlobalFont=.t.

  func oStr_2_105.mHide()
    with this.Parent.oContained
      return (IsAlt() Or .w_SEZIONE$'CPRZA')
    endwith
  endfunc

  add object oStr_2_106 as StdString with uid="HYHQCNMZVZ",Visible=.t., Left=377, Top=391,;
    Alignment=1, Width=141, Height=18,;
    Caption="Magazzino C/Lavoro:"  ;
  , bGlobalFont=.t.

  func oStr_2_106.mHide()
    with this.Parent.oContained
      return (Isalt() Or .w_SEZIONE$'CPRZV')
    endwith
  endfunc

  add object oStr_2_108 as StdString with uid="OZLPYBBZTH",Visible=.t., Left=45, Top=391,;
    Alignment=1, Width=107, Height=18,;
    Caption="Cod. pagamento:"  ;
  , bGlobalFont=.t.

  func oStr_2_108.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CRAVZ' )
    endwith
  endfunc

  add object oStr_2_109 as StdString with uid="YFYQLOOSNW",Visible=.t., Left=45, Top=417,;
    Alignment=1, Width=107, Height=18,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  func oStr_2_109.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CRAVZ' )
    endwith
  endfunc

  add object oStr_2_113 as StdString with uid="QJGMWEZNCG",Visible=.t., Left=27, Top=391,;
    Alignment=1, Width=125, Height=18,;
    Caption="Gestione contenzioso:"  ;
  , bGlobalFont=.t.

  func oStr_2_113.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oStr_2_114 as StdString with uid="AMMOUIKXNZ",Visible=.t., Left=30, Top=450,;
    Alignment=1, Width=122, Height=18,;
    Caption="Pagam.moratoria:"  ;
  , bGlobalFont=.t.

  func oStr_2_114.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oStr_2_115 as StdString with uid="YQPDMYETCI",Visible=.t., Left=342, Top=391,;
    Alignment=1, Width=176, Height=18,;
    Caption="Escl appl. interessi di mora:"  ;
  , bGlobalFont=.t.

  func oStr_2_115.mHide()
    with this.Parent.oContained
      return (!(.w_SEZIONE='Z' AND .w_TIPCON='C') AND !(.w_SEZIONE='P' AND .w_TIPCON='F'))
    endwith
  endfunc

  add object oStr_2_116 as StdString with uid="UYAZMUXCRB",Visible=.t., Left=5, Top=417,;
    Alignment=1, Width=147, Height=18,;
    Caption="Bloc. per contenzioso:"  ;
  , bGlobalFont=.t.

  func oStr_2_116.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oStr_2_119 as StdString with uid="AEYCVSWMVI",Visible=.t., Left=94, Top=482,;
    Alignment=1, Width=58, Height=18,;
    Caption="Fino a:"  ;
  , bGlobalFont=.t.

  func oStr_2_119.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'Z'  Or .w_TIPCON='F')
    endwith
  endfunc

  add object oStr_2_120 as StdString with uid="QUCXNITDTJ",Visible=.t., Left=397, Top=417,;
    Alignment=1, Width=121, Height=18,;
    Caption="Interessi di mora:"  ;
  , bGlobalFont=.t.

  func oStr_2_120.mHide()
    with this.Parent.oContained
      return (!(.w_SEZIONE='Z' AND .w_TIPCON='C') AND !(.w_SEZIONE='P' AND .w_TIPCON='F'))
    endwith
  endfunc

  add object oStr_2_122 as StdString with uid="NSLYSVKTPW",Visible=.t., Left=28, Top=391,;
    Alignment=1, Width=124, Height=18,;
    Caption="Ritenute:"  ;
  , bGlobalFont=.t.

  func oStr_2_122.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oStr_2_123 as StdString with uid="ONHFZLIRVM",Visible=.t., Left=20, Top=417,;
    Alignment=1, Width=132, Height=18,;
    Caption="Tributo I.R.PE.F.:"  ;
  , bGlobalFont=.t.

  func oStr_2_123.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R' )
    endwith
  endfunc

  add object oStr_2_126 as StdString with uid="DEHQJVOWRB",Visible=.t., Left=24, Top=450,;
    Alignment=1, Width=128, Height=18,;
    Caption="Contrib. previdenziale:"  ;
  , bGlobalFont=.t.

  func oStr_2_126.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oStr_2_128 as StdString with uid="RINPZJSKLJ",Visible=.t., Left=14, Top=482,;
    Alignment=1, Width=138, Height=18,;
    Caption="Causale prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_2_128.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oStr_2_129 as StdString with uid="IDIMEAVTXO",Visible=.t., Left=77, Top=551,;
    Alignment=1, Width=75, Height=15,;
    Caption="% Ritenuta:"  ;
  , bGlobalFont=.t.

  func oStr_2_129.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oStr_2_130 as StdString with uid="NMNAUSNLKX",Visible=.t., Left=48, Top=517,;
    Alignment=1, Width=104, Height=15,;
    Caption="% Imponibile:"  ;
  , bGlobalFont=.t.

  func oStr_2_130.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oStr_2_131 as StdString with uid="KNKJVYEGOO",Visible=.t., Left=388, Top=450,;
    Alignment=1, Width=130, Height=18,;
    Caption="% Contr. cassa ordine:"  ;
  , bGlobalFont=.t.

  func oStr_2_131.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oStr_2_132 as StdString with uid="IZPAPNVVEE",Visible=.t., Left=385, Top=417,;
    Alignment=1, Width=133, Height=18,;
    Caption="% a carico percipiente:"  ;
  , bGlobalFont=.t.

  func oStr_2_132.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oStr_2_134 as StdString with uid="PGFNSITHXT",Visible=.t., Left=357, Top=514,;
    Alignment=1, Width=161, Height=18,;
    Caption="Altra assicurazione prev.:"  ;
  , bGlobalFont=.t.

  func oStr_2_134.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oStr_2_135 as StdString with uid="AQVQGUFPDD",Visible=.t., Left=437, Top=551,;
    Alignment=1, Width=81, Height=15,;
    Caption="Codice INPS:"  ;
  , bGlobalFont=.t.

  func oStr_2_135.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oStr_2_136 as StdString with uid="CMDFSVHHWN",Visible=.t., Left=400, Top=485,;
    Alignment=1, Width=118, Height=18,;
    Caption="Codice attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_2_136.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE<>'R')
    endwith
  endfunc

  add object oStr_2_137 as StdString with uid="YCSZYPNUUB",Visible=.t., Left=358, Top=450,;
    Alignment=1, Width=160, Height=18,;
    Caption="Spread su saggio di mora:"  ;
  , bGlobalFont=.t.

  func oStr_2_137.mHide()
    with this.Parent.oContained
      return (!(.w_SEZIONE='Z' AND .w_TIPCON='C') AND !(.w_SEZIONE='P' AND .w_TIPCON='F'))
    endwith
  endfunc

  add object oStr_2_138 as StdString with uid="WEDQLDHQOR",Visible=.t., Left=40, Top=582,;
    Alignment=1, Width=112, Height=18,;
    Caption="Codice destinatario:"  ;
  , bGlobalFont=.t.

  func oStr_2_138.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oStr_2_139 as StdString with uid="BRQAAFGCCM",Visible=.t., Left=348, Top=581,;
    Alignment=1, Width=170, Height=18,;
    Caption="Classificazione FE:"  ;
  , bGlobalFont=.t.

  func oStr_2_139.mHide()
    with this.Parent.oContained
      return (.w_SEZIONE$'CPRZ' )
    endwith
  endfunc

  add object oBox_2_83 as StdBox with uid="QHEGMXSNWS",left=-2, top=383, width=783,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kcf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
