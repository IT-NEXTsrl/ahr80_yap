* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_baf                                                        *
*              Lancia la corretta gestione della classe documentale            *
*                                                                              *
*      Author: GB                                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-08-27                                                      *
* Last revis.: 2008-09-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_baf",oParentObject)
return(i_retval)

define class tgsut_baf as StdBatch
  * --- Local variables
  w_PROG = space(8)
  w_CODICE = space(15)
  w_RIGHTKEY = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine che in base al contesto (Modulo DOCM attivo o no) apre la corretta gestione della classe documentale
    if Type("g_oMenu.oKey")<>"U"
      this.w_RIGHTKEY = .T.
      this.w_CODICE = Nvl(g_oMenu.oKey(1,3),"")
    else
      if i_curform=.NULL.
        i_retcode = 'stop'
        return
      endif
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      * --- Codice classe documentale
      this.w_CODICE = &cCurs..IDCLADOC
    endif
    if g_DOCM = "S"
      * --- Modulo attivo
      this.w_PROG = "GSUT_MCD"
    else
      * --- Modulo disattivo
      this.w_PROG = "GSUT_ACL"
    endif
    OpenGest(iif(this.w_RIGHTKEY,g_oMenu.cBatchType,"S"),this.w_PROG,"CDCODCLA",this.w_CODICE)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
