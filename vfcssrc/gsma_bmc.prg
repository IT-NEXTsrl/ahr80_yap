* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bmc                                                        *
*              Check fuori transazione mov.magazzino                           *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-08                                                      *
* Last revis.: 2010-01-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bmc",oParentObject)
return(i_retval)

define class tgsma_bmc as StdBatch
  * --- Local variables
  w_PADRE = .NULL.
  w_OK = .f.
  w_TRIG = 0
  w_CFUNC = space(10)
  w_MESS = space(200)
  w_CODMAG = space(5)
  w_CODMAT = space(5)
  w_RIGA = space(4)
  w_CAUCOL = space(5)
  w_FLELGM = space(1)
  w_COART = space(20)
  w_ARDTOBSO = ctod("  /  /  ")
  * --- WorkFile variables
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Check fuori transazione conferma movimenti di magazzino
    this.w_PADRE = This.oParentObject
    this.w_CFUNC = this.w_PADRE.cFunction
    this.w_OK = .T.
    * --- Eseguo controllo su data consolidamento impostata nei dati azienda
    this.w_MESS = CHKCONS("M",this.oParentObject.w_MMDATREG,"B","S")
    this.w_OK = Empty(this.w_MESS)
    if this.w_OK
      this.w_PADRE.MarkPos()     
       
 Select ( this.w_PADRE.cTrsName ) 
 Go Top
      this.w_TRIG = 0
      do while Not Eof ( this.w_PADRE.cTrsName )
        * --- La riga � valida...
        if this.w_PADRE.FullRow()
          this.w_TRIG = this.w_TRIG + IIF(Deleted() , 0 , 1 )
          this.w_CODMAG = NVL(t_MMCODMAG, " ")
          this.w_CODMAT = NVL(t_MMCODMAT, " ")
          this.w_RIGA = ALLTR(STR(t_CPROWORD))
          this.w_CAUCOL = NVL(t_MMCAUCOL, " ")
          this.w_FLELGM = iif(t_MMFLELGM=1,"S","")
          this.w_COART = NVL(t_MMCODART, " ")
          do case
            case EMPTY(this.w_CODMAG)
              this.w_MESS = "Riga movimento: %1 Inserire codice magazzino"
              this.w_OK = .F.
            case NOT EMPTY(this.w_CAUCOL) AND EMPTY(this.w_CODMAT)
              this.w_MESS = "Riga movimento: %1 Inserire codice magazzino collegato"
              this.w_OK = .F.
            case (EMPTY(t_MMQTAMOV) OR EMPTY(t_MMQTAUM1))
              if EMPTY(t_MMQTAUM1)
                this.w_MESS = "Riga movimento: %1%0Quantit� nella 1^UM inesistente"
              else
                this.w_MESS = "Riga movimento: %1%0Quantit� inesistente"
              endif
              this.w_OK = .F.
          endcase
          if ! CHKGIOM(this.oParentObject.w_MMCODESE,this.w_CODMAG,this.oParentObject.w_MMDATREG,this.w_FLELGM)
            this.w_MESS = "Registrazione gi� stampata sul giornale magazzino; impossibile variare/cancellare"
            this.w_OK = .F.
          else
            if ! EMPTY(this.w_CODMAT) AND ! CHKGIOM(this.oParentObject.w_MMCODESE,this.w_CODMAT,this.oParentObject.w_MMDATREG,this.w_FLELGM)
              this.w_MESS = "Registrazione gi� stampata sul giornale magazzino; impossibile variare/cancellare"
              this.w_OK = .F.
            endif
          endif
          if this.w_OK AND NOT EMPTY( this.w_COART )
            * --- Controllo della data di obsolescenza degli articoli
            * --- Read from ART_ICOL
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ART_ICOL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ARDTOBSO"+;
                " from "+i_cTable+" ART_ICOL where ";
                    +"ARCODART = "+cp_ToStrODBC(this.w_COART);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ARDTOBSO;
                from (i_cTable) where;
                    ARCODART = this.w_COART;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ARDTOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_ARDTOBSO = NVL( this.w_ARDTOBSO , CTOD( "  -  -  " ) )
            if NOT EMPTY(this.w_ARDTOBSO) AND (this.w_ARDTOBSO<=this.oParentObject.w_OBTEST)
              this.w_MESS = ah_Msgformat("Articolo %1 obsoleto su riga %2", ALLTRIM( this.w_COART ) , ALLTRIM(this.w_RIGA) )
              this.w_OK = .F.
            endif
          endif
        endif
        * --- Se tutto ok passo alla prossima riga altrimenti esco...
        if this.w_OK
           
 Select ( this.w_PADRE.cTrsName ) 
 Skip
        else
          * --- Do il messaggio ed esco
          ah_ErrorMsg(this.w_MESS,,"",this.w_RIGA)
          Exit
        endif
      enddo
      this.w_PADRE.RePos(.F.)     
    endif
    if this.w_OK And g_PERDIS="S" 
      * --- Lancio il check disponibilit� articoli
      this.w_OK = GSAR_BDA( This , "S", this.w_PADRE )
    endif
    if this.w_OK AND this.w_TRIG=0 AND this.w_CFUNC<>"Query"
      this.w_MESS = "Registrazione senza righe di dettaglio"
      Ah_errormsg(this.w_MESS)
      this.w_OK = .F.
    endif
    * --- Ritorno il reponso alla CheckForm
    this.oParentObject.w_RESCHK = iif( this.w_OK , 0 , -1 )
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ART_ICOL'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
