* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bi2                                                        *
*              Attribuzione codici IVA                                         *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_31]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-01-28                                                      *
* Last revis.: 2006-03-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bi2",oParentObject)
return(i_retval)

define class tgsar_bi2 as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_ERR = .f.
  w_MESS = space(250)
  * --- WorkFile variables
  ATTCOIVA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- ATTRIBUZIONE CODICI IVA (Da GASR_ACI)
    *     CONTROLLI UNIVOCITA' COMBINAZIONE
    this.w_ERR = .f.
    this.w_SERIAL = SPACE(10)
    if EMPTY(this.oParentObject.w_ACTIPCLF + this.oParentObject.w_ACTIPART + this.oParentObject.w_ACTIPOPE)
      * --- Nessuna Combinazione inserita
      this.w_MESS = AH_MSGFORMAT( "Nessuna combinazione inserita" )
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    else
      * --- Se in update  e la combinazione ha lo stesso seriale va bene
      *     (se esiste combinazione obsoleta il record viene comunque salvato)
      NOBSO = "(Empty(Nvl(_Curs_ATTCOIVA->ACDTOBSO, cp_CharToDate('  -  -    '))) Or Nvl(_Curs_ATTCOIVA->ACDTOBSO, cp_CharToDate('  -  -    ')) > this.OparentObject.w_OBTEST)"
      do case
        case !Empty(this.oParentObject.w_ACTIPOPE) and !Empty(this.oParentObject.w_ACTIPART) and !Empty(this.oParentObject.w_ACTIPCLF)
          * --- Tutte e tre le tipologie applicate
          * --- Select from ATTCOIVA
          i_nConn=i_TableProp[this.ATTCOIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTCOIVA_idx,2],.t.,this.ATTCOIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ACSERIAL,ACDTOBSO  from "+i_cTable+" ATTCOIVA ";
                +" where ACTIPART= "+cp_ToStrODBC(this.oParentObject.w_ACTIPART)+" And ACTIPCLF= "+cp_ToStrODBC(this.oParentObject.w_ACTIPCLF)+" And ACTIPOPE= "+cp_ToStrODBC(this.oParentObject.w_ACTIPOPE)+"";
                 ,"_Curs_ATTCOIVA")
          else
            select ACSERIAL,ACDTOBSO from (i_cTable);
             where ACTIPART= this.oParentObject.w_ACTIPART And ACTIPCLF= this.oParentObject.w_ACTIPCLF And ACTIPOPE= this.oParentObject.w_ACTIPOPE;
              into cursor _Curs_ATTCOIVA
          endif
          if used('_Curs_ATTCOIVA')
            select _Curs_ATTCOIVA
            locate for 1=1
            do while not(eof())
            if this.oParentObject.w_ACSERIAL <> _Curs_ATTCOIVA.ACSERIAL And &NOBSO
              * --- Trovata stessa tripla
              this.w_ERR = .t.
            endif
              select _Curs_ATTCOIVA
              continue
            enddo
            use
          endif
        case !Empty(this.oParentObject.w_ACTIPOPE) and !Empty(this.oParentObject.w_ACTIPART)
          * --- Tipologia Operazione  'OP' + Tipologia Articolo 'AR'
          * --- Select from ATTCOIVA
          i_nConn=i_TableProp[this.ATTCOIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTCOIVA_idx,2],.t.,this.ATTCOIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ACTIPCLF, ACSERIAL,ACDTOBSO  from "+i_cTable+" ATTCOIVA ";
                +" where ACTIPART= "+cp_ToStrODBC(this.oParentObject.w_ACTIPART)+" And ACTIPOPE= "+cp_ToStrODBC(this.oParentObject.w_ACTIPOPE)+"";
                 ,"_Curs_ATTCOIVA")
          else
            select ACTIPCLF, ACSERIAL,ACDTOBSO from (i_cTable);
             where ACTIPART= this.oParentObject.w_ACTIPART And ACTIPOPE= this.oParentObject.w_ACTIPOPE;
              into cursor _Curs_ATTCOIVA
          endif
          if used('_Curs_ATTCOIVA')
            select _Curs_ATTCOIVA
            locate for 1=1
            do while not(eof())
            if EMPTY(NVL( _Curs_ATTCOIVA.ACTIPCLF , "  " ) ) and this.oParentObject.w_ACSERIAL <> _Curs_ATTCOIVA.ACSERIAL And &NOBSO
              * --- Trovata stessa doppia
              this.w_ERR = .t.
            endif
              select _Curs_ATTCOIVA
              continue
            enddo
            use
          endif
        case !Empty(this.oParentObject.w_ACTIPOPE) and !Empty(this.oParentObject.w_ACTIPCLF)
          * --- Tipologia Operazione  'OP' + Tipologia Cliente 'CF'
          * --- Select from ATTCOIVA
          i_nConn=i_TableProp[this.ATTCOIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTCOIVA_idx,2],.t.,this.ATTCOIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ACTIPART, ACSERIAL,ACDTOBSO  from "+i_cTable+" ATTCOIVA ";
                +" where ACTIPCLF="+cp_ToStrODBC(this.oParentObject.w_ACTIPCLF)+" And ACTIPOPE="+cp_ToStrODBC(this.oParentObject.w_ACTIPOPE)+"";
                 ,"_Curs_ATTCOIVA")
          else
            select ACTIPART, ACSERIAL,ACDTOBSO from (i_cTable);
             where ACTIPCLF=this.oParentObject.w_ACTIPCLF And ACTIPOPE=this.oParentObject.w_ACTIPOPE;
              into cursor _Curs_ATTCOIVA
          endif
          if used('_Curs_ATTCOIVA')
            select _Curs_ATTCOIVA
            locate for 1=1
            do while not(eof())
            if EMPTY(NVL( _Curs_ATTCOIVA.ACTIPART , "  " ) ) and this.oParentObject.w_ACSERIAL <> _Curs_ATTCOIVA.ACSERIAL And &NOBSO
              * --- Trovata stessa doppia
              this.w_ERR = .t.
            endif
              select _Curs_ATTCOIVA
              continue
            enddo
            use
          endif
        case !Empty(this.oParentObject.w_ACTIPCLF) and !Empty(this.oParentObject.w_ACTIPART)
          * --- Tipologia Articolo  'AR' + Tipologia Cliente 'CF'
          * --- Select from ATTCOIVA
          i_nConn=i_TableProp[this.ATTCOIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTCOIVA_idx,2],.t.,this.ATTCOIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ACTIPOPE, ACSERIAL,ACDTOBSO  from "+i_cTable+" ATTCOIVA ";
                +" where ACTIPART= "+cp_ToStrODBC(this.oParentObject.w_ACTIPART)+" And ACTIPCLF="+cp_ToStrODBC(this.oParentObject.w_ACTIPCLF)+"";
                 ,"_Curs_ATTCOIVA")
          else
            select ACTIPOPE, ACSERIAL,ACDTOBSO from (i_cTable);
             where ACTIPART= this.oParentObject.w_ACTIPART And ACTIPCLF=this.oParentObject.w_ACTIPCLF;
              into cursor _Curs_ATTCOIVA
          endif
          if used('_Curs_ATTCOIVA')
            select _Curs_ATTCOIVA
            locate for 1=1
            do while not(eof())
            if EMPTY(NVL( _Curs_ATTCOIVA.ACTIPOPE , "  " ) ) and this.oParentObject.w_ACSERIAL <> _Curs_ATTCOIVA.ACSERIAL And &NOBSO
              * --- Trovata stessa doppia
              this.w_ERR = .t.
            endif
              select _Curs_ATTCOIVA
              continue
            enddo
            use
          endif
        case !Empty(this.oParentObject.w_ACTIPCLF)
          * --- Solo Tipologia Cliente  'CF'
          * --- Select from ATTCOIVA
          i_nConn=i_TableProp[this.ATTCOIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTCOIVA_idx,2],.t.,this.ATTCOIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ACTIPART, ACTIPOPE, ACSERIAL,ACDTOBSO  from "+i_cTable+" ATTCOIVA ";
                +" where ACTIPCLF="+cp_ToStrODBC(this.oParentObject.w_ACTIPCLF)+"";
                 ,"_Curs_ATTCOIVA")
          else
            select ACTIPART, ACTIPOPE, ACSERIAL,ACDTOBSO from (i_cTable);
             where ACTIPCLF=this.oParentObject.w_ACTIPCLF;
              into cursor _Curs_ATTCOIVA
          endif
          if used('_Curs_ATTCOIVA')
            select _Curs_ATTCOIVA
            locate for 1=1
            do while not(eof())
            if EMPTY(NVL( _Curs_ATTCOIVA.ACTIPART , "  " ) ) and EMPTY(NVL( _Curs_ATTCOIVA.ACTIPOPE , "  " ) ) and this.oParentObject.w_ACSERIAL <> _Curs_ATTCOIVA.ACSERIAL And &NOBSO
              * --- Trovata stessa categoria di tipo 'CF'
              this.w_ERR = .t.
            endif
              select _Curs_ATTCOIVA
              continue
            enddo
            use
          endif
        case !Empty(this.oParentObject.w_ACTIPART)
          * --- Solo Tipologia Articolo  'AR'
          * --- Select from ATTCOIVA
          i_nConn=i_TableProp[this.ATTCOIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTCOIVA_idx,2],.t.,this.ATTCOIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ACTIPCLF, ACTIPOPE, ACSERIAL,ACDTOBSO  from "+i_cTable+" ATTCOIVA ";
                +" where ACTIPART= "+cp_ToStrODBC(this.oParentObject.w_ACTIPART)+"";
                 ,"_Curs_ATTCOIVA")
          else
            select ACTIPCLF, ACTIPOPE, ACSERIAL,ACDTOBSO from (i_cTable);
             where ACTIPART= this.oParentObject.w_ACTIPART;
              into cursor _Curs_ATTCOIVA
          endif
          if used('_Curs_ATTCOIVA')
            select _Curs_ATTCOIVA
            locate for 1=1
            do while not(eof())
            if EMPTY(NVL( _Curs_ATTCOIVA.ACTIPCLF , "  " ) ) and EMPTY(NVL( _Curs_ATTCOIVA.ACTIPOPE , "  " ) ) and this.oParentObject.w_ACSERIAL <> _Curs_ATTCOIVA.ACSERIAL And &NOBSO
              * --- Trovata stessa categoria di tipo 'AR'
              this.w_ERR = .t.
            endif
              select _Curs_ATTCOIVA
              continue
            enddo
            use
          endif
        case !Empty(this.oParentObject.w_ACTIPOPE)
          * --- Solo Tipologia Operazione 'OP'
          * --- Select from ATTCOIVA
          i_nConn=i_TableProp[this.ATTCOIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTCOIVA_idx,2],.t.,this.ATTCOIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ACTIPCLF, ACTIPART, ACSERIAL,ACDTOBSO  from "+i_cTable+" ATTCOIVA ";
                +" where ACTIPOPE= "+cp_ToStrODBC(this.oParentObject.w_ACTIPOPE)+"";
                 ,"_Curs_ATTCOIVA")
          else
            select ACTIPCLF, ACTIPART, ACSERIAL,ACDTOBSO from (i_cTable);
             where ACTIPOPE= this.oParentObject.w_ACTIPOPE;
              into cursor _Curs_ATTCOIVA
          endif
          if used('_Curs_ATTCOIVA')
            select _Curs_ATTCOIVA
            locate for 1=1
            do while not(eof())
            if EMPTY(NVL( _Curs_ATTCOIVA.ACTIPCLF , "  " ) ) and EMPTY(NVL( _Curs_ATTCOIVA.ACTIPART , "  " ) ) and this.oParentObject.w_ACSERIAL <> _Curs_ATTCOIVA.ACSERIAL And &NOBSO
              * --- Trovata stessa categoria di tipo 'OP'
              this.w_ERR = .t.
            endif
              select _Curs_ATTCOIVA
              continue
            enddo
            use
          endif
      endcase
      if this.w_ERR
        this.w_MESS = AH_MSGFORMAT( "Combinazione gi� inserita" )
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ATTCOIVA'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_ATTCOIVA')
      use in _Curs_ATTCOIVA
    endif
    if used('_Curs_ATTCOIVA')
      use in _Curs_ATTCOIVA
    endif
    if used('_Curs_ATTCOIVA')
      use in _Curs_ATTCOIVA
    endif
    if used('_Curs_ATTCOIVA')
      use in _Curs_ATTCOIVA
    endif
    if used('_Curs_ATTCOIVA')
      use in _Curs_ATTCOIVA
    endif
    if used('_Curs_ATTCOIVA')
      use in _Curs_ATTCOIVA
    endif
    if used('_Curs_ATTCOIVA')
      use in _Curs_ATTCOIVA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
