* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bzl                                                        *
*              Apre anagrafica listini                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-11-05                                                      *
* Last revis.: 2008-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bzl",oParentObject)
return(i_retval)

define class tgsar_bzl as StdBatch
  * --- Local variables
  w_DXBTN = .f.
  w_LSCODLIS = space(5)
  w_PROG = .NULL.
  w_LSFLGCIC = space(1)
  * --- WorkFile variables
  LISTINI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_DXBTN = .F.
    this.w_LSCODLIS = ""
    * --- Se la variabile che riferisce all'ultimo form aperto � vuota esco
    if Type("g_oMenu.oKey")<>"U"
      * --- Lanciato tramite tasto destro 
      this.w_DXBTN = .T.
      this.w_LSCODLIS = Nvl(g_oMenu.oKey(1,3),"")
    else
      if isnull(i_curform)
        i_retcode = 'stop'
        return
      endif
      * --- Puntatore al nome del Cursore da cui e' stato lanciato
      cCurs = i_curform.z1.cCursor
      this.w_LSCODLIS = &cCurs..LSCODLIS
    endif
    if !EMPTY(NVL(this.w_LSCODLIS, " "))
      if isAHE()
        * --- Read from LISTINI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LISTINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2],.t.,this.LISTINI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LSFLGCIC"+;
            " from "+i_cTable+" LISTINI where ";
                +"LSCODLIS = "+cp_ToStrODBC(this.w_LSCODLIS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LSFLGCIC;
            from (i_cTable) where;
                LSCODLIS = this.w_LSCODLIS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LSFLGCIC = NVL(cp_ToDate(_read_.LSFLGCIC),cp_NullValue(_read_.LSFLGCIC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_LSFLGCIC="E"
          this.w_LSFLGCIC = "V"
        endif
        this.w_PROG = GSVE_ALI(this.w_LSFLGCIC)
      else
        this.w_PROG = GSAR_ALI()
      endif
    endif
    if this.w_DXBTN
      * --- Nel caso ho premuto tasto destro sul controll apro la anagrafica sul codice 
      *     presente nella variabile
      if g_oMenu.cBatchType$"AM"
        * --- Apri o Modifica
        this.w_PROG.ecpFilter()     
        this.w_PROG.w_LSCODLIS = this.w_LSCODLIS
        this.w_PROG.ecpSave()     
        if g_oMenu.cBatchType="M"
          * --- Modifica
          this.w_PROG.ecpEdit()     
        endif
      endif
      if g_oMenu.cBatchType="L"
        * --- Carica
        this.w_PROG.ecpLoad()     
      endif
    else
      if this.w_PROG.bSEC1
        this.w_PROG.w_LSCODLIS = this.w_LSCODLIS
        this.w_PROG.QueryKeySet("LSCODLIS='"+this.w_LSCODLIS+"'")     
        this.w_PROG.LoadRecWarn()     
      else
        L_OLDERR=ON("ERROR")
        ON ERROR L_FErr=.T.
        L_T=1/"A"
        ON ERROR &L_OLDERR
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='LISTINI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
