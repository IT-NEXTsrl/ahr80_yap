* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_kum                                                        *
*              Codici articoli - dati tecnici                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_44]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-15                                                      *
* Last revis.: 2010-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_kum",oParentObject))

* --- Class definition
define class tgsma_kum as StdForm
  Top    = 104
  Left   = 39

  * --- Standard Properties
  Width  = 473
  Height = 281
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-03-01"
  HelpContextID=202736791
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  _IDX = 0
  TIPICONF_IDX = 0
  UNIMIS_IDX = 0
  TIP_COLL_IDX = 0
  CON_COLL_IDX = 0
  cPrg = "gsma_kum"
  cComment = "Codici articoli - dati tecnici"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CACODART = space(10)
  w_CAPESNE3 = 0
  w_CAPESLO3 = 0
  w_CATIPCO3 = space(5)
  o_CATIPCO3 = space(5)
  w_CATPCON3 = space(3)
  o_CATPCON3 = space(3)
  w_COCOL3 = 0
  w_DESTCF = space(30)
  w_CAPZCON3 = 0
  w_CACOCOL3 = 0
  w_CADIMLU3 = 0
  w_CAUMVOL3 = space(3)
  w_CADIMLA3 = 0
  w_CADIMAL3 = 0
  w_VOLUME = 0
  w_CAUMDIM3 = space(3)
  w_CADESVO3 = space(15)
  w_CAFLCON3 = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_DESCOL = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kumPag1","gsma_kum",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCAPESNE3_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='TIPICONF'
    this.cWorkTables[2]='UNIMIS'
    this.cWorkTables[3]='TIP_COLL'
    this.cWorkTables[4]='CON_COLL'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CACODART=space(10)
      .w_CAPESNE3=0
      .w_CAPESLO3=0
      .w_CATIPCO3=space(5)
      .w_CATPCON3=space(3)
      .w_COCOL3=0
      .w_DESTCF=space(30)
      .w_CAPZCON3=0
      .w_CACOCOL3=0
      .w_CADIMLU3=0
      .w_CAUMVOL3=space(3)
      .w_CADIMLA3=0
      .w_CADIMAL3=0
      .w_VOLUME=0
      .w_CAUMDIM3=space(3)
      .w_CADESVO3=space(15)
      .w_CAFLCON3=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_DESCOL=space(35)
      .w_CACODART=oParentObject.w_CACODART
      .w_CAPESNE3=oParentObject.w_CAPESNE3
      .w_CAPESLO3=oParentObject.w_CAPESLO3
      .w_CATIPCO3=oParentObject.w_CATIPCO3
      .w_CATPCON3=oParentObject.w_CATPCON3
      .w_CAPZCON3=oParentObject.w_CAPZCON3
      .w_CACOCOL3=oParentObject.w_CACOCOL3
      .w_CADIMLU3=oParentObject.w_CADIMLU3
      .w_CAUMVOL3=oParentObject.w_CAUMVOL3
      .w_CADIMLA3=oParentObject.w_CADIMLA3
      .w_CADIMAL3=oParentObject.w_CADIMAL3
      .w_CAUMDIM3=oParentObject.w_CAUMDIM3
      .w_CADESVO3=oParentObject.w_CADESVO3
      .w_CAFLCON3=oParentObject.w_CAFLCON3
        .DoRTCalc(1,4,.f.)
        if not(empty(.w_CATIPCO3))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CATPCON3))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,11,.f.)
        if not(empty(.w_CAUMVOL3))
          .link_1_11('Full')
        endif
          .DoRTCalc(12,13,.f.)
        .w_VOLUME = cp_ROUND(.w_CADIMLU3*.w_CADIMLA3*.w_CADIMAL3,3)
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CAUMDIM3))
          .link_1_15('Full')
        endif
          .DoRTCalc(16,17,.f.)
        .w_OBTEST = i_datsys
    endwith
    this.DoRTCalc(19,20,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  proc SetStatus()
    if type('this.oParentObject.cFunction')='C'
      this.cFunction=this.oParentObject.cFunction
      if this.cFunction="Load"
        this.cFunction="Edit"
      endif
    endif
    DoDefault()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCAPESNE3_1_2.enabled = i_bVal
      .Page1.oPag.oCAPESLO3_1_3.enabled = i_bVal
      .Page1.oPag.oCATIPCO3_1_4.enabled = i_bVal
      .Page1.oPag.oCATPCON3_1_5.enabled = i_bVal
      .Page1.oPag.oCAPZCON3_1_8.enabled = i_bVal
      .Page1.oPag.oCACOCOL3_1_9.enabled = i_bVal
      .Page1.oPag.oCADIMLU3_1_10.enabled = i_bVal
      .Page1.oPag.oCAUMVOL3_1_11.enabled = i_bVal
      .Page1.oPag.oCADIMLA3_1_12.enabled = i_bVal
      .Page1.oPag.oCADIMAL3_1_13.enabled = i_bVal
      .Page1.oPag.oCAUMDIM3_1_15.enabled = i_bVal
      .Page1.oPag.oCADESVO3_1_16.enabled = i_bVal
      .Page1.oPag.oCAFLCON3_1_17.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CACODART=.w_CACODART
      .oParentObject.w_CAPESNE3=.w_CAPESNE3
      .oParentObject.w_CAPESLO3=.w_CAPESLO3
      .oParentObject.w_CATIPCO3=.w_CATIPCO3
      .oParentObject.w_CATPCON3=.w_CATPCON3
      .oParentObject.w_CAPZCON3=.w_CAPZCON3
      .oParentObject.w_CACOCOL3=.w_CACOCOL3
      .oParentObject.w_CADIMLU3=.w_CADIMLU3
      .oParentObject.w_CAUMVOL3=.w_CAUMVOL3
      .oParentObject.w_CADIMLA3=.w_CADIMLA3
      .oParentObject.w_CADIMAL3=.w_CADIMAL3
      .oParentObject.w_CAUMDIM3=.w_CAUMDIM3
      .oParentObject.w_CADESVO3=.w_CADESVO3
      .oParentObject.w_CAFLCON3=.w_CAFLCON3
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_CATIPCO3<>.w_CATIPCO3
          .link_1_5('Full')
        endif
        .DoRTCalc(6,8,.t.)
        if .o_CATPCON3<>.w_CATPCON3
            .w_CACOCOL3 = IIF(EMPTY(.w_CACOCOL3) OR .cfunction<>'Query',.w_COCOL3,.w_CACOCOL3)
        endif
        .DoRTCalc(10,13,.t.)
            .w_VOLUME = cp_ROUND(.w_CADIMLU3*.w_CADIMLA3*.w_CADIMAL3,3)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCATPCON3_1_5.enabled = this.oPgFrm.Page1.oPag.oCATPCON3_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCAPZCON3_1_8.enabled = this.oPgFrm.Page1.oPag.oCAPZCON3_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCAUMDIM3_1_15.enabled = this.oPgFrm.Page1.oPag.oCAUMDIM3_1_15.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCATPCON3_1_5.visible=!this.oPgFrm.Page1.oPag.oCATPCON3_1_5.mHide()
    this.oPgFrm.Page1.oPag.oDESTCF_1_7.visible=!this.oPgFrm.Page1.oPag.oDESTCF_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCAPZCON3_1_8.visible=!this.oPgFrm.Page1.oPag.oCAPZCON3_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCACOCOL3_1_9.visible=!this.oPgFrm.Page1.oPag.oCACOCOL3_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_25.visible=!this.oPgFrm.Page1.oPag.oStr_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CATIPCO3
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_lTable = "TIP_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2], .t., this.TIP_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATIPCO3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'TIP_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_CATIPCO3)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_CATIPCO3))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATIPCO3)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATIPCO3) and !this.bDontReportError
            deferred_cp_zoom('TIP_COLL','*','TCCODICE',cp_AbsName(oSource.parent,'oCATIPCO3_1_4'),i_cWhere,'GSAR_MTO',"Tipologie colli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATIPCO3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_CATIPCO3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CATIPCO3)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATIPCO3 = NVL(_Link_.TCCODICE,space(5))
      this.w_DESCOL = NVL(_Link_.TCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATIPCO3 = space(5)
      endif
      this.w_DESCOL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATIPCO3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATPCON3
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_COLL_IDX,3]
    i_lTable = "CON_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2], .t., this.CON_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATPCON3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'CON_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODCON like "+cp_ToStrODBC(trim(this.w_CATPCON3)+"%");
                   +" and TCCODICE="+cp_ToStrODBC(this.w_CATIPCO3);

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON,TCQTACON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE,TCCODCON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',this.w_CATIPCO3;
                     ,'TCCODCON',trim(this.w_CATPCON3))
          select TCCODICE,TCCODCON,TCDESCON,TCQTACON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE,TCCODCON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATPCON3)==trim(_Link_.TCCODCON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATPCON3) and !this.bDontReportError
            deferred_cp_zoom('CON_COLL','*','TCCODICE,TCCODCON',cp_AbsName(oSource.parent,'oCATPCON3_1_5'),i_cWhere,'GSAR_MTO',"Confezioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CATIPCO3<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON,TCQTACON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TCCODICE,TCCODCON,TCDESCON,TCQTACON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON,TCQTACON";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODCON="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TCCODICE="+cp_ToStrODBC(this.w_CATIPCO3);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1);
                       ,'TCCODCON',oSource.xKey(2))
            select TCCODICE,TCCODCON,TCDESCON,TCQTACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATPCON3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCCODCON,TCDESCON,TCQTACON";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODCON="+cp_ToStrODBC(this.w_CATPCON3);
                   +" and TCCODICE="+cp_ToStrODBC(this.w_CATIPCO3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_CATIPCO3;
                       ,'TCCODCON',this.w_CATPCON3)
            select TCCODICE,TCCODCON,TCDESCON,TCQTACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATPCON3 = NVL(_Link_.TCCODCON,space(3))
      this.w_DESTCF = NVL(_Link_.TCDESCON,space(30))
      this.w_COCOL3 = NVL(_Link_.TCQTACON,0)
    else
      if i_cCtrl<>'Load'
        this.w_CATPCON3 = space(3)
      endif
      this.w_DESTCF = space(30)
      this.w_COCOL3 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)+'\'+cp_ToStr(_Link_.TCCODCON,1)
      cp_ShowWarn(i_cKey,this.CON_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATPCON3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUMVOL3
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUMVOL3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_CAUMVOL3)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_CAUMVOL3))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUMVOL3)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUMVOL3) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oCAUMVOL3_1_11'),i_cWhere,'GSAR_AUM',"Unita di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUMVOL3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_CAUMVOL3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_CAUMVOL3)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUMVOL3 = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CAUMVOL3 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUMVOL3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUMDIM3
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUMDIM3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_CAUMDIM3)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_CAUMDIM3))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUMDIM3)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUMDIM3) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oCAUMDIM3_1_15'),i_cWhere,'GSAR_AUM',"Unita di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUMDIM3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_CAUMDIM3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_CAUMDIM3)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUMDIM3 = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CAUMDIM3 = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUMDIM3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCAPESNE3_1_2.value==this.w_CAPESNE3)
      this.oPgFrm.Page1.oPag.oCAPESNE3_1_2.value=this.w_CAPESNE3
    endif
    if not(this.oPgFrm.Page1.oPag.oCAPESLO3_1_3.value==this.w_CAPESLO3)
      this.oPgFrm.Page1.oPag.oCAPESLO3_1_3.value=this.w_CAPESLO3
    endif
    if not(this.oPgFrm.Page1.oPag.oCATIPCO3_1_4.value==this.w_CATIPCO3)
      this.oPgFrm.Page1.oPag.oCATIPCO3_1_4.value=this.w_CATIPCO3
    endif
    if not(this.oPgFrm.Page1.oPag.oCATPCON3_1_5.value==this.w_CATPCON3)
      this.oPgFrm.Page1.oPag.oCATPCON3_1_5.value=this.w_CATPCON3
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTCF_1_7.value==this.w_DESTCF)
      this.oPgFrm.Page1.oPag.oDESTCF_1_7.value=this.w_DESTCF
    endif
    if not(this.oPgFrm.Page1.oPag.oCAPZCON3_1_8.value==this.w_CAPZCON3)
      this.oPgFrm.Page1.oPag.oCAPZCON3_1_8.value=this.w_CAPZCON3
    endif
    if not(this.oPgFrm.Page1.oPag.oCACOCOL3_1_9.value==this.w_CACOCOL3)
      this.oPgFrm.Page1.oPag.oCACOCOL3_1_9.value=this.w_CACOCOL3
    endif
    if not(this.oPgFrm.Page1.oPag.oCADIMLU3_1_10.value==this.w_CADIMLU3)
      this.oPgFrm.Page1.oPag.oCADIMLU3_1_10.value=this.w_CADIMLU3
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUMVOL3_1_11.value==this.w_CAUMVOL3)
      this.oPgFrm.Page1.oPag.oCAUMVOL3_1_11.value=this.w_CAUMVOL3
    endif
    if not(this.oPgFrm.Page1.oPag.oCADIMLA3_1_12.value==this.w_CADIMLA3)
      this.oPgFrm.Page1.oPag.oCADIMLA3_1_12.value=this.w_CADIMLA3
    endif
    if not(this.oPgFrm.Page1.oPag.oCADIMAL3_1_13.value==this.w_CADIMAL3)
      this.oPgFrm.Page1.oPag.oCADIMAL3_1_13.value=this.w_CADIMAL3
    endif
    if not(this.oPgFrm.Page1.oPag.oVOLUME_1_14.value==this.w_VOLUME)
      this.oPgFrm.Page1.oPag.oVOLUME_1_14.value=this.w_VOLUME
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUMDIM3_1_15.value==this.w_CAUMDIM3)
      this.oPgFrm.Page1.oPag.oCAUMDIM3_1_15.value=this.w_CAUMDIM3
    endif
    if not(this.oPgFrm.Page1.oPag.oCADESVO3_1_16.value==this.w_CADESVO3)
      this.oPgFrm.Page1.oPag.oCADESVO3_1_16.value=this.w_CADESVO3
    endif
    if not(this.oPgFrm.Page1.oPag.oCAFLCON3_1_17.RadioValue()==this.w_CAFLCON3)
      this.oPgFrm.Page1.oPag.oCAFLCON3_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOL_1_34.value==this.w_DESCOL)
      this.oPgFrm.Page1.oPag.oDESCOL_1_34.value=this.w_DESCOL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CATPCON3))  and not(EMPTY(.w_CATIPCO3))  and (NOT EMPTY(.w_CATIPCO3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCATPCON3_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CATPCON3)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAPZCON3))  and not(EMPTY(.w_CATPCON3))  and (NOT EMPTY(.w_CATIPCO3))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAPZCON3_1_8.SetFocus()
            i_bnoObbl = !empty(.w_CAPZCON3)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsma_kum
       if NOT EMPTY(.w_CATPCON3) AND EMPTY(.w_CACOCOL3)
         i_bRes = .f.
      	 i_bnoChk = .f.		
      	 i_cErrorMsg = Ah_MsgFormat("Inserire numero confezioni per collo")
       endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CATIPCO3 = this.w_CATIPCO3
    this.o_CATPCON3 = this.w_CATPCON3
    return

enddefine

* --- Define pages as container
define class tgsma_kumPag1 as StdContainer
  Width  = 469
  height = 281
  stdWidth  = 469
  stdheight = 281
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCAPESNE3_1_2 as StdField with uid="MENORAZFNI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CAPESNE3", cQueryName = "CAPESNE3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Massa netta unitaria in kg.",;
    HelpContextID = 7353767,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=108, Top=16, cSayPict='"99999.999"', cGetPict='"99999.999"'

  add object oCAPESLO3_1_3 as StdField with uid="WXFTVNPVWE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CAPESLO3", cQueryName = "CAPESLO3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Massa lorda unitaria in kg.",;
    HelpContextID = 40908199,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=318, Top=16, cSayPict='"99999.999"', cGetPict='"99999.999"'

  add object oCATIPCO3_1_4 as StdField with uid="LQTNDUNXQM",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CATIPCO3", cQueryName = "CATIPCO3",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 194770343,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=108, Top=45, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_CATIPCO3"

  func oCATIPCO3_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
      if .not. empty(.w_CATPCON3)
        bRes2=.link_1_5('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCATIPCO3_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATIPCO3_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_COLL','*','TCCODICE',cp_AbsName(this.parent,'oCATIPCO3_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Tipologie colli",'',this.parent.oContained
  endproc
  proc oCATIPCO3_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_CATIPCO3
     i_obj.ecpSave()
  endproc

  add object oCATPCON3_1_5 as StdField with uid="SZCLWHZGSO",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CATPCON3", cQueryName = "CATPCON3",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Tipo confezione",;
    HelpContextID = 261818969,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=108, Top=74, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CON_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_CATIPCO3", oKey_2_1="TCCODCON", oKey_2_2="this.w_CATPCON3"

  func oCATPCON3_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CATIPCO3))
    endwith
   endif
  endfunc

  func oCATPCON3_1_5.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CATIPCO3))
    endwith
  endfunc

  func oCATPCON3_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATPCON3_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATPCON3_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CON_COLL_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TCCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CATIPCO3)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TCCODICE="+cp_ToStr(this.Parent.oContained.w_CATIPCO3)
    endif
    do cp_zoom with 'CON_COLL','*','TCCODICE,TCCODCON',cp_AbsName(this.parent,'oCATPCON3_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Confezioni",'',this.parent.oContained
  endproc
  proc oCATPCON3_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TCCODICE=w_CATIPCO3
     i_obj.w_TCCODCON=this.parent.oContained.w_CATPCON3
     i_obj.ecpSave()
  endproc

  add object oDESTCF_1_7 as StdField with uid="MEXTSELTKW",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESTCF", cQueryName = "DESTCF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 157352394,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=169, Top=74, InputMask=replicate('X',30)

  func oDESTCF_1_7.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CATIPCO3))
    endwith
  endfunc

  add object oCAPZCON3_1_8 as StdField with uid="HAVHKDQDLD",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CAPZCON3", cQueryName = "CAPZCON3",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero pezzi per confezione",;
    HelpContextID = 262457945,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=108, Top=103, cSayPict="v_PQ(11)", cGetPict="v_GQ(11)"

  func oCAPZCON3_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CATIPCO3))
    endwith
   endif
  endfunc

  func oCAPZCON3_1_8.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CATPCON3))
    endwith
  endfunc

  add object oCACOCOL3_1_9 as StdField with uid="EMXRIZEHNF",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CACOCOL3", cQueryName = "CACOCOL3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di confezioni per collo",;
    HelpContextID = 261683801,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=355, Top=103, cSayPict='"999999"', cGetPict='"999999"'

  func oCACOCOL3_1_9.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CATPCON3))
    endwith
  endfunc

  add object oCADIMLU3_1_10 as StdField with uid="GXSONNNDNF",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CADIMLU3", cQueryName = "CADIMLU3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione di ingombro lineare in lunghezza della confezione",;
    HelpContextID = 46986663,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=108, Top=132, cSayPict='"9999.9"', cGetPict='"9999.9"'

  add object oCAUMVOL3_1_11 as StdField with uid="GDSAMVXRKW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CAUMVOL3", cQueryName = "CAUMVOL3",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura riferita al volume",;
    HelpContextID = 13113945,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=383, Top=190, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_CAUMVOL3"

  func oCAUMVOL3_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUMVOL3_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUMVOL3_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oCAUMVOL3_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'',this.parent.oContained
  endproc
  proc oCAUMVOL3_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_CAUMVOL3
     i_obj.ecpSave()
  endproc

  add object oCADIMLA3_1_12 as StdField with uid="JXRGSWZBNR",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CADIMLA3", cQueryName = "CADIMLA3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione di ingombro lineare in larghezza della confezione",;
    HelpContextID = 46986663,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=108, Top=161, cSayPict='"9999.9"', cGetPict='"9999.9"'

  add object oCADIMAL3_1_13 as StdField with uid="ZIEKFUYLOE",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CADIMAL3", cQueryName = "CADIMAL3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Dimensione di ingombro lineare in altezza della confezione",;
    HelpContextID = 36899417,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=108, Top=190, cSayPict='"9999.9"', cGetPict='"9999.9"'

  add object oVOLUME_1_14 as StdField with uid="GUYPRCSISR",rtseq=14,rtrep=.f.,;
    cFormVar = "w_VOLUME", cQueryName = "VOLUME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Volume della confezione",;
    HelpContextID = 163604138,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=193, Top=190

  add object oCAUMDIM3_1_15 as StdField with uid="DZCQYUIQXX",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CAUMDIM3", cQueryName = "CAUMDIM3",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura dell'ingombro",;
    HelpContextID = 162011737,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=383, Top=132, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_CAUMDIM3"

  func oCAUMDIM3_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VOLUME<>0)
    endwith
   endif
  endfunc

  func oCAUMDIM3_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUMDIM3_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUMDIM3_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oCAUMDIM3_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'',this.parent.oContained
  endproc
  proc oCAUMDIM3_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_CAUMDIM3
     i_obj.ecpSave()
  endproc

  add object oCADESVO3_1_16 as StdField with uid="QEYKTZZGLQ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CADESVO3", cQueryName = "CADESVO3",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Volume unitario del prodotto",;
    HelpContextID = 141620647,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=108, Top=219, InputMask=replicate('X',15)


  add object oCAFLCON3_1_17 as StdCombo with uid="EUNVWOHBTJ",rtseq=17,rtrep=.f.,left=108,top=248,width=83,height=21;
    , ToolTipText = "Tipologia articolo in funzione del contributo ambientale (CONAI)";
    , HelpContextID = 261499481;
    , cFormVar="w_CAFLCON3",RowSource=""+"Prodotto,"+"Utilizzato,"+"Altro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCAFLCON3_1_17.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'U',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oCAFLCON3_1_17.GetRadio()
    this.Parent.oContained.w_CAFLCON3 = this.RadioValue()
    return .t.
  endfunc

  func oCAFLCON3_1_17.SetRadio()
    this.Parent.oContained.w_CAFLCON3=trim(this.Parent.oContained.w_CAFLCON3)
    this.value = ;
      iif(this.Parent.oContained.w_CAFLCON3=='P',1,;
      iif(this.Parent.oContained.w_CAFLCON3=='U',2,;
      iif(this.Parent.oContained.w_CAFLCON3=='A',3,;
      0)))
  endfunc

  add object oDESCOL_1_34 as StdField with uid="WDRSDIUUIZ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCOL", cQueryName = "DESCOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 45220298,;
   bGlobalFont=.t.,;
    Height=21, Width=243, Left=177, Top=45, InputMask=replicate('X',35)

  add object oStr_1_18 as StdString with uid="TCQJRXCLPU",Visible=.t., Left=5, Top=132,;
    Alignment=1, Width=101, Height=15,;
    Caption="Lunghezza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="NBBDIAJRCP",Visible=.t., Left=5, Top=161,;
    Alignment=1, Width=101, Height=15,;
    Caption="Larghezza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="PZVGGGOYRY",Visible=.t., Left=5, Top=190,;
    Alignment=1, Width=101, Height=15,;
    Caption="Altezza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="FFHKBLSFRK",Visible=.t., Left=5, Top=16,;
    Alignment=1, Width=101, Height=15,;
    Caption="Peso netto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="MLZOSQZJDD",Visible=.t., Left=235, Top=16,;
    Alignment=1, Width=81, Height=15,;
    Caption="Peso lordo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="RXAWJCTWXW",Visible=.t., Left=197, Top=16,;
    Alignment=0, Width=48, Height=15,;
    Caption="in kg."  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="WJYNKCLLCI",Visible=.t., Left=408, Top=16,;
    Alignment=0, Width=55, Height=15,;
    Caption="in kg."  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="YVDCAVVJFV",Visible=.t., Left=5, Top=74,;
    Alignment=1, Width=101, Height=15,;
    Caption="Confezione:"  ;
  , bGlobalFont=.t.

  func oStr_1_25.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CATIPCO3))
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="VJFSOXMWSO",Visible=.t., Left=5, Top=103,;
    Alignment=1, Width=101, Height=15,;
    Caption="Pezzi:"  ;
  , bGlobalFont=.t.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CATPCON3))
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="GBEULBKBUW",Visible=.t., Left=5, Top=219,;
    Alignment=1, Width=101, Height=15,;
    Caption="Volume:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="ECVJWUVTLA",Visible=.t., Left=175, Top=190,;
    Alignment=2, Width=15, Height=15,;
    Caption="="  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="CGDAHUNSQP",Visible=.t., Left=3, Top=248,;
    Alignment=1, Width=103, Height=15,;
    Caption="CONAI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="PVBURHZTKM",Visible=.t., Left=214, Top=103,;
    Alignment=1, Width=139, Height=15,;
    Caption="Confezioni per collo:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CATPCON3))
    endwith
  endfunc

  add object oStr_1_33 as StdString with uid="FKPCMWCIDC",Visible=.t., Left=5, Top=45,;
    Alignment=1, Width=101, Height=18,;
    Caption="Tipo collo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="YAYTNIFIRM",Visible=.t., Left=243, Top=132,;
    Alignment=1, Width=139, Height=15,;
    Caption="U.M. dimensioni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="SZETFRKGMU",Visible=.t., Left=303, Top=190,;
    Alignment=1, Width=79, Height=15,;
    Caption="U.M. volume:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_kum','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
