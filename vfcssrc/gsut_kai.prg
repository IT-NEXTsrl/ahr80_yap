* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kai                                                        *
*              Valorizzazione attributi                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-09-01                                                      *
* Last revis.: 2014-09-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kai",oParentObject))

* --- Class definition
define class tgsut_kai as StdForm
  Top    = 2
  Left   = 1

  * --- Standard Properties
  Width  = 806
  Height = 576
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-05"
  HelpContextID=132727657
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  PROMCLAS_IDX = 0
  cPrg = "gsut_kai"
  cComment = "Valorizzazione attributi"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ISALT = .F.
  w_SELCODCLA = space(15)
  w_SELCODCLA = space(15)
  w_DESCLA = space(50)
  w_CMODALL = space(1)
  w_ARCHIVIO = space(20)
  w_GFKEYINDIC = space(15)
  w_GFPATHFILE = space(60)
  w_PERCARC = space(150)
  w_AGGIUNGI = space(1)
  w_MODIFICA = space(1)
  w_ELIMINA = space(1)
  w_NRIGACLAS = space(1)
  w_IGN_ERR = space(1)
  w_ZoomGF = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kaiPag1","gsut_kai",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELCODCLA_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomGF = this.oPgFrm.Pages(1).oPag.ZoomGF
    DoDefault()
    proc Destroy()
      this.w_ZoomGF = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PROMCLAS'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSUT_BAI(this,"A")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ISALT=.f.
      .w_SELCODCLA=space(15)
      .w_SELCODCLA=space(15)
      .w_DESCLA=space(50)
      .w_CMODALL=space(1)
      .w_ARCHIVIO=space(20)
      .w_GFKEYINDIC=space(15)
      .w_GFPATHFILE=space(60)
      .w_PERCARC=space(150)
      .w_AGGIUNGI=space(1)
      .w_MODIFICA=space(1)
      .w_ELIMINA=space(1)
      .w_NRIGACLAS=space(1)
      .w_IGN_ERR=space(1)
        .w_ISALT = Isalt()
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_SELCODCLA))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_SELCODCLA))
          .link_1_3('Full')
        endif
      .oPgFrm.Page1.oPag.ZoomGF.Calculate()
          .DoRTCalc(4,6,.f.)
        .w_GFKEYINDIC = .w_ZoomGF.GetVar("GFKEYINDIC")
        .w_GFPATHFILE = .w_ZoomGF.GetVar("GFPATHFILE")
        .w_PERCARC = iif(isnull(.w_GFPATHFILE) or empty(.w_GFPATHFILE),' ', iif(empty(justdrive(.w_GFPATHFILE)) AND !LEFT(.w_GFPATHFILE,2)=='\\', addbs(SYS(5)+SYS(2003))+alltrim(.w_GFPATHFILE), .w_GFPATHFILE))
        .w_AGGIUNGI = 'N'
        .w_MODIFICA = 'N'
        .w_ELIMINA = 'N'
        .w_NRIGACLAS = 'N'
        .w_IGN_ERR = 'N'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomGF.Calculate()
        .DoRTCalc(1,6,.t.)
            .w_GFKEYINDIC = .w_ZoomGF.GetVar("GFKEYINDIC")
            .w_GFPATHFILE = .w_ZoomGF.GetVar("GFPATHFILE")
            .w_PERCARC = iif(isnull(.w_GFPATHFILE) or empty(.w_GFPATHFILE),' ', iif(empty(justdrive(.w_GFPATHFILE)) AND !LEFT(.w_GFPATHFILE,2)=='\\', addbs(SYS(5)+SYS(2003))+alltrim(.w_GFPATHFILE), .w_GFPATHFILE))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomGF.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oSELCODCLA_1_2.visible=!this.oPgFrm.Page1.oPag.oSELCODCLA_1_2.mHide()
    this.oPgFrm.Page1.oPag.oSELCODCLA_1_3.visible=!this.oPgFrm.Page1.oPag.oSELCODCLA_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_5.visible=!this.oPgFrm.Page1.oPag.oStr_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_20.visible=!this.oPgFrm.Page1.oPag.oBtn_1_20.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomGF.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SELCODCLA
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SELCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_ACL',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_SELCODCLA)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_SELCODCLA))
          select CDCODCLA,CDDESCLA,CDMODALL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SELCODCLA)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SELCODCLA) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oSELCODCLA_1_2'),i_cWhere,'GSUT_ACL',"Classi librerie allegati",'gsut0kai.PROMCLAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDMODALL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SELCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDMODALL";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_SELCODCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_SELCODCLA)
            select CDCODCLA,CDDESCLA,CDMODALL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SELCODCLA = NVL(_Link_.CDCODCLA,space(15))
      this.w_DESCLA = NVL(_Link_.CDDESCLA,space(50))
      this.w_CMODALL = NVL(_Link_.CDMODALL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SELCODCLA = space(15)
      endif
      this.w_DESCLA = space(50)
      this.w_CMODALL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CMODALL$'FL' and not empty(.w_ARCHIVIO) and ( SUBSTR( CHKPECLA( .w_SELCODCLA, i_CODUTE), 1, 1 ) = 'S' )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe libreria allegati non valida o non si possiedono le autorizzazioni per visualizzare documenti per la classe selezionata")
        endif
        this.w_SELCODCLA = space(15)
        this.w_DESCLA = space(50)
        this.w_CMODALL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SELCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SELCODCLA
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SELCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MCD',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_SELCODCLA)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDRIFTAB,CDMODALL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_SELCODCLA))
          select CDCODCLA,CDDESCLA,CDRIFTAB,CDMODALL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SELCODCLA)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CDDESCLA like "+cp_ToStrODBC(trim(this.w_SELCODCLA)+"%");

            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDRIFTAB,CDMODALL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CDDESCLA like "+cp_ToStr(trim(this.w_SELCODCLA)+"%");

            select CDCODCLA,CDDESCLA,CDRIFTAB,CDMODALL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_SELCODCLA) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oSELCODCLA_1_3'),i_cWhere,'GSUT_MCD',"Classi documentali",'gsut0kai.PROMCLAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDRIFTAB,CDMODALL";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDDESCLA,CDRIFTAB,CDMODALL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SELCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDDESCLA,CDRIFTAB,CDMODALL";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_SELCODCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_SELCODCLA)
            select CDCODCLA,CDDESCLA,CDRIFTAB,CDMODALL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SELCODCLA = NVL(_Link_.CDCODCLA,space(15))
      this.w_DESCLA = NVL(_Link_.CDDESCLA,space(50))
      this.w_ARCHIVIO = NVL(_Link_.CDRIFTAB,space(20))
      this.w_CMODALL = NVL(_Link_.CDMODALL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SELCODCLA = space(15)
      endif
      this.w_DESCLA = space(50)
      this.w_ARCHIVIO = space(20)
      this.w_CMODALL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CMODALL$'FL' and not empty(.w_ARCHIVIO) and ( SUBSTR( CHKPECLA( .w_SELCODCLA, i_CODUTE), 1, 1 ) = 'S' )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale non valida o non si possiedono le autorizzazioni per visualizzare documenti per la classe selezionata")
        endif
        this.w_SELCODCLA = space(15)
        this.w_DESCLA = space(50)
        this.w_ARCHIVIO = space(20)
        this.w_CMODALL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SELCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELCODCLA_1_2.value==this.w_SELCODCLA)
      this.oPgFrm.Page1.oPag.oSELCODCLA_1_2.value=this.w_SELCODCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oSELCODCLA_1_3.value==this.w_SELCODCLA)
      this.oPgFrm.Page1.oPag.oSELCODCLA_1_3.value=this.w_SELCODCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLA_1_4.value==this.w_DESCLA)
      this.oPgFrm.Page1.oPag.oDESCLA_1_4.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oCMODALL_1_9.RadioValue()==this.w_CMODALL)
      this.oPgFrm.Page1.oPag.oCMODALL_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oARCHIVIO_1_10.value==this.w_ARCHIVIO)
      this.oPgFrm.Page1.oPag.oARCHIVIO_1_10.value=this.w_ARCHIVIO
    endif
    if not(this.oPgFrm.Page1.oPag.oAGGIUNGI_1_21.RadioValue()==this.w_AGGIUNGI)
      this.oPgFrm.Page1.oPag.oAGGIUNGI_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMODIFICA_1_22.RadioValue()==this.w_MODIFICA)
      this.oPgFrm.Page1.oPag.oMODIFICA_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELIMINA_1_23.RadioValue()==this.w_ELIMINA)
      this.oPgFrm.Page1.oPag.oELIMINA_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNRIGACLAS_1_26.RadioValue()==this.w_NRIGACLAS)
      this.oPgFrm.Page1.oPag.oNRIGACLAS_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oIGN_ERR_1_27.RadioValue()==this.w_IGN_ERR)
      this.oPgFrm.Page1.oPag.oIGN_ERR_1_27.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_SELCODCLA)) or not(.w_CMODALL$'FL' and not empty(.w_ARCHIVIO) and ( SUBSTR( CHKPECLA( .w_SELCODCLA, i_CODUTE), 1, 1 ) = 'S' )))  and not(g_DOCM = 'S' and ! .w_ISALT)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSELCODCLA_1_2.SetFocus()
            i_bnoObbl = !empty(.w_SELCODCLA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe libreria allegati non valida o non si possiedono le autorizzazioni per visualizzare documenti per la classe selezionata")
          case   ((empty(.w_SELCODCLA)) or not(.w_CMODALL$'FL' and not empty(.w_ARCHIVIO) and ( SUBSTR( CHKPECLA( .w_SELCODCLA, i_CODUTE), 1, 1 ) = 'S' )))  and not(g_DOCM <> 'S' or .w_ISALT)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSELCODCLA_1_3.SetFocus()
            i_bnoObbl = !empty(.w_SELCODCLA)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe documentale non valida o non si possiedono le autorizzazioni per visualizzare documenti per la classe selezionata")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kaiPag1 as StdContainer
  Width  = 802
  height = 577
  stdWidth  = 802
  stdheight = 577
  resizeXpos=669
  resizeYpos=427
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSELCODCLA_1_2 as StdField with uid="PKOJJUZXPM",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SELCODCLA", cQueryName = "SELCODCLA",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe libreria allegati non valida o non si possiedono le autorizzazioni per visualizzare documenti per la classe selezionata",;
    ToolTipText = "Codice classe libreria allegati",;
    HelpContextID = 21941122,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=159, Top=5, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSUT_ACL", oKey_1_1="CDCODCLA", oKey_1_2="this.w_SELCODCLA"

  func oSELCODCLA_1_2.mHide()
    with this.Parent.oContained
      return (g_DOCM = 'S' and ! .w_ISALT)
    endwith
  endfunc

  func oSELCODCLA_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSELCODCLA_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSELCODCLA_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oSELCODCLA_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_ACL',"Classi librerie allegati",'gsut0kai.PROMCLAS_VZM',this.parent.oContained
  endproc
  proc oSELCODCLA_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSUT_ACL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_SELCODCLA
     i_obj.ecpSave()
  endproc

  add object oSELCODCLA_1_3 as StdField with uid="TBHDXNLUCM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SELCODCLA", cQueryName = "SELCODCLA",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe documentale non valida o non si possiedono le autorizzazioni per visualizzare documenti per la classe selezionata",;
    ToolTipText = "Codice classe documentale",;
    HelpContextID = 21941122,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=159, Top=5, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSUT_MCD", oKey_1_1="CDCODCLA", oKey_1_2="this.w_SELCODCLA"

  func oSELCODCLA_1_3.mHide()
    with this.Parent.oContained
      return (g_DOCM <> 'S' or .w_ISALT)
    endwith
  endfunc

  func oSELCODCLA_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oSELCODCLA_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSELCODCLA_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oSELCODCLA_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MCD',"Classi documentali",'gsut0kai.PROMCLAS_VZM',this.parent.oContained
  endproc
  proc oSELCODCLA_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_SELCODCLA
     i_obj.ecpSave()
  endproc

  add object oDESCLA_1_4 as StdField with uid="NONZZUTMSV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 236926518,;
   bGlobalFont=.t.,;
    Height=21, Width=445, Left=298, Top=5, InputMask=replicate('X',50)


  add object oCMODALL_1_9 as StdCombo with uid="MUHACDCHGE",rtseq=5,rtrep=.f.,left=536,top=30,width=110,height=22, enabled=.f.;
    , HelpContextID = 141557286;
    , cFormVar="w_CMODALL",RowSource=""+"Copia file,"+"Collegamento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCMODALL_1_9.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'L',;
    space(1))))
  endfunc
  func oCMODALL_1_9.GetRadio()
    this.Parent.oContained.w_CMODALL = this.RadioValue()
    return .t.
  endfunc

  func oCMODALL_1_9.SetRadio()
    this.Parent.oContained.w_CMODALL=trim(this.Parent.oContained.w_CMODALL)
    this.value = ;
      iif(this.Parent.oContained.w_CMODALL=='F',1,;
      iif(this.Parent.oContained.w_CMODALL=='L',2,;
      0))
  endfunc

  add object oARCHIVIO_1_10 as StdField with uid="XRGARBCISD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ARCHIVIO", cQueryName = "ARCHIVIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 218938539,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=159, Top=33, InputMask=replicate('X',20)


  add object oBtn_1_11 as StdButton with uid="YQWTLIBGOF",left=746, top=5, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la ricerca";
    , HelpContextID = 44194582;
    , caption='\<Ricerca';
  , bGlobalFont=.t.

    proc oBtn_1_11.Click()
      this.parent.oContained.NotifyEvent("Ricerca")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomGF as cp_szoombox with uid="MEJCTPXJSO",left=5, top=53, width=789,height=463,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSUT_KAI",bOptions=.f.,bAdvOptions=.f.,bQueryOnLoad=.f.,bNoZoomGridShape=.f.,cZoomOnZoom="",bQueryOnDblClick=.t.,cTable="PROMINDI",bReadOnly=.t.,cMenuFile="",;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 45269990


  add object oBtn_1_16 as StdButton with uid="GQMUPTTTCY",left=12, top=526, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutte le righe";
    , HelpContextID = 132257498;
    , caption='\<Seleziona';
  , bGlobalFont=.t.

    proc oBtn_1_16.Click()
      with this.Parent.oContained
        .w_ZoomGF.CheckAll()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="ZCYHLDNLVF",left=64, top=526, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutte le righe";
    , HelpContextID = 132229066;
    , caption='\<Deselez.';
  , bGlobalFont=.t.

    proc oBtn_1_17.Click()
      with this.Parent.oContained
        .w_ZoomGF.UnCheckAll()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="GUQBLQMZBT",left=116, top=526, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezione";
    , HelpContextID = 124677754;
    , caption='\<Inv. Sel.';
  , bGlobalFont=.t.

    proc oBtn_1_18.Click()
      with this.Parent.oContained
        .w_ZoomGF.InvertSelection()
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="BGOOKNIKBY",left=185, top=526, width=48,height=45,;
    CpPicture="BMP\apertura.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare l'indice del documento archiviato";
    , HelpContextID = 62287750;
    , caption='Vi\<sualizza';
  , bGlobalFont=.t.

    proc oBtn_1_19.Click()
      with this.Parent.oContained
        opengest("A","GSUT_AID", "IDSERIAL", .w_GFKEYINDIC)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(nvl(.w_GFKEYINDIC,"")))
      endwith
    endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="NBZVJTSQXB",left=237, top=526, width=48,height=45,;
    CpPicture="bmp\FOLDER.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aprire la cartella in cui si trova il documento archiviato";
    , HelpContextID = 226985656;
    , caption='\<Apri cart.';
  , bGlobalFont=.t.

    proc oBtn_1_20.Click()
      with this.Parent.oContained
        stapdf(.w_PERCARC)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_PERCARC) or g_DMIP='S')
     endwith
    endif
  endfunc

  add object oAGGIUNGI_1_21 as StdCheck with uid="YBLZCFTDZG",rtseq=10,rtrep=.f.,left=309, top=518, caption="Aggiungi attributi mancanti",;
    ToolTipText = "Aggiunge all'indice gli attributi mancanti rispetto alla classe",;
    HelpContextID = 72058801,;
    cFormVar="w_AGGIUNGI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGGIUNGI_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGIUNGI_1_21.GetRadio()
    this.Parent.oContained.w_AGGIUNGI = this.RadioValue()
    return .t.
  endfunc

  func oAGGIUNGI_1_21.SetRadio()
    this.Parent.oContained.w_AGGIUNGI=trim(this.Parent.oContained.w_AGGIUNGI)
    this.value = ;
      iif(this.Parent.oContained.w_AGGIUNGI=='S',1,;
      0)
  endfunc

  add object oMODIFICA_1_22 as StdCheck with uid="PGXKHVMBDF",rtseq=11,rtrep=.f.,left=309, top=536, caption="Ricalcola attributi presenti",;
    ToolTipText = "Ricalcola gli attributi presenti sia nell'indice che nella classe",;
    HelpContextID = 96751879,;
    cFormVar="w_MODIFICA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMODIFICA_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMODIFICA_1_22.GetRadio()
    this.Parent.oContained.w_MODIFICA = this.RadioValue()
    return .t.
  endfunc

  func oMODIFICA_1_22.SetRadio()
    this.Parent.oContained.w_MODIFICA=trim(this.Parent.oContained.w_MODIFICA)
    this.value = ;
      iif(this.Parent.oContained.w_MODIFICA=='S',1,;
      0)
  endfunc

  add object oELIMINA_1_23 as StdCheck with uid="JZXAGBVYAJ",rtseq=12,rtrep=.f.,left=309, top=554, caption="Elimina attributi non presenti",;
    ToolTipText = "Elimina dall'indice gli attributi non presenti nella classe",;
    HelpContextID = 184065350,;
    cFormVar="w_ELIMINA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELIMINA_1_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oELIMINA_1_23.GetRadio()
    this.Parent.oContained.w_ELIMINA = this.RadioValue()
    return .t.
  endfunc

  func oELIMINA_1_23.SetRadio()
    this.Parent.oContained.w_ELIMINA=trim(this.Parent.oContained.w_ELIMINA)
    this.value = ;
      iif(this.Parent.oContained.w_ELIMINA=='S',1,;
      0)
  endfunc


  add object oBtn_1_24 as StdButton with uid="ZMIZZNTUDJ",left=692, top=526, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 132698906;
    , Caption='\<Ok';
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSUT_BAI(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ("S" $ .w_AGGIUNGI+.w_ELIMINA+.w_MODIFICA)
      endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="JWIBHYOYEX",left=746, top=526, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 125410234;
    , Caption='\<Esci';
  , bGlobalFont=.t.

    proc oBtn_1_25.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNRIGACLAS_1_26 as StdCheck with uid="GZIHJRCXJU",rtseq=13,rtrep=.f.,left=515, top=518, caption="Numero riga da classe",;
    ToolTipText = "Il numero riga sull'indice � importato dal numero riga della classe ",;
    HelpContextID = 9262777,;
    cFormVar="w_NRIGACLAS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNRIGACLAS_1_26.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oNRIGACLAS_1_26.GetRadio()
    this.Parent.oContained.w_NRIGACLAS = this.RadioValue()
    return .t.
  endfunc

  func oNRIGACLAS_1_26.SetRadio()
    this.Parent.oContained.w_NRIGACLAS=trim(this.Parent.oContained.w_NRIGACLAS)
    this.value = ;
      iif(this.Parent.oContained.w_NRIGACLAS=='S',1,;
      0)
  endfunc

  add object oIGN_ERR_1_27 as StdCheck with uid="GQOVMVYYRT",rtseq=14,rtrep=.f.,left=515, top=554, caption="Ignora errori elaborazione",;
    ToolTipText = "In caso di errori di elaborazione di un attributo, aggiorna comunque gli altri attributi",;
    HelpContextID = 248178822,;
    cFormVar="w_IGN_ERR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oIGN_ERR_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oIGN_ERR_1_27.GetRadio()
    this.Parent.oContained.w_IGN_ERR = this.RadioValue()
    return .t.
  endfunc

  func oIGN_ERR_1_27.SetRadio()
    this.Parent.oContained.w_IGN_ERR=trim(this.Parent.oContained.w_IGN_ERR)
    this.value = ;
      iif(this.Parent.oContained.w_IGN_ERR=='S',1,;
      0)
  endfunc

  add object oStr_1_5 as StdString with uid="CBOUTPXTJD",Visible=.t., Left=11, Top=6,;
    Alignment=1, Width=145, Height=18,;
    Caption="Classe libreria allegati:"  ;
  , bGlobalFont=.t.

  func oStr_1_5.mHide()
    with this.Parent.oContained
      return (g_DOCM = 'S')
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="BWPJHUPIDO",Visible=.t., Left=23, Top=6,;
    Alignment=1, Width=133, Height=18,;
    Caption="Classe documentale:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (g_DOCM <> 'S')
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="DLPCTKTMCK",Visible=.t., Left=27, Top=35,;
    Alignment=1, Width=129, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="SNJWZYEFOB",Visible=.t., Left=376, Top=34,;
    Alignment=1, Width=152, Height=18,;
    Caption="Modalit� archiviazione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kai','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
