* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bsi                                                        *
*              Chiusura situaz. patrimoniale                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_46]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-06-24                                                      *
* Last revis.: 2008-01-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bsi",oParentObject)
return(i_retval)

define class tgscg_bsi as StdBatch
  * --- Local variables
  w_STALIG = ctod("  /  /  ")
  w_DATBLO = ctod("  /  /  ")
  w_PNSERIAL = space(10)
  w_PNDATREG = ctod("  /  /  ")
  w_PNCODESE = space(4)
  w_PNCODUTE = 0
  w_PNNUMRER = 0
  w_PNCODVAL = space(3)
  w_PNVALNAZ = space(3)
  w_PNCODCAU = space(5)
  w_PNTIPREG = space(2)
  w_PNCOMPET = space(4)
  w_UTCC = 0
  w_PNCAOVAL = 0
  w_PNDESSUP = space(50)
  w_UTDV = ctod("  /  /  ")
  w_OKREG = .f.
  w_INIREG = .f.
  w_UTCV = 0
  w_CONSUP = space(15)
  w_SLDARPER = 0
  w_UTDC = ctod("  /  /  ")
  w_SLCODICE = space(15)
  w_SLAVEPRO = 0
  w_PNTIPCON = space(1)
  w_SLTIPCON = space(1)
  w_SLDARPRO = 0
  w_PNCAURIG = space(5)
  w_PNIMPDAR = 0
  w_PNFLZERO = space(1)
  w_SLAVEPER = 0
  w_APPO = 0
  w_TOTRIG = 0
  w_PNFLSALF = space(1)
  w_PNCODCON = space(15)
  w_PNDESRIG = space(50)
  w_SALDO = 0
  w_PNFLSALI = space(1)
  w_PNIMPAVE = 0
  w_CPROWORD = 0
  w_CPROWNUM = 0
  w_PNFLSALD = space(1)
  w_AGGIORNA = .f.
  w_OKECON = .f.
  w_PNPRG = space(8)
  w_MESS = space(50)
  w_SEZBIL = space(1)
  w_DECTOT = 0
  w_CCDESSUP = space(254)
  w_CCDESRIG = space(254)
  * --- WorkFile variables
  ESERCIZI_idx=0
  VALUTE_idx=0
  PNT_MAST_idx=0
  SALDICON_idx=0
  CONTI_idx=0
  PNT_DETT_idx=0
  AZIENDA_idx=0
  MASTRI_idx=0
  CAU_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura Situazione Patrimoniale (da GSCG_KSI)
    * --- Variabili Locali
    * --- Controllo se non vi siano altre registrazioni con la causale scelta per la chiusura
    DIMENSION ARPARAM[12,2]
    * --- nell'esercizio - La Solita Query � utilizzata nell'Chiusura Econmica di Esercizio
    * --- Select from GSCG_BCE
    do vq_exec with 'GSCG_BCE',this,'_Curs_GSCG_BCE','',.f.,.t.
    if used('_Curs_GSCG_BCE')
      select _Curs_GSCG_BCE
      locate for 1=1
      do while not(eof())
      if NOT EMPTY(NVL(_Curs_GSCG_BCE.PNSERIAL,""))
        this.w_PNSERIAL = _Curs_GSCG_BCE.PNSERIAL
        * --- Select from PNT_DETT
        i_nConn=i_TableProp[this.PNT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2],.t.,this.PNT_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select PNTIPCON,PNCODCON  from "+i_cTable+" PNT_DETT ";
              +" where PNSERIAL="+cp_ToStrODBC(this.w_PNSERIAL)+"";
               ,"_Curs_PNT_DETT")
        else
          select PNTIPCON,PNCODCON from (i_cTable);
           where PNSERIAL=this.w_PNSERIAL;
            into cursor _Curs_PNT_DETT
        endif
        if used('_Curs_PNT_DETT')
          select _Curs_PNT_DETT
          locate for 1=1
          do while not(eof())
          this.w_PNTIPCON = NVL(_Curs_PNT_DETT.PNTIPCON, " ")
          this.w_PNCODCON = NVL(_Curs_PNT_DETT.PNCODCON," ")
          if (NOT EMPTY(this.w_PNTIPCON)) AND (NOT EMPTY(this.w_PNCODCON))
            * --- Verifica se Conto Economico  (SEZBIL C o R)
            this.w_CONSUP = SPACE(15)
            this.w_SEZBIL = " "
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANCONSUP"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANCONSUP;
                from (i_cTable) where;
                    ANTIPCON = this.w_PNTIPCON;
                    and ANCODICE = this.w_PNCODCON;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Read from MASTRI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.MASTRI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MCSEZBIL"+;
                " from "+i_cTable+" MASTRI where ";
                    +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MCSEZBIL;
                from (i_cTable) where;
                    MCCODICE = this.w_CONSUP;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_SEZBIL = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if this.w_PNTIPCON $ "CF" OR (this.w_PNTIPCON="G" AND this.w_SEZBIL $ "APO")
              ah_ErrorMsg("In prima nota, per l'esercizio da chiudere, sono gi� presenti registrazioni di chiusura sit. patrimoniale%0� necessario annullarle prima di procedere",,"")
              i_retcode = 'stop'
              return
            endif
          endif
            select _Curs_PNT_DETT
            continue
          enddo
          use
        endif
      endif
        select _Curs_GSCG_BCE
        continue
      enddo
      use
    endif
    * --- Blocco Prima Nota
    * --- Try
    local bErr_03D592A0
    bErr_03D592A0=bTrsErr
    this.Try_03D592A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("%1",,"", i_ErrMsg)
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_03D592A0
    * --- End
    * --- Aggiorna Variabili Primanota
    this.w_PNCAOVAL = GETCAM(g_PERVAL, I_DATSYS)
    this.w_PNCODESE = this.oParentObject.w_CODESE
    this.w_PNCOMPET = this.oParentObject.w_CODESE
    this.w_PNDESSUP = this.oParentObject.w_DESSUP
    this.w_PNDATREG = this.oParentObject.w_DATREG
    this.w_PNCODCAU = this.oParentObject.w_CODCAU
    * --- Read from CAU_CONT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAU_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCDESSUP,CCDESRIG"+;
        " from "+i_cTable+" CAU_CONT where ";
            +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCDESSUP,CCDESRIG;
        from (i_cTable) where;
            CCCODICE = this.w_PNCODCAU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CCDESSUP = NVL(cp_ToDate(_read_.CCDESSUP),cp_NullValue(_read_.CCDESSUP))
      this.w_CCDESRIG = NVL(cp_ToDate(_read_.CCDESRIG),cp_NullValue(_read_.CCDESRIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_UTCC = i_CODUTE
    this.w_UTDC = SetInfoDate( g_CALUTD )
    this.w_UTCV = 0
    this.w_UTDV = cp_CharToDate("  -  -    ")
    this.w_PNVALNAZ = this.oParentObject.w_VALNAZ
    this.w_PNTIPREG = "N"
    this.w_PNCODVAL = this.oParentObject.w_VALNAZ
    this.w_PNFLSALD = "+"
    this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
    this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
    * --- Testa se effettua registrazioni
    this.w_OKREG = .F.
    this.w_INIREG = .F.
    this.w_TOTRIG = 0
    this.w_CPROWNUM = 0
    this.w_CPROWORD = 0
    this.w_OKECON = .F.
    * --- Try
    local bErr_03D62F60
    bErr_03D62F60=bTrsErr
    this.Try_03D62F60()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      if empty(i_errmsg)
        ah_ErrorMsg("Operazione abortita",,"")
      else
        ah_ErrorMsg("%1",,"", I_errmsg )
      endif
    endif
    bTrsErr=bTrsErr or bErr_03D62F60
    * --- End
    * --- Sblocca Primanota
    * --- Try
    local bErr_03CE9880
    bErr_03CE9880=bTrsErr
    this.Try_03CE9880()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      ah_ErrorMsg("Impossibile sbloccare la prima nota - semaforo bollati in dati azienda%0Sbloccarlo manualmente",,"")
    endif
    bTrsErr=bTrsErr or bErr_03CE9880
    * --- End
  endproc
  proc Try_03D592A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZSTALIG,AZDATBLO"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZSTALIG,AZDATBLO;
        from (i_cTable) where;
            AZCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case NOT this.oParentObject.w_DATREG > this.w_STALIG
        * --- Raise
        i_Error=ah_Msgformat("Data registrazione inferiore o uguale a ultima stampa libro giornale")
        return
      case Not Empty(CHKCONS("P",this.oParentObject.w_DATREG,"B","N"))
        * --- Raise
        i_Error=CHKCONS("P",this.oParentObject.w_DATREG,"B","N")+"!"
        return
      case this.oParentObject.w_DATREG <= this.w_DATBLO AND NOT EMPTY(this.w_DATBLO)
        this.w_MESS = ah_Msgformat("Prima nota bloccata - semaforo bollati in dati azienda. Operazione annullata")
        * --- Raise
        i_Error=this.w_MESS
        return
    endcase
    * --- Blocca Primanota
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_INIESE),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = this.oParentObject.w_INIESE;
          &i_ccchkf. ;
       where;
          AZCODAZI = i_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03D62F60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inizia la transazione...
    * --- begin transaction
    cp_BeginTrs()
    this.w_PNCAOVAL = GETCAM(this.oParentObject.w_VALNAZ, I_DATSYS)
    this.w_DECTOT = GETVALUT(this.oParentObject.w_VALNAZ, "VADECTOT")
    * --- Legge Saldi Conti Economici <> 0
    * --- Select from SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2],.t.,this.SALDICON_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" SALDICON ";
          +" where SLCODESE="+cp_ToStrODBC(this.oParentObject.w_CODESE)+"";
          +" order by SLTIPCON,SLCODICE";
           ,"_Curs_SALDICON")
    else
      select * from (i_cTable);
       where SLCODESE=this.oParentObject.w_CODESE;
       order by SLTIPCON,SLCODICE;
        into cursor _Curs_SALDICON
    endif
    if used('_Curs_SALDICON')
      select _Curs_SALDICON
      locate for 1=1
      do while not(eof())
      this.w_AGGIORNA = .F.
      this.w_PNTIPCON = NVL(_Curs_SALDICON.SLTIPCON, " ")
      this.w_PNCODCON = NVL(_Curs_SALDICON.SLCODICE, SPACE(15))
      this.w_CONSUP = SPACE(15)
      this.w_SEZBIL = " "
      if (NOT EMPTY(this.w_PNTIPCON)) AND (NOT EMPTY(this.w_PNCODCON))
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCONSUP"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCONSUP;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCON;
                and ANCODICE = this.w_PNCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CONSUP = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_PNTIPCON="G"
          * --- Read from MASTRI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MASTRI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MCSEZBIL"+;
              " from "+i_cTable+" MASTRI where ";
                  +"MCCODICE = "+cp_ToStrODBC(this.w_CONSUP);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MCSEZBIL;
              from (i_cTable) where;
                  MCCODICE = this.w_CONSUP;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SEZBIL = NVL(cp_ToDate(_read_.MCSEZBIL),cp_NullValue(_read_.MCSEZBIL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_SEZBIL $ "CR"
            * --- Conto Economico controlla se e' stata fatta la Chiusura
            this.w_OKECON = IIF(NVL(SLDARFIN,0)<>0 OR NVL(SLAVEFIN,0)<>0, .T., this.w_OKECON)
          endif
        endif
        if (NVL(SLDARPRO,0)+NVL(SLDARPER,0))-(NVL(SLAVEPRO,0)+NVL(SLAVEPER,0))<>0
          * --- Questo filtro va fatto dopo altrimenti potrebbero non venire considerati i Conti Economici per controllonon
          do case
            case this.w_PNTIPCON="C"
              * --- Cliente
              ah_Msg("Elabora cliente: %1",.T.,.F.,.F., this.w_PNCODCON )
              this.w_AGGIORNA = .T.
            case this.w_PNTIPCON="F"
              * --- Fornitore
              ah_Msg("Elabora fornitore: %1",.T.,.F.,.F., this.w_PNCODCON )
              this.w_AGGIORNA = .T.
            case this.w_PNTIPCON="G" AND this.w_SEZBIL $ "APO"
              * --- Conto Patrimoniale o Ordine  (SEZBIL A o P o O)
              ah_Msg("Elabora conto: %1",.T.,.F.,.F., this.w_PNCODCON )
              this.w_AGGIORNA = .T.
          endcase
        endif
      endif
      if this.w_AGGIORNA=.T.
        this.w_APPO = cp_ROUND((NVL(SLDARPRO,0)+NVL(SLDARPER,0))-(NVL(SLAVEPRO,0)+NVL(SLAVEPER,0)),this.w_DECTOT)
        this.w_TOTRIG = this.w_TOTRIG + this.w_APPO
        this.w_PNIMPDAR = IIF(this.w_APPO<0, ABS(this.w_APPO), 0)
        this.w_PNIMPAVE = IIF(this.w_APPO>0, this.w_APPO, 0)
        this.w_PNFLSALF = "+"
        this.w_PNDESRIG = this.oParentObject.w_DESSUP
        this.w_PNCAURIG = this.oParentObject.w_CODCAU
        if Empty(this.w_PNDESSUP) and Not Empty(this.w_CCDESSUP)
          * --- Array elenco parametri per descrizioni di riga e testata
           
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]="" 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]="" 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]="" 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]="" 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]="" 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=""
        endif
        if Not Empty(this.w_CCDESSUP)
          this.w_PNDESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
        endif
        * --- Scrive Primanota (Anagrafica)
        if this.w_INIREG=.F.
          * --- Calcola PNSERIAL e PNNUMRER
          this.w_PNSERIAL = SPACE(10)
          this.w_PNNUMRER = 0
          this.w_CPROWNUM = 0
          this.w_CPROWORD = 0
          i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
          cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
          cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
          * --- Try
          local bErr_03CCD5A0
          bErr_03CCD5A0=bTrsErr
          this.Try_03CCD5A0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Raise
            i_Error=ah_Msgformat("Impossibile inserire documento in prima nota")
            return
          endif
          bTrsErr=bTrsErr or bErr_03CCD5A0
          * --- End
          * --- Write into PNT_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.PNT_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"PNDATREG ="+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
            +",PNNUMRER ="+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
            +",PNCODCAU ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
            +",PNTIPREG ="+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
            +",PNCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PNT_MAST','PNCODVAL');
            +",PNPRG ="+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
            +",PNCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
            +",PNDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
            +",PNCOMPET ="+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
            +",PNVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
            +",UTCC ="+cp_NullLink(cp_ToStrODBC(this.w_UTCC),'PNT_MAST','UTCC');
            +",UTCV ="+cp_NullLink(cp_ToStrODBC(this.w_UTCV),'PNT_MAST','UTCV');
            +",UTDC ="+cp_NullLink(cp_ToStrODBC(this.w_UTDC),'PNT_MAST','UTDC');
            +",UTDV ="+cp_NullLink(cp_ToStrODBC(this.w_UTDV),'PNT_MAST','UTDV');
            +",PNCODESE ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
            +",PNCODUTE ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
            +",PNFLPROV ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_MAST','PNFLPROV');
                +i_ccchkf ;
            +" where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                   )
          else
            update (i_cTable) set;
                PNDATREG = this.w_PNDATREG;
                ,PNNUMRER = this.w_PNNUMRER;
                ,PNCODCAU = this.w_PNCODCAU;
                ,PNTIPREG = this.w_PNTIPREG;
                ,PNCODVAL = this.w_PNCODVAL;
                ,PNPRG = this.w_PNPRG;
                ,PNCAOVAL = this.w_PNCAOVAL;
                ,PNDESSUP = this.w_PNDESSUP;
                ,PNCOMPET = this.w_PNCOMPET;
                ,PNVALNAZ = this.w_PNVALNAZ;
                ,UTCC = this.w_UTCC;
                ,UTCV = this.w_UTCV;
                ,UTDC = this.w_UTDC;
                ,UTDV = this.w_UTDV;
                ,PNCODESE = this.w_PNCODESE;
                ,PNCODUTE = this.w_PNCODUTE;
                ,PNFLPROV = "N";
                &i_ccchkf. ;
             where;
                PNSERIAL = this.w_PNSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_INIREG = .T.
          this.w_OKREG = .T.
        endif
        * --- Scrive Reg.Contabili (Movimentazione)
        if Not Empty(this.w_CCDESRIG)
          this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
        endif
        this.w_CPROWNUM = this.w_CPROWNUM + 1
        this.w_CPROWORD = this.w_CPROWORD + 10
        this.w_PNFLZERO = IIF(this.w_PNIMPAVE=0 AND this.w_PNIMPDAR=0, "S", " ")
        * --- Try
        local bErr_03CC2160
        bErr_03CC2160=bTrsErr
        this.Try_03CC2160()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Raise
          i_Error=ah_Msgformat("Impossibile inserire movimento in prima nota")
          return
        endif
        bTrsErr=bTrsErr or bErr_03CC2160
        * --- End
        * --- Aggiorna Saldo
        * --- Try
        local bErr_03EF2A20
        bErr_03EF2A20=bTrsErr
        this.Try_03EF2A20()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_03EF2A20
        * --- End
        * --- Write into SALDICON
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
          +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
          +",SLAVEFIN =SLAVEFIN+ "+cp_ToStrODBC(this.w_PNIMPDAR);
          +",SLDARFIN =SLDARFIN+ "+cp_ToStrODBC(this.w_PNIMPAVE);
              +i_ccchkf ;
          +" where ";
              +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
              +" and SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
                 )
        else
          update (i_cTable) set;
              SLDARPER = SLDARPER + this.w_PNIMPDAR;
              ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
              ,SLAVEFIN = SLAVEFIN + this.w_PNIMPDAR;
              ,SLDARFIN = SLDARFIN + this.w_PNIMPAVE;
              &i_ccchkf. ;
           where;
              SLTIPCON = this.w_PNTIPCON;
              and SLCODICE = this.w_PNCODCON;
              and SLCODESE = this.oParentObject.w_CODESE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      if this.w_CPROWORD>1000
        * --- Scrive Movimento di Chiusura (se w_TOTRIG<>0)
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Quindi scrive una nuova Registrazione
        this.w_INIREG = .F.
        this.w_CPROWNUM = 0
        this.w_CPROWORD = 0
      endif
        select _Curs_SALDICON
        continue
      enddo
      use
    endif
    * --- Determina Utile/Perdita di Esercizio
    * --- Scrive Movimento di Chiusura (se w_TOTRIG<>0)
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_OKECON=.F.
      * --- Non trovati Saldi Finali nei Conti Economici....
      if NOT ah_YesNo("ATTENZIONE:%0Potrebbe non essere stata eseguita precedentemente la chiusura%0del conto economico; confermo comunque?")
        this.w_MESS = ah_Msgformat("Operazione annullata")
        * --- Raise
        i_Error=this.w_MESS
        return
      endif
    endif
    * --- Conferma la Transazione
    * --- commit
    cp_EndTrs(.t.)
    ah_Msg("Chiusura sit. patrimoniale completata",.T.)
    return
  proc Try_03CE9880()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = i_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_03CCD5A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL)
      insert into (i_cTable) (PNSERIAL &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03CC2160()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLSALF"+",PNDESRIG"+",PNCAURIG"+",PNFLSALD"+",PNFLZERO"+",PNFLSALI"+",PNFLPART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALF),'PNT_DETT','PNFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAURIG),'PNT_DETT','PNCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLZERO),'PNT_DETT','PNFLZERO');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
      +","+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLPART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_PNTIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLSALF',this.w_PNFLSALF,'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCAURIG,'PNFLSALD',this.w_PNFLSALD,'PNFLZERO',this.w_PNFLZERO)
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLSALF,PNDESRIG,PNCAURIG,PNFLSALD,PNFLZERO,PNFLSALI,PNFLPART &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNIMPDAR;
           ,this.w_PNIMPAVE;
           ,this.w_PNFLSALF;
           ,this.w_PNDESRIG;
           ,this.w_PNCAURIG;
           ,this.w_PNFLSALD;
           ,this.w_PNFLZERO;
           ," ";
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03EF2A20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.oParentObject.w_CODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.oParentObject.w_CODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Riga Finale di Chiusura (ATTENZIONE: Conto di Tipo Transitorio, non aggiorna il Saldo Finale)
    if this.w_TOTRIG <> 0
      ah_Msg("Scrivo riga chiusura registrazione",.T.)
      this.w_PNTIPCON = this.oParentObject.w_TIPCON
      this.w_PNCODCON = this.oParentObject.w_CODCON
      this.w_PNIMPDAR = IIF(this.w_TOTRIG>0, this.w_TOTRIG, 0)
      this.w_PNIMPAVE = IIF(this.w_TOTRIG<0, ABS(this.w_TOTRIG), 0)
      * --- Scrive Reg.Contabili (Movimentazione)
      this.w_CPROWNUM = this.w_CPROWNUM + 1
      this.w_CPROWORD = this.w_CPROWORD + 10
      this.w_PNFLZERO = IIF(this.w_PNIMPAVE=0 AND this.w_PNIMPDAR=0, "S", " ")
      if Not Empty(this.w_CCDESRIG)
        this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
      endif
      * --- Try
      local bErr_037C22E8
      bErr_037C22E8=bTrsErr
      this.Try_037C22E8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Raise
        i_Error=ah_Msgformat("Impossibile inserire movimento in prima nota")
        return
      endif
      bTrsErr=bTrsErr or bErr_037C22E8
      * --- End
      * --- Write into PNT_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PNTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
        +",PNCODCON ="+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
        +",PNIMPDAR ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
        +",PNIMPAVE ="+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
        +",PNFLSALF ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
        +",PNDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
        +",PNCAURIG ="+cp_NullLink(cp_ToStrODBC(this.w_PNCAURIG),'PNT_DETT','PNCAURIG');
        +",CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
        +",PNFLSALD ="+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
        +",PNFLZERO ="+cp_NullLink(cp_ToStrODBC(this.w_PNFLZERO),'PNT_DETT','PNFLZERO');
        +",PNFLSALI ="+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
        +",PNFLPART ="+cp_NullLink(cp_ToStrODBC("N"),'PNT_DETT','PNFLPART');
            +i_ccchkf ;
        +" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
               )
      else
        update (i_cTable) set;
            PNTIPCON = this.w_PNTIPCON;
            ,PNCODCON = this.w_PNCODCON;
            ,PNIMPDAR = this.w_PNIMPDAR;
            ,PNIMPAVE = this.w_PNIMPAVE;
            ,PNFLSALF = " ";
            ,PNDESRIG = this.w_PNDESRIG;
            ,PNCAURIG = this.w_PNCAURIG;
            ,CPROWORD = this.w_CPROWORD;
            ,PNFLSALD = this.w_PNFLSALD;
            ,PNFLZERO = this.w_PNFLZERO;
            ,PNFLSALI = " ";
            ,PNFLPART = "N";
            &i_ccchkf. ;
         where;
            PNSERIAL = this.w_PNSERIAL;
            and CPROWNUM = this.w_CPROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorna Conto Profitti e Perdite
      * --- Try
      local bErr_03857A68
      bErr_03857A68=bTrsErr
      this.Try_03857A68()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_03857A68
      * --- End
      * --- Write into SALDICON
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICON_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
        +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
            +i_ccchkf ;
        +" where ";
            +"SLTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCON);
            +" and SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODCON);
            +" and SLCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
               )
      else
        update (i_cTable) set;
            SLDARPER = SLDARPER + this.w_PNIMPDAR;
            ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
            &i_ccchkf. ;
         where;
            SLTIPCON = this.oParentObject.w_TIPCON;
            and SLCODICE = this.oParentObject.w_CODCON;
            and SLCODESE = this.oParentObject.w_CODESE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_TOTRIG = 0
      WAIT CLEAR
    endif
  endproc
  proc Try_037C22E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM)
      insert into (i_cTable) (PNSERIAL,CPROWNUM &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_03857A68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.oParentObject.w_TIPCON,'SLCODICE',this.oParentObject.w_CODCON,'SLCODESE',this.oParentObject.w_CODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.oParentObject.w_TIPCON;
           ,this.oParentObject.w_CODCON;
           ,this.oParentObject.w_CODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='PNT_MAST'
    this.cWorkTables[4]='SALDICON'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='PNT_DETT'
    this.cWorkTables[7]='AZIENDA'
    this.cWorkTables[8]='MASTRI'
    this.cWorkTables[9]='CAU_CONT'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_GSCG_BCE')
      use in _Curs_GSCG_BCE
    endif
    if used('_Curs_PNT_DETT')
      use in _Curs_PNT_DETT
    endif
    if used('_Curs_SALDICON')
      use in _Curs_SALDICON
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
