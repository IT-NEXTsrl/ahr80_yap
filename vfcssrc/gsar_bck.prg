* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bck                                                        *
*              Esegue controlli                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-02-06                                                      *
* Last revis.: 2008-02-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bck",oParentObject,m.pParam)
return(i_retval)

define class tgsar_bck as StdBatch
  * --- Local variables
  pParam = space(1)
  ShowMess = .f.
  w_oPART = .NULL.
  w_oMESS = .NULL.
  w_ObjMCR = .NULL.
  * --- WorkFile variables
  COLCRONO_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- pParam = C :   Controlli finali in Colonne cronologico
    do case
      case this.pParam = "C"
        this.ShowMess = .F.
        this.w_oMESS=createobject("AH_MESSAGE")
        this.w_oPART = this.w_oMESS.AddMsgPartNL( "Attenzione, conto utilizzato nelle seguenti colonne:" )
        this.w_oPART = this.w_oMESS.AddMsgPartNL( "" )
        this.w_ObjMCR = this.oParentObject
        * --- Select from COLCRONO
        i_nConn=i_TableProp[this.COLCRONO_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.COLCRONO_idx,2],.t.,this.COLCRONO_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CCCODCON,CCCODICE,CCDESCRI  from "+i_cTable+" COLCRONO ";
              +" where CCCODCON="+cp_ToStrODBC(this.w_ObjMCR.w_CCCODCON)+"";
               ,"_Curs_COLCRONO")
        else
          select CCCODCON,CCCODICE,CCDESCRI from (i_cTable);
           where CCCODCON=this.w_ObjMCR.w_CCCODCON;
            into cursor _Curs_COLCRONO
        endif
        if used('_Curs_COLCRONO')
          select _Curs_COLCRONO
          locate for 1=1
          do while not(eof())
          this.w_oPART = this.w_oMESS.AddMsgPartNL(_Curs_COLCRONO.CCCODICE+alltrim(_Curs_COLCRONO.CCDESCRI))
          this.ShowMess = .T.
            select _Curs_COLCRONO
            continue
          enddo
          use
        endif
        if this.ShowMess
          this.w_oMESS.AH_ERRORMSG("!")     
          this.w_ObjMCR.w_CCCODCON = SPACE(15)
          this.w_ObjMCR.w_DESCON = SPACE(40)
        endif
    endcase
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='COLCRONO'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_COLCRONO')
      use in _Curs_COLCRONO
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
