* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_ba2                                                        *
*              Gestione maschera acconto IVA                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_311]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-21                                                      *
* Last revis.: 2015-12-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_ba2",oParentObject,m.pTipo)
return(i_retval)

define class tgscg_ba2 as StdBatch
  * --- Local variables
  pTipo = space(10)
  w_OGGETTO = .NULL.
  w_FLPROR = space(1)
  w_PERPRO = 0
  w_IVACRE = 0
  w_KEYATT = space(5)
  w_TOTIVD = 0
  w_ANNO = space(4)
  w_NUMPER1 = 0
  w_AZIENDA = space(5)
  w_INDAZI = space(25)
  w_LOCAZI = space(35)
  w_CAPAZI = space(5)
  w_PROAZI = space(2)
  w_PIVAZI = space(12)
  w_COFAZI = space(16)
  w_PRPARI = 0
  w_INTLIG = space(1)
  w_PREFIS = space(20)
  w_PAGINE = 0
  w_IMPVAL = space(3)
  w_TIPOPE = space(1)
  w_FLPROR = space(1)
  w_FLAGRI = space(1)
  w_PERPRO = 0
  w_VIMPOR6 = 0
  w_TOTALE = 0
  w_DECTOT = 0
  w_RDESABI = space(80)
  w_RDESFIL = space(40)
  w_RINDIRI = space(50)
  w_RCAP = space(5)
  w_RDESBAN = space(35)
  w_RCODABI = space(5)
  w_RCODCAB = space(5)
  w_CODBAN = space(15)
  w_CONCES = space(3)
  w_DATVER = ctod("  /  /  ")
  w_PRPARI = 0
  w_INTLIG = space(1)
  w_PREFIS = space(20)
  w_PAGINE = 0
  * --- WorkFile variables
  ATTIMAST_idx=0
  PRO_RATA_idx=0
  VOCIIVA_idx=0
  AZIENDA_idx=0
  VALUTE_idx=0
  COC_MAST_idx=0
  COD_ABI_idx=0
  COD_CAB_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Maschera Iva Periodica (DA GSCG_SIM (Parametro: D, L), GSCG_SDP (Parametro S), GSCG_SD2 (Parametro L) e GSCG_BDI (Parametro B))
    * --- ATTENZIONE: Le maschere GSCG_SD2 e GSCG_SIM vengono a loro volta lanciate da GSCG_BDI
    do case
      case this.pTipo $ "BL"
        if this.oParentObject.w_TIPSTA="S"
          this.oParentObject.oParentObject.w_STORICO="S"
          i_retcode = 'stop'
          return
        else
          if this.oParentObject.w_ACCONTO<>0 AND this.oParentObject.w_ACCONTO<this.oParentObject.w_MACIVA1
            ah_ErrorMsg("Importo acconto IVA inferiore al versamento minimo",,"")
            this.oParentObject.oParentObject.w_OKELAB="F"
          else
            this.oParentObject.oParentObject.w_OKELAB="T"
          endif
        endif
        WITH this.oParentObject
        this.w_PAGINE = .oParentObject.w_PAGINE
        this.w_PRPARI = .oParentObject.w_PRPARI
        this.w_INTLIG = .oParentObject.w_INTLIG
        this.w_PREFIS = .oParentObject.w_PREFIS
        this.w_ANNO = .oParentObject.w_ANNO
        this.w_FLPROR = .oParentObject.w_FLPROR
        this.w_FLAGRI = .oParentObject.w_FLAGRI
        this.w_PERPRO = .oParentObject.w_PERPRO
        this.w_VIMPOR6 = .oParentObject.w_VIMPOR6
        this.w_CODBAN = .oParentObject.w_CODBAN1
        this.w_CONCES = .oParentObject.w_RCONCES
        this.w_DATVER = .oParentObject.w_RDATVER
        this.w_RDESABI = .oParentObject.w_RDESABI
        this.w_RDESFIL = .oParentObject.w_RDESFIL
        this.w_RINDIRI = .oParentObject.w_RINDIRI
        this.w_RCAP = .oParentObject.w_RCAP
        this.w_RDESBAN = .oParentObject.w_RDESBAN
        this.w_RCODABI = .oParentObject.w_RCODABI
        this.w_RCODCAB = .oParentObject.w_RCODCAB
        ENDWITH
        this.w_AZIENDA = i_CODAZI
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(this.w_AZIENDA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI;
            from (i_cTable) where;
                AZCODAZI = this.w_AZIENDA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDAZI = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
          this.w_LOCAZI = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
          this.w_CAPAZI = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
          this.w_PROAZI = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
          this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
          this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- L_Stampato viene messo a 1 dai Report
        L_STAMPATO=0
        * --- Passo le variabili presenti sulla maschera ai vari report di stampa.
        L_DICHGRU1=" "
        L_VPIMPOR1=0
        L_VPIMPOR2=0
        L_VPCESINT= 0
        L_VPACQINT= 0
        L_VPIMPON3= 0
        L_VPIMPOS3= 0
        L_VPIMPOR5= 0
        L_VPIMPOR6C= 0
        L_VPIMPOR6=0
        L_VPIMPO14=0
        L_VPOPNOIM=0
        L_VPPAR74C4=0
        L_VPVARIMP=0
        L_VPVERNEF=0
        L_VPCORTER=0
        L_VPACQBEA=0
        L_IMPVAL=g_PERVAL
        L_VPCODFIS=" "
        L_VPOPMEDI=0
        L_VPCODCAR=" "
        L_VPCRERIM=0
        L_VPCREUTI= 0
        L_VPEURO19=0
        L_VPEURO17=0
        L_ANNO=this.w_ANNO
        L_VPIMPO7A= this.oParentObject.w_VPIMPO7A
        L_VPIMPO7B= this.oParentObject.w_VPIMPO7B
        L_VPIMPO7C= this.oParentObject.w_VPIMPO7C
        L_VPIMPOR8= this.oParentObject.w_VIMPOR8
        L_VPIMPOR9= this.oParentObject.w_VIMPOR9
        L_VPIMPO10=this.oParentObject.w_VIMPO10
        L_VPIMPO11=this.oParentObject.w_VIMPO11
        L_VPIMPO12=this.oParentObject.w_VIMPO12
        L_VPIMPO13=this.oParentObject.w_VIMPO13
        L_VPIMPO15= this.oParentObject.w_VIMPO15
        L_VPIMPO16= iif(this.oParentObject.w_ACCONTO<this.oParentObject.w_MACIVA1,0,this.oParentObject.w_VIMPO16)
        L_VPIMPVER=this.oParentObject.w_ACCONTO
        L_PERIODO=this.oParentObject.w_NUMPER
        L_TIPLIQ=this.oParentObject.w_MTIPLIQ
        * --- PER NUMERAZIONE PAGINE IN TESTATA
        L_PAGINE=0
        L_PRPARI=this.w_PRPARI
        L_INTLIG=this.w_INTLIG
        L_PREFIS=this.w_PREFIS
        L_INDAZI=this.w_INDAZI
        L_LOCAZI=this.w_LOCAZI
        L_CAPAZI=this.w_CAPAZI
        L_PROAZI=this.w_PROAZI
        L_COFAZI=this.w_COFAZI
        L_PIVAZI=this.w_PIVAZI
        L_FLAGRI=this.w_FLAGRI
        L_FLPROR=this.w_FLPROR
        L_PERPRO=this.w_PERPRO
        L_VPIMPOR6= this.w_VIMPOR6
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(L_IMPVAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT;
            from (i_cTable) where;
                VACODVAL = L_IMPVAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        L_ABI=this.w_RCODABI
        L_CAB=this.w_RCODCAB
        L_DESBAN=this.w_RDESBAN
        L_DESABI=this.w_RDESABI
        L_INDIRI=this.w_RINDIRI
        L_CAP=this.w_RCAP
        L_DESFIL=this.w_RDESFIL
        L_DATVERS=this.w_DATVER
        L_CONCES=this.w_CONCES
        if this.pTipo = "L"
          L_NOSPLIT=This.oparentobject.oparentobject.w_NOSPLIT
        endif
        this.w_PAGINE = 0
        * --- Ciclo sul cursore per avere il totale crediti al netto del proprata arrotondato per ogni attivit�
        SELECT CODATT,SUM(TOTIVD) AS TOTIVD, MIN(PERPRO) AS PERPRO FROM LIQIVA;
         WHERE TIPREG="A" GROUP BY CODATT INTO CURSOR TOTALI
        SELECT TOTALI
        GO TOP
        SCAN
        this.w_TOTALE = this.w_TOTALE+cp_ROUND(((TOTIVD*PERPRO)/100),this.w_DECTOT)
        ENDSCAN
        SELECT LIQIVA
        GO TOP
        * --- Report di Stampa
        * --- Inserisco riga vuota con ORDINE = 2 per problemi di stampa. Raggruppamento su Ordine in pagina nuova stampa i totali.
        *     ORDINE = 1 le normali registraizoni Iva.
        *     '{' in TIPREG per avere questa riga in fondo al cursore poich� TIPREG pu� assumere 'Z'
        SELECT * , 1 AS ORDINE, this.w_TOTALE AS TOTALE FROM LIQIVA INTO CURSOR LIQIVA
        WRCURSOR("LIQIVA")
        SELECT LIQIVA
        INSERT INTO LIQIVA;
        (CODATT, DESCRI,TIPOPE, TIPREG, NUMREG, CODIVA, DESIVA, ;
        TOTIMP, TOTIVD, TOTIVI, TOTMAC, PERMAC, ;
        FLAGRI, PERCOM, TIPAGR, PERPRO, FLPROR, ORDINE, TOTALE) ;
        VALUES ;
        (SPACE(5), " ", " ", "{", 0, " " , " ", ;
        0, 0, 0, 0, 0, ;
        " ", 0, " ", 0, " ", 2, this.w_TOTALE) 
        SELECT LIQIVA
        GO TOP
        * --- Se 2002 o piu' Attivita' e non riepilogativa passa direttamente alla Liquidazione
        Select * From LIQIVA Into Cursor __Tmp__ ORDER BY 19, 1, 4, 3 NoFilter
        Cp_ChPrn("QUERY\ACCIVA.FRX", " ", this)
        this.w_PAGINE = L_PAGINE+this.w_PRPARI
        WITH this.oParentObject
        .oParentObject.w_PAGINE=THIS.w_PAGINE
        ENDWITH
      case this.pTipo="_BEFOREVP11"
        * --- Cambio sfondo al credito utilizzato
        this.w_OGGETTO = This.oParentObject.GETCTRL("w_CRERES")
        this.w_OGGETTO.DisabledBackColor = 11454589
        this.w_OGGETTO.DisabledForeColor = 16777215
        this.w_OGGETTO.Refresh()     
      case this.pTipo="_AFTERVP11"
        * --- Rimetto lo sfondo al credito utilizzato
        this.w_OGGETTO = This.oParentObject.GETCTRL("w_CRERES")
        this.w_OGGETTO.DisabledBackColor = 16777215
        this.w_OGGETTO.DisabledForeColor = 0
        this.w_OGGETTO.Refresh()     
    endcase
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='ATTIMAST'
    this.cWorkTables[2]='PRO_RATA'
    this.cWorkTables[3]='VOCIIVA'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='COC_MAST'
    this.cWorkTables[7]='COD_ABI'
    this.cWorkTables[8]='COD_CAB'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
