* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bct                                                        *
*              GESTIONE CATEGORIE ACCESSORI                                    *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-31                                                      *
* Last revis.: 2016-01-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bct",oParentObject,m.pOPER)
return(i_retval)

define class tgsar_bct as StdBatch
  * --- Local variables
  pOPER = space(6)
  w_TRIG = 0
  w_PADRE = .NULL.
  w_DATARIF = ctod("  /  /  ")
  w_ORECO = 0
  w_RECORD = 0
  w_PREDATINI = ctod("  /  /  ")
  w_PREDATARIF = ctod("  /  /  ")
  w_OLDATARIF = ctod("  /  /  ")
  w_OBJCTRL = .NULL.
  w_UNIMIS = space(3)
  w_SERIAL = space(10)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_MESS = space(100)
  * --- WorkFile variables
  CATDCONT_idx=0
  TIPCONTR_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione categorie accessori - 
    *     Controllo tipo categoria
    *     Aggiornamento date fine validit�
    this.w_PADRE = This.oParentObject
    do case
      case this.pOPER="TIPCAT"
        * --- Controllo al w_CGTIPCAT Changed
        this.w_TRIG = this.w_PADRE.NumRow()
        if this.w_TRIG > 0
          AH_ERRORMSG("Impossibile modificare la categoria contributo, sono presenti righe di dettaglio valorizzate.",48,"")
          this.oParentObject.w_CGTIPCAT = this.oParentObject.o_CGTIPCAT
        endif
      case this.pOPER="CONTRI"
        * --- Controllo al w_CGCONTRI Changed
        this.w_TRIG = this.w_PADRE.NumRow()
        if this.w_TRIG > 0
          AH_ERRORMSG("Impossibile modificare il tipo contributo, sono presenti righe di dettaglio valorizzate.",48,"")
          this.oParentObject.w_CGCONTRI = this.oParentObject.o_CGCONTRI
          * --- Read from TIPCONTR
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIPCONTR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIPCONTR_idx,2],.t.,this.TIPCONTR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TPDESCRI,TPAPPPES,TPUNMIKG"+;
              " from "+i_cTable+" TIPCONTR where ";
                  +"TPCODICE = "+cp_ToStrODBC(this.oParentObject.w_CGCONTRI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TPDESCRI,TPAPPPES,TPUNMIKG;
              from (i_cTable) where;
                  TPCODICE = this.oParentObject.w_CGCONTRI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESTIPCO = NVL(cp_ToDate(_read_.TPDESCRI),cp_NullValue(_read_.TPDESCRI))
            this.oParentObject.w_TPAPPPES = NVL(cp_ToDate(_read_.TPAPPPES),cp_NullValue(_read_.TPAPPPES))
            this.oParentObject.w_TPUNMIKG = NVL(cp_ToDate(_read_.TPUNMIKG),cp_NullValue(_read_.TPUNMIKG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
      case this.pOPER="DATFIN"
        * --- Aggiornamento data di fine validit�, per il record con data validit� pi� alta 
        *     la data di fine validit� sar� i_FINDAT, per gli altri sar� calcolata a partire 
        *     dalla data di inizio validit� del record precedente
        * --- L'aggiornamento delle date deve tenere conto delle unit� di misura, per cui, per ognuna di esse 
        *     occorre eseguire un aggiornamento indipendente delle date
        *     di validit�
        * --- Select from GSAR_BCT
        do vq_exec with 'GSAR_BCT',this,'_Curs_GSAR_BCT','',.f.,.t.
        if used('_Curs_GSAR_BCT')
          select _Curs_GSAR_BCT
          locate for 1=1
          do while not(eof())
          this.w_UNIMIS = NVL( _Curs_GSAR_BCT.CGUNIMIS , "   " )
          this.w_PREDATARIF = i_FINDAT
          * --- Select from CATDCONT
          i_nConn=i_TableProp[this.CATDCONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CATDCONT_idx,2],.t.,this.CATDCONT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select CPROWNUM, CGDATINI, CGIMPZER, CGUNIMIS  from "+i_cTable+" CATDCONT ";
                +" where CGCODICE = "+cp_ToStrODBC(this.oParentObject.w_CGCODICE)+"";
                +" order by CGDATINI desc";
                 ,"_Curs_CATDCONT")
          else
            select CPROWNUM, CGDATINI, CGIMPZER, CGUNIMIS from (i_cTable);
             where CGCODICE = this.oParentObject.w_CGCODICE;
             order by CGDATINI desc;
              into cursor _Curs_CATDCONT
          endif
          if used('_Curs_CATDCONT')
            select _Curs_CATDCONT
            locate for 1=1
            do while not(eof())
            * --- La condizione seguente potrebbe essere applicata come filtro nella select.
            *     Essendo esiguo il numero di record processati, per comodit� si utilizza una istruzione IF
            if NVL( _Curs_CATDCONT.CGUNIMIS , "   ") = this.w_UNIMIS
              this.w_TRIG = _Curs_CATDCONT.CPROWNUM
              * --- Se le date coincidono  la data fine sar� la stessa
              if _Curs_CATDCONT.CGDATINI = this.w_PREDATINI
                this.w_DATARIF = this.w_OLDATARIF
              else
                this.w_DATARIF = this.w_PREDATARIF
              endif
              * --- Write into CATDCONT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.CATDCONT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CATDCONT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.CATDCONT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CGDATFIN ="+cp_NullLink(cp_ToStrODBC(this.w_DATARIF),'CATDCONT','CGDATFIN');
                    +i_ccchkf ;
                +" where ";
                    +"CGCODICE = "+cp_ToStrODBC(this.oParentObject.w_CGCODICE);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_TRIG);
                       )
              else
                update (i_cTable) set;
                    CGDATFIN = this.w_DATARIF;
                    &i_ccchkf. ;
                 where;
                    CGCODICE = this.oParentObject.w_CGCODICE;
                    and CPROWNUM = this.w_TRIG;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              * --- Posso avere le stesse date di inizio se UM differenti
              this.w_PREDATINI = _Curs_CATDCONT.CGDATINI
              this.w_OLDATARIF = this.w_DATARIF
              * --- La data di fine validit� del prossimo record sar� uguale alla data di inizio 
              *     validit� dell'attuale record - 1 giorno
              this.w_PREDATARIF = _Curs_CATDCONT.CGDATINI - 1
            endif
              select _Curs_CATDCONT
              continue
            enddo
            use
          endif
            select _Curs_GSAR_BCT
            continue
          enddo
          use
        endif
      case this.pOPER="CHKDAT"
        * --- Si controlla che la data di inizio validit� sia gi� stata inserita
        if this.w_PADRE.FullRow()
          if this.oParentObject.w_CGTIPCAT = "S"
            this.w_PADRE.MarkPos()     
            this.w_RECORD = this.w_PADRE.RowIndex()
            this.w_ORECO = this.w_PADRE.Search(cp_ToStrODBC(this.oParentObject.w_CGUNIMIS) +" =  t_CGUNIMIS  And " +cp_ToStrODBC(dtoc(this.oParentObject.w_CGDATINI)) +" =  dtoc(t_CGDATINI) And  I_RECNO<> "+cp_ToStrODBC(this.w_RECORD) +"  And Not Deleted() ",0)
            if this.w_ORECO <> -1 And this.w_ORECO <> this.w_RECORD
              AH_ERRORMSG("E' gi� presente un record con data validit� ed unit� di misura specificate!",48,"")
              if this.oParentObject.w_CGDATINI <> this.oParentObject.o_CGDATINI
                this.w_PADRE.Set("w_CGDATINI", this.oParentObject.o_CGDATINI)     
              else
                this.w_PADRE.Set("w_CGUNIMIS", this.oParentObject.o_CGUNIMIS)     
              endif
            endif
            this.w_PADRE.RePos()     
          endif
        endif
      case this.pOPER="CHKDEL"
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVSERIAL"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVCACONT = "+cp_ToStrODBC(this.oParentObject.w_CGCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVSERIAL;
            from (i_cTable) where;
                MVCACONT = this.oParentObject.w_CGCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SERIAL = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVNUMDOC,MVALFDOC,MVDATDOC"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVNUMDOC,MVALFDOC,MVDATDOC;
              from (i_cTable) where;
                  MVSERIAL = this.w_SERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
            this.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
            this.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_MESS = Ah_MsgFormat("Impossibile cancellare: la categoria contributo � gi� utilizzata nel documento numero %1 del %2",Alltrim(STR(this.w_NUMDOC,15,0))+" "+Alltrim(this.w_ALFDOC), DTOC(this.w_DATDOC))
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
          i_retcode = 'stop'
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CATDCONT'
    this.cWorkTables[2]='TIPCONTR'
    this.cWorkTables[3]='DOC_DETT'
    this.cWorkTables[4]='DOC_MAST'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_GSAR_BCT')
      use in _Curs_GSAR_BCT
    endif
    if used('_Curs_CATDCONT')
      use in _Curs_CATDCONT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
