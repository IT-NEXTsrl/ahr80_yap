* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bar                                                        *
*              Manutenzione Application Role                                   *
*                                                                              *
*      Author: Leopoldo Abbateggio                                             *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-25                                                      *
* Last revis.: 2007-06-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bar",oParentObject)
return(i_retval)

define class tgsut_bar as StdBatch
  * --- Local variables
  pPARAM = space(8)
  w_CONFERMA = .f.
  w_RICERCAAPPLICATIONROLE = 0
  w_AGGIORNAAPPLICATIONROLE = 0
  w_TROVATOAPPLICATIONROLE = .f.
  w_ROLENAME = space(128)
  w_ROLEID = 0
  w_ISAPPROLE = 0
  w_ROLE = space(128)
  * --- WorkFile variables
  TMP_ROLE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.pPARAM = IIF( TYPE( "oParentObject" )="C" , oParentObject , "" )
    * --- GSUT_KAR - MANUTENZIONE APPLICATION ROLE
    * --- GSUT_KCC - CIFRATURA CNF
    * --- Create temporary table TMP_ROLE
    i_nIdx=cp_AddTableDef('TMP_ROLE') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    cp_CreateTempTable(i_ServerConn[1,2],i_cTempTable,'*',' from TMP_ROLE_proto';
          )
    this.TMP_ROLE_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if !CP_DBTYPE = "SQLServer" AND UPPER( this.pPARAM ) = "GSUT_KAR"
      AH_ERRORMSG( "Funzionalitą non disponibile con database di tipo %1" , "STOP" , , CP_DBTYPE )
    else
      this.w_RICERCAAPPLICATIONROLE = SQLEXEC( i_ServerConn[1,2] , "SP_HELPROLE" , "ELENCOAPPLICATIONROLE" )
      if this.w_RICERCAAPPLICATIONROLE > 0
        if USED( "ELENCOAPPLICATIONROLE" )
          this.w_ROLE = ""
          this.w_TROVATOAPPLICATIONROLE = .F.
          SELECT ELENCOAPPLICATIONROLE
          GO TOP
          SCAN FOR ISAPPROLE = 1
          this.w_TROVATOAPPLICATIONROLE = .T.
          if EMPTY( this.w_ROLE )
            this.w_ROLE = NVL( ROLENAME , "" )
          endif
          this.w_ROLENAME = NVL( ROLENAME , "" )
          this.w_ROLEID = NVL( ROLEID , 0 )
          this.w_ISAPPROLE = NVL( ISAPPROLE , 0 )
          * --- Try
          local bErr_03619D88
          bErr_03619D88=bTrsErr
          this.Try_03619D88()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_03619D88
          * --- End
          ENDSCAN
          SELECT ELENCOAPPLICATIONROLE
          USE
          if UPPER( this.pPARAM ) = "GSUT_KAR"
            if NOT this.w_TROVATOAPPLICATIONROLE
              AH_ERRORMSG( "Non sono stati trovati application role" )
              i_retcode = 'stop'
              return
            endif
            this.w_CONFERMA = .F.
            do GSUT_KAR with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if this.w_CONFERMA
              this.w_AGGIORNAAPPLICATIONROLE = SQLEXEC( i_ServerConn[1,2] , "SELECT 'GRANT SELECT,INSERT,UPDATE,DELETE ON '+NAME+' TO ["+ALLTRIM(this.w_ROLE)+"]' AS COMANDO FROM SYSOBJECTS WHERE XTYPE='U'" , "AGGIORNAAPPLICATIONROLE" )
              if this.w_AGGIORNAAPPLICATIONROLE > 0
                SELECT "AGGIORNAAPPLICATIONROLE"
                GO TOP
                SCAN
                this.w_AGGIORNAAPPLICATIONROLE = SQLEXEC( i_ServerConn[1,2] , COMANDO )
                if this.w_AGGIORNAAPPLICATIONROLE >0
                  AH_MSG( "Eseguito il comando:%0%1" , .t. , , , ALLTRIM( COMANDO ) )
                endif
                ENDSCAN
                AH_ERRORMSG( "Elaborazione terminata" , "i" )
              else
                AH_ERRORMSG( "Non ci sono i diritti per eseguire questa operazione" )
              endif
            endif
          endif
        endif
      else
        if UPPER( this.pPARAM ) = "GSUT_KAR"
          AH_ERRORMSG( "Non sono stati rilevati application role sul database" )
        endif
      endif
      if UPPER( this.pPARAM ) = "GSUT_KCC"
        do GSUT_KCC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Drop temporary table TMP_ROLE
    i_nIdx=cp_GetTableDefIdx('TMP_ROLE')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMP_ROLE')
    endif
  endproc
  proc Try_03619D88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into TMP_ROLE
    i_nConn=i_TableProp[this.TMP_ROLE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMP_ROLE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMP_ROLE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ROLENAME"+",ROLEID"+",ISAPPROLE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ROLENAME),'TMP_ROLE','ROLENAME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROLEID),'TMP_ROLE','ROLEID');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ISAPPROLE),'TMP_ROLE','ISAPPROLE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ROLENAME',this.w_ROLENAME,'ROLEID',this.w_ROLEID,'ISAPPROLE',this.w_ISAPPROLE)
      insert into (i_cTable) (ROLENAME,ROLEID,ISAPPROLE &i_ccchkf. );
         values (;
           this.w_ROLENAME;
           ,this.w_ROLEID;
           ,this.w_ISAPPROLE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*TMP_ROLE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
