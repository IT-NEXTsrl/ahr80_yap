* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_br1                                                        *
*              Crea path salvataggio word nel registro di sistema              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-12-13                                                      *
* Last revis.: 2013-07-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPATH
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_br1",oParentObject,m.pPATH)
return(i_retval)

define class tgsar_br1 as StdBatch
  * --- Local variables
  pPATH = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --Scrittura registro di sistema
     
 #DEFINE HKEY_CURRENT_USER 2147483649 
 #DEFINE SECURITY_ACCESS_MASK 983103 
 
 DECLARE INTEGER RegCreateKeyEx IN advapi32; 
 INTEGER hKey,; 
 STRING lpSubKey,; 
 INTEGER Reserved,; 
 STRING lpClass,; 
 INTEGER dwOptions,; 
 INTEGER samDesired,; 
 INTEGER lpSecurityAttributes,; 
 INTEGER @ phkResult,; 
 INTEGER @ lpdwDisposition 
 
 DECLARE INTEGER RegSetValueEx IN advapi32; 
 INTEGER hKey,; 
 STRING lpValueName,; 
 INTEGER Reserved,; 
 INTEGER dwType,; 
 STRING @ lpData,; 
 INTEGER cbData 
 
 PUBLIC nRESULT,nDISPLAY,nByteSize,cPath 
 nRESULT=0 
 nDISPLAY=0 
 cPath=Alltrim(this.pPATH) 
 nByteSize=LEN(cPath)
    if (RegCreateKeyEx(HKEY_CURRENT_USER,"Software\Zucchetti\adhoc Revolution",0,"REG_SZ",0,SECURITY_ACCESS_MASK,0,@nRESULT,@nDISPLAY))<>0
      Ah_errormsg("Errore nell'aggiornamento del registro di sistema")
      i_retcode = 'stop'
      return
    else
      if (RegSetValueEx(nResult,"PathWord",0,1,cPath,nByteSize))<>0
        Ah_errormsg("Errore nell'aggiornamento del registro di sistema")
        i_retcode = 'stop'
        return
      endif
    endif
    Ah_msg("Aggiornamento complemento")
  endproc


  proc Init(oParentObject,pPATH)
    this.pPATH=pPATH
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPATH"
endproc
