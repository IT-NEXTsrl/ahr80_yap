* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_brq                                                        *
*              OGGETTO PARAMETRI QUERY                                         *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-17                                                      *
* Last revis.: 2007-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pINDICE,pCLASSEDOCU,pARCHIVIO,pMODELLO,pQUERY,pPATDEST
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_brq",oParentObject,m.pINDICE,m.pCLASSEDOCU,m.pARCHIVIO,m.pMODELLO,m.pQUERY,m.pPATDEST)
return(i_retval)

define class tgsut_brq as StdBatch
  * --- Local variables
  pINDICE = space(20)
  pCLASSEDOCU = space(15)
  pARCHIVIO = space(15)
  pMODELLO = space(254)
  pQUERY = space(60)
  pPATDEST = space(254)
  w_NOMEATTRIB = space(15)
  w_FIELDSLIST = space(0)
  w_POSPIU = 0
  w_NOMECAMPO = space(8)
  w_TIPOCAMPO = space(1)
  w_LUNGCAMPO = 0
  w_PUNTATORE = 0
  * --- WorkFile variables
  PRODINDI_idx=0
  PRODCLAS_idx=0
  XDC_FIELDS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per la scansione/valorizzazione degli attributi �er una determinata classe documentale
    *     
    *     Parametri:
    *     1 - Seriale dell'indice documenti;
    *     2 - Codice della classe documentale;
    *     3 - Nome tabella/archivio
    *     4 - Path e nome file modello Word/Open Office
    *     5 - Query associata al modello documento
    *     6 - Percorso di archiviazione (completo di nome file ed estensione) del documento da creare.
    * --- Istanzio l'oggetto che conterr� le coppie attribuot/valore
     
 public ObjAttribute 
 ObjAttribute = createobject("QueryParam")
    * --- Gestione errori
     
 crash_error = .f. 
 OldError = On("ERROR") 
 On Error crash_error = .t.
    * --- Select from PRODINDI
    i_nConn=i_TableProp[this.PRODINDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select IDCODATT,IDVALATT  from "+i_cTable+" PRODINDI ";
          +" where IDSERIAL = "+cp_ToStrODBC(this.pINDICE)+"";
           ,"_Curs_PRODINDI")
    else
      select IDCODATT,IDVALATT from (i_cTable);
       where IDSERIAL = this.pINDICE;
        into cursor _Curs_PRODINDI
    endif
    if used('_Curs_PRODINDI')
      select _Curs_PRODINDI
      locate for 1=1
      do while not(eof())
      this.w_NOMEATTRIB = NVL(_Curs_PRODINDI.IDCODATT,"")
      * --- Read from PRODCLAS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PRODCLAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRODCLAS_idx,2],.t.,this.PRODCLAS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CDCAMCUR"+;
          " from "+i_cTable+" PRODCLAS where ";
              +"CDCODCLA = "+cp_ToStrODBC(this.pCLASSEDOCU);
              +" and CDCODATT = "+cp_ToStrODBC(this.w_NOMEATTRIB);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CDCAMCUR;
          from (i_cTable) where;
              CDCODCLA = this.pCLASSEDOCU;
              and CDCODATT = this.w_NOMEATTRIB;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FIELDSLIST = NVL(cp_ToDate(_read_.CDCAMCUR),cp_NullValue(_read_.CDCAMCUR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Verifico se l'attributo � costituito da una concatenazione di campi e diversifico di conseguenza la valorizzazione dell'oggetto
      this.w_POSPIU = at("+",this.w_FIELDSLIST)
      if this.w_POSPIU > 0
        this.w_PUNTATORE = 1
        do while len(this.w_FIELDSLIST) > 0
          this.w_NOMECAMPO = alltrim(substr(this.w_FIELDSLIST,1,iif(this.w_POSPIU#0,this.w_POSPIU-1,len(this.w_FIELDSLIST))))
          this.w_FIELDSLIST = iif(this.w_POSPIU#0,alltrim(substr(this.w_FIELDSLIST,this.w_POSPIU+1)),"")
          * --- Read from XDC_FIELDS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.XDC_FIELDS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.XDC_FIELDS_idx,2],.t.,this.XDC_FIELDS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "FLTYPE,FLLENGHT"+;
              " from "+i_cTable+" XDC_FIELDS where ";
                  +"TBNAME = "+cp_ToStrODBC(this.pARCHIVIO);
                  +" and FLNAME = "+cp_ToStrODBC(this.w_NOMECAMPO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              FLTYPE,FLLENGHT;
              from (i_cTable) where;
                  TBNAME = this.pARCHIVIO;
                  and FLNAME = this.w_NOMECAMPO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPOCAMPO = NVL(cp_ToDate(_read_.FLTYPE),cp_NullValue(_read_.FLTYPE))
            this.w_LUNGCAMPO = NVL(cp_ToDate(_read_.FLLENGHT),cp_NullValue(_read_.FLLENGHT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_rows = 0
            * --- Il valore letto non � un nome di campo valido.
          else
            do case
              case this.w_TIPOCAMPO $ "C-M"
                 ObjAttribute.AddParVal(this.w_NOMECAMPO,substr(_Curs_PRODINDI.IDVALATT,this.w_PUNTATORE,this.w_LUNGCAMPO))
                this.w_PUNTATORE = this.w_PUNTATORE + this.w_LUNGCAMPO
              otherwise
                * --- Caso improbabile
                 ObjAttribute.AddParVal(upper(alltrim(this.w_FIELDSLIST)),_Curs_PRODINDI.IDVALATT)
            endcase
          endif
          do case
            case len(this.w_FIELDSLIST) > 0 and at("+",this.w_FIELDSLIST) = 0
              this.w_POSPIU = 0
            case len(this.w_FIELDSLIST) > 0 and at("+",this.w_FIELDSLIST) > 0
              this.w_POSPIU = at("+",this.w_FIELDSLIST)
            otherwise
              this.w_FIELDSLIST = ""
          endcase
        enddo
      else
         ObjAttribute.AddParVal(alltrim(this.w_FIELDSLIST),_Curs_PRODINDI.IDVALATT)
      endif
      if crash_error
        Ah_ErrorMsg("Si � verificato un errore nella fase di valorizzazione filtri.%0La generazione del documento � stata interrotta!",48,"")
        release ObjAttribute 
        i_retcode = 'stop'
        return
      endif
        select _Curs_PRODINDI
        continue
      enddo
      use
    endif
    * --- Chiamo la routine di creazione del modello
    GSUT_BCM(this,this.pMODELLO,ObjAttribute,this.pQUERY,this.pPATDEST)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  proc Init(oParentObject,pINDICE,pCLASSEDOCU,pARCHIVIO,pMODELLO,pQUERY,pPATDEST)
    this.pINDICE=pINDICE
    this.pCLASSEDOCU=pCLASSEDOCU
    this.pARCHIVIO=pARCHIVIO
    this.pMODELLO=pMODELLO
    this.pQUERY=pQUERY
    this.pPATDEST=pPATDEST
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='PRODINDI'
    this.cWorkTables[2]='PRODCLAS'
    this.cWorkTables[3]='XDC_FIELDS'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_PRODINDI')
      use in _Curs_PRODINDI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pINDICE,pCLASSEDOCU,pARCHIVIO,pMODELLO,pQUERY,pPATDEST"
endproc
