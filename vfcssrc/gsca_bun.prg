* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_bun                                                        *
*              Calcolo dati di analitica                                       *
*                                                                              *
*      Author: ZUCCHETTI SPA: CS                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-07-11                                                      *
* Last revis.: 2010-07-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_bun",oParentObject)
return(i_retval)

define class tgsca_bun as StdBatch
  * --- Local variables
  * --- WorkFile variables
  TMPMOVANAL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- La routine elabora una tabella temporanea e deve essere base di tutte le stampe / visualizzazioni
    * --- Competenza
    * --- Data inizio elaborazione
    * --- Data fine elaborazione
    * --- Flag budget: 'S', 'N', 'T'
    * --- Flag scritture di assestamento: 'S', 'N', 'T'
    * --- Flag consuntivo/impegno: 'S', 'I', 'T'
    * --- Flag diretto/ripartito: 'D', 'R', 'T'
    * --- Flag ripartito/da ripartire: 'S', 'N', 'T'
    * --- Flag rprovvisorio/confermato: 'S', 'N', 'T'
    * --- Codice Business Unit
    * --- Filtri su CDC, VOCE e COMMESSA
    * --- Filtro sul livello
    * --- Tipo movimenti: 'E' effettivi, 'P' previsionali, 'T' tutti
    * --- Flag stampa
    * --- Flag competenza ('S', 'N')
    *     Si applica ai soli movimenti extracontabili
    * --- Flag sezione di bilancio: 'P'=patrimoniali, 'E'=reddituali ('C' costi,'R' ricavi), 'T'=tutti
    * --- Flag provenienza: 'T'=tutti, 'A'=manuali, 'P'=primanota
    * --- Flag origine: 'T'=tutti, 'O'=originari, 'V'=Costo del venduto, 'C'=collegati
    * --- Flag per l'esclusione dello storno dei movimenti da ripartizione
    * --- Crea tabella temporanea vuota
    * --- Create temporary table TMPMOVANAL
    i_nIdx=cp_AddTableDef('TMPMOVANAL') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('gsca_qun_bas',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPMOVANAL_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    if (this.oParentObject.w_PROVE="T" OR this.oParentObject.w_PROVE="P") AND this.oParentObject.w_FLGORI$"TO"
      if this.oParentObject.w_FLGBUDG <> "S" AND this.oParentObject.w_FLGCON <> "I" AND this.oParentObject.w_FLGDIRRIP <> "R"
        * --- Insert into TMPMOVANAL
        i_nConn=i_TableProp[this.TMPMOVANAL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVANAL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsca_qun_pn",this.TMPMOVANAL_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      if this.oParentObject.w_FLGBUDG <> "S" AND this.oParentObject.w_FLGCON <> "I" AND this.oParentObject.w_FLGDIRRIP <> "D" 
        * --- Insert into TMPMOVANAL
        i_nConn=i_TableProp[this.TMPMOVANAL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVANAL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsca_qun_pnr",this.TMPMOVANAL_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
    endif
    if this.oParentObject.w_PROVE="T" OR this.oParentObject.w_PROVE="A"
      if this.oParentObject.w_SCRASS <> "S" AND this.oParentObject.w_FLPROV <> "S" AND this.oParentObject.w_FLGDIRRIP <> "R"
        * --- Insert into TMPMOVANAL
        i_nConn=i_TableProp[this.TMPMOVANAL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVANAL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsca_qun_ex",this.TMPMOVANAL_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      if this.oParentObject.w_SCRASS <> "S" AND this.oParentObject.w_FLPROV <> "S" AND this.oParentObject.w_FLGDIRRIP <> "D"
        * --- Insert into TMPMOVANAL
        i_nConn=i_TableProp[this.TMPMOVANAL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVANAL_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gsca_qun_exr",this.TMPMOVANAL_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
    endif
    * --- Nel caso sia stato attivato il flag "Escludi storno movimenti da ripartizione"
    *     devono essere eliminati dal cursore temporaneo TMPMOVANAL tutti 
    *     i record che hanno il campo "MRTIPRIP"=-1
    if this.oParentObject.w_ESSTMORI="S"
      * --- Delete from TMPMOVANAL
      i_nConn=i_TableProp[this.TMPMOVANAL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPMOVANAL_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MRTIPRIP = "+cp_ToStrODBC(-1);
               )
      else
        delete from (i_cTable) where;
              MRTIPRIP = -1;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*TMPMOVANAL'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
