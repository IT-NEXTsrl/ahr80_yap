* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bia                                                        *
*              Creazione azienda                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_2]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-10                                                      *
* Last revis.: 2014-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bia",oParentObject)
return(i_retval)

define class tgsut_bia as StdBatch
  * --- Local variables
  w_OK = .f.
  w_CODICE = 0
  CARCOD = space(1)
  w_UTENTE = 0
  * --- WorkFile variables
  AZIENDA_idx=0
  ESERCIZI_idx=0
  CPUSERS_idx=0
  UTE_AZI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea una Nuova Azienda (Lanciato da GSUT_KIA)
    * --- controllo che non ci siano carattere -pericolosi- nel codice azienda
    this.w_CODICE = 1
    this.oParentObject.w_CODAZI = ALLTRIM(this.oParentObject.w_CODAZI)
    if VAL(left(this.oParentObject.w_CODAZI,1))>0 OR left(this.oParentObject.w_CODAZI,1)="0"
      ah_ErrorMsg("La prima lettera del codice azienda non deve essere numerica",,"")
      i_retcode = 'stop'
      return
    endif
    * --- LA DITTA XXX O xxx non pu� essere creata
    this.w_OK = UPPER(this.oParentObject.W_CODAZI)="XXX"
    * --- Non deve contenere carateri proibiti (1) o spazi (2) (1 Errore in Fox, 2 errore in SQL)
    do while this.w_CODICE < LEN( this.oParentObject.w_CODAZI ) + 1 AND ( NOT this.w_OK )
      this.CARCOD = substr(this.oParentObject.w_CODAZI,this.w_CODICE,1)
      this.w_OK = between(this.CARCOD,"A","Z") or between(this.CARCOD,"a","z") or between(this.CARCOD,"0","9")
      this.w_OK = this.w_OK And Not Empty (this.CARCOD)
      this.w_CODICE = this.w_CODICE + 1
    enddo
    if this.w_OK = .F.
      * --- il codice azienda contiene caratteri proibiti dal sistema
      ah_ErrorMsg("IMPOSSIBILE CREARE AZIENDA: %1%0- Il codice azienda pu� contenere solo lettere e numeri",,"", this.oParentObject.w_CODAZI)
      i_retcode = 'stop'
      return
    endif
    * --- creazione DataBaseVuoto
    this.w_OK = CP_CREATEAZI(this.oParentObject.w_CODAZI, this.oParentObject.w_RAGAZI)
    if this.w_OK = .T.
      * --- Try
      local bErr_00E9B960
      bErr_00E9B960=bTrsErr
      this.Try_00E9B960()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        * --- cancello fisicamente gli archivi creati
        this.w_OK = CP_DELETEAZI(this.oParentObject.w_CODAZI)
        ah_ErrorMsg("CREAZIONE AZIENDA %1 ABORTITA",,"", alltrim(this.oParentObject.w_CODAZI) )
      endif
      bTrsErr=bTrsErr or bErr_00E9B960
      * --- End
    else
      * --- Abbandona...
      ah_ErrorMsg("IMPOSSIBILE CREARE TABELLE AZIENDA: %1",,"", this.oParentObject.w_CODAZI)
    endif
  endproc
  proc Try_00E9B960()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- la ditta non deve esistere altrimenti errore
    * --- Insert into AZIENDA
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AZCODAZI"+",AZRAGAZI"+",AZFLBUNI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODAZI),'AZIENDA','AZCODAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_RAGAZI),'AZIENDA','AZRAGAZI');
      +","+cp_NullLink(cp_ToStrODBC("N"),'AZIENDA','AZFLBUNI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AZCODAZI',this.oParentObject.w_CODAZI,'AZRAGAZI',this.oParentObject.w_RAGAZI,'AZFLBUNI',"N")
      insert into (i_cTable) (AZCODAZI,AZRAGAZI,AZFLBUNI &i_ccchkf. );
         values (;
           this.oParentObject.w_CODAZI;
           ,this.oParentObject.w_RAGAZI;
           ,"N";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Insert into ESERCIZI
    i_nConn=i_TableProp[this.ESERCIZI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ESERCIZI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ESCODAZI"+",ESCODESE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODAZI),'ESERCIZI','ESCODAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'ESERCIZI','ESCODESE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ESCODAZI',this.oParentObject.w_CODAZI,'ESCODESE',this.oParentObject.w_CODESE)
      insert into (i_cTable) (ESCODAZI,ESCODESE &i_ccchkf. );
         values (;
           this.oParentObject.w_CODAZI;
           ,this.oParentObject.w_CODESE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Select from CPUSERS
    i_nConn=i_TableProp[this.CPUSERS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CPUSERS_idx,2],.t.,this.CPUSERS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CODE  from "+i_cTable+" CPUSERS ";
           ,"_Curs_CPUSERS")
    else
      select CODE from (i_cTable);
        into cursor _Curs_CPUSERS
    endif
    if used('_Curs_CPUSERS')
      select _Curs_CPUSERS
      locate for 1=1
      do while not(eof())
      this.w_UTENTE = _Curs_CPUSERS.CODE
      * --- Insert into UTE_AZI
      i_nConn=i_TableProp[this.UTE_AZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.UTE_AZI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.UTE_AZI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"UACODAZI"+",UACODUTE"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODAZI),'UTE_AZI','UACODAZI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_UTENTE),'UTE_AZI','UACODUTE');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'UACODAZI',this.oParentObject.w_CODAZI,'UACODUTE',this.w_UTENTE)
        insert into (i_cTable) (UACODAZI,UACODUTE &i_ccchkf. );
           values (;
             this.oParentObject.w_CODAZI;
             ,this.w_UTENTE;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_CPUSERS
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_Msg("AZIENDA: %1 GENERATA",.T.,.F.,.F., this.oParentObject.w_CODAZI)
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='ESERCIZI'
    this.cWorkTables[3]='CPUSERS'
    this.cWorkTables[4]='UTE_AZI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_CPUSERS')
      use in _Curs_CPUSERS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
