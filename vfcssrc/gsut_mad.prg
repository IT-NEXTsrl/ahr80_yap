* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_mad                                                        *
*              Gestione addon verticalizzazioni                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-03-13                                                      *
* Last revis.: 2011-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsut_mad")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsut_mad")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsut_mad")
  return

* --- Class definition
define class tgsut_mad as StdPCForm
  Width  = 580
  Height = 181
  Top    = 5
  Left   = 18
  cComment = "Gestione addon verticalizzazioni"
  cPrg = "gsut_mad"
  HelpContextID=130630505
  add object cnt as tcgsut_mad
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsut_mad as PCContext
  w_ADORDINE = 0
  w_ADCODICE = space(8)
  w_AD__TIPO = space(1)
  w_ADABILIT = space(1)
  w_ADPRIORI = space(1)
  w_ADNOMMOD = space(8)
  w_ADDESCRI = space(40)
  w_ADFLDSER = 0
  w_ADDESPRO = space(40)
  w_ADMODLIC = space(1)
  proc Save(i_oFrom)
    this.w_ADORDINE = i_oFrom.w_ADORDINE
    this.w_ADCODICE = i_oFrom.w_ADCODICE
    this.w_AD__TIPO = i_oFrom.w_AD__TIPO
    this.w_ADABILIT = i_oFrom.w_ADABILIT
    this.w_ADPRIORI = i_oFrom.w_ADPRIORI
    this.w_ADNOMMOD = i_oFrom.w_ADNOMMOD
    this.w_ADDESCRI = i_oFrom.w_ADDESCRI
    this.w_ADFLDSER = i_oFrom.w_ADFLDSER
    this.w_ADDESPRO = i_oFrom.w_ADDESPRO
    this.w_ADMODLIC = i_oFrom.w_ADMODLIC
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_ADORDINE = this.w_ADORDINE
    i_oTo.w_ADCODICE = this.w_ADCODICE
    i_oTo.w_AD__TIPO = this.w_AD__TIPO
    i_oTo.w_ADABILIT = this.w_ADABILIT
    i_oTo.w_ADPRIORI = this.w_ADPRIORI
    i_oTo.w_ADNOMMOD = this.w_ADNOMMOD
    i_oTo.w_ADDESCRI = this.w_ADDESCRI
    i_oTo.w_ADFLDSER = this.w_ADFLDSER
    i_oTo.w_ADDESPRO = this.w_ADDESPRO
    i_oTo.w_ADMODLIC = this.w_ADMODLIC
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsut_mad as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 580
  Height = 181
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-02"
  HelpContextID=130630505
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  ADDON_IDX = 0
  cFile = "ADDON"
  cKeySelect = "ADFLDSER"
  cKeyWhere  = "ADFLDSER=this.w_ADFLDSER"
  cKeyDetail  = "ADCODICE=this.w_ADCODICE and ADFLDSER=this.w_ADFLDSER"
  cKeyWhereODBC = '"ADFLDSER="+cp_ToStrODBC(this.w_ADFLDSER)';

  cKeyDetailWhereODBC = '"ADCODICE="+cp_ToStrODBC(this.w_ADCODICE)';
      +'+" and ADFLDSER="+cp_ToStrODBC(this.w_ADFLDSER)';

  cKeyWhereODBCqualified = '"ADDON.ADFLDSER="+cp_ToStrODBC(this.w_ADFLDSER)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'ADDON.ADMODLIC DESC,ADDON. ADABILIT DESC,ADDON. ADORDINE'
  cPrg = "gsut_mad"
  cComment = "Gestione addon verticalizzazioni"
  i_nRowNum = 0
  i_nRowPerPage = 5
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ADORDINE = 0
  w_ADCODICE = space(8)
  w_AD__TIPO = space(1)
  w_ADABILIT = space(1)
  w_ADPRIORI = space(1)
  w_ADNOMMOD = space(8)
  w_ADDESCRI = space(40)
  w_ADFLDSER = 0
  w_ADDESPRO = space(40)
  w_ADMODLIC = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_madPag1","gsut_mad",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ADDON'
    * --- Area Manuale = Open Work Table
    * --- gsut_mad
    * colora lo sfondo dei moduli nella chiave
    * Verde se in licenza ed attivo
    * Giallo se in licenza e non attivo
    * Rosso se non in licenza e attivo
    * Bianco se non in licenza e non attivo
    This.oPgFrm.Page1.oPAg.oBody.oBodyCol.DynamicBackColor="IIF(t_ADMODLIC='S',iif(t_ADABILIT=2,RGB(240,228,147),RGB(130,170,255)),iif(T_ADABILIT=1,RGB(249,72,85),RGB(255,255,255)))"
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.ADDON_IDX,5],7]
    this.nPostItConn=i_TableProp[this.ADDON_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsut_mad'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- gsut_mad
          * Mostro solo Addon e verticalizzazioni
          this.cKeyWhereODBCqualified=this.cKeyWhereODBCqualified +'+"'+" And AD__TIPO in ('A','V')"+'"'
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from ADDON where ADCODICE=KeySet.ADCODICE
    *                            and ADFLDSER=KeySet.ADFLDSER
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.ADDON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ADDON_IDX,2],this.bLoadRecFilter,this.ADDON_IDX,"gsut_mad")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('ADDON')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "ADDON.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' ADDON '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ADFLDSER',this.w_ADFLDSER  )
      select * from (i_cTable) ADDON where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_ADFLDSER = NVL(ADFLDSER,0)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'ADDON')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_ADORDINE = NVL(ADORDINE,0)
          .w_ADCODICE = NVL(ADCODICE,space(8))
          .w_AD__TIPO = NVL(AD__TIPO,space(1))
          .w_ADABILIT = NVL(ADABILIT,space(1))
          .w_ADPRIORI = NVL(ADPRIORI,space(1))
          .w_ADNOMMOD = NVL(ADNOMMOD,space(8))
          .w_ADDESCRI = NVL(ADDESCRI,space(40))
          .w_ADDESPRO = NVL(ADDESPRO,space(40))
          .w_ADMODLIC = NVL(ADMODLIC,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace ADCODICE with .w_ADCODICE
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_ADORDINE=0
      .w_ADCODICE=space(8)
      .w_AD__TIPO=space(1)
      .w_ADABILIT=space(1)
      .w_ADPRIORI=space(1)
      .w_ADNOMMOD=space(8)
      .w_ADDESCRI=space(40)
      .w_ADFLDSER=0
      .w_ADDESPRO=space(40)
      .w_ADMODLIC=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        .w_AD__TIPO = 'A'
        .w_ADABILIT = 'N'
        .DoRTCalc(5,7,.f.)
        .w_ADFLDSER = 1
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'ADDON')
    this.DoRTCalc(9,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oADDESCRI_2_7.enabled = i_bVal
      .Page1.oPag.oADDESPRO_2_8.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'ADDON',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.ADDON_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ADFLDSER,"ADFLDSER",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_ADORDINE N(10);
      ,t_ADCODICE C(8);
      ,t_AD__TIPO N(3);
      ,t_ADABILIT N(3);
      ,t_ADPRIORI N(3);
      ,t_ADNOMMOD C(8);
      ,t_ADDESCRI C(40);
      ,t_ADDESPRO C(40);
      ,ADCODICE C(8);
      ,t_ADMODLIC C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsut_madbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oADORDINE_2_1.controlsource=this.cTrsName+'.t_ADORDINE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oADCODICE_2_2.controlsource=this.cTrsName+'.t_ADCODICE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAD__TIPO_2_3.controlsource=this.cTrsName+'.t_AD__TIPO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oADABILIT_2_4.controlsource=this.cTrsName+'.t_ADABILIT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oADPRIORI_2_5.controlsource=this.cTrsName+'.t_ADPRIORI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oADNOMMOD_2_6.controlsource=this.cTrsName+'.t_ADNOMMOD'
    this.oPgFRm.Page1.oPag.oADDESCRI_2_7.controlsource=this.cTrsName+'.t_ADDESCRI'
    this.oPgFRm.Page1.oPag.oADDESPRO_2_8.controlsource=this.cTrsName+'.t_ADDESPRO'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(46)
    this.AddVLine(174)
    this.AddVLine(292)
    this.AddVLine(382)
    this.AddVLine(447)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADORDINE_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.ADDON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ADDON_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.ADDON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ADDON_IDX,2])
      *
      * insert into ADDON
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'ADDON')
        i_extval=cp_InsertValODBCExtFlds(this,'ADDON')
        i_cFldBody=" "+;
                  "(ADORDINE,ADCODICE,AD__TIPO,ADABILIT,ADPRIORI"+;
                  ",ADNOMMOD,ADDESCRI,ADFLDSER,ADDESPRO,ADMODLIC,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_ADORDINE)+","+cp_ToStrODBC(this.w_ADCODICE)+","+cp_ToStrODBC(this.w_AD__TIPO)+","+cp_ToStrODBC(this.w_ADABILIT)+","+cp_ToStrODBC(this.w_ADPRIORI)+;
             ","+cp_ToStrODBC(this.w_ADNOMMOD)+","+cp_ToStrODBC(this.w_ADDESCRI)+","+cp_ToStrODBC(this.w_ADFLDSER)+","+cp_ToStrODBC(this.w_ADDESPRO)+","+cp_ToStrODBC(this.w_ADMODLIC)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'ADDON')
        i_extval=cp_InsertValVFPExtFlds(this,'ADDON')
        cp_CheckDeletedKey(i_cTable,0,'ADCODICE',this.w_ADCODICE,'ADFLDSER',this.w_ADFLDSER)
        INSERT INTO (i_cTable) (;
                   ADORDINE;
                  ,ADCODICE;
                  ,AD__TIPO;
                  ,ADABILIT;
                  ,ADPRIORI;
                  ,ADNOMMOD;
                  ,ADDESCRI;
                  ,ADFLDSER;
                  ,ADDESPRO;
                  ,ADMODLIC;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_ADORDINE;
                  ,this.w_ADCODICE;
                  ,this.w_AD__TIPO;
                  ,this.w_ADABILIT;
                  ,this.w_ADPRIORI;
                  ,this.w_ADNOMMOD;
                  ,this.w_ADDESCRI;
                  ,this.w_ADFLDSER;
                  ,this.w_ADDESPRO;
                  ,this.w_ADMODLIC;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.ADDON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ADDON_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (Not Empty( t_ADCODICE )) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'ADDON')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and ADCODICE="+cp_ToStrODBC(&i_TN.->ADCODICE)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'ADDON')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and ADCODICE=&i_TN.->ADCODICE;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (Not Empty( t_ADCODICE )) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and ADCODICE="+cp_ToStrODBC(&i_TN.->ADCODICE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and ADCODICE=&i_TN.->ADCODICE;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace ADCODICE with this.w_ADCODICE
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update ADDON
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'ADDON')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " ADORDINE="+cp_ToStrODBC(this.w_ADORDINE)+;
                     ",AD__TIPO="+cp_ToStrODBC(this.w_AD__TIPO)+;
                     ",ADABILIT="+cp_ToStrODBC(this.w_ADABILIT)+;
                     ",ADPRIORI="+cp_ToStrODBC(this.w_ADPRIORI)+;
                     ",ADNOMMOD="+cp_ToStrODBC(this.w_ADNOMMOD)+;
                     ",ADDESCRI="+cp_ToStrODBC(this.w_ADDESCRI)+;
                     ",ADDESPRO="+cp_ToStrODBC(this.w_ADDESPRO)+;
                     ",ADMODLIC="+cp_ToStrODBC(this.w_ADMODLIC)+;
                     ",ADCODICE="+cp_ToStrODBC(this.w_ADCODICE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and ADCODICE="+cp_ToStrODBC(ADCODICE)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'ADDON')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      ADORDINE=this.w_ADORDINE;
                     ,AD__TIPO=this.w_AD__TIPO;
                     ,ADABILIT=this.w_ADABILIT;
                     ,ADPRIORI=this.w_ADPRIORI;
                     ,ADNOMMOD=this.w_ADNOMMOD;
                     ,ADDESCRI=this.w_ADDESCRI;
                     ,ADDESPRO=this.w_ADDESPRO;
                     ,ADMODLIC=this.w_ADMODLIC;
                     ,ADCODICE=this.w_ADCODICE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and ADCODICE=&i_TN.->ADCODICE;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.ADDON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.ADDON_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (Not Empty( t_ADCODICE )) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete ADDON
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and ADCODICE="+cp_ToStrODBC(&i_TN.->ADCODICE)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and ADCODICE=&i_TN.->ADCODICE;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (Not Empty( t_ADCODICE )) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.ADDON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ADDON_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_ADMODLIC with this.w_ADMODLIC
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oADDESCRI_2_7.value==this.w_ADDESCRI)
      this.oPgFrm.Page1.oPag.oADDESCRI_2_7.value=this.w_ADDESCRI
      replace t_ADDESCRI with this.oPgFrm.Page1.oPag.oADDESCRI_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oADDESPRO_2_8.value==this.w_ADDESPRO)
      this.oPgFrm.Page1.oPag.oADDESPRO_2_8.value=this.w_ADDESPRO
      replace t_ADDESPRO with this.oPgFrm.Page1.oPag.oADDESPRO_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADORDINE_2_1.value==this.w_ADORDINE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADORDINE_2_1.value=this.w_ADORDINE
      replace t_ADORDINE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADORDINE_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADCODICE_2_2.value==this.w_ADCODICE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADCODICE_2_2.value=this.w_ADCODICE
      replace t_ADCODICE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADCODICE_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAD__TIPO_2_3.RadioValue()==this.w_AD__TIPO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAD__TIPO_2_3.SetRadio()
      replace t_AD__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAD__TIPO_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADABILIT_2_4.RadioValue()==this.w_ADABILIT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADABILIT_2_4.SetRadio()
      replace t_ADABILIT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADABILIT_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADPRIORI_2_5.RadioValue()==this.w_ADPRIORI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADPRIORI_2_5.SetRadio()
      replace t_ADPRIORI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADPRIORI_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADNOMMOD_2_6.value==this.w_ADNOMMOD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADNOMMOD_2_6.value=this.w_ADNOMMOD
      replace t_ADNOMMOD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADNOMMOD_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'ADDON')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(len(alltrim(.w_ADCODICE)) >= 4) and (Not Empty( .w_ADCODICE ))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADCODICE_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Il codice dell'add-on / verticalizzazione deve essere lungo almeno 4 caratteri.")
      endcase
      if Not Empty( .w_ADCODICE )
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(Not Empty( t_ADCODICE ))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_ADORDINE=0
      .w_ADCODICE=space(8)
      .w_AD__TIPO=space(1)
      .w_ADABILIT=space(1)
      .w_ADPRIORI=space(1)
      .w_ADNOMMOD=space(8)
      .w_ADDESCRI=space(40)
      .w_ADDESPRO=space(40)
      .w_ADMODLIC=space(1)
      .DoRTCalc(1,2,.f.)
        .w_AD__TIPO = 'A'
        .w_ADABILIT = 'N'
    endwith
    this.DoRTCalc(5,10,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_ADORDINE = t_ADORDINE
    this.w_ADCODICE = t_ADCODICE
    this.w_AD__TIPO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAD__TIPO_2_3.RadioValue(.t.)
    this.w_ADABILIT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADABILIT_2_4.RadioValue(.t.)
    this.w_ADPRIORI = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADPRIORI_2_5.RadioValue(.t.)
    this.w_ADNOMMOD = t_ADNOMMOD
    this.w_ADDESCRI = t_ADDESCRI
    this.w_ADDESPRO = t_ADDESPRO
    this.w_ADMODLIC = t_ADMODLIC
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_ADORDINE with this.w_ADORDINE
    replace t_ADCODICE with this.w_ADCODICE
    replace t_AD__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAD__TIPO_2_3.ToRadio()
    replace t_ADABILIT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADABILIT_2_4.ToRadio()
    replace t_ADPRIORI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oADPRIORI_2_5.ToRadio()
    replace t_ADNOMMOD with this.w_ADNOMMOD
    replace t_ADDESCRI with this.w_ADDESCRI
    replace t_ADDESPRO with this.w_ADDESPRO
    replace t_ADMODLIC with this.w_ADMODLIC
    if i_srv='A'
      replace ADCODICE with this.w_ADCODICE
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsut_madPag1 as StdContainer
  Width  = 576
  height = 181
  stdWidth  = 576
  stdheight = 181
  resizeXpos=245
  resizeYpos=87
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=9, top=2, width=557,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="ADORDINE",Label1="",Field2="ADCODICE",Label2="Addon",Field3="AD__TIPO",Label3="Tipo",Field4="ADABILIT",Label4="Abilitato",Field5="ADPRIORI",Label5="Precede standard",Field6="ADNOMMOD",Label6="Analisi",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 185460346

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=21,;
    width=553+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=22,width=552+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oADDESCRI_2_7.Refresh()
      this.Parent.oADDESPRO_2_8.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oADDESCRI_2_7 as StdTrsField with uid="VJRQDJKNSA",rtseq=7,rtrep=.t.,;
    cFormVar="w_ADDESCRI",value=space(40),;
    HelpContextID = 11552079,;
    cTotal="", bFixedPos=.t., cQueryName = "ADDESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=177, Top=127, InputMask=replicate('X',40)

  add object oADDESPRO_2_8 as StdTrsField with uid="FZADRDTIEG",rtseq=9,rtrep=.t.,;
    cFormVar="w_ADDESPRO",value=space(40),;
    HelpContextID = 229655893,;
    cTotal="", bFixedPos=.t., cQueryName = "ADDESPRO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=177, Top=153, InputMask=replicate('X',40)

  add object oStr_2_9 as StdString with uid="DOHUPCZRNO",Visible=.t., Left=81, Top=127,;
    Alignment=1, Width=90, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_10 as StdString with uid="AHOZMCIZZD",Visible=.t., Left=81, Top=153,;
    Alignment=1, Width=90, Height=15,;
    Caption="Produttore:"  ;
  , bGlobalFont=.t.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsut_madBodyRow as CPBodyRowCnt
  Width=543
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oADORDINE_2_1 as StdTrsField with uid="ZOVRKCQQMY",rtseq=1,rtrep=.t.,;
    cFormVar="w_ADORDINE",value=0,;
    HelpContextID = 171051701,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=35, Left=-2, Top=0, BackStyle=0

  add object oADCODICE_2_2 as StdTrsField with uid="ZKCYMLLOTJ",rtseq=2,rtrep=.t.,;
    cFormVar="w_ADCODICE",value=space(8),isprimarykey=.t.,;
    HelpContextID = 97137995,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Il codice dell'add-on / verticalizzazione deve essere lungo almeno 4 caratteri.",;
   bGlobalFont=.t.,;
    Height=17, Width=122, Left=38, Top=0, InputMask=replicate('X',8), BackStyle=0

  func oADCODICE_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (len(alltrim(.w_ADCODICE)) >= 4)
    endwith
    return bRes
  endfunc

  add object oAD__TIPO_2_3 as StdTrsCombo with uid="JVVJCFBHJE",rtrep=.t.,;
    cFormVar="w_AD__TIPO", RowSource=""+"Addon,"+"Verticalizzazione" , ;
    HelpContextID = 153356971,;
    Height=21, Width=113, Left=166, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , BackStyle=0;
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oAD__TIPO_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AD__TIPO,&i_cF..t_AD__TIPO),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'V',;
    space(1))))
  endfunc
  func oAD__TIPO_2_3.GetRadio()
    this.Parent.oContained.w_AD__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oAD__TIPO_2_3.ToRadio()
    this.Parent.oContained.w_AD__TIPO=trim(this.Parent.oContained.w_AD__TIPO)
    return(;
      iif(this.Parent.oContained.w_AD__TIPO=='A',1,;
      iif(this.Parent.oContained.w_AD__TIPO=='V',2,;
      0)))
  endfunc

  func oAD__TIPO_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oADABILIT_2_4 as StdTrsCombo with uid="HDBAELCQZL",rtrep=.t.,;
    cFormVar="w_ADABILIT", RowSource=""+"Si,"+"No" , ;
    HelpContextID = 116583078,;
    Height=21, Width=61, Left=284, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , BackStyle=0;
, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oADABILIT_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ADABILIT,&i_cF..t_ADABILIT),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oADABILIT_2_4.GetRadio()
    this.Parent.oContained.w_ADABILIT = this.RadioValue()
    return .t.
  endfunc

  func oADABILIT_2_4.ToRadio()
    this.Parent.oContained.w_ADABILIT=trim(this.Parent.oContained.w_ADABILIT)
    return(;
      iif(this.Parent.oContained.w_ADABILIT=='S',1,;
      iif(this.Parent.oContained.w_ADABILIT=='N',2,;
      0)))
  endfunc

  func oADABILIT_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oADPRIORI_2_5 as StdTrsCheck with uid="UCJTWPKHBB",rtrep=.t.,;
    cFormVar="w_ADPRIORI",  caption="",;
    HelpContextID = 203294031,;
    Left=397, Top=0, Width=18,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , BackStyle=0,AutoSize=.F.;
   , bGlobalFont=.t.


  func oADPRIORI_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..ADPRIORI,&i_cF..t_ADPRIORI),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oADPRIORI_2_5.GetRadio()
    this.Parent.oContained.w_ADPRIORI = this.RadioValue()
    return .t.
  endfunc

  func oADPRIORI_2_5.ToRadio()
    this.Parent.oContained.w_ADPRIORI=trim(this.Parent.oContained.w_ADPRIORI)
    return(;
      iif(this.Parent.oContained.w_ADPRIORI=='S',1,;
      0))
  endfunc

  func oADPRIORI_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oADNOMMOD_2_6 as StdTrsField with uid="YWTZXABTUR",rtseq=6,rtrep=.t.,;
    cFormVar="w_ADNOMMOD",value=space(8),;
    HelpContextID = 94706358,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=81, Left=457, Top=0, InputMask=replicate('X',8), BackStyle=0
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oADORDINE_2_1.When()
    return(.t.)
  proc oADORDINE_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oADORDINE_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=4
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_mad','ADDON','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ADFLDSER=ADDON.ADFLDSER";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
