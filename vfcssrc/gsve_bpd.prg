* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bpd                                                        *
*              Elimina movimento di provvigione                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_40]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-12-14                                                      *
* Last revis.: 2013-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bpd",oParentObject,m.pTipo)
return(i_retval)

define class tgsve_bpd as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_OREC = 0
  w_nAbsRow = 0
  w_nRelRow = 0
  w_LIQ = .f.
  * --- WorkFile variables
  DOC_MAST_idx=0
  MOP_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina il Riferimento della provvigione sul Documento di Origine (da GSVE_MPV)
    * --- Sospende il movimento se l'agente � obsoleto alla data di registrazione
    do case
      case this.pTipo="C"
        if NOT EMPTY(this.oParentObject.w_MPRIFDOC)
          * --- Write into DOC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVGENPRO ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVGENPRO');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MPRIFDOC);
                   )
          else
            update (i_cTable) set;
                MVGENPRO = " ";
                &i_ccchkf. ;
             where;
                MVSERIAL = this.oParentObject.w_MPRIFDOC;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.pTipo$"I-F"
        this.w_LIQ = .F.
        * --- Se il movimento � stato liquidato anche in parte nn pu� essere sospeso
        SELECT (this.oParentObject.cTrsName)
        this.w_OREC = IIF(Eof(),RECNO(This.oParentObject.cTrsname)-1,RECNO(this.oParentObject.cTrsname))
        this.w_nAbsRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nAbsRow
        this.w_nRelRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nRelRow
        GO TOP
        SCAN FOR NOT EMPTY(t_MPTOTIMP)
        * --- Legge i Dati del Temporaneo
        this.oParentObject.WorkFromTrs()
        this.w_LIQ = IIF(NOT EMPTY(this.oParentObject.w_MPDATLIQ),.T.,this.w_LIQ)
        * --- Carica il Temporaneo dei Dati
        this.oParentObject.TrsFromWork()
        * --- Flag Notifica Riga Variata
        if i_SRV<>"A"
          replace i_SRV with "U"
        endif
        ENDSCAN
        * --- Questa Parte derivata dal Metodo LoadRec
        if this.w_OREC>0 AND this.w_OREC<=RECCOUNT()
          GOTO this.w_OREC
        else
          GO TOP
        endif
        With this.oParentObject
        .WorkFromTrs()
        .SaveDependsOn()
        .SetControlsValue()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=this.w_NABSROW
        .oPgFrm.Page1.oPag.oBody.nRelRow=this.w_NRELROW
        EndWith
        if this.w_LIQ=.T.
          if this.pTipo="F"
            if this.oParentObject.w_MPAGSOSP="S"
              ah_ErrorMsg("Impossibile sospendere i movimenti provvigioni gi� liquidati")
              this.oParentObject.w_MPAGSOSP = "C"
            endif
          else
            if this.oParentObject.w_DATOBSO<=this.oParentObject.w_MPDATREG AND NOT EMPTY(this.oParentObject.w_DATOBSO) AND this.oParentObject.w_MPAGSOSP<>"S"
              ah_ErrorMsg("Agente obsoleto alla data di registrazione")
            endif
          endif
        else
          if this.oParentObject.w_DATOBSO<=this.oParentObject.w_MPDATREG AND NOT EMPTY(this.oParentObject.w_DATOBSO) AND this.oParentObject.w_MPAGSOSP<>"S"
            ah_ErrorMsg("Agente obsoleto alla data di registrazione")
            this.oParentObject.w_MPAGSOSP = "S"
          endif
        endif
      case this.pTipo="S"
         
 Select(THIS.OPARENTOBJECT.cTrsName) 
 SUM FOR Not Deleted() T_MPTOTIMP , T_MPTOTIM2 To this.oParentObject.w_TOTIMP, this.oParentObject.w_TOTIMPC
        if this.oParentObject.w_TOTIMP<>this.oParentObject.w_TOTIMPC AND NOT EMPTY(this.oParentObject.w_MPCODCAP) AND (this.oParentObject.w_TOTIMPC<>0 OR this.oParentObject.w_TOTAREA<>0)
          if !ah_Yesno("Attenzione: il totale imponibile agente differisce dal totale imponibile capoarea, confermi ugualmente?")
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=ah_Msgformat("Operazione interrotta")
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='MOP_DETT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
