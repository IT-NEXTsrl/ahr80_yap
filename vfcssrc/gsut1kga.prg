* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut1kga                                                        *
*              Conversioni da rieseguire                                       *
*                                                                              *
*      Author: Zucchetti TAM SpA                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_75]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-11-09                                                      *
* Last revis.: 2009-11-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut1kga
if not cp_IsAdministrator(.F.)
   Ah_ErrorMsg("Procedura utilizzabile solo da utenti con privilegi di amministratore%0Rivolgersi al proprio amministratore di sistema",'!')
   return
endif

* --- Fine Area Manuale
return(createobject("tgsut1kga",oParentObject))

* --- Class definition
define class tgsut1kga as StdForm
  Top    = 1
  Left   = 3

  * --- Standard Properties
  Width  = 772
  Height = 410
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-11-09"
  HelpContextID=233356439
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  AHEPATCH_IDX = 0
  CONVERSI_IDX = 0
  cPrg = "gsut1kga"
  cComment = "Conversioni da rieseguire"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_SELEZI = space(1)
  w_CODREL = space(15)
  w_ORDINE = space(5)
  w_LMESSAGGIO = space(0)
  w_CalcZoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut1kgaPag1","gsut1kga",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSELEZI_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_CalcZoom = this.oPgFrm.Pages(1).oPag.CalcZoom
    DoDefault()
    proc Destroy()
      this.w_CalcZoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='AHEPATCH'
    this.cWorkTables[2]='CONVERSI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SELEZI=space(1)
      .w_CODREL=space(15)
      .w_ORDINE=space(5)
      .w_LMESSAGGIO=space(0)
      .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .w_SELEZI = 'D'
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .w_CODREL = .w_CalcZoom.GetVar('COCODREL')
        .w_ORDINE = .w_CalcZoom.GetVar('COORDINE')
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_ORDINE))
          .link_1_11('Full')
        endif
    endwith
    this.DoRTCalc(4,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
        .DoRTCalc(1,1,.t.)
            .w_CODREL = .w_CalcZoom.GetVar('COCODREL')
            .w_ORDINE = .w_CalcZoom.GetVar('COORDINE')
          .link_1_11('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.CalcZoom.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.CalcZoom.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ORDINE
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONVERSI_IDX,3]
    i_lTable = "CONVERSI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONVERSI_IDX,2], .t., this.CONVERSI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONVERSI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ORDINE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ORDINE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODREL,COORDINE,COERRORE";
                   +" from "+i_cTable+" "+i_lTable+" where COORDINE="+cp_ToStrODBC(this.w_ORDINE);
                   +" and COCODREL="+cp_ToStrODBC(this.w_CODREL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODREL',this.w_CODREL;
                       ,'COORDINE',this.w_ORDINE)
            select COCODREL,COORDINE,COERRORE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ORDINE = NVL(_Link_.COORDINE,space(5))
      this.w_LMESSAGGIO = NVL(_Link_.COERRORE,space(0))
    else
      if i_cCtrl<>'Load'
        this.w_ORDINE = space(5)
      endif
      this.w_LMESSAGGIO = space(0)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONVERSI_IDX,2])+'\'+cp_ToStr(_Link_.COCODREL,1)+'\'+cp_ToStr(_Link_.COORDINE,1)
      cp_ShowWarn(i_cKey,this.CONVERSI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ORDINE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_5.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLMESSAGGIO_1_12.value==this.w_LMESSAGGIO)
      this.oPgFrm.Page1.oPag.oLMESSAGGIO_1_12.value=this.w_LMESSAGGIO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut1kgaPag1 as StdContainer
  Width  = 768
  height = 410
  stdWidth  = 768
  stdheight = 410
  resizeXpos=222
  resizeYpos=322
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_2 as StdButton with uid="TJGKSVEUGA",left=662, top=7, width=48,height=45,;
    CpPicture="BMP\ORIGINE.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire manutenzione tabella conversioni";
    , HelpContextID = 48677862;
    , Caption='\<Origine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      with this.Parent.oContained
        GSUT1BGA(this.Parent.oContained,"ORIG")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_3 as StdButton with uid="FPAUDSVDKI",left=714, top=7, width=48,height=45,;
    CpPicture="BMP\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Riesegue l'interrogazione";
    , HelpContextID = 143297027;
    , Caption='\<Interroga', tabstop=.f.;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      with this.Parent.oContained
        GSUT1BGA(this.Parent.oContained,"CALC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object CalcZoom as cp_szoombox with uid="ZRNYOQOCKV",left=1, top=59, width=765,height=284,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="CONVERSI",cZoomFile="GSUT1KGA",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Legge",;
    nPag=1;
    , HelpContextID = 125516826

  add object oSELEZI_1_5 as StdRadio with uid="NIGCHCKNSU",rtseq=1,rtrep=.f.,left=11, top=351, width=140,height=32;
    , ToolTipText = "Seleziona/deseleziona tutti i record alla cancellazione";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_5.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 53295322
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 53295322
      this.Buttons(2).Top=15
      this.SetAll("Width",138)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutti i record alla cancellazione")
      StdRadio::init()
    endproc

  func oSELEZI_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_5.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_5.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oBtn_1_6 as StdButton with uid="JDOTXLNMID",left=714, top=361, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 240673862;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_7 as cp_runprogram with uid="AOQROZBYTO",left=13, top=435, width=214,height=18,;
    caption='GSUT1BGA',;
   bGlobalFont=.t.,;
    prg="GSUT1BGA('SELE')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 55731367


  add object oObj_1_8 as cp_runprogram with uid="NYAJYHSJEK",left=231, top=435, width=183,height=18,;
    caption='GSUT1BGA',;
   bGlobalFont=.t.,;
    prg="GSUT1BGA('CALC')",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 55731367


  add object oBtn_1_9 as StdButton with uid="HTWZHBFLOY",left=662, top=361, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per impostare da rieseguire le conversioni selezionate";
    , HelpContextID = 233385190;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        GSUT1BGA(this.Parent.oContained,"AGGIO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oLMESSAGGIO_1_12 as StdMemo with uid="EMRMYUICEY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_LMESSAGGIO", cQueryName = "LMESSAGGIO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 74494605,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=54, Width=395, Left=171, Top=351

  add object oStr_1_1 as StdString with uid="RLJHJAMHNI",Visible=.t., Left=11, Top=11,;
    Alignment=0, Width=382, Height=18,;
    Caption="Selezionare le procedure di conversioni che devono essere rieseguite"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut1kga','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
