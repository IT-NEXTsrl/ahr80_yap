* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kab                                                        *
*              Apertura di bilancio                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_76]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-04                                                      *
* Last revis.: 2010-11-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kab",oParentObject))

* --- Class definition
define class tgscg_kab as StdForm
  Top    = 6
  Left   = 33

  * --- Standard Properties
  Width  = 616
  Height = 228
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-11-17"
  HelpContextID=132785513
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  CAU_CONT_IDX = 0
  CONTI_IDX = 0
  ESERCIZI_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gscg_kab"
  cComment = "Apertura di bilancio"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODESE = space(4)
  w_TIPCON = space(1)
  w_CODAZI = space(5)
  w_DATREG = ctod('  /  /  ')
  w_CODCAU = space(5)
  w_DESCAU = space(35)
  w_FLSALF = space(1)
  w_FLSALI = space(1)
  w_TIPREG = space(1)
  w_NUMREG = 0
  w_CODCON = space(15)
  w_CODARR = space(15)
  w_DESCON = space(40)
  w_CONSUP = space(15)
  w_DESSUP = space(45)
  w_CONFER = space(1)
  w_STALIG = ctod('  /  /  ')
  w_INIESE = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_DESCON1 = space(40)
  w_PERVAL = space(3)
  w_OBTEST = ctod('  /  /  ')
  w_CONSUP1 = space(15)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kabPag1","gscg_kab",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODESE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='AZIENDA'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCG_BAB with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_kab
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODESE=space(4)
      .w_TIPCON=space(1)
      .w_CODAZI=space(5)
      .w_DATREG=ctod("  /  /  ")
      .w_CODCAU=space(5)
      .w_DESCAU=space(35)
      .w_FLSALF=space(1)
      .w_FLSALI=space(1)
      .w_TIPREG=space(1)
      .w_NUMREG=0
      .w_CODCON=space(15)
      .w_CODARR=space(15)
      .w_DESCON=space(40)
      .w_CONSUP=space(15)
      .w_DESSUP=space(45)
      .w_CONFER=space(1)
      .w_STALIG=ctod("  /  /  ")
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_DESCON1=space(40)
      .w_PERVAL=space(3)
      .w_OBTEST=ctod("  /  /  ")
      .w_CONSUP1=space(15)
        .w_CODESE = g_CODESE
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODESE))
          .link_1_1('Full')
        endif
        .w_TIPCON = 'G'
        .w_CODAZI = i_CODAZI
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODAZI))
          .link_1_3('Full')
        endif
        .w_DATREG = i_DATSYS
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODCAU))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,11,.f.)
        if not(empty(.w_CODCON))
          .link_1_11('Full')
        endif
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_CODARR))
          .link_1_12('Full')
        endif
          .DoRTCalc(13,21,.f.)
        .w_OBTEST = i_INIDAT
    endwith
    this.DoRTCalc(23,23,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .link_1_3('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,23,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODESE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_1'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESFINESE,ESINIESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_FINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_PERVAL = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_FINESE = ctod("  /  /  ")
      this.w_INIESE = ctod("  /  /  ")
      this.w_PERVAL = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZSTALIG";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZSTALIG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_STALIG = NVL(cp_ToDate(_Link_.AZSTALIG),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_STALIG = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAU
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLSALF,CCFLSALI,CCNUMREG,CCTIPREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CODCAU))
          select CCCODICE,CCDESCRI,CCFLSALF,CCFLSALI,CCNUMREG,CCTIPREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLSALF,CCFLSALI,CCNUMREG,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CODCAU)+"%");

            select CCCODICE,CCDESCRI,CCFLSALF,CCFLSALI,CCNUMREG,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCODCAU_1_5'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCG_ZAB.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLSALF,CCFLSALI,CCNUMREG,CCTIPREG";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLSALF,CCFLSALI,CCNUMREG,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLSALF,CCFLSALI,CCNUMREG,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CODCAU)
            select CCCODICE,CCDESCRI,CCFLSALF,CCFLSALI,CCNUMREG,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLSALF = NVL(_Link_.CCFLSALF,space(1))
      this.w_FLSALI = NVL(_Link_.CCFLSALI,space(1))
      this.w_NUMREG = NVL(_Link_.CCNUMREG,0)
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAU = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_FLSALF = space(1)
      this.w_FLSALI = space(1)
      this.w_NUMREG = 0
      this.w_TIPREG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLSALI='S' AND .w_FLSALF<>'S' AND .w_TIPREG='N' AND .w_NUMREG=0
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente o non corretta! (Solo aggiornamento saldo iniziale)")
        endif
        this.w_CODCAU = space(5)
        this.w_DESCAU = space(35)
        this.w_FLSALF = space(1)
        this.w_FLSALI = space(1)
        this.w_NUMREG = 0
        this.w_TIPREG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_11'),i_cWhere,'GSAR_BZC',"Conti",'GSCG_ZPT.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto di riepilogo inesistente o incongruente! (Selezionare un conto transitorio)")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_CONSUP = NVL(_Link_.ANCONSUP,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_CONSUP = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CALCSEZ(.w_CONSUP)='T'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto di riepilogo inesistente o incongruente! (Selezionare un conto transitorio)")
        endif
        this.w_CODCON = space(15)
        this.w_DESCON = space(40)
        this.w_CONSUP = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODARR
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODARR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODARR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODARR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODARR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODARR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODARR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODARR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODARR_1_12'),i_cWhere,'GSAR_BZC',"Conti",'GSCG1ZPT.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto differenze di conversione inesistente o incongruente! (Selezionare un conto passivitÓ)")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODARR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODARR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODARR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODARR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_CONSUP1 = NVL(_Link_.ANCONSUP,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_CODARR = space(15)
      endif
      this.w_DESCON1 = space(40)
      this.w_CONSUP1 = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CALCSEZ(.w_CONSUP1)='P'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto differenze di conversione inesistente o incongruente! (Selezionare un conto passivitÓ)")
        endif
        this.w_CODARR = space(15)
        this.w_DESCON1 = space(40)
        this.w_CONSUP1 = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODARR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_1.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_1.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG_1_4.value==this.w_DATREG)
      this.oPgFrm.Page1.oPag.oDATREG_1_4.value=this.w_DATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAU_1_5.value==this.w_CODCAU)
      this.oPgFrm.Page1.oPag.oCODCAU_1_5.value=this.w_CODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_6.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_6.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_11.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_11.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCODARR_1_12.value==this.w_CODARR)
      this.oPgFrm.Page1.oPag.oCODARR_1_12.value=this.w_CODARR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_13.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_13.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSUP_1_15.value==this.w_DESSUP)
      this.oPgFrm.Page1.oPag.oDESSUP_1_15.value=this.w_DESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON1_1_28.value==this.w_DESCON1)
      this.oPgFrm.Page1.oPag.oDESCON1_1_28.value=this.w_DESCON1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODESE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATREG_1_4.SetFocus()
            i_bnoObbl = !empty(.w_DATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CODCAU)) or not(.w_FLSALI='S' AND .w_FLSALF<>'S' AND .w_TIPREG='N' AND .w_NUMREG=0))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCAU_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CODCAU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente o non corretta! (Solo aggiornamento saldo iniziale)")
          case   ((empty(.w_CODCON)) or not(CALCSEZ(.w_CONSUP)='T'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON_1_11.SetFocus()
            i_bnoObbl = !empty(.w_CODCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto di riepilogo inesistente o incongruente! (Selezionare un conto transitorio)")
          case   not(CALCSEZ(.w_CONSUP1)='P')  and not(empty(.w_CODARR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODARR_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto differenze di conversione inesistente o incongruente! (Selezionare un conto passivitÓ)")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_kabPag1 as StdContainer
  Width  = 612
  height = 228
  stdWidth  = 612
  stdheight = 228
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODESE_1_1 as StdField with uid="GDZVKJGYJX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza contabile da aprire",;
    HelpContextID = 225481178,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=188, Top=10, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oDATREG_1_4 as StdField with uid="DXMWYXPCZB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATREG", cQueryName = "DATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della registrazione di apertura",;
    HelpContextID = 205692874,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=188, Top=39

  add object oCODCAU_1_5 as StdField with uid="ZCFBQVEWVF",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODCAU", cQueryName = "CODCAU",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente o non corretta! (Solo aggiornamento saldo iniziale)",;
    ToolTipText = "Causale contabile per l'apertura dello stato patrimoniale",;
    HelpContextID = 244486618,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=188, Top=69, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CODCAU"

  func oCODCAU_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAU_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAU_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCODCAU_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCG_ZAB.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCODCAU_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CODCAU
     i_obj.ecpSave()
  endproc

  add object oDESCAU_1_6 as StdField with uid="OIKBACAVFT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 244427722,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=327, Top=69, InputMask=replicate('X',35)

  add object oCODCON_1_11 as StdField with uid="PHAWLWJXZL",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto di riepilogo inesistente o incongruente! (Selezionare un conto transitorio)",;
    ToolTipText = "Conto di riepilogo dello stato patrimoniale iniziale (sez.bilancio=transitori)",;
    HelpContextID = 78811610,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=188, Top=99, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'GSCG_ZPT.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oCODARR_1_12 as StdField with uid="YJFSYLYQPC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODARR", cQueryName = "CODARR",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto differenze di conversione inesistente o incongruente! (Selezionare un conto passivitÓ)",;
    ToolTipText = "Eventuale arrotondamento dovuto a cambio",;
    HelpContextID = 8688090,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=188, Top=127, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODARR"

  func oCODARR_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODARR_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODARR_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODARR_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'GSCG1ZPT.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODARR_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODARR
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_13 as StdField with uid="JDEHTMPBED",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 78752714,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=327, Top=99, InputMask=replicate('X',40)

  add object oDESSUP_1_15 as StdField with uid="QTMJUFCSLA",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESSUP", cQueryName = "DESSUP",;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione aggiuntiva dell'operazione",;
    HelpContextID = 37858250,;
   bGlobalFont=.t.,;
    Height=21, Width=324, Left=188, Top=156, InputMask=replicate('X',45)


  add object oBtn_1_26 as StdButton with uid="HSZEBYCTXQ",left=507, top=180, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la riapertura dello stato patrimoniale";
    , HelpContextID = 132756762;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        do GSCG_BAB with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Abilita(this.Parent.oContained))
      endwith
    endif
  endfunc


  add object oBtn_1_27 as StdButton with uid="GQPNXZUTJS",left=558, top=180, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare";
    , HelpContextID = 125468090;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCON1_1_28 as StdField with uid="XSYLWADQTI",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCON1", cQueryName = "DESCON1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 78752714,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=327, Top=127, InputMask=replicate('X',40)

  add object oStr_1_17 as StdString with uid="FMWHRIQLNP",Visible=.t., Left=29, Top=39,;
    Alignment=1, Width=158, Height=15,;
    Caption="Data registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="LUIVISYPAD",Visible=.t., Left=29, Top=69,;
    Alignment=1, Width=158, Height=15,;
    Caption="Causale contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="CTPMXGSZLM",Visible=.t., Left=29, Top=156,;
    Alignment=1, Width=158, Height=15,;
    Caption="Descrizione agg.va:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="BXKELOKTRF",Visible=.t., Left=29, Top=10,;
    Alignment=1, Width=158, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="GIXVPBSUKD",Visible=.t., Left=314, Top=10,;
    Alignment=0, Width=290, Height=15,;
    Caption="(Esercizio di competenza da aprire)"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="UUAAZWGXOS",Visible=.t., Left=29, Top=99,;
    Alignment=1, Width=158, Height=15,;
    Caption="Conto bil. di apertura:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="KKAPVPKGFL",Visible=.t., Left=8, Top=127,;
    Alignment=1, Width=179, Height=15,;
    Caption="Differenze di conversione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kab','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_kab
Function Abilita
  parameters padre
  if empty(padre.w_CODCON) OR empty(padre.w_CODCAU)OR empty(padre.w_DATREG) OR empty(padre.w_CODESE) 
      Return .F.
  else
      Return .T.
  endif
endfunc 
* --- Fine Area Manuale
