* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_kim                                                        *
*              Importa documenti                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_214]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-14                                                      *
* Last revis.: 2015-10-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsve_kim
L_Padre=oparentobject
if Upper(oparentobject.CLASS)='TGSAR_MPG'
 oparentobject=oparentobject.w_OBJ_GEST
endif
* --- Fine Area Manuale
return(createobject("tgsve_kim",oParentObject))

* --- Class definition
define class tgsve_kim as StdForm
  Top    = 0
  Left   = 16

  * --- Standard Properties
  Width  = 804
  Height = 509+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-27"
  HelpContextID=1428887
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=189

  * --- Constant Properties
  _IDX = 0
  DOC_COLL_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  CONTI_IDX = 0
  TIP_DOCU_IDX = 0
  MAGAZZIN_IDX = 0
  CAN_TIER_IDX = 0
  CENCOST_IDX = 0
  ATTIVITA_IDX = 0
  VOC_COST_IDX = 0
  DIPENDEN_IDX = 0
  DES_DIVE_IDX = 0
  CPAR_DEF_IDX = 0
  CAM_AGAZ_IDX = 0
  cPrg = "gsve_kim"
  cComment = "Importa documenti"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CHIAVE = space(10)
  w_PCAUMAG = space(5)
  w_TDFLNORC = space(1)
  w_MVDATDOC = ctod('  /  /  ')
  w_TESTERR = .F.
  w_OBTEST = ctod('  /  /  ')
  w_MVFLSCOR = space(1)
  w_MVTIPDOC = space(5)
  w_FLGCOM = space(1)
  w_SOTCAT = space(1)
  w_FILCOMM = space(15)
  w_FLRICIVA = .F.
  o_FLRICIVA = .F.
  w_FLNSRI = space(1)
  w_FLVSRI = space(1)
  w_FLRIDE = space(1)
  w_FILSPE = space(1)
  w_FILANT = space(1)
  w_FILGEN = space(1)
  w_FILDIR = space(1)
  w_FILONO = space(1)
  w_FILTEM = space(1)
  w_FLCAUMAG = space(1)
  w_PARCAMAG = space(5)
  w_FLMGPR = space(1)
  w_MAGTP = space(5)
  w_COMAG = space(5)
  w_FLMTPR = space(1)
  w_FLFOM = space(1)
  w_FLCOAC = space(1)
  w_COMAT = space(5)
  w_MAGTC = space(5)
  w_NASCONDIRIGHE = .F.
  w_CALSER = space(10)
  o_CALSER = space(10)
  w_TIPO = space(1)
  w_SERIAL = space(10)
  w_MVQTAEVA = 0
  w_MVIMPEVA = 0
  w_MVTIPCON = space(1)
  w_MVCODCON = space(15)
  w_MVFLVEAC = space(1)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_TIPORN = space(1)
  w_CODORN = space(15)
  w_FLVEAC = space(1)
  w_TIPDOC = space(5)
  w_CAUCOL = space(5)
  w_FLSCOR = space(1)
  w_ESCL1 = space(3)
  w_ESCL2 = space(3)
  w_ESCL3 = space(3)
  w_ESCL4 = space(3)
  w_ESCL5 = space(3)
  w_DTOBSO = ctod('  /  /  ')
  w_TipoRisorsa = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_EVAINI = ctod('  /  /  ')
  w_EVAFIN = ctod('  /  /  ')
  w_FLTRASF = space(1)
  w_FILART = space(20)
  w_FILARM = space(20)
  w_FILMAG = space(5)
  w_FILCAU = space(5)
  o_FILCAU = space(5)
  w_CLIFOR = space(15)
  o_CLIFOR = space(15)
  w_DESFAR = space(40)
  w_DESARM = space(40)
  w_FLVEAC1 = space(1)
  o_FLVEAC1 = space(1)
  w_COLLEG = space(5)
  w_CFCOLL = space(1)
  w_FLINTE = space(1)
  w_FILCLI = space(15)
  o_FILCLI = space(15)
  w_DESCLI = space(40)
  w_CODDES = space(5)
  w_DESCAU = space(35)
  w_MAGTER = space(5)
  w_TIPART = space(1)
  w_DESART = space(40)
  w_SELEZI = space(1)
  w_SELEZ1 = space(1)
  w_FILCOMM = space(15)
  w_DESCOM = space(40)
  w_FILCEN = space(15)
  w_DESCEN = space(40)
  w_FILATTI = space(15)
  w_DESATTI = space(40)
  w_FILVOC = space(15)
  w_DESVOC = space(40)
  w_DESMAG = space(30)
  w_FLSCAF = space(1)
  w_BLOCKMAST = 0
  w_ERR = .F.
  w_ERRGEN = .F.
  w_MVCLADOC = space(2)
  w_FLGROR = space(1)
  w_SCOPE = 0
  w_DESDOC1 = space(250)
  w_DESDOC = space(250)
  o_DESDOC = space(250)
  w_GESTPARENT = .F.
  w_MVFLSCOM = space(1)
  w_NOMECUR = space(10)
  w_FILPRA = space(1)
  w_CAMPO = space(20)
  w_DESCOM = space(40)
  w_FLPRAT = space(1)
  w_FILRES = space(5)
  w_DENOM_PART = space(100)
  w_COGNPART = space(40)
  w_NOMEPART = space(40)
  w_PREINI = ctod('  /  /  ')
  w_PREFIN = ctod('  /  /  ')
  w_TDFLAPCA = space(10)
  w_ROWNUM = space(10)
  w_TIDODE = space(1)
  w_VISPRAT = space(1)
  w_RECTRS = 0
  w_MVCODVAL = space(3)
  w_FLIMPA = space(1)
  w_FLIMAC = space(1)
  w_MVCODIVE = space(3)
  w_MVRIFDIC = space(10)
  w_MVCODPAG = space(5)
  w_MVDATDIV = ctod('  /  /  ')
  w_MVCODBA2 = space(15)
  w_MVCODAG2 = space(5)
  w_MVIVAINC = space(5)
  w_MVIVAIMB = space(5)
  w_MVIVATRA = space(5)
  w_MVIVABOL = space(5)
  w_MVIVACAU = space(5)
  w_MVCODVET = space(5)
  w_MVCODVE2 = space(5)
  w_MVCODVE3 = space(5)
  w_MVCODPOR = space(1)
  w_MVCODSPE = space(3)
  w_MVCONCON = space(1)
  w_MVCODDES = space(5)
  w_MVCODORN = space(15)
  w_MVCODSED = space(5)
  w_MVCODAGE = space(5)
  w_MVCODBAN = space(10)
  w_MVSCOCL1 = 0
  w_MVSCOCL2 = 0
  w_MVSCOPAG = 0
  w_GIAIMP = space(1)
  w_OB_TEST = ctod('  /  /  ')
  w_VISUALIZZACALCOLA = .F.
  w_RIIVDTIN = ctod('  /  /  ')
  o_RIIVDTIN = ctod('  /  /  ')
  w_RIIVDTFI = ctod('  /  /  ')
  w_RIIVLSIV = space(0)
  w_FLGZER = space(1)
  w_MVAGG_01 = space(15)
  w_MVAGG_02 = space(15)
  w_MVAGG_03 = space(15)
  w_MVAGG_04 = space(15)
  w_MVAGG_05 = ctod('  /  /  ')
  w_MVAGG_06 = ctod('  /  /  ')
  w_TDFLIA01 = space(1)
  w_TDFLIA02 = space(1)
  w_TDFLIA03 = space(1)
  w_TDFLIA04 = space(1)
  w_TDFLIA05 = space(1)
  w_TDFLIA06 = space(1)
  w_TDFLRA01 = space(1)
  w_TDFLRA02 = space(1)
  w_TDFLRA03 = space(1)
  w_TDFLRA04 = space(1)
  w_TDFLRA05 = space(1)
  w_TDFLRA06 = space(1)
  w_DACAM_01 = space(30)
  w_DACAM_02 = space(30)
  w_DACAM_03 = space(30)
  w_DACAM_04 = space(30)
  w_DACAM_05 = space(30)
  w_DACAM_06 = space(30)
  w_TSTDAAG1 = space(6)
  w_TSTDAAG2 = space(6)
  w_CUR_NAME = space(10)
  w_TDMINVEN = space(1)
  w_TDRIOTOT = space(1)
  w_OLDQTAEVA = 0
  w_DESDES = space(40)
  w_TIPDORI = space(1)
  w_NUMDOCORI = 0
  w_ALFDOCORI = space(10)
  w_DATDOCORI = ctod('  /  /  ')
  w_DESCAMAG = space(35)
  w_TFLCOMM = space(1)
  w_FLCOMM = space(1)
  w_ZoomMast = .NULL.
  w_ZoomDett = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsve_kim
  *istanzio l'oggetto nella maschera perch� viene utilizzato nei 
  * batch gsve_bi1 e gsve_bi4 per il lancio dello stesso report
  w_oERRORLOG=NULL
  cVqrCheck = '' && Se piena Query utilizzata per check testate su conferma, alternativa al cursore sullo zoom di sinistra
  
  Procedure ModificaZoom( p_azione )
    if upper( p_azione ) = "NASCONDIRIGHE"
      this.w_ZoomDett.visible = .f.
      this.w_NASCONDIRIGHE = .t.
      this.w_zoommast.width = this.width
    endif
    if upper( p_azione ) = "MOSTRARIGHE"
      this.w_ZoomDett.visible = .t.
      this.w_NASCONDIRIGHE = .f.
      this.w_zoommast.width = this.w_zoomdett.left + 12
    endif
  endproc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_kimPag1","gsve_kim",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Documenti")
      .Pages(2).addobject("oPag","tgsve_kimPag2","gsve_kim",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Ulteriori selezioni")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFILCOMM_1_11
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsve_kim
    if VARTYPE(g_SCHEDULER)='C' AND g_SCHEDULER='S'
       this.parent.left=_screen.width+1
    endif
    
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomMast = this.oPgFrm.Pages(1).oPag.ZoomMast
    this.w_ZoomDett = this.oPgFrm.Pages(1).oPag.ZoomDett
    DoDefault()
    proc Destroy()
      this.w_ZoomMast = .NULL.
      this.w_ZoomDett = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[14]
    this.cWorkTables[1]='DOC_COLL'
    this.cWorkTables[2]='KEY_ARTI'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='TIP_DOCU'
    this.cWorkTables[6]='MAGAZZIN'
    this.cWorkTables[7]='CAN_TIER'
    this.cWorkTables[8]='CENCOST'
    this.cWorkTables[9]='ATTIVITA'
    this.cWorkTables[10]='VOC_COST'
    this.cWorkTables[11]='DIPENDEN'
    this.cWorkTables[12]='DES_DIVE'
    this.cWorkTables[13]='CPAR_DEF'
    this.cWorkTables[14]='CAM_AGAZ'
    return(this.OpenAllTables(14))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CHIAVE=space(10)
      .w_PCAUMAG=space(5)
      .w_TDFLNORC=space(1)
      .w_MVDATDOC=ctod("  /  /  ")
      .w_TESTERR=.f.
      .w_OBTEST=ctod("  /  /  ")
      .w_MVFLSCOR=space(1)
      .w_MVTIPDOC=space(5)
      .w_FLGCOM=space(1)
      .w_SOTCAT=space(1)
      .w_FILCOMM=space(15)
      .w_FLRICIVA=.f.
      .w_FLNSRI=space(1)
      .w_FLVSRI=space(1)
      .w_FLRIDE=space(1)
      .w_FILSPE=space(1)
      .w_FILANT=space(1)
      .w_FILGEN=space(1)
      .w_FILDIR=space(1)
      .w_FILONO=space(1)
      .w_FILTEM=space(1)
      .w_FLCAUMAG=space(1)
      .w_PARCAMAG=space(5)
      .w_FLMGPR=space(1)
      .w_MAGTP=space(5)
      .w_COMAG=space(5)
      .w_FLMTPR=space(1)
      .w_FLFOM=space(1)
      .w_FLCOAC=space(1)
      .w_COMAT=space(5)
      .w_MAGTC=space(5)
      .w_NASCONDIRIGHE=.f.
      .w_CALSER=space(10)
      .w_TIPO=space(1)
      .w_SERIAL=space(10)
      .w_MVQTAEVA=0
      .w_MVIMPEVA=0
      .w_MVTIPCON=space(1)
      .w_MVCODCON=space(15)
      .w_MVFLVEAC=space(1)
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .w_TIPORN=space(1)
      .w_CODORN=space(15)
      .w_FLVEAC=space(1)
      .w_TIPDOC=space(5)
      .w_CAUCOL=space(5)
      .w_FLSCOR=space(1)
      .w_ESCL1=space(3)
      .w_ESCL2=space(3)
      .w_ESCL3=space(3)
      .w_ESCL4=space(3)
      .w_ESCL5=space(3)
      .w_DTOBSO=ctod("  /  /  ")
      .w_TipoRisorsa=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_EVAINI=ctod("  /  /  ")
      .w_EVAFIN=ctod("  /  /  ")
      .w_FLTRASF=space(1)
      .w_FILART=space(20)
      .w_FILARM=space(20)
      .w_FILMAG=space(5)
      .w_FILCAU=space(5)
      .w_CLIFOR=space(15)
      .w_DESFAR=space(40)
      .w_DESARM=space(40)
      .w_FLVEAC1=space(1)
      .w_COLLEG=space(5)
      .w_CFCOLL=space(1)
      .w_FLINTE=space(1)
      .w_FILCLI=space(15)
      .w_DESCLI=space(40)
      .w_CODDES=space(5)
      .w_DESCAU=space(35)
      .w_MAGTER=space(5)
      .w_TIPART=space(1)
      .w_DESART=space(40)
      .w_SELEZI=space(1)
      .w_SELEZ1=space(1)
      .w_FILCOMM=space(15)
      .w_DESCOM=space(40)
      .w_FILCEN=space(15)
      .w_DESCEN=space(40)
      .w_FILATTI=space(15)
      .w_DESATTI=space(40)
      .w_FILVOC=space(15)
      .w_DESVOC=space(40)
      .w_DESMAG=space(30)
      .w_FLSCAF=space(1)
      .w_BLOCKMAST=0
      .w_ERR=.f.
      .w_ERRGEN=.f.
      .w_MVCLADOC=space(2)
      .w_FLGROR=space(1)
      .w_SCOPE=0
      .w_DESDOC1=space(250)
      .w_DESDOC=space(250)
      .w_GESTPARENT=.f.
      .w_MVFLSCOM=space(1)
      .w_NOMECUR=space(10)
      .w_FILPRA=space(1)
      .w_CAMPO=space(20)
      .w_DESCOM=space(40)
      .w_FLPRAT=space(1)
      .w_FILRES=space(5)
      .w_DENOM_PART=space(100)
      .w_COGNPART=space(40)
      .w_NOMEPART=space(40)
      .w_PREINI=ctod("  /  /  ")
      .w_PREFIN=ctod("  /  /  ")
      .w_TDFLAPCA=space(10)
      .w_ROWNUM=space(10)
      .w_TIDODE=space(1)
      .w_VISPRAT=space(1)
      .w_RECTRS=0
      .w_MVCODVAL=space(3)
      .w_FLIMPA=space(1)
      .w_FLIMAC=space(1)
      .w_MVCODIVE=space(3)
      .w_MVRIFDIC=space(10)
      .w_MVCODPAG=space(5)
      .w_MVDATDIV=ctod("  /  /  ")
      .w_MVCODBA2=space(15)
      .w_MVCODAG2=space(5)
      .w_MVIVAINC=space(5)
      .w_MVIVAIMB=space(5)
      .w_MVIVATRA=space(5)
      .w_MVIVABOL=space(5)
      .w_MVIVACAU=space(5)
      .w_MVCODVET=space(5)
      .w_MVCODVE2=space(5)
      .w_MVCODVE3=space(5)
      .w_MVCODPOR=space(1)
      .w_MVCODSPE=space(3)
      .w_MVCONCON=space(1)
      .w_MVCODDES=space(5)
      .w_MVCODORN=space(15)
      .w_MVCODSED=space(5)
      .w_MVCODAGE=space(5)
      .w_MVCODBAN=space(10)
      .w_MVSCOCL1=0
      .w_MVSCOCL2=0
      .w_MVSCOPAG=0
      .w_GIAIMP=space(1)
      .w_OB_TEST=ctod("  /  /  ")
      .w_VISUALIZZACALCOLA=.f.
      .w_RIIVDTIN=ctod("  /  /  ")
      .w_RIIVDTFI=ctod("  /  /  ")
      .w_RIIVLSIV=space(0)
      .w_FLGZER=space(1)
      .w_MVAGG_01=space(15)
      .w_MVAGG_02=space(15)
      .w_MVAGG_03=space(15)
      .w_MVAGG_04=space(15)
      .w_MVAGG_05=ctod("  /  /  ")
      .w_MVAGG_06=ctod("  /  /  ")
      .w_TDFLIA01=space(1)
      .w_TDFLIA02=space(1)
      .w_TDFLIA03=space(1)
      .w_TDFLIA04=space(1)
      .w_TDFLIA05=space(1)
      .w_TDFLIA06=space(1)
      .w_TDFLRA01=space(1)
      .w_TDFLRA02=space(1)
      .w_TDFLRA03=space(1)
      .w_TDFLRA04=space(1)
      .w_TDFLRA05=space(1)
      .w_TDFLRA06=space(1)
      .w_DACAM_01=space(30)
      .w_DACAM_02=space(30)
      .w_DACAM_03=space(30)
      .w_DACAM_04=space(30)
      .w_DACAM_05=space(30)
      .w_DACAM_06=space(30)
      .w_TSTDAAG1=space(6)
      .w_TSTDAAG2=space(6)
      .w_CUR_NAME=space(10)
      .w_TDMINVEN=space(1)
      .w_TDRIOTOT=space(1)
      .w_OLDQTAEVA=0
      .w_DESDES=space(40)
      .w_TIPDORI=space(1)
      .w_NUMDOCORI=0
      .w_ALFDOCORI=space(10)
      .w_DATDOCORI=ctod("  /  /  ")
      .w_DESCAMAG=space(35)
      .w_TFLCOMM=space(1)
      .w_FLCOMM=space(1)
      .w_TDFLNORC=oParentObject.w_TDFLNORC
      .w_MVDATDOC=oParentObject.w_MVDATDOC
      .w_MVFLSCOR=oParentObject.w_MVFLSCOR
      .w_MVTIPDOC=oParentObject.w_MVTIPDOC
      .w_FLGCOM=oParentObject.w_FLGCOM
      .w_FLRICIVA=oParentObject.w_FLRICIVA
      .w_FLNSRI=oParentObject.w_FLNSRI
      .w_FLVSRI=oParentObject.w_FLVSRI
      .w_FLRIDE=oParentObject.w_FLRIDE
      .w_FLCAUMAG=oParentObject.w_FLCAUMAG
      .w_PARCAMAG=oParentObject.w_PARCAMAG
      .w_FLMGPR=oParentObject.w_FLMGPR
      .w_COMAG=oParentObject.w_COMAG
      .w_FLMTPR=oParentObject.w_FLMTPR
      .w_FLFOM=oParentObject.w_FLFOM
      .w_COMAT=oParentObject.w_COMAT
      .w_MVTIPCON=oParentObject.w_MVTIPCON
      .w_MVCODCON=oParentObject.w_MVCODCON
      .w_MVFLVEAC=oParentObject.w_MVFLVEAC
      .w_CAUCOL=oParentObject.w_CAUCOL
      .w_ESCL1=oParentObject.w_ESCL1
      .w_ESCL2=oParentObject.w_ESCL2
      .w_ESCL3=oParentObject.w_ESCL3
      .w_ESCL4=oParentObject.w_ESCL4
      .w_ESCL5=oParentObject.w_ESCL5
      .w_MAGTER=oParentObject.w_MAGTER
      .w_MVCLADOC=oParentObject.w_MVCLADOC
      .w_MVFLSCOM=oParentObject.w_MVFLSCOM
      .w_FLPRAT=oParentObject.w_FLPRAT
      .w_TDFLAPCA=oParentObject.w_TDFLAPCA
      .w_VISPRAT=oParentObject.w_VISPRAT
      .w_RIIVDTIN=oParentObject.w_RIIVDTIN
      .w_RIIVDTFI=oParentObject.w_RIIVDTFI
      .w_RIIVLSIV=oParentObject.w_RIIVLSIV
      .w_FLGZER=oParentObject.w_FLGZER
      .w_TDFLIA01=oParentObject.w_TDFLIA01
      .w_TDFLIA02=oParentObject.w_TDFLIA02
      .w_TDFLIA03=oParentObject.w_TDFLIA03
      .w_TDFLIA04=oParentObject.w_TDFLIA04
      .w_TDFLIA05=oParentObject.w_TDFLIA05
      .w_TDFLIA06=oParentObject.w_TDFLIA06
      .w_TDFLRA01=oParentObject.w_TDFLRA01
      .w_TDFLRA02=oParentObject.w_TDFLRA02
      .w_TDFLRA03=oParentObject.w_TDFLRA03
      .w_TDFLRA04=oParentObject.w_TDFLRA04
      .w_TDFLRA05=oParentObject.w_TDFLRA05
      .w_TDFLRA06=oParentObject.w_TDFLRA06
      .w_DACAM_01=oParentObject.w_DACAM_01
      .w_DACAM_02=oParentObject.w_DACAM_02
      .w_DACAM_03=oParentObject.w_DACAM_03
      .w_DACAM_04=oParentObject.w_DACAM_04
      .w_DACAM_05=oParentObject.w_DACAM_05
      .w_DACAM_06=oParentObject.w_DACAM_06
      .w_TDMINVEN=oParentObject.w_TDMINVEN
      .w_TDRIOTOT=oParentObject.w_TDRIOTOT
      .w_TFLCOMM=oParentObject.w_TFLCOMM
        .w_CHIAVE = 'TAM'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CHIAVE))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,4,.f.)
        .w_TESTERR = .T.
        .w_OBTEST = .w_MVDATDOC
          .DoRTCalc(7,9,.f.)
        .w_SOTCAT = IIF(IsAlt(),SOTCATDO(.w_MVTIPDOC),'#')
        .w_FILCOMM = IIF(this.oParentObject.class='Tgsve_mdv' AND !EMPTY(this.oParentObject.w_CODPRA_DA_NEWDOC), this.oParentObject.w_CODPRA_DA_NEWDOC, SPACE(15))
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_FILCOMM))
          .link_1_11('Full')
        endif
          .DoRTCalc(12,15,.f.)
        .w_FILSPE = 'S'
        .w_FILANT = 'S'
        .w_FILGEN = 'S'
        .w_FILDIR = 'S'
        .w_FILONO = 'S'
        .w_FILTEM = 'S'
        .w_FLCAUMAG = 'N'
        .w_PARCAMAG = .w_PCAUMAG
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_PARCAMAG))
          .link_1_23('Full')
        endif
          .DoRTCalc(24,24,.f.)
        .w_MAGTP = .w_MAGTER
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_COMAG))
          .link_1_26('Full')
        endif
          .DoRTCalc(27,27,.f.)
        .w_FLFOM = 'M'
        .w_FLCOAC = .w_TDFLAPCA
        .DoRTCalc(30,30,.f.)
        if not(empty(.w_COMAT))
          .link_1_30('Full')
        endif
        .w_MAGTC = .w_MAGTER
      .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .w_NASCONDIRIGHE = UPPER(L_Padre.cPRG) = "GSAR_MPG"
        .w_CALSER = .w_ZoomMast.getVar('MVSERIAL')
        .w_TIPO = Nvl(.w_ZoomMast.getVar('TIPO'),' ')
        .w_SERIAL = IIF(EMPTY(NVL(.w_CALSER,' ')),'zzzzzzzzzzz', .w_CALSER)
      .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERIAL)
        .w_MVQTAEVA = .w_ZoomDett.getVar('MVQTAEVA')
        .w_MVIMPEVA = .w_ZoomDett.getVar('MVIMPEVA')
      .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
          .DoRTCalc(38,40,.f.)
        .w_TIPCON = .w_MVTIPCON
        .w_CODCON = IIF(EMPTY(.w_MVCODCON), 'xxxzzzyyy',.w_MVCODCON)
        .w_TIPORN = IIF(g_PERORN='S', .w_MVTIPCON, 'X')
        .w_CODORN = IIF(g_PERORN='S' AND NOT EMPTY(.w_MVCODCON), .w_MVCODCON, 'xxxzzzyyy')
        .w_FLVEAC = .w_MVFLVEAC
        .w_TIPDOC = .w_MVTIPDOC
          .DoRTCalc(47,47,.f.)
        .w_FLSCOR = IIF(.w_MVFLSCOR='S', 'S', 'N')
      .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_69.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_79.Calculate()
          .DoRTCalc(49,54,.f.)
        .w_TipoRisorsa = 'P'
        .w_DATINI = .w_MVDATDOC - g_DTINIM
        .w_DATFIN = .w_MVDATDOC + g_DTFIIM
        .DoRTCalc(58,61,.f.)
        if not(empty(.w_FILART))
          .link_2_7('Full')
        endif
        .DoRTCalc(62,62,.f.)
        if not(empty(.w_FILARM))
          .link_2_8('Full')
        endif
        .DoRTCalc(63,63,.f.)
        if not(empty(.w_FILMAG))
          .link_2_9('Full')
        endif
        .DoRTCalc(64,64,.f.)
        if not(empty(.w_FILCAU))
          .link_2_12('Full')
        endif
        .w_CLIFOR = IIF(NOT EMPTY(.w_FILCLI), .w_FILCLI,.w_MVCODCON) 
          .DoRTCalc(66,67,.f.)
        .w_FLVEAC1 = IIF(EMPTY(.w_FILCAU), IIF(.w_MVFLVEAC='V', 'C', 'F'), .w_CFCOLL)
        .w_COLLEG = .w_FILCAU
        .DoRTCalc(69,69,.f.)
        if not(empty(.w_COLLEG))
          .link_2_21('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
          .DoRTCalc(70,71,.f.)
        .w_FILCLI = SPACE(15)
        .DoRTCalc(72,72,.f.)
        if not(empty(.w_FILCLI))
          .link_2_25('Full')
        endif
          .DoRTCalc(73,73,.f.)
        .w_CODDES = SPACE(5)
        .DoRTCalc(74,74,.f.)
        if not(empty(.w_CODDES))
          .link_2_28('Full')
        endif
          .DoRTCalc(75,76,.f.)
        .w_TIPART = .w_ZoomDett.getVar('MVTIPRIG')
        .w_DESART = .w_ZoomDett.getVar('MVDESART')
        .w_SELEZI = 'S'
        .w_SELEZ1 = 'D'
        .DoRTCalc(81,81,.f.)
        if not(empty(.w_FILCOMM))
          .link_2_30('Full')
        endif
        .DoRTCalc(82,83,.f.)
        if not(empty(.w_FILCEN))
          .link_2_33('Full')
        endif
        .DoRTCalc(84,85,.f.)
        if not(empty(.w_FILATTI))
          .link_2_37('Full')
        endif
        .DoRTCalc(86,87,.f.)
        if not(empty(.w_FILVOC))
          .link_2_40('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_91.Calculate()
          .DoRTCalc(88,89,.f.)
        .w_FLSCAF = Nvl( .w_ZoomMast.getVar('MVFLSCAF'), ' ')
        .w_BLOCKMAST = 0
      .oPgFrm.Page1.oPag.oObj_1_94.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_97.Calculate(IIF(.w_TIPART='F',IIF(IsAlt(),'Prestazione a Valore:','Servizio a Valore:'),IIF(.w_TIPART='R','Articolo:',IIF(.w_TIPART='M',IIF(IsAlt(),'Prestazione a Qt� e Val.:','Servizio a Qt� e Val.:'),IIF(.w_TIPART='D','Descrizione:','')))))
          .DoRTCalc(92,94,.f.)
        .w_FLGROR = Nvl(.w_ZoomMast.getVar('DCFLGROR'),' ')
          .DoRTCalc(96,96,.f.)
        .w_DESDOC1 = IIF(NOT EMPTY( .w_DESDOC) , '%' + Alltrim(.w_DESDOC) + '%' , '' )
          .DoRTCalc(98,98,.f.)
        .w_GESTPARENT = L_Padre
          .DoRTCalc(100,101,.f.)
        .w_FILPRA = 'N'
        .DoRTCalc(103,106,.f.)
        if not(empty(.w_FILRES))
          .link_2_52('Full')
        endif
        .w_DENOM_PART = alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART)
          .DoRTCalc(108,110,.f.)
        .w_PREFIN = IIF(IsAlt(),.w_DATFIN,Cp_chartodate('  -  -  '))
      .oPgFrm.Page1.oPag.oObj_1_115.Calculate()
          .DoRTCalc(112,112,.f.)
        .w_ROWNUM = .w_ZoomDett.getVar('CPROWNUM')
        .w_TIDODE = '#'
          .DoRTCalc(115,116,.f.)
        .w_MVCODVAL = this.oparentobject.w_MVCODVAL
        .w_FLIMPA = this.oparentobject.w_FLIMPA
        .w_FLIMAC = this.oparentobject.w_FLIMAC
        .w_MVCODIVE = this.oparentobject.w_MVCODIVE
        .w_MVRIFDIC = this.oparentobject.w_MVRIFDIC
        .w_MVCODPAG = this.oparentobject.w_MVCODPAG
        .w_MVDATDIV = this.oparentobject.w_MVDATDIV
        .w_MVCODBA2 = this.oparentobject.w_MVCODBA2
        .w_MVCODAG2 = this.oparentobject.w_MVCODAG2
        .w_MVIVAINC = this.oparentobject.w_MVIVAINC
        .w_MVIVAIMB = this.oparentobject.w_MVIVAIMB
        .w_MVIVATRA = this.oparentobject.w_MVIVATRA
        .w_MVIVABOL = this.oparentobject.w_MVIVABOL
        .w_MVIVACAU = this.oparentobject.w_MVIVACAU
        .w_MVCODVET = this.oparentobject.w_MVCODVET
        .w_MVCODVE2 = this.oparentobject.w_MVCODVE2
        .w_MVCODVE3 = this.oparentobject.w_MVCODVE3
        .w_MVCODPOR = this.oparentobject.w_MVCODPOR
        .w_MVCODSPE = this.oparentobject.w_MVCODSPE
        .w_MVCONCON = this.oparentobject.w_MVCONCON
        .w_MVCODDES = this.oparentobject.w_MVCODDES
        .w_MVCODORN = this.oparentobject.w_MVCODORN
        .w_MVCODSED = this.oparentobject.w_MVCODSED
        .w_MVCODAGE = this.oparentobject.w_MVCODAGE
        .w_MVCODBAN = this.oparentobject.w_MVCODBAN
        .w_MVSCOCL1 = this.oparentobject.w_MVSCOCL1
        .w_MVSCOCL2 = this.oparentobject.w_MVSCOCL2
        .w_MVSCOPAG = this.oparentobject.w_MVSCOPAG
        .w_GIAIMP = this.oparentobject.w_GIAIMP
        .w_OB_TEST = i_INIDAT
      .oPgFrm.Page1.oPag.oObj_1_151.Calculate()
        .w_VISUALIZZACALCOLA = IsAlt() AND VARTYPE(this.oParentObject.w_GESTPADR)='O' AND UPPER(this.oParentObject.w_GESTPADR.cPRG) = "GSAL_KGD" AND (this.oParentObject.w_GESTPADR.w_TIPCAU='F' OR this.oParentObject.w_GESTPADR.w_TIPCAU='S') AND this.oParentObject.w_GESTPADR.w_FLSDMCIF='S'
          .DoRTCalc(148,151,.f.)
        .w_MVAGG_01 = this.oParentObject.w_MVAGG_01
        .w_MVAGG_02 = this.oParentObject.w_MVAGG_02
        .w_MVAGG_03 = this.oParentObject.w_MVAGG_03
        .w_MVAGG_04 = this.oParentObject.w_MVAGG_04
        .w_MVAGG_05 = this.oParentObject.w_MVAGG_05
        .w_MVAGG_06 = this.oParentObject.w_MVAGG_06
          .DoRTCalc(158,175,.f.)
        .w_TSTDAAG1 = NVL(.w_ZoomMast.getVar('TSTDAAG1'), 'NNNN')
        .w_TSTDAAG2 = NVL(.w_ZoomMast.getVar('TSTDAAG2'), 'NN')
          .DoRTCalc(178,180,.f.)
        .w_OLDQTAEVA = .w_ZoomDett.getVar('MVQTAEVA')
    endwith
    this.DoRTCalc(182,189,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_43.enabled = this.oPgFrm.Page1.oPag.oBtn_1_43.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_103.enabled = this.oPgFrm.Page1.oPag.oBtn_1_103.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_49.enabled = this.oPgFrm.Page2.oPag.oBtn_2_49.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_152.enabled = this.oPgFrm.Page1.oPag.oBtn_1_152.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsve_kim
    *istanzio l'oggetto nella maschera perch� viene utilizzato nei batch gsve_bi1 e gsve_bi4 per il lancio dello stesso report
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    
    *esecuzione della routine che gestisce le dimensioni delgli zoom
    if UPPER(L_Padre.cPRG) = "GSAR_MPG"
      this.modificazoom( 'NascondiRighe')
    else
      this.modificazoom( 'MostraRighe')
    endif
    
    *
    If IsAlt() AND VARTYPE(this.oParentObject.w_GESTPADR)='O' AND this.oParentObject.w_GESTPADR.w_FLSDMCIF='S'
       this.modificazoom( 'NascondiRighe')
       this.NotifyEvent('Carica')
    endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_TDFLNORC=.w_TDFLNORC
      .oParentObject.w_MVDATDOC=.w_MVDATDOC
      .oParentObject.w_MVFLSCOR=.w_MVFLSCOR
      .oParentObject.w_MVTIPDOC=.w_MVTIPDOC
      .oParentObject.w_FLGCOM=.w_FLGCOM
      .oParentObject.w_FLRICIVA=.w_FLRICIVA
      .oParentObject.w_FLNSRI=.w_FLNSRI
      .oParentObject.w_FLVSRI=.w_FLVSRI
      .oParentObject.w_FLRIDE=.w_FLRIDE
      .oParentObject.w_FLCAUMAG=.w_FLCAUMAG
      .oParentObject.w_PARCAMAG=.w_PARCAMAG
      .oParentObject.w_FLMGPR=.w_FLMGPR
      .oParentObject.w_COMAG=.w_COMAG
      .oParentObject.w_FLMTPR=.w_FLMTPR
      .oParentObject.w_FLFOM=.w_FLFOM
      .oParentObject.w_COMAT=.w_COMAT
      .oParentObject.w_MVTIPCON=.w_MVTIPCON
      .oParentObject.w_MVCODCON=.w_MVCODCON
      .oParentObject.w_MVFLVEAC=.w_MVFLVEAC
      .oParentObject.w_CAUCOL=.w_CAUCOL
      .oParentObject.w_ESCL1=.w_ESCL1
      .oParentObject.w_ESCL2=.w_ESCL2
      .oParentObject.w_ESCL3=.w_ESCL3
      .oParentObject.w_ESCL4=.w_ESCL4
      .oParentObject.w_ESCL5=.w_ESCL5
      .oParentObject.w_MAGTER=.w_MAGTER
      .oParentObject.w_MVCLADOC=.w_MVCLADOC
      .oParentObject.w_MVFLSCOM=.w_MVFLSCOM
      .oParentObject.w_FLPRAT=.w_FLPRAT
      .oParentObject.w_TDFLAPCA=.w_TDFLAPCA
      .oParentObject.w_VISPRAT=.w_VISPRAT
      .oParentObject.w_RIIVDTIN=.w_RIIVDTIN
      .oParentObject.w_RIIVDTFI=.w_RIIVDTFI
      .oParentObject.w_RIIVLSIV=.w_RIIVLSIV
      .oParentObject.w_FLGZER=.w_FLGZER
      .oParentObject.w_TDFLIA01=.w_TDFLIA01
      .oParentObject.w_TDFLIA02=.w_TDFLIA02
      .oParentObject.w_TDFLIA03=.w_TDFLIA03
      .oParentObject.w_TDFLIA04=.w_TDFLIA04
      .oParentObject.w_TDFLIA05=.w_TDFLIA05
      .oParentObject.w_TDFLIA06=.w_TDFLIA06
      .oParentObject.w_TDFLRA01=.w_TDFLRA01
      .oParentObject.w_TDFLRA02=.w_TDFLRA02
      .oParentObject.w_TDFLRA03=.w_TDFLRA03
      .oParentObject.w_TDFLRA04=.w_TDFLRA04
      .oParentObject.w_TDFLRA05=.w_TDFLRA05
      .oParentObject.w_TDFLRA06=.w_TDFLRA06
      .oParentObject.w_DACAM_01=.w_DACAM_01
      .oParentObject.w_DACAM_02=.w_DACAM_02
      .oParentObject.w_DACAM_03=.w_DACAM_03
      .oParentObject.w_DACAM_04=.w_DACAM_04
      .oParentObject.w_DACAM_05=.w_DACAM_05
      .oParentObject.w_DACAM_06=.w_DACAM_06
      .oParentObject.w_TDMINVEN=.w_TDMINVEN
      .oParentObject.w_TDRIOTOT=.w_TDRIOTOT
      .oParentObject.w_TFLCOMM=.w_TFLCOMM
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,24,.t.)
            .w_MAGTP = .w_MAGTER
        .DoRTCalc(26,30,.t.)
            .w_MAGTC = .w_MAGTER
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .DoRTCalc(32,32,.t.)
            .w_CALSER = .w_ZoomMast.getVar('MVSERIAL')
            .w_TIPO = Nvl(.w_ZoomMast.getVar('TIPO'),' ')
        if .o_CALSER<>.w_CALSER
            .w_SERIAL = IIF(EMPTY(NVL(.w_CALSER,' ')),'zzzzzzzzzzz', .w_CALSER)
        endif
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERIAL)
            .w_MVQTAEVA = .w_ZoomDett.getVar('MVQTAEVA')
            .w_MVIMPEVA = .w_ZoomDett.getVar('MVIMPEVA')
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_69.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate()
        .DoRTCalc(38,64,.t.)
        if .o_FILCLI<>.w_FILCLI
            .w_CLIFOR = IIF(NOT EMPTY(.w_FILCLI), .w_FILCLI,.w_MVCODCON) 
        endif
        .DoRTCalc(66,67,.t.)
            .w_FLVEAC1 = IIF(EMPTY(.w_FILCAU), IIF(.w_MVFLVEAC='V', 'C', 'F'), .w_CFCOLL)
        if .o_FILCAU<>.w_FILCAU
            .w_COLLEG = .w_FILCAU
          .link_2_21('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        .DoRTCalc(70,71,.t.)
        if .o_FILCAU<>.w_FILCAU.or. .o_FLVEAC1<>.w_FLVEAC1
            .w_FILCLI = SPACE(15)
          .link_2_25('Full')
        endif
        .DoRTCalc(73,73,.t.)
        if .o_CLIFOR<>.w_CLIFOR.or. .o_FLVEAC1<>.w_FLVEAC1.or. .o_FILCAU<>.w_FILCAU.or. .o_FILCLI<>.w_FILCLI
            .w_CODDES = SPACE(5)
          .link_2_28('Full')
        endif
        .DoRTCalc(75,76,.t.)
            .w_TIPART = .w_ZoomDett.getVar('MVTIPRIG')
            .w_DESART = .w_ZoomDett.getVar('MVDESART')
        .oPgFrm.Page1.oPag.oObj_1_91.Calculate()
        .DoRTCalc(79,89,.t.)
            .w_FLSCAF = Nvl( .w_ZoomMast.getVar('MVFLSCAF'), ' ')
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_97.Calculate(IIF(.w_TIPART='F',IIF(IsAlt(),'Prestazione a Valore:','Servizio a Valore:'),IIF(.w_TIPART='R','Articolo:',IIF(.w_TIPART='M',IIF(IsAlt(),'Prestazione a Qt� e Val.:','Servizio a Qt� e Val.:'),IIF(.w_TIPART='D','Descrizione:','')))))
        .DoRTCalc(91,94,.t.)
            .w_FLGROR = Nvl(.w_ZoomMast.getVar('DCFLGROR'),' ')
        .DoRTCalc(96,96,.t.)
        if .o_DESDOC<>.w_DESDOC
            .w_DESDOC1 = IIF(NOT EMPTY( .w_DESDOC) , '%' + Alltrim(.w_DESDOC) + '%' , '' )
        endif
        .DoRTCalc(98,106,.t.)
            .w_DENOM_PART = alltrim(.w_COGNPART)+' '+alltrim(.w_NOMEPART)
        .DoRTCalc(108,110,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_PREFIN = IIF(IsAlt(),.w_DATFIN,Cp_chartodate('  -  -  '))
        endif
        .oPgFrm.Page1.oPag.oObj_1_115.Calculate()
        .DoRTCalc(112,112,.t.)
            .w_ROWNUM = .w_ZoomDett.getVar('CPROWNUM')
        .oPgFrm.Page1.oPag.oObj_1_151.Calculate()
        if .o_FLRICIVA<>.w_FLRICIVA
          .Calculate_WUICMSNHXF()
        endif
        .DoRTCalc(114,175,.t.)
            .w_TSTDAAG1 = NVL(.w_ZoomMast.getVar('TSTDAAG1'), 'NNNN')
            .w_TSTDAAG2 = NVL(.w_ZoomMast.getVar('TSTDAAG2'), 'NN')
        .DoRTCalc(178,180,.t.)
            .w_OLDQTAEVA = .w_ZoomDett.getVar('MVQTAEVA')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(182,189,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomMast.Calculate()
        .oPgFrm.Page1.oPag.ZoomDett.Calculate(.w_SERIAL)
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_48.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_49.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_50.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_68.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_69.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_71.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_83.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_91.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_94.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_97.Calculate(IIF(.w_TIPART='F',IIF(IsAlt(),'Prestazione a Valore:','Servizio a Valore:'),IIF(.w_TIPART='R','Articolo:',IIF(.w_TIPART='M',IIF(IsAlt(),'Prestazione a Qt� e Val.:','Servizio a Qt� e Val.:'),IIF(.w_TIPART='D','Descrizione:','')))))
        .oPgFrm.Page1.oPag.oObj_1_115.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_151.Calculate()
    endwith
  return

  proc Calculate_NVVQJOIHXU()
    with this
          * --- Paer ALTE cambio la query dello zooom
     if Isalt()
          .w_ZoomMast.cCpQueryName = "GSVEZKIM"
     endif
    endwith
  endproc
  proc Calculate_YNAWTBGBPH()
    with this
          * --- Esegue ricerca automatica al cambio di check
          gsve_bim(this;
             )
    endwith
  endproc
  proc Calculate_WUICMSNHXF()
    with this
          * --- Apertura maschera conferma
          .w_FLRICIVA = GSVE_BFI(this, "OPEN")
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLNSRI_1_13.enabled = this.oPgFrm.Page1.oPag.oFLNSRI_1_13.mCond()
    this.oPgFrm.Page1.oPag.oFLVSRI_1_14.enabled = this.oPgFrm.Page1.oPag.oFLVSRI_1_14.mCond()
    this.oPgFrm.Page1.oPag.oFLRIDE_1_15.enabled = this.oPgFrm.Page1.oPag.oFLRIDE_1_15.mCond()
    this.oPgFrm.Page1.oPag.oFILSPE_1_16.enabled = this.oPgFrm.Page1.oPag.oFILSPE_1_16.mCond()
    this.oPgFrm.Page1.oPag.oFILANT_1_17.enabled = this.oPgFrm.Page1.oPag.oFILANT_1_17.mCond()
    this.oPgFrm.Page1.oPag.oFILGEN_1_18.enabled = this.oPgFrm.Page1.oPag.oFILGEN_1_18.mCond()
    this.oPgFrm.Page1.oPag.oFILDIR_1_19.enabled = this.oPgFrm.Page1.oPag.oFILDIR_1_19.mCond()
    this.oPgFrm.Page1.oPag.oFILONO_1_20.enabled = this.oPgFrm.Page1.oPag.oFILONO_1_20.mCond()
    this.oPgFrm.Page1.oPag.oFILTEM_1_21.enabled = this.oPgFrm.Page1.oPag.oFILTEM_1_21.mCond()
    this.oPgFrm.Page1.oPag.oFLCAUMAG_1_22.enabled = this.oPgFrm.Page1.oPag.oFLCAUMAG_1_22.mCond()
    this.oPgFrm.Page1.oPag.oCOMAG_1_26.enabled = this.oPgFrm.Page1.oPag.oCOMAG_1_26.mCond()
    this.oPgFrm.Page1.oPag.oFLMTPR_1_27.enabled = this.oPgFrm.Page1.oPag.oFLMTPR_1_27.mCond()
    this.oPgFrm.Page1.oPag.oCOMAT_1_30.enabled = this.oPgFrm.Page1.oPag.oCOMAT_1_30.mCond()
    this.oPgFrm.Page2.oPag.oFILCLI_2_25.enabled = this.oPgFrm.Page2.oPag.oFILCLI_2_25.mCond()
    this.oPgFrm.Page2.oPag.oCODDES_2_28.enabled = this.oPgFrm.Page2.oPag.oCODDES_2_28.mCond()
    this.oPgFrm.Page1.oPag.oSELEZ1_1_90.enabled_(this.oPgFrm.Page1.oPag.oSELEZ1_1_90.mCond())
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_39.enabled = this.oPgFrm.Page1.oPag.oBtn_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFILCOMM_1_11.visible=!this.oPgFrm.Page1.oPag.oFILCOMM_1_11.mHide()
    this.oPgFrm.Page1.oPag.oFILSPE_1_16.visible=!this.oPgFrm.Page1.oPag.oFILSPE_1_16.mHide()
    this.oPgFrm.Page1.oPag.oFILANT_1_17.visible=!this.oPgFrm.Page1.oPag.oFILANT_1_17.mHide()
    this.oPgFrm.Page1.oPag.oFILGEN_1_18.visible=!this.oPgFrm.Page1.oPag.oFILGEN_1_18.mHide()
    this.oPgFrm.Page1.oPag.oFILDIR_1_19.visible=!this.oPgFrm.Page1.oPag.oFILDIR_1_19.mHide()
    this.oPgFrm.Page1.oPag.oFILONO_1_20.visible=!this.oPgFrm.Page1.oPag.oFILONO_1_20.mHide()
    this.oPgFrm.Page1.oPag.oFILTEM_1_21.visible=!this.oPgFrm.Page1.oPag.oFILTEM_1_21.mHide()
    this.oPgFrm.Page1.oPag.oPARCAMAG_1_23.visible=!this.oPgFrm.Page1.oPag.oPARCAMAG_1_23.mHide()
    this.oPgFrm.Page1.oPag.oFLMGPR_1_24.visible=!this.oPgFrm.Page1.oPag.oFLMGPR_1_24.mHide()
    this.oPgFrm.Page1.oPag.oMAGTP_1_25.visible=!this.oPgFrm.Page1.oPag.oMAGTP_1_25.mHide()
    this.oPgFrm.Page1.oPag.oCOMAG_1_26.visible=!this.oPgFrm.Page1.oPag.oCOMAG_1_26.mHide()
    this.oPgFrm.Page1.oPag.oFLMTPR_1_27.visible=!this.oPgFrm.Page1.oPag.oFLMTPR_1_27.mHide()
    this.oPgFrm.Page1.oPag.oFLFOM_1_28.visible=!this.oPgFrm.Page1.oPag.oFLFOM_1_28.mHide()
    this.oPgFrm.Page1.oPag.oFLCOAC_1_29.visible=!this.oPgFrm.Page1.oPag.oFLCOAC_1_29.mHide()
    this.oPgFrm.Page1.oPag.oCOMAT_1_30.visible=!this.oPgFrm.Page1.oPag.oCOMAT_1_30.mHide()
    this.oPgFrm.Page1.oPag.oMAGTC_1_31.visible=!this.oPgFrm.Page1.oPag.oMAGTC_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_33.visible=!this.oPgFrm.Page1.oPag.oStr_1_33.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_38.visible=!this.oPgFrm.Page1.oPag.oBtn_1_38.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_39.visible=!this.oPgFrm.Page1.oPag.oBtn_1_39.mHide()
    this.oPgFrm.Page2.oPag.oEVAINI_2_4.visible=!this.oPgFrm.Page2.oPag.oEVAINI_2_4.mHide()
    this.oPgFrm.Page2.oPag.oEVAFIN_2_5.visible=!this.oPgFrm.Page2.oPag.oEVAFIN_2_5.mHide()
    this.oPgFrm.Page2.oPag.oFLTRASF_2_6.visible=!this.oPgFrm.Page2.oPag.oFLTRASF_2_6.mHide()
    this.oPgFrm.Page2.oPag.oFILARM_2_8.visible=!this.oPgFrm.Page2.oPag.oFILARM_2_8.mHide()
    this.oPgFrm.Page2.oPag.oFILMAG_2_9.visible=!this.oPgFrm.Page2.oPag.oFILMAG_2_9.mHide()
    this.oPgFrm.Page2.oPag.oFILCAU_2_12.visible=!this.oPgFrm.Page2.oPag.oFILCAU_2_12.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_14.visible=!this.oPgFrm.Page2.oPag.oStr_2_14.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_15.visible=!this.oPgFrm.Page2.oPag.oStr_2_15.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_16.visible=!this.oPgFrm.Page2.oPag.oStr_2_16.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_18.visible=!this.oPgFrm.Page2.oPag.oStr_2_18.mHide()
    this.oPgFrm.Page2.oPag.oDESARM_2_19.visible=!this.oPgFrm.Page2.oPag.oDESARM_2_19.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_24.visible=!this.oPgFrm.Page2.oPag.oStr_2_24.mHide()
    this.oPgFrm.Page2.oPag.oDESCAU_2_29.visible=!this.oPgFrm.Page2.oPag.oDESCAU_2_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_84.visible=!this.oPgFrm.Page1.oPag.oStr_1_84.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_86.visible=!this.oPgFrm.Page1.oPag.oStr_1_86.mHide()
    this.oPgFrm.Page1.oPag.oDESART_1_88.visible=!this.oPgFrm.Page1.oPag.oDESART_1_88.mHide()
    this.oPgFrm.Page1.oPag.oSELEZI_1_89.visible=!this.oPgFrm.Page1.oPag.oSELEZI_1_89.mHide()
    this.oPgFrm.Page2.oPag.oFILCOMM_2_30.visible=!this.oPgFrm.Page2.oPag.oFILCOMM_2_30.mHide()
    this.oPgFrm.Page2.oPag.oDESCOM_2_31.visible=!this.oPgFrm.Page2.oPag.oDESCOM_2_31.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_32.visible=!this.oPgFrm.Page2.oPag.oStr_2_32.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_35.visible=!this.oPgFrm.Page2.oPag.oStr_2_35.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_36.visible=!this.oPgFrm.Page2.oPag.oStr_2_36.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_42.visible=!this.oPgFrm.Page2.oPag.oStr_2_42.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_43.visible=!this.oPgFrm.Page2.oPag.oStr_2_43.mHide()
    this.oPgFrm.Page2.oPag.oDESMAG_2_44.visible=!this.oPgFrm.Page2.oPag.oDESMAG_2_44.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_45.visible=!this.oPgFrm.Page2.oPag.oStr_2_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_100.visible=!this.oPgFrm.Page1.oPag.oStr_1_100.mHide()
    this.oPgFrm.Page1.oPag.oSCOPE_1_101.visible=!this.oPgFrm.Page1.oPag.oSCOPE_1_101.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_102.visible=!this.oPgFrm.Page1.oPag.oStr_1_102.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_103.visible=!this.oPgFrm.Page1.oPag.oBtn_1_103.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_50.visible=!this.oPgFrm.Page2.oPag.oStr_2_50.mHide()
    this.oPgFrm.Page1.oPag.oDESCOM_1_109.visible=!this.oPgFrm.Page1.oPag.oDESCOM_1_109.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_110.visible=!this.oPgFrm.Page1.oPag.oStr_1_110.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_51.visible=!this.oPgFrm.Page2.oPag.oStr_2_51.mHide()
    this.oPgFrm.Page2.oPag.oFILRES_2_52.visible=!this.oPgFrm.Page2.oPag.oFILRES_2_52.mHide()
    this.oPgFrm.Page2.oPag.oDENOM_PART_2_53.visible=!this.oPgFrm.Page2.oPag.oDENOM_PART_2_53.mHide()
    this.oPgFrm.Page2.oPag.oPREINI_2_56.visible=!this.oPgFrm.Page2.oPag.oPREINI_2_56.mHide()
    this.oPgFrm.Page2.oPag.oPREFIN_2_57.visible=!this.oPgFrm.Page2.oPag.oPREFIN_2_57.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_58.visible=!this.oPgFrm.Page2.oPag.oStr_2_58.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_59.visible=!this.oPgFrm.Page2.oPag.oStr_2_59.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_113.visible=!this.oPgFrm.Page1.oPag.oStr_1_113.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_114.visible=!this.oPgFrm.Page1.oPag.oStr_1_114.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_149.visible=!this.oPgFrm.Page1.oPag.oStr_1_149.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_152.visible=!this.oPgFrm.Page1.oPag.oBtn_1_152.mHide()
    this.oPgFrm.Page2.oPag.oTIPDORI_2_63.visible=!this.oPgFrm.Page2.oPag.oTIPDORI_2_63.mHide()
    this.oPgFrm.Page2.oPag.oNUMDOCORI_2_64.visible=!this.oPgFrm.Page2.oPag.oNUMDOCORI_2_64.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_65.visible=!this.oPgFrm.Page2.oPag.oStr_2_65.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_66.visible=!this.oPgFrm.Page2.oPag.oStr_2_66.mHide()
    this.oPgFrm.Page2.oPag.oALFDOCORI_2_67.visible=!this.oPgFrm.Page2.oPag.oALFDOCORI_2_67.mHide()
    this.oPgFrm.Page2.oPag.oDATDOCORI_2_68.visible=!this.oPgFrm.Page2.oPag.oDATDOCORI_2_68.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_69.visible=!this.oPgFrm.Page2.oPag.oStr_2_69.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_70.visible=!this.oPgFrm.Page2.oPag.oStr_2_70.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_190.visible=!this.oPgFrm.Page1.oPag.oStr_1_190.mHide()
    this.oPgFrm.Page1.oPag.oDESCAMAG_1_191.visible=!this.oPgFrm.Page1.oPag.oDESCAMAG_1_191.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsve_kim
      if cevent='w_FILCOMM Changed' and Isalt()
        This.Notifyevent('Carica')
      Endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomMast.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomDett.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_48.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_49.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_50.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_67.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_68.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_69.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_NVVQJOIHXU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_71.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_79.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_83.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_91.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_94.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_97.Event(cEvent)
        if lower(cEvent)==lower("Carica") or lower(cEvent)==lower("w_FILSPE Changed") or lower(cEvent)==lower("w_FILANT Changed") or lower(cEvent)==lower("w_FILDIR Changed") or lower(cEvent)==lower("w_FILONO Changed") or lower(cEvent)==lower("w_FILTEM Changed")
          .Calculate_YNAWTBGBPH()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_115.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_151.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CHIAVE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPAR_DEF_IDX,3]
    i_lTable = "CPAR_DEF"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2], .t., this.CPAR_DEF_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CHIAVE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CHIAVE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PDCHIAVE,PDCAUMAG";
                   +" from "+i_cTable+" "+i_lTable+" where PDCHIAVE="+cp_ToStrODBC(this.w_CHIAVE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PDCHIAVE',this.w_CHIAVE)
            select PDCHIAVE,PDCAUMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CHIAVE = NVL(_Link_.PDCHIAVE,space(10))
      this.w_PCAUMAG = NVL(_Link_.PDCAUMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CHIAVE = space(10)
      endif
      this.w_PCAUMAG = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPAR_DEF_IDX,2])+'\'+cp_ToStr(_Link_.PDCHIAVE,1)
      cp_ShowWarn(i_cKey,this.CPAR_DEF_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CHIAVE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILCOMM
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILCOMM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_FILCOMM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_FILCOMM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILCOMM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_FILCOMM)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_FILCOMM)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FILCOMM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oFILCOMM_1_11'),i_cWhere,'GSPR_ACN',""+iif(not isalt (), "commesse", "pratiche") +"",'GSPR2PPD.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILCOMM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_FILCOMM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_FILCOMM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILCOMM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FILCOMM = space(15)
      endif
      this.w_DESCOM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKPRCLI(.w_FILCOMM,.w_CODCON)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pratica incongruente")
        endif
        this.w_FILCOMM = space(15)
        this.w_DESCOM = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILCOMM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PARCAMAG
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAM_AGAZ_IDX,3]
    i_lTable = "CAM_AGAZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2], .t., this.CAM_AGAZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PARCAMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsma_acm',True,'CAM_AGAZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CMCODICE like "+cp_ToStrODBC(trim(this.w_PARCAMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCOMM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CMCODICE',trim(this.w_PARCAMAG))
          select CMCODICE,CMDESCRI,CMFLCOMM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PARCAMAG)==trim(_Link_.CMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PARCAMAG) and !this.bDontReportError
            deferred_cp_zoom('CAM_AGAZ','*','CMCODICE',cp_AbsName(oSource.parent,'oPARCAMAG_1_23'),i_cWhere,'gsma_acm',"Elenco causali magazzino",'GSPC_CAU.CAM_AGAZ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCOMM";
                     +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',oSource.xKey(1))
            select CMCODICE,CMDESCRI,CMFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PARCAMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CMCODICE,CMDESCRI,CMFLCOMM";
                   +" from "+i_cTable+" "+i_lTable+" where CMCODICE="+cp_ToStrODBC(this.w_PARCAMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CMCODICE',this.w_PARCAMAG)
            select CMCODICE,CMDESCRI,CMFLCOMM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PARCAMAG = NVL(_Link_.CMCODICE,space(5))
      this.w_DESCAMAG = NVL(_Link_.CMDESCRI,space(35))
      this.w_FLCOMM = NVL(_Link_.CMFLCOMM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PARCAMAG = space(5)
      endif
      this.w_DESCAMAG = space(35)
      this.w_FLCOMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=nvl(.w_FLCOMM,'N')='N' or empty(.w_FLCOMM)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PARCAMAG = space(5)
        this.w_DESCAMAG = space(35)
        this.w_FLCOMM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAM_AGAZ_IDX,2])+'\'+cp_ToStr(_Link_.CMCODICE,1)
      cp_ShowWarn(i_cKey,this.CAM_AGAZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PARCAMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMAG
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_COMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_COMAG))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCOMAG_1_26'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_COMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_COMAG)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMAG = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_COMAG = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COMAT
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_COMAT)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_COMAT))
          select MGCODMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COMAT)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COMAT) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCOMAT_1_30'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_COMAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_COMAT)
            select MGCODMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COMAT = NVL(_Link_.MGCODMAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_COMAT = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILART
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_FILART)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_FILART))
          select CACODICE,CADESART,CACODART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILART)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILART) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oFILART_2_7'),i_cWhere,'GSMA_ACA',"Articoli/servizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESART,CACODART";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_FILART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_FILART)
            select CACODICE,CADESART,CACODART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILART = NVL(_Link_.CACODICE,space(20))
      this.w_DESFAR = NVL(_Link_.CADESART,space(40))
      this.w_FILARM = NVL(_Link_.CACODART,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_FILART = space(20)
      endif
      this.w_DESFAR = space(40)
      this.w_FILARM = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILARM
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILARM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_FILARM)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_FILARM))
          select ARCODART,ARDESART;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILARM)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_FILARM)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_FILARM)+"%");

            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FILARM) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oFILARM_2_8'),i_cWhere,'GSMA_BZA',"Articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILARM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_FILARM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_FILARM)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILARM = NVL(_Link_.ARCODART,space(20))
      this.w_DESARM = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FILARM = space(20)
      endif
      this.w_DESARM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILARM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILMAG
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_FILMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_FILMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oFILMAG_2_9'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_FILMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_FILMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_FILMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILCAU
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_FILCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_FILCAU))
          select TDTIPDOC,TDDESDOC,TDFLINTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILCAU)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILCAU) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oFILCAU_2_12'),i_cWhere,'',"Documenti collegati",'GSVE1MDC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_FILCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_FILCAU)
            select TDTIPDOC,TDDESDOC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILCAU = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAU = NVL(_Link_.TDDESDOC,space(35))
      this.w_CFCOLL = NVL(_Link_.TDFLINTE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_FILCAU = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_CFCOLL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COLLEG
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_COLL_IDX,3]
    i_lTable = "DOC_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_COLL_IDX,2], .t., this.DOC_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COLLEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COLLEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DCCODICE,DCCOLLEG,DCFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where DCCOLLEG="+cp_ToStrODBC(this.w_COLLEG);
                   +" and DCCODICE="+cp_ToStrODBC(this.w_MVTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DCCODICE',this.w_MVTIPDOC;
                       ,'DCCOLLEG',this.w_COLLEG)
            select DCCODICE,DCCOLLEG,DCFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COLLEG = NVL(_Link_.DCCOLLEG,space(5))
      this.w_FLINTE = NVL(_Link_.DCFLINTE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COLLEG = space(5)
      endif
      this.w_FLINTE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_COLL_IDX,2])+'\'+cp_ToStr(_Link_.DCCODICE,1)+'\'+cp_ToStr(_Link_.DCCOLLEG,1)
      cp_ShowWarn(i_cKey,this.DOC_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COLLEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILCLI
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_FILCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FLVEAC1);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_FLVEAC1;
                     ,'ANCODICE',trim(this.w_FILCLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oFILCLI_2_25'),i_cWhere,'GSAR_BZC',"Anagrafica clienti/fornitori",'GSCG1MPN.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLVEAC1<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_FLVEAC1);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_FILCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_FLVEAC1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_FLVEAC1;
                       ,'ANCODICE',this.w_FILCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FILCLI = space(15)
      endif
      this.w_DESCLI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODDES
  func Link_2_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_CODDES)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_FLVEAC1);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CLIFOR);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_FLVEAC1;
                     ,'DDCODICE',this.w_CLIFOR;
                     ,'DDCODDES',trim(this.w_CODDES))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODDES)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DDNOMDES like "+cp_ToStrODBC(trim(this.w_CODDES)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_FLVEAC1);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CLIFOR);

            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DDNOMDES like "+cp_ToStr(trim(this.w_CODDES)+"%");
                   +" and DDTIPCON="+cp_ToStr(this.w_FLVEAC1);
                   +" and DDCODICE="+cp_ToStr(this.w_CLIFOR);

            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODDES) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oCODDES_2_28'),i_cWhere,'',"Destinazioni diverse",'GSVE0KFD.DES_DIVE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_FLVEAC1<>oSource.xKey(1);
           .or. this.w_CLIFOR<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_FLVEAC1);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_CLIFOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_CODDES);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_FLVEAC1);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CLIFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_FLVEAC1;
                       ,'DDCODICE',this.w_CLIFOR;
                       ,'DDCODDES',this.w_CODDES)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODDES = NVL(_Link_.DDCODDES,space(5))
      this.w_DESDES = NVL(_Link_.DDNOMDES,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODDES = space(5)
      endif
      this.w_DESDES = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILCOMM
  func Link_2_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILCOMM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_FILCOMM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_FILCOMM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILCOMM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILCOMM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oFILCOMM_2_30'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILCOMM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_FILCOMM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_FILCOMM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILCOMM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCOM = NVL(_Link_.CNDESCAN,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FILCOMM = space(15)
      endif
      this.w_DESCOM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILCOMM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILCEN
  func Link_2_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILCEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_FILCEN)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_FILCEN))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILCEN)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILCEN) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oFILCEN_2_33'),i_cWhere,'GSCA_ACC',"Centro di costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILCEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_FILCEN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_FILCEN)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILCEN = NVL(_Link_.CC_CONTO,space(15))
      this.w_DESCEN = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FILCEN = space(15)
      endif
      this.w_DESCEN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILCEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILATTI
  func Link_2_37(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIVITA_IDX,3]
    i_lTable = "ATTIVITA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2], .t., this.ATTIVITA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILATTI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPC_BZZ',True,'ATTIVITA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_FILATTI)+"%");

          i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODATT',trim(this.w_FILATTI))
          select ATCODATT,ATDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILATTI)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILATTI) and !this.bDontReportError
            deferred_cp_zoom('ATTIVITA','*','ATCODATT',cp_AbsName(oSource.parent,'oFILATTI_2_37'),i_cWhere,'GSPC_BZZ',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',oSource.xKey(1))
            select ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILATTI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_FILATTI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_FILATTI)
            select ATCODATT,ATDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILATTI = NVL(_Link_.ATCODATT,space(15))
      this.w_DESATTI = NVL(_Link_.ATDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FILATTI = space(15)
      endif
      this.w_DESATTI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIVITA_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIVITA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILATTI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILVOC
  func Link_2_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_COST_IDX,3]
    i_lTable = "VOC_COST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2], .t., this.VOC_COST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILVOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_AVC',True,'VOC_COST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VCCODICE like "+cp_ToStrODBC(trim(this.w_FILVOC)+"%");

          i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VCCODICE',trim(this.w_FILVOC))
          select VCCODICE,VCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILVOC)==trim(_Link_.VCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FILVOC) and !this.bDontReportError
            deferred_cp_zoom('VOC_COST','*','VCCODICE',cp_AbsName(oSource.parent,'oFILVOC_2_40'),i_cWhere,'GSCA_AVC',"Voci di costo/rica",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',oSource.xKey(1))
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILVOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VCCODICE,VCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where VCCODICE="+cp_ToStrODBC(this.w_FILVOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VCCODICE',this.w_FILVOC)
            select VCCODICE,VCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILVOC = NVL(_Link_.VCCODICE,space(15))
      this.w_DESVOC = NVL(_Link_.VCDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FILVOC = space(15)
      endif
      this.w_DESVOC = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_COST_IDX,2])+'\'+cp_ToStr(_Link_.VCCODICE,1)
      cp_ShowWarn(i_cKey,this.VOC_COST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILVOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FILRES
  func Link_2_52(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIPENDEN_IDX,3]
    i_lTable = "DIPENDEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2], .t., this.DIPENDEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FILRES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DIPENDEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DPCODICE like "+cp_ToStrODBC(trim(this.w_FILRES)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

          i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DPTIPRIS,DPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DPTIPRIS',this.w_TipoRisorsa;
                     ,'DPCODICE',trim(this.w_FILRES))
          select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DPTIPRIS,DPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FILRES)==trim(_Link_.DPCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStrODBC(trim(this.w_FILRES)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPCOGNOM like "+cp_ToStr(trim(this.w_FILRES)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStrODBC(trim(this.w_FILRES)+"%");
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);

            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DPNOME like "+cp_ToStr(trim(this.w_FILRES)+"%");
                   +" and DPTIPRIS="+cp_ToStr(this.w_TipoRisorsa);

            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FILRES) and !this.bDontReportError
            deferred_cp_zoom('DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(oSource.parent,'oFILRES_2_52'),i_cWhere,'',"Responsabili",'GSARPADP.DIPENDEN_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipoRisorsa<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                     +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',oSource.xKey(1);
                       ,'DPCODICE',oSource.xKey(2))
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FILRES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME";
                   +" from "+i_cTable+" "+i_lTable+" where DPCODICE="+cp_ToStrODBC(this.w_FILRES);
                   +" and DPTIPRIS="+cp_ToStrODBC(this.w_TipoRisorsa);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DPTIPRIS',this.w_TipoRisorsa;
                       ,'DPCODICE',this.w_FILRES)
            select DPTIPRIS,DPCODICE,DPCOGNOM,DPNOME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FILRES = NVL(_Link_.DPCODICE,space(5))
      this.w_COGNPART = NVL(_Link_.DPCOGNOM,space(40))
      this.w_NOMEPART = NVL(_Link_.DPNOME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FILRES = space(5)
      endif
      this.w_COGNPART = space(40)
      this.w_NOMEPART = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIPENDEN_IDX,2])+'\'+cp_ToStr(_Link_.DPTIPRIS,1)+'\'+cp_ToStr(_Link_.DPCODICE,1)
      cp_ShowWarn(i_cKey,this.DIPENDEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FILRES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFILCOMM_1_11.value==this.w_FILCOMM)
      this.oPgFrm.Page1.oPag.oFILCOMM_1_11.value=this.w_FILCOMM
    endif
    if not(this.oPgFrm.Page1.oPag.oFLRICIVA_1_12.RadioValue()==this.w_FLRICIVA)
      this.oPgFrm.Page1.oPag.oFLRICIVA_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLNSRI_1_13.RadioValue()==this.w_FLNSRI)
      this.oPgFrm.Page1.oPag.oFLNSRI_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLVSRI_1_14.RadioValue()==this.w_FLVSRI)
      this.oPgFrm.Page1.oPag.oFLVSRI_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLRIDE_1_15.RadioValue()==this.w_FLRIDE)
      this.oPgFrm.Page1.oPag.oFLRIDE_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILSPE_1_16.RadioValue()==this.w_FILSPE)
      this.oPgFrm.Page1.oPag.oFILSPE_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILANT_1_17.RadioValue()==this.w_FILANT)
      this.oPgFrm.Page1.oPag.oFILANT_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILGEN_1_18.RadioValue()==this.w_FILGEN)
      this.oPgFrm.Page1.oPag.oFILGEN_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILDIR_1_19.RadioValue()==this.w_FILDIR)
      this.oPgFrm.Page1.oPag.oFILDIR_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILONO_1_20.RadioValue()==this.w_FILONO)
      this.oPgFrm.Page1.oPag.oFILONO_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILTEM_1_21.RadioValue()==this.w_FILTEM)
      this.oPgFrm.Page1.oPag.oFILTEM_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCAUMAG_1_22.RadioValue()==this.w_FLCAUMAG)
      this.oPgFrm.Page1.oPag.oFLCAUMAG_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPARCAMAG_1_23.value==this.w_PARCAMAG)
      this.oPgFrm.Page1.oPag.oPARCAMAG_1_23.value=this.w_PARCAMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oFLMGPR_1_24.RadioValue()==this.w_FLMGPR)
      this.oPgFrm.Page1.oPag.oFLMGPR_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGTP_1_25.value==this.w_MAGTP)
      this.oPgFrm.Page1.oPag.oMAGTP_1_25.value=this.w_MAGTP
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMAG_1_26.value==this.w_COMAG)
      this.oPgFrm.Page1.oPag.oCOMAG_1_26.value=this.w_COMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oFLMTPR_1_27.RadioValue()==this.w_FLMTPR)
      this.oPgFrm.Page1.oPag.oFLMTPR_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLFOM_1_28.RadioValue()==this.w_FLFOM)
      this.oPgFrm.Page1.oPag.oFLFOM_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCOAC_1_29.RadioValue()==this.w_FLCOAC)
      this.oPgFrm.Page1.oPag.oFLCOAC_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMAT_1_30.value==this.w_COMAT)
      this.oPgFrm.Page1.oPag.oCOMAT_1_30.value=this.w_COMAT
    endif
    if not(this.oPgFrm.Page1.oPag.oMAGTC_1_31.value==this.w_MAGTC)
      this.oPgFrm.Page1.oPag.oMAGTC_1_31.value=this.w_MAGTC
    endif
    if not(this.oPgFrm.Page2.oPag.oDATINI_2_2.value==this.w_DATINI)
      this.oPgFrm.Page2.oPag.oDATINI_2_2.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDATFIN_2_3.value==this.w_DATFIN)
      this.oPgFrm.Page2.oPag.oDATFIN_2_3.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oEVAINI_2_4.value==this.w_EVAINI)
      this.oPgFrm.Page2.oPag.oEVAINI_2_4.value=this.w_EVAINI
    endif
    if not(this.oPgFrm.Page2.oPag.oEVAFIN_2_5.value==this.w_EVAFIN)
      this.oPgFrm.Page2.oPag.oEVAFIN_2_5.value=this.w_EVAFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oFLTRASF_2_6.RadioValue()==this.w_FLTRASF)
      this.oPgFrm.Page2.oPag.oFLTRASF_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILART_2_7.value==this.w_FILART)
      this.oPgFrm.Page2.oPag.oFILART_2_7.value=this.w_FILART
    endif
    if not(this.oPgFrm.Page2.oPag.oFILARM_2_8.value==this.w_FILARM)
      this.oPgFrm.Page2.oPag.oFILARM_2_8.value=this.w_FILARM
    endif
    if not(this.oPgFrm.Page2.oPag.oFILMAG_2_9.value==this.w_FILMAG)
      this.oPgFrm.Page2.oPag.oFILMAG_2_9.value=this.w_FILMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oFILCAU_2_12.value==this.w_FILCAU)
      this.oPgFrm.Page2.oPag.oFILCAU_2_12.value=this.w_FILCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFAR_2_17.value==this.w_DESFAR)
      this.oPgFrm.Page2.oPag.oDESFAR_2_17.value=this.w_DESFAR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESARM_2_19.value==this.w_DESARM)
      this.oPgFrm.Page2.oPag.oDESARM_2_19.value=this.w_DESARM
    endif
    if not(this.oPgFrm.Page2.oPag.oFILCLI_2_25.value==this.w_FILCLI)
      this.oPgFrm.Page2.oPag.oFILCLI_2_25.value=this.w_FILCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLI_2_27.value==this.w_DESCLI)
      this.oPgFrm.Page2.oPag.oDESCLI_2_27.value=this.w_DESCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODDES_2_28.value==this.w_CODDES)
      this.oPgFrm.Page2.oPag.oCODDES_2_28.value=this.w_CODDES
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU_2_29.value==this.w_DESCAU)
      this.oPgFrm.Page2.oPag.oDESCAU_2_29.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_88.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_88.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_89.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_89.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZ1_1_90.RadioValue()==this.w_SELEZ1)
      this.oPgFrm.Page1.oPag.oSELEZ1_1_90.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFILCOMM_2_30.value==this.w_FILCOMM)
      this.oPgFrm.Page2.oPag.oFILCOMM_2_30.value=this.w_FILCOMM
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOM_2_31.value==this.w_DESCOM)
      this.oPgFrm.Page2.oPag.oDESCOM_2_31.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oFILCEN_2_33.value==this.w_FILCEN)
      this.oPgFrm.Page2.oPag.oFILCEN_2_33.value=this.w_FILCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCEN_2_34.value==this.w_DESCEN)
      this.oPgFrm.Page2.oPag.oDESCEN_2_34.value=this.w_DESCEN
    endif
    if not(this.oPgFrm.Page2.oPag.oFILATTI_2_37.value==this.w_FILATTI)
      this.oPgFrm.Page2.oPag.oFILATTI_2_37.value=this.w_FILATTI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESATTI_2_38.value==this.w_DESATTI)
      this.oPgFrm.Page2.oPag.oDESATTI_2_38.value=this.w_DESATTI
    endif
    if not(this.oPgFrm.Page2.oPag.oFILVOC_2_40.value==this.w_FILVOC)
      this.oPgFrm.Page2.oPag.oFILVOC_2_40.value=this.w_FILVOC
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVOC_2_41.value==this.w_DESVOC)
      this.oPgFrm.Page2.oPag.oDESVOC_2_41.value=this.w_DESVOC
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAG_2_44.value==this.w_DESMAG)
      this.oPgFrm.Page2.oPag.oDESMAG_2_44.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oSCOPE_1_101.value==this.w_SCOPE)
      this.oPgFrm.Page1.oPag.oSCOPE_1_101.value=this.w_SCOPE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDOC_2_48.value==this.w_DESDOC)
      this.oPgFrm.Page2.oPag.oDESDOC_2_48.value=this.w_DESDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCOM_1_109.value==this.w_DESCOM)
      this.oPgFrm.Page1.oPag.oDESCOM_1_109.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oFILRES_2_52.value==this.w_FILRES)
      this.oPgFrm.Page2.oPag.oFILRES_2_52.value=this.w_FILRES
    endif
    if not(this.oPgFrm.Page2.oPag.oDENOM_PART_2_53.value==this.w_DENOM_PART)
      this.oPgFrm.Page2.oPag.oDENOM_PART_2_53.value=this.w_DENOM_PART
    endif
    if not(this.oPgFrm.Page2.oPag.oPREINI_2_56.value==this.w_PREINI)
      this.oPgFrm.Page2.oPag.oPREINI_2_56.value=this.w_PREINI
    endif
    if not(this.oPgFrm.Page2.oPag.oPREFIN_2_57.value==this.w_PREFIN)
      this.oPgFrm.Page2.oPag.oPREFIN_2_57.value=this.w_PREFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESDES_2_61.value==this.w_DESDES)
      this.oPgFrm.Page2.oPag.oDESDES_2_61.value=this.w_DESDES
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPDORI_2_63.RadioValue()==this.w_TIPDORI)
      this.oPgFrm.Page2.oPag.oTIPDORI_2_63.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMDOCORI_2_64.value==this.w_NUMDOCORI)
      this.oPgFrm.Page2.oPag.oNUMDOCORI_2_64.value=this.w_NUMDOCORI
    endif
    if not(this.oPgFrm.Page2.oPag.oALFDOCORI_2_67.value==this.w_ALFDOCORI)
      this.oPgFrm.Page2.oPag.oALFDOCORI_2_67.value=this.w_ALFDOCORI
    endif
    if not(this.oPgFrm.Page2.oPag.oDATDOCORI_2_68.value==this.w_DATDOCORI)
      this.oPgFrm.Page2.oPag.oDATDOCORI_2_68.value=this.w_DATDOCORI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAMAG_1_191.value==this.w_DESCAMAG)
      this.oPgFrm.Page1.oPag.oDESCAMAG_1_191.value=this.w_DESCAMAG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(CHKPRCLI(.w_FILCOMM,.w_CODCON))  and not(!IsAlt() OR (IsAlt() AND g_PRAT<>'S'))  and not(empty(.w_FILCOMM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oFILCOMM_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pratica incongruente")
          case   not(nvl(.w_FLCOMM,'N')='N' or empty(.w_FLCOMM))  and not(.w_FLCAUMAG<>'S')  and not(empty(.w_PARCAMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPARCAMAG_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_COMAG))  and not(NOT .w_FLMGPR='F' OR IsAlt())  and (.w_FLMGPR='F')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMAG_1_26.SetFocus()
            i_bnoObbl = !empty(.w_COMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_COMAT))  and not(NOT (.w_FLMTPR='F' AND NOT EMPTY(.w_CAUCOL)))  and (.w_FLMTPR='F' AND NOT EMPTY(.w_CAUCOL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOMAT_1_30.SetFocus()
            i_bnoObbl = !empty(.w_COMAT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PREINI<=.w_PREFIN OR EMPTY(.w_PREFIN))  and not(!IsAlt())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPREINI_2_56.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
          case   not(.w_PREINI<=.w_PREFIN OR EMPTY(.w_PREFIN))  and not(!IsAlt())
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPREFIN_2_57.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsve_kim
      * --- Controlli Finali
      If IsAlt() AND this.w_FLPRAT='S' AND EMPTY(this.w_FILCOMM) AND EMPTY(this.w_NOMECUR)
         If NOT ah_YesNo("Attenzione, pratica non inserita: i documenti selezionati potrebbero appartenere a pratiche o a CLIENTI DIVERSI. Vuoi proseguire?")
            return (.F.)
         Endif
      Endif
      this.w_TESTERR=.T.
      this.NotifyEvent('ControlliFinali')
      if Not this.w_TESTERR Or Not i_bRes
       Return (this.w_TESTERR)
      ELSE
      * SE GSVE_KIM E' CHIAMATA DA GSAR_MPG OCCORRE DISABILITARE IL BOTTONE DI IMPORTAZIONE IN GSAR_MPG
        IF TYPE( "THIS.w_GESTPARENT.w_ESEGUITOIMPORT" ) ="L"
          THIS.w_GESTPARENT.w_ESEGUITOIMPORT = .T.
        ENDIF
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FLRICIVA = this.w_FLRICIVA
    this.o_CALSER = this.w_CALSER
    this.o_DATFIN = this.w_DATFIN
    this.o_FILCAU = this.w_FILCAU
    this.o_CLIFOR = this.w_CLIFOR
    this.o_FLVEAC1 = this.w_FLVEAC1
    this.o_FILCLI = this.w_FILCLI
    this.o_DESDOC = this.w_DESDOC
    this.o_RIIVDTIN = this.w_RIIVDTIN
    return

enddefine

* --- Define pages as container
define class tgsve_kimPag1 as StdContainer
  Width  = 800
  height = 510
  stdWidth  = 800
  stdheight = 510
  resizeXpos=664
  resizeYpos=283
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oFILCOMM_1_11 as StdField with uid="LWIOQHIFTY",rtseq=11,rtrep=.f.,;
    cFormVar = "w_FILCOMM", cQueryName = "FILCOMM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pratica incongruente",;
    ToolTipText = "Eventuale codice pratica di selezione (vuoto = nessuna selezione)",;
    HelpContextID = 38656854,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=60, Top=100, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSPR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_FILCOMM"

  func oFILCOMM_1_11.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR (IsAlt() AND g_PRAT<>'S'))
    endwith
  endfunc

  func oFILCOMM_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILCOMM_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILCOMM_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oFILCOMM_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPR_ACN',""+iif(not isalt (), "commesse", "pratiche") +"",'GSPR2PPD.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oFILCOMM_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSPR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_FILCOMM
     i_obj.ecpSave()
  endproc

  add object oFLRICIVA_1_12 as StdCheck with uid="KJZMSHAWAR",rtseq=12,rtrep=.f.,left=578, top=103, caption="Ricalcola codici I.V.A.",;
    ToolTipText = "Ricalcola codici I.V.A.",;
    HelpContextID = 40616297,;
    cFormVar="w_FLRICIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLRICIVA_1_12.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oFLRICIVA_1_12.GetRadio()
    this.Parent.oContained.w_FLRICIVA = this.RadioValue()
    return .t.
  endfunc

  func oFLRICIVA_1_12.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLRICIVA==.T.,1,;
      0)
  endfunc

  add object oFLNSRI_1_13 as StdCheck with uid="QNJYIHULOC",rtseq=13,rtrep=.f.,left=16, top=6, caption="Nostro riferimento",;
    ToolTipText = "Se attivo: aggiunge una riga descrittiva di nostro riferimento ad ogni nuovo doc",;
    HelpContextID = 24248746,;
    cFormVar="w_FLNSRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLNSRI_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLNSRI_1_13.GetRadio()
    this.Parent.oContained.w_FLNSRI = this.RadioValue()
    return .t.
  endfunc

  func oFLNSRI_1_13.SetRadio()
    this.Parent.oContained.w_FLNSRI=trim(this.Parent.oContained.w_FLNSRI)
    this.value = ;
      iif(this.Parent.oContained.w_FLNSRI=='S',1,;
      0)
  endfunc

  func oFLNSRI_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(g_ARTDES))
    endwith
   endif
  endfunc

  add object oFLVSRI_1_14 as StdCheck with uid="JTVUITDNDV",rtseq=14,rtrep=.f.,left=16, top=30, caption="Vostro riferimento",;
    ToolTipText = "Se attivo: aggiunge una riga descrittiva di vostro riferimento ad ogni nuovo doc",;
    HelpContextID = 24215978,;
    cFormVar="w_FLVSRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLVSRI_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLVSRI_1_14.GetRadio()
    this.Parent.oContained.w_FLVSRI = this.RadioValue()
    return .t.
  endfunc

  func oFLVSRI_1_14.SetRadio()
    this.Parent.oContained.w_FLVSRI=trim(this.Parent.oContained.w_FLVSRI)
    this.value = ;
      iif(this.Parent.oContained.w_FLVSRI=='S',1,;
      0)
  endfunc

  func oFLVSRI_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(g_ARTDES))
    endwith
   endif
  endfunc

  add object oFLRIDE_1_15 as StdCheck with uid="ONSACLVYYL",rtseq=15,rtrep=.f.,left=16, top=54, caption="Rif. descrittivo",;
    ToolTipText = "Se attivo: aggiunge una riga descrittiva di rif. descrittivo ad ogni nuovo doc",;
    HelpContextID = 106676650,;
    cFormVar="w_FLRIDE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLRIDE_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLRIDE_1_15.GetRadio()
    this.Parent.oContained.w_FLRIDE = this.RadioValue()
    return .t.
  endfunc

  func oFLRIDE_1_15.SetRadio()
    this.Parent.oContained.w_FLRIDE=trim(this.Parent.oContained.w_FLRIDE)
    this.value = ;
      iif(this.Parent.oContained.w_FLRIDE=='S',1,;
      0)
  endfunc

  func oFLRIDE_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(g_ARTDES))
    endwith
   endif
  endfunc

  add object oFILSPE_1_16 as StdCheck with uid="NSRPWXABEU",rtseq=16,rtrep=.f.,left=143, top=6, caption="Inserisci spese",;
    ToolTipText = "Se attivo: durante l'import inserisce le righe del documento di origine con tipologia spesa",;
    HelpContextID = 93463722,;
    cFormVar="w_FILSPE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFILSPE_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILSPE_1_16.GetRadio()
    this.Parent.oContained.w_FILSPE = this.RadioValue()
    return .t.
  endfunc

  func oFILSPE_1_16.SetRadio()
    this.Parent.oContained.w_FILSPE=trim(this.Parent.oContained.w_FILSPE)
    this.value = ;
      iif(this.Parent.oContained.w_FILSPE=='S',1,;
      0)
  endfunc

  func oFILSPE_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oFILSPE_1_16.mHide()
    with this.Parent.oContained
      return (Not IsAlt())
    endwith
  endfunc

  add object oFILANT_1_17 as StdCheck with uid="ZBHTAYKRJU",rtseq=17,rtrep=.f.,left=143, top=30, caption="Inserisci anticipazioni",;
    ToolTipText = "Se attivo: durante l'import inserisce le righe del documento di origine con tipologia anticipazione",;
    HelpContextID = 113517738,;
    cFormVar="w_FILANT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFILANT_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILANT_1_17.GetRadio()
    this.Parent.oContained.w_FILANT = this.RadioValue()
    return .t.
  endfunc

  func oFILANT_1_17.SetRadio()
    this.Parent.oContained.w_FILANT=trim(this.Parent.oContained.w_FILANT)
    this.value = ;
      iif(this.Parent.oContained.w_FILANT=='S',1,;
      0)
  endfunc

  func oFILANT_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oFILANT_1_17.mHide()
    with this.Parent.oContained
      return (Not IsAlt())
    endwith
  endfunc

  add object oFILGEN_1_18 as StdCheck with uid="ZSNLJHJMJF",rtseq=18,rtrep=.f.,left=143, top=54, caption="Inserisci prestazioni generiche",;
    ToolTipText = "Se attivo: durante l'import inserisce le righe del documento di origine con tipologia prestazioni generiche",;
    HelpContextID = 223225002,;
    cFormVar="w_FILGEN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFILGEN_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILGEN_1_18.GetRadio()
    this.Parent.oContained.w_FILGEN = this.RadioValue()
    return .t.
  endfunc

  func oFILGEN_1_18.SetRadio()
    this.Parent.oContained.w_FILGEN=trim(this.Parent.oContained.w_FILGEN)
    this.value = ;
      iif(this.Parent.oContained.w_FILGEN=='S',1,;
      0)
  endfunc

  func oFILGEN_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oFILGEN_1_18.mHide()
    with this.Parent.oContained
      return (Not IsAlt())
    endwith
  endfunc

  add object oFILDIR_1_19 as StdCheck with uid="DQHPYYXMQD",rtseq=19,rtrep=.f.,left=347, top=6, caption="Inserisci diritti",;
    ToolTipText = "Se attivo: durante l'import inserisce le righe del documento di origine con tipologia diritti",;
    HelpContextID = 152118442,;
    cFormVar="w_FILDIR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFILDIR_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILDIR_1_19.GetRadio()
    this.Parent.oContained.w_FILDIR = this.RadioValue()
    return .t.
  endfunc

  func oFILDIR_1_19.SetRadio()
    this.Parent.oContained.w_FILDIR=trim(this.Parent.oContained.w_FILDIR)
    this.value = ;
      iif(this.Parent.oContained.w_FILDIR=='S',1,;
      0)
  endfunc

  func oFILDIR_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oFILDIR_1_19.mHide()
    with this.Parent.oContained
      return (Not IsAlt())
    endwith
  endfunc

  add object oFILONO_1_20 as StdCheck with uid="YDTCIXXGAJ",rtseq=20,rtrep=.f.,left=347, top=30, caption="Inserisci onorari",;
    ToolTipText = "Se attivo: durante l'import inserisce le righe del documento di origine con tipologia onorari",;
    HelpContextID = 196486314,;
    cFormVar="w_FILONO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFILONO_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILONO_1_20.GetRadio()
    this.Parent.oContained.w_FILONO = this.RadioValue()
    return .t.
  endfunc

  func oFILONO_1_20.SetRadio()
    this.Parent.oContained.w_FILONO=trim(this.Parent.oContained.w_FILONO)
    this.value = ;
      iif(this.Parent.oContained.w_FILONO=='S',1,;
      0)
  endfunc

  func oFILONO_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oFILONO_1_20.mHide()
    with this.Parent.oContained
      return (Not IsAlt())
    endwith
  endfunc

  add object oFILTEM_1_21 as StdCheck with uid="ZRJCOEPENN",rtseq=21,rtrep=.f.,left=347, top=54, caption="Inserisci prestazioni a tempo",;
    ToolTipText = "Se attivo: durante l'import inserisce le righe del documento di origine con tipologia prestazioni a tempo",;
    HelpContextID = 239150250,;
    cFormVar="w_FILTEM", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFILTEM_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILTEM_1_21.GetRadio()
    this.Parent.oContained.w_FILTEM = this.RadioValue()
    return .t.
  endfunc

  func oFILTEM_1_21.SetRadio()
    this.Parent.oContained.w_FILTEM=trim(this.Parent.oContained.w_FILTEM)
    this.value = ;
      iif(this.Parent.oContained.w_FILTEM=='S',1,;
      0)
  endfunc

  func oFILTEM_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oFILTEM_1_21.mHide()
    with this.Parent.oContained
      return (Not IsAlt())
    endwith
  endfunc

  add object oFLCAUMAG_1_22 as StdCheck with uid="CYYCBPXBQU",rtseq=22,rtrep=.f.,left=16, top=76, caption="Modifica causale di magazzino per gestione progetti",;
    ToolTipText = "Se attivo, la causale di magazzino delle righe verr� sostituita con questa causale che non gestisce la commessa",;
    HelpContextID = 223654243,;
    cFormVar="w_FLCAUMAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCAUMAG_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLCAUMAG_1_22.GetRadio()
    this.Parent.oContained.w_FLCAUMAG = this.RadioValue()
    return .t.
  endfunc

  func oFLCAUMAG_1_22.SetRadio()
    this.Parent.oContained.w_FLCAUMAG=trim(this.Parent.oContained.w_FLCAUMAG)
    this.value = ;
      iif(this.Parent.oContained.w_FLCAUMAG=='S',1,;
      0)
  endfunc

  func oFLCAUMAG_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLGCOM='S' AND .w_TDFLNORC='S')
    endwith
   endif
  endfunc

  add object oPARCAMAG_1_23 as StdField with uid="EXBGBOPYUH",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PARCAMAG", cQueryName = "PARCAMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale magazzino di default (importa documenti)",;
    HelpContextID = 244435907,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=459, Top=76, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAM_AGAZ", cZoomOnZoom="gsma_acm", oKey_1_1="CMCODICE", oKey_1_2="this.w_PARCAMAG"

  func oPARCAMAG_1_23.mHide()
    with this.Parent.oContained
      return (.w_FLCAUMAG<>'S')
    endwith
  endfunc

  func oPARCAMAG_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oPARCAMAG_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPARCAMAG_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAM_AGAZ','*','CMCODICE',cp_AbsName(this.parent,'oPARCAMAG_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'gsma_acm',"Elenco causali magazzino",'GSPC_CAU.CAM_AGAZ_VZM',this.parent.oContained
  endproc
  proc oPARCAMAG_1_23.mZoomOnZoom
    local i_obj
    i_obj=gsma_acm()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CMCODICE=this.parent.oContained.w_PARCAMAG
     i_obj.ecpSave()
  endproc


  add object oFLMGPR_1_24 as StdCombo with uid="ROEVYALQPC",rtseq=24,rtrep=.f.,left=262,top=6,width=128,height=21;
    , ToolTipText = "Metodo di attribuzione del magazzino per le righe di import";
    , HelpContextID = 144576938;
    , cFormVar="w_FLMGPR",RowSource=""+"Default,"+"Origine,"+"Forzato,"+"Intestatario,"+"Preferenziale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLMGPR_1_24.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'O',;
    iif(this.value =3,'F',;
    iif(this.value =4,'I',;
    iif(this.value =5,'P',;
    space(1)))))))
  endfunc
  func oFLMGPR_1_24.GetRadio()
    this.Parent.oContained.w_FLMGPR = this.RadioValue()
    return .t.
  endfunc

  func oFLMGPR_1_24.SetRadio()
    this.Parent.oContained.w_FLMGPR=trim(this.Parent.oContained.w_FLMGPR)
    this.value = ;
      iif(this.Parent.oContained.w_FLMGPR=='D',1,;
      iif(this.Parent.oContained.w_FLMGPR=='O',2,;
      iif(this.Parent.oContained.w_FLMGPR=='F',3,;
      iif(this.Parent.oContained.w_FLMGPR=='I',4,;
      iif(this.Parent.oContained.w_FLMGPR=='P',5,;
      0)))))
  endfunc

  func oFLMGPR_1_24.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oMAGTP_1_25 as StdField with uid="VIJNZRLNSW",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MAGTP", cQueryName = "MAGTP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 91128774,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=395, Top=6, InputMask=replicate('X',5)

  func oMAGTP_1_25.mHide()
    with this.Parent.oContained
      return (NOT .w_FLMGPR='I')
    endwith
  endfunc

  add object oCOMAG_1_26 as StdField with uid="AKCGITEZAL",rtseq=26,rtrep=.f.,;
    cFormVar = "w_COMAG", cQueryName = "COMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 80474406,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=395, Top=6, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_COMAG"

  func oCOMAG_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLMGPR='F')
    endwith
   endif
  endfunc

  func oCOMAG_1_26.mHide()
    with this.Parent.oContained
      return (NOT .w_FLMGPR='F' OR IsAlt())
    endwith
  endfunc

  func oCOMAG_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMAG_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMAG_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCOMAG_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCOMAG_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_COMAG
     i_obj.ecpSave()
  endproc


  add object oFLMTPR_1_27 as StdCombo with uid="FORBWTTMHN",rtseq=27,rtrep=.f.,left=262,top=30,width=128,height=21;
    , ToolTipText = "Metodo di attribuzione del magazzino collegato per le righe di import";
    , HelpContextID = 143724970;
    , cFormVar="w_FLMTPR",RowSource=""+"Default,"+"Origine,"+"Forzato,"+"Intestatario,"+"Preferenziale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLMTPR_1_27.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'O',;
    iif(this.value =3,'F',;
    iif(this.value =4,'I',;
    iif(this.value =5,'P',;
    space(1)))))))
  endfunc
  func oFLMTPR_1_27.GetRadio()
    this.Parent.oContained.w_FLMTPR = this.RadioValue()
    return .t.
  endfunc

  func oFLMTPR_1_27.SetRadio()
    this.Parent.oContained.w_FLMTPR=trim(this.Parent.oContained.w_FLMTPR)
    this.value = ;
      iif(this.Parent.oContained.w_FLMTPR=='D',1,;
      iif(this.Parent.oContained.w_FLMTPR=='O',2,;
      iif(this.Parent.oContained.w_FLMTPR=='F',3,;
      iif(this.Parent.oContained.w_FLMTPR=='I',4,;
      iif(this.Parent.oContained.w_FLMTPR=='P',5,;
      0)))))
  endfunc

  func oFLMTPR_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CAUCOL))
    endwith
   endif
  endfunc

  func oFLMTPR_1_27.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUCOL) OR IsAlt())
    endwith
  endfunc


  add object oFLFOM_1_28 as StdCombo with uid="SVOWZMOVOO",rtseq=28,rtrep=.f.,left=262,top=53,width=198,height=21;
    , ToolTipText = "Forza dati del documento di origine o mantieni dati del documento di destinazione";
    , HelpContextID = 87653974;
    , cFormVar="w_FLFOM",RowSource=""+"Mantieni dati doc. destinazione,"+"Forza dati doc.origine", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLFOM_1_28.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oFLFOM_1_28.GetRadio()
    this.Parent.oContained.w_FLFOM = this.RadioValue()
    return .t.
  endfunc

  func oFLFOM_1_28.SetRadio()
    this.Parent.oContained.w_FLFOM=trim(this.Parent.oContained.w_FLFOM)
    this.value = ;
      iif(this.Parent.oContained.w_FLFOM=='M',1,;
      iif(this.Parent.oContained.w_FLFOM=='F',2,;
      0))
  endfunc

  func oFLFOM_1_28.mHide()
    with this.Parent.oContained
      return (IsAlt() OR  .w_GIAIMP<>'S' OR (.w_GESTPARENT.NUMROW()<>0  AND Upper(.w_GESTPARENT.CLASS)<>'TGSAR_MPG') OR  .w_GESTPARENT.cfunction<>'Load')
    endwith
  endfunc

  add object oFLCOAC_1_29 as StdCheck with uid="OLZBSTMPHG",rtseq=29,rtrep=.f.,left=262, top=103, caption="Contributi accessori",;
    ToolTipText = "Se attivo i contributi accessori saranno importati proporzionalmente all'articolo padre che li ha determinati",;
    HelpContextID = 143045034,;
    cFormVar="w_FLCOAC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLCOAC_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oFLCOAC_1_29.GetRadio()
    this.Parent.oContained.w_FLCOAC = this.RadioValue()
    return .t.
  endfunc

  func oFLCOAC_1_29.SetRadio()
    this.Parent.oContained.w_FLCOAC=trim(this.Parent.oContained.w_FLCOAC)
    this.value = ;
      iif(this.Parent.oContained.w_FLCOAC=='S',1,;
      0)
  endfunc

  func oFLCOAC_1_29.mHide()
    with this.Parent.oContained
      return (g_COAC='N' or IsAlt())
    endwith
  endfunc

  add object oCOMAT_1_30 as StdField with uid="PAZXEGIXJI",rtseq=30,rtrep=.f.,;
    cFormVar = "w_COMAT", cQueryName = "COMAT",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 94105894,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=395, Top=30, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_COMAT"

  func oCOMAT_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLMTPR='F' AND NOT EMPTY(.w_CAUCOL))
    endwith
   endif
  endfunc

  func oCOMAT_1_30.mHide()
    with this.Parent.oContained
      return (NOT (.w_FLMTPR='F' AND NOT EMPTY(.w_CAUCOL)))
    endwith
  endfunc

  func oCOMAT_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOMAT_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOMAT_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCOMAT_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCOMAT_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_COMAT
     i_obj.ecpSave()
  endproc

  add object oMAGTC_1_31 as StdField with uid="CVXWJUSENF",rtseq=31,rtrep=.f.,;
    cFormVar = "w_MAGTC", cQueryName = "MAGTC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 77497286,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=395, Top=30, InputMask=replicate('X',5)

  func oMAGTC_1_31.mHide()
    with this.Parent.oContained
      return ((NOT (.w_FLMTPR='I' AND NOT EMPTY(.w_CAUCOL))) OR IsAlt())
    endwith
  endfunc


  add object ZoomMast as cp_szoombox with uid="VJRPDOWVIW",left=-3, top=140, width=269,height=297,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSVE_KIM",bOptions=.f.,bQueryOnLoad=.f.,cZoomOnZoom="GSZM_BZM",bAdvOptions=.t.,bReadOnly=.t.,cMenuFile="",bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 89008922


  add object oBtn_1_38 as StdButton with uid="ITPKGUGILQ",left=498, top=465, width=48,height=45,;
    CpPicture="bmp\conclusi.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per mostrare solo la testata dei documenti e nascondere le righe";
    , HelpContextID = 90085322;
    , tabStop=.f., Caption='\<Testata';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      with this.Parent.oContained
        this.parent.ocontained.modificazoom( 'NascondiRighe')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_38.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((!(.w_FLMTPR='F' AND NOT EMPTY(.w_CAUCOL)) Or !Empty(.w_COMAT)) And (!.w_FLMGPR='F' Or !Empty(.w_COMAG)))
      endwith
    endif
  endfunc

  func oBtn_1_38.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NASCONDIRIGHE)
     endwith
    endif
  endfunc


  add object oBtn_1_39 as StdButton with uid="HLFJBOKFMB",left=548, top=465, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per mostrare il dettaglio dei documenti";
    , HelpContextID = 114600982;
    , tabStop=.f., Caption='\<Righe';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_39.Click()
      with this.Parent.oContained
        this.parent.ocontained.modificazoom( 'MostraRighe')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_39.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((!(.w_FLMTPR='F' AND NOT EMPTY(.w_CAUCOL)) Or !Empty(.w_COMAT)) And (!.w_FLMGPR='F' Or !Empty(.w_COMAG)))
      endwith
    endif
  endfunc

  func oBtn_1_39.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT .w_NASCONDIRIGHE)
     endwith
    endif
  endfunc


  add object oBtn_1_41 as StdButton with uid="AKOERXBELZ",left=689, top=465, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 1457638;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_41.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((!(.w_FLMTPR='F' AND NOT EMPTY(.w_CAUCOL)) Or !Empty(.w_COMAT)) And (!.w_FLMGPR='F' Or !Empty(.w_COMAG)))
      endwith
    endif
  endfunc


  add object oBtn_1_42 as StdButton with uid="VAYMUUHAYT",left=741, top=465, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 8746310;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_43 as StdButton with uid="YZZNYCOXRM",left=741, top=95, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per inizio ricerca documenti da importare";
    , HelpContextID = 90084330;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_43.Click()
      with this.Parent.oContained
        do GSVE_BIM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomDett as cp_szoombox with uid="NEMWPYAYDC",left=257, top=140, width=543,height=297,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="DOC_DETT",cZoomFile="GSVEDKIM",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,cZoomOnZoom="GSZM_BZM",bAdvOptions=.t.,cMenuFile="",;
    cEvent = "CalcRig",;
    nPag=1;
    , HelpContextID = 89008922


  add object oObj_1_47 as cp_runprogram with uid="OOKNFUPYOH",left=-5, top=539, width=272,height=18,;
    caption='GSVE_BI1(MRC)',;
   bGlobalFont=.t.,;
    prg='GSVE_BI1("MRC")',;
    cEvent = "ZOOMMAST row checked",;
    nPag=1;
    , HelpContextID = 187233559


  add object oObj_1_48 as cp_runprogram with uid="RJSXBWSNSU",left=-5, top=520, width=272,height=18,;
    caption='GSVE_BI1(MRU)',;
   bGlobalFont=.t.,;
    prg='GSVE_BI1("MRU")',;
    cEvent = "ZOOMMAST row unchecked",;
    nPag=1;
    , HelpContextID = 188413207


  add object oObj_1_49 as cp_runprogram with uid="EDMDVWMAYW",left=268, top=520, width=289,height=18,;
    caption='GSVE_BI1(DBQ)',;
   bGlobalFont=.t.,;
    prg='GSVE_BI1("DBQ")',;
    cEvent = "ZOOMDETT before query",;
    nPag=1;
    , HelpContextID = 188083223


  add object oObj_1_50 as cp_runprogram with uid="EDSNJGRTZW",left=268, top=539, width=289,height=18,;
    caption='GSVE_BI1(DAQ)',;
   bGlobalFont=.t.,;
    prg='GSVE_BI1("DAQ")',;
    cEvent = "ZOOMDETT after query",;
    nPag=1;
    , HelpContextID = 188079127


  add object oObj_1_67 as cp_runprogram with uid="SNFOGKYDRY",left=268, top=558, width=289,height=18,;
    caption='GSVE_BI1(DRC)',;
   bGlobalFont=.t.,;
    prg='GSVE_BI1("DRC")',;
    cEvent = "ZOOMDETT row checked",;
    nPag=1;
    , HelpContextID = 187231255


  add object oObj_1_68 as cp_runprogram with uid="FHFZMDWAGY",left=559, top=577, width=236,height=18,;
    caption='GSVE_BI1(SCH)',;
   bGlobalFont=.t.,;
    prg='GSVE_BI1("SCH")',;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 187501335


  add object oObj_1_69 as cp_runprogram with uid="UJYRBPRZYT",left=268, top=577, width=289,height=18,;
    caption='GSVE_BI1(DRU)',;
   bGlobalFont=.t.,;
    prg='GSVE_BI1("DRU")',;
    cEvent = "ZOOMDETT row unchecked",;
    nPag=1;
    , HelpContextID = 188410903


  add object oObj_1_71 as cp_runprogram with uid="PWUVEHENLH",left=559, top=520, width=236,height=18,;
    caption='GSVE_BIM',;
   bGlobalFont=.t.,;
    prg="GSVE_BIM",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 139494835


  add object oObj_1_79 as cp_runprogram with uid="AFDENMLHZT",left=559, top=558, width=236,height=18,;
    caption='GSVE_BI2(A)',;
   bGlobalFont=.t.,;
    prg='GSVE_BI2("A")',;
    cEvent = "Edit Aborted",;
    nPag=1;
    , HelpContextID = 139680024


  add object oObj_1_83 as cp_runprogram with uid="YHDJKTNFOV",left=559, top=539, width=236,height=18,;
    caption='GSVE_BI2(U)',;
   bGlobalFont=.t.,;
    prg="GSVE_BI2('U')",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 139685144

  add object oDESART_1_88 as StdField with uid="MOXDQLXEXW",rtseq=78,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione articolo",;
    HelpContextID = 109295818,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=507, Top=439, InputMask=replicate('X',40)

  func oDESART_1_88.mHide()
    with this.Parent.oContained
      return (Empty(.w_TIPART))
    endwith
  endfunc

  add object oSELEZI_1_89 as StdRadio with uid="VNICEXLLTR",rtseq=79,rtrep=.f.,left=249, top=439, width=123,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe del documento";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_89.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 16787418
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 16787418
      this.Buttons(2).Top=15
      this.SetAll("Width",121)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe del documento")
      StdRadio::init()
    endproc

  func oSELEZI_1_89.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_89.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_89.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  func oSELEZI_1_89.mHide()
    with this.Parent.oContained
      return (.w_NASCONDIRIGHE)
    endwith
  endfunc

  add object oSELEZ1_1_90 as StdRadio with uid="RMUJOFEZEL",rtseq=80,rtrep=.f.,left=7, top=439, width=136,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona le righe del documento";
    , cFormVar="w_SELEZ1", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZ1_1_90.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutte"
      this.Buttons(1).HelpContextID = 117430310
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutte"
      this.Buttons(2).HelpContextID = 117430310
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona le righe del documento")
      StdRadio::init()
    endproc

  func oSELEZ1_1_90.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZ1_1_90.GetRadio()
    this.Parent.oContained.w_SELEZ1 = this.RadioValue()
    return .t.
  endfunc

  func oSELEZ1_1_90.SetRadio()
    this.Parent.oContained.w_SELEZ1=trim(this.Parent.oContained.w_SELEZ1)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZ1=='S',1,;
      iif(this.Parent.oContained.w_SELEZ1=='D',2,;
      0))
  endfunc

  func oSELEZ1_1_90.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CALSER))
    endwith
   endif
  endfunc


  add object oObj_1_91 as cp_runprogram with uid="AOQCBHIOOA",left=559, top=596, width=237,height=20,;
    caption='GSVE_BI4(SMC)',;
   bGlobalFont=.t.,;
    prg="GSVE_BI4('SMC')",;
    cEvent = "w_SELEZ1 Changed",;
    nPag=1;
    , HelpContextID = 187214618


  add object oObj_1_94 as cp_runprogram with uid="MJDZYZPFKM",left=-5, top=558, width=272,height=18,;
    caption='GSVE_BI1(AMC)',;
   bGlobalFont=.t.,;
    prg='GSVE_BI1("AMC")',;
    cEvent = "FindDocError",;
    nPag=1;
    , HelpContextID = 187210007


  add object oObj_1_97 as cp_calclbl with uid="OFTMOSPQBO",left=372, top=440, width=134,height=16,;
    caption='TipArt',;
   bGlobalFont=.t.,;
    caption="",Fontbold=.t.,Alignment=1,;
    nPag=1;
    , HelpContextID = 75613130

  add object oSCOPE_1_101 as StdField with uid="IVFNWXJANT",rtseq=96,rtrep=.f.,;
    cFormVar = "w_SCOPE", cQueryName = "SCOPE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica il numero di righe di dettaglio che si vogliono selezionare.",;
    HelpContextID = 79365670,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=342, Top=470

  func oSCOPE_1_101.mHide()
    with this.Parent.oContained
      return (.w_NASCONDIRIGHE OR IsAlt())
    endwith
  endfunc


  add object oBtn_1_103 as StdButton with uid="GYZPDFPRIV",left=461, top=470, width=25,height=22,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per selezionare il numero di righe indicate";
    , HelpContextID = 1629910;
  , bGlobalFont=.t.

    proc oBtn_1_103.Click()
      with this.Parent.oContained
        GSVE_BI1(this.Parent.oContained,"FNR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_103.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_NASCONDIRIGHE OR IsAlt())
     endwith
    endif
  endfunc

  add object oDESCOM_1_109 as StdField with uid="TMFMJLZKKF",rtseq=104,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 229750986,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=217, Top=100, InputMask=replicate('X',40)

  func oDESCOM_1_109.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR (IsAlt() AND g_PRAT<>'S'))
    endwith
  endfunc


  add object oObj_1_115 as cp_runprogram with uid="IEJFPDZRMA",left=-5, top=605, width=386,height=18,;
    caption='GSVE_BI1("QTACHG")',;
   bGlobalFont=.t.,;
    prg='GSVE_BI1("QTACHG")',;
    cEvent = "w_zoomdett mvqtaeva beforerowcolchanged",;
    nPag=1;
    , HelpContextID = 4594306


  add object oObj_1_151 as cp_runprogram with uid="CBTWPCLFLN",left=-9, top=658, width=386,height=18,;
    caption='GSVE_BI1(PRZCHG)',;
   bGlobalFont=.t.,;
    prg="GSVE_BI1('PRZCHG')",;
    cEvent = "w_zoomdett mvimpeva beforerowcolchanged",;
    nPag=1;
    , HelpContextID = 186648512


  add object oBtn_1_152 as StdButton with uid="PPGWIIAJQB",left=183, top=465, width=48,height=45,;
    CpPicture="bmp\Genera.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per calcolare il documento";
    , HelpContextID = 210767066;
    , Caption='\<Calcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_152.Click()
      with this.Parent.oContained
        GSAL3BGD(this.Parent.oContained,"SAL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_152.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT (.w_NASCONDIRIGHE AND .w_VISUALIZZACALCOLA))
     endwith
    endif
  endfunc

  add object oDESCAMAG_1_191 as StdField with uid="ECUSOTIRAS",rtseq=187,rtrep=.f.,;
    cFormVar = "w_DESCAMAG", cQueryName = "DESCAMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 244430979,;
   bGlobalFont=.t.,;
    Height=21, Width=211, Left=523, Top=76, InputMask=replicate('X',35)

  func oDESCAMAG_1_191.mHide()
    with this.Parent.oContained
      return (.w_FLCAUMAG<>'S')
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="BNCNHVKGYN",Visible=.t., Left=3, Top=126,;
    Alignment=0, Width=249, Height=15,;
    Caption="Elenco documenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="DCXZNKWDRF",Visible=.t., Left=265, Top=126,;
    Alignment=0, Width=91, Height=15,;
    Caption="Dettaglio righe"  ;
  , bGlobalFont=.t.

  func oStr_1_33.mHide()
    with this.Parent.oContained
      return (.w_NASCONDIRIGHE)
    endwith
  endfunc

  add object oStr_1_72 as StdString with uid="VUNFMVNLKY",Visible=.t., Left=561, Top=29,;
    Alignment=0, Width=58, Height=17,;
    Caption="(Blu)"    , ForeColor=RGB(0,0,255);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_73 as StdString with uid="JLPTGYJBLO",Visible=.t., Left=561, Top=16,;
    Alignment=0, Width=58, Height=17,;
    Caption="(Rosso)"    , ForeColor=RGB(255,0,0);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_74 as StdString with uid="IGJFRFLBPY",Visible=.t., Left=561, Top=3,;
    Alignment=0, Width=58, Height=17,;
    Caption="(Nero)"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_75 as StdString with uid="SNHXGJYXDR",Visible=.t., Left=619, Top=3,;
    Alignment=0, Width=176, Height=17,;
    Caption="Non selezionata, non trasferita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_76 as StdString with uid="KPEGKTKNKF",Visible=.t., Left=619, Top=16,;
    Alignment=0, Width=176, Height=17,;
    Caption="Selezionata, non trasferita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_77 as StdString with uid="NHQJGSHCLY",Visible=.t., Left=619, Top=29,;
    Alignment=0, Width=176, Height=17,;
    Caption="Gi� trasferita"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_78 as StdString with uid="EPYXVQIDXX",Visible=.t., Left=467, Top=2,;
    Alignment=1, Width=91, Height=15,;
    Caption="Legenda righe:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="TMZNCGZDVI",Visible=.t., Left=561, Top=42,;
    Alignment=0, Width=58, Height=17,;
    Caption="(Magenta)"    , ForeColor=RGB(255,0,255);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_82 as StdString with uid="BSAUUOJAIY",Visible=.t., Left=619, Top=42,;
    Alignment=0, Width=176, Height=17,;
    Caption="Incongruente, non selezionabile"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_84 as StdString with uid="VHLPFOXNIY",Visible=.t., Left=193, Top=30,;
    Alignment=1, Width=65, Height=18,;
    Caption="Collegato:"  ;
  , bGlobalFont=.t.

  func oStr_1_84.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_CAUCOL) OR IsAlt())
    endwith
  endfunc

  add object oStr_1_86 as StdString with uid="BCBPFZAEBY",Visible=.t., Left=175, Top=6,;
    Alignment=1, Width=84, Height=18,;
    Caption="Mag.principale:"  ;
  , bGlobalFont=.t.

  func oStr_1_86.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_100 as StdString with uid="CPAIDSFXYT",Visible=.t., Left=250, Top=472,;
    Alignment=0, Width=90, Height=18,;
    Caption="Seleziona prime"  ;
  , bGlobalFont=.t.

  func oStr_1_100.mHide()
    with this.Parent.oContained
      return (.w_NASCONDIRIGHE OR IsAlt())
    endwith
  endfunc

  add object oStr_1_102 as StdString with uid="ETATVWSFVT",Visible=.t., Left=422, Top=472,;
    Alignment=0, Width=40, Height=18,;
    Caption="Righe"  ;
  , bGlobalFont=.t.

  func oStr_1_102.mHide()
    with this.Parent.oContained
      return (.w_NASCONDIRIGHE OR IsAlt())
    endwith
  endfunc

  add object oStr_1_110 as StdString with uid="LLQTDGQJFH",Visible=.t., Left=7, Top=102,;
    Alignment=1, Width=50, Height=18,;
    Caption="Pratica:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_110.mHide()
    with this.Parent.oContained
      return (!IsAlt() OR (IsAlt() AND g_PRAT<>'S'))
    endwith
  endfunc

  add object oStr_1_113 as StdString with uid="ZVCDAEPWXX",Visible=.t., Left=560, Top=56,;
    Alignment=0, Width=47, Height=17,;
    Caption="(Verde)"    , ForeColor=RGB(48,167,107);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_113.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc

  add object oStr_1_114 as StdString with uid="SNNQHBWIMT",Visible=.t., Left=619, Top=56,;
    Alignment=0, Width=176, Height=17,;
    Caption="Contributo accessorio"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_114.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc

  add object oStr_1_149 as StdString with uid="KEENRFDTGF",Visible=.t., Left=139, Top=53,;
    Alignment=1, Width=119, Height=18,;
    Caption="Forza/mantieni dati:"  ;
  , bGlobalFont=.t.

  func oStr_1_149.mHide()
    with this.Parent.oContained
      return (IsAlt() OR  .w_GIAIMP<>'S' OR (.w_GESTPARENT.NUMROW()<>0  AND Upper(.w_GESTPARENT.CLASS)<>'TGSAR_MPG') OR  .w_GESTPARENT.cfunction<>'Load')
    endwith
  endfunc

  add object oStr_1_190 as StdString with uid="LWGAHYQUWM",Visible=.t., Left=333, Top=76,;
    Alignment=1, Width=123, Height=22,;
    Caption="Causale magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_190.mHide()
    with this.Parent.oContained
      return (.w_FLCAUMAG<>'S')
    endwith
  endfunc
enddefine
define class tgsve_kimPag2 as StdContainer
  Width  = 800
  height = 510
  stdWidth  = 800
  stdheight = 510
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_2_2 as StdField with uid="PHKYWKFGDT",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di inizio selezione",;
    HelpContextID = 29076682,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=148, Top=11

  add object oDATFIN_2_3 as StdField with uid="UETOVWEXGF",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento di fine selezione",;
    HelpContextID = 219065546,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=148, Top=39

  add object oEVAINI_2_4 as StdField with uid="AYUGUTBVKR",rtseq=58,rtrep=.f.,;
    cFormVar = "w_EVAINI", cQueryName = "EVAINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data prevista evasione di inizio selezione (vuoto =no selezione)",;
    HelpContextID = 29149114,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=380, Top=11

  func oEVAINI_2_4.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oEVAFIN_2_5 as StdField with uid="NZPEFWQYAO",rtseq=59,rtrep=.f.,;
    cFormVar = "w_EVAFIN", cQueryName = "EVAFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data prevista evasione di fine selezione (vuoto =no selezione)",;
    HelpContextID = 219137978,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=380, Top=39

  func oEVAFIN_2_5.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFLTRASF_2_6 as StdCheck with uid="MNTJDVAAXK",rtseq=60,rtrep=.f.,left=480, top=9, caption="Solo gi� trasferite",;
    ToolTipText = "Se attivato filtra solo documenti gi� trasferiti",;
    HelpContextID = 142778794,;
    cFormVar="w_FLTRASF", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLTRASF_2_6.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLTRASF_2_6.GetRadio()
    this.Parent.oContained.w_FLTRASF = this.RadioValue()
    return .t.
  endfunc

  func oFLTRASF_2_6.SetRadio()
    this.Parent.oContained.w_FLTRASF=trim(this.Parent.oContained.w_FLTRASF)
    this.value = ;
      iif(this.Parent.oContained.w_FLTRASF=='S',1,;
      0)
  endfunc

  func oFLTRASF_2_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFILART_2_7 as StdField with uid="QFAEHIGQLO",rtseq=61,rtrep=.f.,;
    cFormVar = "w_FILART", cQueryName = "FILART",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale codice di ricerca di selezione (vuoto = no selezione)",;
    HelpContextID = 109323434,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=148, Top=79, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_FILART"

  func oFILART_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILART_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILART_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oFILART_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Articoli/servizi",'',this.parent.oContained
  endproc
  proc oFILART_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_FILART
     i_obj.ecpSave()
  endproc

  add object oFILARM_2_8 as StdField with uid="WQUBQWVMAU",rtseq=62,rtrep=.f.,;
    cFormVar = "w_FILARM", cQueryName = "FILARM",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale codice articolo di selezione (vuoto = no selezione)",;
    HelpContextID = 226763946,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=148, Top=108, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_FILARM"

  func oFILARM_2_8.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oFILARM_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILARM_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILARM_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oFILARM_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'',this.parent.oContained
  endproc
  proc oFILARM_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_FILARM
     i_obj.ecpSave()
  endproc

  add object oFILMAG_2_9 as StdField with uid="YOJGLLVLTP",rtseq=63,rtrep=.f.,;
    cFormVar = "w_FILMAG", cQueryName = "FILMAG",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale codice magazzino di selezione (vuoto = no selezione)",;
    HelpContextID = 76031146,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=148, Top=136, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_FILMAG"

  func oFILMAG_2_9.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oFILMAG_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILMAG_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILMAG_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oFILMAG_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oFILMAG_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_FILMAG
     i_obj.ecpSave()
  endproc

  add object oFILCAU_2_12 as StdField with uid="WJROWXMCTI",rtseq=64,rtrep=.f.,;
    cFormVar = "w_FILCAU", cQueryName = "FILCAU",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale documento collegato di selezione (vuoto = no selezione)",;
    HelpContextID = 110240938,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=148, Top=166, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_FILCAU"

  func oFILCAU_2_12.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oFILCAU_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILCAU_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILCAU_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oFILCAU_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Documenti collegati",'GSVE1MDC.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oDESFAR_2_17 as StdField with uid="FIMCEDYFOD",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DESFAR", cQueryName = "DESFAR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 160348362,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=306, Top=80, InputMask=replicate('X',40)

  add object oDESARM_2_19 as StdField with uid="PIOAUNZZKO",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DESARM", cQueryName = "DESARM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 226736330,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=306, Top=108, InputMask=replicate('X',40)

  func oDESARM_2_19.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFILCLI_2_25 as StdField with uid="VXCHAHINBV",rtseq=72,rtrep=.f.,;
    cFormVar = "w_FILCLI", cQueryName = "FILCLI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 31597738,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=148, Top=195, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_FLVEAC1", oKey_2_1="ANCODICE", oKey_2_2="this.w_FILCLI"

  func oFILCLI_2_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((EMPTY(.w_MVCODCON) OR .w_FLINTE='S' OR .w_FLVEAC1$'CF' ) and .w_CFCOLL<>'N')
    endwith
   endif
  endfunc

  func oFILCLI_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILCLI_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILCLI_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_FLVEAC1)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_FLVEAC1)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oFILCLI_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Anagrafica clienti/fornitori",'GSCG1MPN.CONTI_VZM',this.parent.oContained
  endproc
  proc oFILCLI_2_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_FLVEAC1
     i_obj.w_ANCODICE=this.parent.oContained.w_FILCLI
     i_obj.ecpSave()
  endproc

  add object oDESCLI_2_27 as StdField with uid="VCQGTDKOCN",rtseq=73,rtrep=.f.,;
    cFormVar = "w_DESCLI", cQueryName = "DESCLI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 31570122,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=306, Top=197, InputMask=replicate('X',40)

  add object oCODDES_2_28 as StdField with uid="RNXWONQJXX",rtseq=74,rtrep=.f.,;
    cFormVar = "w_CODDES", cQueryName = "CODDES",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale destinazione particolare di selezione (vuoto = no selezione)",;
    HelpContextID = 139566810,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=148, Top=224, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_FLVEAC1", oKey_2_1="DDCODICE", oKey_2_2="this.w_CLIFOR", oKey_3_1="DDCODDES", oKey_3_2="this.w_CODDES"

  func oCODDES_2_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CLIFOR))
    endwith
   endif
  endfunc

  proc oCODDES_2_28.mAfter
    with this.Parent.oContained
      .w_CODDES=chkdesdiv(.w_CODDES, .w_FLVEAC1, .w_CLIFOR)
    endwith
  endproc

  func oCODDES_2_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODDES_2_28.ecpDrop(oSource)
    this.Parent.oContained.link_2_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODDES_2_28.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_FLVEAC1)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CLIFOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_FLVEAC1)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_CLIFOR)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oCODDES_2_28'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Destinazioni diverse",'GSVE0KFD.DES_DIVE_VZM',this.parent.oContained
  endproc

  add object oDESCAU_2_29 as StdField with uid="HPXFEEAYSW",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 110213322,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=306, Top=166, InputMask=replicate('X',35)

  func oDESCAU_2_29.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFILCOMM_2_30 as StdField with uid="OQHJPVWHJG",rtseq=81,rtrep=.f.,;
    cFormVar = "w_FILCOMM", cQueryName = "FILCOMM",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale codice commessa di selezione (vuoto = no selezione)",;
    HelpContextID = 38656854,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=148, Top=253, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_FILCOMM"

  func oFILCOMM_2_30.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oFILCOMM_2_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILCOMM_2_30.ecpDrop(oSource)
    this.Parent.oContained.link_2_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILCOMM_2_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oFILCOMM_2_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oFILCOMM_2_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_FILCOMM
     i_obj.ecpSave()
  endproc

  add object oDESCOM_2_31 as StdField with uid="EJVPYXQKWD",rtseq=82,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 229750986,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=307, Top=253, InputMask=replicate('X',40)

  func oDESCOM_2_31.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFILCEN_2_33 as StdField with uid="NRKPRIDVAK",rtseq=83,rtrep=.f.,;
    cFormVar = "w_FILCEN", cQueryName = "FILCEN",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 223487146,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=148, Top=283, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_FILCEN"

  func oFILCEN_2_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILCEN_2_33.ecpDrop(oSource)
    this.Parent.oContained.link_2_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILCEN_2_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oFILCEN_2_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centro di costo/ricavo",'',this.parent.oContained
  endproc
  proc oFILCEN_2_33.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_FILCEN
     i_obj.ecpSave()
  endproc

  add object oDESCEN_2_34 as StdField with uid="VVHHDZXKXF",rtseq=84,rtrep=.f.,;
    cFormVar = "w_DESCEN", cQueryName = "DESCEN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 223459530,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=307, Top=283, InputMask=replicate('X',40)

  add object oFILATTI_2_37 as StdField with uid="VMCAMNUPZF",rtseq=85,rtrep=.f.,;
    cFormVar = "w_FILATTI", cQueryName = "FILATTI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 161209174,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=148, Top=313, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="ATTIVITA", cZoomOnZoom="GSPC_BZZ", oKey_1_1="ATCODATT", oKey_1_2="this.w_FILATTI"

  func oFILATTI_2_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_37('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILATTI_2_37.ecpDrop(oSource)
    this.Parent.oContained.link_2_37('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILATTI_2_37.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ATTIVITA','*','ATCODATT',cp_AbsName(this.parent,'oFILATTI_2_37'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPC_BZZ',"Attivit�",'',this.parent.oContained
  endproc
  proc oFILATTI_2_37.mZoomOnZoom
    local i_obj
    i_obj=GSPC_BZZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ATCODATT=this.parent.oContained.w_FILATTI
     i_obj.ecpSave()
  endproc

  add object oDESATTI_2_38 as StdField with uid="WAJVCRUQCC",rtseq=86,rtrep=.f.,;
    cFormVar = "w_DESATTI", cQueryName = "DESATTI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 161236790,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=307, Top=313, InputMask=replicate('X',40)

  add object oFILVOC_2_40 as StdField with uid="MQPCFABJUN",rtseq=87,rtrep=.f.,;
    cFormVar = "w_FILVOC", cQueryName = "FILVOC",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 127870122,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=148, Top=343, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="VOC_COST", cZoomOnZoom="GSCA_AVC", oKey_1_1="VCCODICE", oKey_1_2="this.w_FILVOC"

  func oFILVOC_2_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILVOC_2_40.ecpDrop(oSource)
    this.Parent.oContained.link_2_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILVOC_2_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_COST','*','VCCODICE',cp_AbsName(this.parent,'oFILVOC_2_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_AVC',"Voci di costo/rica",'',this.parent.oContained
  endproc
  proc oFILVOC_2_40.mZoomOnZoom
    local i_obj
    i_obj=GSCA_AVC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VCCODICE=this.parent.oContained.w_FILVOC
     i_obj.ecpSave()
  endproc

  add object oDESVOC_2_41 as StdField with uid="JRXLDIITVK",rtseq=88,rtrep=.f.,;
    cFormVar = "w_DESVOC", cQueryName = "DESVOC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 127842506,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=307, Top=343, InputMask=replicate('X',40)

  add object oDESMAG_2_44 as StdField with uid="QXZZUJVXDL",rtseq=89,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 76003530,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=306, Top=136, InputMask=replicate('X',30)

  func oDESMAG_2_44.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oDESDOC_2_48 as StdField with uid="YRCZPQSYZW",rtseq=98,rtrep=.f.,;
    cFormVar = "w_DESDOC", cQueryName = "DESDOC",;
    bObbl = .f. , nPag = 2, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Filtra su riferimento descrittivo cli/for",;
    HelpContextID = 129022154,;
   bGlobalFont=.t.,;
    Height=21, Width=448, Left=148, Top=373, InputMask=replicate('X',250)


  add object oBtn_2_49 as StdButton with uid="ZLCFJDLLUM",left=728, top=11, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per inizio ricerca documenti da importare";
    , HelpContextID = 90084330;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_49.Click()
      with this.Parent.oContained
        do GSVE_BIM with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFILRES_2_52 as StdField with uid="JSIWFWYCGU",rtseq=106,rtrep=.f.,;
    cFormVar = "w_FILRES", cQueryName = "FILRES",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale responsabile di selezione (vuoto = no selezione)",;
    HelpContextID = 138618026,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=148, Top=403, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DIPENDEN", oKey_1_1="DPTIPRIS", oKey_1_2="this.w_TipoRisorsa", oKey_2_1="DPCODICE", oKey_2_2="this.w_FILRES"

  func oFILRES_2_52.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  func oFILRES_2_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_52('Part',this)
    endwith
    return bRes
  endfunc

  proc oFILRES_2_52.ecpDrop(oSource)
    this.Parent.oContained.link_2_52('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFILRES_2_52.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DIPENDEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStrODBC(this.Parent.oContained.w_TipoRisorsa)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DPTIPRIS="+cp_ToStr(this.Parent.oContained.w_TipoRisorsa)
    endif
    do cp_zoom with 'DIPENDEN','*','DPTIPRIS,DPCODICE',cp_AbsName(this.parent,'oFILRES_2_52'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Responsabili",'GSARPADP.DIPENDEN_VZM',this.parent.oContained
  endproc

  add object oDENOM_PART_2_53 as StdField with uid="SKXCKTCAOH",rtseq=107,rtrep=.f.,;
    cFormVar = "w_DENOM_PART", cQueryName = "DENOM_PART",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 197504873,;
   bGlobalFont=.t.,;
    Height=21, Width=377, Left=219, Top=403, InputMask=replicate('X',100)

  func oDENOM_PART_2_53.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oPREINI_2_56 as StdField with uid="VAJTUNAFFS",rtseq=110,rtrep=.f.,;
    cFormVar = "w_PREINI", cQueryName = "PREINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data della prestazione di inizio selezione",;
    HelpContextID = 29133578,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=148, Top=433

  func oPREINI_2_56.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  func oPREINI_2_56.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PREINI<=.w_PREFIN OR EMPTY(.w_PREFIN))
    endwith
    return bRes
  endfunc

  add object oPREFIN_2_57 as StdField with uid="VRHGJGBHQK",rtseq=111,rtrep=.f.,;
    cFormVar = "w_PREFIN", cQueryName = "PREFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale",;
    ToolTipText = "Data della prestazione di fine selezione",;
    HelpContextID = 219122442,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=306, Top=433

  func oPREFIN_2_57.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  func oPREFIN_2_57.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PREINI<=.w_PREFIN OR EMPTY(.w_PREFIN))
    endwith
    return bRes
  endfunc

  add object oDESDES_2_61 as StdField with uid="INPWQBPBRQ",rtseq=182,rtrep=.f.,;
    cFormVar = "w_DESDES", cQueryName = "DESDES",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 139507914,;
   bGlobalFont=.t.,;
    Height=21, Width=383, Left=212, Top=224, InputMask=replicate('X',40)


  add object oTIPDORI_2_63 as StdCombo with uid="MWZLORRVIC",value=7,rtseq=183,rtrep=.f.,left=148,top=459,width=101,height=22;
    , HelpContextID = 122625078;
    , cFormVar="w_TIPDORI",RowSource=""+"Bozza interna,"+"Nota spese,"+"Proforma d'acconto,"+"Proforma,"+"Fattura d'acconto,"+"Fattura,"+"    ", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPDORI_2_63.RadioValue()
    return(iif(this.value =1,'B',;
    iif(this.value =2,'N',;
    iif(this.value =3,'P',;
    iif(this.value =4,'S',;
    iif(this.value =5,'A',;
    iif(this.value =6,'F',;
    iif(this.value =7,' ',;
    space(1)))))))))
  endfunc
  func oTIPDORI_2_63.GetRadio()
    this.Parent.oContained.w_TIPDORI = this.RadioValue()
    return .t.
  endfunc

  func oTIPDORI_2_63.SetRadio()
    this.Parent.oContained.w_TIPDORI=trim(this.Parent.oContained.w_TIPDORI)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDORI=='B',1,;
      iif(this.Parent.oContained.w_TIPDORI=='N',2,;
      iif(this.Parent.oContained.w_TIPDORI=='P',3,;
      iif(this.Parent.oContained.w_TIPDORI=='S',4,;
      iif(this.Parent.oContained.w_TIPDORI=='A',5,;
      iif(this.Parent.oContained.w_TIPDORI=='F',6,;
      iif(this.Parent.oContained.w_TIPDORI=='',7,;
      0)))))))
  endfunc

  func oTIPDORI_2_63.mHide()
    with this.Parent.oContained
      return (! Isalt())
    endwith
  endfunc

  add object oNUMDOCORI_2_64 as StdField with uid="RUPJFYHOFR",rtseq=184,rtrep=.f.,;
    cFormVar = "w_NUMDOCORI", cQueryName = "NUMDOCORI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 139394232,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=306, Top=461, cSayPict='"999999999999999"', cGetPict='"999999999999999"'

  func oNUMDOCORI_2_64.mHide()
    with this.Parent.oContained
      return (! Isalt())
    endwith
  endfunc

  add object oALFDOCORI_2_67 as StdField with uid="DHKOIPFUMF",rtseq=185,rtrep=.f.,;
    cFormVar = "w_ALFDOCORI", cQueryName = "ALFDOCORI",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 139363048,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=388, Top=461, cSayPict='"XXXXXXXXXX"', cGetPict='"XXXXXXXXXX"', InputMask=replicate('X',10)

  func oALFDOCORI_2_67.mHide()
    with this.Parent.oContained
      return (! Isalt())
    endwith
  endfunc

  add object oDATDOCORI_2_68 as StdField with uid="NJHWREKNOI",rtseq=186,rtrep=.f.,;
    cFormVar = "w_DATDOCORI", cQueryName = "DATDOCORI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 139417624,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=520, Top=461

  func oDATDOCORI_2_68.mHide()
    with this.Parent.oContained
      return (! Isalt())
    endwith
  endfunc

  add object oStr_2_10 as StdString with uid="FYWPQOXBEE",Visible=.t., Left=7, Top=11,;
    Alignment=1, Width=139, Height=15,;
    Caption="Da data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="JGLWZGKDER",Visible=.t., Left=19, Top=39,;
    Alignment=1, Width=127, Height=15,;
    Caption="A data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="XSSBOAYRYS",Visible=.t., Left=237, Top=11,;
    Alignment=1, Width=141, Height=15,;
    Caption="Da prevista evasione:"  ;
  , bGlobalFont=.t.

  func oStr_2_14.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_15 as StdString with uid="FNYXEXORXF",Visible=.t., Left=243, Top=39,;
    Alignment=1, Width=135, Height=15,;
    Caption="A prevista evasione:"  ;
  , bGlobalFont=.t.

  func oStr_2_15.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_16 as StdString with uid="MDONVSXVZG",Visible=.t., Left=19, Top=165,;
    Alignment=1, Width=127, Height=18,;
    Caption="Doc. collegato:"  ;
  , bGlobalFont=.t.

  func oStr_2_16.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_18 as StdString with uid="UOAYZZHOIG",Visible=.t., Left=19, Top=80,;
    Alignment=1, Width=127, Height=15,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  func oStr_2_18.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_24 as StdString with uid="WCEYASNNGX",Visible=.t., Left=19, Top=108,;
    Alignment=1, Width=127, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  func oStr_2_24.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_26 as StdString with uid="LSRMRHFVLV",Visible=.t., Left=19, Top=195,;
    Alignment=1, Width=127, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="HREJQXTTEL",Visible=.t., Left=19, Top=253,;
    Alignment=1, Width=127, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.

  func oStr_2_32.mHide()
    with this.Parent.oContained
      return (NOT((g_COMM='S'  OR (g_PERCCR='S' AND g_PERCAN='S'))) OR IsAlt())
    endwith
  endfunc

  add object oStr_2_35 as StdString with uid="YVOERRFVSP",Visible=.t., Left=19, Top=283,;
    Alignment=1, Width=127, Height=18,;
    Caption="Centro di costo:"  ;
  , bGlobalFont=.t.

  func oStr_2_35.mHide()
    with this.Parent.oContained
      return (.w_FLVEAC='V')
    endwith
  endfunc

  add object oStr_2_36 as StdString with uid="AMFAENWPLD",Visible=.t., Left=19, Top=283,;
    Alignment=1, Width=127, Height=18,;
    Caption="Centro di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_2_36.mHide()
    with this.Parent.oContained
      return (.w_FLVEAC='A')
    endwith
  endfunc

  add object oStr_2_39 as StdString with uid="GMVRMRSEOV",Visible=.t., Left=19, Top=313,;
    Alignment=1, Width=127, Height=18,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_42 as StdString with uid="XPZKRLKMHX",Visible=.t., Left=19, Top=343,;
    Alignment=1, Width=127, Height=18,;
    Caption="Voce di costo:"  ;
  , bGlobalFont=.t.

  func oStr_2_42.mHide()
    with this.Parent.oContained
      return (.w_FLVEAC='V')
    endwith
  endfunc

  add object oStr_2_43 as StdString with uid="TUKEOILHSO",Visible=.t., Left=19, Top=343,;
    Alignment=1, Width=127, Height=18,;
    Caption="Voce di ricavo:"  ;
  , bGlobalFont=.t.

  func oStr_2_43.mHide()
    with this.Parent.oContained
      return (.w_FLVEAC='A')
    endwith
  endfunc

  add object oStr_2_45 as StdString with uid="GSUQUCGGND",Visible=.t., Left=19, Top=136,;
    Alignment=1, Width=127, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_2_45.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_2_46 as StdString with uid="CVUXPXWJUB",Visible=.t., Left=19, Top=375,;
    Alignment=1, Width=127, Height=18,;
    Caption="Rif. descrittivo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_50 as StdString with uid="LVCDRXRLEV",Visible=.t., Left=19, Top=80,;
    Alignment=1, Width=127, Height=15,;
    Caption="Prestazione:"  ;
  , bGlobalFont=.t.

  func oStr_2_50.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_2_51 as StdString with uid="WKFYIDWAKE",Visible=.t., Left=19, Top=404,;
    Alignment=1, Width=127, Height=18,;
    Caption="Responsabile:"  ;
  , bGlobalFont=.t.

  func oStr_2_51.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_2_58 as StdString with uid="KLEZMLKSOB",Visible=.t., Left=62, Top=435,;
    Alignment=1, Width=84, Height=18,;
    Caption="Prestazioni dal:"  ;
  , bGlobalFont=.t.

  func oStr_2_58.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_2_59 as StdString with uid="JAHWAWKLCQ",Visible=.t., Left=265, Top=435,;
    Alignment=1, Width=37, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  func oStr_2_59.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_2_62 as StdString with uid="MDCFNFKCXO",Visible=.t., Left=54, Top=224,;
    Alignment=1, Width=92, Height=15,;
    Caption="Destinazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_65 as StdString with uid="AETTYIOKQF",Visible=.t., Left=52, Top=462,;
    Alignment=0, Width=94, Height=18,;
    Caption="Tipo doc. origine:"  ;
  , bGlobalFont=.t.

  func oStr_2_65.mHide()
    with this.Parent.oContained
      return (! Isalt())
    endwith
  endfunc

  add object oStr_2_66 as StdString with uid="VVAISYQEZR",Visible=.t., Left=254, Top=462,;
    Alignment=1, Width=48, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  func oStr_2_66.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_2_69 as StdString with uid="IYPOGWQGMC",Visible=.t., Left=474, Top=462,;
    Alignment=1, Width=37, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  func oStr_2_69.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc

  add object oStr_2_70 as StdString with uid="AUAZTOHMCW",Visible=.t., Left=382, Top=464,;
    Alignment=0, Width=7, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_2_70.mHide()
    with this.Parent.oContained
      return (! Isalt())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_kim','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
