* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kef                                                        *
*              Generazione disco INTRA                                         *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_115]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-14                                                      *
* Last revis.: 2018-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kef",oParentObject))

* --- Class definition
define class tgsar_kef as StdForm
  Top    = 35
  Left   = 15

  * --- Standard Properties
  Width  = 822
  Height = 427
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-03-02"
  HelpContextID=65632105
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=54

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  ELEIMAST_IDX = 0
  ELEIDETT_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  AZDATINT_IDX = 0
  cPrg = "gsar_kef"
  cComment = "Generazione disco INTRA"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ReadAzie = space(5)
  w_ReadAzieDatInt = space(5)
  w_AZUGINAS = ctod('  /  /  ')
  w_AZINPECE = 0
  w_AZINPEAC = 0
  w_AZINPECS = 0
  w_AZINPEAS = 0
  w_AZAICFPI = space(12)
  w_AZPIVAZI = space(12)
  w_TIPOGENE = space(1)
  w_SOLOSERV = space(1)
  w_TipoGeneServizi = space(1)
  w_SOGGDELE = space(1)
  o_SOGGDELE = space(1)
  w_CESSATTI = space(1)
  w_FRONTCAR = space(1)
  w_ANNORIFE = space(4)
  o_ANNORIFE = space(4)
  w_PIvaPres = space(12)
  w_CESS_BENI = space(1)
  w_CESS_SERV = space(1)
  w_ACQ_BENI = space(1)
  w_ACQ_SERV = space(1)
  w_TIPOCESS = space(1)
  o_TIPOCESS = space(1)
  w_VALUTA = space(3)
  w_PERICESS = 0
  o_PERICESS = 0
  w_DECIM = 0
  w_TIPOACQU = space(1)
  o_TIPOACQU = space(1)
  w_PERIACQU = 0
  o_PERIACQU = 0
  w_NOMEFILE = space(40)
  w_NProgEle = 0
  w_FLLIRE = space(1)
  w_AZNPRGEL = 0
  w_AZLOCALI = space(30)
  w_AZAIRAGS = space(40)
  w_AZAICITT = space(25)
  w_DTOBBL = space(1)
  w_PERICSER = 0
  o_PERICSER = 0
  w_TIPOCSERV = space(1)
  o_TIPOCSERV = space(1)
  w_PRIMPRES = space(1)
  o_PRIMPRES = space(1)
  w_PRIMPRES_CS = space(1)
  o_PRIMPRES_CS = space(1)
  w_PRIMPRES_AB = space(1)
  o_PRIMPRES_AB = space(1)
  w_TIPOASERV = space(1)
  o_TIPOASERV = space(1)
  w_PERIASER = 0
  o_PERIASER = 0
  w_PRIMPRES_AS = space(1)
  o_PRIMPRES_AS = space(1)
  w_ROTTURA_CES = space(1)
  w_ACMESINI = 0
  w_ACMESFIN = 0
  w_ACMESINI_SERV = 0
  w_ACMESFIN_SERV = 0
  w_ROTTURA_ACQ = space(1)
  w_AZPERAZI = space(1)
  w_CEMESINI = 0
  w_CEMESINI_SERV = 0
  w_CEMESFIN_SERV = 0
  w_CEMESFIN = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kefPag1","gsar_kef",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTipoGeneServizi_1_13
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='ELEIMAST'
    this.cWorkTables[3]='ELEIDETT'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='AZDATINT'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ReadAzie=space(5)
      .w_ReadAzieDatInt=space(5)
      .w_AZUGINAS=ctod("  /  /  ")
      .w_AZINPECE=0
      .w_AZINPEAC=0
      .w_AZINPECS=0
      .w_AZINPEAS=0
      .w_AZAICFPI=space(12)
      .w_AZPIVAZI=space(12)
      .w_TIPOGENE=space(1)
      .w_SOLOSERV=space(1)
      .w_TipoGeneServizi=space(1)
      .w_SOGGDELE=space(1)
      .w_CESSATTI=space(1)
      .w_FRONTCAR=space(1)
      .w_ANNORIFE=space(4)
      .w_PIvaPres=space(12)
      .w_CESS_BENI=space(1)
      .w_CESS_SERV=space(1)
      .w_ACQ_BENI=space(1)
      .w_ACQ_SERV=space(1)
      .w_TIPOCESS=space(1)
      .w_VALUTA=space(3)
      .w_PERICESS=0
      .w_DECIM=0
      .w_TIPOACQU=space(1)
      .w_PERIACQU=0
      .w_NOMEFILE=space(40)
      .w_NProgEle=0
      .w_FLLIRE=space(1)
      .w_AZNPRGEL=0
      .w_AZLOCALI=space(30)
      .w_AZAIRAGS=space(40)
      .w_AZAICITT=space(25)
      .w_DTOBBL=space(1)
      .w_PERICSER=0
      .w_TIPOCSERV=space(1)
      .w_PRIMPRES=space(1)
      .w_PRIMPRES_CS=space(1)
      .w_PRIMPRES_AB=space(1)
      .w_TIPOASERV=space(1)
      .w_PERIASER=0
      .w_PRIMPRES_AS=space(1)
      .w_ROTTURA_CES=space(1)
      .w_ACMESINI=0
      .w_ACMESFIN=0
      .w_ACMESINI_SERV=0
      .w_ACMESFIN_SERV=0
      .w_ROTTURA_ACQ=space(1)
      .w_AZPERAZI=space(1)
      .w_CEMESINI=0
      .w_CEMESINI_SERV=0
      .w_CEMESFIN_SERV=0
      .w_CEMESFIN=0
        .w_ReadAzie = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_ReadAzie))
          .link_1_1('Full')
        endif
        .w_ReadAzieDatInt = i_codazi
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ReadAzieDatInt))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,10,.f.)
        .w_SOLOSERV = 'N'
        .w_TipoGeneServizi = 'C'
        .w_SOGGDELE = 'S'
        .w_CESSATTI = '0'
        .w_FRONTCAR = 'S'
        .w_ANNORIFE = str(year(i_datsys),4,0)
        .w_PIvaPres = IIF(.w_SOGGDELE<>'S', .w_AZPIVAZI, .w_AZAICFPI)
        .w_CESS_BENI = 'S'
        .w_CESS_SERV = 'S'
        .w_ACQ_BENI = 'S'
        .w_ACQ_SERV = 'S'
          .DoRTCalc(22,22,.f.)
        .w_VALUTA = IIF(g_PERVAL<>g_CODEUR AND .w_FLLIRE='S', g_CODLIR, g_CODEUR)
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_VALUTA))
          .link_1_24('Full')
        endif
        .w_PERICESS = .w_AZINPECE+1
          .DoRTCalc(25,26,.f.)
        .w_PERIACQU = .w_AZINPEAC+1
        .w_NOMEFILE = left(sys(5)+sys(2003)+"\SCAMBI.CEE"+space(40),40)
        .w_NProgEle = IIF(.w_AZNPRGEL=0,1,.w_AZNPRGEL)
          .DoRTCalc(30,35,.f.)
        .w_PERICSER = .w_AZINPECS+1
          .DoRTCalc(37,37,.f.)
        .w_PRIMPRES = '0'
        .w_PRIMPRES_CS = '0'
        .w_PRIMPRES_AB = '0'
          .DoRTCalc(41,41,.f.)
        .w_PERIASER = .w_AZINPEAS+1
        .w_PRIMPRES_AS = '0'
        .w_ROTTURA_CES = iif(.w_TipoCess<>.w_TipoCserv Or .w_PeriCess<>.w_PeriCser Or (.w_TipoCess="T" And .w_TipoCserv="T" And .w_Primpres<>.w_Primpres_CS),"S","N")
        .w_ACMESINI = iif( .w_TIPOACQU="M", .w_PERIACQU, (.w_PERIACQU*3)-2 )
        .w_ACMESFIN = iif( .w_TIPOACQU="M", .w_PERIACQU,IIF(.w_PRIMPRES_AB='0',(.w_PERIACQU*3),IIF(.w_PRIMPRES_AB='8',.w_ACMESINI,(.w_PERIACQU*3)-1)))               
        .w_ACMESINI_SERV = iif( .w_TIPOASERV="M", .w_PERIASER, (.w_PERIASER*3)-2 )
        .w_ACMESFIN_SERV = iif( .w_TIPOASERV="M", .w_PERIASER,IIF(.w_PRIMPRES_AS='0',(.w_PERIASER*3),IIF(.w_PRIMPRES_AS='8',.w_ACMESINI_SERV,(.w_PERIASER*3)-1)))               
        .w_ROTTURA_ACQ = iif(.w_TipoAcqu<>.w_TipoAserv Or .w_PeriAcqu<>.w_PeriAser Or (.w_TipoAcqu="T" And .w_TipoAserv="T" And .w_Primpres_AB<>.w_Primpres_AS),"S","N")
          .DoRTCalc(50,50,.f.)
        .w_CEMESINI = iif( .w_TIPOCESS="M", .w_PERICESS, (.w_PERICESS*3)-2 )
        .w_CEMESINI_SERV = iif( .w_TIPOCSERV="M", .w_PERICSER, (.w_PERICSER*3)-2 )
        .w_CEMESFIN_SERV = iif(.w_TIPOCSERV="M", .w_PERICSER,IIF(.w_PRIMPRES_CS='0',(.w_PERICSER*3),IIF(.w_PRIMPRES_CS='8',.w_CEMESINI_SERV,(.w_PERICSER*3)-1)))
        .w_CEMESFIN = iif( .w_TIPOCESS="M", .w_PERICESS,IIF(.w_PRIMPRES='0',(.w_PERICESS*3),IIF(.w_PRIMPRES='8',.w_CEMESINI,(.w_PERICESS*3)-1)))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_52.enabled = this.oPgFrm.Page1.oPag.oBtn_1_52.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- gsar_kef
    This.NotifyEvent('Valuta')
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
          .link_1_2('Full')
        .DoRTCalc(3,11,.t.)
        if .o_AnnoRife<>.w_AnnoRife
            .w_TipoGeneServizi = 'C'
        endif
        .DoRTCalc(13,16,.t.)
        if .o_SOGGDELE<>.w_SOGGDELE
            .w_PIvaPres = IIF(.w_SOGGDELE<>'S', .w_AZPIVAZI, .w_AZAICFPI)
        endif
        .DoRTCalc(18,22,.t.)
            .w_VALUTA = IIF(g_PERVAL<>g_CODEUR AND .w_FLLIRE='S', g_CODLIR, g_CODEUR)
          .link_1_24('Full')
        .DoRTCalc(24,43,.t.)
        if .o_TipoCess<>.w_TipoCess.or. .o_TipoCserv<>.w_TipoCserv.or. .o_PeriCess<>.w_PeriCess.or. .o_PeriCser<>.w_PeriCser.or. .o_Primpres<>.w_Primpres.or. .o_Primpres_CS<>.w_Primpres_CS
            .w_ROTTURA_CES = iif(.w_TipoCess<>.w_TipoCserv Or .w_PeriCess<>.w_PeriCser Or (.w_TipoCess="T" And .w_TipoCserv="T" And .w_Primpres<>.w_Primpres_CS),"S","N")
        endif
            .w_ACMESINI = iif( .w_TIPOACQU="M", .w_PERIACQU, (.w_PERIACQU*3)-2 )
            .w_ACMESFIN = iif( .w_TIPOACQU="M", .w_PERIACQU,IIF(.w_PRIMPRES_AB='0',(.w_PERIACQU*3),IIF(.w_PRIMPRES_AB='8',.w_ACMESINI,(.w_PERIACQU*3)-1)))               
            .w_ACMESINI_SERV = iif( .w_TIPOASERV="M", .w_PERIASER, (.w_PERIASER*3)-2 )
            .w_ACMESFIN_SERV = iif( .w_TIPOASERV="M", .w_PERIASER,IIF(.w_PRIMPRES_AS='0',(.w_PERIASER*3),IIF(.w_PRIMPRES_AS='8',.w_ACMESINI_SERV,(.w_PERIASER*3)-1)))               
        if .o_TipoAcqu<>.w_TipoAcqu.or. .o_TipoAserv<>.w_TipoAserv.or. .o_PeriAcqu<>.w_PeriAcqu.or. .o_PeriAser<>.w_PeriAser.or. .o_Primpres_AB<>.w_Primpres_AB.or. .o_Primpres_AS<>.w_Primpres_AS
            .w_ROTTURA_ACQ = iif(.w_TipoAcqu<>.w_TipoAserv Or .w_PeriAcqu<>.w_PeriAser Or (.w_TipoAcqu="T" And .w_TipoAserv="T" And .w_Primpres_AB<>.w_Primpres_AS),"S","N")
        endif
        .DoRTCalc(50,50,.t.)
            .w_CEMESINI = iif( .w_TIPOCESS="M", .w_PERICESS, (.w_PERICESS*3)-2 )
            .w_CEMESINI_SERV = iif( .w_TIPOCSERV="M", .w_PERICSER, (.w_PERICSER*3)-2 )
            .w_CEMESFIN_SERV = iif(.w_TIPOCSERV="M", .w_PERICSER,IIF(.w_PRIMPRES_CS='0',(.w_PERICSER*3),IIF(.w_PRIMPRES_CS='8',.w_CEMESINI_SERV,(.w_PERICSER*3)-1)))
            .w_CEMESFIN = iif( .w_TIPOCESS="M", .w_PERICESS,IIF(.w_PRIMPRES='0',(.w_PERICESS*3),IIF(.w_PRIMPRES='8',.w_CEMESINI,(.w_PERICESS*3)-1)))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTipoGeneServizi_1_13.enabled = this.oPgFrm.Page1.oPag.oTipoGeneServizi_1_13.mCond()
    this.oPgFrm.Page1.oPag.oPERICESS_1_25.enabled = this.oPgFrm.Page1.oPag.oPERICESS_1_25.mCond()
    this.oPgFrm.Page1.oPag.oPERIACQU_1_28.enabled = this.oPgFrm.Page1.oPag.oPERIACQU_1_28.mCond()
    this.oPgFrm.Page1.oPag.oFLLIRE_1_48.enabled = this.oPgFrm.Page1.oPag.oFLLIRE_1_48.mCond()
    this.oPgFrm.Page1.oPag.oPERICSER_1_58.enabled = this.oPgFrm.Page1.oPag.oPERICSER_1_58.mCond()
    this.oPgFrm.Page1.oPag.oPRIMPRES_1_62.enabled = this.oPgFrm.Page1.oPag.oPRIMPRES_1_62.mCond()
    this.oPgFrm.Page1.oPag.oPRIMPRES_CS_1_63.enabled = this.oPgFrm.Page1.oPag.oPRIMPRES_CS_1_63.mCond()
    this.oPgFrm.Page1.oPag.oPRIMPRES_AB_1_64.enabled = this.oPgFrm.Page1.oPag.oPRIMPRES_AB_1_64.mCond()
    this.oPgFrm.Page1.oPag.oPERIASER_1_66.enabled = this.oPgFrm.Page1.oPag.oPERIASER_1_66.mCond()
    this.oPgFrm.Page1.oPag.oPRIMPRES_AS_1_67.enabled = this.oPgFrm.Page1.oPag.oPRIMPRES_AS_1_67.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLLIRE_1_48.visible=!this.oPgFrm.Page1.oPag.oFLLIRE_1_48.mHide()
    this.oPgFrm.Page1.oPag.oPRIMPRES_1_62.visible=!this.oPgFrm.Page1.oPag.oPRIMPRES_1_62.mHide()
    this.oPgFrm.Page1.oPag.oPRIMPRES_CS_1_63.visible=!this.oPgFrm.Page1.oPag.oPRIMPRES_CS_1_63.mHide()
    this.oPgFrm.Page1.oPag.oPRIMPRES_AB_1_64.visible=!this.oPgFrm.Page1.oPag.oPRIMPRES_AB_1_64.mHide()
    this.oPgFrm.Page1.oPag.oPRIMPRES_AS_1_67.visible=!this.oPgFrm.Page1.oPag.oPRIMPRES_AS_1_67.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ReadAzie
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ReadAzie) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ReadAzie)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZPIVAZI,AZAICFPI,AZNPRGEL,AZDTOBBL,AZAIRAGS,AZAICITT,AZLOCAZI,AZPERAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_ReadAzie);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_ReadAzie)
            select AZCODAZI,AZPIVAZI,AZAICFPI,AZNPRGEL,AZDTOBBL,AZAIRAGS,AZAICITT,AZLOCAZI,AZPERAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ReadAzie = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZPIVAZI = NVL(_Link_.AZPIVAZI,space(12))
      this.w_AZAICFPI = NVL(_Link_.AZAICFPI,space(12))
      this.w_AZNPRGEL = NVL(_Link_.AZNPRGEL,0)
      this.w_DTOBBL = NVL(_Link_.AZDTOBBL,space(1))
      this.w_AZAIRAGS = NVL(_Link_.AZAIRAGS,space(40))
      this.w_AZAICITT = NVL(_Link_.AZAICITT,space(25))
      this.w_AZLOCALI = NVL(_Link_.AZLOCAZI,space(30))
      this.w_AZPERAZI = NVL(_Link_.AZPERAZI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ReadAzie = space(5)
      endif
      this.w_AZPIVAZI = space(12)
      this.w_AZAICFPI = space(12)
      this.w_AZNPRGEL = 0
      this.w_DTOBBL = space(1)
      this.w_AZAIRAGS = space(40)
      this.w_AZAICITT = space(25)
      this.w_AZLOCALI = space(30)
      this.w_AZPERAZI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ReadAzie Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ReadAzieDatInt
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZDATINT_IDX,3]
    i_lTable = "AZDATINT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZDATINT_IDX,2], .t., this.AZDATINT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZDATINT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ReadAzieDatInt) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ReadAzieDatInt)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ITCODAZI,ITAITIPE,ITAITIPV,ITINPECE,ITINPEAC,ITAITIPS,ITINPECS,ITAITIAS,ITINPEAS";
                   +" from "+i_cTable+" "+i_lTable+" where ITCODAZI="+cp_ToStrODBC(this.w_ReadAzieDatInt);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ITCODAZI',this.w_ReadAzieDatInt)
            select ITCODAZI,ITAITIPE,ITAITIPV,ITINPECE,ITINPEAC,ITAITIPS,ITINPECS,ITAITIAS,ITINPEAS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ReadAzieDatInt = NVL(_Link_.ITCODAZI,space(5))
      this.w_TIPOCESS = NVL(_Link_.ITAITIPE,space(1))
      this.w_TIPOACQU = NVL(_Link_.ITAITIPV,space(1))
      this.w_AZINPECE = NVL(_Link_.ITINPECE,0)
      this.w_AZINPEAC = NVL(_Link_.ITINPEAC,0)
      this.w_TIPOCSERV = NVL(_Link_.ITAITIPS,space(1))
      this.w_AZINPECS = NVL(_Link_.ITINPECS,0)
      this.w_TIPOASERV = NVL(_Link_.ITAITIAS,space(1))
      this.w_AZINPEAS = NVL(_Link_.ITINPEAS,0)
    else
      if i_cCtrl<>'Load'
        this.w_ReadAzieDatInt = space(5)
      endif
      this.w_TIPOCESS = space(1)
      this.w_TIPOACQU = space(1)
      this.w_AZINPECE = 0
      this.w_AZINPEAC = 0
      this.w_TIPOCSERV = space(1)
      this.w_AZINPECS = 0
      this.w_TIPOASERV = space(1)
      this.w_AZINPEAS = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZDATINT_IDX,2])+'\'+cp_ToStr(_Link_.ITCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZDATINT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ReadAzieDatInt Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECIM = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DECIM = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTipoGeneServizi_1_13.RadioValue()==this.w_TipoGeneServizi)
      this.oPgFrm.Page1.oPag.oTipoGeneServizi_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSOGGDELE_1_14.RadioValue()==this.w_SOGGDELE)
      this.oPgFrm.Page1.oPag.oSOGGDELE_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCESSATTI_1_15.RadioValue()==this.w_CESSATTI)
      this.oPgFrm.Page1.oPag.oCESSATTI_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFRONTCAR_1_16.RadioValue()==this.w_FRONTCAR)
      this.oPgFrm.Page1.oPag.oFRONTCAR_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANNORIFE_1_17.value==this.w_ANNORIFE)
      this.oPgFrm.Page1.oPag.oANNORIFE_1_17.value=this.w_ANNORIFE
    endif
    if not(this.oPgFrm.Page1.oPag.oPIvaPres_1_18.value==this.w_PIvaPres)
      this.oPgFrm.Page1.oPag.oPIvaPres_1_18.value=this.w_PIvaPres
    endif
    if not(this.oPgFrm.Page1.oPag.oCESS_BENI_1_19.RadioValue()==this.w_CESS_BENI)
      this.oPgFrm.Page1.oPag.oCESS_BENI_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCESS_SERV_1_20.RadioValue()==this.w_CESS_SERV)
      this.oPgFrm.Page1.oPag.oCESS_SERV_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oACQ_BENI_1_21.RadioValue()==this.w_ACQ_BENI)
      this.oPgFrm.Page1.oPag.oACQ_BENI_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oACQ_SERV_1_22.RadioValue()==this.w_ACQ_SERV)
      this.oPgFrm.Page1.oPag.oACQ_SERV_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOCESS_1_23.RadioValue()==this.w_TIPOCESS)
      this.oPgFrm.Page1.oPag.oTIPOCESS_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERICESS_1_25.value==this.w_PERICESS)
      this.oPgFrm.Page1.oPag.oPERICESS_1_25.value=this.w_PERICESS
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOACQU_1_27.RadioValue()==this.w_TIPOACQU)
      this.oPgFrm.Page1.oPag.oTIPOACQU_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIACQU_1_28.value==this.w_PERIACQU)
      this.oPgFrm.Page1.oPag.oPERIACQU_1_28.value=this.w_PERIACQU
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMEFILE_1_29.value==this.w_NOMEFILE)
      this.oPgFrm.Page1.oPag.oNOMEFILE_1_29.value=this.w_NOMEFILE
    endif
    if not(this.oPgFrm.Page1.oPag.oNProgEle_1_31.value==this.w_NProgEle)
      this.oPgFrm.Page1.oPag.oNProgEle_1_31.value=this.w_NProgEle
    endif
    if not(this.oPgFrm.Page1.oPag.oFLLIRE_1_48.RadioValue()==this.w_FLLIRE)
      this.oPgFrm.Page1.oPag.oFLLIRE_1_48.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERICSER_1_58.value==this.w_PERICSER)
      this.oPgFrm.Page1.oPag.oPERICSER_1_58.value=this.w_PERICSER
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOCSERV_1_59.RadioValue()==this.w_TIPOCSERV)
      this.oPgFrm.Page1.oPag.oTIPOCSERV_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRIMPRES_1_62.RadioValue()==this.w_PRIMPRES)
      this.oPgFrm.Page1.oPag.oPRIMPRES_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRIMPRES_CS_1_63.RadioValue()==this.w_PRIMPRES_CS)
      this.oPgFrm.Page1.oPag.oPRIMPRES_CS_1_63.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPRIMPRES_AB_1_64.RadioValue()==this.w_PRIMPRES_AB)
      this.oPgFrm.Page1.oPag.oPRIMPRES_AB_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOASERV_1_65.RadioValue()==this.w_TIPOASERV)
      this.oPgFrm.Page1.oPag.oTIPOASERV_1_65.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIASER_1_66.value==this.w_PERIASER)
      this.oPgFrm.Page1.oPag.oPERIASER_1_66.value=this.w_PERIASER
    endif
    if not(this.oPgFrm.Page1.oPag.oPRIMPRES_AS_1_67.RadioValue()==this.w_PRIMPRES_AS)
      this.oPgFrm.Page1.oPag.oPRIMPRES_AS_1_67.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ANNORIFE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNORIFE_1_17.SetFocus()
            i_bnoObbl = !empty(.w_ANNORIFE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PERICESS)) or not((.w_PeriCess<5.and..w_TipoCess="T").or.(.w_PeriCess<13.and..w_TipoCess="M")))  and (.w_CESS_BENI="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERICESS_1_25.SetFocus()
            i_bnoObbl = !empty(.w_PERICESS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PERIACQU)) or not((.w_PeriAcqu<5.and..w_TipoAcqu="T").or.(.w_PeriAcqu<13.and..w_TipoAcqu="M")))  and (.w_ACQ_BENI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERIACQU_1_28.SetFocus()
            i_bnoObbl = !empty(.w_PERIACQU)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NOMEFILE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNOMEFILE_1_29.SetFocus()
            i_bnoObbl = !empty(.w_NOMEFILE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NProgEle))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNProgEle_1_31.SetFocus()
            i_bnoObbl = !empty(.w_NProgEle)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PERICSER)) or not((.w_PeriCser<5 and .w_TipoCserv="T") or (.w_PeriCser<13 and .w_TipoCserv="M")))  and (.w_CESS_SERV="S")
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERICSER_1_58.SetFocus()
            i_bnoObbl = !empty(.w_PERICSER)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_PERIASER)) or not((.w_PeriAser<5 and .w_TipoAserv="T") or (.w_PeriAser<13 and .w_TipoAserv="M")))  and (.w_ACQ_SERV='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERIASER_1_66.SetFocus()
            i_bnoObbl = !empty(.w_PERIASER)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SOGGDELE = this.w_SOGGDELE
    this.o_ANNORIFE = this.w_ANNORIFE
    this.o_TIPOCESS = this.w_TIPOCESS
    this.o_PERICESS = this.w_PERICESS
    this.o_TIPOACQU = this.w_TIPOACQU
    this.o_PERIACQU = this.w_PERIACQU
    this.o_PERICSER = this.w_PERICSER
    this.o_TIPOCSERV = this.w_TIPOCSERV
    this.o_PRIMPRES = this.w_PRIMPRES
    this.o_PRIMPRES_CS = this.w_PRIMPRES_CS
    this.o_PRIMPRES_AB = this.w_PRIMPRES_AB
    this.o_TIPOASERV = this.w_TIPOASERV
    this.o_PERIASER = this.w_PERIASER
    this.o_PRIMPRES_AS = this.w_PRIMPRES_AS
    return

enddefine

* --- Define pages as container
define class tgsar_kefPag1 as StdContainer
  Width  = 818
  height = 427
  stdWidth  = 818
  stdheight = 427
  resizeXpos=404
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTipoGeneServizi_1_13 as StdCombo with uid="KHZEZNOULF",rtseq=12,rtrep=.f.,left=19,top=154,width=152,height=22;
    , ToolTipText = "Definisce il tipo di generazione, se semplificata i campi facoltativi (numero e data fattura, modalit� di erogazione e incasso) non verranno valorizzati";
    , HelpContextID = 118129099;
    , cFormVar="w_TipoGeneServizi",RowSource=""+"Completa,"+"Semplificata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTipoGeneServizi_1_13.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oTipoGeneServizi_1_13.GetRadio()
    this.Parent.oContained.w_TipoGeneServizi = this.RadioValue()
    return .t.
  endfunc

  func oTipoGeneServizi_1_13.SetRadio()
    this.Parent.oContained.w_TipoGeneServizi=trim(this.Parent.oContained.w_TipoGeneServizi)
    this.value = ;
      iif(this.Parent.oContained.w_TipoGeneServizi=='C',1,;
      iif(this.Parent.oContained.w_TipoGeneServizi=='S',2,;
      0))
  endfunc

  func oTipoGeneServizi_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AnnoRife>='2015')
    endwith
   endif
  endfunc

  add object oSOGGDELE_1_14 as StdCheck with uid="VWCCTCAVRB",rtseq=13,rtrep=.f.,left=19, top=202, caption="Soggetto delegato",;
    HelpContextID = 173912725,;
    cFormVar="w_SOGGDELE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSOGGDELE_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSOGGDELE_1_14.GetRadio()
    this.Parent.oContained.w_SOGGDELE = this.RadioValue()
    return .t.
  endfunc

  func oSOGGDELE_1_14.SetRadio()
    this.Parent.oContained.w_SOGGDELE=trim(this.Parent.oContained.w_SOGGDELE)
    this.value = ;
      iif(this.Parent.oContained.w_SOGGDELE=='S',1,;
      0)
  endfunc


  add object oCESSATTI_1_15 as StdCombo with uid="ULBITQWNAX",rtseq=14,rtrep=.f.,left=19,top=233,width=226,height=21;
    , ToolTipText = "Definisce i casi particolari riferiti al soggetto obbligato";
    , HelpContextID = 75432559;
    , cFormVar="w_CESSATTI",RowSource=""+"Primo elenco presentato,"+"Cessazione attivit� o variazione P. IVA,"+"Entrambi,"+"Nessuno", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCESSATTI_1_15.RadioValue()
    return(iif(this.value =1,'7',;
    iif(this.value =2,'8',;
    iif(this.value =3,'9',;
    iif(this.value =4,'0',;
    space(1))))))
  endfunc
  func oCESSATTI_1_15.GetRadio()
    this.Parent.oContained.w_CESSATTI = this.RadioValue()
    return .t.
  endfunc

  func oCESSATTI_1_15.SetRadio()
    this.Parent.oContained.w_CESSATTI=trim(this.Parent.oContained.w_CESSATTI)
    this.value = ;
      iif(this.Parent.oContained.w_CESSATTI=='7',1,;
      iif(this.Parent.oContained.w_CESSATTI=='8',2,;
      iif(this.Parent.oContained.w_CESSATTI=='9',3,;
      iif(this.Parent.oContained.w_CESSATTI=='0',4,;
      0))))
  endfunc

  add object oFRONTCAR_1_16 as StdCheck with uid="BCABDFLOJZ",rtseq=15,rtrep=.f.,left=19, top=260, caption="Frontesp. cartaceo",;
    ToolTipText = "Se attivo: stampa frontespizio cartaceo",;
    HelpContextID = 78237608,;
    cFormVar="w_FRONTCAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFRONTCAR_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFRONTCAR_1_16.GetRadio()
    this.Parent.oContained.w_FRONTCAR = this.RadioValue()
    return .t.
  endfunc

  func oFRONTCAR_1_16.SetRadio()
    this.Parent.oContained.w_FRONTCAR=trim(this.Parent.oContained.w_FRONTCAR)
    this.value = ;
      iif(this.Parent.oContained.w_FRONTCAR=='S',1,;
      0)
  endfunc

  add object oANNORIFE_1_17 as StdField with uid="JPHLTXUKOY",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ANNORIFE", cQueryName = "ANNORIFE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 176864075,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=310, Top=46, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  add object oPIvaPres_1_18 as StdField with uid="GUELYLBDSF",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PIvaPres", cQueryName = "PIvaPres",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 209766551,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=616, Top=46, InputMask=replicate('X',12)

  add object oCESS_BENI_1_19 as StdCheck with uid="ELXTNQACKI",rtseq=18,rtrep=.f.,left=22, top=43, caption="Cessioni UE di beni ",;
    HelpContextID = 73336580,;
    cFormVar="w_CESS_BENI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCESS_BENI_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCESS_BENI_1_19.GetRadio()
    this.Parent.oContained.w_CESS_BENI = this.RadioValue()
    return .t.
  endfunc

  func oCESS_BENI_1_19.SetRadio()
    this.Parent.oContained.w_CESS_BENI=trim(this.Parent.oContained.w_CESS_BENI)
    this.value = ;
      iif(this.Parent.oContained.w_CESS_BENI=='S',1,;
      0)
  endfunc

  add object oCESS_SERV_1_20 as StdCheck with uid="LDDDAJKYJX",rtseq=19,rtrep=.f.,left=22, top=68, caption="Cessioni UE di servizi",;
    HelpContextID = 90114008,;
    cFormVar="w_CESS_SERV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCESS_SERV_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCESS_SERV_1_20.GetRadio()
    this.Parent.oContained.w_CESS_SERV = this.RadioValue()
    return .t.
  endfunc

  func oCESS_SERV_1_20.SetRadio()
    this.Parent.oContained.w_CESS_SERV=trim(this.Parent.oContained.w_CESS_SERV)
    this.value = ;
      iif(this.Parent.oContained.w_CESS_SERV=='S',1,;
      0)
  endfunc

  add object oACQ_BENI_1_21 as StdCheck with uid="ZKIQLJVAKL",rtseq=20,rtrep=.f.,left=22, top=93, caption="Acquisti UE di beni ",;
    HelpContextID = 174399409,;
    cFormVar="w_ACQ_BENI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oACQ_BENI_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oACQ_BENI_1_21.GetRadio()
    this.Parent.oContained.w_ACQ_BENI = this.RadioValue()
    return .t.
  endfunc

  func oACQ_BENI_1_21.SetRadio()
    this.Parent.oContained.w_ACQ_BENI=trim(this.Parent.oContained.w_ACQ_BENI)
    this.value = ;
      iif(this.Parent.oContained.w_ACQ_BENI=='S',1,;
      0)
  endfunc

  add object oACQ_SERV_1_22 as StdCheck with uid="HJYGGEBJRL",rtseq=21,rtrep=.f.,left=22, top=118, caption="Acquisti UE di servizi",;
    HelpContextID = 111861852,;
    cFormVar="w_ACQ_SERV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oACQ_SERV_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oACQ_SERV_1_22.GetRadio()
    this.Parent.oContained.w_ACQ_SERV = this.RadioValue()
    return .t.
  endfunc

  func oACQ_SERV_1_22.SetRadio()
    this.Parent.oContained.w_ACQ_SERV=trim(this.Parent.oContained.w_ACQ_SERV)
    this.value = ;
      iif(this.Parent.oContained.w_ACQ_SERV=='S',1,;
      0)
  endfunc


  add object oTIPOCESS_1_23 as StdCombo with uid="FWERBEDQOY",rtseq=22,rtrep=.f.,left=263,top=115,width=153,height=21, enabled=.f.;
    , HelpContextID = 94033801;
    , cFormVar="w_TIPOCESS",RowSource=""+"Mensile,"+"Trimestrale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOCESS_1_23.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oTIPOCESS_1_23.GetRadio()
    this.Parent.oContained.w_TIPOCESS = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCESS_1_23.SetRadio()
    this.Parent.oContained.w_TIPOCESS=trim(this.Parent.oContained.w_TIPOCESS)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCESS=='M',1,;
      iif(this.Parent.oContained.w_TIPOCESS=='T',2,;
      0))
  endfunc

  add object oPERICESS_1_25 as StdField with uid="GBVQCADMDC",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PERICESS", cQueryName = "PERICESS",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 93647689,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=484, Top=114, cSayPict='"@Z 99"', cGetPict='"@Z 99"'

  func oPERICESS_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CESS_BENI="S")
    endwith
   endif
  endfunc

  func oPERICESS_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PeriCess<5.and..w_TipoCess="T").or.(.w_PeriCess<13.and..w_TipoCess="M"))
    endwith
    return bRes
  endfunc


  add object oTIPOACQU_1_27 as StdCombo with uid="FKKVICFOZB",rtseq=26,rtrep=.f.,left=536,top=115,width=153,height=21, enabled=.f.;
    , HelpContextID = 58382219;
    , cFormVar="w_TIPOACQU",RowSource=""+"Mensile,"+"Trimestrale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOACQU_1_27.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oTIPOACQU_1_27.GetRadio()
    this.Parent.oContained.w_TIPOACQU = this.RadioValue()
    return .t.
  endfunc

  func oTIPOACQU_1_27.SetRadio()
    this.Parent.oContained.w_TIPOACQU=trim(this.Parent.oContained.w_TIPOACQU)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOACQU=='M',1,;
      iif(this.Parent.oContained.w_TIPOACQU=='T',2,;
      0))
  endfunc

  add object oPERIACQU_1_28 as StdField with uid="DXNDOKMQAP",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PERIACQU", cQueryName = "PERIACQU",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 57996107,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=759, Top=114, cSayPict='"@Z 99"', cGetPict='"@Z 99"'

  func oPERIACQU_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ACQ_BENI='S')
    endwith
   endif
  endfunc

  func oPERIACQU_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PeriAcqu<5.and..w_TipoAcqu="T").or.(.w_PeriAcqu<13.and..w_TipoAcqu="M"))
    endwith
    return bRes
  endfunc

  add object oNOMEFILE_1_29 as StdField with uid="GDRPKGIZEW",rtseq=28,rtrep=.f.,;
    cFormVar = "w_NOMEFILE", cQueryName = "NOMEFILE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 104813285,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=406, Top=294, InputMask=replicate('X',40)


  add object oBtn_1_30 as StdButton with uid="CAKWZHWZEA",left=693, top=295, width=22,height=21,;
    caption="...", nPag=1;
    , HelpContextID = 65431082;
  , bGlobalFont=.t.

    proc oBtn_1_30.Click()
      with this.Parent.oContained
        .w_NOMEFILE=left(Cp_getdir(sys(5)+sys(2003),"Percorso di destinazione")+"SCAMBI.CEE"+space(40),40)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oNProgEle_1_31 as StdField with uid="RPGEKYDXDM",rtseq=29,rtrep=.f.,;
    cFormVar = "w_NProgEle", cQueryName = "NProgEle",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero progressivo dischetto",;
    HelpContextID = 134414789,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=406, Top=332, cSayPict='"999999"', cGetPict='"999999"'

  add object oFLLIRE_1_48 as StdCheck with uid="NFTWHJZVJJ",rtseq=30,rtrep=.f.,left=19, top=284, caption="Importi in Lire",;
    ToolTipText = "Se attivo: gli elenchi verranno generati riferiti alla valuta nazionale; altrimenti Euro",;
    HelpContextID = 109353302,;
    cFormVar="w_FLLIRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLLIRE_1_48.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLLIRE_1_48.GetRadio()
    this.Parent.oContained.w_FLLIRE = this.RadioValue()
    return .t.
  endfunc

  func oFLLIRE_1_48.SetRadio()
    this.Parent.oContained.w_FLLIRE=trim(this.Parent.oContained.w_FLLIRE)
    this.value = ;
      iif(this.Parent.oContained.w_FLLIRE=='S',1,;
      0)
  endfunc

  func oFLLIRE_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERVAL<>g_CODEUR)
    endwith
   endif
  endfunc

  func oFLLIRE_1_48.mHide()
    with this.Parent.oContained
      return (g_PERVAL=g_CODEUR)
    endwith
  endfunc


  add object oBtn_1_52 as StdButton with uid="ULWOZFBKPU",left=710, top=373, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per inizio generazione";
    , HelpContextID = 65603354;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_52.Click()
      with this.Parent.oContained
        do GSAR_BEF with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_53 as StdButton with uid="ABDPRCJXMN",left=764, top=373, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 58314682;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_53.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPERICSER_1_58 as StdField with uid="KYIATZZRJX",rtseq=36,rtrep=.f.,;
    cFormVar = "w_PERICSER", cQueryName = "PERICSER",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 60093256,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=484, Top=198, cSayPict='"@Z 99"', cGetPict='"@Z 99"'

  func oPERICSER_1_58.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CESS_SERV="S")
    endwith
   endif
  endfunc

  func oPERICSER_1_58.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PeriCser<5 and .w_TipoCserv="T") or (.w_PeriCser<13 and .w_TipoCserv="M"))
    endwith
    return bRes
  endfunc


  add object oTIPOCSERV_1_59 as StdCombo with uid="CZCQJXMWPU",rtseq=37,rtrep=.f.,left=263,top=198,width=153,height=22, enabled=.f.;
    , HelpContextID = 60480744;
    , cFormVar="w_TIPOCSERV",RowSource=""+"Mensile,"+"Trimestrale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOCSERV_1_59.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oTIPOCSERV_1_59.GetRadio()
    this.Parent.oContained.w_TIPOCSERV = this.RadioValue()
    return .t.
  endfunc

  func oTIPOCSERV_1_59.SetRadio()
    this.Parent.oContained.w_TIPOCSERV=trim(this.Parent.oContained.w_TIPOCSERV)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOCSERV=='M',1,;
      iif(this.Parent.oContained.w_TIPOCSERV=='T',2,;
      0))
  endfunc


  add object oPRIMPRES_1_62 as StdCombo with uid="KYYHEPSKPT",rtseq=38,rtrep=.f.,left=263,top=144,width=226,height=22;
    , ToolTipText = "Contenuto elenchi";
    , HelpContextID = 57176137;
    , cFormVar="w_PRIMPRES",RowSource=""+"Trimestre completo,"+"Solo primo mese del trimestre,"+"Primo e secondo mese del trimestre", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRIMPRES_1_62.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'8',;
    iif(this.value =3,'9',;
    space(1)))))
  endfunc
  func oPRIMPRES_1_62.GetRadio()
    this.Parent.oContained.w_PRIMPRES = this.RadioValue()
    return .t.
  endfunc

  func oPRIMPRES_1_62.SetRadio()
    this.Parent.oContained.w_PRIMPRES=trim(this.Parent.oContained.w_PRIMPRES)
    this.value = ;
      iif(this.Parent.oContained.w_PRIMPRES=='0',1,;
      iif(this.Parent.oContained.w_PRIMPRES=='8',2,;
      iif(this.Parent.oContained.w_PRIMPRES=='9',3,;
      0)))
  endfunc

  func oPRIMPRES_1_62.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOCESS='T' AND .w_CESS_BENI="S")
    endwith
   endif
  endfunc

  func oPRIMPRES_1_62.mHide()
    with this.Parent.oContained
      return (.w_TIPOCESS='M')
    endwith
  endfunc


  add object oPRIMPRES_CS_1_63 as StdCombo with uid="AUJIKJGDLB",rtseq=39,rtrep=.f.,left=263,top=228,width=226,height=22;
    , ToolTipText = "Contenuto elenchi";
    , HelpContextID = 57534777;
    , cFormVar="w_PRIMPRES_CS",RowSource=""+"Trimestre completo,"+"Solo primo mese del trimestre,"+"Primo e secondo mese del trimestre", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRIMPRES_CS_1_63.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'8',;
    iif(this.value =3,'9',;
    space(1)))))
  endfunc
  func oPRIMPRES_CS_1_63.GetRadio()
    this.Parent.oContained.w_PRIMPRES_CS = this.RadioValue()
    return .t.
  endfunc

  func oPRIMPRES_CS_1_63.SetRadio()
    this.Parent.oContained.w_PRIMPRES_CS=trim(this.Parent.oContained.w_PRIMPRES_CS)
    this.value = ;
      iif(this.Parent.oContained.w_PRIMPRES_CS=='0',1,;
      iif(this.Parent.oContained.w_PRIMPRES_CS=='8',2,;
      iif(this.Parent.oContained.w_PRIMPRES_CS=='9',3,;
      0)))
  endfunc

  func oPRIMPRES_CS_1_63.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOCSERV='T' AND .w_CESS_SERV="S")
    endwith
   endif
  endfunc

  func oPRIMPRES_CS_1_63.mHide()
    with this.Parent.oContained
      return (.w_TIPOCSERV='M')
    endwith
  endfunc


  add object oPRIMPRES_AB_1_64 as StdCombo with uid="XXSYSCNVHB",rtseq=40,rtrep=.f.,left=536,top=144,width=226,height=22;
    , ToolTipText = "Contenuto elenchi";
    , HelpContextID = 57464633;
    , cFormVar="w_PRIMPRES_AB",RowSource=""+"Trimestre completo,"+"Solo primo mese del trimestre,"+"Primo e secondo mese del trimestre", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRIMPRES_AB_1_64.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'8',;
    iif(this.value =3,'9',;
    space(1)))))
  endfunc
  func oPRIMPRES_AB_1_64.GetRadio()
    this.Parent.oContained.w_PRIMPRES_AB = this.RadioValue()
    return .t.
  endfunc

  func oPRIMPRES_AB_1_64.SetRadio()
    this.Parent.oContained.w_PRIMPRES_AB=trim(this.Parent.oContained.w_PRIMPRES_AB)
    this.value = ;
      iif(this.Parent.oContained.w_PRIMPRES_AB=='0',1,;
      iif(this.Parent.oContained.w_PRIMPRES_AB=='8',2,;
      iif(this.Parent.oContained.w_PRIMPRES_AB=='9',3,;
      0)))
  endfunc

  func oPRIMPRES_AB_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOACQU='T' AND .w_ACQ_BENI='S')
    endwith
   endif
  endfunc

  func oPRIMPRES_AB_1_64.mHide()
    with this.Parent.oContained
      return (.w_TIPOACQU='M')
    endwith
  endfunc


  add object oTIPOASERV_1_65 as StdCombo with uid="BSZOLUHNMW",rtseq=41,rtrep=.f.,left=538,top=199,width=153,height=22, enabled=.f.;
    , HelpContextID = 58383592;
    , cFormVar="w_TIPOASERV",RowSource=""+"Mensile,"+"Trimestrale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOASERV_1_65.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oTIPOASERV_1_65.GetRadio()
    this.Parent.oContained.w_TIPOASERV = this.RadioValue()
    return .t.
  endfunc

  func oTIPOASERV_1_65.SetRadio()
    this.Parent.oContained.w_TIPOASERV=trim(this.Parent.oContained.w_TIPOASERV)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOASERV=='M',1,;
      iif(this.Parent.oContained.w_TIPOASERV=='T',2,;
      0))
  endfunc

  add object oPERIASER_1_66 as StdField with uid="AUQRRKFNKP",rtseq=42,rtrep=.f.,;
    cFormVar = "w_PERIASER", cQueryName = "PERIASER",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 57996104,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=759, Top=198, cSayPict='"@Z 99"', cGetPict='"@Z 99"'

  func oPERIASER_1_66.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ACQ_SERV='S')
    endwith
   endif
  endfunc

  func oPERIASER_1_66.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_PeriAser<5 and .w_TipoAserv="T") or (.w_PeriAser<13 and .w_TipoAserv="M"))
    endwith
    return bRes
  endfunc


  add object oPRIMPRES_AS_1_67 as StdCombo with uid="JCJTQMZWWE",rtseq=43,rtrep=.f.,left=538,top=228,width=226,height=22;
    , ToolTipText = "Contenuto elenchi";
    , HelpContextID = 57534265;
    , cFormVar="w_PRIMPRES_AS",RowSource=""+"Trimestre completo,"+"Solo primo mese del trimestre,"+"Primo e secondo mese del trimestre", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPRIMPRES_AS_1_67.RadioValue()
    return(iif(this.value =1,'0',;
    iif(this.value =2,'8',;
    iif(this.value =3,'9',;
    space(1)))))
  endfunc
  func oPRIMPRES_AS_1_67.GetRadio()
    this.Parent.oContained.w_PRIMPRES_AS = this.RadioValue()
    return .t.
  endfunc

  func oPRIMPRES_AS_1_67.SetRadio()
    this.Parent.oContained.w_PRIMPRES_AS=trim(this.Parent.oContained.w_PRIMPRES_AS)
    this.value = ;
      iif(this.Parent.oContained.w_PRIMPRES_AS=='0',1,;
      iif(this.Parent.oContained.w_PRIMPRES_AS=='8',2,;
      iif(this.Parent.oContained.w_PRIMPRES_AS=='9',3,;
      0)))
  endfunc

  func oPRIMPRES_AS_1_67.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOASERV='T' AND .w_ACQ_SERV='S')
    endwith
   endif
  endfunc

  func oPRIMPRES_AS_1_67.mHide()
    with this.Parent.oContained
      return (.w_TIPOASERV='M')
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="MVZOTOFYWQ",Visible=.t., Left=267, Top=47,;
    Alignment=1, Width=41, Height=15,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="WLLELXIRZP",Visible=.t., Left=313, Top=295,;
    Alignment=1, Width=91, Height=15,;
    Caption="Nome del file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="HGRSVJGZQO",Visible=.t., Left=16, Top=19,;
    Alignment=2, Width=166, Height=15,;
    Caption="Opzioni di generazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_35 as StdString with uid="KOQZOJPYCJ",Visible=.t., Left=261, Top=87,;
    Alignment=0, Width=194, Height=18,;
    Caption="Modello INTRA 1 - cessioni beni UE"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="KYRJTUBUTL",Visible=.t., Left=477, Top=87,;
    Alignment=0, Width=52, Height=15,;
    Caption="Periodo"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="AHZOHRNRFG",Visible=.t., Left=746, Top=90,;
    Alignment=0, Width=52, Height=15,;
    Caption="Periodo"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="BXOWMSYMNO",Visible=.t., Left=534, Top=90,;
    Alignment=0, Width=190, Height=18,;
    Caption="Modello INTRA 2 - acquisti beni UE"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="JOPNWTQRFX",Visible=.t., Left=257, Top=21,;
    Alignment=2, Width=488, Height=15,;
    Caption="Periodo di riferimento e dati di presentazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_45 as StdString with uid="CYFYQAWLHQ",Visible=.t., Left=257, Top=267,;
    Alignment=2, Width=488, Height=15,;
    Caption="Supporto di destinazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="PAJZGLSGNS",Visible=.t., Left=313, Top=332,;
    Alignment=1, Width=91, Height=15,;
    Caption="N. progr. disco:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="XJGCMWTZZQ",Visible=.t., Left=480, Top=47,;
    Alignment=1, Width=130, Height=15,;
    Caption="P.IVA presentatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="VMUJNTTKKI",Visible=.t., Left=264, Top=171,;
    Alignment=0, Width=204, Height=18,;
    Caption="Modello INTRA 1 - cessioni servizi UE"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="EXVXGLONSW",Visible=.t., Left=478, Top=171,;
    Alignment=0, Width=51, Height=15,;
    Caption="Periodo"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="JIDMABCFZX",Visible=.t., Left=747, Top=171,;
    Alignment=0, Width=51, Height=15,;
    Caption="Periodo"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="QXHRRBEFAD",Visible=.t., Left=538, Top=171,;
    Alignment=0, Width=204, Height=18,;
    Caption="Modello INTRA 2 - acquisti servizi UE"  ;
  , bGlobalFont=.t.

  add object oBox_1_10 as StdBox with uid="QDJEKYZMXO",left=11, top=17, width=240,height=350

  add object oBox_1_39 as StdBox with uid="AWEBQGMARU",left=529, top=75, width=1,height=184

  add object oBox_1_41 as StdBox with uid="IBUWOEFTQD",left=257, top=17, width=556,height=245

  add object oBox_1_42 as StdBox with uid="PJQPPJKDJQ",left=257, top=39, width=554,height=1

  add object oBox_1_43 as StdBox with uid="KNPXCQHOXV",left=257, top=75, width=555,height=2

  add object oBox_1_44 as StdBox with uid="IARKIXEXEA",left=12, top=35, width=175,height=2

  add object oBox_1_46 as StdBox with uid="TVOKZTPCRT",left=257, top=260, width=556,height=107

  add object oBox_1_47 as StdBox with uid="CEDVYDUXZV",left=258, top=285, width=553,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kef','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
