* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bgp                                                        *
*              Generazione provvigioni                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_342]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2013-09-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bgp",oParentObject)
return(i_retval)

define class tgsve_bgp as StdBatch
  * --- Local variables
  w_PARAM = space(3)
  w_oERRORLOG = .NULL.
  w_LOG = .f.
  w_GRUFAT1 = 0
  w_GRUSCA1 = 0
  w_GRUINC1 = 0
  w_FLMATCAP = space(1)
  w_ACCOLD = 0
  w_RIFCON = space(10)
  w_ACC_DA_DOC = .f.
  w_MAXLOOP = 0
  w_DATMAX = ctod("  /  /  ")
  w_DATMIN = ctod("  /  /  ")
  w_DATSCA = ctod("  /  /  ")
  w_DATAPPO = ctod("  /  /  ")
  w_WARNING = .f.
  w_MVACCPRE = 0
  w_MVACCON = 0
  w_CONTA = .f.
  w_DATOLD = ctod("  /  /  ")
  w_MVSERRIF = space(10)
  w_MVACCONT = 0
  w_TROVA = space(0)
  w_SPERIP = .f.
  w_TOTIMP2 = 0
  w_MVTIPPR2 = space(2)
  w_MVPROCAP = 0
  w_OLDPROCAP = 0
  w_MVIMPCAP = 0
  w_PERCAP = 0
  w_AGEPRO = space(5)
  w_ESCLUSA = 0
  w_RIGHETOT = 0
  w_APP1 = 0
  w_RESTO = 0
  w_RESTAGE = 0
  w_RESTCAP = 0
  w_REST2 = 0
  w_GIAAGGA = .f.
  w_GIAAGGC = .f.
  w_TOTIMP = 0
  w_TROV = .f.
  w_CODAGE = space(5)
  w_APPO1 = space(10)
  w_SC1 = 0
  w_ARRO = 0
  w_MESS = space(10)
  w_CODAG2 = space(5)
  w_ERRORE = .f.
  w_SERIAL = space(10)
  w_SC2 = 0
  w_MESS1 = space(10)
  w_SERIAL = space(10)
  w_NUDOC = 0
  w_SC3 = 0
  w_CHIAVE = space(10)
  w_OSERIAL = space(10)
  w_OKDOC = 0
  w_SC4 = 0
  w_SM = 0
  w_FLSOSP = space(1)
  w_MPSERIAL = space(10)
  w_SC5 = 0
  w_DATDOC = ctod("  /  /  ")
  w_GRUFAT = 0
  w_GRUSCA = 0
  w_MPNUMREG = 0
  w_SC6 = 0
  w_NUMDOC = 0
  w_GRUINC = 0
  w_MPCODESE = space(4)
  w_SC7 = 0
  w_ALFDOC = space(10)
  IMP = space(10)
  w_CPROWNUM = 0
  w_PERPRA = 0
  w_VALNAZ = space(3)
  PRO = space(10)
  w_TIPMAT = space(2)
  w_PERPRC = 0
  w_CODVAL = space(3)
  RA = space(10)
  w_DATSCA = ctod("  /  /  ")
  w_TOTAGE = 0
  w_CAOVAL = 0
  w_APPO = 0
  w_APPOR = 0
  w_APPAR = 0
  w_APPCR = 0
  w_DATMAT = ctod("  /  /  ")
  w_PERAGE = 0
  w_TIPCON = space(1)
  w_APPA = 0
  w_PPA = 0
  w_TOTZON = 0
  w_CODCON = space(15)
  w_APPC = 0
  w_PPC = 0
  w_CATAGE = space(5)
  w_GRUPRO = space(5)
  w_ACCONT = 0
  w_CATCLI = space(5)
  w_NURATE = 0
  w_TROVPR = .f.
  w_CATART = space(5)
  w_MAXRAT = 0
  w_MAXRATESCL = 0
  w_CHIAVE1 = space(10)
  w_AGSCOPAG = space(1)
  w_AGSOSP = space(1)
  w_DATOBSO = ctod("  /  /  ")
  w_MVTIPPRO = space(2)
  w_ROWNUM = 0
  w_TOTIMP1 = 0
  w_MVPERPRO = 0
  w_MVIMPPRO = 0
  Primo = .f.
  w_COMPOSTO = space(10)
  w_TPPROVVI = space(1)
  w_ACCPRE = 0
  w_CALSCO = space(1)
  w_MVTCOLIS = space(5)
  w_MVCODART = space(20)
  w_MVDATREG = ctod("  /  /  ")
  w_MVQTAMOV = 0
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_MVCODICE = space(20)
  w_UNMIS3 = space(3)
  w_MVUNIMIS = space(3)
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MOLTIP = 0
  w_MOLTI3 = 0
  w_SCOLIS = space(1)
  w_MVQTAUM1 = 0
  w_IVALIS = space(1)
  w_MVFLSCOR = space(1)
  w_MVCODIVA = space(5)
  w_PERIVA = 0
  w_DECUNI = 0
  w_MVPREZZO = 0
  w_CATSCC = space(5)
  w_CATSCA = space(5)
  w_QTAUM1 = 0
  w_LIPREZZO = 0
  w_APPOGGIO = space(12)
  w_SCONTO = 0
  w_CALPRZ = 0
  w_MVFLOMAG = space(1)
  w_LISRIFSCO = space(5)
  w_MVSPEINC = 0
  w_MVFLRINC = space(1)
  w_MVSPEIMB = 0
  w_MVFLRIMB = space(1)
  w_MVSPETRA = 0
  w_MVFLRTRA = space(1)
  w_APP2 = 0
  w_APP2R = 0
  w_ARRPROV = 0
  * --- WorkFile variables
  TAB_PROV_idx=0
  DOC_RATE_idx=0
  MOP_MAST_idx=0
  MOP_DETT_idx=0
  DOC_MAST_idx=0
  PRO_AGEN_idx=0
  AGENTI_idx=0
  PAR_PROV_idx=0
  DOC_DETT_idx=0
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  LISTINI_idx=0
  VOCIIVA_idx=0
  VALUTE_idx=0
  TMP_ACCON_idx=0
  TMP_ACCON2_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione Provvigioni (da GSVE_KGP)
    * --- Variabili di Lavoro definite nella Pag.4
    DIMENSION IMP[3], PRO[3], PRC[3], RA[5, 99], IMPC[3]
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- Carica i Documenti da generare
    ah_Msg("Ricerca documenti da elaborare...")
    vq_exec("QUERY\GSVE_BGP.VQR",this,"DOCUGENE")
    * --- Query che restituisce tutti gli agenti e categorie associate
    vq_exec("QUERY\GSVE1BGP.VQR",this,"AGETOT")
    * --- Testa il Cambio di Documento
    this.w_OSERIAL = REPL("#", 10)
    if USED("DOCUGENE")
      * --- Inizio Aggiornamento vero e proprio
      this.w_NUDOC = 0
      this.w_OKDOC = 0
      this.w_ESCLUSA = 0
      this.w_RIGHETOT = 0
      ah_Msg("Inizio fase di generazione...")
      SELECT DOCUGENE
      GO TOP
      SCAN FOR NVL(VALMAG, 0)<>0 AND NOT EMPTY(NVL(CODAGE," "))
      * --- Testa Cambio Documento
      this.w_LOG = .F.
      if this.w_OSERIAL<>NVL(SERIAL,"???")
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT DOCUGENE
        this.w_ERRORE = .F.
        this.w_CODAGE = CODAGE
        this.w_CATAGE = NVL(CATAGE, SPACE(5))
        this.w_AGEPRO = NVL(AGEPRO, SPACE(5))
        this.w_GRUFAT = NVL(GRUFAT, 0)
        this.w_GRUSCA = NVL(GRUSCA, 0)
        this.w_GRUINC = NVL(GRUINC, 0)
        this.w_CODAG2 = NVL(CODAG2, SPACE(5))
        * --- Read from PAR_PROV
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_PROV_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_PROV_idx,2],.t.,this.PAR_PROV_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PPMATCAP"+;
            " from "+i_cTable+" PAR_PROV where ";
                +"PPCODAZI = "+cp_ToStrODBC(i_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PPMATCAP;
            from (i_cTable) where;
                PPCODAZI = i_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLMATCAP = NVL(cp_ToDate(_read_.PPMATCAP),cp_NullValue(_read_.PPMATCAP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_GRUFAT1 = IIF(this.w_FLMATCAP="S",NVL(GRUFAT1, 0),this.w_GRUFAT)
        this.w_GRUSCA1 = IIF(this.w_FLMATCAP="S",NVL(GRUSCA1, 0),this.w_GRUSCA)
        this.w_GRUINC1 = IIF(this.w_FLMATCAP="S",NVL(GRUINC1, 0),this.w_GRUINC)
        this.w_SERIAL = NVL(SERIAL, SPACE(10))
        this.w_OSERIAL = this.w_SERIAL
        this.w_TIPCON = NVL(TIPCON, "C")
        this.w_CODCON = NVL(CODCON, SPACE(15))
        this.w_CATCLI = NVL(CATCLI, SPACE(5))
        this.w_CATSCC = NVL(CATSCC,SPACE(5))
        this.w_DATDOC = CP_TODATE(DATDOC)
        this.w_NUMDOC = NVL(NUMDOC, 0)
        this.w_ALFDOC = ALLTRIM(NVL(ALFDOC, Space(10)) )
        this.w_VALNAZ = NVL(VALNAZ, g_PERVAL)
        this.w_CODVAL = NVL(CODVAL, g_PERVAL)
        this.w_CAOVAL = NVL(CAOVAL, g_CAOVAL)
        * --- Leggo i decimali totali della valuta
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_CODVAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ARRO = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_ACCONT = NVL(ACCONT, 0)
        this.w_ACCPRE = NVL(ACCPRE,0)
        this.w_ACCOLD = NVL(ACCOLD,0)
        this.w_RIFCON = RIFCON
        this.w_SC5 = NVL(SCOCL1, 0)
        this.w_SC6 = NVL(SCOCL2, 0)
        this.w_SC7 = NVL(SCOPAG, 0)
        this.w_FLSOSP = " "
        * --- Per Eventuali Segnalazioni
        * --- Importi suddivisi per Tipo Maturazione 1=FA; 2=SC; 3=IM
        FOR L_i = 1 TO 3
        IMP[L_i] =0
        PRO[L_i] =0
        PRC[L_i] =0
        IMPC[L_i] =0
        ENDFOR
        this.w_NURATE = 0
        this.w_MAXRAT = 0
        this.w_MAXRATESCL = 0
        if this.w_ACCONT<>0
          * --- Considera l'acconto contestuale
          this.w_NURATE = this.w_NURATE + 1
          RA[1, this.w_NURATE] = this.w_DATDOC
          RA[2, this.w_NURATE] = this.w_ACCONT
          this.w_MAXRAT = this.w_MAXRAT + RA[2, this.w_NURATE]
        endif
        * --- Considera l'acconto precedente
        if this.w_ACCPRE<>0 
          * --- Creo una nuova scadenza con l'importo dell'acconto precedente
          *     e la data scadenza che vado a determinare;
          *     
          *     Se esiste un acconto precedente devo verificare la sua origine:
          *     a) Da antenati nella catena documentale
          *     b) da accorpamento a seguito di contabilizzazione
          *     
          *     Nel caso a) l'algoritmo ripercorre a ritroso la catena documentale fino a 
          *     quando non trova documenti con acconti. Di questi la procedura calcola
          *     la data pi� alta. Se si trovano pi� date w_WARNING sar� posta a .t.
          *     per segnalare la cosa all'utente.
          *     
          *     Di seguito al caso a) � affrontato il caso b), qui, una volta capito che l'acconto
          *     precedente � stato modificato dalla contabilizzazione, si recuperano le dte reg/doc
          *     dei vari acconti utilizzati nell'accorpamento. Di queste � presa la data massima
          *     comprendendo la data calcolata al punto a).
          *     
          *     Anche in questo caso se si trovano due date vi � una segnalazione all'utente.
          *     
          *     NEL CASO IL NUMERO DI ITERAZIONI SUPERA IL LIMITE CABLATO
          *     NEL CODICE LA FATTURA NON GENERERA' IL MOV. PROVVIGIONE
          this.w_ACC_DA_DOC = .F.
          * --- popola la tabella temporanea con mvserrif del documento
          * --- Create temporary table TMP_ACCON
          i_nIdx=cp_AddTableDef('TMP_ACCON') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('GSVE3BGP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMP_ACCON_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Conterr� al termine dell'elaborazione la data scadenza dell'acconto precedente
          this.w_DATSCA = cp_CharToDate("  -  -  ")
          this.w_WARNING = .F.
          this.w_DATOLD = cp_CharToDate("  -  -  ")
          * --- Torna indietro di 10 livelli al massimo per trovare gli acconti precedenti
          *     0 Segnalare all'utente raggiunto limite di iterazioni
          *     >0 tutto ok!.
          this.w_MAXLOOP = 10
          do while this.w_MAXLOOP>0
            this.w_CONTA = .F.
            * --- cicla sui seriali dei documenti d'origine per risalire agli acconti precedenti
            * --- Select from GSVE2BGP
            do vq_exec with 'GSVE2BGP',this,'_Curs_GSVE2BGP','',.f.,.t.
            if used('_Curs_GSVE2BGP')
              select _Curs_GSVE2BGP
              locate for 1=1
              do while not(eof())
              this.w_CONTA = .T.
              this.w_DATMAX = NVL(_Curs_GSVE2BGP.DAT_MA,cp_CharToDate("  -  -    "))
              this.w_DATMIN = NVL(_Curs_GSVE2BGP.DAT_MI,cp_CharToDate("  -  -    "))
              * --- Determino la data scadenza dell'acconto precedente verificando
              *     le date documento dei vari documenti di origine..
              if this.w_DATMAX<>this.w_DATMIN
                * --- Segnalo che devo avvisare per la presenza di acconti in date documenti diversi
                this.w_WARNING = .T.
              else
                if this.w_DATSCA<>this.w_DATMAX And Not Empty( this.w_DATSCA )
                  * --- Segnalo che devo avvisare per la presenza di acconti in date documenti diversi
                  this.w_WARNING = .T.
                endif
              endif
              this.w_DATSCA = Max ( this.w_DATMAX , this.w_DATSCA )
              * --- Mi segnala la presenza di documenti antenati che hanno acconti precedenti.
              this.w_MVACCONT = NVL(_Curs_GSVE2BGP.ACC_ONT,0)
              this.w_ACC_DA_DOC = this.w_ACC_DA_DOC Or this.w_MVACCONT<>0
              this.w_MVACCPRE = NVL(_Curs_GSVE2BGP.ACC_PRE,0)
              if this.w_MVACCPRE=0
                * --- Valorizzo w_MAXLOOOP per uscire dal ciclo while
                this.w_MAXLOOP = -1
              else
                * --- Rigenero TMP_ACCONTI..
                *     Code Painter non gestisce la creazione di una tabella lato server
                *     su se stessa, appoggio i dati su una tabella di comodo..
                * --- Create temporary table TMP_ACCON2
                i_nIdx=cp_AddTableDef('TMP_ACCON2') && aggiunge la definizione nella lista delle tabelle
                i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
                vq_exec('GSVE4BGP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
                this.TMP_ACCON2_idx=i_nIdx
                i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
                * --- Create temporary table TMP_ACCON
                i_nIdx=cp_AddTableDef('TMP_ACCON') && aggiunge la definizione nella lista delle tabelle
                i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
                i_nConn=i_TableProp[this.TMP_ACCON2_idx,3] && recupera la connessione
                i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACCON2_idx,2])
                cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
                      )
                this.TMP_ACCON_idx=i_nIdx
                i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
                * --- Se non trovo niente esco dal ciclo while senza errori...
                this.w_TROVA = .F.
                * --- Select from TMP_ACCON
                i_nConn=i_TableProp[this.TMP_ACCON_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TMP_ACCON_idx,2],.t.,this.TMP_ACCON_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select *  from "+i_cTable+" TMP_ACCON ";
                       ,"_Curs_TMP_ACCON")
                else
                  select * from (i_cTable);
                    into cursor _Curs_TMP_ACCON
                endif
                if used('_Curs_TMP_ACCON')
                  select _Curs_TMP_ACCON
                  locate for 1=1
                  do while not(eof())
                  this.w_TROVA = .T.
                    select _Curs_TMP_ACCON
                    continue
                  enddo
                  use
                endif
                if NOT this.w_TROVA
                  * --- Forzo l'uscita dal loop...
                  this.w_MAXLOOP = -1
                  EXIT
                endif
              endif
              * --- La Select � per costruzione su di un solo record quindi esco
              *     sempre al primo giro....
                select _Curs_GSVE2BGP
                continue
              enddo
              use
            endif
            if Not this.w_CONTA
              * --- Non ho trovato documenti esco dal ciclo while...
              Exit
            else
              this.w_MAXLOOP = this.w_MAXLOOP-1
            endif
          enddo
          if this.w_MAXLOOP=0
            this.w_oERRORLOG.AddMsgLog("Verificare doc.n. %1 del %2", ALLTRIM(STR(this.w_NUMDOC,15))+IIF(EMPTY(this.w_ALFDOC),"","/"+Alltrim(this.w_ALFDOC)), DTOC(this.w_DATDOC))     
            this.w_oERRORLOG.AddMsgLogPartNoTrans(space(20), "Raggiunto limite di iterazioni per determinazione data scadenza acconto precedente")     
            this.w_ERRORE = .T.
          else
            * --- Gestione accorpamento acconti in contabilizzazione.
            *     Se  doc. antenato non ha un acconto (w_ACC_DA_DOC) e w_ACCOLD a
            *     zero oppure w_ACCOLD<>0 significa che l'acconto precedente � in parte o 
            *     completamente derivante da accorpamento acconti in contabilizzazione
            if (this.w_ACCOLD<>0 AND (this.w_ACCPRE -this.w_ACCOLD>0)) Or ( this.w_ACCOLD=0 And Not this.w_ACC_DA_DOC )
              * --- Select from GSVE5BGP
              do vq_exec with 'GSVE5BGP',this,'_Curs_GSVE5BGP','',.f.,.t.
              if used('_Curs_GSVE5BGP')
                select _Curs_GSVE5BGP
                locate for 1=1
                do while not(eof())
                this.w_NURATE = this.w_NURATE + 1
                * --- Se non trovo data scadenza utilizzo la data documento della fattura che determina
                *     il movimento provvigione
                RA[1, this.w_NURATE] = NVL(_Curs_GSVE5BGP.DAT_MI, this.w_DATDOC )
                RA[2, this.w_NURATE] = Nvl( _Curs_GSVE5BGP.IMPORTO , 0 ) 
                this.w_MAXRAT = this.w_MAXRAT + RA[2, this.w_NURATE]
                * --- Decurto l'acconto precedente per ogni acconto accorpato..
                *     ( di fatto potrei creare l'utima rata com w_ACCPRE -w_ACCOLD..)
                this.w_ACCPRE = this.w_ACCPRE - RA[2, this.w_NURATE]
                  select _Curs_GSVE5BGP
                  continue
                enddo
                use
              endif
            endif
            if this.w_ACCPRE<>0
              if this.w_WARNING
                this.w_oERRORLOG.AddMsgLog("Verificare doc.n. %1 del %2", ALLTRIM(STR(this.w_NUMDOC,15))+IIF(EMPTY(this.w_ALFDOC),"","/"+Alltrim(this.w_ALFDOC)), DTOC(this.w_DATDOC))     
                this.w_oERRORLOG.AddMsgLog("Esistono pi� acconti precedenti derivanti da documenti in date diverse:%0Sar� utilizzata la maggiore delle date e pu� non essere possibile la maturazione delle provvigioni")     
              endif
              * --- nuova rata
              this.w_NURATE = this.w_NURATE + 1
              * --- Se non trovo data scadenza utilizzo la data documento della fattura che determina
              *     il movimento provvigione
              RA[1, this.w_NURATE] = IIF( Not Empty ( this.w_DATSCA ) , this.w_DATSCA , this.w_DATDOC )
              RA[2, this.w_NURATE] = this.w_ACCPRE
              this.w_MAXRAT = this.w_MAXRAT + RA[2, this.w_NURATE]
            endif
          endif
        endif
        * --- Select from DOC_RATE
        i_nConn=i_TableProp[this.DOC_RATE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2],.t.,this.DOC_RATE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_RATE ";
              +" where RSSERIAL="+cp_ToStrODBC(this.w_SERIAL)+" AND RSIMPRAT<>0";
              +" order by RSDATRAT";
               ,"_Curs_DOC_RATE")
        else
          select * from (i_cTable);
           where RSSERIAL=this.w_SERIAL AND RSIMPRAT<>0;
           order by RSDATRAT;
            into cursor _Curs_DOC_RATE
        endif
        if used('_Curs_DOC_RATE')
          select _Curs_DOC_RATE
          locate for 1=1
          do while not(eof())
          if this.w_NURATE<99
            if _Curs_DOC_RATE.RSFLPROV <> "S"
              * --- Considero solo le rate non escluse
              this.w_NURATE = this.w_NURATE + 1
              RA[1, this.w_NURATE] = _Curs_DOC_RATE.RSDATRAT
              RA[2, this.w_NURATE] = _Curs_DOC_RATE.RSIMPRAT
            else
              * --- Se ho rate che devono essere escluse, memorizzo il totale delle rate escluse
              *     per poter poi ripartire sulle altre scadenze
              this.w_MAXRATESCL = this.w_MAXRATESCL + _Curs_DOC_RATE.RSIMPRAT
            endif
            this.w_MAXRAT = this.w_MAXRAT + _Curs_DOC_RATE.RSIMPRAT
            * --- ************** DA VERIFICARE SE INSERIRE NELL'ARRAY E QUINDI
            *     NEL CURSORE DETTPROV************************************************************
            this.w_FLSOSP = NVL(_Curs_DOC_RATE.RSFLSOSP, " ")
          endif
            select _Curs_DOC_RATE
            continue
          enddo
          use
        endif
        SELECT DOCUGENE
      endif
      * --- Calcola Imponibile Provvigioni
      this.w_SC1 = NVL(SCONT1, 2)
      this.w_SC2 = NVL(SCONT2, 2)
      this.w_SC3 = NVL(SCONT3, 2)
      this.w_SC4 = NVL(SCONT4, 2)
      * --- Cat.Provvigioni Articolo (di riga)
      this.w_CATART = NVL(CATART, SPACE(5))
      * --- Importo di Riga + Sconti di Piede
      this.w_TOTIMP = NVL(VALMAG, 0)
      this.w_TOTIMP2 = NVL(VALMAG, 0)
      this.w_MVSPEINC = NVL(MVSPEINC,0)
      this.w_MVFLRINC = NVL(MVFLRINC," ")
      this.w_MVSPEIMB = NVL(MVSPEIMB,0)
      this.w_MVFLRIMB = NVL(MVFLRIMB," ")
      this.w_MVSPETRA = NVL(MVSPETRA,0)
      this.w_MVFLRTRA = NVL(MVFLRTRA," ")
      * --- Verifico se ci sono spese ripartite
      this.w_SPERIP = IIF((this.w_MVSPEINC<>0 AND this.w_MVFLRINC="S") OR (this.w_MVSPEIMB<>0 AND this.w_MVFLRIMB="S") OR (this.w_MVSPETRA<>0 AND this.w_MVFLRTRA="S"), .T.,.F.)
      * --- Se esiste uno sconto di piede non devo tenerne conto
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGSCOPAG,AGDTOBSO"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGSCOPAG,AGDTOBSO;
          from (i_cTable) where;
              AGCODAGE = this.w_CODAGE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_AGSCOPAG = NVL(cp_ToDate(_read_.AGSCOPAG),cp_NullValue(_read_.AGSCOPAG))
        this.w_DATOBSO = NVL(cp_ToDate(_read_.AGDTOBSO),cp_NullValue(_read_.AGDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se il flag Sconto Pagamento  non � attivo calcola al lordo
      if this.w_SC7<>0 AND this.w_TOTIMP<>0 AND this.w_AGSCOPAG<>"S" AND this.w_SPERIP AND !this.w_LOG
        this.w_LOG = .T.
        this.w_oERRORLOG.AddMsgLog("Per il doc. n. %1 del %2:", ALLTRIM(STR(this.w_NUMDOC,15))+IIF(EMPTY(this.w_ALFDOC),"","/"+Alltrim(this.w_ALFDOC)), DTOC(this.w_DATDOC))     
        this.w_oERRORLOG.AddMsgLog("Il calcolo delle provvigioni � stato fatto al netto dello sconto pagamento in quanto sono presenti spese ripartite")     
      endif
      if this.w_SC7<>0 AND this.w_TOTIMP<>0 AND this.w_AGSCOPAG<>"S" AND !this.w_SPERIP
        this.w_TOTIMP = (100 * this.w_TOTIMP) / (100 + this.w_SC7)
      endif
      * --- Se l'agente � obsoleto alla data di registrazione sospende la provvigione
      if this.w_DATOBSO <= this.oParentObject.w_DATREG AND NOT EMPTY(this.w_DATOBSO)
        this.w_AGSOSP = "S"
      else
        this.w_AGSOSP = "C"
      endif
      * --- Se il flag tipo provvigioni � suggerito da tabella o da calcolare sbianca le eventuali percentuali
      this.w_ROWNUM = NVL(CPROWNUM,0)
      this.w_MVTIPPRO = NVL(MVTIPPRO,SPACE(2))
      this.w_MVTIPPR2 = NVL(MVTIPPR2,SPACE(2))
      this.w_MVPERPRO = NVL(MVPERPRO,0)
      this.w_MVIMPPRO = NVL(MVIMPPRO,0)
      this.w_MVPROCAP = NVL(MVPROCAP,0)
      this.w_OLDPROCAP = NVL(MVPROCAP,0)
      this.w_MVIMPCAP = NVL(MVIMPCAP,0)
      this.w_MVTCOLIS = NVL(MVTCOLIS,SPACE(5))
      * --- Se non � presente il listino di riferimento nei parametri provvigioni prende quello della testata documenti
      this.w_LISRIFSCO = IIF(NOT EMPTY(NVL(g_LISRIF,SPACE(5))), g_LISRIF, this.w_MVTCOLIS)
      this.w_MVCODART = NVL(MVCODART,SPACE(20))
      this.w_MVDATREG = NVL(MVDATREG, cp_CharToDate("  -  -  "))
      this.w_MVQTAMOV = NVL(MVQTAMOV,0)
      this.w_MVCODICE = NVL(MVCODICE,SPACE(20))
      this.w_MVUNIMIS = NVL(MVUNIMIS,SPACE(3))
      this.w_MVQTAUM1 = NVL(MVQTAUM1,0)
      this.w_MVFLSCOR = NVL(MVFLSCOR," ")
      this.w_MVCODIVA = NVL(MVCODIVA,SPACE(5))
      this.w_MVPREZZO = NVL(MVPREZZO,0)
      this.w_MVFLOMAG = NVL(MVFLOMAG,"")
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_MVCODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      DECLARE pARRPROV (21)
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_MVTIPPRO = pArrprov(1)
      this.w_MVPERPRO = pArrprov(2)
      this.w_MVIMPPRO = IIF(this.w_MVTIPPRO<>"FO", pArrprov(20),this.w_MVIMPPRO)
      this.w_MVTIPPR2 = pArrprov(3)
      if NOT EMPTY(this.w_CODAG2)
        this.w_MVPROCAP = pArrprov(4)
        this.w_MVIMPCAP = IIF(this.w_MVTIPPR2<>"FO" ,pArrprov(21),this.w_MVIMPCAP)
      else
        this.w_MVPROCAP = 0
        this.w_MVIMPCAP = 0
      endif
      this.w_RIGHETOT = this.w_RIGHETOT +1
      if this.w_MVTIPPRO="ET" 
        this.w_ESCLUSA = this.w_ESCLUSA + 1
      else
        this.w_PERAGE = this.w_MVPERPRO
        this.w_TOTAGE = this.w_MVIMPPRO
        this.w_PERCAP = this.w_MVPROCAP
        this.w_TOTZON = this.w_MVIMPCAP
        * --- Se pregresso al campo MVPROCAP, lascio la percentuale 999.9 sulle righe
        this.w_MVPROCAP = iif(this.w_OLDPROCAP=999.9, this.w_OLDPROCAP, this.w_MVPROCAP)
        this.w_MVIMPCAP = IIF(this.w_OLDPROCAP=999.9, 0, this.w_MVIMPCAP)
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVTIPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPRO),'DOC_DETT','MVTIPPRO');
          +",MVIMPPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPPRO),'DOC_DETT','MVIMPPRO');
          +",MVPERPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVPERPRO),'DOC_DETT','MVPERPRO');
          +",MVTIPPR2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPPR2),'DOC_DETT','MVTIPPR2');
          +",MVIMPCAP ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPCAP),'DOC_DETT','MVIMPCAP');
          +",MVPROCAP ="+cp_NullLink(cp_ToStrODBC(this.w_MVPROCAP),'DOC_DETT','MVPROCAP');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                 )
        else
          update (i_cTable) set;
              MVTIPPRO = this.w_MVTIPPRO;
              ,MVIMPPRO = this.w_MVIMPPRO;
              ,MVPERPRO = this.w_MVPERPRO;
              ,MVTIPPR2 = this.w_MVTIPPR2;
              ,MVIMPCAP = this.w_MVIMPCAP;
              ,MVPROCAP = this.w_MVPROCAP;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERIAL;
              and CPROWNUM = this.w_ROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_PPA = 0
        this.w_PPC = 0
        this.Primo = .T.
        if (this.w_PERAGE=0 OR NOT EMPTY(this.w_CODAG2)) AND NOT EMPTY(this.w_AGEPRO)
          * --- Ricerca dell'agente nel caso che non sia presente il cliente o l'articolo
          * --- Controllo se all'interno del documento sono presenti articoli che non devono avere provvigioni
          if this.Primo=.T.
            * --- Read from PRO_AGEN
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.PRO_AGEN_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRO_AGEN_idx,2],.t.,this.PRO_AGEN_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PVCHIAVE"+;
                " from "+i_cTable+" PRO_AGEN where ";
                    +"PVCATAGE = "+cp_ToStrODBC(this.w_CATAGE);
                    +" and PVCHIAVE = "+cp_ToStrODBC(this.w_CHIAVE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PVCHIAVE;
                from (i_cTable) where;
                    PVCATAGE = this.w_CATAGE;
                    and PVCHIAVE = this.w_CHIAVE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CHIAVE1 = NVL(cp_ToDate(_read_.PVCHIAVE),cp_NullValue(_read_.PVCHIAVE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if NOT EMPTY(NVL(this.w_CHIAVE1,""))
              this.Primo = .F.
              this.w_PPA = 0
              this.w_PPC = 0
            endif
          endif
          SELECT DOCUGENE
        endif
        * --- Importi suddivisi per Tipo Maturazione 1=FA; 2=SC; 3=IM
        * --- Agente
        IMP[1] = (this.w_TOTIMP * this.w_GRUFAT) / 100
        IMP[2] = (this.w_TOTIMP * this.w_GRUSCA) / 100
        IMP[3] =(this.w_TOTIMP * this.w_GRUINC) / 100
        this.w_APPO = IMP[1] + IMP[2] + IMP[3]
        FOR L_i = 1 TO 3
        if IMP[L_i]<>0
          IMP[L_i] = IMP[L_i] + (this.w_TOTIMP - this.w_APPO)
          EXIT
        endif
        ENDFOR
        PRO[1] = (this.w_TOTAGE * this.w_GRUFAT) / 100
        PRO[2] = (this.w_TOTAGE * this.w_GRUSCA) / 100
        PRO[3] = (this.w_TOTAGE * this.w_GRUINC) / 100
        this.w_APPO = PRO[1] + PRO[2] + PRO[3]
        FOR L_i = 1 TO 3
        if PRO[L_i]<>0
          PRO[L_i] = PRO[L_i] + (this.w_TOTAGE - this.w_APPO)
          EXIT
        endif
        ENDFOR
        * --- CApoarea
        IMPC[1] = (this.w_TOTIMP2 * this.w_GRUFAT1) / 100
        IMPC[2] = (this.w_TOTIMP2 * this.w_GRUSCA1) / 100
        IMPC[3] = (this.w_TOTIMP2 * this.w_GRUINC1) / 100
        this.w_APP1 = IMPC[1] + IMPC[2] + IMPC[3]
        FOR L_i = 1 TO 3
        if IMPC[L_i]<>0
          IMPC[L_i] = IMPC[L_i] + (this.w_TOTIMP - this.w_APP1)
          EXIT
        endif
        ENDFOR
        PRC[1] = (this.w_TOTZON * this.w_GRUFAT1) / 100
        PRC[2] = (this.w_TOTZON * this.w_GRUSCA1) / 100
        PRC[3] = (this.w_TOTZON * this.w_GRUINC1) / 100
        this.w_APP1 = PRC[1] + PRC[2] + PRC[3]
        FOR L_i = 1 TO 3
        if PRC[L_i]<>0
          PRC[L_i] = PRC[L_i] + (this.w_TOTZON - this.w_APP1)
          EXIT
        endif
        ENDFOR
        * --- % ed importo Provvigioni Agente e Capo Area
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        SELECT DOCUGENE
      endif
      ENDSCAN
      * --- Testa l'Ultima Uscita
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_NUDOC=0 OR (this.w_NUDOC>0 AND this.w_OKDOC=0 AND this.w_RIGHETOT= this.w_ESCLUSA)
        this.w_APPO1 = "Per l'intervallo selezionato non esistono documenti da generare"
      else
        this.w_APPO1 = "Operazione completata%0N. %1 documenti generati%0su %2 documenti da generare"
      endif
      ah_ErrorMsg(this.w_APPO1,,"", ALLTRIM(STR(this.w_OKDOC)), ALLTRIM(STR(this.w_NUDOC)))
      * --- LOG Errori
      this.w_oERRORLOG.PrintLog(this.oParentObject,"Avvisi/errori")     
      * --- Al termine Azzera il Cursore
      SELECT DOCUGENE
      USE
    else
      this.w_APPO1 = "Per l'intervallo selezionato non esistono documenti da generare"
      ah_ErrorMsg(this.w_APPO1)
    endif
    * --- Azzera Tutti i Cursori
    if USED("DettProv")
      SELECT DettProv
      USE
    endif
    if USED("MessErr")
      SELECT MessErr
      USE
    endif
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    if USED("AppCur")
      SELECT AppCur
      USE
    endif
    if USED("AGETOT")
      SELECT AGETOT
      USE
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Provvigioni Agente
    * --- Se non e' il Primo Ingresso, Scrive la Nuova Registrazione
    this.w_RESTO = 0
    this.w_RESTAGE = 0
    this.w_RESTCAP = 0
    this.w_REST2 = 0
    this.w_APP2 = 0
    this.w_APP2R = 0
    if this.w_OSERIAL <> REPL("#", 10)
      this.w_NUDOC = this.w_NUDOC + 1
      if not this.w_ERRORE AND RECCOUNT("DettProv") > 0
        * --- Chiudo il cursore poich� dopo viene reso scrivibile altrimenti ho un errore di alias
        *     gi� utilizzato
        if USED("AppCur")
          SELECT AppCur
          USE
        endif
        SELECT DATSCA, PERPRA, PERPRC, TIPMAT, cp_ROUND(SUM(TOTIMP),this.w_ARRO) AS TOTIMP,; 
 cp_ROUND(SUM(TOTAGE),this.w_ARRO) AS TOTAGE, cp_ROUND(SUM(TOTZON),this.w_ARRO) AS TOTZON,cp_ROUND(SUM(TOTIMP2),this.w_ARRO) AS TOTIMP2 ;
        FROM DettProv INTO CURSOR AppCur ;
        GROUP BY DATSCA, PERPRA, PERPRC, TIPMAT ORDER BY DATSCA, PERPRA DESC
        * --- Elaboro il cursore AppCur per effettuare gli arrotondamenti
        * --- Calcolo i totali arrotondati ai decimali della valuta e quelli arrotondati a 5 decimali
        SELECT DettProv
        CALCULATE SUM(TOTIMP), SUM(TOTAGE), SUM(TOTZON), SUM(TOTIMP2) TO this.w_APPO, this.w_APPA, this.w_APPC, this.w_APP2
        SELECT AppCur
        CALCULATE SUM(TOTIMP), SUM(TOTAGE), SUM(TOTZON), SUM(TOTIMP2) TO this.w_APPOR, this.w_APPAR, this.w_APPCR, this.w_APP2R
        * --- Rendo scrivibile il cursore
        WRCURSOR("APPCUR")
        * --- Se i totali sono divesi riporto la differenza sulla prima rata
        if this.w_APPOR<>this.w_APPO 
          this.w_RESTO = this.w_RESTO + (this.w_APPO - this.w_APPOR) 
        endif
        * --- Dovrei ricalcolare le percentuali ma, a questo punto non ho pi� le informazioni
        *     sulle varie percentuali dato che sto lavorando con i totali, quindi non posso farlo
        if this.w_APPAR<>this.w_APPA
          this.w_RESTAGE = this.w_RESTAGE + (this.w_APPA - this.w_APPAR)
        endif
        if this.w_APPCR<>this.w_APPC 
          this.w_RESTCAP = this.w_RESTCAP + (this.w_APPC - this.w_APPCR)
        endif
        if this.w_APP2R<>this.w_APP2 
          this.w_REST2 = this.w_REST2 + (this.w_APP2 - this.w_APP2R) 
        endif
        * --- Calcola MPSERIAL, MPNUMREG
        this.w_MPSERIAL = SPACE(10)
        this.w_MPNUMREG = 0
        this.w_MPCODESE = STR(YEAR(this.oParentObject.w_DATREG), 4,0)
        this.w_CPROWNUM = 0
        * --- Try
        local bErr_03CB7FF0
        bErr_03CB7FF0=bTrsErr
        this.Try_03CB7FF0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Errore non Compreso nei Controlli
          this.w_oERRORLOG.AddMsgLog("Verificare doc.n. %1 del %2", ALLTRIM(STR(this.w_NUMDOC,15))+IIF(EMPTY(this.w_ALFDOC),"","/"+Alltrim(this.w_ALFDOC)), DTOC(this.w_DATDOC))     
          this.w_oERRORLOG.AddMsgLogNoTranslate(space(50))     
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_03CB7FF0
        * --- End
      else
        if not this.w_ERRORE AND RECCOUNT("DettProv") =0
          * --- Storna il numero di documenti da generare nel caso in cui il doc. sia fatto di righe escluse da tabella (nel caso in cui vi siano errori gestiti w_ERRORE sarebbe a .T.)
          this.w_NUDOC = this.w_NUDOC - 1
        endif
      endif
    endif
    * --- Azzera il Temporaneo di Appoggio
    CREATE CURSOR DettProv ;
    (DATSCA D(8), TOTIMP N(18,4), PERPRA N(5,2), PERPRC N(5,2), TOTAGE N(18,4), TOTZON N(18,4), TIPMAT C(2),TOTIMP2 N(18,4))
  endproc
  proc Try_03CB7FF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.MOP_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEMOP", "i_codazi,w_MPSERIAL")
    cp_NextTableProg(this, i_Conn, "PRMOP", "i_codazi,w_MPCODESE,w_MPNUMREG")
    * --- Scrive il Master
    ah_Msg("Scrivo reg. n.: %1",.T.,.F.,.F., STR(this.w_MPNUMREG,6,0))
    * --- Insert into MOP_MAST
    i_nConn=i_TableProp[this.MOP_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOP_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOP_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MPSERIAL"+",MPCODESE"+",MPNUMREG"+",MPDATREG"+",MPNUMDOC"+",MPALFDOC"+",MPDATDOC"+",MPVALNAZ"+",MPCODVAL"+",MPCAOVAL"+",MPTIPCON"+",MPCODCON"+",MPDESSUP"+",MPCODAGE"+",MPCODCAP"+",MPRIFDOC"+",MPAGSOSP"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MPSERIAL),'MOP_MAST','MPSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MPCODESE),'MOP_MAST','MPCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MPNUMREG),'MOP_MAST','MPNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATREG),'MOP_MAST','MPDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMDOC),'MOP_MAST','MPNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ALFDOC),'MOP_MAST','MPALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATDOC),'MOP_MAST','MPDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_VALNAZ),'MOP_MAST','MPVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'MOP_MAST','MPCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAOVAL),'MOP_MAST','MPCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'MOP_MAST','MPTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'MOP_MAST','MPCODCON');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(50)),'MOP_MAST','MPDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODAGE),'MOP_MAST','MPCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODAG2),'MOP_MAST','MPCODCAP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'MOP_MAST','MPRIFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AGSOSP),'MOP_MAST','MPAGSOSP');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MPSERIAL',this.w_MPSERIAL,'MPCODESE',this.w_MPCODESE,'MPNUMREG',this.w_MPNUMREG,'MPDATREG',this.oParentObject.w_DATREG,'MPNUMDOC',this.w_NUMDOC,'MPALFDOC',this.w_ALFDOC,'MPDATDOC',this.w_DATDOC,'MPVALNAZ',this.w_VALNAZ,'MPCODVAL',this.w_CODVAL,'MPCAOVAL',this.w_CAOVAL,'MPTIPCON',this.w_TIPCON,'MPCODCON',this.w_CODCON)
      insert into (i_cTable) (MPSERIAL,MPCODESE,MPNUMREG,MPDATREG,MPNUMDOC,MPALFDOC,MPDATDOC,MPVALNAZ,MPCODVAL,MPCAOVAL,MPTIPCON,MPCODCON,MPDESSUP,MPCODAGE,MPCODCAP,MPRIFDOC,MPAGSOSP &i_ccchkf. );
         values (;
           this.w_MPSERIAL;
           ,this.w_MPCODESE;
           ,this.w_MPNUMREG;
           ,this.oParentObject.w_DATREG;
           ,this.w_NUMDOC;
           ,this.w_ALFDOC;
           ,this.w_DATDOC;
           ,this.w_VALNAZ;
           ,this.w_CODVAL;
           ,this.w_CAOVAL;
           ,this.w_TIPCON;
           ,this.w_CODCON;
           ,SPACE(50);
           ,this.w_CODAGE;
           ,this.w_CODAG2;
           ,this.w_SERIAL;
           ,this.w_AGSOSP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Scrive il Detail
    this.w_GIAAGGA = .F.
    this.w_GIAAGGC = .F.
    SELECT AppCur
    GO TOP
    SCAN
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    if !this.w_GIAAGGA AND NVL(TOTIMP, 0)<>0
      this.w_GIAAGGA = .T.
      * --- Prima riga agente
      this.w_TOTIMP = cp_ROUND(NVL(TOTIMP, 0) + this.w_RESTO, this.w_ARRO)
      this.w_TOTAGE = cp_ROUND(NVL(TOTAGE, 0) + this.w_RESTAGE, this.w_ARRO)
    else
      this.w_TOTIMP = cp_ROUND(NVL(TOTIMP, 0), this.w_ARRO)
      this.w_TOTAGE = cp_ROUND(NVL(TOTAGE, 0), this.w_ARRO)
    endif
    if !this.w_GIAAGGC AND NVL(TOTIMP2, 0)<>0
      * --- Prima riga capoarea
      this.w_GIAAGGC = .T.
      this.w_TOTIMP2 = cp_ROUND(NVL(TOTIMP2, 0) + this.w_REST2, this.w_ARRO)
      this.w_TOTZON = cp_ROUND(NVL(TOTZON, 0) + this.w_RESTCAP, this.w_ARRO)
    else
      this.w_TOTIMP2 = cp_ROUND(NVL(TOTIMP2, 0), this.w_ARRO)
      this.w_TOTZON = cp_ROUND(NVL(TOTZON, 0), this.w_ARRO)
    endif
    this.w_PERPRA = NVL(PERPRA, 0)
    this.w_PERPRC = NVL(PERPRC, 0)
    this.w_TIPMAT = NVL(TIPMAT, "  ")
    this.w_DATSCA = NVL(DATSCA, cp_CharToDate("  -  -  "))
    this.w_DATMAT = IIF(this.w_TIPMAT="IN", cp_CharToDate("  -  -  "), this.w_DATSCA)
    * --- Insert into MOP_DETT
    i_nConn=i_TableProp[this.MOP_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOP_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.MOP_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MPSERIAL"+",CPROWNUM"+",MPDATSCA"+",MPDATMAT"+",MPTOTIMP"+",MPPERPRA"+",MPPERPRC"+",MPTOTAGE"+",MPTOTZON"+",MPTIPMAT"+",MPFLSOSP"+",MPDATLIQ"+",MPTOTIM2"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MPSERIAL),'MOP_DETT','MPSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'MOP_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATSCA),'MOP_DETT','MPDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATMAT),'MOP_DETT','MPDATMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTIMP),'MOP_DETT','MPTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PERPRA),'MOP_DETT','MPPERPRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PERPRC),'MOP_DETT','MPPERPRC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTAGE),'MOP_DETT','MPTOTAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTZON),'MOP_DETT','MPTOTZON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPMAT),'MOP_DETT','MPTIPMAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLSOSP),'MOP_DETT','MPFLSOSP');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'MOP_DETT','MPDATLIQ');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TOTIMP2),'MOP_DETT','MPTOTIM2');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MPSERIAL',this.w_MPSERIAL,'CPROWNUM',this.w_CPROWNUM,'MPDATSCA',this.w_DATSCA,'MPDATMAT',this.w_DATMAT,'MPTOTIMP',this.w_TOTIMP,'MPPERPRA',this.w_PERPRA,'MPPERPRC',this.w_PERPRC,'MPTOTAGE',this.w_TOTAGE,'MPTOTZON',this.w_TOTZON,'MPTIPMAT',this.w_TIPMAT,'MPFLSOSP',this.w_FLSOSP,'MPDATLIQ',cp_CharToDate("  -  -  "))
      insert into (i_cTable) (MPSERIAL,CPROWNUM,MPDATSCA,MPDATMAT,MPTOTIMP,MPPERPRA,MPPERPRC,MPTOTAGE,MPTOTZON,MPTIPMAT,MPFLSOSP,MPDATLIQ,MPTOTIM2 &i_ccchkf. );
         values (;
           this.w_MPSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_DATSCA;
           ,this.w_DATMAT;
           ,this.w_TOTIMP;
           ,this.w_PERPRA;
           ,this.w_PERPRC;
           ,this.w_TOTAGE;
           ,this.w_TOTZON;
           ,this.w_TIPMAT;
           ,this.w_FLSOSP;
           ,cp_CharToDate("  -  -  ");
           ,this.w_TOTIMP2;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    SELECT AppCur
    ENDSCAN
    * --- Scrive Flag Contabilizzato sul Documento Origine
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVGENPRO ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_MAST','MVGENPRO');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      update (i_cTable) set;
          MVGENPRO = "S";
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    this.w_OKDOC = this.w_OKDOC + 1
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola % ed importo Provvigioni Agente e Capo Area
    FOR L_i = 1 TO 3
    this.w_PERPRA = 0
    this.w_PERPRC = 0
    do case
      case IMP[L_i]<>0 AND this.w_PERAGE<>0
        * --- Se esiste una % Provvigioni Calcola L'Importo
        this.w_PERPRA = this.w_PERAGE
        PRO[L_i] = cp_ROUND(IMP[L_i] * (this.w_PERPRA/100), 5)
      case IMP[L_i]<>0 AND PRO[L_i]<>0
        * --- Se esiste un Importo Provvigioni Calcola Indirettamente la %
        * --- i Decimali della % sono volutamente aumentati per ridurre gli scarti in caso di ulteriori ripartizioni rateali (successivamente sara riportato a 2 dec.)
        this.w_PERPRA = cp_ROUND((PRO[L_i] * 100) / IMP[L_i], 5)
        * --- Le Provvigioni sul Capoarea sono comunque Calcolate dalla Tabella
      case IMP[L_i]<>0
        * --- Provvigioni Calcolate da Tabella (se Esiste)
        if NOT EMPTY(this.w_AGEPRO)
          if this.w_PERAGE=0
            if not this.w_ERRORE
              * --- Manca Tabella Provvigioni Agente
              this.w_oERRORLOG.AddMsgLog("Verificare doc.n. %1 del %2", ALLTRIM(STR(this.w_NUMDOC,15))+IIF(EMPTY(this.w_ALFDOC),"","/"+Alltrim(this.w_ALFDOC)), DTOC(this.w_DATDOC))     
              this.w_oERRORLOG.AddMsgLogPartNoTrans(space(20), "Manca tabella provvigioni (agente: %1|Cli: %2|Art: %3)",this.w_AGEPRO, this.w_CATCLI, this.w_CATART)     
              this.w_ERRORE = .T.
            endif
          else
            * --- Calcola Le Provvigioni
            this.w_PERPRA = this.w_PERAGE
            PRO[L_i] = cp_ROUND((IMP[L_i] * this.w_PERPRA) / 100, 5)
          endif
        else
          if not this.w_ERRORE
            * --- Manca Categoria Provvigioni Agente
            this.w_oERRORLOG.AddMsgLog("Verificare doc.n. %1 del %2", ALLTRIM(STR(this.w_NUMDOC,15))+IIF(EMPTY(this.w_ALFDOC),"","/"+Alltrim(this.w_ALFDOC)), DTOC(this.w_DATDOC))     
            this.w_oERRORLOG.AddMsgLogPartNoTrans(space(20), "Manca categoria provvigioni agente")     
            this.w_ERRORE = .T.
          endif
        endif
    endcase
    * --- Capoarea
    do case
      case IMPC[L_i]<>0 AND this.w_PERCAP<>0
        * --- Se esiste una % Provvigioni Calcola L'Importo
        this.w_PERPRC = this.w_PERCAP
        PRC[L_i] = cp_ROUND(IMPC[L_i] * (this.w_PERCAP/100), 5)
      case IMPC[L_i]<>0 AND PRC[L_i]<>0
        * --- Se esiste un Importo Provvigioni Calcola Indirettamente la %
        * --- i Decimali della % sono volutamente aumentati per ridurre gli scarti in caso di ulteriori ripartizioni rateali (successivamente sara riportato a 2 dec.)
        this.w_PERPRC = cp_ROUND((PRC[L_i] * 100) / IMPC[L_i], 5)
      case IMPC[L_i]<>0
        * --- non d�errore perch� il capoarea pu� anche non esserci
    endcase
    * --- Aggiorna Temporaneo Elaborazione Provvigioni
    if not this.w_ERRORE AND IMP[L_i]<>0 AND PRO[L_i]<>0
      this.w_APPO1 = IIF(L_i = 1, "FA", IIF(L_i = 2, "SC", "IN"))
      if L_i = 1 OR this.w_NURATE=0
        * --- Maturazione a data Fattura o no Scadenze
        this.w_PERPRA = cp_ROUND(this.w_PERPRA, 2)
        INSERT INTO DettProv (DATSCA, TOTIMP, PERPRA, PERPRC, TOTAGE, TOTZON, TIPMAT,TOTIMP2) ;
        VALUES (this.w_DATDOC, IMP[L_i], this.w_PERPRA, 0, PRO[L_i], 0, this.w_APPO1,0)
      else
        * --- Maturazione a data Scadenza o Incasso e rate
        this.w_APPO = 0
        this.w_APPA = 0
        this.w_APPC = 0
        FOR L_r = 1 TO this.w_NURATE
        * --- Ripartisce l'Imponibile sulle Varie Rate Scadenze
        if this.w_MAXRATESCL <> 0 
          RA[3, L_r] = cp_ROUND( ( ( RA[2, L_r] + ( this.w_MAXRATESCL * RA[2, L_r] ) / ( this.w_MAXRAT- this.w_MAXRATESCL ) ) * IMP[L_i]) / this.w_MAXRAT, 5)
        else
          RA[3, L_r] = cp_ROUND((RA[2, L_r] * IMP[L_i]) / this.w_MAXRAT, 5)
        endif
        this.w_APPO = this.w_APPO + RA[3, L_r]
        * --- Di Conseguenza ricalcola Le provvigioni Agente e Capo Area
        RA[4, L_r] = cp_ROUND((RA[3,L_r] * this.w_PERPRA) / 100, 5)
        this.w_APPA = this.w_APPA + RA[4, L_r]
        RA[5, L_r] = cp_ROUND((RA[3,L_r] * this.w_PERPRC) / 100, 5)
        this.w_APPC = this.w_APPC + RA[5, L_r]
        ENDFOR
        * --- Ripartisce l'Eventuale resto sulla Prima Rata
        if IMP[L_i]<>this.w_APPO
          RA[3, 1] = cp_ROUND(RA[3, 1] + (IMP[L_i] - this.w_APPO) , 5)
        endif
        * --- Lo stesso Vale per Gli Importi Provvigioni Calcolati...
        this.w_APPO = cp_ROUND((IMP[L_i] * this.w_PERPRA) / 100, 5)
        * --- Se provvigioni inserite direttamente sulla Riga, confronta il resto con queste ultime
        this.w_APPA = IIF(PRO[L_i]<>0, PRO[L_i], this.w_APPA)
        if this.w_APPO<>this.w_APPA
          RA[4, 1] = cp_ROUND(RA[4, 1] + (this.w_APPO - this.w_APPA) ,5)
        endif
        this.w_APPO = cp_ROUND((IMP[L_i] * this.w_PERPRC) / 100, 5)
        if this.w_APPO<>this.w_APPC
          RA[5, 1] = cp_ROUND(RA[5, 1] + (this.w_APPO - this.w_APPC) , 5)
        endif
        this.w_PERPRA = cp_ROUND(this.w_PERPRA, 2)
        FOR L_r = 1 TO this.w_NURATE
        INSERT INTO DettProv (DATSCA, TOTIMP, PERPRA, PERPRC, TOTAGE, TOTZON, TIPMAT,TOTIMP2) ;
        VALUES (RA[1, L_r], RA[3, L_r], this.w_PERPRA, 0, RA[4, L_r], 0, this.w_APPO1,0)
        ENDFOR
      endif
    endif
    if not this.w_ERRORE AND IMPC[L_i]<>0 AND PRC[L_i]<>0
      this.w_APPO1 = IIF(L_i = 1, "FA", IIF(L_i = 2, "SC", "IN"))
      if L_i = 1 OR this.w_NURATE=0
        * --- Maturazione a data Fattura o no Scadenze
        this.w_PERPRC = cp_ROUND(this.w_PERPRC, 2)
        INSERT INTO DettProv (DATSCA, TOTIMP, PERPRA, PERPRC, TOTAGE, TOTZON, TIPMAT, TOTIMP2) ;
        VALUES (this.w_DATDOC, 0, 0, this.w_PERPRC, 0, PRC[L_i], this.w_APPO1, IMPC[L_i] )
      else
        * --- Maturazione a data Scadenza o Incasso e rate
        this.w_APPO = 0
        this.w_APPA = 0
        this.w_APPC = 0
        FOR L_r = 1 TO this.w_NURATE
        * --- Ripartisce l'Imponibile sulle Varie Rate Scadenze
        if this.w_MAXRATESCL <> 0
          RA[3, L_r] =cp_ROUND( ( ( RA[2, L_r] + ( this.w_MAXRATESCL * RA[2, L_r] ) / ( this.w_MAXRAT- this.w_MAXRATESCL ) ) * IMPC[L_i]) / this.w_MAXRAT, 5)
        else
          RA[3, L_r] = cp_ROUND((RA[2, L_r] * IMPC[L_i]) / this.w_MAXRAT, 5)
        endif
        this.w_APPO = this.w_APPO + RA[3, L_r]
        * --- Di Conseguenza ricalcola Le provvigioni Agente e Capo Area
        RA[4, L_r] = cp_ROUND((RA[3,L_r] * this.w_PERPRA) / 100, 5)
        this.w_APPA = this.w_APPA + RA[4, L_r]
        RA[5, L_r] = cp_ROUND((RA[3,L_r] * this.w_PERPRC) / 100, 5)
        this.w_APPC = this.w_APPC + RA[5, L_r]
        ENDFOR
        * --- Ripartisce l'Eventuale resto sulla Prima Rata
        if IMPC[L_i]<>this.w_APPO
          RA[3, 1] = cp_ROUND(RA[3, 1] + (IMPC[L_i] - this.w_APPO) , 5)
        endif
        * --- Lo stesso Vale per Gli Importi Provvigioni Calcolati...
        this.w_APPO = cp_ROUND((IMP[L_i] * this.w_PERPRA) / 100, 5)
        * --- Se provvigioni inserite direttamente sulla Riga, confronta il resto con queste ultime
        this.w_APPA = IIF(PRO[L_i]<>0, PRO[L_i], this.w_APPA)
        if this.w_APPO<>this.w_APPA
          RA[4, 1] = cp_ROUND(RA[4, 1] + (this.w_APPO - this.w_APPA) ,5)
        endif
        this.w_APPO = cp_ROUND((IMPC[L_i] * this.w_PERPRC) / 100, 5)
        if this.w_APPO<>this.w_APPC
          RA[5, 1] = cp_ROUND(RA[5, 1] + (this.w_APPO - this.w_APPC) , 5)
        endif
        this.w_PERPRA = cp_ROUND(this.w_PERPRA, 2)
        FOR L_r = 1 TO this.w_NURATE
        INSERT INTO DettProv (DATSCA, TOTIMP, PERPRA, PERPRC, TOTAGE, TOTZON, TIPMAT, TOTIMP2) ;
        VALUES (RA[1, L_r], 0, 0, this.w_PERPRC, 0, RA[5, L_r], this.w_APPO1,RA[3, L_r])
        ENDFOR
      endif
    endif
    ENDFOR
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definisce le Variabili di Work
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_PARAM = "CCG"
    pArrprov [1] = this.w_mvtippro 
 pArrprov [2] = this.w_mvperpro 
 pArrprov [3] = this.w_mvtippr2 
 pArrprov [4] =this.w_mvprocap 
 pArrprov [5] = this.w_catcli 
 pArrprov [6] = this.w_catscc 
 pArrprov [7] = this.w_agepro 
 pArrprov [8] = this.w_agscopag 
 pArrprov [9] = this.w_catart 
 pArrprov [10] = this.w_mvflomag 
 pArrprov [11] = this.w_arro 
 pArrprov [12] = this.w_sc1 
 pArrprov [13] = this.w_sc2 
 pArrprov [14] = this.w_sc3 
 pArrprov [15] = this.w_sc4 
 pArrprov [16] = this.w_sc5 
 pArrprov [17] = this.w_sc6 
 pArrprov [18] = this.w_sc7 
 pArrprov [19] = this.w_totimp 
 pArrprov [20] = 0 
 pArrprov [21] = 0 
 
    DIMENSION ARRPROV2[11]
    Arrprov2[ 1 ] = this.w_CODVAL 
 Arrprov2[ 2 ] = this.w_MVCODART 
 Arrprov2[ 3 ] = this.w_LISRIFSCO 
 Arrprov2[ 4 ] = this.w_MVDATREG 
 Arrprov2[ 5 ] = this.w_MVQTAMOV 
 Arrprov2[ 6 ] = this.w_MVCODICE 
 Arrprov2[ 7 ] = this.w_MVUNIMIS 
 Arrprov2[ 8 ] = this.w_MVQTAUM1 
 Arrprov2[ 9 ] = this.w_MVFLSCOR 
 Arrprov2[ 10 ] = this.w_MVPREZZO 
 Arrprov2[ 11 ] = this.w_PERIVA
    this.w_ARRPROV = CAL_PROV(this.w_PARAM,@pArrprov,@Arrprov2)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,16)]
    this.cWorkTables[1]='TAB_PROV'
    this.cWorkTables[2]='DOC_RATE'
    this.cWorkTables[3]='MOP_MAST'
    this.cWorkTables[4]='MOP_DETT'
    this.cWorkTables[5]='DOC_MAST'
    this.cWorkTables[6]='PRO_AGEN'
    this.cWorkTables[7]='AGENTI'
    this.cWorkTables[8]='PAR_PROV'
    this.cWorkTables[9]='DOC_DETT'
    this.cWorkTables[10]='ART_ICOL'
    this.cWorkTables[11]='KEY_ARTI'
    this.cWorkTables[12]='LISTINI'
    this.cWorkTables[13]='VOCIIVA'
    this.cWorkTables[14]='VALUTE'
    this.cWorkTables[15]='*TMP_ACCON'
    this.cWorkTables[16]='*TMP_ACCON2'
    return(this.OpenAllTables(16))

  proc CloseCursors()
    if used('_Curs_GSVE2BGP')
      use in _Curs_GSVE2BGP
    endif
    if used('_Curs_TMP_ACCON')
      use in _Curs_TMP_ACCON
    endif
    if used('_Curs_GSVE5BGP')
      use in _Curs_GSVE5BGP
    endif
    if used('_Curs_DOC_RATE')
      use in _Curs_DOC_RATE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
