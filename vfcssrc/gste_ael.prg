* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_ael                                                        *
*              Elementi                                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-30                                                      *
* Last revis.: 2015-03-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_ael"))

* --- Class definition
define class tgste_ael as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 737
  Height = 394+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-16"
  HelpContextID=192269207
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=74

  * --- Constant Properties
  VAELEMEN_IDX = 0
  VASTRUTT_IDX = 0
  VAFORMAT_IDX = 0
  ENT_DETT_IDX = 0
  XDC_FIELDS_IDX = 0
  PROMCLAS_IDX = 0
  cFile = "VAELEMEN"
  cKeySelect = "ELCODSTR,ELCODICE"
  cKeyWhere  = "ELCODSTR=this.w_ELCODSTR and ELCODICE=this.w_ELCODICE"
  cKeyWhereODBC = '"ELCODSTR="+cp_ToStrODBC(this.w_ELCODSTR)';
      +'+" and ELCODICE="+cp_ToStrODBC(this.w_ELCODICE)';

  cKeyWhereODBCqualified = '"VAELEMEN.ELCODSTR="+cp_ToStrODBC(this.w_ELCODSTR)';
      +'+" and VAELEMEN.ELCODICE="+cp_ToStrODBC(this.w_ELCODICE)';

  cPrg = "gste_ael"
  cComment = "Elementi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_ELCODSTR = space(10)
  w_ELCODICE = space(20)
  w_TIPFIL = space(1)
  w_STFLTYPE = space(1)
  w_ROOT = space(20)
  w_ELDESCRI = space(50)
  w_ELORDINE = 0
  w_ELMODELE = space(1)
  w_ELIDCHLD = 0
  w_EL__TIPO = space(1)
  o_EL__TIPO = space(1)
  w_ELCODPAD = space(20)
  w_ELCODFOR = space(20)
  w_ELLUNMAX = 0
  w_LUNTAG = 0
  w_DECIMAL = 0
  w_TIPO = space(1)
  w_ELCODENT = space(15)
  w_DESSTR = space(30)
  w_DESPAD = space(50)
  w_DESFOR = space(30)
  w_FLPROV = space(1)
  w_MESS = space(10)
  w_FLATAB = space(15)
  w_ELVALTXT = space(50)
  o_ELVALTXT = space(50)
  w_ELINISEZ = space(1)
  w_ELORDASS = 0
  w_ELVALTX2 = space(50)
  o_ELVALTX2 = space(50)
  w_ELVALTX3 = space(254)
  o_ELVALTX3 = space(254)
  w_TESTXT = .F.
  w_EL__MASK = space(50)
  w_LUNMAX = 0
  w_RIFTAB = space(20)
  w_ELFUNSCE = space(254)
  w_ELCODTAB = space(30)
  o_ELCODTAB = space(30)
  w_EL_CAMPO = space(30)
  o_EL_CAMPO = space(30)
  w_ELCODCLA = space(15)
  w_ELTIPCAM = space(1)
  o_ELTIPCAM = space(1)
  w_ELALLINE = space(1)
  o_ELALLINE = space(1)
  w_ELZERFIL = space(1)
  o_ELZERFIL = space(1)
  w_ELCARFIL = space(1)
  w_EL_SEGN1 = space(1)
  o_EL_SEGN1 = space(1)
  w_EL_FLPIU = space(1)
  w_ELTIPTRA = space(1)
  o_ELTIPTRA = space(1)
  w_EL_BATCH = space(50)
  w_ELCONDIT = space(254)
  w_ELCONWHE = space(254)
  w_ELEXPRES = space(254)
  w_ELFUNSC2 = space(254)
  w_ELTABIMP = space(30)
  o_ELTABIMP = space(30)
  w_ELCAMIMP = space(30)
  o_ELCAMIMP = space(30)
  w_ELMEMDAT = space(1)
  w_ELQUALIF = space(1)
  w_ELPOSQUA = 0
  w_ELALLIMP = space(1)
  w_ELCARIMP = space(1)
  w_EL_SEGNO = space(1)
  w_ELTIPTR2 = space(1)
  o_ELTIPTR2 = space(1)
  w_EL_BATC2 = space(50)
  w_DESSTR = space(30)
  w_DESSTR = space(30)
  w_ELCOS = space(20)
  w_ELCOE = space(20)
  w_ELDE = space(50)
  w_ELCOS1 = space(20)
  w_ELCOE1 = space(20)
  w_ELDE1 = space(50)
  w_DESCAMP = space(254)
  w_ELDESC = space(254)
  w_DESCLA = space(50)
  w_ELEXPRE1 = space(254)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'VAELEMEN','gste_ael')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_aelPag1","gste_ael",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Elemento")
      .Pages(1).HelpContextID = 264663883
      .Pages(2).addobject("oPag","tgste_aelPag2","gste_ael",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Export")
      .Pages(2).HelpContextID = 150190010
      .Pages(3).addobject("oPag","tgste_aelPag3","gste_ael",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Import")
      .Pages(3).HelpContextID = 150192762
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oELCODSTR_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='VASTRUTT'
    this.cWorkTables[2]='VAFORMAT'
    this.cWorkTables[3]='ENT_DETT'
    this.cWorkTables[4]='XDC_FIELDS'
    this.cWorkTables[5]='PROMCLAS'
    this.cWorkTables[6]='VAELEMEN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.VAELEMEN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.VAELEMEN_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ELCODSTR = NVL(ELCODSTR,space(10))
      .w_ELCODICE = NVL(ELCODICE,space(20))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_14_joined
    link_1_14_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_2_6_joined
    link_2_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from VAELEMEN where ELCODSTR=KeySet.ELCODSTR
    *                            and ELCODICE=KeySet.ELCODICE
    *
    i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('VAELEMEN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "VAELEMEN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' VAELEMEN '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_14_joined=this.AddJoinedLink_1_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_6_joined=this.AddJoinedLink_2_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ELCODSTR',this.w_ELCODSTR  ,'ELCODICE',this.w_ELCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPFIL = space(1)
        .w_STFLTYPE = space(1)
        .w_ROOT = space(20)
        .w_LUNTAG = 0
        .w_DECIMAL = 0
        .w_TIPO = space(1)
        .w_DESSTR = space(30)
        .w_DESPAD = space(50)
        .w_DESFOR = space(30)
        .w_FLPROV = space(1)
        .w_MESS = space(10)
        .w_FLATAB = space(15)
        .w_LUNMAX = 0
        .w_RIFTAB = space(20)
        .w_DESSTR = space(30)
        .w_DESSTR = space(30)
        .w_DESCAMP = space(254)
        .w_ELDESC = space(254)
        .w_DESCLA = space(50)
        .w_ELCODSTR = NVL(ELCODSTR,space(10))
          if link_1_1_joined
            this.w_ELCODSTR = NVL(STCODICE101,NVL(this.w_ELCODSTR,space(10)))
            this.w_ELCODENT = NVL(STCODENT101,space(15))
            this.w_DESSTR = NVL(STDESCRI101,space(30))
            this.w_ROOT = NVL(STELROOT101,space(20))
            this.w_TIPFIL = NVL(STTIPFIL101,space(1))
            this.w_FLPROV = NVL(STFLPROV101,space(1))
            this.w_LUNTAG = NVL(STLUNTAG101,0)
            this.w_FLATAB = NVL(STFLATAB101,space(15))
            this.w_STFLTYPE = NVL(STFLTYPE101,space(1))
          else
          .link_1_1('Load')
          endif
        .w_ELCODICE = NVL(ELCODICE,space(20))
        .w_ELDESCRI = NVL(ELDESCRI,space(50))
        .w_ELORDINE = NVL(ELORDINE,0)
        .w_ELMODELE = NVL(ELMODELE,space(1))
        .w_ELIDCHLD = NVL(ELIDCHLD,0)
        .w_EL__TIPO = NVL(EL__TIPO,space(1))
        .w_ELCODPAD = NVL(ELCODPAD,space(20))
          if link_1_14_joined
            this.w_ELCODPAD = NVL(ELCODICE114,NVL(this.w_ELCODPAD,space(20)))
            this.w_DESPAD = NVL(ELDESCRI114,space(50))
            this.w_TIPO = NVL(EL__TIPO114,space(1))
          else
          .link_1_14('Load')
          endif
        .w_ELCODFOR = NVL(ELCODFOR,space(20))
          if link_1_16_joined
            this.w_ELCODFOR = NVL(FOCODICE116,NVL(this.w_ELCODFOR,space(20)))
            this.w_DESFOR = NVL(FODESCRI116,space(30))
          else
          .link_1_16('Load')
          endif
        .w_ELLUNMAX = NVL(ELLUNMAX,0)
        .w_ELCODENT = NVL(ELCODENT,space(15))
        .w_ELVALTXT = NVL(ELVALTXT,space(50))
        .w_ELINISEZ = NVL(ELINISEZ,space(1))
        .w_ELORDASS = NVL(ELORDASS,0)
        .w_ELVALTX2 = NVL(ELVALTX2,space(50))
        .w_ELVALTX3 = NVL(ELVALTX3,space(254))
        .w_TESTXT = IIF(Not EMPTY(.w_ELVALTXT) OR Not EMPTY(.w_ELVALTX2) OR Not EMPTY(.w_ELVALTX3),.T.,.F.)
        .w_EL__MASK = NVL(EL__MASK,space(50))
        .w_ELFUNSCE = NVL(ELFUNSCE,space(254))
        .w_ELCODTAB = NVL(ELCODTAB,space(30))
          * evitabile
          *.link_2_4('Load')
        .w_EL_CAMPO = NVL(EL_CAMPO,space(30))
          .link_2_5('Load')
        .w_ELCODCLA = NVL(ELCODCLA,space(15))
          if link_2_6_joined
            this.w_ELCODCLA = NVL(CDCODCLA206,NVL(this.w_ELCODCLA,space(15)))
            this.w_RIFTAB = NVL(CDRIFTAB206,space(20))
            this.w_DESCLA = NVL(CDDESCLA206,space(50))
          else
          .link_2_6('Load')
          endif
        .w_ELTIPCAM = NVL(ELTIPCAM,space(1))
        .w_ELALLINE = NVL(ELALLINE,space(1))
        .w_ELZERFIL = NVL(ELZERFIL,space(1))
        .w_ELCARFIL = NVL(ELCARFIL,space(1))
        .w_EL_SEGN1 = NVL(EL_SEGN1,space(1))
        .w_EL_FLPIU = NVL(EL_FLPIU,space(1))
        .w_ELTIPTRA = NVL(ELTIPTRA,space(1))
        .w_EL_BATCH = NVL(EL_BATCH,space(50))
        .w_ELCONDIT = NVL(ELCONDIT,space(254))
        .w_ELCONWHE = NVL(ELCONWHE,space(254))
        .w_ELEXPRES = NVL(ELEXPRES,space(254))
        .w_ELFUNSC2 = NVL(ELFUNSC2,space(254))
        .w_ELTABIMP = NVL(ELTABIMP,space(30))
          * evitabile
          *.link_3_2('Load')
        .w_ELCAMIMP = NVL(ELCAMIMP,space(30))
        .w_ELMEMDAT = NVL(ELMEMDAT,space(1))
        .w_ELQUALIF = NVL(ELQUALIF,space(1))
        .w_ELPOSQUA = NVL(ELPOSQUA,0)
        .w_ELALLIMP = NVL(ELALLIMP,space(1))
        .w_ELCARIMP = NVL(ELCARIMP,space(1))
        .w_EL_SEGNO = NVL(EL_SEGNO,space(1))
        .w_ELTIPTR2 = NVL(ELTIPTR2,space(1))
        .w_EL_BATC2 = NVL(EL_BATC2,space(50))
        .w_ELCOS = .w_ELCODSTR
        .w_ELCOE = .w_ELCODICE
        .w_ELDE = .w_ELDESCRI
        .w_ELCOS1 = .w_ELCODSTR
        .w_ELCOE1 = .w_ELCODICE
        .w_ELDE1 = .w_ELDESCRI
        .w_ELEXPRE1 = NVL(ELEXPRE1,space(254))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        cp_LoadRecExtFlds(this,'VAELEMEN')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gste_ael
    * --- Propone l'ultima commessa Inserita
    p_OLDSTR=space(10)
    if p_OLDSTR<>this.w_ELCODSTR
       p_OLDSTR=this.w_ELCODSTR
    endif
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ELCODSTR = space(10)
      .w_ELCODICE = space(20)
      .w_TIPFIL = space(1)
      .w_STFLTYPE = space(1)
      .w_ROOT = space(20)
      .w_ELDESCRI = space(50)
      .w_ELORDINE = 0
      .w_ELMODELE = space(1)
      .w_ELIDCHLD = 0
      .w_EL__TIPO = space(1)
      .w_ELCODPAD = space(20)
      .w_ELCODFOR = space(20)
      .w_ELLUNMAX = 0
      .w_LUNTAG = 0
      .w_DECIMAL = 0
      .w_TIPO = space(1)
      .w_ELCODENT = space(15)
      .w_DESSTR = space(30)
      .w_DESPAD = space(50)
      .w_DESFOR = space(30)
      .w_FLPROV = space(1)
      .w_MESS = space(10)
      .w_FLATAB = space(15)
      .w_ELVALTXT = space(50)
      .w_ELINISEZ = space(1)
      .w_ELORDASS = 0
      .w_ELVALTX2 = space(50)
      .w_ELVALTX3 = space(254)
      .w_TESTXT = .f.
      .w_EL__MASK = space(50)
      .w_LUNMAX = 0
      .w_RIFTAB = space(20)
      .w_ELFUNSCE = space(254)
      .w_ELCODTAB = space(30)
      .w_EL_CAMPO = space(30)
      .w_ELCODCLA = space(15)
      .w_ELTIPCAM = space(1)
      .w_ELALLINE = space(1)
      .w_ELZERFIL = space(1)
      .w_ELCARFIL = space(1)
      .w_EL_SEGN1 = space(1)
      .w_EL_FLPIU = space(1)
      .w_ELTIPTRA = space(1)
      .w_EL_BATCH = space(50)
      .w_ELCONDIT = space(254)
      .w_ELCONWHE = space(254)
      .w_ELEXPRES = space(254)
      .w_ELFUNSC2 = space(254)
      .w_ELTABIMP = space(30)
      .w_ELCAMIMP = space(30)
      .w_ELMEMDAT = space(1)
      .w_ELQUALIF = space(1)
      .w_ELPOSQUA = 0
      .w_ELALLIMP = space(1)
      .w_ELCARIMP = space(1)
      .w_EL_SEGNO = space(1)
      .w_ELTIPTR2 = space(1)
      .w_EL_BATC2 = space(50)
      .w_DESSTR = space(30)
      .w_DESSTR = space(30)
      .w_ELCOS = space(20)
      .w_ELCOE = space(20)
      .w_ELDE = space(50)
      .w_ELCOS1 = space(20)
      .w_ELCOE1 = space(20)
      .w_ELDE1 = space(50)
      .w_DESCAMP = space(254)
      .w_ELDESC = space(254)
      .w_DESCLA = space(50)
      .w_ELEXPRE1 = space(254)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_ELCODSTR))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,7,.f.)
        .w_ELMODELE = 'E'
          .DoRTCalc(9,9,.f.)
        .w_EL__TIPO = 'V'
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_ELCODPAD))
          .link_1_14('Full')
          endif
        .w_ELCODFOR = Space(20)
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_ELCODFOR))
          .link_1_16('Full')
          endif
        .w_ELLUNMAX = IIF(.w_TIPFIL='L' and Not Empty(.w_ELVALTXT) ,.w_LUNTAG,.w_ELLUNMAX)
          .DoRTCalc(14,24,.f.)
        .w_ELINISEZ = 'N'
          .DoRTCalc(26,28,.f.)
        .w_TESTXT = IIF(Not EMPTY(.w_ELVALTXT) OR Not EMPTY(.w_ELVALTX2) OR Not EMPTY(.w_ELVALTX3),.T.,.F.)
        .w_EL__MASK = iCASE(.w_ELTIPCAM='N',IIF(.w_ELLUNMAX>0,repl('9',.w_ELLUNMAX-IIF(.w_DECIMAL>0,.w_DECIMAL+1,0))+IIF(.w_DECIMAL>0,'.','')+repl('9',.w_DECIMAL),''),.w_ELTIPCAM='D' ,'AAAAMMGG','')
          .DoRTCalc(31,33,.f.)
        .w_ELCODTAB = Space(30)
        .DoRTCalc(34,34,.f.)
          if not(empty(.w_ELCODTAB))
          .link_2_4('Full')
          endif
        .w_EL_CAMPO = Space(30)
        .DoRTCalc(35,35,.f.)
          if not(empty(.w_EL_CAMPO))
          .link_2_5('Full')
          endif
        .DoRTCalc(36,36,.f.)
          if not(empty(.w_ELCODCLA))
          .link_2_6('Full')
          endif
        .w_ELTIPCAM = 'C'
        .w_ELALLINE = 'N'
        .w_ELZERFIL = 'N'
        .w_ELCARFIL = '0'
        .w_EL_SEGN1 = 'O'
        .w_EL_FLPIU = 'N'
        .w_ELTIPTRA = 'N'
        .w_EL_BATCH = ''
          .DoRTCalc(45,46,.f.)
        .w_ELEXPRES = ''
          .DoRTCalc(48,48,.f.)
        .w_ELTABIMP = Space(30)
        .DoRTCalc(49,49,.f.)
          if not(empty(.w_ELTABIMP))
          .link_3_2('Full')
          endif
        .w_ELCAMIMP = Space(30)
        .w_ELMEMDAT = 'N'
        .w_ELQUALIF = 'N'
          .DoRTCalc(53,53,.f.)
        .w_ELALLIMP = 'N'
        .w_ELCARIMP = ' '
        .w_EL_SEGNO = 'O'
        .w_ELTIPTR2 = 'N'
        .w_EL_BATC2 = ''
          .DoRTCalc(59,60,.f.)
        .w_ELCOS = .w_ELCODSTR
        .w_ELCOE = .w_ELCODICE
        .w_ELDE = .w_ELDESCRI
        .w_ELCOS1 = .w_ELCODSTR
        .w_ELCOE1 = .w_ELCODICE
        .w_ELDE1 = .w_ELDESCRI
          .DoRTCalc(67,69,.f.)
        .w_ELEXPRE1 = iif(Empty(.w_ELCAMIMP),'',.w_ELEXPRE1)
      endif
    endwith
    cp_BlankRecExtFlds(this,'VAELEMEN')
    this.DoRTCalc(71,74,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oELCODSTR_1_1.enabled = i_bVal
      .Page1.oPag.oELCODICE_1_3.enabled = i_bVal
      .Page1.oPag.oELDESCRI_1_7.enabled = i_bVal
      .Page1.oPag.oELORDINE_1_9.enabled = i_bVal
      .Page1.oPag.oELMODELE_1_10.enabled = i_bVal
      .Page1.oPag.oELIDCHLD_1_11.enabled = i_bVal
      .Page1.oPag.oEL__TIPO_1_12.enabled = i_bVal
      .Page1.oPag.oELCODPAD_1_14.enabled = i_bVal
      .Page1.oPag.oELCODFOR_1_16.enabled = i_bVal
      .Page1.oPag.oELLUNMAX_1_17.enabled = i_bVal
      .Page1.oPag.oELVALTXT_1_34.enabled = i_bVal
      .Page1.oPag.oELINISEZ_1_35.enabled = i_bVal
      .Page1.oPag.oELORDASS_1_36.enabled = i_bVal
      .Page1.oPag.oELVALTX2_1_37.enabled = i_bVal
      .Page1.oPag.oELVALTX3_1_38.enabled = i_bVal
      .Page1.oPag.oEL__MASK_1_40.enabled = i_bVal
      .Page2.oPag.oELFUNSCE_2_3.enabled = i_bVal
      .Page2.oPag.oELCODTAB_2_4.enabled = i_bVal
      .Page2.oPag.oEL_CAMPO_2_5.enabled = i_bVal
      .Page2.oPag.oELCODCLA_2_6.enabled = i_bVal
      .Page2.oPag.oELTIPCAM_2_7.enabled = i_bVal
      .Page2.oPag.oELALLINE_2_8.enabled = i_bVal
      .Page2.oPag.oELZERFIL_2_9.enabled = i_bVal
      .Page2.oPag.oELCARFIL_2_10.enabled = i_bVal
      .Page2.oPag.oEL_SEGN1_2_11.enabled = i_bVal
      .Page2.oPag.oEL_FLPIU_2_12.enabled = i_bVal
      .Page2.oPag.oELTIPTRA_2_13.enabled = i_bVal
      .Page2.oPag.oEL_BATCH_2_14.enabled = i_bVal
      .Page2.oPag.oELCONDIT_2_15.enabled = i_bVal
      .Page2.oPag.oELCONWHE_2_16.enabled = i_bVal
      .Page2.oPag.oELEXPRES_2_26.enabled = i_bVal
      .Page3.oPag.oELFUNSC2_3_1.enabled = i_bVal
      .Page3.oPag.oELTABIMP_3_2.enabled = i_bVal
      .Page3.oPag.oELCAMIMP_3_3.enabled = i_bVal
      .Page3.oPag.oELMEMDAT_3_4.enabled = i_bVal
      .Page3.oPag.oELQUALIF_3_5.enabled = i_bVal
      .Page3.oPag.oELPOSQUA_3_6.enabled = i_bVal
      .Page3.oPag.oELALLIMP_3_7.enabled = i_bVal
      .Page3.oPag.oELCARIMP_3_8.enabled = i_bVal
      .Page3.oPag.oEL_SEGNO_3_9.enabled = i_bVal
      .Page3.oPag.oELTIPTR2_3_10.enabled = i_bVal
      .Page3.oPag.oEL_BATC2_3_11.enabled = i_bVal
      .Page3.oPag.oELEXPRE1_3_30.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oELCODSTR_1_1.enabled = .f.
        .Page1.oPag.oELCODICE_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oELCODSTR_1_1.enabled = .t.
        .Page1.oPag.oELCODICE_1_3.enabled = .t.
        .Page1.oPag.oELCODPAD_1_14.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'VAELEMEN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODSTR,"ELCODSTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODICE,"ELCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELDESCRI,"ELDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELORDINE,"ELORDINE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELMODELE,"ELMODELE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELIDCHLD,"ELIDCHLD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EL__TIPO,"EL__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODPAD,"ELCODPAD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODFOR,"ELCODFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELLUNMAX,"ELLUNMAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODENT,"ELCODENT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELVALTXT,"ELVALTXT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELINISEZ,"ELINISEZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELORDASS,"ELORDASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELVALTX2,"ELVALTX2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELVALTX3,"ELVALTX3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EL__MASK,"EL__MASK",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELFUNSCE,"ELFUNSCE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODTAB,"ELCODTAB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EL_CAMPO,"EL_CAMPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCODCLA,"ELCODCLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELTIPCAM,"ELTIPCAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELALLINE,"ELALLINE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELZERFIL,"ELZERFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCARFIL,"ELCARFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EL_SEGN1,"EL_SEGN1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EL_FLPIU,"EL_FLPIU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELTIPTRA,"ELTIPTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EL_BATCH,"EL_BATCH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCONDIT,"ELCONDIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCONWHE,"ELCONWHE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELEXPRES,"ELEXPRES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELFUNSC2,"ELFUNSC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELTABIMP,"ELTABIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCAMIMP,"ELCAMIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELMEMDAT,"ELMEMDAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELQUALIF,"ELQUALIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELPOSQUA,"ELPOSQUA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELALLIMP,"ELALLIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELCARIMP,"ELCARIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EL_SEGNO,"EL_SEGNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELTIPTR2,"ELTIPTR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_EL_BATC2,"EL_BATC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ELEXPRE1,"ELEXPRE1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
    i_lTable = "VAELEMEN"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.VAELEMEN_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.VAELEMEN_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into VAELEMEN
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'VAELEMEN')
        i_extval=cp_InsertValODBCExtFlds(this,'VAELEMEN')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ELCODSTR,ELCODICE,ELDESCRI,ELORDINE,ELMODELE"+;
                  ",ELIDCHLD,EL__TIPO,ELCODPAD,ELCODFOR,ELLUNMAX"+;
                  ",ELCODENT,ELVALTXT,ELINISEZ,ELORDASS,ELVALTX2"+;
                  ",ELVALTX3,EL__MASK,ELFUNSCE,ELCODTAB,EL_CAMPO"+;
                  ",ELCODCLA,ELTIPCAM,ELALLINE,ELZERFIL,ELCARFIL"+;
                  ",EL_SEGN1,EL_FLPIU,ELTIPTRA,EL_BATCH,ELCONDIT"+;
                  ",ELCONWHE,ELEXPRES,ELFUNSC2,ELTABIMP,ELCAMIMP"+;
                  ",ELMEMDAT,ELQUALIF,ELPOSQUA,ELALLIMP,ELCARIMP"+;
                  ",EL_SEGNO,ELTIPTR2,EL_BATC2,ELEXPRE1,UTCC"+;
                  ",UTCV,UTDC,UTDV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_ELCODSTR)+;
                  ","+cp_ToStrODBC(this.w_ELCODICE)+;
                  ","+cp_ToStrODBC(this.w_ELDESCRI)+;
                  ","+cp_ToStrODBC(this.w_ELORDINE)+;
                  ","+cp_ToStrODBC(this.w_ELMODELE)+;
                  ","+cp_ToStrODBC(this.w_ELIDCHLD)+;
                  ","+cp_ToStrODBC(this.w_EL__TIPO)+;
                  ","+cp_ToStrODBCNull(this.w_ELCODPAD)+;
                  ","+cp_ToStrODBCNull(this.w_ELCODFOR)+;
                  ","+cp_ToStrODBC(this.w_ELLUNMAX)+;
                  ","+cp_ToStrODBC(this.w_ELCODENT)+;
                  ","+cp_ToStrODBC(this.w_ELVALTXT)+;
                  ","+cp_ToStrODBC(this.w_ELINISEZ)+;
                  ","+cp_ToStrODBC(this.w_ELORDASS)+;
                  ","+cp_ToStrODBC(this.w_ELVALTX2)+;
                  ","+cp_ToStrODBC(this.w_ELVALTX3)+;
                  ","+cp_ToStrODBC(this.w_EL__MASK)+;
                  ","+cp_ToStrODBC(this.w_ELFUNSCE)+;
                  ","+cp_ToStrODBCNull(this.w_ELCODTAB)+;
                  ","+cp_ToStrODBCNull(this.w_EL_CAMPO)+;
                  ","+cp_ToStrODBCNull(this.w_ELCODCLA)+;
                  ","+cp_ToStrODBC(this.w_ELTIPCAM)+;
                  ","+cp_ToStrODBC(this.w_ELALLINE)+;
                  ","+cp_ToStrODBC(this.w_ELZERFIL)+;
                  ","+cp_ToStrODBC(this.w_ELCARFIL)+;
                  ","+cp_ToStrODBC(this.w_EL_SEGN1)+;
                  ","+cp_ToStrODBC(this.w_EL_FLPIU)+;
                  ","+cp_ToStrODBC(this.w_ELTIPTRA)+;
                  ","+cp_ToStrODBC(this.w_EL_BATCH)+;
                  ","+cp_ToStrODBC(this.w_ELCONDIT)+;
                  ","+cp_ToStrODBC(this.w_ELCONWHE)+;
                  ","+cp_ToStrODBC(this.w_ELEXPRES)+;
                  ","+cp_ToStrODBC(this.w_ELFUNSC2)+;
                  ","+cp_ToStrODBCNull(this.w_ELTABIMP)+;
                  ","+cp_ToStrODBC(this.w_ELCAMIMP)+;
                  ","+cp_ToStrODBC(this.w_ELMEMDAT)+;
                  ","+cp_ToStrODBC(this.w_ELQUALIF)+;
                  ","+cp_ToStrODBC(this.w_ELPOSQUA)+;
                  ","+cp_ToStrODBC(this.w_ELALLIMP)+;
                  ","+cp_ToStrODBC(this.w_ELCARIMP)+;
                  ","+cp_ToStrODBC(this.w_EL_SEGNO)+;
                  ","+cp_ToStrODBC(this.w_ELTIPTR2)+;
                  ","+cp_ToStrODBC(this.w_EL_BATC2)+;
                  ","+cp_ToStrODBC(this.w_ELEXPRE1)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'VAELEMEN')
        i_extval=cp_InsertValVFPExtFlds(this,'VAELEMEN')
        cp_CheckDeletedKey(i_cTable,0,'ELCODSTR',this.w_ELCODSTR,'ELCODICE',this.w_ELCODICE)
        INSERT INTO (i_cTable);
              (ELCODSTR,ELCODICE,ELDESCRI,ELORDINE,ELMODELE,ELIDCHLD,EL__TIPO,ELCODPAD,ELCODFOR,ELLUNMAX,ELCODENT,ELVALTXT,ELINISEZ,ELORDASS,ELVALTX2,ELVALTX3,EL__MASK,ELFUNSCE,ELCODTAB,EL_CAMPO,ELCODCLA,ELTIPCAM,ELALLINE,ELZERFIL,ELCARFIL,EL_SEGN1,EL_FLPIU,ELTIPTRA,EL_BATCH,ELCONDIT,ELCONWHE,ELEXPRES,ELFUNSC2,ELTABIMP,ELCAMIMP,ELMEMDAT,ELQUALIF,ELPOSQUA,ELALLIMP,ELCARIMP,EL_SEGNO,ELTIPTR2,EL_BATC2,ELEXPRE1,UTCC,UTCV,UTDC,UTDV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ELCODSTR;
                  ,this.w_ELCODICE;
                  ,this.w_ELDESCRI;
                  ,this.w_ELORDINE;
                  ,this.w_ELMODELE;
                  ,this.w_ELIDCHLD;
                  ,this.w_EL__TIPO;
                  ,this.w_ELCODPAD;
                  ,this.w_ELCODFOR;
                  ,this.w_ELLUNMAX;
                  ,this.w_ELCODENT;
                  ,this.w_ELVALTXT;
                  ,this.w_ELINISEZ;
                  ,this.w_ELORDASS;
                  ,this.w_ELVALTX2;
                  ,this.w_ELVALTX3;
                  ,this.w_EL__MASK;
                  ,this.w_ELFUNSCE;
                  ,this.w_ELCODTAB;
                  ,this.w_EL_CAMPO;
                  ,this.w_ELCODCLA;
                  ,this.w_ELTIPCAM;
                  ,this.w_ELALLINE;
                  ,this.w_ELZERFIL;
                  ,this.w_ELCARFIL;
                  ,this.w_EL_SEGN1;
                  ,this.w_EL_FLPIU;
                  ,this.w_ELTIPTRA;
                  ,this.w_EL_BATCH;
                  ,this.w_ELCONDIT;
                  ,this.w_ELCONWHE;
                  ,this.w_ELEXPRES;
                  ,this.w_ELFUNSC2;
                  ,this.w_ELTABIMP;
                  ,this.w_ELCAMIMP;
                  ,this.w_ELMEMDAT;
                  ,this.w_ELQUALIF;
                  ,this.w_ELPOSQUA;
                  ,this.w_ELALLIMP;
                  ,this.w_ELCARIMP;
                  ,this.w_EL_SEGNO;
                  ,this.w_ELTIPTR2;
                  ,this.w_EL_BATC2;
                  ,this.w_ELEXPRE1;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.VAELEMEN_IDX,i_nConn)
      *
      * update VAELEMEN
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'VAELEMEN')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ELDESCRI="+cp_ToStrODBC(this.w_ELDESCRI)+;
             ",ELORDINE="+cp_ToStrODBC(this.w_ELORDINE)+;
             ",ELMODELE="+cp_ToStrODBC(this.w_ELMODELE)+;
             ",ELIDCHLD="+cp_ToStrODBC(this.w_ELIDCHLD)+;
             ",EL__TIPO="+cp_ToStrODBC(this.w_EL__TIPO)+;
             ",ELCODPAD="+cp_ToStrODBCNull(this.w_ELCODPAD)+;
             ",ELCODFOR="+cp_ToStrODBCNull(this.w_ELCODFOR)+;
             ",ELLUNMAX="+cp_ToStrODBC(this.w_ELLUNMAX)+;
             ",ELCODENT="+cp_ToStrODBC(this.w_ELCODENT)+;
             ",ELVALTXT="+cp_ToStrODBC(this.w_ELVALTXT)+;
             ",ELINISEZ="+cp_ToStrODBC(this.w_ELINISEZ)+;
             ",ELORDASS="+cp_ToStrODBC(this.w_ELORDASS)+;
             ",ELVALTX2="+cp_ToStrODBC(this.w_ELVALTX2)+;
             ",ELVALTX3="+cp_ToStrODBC(this.w_ELVALTX3)+;
             ",EL__MASK="+cp_ToStrODBC(this.w_EL__MASK)+;
             ",ELFUNSCE="+cp_ToStrODBC(this.w_ELFUNSCE)+;
             ",ELCODTAB="+cp_ToStrODBCNull(this.w_ELCODTAB)+;
             ",EL_CAMPO="+cp_ToStrODBCNull(this.w_EL_CAMPO)+;
             ",ELCODCLA="+cp_ToStrODBCNull(this.w_ELCODCLA)+;
             ",ELTIPCAM="+cp_ToStrODBC(this.w_ELTIPCAM)+;
             ",ELALLINE="+cp_ToStrODBC(this.w_ELALLINE)+;
             ",ELZERFIL="+cp_ToStrODBC(this.w_ELZERFIL)+;
             ",ELCARFIL="+cp_ToStrODBC(this.w_ELCARFIL)+;
             ",EL_SEGN1="+cp_ToStrODBC(this.w_EL_SEGN1)+;
             ",EL_FLPIU="+cp_ToStrODBC(this.w_EL_FLPIU)+;
             ",ELTIPTRA="+cp_ToStrODBC(this.w_ELTIPTRA)+;
             ",EL_BATCH="+cp_ToStrODBC(this.w_EL_BATCH)+;
             ",ELCONDIT="+cp_ToStrODBC(this.w_ELCONDIT)+;
             ",ELCONWHE="+cp_ToStrODBC(this.w_ELCONWHE)+;
             ",ELEXPRES="+cp_ToStrODBC(this.w_ELEXPRES)+;
             ",ELFUNSC2="+cp_ToStrODBC(this.w_ELFUNSC2)+;
             ",ELTABIMP="+cp_ToStrODBCNull(this.w_ELTABIMP)+;
             ",ELCAMIMP="+cp_ToStrODBC(this.w_ELCAMIMP)+;
             ",ELMEMDAT="+cp_ToStrODBC(this.w_ELMEMDAT)+;
             ",ELQUALIF="+cp_ToStrODBC(this.w_ELQUALIF)+;
             ",ELPOSQUA="+cp_ToStrODBC(this.w_ELPOSQUA)+;
             ",ELALLIMP="+cp_ToStrODBC(this.w_ELALLIMP)+;
             ",ELCARIMP="+cp_ToStrODBC(this.w_ELCARIMP)+;
             ",EL_SEGNO="+cp_ToStrODBC(this.w_EL_SEGNO)+;
             ",ELTIPTR2="+cp_ToStrODBC(this.w_ELTIPTR2)+;
             ",EL_BATC2="+cp_ToStrODBC(this.w_EL_BATC2)+;
             ",ELEXPRE1="+cp_ToStrODBC(this.w_ELEXPRE1)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'VAELEMEN')
        i_cWhere = cp_PKFox(i_cTable  ,'ELCODSTR',this.w_ELCODSTR  ,'ELCODICE',this.w_ELCODICE  )
        UPDATE (i_cTable) SET;
              ELDESCRI=this.w_ELDESCRI;
             ,ELORDINE=this.w_ELORDINE;
             ,ELMODELE=this.w_ELMODELE;
             ,ELIDCHLD=this.w_ELIDCHLD;
             ,EL__TIPO=this.w_EL__TIPO;
             ,ELCODPAD=this.w_ELCODPAD;
             ,ELCODFOR=this.w_ELCODFOR;
             ,ELLUNMAX=this.w_ELLUNMAX;
             ,ELCODENT=this.w_ELCODENT;
             ,ELVALTXT=this.w_ELVALTXT;
             ,ELINISEZ=this.w_ELINISEZ;
             ,ELORDASS=this.w_ELORDASS;
             ,ELVALTX2=this.w_ELVALTX2;
             ,ELVALTX3=this.w_ELVALTX3;
             ,EL__MASK=this.w_EL__MASK;
             ,ELFUNSCE=this.w_ELFUNSCE;
             ,ELCODTAB=this.w_ELCODTAB;
             ,EL_CAMPO=this.w_EL_CAMPO;
             ,ELCODCLA=this.w_ELCODCLA;
             ,ELTIPCAM=this.w_ELTIPCAM;
             ,ELALLINE=this.w_ELALLINE;
             ,ELZERFIL=this.w_ELZERFIL;
             ,ELCARFIL=this.w_ELCARFIL;
             ,EL_SEGN1=this.w_EL_SEGN1;
             ,EL_FLPIU=this.w_EL_FLPIU;
             ,ELTIPTRA=this.w_ELTIPTRA;
             ,EL_BATCH=this.w_EL_BATCH;
             ,ELCONDIT=this.w_ELCONDIT;
             ,ELCONWHE=this.w_ELCONWHE;
             ,ELEXPRES=this.w_ELEXPRES;
             ,ELFUNSC2=this.w_ELFUNSC2;
             ,ELTABIMP=this.w_ELTABIMP;
             ,ELCAMIMP=this.w_ELCAMIMP;
             ,ELMEMDAT=this.w_ELMEMDAT;
             ,ELQUALIF=this.w_ELQUALIF;
             ,ELPOSQUA=this.w_ELPOSQUA;
             ,ELALLIMP=this.w_ELALLIMP;
             ,ELCARIMP=this.w_ELCARIMP;
             ,EL_SEGNO=this.w_EL_SEGNO;
             ,ELTIPTR2=this.w_ELTIPTR2;
             ,EL_BATC2=this.w_EL_BATC2;
             ,ELEXPRE1=this.w_ELEXPRE1;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.VAELEMEN_IDX,i_nConn)
      *
      * delete VAELEMEN
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ELCODSTR',this.w_ELCODSTR  ,'ELCODICE',this.w_ELCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,11,.t.)
        if .o_EL__TIPO<>.w_EL__TIPO
            .w_ELCODFOR = Space(20)
          .link_1_16('Full')
        endif
        if .o_ELVALTXT<>.w_ELVALTXT
            .w_ELLUNMAX = IIF(.w_TIPFIL='L' and Not Empty(.w_ELVALTXT) ,.w_LUNTAG,.w_ELLUNMAX)
        endif
        .DoRTCalc(14,24,.t.)
        if .o_ELVALTXT<>.w_ELVALTXT
            .w_ELINISEZ = 'N'
        endif
        .DoRTCalc(26,28,.t.)
        if .o_ELVALTX2<>.w_ELVALTX2.or. .o_ELVALTX3<>.w_ELVALTX3.or. .o_ELVALTXT<>.w_ELVALTXT
            .w_TESTXT = IIF(Not EMPTY(.w_ELVALTXT) OR Not EMPTY(.w_ELVALTX2) OR Not EMPTY(.w_ELVALTX3),.T.,.F.)
        endif
        if .o_EL_CAMPO<>.w_EL_CAMPO
            .w_EL__MASK = iCASE(.w_ELTIPCAM='N',IIF(.w_ELLUNMAX>0,repl('9',.w_ELLUNMAX-IIF(.w_DECIMAL>0,.w_DECIMAL+1,0))+IIF(.w_DECIMAL>0,'.','')+repl('9',.w_DECIMAL),''),.w_ELTIPCAM='D' ,'AAAAMMGG','')
        endif
        .DoRTCalc(31,33,.t.)
        if .o_EL__TIPO<>.w_EL__TIPO
            .w_ELCODTAB = Space(30)
          .link_2_4('Full')
        endif
        if .o_ELCODTAB<>.w_ELCODTAB.or. .o_EL__TIPO<>.w_EL__TIPO
            .w_EL_CAMPO = Space(30)
          .link_2_5('Full')
        endif
        .DoRTCalc(36,36,.t.)
        if .o_EL__TIPO<>.w_EL__TIPO
            .w_ELTIPCAM = 'C'
        endif
        if .o_ELTIPCAM<>.w_ELTIPCAM
            .w_ELALLINE = 'N'
        endif
        if .o_ELTIPCAM<>.w_ELTIPCAM.or. .o_ELALLINE<>.w_ELALLINE
            .w_ELZERFIL = 'N'
        endif
        if .o_ELZERFIL<>.w_ELZERFIL
            .w_ELCARFIL = '0'
        endif
        .DoRTCalc(41,41,.t.)
        if .o_EL_SEGN1<>.w_EL_SEGN1
            .w_EL_FLPIU = 'N'
        endif
        if .o_EL__TIPO<>.w_EL__TIPO.or. .o_ELCODTAB<>.w_ELCODTAB
            .w_ELTIPTRA = 'N'
        endif
        if .o_EL__TIPO<>.w_EL__TIPO.or. .o_ELTIPTRA<>.w_ELTIPTRA
            .w_EL_BATCH = ''
        endif
        if .o_EL_CAMPO<>.w_EL_CAMPO.or. .o_ELCAMIMP<>.w_ELCAMIMP.or. .o_EL__TIPO<>.w_EL__TIPO
          .Calculate_DQCEQBGDRH()
        endif
        .DoRTCalc(45,46,.t.)
        if .o_EL__TIPO<>.w_EL__TIPO
            .w_ELEXPRES = ''
        endif
        .DoRTCalc(48,48,.t.)
        if .o_EL__TIPO<>.w_EL__TIPO
            .w_ELTABIMP = Space(30)
          .link_3_2('Full')
        endif
        if .o_ELTABIMP<>.w_ELTABIMP.or. .o_EL__TIPO<>.w_EL__TIPO
            .w_ELCAMIMP = Space(30)
        endif
        .DoRTCalc(51,51,.t.)
        if .o_EL__TIPO<>.w_EL__TIPO
            .w_ELQUALIF = 'N'
        endif
        .DoRTCalc(53,53,.t.)
        if .o_ELTIPCAM<>.w_ELTIPCAM
            .w_ELALLIMP = 'N'
        endif
        if .o_ELZERFIL<>.w_ELZERFIL
            .w_ELCARIMP = ' '
        endif
        .DoRTCalc(56,56,.t.)
        if .o_EL__TIPO<>.w_EL__TIPO.or. .o_ELTABIMP<>.w_ELTABIMP
            .w_ELTIPTR2 = 'N'
        endif
        if .o_EL__TIPO<>.w_EL__TIPO.or. .o_ELTIPTR2<>.w_ELTIPTR2
            .w_EL_BATC2 = ''
        endif
        .DoRTCalc(59,60,.t.)
            .w_ELCOS = .w_ELCODSTR
            .w_ELCOE = .w_ELCODICE
            .w_ELDE = .w_ELDESCRI
            .w_ELCOS1 = .w_ELCODSTR
            .w_ELCOE1 = .w_ELCODICE
            .w_ELDE1 = .w_ELDESCRI
        .DoRTCalc(67,69,.t.)
        if .o_EL__TIPO<>.w_EL__TIPO.or. .o_ELCAMIMP<>.w_ELCAMIMP
            .w_ELEXPRE1 = iif(Empty(.w_ELCAMIMP),'',.w_ELEXPRE1)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(71,74,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_DQCEQBGDRH()
    with this
          * --- Assegno lunghezza al cambiare dei campi
          .w_ELLUNMAX = IIF(.w_TIPFIL='L' and Not Empty(.w_ELVALTXT) ,.w_LUNTAG,IIF(.w_ELLUNMAX=0,.w_LUNMAX,.w_ELLUNMAX))
          .w_EL__MASK = iCASE(.w_ELTIPCAM='N',IIF(.w_ELLUNMAX>0,repl('9',.w_ELLUNMAX-IIF(.w_DECIMAL>0,.w_DECIMAL+1,0))+IIF(.w_DECIMAL>0,'.','')+repl('9',.w_DECIMAL),''),.w_ELTIPCAM='D' ,'AAAAMMGG','')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oELMODELE_1_10.enabled = this.oPgFrm.Page1.oPag.oELMODELE_1_10.mCond()
    this.oPgFrm.Page1.oPag.oELIDCHLD_1_11.enabled = this.oPgFrm.Page1.oPag.oELIDCHLD_1_11.mCond()
    this.oPgFrm.Page1.oPag.oELCODFOR_1_16.enabled = this.oPgFrm.Page1.oPag.oELCODFOR_1_16.mCond()
    this.oPgFrm.Page1.oPag.oELLUNMAX_1_17.enabled = this.oPgFrm.Page1.oPag.oELLUNMAX_1_17.mCond()
    this.oPgFrm.Page1.oPag.oELINISEZ_1_35.enabled = this.oPgFrm.Page1.oPag.oELINISEZ_1_35.mCond()
    this.oPgFrm.Page1.oPag.oELORDASS_1_36.enabled = this.oPgFrm.Page1.oPag.oELORDASS_1_36.mCond()
    this.oPgFrm.Page1.oPag.oEL__MASK_1_40.enabled = this.oPgFrm.Page1.oPag.oEL__MASK_1_40.mCond()
    this.oPgFrm.Page2.oPag.oELFUNSCE_2_3.enabled = this.oPgFrm.Page2.oPag.oELFUNSCE_2_3.mCond()
    this.oPgFrm.Page2.oPag.oELCODTAB_2_4.enabled = this.oPgFrm.Page2.oPag.oELCODTAB_2_4.mCond()
    this.oPgFrm.Page2.oPag.oEL_CAMPO_2_5.enabled = this.oPgFrm.Page2.oPag.oEL_CAMPO_2_5.mCond()
    this.oPgFrm.Page2.oPag.oELCODCLA_2_6.enabled = this.oPgFrm.Page2.oPag.oELCODCLA_2_6.mCond()
    this.oPgFrm.Page2.oPag.oELTIPCAM_2_7.enabled = this.oPgFrm.Page2.oPag.oELTIPCAM_2_7.mCond()
    this.oPgFrm.Page2.oPag.oELZERFIL_2_9.enabled = this.oPgFrm.Page2.oPag.oELZERFIL_2_9.mCond()
    this.oPgFrm.Page2.oPag.oELTIPTRA_2_13.enabled = this.oPgFrm.Page2.oPag.oELTIPTRA_2_13.mCond()
    this.oPgFrm.Page2.oPag.oELCONWHE_2_16.enabled = this.oPgFrm.Page2.oPag.oELCONWHE_2_16.mCond()
    this.oPgFrm.Page2.oPag.oELEXPRES_2_26.enabled = this.oPgFrm.Page2.oPag.oELEXPRES_2_26.mCond()
    this.oPgFrm.Page3.oPag.oELFUNSC2_3_1.enabled = this.oPgFrm.Page3.oPag.oELFUNSC2_3_1.mCond()
    this.oPgFrm.Page3.oPag.oELTABIMP_3_2.enabled = this.oPgFrm.Page3.oPag.oELTABIMP_3_2.mCond()
    this.oPgFrm.Page3.oPag.oELCAMIMP_3_3.enabled = this.oPgFrm.Page3.oPag.oELCAMIMP_3_3.mCond()
    this.oPgFrm.Page3.oPag.oELMEMDAT_3_4.enabled = this.oPgFrm.Page3.oPag.oELMEMDAT_3_4.mCond()
    this.oPgFrm.Page3.oPag.oELCARIMP_3_8.enabled = this.oPgFrm.Page3.oPag.oELCARIMP_3_8.mCond()
    this.oPgFrm.Page3.oPag.oELTIPTR2_3_10.enabled = this.oPgFrm.Page3.oPag.oELTIPTR2_3_10.mCond()
    this.oPgFrm.Page3.oPag.oELEXPRE1_3_30.enabled = this.oPgFrm.Page3.oPag.oELEXPRE1_3_30.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    local i_show1
    i_show1=not(this.w_STFLTYPE='I')
    this.oPgFrm.Pages(2).enabled=i_show1
    this.oPgFrm.Pages(2).caption=iif(i_show1,cp_translate("Export"),"")
    this.oPgFrm.Pages(2).oPag.visible=this.oPgFrm.Pages(2).enabled
    local i_show2
    i_show2=not(1=1)
    this.oPgFrm.Pages(3).enabled=i_show2
    this.oPgFrm.Pages(3).caption=iif(i_show2,cp_translate("Import"),"")
    this.oPgFrm.Pages(3).oPag.visible=this.oPgFrm.Pages(3).enabled
    this.oPgFrm.Page1.oPag.oELMODELE_1_10.visible=!this.oPgFrm.Page1.oPag.oELMODELE_1_10.mHide()
    this.oPgFrm.Page1.oPag.oELIDCHLD_1_11.visible=!this.oPgFrm.Page1.oPag.oELIDCHLD_1_11.mHide()
    this.oPgFrm.Page1.oPag.oELLUNMAX_1_17.visible=!this.oPgFrm.Page1.oPag.oELLUNMAX_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    this.oPgFrm.Page2.oPag.oELFUNSCE_2_3.visible=!this.oPgFrm.Page2.oPag.oELFUNSCE_2_3.mHide()
    this.oPgFrm.Page2.oPag.oEL_CAMPO_2_5.visible=!this.oPgFrm.Page2.oPag.oEL_CAMPO_2_5.mHide()
    this.oPgFrm.Page2.oPag.oELCODCLA_2_6.visible=!this.oPgFrm.Page2.oPag.oELCODCLA_2_6.mHide()
    this.oPgFrm.Page2.oPag.oELTIPCAM_2_7.visible=!this.oPgFrm.Page2.oPag.oELTIPCAM_2_7.mHide()
    this.oPgFrm.Page2.oPag.oELALLINE_2_8.visible=!this.oPgFrm.Page2.oPag.oELALLINE_2_8.mHide()
    this.oPgFrm.Page2.oPag.oELZERFIL_2_9.visible=!this.oPgFrm.Page2.oPag.oELZERFIL_2_9.mHide()
    this.oPgFrm.Page2.oPag.oELCARFIL_2_10.visible=!this.oPgFrm.Page2.oPag.oELCARFIL_2_10.mHide()
    this.oPgFrm.Page2.oPag.oEL_SEGN1_2_11.visible=!this.oPgFrm.Page2.oPag.oEL_SEGN1_2_11.mHide()
    this.oPgFrm.Page2.oPag.oEL_FLPIU_2_12.visible=!this.oPgFrm.Page2.oPag.oEL_FLPIU_2_12.mHide()
    this.oPgFrm.Page2.oPag.oELTIPTRA_2_13.visible=!this.oPgFrm.Page2.oPag.oELTIPTRA_2_13.mHide()
    this.oPgFrm.Page2.oPag.oEL_BATCH_2_14.visible=!this.oPgFrm.Page2.oPag.oEL_BATCH_2_14.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_17.visible=!this.oPgFrm.Page2.oPag.oStr_2_17.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_18.visible=!this.oPgFrm.Page2.oPag.oStr_2_18.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_19.visible=!this.oPgFrm.Page2.oPag.oStr_2_19.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_20.visible=!this.oPgFrm.Page2.oPag.oStr_2_20.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_21.visible=!this.oPgFrm.Page2.oPag.oStr_2_21.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_22.visible=!this.oPgFrm.Page2.oPag.oStr_2_22.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_23.visible=!this.oPgFrm.Page2.oPag.oStr_2_23.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_24.visible=!this.oPgFrm.Page2.oPag.oStr_2_24.mHide()
    this.oPgFrm.Page3.oPag.oELFUNSC2_3_1.visible=!this.oPgFrm.Page3.oPag.oELFUNSC2_3_1.mHide()
    this.oPgFrm.Page3.oPag.oELQUALIF_3_5.visible=!this.oPgFrm.Page3.oPag.oELQUALIF_3_5.mHide()
    this.oPgFrm.Page3.oPag.oELPOSQUA_3_6.visible=!this.oPgFrm.Page3.oPag.oELPOSQUA_3_6.mHide()
    this.oPgFrm.Page3.oPag.oELALLIMP_3_7.visible=!this.oPgFrm.Page3.oPag.oELALLIMP_3_7.mHide()
    this.oPgFrm.Page3.oPag.oELCARIMP_3_8.visible=!this.oPgFrm.Page3.oPag.oELCARIMP_3_8.mHide()
    this.oPgFrm.Page3.oPag.oEL_SEGNO_3_9.visible=!this.oPgFrm.Page3.oPag.oEL_SEGNO_3_9.mHide()
    this.oPgFrm.Page3.oPag.oELTIPTR2_3_10.visible=!this.oPgFrm.Page3.oPag.oELTIPTR2_3_10.mHide()
    this.oPgFrm.Page3.oPag.oEL_BATC2_3_11.visible=!this.oPgFrm.Page3.oPag.oEL_BATC2_3_11.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_12.visible=!this.oPgFrm.Page3.oPag.oStr_3_12.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_13.visible=!this.oPgFrm.Page3.oPag.oStr_3_13.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_14.visible=!this.oPgFrm.Page3.oPag.oStr_3_14.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_15.visible=!this.oPgFrm.Page3.oPag.oStr_3_15.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_16.visible=!this.oPgFrm.Page3.oPag.oStr_3_16.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_19.visible=!this.oPgFrm.Page3.oPag.oStr_3_19.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_20.visible=!this.oPgFrm.Page3.oPag.oStr_3_20.mHide()
    this.oPgFrm.Page2.oPag.oDESCAMP_2_34.visible=!this.oPgFrm.Page2.oPag.oDESCAMP_2_34.mHide()
    this.oPgFrm.Page2.oPag.oDESCLA_2_35.visible=!this.oPgFrm.Page2.oPag.oDESCLA_2_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_29.visible=!this.oPgFrm.Page3.oPag.oStr_3_29.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ELCODSTR
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VASTRUTT_IDX,3]
    i_lTable = "VASTRUTT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2], .t., this.VASTRUTT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODSTR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VASTRUTT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" STCODICE like "+cp_ToStrODBC(trim(this.w_ELCODSTR)+"%");

          i_ret=cp_SQL(i_nConn,"select STCODICE,STCODENT,STDESCRI,STELROOT,STTIPFIL,STFLPROV,STLUNTAG,STFLATAB,STFLTYPE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by STCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'STCODICE',trim(this.w_ELCODSTR))
          select STCODICE,STCODENT,STDESCRI,STELROOT,STTIPFIL,STFLPROV,STLUNTAG,STFLATAB,STFLTYPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by STCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODSTR)==trim(_Link_.STCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCODSTR) and !this.bDontReportError
            deferred_cp_zoom('VASTRUTT','*','STCODICE',cp_AbsName(oSource.parent,'oELCODSTR_1_1'),i_cWhere,'',"Strutture",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STCODENT,STDESCRI,STELROOT,STTIPFIL,STFLPROV,STLUNTAG,STFLATAB,STFLTYPE";
                     +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',oSource.xKey(1))
            select STCODICE,STCODENT,STDESCRI,STELROOT,STTIPFIL,STFLPROV,STLUNTAG,STFLATAB,STFLTYPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODSTR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select STCODICE,STCODENT,STDESCRI,STELROOT,STTIPFIL,STFLPROV,STLUNTAG,STFLATAB,STFLTYPE";
                   +" from "+i_cTable+" "+i_lTable+" where STCODICE="+cp_ToStrODBC(this.w_ELCODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'STCODICE',this.w_ELCODSTR)
            select STCODICE,STCODENT,STDESCRI,STELROOT,STTIPFIL,STFLPROV,STLUNTAG,STFLATAB,STFLTYPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODSTR = NVL(_Link_.STCODICE,space(10))
      this.w_ELCODENT = NVL(_Link_.STCODENT,space(15))
      this.w_DESSTR = NVL(_Link_.STDESCRI,space(30))
      this.w_ROOT = NVL(_Link_.STELROOT,space(20))
      this.w_TIPFIL = NVL(_Link_.STTIPFIL,space(1))
      this.w_FLPROV = NVL(_Link_.STFLPROV,space(1))
      this.w_LUNTAG = NVL(_Link_.STLUNTAG,0)
      this.w_FLATAB = NVL(_Link_.STFLATAB,space(15))
      this.w_STFLTYPE = NVL(_Link_.STFLTYPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODSTR = space(10)
      endif
      this.w_ELCODENT = space(15)
      this.w_DESSTR = space(30)
      this.w_ROOT = space(20)
      this.w_TIPFIL = space(1)
      this.w_FLPROV = space(1)
      this.w_LUNTAG = 0
      this.w_FLATAB = space(15)
      this.w_STFLTYPE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])+'\'+cp_ToStr(_Link_.STCODICE,1)
      cp_ShowWarn(i_cKey,this.VASTRUTT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODSTR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VASTRUTT_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VASTRUTT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.STCODICE as STCODICE101"+ ",link_1_1.STCODENT as STCODENT101"+ ",link_1_1.STDESCRI as STDESCRI101"+ ",link_1_1.STELROOT as STELROOT101"+ ",link_1_1.STTIPFIL as STTIPFIL101"+ ",link_1_1.STFLPROV as STFLPROV101"+ ",link_1_1.STLUNTAG as STLUNTAG101"+ ",link_1_1.STFLATAB as STFLATAB101"+ ",link_1_1.STFLTYPE as STFLTYPE101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on VAELEMEN.ELCODSTR=link_1_1.STCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and VAELEMEN.ELCODSTR=link_1_1.STCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCODPAD
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAELEMEN_IDX,3]
    i_lTable = "VAELEMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2], .t., this.VAELEMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODPAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_AEL',True,'VAELEMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ELCODICE like "+cp_ToStrODBC(trim(this.w_ELCODPAD)+"%");
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);

          i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ELCODSTR,ELCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ELCODSTR',this.w_ELCODSTR;
                     ,'ELCODICE',trim(this.w_ELCODPAD))
          select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ELCODSTR,ELCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODPAD)==trim(_Link_.ELCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ELDESCRI like "+cp_ToStrODBC(trim(this.w_ELCODPAD)+"%");
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);

            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ELDESCRI like "+cp_ToStr(trim(this.w_ELCODPAD)+"%");
                   +" and ELCODSTR="+cp_ToStr(this.w_ELCODSTR);

            select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCODPAD) and !this.bDontReportError
            deferred_cp_zoom('VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(oSource.parent,'oELCODPAD_1_14'),i_cWhere,'GSTE_AEL',"Elementi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ELCODSTR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, elemento padre non corretto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ELCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',oSource.xKey(1);
                       ,'ELCODICE',oSource.xKey(2))
            select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODPAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where ELCODICE="+cp_ToStrODBC(this.w_ELCODPAD);
                   +" and ELCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ELCODSTR',this.w_ELCODSTR;
                       ,'ELCODICE',this.w_ELCODPAD)
            select ELCODSTR,ELCODICE,ELDESCRI,EL__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODPAD = NVL(_Link_.ELCODICE,space(20))
      this.w_DESPAD = NVL(_Link_.ELDESCRI,space(50))
      this.w_TIPO = NVL(_Link_.EL__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODPAD = space(20)
      endif
      this.w_DESPAD = space(50)
      this.w_TIPO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_ELCODPAD <> .w_ELCODICE
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, elemento padre non corretto")
        endif
        this.w_ELCODPAD = space(20)
        this.w_DESPAD = space(50)
        this.w_TIPO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])+'\'+cp_ToStr(_Link_.ELCODSTR,1)+'\'+cp_ToStr(_Link_.ELCODICE,1)
      cp_ShowWarn(i_cKey,this.VAELEMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODPAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VAELEMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VAELEMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_14.ELCODICE as ELCODICE114"+ ",link_1_14.ELDESCRI as ELDESCRI114"+ ",link_1_14.EL__TIPO as EL__TIPO114"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_14 on VAELEMEN.ELCODPAD=link_1_14.ELCODICE"+" and VAELEMEN.ELCODSTR=link_1_14.ELCODSTR"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_14"
          i_cKey=i_cKey+'+" and VAELEMEN.ELCODPAD=link_1_14.ELCODICE(+)"'+'+" and VAELEMEN.ELCODSTR=link_1_14.ELCODSTR(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCODFOR
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VAFORMAT_IDX,3]
    i_lTable = "VAFORMAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2], .t., this.VAFORMAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'VAFORMAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FOCODICE like "+cp_ToStrODBC(trim(this.w_ELCODFOR)+"%");
                   +" and FOCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);

          i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FOCODSTR,FOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FOCODSTR',this.w_ELCODSTR;
                     ,'FOCODICE',trim(this.w_ELCODFOR))
          select FOCODSTR,FOCODICE,FODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FOCODSTR,FOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODFOR)==trim(_Link_.FOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" FODESCRI like "+cp_ToStrODBC(trim(this.w_ELCODFOR)+"%");
                   +" and FOCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);

            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FODESCRI like "+cp_ToStr(trim(this.w_ELCODFOR)+"%");
                   +" and FOCODSTR="+cp_ToStr(this.w_ELCODSTR);

            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ELCODFOR) and !this.bDontReportError
            deferred_cp_zoom('VAFORMAT','*','FOCODSTR,FOCODICE',cp_AbsName(oSource.parent,'oELCODFOR_1_16'),i_cWhere,'',"Formati",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ELCODSTR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and FOCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODSTR',oSource.xKey(1);
                       ,'FOCODICE',oSource.xKey(2))
            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODSTR,FOCODICE,FODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(this.w_ELCODFOR);
                   +" and FOCODSTR="+cp_ToStrODBC(this.w_ELCODSTR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODSTR',this.w_ELCODSTR;
                       ,'FOCODICE',this.w_ELCODFOR)
            select FOCODSTR,FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODFOR = NVL(_Link_.FOCODICE,space(20))
      this.w_DESFOR = NVL(_Link_.FODESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODFOR = space(20)
      endif
      this.w_DESFOR = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])+'\'+cp_ToStr(_Link_.FOCODSTR,1)+'\'+cp_ToStr(_Link_.FOCODICE,1)
      cp_ShowWarn(i_cKey,this.VAFORMAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VAFORMAT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VAFORMAT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.FOCODICE as FOCODICE116"+ ",link_1_16.FODESCRI as FODESCRI116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on VAELEMEN.ELCODFOR=link_1_16.FOCODICE"+" and VAELEMEN.ELCODSTR=link_1_16.FOCODSTR"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and VAELEMEN.ELCODFOR=link_1_16.FOCODICE(+)"'+'+" and VAELEMEN.ELCODSTR=link_1_16.FOCODSTR(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELCODTAB
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENT_DETT_IDX,3]
    i_lTable = "ENT_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2], .t., this.ENT_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODTAB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ENT_DETT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EN_TABLE like "+cp_ToStrODBC(trim(this.w_ELCODTAB)+"%");
                   +" and ENCODICE="+cp_ToStrODBC(this.w_ELCODENT);

          i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ENCODICE,EN_TABLE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ENCODICE',this.w_ELCODENT;
                     ,'EN_TABLE',trim(this.w_ELCODTAB))
          select ENCODICE,EN_TABLE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ENCODICE,EN_TABLE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODTAB)==trim(_Link_.EN_TABLE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCODTAB) and !this.bDontReportError
            deferred_cp_zoom('ENT_DETT','*','ENCODICE,EN_TABLE',cp_AbsName(oSource.parent,'oELCODTAB_2_4'),i_cWhere,'',"Entit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ELCODENT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ENCODICE,EN_TABLE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                     +" from "+i_cTable+" "+i_lTable+" where EN_TABLE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ENCODICE="+cp_ToStrODBC(this.w_ELCODENT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',oSource.xKey(1);
                       ,'EN_TABLE',oSource.xKey(2))
            select ENCODICE,EN_TABLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODTAB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                   +" from "+i_cTable+" "+i_lTable+" where EN_TABLE="+cp_ToStrODBC(this.w_ELCODTAB);
                   +" and ENCODICE="+cp_ToStrODBC(this.w_ELCODENT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',this.w_ELCODENT;
                       ,'EN_TABLE',this.w_ELCODTAB)
            select ENCODICE,EN_TABLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODTAB = NVL(_Link_.EN_TABLE,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODTAB = space(30)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])+'\'+cp_ToStr(_Link_.ENCODICE,1)+'\'+cp_ToStr(_Link_.EN_TABLE,1)
      cp_ShowWarn(i_cKey,this.ENT_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODTAB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=EL_CAMPO
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_FIELDS_IDX,3]
    i_lTable = "XDC_FIELDS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2], .t., this.XDC_FIELDS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_EL_CAMPO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_FIELDS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FLNAME like "+cp_ToStrODBC(trim(this.w_EL_CAMPO)+"%");
                   +" and TBNAME="+cp_ToStrODBC(this.w_ELCODTAB);

          i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME,FLNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',this.w_ELCODTAB;
                     ,'FLNAME',trim(this.w_EL_CAMPO))
          select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME,FLNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_EL_CAMPO)==trim(_Link_.FLNAME) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_EL_CAMPO) and !this.bDontReportError
            deferred_cp_zoom('XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(oSource.parent,'oEL_CAMPO_2_5'),i_cWhere,'',"Campi tabella",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ELCODTAB<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN";
                     +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TBNAME="+cp_ToStrODBC(this.w_ELCODTAB);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1);
                       ,'FLNAME',oSource.xKey(2))
            select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_EL_CAMPO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN";
                   +" from "+i_cTable+" "+i_lTable+" where FLNAME="+cp_ToStrODBC(this.w_EL_CAMPO);
                   +" and TBNAME="+cp_ToStrODBC(this.w_ELCODTAB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_ELCODTAB;
                       ,'FLNAME',this.w_EL_CAMPO)
            select TBNAME,FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_EL_CAMPO = NVL(_Link_.FLNAME,space(30))
      this.w_ELTIPCAM = NVL(_Link_.FLTYPE,space(1))
      this.w_LUNMAX = NVL(_Link_.FLLENGHT,0)
      this.w_DECIMAL = NVL(_Link_.FLDECIMA,0)
      this.w_DESCAMP = NVL(_Link_.FLCOMMEN,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_EL_CAMPO = space(30)
      endif
      this.w_ELTIPCAM = space(1)
      this.w_LUNMAX = 0
      this.w_DECIMAL = 0
      this.w_DESCAMP = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_FIELDS_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)+'\'+cp_ToStr(_Link_.FLNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_FIELDS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_EL_CAMPO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ELCODCLA
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELCODCLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MCD',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_ELCODCLA)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDRIFTAB,CDDESCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_ELCODCLA))
          select CDCODCLA,CDRIFTAB,CDDESCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELCODCLA)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELCODCLA) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oELCODCLA_2_6'),i_cWhere,'GSUT_MCD',"Classi documentali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDRIFTAB,CDDESCLA";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDRIFTAB,CDDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELCODCLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDRIFTAB,CDDESCLA";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_ELCODCLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_ELCODCLA)
            select CDCODCLA,CDRIFTAB,CDDESCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELCODCLA = NVL(_Link_.CDCODCLA,space(15))
      this.w_RIFTAB = NVL(_Link_.CDRIFTAB,space(20))
      this.w_DESCLA = NVL(_Link_.CDDESCLA,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ELCODCLA = space(15)
      endif
      this.w_RIFTAB = space(20)
      this.w_DESCLA = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Alltrim(.w_RIFTAB)=Alltrim(.w_ELCODTAB)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe con riferimento tabella incongruente")
        endif
        this.w_ELCODCLA = space(15)
        this.w_RIFTAB = space(20)
        this.w_DESCLA = space(50)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELCODCLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PROMCLAS_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_6.CDCODCLA as CDCODCLA206"+ ",link_2_6.CDRIFTAB as CDRIFTAB206"+ ",link_2_6.CDDESCLA as CDDESCLA206"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_6 on VAELEMEN.ELCODCLA=link_2_6.CDCODCLA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_6"
          i_cKey=i_cKey+'+" and VAELEMEN.ELCODCLA=link_2_6.CDCODCLA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ELTABIMP
  func Link_3_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ENT_DETT_IDX,3]
    i_lTable = "ENT_DETT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2], .t., this.ENT_DETT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ELTABIMP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ENT_DETT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EN_TABLE like "+cp_ToStrODBC(trim(this.w_ELTABIMP)+"%");
                   +" and ENCODICE="+cp_ToStrODBC(this.w_ELCODENT);

          i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ENCODICE,EN_TABLE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ENCODICE',this.w_ELCODENT;
                     ,'EN_TABLE',trim(this.w_ELTABIMP))
          select ENCODICE,EN_TABLE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ENCODICE,EN_TABLE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ELTABIMP)==trim(_Link_.EN_TABLE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ELTABIMP) and !this.bDontReportError
            deferred_cp_zoom('ENT_DETT','*','ENCODICE,EN_TABLE',cp_AbsName(oSource.parent,'oELTABIMP_3_2'),i_cWhere,'',"Tabelle",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ELCODENT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ENCODICE,EN_TABLE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                     +" from "+i_cTable+" "+i_lTable+" where EN_TABLE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ENCODICE="+cp_ToStrODBC(this.w_ELCODENT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',oSource.xKey(1);
                       ,'EN_TABLE',oSource.xKey(2))
            select ENCODICE,EN_TABLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ELTABIMP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ENCODICE,EN_TABLE";
                   +" from "+i_cTable+" "+i_lTable+" where EN_TABLE="+cp_ToStrODBC(this.w_ELTABIMP);
                   +" and ENCODICE="+cp_ToStrODBC(this.w_ELCODENT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ENCODICE',this.w_ELCODENT;
                       ,'EN_TABLE',this.w_ELTABIMP)
            select ENCODICE,EN_TABLE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ELTABIMP = NVL(_Link_.EN_TABLE,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ELTABIMP = space(30)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ENT_DETT_IDX,2])+'\'+cp_ToStr(_Link_.ENCODICE,1)+'\'+cp_ToStr(_Link_.EN_TABLE,1)
      cp_ShowWarn(i_cKey,this.ENT_DETT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ELTABIMP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oELCODSTR_1_1.value==this.w_ELCODSTR)
      this.oPgFrm.Page1.oPag.oELCODSTR_1_1.value=this.w_ELCODSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODICE_1_3.value==this.w_ELCODICE)
      this.oPgFrm.Page1.oPag.oELCODICE_1_3.value=this.w_ELCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oELDESCRI_1_7.value==this.w_ELDESCRI)
      this.oPgFrm.Page1.oPag.oELDESCRI_1_7.value=this.w_ELDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oELORDINE_1_9.value==this.w_ELORDINE)
      this.oPgFrm.Page1.oPag.oELORDINE_1_9.value=this.w_ELORDINE
    endif
    if not(this.oPgFrm.Page1.oPag.oELMODELE_1_10.RadioValue()==this.w_ELMODELE)
      this.oPgFrm.Page1.oPag.oELMODELE_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELIDCHLD_1_11.value==this.w_ELIDCHLD)
      this.oPgFrm.Page1.oPag.oELIDCHLD_1_11.value=this.w_ELIDCHLD
    endif
    if not(this.oPgFrm.Page1.oPag.oEL__TIPO_1_12.RadioValue()==this.w_EL__TIPO)
      this.oPgFrm.Page1.oPag.oEL__TIPO_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODPAD_1_14.value==this.w_ELCODPAD)
      this.oPgFrm.Page1.oPag.oELCODPAD_1_14.value=this.w_ELCODPAD
    endif
    if not(this.oPgFrm.Page1.oPag.oELCODFOR_1_16.value==this.w_ELCODFOR)
      this.oPgFrm.Page1.oPag.oELCODFOR_1_16.value=this.w_ELCODFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oELLUNMAX_1_17.value==this.w_ELLUNMAX)
      this.oPgFrm.Page1.oPag.oELLUNMAX_1_17.value=this.w_ELLUNMAX
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSTR_1_24.value==this.w_DESSTR)
      this.oPgFrm.Page1.oPag.oDESSTR_1_24.value=this.w_DESSTR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPAD_1_25.value==this.w_DESPAD)
      this.oPgFrm.Page1.oPag.oDESPAD_1_25.value=this.w_DESPAD
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_26.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_26.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oELVALTXT_1_34.value==this.w_ELVALTXT)
      this.oPgFrm.Page1.oPag.oELVALTXT_1_34.value=this.w_ELVALTXT
    endif
    if not(this.oPgFrm.Page1.oPag.oELINISEZ_1_35.RadioValue()==this.w_ELINISEZ)
      this.oPgFrm.Page1.oPag.oELINISEZ_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELORDASS_1_36.value==this.w_ELORDASS)
      this.oPgFrm.Page1.oPag.oELORDASS_1_36.value=this.w_ELORDASS
    endif
    if not(this.oPgFrm.Page1.oPag.oELVALTX2_1_37.value==this.w_ELVALTX2)
      this.oPgFrm.Page1.oPag.oELVALTX2_1_37.value=this.w_ELVALTX2
    endif
    if not(this.oPgFrm.Page1.oPag.oELVALTX3_1_38.value==this.w_ELVALTX3)
      this.oPgFrm.Page1.oPag.oELVALTX3_1_38.value=this.w_ELVALTX3
    endif
    if not(this.oPgFrm.Page1.oPag.oEL__MASK_1_40.value==this.w_EL__MASK)
      this.oPgFrm.Page1.oPag.oEL__MASK_1_40.value=this.w_EL__MASK
    endif
    if not(this.oPgFrm.Page2.oPag.oELFUNSCE_2_3.value==this.w_ELFUNSCE)
      this.oPgFrm.Page2.oPag.oELFUNSCE_2_3.value=this.w_ELFUNSCE
    endif
    if not(this.oPgFrm.Page2.oPag.oELCODTAB_2_4.value==this.w_ELCODTAB)
      this.oPgFrm.Page2.oPag.oELCODTAB_2_4.value=this.w_ELCODTAB
    endif
    if not(this.oPgFrm.Page2.oPag.oEL_CAMPO_2_5.value==this.w_EL_CAMPO)
      this.oPgFrm.Page2.oPag.oEL_CAMPO_2_5.value=this.w_EL_CAMPO
    endif
    if not(this.oPgFrm.Page2.oPag.oELCODCLA_2_6.value==this.w_ELCODCLA)
      this.oPgFrm.Page2.oPag.oELCODCLA_2_6.value=this.w_ELCODCLA
    endif
    if not(this.oPgFrm.Page2.oPag.oELTIPCAM_2_7.RadioValue()==this.w_ELTIPCAM)
      this.oPgFrm.Page2.oPag.oELTIPCAM_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oELALLINE_2_8.RadioValue()==this.w_ELALLINE)
      this.oPgFrm.Page2.oPag.oELALLINE_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oELZERFIL_2_9.RadioValue()==this.w_ELZERFIL)
      this.oPgFrm.Page2.oPag.oELZERFIL_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oELCARFIL_2_10.value==this.w_ELCARFIL)
      this.oPgFrm.Page2.oPag.oELCARFIL_2_10.value=this.w_ELCARFIL
    endif
    if not(this.oPgFrm.Page2.oPag.oEL_SEGN1_2_11.RadioValue()==this.w_EL_SEGN1)
      this.oPgFrm.Page2.oPag.oEL_SEGN1_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEL_FLPIU_2_12.RadioValue()==this.w_EL_FLPIU)
      this.oPgFrm.Page2.oPag.oEL_FLPIU_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oELTIPTRA_2_13.RadioValue()==this.w_ELTIPTRA)
      this.oPgFrm.Page2.oPag.oELTIPTRA_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oEL_BATCH_2_14.value==this.w_EL_BATCH)
      this.oPgFrm.Page2.oPag.oEL_BATCH_2_14.value=this.w_EL_BATCH
    endif
    if not(this.oPgFrm.Page2.oPag.oELCONDIT_2_15.value==this.w_ELCONDIT)
      this.oPgFrm.Page2.oPag.oELCONDIT_2_15.value=this.w_ELCONDIT
    endif
    if not(this.oPgFrm.Page2.oPag.oELCONWHE_2_16.value==this.w_ELCONWHE)
      this.oPgFrm.Page2.oPag.oELCONWHE_2_16.value=this.w_ELCONWHE
    endif
    if not(this.oPgFrm.Page2.oPag.oELEXPRES_2_26.value==this.w_ELEXPRES)
      this.oPgFrm.Page2.oPag.oELEXPRES_2_26.value=this.w_ELEXPRES
    endif
    if not(this.oPgFrm.Page3.oPag.oELFUNSC2_3_1.value==this.w_ELFUNSC2)
      this.oPgFrm.Page3.oPag.oELFUNSC2_3_1.value=this.w_ELFUNSC2
    endif
    if not(this.oPgFrm.Page3.oPag.oELTABIMP_3_2.value==this.w_ELTABIMP)
      this.oPgFrm.Page3.oPag.oELTABIMP_3_2.value=this.w_ELTABIMP
    endif
    if not(this.oPgFrm.Page3.oPag.oELCAMIMP_3_3.value==this.w_ELCAMIMP)
      this.oPgFrm.Page3.oPag.oELCAMIMP_3_3.value=this.w_ELCAMIMP
    endif
    if not(this.oPgFrm.Page3.oPag.oELMEMDAT_3_4.RadioValue()==this.w_ELMEMDAT)
      this.oPgFrm.Page3.oPag.oELMEMDAT_3_4.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oELQUALIF_3_5.RadioValue()==this.w_ELQUALIF)
      this.oPgFrm.Page3.oPag.oELQUALIF_3_5.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oELPOSQUA_3_6.value==this.w_ELPOSQUA)
      this.oPgFrm.Page3.oPag.oELPOSQUA_3_6.value=this.w_ELPOSQUA
    endif
    if not(this.oPgFrm.Page3.oPag.oELALLIMP_3_7.RadioValue()==this.w_ELALLIMP)
      this.oPgFrm.Page3.oPag.oELALLIMP_3_7.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oELCARIMP_3_8.value==this.w_ELCARIMP)
      this.oPgFrm.Page3.oPag.oELCARIMP_3_8.value=this.w_ELCARIMP
    endif
    if not(this.oPgFrm.Page3.oPag.oEL_SEGNO_3_9.RadioValue()==this.w_EL_SEGNO)
      this.oPgFrm.Page3.oPag.oEL_SEGNO_3_9.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oELTIPTR2_3_10.RadioValue()==this.w_ELTIPTR2)
      this.oPgFrm.Page3.oPag.oELTIPTR2_3_10.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oEL_BATC2_3_11.value==this.w_EL_BATC2)
      this.oPgFrm.Page3.oPag.oEL_BATC2_3_11.value=this.w_EL_BATC2
    endif
    if not(this.oPgFrm.Page3.oPag.oDESSTR_3_21.value==this.w_DESSTR)
      this.oPgFrm.Page3.oPag.oDESSTR_3_21.value=this.w_DESSTR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSTR_2_28.value==this.w_DESSTR)
      this.oPgFrm.Page2.oPag.oDESSTR_2_28.value=this.w_DESSTR
    endif
    if not(this.oPgFrm.Page2.oPag.oELCOS_2_31.value==this.w_ELCOS)
      this.oPgFrm.Page2.oPag.oELCOS_2_31.value=this.w_ELCOS
    endif
    if not(this.oPgFrm.Page2.oPag.oELCOE_2_32.value==this.w_ELCOE)
      this.oPgFrm.Page2.oPag.oELCOE_2_32.value=this.w_ELCOE
    endif
    if not(this.oPgFrm.Page2.oPag.oELDE_2_33.value==this.w_ELDE)
      this.oPgFrm.Page2.oPag.oELDE_2_33.value=this.w_ELDE
    endif
    if not(this.oPgFrm.Page3.oPag.oELCOS1_3_24.value==this.w_ELCOS1)
      this.oPgFrm.Page3.oPag.oELCOS1_3_24.value=this.w_ELCOS1
    endif
    if not(this.oPgFrm.Page3.oPag.oELCOE1_3_25.value==this.w_ELCOE1)
      this.oPgFrm.Page3.oPag.oELCOE1_3_25.value=this.w_ELCOE1
    endif
    if not(this.oPgFrm.Page3.oPag.oELDE1_3_26.value==this.w_ELDE1)
      this.oPgFrm.Page3.oPag.oELDE1_3_26.value=this.w_ELDE1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAMP_2_34.value==this.w_DESCAMP)
      this.oPgFrm.Page2.oPag.oDESCAMP_2_34.value=this.w_DESCAMP
    endif
    if not(this.oPgFrm.Page3.oPag.oELDESC_3_28.value==this.w_ELDESC)
      this.oPgFrm.Page3.oPag.oELDESC_3_28.value=this.w_ELDESC
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCLA_2_35.value==this.w_DESCLA)
      this.oPgFrm.Page2.oPag.oDESCLA_2_35.value=this.w_DESCLA
    endif
    if not(this.oPgFrm.Page3.oPag.oELEXPRE1_3_30.value==this.w_ELEXPRE1)
      this.oPgFrm.Page3.oPag.oELEXPRE1_3_30.value=this.w_ELEXPRE1
    endif
    cp_SetControlsValueExtFlds(this,'VAELEMEN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_ELCODSTR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELCODSTR_1_1.SetFocus()
            i_bnoObbl = !empty(.w_ELCODSTR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ELCODICE)) or not(CHKSTRNUM(.w_ELCODICE,'_') OR CHKSTRNUM(.w_ELCODICE,'-') OR CHKSTRNUM(.w_ELCODICE,'.')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELCODICE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_ELCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione! Il codice elemento contiene caratteri non ammessi!")
          case   (empty(.w_ELIDCHLD))  and not(.w_TIPO<>'S')  and (.w_TIPO='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELIDCHLD_1_11.SetFocus()
            i_bnoObbl = !empty(.w_ELIDCHLD)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_ELCODPAD <> .w_ELCODICE)  and not(empty(.w_ELCODPAD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELCODPAD_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, elemento padre non corretto")
          case   (empty(.w_ELCODFOR))  and (.w_EL__TIPO<>'G')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oELCODFOR_1_16.SetFocus()
            i_bnoObbl = !empty(.w_ELCODFOR)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ELFUNSCE))  and not(.w_EL__TIPO<>'S')  and (.w_STFLTYPE<>'I' AND .w_EL__TIPO='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELFUNSCE_2_3.SetFocus()
            i_bnoObbl = !empty(.w_ELFUNSCE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(Alltrim(.w_RIFTAB)=Alltrim(.w_ELCODTAB))  and not(.w_EL__TIPO<>'L')  and (Not empty(.w_ELCODTAB))  and not(empty(.w_ELCODCLA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELCODCLA_2_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe con riferimento tabella incongruente")
          case   (empty(.w_ELCARFIL))  and not(.w_STFLTYPE='I'  OR .w_ELZERFIL='N' OR .w_EL__TIPO='L')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELCARFIL_2_10.SetFocus()
            i_bnoObbl = !empty(.w_ELCARFIL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Carattere di riempimento non inserito")
          case   (empty(.w_ELEXPRES))  and (.w_EL__TIPO='E')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oELEXPRES_2_26.SetFocus()
            i_bnoObbl = !empty(.w_ELEXPRES)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ELFUNSC2))  and not(.w_EL__TIPO<>'S')  and (.w_STFLTYPE<>'O' AND .w_EL__TIPO='S')
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oELFUNSC2_3_1.SetFocus()
            i_bnoObbl = !empty(.w_ELFUNSC2)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_EL__TIPO = this.w_EL__TIPO
    this.o_ELVALTXT = this.w_ELVALTXT
    this.o_ELVALTX2 = this.w_ELVALTX2
    this.o_ELVALTX3 = this.w_ELVALTX3
    this.o_ELCODTAB = this.w_ELCODTAB
    this.o_EL_CAMPO = this.w_EL_CAMPO
    this.o_ELTIPCAM = this.w_ELTIPCAM
    this.o_ELALLINE = this.w_ELALLINE
    this.o_ELZERFIL = this.w_ELZERFIL
    this.o_EL_SEGN1 = this.w_EL_SEGN1
    this.o_ELTIPTRA = this.w_ELTIPTRA
    this.o_ELTABIMP = this.w_ELTABIMP
    this.o_ELCAMIMP = this.w_ELCAMIMP
    this.o_ELTIPTR2 = this.w_ELTIPTR2
    return

  func CanEdit()
    local i_res
    i_res=this.w_FLPROV='S'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attenzione, struttura confermata impossibile modificare"))
    endif
    return(i_res)
  func CanAdd()
    local i_res
    i_res=Type('g_VEFA')='C' and g_VEFA='S'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione non consentita"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=this.w_FLPROV='S'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Attenzione, struttura confermata impossibile eliminare"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgste_aelPag1 as StdContainer
  Width  = 733
  height = 394
  stdWidth  = 733
  stdheight = 394
  resizeXpos=426
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oELCODSTR_1_1 as StdField with uid="CNHSUULGRU",rtseq=1,rtrep=.f.,;
    cFormVar = "w_ELCODSTR", cQueryName = "ELCODSTR",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice struttura",;
    HelpContextID = 217494376,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=135, Top=23, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="VASTRUTT", oKey_1_1="STCODICE", oKey_1_2="this.w_ELCODSTR"

  func oELCODSTR_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
      if .not. empty(.w_ELCODPAD)
        bRes2=.link_1_14('Full')
      endif
      if .not. empty(.w_ELCODFOR)
        bRes2=.link_1_16('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oELCODSTR_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODSTR_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VASTRUTT','*','STCODICE',cp_AbsName(this.parent,'oELCODSTR_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Strutture",'',this.parent.oContained
  endproc

  add object oELCODICE_1_3 as StdField with uid="ZJELZMIDFF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ELCODICE", cQueryName = "ELCODSTR,ELCODICE",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione! Il codice elemento contiene caratteri non ammessi!",;
    ToolTipText = "Codice elemento",;
    HelpContextID = 116831093,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=135, Top=52, InputMask=replicate('X',20)

  func oELCODICE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKSTRNUM(.w_ELCODICE,'_') OR CHKSTRNUM(.w_ELCODICE,'-') OR CHKSTRNUM(.w_ELCODICE,'.'))
    endwith
    return bRes
  endfunc

  add object oELDESCRI_1_7 as StdField with uid="LDHTJXCLBT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ELDESCRI", cQueryName = "ELDESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione elemento",;
    HelpContextID = 202417009,;
   bGlobalFont=.t.,;
    Height=21, Width=434, Left=292, Top=52, InputMask=replicate('X',50)

  add object oELORDINE_1_9 as StdField with uid="ADZGIETKED",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ELORDINE", cQueryName = "ELORDINE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ordine rispetto ai fratelli",;
    HelpContextID = 151850123,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=134, Top=81


  add object oELMODELE_1_10 as StdCombo with uid="YQMECKXJRL",rtseq=8,rtrep=.f.,left=599,top=81,width=127,height=21;
    , ToolTipText = "Modalit� di collegamento dell'elemento figlio";
    , HelpContextID = 84536459;
    , cFormVar="w_ELMODELE",RowSource=""+"Attributo,"+"Elemento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oELMODELE_1_10.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oELMODELE_1_10.GetRadio()
    this.Parent.oContained.w_ELMODELE = this.RadioValue()
    return .t.
  endfunc

  func oELMODELE_1_10.SetRadio()
    this.Parent.oContained.w_ELMODELE=trim(this.Parent.oContained.w_ELMODELE)
    this.value = ;
      iif(this.Parent.oContained.w_ELMODELE=='A',1,;
      iif(this.Parent.oContained.w_ELMODELE=='E',2,;
      0))
  endfunc

  func oELMODELE_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPFIL='X')
    endwith
   endif
  endfunc

  func oELMODELE_1_10.mHide()
    with this.Parent.oContained
      return (.w_TIPFIL<>'X')
    endwith
  endfunc

  add object oELIDCHLD_1_11 as StdField with uid="PHSXGURRBC",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ELIDCHLD", cQueryName = "ELIDCHLD",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Identificativo numerico nel caso di padre di tipo scelta",;
    HelpContextID = 133082250,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=135, Top=110, cSayPict='"999"', cGetPict='"999"'

  func oELIDCHLD_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPO='S')
    endwith
   endif
  endfunc

  func oELIDCHLD_1_11.mHide()
    with this.Parent.oContained
      return (.w_TIPO<>'S')
    endwith
  endfunc


  add object oEL__TIPO_1_12 as StdCombo with uid="QPIHKQEYJQ",rtseq=10,rtrep=.f.,left=135,top=139,width=153,height=21;
    , ToolTipText = "Tipo ( contenitore valore )";
    , HelpContextID = 169544853;
    , cFormVar="w_EL__TIPO",RowSource=""+"Valore,"+"Espressione,"+"Fisso,"+"Variabile,"+"Reiterabile,"+"Apertura file,"+"Chiusura file,"+"Apertura,"+"Chiusura,"+"Scelta,"+"Allegato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oEL__TIPO_1_12.RadioValue()
    return(iif(this.value =1,'V',;
    iif(this.value =2,'E',;
    iif(this.value =3,'G',;
    iif(this.value =4,'T',;
    iif(this.value =5,'D',;
    iif(this.value =6,'A',;
    iif(this.value =7,'C',;
    iif(this.value =8,'O',;
    iif(this.value =9,'K',;
    iif(this.value =10,'S',;
    iif(this.value =11,'L',;
    space(1)))))))))))))
  endfunc
  func oEL__TIPO_1_12.GetRadio()
    this.Parent.oContained.w_EL__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oEL__TIPO_1_12.SetRadio()
    this.Parent.oContained.w_EL__TIPO=trim(this.Parent.oContained.w_EL__TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_EL__TIPO=='V',1,;
      iif(this.Parent.oContained.w_EL__TIPO=='E',2,;
      iif(this.Parent.oContained.w_EL__TIPO=='G',3,;
      iif(this.Parent.oContained.w_EL__TIPO=='T',4,;
      iif(this.Parent.oContained.w_EL__TIPO=='D',5,;
      iif(this.Parent.oContained.w_EL__TIPO=='A',6,;
      iif(this.Parent.oContained.w_EL__TIPO=='C',7,;
      iif(this.Parent.oContained.w_EL__TIPO=='O',8,;
      iif(this.Parent.oContained.w_EL__TIPO=='K',9,;
      iif(this.Parent.oContained.w_EL__TIPO=='S',10,;
      iif(this.Parent.oContained.w_EL__TIPO=='L',11,;
      0)))))))))))
  endfunc

  add object oELCODPAD_1_14 as StdField with uid="WOPQIHWIKW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ELCODPAD", cQueryName = "ELCODPAD",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, elemento padre non corretto",;
    ToolTipText = "Riferimento elemento padre",;
    HelpContextID = 267826038,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=135, Top=164, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="VAELEMEN", cZoomOnZoom="GSTE_AEL", oKey_1_1="ELCODSTR", oKey_1_2="this.w_ELCODSTR", oKey_2_1="ELCODICE", oKey_2_2="this.w_ELCODPAD"

  func oELCODPAD_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODPAD_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODPAD_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VAELEMEN_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStrODBC(this.Parent.oContained.w_ELCODSTR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ELCODSTR="+cp_ToStr(this.Parent.oContained.w_ELCODSTR)
    endif
    do cp_zoom with 'VAELEMEN','*','ELCODSTR,ELCODICE',cp_AbsName(this.parent,'oELCODPAD_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_AEL',"Elementi",'',this.parent.oContained
  endproc
  proc oELCODPAD_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSTE_AEL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ELCODSTR=w_ELCODSTR
     i_obj.w_ELCODICE=this.parent.oContained.w_ELCODPAD
     i_obj.ecpSave()
  endproc

  add object oELCODFOR_1_16 as StdField with uid="NTMZVEGTQK",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ELCODFOR", cQueryName = "ELCODFOR",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice formato",;
    HelpContextID = 101272728,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=135, Top=193, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="VAFORMAT", oKey_1_1="FOCODSTR", oKey_1_2="this.w_ELCODSTR", oKey_2_1="FOCODICE", oKey_2_2="this.w_ELCODFOR"

  func oELCODFOR_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EL__TIPO<>'G')
    endwith
   endif
  endfunc

  func oELCODFOR_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODFOR_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODFOR_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.VAFORMAT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FOCODSTR="+cp_ToStrODBC(this.Parent.oContained.w_ELCODSTR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"FOCODSTR="+cp_ToStr(this.Parent.oContained.w_ELCODSTR)
    endif
    do cp_zoom with 'VAFORMAT','*','FOCODSTR,FOCODICE',cp_AbsName(this.parent,'oELCODFOR_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Formati",'',this.parent.oContained
  endproc

  add object oELLUNMAX_1_17 as StdField with uid="VTDHUTCUBF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_ELLUNMAX", cQueryName = "ELLUNMAX",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Lunghezza massima del campo\espressione",;
    HelpContextID = 38806370,;
   bGlobalFont=.t.,;
    Height=21, Width=59, Left=135, Top=222, cSayPict="'99999'", cGetPict="'99999'"

  func oELLUNMAX_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_TIPFIL='L' AND Empty(.w_ELVALTXT)) or ((.w_ELLUNMAX=0 or Not Empty(.w_EL_CAMPO) or Not Empty(.w_ELCAMIMP)or .w_EL__TIPO='E') and  .w_TIPFIL<> 'L' ))
    endwith
   endif
  endfunc

  func oELLUNMAX_1_17.mHide()
    with this.Parent.oContained
      return (Not(.w_EL__TIPO $ 'E-V') and .w_TIPFIL<>'L')
    endwith
  endfunc

  add object oDESSTR_1_24 as StdField with uid="FFTHBYFFRS",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESSTR", cQueryName = "DESSTR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 217168586,;
   bGlobalFont=.t.,;
    Height=21, Width=434, Left=292, Top=23, InputMask=replicate('X',30)

  add object oDESPAD_1_25 as StdField with uid="GFUQAIBRYB",rtseq=19,rtrep=.f.,;
    cFormVar = "w_DESPAD", cQueryName = "DESPAD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 64701750,;
   bGlobalFont=.t.,;
    Height=21, Width=434, Left=292, Top=164, InputMask=replicate('X',50)

  add object oDESFOR_1_26 as StdField with uid="UQVIHSGNNR",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 223263434,;
   bGlobalFont=.t.,;
    Height=21, Width=434, Left=292, Top=193, InputMask=replicate('X',30)

  add object oELVALTXT_1_34 as StdField with uid="WOBDNYJBXL",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ELVALTXT", cQueryName = "ELVALTXT",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Codice txt primario",;
    HelpContextID = 75267226,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=135, Top=251, InputMask=replicate('X',50)

  add object oELINISEZ_1_35 as StdCheck with uid="RWRHFDJUVR",rtseq=25,rtrep=.f.,left=372, top=251, caption="Elemento di rottura",;
    ToolTipText = "Se attivo identifica elemento di rottura",;
    HelpContextID = 212292448,;
    cFormVar="w_ELINISEZ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oELINISEZ_1_35.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oELINISEZ_1_35.GetRadio()
    this.Parent.oContained.w_ELINISEZ = this.RadioValue()
    return .t.
  endfunc

  func oELINISEZ_1_35.SetRadio()
    this.Parent.oContained.w_ELINISEZ=trim(this.Parent.oContained.w_ELINISEZ)
    this.value = ;
      iif(this.Parent.oContained.w_ELINISEZ=='S',1,;
      0)
  endfunc

  func oELINISEZ_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_ELVALTXT))
    endwith
   endif
  endfunc

  add object oELORDASS_1_36 as StdField with uid="MOBKRTMVFE",rtseq=26,rtrep=.f.,;
    cFormVar = "w_ELORDASS", cQueryName = "ELORDASS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ordinamento assoluto in import",;
    HelpContextID = 250803047,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=135, Top=280

  func oELORDASS_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_ELVALTXT))
    endwith
   endif
  endfunc

  add object oELVALTX2_1_37 as StdField with uid="XPWRQJQHHN",rtseq=27,rtrep=.f.,;
    cFormVar = "w_ELVALTX2", cQueryName = "ELVALTX2",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Codice txt secondario",;
    HelpContextID = 75267192,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=135, Top=309, InputMask=replicate('X',50)

  add object oELVALTX3_1_38 as StdField with uid="IIXPARNGJE",rtseq=28,rtrep=.f.,;
    cFormVar = "w_ELVALTX3", cQueryName = "ELVALTX3",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Codice txt descrittivo",;
    HelpContextID = 75267193,;
   bGlobalFont=.t.,;
    Height=21, Width=591, Left=134, Top=338, InputMask=replicate('X',254)

  add object oEL__MASK_1_40 as StdField with uid="XMFSVAPCGG",rtseq=30,rtrep=.f.,;
    cFormVar = "w_EL__MASK", cQueryName = "EL__MASK",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Maschera formato campo, per le date a anno M mese G giorno O ora T minuti S secondi",;
    HelpContextID = 240448367,;
   bGlobalFont=.t.,;
    Height=21, Width=591, Left=134, Top=367, InputMask=replicate('X',50)

  func oEL__MASK_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EL__TIPO='E' or .w_EL__TIPO='V')
    endwith
   endif
  endfunc

  add object oStr_1_2 as StdString with uid="WTFKWKCADK",Visible=.t., Left=-9, Top=25,;
    Alignment=1, Width=138, Height=18,;
    Caption="Codice struttura:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="EVSVGVQFPR",Visible=.t., Left=-9, Top=56,;
    Alignment=1, Width=138, Height=18,;
    Caption="Codice elemento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="BVBPKXMWTE",Visible=.t., Left=-9, Top=140,;
    Alignment=1, Width=138, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="CIFUSNBOQE",Visible=.t., Left=-9, Top=167,;
    Alignment=1, Width=138, Height=18,;
    Caption="Padre:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="YINFVNFTCT",Visible=.t., Left=-9, Top=196,;
    Alignment=1, Width=138, Height=18,;
    Caption="Formato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="PJIHNITBGC",Visible=.t., Left=-9, Top=254,;
    Alignment=1, Width=138, Height=18,;
    Caption="Id primario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="DKFUXAIYIU",Visible=.t., Left=-9, Top=312,;
    Alignment=1, Width=138, Height=18,;
    Caption="Id secondario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="KPOZNCGXQI",Visible=.t., Left=-9, Top=341,;
    Alignment=1, Width=138, Height=18,;
    Caption="Txt descrittivo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="JBEJKCAGZL",Visible=.t., Left=505, Top=82,;
    Alignment=1, Width=91, Height=18,;
    Caption="Modalit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (.w_TIPFIL<>'X')
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="ATOQPCQWMC",Visible=.t., Left=-9, Top=113,;
    Alignment=1, Width=138, Height=18,;
    Caption="Scelta num.:"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.w_TIPO<>'S')
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="EVMPWBEEQG",Visible=.t., Left=-9, Top=370,;
    Alignment=1, Width=138, Height=18,;
    Caption="Maschera campo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="UGZDGVGJZM",Visible=.t., Left=53, Top=84,;
    Alignment=1, Width=76, Height=18,;
    Caption="Ordine:"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='I')
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="KMSEOOYPNX",Visible=.t., Left=2, Top=283,;
    Alignment=1, Width=127, Height=18,;
    Caption="Ordine assoluto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="HPJCFBKRQO",Visible=.t., Left=47, Top=225,;
    Alignment=1, Width=82, Height=18,;
    Caption="Lung.:"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (Not(.w_EL__TIPO $ 'E-V') and .w_TIPFIL<>'L')
    endwith
  endfunc
enddefine
define class tgste_aelPag2 as StdContainer
  Width  = 733
  height = 394
  stdWidth  = 733
  stdheight = 394
  resizeXpos=555
  resizeYpos=366
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oELFUNSCE_2_3 as StdField with uid="SIXLHSEIXN",rtseq=33,rtrep=.f.,;
    cFormVar = "w_ELFUNSCE", cQueryName = "ELFUNSCE",;
    bObbl = .t. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Funzione di scelta elementi figli",;
    HelpContextID = 206603125,;
   bGlobalFont=.t.,;
    Height=21, Width=591, Left=136, Top=81, InputMask=replicate('X',254)

  func oELFUNSCE_2_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STFLTYPE<>'I' AND .w_EL__TIPO='S')
    endwith
   endif
  endfunc

  func oELFUNSCE_2_3.mHide()
    with this.Parent.oContained
      return (.w_EL__TIPO<>'S')
    endwith
  endfunc

  add object oELCODTAB_2_4 as StdField with uid="PKHLTSLJLN",rtseq=34,rtrep=.f.,;
    cFormVar = "w_ELCODTAB", cQueryName = "ELCODTAB",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Tabella (obbligatorio per valori e TAG ripetuti)",;
    HelpContextID = 200717176,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=136, Top=110, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="ENT_DETT", oKey_1_1="ENCODICE", oKey_1_2="this.w_ELCODENT", oKey_2_1="EN_TABLE", oKey_2_2="this.w_ELCODTAB"

  func oELCODTAB_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_EL__TIPO $ 'V-D-E-L' OR (Not Empty(.w_ELVALTXT))))
    endwith
   endif
  endfunc

  func oELCODTAB_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
      if .not. empty(.w_EL_CAMPO)
        bRes2=.link_2_5('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oELCODTAB_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODTAB_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ENT_DETT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ENCODICE="+cp_ToStrODBC(this.Parent.oContained.w_ELCODENT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ENCODICE="+cp_ToStr(this.Parent.oContained.w_ELCODENT)
    endif
    do cp_zoom with 'ENT_DETT','*','ENCODICE,EN_TABLE',cp_AbsName(this.parent,'oELCODTAB_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Entit�",'',this.parent.oContained
  endproc

  add object oEL_CAMPO_2_5 as StdField with uid="EJTGLTUTMT",rtseq=35,rtrep=.f.,;
    cFormVar = "w_EL_CAMPO", cQueryName = "EL_CAMPO",;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Campo associato al valore (editabile ed obbligatorio solo per i valori)",;
    HelpContextID = 214895765,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=136, Top=139, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="XDC_FIELDS", oKey_1_1="TBNAME", oKey_1_2="this.w_ELCODTAB", oKey_2_1="FLNAME", oKey_2_2="this.w_EL_CAMPO"

  func oEL_CAMPO_2_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STFLTYPE<>'I' AND Not empty(.w_ELCODTAB) and .w_EL__TIPO<>'D')
    endwith
   endif
  endfunc

  func oEL_CAMPO_2_5.mHide()
    with this.Parent.oContained
      return (.w_EL__TIPO='L')
    endwith
  endfunc

  func oEL_CAMPO_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oEL_CAMPO_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oEL_CAMPO_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.XDC_FIELDS_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStrODBC(this.Parent.oContained.w_ELCODTAB)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TBNAME="+cp_ToStr(this.Parent.oContained.w_ELCODTAB)
    endif
    do cp_zoom with 'XDC_FIELDS','*','TBNAME,FLNAME',cp_AbsName(this.parent,'oEL_CAMPO_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Campi tabella",'',this.parent.oContained
  endproc

  add object oELCODCLA_2_6 as StdField with uid="XDWIYIZHQC",rtseq=36,rtrep=.f.,;
    cFormVar = "w_ELCODCLA", cQueryName = "ELCODCLA",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe con riferimento tabella incongruente",;
    HelpContextID = 50941063,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=136, Top=139, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSUT_MCD", oKey_1_1="CDCODCLA", oKey_1_2="this.w_ELCODCLA"

  func oELCODCLA_2_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not empty(.w_ELCODTAB))
    endwith
   endif
  endfunc

  func oELCODCLA_2_6.mHide()
    with this.Parent.oContained
      return (.w_EL__TIPO<>'L')
    endwith
  endfunc

  func oELCODCLA_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oELCODCLA_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELCODCLA_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oELCODCLA_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MCD',"Classi documentali",'',this.parent.oContained
  endproc
  proc oELCODCLA_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_ELCODCLA
     i_obj.ecpSave()
  endproc


  add object oELTIPCAM_2_7 as StdCombo with uid="XORJBGKNSE",rtseq=37,rtrep=.f.,left=136,top=168,width=113,height=21;
    , ToolTipText = "Tipologia campo/espressione";
    , HelpContextID = 205235053;
    , cFormVar="w_ELTIPCAM",RowSource=""+"Numerico,"+"Data,"+"Carattere,"+"Memo", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oELTIPCAM_2_7.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'D',;
    iif(this.value =3,'C',;
    iif(this.value =4,'M',;
    space(1))))))
  endfunc
  func oELTIPCAM_2_7.GetRadio()
    this.Parent.oContained.w_ELTIPCAM = this.RadioValue()
    return .t.
  endfunc

  func oELTIPCAM_2_7.SetRadio()
    this.Parent.oContained.w_ELTIPCAM=trim(this.Parent.oContained.w_ELTIPCAM)
    this.value = ;
      iif(this.Parent.oContained.w_ELTIPCAM=='N',1,;
      iif(this.Parent.oContained.w_ELTIPCAM=='D',2,;
      iif(this.Parent.oContained.w_ELTIPCAM=='C',3,;
      iif(this.Parent.oContained.w_ELTIPCAM=='M',4,;
      0))))
  endfunc

  func oELTIPCAM_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EL__TIPO='E')
    endwith
   endif
  endfunc

  func oELTIPCAM_2_7.mHide()
    with this.Parent.oContained
      return (NOT(.w_EL__TIPO $ 'E-V'))
    endwith
  endfunc


  add object oELALLINE_2_8 as StdCombo with uid="XKFUSQGVVM",rtseq=38,rtrep=.f.,left=136,top=192,width=114,height=21;
    , ToolTipText = "Tipo allineamento campi numerici";
    , HelpContextID = 159788171;
    , cFormVar="w_ELALLINE",RowSource=""+"Destra,"+"Sinistra,"+"Nessuno", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oELALLINE_2_8.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oELALLINE_2_8.GetRadio()
    this.Parent.oContained.w_ELALLINE = this.RadioValue()
    return .t.
  endfunc

  func oELALLINE_2_8.SetRadio()
    this.Parent.oContained.w_ELALLINE=trim(this.Parent.oContained.w_ELALLINE)
    this.value = ;
      iif(this.Parent.oContained.w_ELALLINE=='D',1,;
      iif(this.Parent.oContained.w_ELALLINE=='S',2,;
      iif(this.Parent.oContained.w_ELALLINE=='N',3,;
      0)))
  endfunc

  func oELALLINE_2_8.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='I' OR Not .w_ELTIPCAM $ 'N-C-M' OR .w_EL__TIPO='L')
    endwith
  endfunc

  add object oELZERFIL_2_9 as StdCheck with uid="ROTXXXDSYN",rtseq=39,rtrep=.f.,left=260, top=192, caption="Fill",;
    ToolTipText = "filler sul campo\espresione numerico",;
    HelpContextID = 115391634,;
    cFormVar="w_ELZERFIL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oELZERFIL_2_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oELZERFIL_2_9.GetRadio()
    this.Parent.oContained.w_ELZERFIL = this.RadioValue()
    return .t.
  endfunc

  func oELZERFIL_2_9.SetRadio()
    this.Parent.oContained.w_ELZERFIL=trim(this.Parent.oContained.w_ELZERFIL)
    this.value = ;
      iif(this.Parent.oContained.w_ELZERFIL=='S',1,;
      0)
  endfunc

  func oELZERFIL_2_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ELALLINE<>'N')
    endwith
   endif
  endfunc

  func oELZERFIL_2_9.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='I' OR .w_ELALLINE= 'N' OR .w_EL__TIPO='L')
    endwith
  endfunc

  add object oELCARFIL_2_10 as StdField with uid="OVNBNTALOZ",rtseq=40,rtrep=.f.,;
    cFormVar = "w_ELCARFIL", cQueryName = "ELCARFIL",;
    bObbl = .t. , nPag = 2, value=space(1), bMultilanguage =  .f.,;
    sErrorMsg = "Carattere di riempimento non inserito",;
    ToolTipText = "Carattere di riempimento",;
    HelpContextID = 115035282,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=312, Top=192, InputMask=replicate('X',1)

  func oELCARFIL_2_10.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='I'  OR .w_ELZERFIL='N' OR .w_EL__TIPO='L')
    endwith
  endfunc


  add object oEL_SEGN1_2_11 as StdCombo with uid="UYMYUTZTBN",rtseq=41,rtrep=.f.,left=136,top=218,width=139,height=21;
    , ToolTipText = "Segno dei campi di tipo numerico";
    , HelpContextID = 119475319;
    , cFormVar="w_EL_SEGN1",RowSource=""+"Segno +,"+"Segno -,"+"Originario", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oEL_SEGN1_2_11.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'M',;
    iif(this.value =3,'O',;
    space(1)))))
  endfunc
  func oEL_SEGN1_2_11.GetRadio()
    this.Parent.oContained.w_EL_SEGN1 = this.RadioValue()
    return .t.
  endfunc

  func oEL_SEGN1_2_11.SetRadio()
    this.Parent.oContained.w_EL_SEGN1=trim(this.Parent.oContained.w_EL_SEGN1)
    this.value = ;
      iif(this.Parent.oContained.w_EL_SEGN1=='P',1,;
      iif(this.Parent.oContained.w_EL_SEGN1=='M',2,;
      iif(this.Parent.oContained.w_EL_SEGN1=='O',3,;
      0)))
  endfunc

  func oEL_SEGN1_2_11.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='I' OR .w_ELTIPCAM <>'N')
    endwith
  endfunc

  add object oEL_FLPIU_2_12 as StdCheck with uid="WQQGXOEQGN",rtseq=42,rtrep=.f.,left=287, top=218, caption="Evidenzia +",;
    ToolTipText = "Se attivo evidenzia segno + su importi maggiori di zero",;
    HelpContextID = 8522907,;
    cFormVar="w_EL_FLPIU", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oEL_FLPIU_2_12.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oEL_FLPIU_2_12.GetRadio()
    this.Parent.oContained.w_EL_FLPIU = this.RadioValue()
    return .t.
  endfunc

  func oEL_FLPIU_2_12.SetRadio()
    this.Parent.oContained.w_EL_FLPIU=trim(this.Parent.oContained.w_EL_FLPIU)
    this.value = ;
      iif(this.Parent.oContained.w_EL_FLPIU=='S',1,;
      0)
  endfunc

  func oEL_FLPIU_2_12.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='I' OR .w_EL_SEGN1='M' or .w_ELTIPCAM <>'N')
    endwith
  endfunc


  add object oELTIPTRA_2_13 as StdCombo with uid="HACABKANEL",rtseq=43,rtrep=.f.,left=136,top=244,width=139,height=21;
    , ToolTipText = "Tipo di trascodifica";
    , HelpContextID = 188457849;
    , cFormVar="w_ELTIPTRA",RowSource=""+"Da intestatario,"+"Da struttura,"+"Da routine,"+"Non gestita", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oELTIPTRA_2_13.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'S',;
    iif(this.value =3,'B',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oELTIPTRA_2_13.GetRadio()
    this.Parent.oContained.w_ELTIPTRA = this.RadioValue()
    return .t.
  endfunc

  func oELTIPTRA_2_13.SetRadio()
    this.Parent.oContained.w_ELTIPTRA=trim(this.Parent.oContained.w_ELTIPTRA)
    this.value = ;
      iif(this.Parent.oContained.w_ELTIPTRA=='I',1,;
      iif(this.Parent.oContained.w_ELTIPTRA=='S',2,;
      iif(this.Parent.oContained.w_ELTIPTRA=='B',3,;
      iif(this.Parent.oContained.w_ELTIPTRA=='N',4,;
      0))))
  endfunc

  func oELTIPTRA_2_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not empty(.w_EL_CAMPO))
    endwith
   endif
  endfunc

  func oELTIPTRA_2_13.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='I' OR Not .w_EL__TIPO $ 'V-E')
    endwith
  endfunc

  add object oEL_BATCH_2_14 as StdField with uid="FHUOFOWKNS",rtseq=44,rtrep=.f.,;
    cFormVar = "w_EL_BATCH", cQueryName = "EL_BATCH",;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Routine da utilizzare per eseguire trascodifica",;
    HelpContextID = 204600178,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=504, Top=246, InputMask=replicate('X',50)

  func oEL_BATCH_2_14.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='I' OR .w_ELTIPTRA<>'B' OR Not .w_EL__TIPO $ 'V-E')
    endwith
  endfunc

  add object oELCONDIT_2_15 as StdField with uid="OPRDDEFUAI",rtseq=45,rtrep=.f.,;
    cFormVar = "w_ELCONDIT", cQueryName = "ELCONDIT",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Condizione di applicabilit� dell'elemento",;
    HelpContextID = 78204058,;
   bGlobalFont=.t.,;
    Height=21, Width=591, Left=136, Top=270, InputMask=replicate('X',254)

  add object oELCONWHE_2_16 as StdField with uid="GSQMDLVFFM",rtseq=46,rtrep=.f.,;
    cFormVar = "w_ELCONWHE", cQueryName = "ELCONWHE",;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Condizione di where tabella elemento reiterabile",;
    HelpContextID = 128535691,;
   bGlobalFont=.t.,;
    Height=21, Width=591, Left=136, Top=299, InputMask=replicate('X',254)

  func oELCONWHE_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EL__TIPO='D')
    endwith
   endif
  endfunc

  add object oELEXPRES_2_26 as StdField with uid="NPTADIBSYF",rtseq=47,rtrep=.f.,;
    cFormVar = "w_ELEXPRES", cQueryName = "ELEXPRES",;
    bObbl = .t. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Espressione di calcolo campo",;
    HelpContextID = 221090663,;
   bGlobalFont=.t.,;
    Height=62, Width=591, Left=136, Top=328, InputMask=replicate('X',254)

  func oELEXPRES_2_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EL__TIPO='E')
    endwith
   endif
  endfunc

  add object oDESSTR_2_28 as StdField with uid="JCPEKUNUEZ",rtseq=60,rtrep=.f.,;
    cFormVar = "w_DESSTR", cQueryName = "DESSTR",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 217168586,;
   bGlobalFont=.t.,;
    Height=21, Width=434, Left=293, Top=23, InputMask=replicate('X',30)

  add object oELCOS_2_31 as StdField with uid="HNFLCUOXJO",rtseq=61,rtrep=.f.,;
    cFormVar = "w_ELCOS", cQueryName = "ELCOS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice struttura",;
    HelpContextID = 16337990,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=136, Top=23, InputMask=replicate('X',20)

  add object oELCOE_2_32 as StdField with uid="JYTDCLHTLA",rtseq=62,rtrep=.f.,;
    cFormVar = "w_ELCOE", cQueryName = "ELCOE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice elemento",;
    HelpContextID = 1657926,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=136, Top=52, InputMask=replicate('X',20)

  add object oELDE_2_33 as StdField with uid="JKRVSLSORK",rtseq=63,rtrep=.f.,;
    cFormVar = "w_ELDE", cQueryName = "ELDE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione elemento",;
    HelpContextID = 197090374,;
   bGlobalFont=.t.,;
    Height=21, Width=434, Left=293, Top=52, InputMask=replicate('X',50)

  add object oDESCAMP_2_34 as StdField with uid="VTDPCKMZSA",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DESCAMP", cQueryName = "DESCAMP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 214844726,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=363, Top=139, InputMask=replicate('X',254)

  func oDESCAMP_2_34.mHide()
    with this.Parent.oContained
      return (.w_EL__TIPO='L')
    endwith
  endfunc

  add object oDESCLA_2_35 as StdField with uid="MCHVDRZIUG",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DESCLA", cQueryName = "DESCLA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 25052470,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=363, Top=139, InputMask=replicate('X',50)

  func oDESCLA_2_35.mHide()
    with this.Parent.oContained
      return (.w_EL__TIPO<>'L')
    endwith
  endfunc

  add object oStr_2_1 as StdString with uid="WRTVTMQOIO",Visible=.t., Left=-8, Top=273,;
    Alignment=1, Width=138, Height=18,;
    Caption="Condizione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_2 as StdString with uid="BSZEGSJGCJ",Visible=.t., Left=-8, Top=302,;
    Alignment=1, Width=138, Height=18,;
    Caption="Filtro su tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="OWHXUQUJIE",Visible=.t., Left=-22, Top=143,;
    Alignment=1, Width=152, Height=18,;
    Caption="Classe documentale:"  ;
  , bGlobalFont=.t.

  func oStr_2_17.mHide()
    with this.Parent.oContained
      return (.w_EL__TIPO<>'L')
    endwith
  endfunc

  add object oStr_2_18 as StdString with uid="WJKYFINZXO",Visible=.t., Left=-8, Top=84,;
    Alignment=1, Width=138, Height=18,;
    Caption="Funzione:"  ;
  , bGlobalFont=.t.

  func oStr_2_18.mHide()
    with this.Parent.oContained
      return (.w_EL__TIPO<>'S')
    endwith
  endfunc

  add object oStr_2_19 as StdString with uid="LFFEWQFLWE",Visible=.t., Left=-8, Top=219,;
    Alignment=1, Width=138, Height=18,;
    Caption="Segno:"  ;
  , bGlobalFont=.t.

  func oStr_2_19.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='I' OR .w_ELTIPCAM <>'N')
    endwith
  endfunc

  add object oStr_2_20 as StdString with uid="SFLTSJDWCZ",Visible=.t., Left=-8, Top=193,;
    Alignment=1, Width=138, Height=18,;
    Caption="Allineamento:"  ;
  , bGlobalFont=.t.

  func oStr_2_20.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='I' OR Not .w_ELTIPCAM $ 'N-C-M' OR .w_EL__TIPO='L')
    endwith
  endfunc

  add object oStr_2_21 as StdString with uid="KOAIHJPADL",Visible=.t., Left=-8, Top=169,;
    Alignment=1, Width=138, Height=18,;
    Caption="Tipo campo/espr.:"  ;
  , bGlobalFont=.t.

  func oStr_2_21.mHide()
    with this.Parent.oContained
      return (NOT(.w_EL__TIPO $ 'E-V'))
    endwith
  endfunc

  add object oStr_2_22 as StdString with uid="WBJSUNROBU",Visible=.t., Left=364, Top=249,;
    Alignment=1, Width=138, Height=18,;
    Caption="Routine:"  ;
  , bGlobalFont=.t.

  func oStr_2_22.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='I' OR .w_ELTIPTRA<>'B' OR Not .w_EL__TIPO $ 'V-E')
    endwith
  endfunc

  add object oStr_2_23 as StdString with uid="GUTWECALYT",Visible=.t., Left=-8, Top=245,;
    Alignment=1, Width=138, Height=18,;
    Caption="Tipo trascodifica:"  ;
  , bGlobalFont=.t.

  func oStr_2_23.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='I' OR Not .w_EL__TIPO $ 'V-E')
    endwith
  endfunc

  add object oStr_2_24 as StdString with uid="VEJBBXBWVC",Visible=.t., Left=-8, Top=143,;
    Alignment=1, Width=138, Height=18,;
    Caption="Campo:"  ;
  , bGlobalFont=.t.

  func oStr_2_24.mHide()
    with this.Parent.oContained
      return (.w_EL__TIPO='L')
    endwith
  endfunc

  add object oStr_2_25 as StdString with uid="UAPUPHUFDY",Visible=.t., Left=-8, Top=113,;
    Alignment=1, Width=138, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="WPRXHFYAUB",Visible=.t., Left=-8, Top=329,;
    Alignment=1, Width=138, Height=18,;
    Caption="Espressione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="ENDHHMWHOY",Visible=.t., Left=-8, Top=56,;
    Alignment=1, Width=138, Height=18,;
    Caption="Codice elemento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_30 as StdString with uid="WORYUZYNFM",Visible=.t., Left=-8, Top=25,;
    Alignment=1, Width=138, Height=18,;
    Caption="Codice struttura:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgste_aelPag3 as StdContainer
  Width  = 733
  height = 394
  stdWidth  = 733
  stdheight = 394
  resizeXpos=570
  resizeYpos=352
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oELFUNSC2_3_1 as StdField with uid="QHMNWPZYKG",rtseq=48,rtrep=.f.,;
    cFormVar = "w_ELFUNSC2", cQueryName = "ELFUNSC2",;
    bObbl = .t. , nPag = 3, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Funzione di scelta elementi figli",;
    HelpContextID = 206603144,;
   bGlobalFont=.t.,;
    Height=21, Width=591, Left=136, Top=81, InputMask=replicate('X',254)

  func oELFUNSC2_3_1.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STFLTYPE<>'O' AND .w_EL__TIPO='S')
    endwith
   endif
  endfunc

  func oELFUNSC2_3_1.mHide()
    with this.Parent.oContained
      return (.w_EL__TIPO<>'S')
    endwith
  endfunc

  add object oELTABIMP_3_2 as StdField with uid="HZMWAYBRFW",rtseq=49,rtrep=.f.,;
    cFormVar = "w_ELTABIMP", cQueryName = "ELTABIMP",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Codice tabella di import",;
    HelpContextID = 148659350,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=136, Top=110, InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="ENT_DETT", oKey_1_1="ENCODICE", oKey_1_2="this.w_ELCODENT", oKey_2_1="EN_TABLE", oKey_2_2="this.w_ELTABIMP"

  func oELTABIMP_3_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_EL__TIPO $ 'V-D-E' OR (Not Empty(.w_ELVALTXT))))
    endwith
   endif
  endfunc

  func oELTABIMP_3_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oELTABIMP_3_2.ecpDrop(oSource)
    this.Parent.oContained.link_3_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oELTABIMP_3_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ENT_DETT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ENCODICE="+cp_ToStrODBC(this.Parent.oContained.w_ELCODENT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ENCODICE="+cp_ToStr(this.Parent.oContained.w_ELCODENT)
    endif
    do cp_zoom with 'ENT_DETT','*','ENCODICE,EN_TABLE',cp_AbsName(this.parent,'oELTABIMP_3_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Tabelle",'',this.parent.oContained
  endproc

  add object oELCAMIMP_3_3 as StdField with uid="VSSFSRGPVM",rtseq=50,rtrep=.f.,;
    cFormVar = "w_ELCAMIMP", cQueryName = "ELCAMIMP",;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Campo di import",;
    HelpContextID = 160124054,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=136, Top=139, InputMask=replicate('X',30)

  func oELCAMIMP_3_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_STFLTYPE<>'O' AND (Not empty(.w_ELTABIMP) and  Not .w_EL__TIPO $  'D-G-T'))
    endwith
   endif
  endfunc

  add object oELMEMDAT_3_4 as StdCheck with uid="FWLDRSAVYN",rtseq=51,rtrep=.f.,left=136, top=168, caption="Var. globale",;
    ToolTipText = "Se attivo memorizza dato in variabile globale",;
    HelpContextID = 191894374,;
    cFormVar="w_ELMEMDAT", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oELMEMDAT_3_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oELMEMDAT_3_4.GetRadio()
    this.Parent.oContained.w_ELMEMDAT = this.RadioValue()
    return .t.
  endfunc

  func oELMEMDAT_3_4.SetRadio()
    this.Parent.oContained.w_ELMEMDAT=trim(this.Parent.oContained.w_ELMEMDAT)
    this.value = ;
      iif(this.Parent.oContained.w_ELMEMDAT=='S',1,;
      0)
  endfunc

  func oELMEMDAT_3_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_ELCAMIMP))
    endwith
   endif
  endfunc


  add object oELQUALIF_3_5 as StdCombo with uid="XPMMDNJTJR",rtseq=52,rtrep=.f.,left=136,top=198,width=105,height=21;
    , ToolTipText = "Indica su quale campo dell'elemento � contenuto il valore qualificatore Nessuno (default), Id secondario, Cod. txt descr";
    , HelpContextID = 199240844;
    , cFormVar="w_ELQUALIF",RowSource=""+"Id secondario,"+"Cod. txt descr,"+"Nessuno", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oELQUALIF_3_5.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'T',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oELQUALIF_3_5.GetRadio()
    this.Parent.oContained.w_ELQUALIF = this.RadioValue()
    return .t.
  endfunc

  func oELQUALIF_3_5.SetRadio()
    this.Parent.oContained.w_ELQUALIF=trim(this.Parent.oContained.w_ELQUALIF)
    this.value = ;
      iif(this.Parent.oContained.w_ELQUALIF=='S',1,;
      iif(this.Parent.oContained.w_ELQUALIF=='T',2,;
      iif(this.Parent.oContained.w_ELQUALIF=='N',3,;
      0)))
  endfunc

  func oELQUALIF_3_5.mHide()
    with this.Parent.oContained
      return (.w_EL__TIPO <>  'V')
    endwith
  endfunc

  add object oELPOSQUA_3_6 as StdField with uid="DHLPHDBOQU",rtseq=53,rtrep=.f.,;
    cFormVar = "w_ELPOSQUA", cQueryName = "ELPOSQUA",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Indica la posizione del qualificatore all'interno della riga",;
    HelpContextID = 235266937,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=314, Top=195

  func oELPOSQUA_3_6.mHide()
    with this.Parent.oContained
      return ( .w_ELQUALIF='N')
    endwith
  endfunc


  add object oELALLIMP_3_7 as StdCombo with uid="BRQEMTPEBQ",rtseq=54,rtrep=.f.,left=136,top=224,width=114,height=21;
    , ToolTipText = "Allineamento utilizzato in fase di import per ricostruire il dato";
    , HelpContextID = 159788182;
    , cFormVar="w_ELALLIMP",RowSource=""+"Destra,"+"Sinistra,"+"Nessuno", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oELALLIMP_3_7.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oELALLIMP_3_7.GetRadio()
    this.Parent.oContained.w_ELALLIMP = this.RadioValue()
    return .t.
  endfunc

  func oELALLIMP_3_7.SetRadio()
    this.Parent.oContained.w_ELALLIMP=trim(this.Parent.oContained.w_ELALLIMP)
    this.value = ;
      iif(this.Parent.oContained.w_ELALLIMP=='D',1,;
      iif(this.Parent.oContained.w_ELALLIMP=='S',2,;
      iif(this.Parent.oContained.w_ELALLIMP=='N',3,;
      0)))
  endfunc

  func oELALLIMP_3_7.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='O' OR Not .w_ELTIPCAM $ 'N-C-M' OR .w_EL__TIPO='L')
    endwith
  endfunc

  add object oELCARIMP_3_8 as StdField with uid="EKKOULILUM",rtseq=55,rtrep=.f.,;
    cFormVar = "w_ELCARIMP", cQueryName = "ELCARIMP",;
    bObbl = .f. , nPag = 3, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Carattere di conversione filling utilizzato in import per ricostruire il dato",;
    HelpContextID = 165366934,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=255, Top=224, InputMask=replicate('X',1)

  func oELCARIMP_3_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_EL__TIPO<>'D')
    endwith
   endif
  endfunc

  func oELCARIMP_3_8.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='O' OR Not .w_ELTIPCAM $ 'N-C-M' OR .w_EL__TIPO='L')
    endwith
  endfunc


  add object oEL_SEGNO_3_9 as StdCombo with uid="DARDYFNLZO",rtseq=56,rtrep=.f.,left=136,top=250,width=139,height=21;
    , HelpContextID = 119475349;
    , cFormVar="w_EL_SEGNO",RowSource=""+"Segno +,"+"Segno -,"+"Originario", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oEL_SEGNO_3_9.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'M',;
    iif(this.value =3,'O',;
    space(1)))))
  endfunc
  func oEL_SEGNO_3_9.GetRadio()
    this.Parent.oContained.w_EL_SEGNO = this.RadioValue()
    return .t.
  endfunc

  func oEL_SEGNO_3_9.SetRadio()
    this.Parent.oContained.w_EL_SEGNO=trim(this.Parent.oContained.w_EL_SEGNO)
    this.value = ;
      iif(this.Parent.oContained.w_EL_SEGNO=='P',1,;
      iif(this.Parent.oContained.w_EL_SEGNO=='M',2,;
      iif(this.Parent.oContained.w_EL_SEGNO=='O',3,;
      0)))
  endfunc

  func oEL_SEGNO_3_9.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='O' OR .w_ELTIPCAM <>'N')
    endwith
  endfunc


  add object oELTIPTR2_3_10 as StdCombo with uid="UVIEEAMORC",rtseq=57,rtrep=.f.,left=136,top=276,width=139,height=21;
    , HelpContextID = 188457864;
    , cFormVar="w_ELTIPTR2",RowSource=""+"Da intestatario,"+"Da struttura,"+"Da routine,"+"Non gestita", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oELTIPTR2_3_10.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'S',;
    iif(this.value =3,'B',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oELTIPTR2_3_10.GetRadio()
    this.Parent.oContained.w_ELTIPTR2 = this.RadioValue()
    return .t.
  endfunc

  func oELTIPTR2_3_10.SetRadio()
    this.Parent.oContained.w_ELTIPTR2=trim(this.Parent.oContained.w_ELTIPTR2)
    this.value = ;
      iif(this.Parent.oContained.w_ELTIPTR2=='I',1,;
      iif(this.Parent.oContained.w_ELTIPTR2=='S',2,;
      iif(this.Parent.oContained.w_ELTIPTR2=='B',3,;
      iif(this.Parent.oContained.w_ELTIPTR2=='N',4,;
      0))))
  endfunc

  func oELTIPTR2_3_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not empty(.w_ELCAMIMP))
    endwith
   endif
  endfunc

  func oELTIPTR2_3_10.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='O' OR Not .w_EL__TIPO $ 'V-E')
    endwith
  endfunc

  add object oEL_BATC2_3_11 as StdField with uid="SKTQNZNZEK",rtseq=58,rtrep=.f.,;
    cFormVar = "w_EL_BATC2", cQueryName = "EL_BATC2",;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Routine da utilizzare per eseguire trascodifica",;
    HelpContextID = 204600200,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=504, Top=275, InputMask=replicate('X',50)

  func oEL_BATC2_3_11.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='O' OR .w_ELTIPTR2<>'B' OR Not .w_EL__TIPO $ 'V-E')
    endwith
  endfunc

  add object oDESSTR_3_21 as StdField with uid="LIBXOKAFFW",rtseq=59,rtrep=.f.,;
    cFormVar = "w_DESSTR", cQueryName = "DESSTR",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 217168586,;
   bGlobalFont=.t.,;
    Height=21, Width=434, Left=293, Top=23, InputMask=replicate('X',30)

  add object oELCOS1_3_24 as StdField with uid="NHCYTAQUDP",rtseq=64,rtrep=.f.,;
    cFormVar = "w_ELCOS1", cQueryName = "ELCOS1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice struttura",;
    HelpContextID = 33115206,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=136, Top=23, InputMask=replicate('X',20)

  add object oELCOE1_3_25 as StdField with uid="BUFGOTFWVF",rtseq=65,rtrep=.f.,;
    cFormVar = "w_ELCOE1", cQueryName = "ELCOE1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Codice elemento",;
    HelpContextID = 18435142,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=136, Top=52, InputMask=replicate('X',20)

  add object oELDE1_3_26 as StdField with uid="MGEAVTGBJZ",rtseq=66,rtrep=.f.,;
    cFormVar = "w_ELDE1", cQueryName = "ELDE1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione elemento",;
    HelpContextID = 248470598,;
   bGlobalFont=.t.,;
    Height=21, Width=434, Left=293, Top=52, InputMask=replicate('X',50)

  add object oELDESC_3_28 as StdField with uid="RFLYKXJJJF",rtseq=68,rtrep=.f.,;
    cFormVar = "w_ELDESC", cQueryName = "ELDESC",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 66018374,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=364, Top=139, InputMask=replicate('X',254)

  add object oELEXPRE1_3_30 as StdField with uid="EAGEYAZQEI",rtseq=70,rtrep=.f.,;
    cFormVar = "w_ELEXPRE1", cQueryName = "ELEXPRE1",;
    bObbl = .f. , nPag = 3, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Espressione di calcolo campo",;
    HelpContextID = 221090697,;
   bGlobalFont=.t.,;
    Height=63, Width=591, Left=136, Top=327, InputMask=replicate('X',254)

  func oELEXPRE1_3_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_ELCAMIMP))
    endwith
   endif
  endfunc

  add object oStr_3_12 as StdString with uid="YUTBQOXUYZ",Visible=.t., Left=29, Top=84,;
    Alignment=1, Width=101, Height=18,;
    Caption="Funzione:"  ;
  , bGlobalFont=.t.

  func oStr_3_12.mHide()
    with this.Parent.oContained
      return (.w_EL__TIPO<>'S')
    endwith
  endfunc

  add object oStr_3_13 as StdString with uid="NYOGJUWGYU",Visible=.t., Left=24, Top=224,;
    Alignment=1, Width=106, Height=18,;
    Caption="Allineamento:"  ;
  , bGlobalFont=.t.

  func oStr_3_13.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='O' OR Not .w_ELTIPCAM $ 'N-C-M' OR .w_EL__TIPO='L')
    endwith
  endfunc

  add object oStr_3_14 as StdString with uid="PINNZYIQIR",Visible=.t., Left=32, Top=252,;
    Alignment=1, Width=98, Height=18,;
    Caption="Segno:"  ;
  , bGlobalFont=.t.

  func oStr_3_14.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='O' OR .w_ELTIPCAM <>'N')
    endwith
  endfunc

  add object oStr_3_15 as StdString with uid="YUVYVSYFDO",Visible=.t., Left=403, Top=279,;
    Alignment=1, Width=101, Height=18,;
    Caption="Routine:"  ;
  , bGlobalFont=.t.

  func oStr_3_15.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='O' OR .w_ELTIPTR2<>'B' OR Not .w_EL__TIPO $ 'V-E')
    endwith
  endfunc

  add object oStr_3_16 as StdString with uid="YWYJUELEYW",Visible=.t., Left=-48, Top=277,;
    Alignment=1, Width=178, Height=18,;
    Caption="Tipo trascodifica:"  ;
  , bGlobalFont=.t.

  func oStr_3_16.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='O' OR Not .w_EL__TIPO $ 'V-E')
    endwith
  endfunc

  add object oStr_3_17 as StdString with uid="SWCYANEMND",Visible=.t., Left=29, Top=142,;
    Alignment=1, Width=101, Height=18,;
    Caption="Campo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_18 as StdString with uid="IMENXIZNNU",Visible=.t., Left=29, Top=113,;
    Alignment=1, Width=101, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_3_19 as StdString with uid="NPXZDDJURC",Visible=.t., Left=261, Top=198,;
    Alignment=1, Width=49, Height=18,;
    Caption="Pos.:"  ;
  , bGlobalFont=.t.

  func oStr_3_19.mHide()
    with this.Parent.oContained
      return ( .w_ELQUALIF='N')
    endwith
  endfunc

  add object oStr_3_20 as StdString with uid="RZNQOBOQZF",Visible=.t., Left=48, Top=198,;
    Alignment=1, Width=82, Height=18,;
    Caption="Qualif.:"  ;
  , bGlobalFont=.t.

  func oStr_3_20.mHide()
    with this.Parent.oContained
      return (.w_EL__TIPO <>  'V')
    endwith
  endfunc

  add object oStr_3_22 as StdString with uid="IIQZRJAKOG",Visible=.t., Left=-8, Top=56,;
    Alignment=1, Width=138, Height=18,;
    Caption="Codice elemento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_23 as StdString with uid="KDXNZGMLQA",Visible=.t., Left=-8, Top=25,;
    Alignment=1, Width=138, Height=18,;
    Caption="Codice struttura:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_27 as StdString with uid="XVHTQKPADG",Visible=.t., Left=-8, Top=327,;
    Alignment=1, Width=138, Height=18,;
    Caption="Espressione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_29 as StdString with uid="MBEWBEIFAZ",Visible=.t., Left=277, Top=230,;
    Alignment=0, Width=50, Height=18,;
    Caption="Fill"  ;
  , bGlobalFont=.t.

  func oStr_3_29.mHide()
    with this.Parent.oContained
      return (.w_STFLTYPE='O' OR Not .w_ELTIPCAM $ 'N-C-M' OR .w_EL__TIPO='L')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_ael','VAELEMEN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ELCODSTR=VAELEMEN.ELCODSTR";
  +" and "+i_cAliasName2+".ELCODICE=VAELEMEN.ELCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
