* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_atc                                                        *
*              Tipologie costi/ricavi                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30] [VRS_16]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2009-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsca_atc"))

* --- Class definition
define class tgsca_atc as StdForm
  Top    = 54
  Left   = 36

  * --- Standard Properties
  Width  = 458
  Height = 118+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-01"
  HelpContextID=92964201
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  TIPCOSTO_IDX = 0
  cFile = "TIPCOSTO"
  cKeySelect = "TCCODICE"
  cKeyWhere  = "TCCODICE=this.w_TCCODICE"
  cKeyWhereODBC = '"TCCODICE="+cp_ToStrODBC(this.w_TCCODICE)';

  cKeyWhereODBCqualified = '"TIPCOSTO.TCCODICE="+cp_ToStrODBC(this.w_TCCODICE)';

  cPrg = "gsca_atc"
  cComment = "Tipologie costi/ricavi"
  icon = "anag.ico"
  cAutoZoom = 'GSCA0ATC'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TCCODICE = space(5)
  w_TCFLGEFA = space(1)
  w_TCDESCRI = space(35)
  w_TCTIPCOS = space(2)
  w_TCDESBRE = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TIPCOSTO','gsca_atc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsca_atcPag1","gsca_atc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Tipologie costi/ricavi")
      .Pages(1).HelpContextID = 219734078
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTCCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='TIPCOSTO'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TIPCOSTO_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TIPCOSTO_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TCCODICE = NVL(TCCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TIPCOSTO where TCCODICE=KeySet.TCCODICE
    *
    i_nConn = i_TableProp[this.TIPCOSTO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TIPCOSTO')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TIPCOSTO.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TIPCOSTO '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TCCODICE',this.w_TCCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TCCODICE = NVL(TCCODICE,space(5))
        .w_TCFLGEFA = NVL(TCFLGEFA,space(1))
        .w_TCDESCRI = NVL(TCDESCRI,space(35))
        .w_TCTIPCOS = NVL(TCTIPCOS,space(2))
        .w_TCDESBRE = NVL(TCDESBRE,space(5))
        cp_LoadRecExtFlds(this,'TIPCOSTO')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TCCODICE = space(5)
      .w_TCFLGEFA = space(1)
      .w_TCDESCRI = space(35)
      .w_TCTIPCOS = space(2)
      .w_TCDESBRE = space(5)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_TCFLGEFA = 'N'
          .DoRTCalc(3,3,.f.)
        .w_TCTIPCOS = 'AL'
      endif
    endwith
    cp_BlankRecExtFlds(this,'TIPCOSTO')
    this.DoRTCalc(5,5,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTCCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oTCFLGEFA_1_2.enabled = i_bVal
      .Page1.oPag.oTCDESCRI_1_4.enabled = i_bVal
      .Page1.oPag.oTCTIPCOS_1_7.enabled = i_bVal
      .Page1.oPag.oTCDESBRE_1_8.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTCCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTCCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TIPCOSTO',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TIPCOSTO_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCCODICE,"TCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCFLGEFA,"TCFLGEFA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCDESCRI,"TCDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCTIPCOS,"TCTIPCOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TCDESBRE,"TCDESBRE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TIPCOSTO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])
    i_lTable = "TIPCOSTO"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TIPCOSTO_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("QUERY\GSCA_QTC.VQR,QUERY\GSCA_QTC.FRX",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TIPCOSTO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TIPCOSTO_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TIPCOSTO
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TIPCOSTO')
        i_extval=cp_InsertValODBCExtFlds(this,'TIPCOSTO')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TCCODICE,TCFLGEFA,TCDESCRI,TCTIPCOS,TCDESBRE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TCCODICE)+;
                  ","+cp_ToStrODBC(this.w_TCFLGEFA)+;
                  ","+cp_ToStrODBC(this.w_TCDESCRI)+;
                  ","+cp_ToStrODBC(this.w_TCTIPCOS)+;
                  ","+cp_ToStrODBC(this.w_TCDESBRE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TIPCOSTO')
        i_extval=cp_InsertValVFPExtFlds(this,'TIPCOSTO')
        cp_CheckDeletedKey(i_cTable,0,'TCCODICE',this.w_TCCODICE)
        INSERT INTO (i_cTable);
              (TCCODICE,TCFLGEFA,TCDESCRI,TCTIPCOS,TCDESBRE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TCCODICE;
                  ,this.w_TCFLGEFA;
                  ,this.w_TCDESCRI;
                  ,this.w_TCTIPCOS;
                  ,this.w_TCDESBRE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TIPCOSTO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TIPCOSTO_IDX,i_nConn)
      *
      * update TIPCOSTO
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TIPCOSTO')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TCFLGEFA="+cp_ToStrODBC(this.w_TCFLGEFA)+;
             ",TCDESCRI="+cp_ToStrODBC(this.w_TCDESCRI)+;
             ",TCTIPCOS="+cp_ToStrODBC(this.w_TCTIPCOS)+;
             ",TCDESBRE="+cp_ToStrODBC(this.w_TCDESBRE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TIPCOSTO')
        i_cWhere = cp_PKFox(i_cTable  ,'TCCODICE',this.w_TCCODICE  )
        UPDATE (i_cTable) SET;
              TCFLGEFA=this.w_TCFLGEFA;
             ,TCDESCRI=this.w_TCDESCRI;
             ,TCTIPCOS=this.w_TCTIPCOS;
             ,TCDESBRE=this.w_TCDESBRE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TIPCOSTO_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TIPCOSTO_IDX,i_nConn)
      *
      * delete TIPCOSTO
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TCCODICE',this.w_TCCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TIPCOSTO_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCOSTO_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTCFLGEFA_1_2.enabled = this.oPgFrm.Page1.oPag.oTCFLGEFA_1_2.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTCCODICE_1_1.value==this.w_TCCODICE)
      this.oPgFrm.Page1.oPag.oTCCODICE_1_1.value=this.w_TCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oTCFLGEFA_1_2.RadioValue()==this.w_TCFLGEFA)
      this.oPgFrm.Page1.oPag.oTCFLGEFA_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTCDESCRI_1_4.value==this.w_TCDESCRI)
      this.oPgFrm.Page1.oPag.oTCDESCRI_1_4.value=this.w_TCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTCTIPCOS_1_7.RadioValue()==this.w_TCTIPCOS)
      this.oPgFrm.Page1.oPag.oTCTIPCOS_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTCDESBRE_1_8.value==this.w_TCDESBRE)
      this.oPgFrm.Page1.oPag.oTCDESBRE_1_8.value=this.w_TCDESBRE
    endif
    cp_SetControlsValueExtFlds(this,'TIPCOSTO')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsca_atcPag1 as StdContainer
  Width  = 454
  height = 118
  stdWidth  = 454
  stdheight = 118
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTCCODICE_1_1 as StdField with uid="KEHMSZGGOV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TCCODICE", cQueryName = "TCCODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice tipologia di costo/ricavo",;
    HelpContextID = 134804347,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=83, Top=11, cSayPict='"XXXXX"', cGetPict='"XXXXX"', InputMask=replicate('X',5)

  add object oTCFLGEFA_1_2 as StdCheck with uid="AZAGYCVDZM",rtseq=2,rtrep=.f.,left=330, top=11, caption="Gen. fabbisogni",;
    ToolTipText = "Genera o meno fabbisogni (pianificazione commessa)",;
    HelpContextID = 70656887,;
    cFormVar="w_TCFLGEFA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTCFLGEFA_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTCFLGEFA_1_2.GetRadio()
    this.Parent.oContained.w_TCFLGEFA = this.RadioValue()
    return .t.
  endfunc

  func oTCFLGEFA_1_2.SetRadio()
    this.Parent.oContained.w_TCFLGEFA=trim(this.Parent.oContained.w_TCFLGEFA)
    this.value = ;
      iif(this.Parent.oContained.w_TCFLGEFA=='S',1,;
      0)
  endfunc

  func oTCFLGEFA_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COMM='S')
    endwith
   endif
  endfunc

  add object oTCDESCRI_1_4 as StdField with uid="GRUYCWTJNF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TCDESCRI", cQueryName = "TCDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione completa della tipologia del costo",;
    HelpContextID = 49218431,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=83, Top=39, InputMask=replicate('X',35)


  add object oTCTIPCOS_1_7 as StdCombo with uid="SZVTPIEZUS",rtseq=4,rtrep=.f.,left=84,top=67,width=124,height=21;
    , ToolTipText = "Tipo di costo";
    , HelpContextID = 222035063;
    , cFormVar="w_TCTIPCOS",RowSource=""+"Materiali,"+"Manodopera,"+"Appalti,"+"Altro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTCTIPCOS_1_7.RadioValue()
    return(iif(this.value =1,'MA',;
    iif(this.value =2,'MD',;
    iif(this.value =3,'AP',;
    iif(this.value =4,'AL',;
    space(2))))))
  endfunc
  func oTCTIPCOS_1_7.GetRadio()
    this.Parent.oContained.w_TCTIPCOS = this.RadioValue()
    return .t.
  endfunc

  func oTCTIPCOS_1_7.SetRadio()
    this.Parent.oContained.w_TCTIPCOS=trim(this.Parent.oContained.w_TCTIPCOS)
    this.value = ;
      iif(this.Parent.oContained.w_TCTIPCOS=='MA',1,;
      iif(this.Parent.oContained.w_TCTIPCOS=='MD',2,;
      iif(this.Parent.oContained.w_TCTIPCOS=='AP',3,;
      iif(this.Parent.oContained.w_TCTIPCOS=='AL',4,;
      0))))
  endfunc

  add object oTCDESBRE_1_8 as StdField with uid="EMCYKTTUVW",rtseq=5,rtrep=.f.,;
    cFormVar = "w_TCDESBRE", cQueryName = "TCDESBRE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione abbreviata",;
    HelpContextID = 32441211,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=387, Top=39, InputMask=replicate('X',5)

  add object oStr_1_3 as StdString with uid="IUQKOFKHEK",Visible=.t., Left=2, Top=11,;
    Alignment=1, Width=79, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="ZFLVZEMCNJ",Visible=.t., Left=2, Top=39,;
    Alignment=1, Width=79, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="LOHQSEKPUH",Visible=.t., Left=2, Top=67,;
    Alignment=1, Width=79, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="NSGLTRVUVU",Visible=.t., Left=340, Top=39,;
    Alignment=1, Width=45, Height=15,;
    Caption="Breve:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsca_atc','TIPCOSTO','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TCCODICE=TIPCOSTO.TCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
