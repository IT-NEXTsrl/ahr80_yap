* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_klp                                                        *
*              Autenticazione invio E-mail                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_59]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-05-14                                                      *
* Last revis.: 2014-12-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_klp",oParentObject))

* --- Class definition
define class tgsut_klp as StdForm
  Top    = 77
  Left   = 100

  * --- Standard Properties
  Width  = 430
  Height = 249
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-12-19"
  HelpContextID=51821719
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  UTE_NTI_IDX = 0
  AUT_LPE_IDX = 0
  CPUSRGRP_IDX = 0
  cPrg = "gsut_klp"
  cComment = "Autenticazione invio E-mail"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_UTENTE = 0
  o_UTENTE = 0
  w_UTELOG = 0
  w_OLDPSW1 = space(254)
  w_LOGIN1 = space(254)
  o_LOGIN1 = space(254)
  w_UTEGRU = 0
  w_GROUP = 0
  w_DESCUTE = space(40)
  w_EMAIL = space(254)
  w_LOGIN = space(254)
  w_OLDPSW = space(254)
  w_NEWPSW = space(254)
  w_NEWPSW2 = space(254)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_klpPag1","gsut_klp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oUTENTE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='UTE_NTI'
    this.cWorkTables[2]='AUT_LPE'
    this.cWorkTables[3]='CPUSRGRP'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_UTENTE=0
      .w_UTELOG=0
      .w_OLDPSW1=space(254)
      .w_LOGIN1=space(254)
      .w_UTEGRU=0
      .w_GROUP=0
      .w_DESCUTE=space(40)
      .w_EMAIL=space(254)
      .w_LOGIN=space(254)
      .w_OLDPSW=space(254)
      .w_NEWPSW=space(254)
      .w_NEWPSW2=space(254)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_UTENTE))
          .link_1_1('Full')
        endif
        .w_UTELOG = .w_UTENTE
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_UTELOG))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_UTEGRU = i_CODUTE
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_UTEGRU))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,8,.f.)
        .w_LOGIN = .w_LOGIN1
        .w_OLDPSW = ' '
        .w_NEWPSW = ' '
        .w_NEWPSW2 = ' '
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate('*')
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate('*')
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate('*')
      .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsut_klp
    If type('oParentObject')='O' and type('oParentObject.w_UTCODICE')='N'
     This.w_UTENTE = oParentObject.w_UTCODICE
     This.Link_1_1('Full')
     This.NotifyEvent('w_UTENTE Changed')
     This.mCalc(.t.)
     This.SaveDependsOn()
    Endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_UTENTE<>.w_UTENTE
            .w_UTELOG = .w_UTENTE
          .link_1_2('Full')
        endif
        .DoRTCalc(3,4,.t.)
        if .o_UTENTE<>.w_UTENTE
            .w_UTEGRU = i_CODUTE
          .link_1_5('Full')
        endif
        .DoRTCalc(6,8,.t.)
        if .o_LOGIN1<>.w_LOGIN1.or. .o_UTENTE<>.w_UTENTE
            .w_LOGIN = .w_LOGIN1
        endif
        if .o_UTENTE<>.w_UTENTE
            .w_OLDPSW = ' '
        endif
        if .o_UTENTE<>.w_UTENTE
            .w_NEWPSW = ' '
        endif
        if .o_UTENTE<>.w_UTENTE
            .w_NEWPSW2 = ' '
        endif
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate('*')
        .oPgFrm.Page1.oPag.oObj_1_26.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLOGIN_1_9.enabled = this.oPgFrm.Page1.oPag.oLOGIN_1_9.mCond()
    this.oPgFrm.Page1.oPag.oOLDPSW_1_10.enabled = this.oPgFrm.Page1.oPag.oOLDPSW_1_10.mCond()
    this.oPgFrm.Page1.oPag.oNEWPSW_1_11.enabled = this.oPgFrm.Page1.oPag.oNEWPSW_1_11.mCond()
    this.oPgFrm.Page1.oPag.oNEWPSW2_1_12.enabled = this.oPgFrm.Page1.oPag.oNEWPSW2_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_13.visible=!this.oPgFrm.Page1.oPag.oBtn_1_13.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_26.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=UTENTE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UTE_NTI_IDX,3]
    i_lTable = "UTE_NTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_NTI_IDX,2], .t., this.UTE_NTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_NTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UTE_NTI')
        if i_nConn<>0
          i_cWhere = " UTCODICE="+cp_ToStrODBC(this.w_UTENTE);

          i_ret=cp_SQL(i_nConn,"select UTCODICE,UTDESUTE,UTMITTEN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UTCODICE',this.w_UTENTE)
          select UTCODICE,UTDESUTE,UTMITTEN;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_UTENTE) and !this.bDontReportError
            deferred_cp_zoom('UTE_NTI','*','UTCODICE',cp_AbsName(oSource.parent,'oUTENTE_1_1'),i_cWhere,'',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UTCODICE,UTDESUTE,UTMITTEN";
                     +" from "+i_cTable+" "+i_lTable+" where UTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UTCODICE',oSource.xKey(1))
            select UTCODICE,UTDESUTE,UTMITTEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UTCODICE,UTDESUTE,UTMITTEN";
                   +" from "+i_cTable+" "+i_lTable+" where UTCODICE="+cp_ToStrODBC(this.w_UTENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UTCODICE',this.w_UTENTE)
            select UTCODICE,UTDESUTE,UTMITTEN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTENTE = NVL(_Link_.UTCODICE,0)
      this.w_DESCUTE = NVL(_Link_.UTDESUTE,space(40))
      this.w_EMAIL = NVL(_Link_.UTMITTEN,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_UTENTE = 0
      endif
      this.w_DESCUTE = space(40)
      this.w_EMAIL = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UTE_NTI_IDX,2])+'\'+cp_ToStr(_Link_.UTCODICE,1)
      cp_ShowWarn(i_cKey,this.UTE_NTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTELOG
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AUT_LPE_IDX,3]
    i_lTable = "AUT_LPE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AUT_LPE_IDX,2], .t., this.AUT_LPE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AUT_LPE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTELOG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTELOG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LPCODUTE,LPLOGIN,LPPSW";
                   +" from "+i_cTable+" "+i_lTable+" where LPCODUTE="+cp_ToStrODBC(this.w_UTELOG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LPCODUTE',this.w_UTELOG)
            select LPCODUTE,LPLOGIN,LPPSW;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTELOG = NVL(_Link_.LPCODUTE,0)
      this.w_LOGIN1 = NVL(_Link_.LPLOGIN,space(254))
      this.w_OLDPSW1 = NVL(_Link_.LPPSW,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_UTELOG = 0
      endif
      this.w_LOGIN1 = space(254)
      this.w_OLDPSW1 = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AUT_LPE_IDX,2])+'\'+cp_ToStr(_Link_.LPCODUTE,1)
      cp_ShowWarn(i_cKey,this.AUT_LPE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTELOG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTEGRU
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSRGRP_IDX,3]
    i_lTable = "CPUSRGRP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSRGRP_IDX,2], .t., this.CPUSRGRP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSRGRP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTEGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTEGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select USERCODE,GROUPCODE";
                   +" from "+i_cTable+" "+i_lTable+" where USERCODE="+cp_ToStrODBC(this.w_UTEGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'USERCODE',this.w_UTEGRU)
            select USERCODE,GROUPCODE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTEGRU = NVL(_Link_.USERCODE,0)
      this.w_GROUP = NVL(_Link_.GROUPCODE,0)
    else
      if i_cCtrl<>'Load'
        this.w_UTEGRU = 0
      endif
      this.w_GROUP = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSRGRP_IDX,2])+'\'+cp_ToStr(_Link_.USERCODE,1)
      cp_ShowWarn(i_cKey,this.CPUSRGRP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTEGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oUTENTE_1_1.value==this.w_UTENTE)
      this.oPgFrm.Page1.oPag.oUTENTE_1_1.value=this.w_UTENTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCUTE_1_7.value==this.w_DESCUTE)
      this.oPgFrm.Page1.oPag.oDESCUTE_1_7.value=this.w_DESCUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oEMAIL_1_8.value==this.w_EMAIL)
      this.oPgFrm.Page1.oPag.oEMAIL_1_8.value=this.w_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oLOGIN_1_9.value==this.w_LOGIN)
      this.oPgFrm.Page1.oPag.oLOGIN_1_9.value=this.w_LOGIN
    endif
    if not(this.oPgFrm.Page1.oPag.oOLDPSW_1_10.value==this.w_OLDPSW)
      this.oPgFrm.Page1.oPag.oOLDPSW_1_10.value=this.w_OLDPSW
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWPSW_1_11.value==this.w_NEWPSW)
      this.oPgFrm.Page1.oPag.oNEWPSW_1_11.value=this.w_NEWPSW
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWPSW2_1_12.value==this.w_NEWPSW2)
      this.oPgFrm.Page1.oPag.oNEWPSW2_1_12.value=this.w_NEWPSW2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_UTENTE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUTENTE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_UTENTE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_UTEGRU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oUTEGRU_1_5.SetFocus()
            i_bnoObbl = !empty(.w_UTEGRU)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CifraCnf( ALLTRIM(.w_OLDPSW) , 'C' ) = ALLTRIM(.w_OLDPSW1))  and (NOT EMPTY(.w_LOGIN1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oOLDPSW_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La password digitata non � corretta")
          case   not(.w_NEWPSW=.w_NEWPSW2)  and (NOT EMPTY(.w_NEWPSW))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNEWPSW2_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Le password digitate non corrispondono")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_UTENTE = this.w_UTENTE
    this.o_LOGIN1 = this.w_LOGIN1
    return

enddefine

* --- Define pages as container
define class tgsut_klpPag1 as StdContainer
  Width  = 426
  height = 249
  stdWidth  = 426
  stdheight = 249
  resizeXpos=259
  resizeYpos=71
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oUTENTE_1_1 as StdField with uid="TTXYNRTTYX",rtseq=1,rtrep=.f.,;
    cFormVar = "w_UTENTE", cQueryName = "UTENTE",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Utente",;
    HelpContextID = 39229882,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=60, Top=16, cSayPict="'9999'", cGetPict="'9999'", bHasZoom = .t. , cLinkFile="UTE_NTI", oKey_1_1="UTCODICE", oKey_1_2="this.w_UTENTE"

  func oUTENTE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oUTENTE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUTENTE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UTE_NTI','*','UTCODICE',cp_AbsName(this.parent,'oUTENTE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Utenti",'',this.parent.oContained
  endproc

  add object oDESCUTE_1_7 as StdField with uid="AAAOTEAHAP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCUTE", cQueryName = "DESCUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 212809270,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=117, Top=16, InputMask=replicate('X',40)

  add object oEMAIL_1_8 as StdField with uid="IQXPRLQSDS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_EMAIL", cQueryName = "EMAIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo E-mail dell'utente",;
    HelpContextID = 136584774,;
   bGlobalFont=.t.,;
    Height=21, Width=350, Left=60, Top=43, InputMask=replicate('X',254)

  add object oLOGIN_1_9 as StdField with uid="SNUNNJBMYQ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_LOGIN", cQueryName = "LOGIN",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Login",;
    HelpContextID = 138707126,;
   bGlobalFont=.t.,;
    Height=21, Width=232, Left=180, Top=88, InputMask=replicate('X',254)

  func oLOGIN_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_LOGIN1))
    endwith
   endif
  endfunc

  add object oOLDPSW_1_10 as StdField with uid="OKEPJEIKGG",rtseq=10,rtrep=.f.,;
    cFormVar = "w_OLDPSW", cQueryName = "OLDPSW",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "La password digitata non � corretta",;
    ToolTipText = "Vecchia password",;
    HelpContextID = 6599194,;
   bGlobalFont=.t.,;
    Height=21, Width=232, Left=180, Top=114, InputMask=replicate('X',254)

  func oOLDPSW_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_LOGIN1))
    endwith
   endif
  endfunc

  func oOLDPSW_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CifraCnf( ALLTRIM(.w_OLDPSW) , 'C' ) = ALLTRIM(.w_OLDPSW1))
    endwith
    return bRes
  endfunc

  add object oNEWPSW_1_11 as StdField with uid="PORZOOKYQF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_NEWPSW", cQueryName = "NEWPSW",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Nuova password",;
    HelpContextID = 6523178,;
   bGlobalFont=.t.,;
    Height=21, Width=232, Left=180, Top=140, InputMask=replicate('X',254)

  func oNEWPSW_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_OLDPSW) OR (EMPTY(.w_OLDPSW) AND EMPTY(.w_LOGIN1)))
    endwith
   endif
  endfunc

  add object oNEWPSW2_1_12 as StdField with uid="CCBMOOPWJZ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NEWPSW2", cQueryName = "NEWPSW2",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Le password digitate non corrispondono",;
    ToolTipText = "Conferma nuova password",;
    HelpContextID = 6523178,;
   bGlobalFont=.t.,;
    Height=21, Width=232, Left=180, Top=166, InputMask=replicate('X',254)

  func oNEWPSW2_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_NEWPSW))
    endwith
   endif
  endfunc

  func oNEWPSW2_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_NEWPSW=.w_NEWPSW2)
    endwith
    return bRes
  endfunc


  add object oBtn_1_13 as StdButton with uid="JSQALMJPAL",left=20, top=201, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare login/password";
    , HelpContextID = 132465338;
    , Caption='E\<limina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        GSUT_BPE(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_GROUP = 1 AND .w_UTENTE<>0 AND NOT EMPTY(.w_LOGIN1))
      endwith
    endif
  endfunc

  func oBtn_1_13.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_GROUP <> 1)
     endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="APIWEFNJEL",left=313, top=201, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare login/password";
    , HelpContextID = 125667818;
    , Caption='\<OK';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY (.w_NEWPSW2) AND NOT EMPTY (.w_LOGIN))
      endwith
    endif
  endfunc


  add object oBtn_1_15 as StdButton with uid="ZJIUAZYUNY",left=368, top=201, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 125667818;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_19 as cp_setobjprop with uid="AUWPJKIPCT",left=0, top=271, width=120,height=21,;
    caption='NEWPSW',;
   bGlobalFont=.t.,;
    cObj="w_NEWPSW",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 6523178


  add object oObj_1_20 as cp_setobjprop with uid="ZXZGERWBKE",left=0, top=291, width=120,height=21,;
    caption='NEWPSW2',;
   bGlobalFont=.t.,;
    cObj="w_NEWPSW2",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 6523178


  add object oObj_1_21 as cp_setobjprop with uid="KWUURUPULT",left=0, top=251, width=120,height=21,;
    caption='OLDPSW',;
   bGlobalFont=.t.,;
    cObj="w_OLDPSW",cProp="passwordchar",;
    nPag=1;
    , HelpContextID = 6599194


  add object oObj_1_26 as cp_runprogram with uid="FSTKNAGSIX",left=163, top=271, width=191,height=19,;
    caption='GSUT_BPE(S)',;
   bGlobalFont=.t.,;
    prg="GSUT_BPE('S')",;
    cEvent = "Update start",;
    nPag=1;
    , HelpContextID = 77379029

  add object oStr_1_16 as StdString with uid="FPPIBGTEZW",Visible=.t., Left=20, Top=114,;
    Alignment=1, Width=158, Height=18,;
    Caption="Vecchia password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="EMUXMWUMPQ",Visible=.t., Left=14, Top=166,;
    Alignment=1, Width=164, Height=18,;
    Caption="Conferma nuova password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="VDFPFVJAYN",Visible=.t., Left=20, Top=140,;
    Alignment=1, Width=158, Height=18,;
    Caption="Nuova password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="VCAHIROQMH",Visible=.t., Left=8, Top=16,;
    Alignment=1, Width=52, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="VWXXLVYHWU",Visible=.t., Left=8, Top=43,;
    Alignment=1, Width=52, Height=18,;
    Caption="e-mail:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="FLGKZZFDXH",Visible=.t., Left=112, Top=88,;
    Alignment=1, Width=66, Height=18,;
    Caption="Login:"  ;
  , bGlobalFont=.t.

  add object oBox_1_25 as StdBox with uid="IPYBJEHIZG",left=11, top=79, width=408,height=118
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_klp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
