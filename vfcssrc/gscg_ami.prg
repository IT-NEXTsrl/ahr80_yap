* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_ami                                                        *
*              Misure                                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-15                                                      *
* Last revis.: 2009-01-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_ami"))

* --- Class definition
define class tgscg_ami as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 673
  Height = 99+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-01-02"
  HelpContextID=58055319
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  TAB_MISU_IDX = 0
  TAB_FONT_IDX = 0
  cFile = "TAB_MISU"
  cKeySelect = "MICODICE,MI_FONTE"
  cKeyWhere  = "MICODICE=this.w_MICODICE and MI_FONTE=this.w_MI_FONTE"
  cKeyWhereODBC = '"MICODICE="+cp_ToStrODBC(this.w_MICODICE)';
      +'+" and MI_FONTE="+cp_ToStrODBC(this.w_MI_FONTE)';

  cKeyWhereODBCqualified = '"TAB_MISU.MICODICE="+cp_ToStrODBC(this.w_MICODICE)';
      +'+" and TAB_MISU.MI_FONTE="+cp_ToStrODBC(this.w_MI_FONTE)';

  cPrg = "gscg_ami"
  cComment = "Misure"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MICODICE = space(15)
  w_MIDESCRI = space(80)
  w_MI_FONTE = space(10)
  w_DESFON = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'TAB_MISU','gscg_ami')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_amiPag1","gscg_ami",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Misure")
      .Pages(1).HelpContextID = 1210566
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMICODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='TAB_FONT'
    this.cWorkTables[2]='TAB_MISU'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TAB_MISU_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TAB_MISU_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MICODICE = NVL(MICODICE,space(15))
      .w_MI_FONTE = NVL(MI_FONTE,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from TAB_MISU where MICODICE=KeySet.MICODICE
    *                            and MI_FONTE=KeySet.MI_FONTE
    *
    i_nConn = i_TableProp[this.TAB_MISU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_MISU_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TAB_MISU')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TAB_MISU.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TAB_MISU '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MICODICE',this.w_MICODICE  ,'MI_FONTE',this.w_MI_FONTE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESFON = space(40)
        .w_MICODICE = NVL(MICODICE,space(15))
        .w_MIDESCRI = NVL(MIDESCRI,space(80))
        .w_MI_FONTE = NVL(MI_FONTE,space(10))
          if link_1_3_joined
            this.w_MI_FONTE = NVL(FOCODICE103,NVL(this.w_MI_FONTE,space(10)))
            this.w_DESFON = NVL(FODESCRI103,space(40))
          else
          .link_1_3('Load')
          endif
        cp_LoadRecExtFlds(this,'TAB_MISU')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MICODICE = space(15)
      .w_MIDESCRI = space(80)
      .w_MI_FONTE = space(10)
      .w_DESFON = space(40)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
          if not(empty(.w_MI_FONTE))
          .link_1_3('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'TAB_MISU')
    this.DoRTCalc(4,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMICODICE_1_1.enabled = i_bVal
      .Page1.oPag.oMIDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oMI_FONTE_1_3.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMICODICE_1_1.enabled = .f.
        .Page1.oPag.oMI_FONTE_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMICODICE_1_1.enabled = .t.
        .Page1.oPag.oMI_FONTE_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'TAB_MISU',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TAB_MISU_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MICODICE,"MICODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MIDESCRI,"MIDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MI_FONTE,"MI_FONTE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TAB_MISU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_MISU_IDX,2])
    i_lTable = "TAB_MISU"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.TAB_MISU_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TAB_MISU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_MISU_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.TAB_MISU_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into TAB_MISU
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TAB_MISU')
        i_extval=cp_InsertValODBCExtFlds(this,'TAB_MISU')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MICODICE,MIDESCRI,MI_FONTE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MICODICE)+;
                  ","+cp_ToStrODBC(this.w_MIDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_MI_FONTE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TAB_MISU')
        i_extval=cp_InsertValVFPExtFlds(this,'TAB_MISU')
        cp_CheckDeletedKey(i_cTable,0,'MICODICE',this.w_MICODICE,'MI_FONTE',this.w_MI_FONTE)
        INSERT INTO (i_cTable);
              (MICODICE,MIDESCRI,MI_FONTE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MICODICE;
                  ,this.w_MIDESCRI;
                  ,this.w_MI_FONTE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.TAB_MISU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_MISU_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.TAB_MISU_IDX,i_nConn)
      *
      * update TAB_MISU
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'TAB_MISU')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MIDESCRI="+cp_ToStrODBC(this.w_MIDESCRI)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'TAB_MISU')
        i_cWhere = cp_PKFox(i_cTable  ,'MICODICE',this.w_MICODICE  ,'MI_FONTE',this.w_MI_FONTE  )
        UPDATE (i_cTable) SET;
              MIDESCRI=this.w_MIDESCRI;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TAB_MISU_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_MISU_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.TAB_MISU_IDX,i_nConn)
      *
      * delete TAB_MISU
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MICODICE',this.w_MICODICE  ,'MI_FONTE',this.w_MI_FONTE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TAB_MISU_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_MISU_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MI_FONTE
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TAB_FONT_IDX,3]
    i_lTable = "TAB_FONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2], .t., this.TAB_FONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MI_FONTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gscg_afo',True,'TAB_FONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FOCODICE like "+cp_ToStrODBC(trim(this.w_MI_FONTE)+"%");

          i_ret=cp_SQL(i_nConn,"select FOCODICE,FODESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FOCODICE',trim(this.w_MI_FONTE))
          select FOCODICE,FODESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MI_FONTE)==trim(_Link_.FOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MI_FONTE) and !this.bDontReportError
            deferred_cp_zoom('TAB_FONT','*','FOCODICE',cp_AbsName(oSource.parent,'oMI_FONTE_1_3'),i_cWhere,'gscg_afo',"FONTI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODICE,FODESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODICE',oSource.xKey(1))
            select FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MI_FONTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FOCODICE,FODESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FOCODICE="+cp_ToStrODBC(this.w_MI_FONTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FOCODICE',this.w_MI_FONTE)
            select FOCODICE,FODESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MI_FONTE = NVL(_Link_.FOCODICE,space(10))
      this.w_DESFON = NVL(_Link_.FODESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MI_FONTE = space(10)
      endif
      this.w_DESFON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])+'\'+cp_ToStr(_Link_.FOCODICE,1)
      cp_ShowWarn(i_cKey,this.TAB_FONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MI_FONTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TAB_FONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TAB_FONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.FOCODICE as FOCODICE103"+ ",link_1_3.FODESCRI as FODESCRI103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on TAB_MISU.MI_FONTE=link_1_3.FOCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and TAB_MISU.MI_FONTE=link_1_3.FOCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMICODICE_1_1.value==this.w_MICODICE)
      this.oPgFrm.Page1.oPag.oMICODICE_1_1.value=this.w_MICODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMIDESCRI_1_2.value==this.w_MIDESCRI)
      this.oPgFrm.Page1.oPag.oMIDESCRI_1_2.value=this.w_MIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMI_FONTE_1_3.value==this.w_MI_FONTE)
      this.oPgFrm.Page1.oPag.oMI_FONTE_1_3.value=this.w_MI_FONTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFON_1_7.value==this.w_DESFON)
      this.oPgFrm.Page1.oPag.oDESFON_1_7.value=this.w_DESFON
    endif
    cp_SetControlsValueExtFlds(this,'TAB_MISU')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_MICODICE)) or not(occur('.',.w_MICODICE)=0 and occur('+',.w_MICODICE)=0  and occur('-',.w_MICODICE)=0 and occur('/',.w_MICODICE)=0 and occur('(',.w_MICODICE)=0 and occur(')',.w_MICODICE)=0 and occur('*',.w_MICODICE)=0))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMICODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_MICODICE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione: non sono ammessi caratteri speciali nel codice misura")
          case   (empty(.w_MI_FONTE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMI_FONTE_1_3.SetFocus()
            i_bnoObbl = !empty(.w_MI_FONTE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_amiPag1 as StdContainer
  Width  = 669
  height = 99
  stdWidth  = 669
  stdheight = 99
  resizeXpos=437
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMICODICE_1_1 as StdField with uid="GSCJZIYEWG",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MICODICE", cQueryName = "MICODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione: non sono ammessi caratteri speciali nel codice misura",;
    ToolTipText = "Codice misura",;
    HelpContextID = 251045621,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=124, Left=90, Top=16, InputMask=replicate('X',15)

  func oMICODICE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (occur('.',.w_MICODICE)=0 and occur('+',.w_MICODICE)=0  and occur('-',.w_MICODICE)=0 and occur('/',.w_MICODICE)=0 and occur('(',.w_MICODICE)=0 and occur(')',.w_MICODICE)=0 and occur('*',.w_MICODICE)=0)
    endwith
    return bRes
  endfunc

  add object oMIDESCRI_1_2 as StdField with uid="FRHRLMTHYD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MIDESCRI", cQueryName = "MIDESCRI",;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della misura",;
    HelpContextID = 200239375,;
   bGlobalFont=.t.,;
    Height=21, Width=573, Left=90, Top=42, InputMask=replicate('X',80)

  add object oMI_FONTE_1_3 as StdField with uid="LXJJERJACL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MI_FONTE", cQueryName = "MICODICE,MI_FONTE",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice fonte associata",;
    HelpContextID = 156100341,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=97, Left=90, Top=72, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TAB_FONT", cZoomOnZoom="gscg_afo", oKey_1_1="FOCODICE", oKey_1_2="this.w_MI_FONTE"

  func oMI_FONTE_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oMI_FONTE_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMI_FONTE_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TAB_FONT','*','FOCODICE',cp_AbsName(this.parent,'oMI_FONTE_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'gscg_afo',"FONTI",'',this.parent.oContained
  endproc
  proc oMI_FONTE_1_3.mZoomOnZoom
    local i_obj
    i_obj=gscg_afo()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FOCODICE=this.parent.oContained.w_MI_FONTE
     i_obj.ecpSave()
  endproc

  add object oDESFON_1_7 as StdField with uid="XVGINTYDIV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESFON", cQueryName = "DESFON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della fonte",;
    HelpContextID = 112284726,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=189, Top=72, InputMask=replicate('X',40)

  add object oStr_1_4 as StdString with uid="FUICSXQDFW",Visible=.t., Left=23, Top=74,;
    Alignment=1, Width=62, Height=19,;
    Caption="Fonte:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="UYJACJKXFG",Visible=.t., Left=9, Top=17,;
    Alignment=1, Width=76, Height=19,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="WDKHUYITKV",Visible=.t., Left=3, Top=46,;
    Alignment=1, Width=82, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_ami','TAB_MISU','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MICODICE=TAB_MISU.MICODICE";
  +" and "+i_cAliasName2+".MI_FONTE=TAB_MISU.MI_FONTE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
