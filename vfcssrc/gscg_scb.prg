* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_scb                                                        *
*              Stampa controllo pdc-bilancio                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_29]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-15                                                      *
* Last revis.: 2014-10-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_scb",oParentObject))

* --- Class definition
define class tgscg_scb as StdForm
  Top    = 30
  Left   = 57

  * --- Standard Properties
  Width  = 418
  Height = 135
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-30"
  HelpContextID=90842473
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_scb"
  cComment = "Stampa controllo pdc-bilancio"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_BILANCIO = space(15)
  w_DATA = ctod('  /  /  ')
  w_attivita = space(1)
  w_passivi = space(1)
  w_costi = space(1)
  w_ricavi = space(1)
  w_ordine = space(1)
  w_transi = space(1)
  w_TIPO = space(1)
  w_Obsoleti = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_scbPag1","gscg_scb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATA_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCG_BCB with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_BILANCIO=space(15)
      .w_DATA=ctod("  /  /  ")
      .w_attivita=space(1)
      .w_passivi=space(1)
      .w_costi=space(1)
      .w_ricavi=space(1)
      .w_ordine=space(1)
      .w_transi=space(1)
      .w_TIPO=space(1)
      .w_Obsoleti=space(1)
        .w_BILANCIO = 'BILANCOGE'
        .w_DATA = I_DATSYS
        .w_attivita = 'A'
        .w_passivi = 'P'
        .w_costi = 'C'
        .w_ricavi = 'R'
          .DoRTCalc(7,8,.f.)
        .w_TIPO = 'M'
        .w_Obsoleti = 'N'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATA_1_4.value==this.w_DATA)
      this.oPgFrm.Page1.oPag.oDATA_1_4.value=this.w_DATA
    endif
    if not(this.oPgFrm.Page1.oPag.oattivita_1_5.RadioValue()==this.w_attivita)
      this.oPgFrm.Page1.oPag.oattivita_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.opassivi_1_6.RadioValue()==this.w_passivi)
      this.oPgFrm.Page1.oPag.opassivi_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ocosti_1_7.RadioValue()==this.w_costi)
      this.oPgFrm.Page1.oPag.ocosti_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oricavi_1_8.RadioValue()==this.w_ricavi)
      this.oPgFrm.Page1.oPag.oricavi_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oordine_1_9.RadioValue()==this.w_ordine)
      this.oPgFrm.Page1.oPag.oordine_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.otransi_1_10.RadioValue()==this.w_transi)
      this.oPgFrm.Page1.oPag.otransi_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oObsoleti_1_14.RadioValue()==this.w_Obsoleti)
      this.oPgFrm.Page1.oPag.oObsoleti_1_14.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_scbPag1 as StdContainer
  Width  = 414
  height = 135
  stdWidth  = 414
  stdheight = 135
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_2 as StdButton with uid="GVHDLWXRYO",left=307, top=84, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare la stampa";
    , HelpContextID = 90813722;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      with this.Parent.oContained
        do GSCG_BCB with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_2.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_BILANCIO) and (.w_costi='C' or .w_attivita='A' or .w_passivi='P' or .w_ricavi='R' or .w_ordine='O' or .w_transi='T'))
      endwith
    endif
  endfunc


  add object oBtn_1_3 as StdButton with uid="CNJAZWKPCA",left=358, top=84, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 83525050;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDATA_1_4 as StdField with uid="TAQSGTKNRB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATA", cQueryName = "DATA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento per il controllo dell'obsolescenza",;
    HelpContextID = 86220746,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=325, Top=15

  add object oattivita_1_5 as StdCheck with uid="AABJQKNNKO",rtseq=3,rtrep=.f.,left=142, top=17, caption="Attivit�",;
    ToolTipText = "Stampa i conti con sezione bilancio = attivit�",;
    HelpContextID = 191272295,;
    cFormVar="w_attivita", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oattivita_1_5.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oattivita_1_5.GetRadio()
    this.Parent.oContained.w_attivita = this.RadioValue()
    return .t.
  endfunc

  func oattivita_1_5.SetRadio()
    this.Parent.oContained.w_attivita=trim(this.Parent.oContained.w_attivita)
    this.value = ;
      iif(this.Parent.oContained.w_attivita=='A',1,;
      0)
  endfunc

  add object opassivi_1_6 as StdCheck with uid="KHJWDXSGZV",rtseq=4,rtrep=.f.,left=142, top=35, caption="Passivit�",;
    ToolTipText = "Stampa i conti con sezione bilancio = passivit�",;
    HelpContextID = 140479754,;
    cFormVar="w_passivi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func opassivi_1_6.RadioValue()
    return(iif(this.value =1,'P',;
    ' '))
  endfunc
  func opassivi_1_6.GetRadio()
    this.Parent.oContained.w_passivi = this.RadioValue()
    return .t.
  endfunc

  func opassivi_1_6.SetRadio()
    this.Parent.oContained.w_passivi=trim(this.Parent.oContained.w_passivi)
    this.value = ;
      iif(this.Parent.oContained.w_passivi=='P',1,;
      0)
  endfunc

  add object ocosti_1_7 as StdCheck with uid="TWTOLQEDQK",rtseq=5,rtrep=.f.,left=142, top=53, caption="Costi",;
    ToolTipText = "Stampa i conti con sezione bilancio = costi",;
    HelpContextID = 241074138,;
    cFormVar="w_costi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func ocosti_1_7.RadioValue()
    return(iif(this.value =1,'C',;
    ' '))
  endfunc
  func ocosti_1_7.GetRadio()
    this.Parent.oContained.w_costi = this.RadioValue()
    return .t.
  endfunc

  func ocosti_1_7.SetRadio()
    this.Parent.oContained.w_costi=trim(this.Parent.oContained.w_costi)
    this.value = ;
      iif(this.Parent.oContained.w_costi=='C',1,;
      0)
  endfunc

  add object oricavi_1_8 as StdCheck with uid="EIYUBJCWFN",rtseq=6,rtrep=.f.,left=142, top=71, caption="Ricavi",;
    ToolTipText = "Stampa i conti con sezione bilancio = ricavi",;
    HelpContextID = 77759722,;
    cFormVar="w_ricavi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oricavi_1_8.RadioValue()
    return(iif(this.value =1,'R',;
    ' '))
  endfunc
  func oricavi_1_8.GetRadio()
    this.Parent.oContained.w_ricavi = this.RadioValue()
    return .t.
  endfunc

  func oricavi_1_8.SetRadio()
    this.Parent.oContained.w_ricavi=trim(this.Parent.oContained.w_ricavi)
    this.value = ;
      iif(this.Parent.oContained.w_ricavi=='R',1,;
      0)
  endfunc

  add object oordine_1_9 as StdCheck with uid="YLDQUYRABG",rtseq=7,rtrep=.f.,left=142, top=89, caption="Ordine",;
    ToolTipText = "Stampa i conti con sezione bilancio = ordine",;
    HelpContextID = 152726554,;
    cFormVar="w_ordine", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oordine_1_9.RadioValue()
    return(iif(this.value =1,'O',;
    ' '))
  endfunc
  func oordine_1_9.GetRadio()
    this.Parent.oContained.w_ordine = this.RadioValue()
    return .t.
  endfunc

  func oordine_1_9.SetRadio()
    this.Parent.oContained.w_ordine=trim(this.Parent.oContained.w_ordine)
    this.value = ;
      iif(this.Parent.oContained.w_ordine=='O',1,;
      0)
  endfunc

  add object otransi_1_10 as StdCheck with uid="DGZMKRGUKK",rtseq=8,rtrep=.f.,left=142, top=107, caption="Transitori",;
    ToolTipText = "Stampa i conti con sezione bilancio = transitori",;
    HelpContextID = 80059338,;
    cFormVar="w_transi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func otransi_1_10.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func otransi_1_10.GetRadio()
    this.Parent.oContained.w_transi = this.RadioValue()
    return .t.
  endfunc

  func otransi_1_10.SetRadio()
    this.Parent.oContained.w_transi=trim(this.Parent.oContained.w_transi)
    this.value = ;
      iif(this.Parent.oContained.w_transi=='T',1,;
      0)
  endfunc

  add object oObsoleti_1_14 as StdCheck with uid="MPRADQIRBK",rtseq=10,rtrep=.f.,left=325, top=53, caption="Obsoleti",;
    ToolTipText = "Stampa i conti obsoleti",;
    HelpContextID = 114061903,;
    cFormVar="w_Obsoleti", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oObsoleti_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oObsoleti_1_14.GetRadio()
    this.Parent.oContained.w_Obsoleti = this.RadioValue()
    return .t.
  endfunc

  func oObsoleti_1_14.SetRadio()
    this.Parent.oContained.w_Obsoleti=trim(this.Parent.oContained.w_Obsoleti)
    this.value = ;
      iif(this.Parent.oContained.w_Obsoleti=='S',1,;
      0)
  endfunc

  add object oStr_1_11 as StdString with uid="HVACPIACQH",Visible=.t., Left=11, Top=17,;
    Alignment=1, Width=127, Height=15,;
    Caption="Sezione bilancio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="TPYXEYIGWA",Visible=.t., Left=255, Top=17,;
    Alignment=1, Width=70, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_scb','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
